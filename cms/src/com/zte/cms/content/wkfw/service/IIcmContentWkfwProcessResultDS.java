package com.zte.cms.content.wkfw.service;

import java.util.List;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwProcessResult;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 提供了用于工作流中心中的业务能力我的申请工单的一套接口
 * 
 * @author 高小芹 gao.xiaoqin@zte.com.cn
 * @version ZXMCISMP-UMAPV2.01.01
 */

public interface IIcmContentWkfwProcessResultDS
{

    /**
     * <p>
     * 根据条件查询我申请的工单数量
     * </p>
     * 
     * @param uappablWkfwCondition UappablWkfwCondition对象，查询条件
     * @return 查询结果
     * @throws DomainServiceException抛出异常
     * 
     */
    public long getMyStartWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition)
            throws DomainServiceException;

    /**
     * <p>
     * 工作流中心中的业务能力我的申请工单接口
     * </p>
     * 根据条件查询UappablWkfwProcessResult对象
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @return 满足条件的UappablWkfwProcessResult对象集
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public List<IcmContentWkfwProcessResult> getUappablWkfwProcessResultByCond(
            IcmContentWkfwCondition contentWkfwCondition) throws DomainServiceException;

    /**
     * <p>
     * 工作流中心中的业务能力我的申请工单接口
     * </p>
     * 根据条件分页查询UappablWkfwProcessResult对象
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @return 查询结果，返回工作流中心中的业务能力我的申请工单查询结果
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询UappablWkfwProcessResult对象，作为查询条件的参数
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

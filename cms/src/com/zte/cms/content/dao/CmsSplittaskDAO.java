package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.dao.ICmsSplittaskDAO;
import com.zte.cms.content.model.CmsSplittask;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsSplittaskDAO extends DynamicObjectBaseDao implements ICmsSplittaskDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsSplittask(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("insert cmsSplittask starting...");
        super.insert("insertCmsSplittask", cmsSplittask);
        log.debug("insert cmsSplittask end");
    }

    public void updateCmsSplittask(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("update cmsSplittask by pk starting...");
        super.update("updateCmsSplittask", cmsSplittask);
        log.debug("update cmsSplittask by pk end");
    }

    public void updateCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("update cmsSplittask by conditions starting...");
        super.update("updateCmsSplittaskByCond", cmsSplittask);
        log.debug("update cmsSplittask by conditions end");
    }

    public void deleteCmsSplittask(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("delete cmsSplittask by pk starting...");
        super.delete("deleteCmsSplittask", cmsSplittask);
        log.debug("delete cmsSplittask by pk end");
    }

    public void deleteCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("delete cmsSplittask by conditions starting...");
        super.delete("deleteCmsSplittaskByCond", cmsSplittask);
        log.debug("update cmsSplittask by conditions end");
    }

    public CmsSplittask getCmsSplittask(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("query cmsSplittask starting...");
        CmsSplittask resultObj = (CmsSplittask) super.queryForObject("getCmsSplittask", cmsSplittask);
        log.debug("query cmsSplittask end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsSplittask> getCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException
    {
        log.debug("query cmsSplittask by condition starting...");
        List<CmsSplittask> rList = (List<CmsSplittask>) super.queryForList("queryCmsSplittaskListByCond", cmsSplittask);
        log.debug("query cmsSplittask by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsSplittask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSplittaskListCntByCond", cmsSplittask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSplittask> rsList = (List<CmsSplittask>) super.pageQuery("queryCmsSplittaskListByCond",
                    cmsSplittask, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsSplittask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsSplittaskListByCond", "queryCmsSplittaskListCntByCond", cmsSplittask,
                start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQuerySqlit starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySplittaskListCntByCond", cmsSplittask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSplittask> rsList = (List<CmsSplittask>) super.pageQuery("querySplittaskListByCond", cmsSplittask,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQuerySqlit end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("querySplittaskListByCond", "querySplittaskListCntByCond", cmsSplittask, start,
                pageSize, puEntity);
    }

}
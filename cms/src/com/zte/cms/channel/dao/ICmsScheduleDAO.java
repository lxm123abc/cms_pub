package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsScheduleDAO
{
    /**
     * 新增CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DAOException dao异常
     */
    public void insertCmsSchedule(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据主键更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DAOException dao异常
     */
    public void updateCmsSchedule(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据条件更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据主键删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DAOException dao异常
     */
    public void deleteCmsSchedule(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据条件删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据主键查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return 满足条件的CmsSchedule对象
     * @throws DAOException dao异常
     */
    public CmsSchedule getCmsSchedule(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据条件查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return 满足条件的CmsSchedule对象集
     * @throws DAOException dao异常
     */
    public List<CmsSchedule> getCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException;

    /**
     * 根据条件分页查询CmsSchedule对象，作为查询条件的参数
     * 
     * @param cmsSchedule CmsSchedule对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSchedule对象，作为查询条件的参数
     * 
     * @param cmsSchedule CmsSchedule对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryForRecord(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSchedule对象，作为查询条件的参数
     * 
     * @param cmsSchedule CmsSchedule对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 查询同一个频道下时间是否有重复
     * 
     * @param icmChannelSchedule
     * @return
     */
    public List<CmsSchedule> isTimeRepeated(CmsSchedule cmsSchedule) throws DAOException;
    
    /**
     * 根据条件查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return 满足条件的CmsSchedule对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSchedule> getCmsScheduleByID(CmsSchedule cmsSchedule) throws DAOException;
}
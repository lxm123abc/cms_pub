package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.model.IcmContentExcelimport;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmContentExcelimportDAO
{
    /**
     * 新增IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @throws DAOException dao异常
     */
    public void insertIcmContentExcelimport(IcmContentExcelimport icmContentExcelimport) throws DAOException;

    /**
     * 根据条件查询IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @return 满足条件的IcmContentExcelimport对象集
     * @throws DAOException dao异常
     */
    public List<IcmContentExcelimport> getIcmContentExcelimportByCond(IcmContentExcelimport icmContentExcelimport)
            throws DAOException;

    /**
     * 根据条件分页查询IcmContentExcelimport对象，作为查询条件的参数
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询IcmContentExcelimport对象，作为查询条件的参数
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
package com.zte.cms.targetSystem.common;

public class TargetSystemConstant {
	    /**
	     * 目标系统管理
	     */
	    public static final String MGTTYPE_TARGETSYSTEM = "log.target.mgt";

	    public static final String TARGET_ID = "log.target.id";
	    
	    /**
	     * opertype 操作类型（String)
	     */
	    public static final String OPERTYPE_TARGET_ADD = "log.target.add";// 目标系统新增
	    public static final String OPERTYPE_TARGET_MOD = "log.target.modify";// 目标系统修改
	    public static final String OPERTYPE_TARGET_DEL = "log.target.delete";// 目标系统注销
	    public static final String OPERTYPE_TARGET_PAU = "log.target.pause";// 目标系统暂停
	    
	    
	    /**
	     * opertypeinfo 操作类型（String)
	     */
	    public static final String OPERTYPE_TARGET_ADD_INFO = "log.target.add.info";
	    public static final String OPERTYPE_TARGET_MOD_INFO = "log.target.modify.info";
	    public static final String OPERTYPE_TARGET_DEL_INFO = "log.target.delete.info";
	    public static final String OPERTYPE_TARGET_PAU_INFO = "log.target.pause.info";

	   // 目标系统管理操作结果资源标识
	    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success"; // 操作成功
	    public static final String RESOURCE_OPERATION_FAIL = "operation.fail"; // 操作失败
}

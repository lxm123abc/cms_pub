package com.zte.cms.content.movie.ls;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.common.Generator;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.UpLoadFile;
import com.zte.cms.common.UpLoadResult;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.modwkfw.ICntModApply;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.ls.IProgramPlatformSynLS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.sapi.ServiceAccess;
import com.zte.sapi.protocol.FTPService;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsMovieLS extends DynamicObjectBaseDS implements ICmsMovieLS
{
	private Log log = SSBBus.getLog(getClass());
    private ICmsMovieDS cmsMovieDS;
    private ICmsMovieBufDS cmsMovieBufDS;
    private ICmsProgramDS cmsProgramDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private IUsysConfigDS usysConfigDS;
    private ICmsFtptaskDS cmsFtptaskDS;
    private ICntModApply cntModApply = null;
    private IIcmSourcefileDS icmSourcefileDS;
    private ICmsLpopNoauditDS cmsLpopNoauditDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IProgramPlatformSynLS programPlatformSynLS;

    public static final String ONLINE_ID = "2"; // 临时区ID

    /**
     * 新增子内容
     */
    /**  
    ResourceMgt.findDefaultText("dest.adress.notfound.checkbind")
      */
    public String insertCmsMovie(CmsMovie cmsMovie, int type) throws Exception
    {
        log.debug("insert cmsMovie starting...");
        
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");
        
        int uploadtype = type; // 上传方式 0:http上传
        String currenttime = null; // 当前时间
        try
        {
            // 查询内容是否存在
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(cmsMovie.getProgramindex());
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if (cmsProgram == null)
            {
                //return "1:操作失败,点播内容不存在";
                return ResourceMgt.findDefaultText("do.fail.demandcnt.notexist");
            }

            int status = cmsProgram.getStatus();
            if (status != 110 && status != 130 && status != 150 && status != 180)
            {
                //return "1:操作失败,点播内容状态不正确";
                return ResourceMgt.findDefaultText("do.fail.demandcnt.status.wrong");
            }

            // 获取movieID
            String movieid = Generator.getPhysicalContentId(ObjectType.MOVI);
            cmsMovie.setMovieid(movieid);

            String desPath = cmsStorageareaLS.getAddress(ONLINE_ID);
            if (null == desPath || ("1056020").equals(desPath)) // 获取在线区地址失败
            {
                //return "1:操作失败,获取在线区地址失败";
                return ResourceMgt.findDefaultText("do.fail.get.onlinearea.adress.faild");
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    //return "1:操作失败,找不到目标地址，请检查存储区绑定";
                    return ResourceMgt.findDefaultText("do.fail.dest.adress.notfound.checkbind");
                }
                else
                {
                    Date date = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    currenttime = dateFormat.format(date);

                    if (uploadtype == 0)// http上传方式
                    {
                        // 判断目标文件夹是否存在，若不存在则创建
                        StringBuffer destfile = new StringBuffer("");
                        destfile.append(GlobalConstants.getMountPoint());
                        destfile.append(desPath);
                        destfile.append(File.separator);
                        destfile.append(currenttime);
                        destfile.append(File.separator);
                        File destDir = new File(destfile.toString());
                        if (!destDir.exists())
                        {// 目标文件夹不存在，创建
                            destDir.mkdirs();
                        }

                        String filename = date.getTime() + "_" + cmsMovie.getFilename();
                        cmsMovie.setFilename(filename);

                        UpLoadResult uploadResult = null;
                        String destpath = GlobalConstants.getMountPoint() + desPath + File.separator + currenttime;
                        uploadResult = UpLoadFile.singleFileUplord(cmsMovie.getFilename(),
                                CntConstants.MAX_FILESIZE, destpath);
                        if (null != uploadResult)
                        {
                            String addresspath = desPath + File.separator + currenttime + File.separator
                                    + cmsMovie.getFilename();// 文件路径
                            int charlength = addresspath.length();
                            if (charlength > 180)
                            {
                            	 return ResourceMgt.findDefaultText("do.fail.filename.outlong");
                            }
                            Integer sizeFlag = uploadResult.getFlag();
                            if (-1 == sizeFlag.intValue())
                            {// 文件大小超过2G
                                //return "1:操作失败,文件大小超过2G";
                                return ResourceMgt.findDefaultText("do.fail.filesize.outtwo");
                            }

                            CmsLpopNoaudit cmsLpopNoaudit = new CmsLpopNoaudit();
                            cmsLpopNoaudit.setCpid(cmsProgram.getCpid());
                            cmsLpopNoaudit.setLptype(4);
                            List<CmsLpopNoaudit> cmsLpopNoauditList = cmsLpopNoauditDS.getCmsLpopNoauditByCond(cmsLpopNoaudit);
                            if(cmsLpopNoauditList!=null && cmsLpopNoauditList.size() >0)
                            {
                                //判断CP是否免审，若是且movie为正片，movie，program状态置为审核通过
                                if(cmsMovie.getMovietype()==1){
                                    cmsMovie.setStatus(0);
                                    cmsProgram.setStatus(0);
                                }else if(cmsMovie.getMovietype()==2)
                                {
                                    //CP为免审，movie为预览片，只把movie状态置为审核通过
                                    cmsMovie.setStatus(0);
                                }
                            }
                            
                            cmsMovie.setFilesize(uploadResult.getFileSize());
                            cmsMovie.setFileurl(addresspath);
                            cmsMovieDS.insertCmsMovie(cmsMovie);

                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(cmsMovie.getMovieid(), CommonLogConstant.MGTTYPE_MOVIE,
                                    CommonLogConstant.OPERTYPE_MOVIE_ADD, CommonLogConstant.OPERTYPE_MOVIE_ADD_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        else
                        {
                            // 文件上传失败
                            //return "1:操作失败,文件上传失败";
                        	return ResourceMgt.findDefaultText("do.fail.file.push.failed");
                        }
                    }else if(uploadtype == 1)
                    {
                    	 String path = cmsMovie.getFilepath();
                    	 File file = new File(GlobalConstants.getMountPoint() + desPath + File.separator + path);
                         if (!file.exists())
                         {
                             //return "1:操作失败,找不到子内容源文件";
                             return ResourceMgt.findDefaultText("do.fail.subcnt.srcfile.cannotfind");
                         }
                         else
                         {
                             int charlength = (desPath + File.separator + path).length();
                             if (charlength > 180)
                             {
                             	 return ResourceMgt.findDefaultText("do.fail.filename.outlong");
                             }
                        	 
                             FileInputStream fis = null;
                             try
                             {
                                 fis = new FileInputStream(file); 
                                 cmsMovie.setFilesize(Long.valueOf(fis.available()));
                                 CmsLpopNoaudit cmsLpopNoaudit = new CmsLpopNoaudit();
                                 cmsLpopNoaudit.setCpid(cmsProgram.getCpid());
                                 cmsLpopNoaudit.setLptype(4);
                                 List<CmsLpopNoaudit> cmsLpopNoauditList = cmsLpopNoauditDS.getCmsLpopNoauditByCond(cmsLpopNoaudit);
                                 if(cmsLpopNoauditList !=null && cmsLpopNoauditList.size() > 0)
                                 {
                                   //判断CP是否免审，若是且movie为正片，movie，program状态置为审核通过
                                     if(cmsMovie.getMovietype()==1){
                                         cmsMovie.setStatus(0);
                                         cmsProgram.setStatus(0);
                                     }else if(cmsMovie.getMovietype()==2)
                                     {
                                         //CP为免审，movie为预览片，只把movie状态置为审核通过
                                         cmsMovie.setStatus(0);
                                     }
                                 }
                                 
                            	 cmsMovie.setFileurl(desPath + File.separator + path);
                            	 cmsMovieDS.insertCmsMovie(cmsMovie);
                             }catch(IOException e)
                             {   
                                 //return "1:操作失败,获取文件大小失败";
                                 return ResourceMgt.findDefaultText("do.fail.get.filesize.failed");
                             }finally
                             {
                            	 if(fis!=null)
                            	 {
                            		 fis.close();
                            	 }
                             }
                        	 
                         }    
                    }else if(uploadtype == 2)
                    {
                    	String[] upload = cmsMovie.getFileurl().split("&");
                    	upload[4] = CntConstants.LINUX_SEPERATOR + upload[4] + CntConstants.LINUX_SEPERATOR;
                    	StringBuffer addresspath = new StringBuffer();
                    	
                        addresspath.append(CntConstants.FTP_PREFIX);
                        addresspath.append(upload[2]); // 用户名
                        addresspath.append(":");
                        addresspath.append(upload[3]); // 密码
                        addresspath.append("@");
                        addresspath.append(upload[0]); // IP
                        addresspath.append(":");
                        addresspath.append(upload[1]); // 端口号
                        addresspath.append(upload[4]); // 文件路径
                        addresspath.append(upload[5]); // 文件名
                        try
                        {
                            // 判断源文件是否存在
                            FTPService ftpService = ServiceAccess.getFTPService();
                            if (!ftpService.connect(upload[0], (Integer.valueOf(upload[1])).intValue(), upload[2], upload[3]))
                            {// 连接失败			
                                //return "1:操作失败,FTP连接失败，请检查网络配置";
                                return ResourceMgt.findDefaultText("do.fail.connect.ftp.failed.check.netset");
                            }
                            else
                            {// 连接成功
                                String[] names = ftpService.listNames(upload[4] + upload[5]);
                                if (null == names || names.length <= 0)
                                {// 源文件不存在
                                    //return "1:操作失败,源文件不存在";
                                    return ResourceMgt.findDefaultText("do.fail.sourcefile.notexist");
                                }
                                ftpService.disconnect();
                            }
                        }
                        catch (Exception e)
                        {
                            log.debug("FTP connection failed, please check network configuration", e);
                           // return "1:操作失败,FTP连接失败，请检查网络配置";
                            return ResourceMgt.findDefaultText("do.fail.connect.ftp.failed.check.netset");
                        }
                        
                        String filename = date.getTime() + "_" + upload[5];
                        String srcaddress = cmsMovie.getFileurl();
                        String destaddress = desPath + File.separator + currenttime + File.separator + filename;
                        int charlength = destaddress.length();
                        if (charlength > 180)
                        {
                        	 return ResourceMgt.findDefaultText("do.fail.filename.outlong");
                        }
                        cmsMovie.setFilename(filename);
                        cmsMovie.setStatus(100);
                        cmsMovie.setFilesize(new Long(0));
                        cmsMovie.setFileurl(destaddress);
                   	    cmsMovieDS.insertCmsMovie(cmsMovie);
                        
                        CmsFtptask cmsFtptask = new CmsFtptask();
                        cmsFtptask.setFileindex(cmsMovie.getMovieindex());
                        cmsFtptask.setTasktype(Long.parseLong("1")); //1.普通类型
                        cmsFtptask.setContentindex(cmsMovie.getProgramindex());
                        cmsFtptask.setUploadtype(3); //内容上传类型 3:从一个ftp
                        cmsFtptask.setSrcaddress(addresspath.toString()); //源文件地址
                        cmsFtptask.setDestaddress(destaddress);
                        cmsFtptask.setStatus(0); //0：待上传
                        cmsFtptask.setSrcfilehandle(0); //0:不处理
                        cmsFtptask.setFiletype(1); //迁移类型 1:MOVIE
                        cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                        
                    }else if(uploadtype == 3)
                    {
                    	 File file = new File(GlobalConstants.getMountPoint() + cmsMovie.getFileurl());
                         if (!file.exists())
                         {
                             //return "1:操作失败,找不到子内容源文件";
                             return ResourceMgt.findDefaultText("do.fail.subcnt.srcfile.cannotfind");
                         }
                    	
                          String filename = date.getTime() + "_" + cmsMovie.getFilename();
                          String srcaddress = cmsMovie.getFileurl();
                          String destaddress = desPath + File.separator + currenttime + File.separator + filename;
                          cmsMovie.setFilename(filename);
                          cmsMovie.setStatus(100);
                          cmsMovie.setFileurl(destaddress);
                          int charlength = destaddress.length();
                          if (charlength > 180)
                          {
                          	 return ResourceMgt.findDefaultText("do.fail.filename.outlong");
                          }                          
                     	  cmsMovieDS.insertCmsMovie(cmsMovie);
                          
                          CmsFtptask cmsFtptask = new CmsFtptask();
                          cmsFtptask.setFileindex(cmsMovie.getMovieindex());
                          cmsFtptask.setTasktype(Long.parseLong("1")); //1.普通类型
                          cmsFtptask.setContentindex(cmsMovie.getProgramindex());
                          cmsFtptask.setUploadtype(2); //内容上传类型 2:本地目录迁移
                          cmsFtptask.setSrcaddress(srcaddress); //源文件地址
                          cmsFtptask.setDestaddress(destaddress);
                          cmsFtptask.setStatus(0); //0：待上传
                          cmsFtptask.setSrcfilehandle(0); //0:不处理
                          cmsFtptask.setFiletype(1); //迁移类型 1:MOVIE
                          cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                    }
                }
                //新增子内容后，更新内容加密状态为未加密
                cmsProgram.setEncryptionstatus(0);
                cmsProgramDS.updateCmsProgram(cmsProgram);
            }
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        
        log.debug("insert cmsMovie end");
        return result;
    }

    /**
     * 获取子内容
     */
    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws Exception
    {
        log.debug("getCmsMovie starting...");
        CmsMovie movie = null;
        try
        {
            movie = cmsMovieDS.getCmsMovie(cmsMovie);
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        log.debug("getCmsMovie end...");
        return movie;
    }

    /**
     * 子内容修改
     * 
     * @param cmsProgram
     * @return
     * @throws Exception
     */
    public String movieModApply(CmsMovie cmsMovie) throws Exception
    {
    	log.debug("movieModApply starting...");
        String result = "0:" + CntUtils.getResourceText(CntConstants.CNT_SUCCESS);
        
        try
        {
            if (cmsMovie.getMovieindex() != null)
            {// 子内容修改触发工作流
                CmsMovie movie = new CmsMovie();
                movie.setMovieindex(cmsMovie.getMovieindex());
                movie = cmsMovieDS.getCmsMovie(movie);
                // 判断子内容状态 如果是待审核110 一审失败130 二审失败150 三审失败180 不走工作流
                if (null == movie)
                {
                    //return "1:子内容不存在";
                    return ResourceMgt.findDefaultText("cnt.subcnt.notexist");
                }

                UsysConfig usysConfig = new UsysConfig();
                usysConfig.setCfgkey("cms.moviereupload.updateurl.switch");
                List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                String updateUrl = "0";
                if (null != usysConfigList && usysConfigList.size() > 0)
                {
                    usysConfig = usysConfigList.get(0);
                    updateUrl = usysConfig.getCfgvalue();
                }
                
                /**
                //判断子内容状态不能为提交加密和加密中和已加密
            	int encryptionstatus = movie.getEncryptionstatus();
            	//2：提交加密，3：加密中，1：已加密
                if(encryptionstatus==2 || encryptionstatus==3 || encryptionstatus==1){
    				return ResourceMgt.findDefaultText("subcnt.status,not.encrypted");
    			}
    			*/
                
                boolean modSyncFlag = false;
                //点播内容在下游平台是否发布，默认为未发布
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(3);
                target.setObjectid(cmsMovie.getProgramid());
                List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                if(targetList!=null && targetList.size()>0){
                    for(CntTargetSync cntTarget:targetList){
                        int cntSyncStatus = cntTarget.getStatus();
                        //点播内容发布状态为发布中、预下线 200：发布中，
                        if(cntSyncStatus==200){
                            return "1:点播内容发布状态为发布中,不允许修改";
                        }else if(cntSyncStatus==300||cntSyncStatus==500)
                        {
                            modSyncFlag = true;
                        }
                    }
                }

                int status = movie.getStatus();
                switch (status)
                {
                    case 110: // 待审核
                    case 130: // 一审失败
                    case 150: // 二审失败
                    case 180: // 三审失败
                        if (updateUrl.equals("1") && null != cmsMovie.getFilename() && !("").equals(cmsMovie.getFilename().trim()))
                        {// 子内容实体文件有修改，需要首先进行HTTP上传
                            String uploadResult = "fail";
                            uploadResult = updateFileUrl(cmsMovie, 0, updateUrl);
                            if (!"success".equals(uploadResult))
                            {// http上传成功
                                return uploadResult;
                            }
                        }
                        
                        cmsMovie.setLastupdatetime(getCurrentTime());
                        cmsMovieDS.updateCmsMovie(cmsMovie);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(movie.getMovieid(), CommonLogConstant.MGTTYPE_MOVIE,
                                CommonLogConstant.OPERTYPE_MOVIE_MODIFY, CommonLogConstant.OPERTYPE_MOVIE_MODIFY_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        break;
                    case 0:
                        //判断CP是否免审，修改后发起修改发布流程
                        CmsProgram cmsProgram = new CmsProgram();
                        cmsProgram.setProgramindex(cmsMovie.getProgramindex());
                        cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
                        CmsLpopNoaudit cmsLpopNoaudit = new CmsLpopNoaudit();
                        cmsLpopNoaudit.setCpid(cmsProgram.getCpid());
                        cmsLpopNoaudit.setLptype(4);
                        List<CmsLpopNoaudit> cmsLpopNoauditList = cmsLpopNoauditDS.getCmsLpopNoauditByCond(cmsLpopNoaudit);
                        if(cmsLpopNoauditList !=null && cmsLpopNoauditList.size() >0)
                        {
                            if (updateUrl.equals("1") && null != cmsMovie.getFilename() && !("").equals(cmsMovie.getFilename().trim()))
                            {// 子内容实体文件有修改，需要首先进行HTTP上传
                                String uploadResult = "fail";
                                uploadResult = updateFileUrl(cmsMovie, 0, updateUrl);
                                if (!"success".equals(uploadResult))
                                {// http上传成功
                                    return uploadResult;
                                }
                            }
                            
                            cmsMovie.setLastupdatetime(getCurrentTime());
                            cmsMovieDS.updateCmsMovie(cmsMovie);
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(movie.getMovieid(), CommonLogConstant.MGTTYPE_MOVIE,
                                    CommonLogConstant.OPERTYPE_MOVIE_MODIFY, CommonLogConstant.OPERTYPE_MOVIE_MODIFY_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            
                            //若点播内容已发布，且CP免审，则movie修改需触发修改发布流程
                            if(modSyncFlag)
                            {
                                programPlatformSynLS.modifyProgramPlatformSyn(cmsProgram.getProgramindex().toString());
                            }
                            
                        }else
                        {
                            result = cntModApply.runModApply(cmsMovie, updateUrl);
                            if (!CntConstants.CNT_SUCCESS.equals(result))
                            {// 提交修改申请失败
                                return "1:" + CntUtils.getResourceText(result);
                            }
                            else
                            {
                                result = "0:" + CntUtils.getResourceText(result);
                            }
                        }
                        
                        break;
                    default:
                        //return "1:子内容状态不正确";
                        return ResourceMgt.findDefaultText("cnt.subcnt.status.wrong");
                }
            }
            else
            {// 子内容新增触发工作流
                result = cntModApply.runModApply(cmsMovie, "1");
                if (!CntConstants.CNT_SUCCESS.equals(result))
                {// 提交修改申请失败
                    return "1:" + CntUtils.getResourceText(result);
                }
                else
                {
                    result = "0:" + CntUtils.getResourceText(result);
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in modMovie method");
            log.error(e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("movieModApply end...");
        return result;
        
    }

    /**
     * 修改CmsProgram对象
     */
    public String updateCmsMovie(CmsMovie cmsMovie) throws Exception
    {
        log.debug("updateCmsMovie starting...");
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");
        CmsMovie movie = null;
        try
        {
            // 验证子内容状态是否可以修改
            movie = new CmsMovie();
            movie.setMovieindex(cmsMovie.getMovieindex());
            movie = cmsMovieDS.getCmsMovie(movie);
            if (movie == null)
            {
                //return "1:操作失败，子内容不存在";
                return ResourceMgt.findDefaultText("do.fail.subcnt.notexist");
            }

            UsysConfig usysConfig = new UsysConfig();
            usysConfig.setCfgkey("cms.moviereupload.updateurl.switch");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            String updateUrl = "0";
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                updateUrl = usysConfig.getCfgvalue();
            }

            int status = movie.getStatus();
            switch (status)
            {
                case 110: // 待审核
                case 130: // 一审失败
                case 150: // 二审失败
                case 180: // 三审失败

                    // String srcaddress =cmsMovie.getFileurl();
                    String urlResult = "fail";
                    if (cmsMovie.getFilename() != null && !cmsMovie.getFilename().trim().equals(""))
                    {
                        urlResult = updateFileUrl(cmsMovie, 0, updateUrl);

                        if (!urlResult.equals("success"))
                        {
                            return urlResult;
                        }
                        // else
                        // {
                        // CmsFtptask cmsFtptask = new CmsFtptask();
                        // cmsFtptask.setFileindex(movie.getMovieindex());
                        // cmsFtptask.setTasktype(Long.parseLong("1")); //发布迁移
                        // cmsFtptask.setContentindex(movie.getProgramindex());
                        // cmsFtptask.setUploadtype(5); //内容上传类型 2:本地文件删除
                        // cmsFtptask.setSrcaddress(srcaddress); //源文件地址
                        // cmsFtptask.setStatus(0); //0：待上传
                        // cmsFtptask.setSrcfilehandle(1); //1：删除
                        // cmsFtptask.setFiletype(1); //迁移类型 1:MOVIE
                        // cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                        // }
                    }

                    cmsMovieDS.updateCmsMovie(cmsMovie);

                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(movie.getMovieid(), CommonLogConstant.MGTTYPE_MOVIE,
                            CommonLogConstant.OPERTYPE_MOVIE_MODIFY, CommonLogConstant.OPERTYPE_MOVIE_MODIFY_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    break;
                default:
                    //return "1:操作失败,子内容状态不正确";
                    return ResourceMgt.findDefaultText("do.fail.subcnt.status.wrong");
            }

        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        log.debug("updateCmsMovie end");
        return result;
    }

    private String updateFileUrl(CmsMovie cmsMovie, int uploadtype, String updateUrl) throws Exception
    {
    	log.debug("updateFileUrl starting...");
        String currenttime = null;
        // 当前时间
        String desPath = cmsStorageareaLS.getAddress(ONLINE_ID);
        if (null == desPath || ("1056020").equals(desPath)) // 获取在线区地址失败
        {
            //return "1:操作失败,获取在线区地址失败";
            return ResourceMgt.findDefaultText("do.fail.get.onlinearea.adress.faild");
        }
        else
        {
            File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
            if (!makePath.exists())
            {// 找不到目标地址，请检查存储区绑定
                //return "1:操作失败,找不到目标地址，请检查存储区绑定";
                return ResourceMgt.findDefaultText(" do.fail.dest.adress.notfound.checkbind");
            }
            else
            {
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                currenttime = dateFormat.format(date);

                if (uploadtype == 0)// http上传方式
                {
                    // 判断目标文件夹是否存在，若不存在则创建
                    StringBuffer destfile = new StringBuffer("");
                    destfile.append(GlobalConstants.getMountPoint());
                    destfile.append(desPath);
                    destfile.append(File.separator);
                    destfile.append(currenttime);
                    destfile.append(File.separator);
                    File destDir = new File(destfile.toString());
                    if (!destDir.exists())
                    {// 目标文件夹不存在，创建
                        destDir.mkdirs();
                    }

                    String filename = date.getTime() + "_" + cmsMovie.getFilename();
                    cmsMovie.setFilename(filename);

                    UpLoadResult uploadResult = null;
                    String destpath = GlobalConstants.getMountPoint() + desPath + File.separator + currenttime;
                    uploadResult = UpLoadFile.singleFileUplord(cmsMovie.getFilename(), CntConstants.MAX_FILESIZE,
                            destpath);
                    if (null != uploadResult)
                    {
                        String addresspath = desPath + File.separator + currenttime + File.separator
                                + cmsMovie.getFilename();// 文件路径
                        Integer sizeFlag = uploadResult.getFlag();
                        if (-1 == sizeFlag.intValue())
                        {// 文件大小超过2G
                            //return "1:操作失败,文件大小超过2G";
                            return ResourceMgt.findDefaultText(" do.fail.filesize.outtwo");
                        }

                        if (updateUrl.equals("1"))
                        {
                            cmsMovie.setFilesize(uploadResult.getFileSize());
                            cmsMovie.setFileurl(addresspath);
                        }
                    }
                    else
                    {
                        // 文件上传失败
                       // return "1:操作失败,文件上传失败";
                        return ResourceMgt.findDefaultText("do.fail.file.push.failed");
                    }
                }
            }
        }
        log.debug("updateFileUrl end...");
        return "success";
    }

    /**
     * 删除CmsMovie对象
     */
    public String deleteCmsMovie(String movieIndex) throws Exception
    {
        log.debug("deleteCmsMovie starting...");
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");
        CmsMovie cmsMovie = null;
        try
        {
            cmsMovie = new CmsMovie();
            cmsMovie.setMovieindex(Long.valueOf(movieIndex));
            cmsMovie = cmsMovieDS.getCmsMovie(cmsMovie);
            if (cmsMovie == null)
            {
                //return "1:操作失败,子内容不存在";
                return ResourceMgt.findDefaultText("do.fail.subcnt.notexist");
            }

            int count = 0;
            int num = 0;
            int total = 0;
            Long programindex = cmsMovie.getProgramindex();
            CmsMovie movie = new CmsMovie();
            movie.setProgramindex(programindex);
            
            // 判断内容的状态是否可以删除
            int status = cmsMovie.getStatus();
        	int encryptionstatus = cmsMovie.getEncryptionstatus();
            switch (status)
            {
                case 110: // 待审核
                case 130: // 一审失败
                case 150: // 二审失败
                case 160: // 回收站
                case 180: // 三审失败
                	//判断子内容状态不能为提交加密和加密中 2：提交加密，3：加密中
                	if(encryptionstatus==2 || encryptionstatus==3){
        				return ResourceMgt.findDefaultText("subcnt.status,not.encrypting");
        			}
                	
                	CntTargetSync target = new CntTargetSync();
                    target.setObjecttype(3);
                    target.setObjectid(cmsMovie.getProgramid());
                    List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    if(targetList!=null && targetList.size()>0){
                        for(CntTargetSync cntTarget:targetList){
                            int cntSyncStatus = cntTarget.getStatus();
                            if(cntSyncStatus==200||cntSyncStatus==300||cntSyncStatus==500||cntSyncStatus==600){
                                return "点播内容发布状态不正确，不允许删除";
                            }
                        }
                    }
                	
                    // 判断是否存在相同的文件删除任务，若不存在插入文件删除任务，否则需要插入
                    List<CmsFtptask> cmsFtptaskList = null;
                    CmsFtptask cmsFtptaskCond = new CmsFtptask();

                    cmsFtptaskCond.setContentindex(cmsMovie.getProgramindex());
                    cmsFtptaskCond.setFileindex(cmsMovie.getMovieindex());
                    cmsFtptaskCond.setUploadtype(5); // 5本地文件删除
                    cmsFtptaskCond.setSrcaddress(cmsMovie.getFileurl());
                    cmsFtptaskCond.setStatus(0); // 0：待上传

                    try
                    {
                        cmsFtptaskList = cmsFtptaskDS.getCmsFtptaskByCond(cmsFtptaskCond);
                    }
                    catch (Exception e)
                    {
                        log.error("Error occurred in getCmsFtptaskByCond method", e);
                        throw new Exception(ResourceManager.getResourceText(e));
                    }

                    if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
                    {// 不存在相同的文件删除任务
                        CmsFtptask cmsFtptask = new CmsFtptask();
                        cmsFtptask.setFiletype(1);
                        cmsFtptask.setTasktype(Long.parseLong(1+""));
                        cmsFtptask.setContentindex(cmsMovie.getProgramindex());
                        cmsFtptask.setFileindex(cmsMovie.getMovieindex());
                        cmsFtptask.setUploadtype(5); // 5本地文件删除
                        cmsFtptask.setSrcaddress(cmsMovie.getFileurl());
                        cmsFtptask.setStatus(0); // 0：待上传
                        cmsFtptask.setSrcfilehandle(0); // 0：不处理

                        try
                        {
                            cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                        }
                        catch (Exception e)
                        {
                            log.error("Error occurred in insertCmsFtptask method", e);
                            throw new Exception(ResourceManager.getResourceText(e));
                        }
                        
                        //当子内容加密状态为已加密时，删除加密后文件
                        if(encryptionstatus==1 && cmsMovie.getEncryptiontype() == 1){
                        	CmsFtptask task = new CmsFtptask();
                        	task.setFiletype(1);
                        	task.setTasktype(Long.parseLong(1+""));
                        	task.setFileindex(cmsMovie.getMovieindex());
                        	task.setUploadtype(5); // 5本地文件删除
                        	task.setSrcaddress(cmsMovie.getEncryptionfilepath());
                        	task.setStatus(0); // 0：待上传
                        	task.setSrcfilehandle(0); // 0：不处理

                            try
                            {
                                cmsFtptaskDS.insertCmsFtptask(task);
                            }
                            catch (Exception e)
                            {
                                log.error("Error occurred in insertCmsFtptask method", e);
                                throw new Exception(ResourceManager.getResourceText(e));
                            }
                        }else if(encryptionstatus==1 && cmsMovie.getEncryptiontype() == 2){
                        	CmsFtptask task = new CmsFtptask();
                        	task.setFiletype(1);
                        	task.setTasktype(Long.parseLong(1+""));
                        	task.setFileindex(cmsMovie.getMovieindex());
                        	task.setUploadtype(6); // 6删除文件夹
                        	task.setSrcaddress(cmsMovie.getEncryptionfilepath());
                        	task.setStatus(0); // 0：待上传
                        	task.setSrcfilehandle(0); // 0：不处理

                            try
                            {
                                cmsFtptaskDS.insertCmsFtptask(task);
                            }
                            catch (Exception e)
                            {
                                log.error("Error occurred in insertCmsFtptask method", e);
                                throw new Exception(ResourceManager.getResourceText(e));
                            }
                        }
                    }
                    
                    cmsMovieDS.removeCmsMovie(cmsMovie);
                    
                    CmsProgram program = new CmsProgram();
                	program.setProgramindex(programindex);
                	program = cmsProgramDS.getCmsProgram(program);
                    List<CmsMovie> movieList = cmsMovieDS.getCmsMovieByCond(movie);
                    if(movieList!=null&&movieList.size()>0){
                    	//当删除的子内容加密状态为未加密和加密失败时 0：未加密 4：加密失败 
                    	if(encryptionstatus==0 || encryptionstatus==4){
                    		for(CmsMovie movielist:movieList){
                    			if(movielist.getEncryptionstatus() == 1){
                    				count++;
                    			}else if(movielist.getEncryptionstatus() == 2 || movielist.getEncryptionstatus() == 3){
                    				num++;
                    			}
                    		}
                    		total = count + num;
                    		//被删除子内容所属内容下其余子内容加密状态都为已加密，更新内容加密状态为已加密
                    		if(count == movieList.size()){
                    			program.setEncryptionstatus(1);
                    	        try
                    	        {
                    	        	cmsProgramDS.updateCmsProgram(program);
                    	        }
                    	        catch (Exception e)
                    	        {
                    	            log.error("cmsMovieLS exception:" + e.getMessage());
                    	        }
                    		}else if(num == movieList.size() || total== movieList.size()){
                    			//被删除子内容所属内容下其余子内容加密状态为已加密或者是包含加密中，更新内容加密状态为加密中
                    			program.setEncryptionstatus(2);
                    	        try
                    	        {
                    	        	cmsProgramDS.updateCmsProgram(program);
                    	        }
                    	        catch (Exception e)
                    	        {
                    	            log.error("cmsMovieLS exception:" + e.getMessage());
                    	        }
                    		}
                    	}
                    }else{
                    	//被删除子内容所属内容下没有其它子内容时，更新内容加密状态为未加密
                    	program.setEncryptionstatus(0);
                        
                        try
                        {
                        	cmsProgramDS.updateCmsProgram(program);
                        }
                        catch (Exception e)
                        {
                            log.error("cmsMovieLS exception:" + e.getMessage());
                        }
                    }

                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(cmsMovie.getMovieid(), CommonLogConstant.MGTTYPE_MOVIE,
                            CommonLogConstant.OPERTYPE_MOVIE_DELETE, CommonLogConstant.OPERTYPE_MOVIE_DELETE_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    break;
                default:
                    //return "1:操作失败,子内容状态不正确";
                    return ResourceMgt.findDefaultText("do.fail.subcnt.status.wrong");
            }

        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        log.debug("deleteCmsMovie starting...");
        return result;
    }

    /**
     * 查询子内容列表
     */
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = cmsMovieDS.pageInfoQuery(cmsMovie, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return dataInfo;
    }

    /**
     * 查询子内容列表
     */
    public TableDataInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQueryEncryption starting...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = cmsMovieDS.pageInfoQueryEncryption(cmsMovie, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
        }
        log.debug("pageInfoQueryEncryption end");
        return dataInfo;
    }    
    
    /**
     * 获取子内容预览路径
     */
    public String getPreviewPath(Long movieindex) throws Exception
    {
        log.debug("getPreviewPath starting...");

        CmsMovie cmsMovie = new CmsMovie();
        UsysConfig usysConfig = new UsysConfig();
        String previewPath = "";
        try
        {
            usysConfig.setCfgkey("cms.preview.path");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                cmsMovie.setMovieindex(movieindex);
                cmsMovie = cmsMovieDS.getCmsMovie(cmsMovie);
                if (cmsMovie == null)
                {
                    //throw new Exception("子内容不存在");
                    throw new Exception(ResourceMgt.findDefaultText("subcnt.notexist"));
                }
                else
                {
                    previewPath = usysConfig.getCfgvalue() + cmsMovie.getFileurl();
                }
            }
            else
            {// "272201"：未配置 系统参数WINDOWS共享访问点
                //throw new Exception("未配置系统参数WINDOWS共享访问点");
                throw new Exception(ResourceMgt.findDefaultText("parameter.notset.windows.sharepoint"));
            }
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("getPreviewPath end");
        return previewPath;
    }

    /**
     * 获取子内容预览路径//cms.windows.share \\10.129.170.22\root\ +路径形式
     */
    public String getPreviewPath2(Long movieindex) throws Exception
    {
        log.debug("getPreviewPath starting...");

        CmsMovie cmsMovie = new CmsMovie();
        UsysConfig usysConfig = new UsysConfig();
        String previewPath = "";
        try
        {
            usysConfig.setCfgkey("cms.preview.path");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                cmsMovie.setMovieindex(movieindex);
                cmsMovie = cmsMovieDS.getCmsMovie(cmsMovie);
                if (cmsMovie == null)
                {
                    //throw new Exception("子内容不存在");
                    throw new Exception(ResourceMgt.findDefaultText("subcnt.notexist"));
                    
                }
                else
                {
                    previewPath = usysConfig.getCfgvalue() + cmsMovie.getFileurl();
                }
            }
            else
            {// "272201"：未配置 系统参数WINDOWS共享访问点
                //throw new Exception("未配置系统参数WINDOWS共享访问点");
                throw new Exception(ResourceMgt.findDefaultText("parameter.notset.windows.sharepoint"));
            }
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("getPreviewPath end");
        return previewPath;
    }

    /**
     * 根据 programid 来查询出 movie信息
     */
    public List<CmsMovie> getCmsMovieByProgramid(String programid) throws Exception
    {
        log.debug("getCmsMovieByProgramid begin........");
        List<CmsMovie> movieList = null;
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setProgramid(programid);
        movieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);

        log.debug("getCmsMovieByProgramid end........");
        return movieList;
    }

    /**
     * 获取子内容缓存表预览路径
     */
    public String getCmsMovieBufPreviewPath(Long movieindex) throws Exception
    {
        log.debug("getPreviewPath starting...");

        CmsMovieBuf cmsMovieBuf = new CmsMovieBuf();
        UsysConfig usysConfig = new UsysConfig();
        String previewPath = "";
        try
        {
            usysConfig.setCfgkey("cms.preview.path");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                cmsMovieBuf.setMovieindex(movieindex);
                cmsMovieBuf = cmsMovieBufDS.getCmsMovieBuf(cmsMovieBuf);
                if (cmsMovieBuf == null)
                {
                   // throw new Exception("子内容不存在");
                    throw new Exception(ResourceMgt.findDefaultText("subcnt.notexist"));
                }
                else
                {
                    previewPath = usysConfig.getCfgvalue() + cmsMovieBuf.getFileurl();
                }
            }
            else
            {// "272201"：未配置 系统参数WINDOWS共享访问点
                //throw new Exception("未配置系统参数WINDOWS共享访问点");
                throw new Exception(ResourceMgt.findDefaultText("parameter.notset.windows.sharepoint"));
            }
        }
        catch (Exception e)
        {
            log.error("cmsMovieLS exception:" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("getPreviewPath end");
        return previewPath;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 获取时间 如"20111002"
    private static String getCurrentTime() throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    // 获取在线区地址
    public String getOnlinePath()throws Exception
    {
        log.debug("getOnlinePath starting...");
        String onlineArea = "";
        // 获取在线区地址
        try
        {
            onlineArea = cmsStorageareaLS.getAddress(CntConstants.STORAGEAREA_ONLINE); // 2：在线区
        }
        catch (Exception e)
        {
            log.error("Error occurred in getAddress method",e);
        }
        log.debug("getOnlinePath end...");
        return onlineArea+File.separator;
    } 
    
    // 获取制作区地址
    public String getMakeAreaPath()throws Exception
    {
        log.debug("getOnlinePath starting...");
        String onlineArea = "";
        // 获取在线区地址
        try
        {
            onlineArea = cmsStorageareaLS.getAddress("4"); // 2：在线区
        }
        catch (Exception e)
        {
            log.error("Error occurred in getAddress method",e);
        }
        log.debug("getOnlinePath end...");
        return onlineArea+File.separator;
    }
    
    
    
    public CmsMovie getSourcefile(Long fileindex)
    {
    	log.debug("getSourcefile starting...");
    	CmsMovie cmsMovie = new CmsMovie();
    	IcmSourcefile icmSourcefile = new IcmSourcefile();
    	icmSourcefile.setFileindex(fileindex);
    	try {
			icmSourcefile=icmSourcefileDS.getIcmSourcefile(icmSourcefile);
			cmsMovie.setNamecn(icmSourcefile.getNamecn());
			cmsMovie.setFilesize(Long.valueOf(icmSourcefile.getFilesize()));
			cmsMovie.setFileurl(icmSourcefile.getAddresspath());
			cmsMovie.setMovietype(icmSourcefile.getFiletype());
			cmsMovie.setSystemlayer(Integer.valueOf(icmSourcefile.getFormatindex().toString()));
			cmsMovie.setBitratetype(getBitratetype(icmSourcefile.getBitrate()));
			cmsMovie.setFilename(icmSourcefile.getFilename());
			cmsMovie.setDuration(getDuration(icmSourcefile.getDuration()));
			cmsMovie.setVideotype(icmSourcefile.getEncodingformat());
		} catch (Exception e) {
			 log.error("cmsMovieLS exception:" + e.getMessage());
		}
		log.debug("getSourcefile end...");
    	return cmsMovie;
    }
    
    /**
     * 将母片播放时长转换成子内容的播放时长
     * @param duration 母片播放时长 单位：秒
     * @return 子内容的播放时长
     */
    private String getDuration(String duration)
    {
        String value="";
        int i = Integer.valueOf(duration);
        int j = i/3600; //计算HH
        if(j < 10)
        {
            value = "0" + j;
        }else {
            value = "" + j;
        }
        
        int k = i%3600; //计算MI
        int x = k/60;
        if(x < 10)
        {
            value = value + "0" + x;
        }else {
            value = value + x;
        }
        
        int y = k%60;  //计算SS
        if(y < 10)
        {
            value = value + "0" + y;
        }else {
            value = value + y;
        }
        
        value = value + "00";
        
        return value;
    }
    
    private int getBitratetype(String bitrate)
    {
    	int bitratetype = Integer.valueOf(bitrate);
    	if(bitratetype == 409600)
    	{
    		return 1;
    	}else if(bitratetype == 716800)
    	{
    		return 2;
	    }else if(bitratetype == 1363149)
	    {
	    	return 3;
		}else if(bitratetype == 2097152)
		{
			return 4;
		}else if(bitratetype == 2621440)
		{
			return 5;
		}else if(bitratetype == 8388608)
		{
			return 6;
		}else if(bitratetype == 10485760)
		{
			return 7;
		}else
		{
			return 0;
		}
	    	
    }
    
    public String getCpid(Long programindex)
    {
    	log.debug("getCpid starting...");
    	CmsProgram cmsProgram = new CmsProgram();
    	String cpid = "";
    	try {
        	cmsProgram.setProgramindex(programindex);
			cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
			cpid = cmsProgram.getCpid();
		} catch (DomainServiceException e) {
			 log.error("cmsMovieLS exception:" + e.getMessage());
		}
		log.debug("getCpid end...");
    	return cpid;
    }
    
    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }

    public void setCntModApply(ICntModApply cntModApply)
    {
        this.cntModApply = cntModApply;
    }

    public void setCmsMovieBufDS(ICmsMovieBufDS cmsMovieBufDS)
    {
        this.cmsMovieBufDS = cmsMovieBufDS;
    }

	public void setIcmSourcefileDS(IIcmSourcefileDS icmSourcefileDS) {
		this.icmSourcefileDS = icmSourcefileDS;
	}

    public ICmsLpopNoauditDS getCmsLpopNoauditDS()
    {
        return cmsLpopNoauditDS;
    }

    public void setCmsLpopNoauditDS(ICmsLpopNoauditDS cmsLpopNoauditDS)
    {
        this.cmsLpopNoauditDS = cmsLpopNoauditDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setProgramPlatformSynLS(IProgramPlatformSynLS programPlatformSynLS)
    {
        this.programPlatformSynLS = programPlatformSynLS;
    }

}


var Main_Tab	= null;
var cur_row	= null;
var cur_col	= null;
var cur_cell	= null;
var Org_con	= "";
var sort_col	= null;

var show_col	= false;
var charMode	= true;
var act_bgc	= "#BEC5DE";
var act_fc	= "black";
var cur_bgc	= "#ccffcc";
var cur_fc	= "black";

function fltable_init(){
	cur_row			= null;
	cur_col			= null;
	cur_cell		= null;
	sort_col		= null;
	Main_Tab 		= PowerTable;
	read_def(Main_Tab)
	Main_Tab.onmouseover	= overIt;
	Main_Tab.onmouseout	= outIt;
	Main_Tab.onclick	= clickIt;
	Main_Tab.ondblclick	= dblclickIt;
	Org_con			= Main_Tab.outerHTML;
	
	arrowUp = document.createElement("SPAN");
	arrowUp.innerHTML	= "5";
	arrowUp.style.cssText 	= "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

	arrowDown = document.createElement("SPAN");
	arrowDown.innerHTML	= "6";
	arrowDown.style.cssText = "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

        drag = document.createElement("DIV");
	drag.innerHTML		= "";
	drag.style.textAlign 	= "center";
	drag.style.position 	= "absolute";
	//drag.style.cursor 	= "hand";
	drag.style.border 	= "1 solid black";
	drag.style.display 	= "none";
	drag.style.zIndex 	= "999";
	
	document.body.insertBefore(drag);
	setInterval("judge_move()",100);
}

function judge_move(){
	document.all.fltable_move[0].disabled=(cur_row == null || cur_row<=1);
	document.all.fltable_move[1].disabled=(cur_row == null || cur_row==Main_Tab.rows.length-1 || cur_row == 0);
	document.all.fltable_move[2].disabled=(cur_col == null || cur_col==0);
	document.all.fltable_move[3].disabled=(cur_col == null || cur_col==Main_Tab.rows[0].cells.length-1);
}



document.onmouseup	= drag_end;

function clear_color(){
	the_table=Main_Tab;
	if(cur_col!=null){
		for(i=0;i<the_table.rows.length;i++){
			with(the_table.rows[i].cells[cur_col]){
				style.backgroundColor=oBgc;
				style.color=oFc;
			}
		}
	}
	if(cur_row!=null){
		for(i=0;i<the_table.rows[cur_row].cells.length;i++){
			with(the_table.rows[cur_row].cells[i]){
				style.backgroundColor=oBgc;
				style.color=oFc;
			}
		}
	}
	if(cur_cell!=null){
		cur_cell.children[0].contentEditable = false;
		with(cur_cell.children[0].runtimeStyle){
			borderLeft=borderTop="";
			borderRight=borderBottom="";
			backgroundColor="";
			paddingLeft="";
			textAlign="";
		}
	}
}

function document.onclick(){
	window.status = "";
	clear_color();
	cur_row  = null;
	cur_col  = null;
	cur_cell = null;
}

function read_def(the_table){
	for(var i=0;i<the_table.rows.length;i++){
		for(var j=0;j<the_table.rows[i].cells.length;j++){
			with(the_table.rows[i]){
				cells[j].oBgc = cells[j].currentStyle.backgroundColor;
				cells[j].oFc  = cells[j].currentStyle.color;
				if(i==0){
					//cells[j].onmousedown	= drag_start;
					//cells[j].onmouseup	= drag_end;
				}
			}
		}
	}
}

function get_Element(the_ele,the_tag){
	the_tag = the_tag.toLowerCase();
	if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
	while(the_ele=the_ele.offsetParent){
		if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
	}
	return(null);
}

var dragStart		= false;
var dragColStart	= null;
var dragColEnd		= null;

function drag_start(){
	
	var the_td	= get_Element(event.srcElement,"td");
	if(the_td==null) return;
	dragStart	= true;
	dragColStart	= the_td.cellIndex;
	drag.style.width	= the_td.offsetWidth;
	drag.style.height	= the_td.offsetHeight;
	function document.onmousemove(){
		drag.style.display	= "";
		drag.style.top		= event.y - drag.offsetHeight/2;
		drag.style.left		= event.x - drag.offsetWidth/2;
		for(var i=0;i<Main_Tab.rows[0].cells.length;i++){
			with(Main_Tab.rows[0].cells[i]){
				if((event.y>offsetTop+parseInt(document.body.currentStyle.marginTop) && event.y<offsetTop+offsetHeight+parseInt(document.body.currentStyle.marginTop)) && (event.x>offsetLeft+parseInt(document.body.currentStyle.marginLeft) && event.x<offsetLeft+offsetWidth+parseInt(document.body.currentStyle.marginLeft))){
					runtimeStyle.backgroundColor=act_bgc;
					dragColEnd=cellIndex;
				}else{
					runtimeStyle.backgroundColor="";
				}
			}
		}
		if(!(event.y>Main_Tab.rows[0].offsetTop+parseInt(document.body.currentStyle.marginTop) && event.y<Main_Tab.rows[0].offsetTop+Main_Tab.rows[0].offsetHeight+parseInt(document.body.currentStyle.marginTop))) dragColEnd=null;
	}
	drag.innerHTML = the_td.innerHTML;
	drag.style.backgroundColor = the_td.oBgc;
	drag.style.color = the_td.oFc;
}

function drag_end(){
	
	dragStart = false;
	drag.style.display="none";
	drag.innerHTML = "";
	drag.style.width = 0;
	drag.style.height = 0;
	for(var i=0;i<Main_Tab.rows[0].cells.length;i++){
		Main_Tab.rows[0].cells[i].runtimeStyle.backgroundColor="";
	}
	if(dragColStart!=null && dragColEnd!=null && dragColStart!=dragColEnd){
		change_col(Main_Tab,dragColStart,dragColEnd);
		if(dragColStart==sort_col)sort_col=dragColEnd;
		else if(dragColEnd==sort_col)sort_col=dragColStart;
		document.onclick();
	}
	dragColStart = null;
	dragColEnd = null;
	document.onmousemove=null;
}

function clickIt(){
	event.cancelBubble=true;
	var the_obj = event.srcElement;
	var i = 0 ,j = 0;
	if(cur_cell!=null && cur_row!=0){
		cur_cell.children[0].contentEditable = false;
		with(cur_cell.children[0].runtimeStyle){
			borderLeft=borderTop="";
			borderRight=borderBottom="";
			backgroundColor="";
			paddingLeft="";
			textAlign="";
		}
	}
	if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "tr"){
		var the_td	= get_Element(the_obj,"td");
		if(the_td==null) return;
		var the_tr	= the_td.parentElement;
		var the_table	= get_Element(the_td,"table");
		var i 		= 0;
		clear_color();
		cur_row = the_tr.rowIndex;
		cur_col = the_td.cellIndex;
		if(cur_row!=0){
			for(i=0;i<the_tr.cells.length;i++){
				with(the_tr.cells[i]){
					style.backgroundColor=cur_bgc;
					style.color=cur_fc;
				}
			}
		}else{
			if(show_col){
				for(i=1;i<the_table.rows.length;i++){
					with(the_table.rows[i].cells[cur_col]){
						style.backgroundColor=cur_bgc;
						style.color=cur_fc;
					}
				}
			}
			
			the_td.mode = !the_td.mode;
			if(sort_col!=null){
				with(the_table.rows[0].cells[sort_col])
					removeChild(lastChild);
			}
			with(the_table.rows[0].cells[cur_col])
				appendChild(the_td.mode?arrowUp:arrowDown);
			sort_tab(the_table,cur_col,the_td.mode);
			sort_col=cur_col;
		}
	}
}

function dblclickIt(){
        return;
	event.cancelBubble=true;
	if(cur_row!=0){
		var the_obj = event.srcElement;
		if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "tr"){
			var the_td	= get_Element(the_obj,"td");
			if(the_td==null) return;
			cur_cell	= the_td;
			if(the_td.children.length!=1)
				the_td.innerHTML="<div>" + the_td.innerHTML + "</div>";
			else if(the_td.children.length==1 && the_td.children[0].tagName.toLowerCase()!="div")
				the_td.innerHTML="<div>" + the_td.innerHTML + "</div>";
			cur_cell.children[0].contentEditable = true;
			with(cur_cell.children[0].runtimeStyle){
				borderRight=borderBottom="buttonhighlight 1px solid";
				borderLeft=borderTop="black 1px solid";
				backgroundColor="#dddddd";
				paddingLeft="5px";
				//textAlign="center";
			}
		}
	}
}

function overIt(){
	if(dragStart)return;
	var the_obj = event.srcElement;
	var i = 0;
	if(the_obj.tagName.toLowerCase() != "table"){
		var the_td	= get_Element(the_obj,"td");
		if(the_td==null) return;
		var the_tr	= the_td.parentElement;
		var the_table	= get_Element(the_td,"table");
		if(the_tr.rowIndex!=0){
			for(i=0;i<the_tr.cells.length;i++){
				with(the_tr.cells[i]){
					runtimeStyle.backgroundColor=act_bgc;
					runtimeStyle.color=act_fc;					
				}
			}
		}else{
			for(i=1;i<the_table.rows.length;i++){
				with(the_table.rows[i].cells(the_td.cellIndex)){
					runtimeStyle.backgroundColor=act_bgc;
					runtimeStyle.color=act_fc;
				}
			}
			if(the_td.mode==undefined)the_td.mode = false;
			//the_td.style.cursor=the_td.mode?"n-resize":"s-resize";
		}
	}
}

function outIt(){
	var the_obj = event.srcElement;
	var i=0;
	if(the_obj.tagName.toLowerCase() != "table"){
		var the_td	= get_Element(the_obj,"td");
		if(the_td==null) return;
		var the_tr	= the_td.parentElement;
		var the_table	= get_Element(the_td,"table");
		if(the_tr.rowIndex!=0){
			for(i=0;i<the_tr.cells.length;i++){
				with(the_tr.cells[i]){
					runtimeStyle.backgroundColor='';
					runtimeStyle.color='';				
				}
			}
		}else{
			var the_table=the_tr.parentElement.parentElement;
			for(i=0;i<the_table.rows.length;i++){
				with(the_table.rows[i].cells(the_td.cellIndex)){
					runtimeStyle.backgroundColor='';
					runtimeStyle.color='';
				}
			}
		}
	}
}

var charPYStr = "";
var charBHStr = "";
function judge_CN(char1,char2,mode){
    var charSet=charMode?charPYStr:charBHStr;
	for(var n=0;n<(char1.length>char2.length?char1.length:char2.length);n++){
		if(char1.charAt(n)!=char2.charAt(n)){
			if(mode) return(charSet.indexOf(char1.charAt(n))>charSet.indexOf(char2.charAt(n))?1:-1);
			else	 return(charSet.indexOf(char1.charAt(n))<charSet.indexOf(char2.charAt(n))?1:-1);
			break;
		}
	}
	return(0);
}

function sort_tab(the_tab,col,mode){
	var tab_arr = new Array();
	var i;
	var start=new Date;
	for(i=1;i<the_tab.rows.length;i++){
		tab_arr.push(new Array(the_tab.rows[i].cells[col].innerText.toLowerCase(),the_tab.rows[i]));
	}
	function SortArr(mode) {
		return function (arr1, arr2){
			var flag;
			var a,b;
			a = arr1[0];
			b = arr2[0];
			if(/^(\+|-)?\d+($|\.\d+$)/.test(a) && /^(\+|-)?\d+($|\.\d+$)/.test(b)){
				a=eval(a);
				b=eval(b);
				flag=mode?(a>b?1:(a<b?-1:0)):(a<b?1:(a>b?-1:0));
			}else{
				a=a.toString();
				b=b.toString();
				if(a.charCodeAt(0)>=19968 && b.charCodeAt(0)>=19968){
					flag = judge_CN(a,b,mode);
				}else{
					flag=mode?(a>b?1:(a<b?-1:0)):(a<b?1:(a>b?-1:0));
				}
			}
			return flag;
		};
	}
	tab_arr.sort(SortArr(mode));

	for(i=0;i<tab_arr.length;i++){
		the_tab.lastChild.appendChild(tab_arr[i][1]);
	}

	window.status = " (Time spent: " + (new Date - start) + "ms)";
}

function change_row(the_tab,line1,line2){
	the_tab.rows[line1].swapNode(the_tab.rows[line2])
}

function change_col(the_tab,line1,line2){
	for(var i=0;i<the_tab.rows.length;i++)
		the_tab.rows[i].cells[line1].swapNode(the_tab.rows[i].cells[line2]);
}

function Move_up(the_table){
	event.cancelBubble=true;
	if(cur_row==null || cur_row<=1)return;
	change_row(the_table,cur_row,--cur_row);
}

function Move_down(the_table){
	event.cancelBubble=true;
	if(cur_row==null || cur_row==the_table.rows.length-1 || cur_row==0)return;
	change_row(the_table,cur_row,++cur_row);
}

function Move_left(the_table){
	event.cancelBubble=true;
	if(cur_col==null || cur_col==0)return;
	change_col(the_table,cur_col,--cur_col);
	if(cur_col==sort_col)sort_col=cur_col+1;
	else if(cur_col+1==sort_col)sort_col=cur_col;
}

function Move_right(the_table){
	event.cancelBubble=true;
	if(cur_col==null || cur_col==the_table.rows[0].cells.length-1)return;
	change_col(the_table,cur_col,++cur_col);
	if(cur_col==sort_col)sort_col=cur_col-1;
	else if(cur_col-1==sort_col)sort_col=cur_col;
}

function add_row(the_table,dataArray) {
	event.cancelBubble=true;
	var the_row,the_cell;
	the_row = cur_row==null?-1:(cur_row+1);
	clear_color();
	var newrow=the_table.insertRow(the_row);
	for (var i=0;i<the_table.rows[0].cells.length;i++) {
		the_cell=newrow.insertCell(i);
		//the_cell.innerText="NewRow_" + the_cell.parentElement.rowIndex;
		the_cell.innerText=dataArray[i];
		if(i==1 || i==7){
			the_cell.style.display="none";
		}
	}
	read_def(the_table);
}

function del_row(the_table) {
	if(the_table.rows.length==1) return;
	var the_row;
	the_row = (cur_row==null || cur_row==0)?-1:cur_row;
	the_table.deleteRow(the_row);
	cur_row = null;
	cur_cell=null;
}

function add_col(the_table) {
	event.cancelBubble=true;
	var the_col,i,the_cell;
	the_col = cur_col==null?-1:(cur_col+1);
	var the_title=prompt("Please input the title: ","Untitled");
	if(the_title==null)return;
	if(the_col!=-1 && the_col<=sort_col && sort_col!=null)sort_col++;
	the_title=the_title==""?"Untitled":the_title
	clear_color();
	for(var i=0;i<the_table.rows.length;i++){
		the_cell=the_table.rows[i].insertCell(the_col);
		the_cell.innerText=i==0?the_title:("NewCol_" + the_cell.cellIndex);
	}
	read_def(the_table);
}

function del_col(the_table) {
	if(the_table.rows[0].cells.length==1) return;
	var the_col,the_cell;
	the_col = cur_col==null?(the_table.rows[0].cells.length-1):cur_col;
	if(the_col!=-1 && the_col<sort_col && sort_col!=null)sort_col--;
	else if(the_col==sort_col)sort_col=null;
	for(var i=0;i<the_table.rows.length;i++) the_table.rows[i].deleteCell(the_col);
	cur_col = null;
	cur_cell=null;
}

function res_tab(the_table){
	the_table.outerHTML=Org_con;
	init();
}

function exp_tab(the_table){
	var the_content="";
	document.onclick();
	the_content=the_table.outerHTML;
	the_content=the_content.replace(/ style=\"[^\"]*\"/g,"");
	the_content=the_content.replace(/ mode=\"(false|true)"/g,"");
	the_content=the_content.replace(/ oBgc=\"[\w#\d]*\"/g,"");
	the_content=the_content.replace(/ oFc=\"[\w#\d]*\"/g,"");
	the_content=the_content.replace(/<DIV contentEditable=false>([^<]*)<\/DIV>/ig,"$1");
	the_content="<style>table{font-size: 9pt;word-break:break-all;cursor: default;BORDER: black 1px solid;background-color:#eeeecc;border-collapse:collapse;border-Color:#999999;align:center;}</style>\n"+the_content;
	var newwin=window.open("about:blank","_blank","");
	newwin.document.open();
	newwin.document.write(the_content);
	newwin.document.close();
	newwin=null;
}

function getDataByArray(){
	var _data = new Array(Main_Tab.rows.length-1);
	for(var i=1;i<Main_Tab.rows.length;i++){
		//var _datacell = new Array(Main_Tab.rows[i].cells.length);
		//var jj=0;
		var _datacell="";
		for(var j=0;j<Main_Tab.rows[i].cells.length;j++){
			with(Main_Tab.rows[i]){
				if(!(j==0||j==6)){
				 //_datacell[jj]=cells[j].innerText;
				 //jj++;
				   if(_datacell.length<1){
				     _datacell=cells[j].innerText;
				    }else{
				     _datacell=_datacell+"f,f,"+cells[j].innerText;
				    }
				}				
			}
		}
	     _data[i-1]=_datacell;
	 }
	return _data;	
}

function getSumOfCol(cols){
	var sum = 0;
	for(var i=1;i<Main_Tab.rows.length;i++){
		sum=sum+parseInt(Main_Tab.rows[i].cells[cols].innerText);		
	 }
	return sum;	
}
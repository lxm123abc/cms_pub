package com.zte.cms.impSyncTask.service;

import java.util.List;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.cms.impSyncTask.dao.IImpCmsSynctaskDAO;
import com.zte.cms.impSyncTask.service.IImpCmsSynctaskDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ImpCmsSynctaskDS implements IImpCmsSynctaskDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IImpCmsSynctaskDAO dao = null;
	
	public void setDao(IImpCmsSynctaskDAO dao)
	{
	    this.dao = dao;
	}

	public void insertImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug("insert impCmsSynctask starting...");
		try
		{
			dao.insertImpCmsSynctask( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert impCmsSynctask end");
	}
	
	public void insertImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException
	{
		log.debug("insert impCmsSynctaskList by pk starting...");
		if(null == impCmsSynctaskList || impCmsSynctaskList.size() == 0 )
		{
			log.debug("there is no datas in impCmsSynctaskList");
			return;
		}
	    try
		{
			dao.insertImpCmsSynctaskList( impCmsSynctaskList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert impCmsSynctaskList by pk end");
	}
	
	public void updateImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug("update impCmsSynctask by pk starting...");
	    try
		{
			dao.updateImpCmsSynctask( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update impCmsSynctask by pk end");
	}

	public void updateImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException
	{
		log.debug("update impCmsSynctaskList by pk starting...");
		if(null == impCmsSynctaskList || impCmsSynctaskList.size() == 0 )
		{
			log.debug("there is no datas in impCmsSynctaskList");
			return;
		}
	    try
		{
			dao.updateImpCmsSynctaskList( impCmsSynctaskList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update impCmsSynctaskList by pk end");
	}

	public void updateImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug("update impCmsSynctask by condition starting...");
		try
		{
			dao.updateImpCmsSynctaskByCond( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update impCmsSynctask by condition end");
	}

	public void updateImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException
	{
		log.debug("update impCmsSynctaskList by condition starting...");
		if(null == impCmsSynctaskList || impCmsSynctaskList.size() == 0 )
		{
			log.debug("there is no datas in impCmsSynctaskList");
			return;
		}
		try
		{
			dao.updateImpCmsSynctaskListByCond( impCmsSynctaskList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update impCmsSynctaskList by condition end");
	}

	public void removeImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug( "remove impCmsSynctask by pk starting..." );
		try
		{
			dao.deleteImpCmsSynctask( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove impCmsSynctask by pk end" );
	}

	public void removeImpCmsSynctaskList ( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException
	{
		log.debug( "remove impCmsSynctaskList by pk starting..." );
		if(null == impCmsSynctaskList || impCmsSynctaskList.size() == 0 )
		{
			log.debug("there is no datas in impCmsSynctaskList");
			return;
		}
		try
		{
			dao.deleteImpCmsSynctaskList( impCmsSynctaskList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove impCmsSynctaskList by pk end" );
	}

	public void removeImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug( "remove impCmsSynctask by condition starting..." );
		try
		{
			dao.deleteImpCmsSynctaskByCond( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove impCmsSynctask by condition end" );
	}

	public void removeImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException
	{
		log.debug( "remove impCmsSynctaskList by condition starting..." );
		if(null == impCmsSynctaskList || impCmsSynctaskList.size() == 0 )
		{
			log.debug("there is no datas in impCmsSynctaskList");
			return;
		}
		try
		{
			dao.deleteImpCmsSynctaskListByCond( impCmsSynctaskList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove impCmsSynctaskList by condition end" );
	}

	public ImpCmsSynctask getImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug( "get impCmsSynctask by pk starting..." );
		ImpCmsSynctask rsObj = null;
		try
		{
			rsObj = dao.getImpCmsSynctask( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get impCmsSynctaskList by pk end" );
		return rsObj;
	}

	public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException
	{
		log.debug( "get impCmsSynctask by condition starting..." );
		List<ImpCmsSynctask> rsList = null;
		try
		{
			rsList = dao.getImpCmsSynctaskByCond( impCmsSynctask );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get impCmsSynctask by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get impCmsSynctask page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(impCmsSynctask, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ImpCmsSynctask>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get impCmsSynctask page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get impCmsSynctask page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(impCmsSynctask, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ImpCmsSynctask>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get impCmsSynctask page info by condition end" );
		return tableInfo;
	}
}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IProgramPictureMapDAO
{
    /**
     * 新增ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DAOException dao异常
     */
    public void insertProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException;

    /**
     * 根据主键更新ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DAOException dao异常
     */
    public void updateProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException;

    /**
     * 根据主键删除ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException;

    /**
     * 根据主键查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return 满足条件的ProgramPictureMap对象
     * @throws DAOException dao异常
     */
    public ProgramPictureMap getProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException;

    /**
     * 根据条件查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return 满足条件的ProgramPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<ProgramPictureMap> getProgramPictureMapByCond(ProgramPictureMap programPictureMap) throws DAOException;

    /**
     * 根据条件分页查询ProgramPictureMap对象，作为查询条件的参数
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ProgramPictureMap对象，作为查询条件的参数
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    
    /**
     * 根据条件分页查询ProgramPictureMap对象，作为查询条件的参数
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ProgramPictureMap对象，作为查询条件的参数
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException;
    
    
    /**
     * 根据条件查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return 满足条件的ProgramPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<ProgramPictureMap> getProgramnormalPictureMapByCond(ProgramPictureMap programPictureMap) throws DAOException;
}
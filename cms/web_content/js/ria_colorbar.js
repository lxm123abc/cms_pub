// @version 3.00.04

ColorBarCtrObj._COLORBAR = "COLORBAR";
ColorBarCtrObj._ZCOLORBAR = "Z:COLORBAR";
ColorBarCtrObj.colorBarArray = [];

//var colorBarTagArrary = [];

function ColorBarCtrObj(id){
	this.id = id;
	this.user_class = "";
	this.min = 0;
	this.max = 100;
	this.value = 0;
	this.bar_title =  "" ;
	this.width = "";
};

ColorBarCtrObj.initColorBar = function () {
	var colorBarTags = document.getElementsByTagName(ColorBarCtrObj._COLORBAR);
	if(!colorBarTags || colorBarTags.length == 0) {
		colorBarTags=document.getElementsByTagName(ColorBarCtrObj._ZCOLORBAR);	
	}
	
	if(!colorBarTags || colorBarTags.length == 0)
		return;
	var colorBar={};
	var _colorBarArray = [];
	for(var i=0; i < colorBarTags.length; i++) {
		colorBar = new ColorBarCtrObj.getColorBarTag(colorBarTags[i].getAttribute("id"));
		_colorBarArray[colorBarTags[i].getAttribute("id")] = colorBar;
		colorBar.initialize();
	}
	ColorBarCtrObj.colorBarArray = _colorBarArray;
};

ColorBarCtrObj.getColorBarTag = function (id) {
	var tagObj=document.getElementById(id);
	var _colorBar={};
	_colorBar = new ColorBarCtrObj(id);

	if (tagObj.getAttribute("class")) {	
		_colorBar.user_class = tagObj.getAttribute("class");
	} else {
		_colorBar.user_class = tagObj.getAttribute("className");
	}	
		
	if (tagObj.getAttribute("exttitle"))	
		_colorBar.bar_title = tagObj.getAttribute("exttitle");

	if (tagObj.getAttribute("max"))	
		_colorBar.max = tagObj.getAttribute("max");
		
	if (tagObj.getAttribute("min"))	
		_colorBar.min = tagObj.getAttribute("min");

	if (tagObj.getAttribute("value")) {
		_colorBar.value = tagObj.getAttribute("value");
	}	

	if (tagObj.getAttribute("width"))	
		_colorBar.width = tagObj.getAttribute("width");
	
	return _colorBar;
};

ColorBarCtrObj.getAgentType = function () {
	if (navigator.userAgent.indexOf("MSIE")>0) {
		return 0; //IE
	} else if(isFirefox=navigator.userAgent.indexOf("Firefox")>0) {
		return 1; //Firefox
	} else {
		return 2; //other
	}
};

ColorBarCtrObj.displayTitle = function (titleDivID) {
	if (document.getElementById(titleDivID).style.display != "block")
		document.getElementById(titleDivID).style.display = "block";
};
	
ColorBarCtrObj.disappearTitle = function (titleDivID) {
	document.getElementById(titleDivID).style.display = "none"; 
};

ColorBarCtrObj.prototype = {

	createColorBar:function() {
		var tmpOutDivID = "out_id" + this.id;
		var tmpInnerDivID = "inner_id" + this.id;
		var titleDivID = "title_id" + this.id;

		var _title = this.bar_title;
		//\u5982\u679c\u914d\u7f6e\u4e86title\uff0c\u5219\u4e3a\u989c\u8272\u6761\u52a0\u4e0a\u9f20\u6807\u79fb\u8fc7\u663e\u793atitle		
		if (_title) {
			for (var variable in ColorBarCtrObj.variableMap) {
				var processResult = ColorBarCtrObj.variableMap[variable](this.max, this.min, this.value);
				var regex = new RegExp(variable, "g");
				_title = _title.replace(regex, processResult);				
			}
		}	
		
		var titleHtmlID = "titleID" + this.id;		
		var html = '<div id="' + tmpOutDivID + '" >' +
						'<div id="' + tmpInnerDivID + '" valueext="' + this.value + '" ></div>' +
				   '</div>' + 
				   '<div id="' + titleDivID + '">' + _title +
				   '</div>';	
				
		$("#"+this.id).html(html);
		var _outdiv = document.getElementById(tmpOutDivID);
		var _titlediv = document.getElementById(titleDivID);
		var _innerdiv = document.getElementById(tmpInnerDivID);
		var _outclass = "";
		if (this.user_class) {
			_outclass =  "ui-colorbar-bg "   + this.user_class;
		} else {
			_outclass =  "ui-colorbar-bg";
		}
		_outdiv.setAttribute("class", _outclass);
		_innerdiv.setAttribute("class", "ui-colorbar-fg");
				
		_outdiv.setAttribute("className", _outclass);
		_innerdiv.setAttribute("className", "ui-colorbar-fg");
		if (this.width) {
			try {
				if(isNaN(this.width)) {
					_outdiv.style.width = this.width;
				} else {
					_outdiv.style.width = this.width + "px";
				}
			} catch(e) {
				//_outdiv.style.width = "200px";
			}	
		}
		//document.getElementById('xxx').style.cssText = 'left:200px;top:200px;';		
		var _width = (this.value - this.min)/(this.max - this.min);
		_innerdiv.style.width = _width*100 + "%";
		
		//\u5982\u679c\u914d\u7f6e\u4e86title\uff0c\u5219\u4e3a\u989c\u8272\u6761\u52a0\u4e0a\u9f20\u6807\u79fb\u8fc7\u663e\u793atitle		
		if (_title) {
			_titlediv.setAttribute("className", "ui-title-div");
			_titlediv.setAttribute("class", "ui-title-div");
			EventManager.Add(_outdiv,"mouseover" ,function () { ColorBarCtrObj.displayTitle(titleDivID) });
			EventManager.Add(_outdiv,"mouseout" ,function () { ColorBarCtrObj.disappearTitle(titleDivID) });
			
			_titlediv.style.display = "none";
		}
	},
	
	initialize:function() {

			if (isNaN(this.min) || isNaN(this.max) || isNaN(this.value)) {
				return;
			}
			var _min = Number(this.min);
			var _max = Number(this.max);
			var _value = Number(this.value);
			if ( _min >= _max || _value < _min || _value > _max ) {
				//alert("[" + _value + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
				return;
			}

		this.createColorBar();
	},
	
	setValue:function(value){
		if(isNaN(value)) {
			//alert("[" + value + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
			return;
		}
		this.value = value;
		this.initialize();
		return true;
	},
	
	getValue:function() {
		var _id = "inner_id" + this.id;
		return document.getElementById(_id).getAttribute("valueext");
	}
};

//\u5916\u90e8\u63a5\u53e3\uff0c\u8bbe\u7f6e\u989c\u8272\u6761value\u503c
ColorBarCtrObj.setValueObjFromColorBar = function (id, inputValue) {
	if(isNaN(inputValue)) {
		//alert("[" + inputValue + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
		return;
	}
	var colorBarObj = new ColorBarCtrObj.getColorBarTag(id);
	colorBarObj.value = inputValue; 
	colorBarObj.initialize();
	return true;
};

//\u5916\u90e8\u63a5\u53e3\uff0c\u83b7\u53d6\u989c\u8272\u6761value\u503c
ColorBarCtrObj.getValueObjFromColorBar = function (id) {
	var _id = "inner_id" + id;
	return document.getElementById(_id).getAttribute("valueext");
};

//\u7528\u4e8e\u5b58\u50a8title\u4e2d\u914d\u7f6e\u7684map\u5bf9\u8c61
ColorBarCtrObj.variableMap = {};

//\u5411variableMap\u6dfb\u52a0title\u4e2d\u914d\u7f6e\u7684\u503c
ColorBarCtrObj.addVariable = function (variable, processFun) {
	ColorBarCtrObj.variableMap[variable] = processFun;
};

//title\u63d0\u793a\u5904\u7406\u65b9\u6cd5\uff0c\u8fd4\u56de\u6700\u5927\u503c\uff0cmax\u4e3a\u6700\u5927\u503c\uff0cmin\u4e3a\u6700\u5c0f\u503c\uff0cvalue\u4e3a\u5f53\u524d\u503c\uff0c\u4e3a\u6807\u7b7e\u4e2d\u914d\u7f6e\u7684\u503c
ColorBarCtrObj.processMAXVariable = function (max, min, value) {
	return max;
};

//title\u63d0\u793a\u5904\u7406\u65b9\u6cd5\uff0c\u8fd4\u56de\u6700\u5c0f\u503c\uff0cmin\u4e3a\u6700\u5c0f\u503c\uff0cvalue\u4e3a\u5f53\u524d\u503c\uff0c\u4e3a\u6807\u7b7e\u4e2d\u914d\u7f6e\u7684\u503c
ColorBarCtrObj.processMINVariable = function (max, min, value) {
	return min;
};

//title\u63d0\u793a\u5904\u7406\u65b9\u6cd5\uff0c\u8fd4\u56de\u5f53\u524d\u503c\uff0cmin\u4e3a\u6700\u5c0f\u503c\uff0cvalue\u4e3a\u5f53\u524d\u503c\uff0c\u4e3a\u6807\u7b7e\u4e2d\u914d\u7f6e\u7684\u503c
ColorBarCtrObj.processVALUEVariable = function (max, min, value) {
	return value;
};

//title\u63d0\u793a\u5904\u7406\u65b9\u6cd5\uff0c\u8fd4\u56de\u5f53\u524d\u503c\u7684\u767e\u5206\u6bd4\uff0cmin\u4e3a\u6700\u5c0f\u503c\uff0cvalue\u4e3a\u5f53\u524d\u503c\uff0c\u4e3a\u6807\u7b7e\u4e2d\u914d\u7f6e\u7684\u503c
ColorBarCtrObj.processRATIOVariable = function (max, min, value) {
	var result = ((value - min) / (max - min)) * 100;
	var resultReturn = result.toFixed(2) + "%";
	return resultReturn;
}

ColorBarCtrObj.addVariable("%MAX", ColorBarCtrObj.processMAXVariable);
ColorBarCtrObj.addVariable("%MIN", ColorBarCtrObj.processMINVariable);
ColorBarCtrObj.addVariable("%VALUE", ColorBarCtrObj.processVALUEVariable);
ColorBarCtrObj.addVariable("%RATIO", ColorBarCtrObj.processRATIOVariable);


addOnloadEvent(ColorBarCtrObj.initColorBar);

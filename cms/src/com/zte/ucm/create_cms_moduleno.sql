declare 
v_retcode number(10);
v_retdesc varchar2(1024);
begin
  zxdbm_ucm.sp_ucm_addorupdatemodule('CMSDB','CMS_DB','CMS DB UCM',v_retcode,v_retdesc);
  zxdbm_ucm.sp_ucm_addorupdatemodule('CMSWEB','CMS_WEB','CMS WEB UCM',v_retcode,v_retdesc);
  zxdbm_ucm.sp_ucm_addorupdatemodule_span('CMSIMP','CMS_IMP','CMS IMP SERVICE UCM',v_retcode,v_retdesc);
  zxdbm_ucm.sp_ucm_addorupdatemodule_span('CMSIMPPT','CMS_IMPPT','CMS IMP PLATFORM UCM',v_retcode,v_retdesc);
end;
/
exit
package com.zte.cms.channel.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.channel.dao.ICategoryChannelMapDAO;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.zorm.mapping.PrimaryKey;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CategoryChannelMapDS extends DynamicObjectBaseDS implements
		ICategoryChannelMapDS {
	// 日志
	private Log log = SSBBus.getLog(getClass());

	private ICategoryChannelMapDAO dao = null;

	public void setDao(ICategoryChannelMapDAO dao) {
		this.dao = dao;
	}

	public void insertCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("insert categoryChannelMap starting...");
		try {
			Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
					"channel_catagory_map");
			categoryChannelMap.setMapindex(index);
			categoryChannelMap.setStatus(0);// 待发布
			String objectId = String.format("%02d", 28)+ String.format("%030d",index);
			categoryChannelMap.setMappingid(objectId);
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			String dateStr = dateFormat.format(date);
			categoryChannelMap.setCreatetime(dateStr);
			dao.insertCategoryChannelMap(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("insert categoryChannelMap end");
	}

	public void updateCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("update categoryChannelMap by pk starting...");
		try {
			dao.updateCategoryChannelMap(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update categoryChannelMap by pk end");
	}

	public void updateCategoryChannelMapList(
			List<CategoryChannelMap> categoryChannelMapList)
			throws DomainServiceException {
		log.debug("update categoryChannelMapList by pk starting...");
		if (null == categoryChannelMapList
				|| categoryChannelMapList.size() == 0) {
			log.debug("there is no datas in categoryChannelMapList");
			return;
		}
		try {
			for (CategoryChannelMap categoryChannelMap : categoryChannelMapList) {
				dao.updateCategoryChannelMap(categoryChannelMap);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update categoryChannelMapList by pk end");
	}

	public void updateCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("update categoryChannelMap by condition starting...");
		try {
			dao.updateCategoryChannelMapByCond(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update categoryChannelMap by condition end");
	}

	public void updateCategoryChannelMapListByCond(
			List<CategoryChannelMap> categoryChannelMapList)
			throws DomainServiceException {
		log.debug("update categoryChannelMapList by condition starting...");
		if (null == categoryChannelMapList
				|| categoryChannelMapList.size() == 0) {
			log.debug("there is no datas in categoryChannelMapList");
			return;
		}
		try {
			for (CategoryChannelMap categoryChannelMap : categoryChannelMapList) {
				dao.updateCategoryChannelMapByCond(categoryChannelMap);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update categoryChannelMapList by condition end");
	}

	public void removeCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("remove categoryChannelMap by pk starting...");
		try {
			dao.deleteCategoryChannelMap(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove categoryChannelMap by pk end");
	}

	public void removeCategoryChannelMapList(
			List<CategoryChannelMap> categoryChannelMapList)
			throws DomainServiceException {
		log.debug("remove categoryChannelMapList by pk starting...");
		if (null == categoryChannelMapList
				|| categoryChannelMapList.size() == 0) {
			log.debug("there is no datas in categoryChannelMapList");
			return;
		}
		try {
			for (CategoryChannelMap categoryChannelMap : categoryChannelMapList) {
				dao.deleteCategoryChannelMap(categoryChannelMap);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove categoryChannelMapList by pk end");
	}

	public void removeCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("remove categoryChannelMap by condition starting...");
		try {
			dao.deleteCategoryChannelMapByCond(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove categoryChannelMap by condition end");
	}

	public void removeCategoryChannelMapListByCond(
			List<CategoryChannelMap> categoryChannelMapList)
			throws DomainServiceException {
		log.debug("remove categoryChannelMapList by condition starting...");
		if (null == categoryChannelMapList
				|| categoryChannelMapList.size() == 0) {
			log.debug("there is no datas in categoryChannelMapList");
			return;
		}
		try {
			for (CategoryChannelMap categoryChannelMap : categoryChannelMapList) {
				dao.deleteCategoryChannelMapByCond(categoryChannelMap);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove categoryChannelMapList by condition end");
	}

	public CategoryChannelMap getCategoryChannelMap(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("get categoryChannelMap by pk starting...");
		CategoryChannelMap rsObj = null;
		try {
			rsObj = dao.getCategoryChannelMap(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("get categoryChannelMapList by pk end");
		return rsObj;
	}

	public List<CategoryChannelMap> getCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("get categoryChannelMap by condition starting...");
		List<CategoryChannelMap> rsList = null;
		try {
			rsList = dao.getCategoryChannelMapByCond(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("get categoryChannelMap by condition end");
		return rsList;
	}

	public List<CategoryChannelMap> getCategoryChannelMapByCond2(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException {
		log.debug("get categoryChannelMap2 by condition starting...");
		List<CategoryChannelMap> rsList = null;
		try {
			rsList = dao.getCategoryChannelMapByCond2(categoryChannelMap);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("get categoryChannelMap by condition end");
		return rsList;
	}

	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CategoryChannelMap categoryChannelMap,
			int start, int pageSize) throws DomainServiceException {
		log.debug("get categoryChannelMap page info by condition starting...");
		PageInfo pageInfo = null;
		try {
			pageInfo = dao.pageInfoQuery(categoryChannelMap, start, pageSize);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CategoryChannelMap>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug("get categoryChannelMap page info by condition end");
		return tableInfo;
	}

}

package com.zte.cms.task.common;

public class CmsTaskauditConstants
{
    public static final String success = "0";

    public static final String fail = "1";
   
    /**
     * 牌照方
     */
    public static final int lp_type = 2;
    /**
     * 运营商
     */
    public static final int op_type = 3;

    /**
     * CP
     */
    public static final int cp_type = 1;
    /**
     * 免审
     */
    public static final int noaudit_type = 4;  

    /**
     * 直通
     */
    public static final int directToSync_type = 5; 
}

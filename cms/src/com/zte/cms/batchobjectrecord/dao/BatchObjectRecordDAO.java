package com.zte.cms.batchobjectrecord.dao;

import java.util.List;

import com.zte.cms.batchobjectrecord.dao.IBatchObjectRecordDAO;
import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class BatchObjectRecordDAO extends DynamicObjectBaseDao implements IBatchObjectRecordDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("insert batchObjectRecord starting...");
        super.insert("insertBatchObjectRecord", batchObjectRecord);
        log.debug("insert batchObjectRecord end");
    }

    public void updateBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("update batchObjectRecord by pk starting...");
        super.update("updateBatchObjectRecord", batchObjectRecord);
        log.debug("update batchObjectRecord by pk end");
    }

    public void updateBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("update batchObjectRecord by conditions starting...");
        super.update("updateBatchObjectRecordByCond", batchObjectRecord);
        log.debug("update batchObjectRecord by conditions end");
    }

    public void deleteBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("delete batchObjectRecord by pk starting...");
        super.delete("deleteBatchObjectRecord", batchObjectRecord);
        log.debug("delete batchObjectRecord by pk end");
    }

    public void deleteBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("delete batchObjectRecord by conditions starting...");
        super.delete("deleteBatchObjectRecordByCond", batchObjectRecord);
        log.debug("update batchObjectRecord by conditions end");
    }

    public BatchObjectRecord getBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("query batchObjectRecord starting...");
        BatchObjectRecord resultObj = (BatchObjectRecord) super.queryForObject("getBatchObjectRecord",
                batchObjectRecord);
        log.debug("query batchObjectRecord end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<BatchObjectRecord> getBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException
    {
        log.debug("query batchObjectRecord by condition starting...");
        List<BatchObjectRecord> rList = (List<BatchObjectRecord>) super.queryForList(
                "queryBatchObjectRecordListByCond", batchObjectRecord);
        log.debug("query batchObjectRecord by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize) throws DAOException
    {
        log.debug("page query batchObjectRecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryBatchObjectRecordListCntByCond", batchObjectRecord))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<BatchObjectRecord> rsList = (List<BatchObjectRecord>) super.pageQuery(
                    "queryBatchObjectRecordListByCond", batchObjectRecord, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query batchObjectRecord by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryBatchObjectRecordListByCond", "queryBatchObjectRecordListCntByCond",
                batchObjectRecord, start, pageSize, puEntity);
    }

}
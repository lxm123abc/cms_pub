    function get19Time(time14)
{
	if(time14 != undefined && time14 != null && strTrim(time14) != "")
    {
     	   var  tmpTime = strTrim(time14);
     	   if(tmpTime.length == 14)
     	   {
     	   	   var year = tmpTime.substring(0,4);
     	   	   var month = tmpTime.substring(4,6);
     	   	   var day = tmpTime.substring(6,8);
     	   	   var hour = tmpTime.substring(8,10);
     	   	   var minutes = tmpTime.substring(10,12);
     	   	   var seconds = tmpTime.substring(12,14);
     	   	   return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
     	   }
     	   else
     	   {
     	   	 	return "";
     	   }
   	}
   	else
   	{
   		  return "";
   	}
}
function strTrim(str)
	{
	    var num = str.length;
	    var i = 0;
	    for(i = 0; i < num;i++) {
	        if(str.charAt(i) != " ")
	            break;
	    }
	    str = str.substring(i);
	    num = str.length;
	    for(i = num-1; i > -1;i--) {
	        if(str.charAt(i) != " ")
	            break;
	    }
	    str = str.substring(0, i+1);
	    return str;
	}
    var cpid ={};
    function popcpsp(){
        var page = "../../cpsp/popCpsp.html";
        var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
        var rtnobj = window.showModalDialog(page, "", sFeatures);
        
        if(typeof(rtnobj) != "undefined"){
	        $("#cpcnshortname").val(rtnobj.cpcnshortname);
	        $("#cpindex").val(rtnobj.cpindex);
	        $("#cpid").val(rtnobj.cpid);    
	        
	        var providValue="";
	    	for(var i=0;i<countryMap["valueList"].length;i++)
			{
				if(rtnobj.businessscope==countryMap["valueList"][i])
				{
					providValue=countryMap["textList"][i];
					break;
				}
			}
			$("#providValue").text(providValue);
			$("#provid").val(rtnobj.businessscope);
	     	cpid = rtnobj.cpid;
	        callSid('getCpBase',cpid);
	        callSid('getProgramType','[cpid,true]','$M.obj.programtype');
        }
    }
    
    
    function initReleaseYear(comID)
    {
		var releaseYearEle=document.getElementById(comID);
		var option;
		var text;
		
		option=document.createElement("option");
	
		text=document.createTextNode("--请选择--");
		option.setAttribute("value","");
		option.appendChild(text);
		releaseYearEle.appendChild(option);
		
		for(var i=1900;i<=2050;i++)
		{
			option=document.createElement("option");
			text=document.createTextNode(i);
			option.setAttribute("value",i);
			option.appendChild(text);
			releaseYearEle.appendChild(option);
		}
    }
    
    function popCatalog()
  	{
		var page = "../CatalogChooseWindow.html";                                //弹出窗口路径
		var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
		var name = window.showModalDialog(page, "", sFeatures);       
		$("#contenttags").val(name);                                                         //其中tagname为内容页面的标签输入框的id                                            
   	}
    
    
    function doValidPage()
    {
    	$("label.validFont").text("");
    	var isPass=false;
    	var namecn=document.getElementById("namecn");
    	isPass=isNull(namecn,"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}	
    	isPass=isMaxLength(namecn,"namecn_msg",namecn.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr1($.trim(namecn.value),"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	///内容名称校验结束
    	
    	var isPass=false;
    	var platform=document.getElementById("platform");
    	isPass=isNull(platform,"platform_msg");
    	if(isPass==false)
    	{
    		return false;
    	}	
    	isPass=isMaxLength(platform,"platform_msg",platform.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr1($.trim(platform.value),"platform_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	
    	var originalnamecn=document.getElementById("originalnamecn");
    	isPass=isMaxLength(originalnamecn,"originalnamecn_msg",originalnamecn.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr($.trim(originalnamecn.value),"originalnamecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	//连续剧原来名称校验结束
    	
    	var cpid=document.getElementById("cpid");
    	isPass=isNull(cpid,"cpid_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	//验证内容提供商结束
    	
    	var licenseid=$("#licenseid").val();
    	if(licenseid=="")
    	{
    		$("#licenseid_msg").text("请选择");
    		return false;
    	}
    	//验证牌照方结束
    	
    	var programtype=$("#programtype").val();

    	if(programtype=="")
    	{
    		$("#programtype_msg").text("请选择");
    		return false;
    	}
    	//验证节目类型结束
    		   	
	   	var genre=document.getElementById("genre");
    	isPass=isMaxLength(genre,"genre_msg",genre.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr($.trim(genre.value),"genre_msg");
    	if(isPass==false)
    	{
			return false;
	   	}
	   	//类别验证结束
	   	
	   	var duration=document.getElementById("duration");
	   	isPass=isAboveZero(duration,"duration_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证播放时长结束
	   	
	   	var sortname=document.getElementById("sortname");
	   	isPass=isMaxLength(sortname,"sortname_msg",sortname.maxLength);
	   	if(isPass==false)
	   	{
			return false;
	   	}	   	
	   	isPass=isSpecialStr($.trim(sortname.value),"sortname_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证索引名称结束
	   	
		var searchname=document.getElementById("searchname");
		isPass=isMaxLength(searchname,"searchname_msg",searchname.maxLength);
		if(isPass==false)
		{
			return false;
		}	   	
		isPass=isSpecialStr($.trim(searchname.value),"searchname_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证索引名称结束
		
		var contentversion=document.getElementById("contentversion");
		isPass=isMaxLength(contentversion,"contentversion_msg",contentversion.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(contentversion.value),"contentversion_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容版本结束
		
		var keywords=document.getElementById("keywords");
		isPass=isMaxLength(keywords,"keywords_msg",keywords.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(keywords.value),"keywords_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证关键字结束
		
		
		var copyrightcn=document.getElementById("copyrightcn");
		isPass=isMaxLength(copyrightcn,"copyrightcn_msg",copyrightcn.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyrightcn.value),"copyrightcn_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权信息结束
		isPass=betweenTimeValidate("effectivedate","expirydate","effectivedate_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容生效失效时间结束
		
		var exclusivevalidity=document.getElementById("exclusivevalidity");
		isPass=isPlusInteger(exclusivevalidity,"exclusivevalidity_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证独家有效期结束

		var copyrightexpiration=document.getElementById("copyrightexpiration");	
		isPass=isPlusInteger(copyrightexpiration,"copyrightexpiration_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权失效期结束
		
		var llicensofflinetime=document.getElementById("llicensofflinetime").value;
		if(llicensofflinetime!="")
		{
			isPass=validatedate("llicensofflinetime","llicensofflinetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}
		//验证牌照方自定义下线时间结束
		
		var deletetime=document.getElementById("deletetime").value;
		if(deletetime!="")
		{
			isPass=validatedate("deletetime","deletetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}	
		//验证内容删除时间结束
		
		
		var copyproperty=document.getElementById("copyproperty");
		isPass=isMaxLength(copyproperty,"copyproperty_msg",copyproperty.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyproperty.value),"copyproperty_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权性质结束
		
		var country=document.getElementById("country");
		isPass=isMaxLength(country,"country_msg",country.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(country.value),"country_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证国家结束
		
		
		var importlisence=document.getElementById("importlisence");
		isPass=isMaxLength(importlisence,"importlisence_msg",importlisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(importlisence.value),"importlisence_msg");
		if(isPass==false)
		{
			return false;
		}
		
	
		//引进批准号结束
		
		var releaselisence=document.getElementById("releaselisence");
		isPass=isMaxLength(releaselisence,"releaselisence_msg",releaselisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(releaselisence.value),"releaselisence_msg");
		if(isPass==false)
		{
			return false;
		}

		//验证公映许可证结束
		
		var ordernumber=document.getElementById("ordernumber");
	   	isPass=isPlusInteger(ordernumber,"ordernumber_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证节目订购编号结束
		
		var contenttags=document.getElementById("contenttags");
		isPass=isMaxLength(contenttags,"contenttags_msg",256);
		if(isPass==false)
		{
			return false;
		}
		//验证内容标签关联结束
		var pricetaxin=document.getElementById("pricetaxin");
		isPass=checkpRicetaxin(pricetaxin,"pricetaxin_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证列表定价结束
		
		var newcomedays=document.getElementById("newcomedays");
		isPass=isPlusInteger(newcomedays,"newcomedays_msg");
		if(isPass==false)
		{
			return false;
		}
		//新到天数结束
		var remainingdays=document.getElementById("remainingdays");
		isPass=isPlusInteger(remainingdays,"remainingdays_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证剩余天数结束
		
		var recommendstart=document.getElementById("recommendstart");
		isPass=isPlusInteger(recommendstart,"recommendstart_msg");
		if(isPass==false)
		{
			return false;
		}
		else
		{
			var recommendstartValue=$.trim(recommendstart.value);
			if(parseInt(recommendstartValue)>10||parseInt(recommendstartValue)<1)
			{
				$("#recommendstart_msg").text("推荐星级为1-10的整数");
				return false;
			}
			else
			{
				$("#recommendstart_msg").text("");
			}
		}
		//验证推荐星级结束
		
		var licensingstart=document.getElementById("licensingstart");
		var licensingend=document.getElementById("licensingend");
		isPass=betweenTimeValidate("licensingstart","licensingend","licensingstart_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证有效时间结束
		
		var viewpoint=document.getElementById("viewpoint");
		isPass=isSpecialStr($.trim(viewpoint.value),"viewpoint_msg");
		if(isPass==false)
		{
			return false;
		}
		
		//验证看点结束
		
		var awards=document.getElementById("awards");
		isPass=isMaxLength(awards,"awards_msg",awards.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(awards.value),"awards_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证所含奖项结束
		
		var actors=document.getElementById("actors");		
		isPass=isMaxLength(actors,"actors_msg",actors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(actors.value),"actors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证演员结束
		
		var writers=document.getElementById("writers");		
		isPass=isMaxLength(writers,"writers_msg",writers.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(writers.value),"writers_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证作者结束
		
		var directors=document.getElementById("directors");		
		isPass=isMaxLength(directors,"directors_msg",directors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(directors.value),"directors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证导演结束
		
		var productcorp=document.getElementById("productcorp");		
		isPass=isMaxLength(productcorp,"productcorp_msg",productcorp.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(productcorp.value),"productcorp_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片商结束
		
		var producer=document.getElementById("producer");
		isPass=isMaxLength(producer,"producer_msg",producer.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(producer.value),"producer_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片人结束
		
		var publisher=document.getElementById("publisher");
		isPass=isMaxLength(publisher,"publisher_msg",publisher.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(publisher.value),"publisher_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证发行商结束
		
		
		var title=document.getElementById("title");
		isPass=isMaxLength(title,"title_msg",title.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(title.value),"title_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容标题结束
		
		var desccn=document.getElementById("desccn");
		isPass=isSpecialStr($.trim(desccn.value),"desccn_msg");
		if(isPass==false)
		{
			return false;
		}
		return true;
		
    }
    
       
   //内容查看页面和修改页面格式化时间开始 
    
   //格式化内容生效时间
   function effectiveCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('effectivedate');
        return getTime(time);
   }
   
   //格式化内容失去效时间
   function expiryCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('expirydate');
        return getTime(time);
   }
   
   //格式化牌照方自定义内容下线时间
   function llicensofflineCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('llicensofflinetime');
        return getTime(time);
   }
   
   //格式化内容删除时间
   function deleteCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('deletetime');
        return getTime(time);
   }
   
   //格式化首播日期
   function orgairCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('orgairdate');
        return getTime(time);
   }
   
   //格式化有效开始时间
   function licensingstartCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('licensingstart');
        return getTime(time);
   }
   
   //格式化有效结束时间
   function licensingendCulate(keyvalue) 
   {
        var time = getRiaCtrlValue('licensingend');
        return getTime(time);
   } 
   
   //内容查看页面和修改页面格式化时间开始 
   
   




   
   function seriesflagCalculate(keyvalue)
   {
   	   	var seriesflag = getRiaCtrlValue("seriesflag");
   		if(seriesflag == "0")
   		{
   			return "普通VOD";
   		}else if(seriesflag == "1")
   		{
   			return "连续剧剧集";
   		}else
   		{
   			return "";
   		}
   }
   
   function sourcetypeCalculate(keyvalue)
   {
   		var sourcetype = getRiaCtrlValue("sourcetype");
   		if(sourcetype == "1")
   		{
   			return "VOD";
   		}else if(sourcetype == "5")
   		{
   			return "Advertisement";
   		}else
   		{
   			return "";
   		}
   }
   
   function isvalidCalculate(keyvalue)
   {
   		var isvalid = getRiaCtrlValue("isvalid");
   		if(isvalid == "0")
   		{
   			return "失效";
   		}else if(isvalid == "1")
   		{
   			return "生效";
   		}else
   		{
   			return "";
   		}
   }
   
   
   //内容查看页面显示"业务平台"
   function platformCalculate(keyvalue)
   {
   		var platform = getRiaCtrlValue("platform");
   		if(platform == "1")
   		{
   			return "互联网视讯";
   		}else if(platform == "2")
   		{
   			return "天翼视讯";
   		}else if(platform == "3")
   		{
   			return "IPTV";
   		}else if(platform == "4")
   		{
   			return "魔屏";
   		}else
   		{
   			return "";
   		}
   }
   
   
   //内容查看页面显示"地区类型"
   function countrytypeCalculate(keyvalue)
   {
   		 provinceMap=provinceMap.rtnValue;
   		 return provinceMap[getRiaCtrlValue("countrytype")];
   }
   
   //内容查看页面显示"交易类别"
   function tradetypeCalculate(keyvalue)
   {
   		 feetypeMap=feetypeMap.rtnValue;
   		 return feetypeMap[getRiaCtrlValue("tradetype")];
   }
   
   //内容查看页面显示"区域"
   function providCalculate(keyvalue)
   {
   		countryMap=countryMap.rtnValue;
   		for(var i =0;i<countryMap["valueList"].length;i++) 
		{ 
			if(getRiaCtrlValue("provid")==countryMap["valueList"][i])
			{
				return countryMap["textList"][i];
				
			}
		}
   } 
   
   //内容查看页面显示"推荐星级"
   function recommendstartCalculate(keyvalue)
   {
   		var recommendstart = getRiaCtrlValue("recommendstart");
	   	if(recommendstart>=1 && recommendstart<3){
	        return "一级";
	    }else if(recommendstart>=3 && recommendstart<5){
	        return "二级";
	    }else if(recommendstart>=5 && recommendstart<7){
	        return "三级";
	    }else if(recommendstart>=7 && recommendstart<9){
	        return "四级";
	    }else if(recommendstart>=9 && recommendstart<=10){
	        return "五级";
	    }else{
	        return "";
	    }
   }
   
   //内容查看页面显示"拷贝保护"
   function macrovisionCalculate(keyvalue)
   {
   		var macrovision = getRiaCtrlValue("macrovision");
   		if(macrovision == "0")
   		{
   			return "无拷贝保护";
   		}else if(macrovision == "1")
   		{
   			return "有拷贝保护";
   		}else
   		{
   			return "";
   		}
   }
   
   //内容查看页面显示"字幕语言"
   function subtitlelanguageCalculate(keyvalue)
   {
   		return languageMap[getRiaCtrlValue("subtitlelanguage")];
   }
   
   //内容查看页面显示"配音语种"
   function dubbinglanguageCalculate(keyvalue)
   {
   		return languageMap[getRiaCtrlValue("dubbinglanguage")];
   }
   
   //内容查看页面显示"语种"
   function languageCalculate(keyvalue)
   {
   		return languageMap[getRiaCtrlValue("language")];
   }
   
   //内容查看页面显示"内容级别"
   function ratingCalculate(keyvalue)
   {
   		limitlevelMap=limitlevelMap.rtnValue;
   		return limitlevelMap[getRiaCtrlValue("rating")];
   }
   



//根据权限和状态显示查看按钮
function query_view(i)
{
	if(i>0)
	{
		if(viewRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}

    
//根据权限和状态显示修改按钮
function query_modify(i)
{
	if(i>0 && modRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case   0:	//待发布
			 case  20:	//新增同步成功
			 case  50:	//修改同步成功
			 case  60:	//修改同步失败
			 case  80:	//取消同步成功
			 case 110:  //待审核
			 case 130:	//一审失败
			 case 150:  //二审失败
			 case 180:	//三审失败	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}




//根据权限和状态显示删除按钮
function query_del(i)
{
	if(i>0 && delRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case   0:  //待发布
			 case  80:	//取消同步成功
			 case 110:  //待审核
			 case 150:  //二审失败
			 case 160:  //回收站
			 case 180:	//三审失败		
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

//根据权限和状态显示发布按钮
function query_publish(i)
{
	if(i>0 && pubRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case   0:  //待发布
			 case  30:	//新增同步失败
			 case  60:	//修改同步失败	
			 case  80:  //取消同步成功
			 case  90:	//取消同步失败	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

//根据权限和状态显示取消发布按钮
function query_delPublish(i)
{
	if(i>0 && delPubRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  30:	//新增同步失败
			 case  65:	//预下线
			 case  90:	//取消同步失败	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

//根据权限和状态显示子内容管理按钮
function query_movie(i)
{
	if(i>0 && movRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  100:  //初始
			 case   65:  //预下线
				 this.style.display = "none";
				 break;
			 default:
				 this.style.display = "";
			}	
	}
}

//根据权限和状态显示关联角色按钮
function query_castrole(i)
{
	if(i>0 && rolRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  20:	//新增同步成功
			 case  50:	//修改同步成功
			 case  60:	//修改同步失败
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

//根据权限和状态显示关联栏目按钮
function query_category(i)
{
	if(i>0 && catRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  20:	//新增同步成功
			 case  50:	//修改同步成功
			 case  60:	//修改同步失败	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
			
		var seriesflag = data[i-1].seriesflag;
		if(seriesflag == 1)
		{
			this.style.display = "none";
		}
	}
}

//根据权限和状态显示海报管理按钮
function query_picture(i)
{
	if(i>0 && picRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  100:  //初始
			 case   65:  //预下线
				 this.style.display = "none";
				 break;
			 default:
				 this.style.display = "";
			}	
	}
}

function query_preoffline(i)
{
	if(i>0 && delPubRight)
	{
			var status=data[i-1].status;
			switch(status)
			{
			 case  20: 
			 case  50: 
			 case  60:	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}
function query_schedualrecord(i)
{
	if(i>0 && schrecordmgtRight)
	{
			var status=data[i-1].status;
			switch(status)
			{
			 case  20: 
			 case  50: 
			 case  60:	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

function query_recycle(i)
{
	if(i>0 && recycleRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  180: 
			 case  150: 
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}




























function doValidPage2()
{
    	$("label.validFont").text("");
    	var isPass=false;   	
	   	var genre=document.getElementById("genre");
    	isPass=isMaxLength(genre,"genre_msg",genre.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr($.trim(genre.value),"genre_msg");
    	if(isPass==false)
    	{
			return false;
	   	}
	   	//类别验证结束
	   	
	   	var duration=document.getElementById("duration");
	   	isPass=isAboveZero(duration,"duration_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证播放时长结束
	   	
	   	var sortname=document.getElementById("sortname");
	   	isPass=isMaxLength(sortname,"sortname_msg",sortname.maxLength);
	   	if(isPass==false)
	   	{
			return false;
	   	}	   	
	   	isPass=isSpecialStr($.trim(sortname.value),"sortname_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证索引名称结束
	   	
		var searchname=document.getElementById("searchname");
		isPass=isMaxLength(searchname,"searchname_msg",searchname.maxLength);
		if(isPass==false)
		{
			return false;
		}	   	
		isPass=isSpecialStr($.trim(searchname.value),"searchname_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证索引名称结束
		
		var contentversion=document.getElementById("contentversion");
		isPass=isMaxLength(contentversion,"contentversion_msg",contentversion.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(contentversion.value),"contentversion_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容版本结束
		
		var keywords=document.getElementById("keywords");
		isPass=isMaxLength(keywords,"keywords_msg",keywords.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(keywords.value),"keywords_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证关键字结束
		
		
		var copyrightcn=document.getElementById("copyrightcn");
		isPass=isMaxLength(copyrightcn,"copyrightcn_msg",copyrightcn.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyrightcn.value),"copyrightcn_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权信息结束
		isPass=betweenTimeValidate("effectivedate","expirydate","effectivedate_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容生效失效时间结束
		
		var exclusivevalidity=document.getElementById("exclusivevalidity");
		isPass=isPlusInteger(exclusivevalidity,"exclusivevalidity_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证独家有效期结束

		var copyrightexpiration=document.getElementById("copyrightexpiration");	
		isPass=isPlusInteger(copyrightexpiration,"copyrightexpiration_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权失效期结束
		
		var llicensofflinetime=document.getElementById("llicensofflinetime").value;
		if(llicensofflinetime!="")
		{
			isPass=validatedate("llicensofflinetime","llicensofflinetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}
		//验证牌照方自定义下线时间结束
		
		var deletetime=document.getElementById("deletetime").value;
		if(deletetime!="")
		{
			isPass=validatedate("deletetime","deletetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}	
		//验证内容删除时间结束
		
		
		var copyproperty=document.getElementById("copyproperty");
		isPass=isMaxLength(copyproperty,"copyproperty_msg",copyproperty.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyproperty.value),"copyproperty_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权性质结束
		
		var country=document.getElementById("country");
		isPass=isMaxLength(country,"country_msg",country.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(country.value),"country_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证国家结束
		
		
		var importlisence=document.getElementById("importlisence");
		isPass=isMaxLength(importlisence,"importlisence_msg",importlisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(importlisence.value),"importlisence_msg");
		if(isPass==false)
		{
			return false;
		}
		
	
		//引进批准号结束
		
		var releaselisence=document.getElementById("releaselisence");
		isPass=isMaxLength(releaselisence,"releaselisence_msg",releaselisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(releaselisence.value),"releaselisence_msg");
		if(isPass==false)
		{
			return false;
		}

		//验证公映许可证结束
		
		var ordernumber=document.getElementById("ordernumber");
	   	isPass=isPlusInteger(ordernumber,"ordernumber_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证节目订购编号结束
		
		var contenttags=document.getElementById("contenttags");
		isPass=isMaxLength(contenttags,"contenttags_msg",256);
		if(isPass==false)
		{
			return false;
		}
		//验证内容标签关联结束
		var pricetaxin=document.getElementById("pricetaxin");
		isPass=checkpRicetaxin(pricetaxin,"pricetaxin_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证列表定价结束
		
		var newcomedays=document.getElementById("newcomedays");
		isPass=isPlusInteger(newcomedays,"newcomedays_msg");
		if(isPass==false)
		{
			return false;
		}
		//新到天数结束
		var remainingdays=document.getElementById("remainingdays");
		isPass=isPlusInteger(remainingdays,"remainingdays_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证剩余天数结束
		
		var recommendstart=document.getElementById("recommendstart");
		isPass=isPlusInteger(recommendstart,"recommendstart_msg");
		if(isPass==false)
		{
			return false;
		}
		else
		{
			var recommendstartValue=$.trim(recommendstart.value);
			if(parseInt(recommendstartValue)>10||parseInt(recommendstartValue)<1)
			{
				$("#recommendstart_msg").text("推荐星级为1-10的整数");
				return false;
			}
			else
			{
				$("#recommendstart_msg").text("");
			}
		}
		//验证推荐星级结束
		
		var licensingstart=document.getElementById("licensingstart");
		var licensingend=document.getElementById("licensingend");
		isPass=betweenTimeValidate("licensingstart","licensingend","licensingstart_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证有效时间结束
		
		var viewpoint=document.getElementById("viewpoint");
		isPass=isSpecialStr($.trim(viewpoint.value),"viewpoint_msg");
		if(isPass==false)
		{
			return false;
		}
		
		//验证看点结束
		
		var awards=document.getElementById("awards");
		isPass=isMaxLength(awards,"awards_msg",awards.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(awards.value),"awards_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证所含奖项结束
		
		var actors=document.getElementById("actors");		
		isPass=isMaxLength(actors,"actors_msg",actors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(actors.value),"actors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证演员结束
		
		var writers=document.getElementById("writers");		
		isPass=isMaxLength(writers,"writers_msg",writers.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(writers.value),"writers_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证作者结束
		
		var directors=document.getElementById("directors");		
		isPass=isMaxLength(directors,"directors_msg",directors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(directors.value),"directors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证导演结束
		
		var productcorp=document.getElementById("productcorp");		
		isPass=isMaxLength(productcorp,"productcorp_msg",productcorp.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(productcorp.value),"productcorp_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片商结束
		
		var producer=document.getElementById("producer");
		isPass=isMaxLength(producer,"producer_msg",producer.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(producer.value),"producer_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片人结束
		
		var publisher=document.getElementById("publisher");
		isPass=isMaxLength(publisher,"publisher_msg",publisher.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(publisher.value),"publisher_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证发行商结束
		
		
		var title=document.getElementById("title");
		isPass=isMaxLength(title,"title_msg",title.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(title.value),"title_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容标题结束
		
		var desccn=document.getElementById("desccn");
		isPass=isSpecialStr($.trim(desccn.value),"desccn_msg");
		if(isPass==false)
		{
			return false;
		}
		return true;
		
    }

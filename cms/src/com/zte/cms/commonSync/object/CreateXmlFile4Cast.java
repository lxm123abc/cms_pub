package com.zte.cms.commonSync.object;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;

public class CreateXmlFile4Cast extends AbstractCreateXmlFile4Object
{

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        
        Date date = new Date();
        String fileName = "cast_" +  "20_" + date.getTime()+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "cast" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        List<Castcdn> list = (List<Castcdn>) objectMap.get("castList");
        Castcdn cast = list.get(0);
        HashMap<Integer, String> bloodMap=new HashMap<Integer, String>();
        bloodMap.put(1, "A");
        bloodMap.put(2, "B");
        bloodMap.put(3, "O");
        bloodMap.put(4, "AB");
        
        Element castObjectElement = null;
        Element castPertyElement = null;

        // 元素节点
        castObjectElement = objectsElement.addElement("Object");
        castObjectElement.addAttribute("ElementType", "Cast");
        castObjectElement.addAttribute("ID", cast.getCastcode());
        castObjectElement.addAttribute("Code", cast.getCastcode());
        castObjectElement.addAttribute("Action", action);

        // 属性节点                
        writeElement(castObjectElement, castPertyElement, "Name",cast.getCastname(), false);
        writeElement(castObjectElement, castPertyElement, "PersonDisplayName", cast.getPersondisplayname(), false); 
        writeElement(castObjectElement, castPertyElement, "PersonSortName", cast.getPersonsortname(), false);  
        writeElement(castObjectElement, castPertyElement, "PersonSearchName", cast.getPersonsearchname(), false);  
        writeElement(castObjectElement, castPertyElement, "FirstName", cast.getFirstname(), false);  
        writeElement(castObjectElement, castPertyElement, "MiddleName", cast.getMiddlename(), false);  
        writeElement(castObjectElement, castPertyElement, "LastName", cast.getLastname(), false);  
        writeElement(castObjectElement, castPertyElement, "Sex", cast.getSex(), true);  
        writeElement(castObjectElement, castPertyElement, "Birthday", cast.getBirthday(), true);  
        writeElement(castObjectElement, castPertyElement, "Hometown", cast.getHometown(), true);  
        writeElement(castObjectElement, castPertyElement, "Education", cast.getEducation(), true);  
        writeElement(castObjectElement, castPertyElement, "Height", cast.getHeight(), true);  
        writeElement(castObjectElement, castPertyElement, "Weight", cast.getWeight(), true);  
        writeElement(castObjectElement, castPertyElement, "BloodGroup", bloodMap.get(cast.getBloodgroup()), true);  
        writeElement(castObjectElement, castPertyElement, "Marriage", cast.getMarriage(), true); 
        writeElement(castObjectElement, castPertyElement, "Favorite", cast.getFavorite(), true); 
        writeElement(castObjectElement, castPertyElement, "Webpage", cast.getWebpage(), true); 
        writeElement(castObjectElement, castPertyElement, "Description", cast.getDescription(), true); 
        
        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
         return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else if(2 == syncType)
        {
            action = "REGIST or UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        
        Date date = new Date();
        String fileName = "cast_epg_" + date.getTime()+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "cast" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        List<Castcdn> list = (List<Castcdn>) objectMap.get("castList");
        Castcdn castObj = list.get(0);
        Element objectElement = null;
        Element propertyElement = null;

        // 元素节点
        objectElement = objectsElement.addElement("Object");
        objectElement.addAttribute("Action", action);
        objectElement.addAttribute("ElementType", "Cast");
        objectElement.addAttribute("Castid", castObj.getCastid());

        // 属性节点
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Name");
        propertyElement.addCDATA(castObj.getCastname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "PersonDisplayName");
        propertyElement.addCDATA(castObj.getPersondisplayname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "PersonSortName");
        propertyElement.addCDATA(castObj.getPersonsortname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "PersonSearchName");
        propertyElement.addCDATA(castObj.getPersonsearchname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FirstName");
        propertyElement.addCDATA(castObj.getFirstname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "MiddleName");
        propertyElement.addCDATA(castObj.getMiddlename());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "LastName");
        propertyElement.addCDATA(castObj.getLastname());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sex");
        if (castObj.getSex() == null || "".equals(castObj.getSex()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getSex().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Birthday");
        if (castObj.getBirthday() == null || "".equals(castObj.getBirthday()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getBirthday());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Hometown");
        propertyElement.addCDATA(castObj.getHometown());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Education");
        propertyElement.addCDATA(castObj.getEducation());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Height");
        if (castObj.getHeight() == null || "".equals(castObj.getHeight()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getHeight().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Weight");
        if (castObj.getWeight() == null || "".equals(castObj.getWeight()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getWeight().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "BloodGroup");
        if (castObj.getBloodgroup() == null || "".equals(castObj.getBloodgroup()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getBloodgroup().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Marriage");
        if (castObj.getMarriage() == null || "".equals(castObj.getMarriage()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(castObj.getMarriage().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Favorite");
        propertyElement.addCDATA(castObj.getFavorite());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Webpage");
        if (castObj.getWebpage() == null || "".equals(castObj.getWebpage()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addCDATA(castObj.getWebpage());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Description");
        propertyElement.addCDATA(castObj.getDescription());

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            retPath = null;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    retPath = null;
                }
            }
        }

        return retPath;
       
    }
     
    //央视规范没有cast
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {
        return "";
    }

}

package com.zte.cms.content.common;

public interface TagLogConstant
{

    public final static String MGTTYPE_TAG_CONTENT = "log.content.tag.mgt";

    // 类型

    public static final String OPERTYPE_LOG_TAG_ADD = "log.tag.add";
    public static final String OPERTYPE_LOG_TAG_MODIFY = "log.tag.modify";
    public static final String OPERTYPE_LOG_TAG_DEL = "log.tag.del";

    public static final String OPERTYPE_LOG_TAG_CONTENT_ADD = "log.tag.content.add";
    public static final String OPERTYPE_LOG_TAG_CONTENT_DEL = "log.tag.content.del";

    // 操作
    public static final String OPERTYPE_LOG_TAG_ADD_INFO = "log.tag.add.info";
    public static final String OPERTYPE_LOG_TAG_MODIFY_INFO = "log.tag.modify.info";
    public static final String OPERTYPE_LOG_TAG_DEL_INFO = "log.tag.del.info";

    public static final String OPERTYPE_LOG_TAG_CONTENT_ADD_INFO = "log.tag.content.add.info";
    public static final String OPERTYPE_LOG_TAG_CONTENT_DEL_INFO = "log.tag.content.del.info";
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

}

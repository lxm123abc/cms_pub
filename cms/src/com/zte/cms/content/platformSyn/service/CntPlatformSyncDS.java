package com.zte.cms.content.platformSyn.service;

import java.util.List;

import com.zte.cms.content.platformSyn.dao.ICntPlatformSyncDAO;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CntPlatformSyncDS extends DynamicObjectBaseDS implements ICntPlatformSyncDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
  	private ICntPlatformSyncDAO dao = null;
	
	public void setDao(ICntPlatformSyncDAO dao)
	{
	    this.dao = dao;
	}

	public void insertCntPlatformSync( CntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug("insert cntPlatformSync starting...");
		Long cntPlatformSyncindex = null;
		try
		{
		    if(cntPlatformSync.getSyncindex() == null){
		        cntPlatformSyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");
		        cntPlatformSync.setSyncindex(cntPlatformSyncindex);
		    }
		    dao.insertCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert cntPlatformSync end");
	}
	
	public Long insertCntPlatformSyncRtn( CntPlatformSync cntPlatformSync )throws DomainServiceException
    {
        log.debug("insert cntPlatformSync starting...");
        Long cntPlatformSyncindex = null;
        try
        {
            if(cntPlatformSync.getSyncindex() == null){
                cntPlatformSyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");
                cntPlatformSync.setSyncindex(cntPlatformSyncindex);
            }
            dao.insertCntPlatformSync( cntPlatformSync );
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug("insert cntPlatformSync end");
        return cntPlatformSyncindex;
    }

    public void updateCntPlatformSync( CntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug("update cntPlatformSync by pk starting...");
	    try
		{
			dao.updateCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntPlatformSync by pk end");
	}

	public void updateCntPlatformSyncList( List<CntPlatformSync> cntPlatformSyncList )throws DomainServiceException
	{
		log.debug("update cntPlatformSyncList by pk starting...");
		if(null == cntPlatformSyncList || cntPlatformSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntPlatformSyncList");
			return;
		}
	    try
		{
			for( CntPlatformSync cntPlatformSync : cntPlatformSyncList )
			{
		    	dao.updateCntPlatformSync( cntPlatformSync );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntPlatformSyncList by pk end");
	}



	public void removeCntPlatformSync( CntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "remove cntPlatformSync by pk starting..." );
		try
		{
			dao.deleteCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntPlatformSync by pk end" );
	}

	public void removeCntPlatformSyncList ( List<CntPlatformSync> cntPlatformSyncList )throws DomainServiceException
	{
		log.debug( "remove cntPlatformSyncList by pk starting..." );
		if(null == cntPlatformSyncList || cntPlatformSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntPlatformSyncList");
			return;
		}
		try
		{
			for( CntPlatformSync cntPlatformSync : cntPlatformSyncList )
			{
				dao.deleteCntPlatformSync( cntPlatformSync );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntPlatformSyncList by pk end" );
	}



	public CntPlatformSync getCntPlatformSync( CntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "get cntPlatformSync by pk starting..." );
		CntPlatformSync rsObj = null;
		try
		{
			rsObj = dao.getCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntPlatformSyncList by pk end" );
		return rsObj;
	}

	public List<CntPlatformSync> getCntPlatformSyncByCond( CntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "get cntPlatformSync by condition starting..." );
		List<CntPlatformSync> rsList = null;
		try
		{
			rsList = dao.getCntPlatformSyncByCond( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntPlatformSync by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntPlatformSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntPlatformSync, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	public void removeCntPlatformSynListByObjindex( List<CntPlatformSync> cntPlatformSynList )throws DomainServiceException  
    {
        log.debug( "remove cntTaPlatformSynList by condition starting..." );
        if(null == cntPlatformSynList || cntPlatformSynList.size() == 0 )
        {
            log.debug("there is no datas in cntTaPlatformSynList");
            return;
        }
        try
        {
            for( CntPlatformSync cntPlatformSync : cntPlatformSynList )
            {
                dao.deleteCntPlatformSyncByObjindex( cntPlatformSync );
            }
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "remove cntTaPlatformSynList by condition end" );
    }

    public CntPlatformSync getCntPlatformSync4Object(CntPlatformSync cntPlatformSync) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get cntPlatformSync by objecttype、objectindex 、platform starting..." );
        CntPlatformSync rsObj = null;
        try
        {
            rsObj = dao.getCntPlatformSync4Object( cntPlatformSync );
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get cntPlatformSyncList by objecttype、objectindex 、platform end" );
        return rsObj;
    }

    public int getCntPlatformSyncChildNumber(CntPlatformSync cntPlatformSync) throws DomainServiceException
    {
        // TODO Auto-generated method stub

        log.debug( "get cntPlatformSync child Targetsystem by objecttype、objectindex 、platform starting..." );
        int  number = 0;
        try
        {
            number = dao.getCntPlatformSyncChildNumber(cntPlatformSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get cntPlatformSync child Targetsystem by objecttype、objectindex 、platform end" );
        return number;
    }
    
}

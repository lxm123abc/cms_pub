package com.zte.cms.cpsp.service;

import com.zte.cms.common.MD5;
import com.zte.cms.notice.dao.INoticemapDAO;
import com.zte.cms.notice.model.Noticemap;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.List;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:35 2019/6/26
 * @Description:
 */
public class FtpUploadServiceImpl extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements FtpUploadService {

    private INoticemapDAO noticemapDao = null;

    public void setNoticemapDao(INoticemapDAO noticemapDao) {
        this.noticemapDao = noticemapDao;
    }


    @Override
    public String upload(Long l) throws DAOException {
        String newName = "";
        //上传FTP
        RIAContext context = RIAContext.getCurrentInstance();
        String qualificationsurl = "";
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        for (FileItem fileItem : fileList) {
            Long attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
            String path = fileItem.getName();
            String name = "";
            if (path.indexOf("/") != -1) {
                //linux
                name = path.substring(path.lastIndexOf("/") + 1);
            } else {
                //windows
                name = path.substring(path.lastIndexOf("\\") + 1);
            }
            String suffix = name.substring(name.indexOf("."));
            newName = attachid + suffix;
            Noticemap noticemap = new Noticemap();
            noticemap.setAttachid(attachid.intValue());
            noticemap.setMessageid(l.intValue());
            noticemap.setAttachurl(newName);
            noticemap.setAttachname(name);
            noticemapDao.insertAttach(fileItem, noticemap);
            qualificationsurl += newName + "|";
        }
        return qualificationsurl;
    }

    public void delete(int attachid,String attachurl) throws DAOException {
        Noticemap noticemap = new Noticemap();
        noticemap.setAttachid(attachid);
        noticemap.setAttachurl(attachurl);
        noticemapDao.deleteAttach(noticemap);
    }
}

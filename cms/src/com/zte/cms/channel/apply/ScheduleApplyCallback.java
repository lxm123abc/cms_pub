package com.zte.cms.channel.apply;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleApplyCallback extends CommonCallback
{
    // 日志
    protected ICmsScheduleDS scheduleds = null;
    protected IScheduleWkfwhisDS scheduleWkfwhisDS;
    protected CmsSchedule schedule;
    protected ScheduleWkfwhis scheduleWkfwhis;
    private Log log = SSBBus.getLog(getClass());

    public ICmsScheduleDS getScheduleds()
    {
        return scheduleds;
    }

    public void setScheduleds(ICmsScheduleDS scheduleds)
    {
        this.scheduleds = scheduleds;
    }

    public IScheduleWkfwhisDS getScheduleWkfwhisDS()
    {
        return scheduleWkfwhisDS;
    }

    public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS)
    {
        this.scheduleWkfwhisDS = scheduleWkfwhisDS;
    }

    public CmsSchedule getSchedule()
    {
        return schedule;
    }

    public void setSchedule(CmsSchedule schedule)
    {
        this.schedule = schedule;
    }

    public ScheduleWkfwhis getScheduleWkfwhis()
    {
        return scheduleWkfwhis;
    }

    public void setScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis)
    {
        this.scheduleWkfwhis = scheduleWkfwhis;
    }

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        String optObjecttype = ChannelConstants.getWorkflowTypeName(ChannelConstants.WKFWTYPECODE_CHANNEL_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(wkflItem.getPboIndex().toString());

        String info;
        String logKey = "";
        if (wkflItem.getNodeId().longValue() == 1)
        {
            logKey = "ucdn.schedule.log.wkfw.cntdistribute.add.apply";
        }
        else
        {
            logKey += "ucdn.schedule.log.wkfw.cntdistribute.add.audit";
        }
        info = ResourceMgt.findDefaultText(logKey);
        if (null != info)
        {
            info = info.replace("[opername]", wkflItem.getExcutorName());
            info = info.replace("[programname]", schedule.getProgramname());
            info = info.replace("[nodename]", wkflItem.getNodeName());
            info = info.replace("[handleevent]", wkflItem.getHandleEvent());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long scheduleindex = wkflItem.getPboIndex();
        schedule = new CmsSchedule();
        schedule.setScheduleindex(scheduleindex);

        // 查询channel对象
        try
        {
            schedule = scheduleds.getCmsSchedule(schedule);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        // strategy对象处理，修改正式表状态
        schedule.setWorkflowlife(wkflItem.getPboState());

        // 工作流对象处理，设置生效时间
        wkflItem.setEffectiveTime("0");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        String opergrid = (String) httpSession.getAttribute("OPERGRPID");
        String CpId = (String) httpSession.getAttribute("CPID");

        // 创建工作流历史对象
        scheduleWkfwhis = schedule.newStrategyWkfwhis();
        scheduleWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
        scheduleWkfwhis.setTaskindex(Long.valueOf(wkflItem.getTaskId()));
        scheduleWkfwhis.setNodeid(wkflItem.getNodeId().longValue());
        scheduleWkfwhis.setNodename(wkflItem.getNodeName());
        scheduleWkfwhis.setOpername(wkflItem.getExcutorId());
        scheduleWkfwhis.setOpopinion(wkflItem.getComments());
        scheduleWkfwhis.setTemplateindex(wkflItem.getTemplateSeqNo().longValue());
        scheduleWkfwhis.setHandleevent(wkflItem.getHandleEvent());
        scheduleWkfwhis.setChannelid(schedule.getChannelid());
        scheduleWkfwhis.setReservecode1(schedule.getReservecode1());

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        if (wkflItem.getStartTime() != null)
        {
            String workflowstarttime = dateformat.format(wkflItem.getStartTime());
            scheduleWkfwhis.setWorkflowstarttime(workflowstarttime);
        }
        if (wkflItem.getFinishTime() != null)
        {
            String workflowendtime = dateformat.format(wkflItem.getFinishTime());
            scheduleWkfwhis.setWorkflowendtime(workflowendtime);
        }
    }

    @Override
    public void operate()
    {
        log.info("domain exception:");
        try
        {
            if (wkflItem.getPboState().intValue() == -1)
            {
                schedule.setStatus(130); // 状态改成审核失败
                schedule.setWorkflow(0); // 未启动
                schedule.setWorkflowlife(0); // 未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                schedule.setEffecttime(format.format(new Date())); // 将生效时间字段设为当前时间
                scheduleds.updateCmsSchedule(schedule);

            }
            else
            {
                scheduleds.updateCmsSchedule(schedule);
            }

            scheduleWkfwhisDS.insertScheduleWkfwhis(scheduleWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

}

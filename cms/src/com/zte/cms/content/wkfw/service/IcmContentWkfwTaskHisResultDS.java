package com.zte.cms.content.wkfw.service;

import java.util.List;

import com.zte.cms.content.wkfw.dao.IIcmContentWkfwTaskHisResultDAO;
import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwTaskResult;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import com.zte.umap.common.EspecialCharMgt;

public class IcmContentWkfwTaskHisResultDS implements IIcmContentWkfwTaskHisResultDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmContentWkfwTaskHisResultDAO dao = null;

    public void setDao(IIcmContentWkfwTaskHisResultDAO dao)
    {
        this.dao = dao;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get IcmContentWkfwTaskResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(contentWkfwCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmContentWkfwTaskResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get IcmContentWkfwTaskResult page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        List<IcmContentWkfwTaskResult> ContentWkfwTaskResult = null;
        log.debug("get IcmContentWkfwTaskResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(contentWkfwCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        ContentWkfwTaskResult = (List<IcmContentWkfwTaskResult>) pageInfo.getResult();
        if (null != ContentWkfwTaskResult)
        {
            for (int i = 0; i < ContentWkfwTaskResult.size(); i++)
            {
                String cpname = ContentWkfwTaskResult.get(i).getCpcnshortname();
                String contentid = ContentWkfwTaskResult.get(i).getContentid();
                if (null == cpname || ("".equals(cpname)))
                {
                    ContentWkfwTaskResult.get(i).setCpcnshortname("该CP已被删除");
                    ContentWkfwTaskResult.get(i).setCpid("该CP已被删除");
                }
                if (null == contentid || ("".equals(contentid)))
                {
                    ContentWkfwTaskResult.get(i).setContentid("该内容已被删除");
                    ContentWkfwTaskResult.get(i).setNamecn("该内容已被删除");
                }
            }
        }
        tableInfo.setData(ContentWkfwTaskResult);
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get IcmContentWkfwTaskResult page info by condition end");
        return tableInfo;
    }

    public long getMyHaveHandleWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition)
            throws DomainServiceException
    {
        log.debug("get getMyHaveHandleWorkflowItemListCount info by  condition starting...");
        long result = 0;
        try
        {

            result = dao.getMyHaveHandleWorkflowItemListCount(contentWkfwCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getMyHaveHandleWorkflowItemListCount info by condition end");
        return result;
    }

}

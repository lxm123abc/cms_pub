package com.zte.cms.channel.apply;


import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IScheduleApply
{

    /**
     * 工作流申请
     * 
     * @param scheduleIdx Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String apply(Long scheduleIdx) throws DomainServiceException;

    /**
     * 
     * @param Schedule CmsSchedule
     * @param templateRoute String
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsSchedule schedule, String templateRoute, String priority)
            throws DomainServiceException;

    /**
     * 工作流审核
     * 
     * @param Schedule CmsSchedule
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String audit(CmsSchedule Schedule, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException;

    /**
     * 
     * @param ScheduleWkfwhis ScheduleWkfwhis
     * @return ChannelWkfwhis
     * @throws Exception Exception
     */
    public ScheduleWkfwhis getScheduleWkfwHis(ScheduleWkfwhis scheduleWkfwhis) throws Exception;

}
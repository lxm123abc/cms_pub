package com.zte.cms.service.categoryservicemap.dao;

import java.util.List;

import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategoryServiceMapDAO
{
     /**
	  * 新增CategoryServiceMap对象 
	  * 
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @throws DAOException dao异常
	  */	
     public void insertCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException;
     
     /**
	  * 根据主键更新CategoryServiceMap对象
	  * 
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @throws DAOException dao异常
	  */
     public void updateCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException;


     /**
	  * 根据主键删除CategoryServiceMap对象
	  *
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @throws DAOException dao异常
	  */
     public void deleteCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException;
     
     
     /**
	  * 根据主键查询CategoryServiceMap对象
	  *
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @return 满足条件的CategoryServiceMap对象
	  * @throws DAOException dao异常
	  */
     public CategoryServiceMap getCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException;
     
     /**
	  * 根据条件查询CategoryServiceMap对象 
	  *
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @return 满足条件的CategoryServiceMap对象集
	  * @throws DAOException dao异常
	  */
     public List<CategoryServiceMap> getCategoryServiceMapByCond( CategoryServiceMap categoryServiceMap )throws DAOException;

     /**
	  * 根据条件分页查询CategoryServiceMap对象，作为查询条件的参数
	  *
	  * @param categoryServiceMap CategoryServiceMap对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CategoryServiceMap对象，作为查询条件的参数
	  *
	  * @param categoryServiceMap CategoryServiceMap对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
        
}
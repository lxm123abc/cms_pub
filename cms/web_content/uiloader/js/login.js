// JavaScript Document
//$(document).ready(function(){
//						   var h = $(".loginBox").height() ;
//						   var w = $(".loginBox").width();
//						   var t = (screen.availHeight - h)/2 ;
//						   var l = (screen.availWidth - w)/2 ;
//						   $(".outeBox").eq(0).css({"top":t-30,left:l,"position":"absolute"});
//			   
//						   })
if (self != top) 
{
  parent.location.href = "login.html";
}
function showMsg(msg)
{
	var obj = document.getElementById("msg");
	obj.innerHTML=msg;
}
function vaildateForm()
{
	var name = document.getElementById("username");
	var pwd = document.getElementById("password");
	var v = document.getElementById("validate");
	document.getElementById ('msg').innerHTML = "";
	if(name.value=='')
	{
		showMessage($res_entry("login.error.username.null",""));
		name.focus();
		return false;
    }
	if(pwd.value=='')
	{
		showMessage($res_entry("login.error.password.null",""));
		pwd.focus();
		return false;
	}
	if(v.value=='')
	{
		showMessage($res_entry("login.error.validCode.null"));
		v.focus();
		return false;
	}
	submitForm();
}

function LoadPage()
{
	var loginErrorType = divErrorType.innerHTML;
	if(loginErrorType == "username")
		document.frmLogin.username.focus();
	else if(loginErrorType == "password")
		document.frmLogin.password.focus();			
}

function showLogin() 
 {  
   var assistant = getAssistant();
  // alert(assistant);
   var temp = assistant.version;
   var versionstr = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ temp;
   document.getElementById ('version').innerHTML= versionstr ;
   var local = getLocaleInfo();
   if(local == "zh")
   {
      document.getElementById ('chinese').setAttribute("selected","selected");
   }
   if(local == "en")
   {
      document.getElementById ('english').setAttribute("selected","selected");
   }      
  // var picurl = $res_entry('picture.login');
  // document.getElementById ('container1').style.background = 'url("'+picurl+'") #013d83 no-repeat';
 }
 
 var cookiestr="";
 function init() 
 {
   var arrStr = document.cookie.split(";");
   for(var i = 0;i < arrStr.length;i ++)
   {
      var temp = arrStr[i].split("=");
      if(temp[0] == " userid" || temp[0] == "userid") 
      {
         cookiestr = unescape(temp[1]);
         if(cookiestr !="undefined"&&cookiestr != undefined)
         {
           document.getElementById('username').value = cookiestr;
         }
      }
      if(cookiestr == "" || cookiestr == undefined)
      {
         document.frmLogin.username.focus();
      }
      else
      {
         document.frmLogin.password.focus();
      }    
   }
   if (top.location.toString().indexOf("?") != -1)
   {
		document.getElementById ('error').innerHTML=$res_entry("login.error.sessiontimeout"); 
   }
 }        
 
 function keyPressInUser() {
	var keyValue;
	keyValue=window.event.keyCode;
	if(keyValue==13) document.frmLogin.password.focus();
}

function keyPressInPassword() {
	var keyValue;
	keyValue=window.event.keyCode;
	if(keyValue==13) document.frmLogin.validate.focus();
}

function keyPressInValidate() {
	var keyValue;
	keyValue=window.event.keyCode;
	if(keyValue==13) vaildateForm();
}

function submitForm() 
{
var username = document.getElementById('username').value;
var password = document.getElementById('password').value;
var vaildCode = document.getElementById('validate').value;
document.cookie ='userid='+ escape(username);
var returnStr = "" ;
returnStr = login(username,password,vaildCode);
showMessage(returnStr);
}
function showMessage(returnStr)
{  
   if(returnStr != undefined)
   {
      document.getElementById ('msg').innerHTML=returnStr; 
   }
}
function submitForm2() 
{
   languageChange(document.form2.localeName.value);
  // window.location.reload();
}

package com.zte.cms.epgtempltsync.dao;

import java.util.List;

import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class EpgtempltSyncTaskDAO extends DynamicObjectBaseDao implements IEpgtempltSyncTaskDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException
    {
        log.debug("insert epgtempltSyncTask starting...");
        super.insert("insertEpgtempltSyncTask", epgtempltSyncTask);
        log.debug("insert epgtempltSyncTask end");
    }

    public void updateEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException
    {
        log.debug("update epgtempltSyncTask by pk starting...");
        super.update("updateEpgtempltSyncTask", epgtempltSyncTask);
        log.debug("update epgtempltSyncTask by pk end");
    }

    public void deleteEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException
    {
        log.debug("delete epgtempltSyncTask by pk starting...");
        super.delete("deleteEpgtempltSyncTask", epgtempltSyncTask);
        log.debug("delete epgtempltSyncTask by pk end");
    }

    public EpgtempltSyncTask getEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException
    {
        log.debug("query epgtempltSyncTask starting...");
        EpgtempltSyncTask resultObj = (EpgtempltSyncTask) super.queryForObject("getEpgtempltSyncTask",
                epgtempltSyncTask);
        log.debug("query epgtempltSyncTask end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<EpgtempltSyncTask> getEpgtempltSyncTaskByCond(EpgtempltSyncTask epgtempltSyncTask) throws DAOException
    {
        log.debug("query epgtempltSyncTask by condition starting...");
        List<EpgtempltSyncTask> rList = (List<EpgtempltSyncTask>) super.queryForList(
                "queryEpgtempltSyncTaskListByCond", epgtempltSyncTask);
        log.debug("query epgtempltSyncTask by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize) throws DAOException
    {
        log.debug("page query epgtempltSyncTask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryEpgtempltSyncTaskListCntByCond", epgtempltSyncTask))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<EpgtempltSyncTask> rsList = (List<EpgtempltSyncTask>) super.pageQuery(
                    "queryEpgtempltSyncTaskListByCond", epgtempltSyncTask, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query epgtempltSyncTask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryEpgtempltSyncTaskListByCond", "queryEpgtempltSyncTaskListCntByCond",
                epgtempltSyncTask, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        // TODO Auto-generated method stub
        if(upstreamEPGTask.getResult() == null ){
            return super.indexPageQuery("queryUpstreamEPGTaskListByCond", "queryUpstreamEPGTaskListCntByCond",
                    upstreamEPGTask, start, pageSize, puEntity);
        }
            
        int findType = upstreamEPGTask.getResult() ;
        if ((findType == 0) || (findType == 999)){
            //如果是待执行999或执行成功0
            return super.indexPageQuery("queryUpstreamEPGTaskListByCond", "queryUpstreamEPGTaskListCntByCond",
                    upstreamEPGTask, start, pageSize, puEntity);
        }else{
            //否则就是查询执行失败的
            return super.indexPageQuery("queryUpstreamEPGTaskListByCondFail", "queryUpstreamEPGTaskListCntByCondFail",
                    upstreamEPGTask, start, pageSize, puEntity);
        }
    }

    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail(UpstreamEPGTaskDetail rsObj) throws DAOException
    {
        // TODO Auto-generated method stub

        log.debug("query UpstreamEPGTaskDetail starting...");
        UpstreamEPGTaskDetail resultObj = (UpstreamEPGTaskDetail) super.queryForObject("getUpstreamEPGTaskDetail",
                rsObj);
        log.debug("query UpstreamEPGTaskDetail end");
        return resultObj;
    }

}
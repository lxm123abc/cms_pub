package com.zte.cms.content.movie.service;

import java.util.List;

import com.zte.cms.common.Generator;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.dao.ICmsMovieDAO;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsMovieDS extends DynamicObjectBaseDS implements ICmsMovieDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsMovieDAO dao = null;

    public void setDao(ICmsMovieDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsMovie(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("insert cmsMovie starting...");
        try
        {
            Long mapinex;
            String mappingid;
            
            if (cmsMovie.getMovieindex() == null)
            {
                mapinex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_movie_index");
                mappingid = String.format("%02d", 32)+ String.format("%030d", mapinex);
                cmsMovie.setMovieindex(mapinex);
            }else{
                mappingid = String.format("%02d", 32)+ String.format("%030d", cmsMovie.getMovieindex());
            }
            //String cpcontentid = Generator.getWGCodeByPhysicalContentId(cmsMovie.getMovieid());
            cmsMovie.setCpcontentid(cmsMovie.getMovieid());
            cmsMovie.setMappingid(mappingid);
            dao.insertCmsMovie(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsMovie end");
    }

    public void updateCmsMovie(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("update cmsMovie by pk starting...");
        try
        {
            dao.updateCmsMovie(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovie by pk end");
    }

    public void updateCmsMovieList(List<CmsMovie> cmsMovieList) throws DomainServiceException
    {
        log.debug("update cmsMovieList by pk starting...");
        if (null == cmsMovieList || cmsMovieList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieList");
            return;
        }
        try
        {
            for (CmsMovie cmsMovie : cmsMovieList)
            {
                dao.updateCmsMovie(cmsMovie);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieList by pk end");
    }

    public void updateCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("update cmsMovie by condition starting...");
        try
        {
            dao.updateCmsMovieByCond(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovie by condition end");
    }

    public void updateCmsMovieListByCond(List<CmsMovie> cmsMovieList) throws DomainServiceException
    {
        log.debug("update cmsMovieList by condition starting...");
        if (null == cmsMovieList || cmsMovieList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieList");
            return;
        }
        try
        {
            for (CmsMovie cmsMovie : cmsMovieList)
            {
                dao.updateCmsMovieByCond(cmsMovie);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieList by condition end");
    }

    public void removeCmsMovie(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("remove cmsMovie by pk starting...");
        try
        {
            dao.deleteCmsMovie(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovie by pk end");
    }

    public void removeCmsMovieList(List<CmsMovie> cmsMovieList) throws DomainServiceException
    {
        log.debug("remove cmsMovieList by pk starting...");
        if (null == cmsMovieList || cmsMovieList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieList");
            return;
        }
        try
        {
            for (CmsMovie cmsMovie : cmsMovieList)
            {
                dao.deleteCmsMovie(cmsMovie);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieList by pk end");
    }

    public void removeCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("remove cmsMovie by condition starting...");
        try
        {
            dao.deleteCmsMovieByCond(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovie by condition end");
    }

    public void removeCmsMovieListByCond(List<CmsMovie> cmsMovieList) throws DomainServiceException
    {
        log.debug("remove cmsMovieList by condition starting...");
        if (null == cmsMovieList || cmsMovieList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieList");
            return;
        }
        try
        {
            for (CmsMovie cmsMovie : cmsMovieList)
            {
                dao.deleteCmsMovieByCond(cmsMovie);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieList by condition end");
    }

    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("get cmsMovie by pk starting...");
        CmsMovie rsObj = null;
        try
        {
            rsObj = dao.getCmsMovie(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsMovieList by pk end");
        return rsObj;
    }

    public List<CmsMovie> getCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("get cmsMovie by condition starting...");
        List<CmsMovie> rsList = null;
        try
        {
            rsList = dao.getCmsMovieByCond(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsMovie by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsMovie page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsMovie, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsMovie>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsMovie page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsMovie page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryEncryption(cmsMovie, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsMovie>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsMovie page info by condition end");
        return tableInfo;
    }    

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsMovie page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsMovie, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsMovie>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsMovie page info by condition end");
        return tableInfo;
    }

    /**
     * 根据条件查询CmsMovie对象数量
     * 
     * @param CmsMovie
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public int getCmsMovieCount(CmsMovie cmsMovie) throws DomainServiceException
    {
        log.debug("query cmsMovie count by condition starting...");
        int totalCnt = 0;
        try
        {
            totalCnt = dao.getCmsMovieCount(cmsMovie);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query cmsMovie count by condition end");
        return totalCnt;
    }
}

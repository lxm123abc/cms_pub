package com.zte.cms.cntSyncRecord.service;

import java.util.List;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICntSyncRecordDS
{
    /**
     * 新增CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public void insertCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException;

    /**
     * 更新CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException;

    /**
     * 批量更新CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncRecordList(List<CntSyncRecord> cntSyncRecordList) throws DomainServiceException;

    /**
     * 删除CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException;

    /**
     * 批量删除CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncRecordList(List<CntSyncRecord> cntSyncRecordList) throws DomainServiceException;

    /**
     * 查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @return CntSyncRecord对象
     * @throws DomainServiceException ds异常
     */
    public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord) throws DomainServiceException;

    /**
     * 根据条件查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @return 满足条件的CntSyncRecord对象集
     * @throws DomainServiceException ds异常
     */
    public List<CntSyncRecord> getCntSyncRecordByCond(CntSyncRecord cntSyncRecord) throws DomainServiceException;

    /**
     * 根据条件分页查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

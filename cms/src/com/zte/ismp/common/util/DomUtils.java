package com.zte.ismp.common.util;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Comment;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomUtils
{
    /**
     * 比较节点名称和指定的名称是否相等
     * 
     * @param node
     * @param desiredName
     * @return true 相等 ，false 不相等
     */
    public static boolean nodeNameEquals(Node node, String desiredName)
    {
        if (node == null)
        {
            return false;
        }
        if (desiredName == null || desiredName.trim().length() == 0)
        {
            return false;
        }

        return desiredName.equals(node.getNodeName()) || desiredName.equals(node.getLocalName());
    }

    /**
     * 得到指定的子节点列表
     * 
     * @param ele 父节点
     * @param childEleName 子节点名称
     * @return
     */
    public static List getChildElementsByTagName(Element ele, String childEleName)
    {
        NodeList nl = ele.getChildNodes();
        List childEles = new ArrayList();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);
            if (node instanceof Element && nodeNameEquals(node, childEleName))
            {
                childEles.add(node);
            }
        }
        return childEles;
    }

    /**
     * 返回第一个符合条件的子节点对象
     * 
     * @param ele
     * @param childEleName
     * @return
     */
    public static Element getChildElementByTagName(Element ele, String childEleName)
    {
        NodeList nl = ele.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);
            if (node instanceof Element && nodeNameEquals(node, childEleName))
            {
                return (Element) node;
            }
        }
        return null;
    }

    /**
     * 得到DOM元素的节点值
     * 
     * @param valueEle
     * @return
     */
    public static String getTextValue(Element valueEle)
    {
        StringBuffer value = new StringBuffer();
        NodeList nl = valueEle.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node item = nl.item(i);
            if ((item instanceof CharacterData && !(item instanceof Comment)) || item instanceof EntityReference)
            {
                value.append(item.getNodeValue());
            }
        }
        return value.toString();
    }

    /**
     * 得到第一个符合条件的子节点的值
     * 
     * @param ele
     * @param childEleName
     * @return null 没有找到子节点
     */
    public static String getChildElementValueByTagName(Element ele, String childEleName)
    {
        Element child = getChildElementByTagName(ele, childEleName);
        return (child != null ? getTextValue(child) : null);
    }

}

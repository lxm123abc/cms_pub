package com.zte.cms.content.movie.ls;

import java.util.List;

import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsMovieLS
{

    /**
     * 新增子内容
     * 
     * @param cmsMovie
     */
    public String insertCmsMovie(CmsMovie cmsMovie, int type) throws Exception;

    /**
     * 根据movieindex获取子内容
     * 
     * @param cmsMovie
     * @return CmsMovie
     */
    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws Exception;

    /**
     * 修改子内容
     * 
     * @param cmsMovie
     * @return 操作结果
     */
    public String updateCmsMovie(CmsMovie cmsMovie) throws Exception;

    /**
     * 子内容修改
     * 
     * @param cmsProgram
     * @return
     * @throws Exception
     */
    public String movieModApply(CmsMovie cmsMovie) throws Exception;

    /**
     * 根据movieindex 删除子内容
     * 
     * @param movieIndex 子内容编号
     * @return 操作结果
     */
    public String deleteCmsMovie(String movieIndex) throws Exception;

    /**
     * 根据条件分页查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws Exception;

    /**
     * 获取子内容预览路径
     * 
     * @param fileindex 子内容索引
     * @return 子内容预览路径
     * @throws Exception
     */
    public String getPreviewPath(Long movieindex) throws Exception;

    /**
     * 获取子内容预览路径cms.windows.share \\10.129.170.22\root\ +路径形式
     * 
     * @param fileindex 子内容索引
     * @return 子内容预览路径
     * @throws Exception
     */
    public String getPreviewPath2(Long movieindex) throws Exception;

    /**
     * 获取内容缓存表预览路径
     * 
     * @param movieindex
     * @return
     * @throws Exception
     */
    public String getCmsMovieBufPreviewPath(Long movieindex) throws Exception;

    /**
     * 根据 programid 来查询出movie信息
     * 
     * @return
     * @throws Exception
     */
    public List<CmsMovie> getCmsMovieByProgramid(String programid) throws Exception;
    
    /**
     * 获取在线区地址
     * @return
     * @throws Exception
     */
    public String getOnlinePath()throws Exception;
    
    /**
     * 获取制作区地址
     * @return
     * @throws Exception
     */
    public String getMakeAreaPath()throws Exception;
    
    /**
     * 根据母片索引查询母片将母片信息添加到子内容中
     * @param fileindex 母片索引
     * @return 子内容
     * @throws Exception
     */
    public CmsMovie getSourcefile(Long fileindex) throws Exception;
    
    /**
     * 根据内容索引获取cpid
     * @param programindex 内容索引
     * @return cpid
     * @throws Exception
     */
    public String getCpid(Long programindex)throws Exception;
    
    /**
     * 根据条件分页查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */    
    public TableDataInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws Exception;

}

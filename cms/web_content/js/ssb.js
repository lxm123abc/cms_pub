
function isEmpty(str)
{
   var i;
   for (i = 0;i < str.length;i++)
   {
      if (str.charAt(i) != ' ')
      { 
         return false;
      }
   }
   return true;
}
/**
*
*为空 返回true 否则返回false
*/
function Empty(obj,desc)
{

   if(isEmpty(obj.value))
   {
       alert(desc + "不能为空 " );
       obj.focus();
       return true;
   }
   return false;
}

function strlen(str)
{
   var i;
   var len;
   len = 0;
   for (i = 0; i < str.length; i++)
   {
      if (str.charCodeAt(i)>255)
      { 
          len+=2;
      }
      else
      {
           len++;
      }
   }
   return len;
}
/**
*长度大于指定长度 返回true 否则false
*/
function checklen(obj,targetLen,desc)
{
   var i;
   var len;
   len = 0;
   for (i = 0; i < obj.value.length; i++)
   {
      if (obj.value.charCodeAt(i)>255)
      { 
          len+=2;
      }
      else
      {
           len++;
      }
   }
 
   if(len > targetLen){
     alert(desc + " 的长度不能大于 " + targetLen);
     obj.focus();
     return true;
   }
   return false;
}

function checkInvalidChar(field)
{
	
	for (i = 0; i < field.length; i++)
	{
		var c = field.charAt(i);

		if( c == '&' || c == '<' || c == ';' || c == '*' || c == '(' || c == ')'
		   || c == '>' || c == '"'||c == ',' || c== '\\' 
		   || c == '@' || c == '#'|| c == "'" || c == '?' || c==':' || c=='`'
		   || c == '$' || c == '^' ||  c == '~' || c == '%' || c == '!' || c == '=')
		{
			return true;
		}
	}
	return false; 
}
/**
*存在不允许输入的特殊字符 返回true 否则fasle
*/
function checkChar(obj,desc)
{
	if(checkInvalidChar(obj.value))
	{
	  alert(desc + "存在不允许输入的特殊字符 ");
      obj.focus();
      return true;
	   
	}
	return false;
	
}
function checkSpace(obj,desc)
{
	if(checkInvalidChar(obj.value))
	{
	  alert(desc + "多条件模糊查询请以空格分隔 ");
      obj.focus();
      return true;	   
	}
	return false;
	
}
function checkpageCode(field)
{
	
	var patrn=/^([a-zA-Z0-9]|[._-])*$/; 
    if (patrn.test(field))
    {
      return false; 
    }  
    return true; 
}
/**
*存在不允许输入字符 返回true 否则fasle
*/
function checkCode(obj,desc)
{
  	if(checkpageCode(obj.value))
	{
	  alert(desc + "只能输入字母、数字、“_”、“.”、“-”的字串 ");
      obj.focus();
      return true;
	   
	}
	return false;
}
function checkpageurl(field)
{
    
	var field = field.toString().replace(new RegExp("\s"), "");
	
	
	var patrn=/^[\w._/\-:?&=%~]*(\s)*$/; 

	
    if (patrn.test(field))
    {
      return false; 
    }  
    return true; 
}

function checkfileurl(field)
{
    
	var field = field.toString().replace(new RegExp("\s"), "");
	
	
    var patrn = /^[\w._/\-=%~]*(\s)*$/; 

	
    if (patrn.test(field))
    {
      return false; 
    }  
    return true; 
}
/**
*存在不允许输入字符 返回true 否则fasle
*/
function checkurl(obj,desc)
{
    if(checkpageurl(obj.value))
	{
	  alert(desc + "只能输入字母、数字、“_ . / - : ? & = ~ % ”的字串 ");
      obj.focus();
      return true;
	   
	}
	return false;
	
}
function checksystemNameEn(field)
{
	
	
	var patrn=/^([a-zA-Z0-9]|[./\s])*$/;
    if (patrn.test(field))
    {
      return false; 
    }  
    return true; 
}

//注册的名字只能是以英文字母开头
function checkRegName(obj)
	{
	   var patrn=/^[a-zA-Z]([a-zA-Z0-9_])*$/;
	   if(patrn.test(obj))
	   {
	       return false;
	   }
	   return true;
	}
//只能输入数字
function Number(field)
{
	var patrn=/^([0-9]|)*$/;
    if (patrn.test(field))
    {
      return false; 
    }  
    return true; 
}

function checkNumber(obj,desc)
{
    if(Number(obj.value))
	{
	  alert(desc + "只能输入数字 ");
      obj.focus();
      return true;
	   
	}
	return false;
	
}

/**
*存在不是字母的字符 返回true 否则fasle
*/
function checkNameEn(obj,desc)
{
	
    if(!checksystemNameEn(obj.value))
	{
	  alert(desc + "只能输入字母 ");
      obj.focus();
      return true;
	   
	}
	return false;
}


function isEarlyDate(deployDate)
{
	var today = new Date();
	var currentDate = today.getYear() +"-" + (today.getMonth() + 1) + "-" + today.getDate();
	
  	var time1 = new Date(deployDate.replace("-","/").replace("-","/"));   
    var time2 = new Date(currentDate.replace("-","/").replace("-","/"));
    
  	if((time1.getTime()-time2.getTime()) < 0) 
  	{  
      	return true;   
  	}
  	return false;      	
}

function checkisEarlyDate(obj,desc)
{
    if(isEarlyDate(obj.value))
    {
	  alert(desc + "不能早于当前日期 ");
      obj.focus();
      return true;
    }
    return false;    	
}
function compareDate(startDate,endDate)
{     
  var   time1=new   Date(startDate.replace("-","/").replace("-","/"));   
  var   time2=new   Date(endDate.replace("-","/").replace("-","/"));   
    
  if((time1.getTime()-time2.getTime()) > 0) 
  {  
      return true;   
  }
  return false;  
   
} 

function checkcompareDate(obj1,desc1,obj2,desc2)
{     
     if(compareDate(obj1.value,obj2.value))
    {
	  alert(desc1 + "不能晚于 "+desc2);
      obj1.focus();
      return true;
    }
    return false; 
   
}    

/**
*  输入只能为英文,数字,下划线
*  输入为英文,数字,下划线时返回false, 否则返回true
*/

function checkForAssisInput(obj)
{
   var patrn=/^([a-zA-Z0-9_])*$/;
   if(patrn.test(obj))
   {
       return false;
   }
   return true;
}

//////////////////////////////
function getCurrentPageNum(contextPath)
{
   var value = document.getElementById("currentPage");   
   if(value == null)
   {
     return;
   }
   
//   var table = document.getElementById(form);
//   var newRow = table.insertRow();
//   var td=newRow.insertCell();
//   td.innerHTML="<input name='searchPage' value='searchPage' type='hidden'/>";
   
   var text = value.innerText;
   var arr = text.split("/");
   var pageNum = arr[0].substring(1,arr[0].length-1);
  //save zhe current page number in session 
  //zhang.hongwei
  var url = contextPath+"/savePageNum.do?currentPageNum="+pageNum;
   savePageNumByAjax(url);
}   

function getPageNum(contextPath)
{
   var value = document.getElementById("currentPage");   
   if(value == null)
   {
     return;
   }
   
//   var table = document.getElementById(form);
//   var newRow = table.insertRow();
//   var td=newRow.insertCell();
//   td.innerHTML="<input name='searchPage' value='searchPage' type='hidden'/>";
   
   var text = value.innerText;
   var arr = text.split("/");
   var pageNum = arr[0].substring(1,arr[0].length-1);
  //save zhe current page number in session 
  //zhang.hongwei
   return pageNum;
}   	
	
	//////////////////////////////////////////////
	////////////begin Ajax//////////////
	function createXMLHttp()
    {
	xmlHttp = false;
	if(typeof XMLHttpRequest != 'undefined')
	{
		return new XMLHttpRequest();
	}else if(window.ActiveXObject)
	{ 
		var IEXMLHttpVersion = ["MSXML2.XMLHttp.5.0","MSXML2.XMLHttp.4.0","MSXML2.XMLHttp.3.0","MSXML2.XMLHttp","Microsoft.XMLHttp"];
	//for Microsoft Internet Explorer
		for(var i= 0 ;i < IEXMLHttpVersion.length; i++)
		{
			try
			{
				return  new ActiveXObject(IEXMLHttpVersion[i]); 

			}catch(e)
			{
			//Do nothing
			}
		}//end for
	}
	else
	{
		throw new Error("XMLHttp object could be created.");
	}
}


function createDocument() 
{

    var aVersions = [ "MSXML2.DOMDocument.5.0",

       "MSXML2.DOMDocument.4.0","MSXML2.DOMDocument.3.0",

       "MSXML2.DOMDocument","Microsoft.XmlDom"

    ];

      for (var i = 0; i < aVersions.length; i++) {

         try {

             var oXmlDom = new ActiveXObject(aVersions[i]);

             return oXmlDom;

         } catch (oError) {

               // ä¸åä»»ä½å¤ç
		 }//end catch

	 } //end for
	 return new Error("fail to createDocument ,MSXML is not installed");

}//end function	  createDocument

function getXmlDom(sXml)
{
	var oXmlDom;

	//firefox
	if(typeof(DomParser) != 'undefined' )
	{
		var oParser = new DomParser();
		oXmlDom = oParser.parseFromString(sXml);
	}
	else
	{
		
		oXmlDom = createDocument();
		///sXml='<datalist><data id="11" label="name11" /><data id="22" label="name22" /></datalist>';
		oXmlDom.loadXML(sXml);
	}
	return oXmlDom;
}


//å·æ°æå¡çç»ä»¶
function savePageNumByAjax(url)
{
   
    var comboId='serviceSelect';
	var xmlHttp = createXMLHttp();
//	var Id = window.document.getElementById("hiddenId");
//	var url = "${contextPath}/servicesToImplXml.do?serviceId="+Id.value;
   
//	alert(url);
	xmlHttp.open("GET", url, true);
	xmlHttp.onreadystatechange = function ()
	{
		var State = xmlHttp.readyState;
		
		if(State=="4")
		{

			if(xmlHttp.status==200)
			{
				
			}
		}
	}//end function 
	xmlHttp.send(null);
 
}

/////////////////////////end Ajax/////////////////////////////	 

package com.zte.cms.storageManage.service;

import java.util.List;
import com.zte.cms.storageManage.model.CmsDeviceWindiskidBind;
import com.zte.cms.storageManage.dao.ICmsDeviceWindiskidBindDAO;
import com.zte.cms.storageManage.service.ICmsDeviceWindiskidBindDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsDeviceWindiskidBindDS implements ICmsDeviceWindiskidBindDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsDeviceWindiskidBindDAO dao = null;

    public void setDao(ICmsDeviceWindiskidBindDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException
    {
        log.debug("insert cmsDeviceWindiskidBind starting...");
        try
        {
            dao.insertCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsDeviceWindiskidBind end");
    }

    public void updateCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException
    {
        log.debug("update cmsDeviceWindiskidBind by pk starting...");
        try
        {
            dao.updateCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsDeviceWindiskidBind by pk end");
    }

    public void updateCmsDeviceWindiskidBindList(List<CmsDeviceWindiskidBind> cmsDeviceWindiskidBindList)
            throws DomainServiceException
    {
        log.debug("update cmsDeviceWindiskidBindList by pk starting...");
        if (null == cmsDeviceWindiskidBindList || cmsDeviceWindiskidBindList.size() == 0)
        {
            log.debug("there is no datas in cmsDeviceWindiskidBindList");
            return;
        }
        try
        {
            for (CmsDeviceWindiskidBind cmsDeviceWindiskidBind : cmsDeviceWindiskidBindList)
            {
                dao.updateCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsDeviceWindiskidBindList by pk end");
    }

    public void removeCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException
    {
        log.debug("remove cmsDeviceWindiskidBind by pk starting...");
        try
        {
            dao.deleteCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsDeviceWindiskidBind by pk end");
    }

    public void removeCmsDeviceWindiskidBindList(List<CmsDeviceWindiskidBind> cmsDeviceWindiskidBindList)
            throws DomainServiceException
    {
        log.debug("remove cmsDeviceWindiskidBindList by pk starting...");
        if (null == cmsDeviceWindiskidBindList || cmsDeviceWindiskidBindList.size() == 0)
        {
            log.debug("there is no datas in cmsDeviceWindiskidBindList");
            return;
        }
        try
        {
            for (CmsDeviceWindiskidBind cmsDeviceWindiskidBind : cmsDeviceWindiskidBindList)
            {
                dao.deleteCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsDeviceWindiskidBindList by pk end");
    }

    public CmsDeviceWindiskidBind getCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException
    {
        log.debug("get cmsDeviceWindiskidBind by pk starting...");
        CmsDeviceWindiskidBind rsObj = null;
        try
        {
            rsObj = dao.getCmsDeviceWindiskidBind(cmsDeviceWindiskidBind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsDeviceWindiskidBindList by pk end");
        return rsObj;
    }

    public List<CmsDeviceWindiskidBind> getCmsDeviceWindiskidBindByCond(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException
    {
        log.debug("get cmsDeviceWindiskidBind by condition starting...");
        List<CmsDeviceWindiskidBind> rsList = null;
        try
        {
            rsList = dao.getCmsDeviceWindiskidBindByCond(cmsDeviceWindiskidBind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsDeviceWindiskidBind by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsDeviceWindiskidBind page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsDeviceWindiskidBind, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsDeviceWindiskidBind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsDeviceWindiskidBind page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsDeviceWindiskidBind page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsDeviceWindiskidBind, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsDeviceWindiskidBind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsDeviceWindiskidBind page info by condition end");
        return tableInfo;
    }
}

function setLabelValue(labelId,labelmsg)
{
      	document.getElementById(labelId).innerText=labelmsg;
}
 function getTime(str)
{
		if (str.length > 6)
		{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
		}else{
			return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
		}
}
//获得当前时间yyyymmdd
    function time(){
	       var myDate=new Date();
		   var yue=myDate.getMonth()+1;
		   if(yue<10){
		       yue=myDate.getFullYear().toString().concat("0",yue);
		   }else{
		       yue=myDate.getFullYear().toString().concat(yue);
		   }
		   var ri=myDate.getDate();
		   if(myDate.getDate()<10){
		      ri=yue.concat("0",ri);
		   }else{
		      ri=yue.concat(ri);
		   }
		   return parseInt(ri) ;
		}
 function formatStarttime()
 {
    var time = getRiaCtrlValue('starttime')
	 return getTime(time);
 }
/**
 function formatEndtime()
 {
 var time = getRiaCtrlValue('endtime')
	 return getTime(time);
 }
 **/
function get19Time(time14)
{
	if(time14 != undefined && time14 != null && strTrim(time14) != "")
    {
     	   var  tmpTime = strTrim(time14);
     	   if(tmpTime.length == 14)
     	   {
     	   	   var year = tmpTime.substring(0,4);
     	   	   var month = tmpTime.substring(4,6);
     	   	   var day = tmpTime.substring(6,8);
     	   	   var hour = tmpTime.substring(8,10);
     	   	   var minutes = tmpTime.substring(10,12);
     	   	   var seconds = tmpTime.substring(12,14);
     	   	   return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
     	   }
     	   else
     	   {
     	   	 	return "";
     	   }
   	}
   	else
   	{
   		  return "";
   	}
}

function getNowFormatDate()
{
   var day = new Date();
   
   var Year = 0;
   var Month = 0;
   var Day = 0;
   var hour = 0;
   var minute = 0;
   var second = 0;
   var CurrentDate = "";
   //初始化时间   
   Year       = day.getFullYear();
   Month      = day.getMonth()+1;
   Day        = day.getDate();
   hour = day.getHours();
   minute = day.getMinutes();
   second = day.getSeconds();
   
   CurrentDate += Year ;
   
   if (Month >= 10 )
   {
    CurrentDate += Month ;
   }
   else
   {
    CurrentDate += "0" + Month ;
   }
   if (Day >= 10 )
   {
    CurrentDate += Day ;
   }
   else
   {
    CurrentDate += "0" + Day ;
   } 
   
   if (hour >= 10 )
   {
    CurrentDate += hour ;
   }
   else
   {
    CurrentDate += "0" + Month ;
   }
   if (minute >= 10 )
   {
    CurrentDate += minute ;
   }
   else
   {
    CurrentDate += "0" + minute ;
   } 
   if (second >= 10 )
   {
    CurrentDate += second ;
   }
   else
   {
    CurrentDate += "0" + second ;
   }
   return CurrentDate;
}
function turndatetostring(starttime){
	
	var finalstring = starttime.substring(0,4)+starttime.substring(5,7)+starttime.substring(8,10)+
	starttime.substring(11,13)+starttime.substring(14,16)+starttime.substring(17,19);
	
	return finalstring;
}

function validatorPage(){
 //var valid="` ~ ! @ # $ % ^ & * ( ) = + \\ | [ { ] } ; ' \" , < > /";
        var valid="!@#$%^&*;|?()[]{}<>'\"";
        setLabelValue("programname_msg",'');
        setLabelValue("starttime_msg",'');
        setLabelValue("duration_msg",'');
        setLabelValue("description_msg",'');
        setLabelValue("storageduration_msg",'');
       if(document.all.programname.value.trim()==""){
         setLabelValue("programname_msg",$res_entry("msg.info.basicdata.033"));
		     document.all.programname.focus();
         return false;
        }  
         if(utf8_strlen2(getHtmlCtrlTrimValue('programname'))>128){
         setLabelValue("programname_msg",$res_entry("msg.info.basicdata.120"));
		     document.all.programname.focus();
          return false;
       }
      
      if(checkstring(document.all.programname.value,valid))
	  {
        setLabelValue("programname_msg",$res_entry("msg.info.channel.023")+"!@#$%^&*;|?()\"[]{}<>'");
        document.all.programname.focus();
        return false;
      }
      document.getElementById("programname_msg").innerText="";
      
      var text=document.getElementById("upInsert").innerHTML;
      if(document.all.starttime.value=="")
	  {
         setLabelValue("starttime_msg",$res_entry("msg.info.basicdata.033"));
		     document.all.starttime.focus();
         return false;
      }
      var today = getNowFormatDate(); 

 
      var stattimevalue = document.all.starttime.value;
      stattimevalue = turndatetostring(stattimevalue);
      
      
      if(stattimevalue<today){
    	  setLabelValue("starttime_msg","节目开始时间应大于当前时间");
		     document.all.starttime.focus();
      return false;
      }
	  setLabelValue("starttime_msg","");
      // var duration=document.all.duration.value.trim();
      var dura={};
      dura=document.getElementsByName("duration1");
      var storageduration =0;
      storageduration = document.getElementById("storageduration").value;
      var duration=dura[0].value+dura[1].value+dura[2].value;
	   if(duration=="")
	  {
         setLabelValue("duration_msg",$res_entry("msg.info.basicdata.033"));
		     document.all.duration.focus();
         return false;
      }
      if(duration.length<6)
	  {
         setLabelValue("duration_msg",$res_entry("msg.info.channel.022"));
		 document.all.duration.focus();
         return false;
      }
	  if(checkstring(duration,"!@#$%^&*;|?()[]{}<>'\"+")){
        setLabelValue("duration_msg",$res_entry("msg.info.channel.023")+"!@#$%^&*;|?()\"[]{}<>'+");
        document.all.description.focus();   
        return false;
      }
      //if(isNaN(duration)||duration.indexOf(".")!=-1||duration.substring(0,1)==0||duration<=0)
	  if(isNaN(duration)||duration.indexOf(".")!=-1||duration<=0)
	  {
         setLabelValue("duration_msg",$res_entry("msg.info.channel.025"));
		 document.all.duration.focus();
         return false;
      }
      if(duration.substring(0,1)<3){
	          if(duration.substring(0,1)==0&&duration.substring(1,2)<=9&&duration.substring(2,3)<=5&&duration.substring(3,4)<=9&&duration.substring(4,5)<=5&&duration.substring(5,6)<=9){
	          }else if(duration.substring(0,1)==1&&duration.substring(1,2)<=9&&duration.substring(2,3)<=5&&duration.substring(3,4)<=9&&duration.substring(4,5)<=5&&duration.substring(5,6)<=9){
	          }else if(duration.substring(0,1)==2&&duration.substring(1,2)<4&&duration.substring(2,3)<=5&&duration.substring(3,4)<=9&&duration.substring(4,5)<=5&&duration.substring(5,6)<=9){
	          }else{
	             setLabelValue("duration_msg",$res_entry("msg.info.channel.022"));
				 document.all.duration.focus();
		         return false;
	          }
      }else{
         setLabelValue("duration_msg",$res_entry("msg.info.channel.022"));
 		 document.all.duration.focus();
      	 return false;
	  }
      if(duration.length>6)
	  {
         setLabelValue("duration_msg",$res_entry("msg.info.channel.024"));
		 document.all.duration.focus();
         return false;
      }
      document.getElementById("duration").value=duration;
	  //setLabelValue("duration_msg",'');
	  if(storageduration!=''){
		  if(isNaN(storageduration)||storageduration.indexOf(".")!=-1||storageduration<=0)
		  {
			  setLabelValue("storageduration_msg",$res_entry("msg.info.channel.025"));
			  document.all.storageduration.focus();
			  return false;
		  }
	  }
	  var valids="!@#$%^&*;|?()[]{}<>'\"";
	  if(checkstring(document.all.description.value.trim(),valids)){
        setLabelValue("description_msg",$res_entry("msg.info.channel.023")+"!@#$%^&*;|?()\"[]{}<>'");
        document.all.description.focus();      
        return false;
      }
     if(utf8_strlen2(getHtmlCtrlTrimValue('description'))>1024){
          setLabelValue("description_msg",$res_entry("msg.info.basicdata.090"));
          document.all.description.focus();
          return false;
      }
      document.getElementById("description_msg").innerText="";
      return true;     
     
  }
   function checkstring(checkstr,userinput)
    {

    var allValid = false;
    for (i = 0;i<userinput.length;i++) {

        ch = userinput.charAt(i);

        if(checkstr.indexOf(ch) >= 0) {

            allValid = true;

            break;
          }
      }
     return allValid;
   }
   function curDateTime(){
    var curDate = new Date();
    var minutes = curDate.getMinutes()+15;
	curDate.setMinutes(minutes);
    var DTAsString = "";
    var y = 0;
    var m = 0;
    var d = 0;
    var h = 0;
    var mi = 0;
    var s = 0;

    y = curDate.getYear();
    m = curDate.getMonth() + 1;
    d = curDate.getDate();

    if(y < 100){
        DTAsString += (1900+y);
    }else{
        DTAsString = y;
    }
    if(m < 10){
        DTAsString += "-0" + m;
    }else{
        DTAsString += "-" + m;
    }
    if(d < 10){
        DTAsString += "-0" +d;
    }else{
        DTAsString += "-" + d;
    }

    DTAsString += " ";
    h = curDate.getHours();
    mi = curDate.getMinutes();
    s = curDate.getSeconds();
    if (h < 10) {
        DTAsString += "0" + h;
    }else{
        DTAsString += h;
    }
    if (mi < 10) {
        DTAsString += ":0" + mi;
    }else{
        DTAsString += ":" + mi;
    }
    if (s < 10) {
        DTAsString += ":0" + s;
    }else{
        DTAsString += ":" + s;
    }
    return DTAsString;
}

	function showErrorText(id, text){
		var obj = document.getElementById(id + "_msg");
		if (obj)
		{
			if (text != "")
			{
				obj.innerText = text;
			}else{
				obj.innerText = $res_entry("msg.info.basicdata.033");
			}
		}
	}
	function utf8_strlen2(str) 
    { 
        var cnt = 0; 
	    for( i=0; i<str.length; i++) 
	    { 
		        var value = str.charCodeAt(i); 
		        if( value < 0x080) 
		        { 
		            cnt += 1; 
		        } 
		        else if( value < 0x0800) 
		        { 
		            cnt += 2; 
		        } 
		        else  if(value < 0x010000)
		        { 
		            cnt += 3; 
		        }
				else
				{
					cnt += 4;
				}
	    } 
      return cnt; 
    }
	function getHtmlCtrlTrimValue(id)
	{
		return strTrim(document.getElementById(id).value);
	}
	function strTrim(str)
	{
	    var num = str.length;
	    var i = 0;
	    for(i = 0; i < num;i++) {
	        if(str.charAt(i) != " ")
	            break;
	    }
	    str = str.substring(i);
	    num = str.length;
	    for(i = num-1; i > -1;i--) {
	        if(str.charAt(i) != " ")
	            break;
	    }
	    str = str.substring(0, i+1);
	    return str;
	}
	function Upload_init(){
	//wanghao8
	var inputs = document.getElementsByTagName("input");
	for(var ii = 0;ii<inputs.length;ii++)
	{
		if(inputs[ii].type.toUpperCase() == "FILE")
		{
			input_file.push(inputs[ii]);
			__file_List.push(inputs[ii]);
			if(typeof(inputs[ii].groupId)=="undefined" || inputs[ii].groupId==null || trim(inputs[ii].groupId)=="")
			{
				file_group["noId"].push(inputs[ii]);
			}
			else
			{
				if(!file_group[inputs[ii].groupId])
					file_group[inputs[ii].groupId] = [];
				
				file_group[inputs[ii].groupId].push(inputs[ii]);
	
			}
		}
		
	}
	
	var atags=document.getElementsByTagName(_ZUPLOAD_TAG);
	if(!atags||atags.length==0)
	{
		atags=document.getElementsByTagName(_UPLOAD_TAG);	
	}
	var upload={};
	for(var i=0;i<atags.length;i++)
	{
		var tagType = atags[i].parentNode;
		var ptn = tagType.tagName.toUpperCase();
		//\u5982\u679c\u63a7\u4ef6\u4e0d\u5728\u8868\u683c\u63a7\u4ef6\u4e2d\u65f6
		if(ptn != _ZTABLE_CLUMN && ptn != _TABLE_CLUMN)
		{
			upload=new UploadTag(atags[i].getAttribute("id"));
		    upload.init();
		}		
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}
var historyStr ="";
function checkMaxInput(_this,maxleng,leftInforId) 
{ 
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 
    var remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum; 
    if(remainnum < 0) 
    { 
        if(_this.value.length!=len) 
        { 
            if((len-_this.value.length)>(maxleng/3)) 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng/3); 
            } 
            else 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng-(len-_this.value.length)); 
            } 
        }else{ 
            _this.value=_this.value.substring(0,maxleng); 
        } 
            left.value=0; 
            return; 
    } 
    historyStr = _this.value;
}

function dealErrResult(errorCode,rtn_expDesc)
 {
	openMsg("1:" + rtn_expDesc);
 }

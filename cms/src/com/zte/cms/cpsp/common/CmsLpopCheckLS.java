package com.zte.cms.cpsp.common;

import java.util.List;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;

public class CmsLpopCheckLS implements ICmsLpopCheckLS
{
    private ICmsLpopNoauditDS ds;
    private IUcpBasicDS ucpBasicDS;

    public boolean checkCmsLpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        List<CmsLpopNoaudit> list = ds.getCmsLpopNoauditByCond(cmsLpopNoaudit);
        if (list == null || list.size() == 0)
        {
            return true;
        }
        return false;
    }

    public boolean checkCmsOpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        List<CmsLpopNoaudit> list = ds.getCmsLpopNoauditByCond(cmsLpopNoaudit);
        if (list == null || list.size() == 0)
        {
            return true;
        }
        return false;
    }

    /*
     * 在进行牌照方和运营商免审设置的时候检查该CP状态是否为非删除(4)(1正常,21同步中,40已同步,41新增同步失败,42删除同步失败)
     * 
     */
    public boolean checkCmsCpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        UcpBasic ucpBasic = new UcpBasic();
        ucpBasic.setCpid(cmsLpopNoaudit.getCpid());
        // ucpBasic.setStatus(1);在CmsLpopNoauditLS中已经对不能绑定的状态进行过滤
        List<UcpBasic> list = ucpBasicDS.getUcpBasicExactByCond(ucpBasic);
        if (list == null || list.size() == 0)
        {
            return false;
        }

        return true;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    public void setDs(ICmsLpopNoauditDS ds)
    {
        this.ds = ds;
    }
}

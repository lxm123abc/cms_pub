package com.zte.cms.content.CSplatformSyn.service;

import java.util.List;

import com.zte.cms.content.CSplatformSyn.dao.ICSCntPlatformSyncDAO;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CSCntPlatformSyncDS extends DynamicObjectBaseDS implements ICSCntPlatformSyncDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
  	private ICSCntPlatformSyncDAO dao = null;
	
	public void setDao(ICSCntPlatformSyncDAO dao)
	{
	    this.dao = dao;
	}

	public void insertCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug("insert cntPlatformSync starting...");
		Long cntPlatformSyncindex = null;
		try
		{
		    if(cntPlatformSync.getSyncindex() == null){
		        cntPlatformSyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");
		        cntPlatformSync.setSyncindex(cntPlatformSyncindex);
		    }
		    dao.insertCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert cntPlatformSync end");
	}
	
	public Long insertCntPlatformSyncRtn( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
    {
        log.debug("insert cntPlatformSync starting...");
        Long cntPlatformSyncindex = null;
        try
        {
            if(cntPlatformSync.getSyncindex() == null){
                cntPlatformSyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");
                cntPlatformSync.setSyncindex(cntPlatformSyncindex);
            }
            dao.insertCntPlatformSync( cntPlatformSync );
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug("insert cntPlatformSync end");
        return cntPlatformSyncindex;
    }

    public void updateCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug("update cntPlatformSync by pk starting...");
	    try
		{
			dao.updateCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntPlatformSync by pk end");
	}

	public void updateCntPlatformSyncList( List<CSCntPlatformSync> cntPlatformSyncList )throws DomainServiceException
	{
		log.debug("update cntPlatformSyncList by pk starting...");
		if(null == cntPlatformSyncList || cntPlatformSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntPlatformSyncList");
			return;
		}
	    try
		{
			for( CSCntPlatformSync cntPlatformSync : cntPlatformSyncList )
			{
		    	dao.updateCntPlatformSync( cntPlatformSync );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntPlatformSyncList by pk end");
	}



	public void removeCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "remove cntPlatformSync by pk starting..." );
		try
		{
			dao.deleteCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntPlatformSync by pk end" );
	}

	public void removeCntPlatformSyncList ( List<CSCntPlatformSync> cntPlatformSyncList )throws DomainServiceException
	{
		log.debug( "remove cntPlatformSyncList by pk starting..." );
		if(null == cntPlatformSyncList || cntPlatformSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntPlatformSyncList");
			return;
		}
		try
		{
			for( CSCntPlatformSync cntPlatformSync : cntPlatformSyncList )
			{
				dao.deleteCntPlatformSync( cntPlatformSync );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntPlatformSyncList by pk end" );
	}



	public CSCntPlatformSync getCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "get cntPlatformSync by pk starting..." );
		CSCntPlatformSync rsObj = null;
		try
		{
			rsObj = dao.getCntPlatformSync( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntPlatformSyncList by pk end" );
		return rsObj;
	}

	public List<CSCntPlatformSync> getCntPlatformSyncByCond( CSCntPlatformSync cntPlatformSync )throws DomainServiceException
	{
		log.debug( "get cntPlatformSync by condition starting..." );
		List<CSCntPlatformSync> rsList = null;
		try
		{
			rsList = dao.getCntPlatformSyncByCond( cntPlatformSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntPlatformSync by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntPlatformSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQueryCha(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQueryCha(cntPlatformSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQuerySche(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuerySche(cntPlatformSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQueryCach(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		// 先进行字符的转换
		cntPlatformSync.setCategoryname(EspecialCharMgt.conversion(cntPlatformSync.getCategoryname()));
		cntPlatformSync.setProgramname(EspecialCharMgt.conversion(cntPlatformSync.getProgramname()));
        cntPlatformSync.setParentid(EspecialCharMgt.conversion(cntPlatformSync.getParentid()));
		try
		{
			pageInfo = dao.pageInfoQueryCach(cntPlatformSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get cntPlatformSync page info by condition starting..." );
		PageInfo pageInfo = null;
		// 先进行字符的转换
        cntPlatformSync.setCategoryname(EspecialCharMgt.conversion(cntPlatformSync.getCategoryname()));
        cntPlatformSync.setProgramname(EspecialCharMgt.conversion(cntPlatformSync.getProgramname()));
        cntPlatformSync.setParentid(EspecialCharMgt.conversion(cntPlatformSync.getParentid()));
		try
		{
			pageInfo = dao.pageInfoQuery(cntPlatformSync, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntPlatformSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntPlatformSync page info by condition end" );
		return tableInfo;
	}
	
	public void removeCntPlatformSynListByObjindex( List<CSCntPlatformSync> cntPlatformSynList )throws DomainServiceException  
    {
        log.debug( "remove cntTaPlatformSynList by condition starting..." );
        if(null == cntPlatformSynList || cntPlatformSynList.size() == 0 )
        {
            log.debug("there is no datas in cntTaPlatformSynList");
            return;
        }
        try
        {
            for( CSCntPlatformSync cntPlatformSync : cntPlatformSynList )
            {
                dao.deleteCntPlatformSyncByObjindex( cntPlatformSync );
            }
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "remove cntTaPlatformSynList by condition end" );
    }

    public CSCntPlatformSync getCntPlatformSync4Object(CSCntPlatformSync cntPlatformSync) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get cntPlatformSync by objecttype、objectindex 、platform starting..." );
        CSCntPlatformSync rsObj = null;
        try
        {
            rsObj = dao.getCntPlatformSync4Object( cntPlatformSync );
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get cntPlatformSyncList by objecttype、objectindex 、platform end" );
        return rsObj;
    }

    public int getCntPlatformSyncChildNumber(CSCntPlatformSync cntPlatformSync) throws DomainServiceException
    {
        // TODO Auto-generated method stub

        log.debug( "get cntPlatformSync child Targetsystem by objecttype、objectindex 、platform starting..." );
        int  number = 0;
        try
        {
            number = dao.getCntPlatformSyncChildNumber(cntPlatformSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get cntPlatformSync child Targetsystem by objecttype、objectindex 、platform end" );
        return number;
    }
    
}

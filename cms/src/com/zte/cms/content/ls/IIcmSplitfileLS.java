package com.zte.cms.content.ls;

import java.util.List;

import com.zte.cms.content.model.CmsSplittask;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmSplitfileLS
{

    /**
     * 拆条：多个小段内容简单合为一个视频内容
     * 
     * @param soureFileIndexs 素材文件主键列表
     * @return
     * @throws Exception
     */
    public String addMultiMergeToOneSplitTask(List<String> soureFileIndexs, String cpid) throws Exception;

    /**
     * 拆条：将一个内容切割为多个视频
     * 
     * @param soureFileIndex 素材文件主键
     * @return
     * @throws Exception
     */
    public String addOneSplitToMutiSplitTask(String soureFileIndex) throws Exception;

    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize);

}

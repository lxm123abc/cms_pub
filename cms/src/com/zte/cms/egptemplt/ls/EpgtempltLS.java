package com.zte.cms.egptemplt.ls;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.FileUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.UpLoadFile;
import com.zte.cms.common.UpLoadResult;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.cms.egptemplt.service.IEpgtempltDS;
import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.cms.epgtempltsync.service.IEpgtempltSyncTaskDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;

import com.zte.cms.commonSync.common.StandardImpFactory;

import com.zte.ismp.common.OperateInfo;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 * @author Administrator 模板管理,查询
 */
public class EpgtempltLS implements IEpgtempltLS {
	// 日志
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	private FileUtil util = new FileUtil();
	
	private Log log = SSBBus.getLog(getClass());
	private ICmsStorageareaLS cmsStorageareaLS;

	private IEpgtempltDS ds = null;
	private IEpgtempltSyncTaskDS syncds = null;

	private DbUtil dbUtil = new DbUtil();
	private Map map = new HashMap<String, List>();

	EpgtempltLS() {
		ResourceMgt.addDefaultResourceBundle("cms_content_resource");
	}

	/**
	 * 
	 * @return IEpgtempltDS
	 */
	public IEpgtempltDS getDs() {
		return ds;
	}

	/**
	 * 
	 * @param ds IEpgtempltDS
	 */
	public void setDs(IEpgtempltDS ds) {
		this.ds = ds;
	}

	/**
	 * 
	 * @return IEpgtempltSyncTaskDS
	 */
	public IEpgtempltSyncTaskDS getSyncds() {
		return syncds;
	}

	/**
	 * 
	 * @param syncds IEpgtempltSyncTaskDS
	 */
	public void setSyncds(IEpgtempltSyncTaskDS syncds) {
		this.syncds = syncds;
	}

	/**
	 * @param epgtemplt Epgtemplt
	 * @return Epgtemplt
	 * @throws DomainServiceException DomainServiceException
	 */
	public Epgtemplt getEpgtemplt(Epgtemplt epgtemplt)
			throws DomainServiceException {
		if (epgtemplt != null) {
			epgtemplt = ds.getEpgtemplt(epgtemplt);
		}

		return epgtemplt;
	}

	/**
	 * 
	 * @return ICmsStorageareaLS
	 */
	public ICmsStorageareaLS getCmsStorageareaLS() {
		return cmsStorageareaLS;
	}

	/**
	 * 
	 * @param cmsStorageareaLS ICmsStorageareaLS
	 */
	public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS) {
		this.cmsStorageareaLS = cmsStorageareaLS;
	}

	/**
	 * @新增template,templtid不能重复
	 */
	/**
	 * @param epgtemplt Epgtemplt
	 * @param paths String
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String newTemplate(Epgtemplt epgtemplt, String[] paths)
			throws DomainServiceException {
		// if (epgtemplt.getTempltid() != null &&
		// !epgtemplt.getTempltid().equals(""))
		// {
		// String resultString = checkRepeat(epgtemplt.getTempltid());
		// if (resultString.equals("1"))
		// {
		// return "0:" + "模板ID重复";
		// }
		// }
		if(epgtemplt.getTempltdftflag() == 1)
		{
			DbUtil db = new DbUtil();
			int num = 0;			
			try{
			    String sqlstr = "select count(1) from zxdbm_cms.epgtemplt where templtdftflag = 1";
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:" + "系统错误!"; 
			}
			if(num >= 1)
			{
				return "1:" + "系统已经存在默认模板"; 
			}
		}
		
		DbUtil db2 = new DbUtil();
		int num2 = 0;			
		try{
		    String sqlstr2 = "select count(1) from zxdbm_cms.epgtemplt where epggroup = '" 
		    	             + epgtemplt.getEpggroup()+"'";
		    num2 = db2.queryForCount(sqlstr2);	
		}catch(Exception e)
		{
			log.error(e);
			return "1:" + "系统错误!"; 
		}
		if(num2 >= 1)
		{
			return "1:" + "系统中已经存在模板编号为'"+epgtemplt.getEpggroup()+"'的模板!"; 
		}
		
		String begintime = epgtemplt.getBegintime();
		String expiredtime = epgtemplt.getExpiredtime();
		String begintimechange = begintime.replaceAll("-", "").replaceAll(" ",
				"").replaceAll(":", "");
		String expiredtimechange = expiredtime.replaceAll("-", "").replaceAll(
				" ", "").replaceAll(":", "");
		epgtemplt.setBegintime(begintimechange);
		epgtemplt.setExpiredtime(expiredtimechange);

		if (epgtemplt.getAddresspath() == null) {
			epgtemplt.setAddresspath("");
		}
		Date date = new Date();
		if (epgtemplt.getBegintime() == null) {
			epgtemplt.setBegintime(dateFormat.format(date));
		}
		if (epgtemplt.getExpiredtime() == null) {
			epgtemplt.setExpiredtime("20991111111111");
		}

		if (epgtemplt.getTempltver() == null) {
			epgtemplt.setTempltver("");
		}
		/*
		 * status模板状态 查看、修改、删除、发布、取消发布--all 0 待发布 查看、修改、删除、发布 10 新增同步中 查看 20
		 * 新增同步成功 查看、修改（同步）、取消发布 30 新增同步失败 查看、修改、删除、发布 40 修改同步中 查看 50 修改同步成功
		 * 查看、修改（同步）、取消发布 60 修改同步失败 查看、修改（同步）、取消发布 70 取消同步中 查看 80 取消同步成功
		 * 查看、修改、删除、发布 90 取消同步失败 查看、修改（同步）、取消发布
		 */
		epgtemplt.setStatus(0);
		// 上传文件并将文件重命名
		String newfilename = null; 

		//String time = dateFormat.format(date);
		// 修改文件名
		if (!paths[0].equals("")) {
			if (!paths[0].equals(paths[1])) {
				newfilename = epgtemplt.getEpggroup()
						+ paths[1].substring(paths[1].lastIndexOf(".") );
			} else {
				newfilename = epgtemplt.getEpggroup()
						+ paths[0].substring(paths[0].lastIndexOf(".") );
			}
			newfilename.replaceAll(" ", "");
			if (paths[0].lastIndexOf("\\") == -1) {
				return "1:" + ResourceMgt.findDefaultText("err.address.error"); // 上传文件路径获取错误
			}

			UpLoadResult result = new UpLoadResult();
			// 路径为file:/F:/study/work/workspaceforidt/cmsnew/form/WEB-INF/classes/，即为编译后的路径
			// URL url =
			// Thread.currentThread().getContextClassLoader().getResource("");
			// 存储在NAS服务器上

			String url = cmsStorageareaLS.getAddress("5");
			log.debug("cmsStorageareaLS.getAddress(5)insert    " + url);
			File Storge = new File(GlobalConstants.getMountPoint() + url);

			// //如果文件夹不存在，则新建文件夹
			if (!Storge.exists()) {
				Storge.mkdirs();
			}
			if (url.equals("1056020") || !Storge.exists()) {
				// ContentUtil.insertOperatorLog("", 6, 1);
				return "1:" + "找不到目标地址";
			}
			String path = GlobalConstants.getMountPoint() + url + "/";
			// String afterpath = path.substring(6);

			log.debug("path      " + path);
			try {
				// 附件大小为500M
				result = UpLoadFile.singleFileUplord(newfilename,
						500L * 1024L * 1024L, path);
				if (result != null) {
					Integer sizeFlag = result.getFlag();

					if (sizeFlag == -1) {// 当文件大小超过500M时
						return "1:"
								+ ResourceMgt
										.findDefaultText("err.file.tolarge"); // 文件过大
					}
				} else {
					return "1:" + ResourceMgt.findDefaultText("operation.fail");
				}   
				String temptar = path  +  "temptar" + File.separator;
				//解压缩zip包
				unZip(path + newfilename,temptar);
				//删除解压缩文件
				com.zte.ismp.common.util.FileUtil fu = new com.zte.ismp.common.util.FileUtil();
				fu.delFolder(path  +  "temptar");								
				epgtemplt.setAddresspath(url + File.separator + newfilename);
			} catch (Exception e) {
				log.error(e);
				File filezip = new File(path + newfilename);
				if(filezip.exists())
				{
					filezip.delete();
				}
				return "1:" + ResourceMgt.findDefaultText("operation.fail");
			}
		}
		ds.insertEpgtemplt(epgtemplt);
		log.debug("insertCmstemplateinfo()");

		// 记录业务日志
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		// 操作对象类型
		String optType = ResourceManager.getResourceText("log.epgtemplate.title"); // epg模板管理
		String optObject = String.valueOf(epgtemplt.getTempltindex());
		String optObjecttype = ResourceManager
				.getResourceText("log.epgtemplate.add"); // 模板新增

		OperInfo loginUser = LoginMgt
				.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
		// 操作员编码
		String operId = loginUser.getOperid();
		// 操作员名称
		String operName = loginUser.getUserId();

		String optDetail = ResourceManager
				.getResourceText("log.epgtemplatedetail.add");
		// 操作员[opername]新增了模板[templatename]
		optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
				.filterDollarStr(operName));
		optDetail = optDetail.replaceAll("\\[templatename\\]", String
				.valueOf(epgtemplt.getTempltindex()));

		loginfo.setUserId(operId);
		loginfo.setOptType(optType);
		loginfo.setOptObject(optObject);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptDetail(optDetail);
		loginfo.setOptTime("");
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		LogInfoMgt.doLog(loginfo);
		String rtn = ResourceManager.getResourceText("msg.info.0000040");
		return "0:" + rtn;
	}

	/**
	 * 修改epg模板的方法
	 */
	/**
	 * @param epgtemplt Epgtemplt
	 * @param paths String
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String renewTemplate(Epgtemplt epgtemplt, String[] paths)
			throws DomainServiceException {

		String rtn = "";
		DbUtil db = new DbUtil();
		Epgtemplt epg = ds.getEpgtemplt(epgtemplt);
		if (epg == null) {
			rtn = ResourceManager.getResourceText("msg.epgtemplate.notexist");
			return "1:" + rtn;
		}
		epgtemplt.setStatus(epg.getStatus());
		epgtemplt.setAddresspath(epg.getAddresspath());
		if(epgtemplt.getTempltdftflag() == 1 && !epg.getTempltdftflag().equals(epgtemplt.getTempltdftflag()))
		{		
			int num = 0;			
			try{
			    String sqlstr = "select count(1) from zxdbm_cms.epgtemplt where templtdftflag = 1";
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:" + "系统错误!"; 
			}
			if(num >= 1)
			{
				return "1:" + "系统已经存在默认模板"; 
			}			
		}
		int num1 = 0;
		String sqlstr1 = "select count(1) from epgtemplt_sync_task where status in (1,2) and "
				          + "templtindex = " + epgtemplt.getTempltindex();
		try
		{			    
			    num1 = db.queryForCount(sqlstr1);			
		}catch(Exception e)
		{
				log.error(e);
				return "1:" + "系统错误!"; 
		}
		if(num1 >= 1)
		{
				return "1:" + "该EPG模板存在'待执行'或'执行中'的任务,不能修改!"; 
		}
		//if(!epgtemplt.getEpggroup().equals(epg.getEpggroup()))
		//{
		//    DbUtil db2 = new DbUtil();
		//    int num2 = 0;			
		//    try{
		//        String sqlstr2 = "select count(1) from zxdbm_cms.epgtemplt where epggroup = '" 
		//    	                 + epgtemplt.getEpggroup()+"'";
		//        num2 = db2.queryForCount(sqlstr2);	
		//    }catch(Exception e)
		//    {
		//	    log.error(e);
		//	    return "1:" + "系统错误!"; 
		//    }
		//    if(num2 >= 1)
		//    {
		//	    return "1:" + "系统中已经存在模板编号为'"+epgtemplt.getEpggroup()+"'的模板!"; 
		//    }
	    //}
		int iftar = 0;
		String validtime = epgtemplt.getBegintime();
		String invalidtime = epgtemplt.getExpiredtime();
		String validtimechange = validtime.replaceAll("-", "").replaceAll(" ",
				"").replaceAll(":", "");
		String invalidtimechange = invalidtime.replaceAll("-", "").replaceAll(
				" ", "").replaceAll(":", "");
		epgtemplt.setBegintime(validtimechange);
		epgtemplt.setExpiredtime(invalidtimechange);
		// remove file
		String url = cmsStorageareaLS.getAddress("5");//模板存储区地址是5
		log.debug("cmsStorageareaLS.getAddress(5)       " + url);
		String path = GlobalConstants.getMountPoint() + epgtemplt.getAddresspath();
		String path1 = GlobalConstants.getMountPoint() + url + File.separator;
		//System.out.println("removepath    " + path);
		String temptar = path1  +  "temptar" + File.separator;;
		//if (!paths[0].equals("")) {
		//	UpLoadFile.removeFile(path);
		//}
		// add file
		String newfilename = epg.getEpggroup() + epg.getAddresspath().substring(epg.getAddresspath().lastIndexOf(".")); 
		
		// 修改文件名
		if (!paths[0].equals("")) {
			if (!paths[0].equals(paths[1])) {
				newfilename = epgtemplt.getEpggroup()
						+ paths[1].substring(paths[1].lastIndexOf("."));
			} else {
				newfilename = epgtemplt.getEpggroup()
						+ paths[0].substring(paths[0].lastIndexOf("."));
			}
			newfilename.replaceAll(" ", "");
			if (paths[0].lastIndexOf("\\") == -1) {
				return "1:" + ResourceMgt.findDefaultText("err.address.error"); // 上传文件路径获取错误
			}

			UpLoadResult result = new UpLoadResult();

			File Storge = new File(GlobalConstants.getMountPoint() + url);

			//如果文件夹不存在，则新建文件夹
			if (!Storge.exists()) {
				Storge.mkdirs();
			}
			if (url.equals("1056020") || !Storge.exists()) {
				// ContentUtil.insertOperatorLog("", 6, 1);
				return "1:" + ResourceMgt.findDefaultText("err.cannotfinddest");
			}
			String addpath = GlobalConstants.getMountPoint() + url + "/";
			// String afterpath = path.substring(6);
			System.out.println("path in the insertcmsnotice method      "
					+ path);

			log.debug("path      " + path);
			try {
				// 附件大小为500M
				result = UpLoadFile.singleFileUplord(newfilename,
						500L * 1024L * 1024L, addpath);
				if (result != null) {
					Integer sizeFlag = result.getFlag();

					if (sizeFlag == -1) {// 当文件大小超过500M时
						return "1:" + ResourceMgt.findDefaultText("err.file.tolarge"); // 文件过大
					}
				} else {
					return "1:" + ResourceMgt.findDefaultText("operation.fail");
				}
				
				temptar = path1  +  "temptar" + File.separator;
				//解压缩zip包
				unZip(path1 + newfilename,temptar);	
				//删除解压缩文件
				com.zte.ismp.common.util.FileUtil fu = new com.zte.ismp.common.util.FileUtil();
				fu.delFolder(path1  +  "temptar");
				epgtemplt.setAddresspath(url + File.separator + newfilename);				

			} catch (Exception e) {
				log.error(e);
				return "1:" + ResourceMgt.findDefaultText("operation.fail");
			}
		}

		if(epgtemplt.getSendto20flag() == 1)
		{
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = 1 and "
				          + "templtindex = " + epgtemplt.getTempltindex();
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:"+"系统错误!";
			}
			if(num == 0)
			{
				String sqlstr20 = " select TASKINDEX,STARTTIME,ENDTIME,STATUS,TEMPLTINDEX,TASKTYPE,PLATFORM FROM EPGTEMPLT_SYNC_TASK_his "
					            + " where STATUS = 3 and platform = 1 and templtindex = "+ epgtemplt.getTempltindex()
					            + " order by endtime desc";
				List listepg20 = new ArrayList();
				try
				{
					listepg20 = db.getQuery(sqlstr20);
					if(listepg20.size()>0)
					{
						HashMap hashMap = (HashMap) listepg20.get(0);
					    int tasktypeint = Integer.parseInt((String)hashMap.get("tasktype"));
					    if(tasktypeint==1)
					    {
					    	//解压缩zip包
							unZip(path1 + newfilename,temptar);	
					    	//打成tar包
							com.zte.ismp.common.util.FileUtil.archive(temptar,path1+epgtemplt.getEpggroup()+".tar");
							//删除解压缩文件
							com.zte.ismp.common.util.FileUtil fu = new com.zte.ismp.common.util.FileUtil();							
							fu.delFolder(path1  +  "temptar");
							iftar = 1;
							List<Epgtemplt> epgTempltList = new ArrayList<Epgtemplt>();
							epgTempltList.add(epgtemplt);
					        map.put("epgtemplt", epgTempltList);
							String epgxml = "";
							try
							{
								epgxml = StandardImpFactory.getInstance().create(1).createXmlFile(map, 0, 16, 2);
							}catch(Exception e)
							{
								log.error(e);
								return "1:" + "xml指令文件生成异常!"; 
							}					   					  					    						    	
					    	EpgtempltSyncTask sync = new EpgtempltSyncTask();
							sync.setPriority(2); // 2中
							sync.setStatus(1); // 修改同步中
							sync.setPlatform(1); //1-2.0平台，2-3.0平台
							sync.setTempltindex(epgtemplt.getTempltindex());
							sync.setTasktype(1); // 2 UPDATE
							sync.setCmdfileurl(epgxml);
							syncds.insertEpgtempltSyncTask(sync);
							epgtemplt.setStatus(40);
					    	
					    }
					}					
				}catch(Exception e)
				{
					log.error(e);
					return "1:"+"系统错误!";
				}
				
			}
			
		}
		if(epgtemplt.getSendto30flag() == 1)
		{
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = 2 and "
				          + "templtindex = " + epgtemplt.getTempltindex();
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:"+"系统错误!";
			}
			if(num == 0)
			{
				String sqlstr30 = " select TASKINDEX,STARTTIME,ENDTIME,STATUS,TEMPLTINDEX,TASKTYPE,PLATFORM FROM EPGTEMPLT_SYNC_TASK_his "
					            + " where STATUS =3 and platform = 2 and templtindex = "+ epgtemplt.getTempltindex()
					            + " order by endtime desc";
				List listepg30 = new ArrayList();
				try
				{
					listepg30 = db.getQuery(sqlstr30);
					if(listepg30.size()>0)
					{
						HashMap hashMap30 = (HashMap) listepg30.get(0);
					    int tasktypeint30 = Integer.parseInt((String)hashMap30.get("tasktype"));
					    if(tasktypeint30==1)
					    {					    		   					  					    						    	
					    	
					    	if(epgtemplt.getStandard30() == 2){
					    	    EpgtempltSyncTask sync = new EpgtempltSyncTask();
							    sync.setPriority(2); // 2中
							    sync.setStatus(1); // 修改同步中
							    sync.setPlatform(2); //1-2.0平台，2-3.0平台
							    sync.setTempltindex(epgtemplt.getTempltindex());
							    sync.setTasktype(2); // 2 UPDATE
							    syncds.insertEpgtempltSyncTask(sync);
							    epgtemplt.setStatus(40);
					    	}else
					    	{
					    		if(iftar == 0)
					    		{
							    	//解压缩zip包
									unZip(path1 + newfilename,temptar);	
							    	//打成tar包
									com.zte.ismp.common.util.FileUtil.archive(temptar,path1+epgtemplt.getEpggroup()+".tar");
									//删除解压缩文件
									com.zte.ismp.common.util.FileUtil fu = new com.zte.ismp.common.util.FileUtil();	
									fu.delFolder(path1  +  "temptar");
					    		}
					    		List<Epgtemplt> epgTempltList = new ArrayList<Epgtemplt>();
								epgTempltList.add(epgtemplt);
						        map.put("epgtemplt", epgTempltList);
								String epgxml = "";
								try
								{
									epgxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, 0, 16, 2);
								}catch(Exception e)
								{
									log.error(e);
									return "1:" + "xml指令文件生成异常!"; 
								}					   					  					    						    	
						    	EpgtempltSyncTask sync = new EpgtempltSyncTask();
								sync.setPriority(2); // 2中
								sync.setStatus(1); // 修改同步中
								sync.setPlatform(2); //1-2.0平台，2-3.0平台
								sync.setTempltindex(epgtemplt.getTempltindex());
								sync.setTasktype(1); // 2 UPDATE
								sync.setCmdfileurl(epgxml);
								syncds.insertEpgtempltSyncTask(sync);
								epgtemplt.setStatus(40);					    							    		
					    	}
					    	
					    }
					}					
				}catch(Exception e)
				{
					log.error(e);
					return "1:"+"系统错误!";
				}
				
			}
			
		}
		//if (epgtemplt.getStatus() == 20 || epgtemplt.getStatus() == 50
		//		|| epgtemplt.getStatus() == 60 || epgtemplt.getStatus() == 90) {// 新增同步成功,修改同步成功,修改同步失败,取消同步失败
		//	EpgtempltSyncTask sync = new EpgtempltSyncTask();
		//	sync.setPriority(2); // 2中
		//	sync.setStatus(1); // 修改同步中
		//	sync.setPlatform(2); //1-2.0平台，2-3.0平台
		//	sync.setTempltindex(epgtemplt.getTempltindex());
		//	sync.setTasktype(2); // 2 UPDATE
        //
		//	syncds.insertEpgtempltSyncTask(sync);
		//	epgtemplt.setStatus(40);
		//}
		ds.updateEpgtemplt(epgtemplt);
		log.debug("updatetemplate()");
		// 记录业务日志
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		// 操作对象类型
		String optType = ResourceManager.getResourceText("log.epgtemplate.title"); // 
		String optObject = String.valueOf(epgtemplt.getTempltindex());
		String optObjecttype = ResourceManager
				.getResourceText("log.epgtemplate.mod"); // 

		OperInfo loginUser = LoginMgt
				.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
		// 操作员编码
		String operId = loginUser.getOperid();
		// 操作员名称
		String operName = loginUser.getUserId();

		String optDetail = ResourceManager
				.getResourceText("log.epgtemplatedetail.mod"); // 操作员[opername]修改了[templatename]
		optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
				.filterDollarStr(operName));
		optDetail = optDetail.replaceAll("\\[templatename\\]", String
				.valueOf(epgtemplt.getTempltindex()));

		loginfo.setUserId(operId);
		loginfo.setOptType(optType);
		loginfo.setOptObject(optObject);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptDetail(optDetail);
		loginfo.setOptTime("");
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		LogInfoMgt.doLog(loginfo);
		rtn = ResourceManager
				.getResourceText("label.cms.storageManage.updateoperSuccessful");

		return "0:" + rtn;
	}

	/**
	 * @param templateindex Long
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String removeTemplate(Long templateindex)
			throws DomainServiceException {
		String rtn = "";
        DbUtil db = new DbUtil();
		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltindex(templateindex);
		epgtemplt = ds.getEpgtemplt(epgtemplt);
		// 0,初始，30新增失败，80取消成功
		//if (epgtemplt != null
		//		&& (epgtemplt.getStatus() == 0 || epgtemplt.getStatus() == 30 || epgtemplt
		//				.getStatus() == 80)) {
		
		if (epgtemplt == null) {
			rtn = ResourceManager.getResourceText("msg.epgtemplate.notexist");
			return "1:" + rtn;
		} 
		else
		{		
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and "
				          + "templtindex = " + templateindex;
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:" + "系统错误!"; 
			}
			if(num >= 1)
			{
				return "1:" + "该EPG模板存在'待执行'或'执行中'的任务,不能删除!"; 
			}
		}
		
        String sqlstrdel = " select TASKINDEX,STARTTIME,ENDTIME,STATUS,TEMPLTINDEX,TASKTYPE,PLATFORM FROM EPGTEMPLT_SYNC_TASK_his "
					            + " where STATUS = 3 and templtindex = "+ epgtemplt.getTempltindex()
					            + " order by endtime desc";
        List listepgdel = new ArrayList();
		try
		{
		    listepgdel = db.getQuery(sqlstrdel);
                if(listepgdel.size()>0)
				{
				    HashMap hashMap = (HashMap) listepgdel.get(0);
				    int tasktypeint = Integer.parseInt((String)hashMap.get("tasktype"));
				    if(tasktypeint != 3 )
                    {
                        return "1:" + "该EPG模板已经发布,不能删除!";                                
                    }
                }
        }catch(Exception e)
	    {
		    log.error(e);
            return "1:" + "系统错误!"; 
	    }
		
				
			UpLoadFile.removeFile(GlobalConstants.getMountPoint() + epgtemplt.getAddresspath());
			String tarfile = epgtemplt.getAddresspath().substring(0,epgtemplt.getAddresspath().lastIndexOf("."))+".tar";
			UpLoadFile.removeFile(GlobalConstants.getMountPoint() + tarfile);

			URL addresspath = Thread.currentThread().getContextClassLoader()
					.getResource("");
			String newPath = addresspath.toString();
			int position = newPath.indexOf("WEB-INF/classes"); // 得到48
			String[] splitstring = newPath.split("WEB-INF/classes");
			String newpath = "";
			newpath = splitstring[0] + "CMS/egptemplt/attachment/";
			// linux上是5

			int locate = epgtemplt.getAddresspath().lastIndexOf("/");
			String path = epgtemplt.getAddresspath().substring(locate);
			String changepath = newpath.substring(5) + UpLoadFile.FILESEPARATOR
					+ path;
			UpLoadFile.removeFile(changepath);

			ds.removeEpgtemplt(epgtemplt);

			// 记录业务日志
			AppLogInfoEntity loginfo = new AppLogInfoEntity();

			// 操作对象类型
			String optType = ResourceManager
					.getResourceText("log.epgtemplate.title"); // EPG模板管理
			String optObject = String.valueOf(epgtemplt.getTempltindex());
			String optObjecttype = ResourceManager
					.getResourceText("log.epgtemplate.del"); // EPG模板删除

			OperInfo loginUser = LoginMgt
					.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
			// 操作员编码
			String operId = loginUser.getOperid();
			// 操作员名称
			String operName = loginUser.getUserId();

			String optDetail = ResourceManager
					.getResourceText("log.epgtemplatedetail.del");
			// 操作员[opername]删除了EPG模板[templatename]
			optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
					.filterDollarStr(operName));
			optDetail = optDetail.replaceAll("\\[templatename\\]", String
					.valueOf(epgtemplt.getTempltindex()));

			loginfo.setUserId(operId);
			loginfo.setOptType(optType);
			loginfo.setOptObject(optObject);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptDetail(optDetail);
			loginfo.setOptTime("");
			loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
			LogInfoMgt.doLog(loginfo);
			String rtndelete = ResourceManager
					.getResourceText("label.cms.storageManage.deleteoperSuccessful");

			return "0:" + rtndelete;
		//} else {

		//}
	}

	// 同步
	/**
	 * @param templateindex Long
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String deployTemplate(Long templateindex)
			throws DomainServiceException {
		String rtn = "";

		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltindex(templateindex);
		epgtemplt = ds.getEpgtemplt(epgtemplt);
		if (epgtemplt == null) {
			rtn = ResourceManager.getResourceText("msg.epgtemplate.notexist");
			return "1:" + rtn;
		} 
		else
		{
			DbUtil db = new DbUtil();
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = 2 and "
				          + "templtindex = " + templateindex;
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:" + "系统错误!"; 
			}
			if(num >= 1)
			{
				return "1:" + "该EPG模板已存在'待执行'或'执行中'的任务!"; 
			}
		}
		
		//else if (!(epgtemplt.getStatus() == 0 || epgtemplt.getStatus() == 30 || epgtemplt
		//		.getStatus() == 80)) {
		//	rtn = ResourceManager
		//			.getResourceText("msg.epgtemplate.statuswrong");
		//	return "1:" + rtn;
		//}
		// 插入到同步任务表中
		EpgtempltSyncTask sync = new EpgtempltSyncTask();
		sync.setPriority(2); // 1高 2中 3低
		sync.setStatus(1); // 1待执行 2执行中 3成功结束 4失败结束
		sync.setPlatform(2); //1-2.0平台，2-3.0平台
		sync.setTempltindex(templateindex);
		sync.setTasktype(1); // 1 REGIST，2 UPDATE，3 DELETE
		syncds.insertEpgtempltSyncTask(sync);
		epgtemplt.setStatus(10); // 10 新增同步中
		ds.updateEpgtemplt(epgtemplt);

		// 记录业务日志
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		// 操作对象类型
		String optType = ResourceManager.getResourceText("log.epgtemplate.title"); // EPG模板管理
		String optObject = String.valueOf(epgtemplt.getTempltindex());
		String optObjecttype = ResourceManager
				.getResourceText("log.epgtemplate.deploy"); // EPG模板发布

		OperInfo loginUser = LoginMgt
				.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
		// 操作员编码
		String operId = loginUser.getOperid();
		// 操作员名称
		String operName = loginUser.getUserId();

		String optDetail = ResourceManager
				.getResourceText("log.epgtemplatedetail.deploy");
		// 操作员[opername]删除了EPG模板[templatename]
		optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
				.filterDollarStr(operName));
		optDetail = optDetail.replaceAll("\\[templatename\\]", String
				.valueOf(epgtemplt.getTempltindex()));

		loginfo.setUserId(operId);
		loginfo.setOptType(optType);
		loginfo.setOptObject(optObject);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptDetail(optDetail);
		loginfo.setOptTime("");
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		LogInfoMgt.doLog(loginfo);
		String rtndelete = ResourceManager.getResourceText("msg.info.0000120");

		return "0:" + rtndelete;
	}

	public String deployTemplate20(Long templateindex,int typenum) throws DomainServiceException
	{	
		String rtn = "";
        int plat = 1;
        if(typenum==1)
        {
        	plat=2;
        }
		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltindex(templateindex);
		epgtemplt = ds.getEpgtemplt(epgtemplt);
		if (epgtemplt == null) {
			rtn = ResourceManager.getResourceText("msg.epgtemplate.notexist");
			return "1:" + rtn;
		} 
		else
		{
			DbUtil db = new DbUtil();
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = "+plat+" and "
				          + "templtindex = " + templateindex;
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:" + "系统错误!"; 
			}
			if(num >= 1)
			{
				return "1:" + "该EPG模板已存在'待执行'或'执行中'的任务!"; 
			}
		}
		String url = cmsStorageareaLS.getAddress("5");
		String path = GlobalConstants.getMountPoint() + url + "/";
		String temptar = path  +  "temptar" + File.separator;
		try{
		//解压缩zip包
		unZip(GlobalConstants.getMountPoint()+epgtemplt.getAddresspath(),temptar);	
    	//打成tar包
		com.zte.ismp.common.util.FileUtil.archive(temptar,path+epgtemplt.getEpggroup()+".tar");
		//删除解压缩文件
		com.zte.ismp.common.util.FileUtil fu = new com.zte.ismp.common.util.FileUtil();	
		fu.delFolder(path  +  "temptar");
		}catch(Exception e)
		{
			log.error(e);
			return "1:" + "文件打包异常!"; 
		}			
		//调用方法生成xml指令文件，返回文件路径
		List<Epgtemplt> epgTempltList = new ArrayList<Epgtemplt>();
		epgTempltList.add(epgtemplt);
        map.put("epgtemplt", epgTempltList);
		String epgxml = "";
		try
		{
			if(typenum==1){
			   epgxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, 0, 16, 1);
			}else{
			   epgxml = StandardImpFactory.getInstance().create(1).createXmlFile(map, 0, 16, 1);
			}
		}catch(Exception e)
		{
			log.error(e);
			return "1:" + "xml指令文件生成异常!"; 
		}
		
		// 插入到同步任务表中
		EpgtempltSyncTask sync = new EpgtempltSyncTask();
		sync.setPriority(2); // 1高 2中 3低
		sync.setStatus(1); // 1待执行 2执行中 3成功结束 4失败结束
		sync.setPlatform(plat); //1-2.0平台，2-3.0平台
		sync.setTempltindex(templateindex);
		sync.setTasktype(1); // 1 REGIST，2 UPDATE，3 DELETE
		sync.setCmdfileurl(epgxml);
		syncds.insertEpgtempltSyncTask(sync);
		epgtemplt.setStatus(10); // 10 新增同步中
		ds.updateEpgtemplt(epgtemplt);

		// 记录业务日志
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		// 操作对象类型
		String optType = ResourceManager.getResourceText("log.epgtemplate.title"); // EPG模板管理
		String optObject = String.valueOf(epgtemplt.getTempltindex());
		String optObjecttype = ResourceManager
				.getResourceText("log.epgtemplate.deploy"); // EPG模板发布

		OperInfo loginUser = LoginMgt
				.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
		// 操作员编码
		String operId = loginUser.getOperid();
		// 操作员名称
		String operName = loginUser.getUserId();

		String optDetail = ResourceManager
				.getResourceText("log.epgtemplatedetail.deploy");
		// 操作员[opername]删除了EPG模板[templatename]
		optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
				.filterDollarStr(operName));
		optDetail = optDetail.replaceAll("\\[templatename\\]", String
				.valueOf(epgtemplt.getTempltindex()));

		loginfo.setUserId(operId);
		loginfo.setOptType(optType);
		loginfo.setOptObject(optObject);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptDetail(optDetail);
		loginfo.setOptTime("");
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		LogInfoMgt.doLog(loginfo);
		String rtndelete = ResourceManager.getResourceText("msg.info.0000120");

		return "0:" + rtndelete;
	}
	
	// 取消同步发布
	/**
	 * @param templateindex Long
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String canceldeployTemplate(Long templateindex)
			throws DomainServiceException {
		String rtn = "";

		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltindex(templateindex);
		epgtemplt = ds.getEpgtemplt(epgtemplt);
		if (epgtemplt == null) {
			rtn = ResourceManager.getResourceText("msg.epgtemplate.notexist");
			return "1:" + rtn;
			// 20 50 60 90
		} 
		if(epgtemplt.getSendto20flag() == 1)
		{
			DbUtil db = new DbUtil();
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = 1 and "
				          + "templtindex = " + epgtemplt.getTempltindex();
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:"+"系统错误!";
			}
			if(num == 0)
			{
				String sqlstr20 = " select TASKINDEX,STARTTIME,ENDTIME,STATUS,TEMPLTINDEX,TASKTYPE,PLATFORM FROM EPGTEMPLT_SYNC_TASK_his "
					            + " where STATUS in (3,4) and platform = 1 and templtindex = "+ epgtemplt.getTempltindex()
					            + " order by endtime desc";
				List listepg20 = new ArrayList();
				try
				{
					listepg20 = db.getQuery(sqlstr20);
					if(listepg20.size()>0)
					{
						HashMap hashMap = (HashMap) listepg20.get(0);
					    int statusint = Integer.parseInt((String)hashMap.get("status"));
					    int tasktypeint = Integer.parseInt((String)hashMap.get("tasktype"));
					    if((tasktypeint==1 && statusint==3) || (tasktypeint==3 && statusint==4))
					    {
					    	List<Epgtemplt> epgTempltList = new ArrayList<Epgtemplt>();
							epgTempltList.add(epgtemplt);
					        map.put("epgtemplt", epgTempltList);
							String epgxml = "";
							try
							{
								epgxml = StandardImpFactory.getInstance().create(1).createXmlFile(map, 0, 16, 3);
							}catch(Exception e)
							{
								log.error(e);
								return "1:" + "xml指令文件生成异常!"; 
							}					   					  					    						    	
					    	EpgtempltSyncTask sync = new EpgtempltSyncTask();
							sync.setPriority(2); // 2中
							sync.setStatus(1); // 修改同步中
							sync.setPlatform(1); //1-2.0平台，2-3.0平台
							sync.setTempltindex(epgtemplt.getTempltindex());
							sync.setTasktype(3); 
							sync.setCmdfileurl(epgxml);
							syncds.insertEpgtempltSyncTask(sync);
							epgtemplt.setStatus(70);
					    	
					    }
					}					
				}catch(Exception e)
				{
					log.error(e);
					return "1:"+"系统错误!";
				}
				
			}
			
		}
		if(epgtemplt.getSendto30flag() == 1)
		{
			DbUtil db = new DbUtil();
			int num = 0;
			String sqlstr = "select count(1) from epgtemplt_sync_task where status in (1,2) and platform = 2 and "
				          + "templtindex = " + epgtemplt.getTempltindex();
			try
			{			    
			    num = db.queryForCount(sqlstr);			
			}catch(Exception e)
			{
				log.error(e);
				return "1:"+"系统错误!";
			}
			if(num == 0)
			{
				String sqlstr30 = " select TASKINDEX,STARTTIME,ENDTIME,STATUS,TEMPLTINDEX,TASKTYPE,PLATFORM FROM EPGTEMPLT_SYNC_TASK_his "
					            + " where STATUS in (3,4) and platform = 2 and templtindex = "+ epgtemplt.getTempltindex()
					            + " order by endtime desc";
				List listepg30 = new ArrayList();
				try
				{
					listepg30 = db.getQuery(sqlstr30);
					if(listepg30.size()>0)
					{
						HashMap hashMap30 = (HashMap) listepg30.get(0);
					    int statusint30 = Integer.parseInt((String)hashMap30.get("status"));
					    int tasktypeint30 = Integer.parseInt((String)hashMap30.get("tasktype"));
					    if((tasktypeint30==1 && statusint30==3) || (tasktypeint30==3 && statusint30==4))
					    {					    		   					  					    						    	
					    	EpgtempltSyncTask sync = new EpgtempltSyncTask();
							sync.setPriority(2); // 2中
							sync.setStatus(1); // 修改同步中
							sync.setPlatform(2); //1-2.0平台，2-3.0平台
							sync.setTempltindex(epgtemplt.getTempltindex());
							sync.setTasktype(3); 
							syncds.insertEpgtempltSyncTask(sync);
							epgtemplt.setStatus(70);
					    	
					    }
					}					
				}catch(Exception e)
				{
					log.error(e);
					return "1:"+"系统错误!";
				}
				
			}
			
		}		
		ds.updateEpgtemplt(epgtemplt);

		// 记录业务日志
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		// 操作对象类型
		String optType = ResourceManager.getResourceText("log.epgtemplate.title"); // EPG模板管理
		String optObject = String.valueOf(epgtemplt.getTempltindex());
		String optObjecttype = ResourceManager
				.getResourceText("log.epgtemplate.canceldeploy"); // EPG模板取消发布

		OperInfo loginUser = LoginMgt
				.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
		// 操作员编码
		String operId = loginUser.getOperid();
		// 操作员名称
		String operName = loginUser.getUserId();

		String optDetail = ResourceManager
				.getResourceText("log.epgtemplatedetail.canceldeploy");
		// 操作员[opername]删除了EPG模板[templatename]
		optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
				.filterDollarStr(operName));
		optDetail = optDetail.replaceAll("\\[templatename\\]", String
				.valueOf(epgtemplt.getTempltindex()));

		loginfo.setUserId(operId);
		loginfo.setOptType(optType);
		loginfo.setOptObject(optObject);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptDetail(optDetail);
		loginfo.setOptTime("");
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		LogInfoMgt.doLog(loginfo);
		String rtndelete = ResourceManager.getResourceText("msg.info.0000130");

		return "0:" + rtndelete;
	}

	/**
	 * @param templateindex Long
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String checkExist(Long templateindex) throws DomainServiceException {
		String result = "0";
		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltindex(templateindex);
		List<Epgtemplt> list = ds.getEpgtempltByCond(epgtemplt);
		if (list.size() < 1) {
			result = "1";
		}
		return result;
	}

	/**
	 * 新增是校验tempid重复与否
	 * 
	 * @param templtid String
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String checkRepeat(String templtid) throws DomainServiceException {
		String result = "0";
		Epgtemplt epgtemplt = new Epgtemplt();
		epgtemplt.setTempltid(templtid);
		List<Epgtemplt> list = ds.getEpgtempltByCond(epgtemplt);

		if (list.size() < 1) {
			result = "0";
		} else {
			result = "1";
		}
		return result;
	}

	/**
	 * @param epgtemplt Epgtemplt
	 * @param start int
	 * @param pageSize int
	 * @return TableDataInfo
	 * @throws DomainServiceException DomainServiceException
	 */
	public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start,
			int pageSize) throws DomainServiceException {
		PageUtilEntity puEntity = new PageUtilEntity();
		puEntity.setIsAlwaysSearchSize(true);
		puEntity.setIsAsc(false);
		puEntity.setOrderByColumn("TEMPLTINDEX");
		if (!epgtemplt.getBegintime().equals("")) {
			String starttime1 = epgtemplt.getBegintime().substring(0, 4)
					+ epgtemplt.getBegintime().substring(5, 7)
					+ epgtemplt.getBegintime().substring(8, 10)
					+ epgtemplt.getBegintime().substring(11, 13)
					+ epgtemplt.getBegintime().substring(14, 16)
					+ epgtemplt.getBegintime().substring(17, 19);
			epgtemplt.setBegintime(starttime1);
		}
		if (!epgtemplt.getExpiredtime().equals("")) {
			String expiredtime1 = epgtemplt.getExpiredtime().substring(0, 4)
					+ epgtemplt.getExpiredtime().substring(5, 7)
					+ epgtemplt.getExpiredtime().substring(8, 10)
					+ epgtemplt.getExpiredtime().substring(11, 13)
					+ epgtemplt.getExpiredtime().substring(14, 16)
					+ epgtemplt.getExpiredtime().substring(17, 19);
			epgtemplt.setExpiredtime(expiredtime1);
		}
		return pageInfoQuery(epgtemplt, start, pageSize, puEntity);
	}

	/**
	 * @param epgtemplt Epgtemplt
	 * @param start int
	 * @param pageSize int
	 * @param puEntity PageUtilEntity
	 * @return TableDataInfo
	 * @throws DomainServiceException DomainServiceException
	 */
	public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start,
			int pageSize, PageUtilEntity puEntity)
			throws DomainServiceException {
		return ds.pageInfoQuery(epgtemplt, start, pageSize, puEntity);
	}

	// 每次用户要下载时先从NAS服务器上下载到本地，然后再在页面上打开新窗口让用户下载到任何地方
	/**
	 * @param filename String
	 * @return String
	 * @throws Exception Exception
	 */
	public String downloadFromNAS(String filename) throws Exception {
		try {
			log.debug("in the downloadFromNAS method");
			UpLoadResult result = new UpLoadResult();
			// filepath="F:/study/work/workspaceforidt/cmsnew/form/WEB-INF/classes/attachment/";
			URL addresspath = Thread.currentThread().getContextClassLoader()
					.getResource("");
			log.debug("addresspath         " + addresspath.toString());
			String oldFullPath = filename;
			int locate = filename.lastIndexOf("/");
			filename = filename.substring(locate);
			// String oldFullPath = GlobalConstants.getMountPoint()
			// + cmsStorageareaLS.getAddress("6") + "/" + "template" + "/"
			// + filename;
			log.debug("oldFullPath       " + oldFullPath);
			String newPath = addresspath.toString();
			// int position = newPath.indexOf("WEB-INF/classes");// 得到48
			String[] splitstring = newPath.split("WEB-INF/classes");

			String newpath = splitstring[0] + "CMS/egptemplt/attachment/";

			// file:/F:/study/work/workspaceforidt/cmsnew/form/cms/noticemgt/attachment
			String changepath = newpath.substring(5);
			log.debug("changepath      " + changepath);
			System.out.println("newPath         " + newPath.toString());
			System.out.println("addresspath         " + addresspath.toString());
			System.out.println("cmsStorageareaLS.getAddress         "
					+ cmsStorageareaLS.getAddress("6"));

			System.out.println("changepath    " + changepath);
			System.out.println("oldFullPath       " + oldFullPath);
			System.out.println("filename       " + filename);
			util.copyFileCheckExits(oldFullPath, changepath, filename);

		} catch (Exception e) {
			log.error(e);
			return "1:" + ResourceMgt.findDefaultText("operation.fail");
		}

		return "0:";
	}

	/**
	 * @param templtIndex String
	 * @return String
	 * @throws DomainServiceException DomainServiceException
	 */
	public String templtPublishBatch(String templtIndex)
			throws DomainServiceException {
		// 获取模板index列表
		ReturnInfo rtnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		String[] templtIndexList = templtIndex.split(":");
		int totalCount = templtIndexList.length;
		Integer sequence = 1;
		int failcount = 0;

		for (int i = 0; i < totalCount; i++) {
			String tempindex = templtIndexList[i].toString();
			Epgtemplt egp = new Epgtemplt();
			egp.setTempltindex(new Long(tempindex));
			egp = ds.getEpgtemplt(egp);
			if (egp == null) {
				failcount++;
				operateInfo = new OperateInfo(sequence.toString(), tempindex,
						GlobalConstants.FAIL, "0000030");
				sequence++;
				rtnInfo.appendOperateInfo(operateInfo);
			} else {
				int status = egp.getStatus();
				int source = egp.getSource();
				if (source == 1) {// 文广下发
					failcount++;
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.FAIL, "0000030");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				} else if (status == 0 || status == 30 || status == 80) {// 可以发布
					try {
						deployTemplate(new Long(tempindex));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						failcount++;
						operateInfo = new OperateInfo(sequence.toString(),
								tempindex, GlobalConstants.FAIL, "0000030");
						sequence++;
						rtnInfo.appendOperateInfo(operateInfo);
						e.printStackTrace();
					}
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.SUCCESS, "0000010");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				} else {
					failcount++;
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.FAIL, "0000030");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				}
			}
		}
		if (failcount == 0) {
			rtnInfo.setFlag(GlobalConstants.SUCCESS);
			rtnInfo.setReturnMessage("0000010");
		} else if (failcount < totalCount) {
			rtnInfo.setFlag(GlobalConstants.SUCCESS);
			rtnInfo.setReturnMessage("0000020");
		} else {
			rtnInfo.setFlag(GlobalConstants.FAIL);
			rtnInfo.setReturnMessage("0000030");
		}
		return rtnInfo.toString();
	}

	/**
	 * @param templtIndex String
	 * @return String
	 * @throws Exception Exception
	 */
	public String templtCancelPublishBatch(String templtIndex) throws Exception {
		// 获取模板index列表
		ReturnInfo rtnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		String[] templtIndexList = templtIndex.split(":");
		int totalCount = templtIndexList.length;
		Integer sequence = 1;
		int failcount = 0;
		for (int i = 0; i < totalCount; i++) {
			String tempindex = templtIndexList[i].toString();
			Epgtemplt egp = new Epgtemplt();
			egp.setTempltindex(new Long(tempindex));
			egp = ds.getEpgtemplt(egp);
			if (egp == null) {
				failcount++;
				operateInfo = new OperateInfo(sequence.toString(), tempindex,
						GlobalConstants.FAIL, "0000030");
				sequence++;
				rtnInfo.appendOperateInfo(operateInfo);
			} else {
				int status = egp.getStatus();
				int source = egp.getSource();
				if (source == 1) {// 文广下发
					failcount++;
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.FAIL, "0000030");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				} else if (status == 20 || status == 50 || status == 60
						|| status == 90) {// 可以发布

					try {
						canceldeployTemplate(new Long(tempindex));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						failcount++;
						operateInfo = new OperateInfo(sequence.toString(),
								tempindex, GlobalConstants.FAIL, "0000030");
						sequence++;
						rtnInfo.appendOperateInfo(operateInfo);
						e.printStackTrace();
					}
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.SUCCESS, "0000010");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				} else {
					failcount++;
					operateInfo = new OperateInfo(sequence.toString(),
							tempindex, GlobalConstants.FAIL, "0000030");
					sequence++;
					rtnInfo.appendOperateInfo(operateInfo);
				}
			}
		}
		if (failcount == 0) {
			rtnInfo.setFlag(GlobalConstants.SUCCESS);
			rtnInfo.setReturnMessage("0000010");
		} else if (failcount < totalCount) {
			rtnInfo.setFlag(GlobalConstants.SUCCESS);
			rtnInfo.setReturnMessage("0000020");
		} else {
			rtnInfo.setFlag(GlobalConstants.FAIL);
			rtnInfo.setReturnMessage("0000030");
		}
		return rtnInfo.toString();
	}
	public TableDataInfo pageInfoQueryforTask(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize)
    throws DomainServiceException
    {
		return syncds.pageInfoQuery(epgtempltSyncTask, start, pageSize);
    }

    /**
     * 将压缩文件解压到指定文件夹下.
     * 
     * @param unZipfileName 解压文件名(全路径)
     * @return 0 解压成功，非0 解压失败
     * @throws IOException Signals that an I/O exception has occurred.
     */
	ZipFile zipFile;
    public String unZip(String unZipfileName,String unzipDir) throws IOException
    {
        FileOutputStream fileOut = null;
        File file;
        InputStream inputStream = null;
        ZipFile zipFile;
        zipFile = new ZipFile(unZipfileName, "GBK"); 
        try
        {
            for (Enumeration entries = zipFile.getEntries(); entries.hasMoreElements();)
            {
                ZipEntry entry = (ZipEntry) entries.nextElement();

                file = new File(unzipDir + entry.getName());

                if (entry.isDirectory())
                {
                    file.mkdirs();
                }
                else
                {
                    // 如果指定文件的目录不存在,则创建之.
                    File parent = file.getParentFile();
                    if (!parent.exists())
                    {
                        parent.mkdirs();
                    }
                    inputStream = zipFile.getInputStream(entry);
                    byte[] buf = new byte[2*1024];
                    int len;
                    fileOut = new FileOutputStream(file);
                    while ((len = inputStream.read(buf)) > 0)
                    {
                        fileOut.write(buf, 0, len);
                    }
                    fileOut.close();
                    inputStream.close();
                }
            }
            zipFile.close();
        }
        catch (IOException e)
        {
            if (fileOut != null)
            {
                fileOut.close();
            }
            if (inputStream != null)
            {
                inputStream.close();
            }
            log.error(e);
            return "1";
        }
        finally
        {
            try
            {
                zipFile.close();
            }
            catch (IOException e)
            {
                throw e;
            }
        }
        return "0";
    }
	
    public String repeatdeploy(String taskindex) throws Exception
    {
    	EpgtempltSyncTask sync = new EpgtempltSyncTask();
    	sync.setTaskindex(Long.parseLong(taskindex));
    	sync = syncds.getEpgtempltSyncTask(sync);
		if (sync == null) {
			return "1: 该条任务不存在!";
		} 
		//重新插入待执行任务
		sync.setTaskindex(null);
		sync.setStarttime(""); 
		sync.setEndtime("");
		sync.setStatus(1);
		sync.setResultcode(null);
		sync.setResultdesc("");
		sync.setResultfileurl(""); 
		syncds.insertEpgtempltSyncTask(sync);
		//删除原任务
		DbUtil db = new DbUtil();
		String sqlstr = "delete from epgtemplt_sync_task_his where taskindex = "+ taskindex;
		try
		{			    
		    db.excute(sqlstr);			
		}catch(Exception e)
		{
			log.error(e);
			return "1:"+"系统错误!";
		}
		
    	return "0: 重新发布成功!";
    }

    public TableDataInfo getUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize) throws Exception
    {
     // TODO Auto-generated method stub
        log.debug("query category from content information given starting...");
     // 进行模板生效时间格式的转换//
        String startBeginTime = upstreamEPGTask.getBegintime1();
        startBeginTime = startBeginTime.trim().replace("-", ".");
        upstreamEPGTask.setBegintime1(startBeginTime);
        String endBeginTime = upstreamEPGTask.getBegintime2();
        endBeginTime = endBeginTime.trim().replace("-", ".");
        upstreamEPGTask.setBegintime2(endBeginTime);

        upstreamEPGTask.setTaskid(EspecialCharMgt.conversion(upstreamEPGTask.getTaskid()));
        upstreamEPGTask.setEpggroup(EspecialCharMgt.conversion(upstreamEPGTask.getEpggroup()));
        upstreamEPGTask.setFileid(EspecialCharMgt.conversion(upstreamEPGTask.getFileid())); 
        
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("operatetime");
        try
        {
            tableInfo = getUpstreamEPGTask(upstreamEPGTask, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query category from content information given ends...");

        return tableInfo;
    }

    public TableDataInfo getUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize,
            PageUtilEntity puEntity) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("query category from content information given starting...");
        TableDataInfo tableInfo = null;
        try
        {//本处根据不同的type查询不同的关联关系。
            tableInfo = syncds.pageInfoQueryUpstreamEPGTask(upstreamEPGTask, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            throw new DomainServiceException(dsEx);
        }
        log.debug("query category from content information given ends...");
        return tableInfo;
    }

    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail(UpstreamEPGTaskDetail rsObj ) throws Exception
    {
        log.debug("get UpstreamEPGTask by pk starting...");

        try
        {
            rsObj = syncds.getUpstreamEPGTaskDetail(rsObj);
        }
        catch (Exception e)
        {
            log.error("get UpstreamEPGTask exception:", e);
            throw e;
        }
        log.debug("get UpstreamEPGTask by pk end");
        return rsObj;
    }
}

package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.cms.service.servicecntbindmap.dao.IServiceSeriesMapDAO;
import com.zte.cms.service.servicecntbindmap.service.IServiceSeriesMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceSeriesMapDS extends DynamicObjectBaseDS implements IServiceSeriesMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceSeriesMapDAO dao = null;

    public void setDao(IServiceSeriesMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException
    {
        log.debug("insert serviceSeriesMap starting...");
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_service_series_map_index");
        serviceSeriesMap.setMapindex(index);
        //mappingid =mapping对象编码（两位）+mapping对象index（30位，不足30前面补零）
        String id = index.toString();
        for (int i = id.length();i<30;i++){
            id = "0"+id;
        }
        id = "22"+id;
        serviceSeriesMap.setMappingid(id);
        try
        {
            dao.insertServiceSeriesMap(serviceSeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceSeriesMap end");
    }

    public void updateServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException
    {
        log.debug("update serviceSeriesMap by pk starting...");
        try
        {
            dao.updateServiceSeriesMap(serviceSeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceSeriesMap by pk end");
    }

    public void updateServiceSeriesMapList(List<ServiceSeriesMap> serviceSeriesMapList) throws DomainServiceException
    {
        log.debug("update serviceSeriesMapList by pk starting...");
        if (null == serviceSeriesMapList || serviceSeriesMapList.size() == 0)
        {
            log.debug("there is no datas in serviceSeriesMapList");
            return;
        }
        try
        {
            for (ServiceSeriesMap serviceSeriesMap : serviceSeriesMapList)
            {
                dao.updateServiceSeriesMap(serviceSeriesMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceSeriesMapList by pk end");
    }

    public void removeServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException
    {
        log.debug("remove serviceSeriesMap by pk starting...");
        try
        {
            dao.deleteServiceSeriesMap(serviceSeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceSeriesMap by pk end");
    }

    public void removeServiceSeriesMapList(List<ServiceSeriesMap> serviceSeriesMapList) throws DomainServiceException
    {
        log.debug("remove serviceSeriesMapList by pk starting...");
        if (null == serviceSeriesMapList || serviceSeriesMapList.size() == 0)
        {
            log.debug("there is no datas in serviceSeriesMapList");
            return;
        }
        try
        {
            for (ServiceSeriesMap serviceSeriesMap : serviceSeriesMapList)
            {
                dao.deleteServiceSeriesMap(serviceSeriesMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceSeriesMapList by pk end");
    }

    public ServiceSeriesMap getServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DomainServiceException
    {
        log.debug("get serviceSeriesMap by pk starting...");
        ServiceSeriesMap rsObj = null;
        try
        {
            rsObj = dao.getServiceSeriesMap(serviceSeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceSeriesMapList by pk end");
        return rsObj;
    }

    public List<ServiceSeriesMap> getServiceSeriesMapByCond(ServiceSeriesMap serviceSeriesMap)
            throws DomainServiceException
    {
        log.debug("get serviceSeriesMap by condition starting...");
        List<ServiceSeriesMap> rsList = null;
        try
        {
            rsList = dao.getServiceSeriesMapByCond(serviceSeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceSeriesMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceSeriesMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceSeriesMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceSeriesMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceSeriesMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceSeriesMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceSeriesMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceSeriesMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceSeriesMap page info by condition end");
        return tableInfo;
    }
}

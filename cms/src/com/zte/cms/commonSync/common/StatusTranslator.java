package com.zte.cms.commonSync.common;

import java.util.ArrayList;
import java.util.List;

public class StatusTranslator
{
    /*发布状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     *同步结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 
     *         50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    public static final int STATUS_TOPUBLISH = 0;
    public static final int STATUS_PUBLISHING = 200;
    public static final int STATUS_PUBLISHED = 300;
    public static final int STATUS_PUBFAILED = 400;
    public static final int STATUS_MODIFYPUBFAILED = 500;
    public static final int STATUS_TOOFFLINE = 600;
    
    public static final int OPER_STATUS_TOPUBLISH = 0;
    public static final int OPER_STATUS_ADDPUBLISHING = 10;
    public static final int OPER_STATUS_ADDPUBLISHED = 20;
    public static final int OPER_STATUS_ADDPUBFAILED = 30;
    public static final int OPER_STATUS_MODIFYPUBLISHING = 40;
    public static final int OPER_STATUS_MODIFYPUBLISHED = 50;
    public static final int OPER_STATUS_MODIFYPUBFAILED = 60;
    public static final int OPER_STATUS_DELPUBLISHING = 70;
    public static final int OPER_STATUS_DELPUBLISHED = 80;
    public static final int OPER_STATUS_DELPUBFAILED = 90;
    
    public static int getTargetSyncStatus(int operresult)
     {
         int targetSyncStatus =0;
         switch (operresult)
         {
             case OPER_STATUS_TOPUBLISH:
             case OPER_STATUS_DELPUBLISHED:
                 targetSyncStatus = STATUS_TOPUBLISH;
                 break;
             case OPER_STATUS_ADDPUBLISHING:
             case OPER_STATUS_MODIFYPUBLISHING:
             case OPER_STATUS_DELPUBLISHING:
                 targetSyncStatus = STATUS_PUBLISHING;
                 break;
             case OPER_STATUS_ADDPUBLISHED:
             case OPER_STATUS_MODIFYPUBLISHED:
             case OPER_STATUS_DELPUBFAILED:
                 targetSyncStatus = STATUS_PUBLISHED;
                 break;
             case OPER_STATUS_ADDPUBFAILED:
                 targetSyncStatus = STATUS_PUBFAILED;
                 break;
             case OPER_STATUS_MODIFYPUBFAILED:
                 targetSyncStatus = STATUS_MODIFYPUBFAILED;
                 break;
             default:
                 targetSyncStatus = STATUS_TOPUBLISH;
             
             
             
         }
         return targetSyncStatus;
     }
     
     public static int getPlatformSyncStatus(int[] targetSyncStatus)
     {
         int[][] priority = {{1,STATUS_TOOFFLINE},{2,STATUS_TOPUBLISH},{3,STATUS_PUBFAILED},{4,STATUS_MODIFYPUBFAILED},{5,STATUS_PUBLISHING},{6,STATUS_PUBLISHED}};
         int currentLevel = 10;
       
         for (int i=0; i<targetSyncStatus.length; i++)
         {
             switch (targetSyncStatus[i])
             {
                 case 0:
                     if (currentLevel > 2)
                     {
                         currentLevel = 2;
                     }
                     break;
                 case 200:
                     if (currentLevel > 5)
                     {
                         currentLevel = 5;
                     }
                     break;
                 case 300:
                     if (currentLevel > 6)
                     {
                         currentLevel = 6;
                     }
                     break;   
                 case 400:
                     if (currentLevel > 3)
                     {
                         currentLevel = 3;
                     }
                     break;
                 case 500:
                     if (currentLevel > 4)
                     {
                         currentLevel = 4;
                     }
                     break;
                 case 600:
                     if (currentLevel > 1)
                     {
                         currentLevel = 1;
                     }
                 default:
                     currentLevel = -1;
                     
             }
         }
         
         return priority[currentLevel-1][1];
     }
     
     public static void main(String[] args)
     {
         System.out.println("getTargetSyncStatus(80):" + getTargetSyncStatus(80));
         System.out.println("getTargetSyncStatus(112):" + getTargetSyncStatus(112));
         int[] test = {200,300};
         System.out.println("getPlatformSyncStatus({200,300}):" + getPlatformSyncStatus(test));
         System.out.println("getPlatformSyncStatus({200,100}):" + getPlatformSyncStatus(test));
     }
}

package com.zte.cms.content.modwkfw;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.cglib.beans.BeanCopier;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;


import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.UpLoadFile;
import com.zte.cms.common.UpLoadResult;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.cms.content.program.programsync.ls.IProgramPlatformSynLS;
import com.zte.cms.content.program.service.ICmsProgramBufDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntException;
import com.zte.cms.content.util.ModApplyPool;
import com.zte.cms.content.util.ModAuditPool;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.jwf.workflowEngine.common.util.BpsException;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CntModApply extends WorkflowBase implements ICntModApply
{
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramBufDS cmsProgramBufDS = null;
    private ICmsMovieBufDS cmsMovieBufDS = null;
    private ICmsProgramDS cmsProgramDS = null;
    private ICmsMovieDS cmsMovieDS;
    private ICmsStorageareaLS cmsStorageareaLS = null;
    private ICntModWkfwCheck cntModWkfwCheck = null;
    private IPrimaryKeyGenerator primaryKeyGenerator = null;
    private ITargetsystemDS targetSystemDS = null;
    private IUsysConfigDS usysConfigDS = null;
    private ICntSyncTaskDS cntsyncTaskDS = null;
    private IBatchObjectRecordDS batchObjectDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IProgramPlatformSynLS programPlatformSynLS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private String callbackProcess;

    // private Vector<String> modApplyList = new Vector<String>(); //
    // 存放已发起工作流的内容ID列表，防止不同操作员对同一内容发起工作流
    // private Vector<String> modAuditList = new Vector<String>(); //
    // 存放正在进行审核操作的内容ID
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public String runModApply(CmsProgram cmsProgram) throws Exception
    {// 修改内容触发工作流
        try
        {
            // if
            // (modApplyList.contains(String.valueOf(cmsProgram.getProgramindex())))
            if (ModApplyPool.getInstance().contains(String.valueOf(cmsProgram.getProgramindex())))
            {// 内容已被提交修改申请
             // 250201
                return CntConstants.CNT_MODWKFW_ERROR; // "250201";
            }

            // modApplyList.add(String.valueOf(cmsProgram.getProgramindex()));

            // 查询修改前的内容信息
            CmsProgram oldProgram = new CmsProgram();
            oldProgram.setProgramindex(cmsProgram.getProgramindex());
            oldProgram = cmsProgramDS.getCmsProgram(cmsProgram);

            // 校验当前内容是否满足提交修改申请的条件
            String errorCode = cntModWkfwCheck.checkCntModwkfwApply(oldProgram); // "250000";
                                                                                 // //校验通过
            if (!errorCode.equals(CntConstants.CNT_SUCCESS))
            {// 校验未通过
                // modApplyList.remove(String.valueOf(cmsProgram.getProgramindex()));
                ModApplyPool.getInstance().remove(String.valueOf(cmsProgram.getProgramindex()));
                return errorCode;
            }

            // 更新正式表的信息
            CmsProgram cnt = new CmsProgram();
            cnt.setProgramindex(cmsProgram.getProgramindex());
            cnt.setWorkflow(2); // 2:修改
            cmsProgramDS.updateCmsProgram(cnt);

            // 复制修改前的内容信息到内容buf表
            CmsProgramBuf oldProgramBuf = new CmsProgramBuf();
            BeanCopier oldProgramBufCopier = BeanCopier.create(CmsProgram.class, CmsProgramBuf.class, true);
            oldProgramBufCopier.copy(oldProgram, oldProgramBuf, new BeanPropertyConvert());
            // 将修改前的内容信息插入buf表
            cmsProgramBufDS.insertCmsProgramBufNoPK(oldProgramBuf);
            // 将修改后的内容信息插入buf表
            CmsProgramBuf newProgramBuf = new CmsProgramBuf();
            BeanCopier newProgramCopier = BeanCopier.create(CmsProgram.class, CmsProgramBuf.class, false);
            // newProgramCopier.copy(cmsProgram, newProgramBuf, new
            // BeanPropertyConvert());
            newProgramCopier.copy(cmsProgram, newProgramBuf, null);
            cmsProgramBufDS.updateCmsProgramBuf(newProgramBuf);

            // 复制修改前的子内容信息到子内容buf表，内容修改触发，子内容未做更改，只需复制修改前的子内容信息到其buf表
            List<CmsMovie> cmsMovieList = null;
            CmsMovie movie = new CmsMovie();
            movie.setProgramindex(cmsProgram.getProgramindex());
            cmsMovieList = cmsMovieDS.getCmsMovieByCond(movie);

            if (null != cmsMovieList && cmsMovieList.size() > 0)
            {
                CmsMovie cmsMovie = new CmsMovie();
                for (int i = 0; i < cmsMovieList.size(); i++)
                {
                    cmsMovie = cmsMovieList.get(i);
                    CmsMovieBuf oldMovieBuf = new CmsMovieBuf();
                    // 复制修改前的子内容信息到子内容buf表中
                    BeanCopier oldMovieCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, true);
                    oldMovieCopier.copy(cmsMovie, oldMovieBuf, new BeanPropertyConvert());
                    cmsMovieBufDS.insertCmsMovieBufNoPK(oldMovieBuf);
                }
            }

            // 工作流对象名称
            String pboName = cmsProgram.getNamecn() + "[" + cmsProgram.getCpid() + "]";
            // 工作流对象ID
            String pboId = String.valueOf(cmsProgram.getProgramindex());
            // 工作流对象类型
            String pboType = CntConstants.CNT_PBOTYPE_UPDATE;
            // 工作流进程名称
            String processName = pboType + ":" + pboName;
            // 工作流参数变量
            StringBuffer wkfwParams = new StringBuffer();
            wkfwParams.append("callbackProcess,");
            wkfwParams.append(this.getCallbackProcess());
            String route = cmsProgramDS.getModRoute(cmsProgram);
            wkfwParams.append(route); // 拼接工作流路由信息
            // 启动工作流
            WorkflowService wkflService = new WorkflowService();

            int result = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType,
                    "", wkfwParams.toString(), null);
            if (result < 0)
            {
                throw new BpsException(result + "", "Error occurred in startProcessInstance method");
            }
        }
        catch (Exception e)
        {
            // modApplyList.remove(String.valueOf(cmsProgram.getProgramindex()));
            ModApplyPool.getInstance().remove(String.valueOf(cmsProgram.getProgramindex()));

            if (e instanceof BpsException)
            {
                BpsException bpsEx = (BpsException) e;
                log.error("CntModApply: BpsException occurred in runModApply, errorCode=" + bpsEx.getCode());
                // "250203" 内容提交修改申请流程执行失败
                throw new Exception(CntConstants.CNT_MODAPPLYWKFW_EXECERROR);
            }
            else
            {
                log.error("CntModApply: Exception occurred in runModApply", e);
                throw new Exception(CntConstants.CNT_FAIL);
            }
        }

        // modApplyList.remove(String.valueOf(cmsProgram.getProgramindex()));
        ModApplyPool.getInstance().remove(String.valueOf(cmsProgram.getProgramindex()));

        return CntConstants.CNT_SUCCESS;
    }

    public String runModApply(CmsMovie cmsMovie, String isUpload) throws Exception
    {
        try
        {
            // if
            // (modApplyList.contains(String.valueOf(cmsMovie.getProgramindex())))
            if (ModApplyPool.getInstance().contains(String.valueOf(cmsMovie.getProgramindex())))
            {// 内容已被提交修改申请:
             // 250201
                return CntConstants.CNT_MODWKFW_ERROR;
            }

            // modApplyList.add(String.valueOf(cmsMovie.getProgramindex()));

            // 上传视频文件
            String uploadResult = uploadFile(cmsMovie, 0, isUpload);
            if (!CntConstants.CNT_SUCCESS.equals(uploadResult))
            {// 文件上传失败
                // modApplyList.remove(String.valueOf(cmsMovie.getProgramindex()));
                ModApplyPool.getInstance().remove(String.valueOf(cmsMovie.getProgramindex()));
                return uploadResult;
            }

            // 查询子内容对应的内容信息
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(cmsMovie.getProgramindex());
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);

            // 校验内容是否符合发起修改工作流的条件
            String errorCode = cntModWkfwCheck.checkCntModwkfwApply(cmsProgram); // 校验通过
            if (!errorCode.equals(CntConstants.CNT_SUCCESS))
            {// 校验未通过
                // modApplyList.remove(String.valueOf(cmsMovie.getProgramindex()));
                ModApplyPool.getInstance().remove(String.valueOf(cmsMovie.getProgramindex()));
                return errorCode;
            }

            // 复制修改前的内容信息到内容buf表，由于子内容修改触发的修改，只需复制修改前的内容到buf表
            CmsProgramBuf oldProgramBuf = new CmsProgramBuf();
            BeanCopier oldProgramBufCopier = BeanCopier.create(CmsProgram.class, CmsProgramBuf.class, true);
            oldProgramBufCopier.copy(cmsProgram, oldProgramBuf, new BeanPropertyConvert());
            // 将修改前的内容信息插入buf表
            cmsProgramBufDS.insertCmsProgramBufNoPK(oldProgramBuf);

            if (cmsMovie.getMovieindex() != null)
            {// 修改
                // 查询修改前的子内容信息
                CmsMovie oldMovie = new CmsMovie();
                oldMovie.setMovieindex(cmsMovie.getMovieindex());
                oldMovie = cmsMovieDS.getCmsMovie(cmsMovie);
                CmsMovieBuf oldMovieBuf = new CmsMovieBuf();
                // 复制修改前的子内容信息到子内容buf表中
                BeanCopier oldMovieCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, true);
                oldMovieCopier.copy(oldMovie, oldMovieBuf, new BeanPropertyConvert());
                cmsMovieBufDS.insertCmsMovieBufNoPK(oldMovieBuf);

                // 复制修改后的子内容信息到子内容buf表中
                CmsMovieBuf newMovieBuf = new CmsMovieBuf();
                BeanCopier newMovieCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, false);
                newMovieCopier.copy(cmsMovie, newMovieBuf, null);
                // 更新子内容缓存表
                cmsMovieBufDS.updateCmsMovieBuf(newMovieBuf);

                // 复制其它未修改的子内容入buf表
                List<CmsMovie> cmsMovieList = null;
                CmsMovie movieCond = new CmsMovie();
                movieCond.setProgramindex(cmsMovie.getProgramindex());
                cmsMovieList = cmsMovieDS.getCmsMovieByCond(movieCond);

                if (null != cmsMovieList && cmsMovieList.size() > 0)
                {
                    for (CmsMovie movie : cmsMovieList)
                    {
                        if (movie.getMovieindex().longValue() != cmsMovie.getMovieindex().longValue())
                        {
                            CmsMovieBuf oldBuf = new CmsMovieBuf();
                            BeanCopier oldCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, true);
                            oldCopier.copy(movie, oldBuf, new BeanPropertyConvert());
                            cmsMovieBufDS.insertCmsMovieBufNoPK(oldBuf);
                        }
                    }
                }
            }
            else
            {// 新增的子内容
                // 复制新增的子内容信息到子内容buf表中
                CmsMovieBuf cmsMovieBuf = new CmsMovieBuf();
                BeanCopier newMovieCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, true);
                newMovieCopier.copy(cmsMovie, cmsMovieBuf, null);
                // 更新子内容缓存表，需要主键
                cmsMovieBufDS.insertCmsMovieBuf(cmsMovieBuf);

                // 复制除了修改前已存在的子内容入buf表
                List<CmsMovie> cmsMovieList = null;
                CmsMovie movieCond = new CmsMovie();
                movieCond.setProgramindex(cmsMovie.getProgramindex());
                cmsMovieList = cmsMovieDS.getCmsMovieByCond(movieCond);
                if (null != cmsMovieList && cmsMovieList.size() > 0)
                {
                    for (int i = 0; i < cmsMovieList.size(); i++)
                    {// 复制其它子内容入buf表
                        for (CmsMovie movie : cmsMovieList)
                        {
                            CmsMovieBuf oldBuf = new CmsMovieBuf();
                            BeanCopier oldCopier = BeanCopier.create(CmsMovie.class, CmsMovieBuf.class, true);
                            oldCopier.copy(movie, oldBuf, new BeanPropertyConvert());
                            cmsMovieBufDS.insertCmsMovieBufNoPK(oldBuf);
                        }
                    }
                }
            }

            CmsProgram cnt = new CmsProgram();
            cnt.setProgramindex(cmsProgram.getProgramindex());
            cnt.setWorkflow(2); // 2:修改
            cmsProgramDS.updateCmsProgram(cnt);

            // 工作流处理
            // 工作流对象名称
            String pboName = cmsProgram.getNamecn() + "[" + cmsProgram.getCpid() + "]";

            // 工作流对象ID
            String pboId = String.valueOf(cmsProgram.getProgramindex());
            // 工作流对象类型
            String pboType = CntConstants.CNT_PBOTYPE_UPDATE;
            // 工作流进程名称
            String processName = pboType + ":" + pboName;
            // 工作流参数变量
            StringBuffer wkfwParams = new StringBuffer();
            wkfwParams.append("callbackProcess,");
            wkfwParams.append(this.getCallbackProcess());
            String route = cmsProgramDS.getModRoute(cmsProgram);
            wkfwParams.append(route); // 拼接工作流路由信息
            // 启动工作流
            WorkflowService wkflService = new WorkflowService();

            int result = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType,
                    "", wkfwParams.toString(), null);
            if (result < 0)
            {
                throw new BpsException(result + "", "Error occurred in startProcessInstance method");
            }
        }
        catch (Exception e)
        {
            // modApplyList.remove(String.valueOf(cmsMovie.getProgramindex()));
            ModApplyPool.getInstance().remove(String.valueOf(cmsMovie.getProgramindex()));

            if (e instanceof BpsException)
            {
                BpsException bpsEx = (BpsException) e;
                log.error("CntModApply: BpsException occurred in runModApply, errorCode=" + bpsEx.getCode());
                // "250203" 内容提交修改申请流程执行失败
                throw new Exception(CntConstants.CNT_MODAPPLYWKFW_EXECERROR);
            }
            else
            {
                log.error("CntModApply: Exception occurred in runModApply", e);
                throw new Exception(CntConstants.CNT_FAIL);
            }
        }

        // modApplyList.remove(String.valueOf(cmsMovie.getProgramindex()));
        ModApplyPool.getInstance().remove(String.valueOf(cmsMovie.getProgramindex()));
        return CntConstants.CNT_SUCCESS;
    }

    // 审核具体处理流程 返回操作结果码 工作流参数：内容主键、工单任务号、工单流程号、工单节点名称、审核意见、审核结果
    public String runModAudit(String[] wkfwParam) throws Exception
    {
        log.debug("CntModApply: runModAudit begin, programindex=" + wkfwParam[0] + ", taskid=" + wkfwParam[1]
                + ", processid=" + wkfwParam[2] + ", nodeName=" + wkfwParam[3] + ", handleMsg=" + wkfwParam[4]
                + ", handleEvent=" + wkfwParam[5]);

        String programindex = wkfwParam[0];
        String taskid = wkfwParam[1];
        String processid = wkfwParam[2];
        String nodeName = wkfwParam[3]; // 工单节点名称
        String handleMsg = wkfwParam[4]; // 审核意见
        String handleEvent = wkfwParam[5]; // 审核结果

        try
        {
            // if (modAuditList.contains(programindex))
            if (ModAuditPool.getInstance().contains(programindex))
            {// 内容正在被其它操作员审核
                return CntConstants.CNT_BEING_AUDITED;
            }
            // modAuditList.add(programindex);

            // 据contentid获取当前内容的相关信息
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(Long.parseLong(programindex));
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);

            // 判断当前内容是否满足审核条件
            String rtnCode = cntModWkfwCheck.checkCntModwkfwAudit(cmsProgram, processid, nodeName);
            if (!CntConstants.CNT_SUCCESS.equals(rtnCode))
            {// 不满足审核条件
                // modAuditList.remove(programindex);
                ModAuditPool.getInstance().remove(programindex);
                return rtnCode;
            }

            // 启动工作流
            // 工作流处理
            WorkflowService wkflService = new WorkflowService();
            wkflService.completeTask(taskid, handleMsg, handleEvent);
            
                if (
                // 三审，通过
                ResourceMgt.findDefaultText("cnt.third.audit").equals(nodeName)
                        && ResourceMgt.findDefaultText("cnt.pass").equals(handleEvent))
                {// 如果原内容状态是新增同步成功20 修改同步成功50
                 // 修改同步失败60，则需要将内容进行同步操作
                    CmsProgram newCnt = new CmsProgram();
                    newCnt.setProgramindex(Long.parseLong(programindex));
                    newCnt = cmsProgramDS.getCmsProgram(newCnt);

                    if (null != newCnt)
                    {

                        CntTargetSync sync = new CntTargetSync();
                        sync.setObjecttype(3);
                        sync.setObjectid(newCnt.getProgramid());
                        List list = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                        boolean modifyFlag = false;
                        if (list != null && list.size() > 0)
                        {
                            for (int i = 0; i < list.size(); i++)
                            {
                                sync = (CntTargetSync) list.get(i);
                                if (sync.getStatus() == 300 || sync.getStatus() == 500)
                                {
                                    modifyFlag = true;
                                }
                            }
                            if (modifyFlag)
                            {
                                programPlatformSynLS.modifyProgramPlatformSyn(newCnt.getProgramindex().toString());
                            }
                        }
                        // int status = newCnt.getStatus().intValue();
                        // if (20 == status || 50 == status || 60 == status) {
                        // String rtnMsg = insertTask(programindex);
                        // if (!CntConstants.CNT_SUCCESS.equals(rtnMsg)) {// 内容同步失败
                        // // throw new Exception(rtnMsg);
                        // throw new CntException(rtnMsg, CntUtils
                        // .getResourceText(rtnMsg));
                        // } else {// 内容同步成功，更新内容状态为 40：修改同步中
                        // newCnt.setStatus(40);
                        // cmsProgramDS.updateCmsProgram(newCnt);
                        // // 更新内容下所有的子内容为 40修改同步中
                        // List<CmsMovie> cmsMovieList = null;
                        // CmsMovie movie = new CmsMovie();
                        // movie.setProgramindex(newCnt.getProgramindex());
                        // cmsMovieList = cmsMovieDS
                        // .getCmsMovieByCond(movie);
                        // if (null != cmsMovieList
                        // && cmsMovieList.size() > 0) {
                        // for (int i = 0; i < cmsMovieList.size(); i++) {
                        // cmsMovieList.get(i).setStatus(
                        // newCnt.getStatus());
                        // }
                        // cmsMovieDS.updateCmsMovieList(cmsMovieList);
                        // }
                        // }
                        // }
                    }
                }
            
        }
        catch (Exception e)
        {
            log.error("Error occurred while auditing content", e);

            // modAuditList.remove(programindex);
            ModAuditPool.getInstance().remove(programindex);

            if (e instanceof BpsException)
            {
                BpsException bpsEx = (BpsException) e;
                log.debug("CntModApply: BpsException occurred in runModAudit, errorCode=" + bpsEx.getCode());
                // 内容审核流程执行失败 250011
                throw new Exception(CntConstants.CNT_AUDITWKFW_EXECERROR);
            }
            else if (e instanceof CntException)
            {
                CntException cntEx = (CntException) e;
                log.debug("CntModApply: CntException occurred in runModAudit, errorCode=" + cntEx.getCode()
                        + ", meaning=" + cntEx.getMessage());

                throw new Exception(cntEx.getCode());
            }
            else
            {
                throw new Exception(CntConstants.CNT_FAIL);
            }
        }

        // modAuditList.remove(programindex);
        ModAuditPool.getInstance().remove(programindex);

        log.debug("CntModApply: runModAudit end, programindex=" + wkfwParam[0] + ", taskid=" + wkfwParam[1]
                + ", processid=" + wkfwParam[2] + ", nodeName=" + wkfwParam[3] + ", handleMsg=" + wkfwParam[4]
                + ", handleEvent=" + wkfwParam[5]);
        return CntConstants.CNT_SUCCESS;
    }

    private String uploadFile(CmsMovie cmsMovie, int uploadtype, String updateUrl) throws Exception
    {
        if ("1".equals(updateUrl) && null != cmsMovie.getFilename() && !("").equals(cmsMovie.getFilename().trim()))
        {
            String currenttime = null;
            // 当前时间
            String desPath = cmsStorageareaLS.getAddress("2"); // 在线区地址
            if (null == desPath || CntConstants.GET_STORAGEAREA_FAIL.equals(desPath))
            {// 获取在线区地址失败:
             // 250212
                return CntConstants.ONLINEAREA_NOTFOUND;
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return CntConstants.DEST_NOTFOUND;
                }
                else
                {
                    Date date = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    currenttime = dateFormat.format(date);

                    if (uploadtype == 0)// http上传方式
                    {
                        // 判断目标文件夹是否存在，若不存在则创建
                        StringBuffer destfile = new StringBuffer("");
                        destfile.append(GlobalConstants.getMountPoint());
                        destfile.append(desPath);
                        destfile.append(File.separator);
                        destfile.append(currenttime);
                        destfile.append(File.separator);
                        File destDir = new File(destfile.toString());
                        if (!destDir.exists())
                        {// 目标文件夹不存在，创建
                            destDir.mkdirs();
                        }

                        String filename = date.getTime() + "_" + cmsMovie.getFilename();
                        cmsMovie.setFilename(filename);

                        UpLoadResult uploadResult = null;
                        String destpath = GlobalConstants.getMountPoint() + desPath + File.separator + currenttime;
                        uploadResult = UpLoadFile.singleFileUplord(cmsMovie.getFilename(), CntConstants.MAX_FILESIZE,
                                destpath);
                        if (null != uploadResult)
                        {
                            String addresspath = desPath + File.separator + currenttime + File.separator
                                    + cmsMovie.getFilename();// 文件路径
                            Integer sizeFlag = uploadResult.getFlag();
                            if (-1 == sizeFlag.intValue())
                            {// 文件大小超过2G
                                return CntConstants.SUBCNT_TOOLARGE;
                            }

                            cmsMovie.setFilesize(uploadResult.getFileSize());
                            cmsMovie.setFileurl(addresspath);
                        }

                        else
                        {
                            // 文件上传失败
                            return CntConstants.SUBCNT_UPLOAD_FAIL;
                        }
                    }
                }
            }
        }

        return CntConstants.CNT_SUCCESS;
    }

    @SuppressWarnings("unchecked")
    private String insertTask(String programindex) throws Exception
    {
        log.debug("insertTask starting...");
        String result = CntConstants.CNT_SUCCESS; // "250000";
        CmsProgram program = null; // 创建发布的内容对象
        List<CmsProgram> programList = new ArrayList<CmsProgram>();
        List<CmsMovie> movieList = new ArrayList<CmsMovie>();
        CmsMovie cmsMovie = null; // 创建发布的子内容对象
        Map map = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象
        try
        {
            // 获取发布的内容对象
            program = new CmsProgram();
            program.setProgramindex(Long.valueOf(programindex));
            program = cmsProgramDS.getCmsProgram(program);
            if (program == null)
            {
                return CntConstants.CNT_NOT_EXIST;
            }
            else
            {
                programList.add(program);
                map.put("programList", programList);
            }

            // 创建发布的子内容对象List
            cmsMovie = new CmsMovie();
            cmsMovie.setProgramindex(program.getProgramindex());
            movieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
            if (movieList == null || movieList.size() <= 0)
            {
                return CntConstants.CNT_HASNO_SUBCNT; // "1:操作失败,内容下没有子内容"
            }
            else
            {
                map.put("movieList", movieList);
            }

            String desPath = cmsStorageareaLS.getAddress("1"); // 临时区
            if (null == desPath || CntConstants.GET_STORAGEAREA_FAIL.equals(desPath))
            {
                return CntConstants.TEMPAREA_NOTFOUND;
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return CntConstants.DEST_NOTFOUND;
                }

                String batchid = this.primaryKeyGenerator.getPrimarykey("ucdn_task_batch_id").toString();// 获取任务批次号
                String correlateidBMS = this.primaryKeyGenerator.getPrimarykey("ucdn_task_correlate_id").toString();
                String correlateidEPG = this.primaryKeyGenerator.getPrimarykey("ucdn_task_correlate_id").toString();
                String correlateidCDN = this.primaryKeyGenerator.getPrimarykey("ucdn_task_correlate_id").toString();

                int status = 1;

                String bmsDestIndex = getTargetSystem(1);
                if (bmsDestIndex == null)
                {
                    return CntConstants.BMS_NOTFOUND;
                }

                String epgDestIndex = getTargetSystem(2);
                if (epgDestIndex == null)
                {
                    return CntConstants.EPG_NOTFOUND;
                }

                String cdnDestIndex = getTargetSystem(3);
                if (cdnDestIndex == null)
                {
                    return CntConstants.CDN_NOTFOUND;
                }

                List<UsysConfig> usysConfigList = null;
                UsysConfig usysConfig = new UsysConfig();
                usysConfig.setCfgkey("cms.cntsyn.ftpaddress");
                usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                if (null == usysConfigList || usysConfigList.size() <= 0)
                {
                    return CntConstants.CNTSYNC_FTPADDR_NOTFOUND;
                }
                else
                {
                    usysConfig = usysConfigList.get(0);
                }

                String BMS_XMLAddress = writeXML(map, 2, desPath, 1, usysConfig.getCfgvalue()); // 生成发布BMS的XML文件，并获取XML存放路径
                String EPG_XMLAddress = writeXML(map, 2, desPath, 2, usysConfig.getCfgvalue()); // 生成发布EPG的XML文件，并获取XML存放路径
                String CDN_XMLAddress = writeXML(map, 2, desPath, 3, usysConfig.getCfgvalue()); // 生成发布CDN的XML文件，并获取XML存放路径

                // 内容向BMS系统发布 插入同步任务 和生成批处理对象信息
                insertCntSyncTask(batchid, correlateidBMS, program.getProgramindex(), BMS_XMLAddress, 3, 2, 1,
                        bmsDestIndex, 0, 1, status);
                inserBatchObjectRecord(batchid, program.getProgramindex(), 3, 1, bmsDestIndex);

                // 内容向EPG系统发布 插入同步任务 并且生成内容和其子内容的批处理对象信息
                insertCntSyncTask(batchid, correlateidEPG, program.getProgramindex(), EPG_XMLAddress, 3, 2, 2,
                        epgDestIndex, 0, 0, status);
                inserBatchObjectRecord(batchid, program.getProgramindex(), 3, 2, epgDestIndex);
                for (CmsMovie cmsmovie : movieList) // 循环生成子内容的批处理对象信息
                {
                    inserBatchObjectRecord(batchid, cmsmovie.getMovieindex(), 7, 2, epgDestIndex);
                }

                if (movieList.size() == 1)// 发布到CDN的子内容只有一个
                {
                    insertCntSyncTask(batchid, correlateidCDN, program.getProgramindex(), CDN_XMLAddress, 7, 2, 3,
                            cdnDestIndex, 0, 1, status);
                }
                else
                // 发布到CDN的子内容有多个
                {
                    insertCntSyncTask(batchid, correlateidCDN, program.getProgramindex(), CDN_XMLAddress, 7, 2, 3,
                            cdnDestIndex, 0, 0, status);
                }

                for (CmsMovie cmsmovie : movieList) // 循环生成子内容的批处理对象信息
                {
                    inserBatchObjectRecord(batchid, cmsmovie.getMovieindex(), 7, 3, cdnDestIndex);
                }
            }

        }
        catch (Exception e)
        {
            log.error("Error occurred in insertTask method:", e);
            // 内容修改同步过程失败，请稍后重试：250206
            return CntConstants.CNT_MODSYNC_FAIL;
        }
        log.debug("insertTask end");
        return result;

    }

    /**
     * 根据要发布的系统类型或者对应的系统index
     * 
     * @param type
     *            系统类型
     * @return 发布系统index
     */
    private String getTargetSystem(int type) throws Exception
    {
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(type);
        List<Targetsystem> listtsystem;
        String systemIndex = null;
        try
        {
            listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem != null && listtsystem.size() > 0)
            {
                targetSystem = listtsystem.get(0);
                systemIndex = targetSystem.getTargetindex().toString();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in getTargetSystem", e);
            // 内容修改同步过程失败，请稍后重试：250206
            throw new Exception(CntConstants.CNT_MODSYNC_FAIL);
        }
        return systemIndex;
    }

    /**
     * 根据发布系统类型，生成对应的XML文件，并返回XML存放的路径
     * 
     * @param map
     *            装有内容对象List和其子内容对象List的map
     * @param syncType
     *            发布Action类型
     * @param tempAreaPath
     *            临时区路径
     * @param type
     *            发布系统类型
     * @throws Exception
     * @return XML存放的路径
     */
    @SuppressWarnings("unchecked")
    private String writeXML(Map<String, List> map, int syncType, String tempAreaPath, int type, String ftpAddress)
            throws Exception
    {
        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = "";

        // 根据发布系统类型 获取相应的XML文件名
        switch (type)
        {
            case 1:
                fileName = date.getTime() + "_program.xml";
                break;
            case 2:
                fileName = date.getTime() + "_program_movie.xml";
                break;
            case 3:
                fileName = date.getTime() + "_movie.xml";
                break;
        }

        // 创建XML文件
        String synctype = "";
        String tempFilePath = tempAreaPath + File.separator + "xmlsync" + File.separator + dir + File.separator
                + "program" + File.separator;
        String mountAndTempfilePath = GlobalConstants.getMountPoint() + tempFilePath;
        String retPath = filterSlashStr(tempFilePath + fileName);
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");

        // 获取发布Action类型
        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }

        CmsProgram cmsProgram = null;
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;
        if (type == 1 || type == 2) // 发布BMS和EPG系统 生成内容相关的XML内容
        {
            List<CmsProgram> list = (List<CmsProgram>) map.get("programList");
            cmsProgram = list.get(0);

            Element objectElement = null;
            Element propertyElement = null;

            // 元素节点
            objectElement = objectsElement.addElement("Object");
            objectElement.addAttribute("ElementType", "Program");
            objectElement.addAttribute("ContentID", cmsProgram.getProgramid());
            objectElement.addAttribute("Action", synctype);

            writeElement(objectElement, propertyElement, "CPContentID", cmsProgram.getCpcontentid(), false);
            writeElement(objectElement, propertyElement, "Name", cmsProgram.getNamecn(), true);
            writeElement(objectElement, propertyElement, "OrderNumber", cmsProgram.getOrdernumber(), false);
            writeElement(objectElement, propertyElement, "OriginalName", cmsProgram.getOriginalnamecn(), true);
            writeElement(objectElement, propertyElement, "SortName", cmsProgram.getSortname(), true);
            writeElement(objectElement, propertyElement, "SearchName", cmsProgram.getSearchname(), true);
            writeElement(objectElement, propertyElement, "ActorDisplay", cmsProgram.getActors(), true);
            writeElement(objectElement, propertyElement, "WriterDisplay", cmsProgram.getWriters(), true);
            writeElement(objectElement, propertyElement, "OriginalCountry", cmsProgram.getCountry(), false);
            writeElement(objectElement, propertyElement, "Language", cmsProgram.getLanguage(), false);
            writeElement(objectElement, propertyElement, "ReleaseYear", cmsProgram.getReleaseyear(), false);
            writeElement(objectElement, propertyElement, "OrgAirDate", cmsProgram.getOrgairdate(), false);
            writeElement(objectElement, propertyElement, "LicensingWindowStart", cmsProgram.getLicensingstart(), false);
            writeElement(objectElement, propertyElement, "LicensingWindowEnd", cmsProgram.getLicensingend(), false);
            writeElement(objectElement, propertyElement, "DisplayAsNew", cmsProgram.getNewcomedays(), false);
            writeElement(objectElement, propertyElement, "DisplayAsLastChance", cmsProgram.getRemainingdays(), false);
            writeElement(objectElement, propertyElement, "Macrovision", cmsProgram.getMacrovision(), false);
            writeElement(objectElement, propertyElement, "Description", cmsProgram.getDesccn(), true);
            writeElement(objectElement, propertyElement, "PriceTaxIn", cmsProgram.getPricetaxin(), false);
            writeElement(objectElement, propertyElement, "Status", cmsProgram.getIsvalid(), false);
            writeElement(objectElement, propertyElement, "SourceType", cmsProgram.getSourcetype(), false);
            writeElement(objectElement, propertyElement, "SeriesFlag", cmsProgram.getSeriesflag(), false);
            writeElement(objectElement, propertyElement, "ContentProvider", cmsProgram.getCpid(), false);
            writeElement(objectElement, propertyElement, "KeyWords", cmsProgram.getKeywords(), true);
            writeElement(objectElement, propertyElement, "Tags", cmsProgram.getContenttags(), false);
            writeElement(objectElement, propertyElement, "ViewPoint", cmsProgram.getViewpoint(), true);
            writeElement(objectElement, propertyElement, "StarLevel", cmsProgram.getRecommendstart(), false);
            writeElement(objectElement, propertyElement, "Rating", cmsProgram.getRating(), false);
            writeElement(objectElement, propertyElement, "Awards", cmsProgram.getAwards(), true);
            writeElement(objectElement, propertyElement, "Length", cmsProgram.getDuration(), false);
            writeElement(objectElement, propertyElement, "ProgramType", cmsProgram.getProgramtype(), true);
            writeElement(objectElement, propertyElement, "Reserve1", cmsProgram.getReserve1(), false);
            writeElement(objectElement, propertyElement, "Reserve2", cmsProgram.getReserve2(), false);
            writeElement(objectElement, propertyElement, "Reserve3", cmsProgram.getReserve3(), false);
            writeElement(objectElement, propertyElement, "Reserve4", cmsProgram.getReserve4(), false);
            writeElement(objectElement, propertyElement, "Reserve5", cmsProgram.getReserve5(), false);
            writeElement(objectElement, propertyElement, "UniContentId", cmsProgram.getUnicontentid(), true);
        }

        if (type == 3 || type == 2)// 发布CDN 和 EGP 系统需要生成的子内容相关XML
        {
            List<CmsMovie> movieList = (List<CmsMovie>) map.get("movieList");
            mapList = new ArrayList<Map>();
            for (CmsMovie cmsMovie : movieList)
            {
                Element objectElement = null;
                Element propertyElement = null;

                // 元素节点
                objectElement = objectsElement.addElement("Object");
                objectElement.addAttribute("ElementType", "Movie");
                objectElement.addAttribute("PhysicalContentID", cmsMovie.getMovieid());
                objectElement.addAttribute("Action", synctype);
                objectElement.addAttribute("Type", cmsMovie.getMovietype().toString());

                writeElement(objectElement, propertyElement, "FileURL", ftpAddress + GlobalConstants.getMountPoint()
                        + cmsMovie.getFileurl(), false);
                writeElement(objectElement, propertyElement, "CPContentID", cmsMovie.getCpcontentid(), false);
                writeElement(objectElement, propertyElement, "SourceDRMType", cmsMovie.getSourcedrmtype(), false);
                writeElement(objectElement, propertyElement, "DestDRMType", cmsMovie.getDestdrmtype(), false);
                writeElement(objectElement, propertyElement, "AudioType", cmsMovie.getAudiotype(), false);
                writeElement(objectElement, propertyElement, "ScreenFormat", cmsMovie.getScreenformat(), false);
                writeElement(objectElement, propertyElement, "ClosedCaptioning", cmsMovie.getClosedcaptioning(), false);
                writeElement(objectElement, propertyElement, "Duration", cmsMovie.getDuration(), false);
                writeElement(objectElement, propertyElement, "FileSize", cmsMovie.getFilesize(), false);
                writeElement(objectElement, propertyElement, "BitRateType", cmsMovie.getBitratetype(), false);
                writeElement(objectElement, propertyElement, "VideoType", cmsMovie.getVideotype(), false);
                writeElement(objectElement, propertyElement, "AudioEncodingType", cmsMovie.getAudiotrack(), false);
                writeElement(objectElement, propertyElement, "Resolution", cmsMovie.getResolution(), false);
                writeElement(objectElement, propertyElement, "Video Profile", cmsMovie.getVideoprofile(), false);
                writeElement(objectElement, propertyElement, "System Layer", cmsMovie.getSystemlayer(), false);
                writeElement(objectElement, propertyElement, "Domain", cmsMovie.getDomain(), false);
                writeElement(objectElement, propertyElement, "Hotdegree", cmsMovie.getHotdegree(), false);

                if (type == 2) // 获取发布EPG系统生成相应的MAP关系
                {
                    String objectId = getObjectId(cmsMovie.getMovieindex().toString());
                    mappingMap = new HashMap<String, String>();
                    mappingMap.put("ParentID", cmsProgram.getProgramid());
                    mappingMap.put("ObjectID", objectId);
                    mappingMap.put("ElementID", cmsMovie.getMovieid());
                    mappingMap.put("Type", "");
                    mappingMap.put("Sequence", "");
                    mappingMap.put("ValidStart", "");
                    mappingMap.put("ValidEnd", "");
                    mapList.add(mappingMap);
                }
            }
        }

        if (type == 2)// 发布EPG系统生成相应的MAP关系XML
        {
            // 生成Mappings节点
            Element mappingElement = adiElement.addElement("Mappings");
            for (Map map2 : mapList)
            {
                Element objectElement = null;
                Element propertyElement = null;
                objectElement = mappingElement.addElement("Mapping");
                objectElement.addAttribute("ParentType", "Program");
                objectElement.addAttribute("ParentID", map2.get("ParentID").toString());
                objectElement.addAttribute("ObjectID", map2.get("ObjectID").toString());
                objectElement.addAttribute("ElementType", "Movie");
                objectElement.addAttribute("ElementID", map2.get("ElementID").toString());
                objectElement.addAttribute("Action", synctype);

                writeElement(objectElement, propertyElement, "Type", "", false);
                writeElement(objectElement, propertyElement, "Sequence", "", false);
                writeElement(objectElement, propertyElement, "ValidStart", "", false);
                writeElement(objectElement, propertyElement, "ValidEnd", "", false);
            }
        }

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("Error occurred in write method:" + e);
            // 内容修改同步过程失败，请稍后重试: 250206
            return CntConstants.CNT_MODSYNC_FAIL;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    log.error("IO exception:" + e);
                    // 内容修改同步过程失败，请稍后重试: 250206
                    throw new Exception(CntConstants.CNT_MODSYNC_FAIL);
                }
            }
        }

        return retPath;
    }

    /**
     * 写入节点属性
     * 
     * @param objectElement
     *            父节点
     * @param propertyElement
     *            子节点
     * @param key
     *            节点名称
     * @param value
     *            节点值
     */
    private void writeElement(Element objectElement, Element propertyElement, String key, Object value, Boolean convert)
    {
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", key);
        if (value == null)
        {
            propertyElement.addText("");
        }
        else
        {
            if (convert)
            {
                propertyElement.addCDATA(String.valueOf(value));
            }
            else
            {
                propertyElement.addText(String.valueOf(value));
            }
        }
    }

    /**
     * 
     * @param batchid
     *            任务批次号
     * @param correlateid
     *            任务流水号
     * @param programIndex
     *            同步对象index
     * @param xmlAddress
     *            内容相关对象的XML文件URL
     * @param objecttype
     *            同步对象类型：若干object，若干map关系
     * @param taskType
     *            同步类型：1 REGIST，2 UPDATE，3 DELETE
     * @param desttype
     *            目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex
     *            目标系统index
     * @param batchsingleflag
     *            本批任务是否针对单个对象：0否，1是
     * @param tasksingleflag
     *            当前任务是否针对单个对象：0否，1是
     */
    private void insertCntSyncTask(String batchid, String correlateid, Long programIndex, String xmlAddress,
            int objecttype, int taskType, int desttype, String destindex, int batchsingleflag, int tasksingleflag,
            int status) throws Exception
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        try
        {
            cntSyncTask.setStatus(status);
            cntSyncTask.setPriority(2);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);

            cntSyncTask.setDestindex(Long.valueOf(destindex));
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("Error occurred in insertCntSyncTask", e);
            // 内容修改同步过程失败，请稍后重试: 250206
            throw new Exception(CntConstants.CNT_MODSYNC_FAIL);
        }
    }

    /**
     * 
     * @param batchid
     *            任务批次号
     * @param objindex
     *            同步对象index
     * @param objtype
     *            同步对象类型：若干object
     * @param desttype
     *            目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex
     *            目标系统index
     */
    private void inserBatchObjectRecord(String batchid, Long objindex, int objtype, int desttype, String destindex)
            throws Exception
    {
        BatchObjectRecord batchObjectRecord = new BatchObjectRecord();
        try
        {
            batchObjectRecord.setBatchid(batchid);
            batchObjectRecord.setObjindex(objindex);
            batchObjectRecord.setObjtype(objtype);
            batchObjectRecord.setDesttype(desttype);
            batchObjectRecord.setDestindex(Long.valueOf(destindex));

            batchObjectDS.insertBatchObjectRecord(batchObjectRecord);
        }
        catch (Exception e)
        {
            log.error("Error occurred in inserBatchObjectRecord", e);
            // 内容修改同步过程失败，请稍后重试: 250206
            throw new Exception(CntConstants.CNT_MODSYNC_FAIL);
        }
    }

    // 获取时间 如"20111002"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 根据mapping对象index生成ObjectId
     * 
     * @param index
     *            mapping对象index
     * @return ObjectId
     */
    private String getObjectId(String index)
    {
        String objectId = "";
        objectId = String.format("%02d", 32) + String.format("%030d", Integer.valueOf(index));
        return objectId;

    }

    public void setCmsProgramBufDS(ICmsProgramBufDS cmsProgramBufDS)
    {
        this.cmsProgramBufDS = cmsProgramBufDS;
    }

    public void setCmsMovieBufDS(ICmsMovieBufDS cmsMovieBufDS)
    {
        this.cmsMovieBufDS = cmsMovieBufDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCntModWkfwCheck(ICntModWkfwCheck cntModWkfwCheck)
    {
        this.cntModWkfwCheck = cntModWkfwCheck;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setBatchObjectDS(IBatchObjectRecordDS batchObjectDS)
    {
        this.batchObjectDS = batchObjectDS;
    }

    public IProgramPlatformSynLS getProgramPlatformSynLS()
    {
        return programPlatformSynLS;
    }

    public void setProgramPlatformSynLS(IProgramPlatformSynLS programPlatformSynLS)
    {
        this.programPlatformSynLS = programPlatformSynLS;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

}

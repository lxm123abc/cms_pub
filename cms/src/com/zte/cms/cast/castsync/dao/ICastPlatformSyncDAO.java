package com.zte.cms.cast.castsync.dao;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICastPlatformSyncDAO
{

    /**
     * 根据条件分页查询CastPlatformSync对象，作为查询条件的参数
     *
     * @param castPlatformSync CastPlatformSync对象 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CastPlatformSync castPlatformSync, int start, int pageSize)throws DAOException;
    
    /**
     * 按条件查询
     * @param castPlatformSync
     * @return
     * @throws DAOException
     */
    public CastPlatformSync getCastPlatformSync( CastPlatformSync castPlatformSync )throws DAOException;

}

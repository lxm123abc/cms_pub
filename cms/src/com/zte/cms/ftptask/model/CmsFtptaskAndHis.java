package com.zte.cms.ftptask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsFtptaskAndHis extends DynamicBaseObject
{
    private java.lang.Long taskindex;
    private java.lang.Long tasktype;
    private java.lang.Long contentindex;
    private java.lang.Long fileindex;
    private java.lang.Integer uploadtype;
    private java.lang.String srcaddress;
    private java.lang.String destaddress;
    private java.lang.Integer status;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer retrytimes;
    private java.lang.Integer srcfilehandle;
    private java.lang.String resultdesc;
    private java.lang.String batchid;
    private java.lang.String fileid;
    private java.lang.Integer filetype;
    private java.lang.String startDate1;
    private java.lang.String startDate2;
    private java.lang.String endDate1;
    private java.lang.String endDate2;

    public java.lang.String getStartDate1()
    {
        return startDate1;
    }

    public void setStartDate1(java.lang.String startDate1)
    {
        this.startDate1 = startDate1;
    }

    public java.lang.String getStartDate2()
    {
        return startDate2;
    }

    public void setStartDate2(java.lang.String startDate2)
    {
        this.startDate2 = startDate2;
    }

    public java.lang.String getEndDate1()
    {
        return endDate1;
    }

    public void setEndDate1(java.lang.String endDate1)
    {
        this.endDate1 = endDate1;
    }

    public java.lang.String getEndDate2()
    {
        return endDate2;
    }

    public void setEndDate2(java.lang.String endDate2)
    {
        this.endDate2 = endDate2;
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.Long getContentindex()
    {
        return contentindex;
    }

    public void setContentindex(java.lang.Long contentindex)
    {
        this.contentindex = contentindex;
    }

    public java.lang.Long getFileindex()
    {
        return fileindex;
    }

    public void setFileindex(java.lang.Long fileindex)
    {
        this.fileindex = fileindex;
    }

    public java.lang.Integer getUploadtype()
    {
        return uploadtype;
    }

    public void setUploadtype(java.lang.Integer uploadtype)
    {
        this.uploadtype = uploadtype;
    }

    public java.lang.String getSrcaddress()
    {
        return srcaddress;
    }

    public void setSrcaddress(java.lang.String srcaddress)
    {
        this.srcaddress = srcaddress;
    }

    public java.lang.String getDestaddress()
    {
        return destaddress;
    }

    public void setDestaddress(java.lang.String destaddress)
    {
        this.destaddress = destaddress;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getRetrytimes()
    {
        return retrytimes;
    }

    public void setRetrytimes(java.lang.Integer retrytimes)
    {
        this.retrytimes = retrytimes;
    }

    public java.lang.Integer getSrcfilehandle()
    {
        return srcfilehandle;
    }

    public void setSrcfilehandle(java.lang.Integer srcfilehandle)
    {
        this.srcfilehandle = srcfilehandle;
    }

    public java.lang.String getResultdesc()
    {
        return resultdesc;
    }

    public void setResultdesc(java.lang.String resultdesc)
    {
        this.resultdesc = resultdesc;
    }

    public java.lang.Long getTasktype()
    {
        return tasktype;
    }

    public void setTasktype(java.lang.Long tasktype)
    {
        this.tasktype = tasktype;
    }

    public java.lang.String getBatchid()
    {
        return batchid;
    }

    public void setBatchid(java.lang.String batchid)
    {
        this.batchid = batchid;
    }

    public java.lang.String getFileid()
    {
        return fileid;
    }

    public void setFileid(java.lang.String fileid)
    {
        this.fileid = fileid;
    }

    public java.lang.Integer getFiletype()
    {
        return filetype;
    }

    public void setFiletype(java.lang.Integer filetype)
    {
        this.filetype = filetype;
    }

    public void initRelation()
    {
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("tasktype", "TASKTYPE");
        this.addRelation("contentindex", "CONTENTINDEX");
        this.addRelation("fileindex", "FILEINDEX");
        this.addRelation("uploadtype", "UPLOADTYPE");
        this.addRelation("srcaddress", "SRCADDRESS");
        this.addRelation("destaddress", "DESTADDRESS");
        this.addRelation("status", "STATUS");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("retrytimes", "RETRYTIMES");
        this.addRelation("srcfilehandle", "SRCFILEHANDLE");
        this.addRelation("resultdesc", "RESULTDESC");
        this.addRelation("batchid", "BATCHID");
        this.addRelation("fileid", "FILEID");
        this.addRelation("filetype", "FILETYPE");
    }

}

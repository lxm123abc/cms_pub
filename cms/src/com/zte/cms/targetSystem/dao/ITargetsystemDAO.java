package com.zte.cms.targetSystem.dao;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ITargetsystemDAO
{
    /**
     * 新增Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DAOException dao异常
     */
    public void insertTargetsystem(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据主键更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DAOException dao异常
     */
    public void updateTargetsystem(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据条件更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem更新条件
     * @throws DAOException dao异常
     */
    public void updateTargetsystemByCond(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据主键删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DAOException dao异常
     */
    public void deleteTargetsystem(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据条件删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem删除条件
     * @throws DAOException dao异常
     */
    public void deleteTargetsystemByCond(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据主键查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return 满足条件的Targetsystem对象
     * @throws DAOException dao异常
     */
    public Targetsystem getTargetsystem(Targetsystem targetsystem) throws DAOException;
    
    /**
     * 检查目标系统的id是否重复
     * @param targetsystemID
     * @return int
     * @throws DomainServiceException
     */
    public int checkTargetsystemID(String targetsystemID)throws DAOException;

    /**
     * 根据条件查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return 满足条件的Targetsystem对象集
     * @throws DAOException dao异常
     */
    public List<Targetsystem> getTargetsystemByCond(Targetsystem targetsystem) throws DAOException;

    /**
     * 根据条件分页查询Targetsystem对象，作为查询条件的参数
     * 
     * @param targetsystem Targetsystem对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Targetsystem对象，作为查询条件的参数
     * 
     * @param targetsystem Targetsystem对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    public PageInfo pageInfoQueryPop(Targetsystem targetsystem, int start, int pageSize) throws DAOException;

    /**
     * 查询服务关联目标系统
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws DAOException
     */
    public PageInfo queryServiceBindTargetListByCond(Service service, int start, int pageSize)throws DAOException;
}
package com.zte.cms.cast.castsync.service;

import java.util.List;

import com.zte.cms.cast.castsync.dao.ICastTargetSyncDAO;
import com.zte.cms.cast.castsync.model.CastTargetSync;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CastTargetSyncDS extends DynamicObjectBaseDS implements ICastTargetSyncDS
{
private Log log = SSBBus.getLog(getClass());
    
    private ICastTargetSyncDAO dao = null;
    
    public void setDao(ICastTargetSyncDAO dao)
    {
        this.dao = dao;
    }
    
    public TableDataInfo pageInfoQuery(CastTargetSync castTargetSync, int start, int pageSize)throws DomainServiceException
    {
        log.debug( "get castTargetSync page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castTargetSync, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get castTargetSync page info by condition end" );
        return tableInfo;
    }
}

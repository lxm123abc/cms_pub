package com.zte.cms.storageManage.service;

import java.util.List;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.cms.storageManage.dao.ICmsStoragemanageDAO;
import com.zte.cms.storageManage.service.ICmsStoragemanageDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.cms.common.GlobalConstants;

public class CmsStoragemanageDS extends DynamicObjectBaseDS implements ICmsStoragemanageDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsStoragemanageDAO dao = null;

    // private String rootAddress = "d:\\storageManage\\"; //挂载点

    // public String getRootAddress()
    // {
    // return rootAddress;
    // }

    public void setDao(ICmsStoragemanageDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        log.debug("insert cmsStoragemanage starting...");
        try
        {
            Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_storageManage_storageIndex");
            cmsStoragemanage.setStorageindex(id);
            dao.insertCmsStoragemanage(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsStoragemanage end");
    }

    public void updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        log.debug("update cmsStoragemanage by pk starting...");
        try
        {
            dao.updateCmsStoragemanage(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsStoragemanage by pk end");
    }

    public void updateCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException
    {
        log.debug("update cmsStoragemanageList by pk starting...");
        if (null == cmsStoragemanageList || cmsStoragemanageList.size() == 0)
        {
            log.debug("there is no datas in cmsStoragemanageList");
            return;
        }
        try
        {
            for (CmsStoragemanage cmsStoragemanage : cmsStoragemanageList)
            {
                dao.updateCmsStoragemanage(cmsStoragemanage);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsStoragemanageList by pk end");
    }

    public void removeCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        log.debug("remove cmsStoragemanage by pk starting...");
        try
        {
            dao.deleteCmsStoragemanage(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsStoragemanage by pk end");
    }

    public void removeCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException
    {
        log.debug("remove cmsStoragemanageList by pk starting...");
        if (null == cmsStoragemanageList || cmsStoragemanageList.size() == 0)
        {
            log.debug("there is no datas in cmsStoragemanageList");
            return;
        }
        try
        {
            for (CmsStoragemanage cmsStoragemanage : cmsStoragemanageList)
            {
                dao.deleteCmsStoragemanage(cmsStoragemanage);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsStoragemanageList by pk end");
    }

    public CmsStoragemanage getCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        log.debug("get cmsStoragemanage by pk starting...");
        CmsStoragemanage rsObj = null;
        try
        {
            rsObj = dao.getCmsStoragemanage(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStoragemanageList by pk end");
        return rsObj;
    }

    public List<CmsStoragemanage> getCmsStoragemanageByCond(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException
    {
        log.debug("get cmsStoragemanage by condition starting...");
        List<CmsStoragemanage> rsList = null;
        try
        {
            rsList = dao.getCmsStoragemanageByCond(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStoragemanage by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryPriority(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsStoragemanage page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryPriority(cmsStoragemanage, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsStoragemanage>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsStoragemanage page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsStoragemanage page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsStoragemanage, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsStoragemanage>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsStoragemanage page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsStoragemanage page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsStoragemanage, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsStoragemanage>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsStoragemanage page info by condition end");
        return tableInfo;
    }

    // 通过连表查询，使空间得到设备的优先级
    public List<CmsStoragemanage> getCmsStoragemanageByCondPriority(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException
    {
        log.debug("get cmsStoragemanage with priority by condition starting...");
        List<CmsStoragemanage> rsList = null;
        try
        {
            rsList = dao.getCmsStoragemanageByCondPriority(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStoragemanage with priority by condition end");
        return rsList;
    }

    // 查找绑定的空间中优先级最高，容量最大，且可用的空间
    public List<CmsStoragemanage> getCmsStoragemanageofbindedSpace(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException
    {
        log.debug("get cmsStoragemanage with priority by condition starting...");
        List<CmsStoragemanage> rsList = null;
        try
        {
            rsList = dao.getCmsStoragemanageofbindedSpace(cmsStoragemanage);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsStoragemanage with priority by condition end");
        return rsList;
    }

    /*******************************************************************************************************************
     * 查询存储区可用空间大小
     * 
     ******************************************************************************************************************/
    public List<CmsStoragemanage> getCmsStoragemanageList() throws DomainServiceException
    {
        List<CmsStoragemanage> cmsStorageManageList = dao.getCmsStoragemanageLists();
        return cmsStorageManageList;
    }

}

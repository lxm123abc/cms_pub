package com.zte.cms.common;

import net.sf.cglib.core.Converter;

public class BeanPropertyConvert
  implements Converter
{
  public Object convert(Object value, Class target, Object context)
  {
    if ((target.equals(String.class)) && 
      (value == null)) {
      value = "";
      return value;
    }

    return value;
  }
}
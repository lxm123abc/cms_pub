package com.zte.cms.cpsp.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsCpbind extends DynamicBaseObject
{
    private java.lang.Long bindindex;
    private java.lang.String cpid;
    private java.lang.String lpid;
    private java.lang.String cpname;

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getLpid()
    {
        return lpid;
    }

    public void setLpid(java.lang.String lpid)
    {
        this.lpid = lpid;
    }

    public java.lang.String getCpname()
    {
        return cpname;
    }

    public void setCpname(java.lang.String cpname)
    {
        this.cpname = cpname;
    }

    public void initRelation()
    {
        this.addRelation("bindinex", "BINDINEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("lpid", "LPID");
        this.addRelation("cpname", "CPNAME");
    }

    public java.lang.Long getBindindex()
    {
        return bindindex;
    }

    public void setBindindex(java.lang.Long bindindex)
    {
        this.bindindex = bindindex;
    }
}

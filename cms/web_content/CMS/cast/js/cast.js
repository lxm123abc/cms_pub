    //判断是否包含不允许输入的字符 !@#$%^*;|?()[]{}<>' \"
    function checkUnallowedCharacter(str){
		var pattern = /[!@#$%^*\(\)\\|\[\]\{\};\<\>\'\"?]/;
		return pattern.test(str);
    }

   //日期格式转换成字符串
    function formatDBDate(str, len) {
        str = str.replace(/:/g,"");<!-- replace :     -->
        str = str.replace(/ /g,"");<!-- replace blank -->
        str = str.replace(/-/g,"");<!-- replace -     -->
        if (str == "") {
            str = "00000000000000";
        }
        return str.substr(0, len);
    }
    
function getTime(str){  
    if(str!=null){
    	if(str.length==4){
    		return str.substr(0,4);
    	}
    	if(str.length == 8){
	    	return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    }
	    
	    if (str.length == 10){ 
	        return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2)+ " " + str.substr(8,2);
	    }
	    
	    if(str.length ==14){
	       return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	    }else{
	        return "";
	    }  
    }else{
        return "";
    }
}
    //字符串转换成日期格式
	function formatFromDBDate(str) {
	    var result = "";
	    str = trim(str);
	    if (str.length == 0 || str.charAt(0) == '0')
	        return result;
	        
	    if (str.length == 10 || str.length == 19)
	        return str;
	    
	    result = str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    if (str.length == 14)
	        result += " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	        
	    return result;
	}
	
	
	
//验证输入框中字数是否超过最大长度
function IsMaxLength(input,input_msg,len){

  if(utf8_strlen2(trim(input.value)) > len){
      document.getElementById(input_msg).innerText = $res_entry("input.system.maxlength.suggest.13")+len+$res_entry("input.system.maxlength.suggest.14");//"不能超过"+len+ "个字符";
      document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}   


function hasChineseChars(sCheck){
  for (var nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charCodeAt(nIndex);
        if (cCheck > 255)
           return true;
  }
  return false;
}

function dynChgSelectWidth(id)
{
	
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width = getCharWidth(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}

//验证URL格式
function isURLFormat(input,input_msg)
{
  var inputValue=document.getElementById(input).value;
  if(trim(inputValue) != '' && inputValue != null)
  {
    var strRegex = /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/; 
    var exp=new RegExp(strRegex);
    var inputvalue = inputValue.toLowerCase().trim();
    var reg = inputvalue.match(exp);
    if(reg == null)
    {
        document.getElementById(input_msg).innerText = $res_entry("msg.info.cast.20")+" http://ss.cn"; 
        document.getElementById(input).onfocus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;   
    }      
  }
}



	function check(){
	
		clearAllMsgLabel();
	
		var castname=$("#castname").val();
		var persondisplayname=$("#persondisplayname").val();
		var personsortname=$("#personsortname").val();
		var personsearchname=$("#personsearchname").val();
		var firstname=$("#firstname").val();
		var lastname=$("#lastname").val();
		var middlename=$("#middlename").val();
		var hometown=$("#hometown").val();
		var education=$("#education").val();
		var height=$("#height").val();
		var weight=$("#weight").val();
		var favorite=$("#favorite").val();
		var webpage=$("#webpage").val();
		var description=$("#description").val();
		var birthday=document.getElementById("birthdayTime").value;
		/*人物名称*/
	    if(checkNull(castname))
	    {	
	        document.getElementById("castname_msg").innerHTML=$res_entry("msg.info.basicdata.033"); 
	        document.cast_add_form.castname.focus();
	        return false;  
	    }
	    else if(checkUnallowedCharacter(castname))
	    {
	    	document.getElementById("castname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        document.cast_add_form.castname.focus();
	         return false;  
	    }
	   	error = IsMaxLength(document.getElementById('castname'),'castname_msg','64');
    	if(error == false)
    	{
        	return false;
    	}
    	/*显示名称*/
    	if(!checkNull(persondisplayname))
    	{
    		if(checkUnallowedCharacter(persondisplayname))
    		{
    			document.getElementById("persondisplayname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.persondisplayname.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('persondisplayname'),'persondisplayname_msg','64');
    	if(error == false)
    	{
        	return false;
    	}
    	/*排序名称*/
    	if(!checkNull(personsortname))
    	{
    		if(checkUnallowedCharacter(personsortname))
    		{
    			document.getElementById("personsortname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.personsortname.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('personsortname'),'personsortname_msg','64');
    	if(error == false)
    	{
        	return false;
    	}
    	/*索引名称*/
    	if(!checkNull(personsearchname))
    	{
    		if(checkUnallowedCharacter(personsearchname))
    		{
    			document.getElementById("personsearchname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.personsearchname.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('personsearchname'),'personsearchname_msg','64');
    	if(error == false)
    	{
        	return false;
    	}
    	/*姓*/
    	if(checkNull(firstname))
    	{
    		document.getElementById("firstname_msg").innerHTML=$res_entry("msg.info.basicdata.033"); 
        	document.cast_add_form.firstname.focus();
         	return false; 
    		
    	}else if(checkUnallowedCharacter(firstname))
		{
			document.getElementById("firstname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
        	document.cast_add_form.firstname.focus();
         	return false;  
		}
    	error = IsMaxLength(document.getElementById('firstname'),'firstname_msg','32');
    	if(error == false)
    	{
        	return false;
    	}
    	/*名*/
    	if(!checkNull(lastname))
    	{
    		if(checkUnallowedCharacter(lastname))
    		{
    			document.getElementById("lastname_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.lastname.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('lastname'),'lastname_msg','32');
    	if(error == false)
    	{
        	return false;
    	}
    	/*中间名*/
    	if(!checkNull(middlename))
    	{
    		if(checkUnallowedCharacter(middlename))
    		{
    			document.getElementById("middlename_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.middlename.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('middlename'),'middlename_msg','32');
    	if(error == false)
    	{
        	return false;
    	}
    	/*籍贯*/
    	if(!checkNull(hometown))
    	{
    		if(checkUnallowedCharacter(hometown))
    		{
    			document.getElementById("hometown_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.hometown.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('hometown'),'hometown_msg','128');
    	if(error == false)
    	{
        	return false;
    	}
    	/*教育程度*/
    	if(!checkNull(education))
    	{
    		if(checkUnallowedCharacter(education))
    		{
    			document.getElementById("education_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.education.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('education'),'education_msg','128');
    	if(error == false)
    	{
        	return false;
    	}
    	/*身高*/
    	if(!checkNull(height))
    	{
    		var pattern_height=/^[1-9]\d+$/;
    		if(!pattern_height.test($.trim(height)))
    		{
    			document.getElementById("height_msg").innerHTML=$res_entry("msg.info.cast.21"); 
	        	document.cast_add_form.height.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('height'),'height_msg','5');
    	if(error == false)
    	{
        	return false;
    	}
    	/*体重*/
    	if(!checkNull(weight))
    	{
    		var pattern_weight=/^[1-9]\d*$/;
    		if(!pattern_weight.test($.trim(weight)))
    		{
    			document.getElementById("weight_msg").innerHTML=$res_entry("msg.info.cast.22"); 
	        	document.cast_add_form.weight.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('weight'),'weight_msg','5');
    	if(error == false)
    	{
        	return false;
    	}
    	/*爱好*/
    	if(!checkNull(favorite))
    	{
    		if(checkUnallowedCharacter(favorite))
    		{
    			document.getElementById("favorite_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.education.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('favorite'),'favorite_msg','128');
    	if(error == false)
    	{
        	return false;
    	}
    	/*主页*/
    	if(!checkNull(webpage))
    	{
    		isURLFormat("webpage","webpage_msg");
    		if(checkUnallowedCharacter(webpage))
    		{
    			document.getElementById("webpage_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.webpage.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('webpage'),'webpage_msg','128');
    	if(error == false)
    	{
        	return false;
    	}
    	/*描述*/
    	if(!checkNull(description))
    	{
    		if(checkUnallowedCharacter(description))
    		{
    			document.getElementById("description_msg").innerHTML=$res_entry("msg.info.basicdata.035")+"!@#$%^*;|?()[]{}<>' \""; 
	        	document.cast_add_form.description.focus();
	         	return false;  
    		}
    	}
    	error = IsMaxLength(document.getElementById('description'),'description_msg','1024');
    	if(error == false)
    	{
        	return false;
    	}
    	
    	$("#birthday").val(birthday.replace(/\D/g,""));
		return true;

	}
	
	
	
	
	
	
	
	
	
     
//日期校验,所选日期应大于当前系统日期时间
function validatedate(dateid,datetarget,date_msg)
{
    var date=formatDBDate(document.getElementById(datetarget).value,14);
    var nowdate=getTimefromUTC("yyyy-mm-dd hh:mm:ss", new Date());
    var date1=formatDBDate(nowdate,14);
    if((date-date1)<0)
    {
    	$("#"+date_msg).html($res_entry("msg.info.cast.23"));
    	dateid.onfocus();
    	return ;
    }else{
    	$("#"+date_msg).html("");
    }
}
            	
	
function beginTimeLEndTime(beginTime,endTime,input_msg)
{
  document.getElementById(input_msg).innerText = "";   
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.12"); 
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        return true;               
    }
  }
}

function startSmallestEndBiggest(beginTime,endTime,input_msg)
{
  document.getElementById(input_msg).innerText = "";   
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.info.cast.24"); 
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        return true;               
    }
  }
}

  package com.zte.cms.service.servicepublishmanager.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServiceTargetSyn extends DynamicBaseObject
{
    private java.lang.Long psyncindex;
    private java.lang.String serviceid;
    private java.lang.String servicename;
    private java.lang.String  createtime;
    private java.lang.Integer feetype;
    private java.lang.Integer feecode;
    private java.lang.Integer fixedfee;
    private java.lang.String description;
    private java.lang.String licensingwindowstart;
    private java.lang.String licensingwindowend;    
    private java.lang.String servicecode;
    
    

    private java.lang.Integer  targettype;
    private java.lang.String  targetname;
    
	private java.lang.Long syncindex;
	private java.lang.Long relateindex;
	private java.lang.Integer objecttype;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String elementid;
	private java.lang.String parentid;
	private java.lang.Long targetindex;
	private java.lang.Integer status;
	private java.lang.Integer operresult;
	private java.lang.String publishtime;
	private java.lang.String cancelpubtime;
	private java.lang.Long reserve01;
	private java.lang.String reserve02;
	
	private java.lang.String programid;
	private java.lang.String namecn;
	private java.lang.String seriesid;
	private java.lang.String channelid;
	private java.lang.String channelname;
	
	private java.lang.String cpcontentid;
	
	 private java.lang.Integer sequence;

	
	
	
	public java.lang.Integer getSequence()
    {
        return sequence;
    }



    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }



    public java.lang.String getServiceid()
    {
        return serviceid;
    }



    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }



    public java.lang.String getServicename()
    {
        return servicename;
    }



    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }



    public java.lang.String getCreatetime()
    {
        return createtime;
    }



    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }



    public java.lang.Long getSyncindex()
    {
        return syncindex;
    }



    public void setSyncindex(java.lang.Long syncindex)
    {
        this.syncindex = syncindex;
    }



    public java.lang.Long getRelateindex()
    {
        return relateindex;
    }



    public void setRelateindex(java.lang.Long relateindex)
    {
        this.relateindex = relateindex;
    }



    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    }



    public void setObjecttype(java.lang.Integer objecttype)
    {
        this.objecttype = objecttype;
    }



    public java.lang.Long getObjectindex()
    {
        return objectindex;
    }



    public void setObjectindex(java.lang.Long objectindex)
    {
        this.objectindex = objectindex;
    }



    public java.lang.String getObjectid()
    {
        return objectid;
    }



    public void setObjectid(java.lang.String objectid)
    {
        this.objectid = objectid;
    }



    public java.lang.String getElementid()
    {
        return elementid;
    }



    public void setElementid(java.lang.String elementid)
    {
        this.elementid = elementid;
    }



    public java.lang.String getParentid()
    {
        return parentid;
    }



    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }



    public java.lang.Long getTargetindex()
    {
        return targetindex;
    }



    public void setTargetindex(java.lang.Long targetindex)
    {
        this.targetindex = targetindex;
    }



    public java.lang.Integer getStatus()
    {
        return status;
    }



    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }



    public java.lang.Integer getOperresult()
    {
        return operresult;
    }



    public void setOperresult(java.lang.Integer operresult)
    {
        this.operresult = operresult;
    }



    public java.lang.String getPublishtime()
    {
        return publishtime;
    }



    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }



    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }



    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }



    public java.lang.Long getReserve01()
    {
        return reserve01;
    }



    public void setReserve01(java.lang.Long reserve01)
    {
        this.reserve01 = reserve01;
    }



    public java.lang.String getReserve02()
    {
        return reserve02;
    }



    public void setReserve02(java.lang.String reserve02)
    {
        this.reserve02 = reserve02;
    }



    public java.lang.Long getPsyncindex()
    {
        return psyncindex;
    }



    public void setPsyncindex(java.lang.Long psyncindex)
    {
        this.psyncindex = psyncindex;
    }



    public java.lang.Integer getTargettype()
    {
        return targettype;
    }



    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }



    public java.lang.String getTargetname()
    {
        return targetname;
    }



    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }



    public java.lang.Integer getFeetype()
    {
        return feetype;
    }



    public void setFeetype(java.lang.Integer feetype)
    {
        this.feetype = feetype;
    }



    public java.lang.Integer getFeecode()
    {
        return feecode;
    }



    public void setFeecode(java.lang.Integer feecode)
    {
        this.feecode = feecode;
    }



    public java.lang.Integer getFixedfee()
    {
        return fixedfee;
    }



    public void setFixedfee(java.lang.Integer fixedfee)
    {
        this.fixedfee = fixedfee;
    }



    public java.lang.String getDescription()
    {
        return description;
    }



    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }



    public java.lang.String getLicensingwindowstart()
    {
        return licensingwindowstart;
    }



    public void setLicensingwindowstart(java.lang.String licensingwindowstart)
    {
        this.licensingwindowstart = licensingwindowstart;
    }



    public java.lang.String getLicensingwindowend()
    {
        return licensingwindowend;
    }



    public void setLicensingwindowend(java.lang.String licensingwindowend)
    {
        this.licensingwindowend = licensingwindowend;
    }



    public java.lang.String getServicecode()
    {
        return servicecode;
    }



    public void setServicecode(java.lang.String servicecode)
    {
        this.servicecode = servicecode;
    }


    public java.lang.String getProgramid()
    {
        return programid;
    }



    public void setProgramid(java.lang.String programid)
    {
        this.programid = programid;
    }



    public java.lang.String getNamecn()
    {
        return namecn;
    }



    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }



    public java.lang.String getSeriesid()
    {
        return seriesid;
    }



    public void setSeriesid(java.lang.String seriesid)
    {
        this.seriesid = seriesid;
    }



    public java.lang.String getChannelid()
    {
        return channelid;
    }



    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }



    public java.lang.String getChannelname()
    {
        return channelname;
    }



    public void setChannelname(java.lang.String channelname)
    {
        this.channelname = channelname;
    }


    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }



    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }



    public void initRelation()
    {
        this.addRelation("psyncindex","PSYNCINDEX");

	    this.addRelation("serviceid","SERVICEID"); 
        this.addRelation("servicename","SERVICENAME");   
        this.addRelation("createtime","CREATETIME");     
        this.addRelation("feetype","FEETYPE");   
        this.addRelation("feecode","FEECODE");   
        this.addRelation("fixedfee","FIXEDFEE");      
        
        this.addRelation("servicecode","SERVICECODE");   
        
        this.addRelation("description","DESCRIPTION");      
        this.addRelation("licensingwindowstart","LICENSINGWINDOWSTART");      
        this.addRelation("licensingwindowend","LICENSINGWINDOWEND");      
        
        this.addRelation("targettype","TARGETTYPE");   
        this.addRelation("targetname","TARGETNAME");   

        this.addRelation("syncindex","SYNCINDEX");
        this.addRelation("relateindex","RELATEINDEX");
        this.addRelation("objecttype","OBJECTTYPE");  
        this.addRelation("objectindex","OBJECTINDEX");  
        this.addRelation("objectid","OBJECTID");  
        this.addRelation("elementid","ELEMENTID");   
        this.addRelation("parentid","PARENTID");  
        this.addRelation("targetindex","TARGETINDEX");  
        this.addRelation("status","STATUS");
        this.addRelation("operresult","OPERRESULT");         
        this.addRelation("publishtime","PUBLISHTIME");   
        this.addRelation("cancelpubtime","CANCELPUBTIME");   
        this.addRelation("reserve01","RESERVE01");   
        this.addRelation("reserve02","RESERVE02");   
        
        this.addRelation("programid","PROGRAMID");   
        this.addRelation("namecn","NAMECN");   
        this.addRelation("seriesid","SERIESID");   
        this.addRelation("channelid","CHANNELID");   
        this.addRelation("channelname","CHANNELNAME");   

    }	
 
}

package com.zte.cms.content.relation.dao;

import java.util.List;

import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsContentRelationDAO
{
    /**
     * 新增CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DAOException dao异常
     */
    public void insertCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException;

    /**
     * 根据主键更新CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DAOException dao异常
     */
    public void updateCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException;

    /**
     * 根据主键删除CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DAOException dao异常
     */
    public void deleteCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException;

    /**
     * 根据主键查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @return 满足条件的CmsContentRelation对象
     * @throws DAOException dao异常
     */
    public CmsContentRelation getCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException;

    /**
     * 根据条件查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @return 满足条件的CmsContentRelation对象集
     * @throws DAOException dao异常
     */
    public List<CmsContentRelation> getCmsContentRelationByCond(CmsContentRelation cmsContentRelation)
            throws DAOException;

    /**
     * 根据条件分页查询CmsContentRelation对象，作为查询条件的参数
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsContentRelation对象，作为查询条件的参数
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

    public PageInfo pageInfoQuery2(CmsContentRelation cmsContentRelation, int start, int pageSize) throws DAOException;

    public PageInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQuery(String sql, String sqlcount, int start, int pageSize) throws DAOException;

    public List<CmsContentRelation> querySelectedContentList(CmsContentRelation cmsContentRelation) throws DAOException;

    public List<CmsContentRelationInfo> pageInfoQueryList(String sql, String sqlcount) throws DAOException;
}
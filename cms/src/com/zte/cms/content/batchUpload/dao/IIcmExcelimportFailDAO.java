package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.model.IcmExcelimportFail;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmExcelimportFailDAO
{
    /**
     * 新增IcmExcelimportFail对象
     * 
     * @param icmExcelimportFail IcmExcelimportFail对象
     * @throws DAOException dao异常
     */
    public void insertIcmExcelimportFail(IcmExcelimportFail icmExcelimportFail) throws DAOException;

    /**
     * 根据条件删除IcmExcelimportFail对象，作为查询条件的参数
     * 
     * @param icmExcelimportFail IcmExcelimportFail对象
     * @throws DAOException dao异常
     */
    public void deleteIcmExcelimportFailByCond(IcmExcelimportFail icmExcelimportFail) throws DAOException;

    /**
     * 根据条件查询IcmExcelimportFail对象
     * 
     * @param icmExcelimportFail IcmExcelimportFail对象
     * @return 满足条件的IcmExcelimportFail对象集
     * @throws DAOException dao异常
     */
    public List<IcmExcelimportFail> getIcmExcelimportFailByCond(IcmExcelimportFail icmExcelimportFail)
            throws DAOException;

    /**
     * 根据条件分页查询IcmExcelimportFail对象，作为查询条件的参数
     * 
     * @param icmExcelimportFail IcmExcelimportFail对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmExcelimportFail对象，作为查询条件的参数
     * 
     * @param icmExcelimportFail IcmExcelimportFail对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
package com.zte.ismp.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.NumberFormats;

import org.apache.tools.zip.ZipOutputStream;

public class ExportToExcel
{

    private final int MAXROWNUM = 5000;

    /**
     * 数据分Excel文件导出并打包成zip文件 该方法适用于数据量小的情况 注意：数据量足够大的情况下，map对象的数据列表很消耗内存，请谨慎使用
     * 
     * @param dataList 导出数据列表，map对象格式
     * @param headList excel表头列表，headList的长度应等于dataList中HashMap的key值个数
     * @param maxRowNum 每个excel文件一个sheet，每个sheet中数据导出最大行数，
     *            默认5000行，实际情况需要根据导出的列数决定，如果列数过多，那么最大行数就不能设置过大，不然每导出1个excel文件会消耗很大的系统资源
     * @param xlsName 用于excel表文件名称、sheet名称、zip包名称， zip包名是中文，需要在tomcat的server.xml中<Connector>节点配置URIEncoding= "UTF-8"
     * @param localPath 本地路径，当前应用的路径
     * @param relatePath 相对路径，相对于本地路径，存放导出文件的路径
     * @return returnStr zip包全路径
     * @throws Exception
     */
    public String exportExcelZipByMap(List<HashMap<String, String>> dataList, List<ExcelHead> headList, int maxRowNum,
            String xlsName, String localPath, String relatePath) throws Exception
    {
        // 返回值
        String returnStr = null;

        // 设定每个Excel文件导出的最大行数
        if (maxRowNum <= 0)
        {
            maxRowNum = MAXROWNUM;
        }

        // excel文件对象
        WritableWorkbook workbook = null;

        try
        {
            // 入参有效性校验
            // 数据总记录数
            int dataListLen = 0;
            if (dataList == null || dataList.size() == 0)
            {
                throw new Exception("dataList is null or size is 0");
            }
            else
            {
                dataListLen = dataList.size();
            }
            // excel有效列数
            int headListLen = 0;
            if (headList == null || headList.size() == 0)
            {
                throw new Exception("headList is null or size is 0");
            }
            else
            {
                headListLen = headList.size();
            }

            if (xlsName == null || "".equals(xlsName))
            {
                throw new Exception("xlsName is null");
            }
            if (localPath == null || "".equals(localPath))
            {
                throw new Exception("localPath is null");
            }
            if (relatePath == null || "".equals(relatePath))
            {
                throw new Exception("relatePath is null");
            }

            // 本次调用导出循环次数
            int cycleTimes = dataListLen / maxRowNum;
            if (dataListLen % maxRowNum != 0)
            {
                cycleTimes += 1;
            }
            // 当前时间
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名

            localPath = localPath.replace("//", "/");

            relatePath = relatePath + "/" + currTime + "/";
            String realPath = localPath + "/" + relatePath;
            realPath = realPath.replace("//", "/");

            // 临时文件对象，用于校验realPath路径中的文件夹是否存在
            File tempFile = new File(realPath);
            if (!tempFile.isDirectory())
            {
                // 目录不存在，则创建
                tempFile.mkdirs();
            }

            String fileName = null; // 不含路径文件名
            String fileAllName = null; // 含路径文件名

            // sheet对象
            WritableSheet sheet = null;

            // 标题行格式对象
            WritableCellFormat wcf_head = null;
            // 内容行格式，文本格式
            WritableCellFormat wcf_content = null;

            // sheet 中行记录对象
            Label label = null;
            // 行记录对象
            HashMap<String, String> hashMap = null;
            // 列表头对象
            ExcelHead excelHead = null;
            // 表头对象字段
            String headField = null;
            // 表头名称
            String headName = null;

            List<HashMap<String, String>> subDataList = null;

            int fromIndex = 0;
            int toIndex = 0;
            for (int i = 0; i < cycleTimes; i++)
            {
                // 获取截取子列表的截止index
                toIndex = (i + 1) * maxRowNum;
                toIndex = toIndex < dataListLen ? toIndex : dataListLen;

                // 获取本次循环需要导出到excel的数据
                subDataList = dataList.subList(fromIndex, toIndex);

                fileName = xlsName + currTime + "_" + (i + 1) + ".xls";
                fileAllName = realPath + "/" + fileName;
                fileAllName.replace("//", "/");
                workbook = Workbook.createWorkbook(new File(fileAllName));
                sheet = workbook.createSheet(xlsName, 0); // 默认每个生成的excel均为一个sheet
                // 标题行格式
                wcf_head = new WritableCellFormat();
                // 标题行居中，边框
                wcf_head.setAlignment(jxl.format.Alignment.CENTRE);
                wcf_head.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                wcf_head.setBackground(Colour.YELLOW);
                // 内容行格式，文本格式
                wcf_content = new WritableCellFormat(NumberFormats.TEXT);

                // sheet插入表头
                for (int m = 0; m < headListLen; m++)
                {
                    excelHead = headList.get(m);
                    headName = excelHead.getHeadName();

                    label = new Label(m, 0, headName, wcf_head);
                    sheet.addCell(label);
                }

                for (int j = 0; j < maxRowNum && j < subDataList.size(); j++)
                {
                    // 取得行记录的hashmap对象
                    hashMap = subDataList.get(j);
                    for (int k = 0; k < headListLen; k++)
                    {
                        excelHead = headList.get(k);
                        headField = excelHead.getHeadField();

                        // 通过headField从hashMap中取得实际数据
                        // wcf_content 单元格文本格式
                        label = new Label(k, j + 1, trans(hashMap.get(headField)), wcf_content);
                        sheet.addCell(label);
                    }
                }

                // 初始化fromIndex
                fromIndex = toIndex;

                // 关闭excel输出
                workbook.write();
                workbook.close();
            }

            // 打包xls文件为zip包
            String zipname = xlsName + currTime + ".zip";
            zipFile(realPath, zipname);
            returnStr = relatePath + "/" + zipname;
            returnStr.replace("//", "/");
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 数据分Excel文件导出并打包成zip文件 该方法适用于数据量小的情况
     * 
     * @param dataList 导出数据列表，list中的对象是POJO——简单java对象，即list中对象需要符合javabean规则，带有属性的get set方法
     * @param headList excel表头列表，headList的长度应小于等于dataList中对象的属性个数 且表头对象的headField属性值必须是dataList列表中对象的属性字段
     * @param maxRowNum 每个excel文件一个sheet，每个sheet中数据导出最大行数，
     *            默认5000行，实际情况需要根据导出的列数决定，如果列数过多，那么最大行数就不能设置过大，不然每导出1个excel文件会消耗很大的系统资源
     * @param xlsName 用于excel表文件名称、sheet名称、zip包名称， zip包名是中文，需要在tomcat的server.xml中<Connector>节点配置URIEncoding= "UTF-8"
     * @param localPath 本地路径，当前应用的路径
     * @param relatePath 相对路径，相对于本地路径，存放导出文件的路径
     * @return returnStr zip包全路径
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public String exportExcelZipByObject(List<Object> dataList, String className, List<ExcelHead> headList,
            int maxRowNum, String xlsName, String localPath, String relatePath) throws Exception
    {
        // 返回值
        String returnStr = null;

        // 设定每个Excel文件导出的最大行数
        if (maxRowNum <= 0)
        {
            maxRowNum = MAXROWNUM;
        }

        // excel文件对象
        WritableWorkbook workbook = null;

        try
        {
            // 入参有效性校验
            // 数据总记录数
            int dataListLen = 0;
            if (dataList == null || dataList.size() == 0)
            {
                throw new Exception("dataList is null or size is 0");
            }
            else
            {
                dataListLen = dataList.size();
            }

            // 对象classname
            if (className == null || "".equals(className.trim()))
            {
                throw new Exception("className is null");
            }

            // excel有效列数
            int headListLen = 0;
            if (headList == null || headList.size() == 0)
            {
                throw new Exception("headList is null or size is 0");
            }
            else
            {
                headListLen = headList.size();
            }

            if (xlsName == null || "".equals(xlsName))
            {
                throw new Exception("xlsName is null");
            }
            if (localPath == null || "".equals(localPath))
            {
                throw new Exception("localPath is null");
            }
            if (relatePath == null || "".equals(relatePath))
            {
                throw new Exception("relatePath is null");
            }

            // 数据列表中对象类型
            Class classType = Class.forName(className);
            // 对象方法引用
            Method method = null;
            String getMethodName = null;
            String retValue = null;

            // 导出循环次数
            int cycleTimes = dataListLen / maxRowNum;
            if (dataListLen % maxRowNum != 0)
            {
                cycleTimes += 1;
            }
            // 当前时间
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名

            localPath = localPath.replace("//", "/");

            relatePath = relatePath + "/" + currTime + "/";
            String realPath = localPath + "/" + relatePath;
            realPath = realPath.replace("//", "/");

            // 临时文件对象，用于校验realPath路径中的文件夹是否存在
            File tempFile = new File(realPath);
            if (!tempFile.isDirectory())
            {
                // 目录不存在，则创建
                tempFile.mkdirs();
            }

            String fileName = null; // 不含路径文件名
            String fileAllName = null; // 含路径文件名

            // sheet对象
            WritableSheet sheet = null;

            // 标题行格式对象
            WritableCellFormat wcf_head = null;
            // 内容行格式，文本格式
            WritableCellFormat wcf_content = null;

            // sheet 中行记录对象
            Label label = null;
            // 行记录对象
            Object obj = null;
            // 列表头对象
            ExcelHead excelHead = null;
            // 表头对象字段
            String headField = null;
            // 表头名称
            String headName = null;

            List<Object> subDataList = null;

            int fromIndex = 0;
            int toIndex = 0;
            for (int i = 0; i < cycleTimes; i++)
            {
                // 获取截取子列表的截止index
                toIndex = (i + 1) * maxRowNum;
                toIndex = toIndex < dataListLen ? toIndex : dataListLen;

                // 获取本次循环需要导出到excel的数据
                subDataList = dataList.subList(fromIndex, toIndex);

                fileName = xlsName + currTime + "_" + (i + 1) + ".xls";
                fileAllName = realPath + "/" + fileName;
                fileAllName.replace("//", "/");
                workbook = Workbook.createWorkbook(new File(fileAllName));
                sheet = workbook.createSheet(xlsName, 0); // 默认每个生成的excel均为一个sheet
                // 标题行格式
                wcf_head = new WritableCellFormat();
                // 标题行居中，边框
                wcf_head.setAlignment(jxl.format.Alignment.CENTRE);
                wcf_head.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                wcf_head.setBackground(Colour.YELLOW);
                // 内容行格式，文本格式
                wcf_content = new WritableCellFormat(NumberFormats.TEXT);

                // sheet插入表头
                for (int m = 0; m < headListLen; m++)
                {
                    excelHead = headList.get(m);
                    headName = excelHead.getHeadName();

                    label = new Label(m, 0, headName, wcf_head);
                    sheet.addCell(label);
                }

                for (int j = 0; j < maxRowNum && j < subDataList.size(); j++)
                {
                    // 取得行记录的hashmap对象
                    obj = subDataList.get(j);
                    for (int k = 0; k < headListLen; k++)
                    {
                        excelHead = headList.get(k);
                        headField = excelHead.getHeadField();

                        // 获取对象类型中get方法名称
                        getMethodName = getMethodName(headField);
                        // 使用反射机制通过headField从对象中取得实际数据
                        method = classType.getMethod(getMethodName, new Class[0]);
                        // 方法返回值
                        retValue = trans(method.invoke(obj, new Object[0]));
                        // wcf_content 单元格文本格式
                        label = new Label(k, j + 1, retValue, wcf_content);
                        sheet.addCell(label);
                    }
                }

                // 初始化fromIndex
                fromIndex = toIndex;

                // 关闭excel输出
                workbook.write();
                workbook.close();
            }

            // 打包xls文件为zip包
            String zipname = xlsName + currTime + ".zip";
            zipFile(realPath, zipname);
            returnStr = relatePath + "/" + zipname;
            returnStr.replace("//", "/");
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 数据分Excel文件导出至指定文件目录 注意：数据量足够大的情况下，map对象的数据列表很消耗内存，请谨慎使用
     * 
     * @param dataList 导出数据列表，map对象格式
     * @param headList excel表头列表，headList的长度应等于dataList中HashMap的key值个数
     * @param maxRowNum 每个excel文件一个sheet，每个sheet中数据导出最大行数，
     *            默认5000行，实际情况需要根据导出的列数决定，如果列数过多，那么最大行数就不能设置过大，不然每导出1个excel文件会消耗很大的系统资源
     * @param xlsName 用于excel表文件名称、sheet名称
     * @param startNo 用于给导出的excel文件编号，导出的excel文件名格式：xlsName + "_" + startNo，一次调用该方法导出多个excel文件startNo值会+1
     * @param xlsPath excel文件导出的指定文件路径
     * @return endNo 导出结束文件序号
     * @throws Exception
     */
    public int exportExcelByMap(List<HashMap<String, String>> dataList, List<ExcelHead> headList, int maxRowNum,
            String xlsName, int startNo, String xlsPath) throws Exception
    {
        // 返回值
        int endNo = 0;

        // 设定每个Excel文件导出的最大行数
        if (maxRowNum <= 0)
        {
            maxRowNum = MAXROWNUM;
        }

        // excel文件对象
        WritableWorkbook workbook = null;

        try
        {
            // 入参有效性校验
            // 数据总记录数
            int dataListLen = 0;
            if (dataList == null || dataList.size() == 0)
            {
                throw new Exception("dataList is null or size is 0");
            }
            else
            {
                dataListLen = dataList.size();
            }

            // excel有效列数
            int headListLen = 0;
            if (headList == null || headList.size() == 0)
            {
                throw new Exception("headList is null or size is 0");
            }
            else
            {
                headListLen = headList.size();
            }

            if (xlsName == null || "".equals(xlsName))
            {
                throw new Exception("xlsName is null");
            }
            if (xlsPath == null || "".equals(xlsPath))
            {
                throw new Exception("xlsPath is null");
            }

            // 导出循环次数
            int cycleTimes = dataListLen / maxRowNum;
            if (dataListLen % maxRowNum != 0)
            {
                cycleTimes += 1;
            }
            // 当前时间
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名

            xlsPath = xlsPath.replace("//", "/");

            // 临时文件对象，用于校验filePath路径中的文件夹是否存在
            File tempFile = new File(xlsPath);
            if (!tempFile.isDirectory())
            {
                // 目录不存在，则创建
                tempFile.mkdirs();
            }

            String fileName = null; // 不含路径文件名
            String fileAllName = null; // 含路径文件名

            // sheet对象
            WritableSheet sheet = null;

            // 标题行格式对象
            WritableCellFormat wcf_head = null;
            // 内容行格式，文本格式
            WritableCellFormat wcf_content = null;

            // sheet 中行记录对象
            Label label = null;
            // 行记录对象
            HashMap<String, String> hashMap = null;
            // 列表头对象
            ExcelHead excelHead = null;
            // 表头对象字段
            String headField = null;
            // 表头名称
            String headName = null;

            List<HashMap<String, String>> subDataList = null;

            int fromIndex = 0;
            int toIndex = 0;
            int i = 0;
            for (i = 0; i < cycleTimes; i++)
            {
                // 获取截取子列表的截止index
                toIndex = (i + 1) * maxRowNum;
                toIndex = toIndex < dataListLen ? toIndex : dataListLen;

                // 获取本次循环需要导出到excel的数据
                subDataList = dataList.subList(fromIndex, toIndex);

                fileName = xlsName + currTime + "_" + (startNo + i) + ".xls";
                fileAllName = xlsPath + "/" + fileName;
                fileAllName.replace("//", "/");
                workbook = Workbook.createWorkbook(new File(fileAllName));
                sheet = workbook.createSheet(xlsName, 0); // 默认每个生成的excel均为一个sheet
                // 标题行格式
                wcf_head = new WritableCellFormat();
                // 标题行居中，边框
                wcf_head.setAlignment(jxl.format.Alignment.CENTRE);
                wcf_head.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                wcf_head.setBackground(Colour.YELLOW);
                // 内容行格式，文本格式
                wcf_content = new WritableCellFormat(NumberFormats.TEXT);

                // sheet插入表头
                for (int m = 0; m < headListLen; m++)
                {
                    excelHead = headList.get(m);
                    headName = excelHead.getHeadName();

                    label = new Label(m, 0, headName, wcf_head);
                    sheet.addCell(label);
                }

                for (int j = 0; j < maxRowNum && j < subDataList.size(); j++)
                {
                    // 取得行记录的hashmap对象
                    hashMap = subDataList.get(j);
                    for (int k = 0; k < headListLen; k++)
                    {
                        excelHead = headList.get(k);
                        headField = excelHead.getHeadField();

                        // 通过headField从hashMap中取得实际数据
                        // wcf_content 单元格文本格式
                        label = new Label(k, j + 1, trans(hashMap.get(headField)), wcf_content);
                        sheet.addCell(label);
                    }
                }

                // 初始化fromIndex
                fromIndex = toIndex;

                // 关闭excel输出
                workbook.write();
                workbook.close();
            }

            endNo = startNo + i;
        }
        catch (Exception e)
        {
            throw e;
        }

        return endNo;
    }

    /**
     * 数据分Excel文件导出至指定文件目录 注意：数据量足够大的情况下，map对象的数据列表很消耗内存，请谨慎使用
     * 
     * @param dataList 导出数据列表，list中的对象是POJO——简单java对象，即list中对象需要符合javabean规则，带有属性的get set方法
     * @param className dataList中对象的类型名称，需包含类的包路径
     * @param headList excel表头列表，headList的长度应小于等于dataList中对象的属性个数 且表头对象的headField属性值必须是dataList列表中对象的属性字段
     * @param maxRowNum 每个excel文件一个sheet，每个sheet中数据导出最大行数，
     *            默认5000行，实际情况需要根据导出的列数决定，如果列数过多，那么最大行数就不能设置过大，不然每导出1个excel文件会消耗很大的系统资源
     * @param xlsName 用于excel表文件名称、sheet名称
     * @param startNo 用于给导出的excel文件编号，导出的excel文件名格式：xlsName + "_" + startNo，一次调用该方法导出多个excel文件startNo值会+1
     * @param xlsPath excel文件导出的指定文件路径
     * @return endNo 导出结束文件序号
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public int exportExcelByOjbect(List<Object> dataList, String className, List<ExcelHead> headList, int maxRowNum,
            String xlsName, int startNo, String xlsPath) throws Exception
    {
        // 返回值
        int endNo = 0;

        // 设定每个Excel文件导出的最大行数
        if (maxRowNum <= 0)
        {
            maxRowNum = MAXROWNUM;
        }

        // excel文件对象
        WritableWorkbook workbook = null;

        try
        {
            // 入参有效性校验
            // 数据总记录数
            int dataListLen = 0;
            if (dataList == null || dataList.size() == 0)
            {
                throw new Exception("dataList is null or size is 0");
            }
            else
            {
                dataListLen = dataList.size();
            }

            // 对象classname
            if (className == null || "".equals(className.trim()))
            {
                throw new Exception("className is null");
            }

            // excel有效列数
            int headListLen = 0;
            if (headList == null || headList.size() == 0)
            {
                throw new Exception("headList is null or size is 0");
            }
            else
            {
                headListLen = headList.size();
            }

            if (xlsName == null || "".equals(xlsName.trim()))
            {
                throw new Exception("xlsName is null");
            }
            if (xlsPath == null || "".equals(xlsPath.trim()))
            {
                throw new Exception("xlsPath is null");
            }

            // 导出循环次数
            int cycleTimes = dataListLen / maxRowNum;
            if (dataListLen % maxRowNum != 0)
            {
                cycleTimes += 1;
            }

            // 数据列表中对象类型
            Class classType = Class.forName(className);
            // 对象方法引用
            Method method = null;
            String getMethodName = null;
            String retValue = null;

            // 当前时间，用于文件命名
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名

            xlsPath = xlsPath.replace("//", "/");

            // 临时文件对象，用于校验filePath路径中的文件夹是否存在
            File tempFile = new File(xlsPath);
            if (!tempFile.isDirectory())
            {
                // 目录不存在，则创建
                tempFile.mkdirs();
            }

            String fileName = null; // 不含路径文件名
            String fileAllName = null; // 含路径文件名

            // sheet对象
            WritableSheet sheet = null;

            // 标题行格式对象
            WritableCellFormat wcf_head = null;
            // 内容行格式，文本格式
            WritableCellFormat wcf_content = null;

            // sheet 中行记录对象
            Label label = null;
            // 行记录对象
            Object obj = null;
            // 列表头对象
            ExcelHead excelHead = null;
            // 表头对象字段
            String headField = null;
            // 表头名称
            String headName = null;

            List<Object> subDataList = null;

            int fromIndex = 0;
            int toIndex = 0;
            int i = 0;
            for (i = 0; i < cycleTimes; i++)
            {
                // 获取截取子列表的截止index
                toIndex = (i + 1) * maxRowNum;
                toIndex = toIndex < dataListLen ? toIndex : dataListLen;

                // 获取本次循环需要导出到excel的数据
                subDataList = dataList.subList(fromIndex, toIndex);

                fileName = xlsName + currTime + "_" + (startNo + i) + ".xls";
                fileAllName = xlsPath + "/" + fileName;
                fileAllName.replace("//", "/");
                workbook = Workbook.createWorkbook(new File(fileAllName));
                sheet = workbook.createSheet(xlsName, 0); // 默认每个生成的excel均为一个sheet

                // 标题行格式
                wcf_head = new WritableCellFormat();
                // 标题行居中，边框
                wcf_head.setAlignment(jxl.format.Alignment.CENTRE);
                wcf_head.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
                wcf_head.setBackground(Colour.YELLOW);
                // 内容行格式，文本格式
                wcf_content = new WritableCellFormat(NumberFormats.TEXT);

                // sheet插入表头
                for (int m = 0; m < headListLen; m++)
                {
                    excelHead = headList.get(m);
                    headName = excelHead.getHeadName();

                    label = new Label(m, 0, headName, wcf_head);
                    sheet.addCell(label);
                }

                for (int j = 0; j < maxRowNum && j < subDataList.size(); j++)
                {
                    // 取得行记录的hashmap对象
                    obj = subDataList.get(j);
                    for (int k = 0; k < headListLen; k++)
                    {
                        excelHead = headList.get(k);
                        headField = excelHead.getHeadField();

                        // 获取对象类型中get方法名称
                        getMethodName = getMethodName(headField);
                        // 使用反射机制通过headField从对象中取得实际数据
                        method = classType.getMethod(getMethodName, new Class[0]);
                        // 方法返回值
                        retValue = trans(method.invoke(obj, new Object[0]));
                        // wcf_content 单元格文本格式
                        label = new Label(k, j + 1, retValue, wcf_content);
                        sheet.addCell(label);
                    }
                }

                // 初始化fromIndex
                fromIndex = toIndex;

                // 关闭excel输出
                workbook.write();
                workbook.close();
            }

            endNo = startNo + i;
        }
        catch (Exception e)
        {
            throw e;
        }

        return endNo;
    }

    /**
     * 根据字段名称返回get方法名称
     * 
     * @param headField
     * @return
     * @throws Exception
     */
    private String getMethodName(String headField) throws Exception
    {
        String methodName = null;
        try
        {
            if (headField == null && "".equals(headField.trim()))
            {
                throw new Exception("headField is null");
            }

            // 得到headField值的get方法名称
            methodName = "get" + headField.substring(0, 1).toUpperCase() + headField.substring(1);
        }
        catch (Exception e)
        {
            throw e;
        }

        return methodName;
    }

    /**
     * 打包文件
     * 
     * @throws IOException
     */
    private void zipFile(String filePath, String zipName) throws IOException
    {

        File file = new File(filePath);
        if (!file.isDirectory())
        {
            throw new IllegalArgumentException("Not a directory:  " + filePath);
        }

        String[] entries = file.list();
        byte[] buffer = new byte[4096]; // Create a buffer for copying
        int bytesRead;

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(filePath + "/" + zipName));

        for (int i = 0; i < entries.length; i++)
        {
            File f = new File(file, entries[i]);

            FileInputStream in = new FileInputStream(f);
            org.apache.tools.zip.ZipEntry entry = new org.apache.tools.zip.ZipEntry(f.getName());
            out.putNextEntry(entry);
            while ((bytesRead = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytesRead);
            }
            in.close();
        }
        out.close();
    }

    /**
     * 将空对象转换为""
     * 
     * @param o Object
     * @return
     */
    public String trans(Object o)
    {
        if (o == null)
        {
            return "";
        }
        else
        {
            return o.toString().trim();
        }
    }

}

package com.zte.cms.service.service;

import com.zte.cms.service.model.Service;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceContentBindDS
{
    /**
     * 查询服务和内容的绑定关系列表
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 查询满足绑定条件的内容，用于服务和内容的绑定
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    
    /**
     * 查询服务和栏目的关联关系列表
     * 
     * @param service Service
     * @param start int
     * @param pageSize int 
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return PageInfo
     * @throws DAOException DAOException
     */
    public TableDataInfo serviceCntMapQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
    throws DomainServiceException;
    
    //返回是否绑定了内容
    public boolean isBingContent(Service service ) throws DomainServiceException;

}

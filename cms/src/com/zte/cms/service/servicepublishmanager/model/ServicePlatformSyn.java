
  package com.zte.cms.service.servicepublishmanager.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServicePlatformSyn extends DynamicBaseObject
{
    
	//service表中的字段
	private java.lang.String serviceid;
    private java.lang.String servicename;
    private java.lang.String  createtime;
    private java.lang.String  createtime1;
    private java.lang.String  createtime2;


	//cnt_platform_sync表中的字段
	private java.lang.Long syncindex;
	private java.lang.Integer objecttype;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String elementid;
	private java.lang.String parentid;
	private java.lang.Integer platform;
	private java.lang.Integer status;
    private java.lang.String publishtime;
    private java.lang.String publishtime1;
    private java.lang.String publishtime2;
    private java.lang.String cancelpubtime;
    private java.lang.String cancelpubtime1;
    private java.lang.String cancelpubtime2;
	private java.lang.Integer isfiledelete;
	private java.lang.Long reserve01;
	private java.lang.String reserve02;
	
	//cms_program表中的字段
	private java.lang.String programid;
	private java.lang.String namecn;
	
    //cms_series表中的字段
    private java.lang.String seriesid;
   //cms_channel表中的字段
    private java.lang.String channelid;
    private java.lang.String channelname;

    public java.lang.String getServiceid()
    {
        return serviceid;
    }



    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }



    public java.lang.String getServicename()
    {
        return servicename;
    }



    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }



    public java.lang.String getCreatetime()
    {
        return createtime;
    }



    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }



    public java.lang.Long getSyncindex()
    {
        return syncindex;
    }



    public void setSyncindex(java.lang.Long syncindex)
    {
        this.syncindex = syncindex;
    }



    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    }



    public void setObjecttype(java.lang.Integer objecttype)
    {
        this.objecttype = objecttype;
    }



    public java.lang.Long getObjectindex()
    {
        return objectindex;
    }



    public void setObjectindex(java.lang.Long objectindex)
    {
        this.objectindex = objectindex;
    }



    public java.lang.String getObjectid()
    {
        return objectid;
    }



    public void setObjectid(java.lang.String objectid)
    {
        this.objectid = objectid;
    }



    public java.lang.String getElementid()
    {
        return elementid;
    }



    public void setElementid(java.lang.String elementid)
    {
        this.elementid = elementid;
    }



    public java.lang.String getParentid()
    {
        return parentid;
    }



    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }



    public java.lang.Integer getPlatform()
    {
        return platform;
    }



    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }



    public java.lang.Integer getStatus()
    {
        return status;
    }



    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }



    public java.lang.String getPublishtime()
    {
        return publishtime;
    }



    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }



    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }



    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }



    public java.lang.Integer getIsfiledelete()
    {
        return isfiledelete;
    }



    public void setIsfiledelete(java.lang.Integer isfiledelete)
    {
        this.isfiledelete = isfiledelete;
    }



    public java.lang.Long getReserve01()
    {
        return reserve01;
    }



    public void setReserve01(java.lang.Long reserve01)
    {
        this.reserve01 = reserve01;
    }



    public java.lang.String getReserve02()
    {
        return reserve02;
    }



    public void setReserve02(java.lang.String reserve02)
    {
        this.reserve02 = reserve02;
    }



    public java.lang.String getCreatetime1()
    {
        return createtime1;
    }



    public void setCreatetime1(java.lang.String createtime1)
    {
        this.createtime1 = createtime1;
    }



    public java.lang.String getCreatetime2()
    {
        return createtime2;
    }



    public void setCreatetime2(java.lang.String createtime2)
    {
        this.createtime2 = createtime2;
    }



    public java.lang.String getPublishtime1()
    {
        return publishtime1;
    }



    public void setPublishtime1(java.lang.String publishtime1)
    {
        this.publishtime1 = publishtime1;
    }



    public java.lang.String getPublishtime2()
    {
        return publishtime2;
    }



    public void setPublishtime2(java.lang.String publishtime2)
    {
        this.publishtime2 = publishtime2;
    }



    public java.lang.String getCancelpubtime1()
    {
        return cancelpubtime1;
    }



    public void setCancelpubtime1(java.lang.String cancelpubtime1)
    {
        this.cancelpubtime1 = cancelpubtime1;
    }



    public java.lang.String getCancelpubtime2()
    {
        return cancelpubtime2;
    }



    public void setCancelpubtime2(java.lang.String cancelpubtime2)
    {
        this.cancelpubtime2 = cancelpubtime2;
    }


    public java.lang.String getProgramid()
    {
        return programid;
    }



    public void setProgramid(java.lang.String programid)
    {
        this.programid = programid;
    }



    public java.lang.String getNamecn()
    {
        return namecn;
    }



    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }
    


    public java.lang.String getSeriesid()
    {
        return seriesid;
    }



    public void setSeriesid(java.lang.String seriesid)
    {
        this.seriesid = seriesid;
    }



    public java.lang.String getChannelid()
    {
        return channelid;
    }



    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }



    public java.lang.String getChannelname()
    {
        return channelname;
    }



    public void setChannelname(java.lang.String channelname)
    {
        this.channelname = channelname;
    }



    public void initRelation()
    {     
        this.addRelation("serviceid","SERVICEID"); 
        this.addRelation("servicename","SERVICENAME");   
        this.addRelation("createtime","CREATETIME");   
        this.addRelation("createtime1","CREATETIME1");   
        this.addRelation("createtime2","CREATETIME2");   
     
        this.addRelation("syncindex","SYNCINDEX");   
        this.addRelation("objecttype","OBJECTTYPE");   
        this.addRelation("platform","PLATFORM");   
        this.addRelation("elementid","ELEMENTID");   
        this.addRelation("parentid","PARENTID");  
        this.addRelation("platform","PLATFORM");  
        this.addRelation("status","STATUS");   
        this.addRelation("publishtime","PUBLISHTIME");   
        this.addRelation("publishtime1","PUBLISHTIME1");   
        this.addRelation("publishtime2","PUBLISHTIME2");   
        this.addRelation("cancelpubtime","CANCELPUBTIME");   
        this.addRelation("cancelpubtime1","CANCELPUBTIME1");   
        this.addRelation("cancelpubtime2","CANCELPUBTIME2");   
        this.addRelation("isfiledelete","ISFILEDELETE");   
        this.addRelation("reserve01","RESERVE01");   
        this.addRelation("reserve02","RESERVE02");   
  
        this.addRelation("programid","PROGRAMID"); 
        this.addRelation("namecn","NAMECN");   
        
        this.addRelation("seriesid","SERIESID");   
        
        this.addRelation("channelid","CHANNELID"); 
        this.addRelation("channelname","CHANNELNAME");     
        
    }	


}

package com.zte.cms.operlimit.service;

import java.util.List;
import java.util.Map;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.operlimit.model.OperRole;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IOperRoleDS
{
    
    public List getOperRole() throws DomainServiceException;
    
    public List getUserOperFunById(Integer operGrpId) throws DomainServiceException;
    
    public boolean insertOperRole(Map operRoleValues) throws DomainServiceException;
    
    
}
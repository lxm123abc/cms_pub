package com.zte.cms.check;

import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.device.customtimer.service.IJob;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.cms.storageManage.service.ICmsStoragemanageDS;
import com.zte.cms.storageArea.service.ICmsStorageareaDS;
import com.zte.cms.storageArea.model.CmsStoragearea;

/**
 * <p>
 * 文件名称: CycleJob.java
 * </p>
 * <p>
 * 文件描述: 定时任务工作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 定时任务具体工作类
 * </p>
 * <p>
 * 其他说明: 供定时任务发起时调用
 * </p>
 * <p>
 * 完成日期：2011年1月20日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 黄杰弟
 */
public class CycleJob implements IJob
{

    private Log log = SSBBus.getLog(getClass());

    private ICmsStoragemanageDS cmsStoragemanagerDS;

    private ICmsStorageareaDS cmsStoreageareaDS;

    // public ICmsStoragemanageDS getCmsStoragemanagerDS() {
    // return cmsStoragemanagerDS;
    // }
    //
    // public void setCmsStoragemanagerDS(ICmsStoragemanageDS cmsStoragemanagerDS) {
    // this.cmsStoragemanagerDS = cmsStoragemanagerDS;
    // }
    //	
    // public ICmsStorageareaDS getCmsStoreageareaDS() {
    // return cmsStoreageareaDS;
    // }
    //
    // public void setCmsStoreageareaDS(ICmsStorageareaDS cmsStoreageareaDS) {
    // this.cmsStoreageareaDS = cmsStoreageareaDS;
    // }

    /**
     * 执行定时任务的工作
     */
    public void myExecute(int firstornot) throws JobExecutionException {

        try {
            cmsStoragemanagerDS = (ICmsStoragemanageDS) SSBBus.findDomainService("cmsStoragemanageDS");
            cmsStoreageareaDS = (ICmsStorageareaDS) SSBBus.findDomainService("cmsStorageareaDS");
            log.debug("start cms_check timer excute................");
            System.out.println("start cms_check timer excute................in myexcute");
            // if(firstornot == 0){
            // upDateDevice();
            // }
            checkDevice();
            caculateStorage();
            System.out.println("end cms_check timer excute................in myexcute");
            log.debug("end cms_check timer excute................");
        }
        catch (Exception e)
        {
            // System.out.println("error occur in cms_check timer excute.........");
            log.error("error occur in cms_check timer excute.........");
            log.error(e);
            throw new JobExecutionException(e);
        }
    }

    /**
     * 执行定时任务的工作
     */
    public void execute(JobExecutionContext arg0) throws JobExecutionException
    {

        try
        {
            cmsStoragemanagerDS = (ICmsStoragemanageDS) SSBBus.findDomainService("cmsStoragemanageDS");
            cmsStoreageareaDS = (ICmsStorageareaDS) SSBBus.findDomainService("cmsStorageareaDS");
            log.debug("start cms_check timer excute................");
            System.out.println("start cms_check timer excute................");
            checkDevice();
            caculateStorage();
            System.out.println("end cms_check timer excute................");
            log.debug("end cms_check timer excute................");
        }
        catch (Exception e)
        {
            // System.out.println("error occur in cms_check timer excute.........");
            log.error("error occur in cms_check timer excute.........");
            log.error(e);
            throw new JobExecutionException(e);
        }
    }

    public void interrupt() throws UnableToInterruptJobException
    {
        // nothing
    }

    /**
     * 更新库区信息
     * 
     * @return
     * @throws Exception
     */
    private String caculateStorage() throws JobExecutionException
    {
        String result = "";
        log.debug("start caculateStorage...............");
        // System.out.println("start caculateStorage...............");
        try
        {
            List<CmsStoragemanage> storelist = null;
            List<CmsStoragearea> arealist = null;
            CmsStoragearea condition = new CmsStoragearea();
            int areasize = 0;
            int storesize = 0;
            float totalperc = 0;
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.#");
            Long totalsize = Long.parseLong("0");
            //Long ressize = Long.parseLong("0");

            condition.setNumbackup1(Long.parseLong("0"));

            // 获取库区记录
            arealist = cmsStoreageareaDS.getCmsStorageareaByCond(condition);

            // 有库区记录
            if (arealist != null)
            {
                areasize = arealist.size();

                for (int i = 0; i < areasize; i++)
                {
                    CmsStoragemanage tempcondition = new CmsStoragemanage();
                    CmsStoragearea temparea = arealist.get(i);
                    CmsStoragemanage tempstore = null;
                    tempcondition.setStoragearea(temparea.getAreaid());

                    // 获取库区对应的控件
                    storelist = cmsStoragemanagerDS.getCmsStoragemanageByCond(tempcondition);
                    if (storelist == null)
                    {
                        continue;
                    }
                    totalsize = Long.parseLong("0");
                    //ressize = Long.parseLong("0");
                    float ressizef = 0;
                    totalperc = 0;
                    // 计算空间百分比和设置状态
                    storesize = storelist.size();
                    for (int j = 0; j < storesize; j++)
                    {
                        tempstore = storelist.get(j);

                        totalsize = totalsize + Long.parseLong(tempstore.getStoragecapacity());

                        ressizef = ressizef + Float.parseFloat(tempstore.getCurrentcapacity());

                    }
                    if (storesize > 0)
                    {
                        totalperc = (totalsize.floatValue() - ressizef) * 100 / totalsize.floatValue();

                        totalperc = (float) Double.parseDouble(df.format(totalperc));
                    }

                    temparea.setStoragepercentage(String.valueOf(totalperc));

                    if (totalperc >= (float) Double.parseDouble(temparea.getMonitorthreshold()))
                    {
                        temparea.setAreastatus(0);
                    }
                    else
                    {
                        temparea.setAreastatus(1);
                    }
                    try
                    {
                        cmsStoreageareaDS.updateCmsStoragearea(temparea);
                    }
                    catch (Exception e)
                    {
                        log.error("error occur in caculateStorage:");
                        log.error(e);
                        continue;
                    }
                }

            }
        }
        catch (Exception e)
        {
            // System.out.println("error occured in caculateStorage...........");
            log.error("error occured in caculateStorage...........");
            log.error(e);
            throw new JobExecutionException(e);
        }
        // System.out.println("end caculateStorage.................");
        log.debug("end caculateStorage.................");
        return result;
    }

    private void upDateDevice() throws Exception
    {
        log.debug("now update the device start................");
        // System.out.println("----------------------in upDateDevice function");
        try
        {
            String details = "";
            String rootPath = GlobalConstants.getMountPoint();
            String realPath = "";
            String foldersize = "";
            String childsize = "";
            String tempsize = "";
            List<CmsStoragemanage> firstList = null;
            List<CmsStoragemanage> secondList = null;
            CmsStoragemanage condition = new CmsStoragemanage();
            CmsStoragemanage temp = null;
            CmsStoragemanage secondleveltemp = null;
            int firstlevelsize = 0;
            int secondlevelsize = 0;
            int Gnum = 0;
            long freesize = 0;

            // 更新设备和控件的信息
            condition.setParentindex(Long.parseLong("1"));

            // 获取第一层目录
            firstList = cmsStoragemanagerDS.getCmsStoragemanageByCond(condition);

            if (firstList == null)
            {

                throw new Exception("10000");
            }

            firstlevelsize = firstList.size();
            // System.out.println("&&&&&&&&&&&&&&&&&&&&& firstlevelsize = "+firstlevelsize);
            for (int i = 0; i < firstlevelsize; i++)
            {
                temp = firstList.get(i);

                realPath = rootPath + temp.getStorageid();
                realPath = filterSlashStr(realPath);

                Gnum = checkContainG(realPath);

                // System.out.println("-------------------------- realPath = "+realPath);

                details = exeCmd("df -h " + realPath);

                // System.out.println("-----------------------details="+details);

                if (details == null)
                {
                    continue;
                }
                foldersize = getfreesize(details, Gnum);

                // System.out.println("*******************foldersize = "+foldersize);
                childsize = "0";

                CmsStoragemanage secondcond = new CmsStoragemanage();
                secondcond.setParentindex(temp.getStorageindex());
                secondList = cmsStoragemanagerDS.getCmsStoragemanageByCond(secondcond);

                if (secondList == null)
                {
                    secondlevelsize = 0;
                }
                else
                {
                    secondlevelsize = secondList.size();
                }

                for (int j = 0; j < secondlevelsize; j++)
                {
                    secondleveltemp = secondList.get(j);
                    tempsize = secondleveltemp.getStoragecapacity();

                    try
                    {
                        childsize = String.valueOf(Long.parseLong(childsize) + Long.parseLong(tempsize));
                    }
                    catch (Exception e)
                    {
                        childsize = "0";
                        break;
                    }

                }

                temp.setStoragecapacity(foldersize);

                freesize = Long.parseLong(foldersize) - Long.parseLong(childsize);
                freesize = freesize < 0 ? 0 : freesize;
                temp.setCurrentcapacity(String.valueOf(freesize));
                cmsStoragemanagerDS.updateCmsStoragemanage(temp);

            }
        }
        catch (Exception e)
        {
            log.error("Error occur in updatedevice");
            log.error(e);
        }
        log.debug("update the device job end..................");
    }

    /**
     * 更新设备和控件的存储信息
     * 
     * @return
     * @throws Exception
     */
    private String checkDevice() throws JobExecutionException
    {
        String result = "";
        // System.out.println("start check device....................");
        log.debug("start check device....................");
        try
        {
            String details = "";
            String rootPath = GlobalConstants.getMountPoint();
            String secondlevelPath = "";
            String realPath = "";
            int fistlevelsize = 0;

            List<CmsStoragemanage> firstList = null;
            List<CmsStoragemanage> secondList = null;
            CmsStoragemanage condition = new CmsStoragemanage();
            CmsStoragemanage temp = null;
            CmsStoragemanage secondleveltemp = null;

            // 更新设备和控件的信息
            condition.setParentindex(Long.parseLong("1"));

            // 获取第一层目录
            firstList = cmsStoragemanagerDS.getCmsStoragemanageByCond(condition);

            if (firstList == null)
            {

                throw new Exception("10000");
            }

            fistlevelsize = firstList.size();

            for (int i = 0; i < fistlevelsize; i++)
            {
                temp = firstList.get(i);

                try
                {
                    dealFolder(temp, rootPath, 1);
                }
                catch (Exception e)
                {
                    log.error(e);
                    continue;
                }

                // 根据第一层目录获取以第一层目录为父目录的第二目录list
                secondlevelPath = temp.getStorageid();

                CmsStoragemanage secondcond = new CmsStoragemanage();
                secondcond.setParentindex(temp.getStorageindex());
                secondList = cmsStoragemanagerDS.getCmsStoragemanageByCond(secondcond);

                if (secondList == null)
                {

                    continue;
                }

                // 获取需要查询大小的目录路径
                realPath = rootPath + secondlevelPath + "\\";
                int secondlevelsize = secondList.size();

                // 遍历第二层目录获取目录的大小
                for (int j = 0; j < secondlevelsize; j++)
                {

                    secondleveltemp = secondList.get(j);
                    try
                    {
                        dealFolder(secondleveltemp, realPath, 2);
                    }
                    catch (Exception e)
                    {
                        log.error(e);
                        continue;
                    }
                }
            }

        }
        catch (Exception e)
        {

            log.error("error in check device...........");
            log.error(e);
            throw new JobExecutionException(e);
        }
        // System.out.println("finish check device...............");
        log.debug("finish check device...............");
        return result;
    }

    private void dealFolder(CmsStoragemanage cmsstrmgt, String fatherPath, int level) throws Exception
    {

        Long totalsize = null;
        String realPath = "";
        String details = "";
        int flage = 1;

        totalsize = Long.parseLong(cmsstrmgt.getStoragecapacity());
        // 获取需要查询大小的目录路径
        realPath = fatherPath + cmsstrmgt.getStorageid();
        realPath = filterSlashStr(realPath);
        // 调用shell查询目录大小

        details = exeCmd("du -ms " + realPath);
        // System.out.println("-----------"+realPath+" -------- "+details);
        if (details == null)
        {
            cmsstrmgt.setStatus(0);
            System.out.println("device status change to 0***********1*********");
            cmsStoragemanagerDS.updateCmsStoragemanage(cmsstrmgt);
            return;
        }

        // 获取目录大小的数值，单位kb
        details = details.substring(0, findfirstspace(details));

        // 根据目录大小、设置的总容量，计算可用空间、百分比和设置是否可用
        flage = setProperty(cmsstrmgt, totalsize, details, level);

        // System.out.println("-------Currentcapacity="+cmsstrmgt.getCurrentcapacity()+"
        // Storagecapacity="+cmsstrmgt.getStoragecapacity());
        // 更新目录信息
        if (flage == 0)
        {
            cmsStoragemanagerDS.updateCmsStoragemanage(cmsstrmgt);
        }

    }

    /**
     * 根据目录大小、设置的总容量，计算可用空间、百分比和设置是否可用
     * 
     * @param cmsstrmgt
     * @param totalsize
     * @param details
     * @return 0：目录存在，信息获取成功。 1：目录不存在，信息获取失败
     * @throws Exception 如果目录的大小超过97656TB，超过会抛错
     */
    private int setProperty(CmsStoragemanage cmsstrmgt, Long totalsize, String details, int level) throws Exception
    {

        int flage = 0;
        long restsize = 0;
        float restsizef = 0;
        float usedsize = 0;
        float totalsizef = 0;
        Long foldsize = null;
        float foldsizef = 0;
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.#");
        java.text.DecimalFormat dfd = new java.text.DecimalFormat("#.##");
        float percent = 0;
        // System.out.println("begin setProperty............");
        log.debug("begin setProperty............");
        try
        {
            foldsize = Long.parseLong(details);
            System.out.println("details size is :" + details);
            System.out.println("foldsize size is Long.parseLong(details) :" + foldsize);

        }
        catch (Exception e)
        {
            // details信息不是数字，即信息无效
            log.error("parselong error details=" + details);
            flage++;
        }

        usedsize = (float) foldsize.floatValue() / (float) (1024);
        System.out.println("usedsize size is (float) foldsize.floatValue() / (float) (1024) :" + usedsize);
        
        //foldsize =(long) Math.round(usedsize);
        foldsizef = usedsize;
        totalsizef = totalsize;
        // 根据目录大小、设置的总容量，计算可用空间、百分比和设置是否可用
        if (flage == 0)
        {

            // 如果是设备，不需要更新可用容量
            if (level == 1)
            {
                if (totalsizef <= foldsizef)
                {
                    // cmsstrmgt.setCurrentcapacity("0");
                    cmsstrmgt.setStoragepercentage("100");
                    cmsstrmgt.setStatus(0);
                    System.out.println("device status change to 0***********2*********");
                }
                else
                {
                	restsizef = totalsizef - foldsizef;
                    // cmsstrmgt.setCurrentcapacity(String.valueOf(restsize));
                    percent = ((float) (foldsizef * 100) / (float) totalsizef);
                    percent = (float) Double.parseDouble(df.format(percent));

                    cmsstrmgt.setStoragepercentage(String.valueOf(percent));

                    if (percent <= (float) Double.parseDouble(cmsstrmgt.getMonitorthreshold()))
                    {
                        cmsstrmgt.setStatus(1);
                    }
                    else
                    {
                        cmsstrmgt.setStatus(0);
                        System.out.println("device status change to 0***********3*********");
                    }

                }
            }
            else if (level == 2)
            {
                restsizef = totalsizef - foldsizef;
                // System.out.println("totalsize="+totalsize+" foldsize="+foldsize);
                if ((totalsizef <= foldsizef) )
                {
                	System.out.println("*************all values***************");
                	System.out.println("total size is :" + totalsize);
                	System.out.println("fold size is :" + foldsize);
                	System.out.println("rest size is :" + restsize);
                	System.out.println("round size is  :" + (long) Math.round(restsize));
                    cmsstrmgt.setCurrentcapacity("0");
                    cmsstrmgt.setStoragepercentage("100");
                    cmsstrmgt.setStatus(0);
                    System.out.println("space status change to 0***********4*********");
                }
                else
                {
                	
                    cmsstrmgt.setCurrentcapacity(String.valueOf(dfd.format(restsizef)));

                    percent = ((float) (foldsizef * 100) / (float) totalsizef);
                    percent = (float) Double.parseDouble(df.format(percent));

                    cmsstrmgt.setStoragepercentage(String.valueOf(percent));

                    cmsstrmgt.setStatus(1);

                }
            }

        }
        // System.out.println("fininsh setProperty..............");
        log.debug("fininsh setProperty..............");
        return flage;
    }

    /**
     * 执行shell
     * 
     * @param command String
     * @return String
     * @throws Exception Exception
     */
    public static String exeCmd(String command) throws Exception
    {
        String retCmdInfo = null;
        Process process = null;
        try
        {

            Runtime runtime = Runtime.getRuntime();

            process = runtime.exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            String test = in.toString();
            int index = 0;

            while ((line = in.readLine()) != null)
            {

                if (retCmdInfo == null)
                {
                    retCmdInfo = line;
                }
                else
                {
                    retCmdInfo += ("\r\n" + line);
                }

            }
            process.destroy();

        }
        catch (Exception e)
        {
            if (process != null)
            {
                process.destroy();
            }
            throw e;
        }
        return retCmdInfo;
    }

    public static void main(String[] args)
    {
        String rootPath = GlobalConstants.PORTAL_MOUNT_POINT;
        String secondlevelPath = "device1";
        String realPath = "";
        realPath = rootPath + secondlevelPath + "\\" + "space1ofdevice1";

        realPath = filterSlashStr(realPath);
        System.out.println(realPath);

        String temp = "48804   /device1/space1ofdevice1";
        String temp2 = "du: cannot access `/sbe': No such file or directoryYou have new mail in /var/mail/root";

        temp2 = temp2.substring(0, temp2.indexOf(" ", 0));
        Integer ing = null;
        try
        {
            ing = Integer.parseInt(temp2);
        }
        catch (Exception e)
        {
            System.out.println("error.......");
        }
        System.out.println(ing);

        Long a = Long.parseLong("3");
        Long b = Long.parseLong("8");
        System.out.println(((float) a * 100 / (float) b));

        float c = ((float) a * 100 / (float) b);

        String d = "37";
        if (c <= (float) Double.parseDouble(d))
        {
            System.out.println("c less");
        }
        else
        {
            System.out.println("c lager");
        }
        System.out.println(c);

    }

    public static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    public static int findfirstspace(String str)
    {
        int firstspace = -1;
        int index = 0;
        int totallength = 0;

        if (!str.trim().equals(""))
        {
            totallength = str.length();
            while (totallength > 0)
            {

                if (" ".equals(str.substring(index, index + 1)) || "\t".equals(str.substring(index, index + 1)))
                {
                    firstspace = index;
                    break;
                }
                index++;
                totallength--;
            }
        }
        return firstspace;
    }

    /**
     * 获取/根目录的可用控件大小，单位是G
     * 
     * @param str
     * @return
     */
    private String getfreesize(String str, int Gnum)
    {
        String result = "";
        String temp = "";
        StringBuffer sb = new StringBuffer();
        int flag = 0;

        for (int i = 0; i < str.length(); i++)
        {
            temp = str.substring(i, i + 1);
            if ("G".equals(temp))
            {
                if ((++flag) == (3 + Gnum))
                {
                    result = getsize(sb.toString());
                }
            }
            else
            {
                sb.append(temp);
            }
        }

        return result;
    }

    /**
     * 获取数字
     * 
     * @param str
     * @return
     */
    public String getsize(String str)
    {
        String size = "";
        char temp;
        for (int i = str.length() - 1; i >= 0; i--)
        {
            temp = str.charAt(i);
            if (checknum(temp))
            {
                size = temp + size;
            }
            else
            {
                return size;
            }
        }
        return size;
    }

    /**
     * 检查是否是数字
     * 
     * @param str String类型
     * @return boolean类型
     */
    private boolean checknum(char chr)
    {

        String mask = "0123456789";

        if (mask.indexOf(chr) != -1)
        {
            return true;
        }

        return false;
    }

    /**
     * 检查字符串包含G的个数
     * 
     * @param str String类型
     * @return int类型
     */
    private int checkContainG(String str)
    {

        String mask = "G";
        int countG = 0;
        for (int i = 0; i < str.length(); i++)
        {
            if (mask.indexOf(str.charAt(i)) != -1)
            {
                countG++;
            }
        }

        return countG;
    }
}

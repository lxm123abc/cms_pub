package com.zte.cms.storageArea.ls;

import com.zte.umap.common.ResourceMgt;

public class storageareaConstants
{

    public static final String CMS_STORAGEAREA_RES_FILE_NAME = "cms_storagearea_resource";// 资源文件名
    public static final String CMS_STORAGEAREA_MSG_GETPATH_NO_SPACE_AVALIABLE = "cms_storagearea_msg_getpath_no_space_avaliable";
    public static final String CMS_STORAGEAREA_MSG_SPACE_RELEASEED = "cms_storagearea_msg_space_realsed";
    public static final String CMS_STORAGEAREA_MSG_SPACE_HAS_BINDED = "cms_storagearea_msg_space_has_binded";
    public static final String CMS_STORAGEAREA_MSG_SPACE_DOESNOT_EXIST = "cms_storagearea_msg_space_doesnot_exist";

    public static String setSourceFile()
    {
        ResourceMgt.addDefaultResourceBundle(CMS_STORAGEAREA_RES_FILE_NAME);
        return null;
    }

}

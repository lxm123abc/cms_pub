package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IPhysicalchannelBufDS
{
    /**
	 * 新增PhysicalchannelBuf对象
	 *
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException;
	  
    /**
	 * 更新PhysicalchannelBuf对象
	 *
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void updatePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException;

    /**
	 * 批量更新PhysicalchannelBuf对象
	 *
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void updatePhysicalchannelBufList( List<PhysicalchannelBuf> physicalchannelBufList )throws DomainServiceException;



	/**
	 * 删除PhysicalchannelBuf对象
	 *
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void removePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException;

	/**
	 * 批量删除PhysicalchannelBuf对象
	 *
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void removePhysicalchannelBufList( List<PhysicalchannelBuf> physicalchannelBufList )throws DomainServiceException;




	/**
	 * 查询PhysicalchannelBuf对象
	 
	 * @param physicalchannelBuf PhysicalchannelBuf对象
	 * @return PhysicalchannelBuf对象
	 * @throws DomainServiceException ds异常 
	 */
	 public PhysicalchannelBuf getPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询PhysicalchannelBuf对象 
	  * 
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @return 满足条件的PhysicalchannelBuf对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<PhysicalchannelBuf> getPhysicalchannelBufByCond( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException;

	 /**
	  * 根据条件分页查询PhysicalchannelBuf对象 
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询PhysicalchannelBuf对象 
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
 	 
     public void removePhysicalchannelBufByChId( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException;

}

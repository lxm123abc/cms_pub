package com.zte.cms.basicdata.service;

import java.util.List;

import com.zte.cms.basicdata.dao.ICmsProgramtypeDAO;
import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramtypeDS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsProgramtypeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramtypeDAO dao = null;

    public void setDao(ICmsProgramtypeDAO dao)
    {
        this.dao = dao;
    }

    public CmsProgramtype getCmsProgramtype(CmsProgramtype cmsProgramtype) throws DomainServiceException
    {
        log.debug("get cmsProgramtype by pk starting...");
        CmsProgramtype rsObj = null;
        try
        {
            rsObj = dao.getCmsProgramtype(cmsProgramtype);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramtypeList by pk end");
        return rsObj;
    }

    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DomainServiceException
    {
        log.debug("get cmsProgramtype by condition starting...");
        List<CmsProgramtype> rsList = null;
        try
        {
            rsList = dao.getCmsProgramtypeByCond(cmsProgramtype);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramtype by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramtype cmsProgramtype, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsProgramtype page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramtype, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtype>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramtype page info by condition end");
        return tableInfo;
    }

    public TableDataInfo queryCmsProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("queryCmsProgramtype info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramtype, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtype>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("queryCmsProgramtype info by condition end");
        return tableInfo;
    }

    // tree显示
    public List<CmsProgramtype> queryForTree() throws DomainServiceException
    {
        log.debug("get cmsProgramtype page info by condition starting...");
        List<CmsProgramtype> listMenuTree = null;
        listMenuTree = dao.queryForMenuTree(new CmsProgramtype());
        return listMenuTree;
    }

    // 按照id查询
    public CmsProgramtype getCmsProgramtypeById(Long id)
    {
        return dao.getCmsProgramtypeById(id);
    }

    public void deleteProgramtype(Long id)
    {
        log.debug("deleteProgramtype start...");
        try
        {
            dao.deleteProgramtype(id);
        }
        catch (DAOException e)
        {
            log.error(e);
        }
        log.debug("deleteProgramtype end");
    }

    public List<CmsProgramtype> queryChildCmsProgramtype(CmsProgramtype programtype)
    {
        return dao.queryChildCmsProgramtype(programtype);
    }

    public CmsProgramtype programtypeUpdate(CmsProgramtype cmsProgramtype)
    {
        try
        {
            dao.updateCmsProgramtype(cmsProgramtype);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
        return cmsProgramtype;
    }

    public CmsProgramtype programtypeInsert(CmsProgramtype cmsProgramtype)
    {
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_programtype");
        cmsProgramtype.setTypeindex(index);
        try
        {
            dao.insertCmsProgramtype(cmsProgramtype);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
        return cmsProgramtype;
    }

    public TableDataInfo queryParentProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize)
    {
        log.debug("queryCmsProgramtype info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryParentProgramtype(cmsProgramtype, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtype>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("queryCmsProgramtype info by condition end");
        return tableInfo;
    }

    public Integer queryByTypeid(CmsProgramtype cmsProgramtype)
    {
        return dao.queryByTypeid(cmsProgramtype);
    }

    public Integer queryByTypename(CmsProgramtype cmsProgramtype)
    {
        return dao.queryByTypename(cmsProgramtype);
    }

    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype)
    {
        return dao.queryTypeindexByTypename(cmsProgramtype);
    }

    public CmsProgramtype queryByParentid(String typeid)
    {
        return dao.queryByParentid(typeid);
    }

    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype)
    {
        return dao.listCmsprogramtypeByParentid(cmsProgramtype);
    }

    public CmsProgramtype getCmsProgramtype(String typeid)
    {
        return dao.getCmsProgramtype(typeid);
    }

    public Integer isHasChild(String typeid)
    {
        return dao.isHasChild(typeid);
    }

    public List<CmsProgramtype> listMenuTreeByParentid(String typeid)
    {
        return dao.listMenuTreeByParentid(typeid);
    }

    public CmsProgramtype getCmsProgramtype(Long typeindex)
    {
        return dao.getCmsProgramtype(typeindex);
    }

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex)
    {
        return dao.getCmsprogramtypeByTypeindex(typeindex);
    }

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException
    {
        return dao.getProgramtypeByCond(cmsProgramtype);
    }
}

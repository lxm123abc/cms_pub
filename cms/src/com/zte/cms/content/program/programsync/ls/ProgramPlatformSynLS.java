package com.zte.cms.content.program.programsync.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.basicdata.service.ICmsProgramtypeDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.cms.content.program.programsync.service.IProgramPlatformSynDS;
import com.zte.cms.content.program.programsync.service.IProgramTargetSynDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.ctgpgmap.service.ICategoryProgramMapDS;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.picture.service.IProgramPictureMapDS;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.cms.seriespromap.service.ISeriesProgramMapDS;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;


public class ProgramPlatformSynLS extends DynamicObjectBaseDS implements IProgramPlatformSynLS
{
    
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());

    private IProgramPlatformSynDS programPlatformSynDS;


	private ICmsProgramDS cmsProgramDS;
    private ICmsMovieDS cmsMovieDS;
    private IUsysConfigDS usysConfigDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private IProgramTargetSynDS programTargetSynDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IServiceProgramMapDS serviceProgramMapDS;

	private IProgramPictureMapDS programPictureMapDS;
    private ISeriesProgramMapDS seriesProgramMapDS;
    private IProgramCrmMapDS programCrmMapDS;
    private IPictureDS pictureDS;
    private ICmsProgramtypeDS cmsProgramtypeDS;

	private ICntSyncTaskDS cntsyncTaskDS;
	private IObjectSyncRecordDS objectSyncRecordDS;
    private ICategoryProgramMapDS ctgpgmapDS;
    private Targetsystem targetsystem;
    private Map map = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象
    private Map propicmap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象
    private String desPath;
    private String result = "0:操作成功";
    public final static String CNTSYNCXML = "xmlsync";
    public final static String PROGRAM_DIR = "program";

    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    
    // 内容相关对象同步任务表 objectType
    public final static int PROGRAM_OBJECTTYPE = 1;
    public final static int MOVIE_OBJECTTYPE = 2;
    
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress="";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
      
    public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            // 格式化内容带有时间的字段
            programPlatformSyn.setCreateStarttime(DateUtil2.get14Time(programPlatformSyn.getCreateStarttime()));
            programPlatformSyn.setCreateEndtime(DateUtil2.get14Time(programPlatformSyn.getCreateEndtime()));
            programPlatformSyn.setOnlinetimeStartDate(DateUtil2.get14Time(programPlatformSyn.getOnlinetimeStartDate()));
            programPlatformSyn.setOnlinetimeEndDate(DateUtil2.get14Time(programPlatformSyn.getOnlinetimeEndDate()));
            programPlatformSyn.setCntofflinestarttime(DateUtil2.get14Time(programPlatformSyn.getCntofflinestarttime()));
            programPlatformSyn.setCntofflineendtime(DateUtil2.get14Time(programPlatformSyn.getCntofflineendtime()));
            
            if(programPlatformSyn.getProgramid()!=null&&!"".equals(programPlatformSyn.getProgramid())){
            	programPlatformSyn.setProgramid(EspecialCharMgt.conversion(programPlatformSyn.getProgramid()));
            }
            if(programPlatformSyn.getNamecn()!=null&&!"".equals(programPlatformSyn.getNamecn())){
            	programPlatformSyn.setNamecn(EspecialCharMgt.conversion(programPlatformSyn.getNamecn()));
            }

            if (programPlatformSyn.getCpid()!=null&&!"".equals(programPlatformSyn.getCpid()))
            {
                programPlatformSyn.setCpid(EspecialCharMgt.conversion(programPlatformSyn.getCpid()));
            }
            if (programPlatformSyn.getCpcnshortname()!=null&&!"".equals(programPlatformSyn.getCpcnshortname()))
            {
                programPlatformSyn.setCpcnshortname(EspecialCharMgt.conversion(programPlatformSyn.getCpcnshortname()));
            }
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            programPlatformSyn.setOperid(operinfo.getOperid());
            dataInfo = programPlatformSynDS.pageInfoQuery(programPlatformSyn, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }
    //批量发布
    public String batchPublishOrDelPublish(List<CmsProgram> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
    
            if (operType.equals("0"))  //批量发布
            {
                for (CmsProgram program : list)
                {
                    message = publishProgramPlatformSyn(program.getProgramindex().toString(), type , 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), program.getProgramid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), program.getProgramid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                int platform_type = 1;
                if (type ==1)
                {
                    platform_type = 2;
                }

                for (CmsProgram program : list)
                {
                    message = delPublishProgramPlatformSyn(program.getProgramindex().toString(), platform_type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), program.getProgramid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), program.getProgramid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
         
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("ProgramPlatformSynLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }

        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
    
    /**
     * 判断获取修改发布时应填的状态
     * @param programTargetSynList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getmodifyTaskstatus(List<ProgramTargetSyn> programTargetSynList) throws Exception
    {
        int  tgType = 3;
        boolean hascdn = false; //判断有无关联cdn网元
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(ProgramTargetSyn targetsysSyn:programTargetSynList)
        {
            if (targetsysSyn.getTargettype()==3&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hascdn = true;
            }
            if (targetsysSyn.getTargettype()==1&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hasepg = true;
            }
        }
        
        if (hascdn) //存在cdn的情况
        {
            tgType = 3;
        }
        
        if (!hascdn&&hasbms) //不存在cdn，存在bms的情况
        {
            tgType = 1;
        }
        if (!hascdn&&!hasbms&&hasepg) //不存在cdn,bms,只存在epg的情况
        {
            tgType =2;
        }
        return tgType;
    }       
    
    
    /**
     * 判断获取发布时应填的状态
     * @param programTargetSynList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getTaskstatus(List<ProgramTargetSyn> programTargetSynList) throws Exception
    {
        int  tgType = 3;
        boolean hascdn = false; //判断有无关联cdn网元
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(ProgramTargetSyn targetsysSyn:programTargetSynList)
        {
            if (targetsysSyn.getTargettype()==3&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400||
                    targetsysSyn.getStatus()== 500))
            {
                hascdn = true;
            }
            if (targetsysSyn.getTargettype()==1&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400||
                    targetsysSyn.getStatus()== 500))
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400||
                    targetsysSyn.getStatus()== 500))
            {
                hasepg = true;
            }
        }
        
        if (hascdn) //存在cdn的情况
        {
            tgType = 3;
        }
        
        if (!hascdn&&hasbms) //不存在cdn，存在bms的情况
        {
            tgType = 1;
        }
        if (!hascdn&&!hasbms&&hasepg) //不存在cdn,bms,只存在epg的情况
        {
            tgType =2;
        }
        return tgType;
    }    

    /**
     * 判断获取取消发布时应填的状态
     * @param programTargetSynList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getdelTaskstatus(List<ProgramTargetSyn> programTargetSynList) throws Exception
    {
        int  tgType = 2;
        boolean hascdn = false; //判断有无关联cdn网元
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(ProgramTargetSyn targetsysSyn:programTargetSynList)
        {
            if (targetsysSyn.getTargettype()==3&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hascdn = true;
            }
            if (targetsysSyn.getTargettype()==1&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&(targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500))
            {
                hasepg = true;
            }
        }
        if (hasepg) //存在epg的情况
        {
            tgType =2;
        }
        
        if (!hasepg&&hasbms) //不存在epg，存在bms的情况
        {
            tgType =1;

        }
        if (!hasepg&&!hasbms&&hascdn) //不存在epg,bms,只存在cdn的情况
        {
            tgType =3;
        }
        return tgType;
    }     
    
    //点播内容针对工作流的修改发布
    public String modifyProgramPlatformSyn(String programindex) throws Exception
    {
        boolean sucessPubPlat3 =false ;
        boolean sucessPubPlat2 =false ;
        String rtnPubStr = null; 
        StringBuffer resultPubStr = new StringBuffer();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            String initresult="";
            int synType = 2; //修改发布
            Long batchid = 0l;//发布批次号
            int stat = 1; //待发布
            CmsProgram programtemp =new CmsProgram(); // 创建发布的内容对象
            programtemp.setProgramindex(Long.valueOf(programindex));
            programtemp = cmsProgramDS.getCmsProgram(programtemp);      
            int tgType = 3;
            boolean hasplat3 = false;
            int platform = 2;
            initresult =initData(programindex,8,synType,"platform"); //判断发布需满足的基本条件
        
            if(!ProgramSynConstants.SUCCESS.equals(initresult)) //不满足，则退出发布
            {
                return initresult;
            }
             //获取平台
            ProgramPlatformSyn programPlatformSynTmpPlat3=new ProgramPlatformSyn();
            programPlatformSynTmpPlat3.setProgramindex(Long.parseLong(programindex));
            
            //获取平台
           ProgramPlatformSyn programPlatformSynTmpPlat2=new ProgramPlatformSyn();
           programPlatformSynTmpPlat2.setProgramindex(Long.parseLong(programindex));
           
            //获取平台下网元
            List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
            ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
            programTargetSynObj.setObjectindex(Long.parseLong(programindex)); 
            //programTargetSynObj.setPlatform(platform);
            programTargetSynObj.setObjecttype(3);           
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCond(programTargetSynObj);         
            
            for(ProgramTargetSyn targetsysSyn:programTargetSynList)
            {
                if ((targetsysSyn.getTargettype() == 1&&(targetsysSyn.getStatus()==300 || targetsysSyn.getStatus()==500)) 
                        || (targetsysSyn.getTargettype() == 2&&(targetsysSyn.getStatus()==300 || targetsysSyn.getStatus()==500)) 
                        || (targetsysSyn.getTargettype() == 3&&(targetsysSyn.getStatus()==300 || targetsysSyn.getStatus()==500)))
                {
                    hasplat3 = true;              
                }
            }

            if (hasplat3)
            {
                batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                tgType = getmodifyTaskstatus(programTargetSynList);
                programPlatformSynTmpPlat3=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSynTmpPlat3);
            }
                
            programPlatformSynTmpPlat2=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSynTmpPlat2);
            
            for(ProgramTargetSyn targetsysSyn:programTargetSynList)
            {
                if (targetsysSyn.getTargetstatus()!=0)
                {
                    resultPubStr.append(PUBTARGETSYSTEM
                             +targetsysSyn.getTargetid()+": "+"目标系统状态不正确"
                            +"  ");
                    continue;
                }
                if (targetsysSyn.getStatus()==300||targetsysSyn.getStatus()== 500)
                {
                    if (targetsysSyn.getTargettype() == 1 || targetsysSyn.getTargettype() == 2 || targetsysSyn.getTargettype() == 3)
                    {
                        platform = 2;
                    }
                    else
                    {
                        platform = 1;
                    }

                    String xml;
                    xml = StandardImpFactory.getInstance().create(platform).createXmlFile(map, targetsysSyn.getTargettype(), 3, synType); 
                    if (null == xml)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM
                                     +targetsysSyn.getTargetid()+": "+"生成同步指令xml文件失败"
                                     +"  ");
                        continue;
                    }
                                                
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long targetindex =targetsysSyn.getTargetindex();
                    if (targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 || targetsysSyn.getTargettype() == 1 || targetsysSyn.getTargettype() == 2)
                    {
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取syncindex
                        Long objectindex = targetsysSyn.getProgramindex();
                        String objectid = targetsysSyn.getProgramid();
                        String objectcode = programtemp.getCpcontentid();
                        insertObjectRecord( syncindex, index, objectindex, objectid,3, targetindex, synType,objectcode,1);  //插入对象日志发布表
                    }
                    
                    if (targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 || targetsysSyn.getTargettype() == 2 || targetsysSyn.getTargettype() == 3)
                    {
                        List<CmsMovie> movieTmpList = (List<CmsMovie>)map.get("movieList");
                        for (int i =0; i<movieTmpList.size(); i++)
                        {
                            //movie 对象日志记录
                            Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                            Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                            String objectidMovie = movieTmpList.get(i).getMovieid();
                            String objectcode = movieTmpList.get(i).getCpcontentid();
                            insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,targetindex, 
                                    synType,objectcode,1);
                            
                            if(targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 ||targetsysSyn.getTargettype() == 2)
                            {
                                //Mapping 对象日志记录
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = movieTmpList.get(i).getMappingid();
                                String elementid = objectidMovie;
                                String  parentid = targetsysSyn.getProgramid();
                                insertObjectMappingRecord( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, 
                                        parentid,targetindex,synType);
                            }
                            
                        }
                    }
                    
                    if (targetsysSyn.getTargettype() == 1 || targetsysSyn.getTargettype() == 2 || targetsysSyn.getTargettype() == 3)
                    {
                        if (targetsysSyn.getTargettype() == tgType)
                        {
                            stat = 1;
                        }
                        else
                        {
                            stat = 0;
                        }
                        insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                        sucessPubPlat3=true;
                    }
                    else
                    {
                        Long batchid_plat2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                        insertSyncTask(index,xml,correlateid,targetindex,batchid_plat2,1); //插入同步任务
                        sucessPubPlat2=true;
                    }
                    
                    CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                    cntTargetSyncTemp.setObjectindex(Long.parseLong(programindex));
                    cntTargetSyncTemp.setOperresult(40);  //修改同步中

                    cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);  //修改网元发布信息
                    CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    resultPubStr.append(PUBTARGETSYSTEM
                            +targetsysSyn.getTargetid()+": "
                            +operSucess+"  "); 
                }
            }                           
            
            // 更新平台发布状态       
            if (sucessPubPlat3)
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
                cntPlatformSyncTmp.setSyncindex(programPlatformSynTmpPlat3.getSyncindex());
                cntPlatformSyncTmp.setStatus(200);//发布中
                cntPlatformSyncTmp.setPlatform(2);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
            if (sucessPubPlat2)
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
                cntPlatformSyncTmp.setSyncindex(programPlatformSynTmpPlat2.getSyncindex());
                cntPlatformSyncTmp.setStatus(200);//发布中
                cntPlatformSyncTmp.setPlatform(1);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            throw e;
        } 

        rtnPubStr = (sucessPubPlat3||sucessPubPlat2)==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
        return rtnPubStr;
    }
    
    //点播内容针对平台发布
    public String publishProgramPlatformSyn(String programindex, int platform,int synType) throws Exception
    {
        boolean sucessPub =false ;
        String rtnPubStr = null; 
        StringBuffer resultPubStr = new StringBuffer();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            String initresult="";
            Long batchid = 0l;//发布批次号
            int stat = 1; //待发布
            CmsProgram programtemp =new CmsProgram(); // 创建发布的内容对象
            programtemp.setProgramindex(Long.valueOf(programindex));
            programtemp = cmsProgramDS.getCmsProgram(programtemp);      
            int tgType = 3;
            if (platform == 2) //发布到3.0平台
            {
                initresult =initData(programindex,1,synType,"platform"); //判断发布需满足的基本条件
            }
            
            else //发布到2.0平台
            {
                initresult =initData(programindex,2,synType,"platform"); //判断发布需满足的基本条件
            }
            
            if(!ProgramSynConstants.SUCCESS.equals(initresult)) //不满足，则退出发布
            {
                return initresult;
            }
            
             //获取平台
            ProgramPlatformSyn programPlatformSynTmp=new ProgramPlatformSyn();
            programPlatformSynTmp.setProgramindex(Long.parseLong(programindex));
            
            //获取平台下网元
            List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
            ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
            programTargetSynObj.setObjectindex(Long.parseLong(programindex)); 
            programTargetSynObj.setPlatform(platform);
            programTargetSynObj.setObjecttype(3);           
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCond(programTargetSynObj);         
            
            if (platform == 2) //发布到3.0平台
            {
                programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSynTmp);
                batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                tgType = getTaskstatus(programTargetSynList);
            }
            else //发布到2.0平台
            {
                programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSynTmp);
            }
            
            for(ProgramTargetSyn targetsysSyn:programTargetSynList)
            {
                if (targetsysSyn.getTargetstatus()!=0)
                {
                    resultPubStr.append(PUBTARGETSYSTEM
                             +targetsysSyn.getTargetid()+": "+"目标系统状态不正确"
                            +"  ");
                    continue;
                }
                if (targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400||
                        targetsysSyn.getStatus()== 500)
                {
                    if (targetsysSyn.getStatus()== 500) //走修改发布流程
                    {
                        synType = 2;
                    }
                    else
                    {
                        synType = 1;
                    }
                    
                    String xml;
                    xml = StandardImpFactory.getInstance().create(platform).createXmlFile(map, targetsysSyn.getTargettype(), 3, synType); 
                    if (null == xml)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM
                                     +targetsysSyn.getTargetid()+": "+"生成同步指令xml文件失败"
                                     +"  ");
                        continue;
                    }
                                                
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long targetindex =targetsysSyn.getTargetindex();
                    if (targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 || targetsysSyn.getTargettype() == 1 || targetsysSyn.getTargettype() == 2)
                    {
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取syncindex
                        Long objectindex = targetsysSyn.getProgramindex();
                        String objectid = targetsysSyn.getProgramid();
                        String objectcode = programtemp.getCpcontentid();
                        insertObjectRecord( syncindex, index, objectindex, objectid,3, targetindex, synType,objectcode,1);  //插入对象日志发布表
                    }
                    
                    if (targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 || targetsysSyn.getTargettype() == 2 || targetsysSyn.getTargettype() == 3)
                    {
                        List<CmsMovie> movieTmpList = (List<CmsMovie>)map.get("movieList");
                        for (int i =0; i<movieTmpList.size(); i++)
                        {
                            //movie 对象日志记录
                            Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                            Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                            String objectidMovie = movieTmpList.get(i).getMovieid();
                            String objectcode = movieTmpList.get(i).getCpcontentid();
                            insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,targetindex, 
                                    synType,objectcode,1);
                            
                            if(targetsysSyn.getTargettype() == 0 || targetsysSyn.getTargettype() == 10 ||targetsysSyn.getTargettype() == 2)
                            {
                                //Mapping 对象日志记录
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = movieTmpList.get(i).getMappingid();
                                String elementid = objectidMovie;
                                String  parentid = targetsysSyn.getProgramid();
                                insertObjectMappingRecord( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, 
                                        parentid,targetindex,synType);
                            }
                            
                        }
                    }
                    
                    if (platform == 1)
                    {
                        Long batchid_plat2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                        insertSyncTask(index,xml,correlateid,targetindex,batchid_plat2,stat); //插入同步任务
                    }
                    else
                    {
                        if (targetsysSyn.getTargettype() == tgType)
                        {
                            stat = 1;
                        }
                        else
                        {
                            stat = 0;
                        }
                        insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                    }
                    
                    CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                    cntTargetSyncTemp.setObjectindex(Long.parseLong(programindex));
                    if (synType ==1)
                    {
                        cntTargetSyncTemp.setOperresult(10); //新增同步中
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);  //修改同步中
                    }
                    cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);  //修改网元发布信息
                    sucessPub=true;
                    if (synType ==1) //发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else //修改发布日志
                    {
                        CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    resultPubStr.append(PUBTARGETSYSTEM
                            +targetsysSyn.getTargetid()+": "
                            +operSucess+"  "); 
                }
            }                           
            
            // 更新平台发布状态       
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
                cntPlatformSyncTmp.setSyncindex(programPlatformSynTmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(200);//发布中
                cntPlatformSyncTmp.setPlatform(platform);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            throw e;
        } 

        rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
        return rtnPubStr;
    }    
    
    
    //针对网元单独发布
    public String publishProgramTarget(String programindex, String targetindex,int type,int modifyType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String initresult="";
        List<ProgramTargetSyn> programTargetSynListTemp = new ArrayList<ProgramTargetSyn>();
        CntTargetSync cntTargetSyncTemp = new CntTargetSync();
        ProgramPlatformSyn programPlatformSynTmp=new ProgramPlatformSyn();
        programPlatformSynTmp.setProgramindex(Long.parseLong(programindex));
        programPlatformSynTmp.setTargetindex(Long.parseLong(targetindex));
        int platformType = 1; //平台类型
        try
        {  
            ProgramTargetSyn programTargetSyntemp = new ProgramTargetSyn();
            programTargetSyntemp.setTargetindex(Long.parseLong(targetindex));
            programTargetSyntemp.setProgramindex(Long.parseLong(programindex));
            programTargetSynListTemp = programTargetSynDS.getProgramTargetSynByCond(programTargetSyntemp);
            int synType = 1;

            ProgramPlatformSyn programPlatformSynObj=new ProgramPlatformSyn();
            programPlatformSynObj.setProgramindex(Long.parseLong(programindex));
    		int synTargetType =1;
            if(modifyType == 1)
            {
            	synType = 2;
            	synTargetType = 2;
            }
            if (programTargetSynListTemp == null|| programTargetSynListTemp.size()==0)
            {
            	return "1:操作失败,获取目标网元发布记录失败";
            }
            
        	if (programTargetSynListTemp.get(0).getTargetstatus()!=0)
        	{
            	return "1:操作失败,目标系统状态不正确";
        	}
            if(programTargetSynListTemp.get(0).getStatus()==500)
            {
        		synTargetType =2;
        		synType = 2;
            }
            initresult =initData(programindex,3,synType,targetindex); //判断发布需满足的基本条件
            if(!ProgramSynConstants.SUCCESS.equals(initresult)){
                return initresult;
            }
            List<CmsMovie> movieTmpList = (List<CmsMovie>)map.get("movieList");
            CmsProgram CmsProgramTmp = new CmsProgram();
            CmsProgramTmp.setProgramindex(Long.valueOf(programindex));
            CmsProgramTmp = cmsProgramDS.getCmsProgram(CmsProgramTmp);
            int targettype =programTargetSynListTemp.get(0).getTargettype();
            String targetid = programTargetSynListTemp.get(0).getTargetid();
            int stat = 1;

            if (type == 1)  //发布到3.0平台网元
            {
            	platformType = 2;
            	programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSynObj);
               	if (targettype ==1 )  //发布到BMS系统  1个program
            	{
            		String bmsxml;
               		if (synTargetType==1)
               		{
               			bmsxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 1); //新增发布
                        if (null == bmsxml)
                        {
                        	return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
               			synType =2;
               			bmsxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 2);  //修改发布
                        if (null == bmsxml)
                        {
                        	return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                          
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    String objectid = programTargetSynListTemp.get(0).getProgramid();
                    String objectcode = CmsProgramTmp.getCpcontentid();
                    insertObjectRecord( syncindex, index, Long.parseLong(targetindex), objectid,3, 
                    		Long.parseLong(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));  
                    
                    insertSyncTask(index,bmsxml,correlateidBMS,Long.parseLong(targetindex),batchid,stat); //插入同步任务
                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    if(synTargetType == 1)
                    {
                        cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);
                    }
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);  //修改网元发布状态      
                    if (synTargetType == 1) //发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else  //修改发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
            	}
            	else if (targettype ==2 )  //EPG系统   1program  N movie  N mapping 
            	{
            		String epgxml;
               		if (synTargetType==1)
               		{
               			epgxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 1);
                        if (null == epgxml)
                        {
                        	return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
               			synType =2;
               			epgxml =StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 2);
                        if (null == epgxml)
                        {
                        	return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
                    String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    //program 对象对象日志记录
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    Long objectindex = programTargetSynListTemp.get(0).getProgramindex();
                    String objectid = programTargetSynListTemp.get(0).getProgramid();
                    String objectcode = CmsProgramTmp.getCpcontentid();
                    insertObjectRecord( syncindex, index, objectindex, objectid, 3,Long.parseLong(targetindex), synType,objectcode,1);
                    
                    for (int i =0; i<movieTmpList.size(); i++)
                    {
                    	//movie 对象日志记录
                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                        String objectidMovie = movieTmpList.get(i).getMovieid();
                        objectcode =  movieTmpList.get(i).getCpcontentid();
                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,Long.parseLong(targetindex), 
                        		synType,objectcode,1);
                        
                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = movieTmpList.get(i).getMappingid();
                        String elementid = objectidMovie;
                        String  parentid = objectid;
                        insertObjectMappingRecord( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, parentid,
                        Long.parseLong(targetindex),synType);
                    }
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    
                    insertSyncTask(index,epgxml,correlateidEPG,Long.parseLong(targetindex),batchid,stat);  // 插入同步任务
                    
                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    if(synTargetType == 1)
                    {
                        cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);
                    }                        
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    if (synTargetType == 1) //发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else  //修改发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
            	}
            	else if (targettype ==3)  //CDN系统  N program 发布状态为同步中、发布成功以及预下线不能新增同步
            	{
            		String cdnxml;
               		if (synTargetType==1)
               		{
                         cdnxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 1);
                         if (null == cdnxml)
                         {
                         	return "1:操作失败,生成同步指令xml文件失败";
                         }
               		}
               		else
               		{
               			synType =2;
                        cdnxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 2);
                        if (null == cdnxml)
                        {
                        	return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
                    String correlateidCDN = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    //N movie 对象日志记录
                    for (int i =0; i<movieTmpList.size(); i++)
                    {
                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                        String objectidMovie = movieTmpList.get(i).getMovieid();
                        String objectcode = movieTmpList.get(i).getCpcontentid();
                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,
                        Long.parseLong(targetindex), synType,objectcode,1);
                    }
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                    
                    insertSyncTask(index,cdnxml,correlateidCDN,Long.parseLong(targetindex),batchid,stat); //插入同步任务
                                            
                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    if(synTargetType == 1)
                    {
                        cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);

                    }                        
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    if (synTargetType == 1) //发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else  //修改发布业务日志
                    {
                        CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                                CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
            	}                	
            }

            if (type == 2)  //发布到2.0平台网元
            {
        		programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSynObj);

        		String wg18xml;
           		if (synTargetType==1)
           		{
                     wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, targettype, 3, 1);
                     if (null == wg18xml)
                     {
                     	return "1:操作失败,生成同步指令xml文件失败";
                     }
           		}
           		else
           		{
           			synType =2;                    
                    wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, targettype, 3, 2);
                    if (null == wg18xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
           		}
  
                String correlateidtarget2 = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                
                //program 对象对象日志记录
               
                String objectcode = CmsProgramTmp.getCpcontentid();
                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                Long objectindex = programTargetSynListTemp.get(0).getProgramindex();
                String objectid = programTargetSynListTemp.get(0).getProgramid();
                insertObjectRecord( syncindex, index, objectindex, objectid, 3,Long.parseLong(targetindex), synType,objectcode,2);
                for (int i =0; i<movieTmpList.size(); i++)
                {
                	//movie 对象日志记录
                    Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                    String objectidMovie = movieTmpList.get(i).getMovieid();
                    objectcode = movieTmpList.get(i).getCpcontentid();
                    insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,
                    Long.parseLong(targetindex), synType,objectcode,2);
                    
                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = movieTmpList.get(i).getMappingid();
                    String elementid = objectidMovie;
                    String  parentid = objectid;
                    String elementcode = objectcode;
                    String parentcode = CmsProgramTmp.getCpcontentid();
                    insertObjectMappingIPTV2Record( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, parentid,elementcode,parentcode,Long.parseLong(targetindex),synType);
                }                     
                Long batchid_plat2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));

                insertSyncTask(index,wg18xml,correlateidtarget2,Long.parseLong(targetindex),batchid_plat2,stat);                         
                cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                if(synTargetType == 1)
                {
                    cntTargetSyncTemp.setOperresult(10);
                }
                else
                {
                    cntTargetSyncTemp.setOperresult(40);

                }                    
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态     
                if (synTargetType == 1) //发布业务日志
                {
                    CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else  //修改发布业务日志
                {
                    CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB, CommonLogConstant.OPERTYPE_PROGRAM_ModifyPUB_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
        }
    	catch (Exception e) 
    	{
            e.printStackTrace();
            throw e;
        } 

        List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
        ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
        programTargetSynObj.setObjectindex(Long.parseLong(programindex));          
        programTargetSynObj.setObjecttype(3);
        if(type==1)
        {
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSynObj);   
        }
        else
        {
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSynObj);   
        }
        int[] targetSyncStatus = new int[programTargetSynList.size()];
        for(int i = 0;i<programTargetSynList.size() ;i++)
        {
        	targetSyncStatus[i] =programTargetSynList.get(i).getStatus();
        }
        int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
        if (programPlatformSynTmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
        {
            CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
            cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
            cntPlatformSyncTmp.setSyncindex(programPlatformSynTmp.getSyncindex());
            cntPlatformSyncTmp.setStatus(status);
            cntPlatformSyncTmp.setPlatform(platformType);
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
        }
		return result;
    }
 
    //针对网元单独取消发布
    public String delPublishProgramTarget(String programindex, String targetindex,int type) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String initresult="";
        String initBindresult="";
        List<ProgramTargetSyn> programTargetSynListTemp = new ArrayList<ProgramTargetSyn>();
        CntTargetSync cntTargetSyncTemp = new CntTargetSync();
        ProgramPlatformSyn programPlatformSynTmp=new ProgramPlatformSyn();
        programPlatformSynTmp.setProgramindex(Long.parseLong(programindex));
        ProgramTargetSyn programTargetSyntemp = new ProgramTargetSyn();
        programTargetSyntemp.setTargetindex(Long.parseLong(targetindex));
        programTargetSyntemp.setProgramindex(Long.parseLong(programindex));
        programTargetSynListTemp = programTargetSynDS.getProTargetSynByCond(programTargetSyntemp);
        int synType=3;
        int platformType =1;
        if (programTargetSynListTemp == null|| programTargetSynListTemp.size()==0)
        {
        	return "1:操作失败,获取目标网元发布记录失败";
        }
    	if (programTargetSynListTemp.get(0).getTargetstatus()!=0)
    	{
        	return "1:操作失败,目标系统状态不正确";
    	}
        try
        {  
            initresult =initData(programindex,3,synType,targetindex); //判断发布需满足的基本条件
            if(!ProgramSynConstants.SUCCESS.equals(initresult)){
                return initresult;
            }
            List<CmsMovie> movieTmpList = (List<CmsMovie>)map.get("movieList");
            CmsProgram CmsProgramTmp = new CmsProgram();
            CmsProgramTmp.setProgramindex(Long.valueOf(programindex));
            CmsProgramTmp = cmsProgramDS.getCmsProgram(CmsProgramTmp);
            int targettype =programTargetSynListTemp.get(0).getTargettype();
            String targetid = programTargetSynListTemp.get(0).getTargetid();
            int stat = 1;
            if (type == 1)  //发布到3.0平台网元
            {
            	platformType = 2;
				//判断点播内容是否关联到其它对象，且关联关系是否已经发布
				initBindresult =initCheckproBind(programindex,programTargetSynListTemp.get(0).getTargetindex()); //判断发布需满足的基本条件

	            if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
	            {
	            	return "1:"+"该网元"+initBindresult;
            	}
        	    programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSynTmp);

               	if (targettype ==1 )  //发布到BMS系统  1个program
            	{
                    String bmsxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3);
                    if (null == bmsxml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                          
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    String objectid = programTargetSynListTemp.get(0).getProgramid();
                    String objectcode =CmsProgramTmp.getCpcontentid();
                    insertObjectRecord( syncindex, index, Long.parseLong(targetindex), objectid,3, Long.parseLong(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));

                    insertSyncTask(index,bmsxml,correlateidBMS,Long.parseLong(targetindex),batchid,stat); //插入同步任务

                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);  //修改网元发布状态      
                    CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid,
                    		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS); 
            	}
            	
            	else if (targettype ==2 )  //EPG系统   1program  N movie  N mapping 
            	{                	
                    String epgxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3);
                    if (null == epgxml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    
                    String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    //program 对象对象日志记录
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    Long objectindex = programTargetSynListTemp.get(0).getProgramindex();
                    String objectid = programTargetSynListTemp.get(0).getProgramid();
                    String objectcode =CmsProgramTmp.getCpcontentid();
                    insertObjectRecord( syncindex, index, objectindex, objectid, 3,Long.parseLong(targetindex), synType,objectcode,1);
                    
                    for (int i =0; i<movieTmpList.size(); i++)
                    {
                    	//movie 对象日志记录
                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                        String objectidMovie = movieTmpList.get(i).getMovieid();
                        objectcode = movieTmpList.get(i).getCpcontentid();
                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,Long.parseLong(targetindex),
                        		synType,objectcode,1);
                        
                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = movieTmpList.get(i).getMappingid();
                        String elementid = objectidMovie;
                        String  parentid = objectid;
                        insertObjectMappingRecord( syncindexMapping, index,objectindexMovie,objectidMapping, elementid,
                        		parentid,Long.parseLong(targetindex),synType);
                    }
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    
                    insertSyncTask(index,epgxml,correlateidEPG,Long.parseLong(targetindex),batchid,stat);  // 插入同步任务
                    
                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);                     
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid, 
                    		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);           
            	}

            	else if (targettype ==3)  //CDN系统  N program 发布状态为同步中、发布成功以及预下线不能新增同步
            	{
                    String cdnxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3);
                    if (null == cdnxml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    
                    String correlateidCDN = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    //N movie 对象日志记录
                    for (int i =0; i<movieTmpList.size(); i++)
                    {
                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                        String objectidMovie = movieTmpList.get(i).getMovieid();
                        String objectcode = movieTmpList.get(i).getCpcontentid();
                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,Long.parseLong(targetindex),
                        synType,objectcode,1);
                    }
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                   
                    insertSyncTask(index,cdnxml,correlateidCDN,Long.parseLong(targetindex),batchid,stat); //插入同步任务
                                            
                    cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);                       
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid, 
                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            	}                
            }
         
            if (type == 2)  //发布到2.0平台网元
            {
				//判断点播内容是否关联到其它对象，且关联关系是否已经发布
				initBindresult =initCheckproBind(programindex,programTargetSynListTemp.get(0).getTargetindex()); //判断发布需满足的基本条件

	            if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
	            {
	            	return "1:"+"该网元"+initBindresult;
            	}

        		programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSynTmp);
                String wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, targettype, 3, 3);
                String correlateidtarget2 = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                
                //program 对象对象日志记录
                String objectcode = CmsProgramTmp.getCpcontentid();
                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                Long objectindex = programTargetSynListTemp.get(0).getProgramindex();
                String objectid = programTargetSynListTemp.get(0).getProgramid();
                insertObjectRecord( syncindex, index, objectindex, objectid, 3,Long.parseLong(targetindex), synType,objectcode,2);
                
                for (int i =0; i<movieTmpList.size(); i++)
                {
                	//movie 对象日志记录
                    Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                    String objectidMovie = movieTmpList.get(i).getMovieid();
                    objectcode = movieTmpList.get(i).getCpcontentid();
                    insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,Long.parseLong(targetindex), synType,objectcode,2);
                    
                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = movieTmpList.get(i).getMappingid();
                    String elementid = objectidMovie;
                    String  parentid = objectid;
                    String elementcode = objectcode;
                    String parentcode = CmsProgramTmp.getCpcontentid();
                    insertObjectMappingIPTV2Record( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, 
                    		parentid,elementcode,parentcode,Long.parseLong(targetindex),synType);
                } 
                Long batchid_plat2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                
                insertSyncTask(index,wg18xml,correlateidtarget2,Long.parseLong(targetindex),batchid_plat2,stat); 
                
                cntTargetSyncTemp.setSyncindex(programTargetSynListTemp.get(0).getSyncindex());
                cntTargetSyncTemp.setOperresult(70);                   
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态     
                CommonLogUtil.insertOperatorLog(programTargetSynListTemp.get(0).getProgramid()+","+targetid, 
                        CommonLogConstant.MGTTYPE_VODTARGETSYN,
                        CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);        		
        }
        		
        List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
        ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
        programTargetSynObj.setObjectindex(Long.parseLong(programindex));          
        programTargetSynObj.setObjecttype(3);
        if (type == 1)
        {
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSynObj);   
        }
        else
        {
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSynObj);   
        }
        int[] targetSyncStatus = new int[programTargetSynList.size()];
        for(int i = 0;i<programTargetSynList.size() ;i++)
        {
        	targetSyncStatus[i] =programTargetSynList.get(i).getStatus();
        }
        
        int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
        if (programPlatformSynTmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
        {
            CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
            cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
            cntPlatformSyncTmp.setSyncindex(programPlatformSynTmp.getSyncindex());
            cntPlatformSyncTmp.setStatus(status);
            cntPlatformSyncTmp.setPlatform(platformType);
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
        }
        }
    	catch (Exception e) 
    	{
            e.printStackTrace();
            throw e;
        } 
		return result;
    }    
    
    public String initCheckproBind(String programindex,Long targetindex)throws Exception
    {
    	try
    	{
            CntTargetSync cntTargetSync = new CntTargetSync();
            CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
            categoryProgramMap.setProgramindex(Long.valueOf(programindex));
            List<CategoryProgramMap> categoryProgramMapList = ctgpgmapDS
                    .getCategoryProgramMapByCond(categoryProgramMap);
            //点播内容和栏目存在关联关系
            if (categoryProgramMapList != null && categoryProgramMapList.size() > 0)
            {
            	for (CategoryProgramMap categoryProMap:categoryProgramMapList)
            	{
            		cntTargetSync.setObjectindex(categoryProMap.getMapindex());
            		cntTargetSync.setObjecttype(26);
            		cntTargetSync.setTargetindex(targetindex);
                    CntTargetSync cntTargetSyncTmp = new CntTargetSync();

                    cntTargetSyncTmp=cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                    if (cntTargetSyncTmp !=null&&cntTargetSyncTmp.getStatus()!=0&&cntTargetSyncTmp.getStatus()!=400 )
                    {
    	                return "已发布该点播内容和栏目的关联关系,不能取消发布  ";
                    }
            	}
            }            
            ServiceProgramMap serviceProgramMap = new ServiceProgramMap();
            serviceProgramMap.setProgramindex(Long.valueOf(programindex));
            List<ServiceProgramMap> serviceProgramMapList = serviceProgramMapDS
                    .getServiceProgramMapByCond(serviceProgramMap);
            //点播内容和服务存在关联关系
            if (serviceProgramMapList != null && serviceProgramMapList.size() > 0)
            {
            	for (ServiceProgramMap serviceProMap:serviceProgramMapList)
            	{
            		cntTargetSync.setObjectindex(serviceProMap.getMapindex());
            		cntTargetSync.setObjecttype(21);
            		cntTargetSync.setTargetindex(targetindex);
                    CntTargetSync cntTargetSyncTmp = new CntTargetSync();

                    cntTargetSyncTmp=cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                    if (cntTargetSyncTmp !=null&&cntTargetSyncTmp.getStatus()!=0&&cntTargetSyncTmp.getStatus()!=400 )
                    {
    	                return "已发布服务打包该点播内容,不能取消发布  ";
                   }
            	}
            }  	
            
            ProgramCrmMap programCrmMap = new ProgramCrmMap();
            programCrmMap.setProgramindex(Long.valueOf(programindex));
            List<ProgramCrmMap> programCrmMapList = programCrmMapDS
            		.getProgramCrmMapByCond(programCrmMap);
            //点播内容和角色存在关联关系
            if (programCrmMapList != null && programCrmMapList.size() > 0)
            {
            	for (ProgramCrmMap proCrmMap:programCrmMapList)
            	{
            		cntTargetSync.setObjectindex(proCrmMap.getMapindex());
            		cntTargetSync.setObjecttype(34);
            		cntTargetSync.setTargetindex(targetindex);
                    CntTargetSync cntTargetSyncTmp = new CntTargetSync();

                    cntTargetSyncTmp=cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                    if (cntTargetSyncTmp !=null&&cntTargetSyncTmp.getStatus()!=0&&cntTargetSyncTmp.getStatus()!=400 )
                    {
    	                return "已发布该点播内容和角色的关联关系,不能取消发布  ";
                   }
            	}
            }  

            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setProgramindex(Long.valueOf(programindex));
            List<SeriesProgramMap> seriesProgramMapList = seriesProgramMapDS
            		.getSeriesProgramMapByCond(seriesProgramMap);
            //点播内容和连续剧存在关联关系
            if (seriesProgramMapList != null && seriesProgramMapList.size() > 0)
            {
            	for (SeriesProgramMap seriesProMap:seriesProgramMapList)
            	{
            		cntTargetSync.setObjectindex(seriesProMap.getMapindex());
            		cntTargetSync.setObjecttype(36);
            		cntTargetSync.setTargetindex(targetindex);
                    CntTargetSync cntTargetSyncTmp = new CntTargetSync();

                    cntTargetSyncTmp=cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                    if (cntTargetSyncTmp !=null&&cntTargetSyncTmp.getStatus()!=0&&cntTargetSyncTmp.getStatus()!=400 )
                    {
    	                return "已发布该单集和连续剧的关联关系,不能取消发布 ";
                   }
            	}
            }     
            
            ProgramPictureMap programPictureMap = new ProgramPictureMap();
            programPictureMap.setProgramindex(Long.valueOf(programindex));
            List<ProgramPictureMap> pictureMapList = programPictureMapDS
            		.getProgramPictureMapByCond(programPictureMap);
            //点播内容存在海报
            if (pictureMapList != null && pictureMapList.size() > 0)
            {
            	for (ProgramPictureMap proPictureMap:pictureMapList)
            	{
            		cntTargetSync.setObjectindex(proPictureMap.getMapindex());
            		cntTargetSync.setObjecttype(35);
            		cntTargetSync.setTargetindex(targetindex);
                    CntTargetSync cntTargetSyncTmp = new CntTargetSync();

                    cntTargetSyncTmp=cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                    if (cntTargetSyncTmp !=null&&cntTargetSyncTmp.getStatus()!=0&&cntTargetSyncTmp.getStatus()!=400 )
                    {
    	                return "已发布该点播内和容海报的关联关系,不能取消发布  ";
                   }
            	}
            }             
    	}
        catch (Exception e) 
        {
            e.printStackTrace();
            throw e;
        }
         return "success";
    }
    public String initData(String programindex,int type,int synType,String targetindex)throws Exception{
        try{
                /***
                 * 验证program-状态及是否存在子内容
                 * */
                CmsProgram program = null; // 创建发布的内容对象
                List<CmsProgram> programList = new ArrayList<CmsProgram>();
                List<CmsMovie> movieList = new ArrayList<CmsMovie>();

                // 获取发布的内容对象
                program = new CmsProgram();
                program.setProgramindex(Long.valueOf(programindex));
                program = cmsProgramDS.getCmsProgram(program);               
                if (program == null)
                { 
                    return "1:操作失败,内容不存在";
                }
                else if (program.getStatus() != ProgramSynConstants.programAuditSucess)
                {

                    return "1:操作失败,内容状态不正确";
                }
                else
                {
                    List<String> programTypeList = new ArrayList<String>();
                    programList.add(program);
                    map.put("programList", programList);
                    if (program.getEntrysource() == 1 || program.getProgramtype()==null || program.getProgramtype().equals(""))
                    {
                        programTypeList.add(program.getProgramtype());
                        map.put("programtypeid",programTypeList );
                    }
                    else
                    {
                        TableDataInfo dataInfo = null;
                        String programTypeId = null;
                        CmsProgramtype cmsProgramtype = new CmsProgramtype();
                        cmsProgramtype.setTypename(program.getProgramtype());
                        dataInfo = cmsProgramtypeDS.queryCmsProgramtype(cmsProgramtype, 0, 10);
                        List cmsProtypeList = dataInfo.getData();
                        if (null != cmsProtypeList && cmsProtypeList.size() > 0)
                        {
                            CmsProgramtype cmsProgramtypeTemp = (CmsProgramtype) cmsProtypeList.get(0);
                            programTypeId = cmsProgramtypeTemp.getTypeid();
                        }
                        else
                        {
                            programTypeId = "1000";
                        }
                        programTypeList.add(programTypeId);

                        map.put("programtypeid", programTypeList);
                    }
                }
            
                List<UsysConfig> usysConfigList = null;
                UsysConfig usysConfig = new UsysConfig();
                usysConfig = usysConfigDS.getUsysConfigByCfgkey(FTPADDRESS);
                if (null == usysConfig || null == usysConfig.getCfgvalue()
                    || (usysConfig.getCfgvalue().trim().equals("")))
                {
                    return "1:操作失败,获取同步内容实体文件FTP地址失败";
                }            
                // 创建发布的子内容对象List
                CmsMovie cmsMovie = null;
                cmsMovie = new CmsMovie();
                cmsMovie.setProgramindex(program.getProgramindex());
                movieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
                if (movieList == null || movieList.size() <= 0)
                {
                    return "1:操作失败,内容下没有子内容";
                }
                else
                { 
                    for(CmsMovie movie:movieList)
                    {
                        movie.setFileurl(usysConfig.getCfgvalue() + movie.getFileurl());
                    }                
                }            
                map.put("movieList", movieList);

                /***查询点播内容关联的角色相关信息    begin  ****/
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                List<ProgramCrmMap> programCrmMapList = new ArrayList<ProgramCrmMap>();
                programCrmMap.setProgramindex(Long.valueOf(programindex));
                programCrmMapList =programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                map.put("programCrmMapList", programCrmMapList);
                /***查询点播内容关联的角色相关信息    end  ****/
                
                String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
                if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
                {
                    return "1:操作失败,获取临时区地址失败";
                }
                else
                {
                    File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                    if (!makePath.exists())
                    {// 找不到目标地址，请检查存储区绑定
                        return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                    }             
                }
      
                /***
                 * 验证program-运营平台关联对象的状态是否为待发布或不一致
                 * */            
        	    if (type == 1 ||type ==2 ) //点播内容发布到平台
        	    {
                    ProgramPlatformSyn programPlatformSyn=new ProgramPlatformSyn();
                    programPlatformSyn.setProgramindex(Long.parseLong(programindex));
        	    	if (type ==1)  //点播发内容发布到3.0平台
        	    	{
                        programPlatformSyn=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSyn);
                        if(programPlatformSyn==null)
                        {
                            return "1:操作失败,该发布记录已不存在";
                        }
        	    	}
        	    	
        	    	if (type ==2) //点播内容发布到2.0平台
        	    	{
                        programPlatformSyn=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSyn);
                        if(programPlatformSyn==null)
                        {
                            return "1:操作失败,该发布记录已不存在";
                        }
        	    	}
        	    	
                    switch(synType)
                    {
                        case 1:  //发布
                    	    if (programPlatformSyn.getStatus()!=ProgramSynConstants.programTobePub && programPlatformSyn.getStatus()!=ProgramSynConstants.programPubFail&& programPlatformSyn.getStatus()!=ProgramSynConstants.programModifyPubFail)
                    	    {
                                return "1:操作失败,该发布记录状态不正确";
                    	    }
                    	    break;
                    	
                        case 3:  //取消发布
                    	    if (programPlatformSyn.getStatus()!=ProgramSynConstants.programPubSUCESS && programPlatformSyn.getStatus()!=ProgramSynConstants.programModifyPubFail)
                    	    {
                                return "1:操作失败,该发布记录状态不正确";
                    	    }
                    	    break; 
                    	
                        default:
                    }
                    /**
                     * 获取关联目标网元及状态
                     * */
                    List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
                    ProgramTargetSyn programTargetSyn = new ProgramTargetSyn();
                    programTargetSyn.setRelateindex(programPlatformSyn.getSyncindex());
                    programTargetSynList = programTargetSynDS.getProgramTargetSynByCond(programTargetSyn);
                    if (programTargetSynList == null||programTargetSynList.size()==0)
                    {
                        return "1:操作失败,获取目标网元发布记录失败";
                    }
                    
                    if (synType ==1)
                    {
                    	boolean targetstatus = false;
                    	for (ProgramTargetSyn programTargetSynObj:programTargetSynList)
                    	{
                    		if (programTargetSynObj.getStatus()==0||programTargetSynObj.getStatus()==400||programTargetSynObj.getStatus()==500)
                    		{
                    			targetstatus = true;
                    		}
                    	}
                    	if (!targetstatus)
                    	{
                            return "1:操作失败,目标网元发布记录状态不正确";

                    	}
                    }
                    else
                    {
                    	boolean targetstatus = false;
                    	for (ProgramTargetSyn programTargetSynObj:programTargetSynList)
                    	{
                    		if (programTargetSynObj.getStatus()==300||programTargetSynObj.getStatus()==500)
                    		{
                    			targetstatus = true;
                    		}
                    	}
                    	if (!targetstatus)
                    	{
                            return "1:操作失败,目标网元发布记录状态不正确";

                    	}
                    }
                }
        	    
        	    if (type == 3) //针对单个网元进行发布
        	    {
                    List<ProgramTargetSyn> programTargetSynListTmp = new ArrayList<ProgramTargetSyn>();
                    ProgramTargetSyn programTargetSyntmp = new ProgramTargetSyn();
                    programTargetSyntmp.setTargetindex(Long.parseLong(targetindex));
                    programTargetSyntmp.setProgramindex(Long.parseLong(programindex));
                    programTargetSynListTmp = programTargetSynDS.getProgramTargetSynByCond(programTargetSyntmp);
                    if (programTargetSynListTmp ==null ||programTargetSynListTmp.size()==0 )
                    {
                        return "1:操作失败,获取目标网元发布记录失败";
                    }
                    switch(synType)
                    {
                        case 1:  //发布
                    	    if (programTargetSynListTmp.get(0).getStatus()!=ProgramSynConstants.programTobePub && 
                    	    programTargetSynListTmp.get(0).getStatus()!=ProgramSynConstants.programPubFail&&
                    	    programTargetSynListTmp.get(0).getStatus()!=ProgramSynConstants.programModifyPubFail)
                    	    {
                                return "1:操作失败,该发布记录状态不正确";
                    	    }
                    	    break;
                    	
                        case 3:  //取消发布
                    	    if (programTargetSynListTmp.get(0).getStatus()!=ProgramSynConstants.programPubSUCESS 
                    	    && programTargetSynListTmp.get(0).getStatus()!=ProgramSynConstants.programModifyPubFail)
                    	    {
                                return "1:操作失败,该发布记录状态不正确";
                    	    }
                    	    break;                   	
                        default:
                    }
        	    }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            throw e;
        }
         return "success";
    }        

    public String delPublishProgramPlatformSyn(String programindex, int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        boolean sucessPub =false ;
        String rtnPubStr = null; 
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{
            String initresult="";
            String initBindresult="";
            ProgramPlatformSyn programPlatformSynTmp=new ProgramPlatformSyn();
            programPlatformSynTmp.setProgramindex(Long.parseLong(programindex));
            int platformType = 1;
            if (type == 1) //发布到3.0平台
            {
            	programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSynTmp);
            	platformType=2;
            }
	    	
            if (type == 2) //发布到2.0平台
            {
            	programPlatformSynTmp=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSynTmp);
	    	}
            initresult =initData(programindex,type,synType,"platform"); //判断发布需满足的基本条件
            if(!ProgramSynConstants.SUCCESS.equals(initresult)){
                return initresult;
            }
            List<ProgramTargetSyn> programTargetSynList = new ArrayList<ProgramTargetSyn>();
            ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
            programTargetSynObj.setObjectindex(Long.parseLong(programindex));          
            programTargetSynObj.setObjecttype(3);
            programTargetSynList = programTargetSynDS.getProgramTargetSynByCond(programTargetSynObj);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            cntTargetSyncTemp.setObjectindex(Long.parseLong(programindex));
            List<CmsMovie> movieTmpList = (List<CmsMovie>)map.get("movieList");

            CmsProgram programtemp =new CmsProgram(); // 创建发布的内容对象
            programtemp.setProgramindex(Long.valueOf(programindex));
            programtemp = cmsProgramDS.getCmsProgram(programtemp);  
            int stat = 1;
            int tgType = 2;
            if (type == 1) //发布到3.0平台
            {               
                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                tgType = getdelTaskstatus(programTargetSynList);
                
                Long targetindexEpg = null;
                Long targetindexBms = null;
                String tgtidEpg = "";
                String tgtidBms = "";
                String bindResult = ""; 
                for (ProgramTargetSyn targetsysSyn:programTargetSynList)
                {
                    if (targetsysSyn.getTargettype() == 1)
                    {
                        targetindexBms = targetsysSyn.getTargetindex();
                        tgtidBms = targetsysSyn.getTargetid();
                    }
                    if (targetsysSyn.getTargettype() == 2)
                    {
                        targetindexEpg = targetsysSyn.getTargetindex();
                        tgtidEpg = targetsysSyn.getTargetid();
                    }
                }
                if (tgType == 2)
                {
                    bindResult =initCheckproBind(programindex,targetindexEpg); //判断发布需满足的基本条件
                    if(!ProgramSynConstants.SUCCESS.equals(bindResult)) //epg有关联关系已发布，不能取消平台的发布
                    {
                        return "1:操作失败,"+TARGETSYSTEM+tgtidEpg+": "+bindResult;
                    }
                }
                if (tgType == 1)
                {
                    bindResult =initCheckproBind(programindex,targetindexBms); //判断发布需满足的基本条件
                    if(!ProgramSynConstants.SUCCESS.equals(bindResult)) //bms有关联关系已发布，不能取消平台的发布
                    {
                        return "1:操作失败,"+TARGETSYSTEM+tgtidBms+": "+bindResult;
                    }
                }                
            	for (ProgramTargetSyn targetsysSyn:programTargetSynList)
            	{
            	    int targettype =targetsysSyn.getTargettype();
            		boolean isstatus = true;
            		if (targetsysSyn.getTargetstatus()!=0)
            		{
                        resultPubStr.append(TARGETSYSTEM
               	    	+targetsysSyn.getTargetid()+": "+"目标系统状态不正确"+"  ");    
                	    isstatus = false;
            		}
            		if (isstatus)
            		{
    				boolean ispub = true;
    				boolean isxml = true;
                	if (targetsysSyn.getStatus() == 300 || targetsysSyn.getStatus() == 500 )
                    {
                   	    if (targettype ==1 )  //BMS系统
                	    {
    				        initBindresult =initCheckproBind(programindex,targetsysSyn.getTargetindex()); //判断发布需满足的基本条件

    	    	            if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
                    		{
                    	        resultPubStr.append(TARGETSYSTEM
                   	    		 +targetsysSyn.getTargetid()+": "+
                                   initBindresult+"  ");    
                    	         ispub = false;
                		   }
    	                   if (ispub)
                   	       {
                               String  bmsxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3); //新增发布
                               if (null == bmsxml)
                               {
                               	   isxml = false;
                           	       resultPubStr.append(DELPUBTARGETSYSTEM
                         	    		 +targetsysSyn.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                         +"  ");                               
                           	   }
                               if (isxml)
                               {
                                   String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                   Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                   Long targetindex =targetsysSyn.getTargetindex();
                                                         
                                   Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                   Long objectindex = targetsysSyn.getProgramindex();
                                   String objectid = targetsysSyn.getProgramid();
                                   String objectcode = programtemp.getCpcontentid();
                                   insertObjectRecord( syncindex, index, objectindex, objectid,3, targetindex, synType,objectcode,1);  //插入对象日志发布表
                                   if (targettype == tgType)
                                   {
                                       stat = 1;
                                   }
                                   else
                                   {
                                       stat = 0;
                                   }
                                   insertSyncTask(index,bmsxml,correlateidBMS,targetindex,batchid,stat); //插入同步任务

                                   cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());                        
                                   cntTargetSyncTemp.setOperresult(70);
                                   cntTargetSyncTemp.setStatus(200);
                                   cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);  
                                   sucessPub=true;
                                   CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), 
                                           CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                           CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                                           CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                           	       resultPubStr.append(DELPUBTARGETSYSTEM
                           	    		+targetsysSyn.getTargetid()+delPub+": "
                                           +operSucess+"  "); 
                               }
                            }
                         }
                       	
                    	else if (targettype ==2 )  //EPG系统
                	    {
            				initBindresult =initCheckproBind(programindex,targetsysSyn.getTargetindex()); //判断发布需满足的基本条件
            	            if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
                    		{
                    	        resultPubStr.append(TARGETSYSTEM
                   	    		 +targetsysSyn.getTargetid()+": "+
                                   initBindresult+"  ");    
                    	         ispub = false;
                		    }
            	            if (ispub)
                    	    {
                           	    String  epgxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3);
                                if (null == epgxml)
                                {
                                    isxml = false;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                          	            +targetsysSyn.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                        +"  ");                               
                                }
                                if (isxml)
                                {
                                    String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =targetsysSyn.getTargetindex();
                                    
                                    //program 对象对象日志记录
                                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                    Long objectindex = targetsysSyn.getProgramindex();
                                    String objectid = targetsysSyn.getProgramid();
                                    String objectcode = programtemp.getCpcontentid();
                                    insertObjectRecord( syncindex, index, objectindex, objectid, 3,targetindex,  synType,objectcode,1);
                                    
                                    for (int i =0; i<movieTmpList.size(); i++)
                                    {
                            	        //movie 对象日志记录
                                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();

                                        String objectidMovie = movieTmpList.get(i).getMovieid();
                                        objectcode = movieTmpList.get(i).getCpcontentid();
                                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,targetindex,  synType,objectcode,1);  //插入对象日志发布表

                                        //Mapping 对象日志记录
                                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        String objectidMapping = movieTmpList.get(i).getMappingid();
                                        String elementid = objectidMovie;
                                        String  parentid = objectid;
                                        insertObjectMappingRecord( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, parentid,targetindex,synType);
                                    } 
                                    if (targettype == tgType)
                                    {
                                        stat = 1;
                                    }
                                    else
                                    {
                                        stat = 0;
                                    }
                                    insertSyncTask(index,epgxml,correlateidEPG,targetindex,batchid,stat);  // 插入同步任务

                                    cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());
                                    cntTargetSyncTemp.setOperresult(70);
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态     
                                    CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), 
                                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub=true;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                                  	    	+targetsysSyn.getTargetid()+delPub+": "
                                            +operSucess+"  "); 
                                }
                    	    }
                	    }
                        else if (targettype ==3 )  //CDN系统
                    	{
            				initBindresult =initCheckproBind(programindex,targetsysSyn.getTargetindex()); //判断发布需满足的基本条件
            	            if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
                    		{
                    	        resultPubStr.append(TARGETSYSTEM
                   	    		 +targetsysSyn.getTargetid()+": "+
                                   initBindresult+"  ");    
                    	         ispub = false;
                		    }
            	            if (ispub)
                        	{
                           		String  cdnxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, targettype, 3, 3);
                                if (null == cdnxml)
                                {
                                    isxml = false;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                          	            +targetsysSyn.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                        +"  ");                               
                                }  
                                if (isxml)
                                {
                                    String correlateidCDN = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =targetsysSyn.getTargetindex();
                                    for (int i =0; i<movieTmpList.size(); i++)
                                    {
                                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                                        String objectidMovie = movieTmpList.get(i).getMovieid();
                                        String objectcode = movieTmpList.get(i).getCpcontentid();
                                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,targetindex,  synType,objectcode,1);  //插入对象日志发布表
                                    }
                                    if (targettype == tgType)
                                    {
                                        stat = 1;
                                    }
                                    else
                                    {
                                        stat = 0;
                                    }                                    
                                    insertSyncTask(index,cdnxml,correlateidCDN,targetindex,batchid,stat);
                                    cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());                        
                                    cntTargetSyncTemp.setOperresult(70);                         
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp);   //修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), 
                                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub=true;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                                  	    	+targetsysSyn.getTargetid()+delPub+": "
                                            +operSucess+"  ");
                                }
                        	}
                    	}
                	}
                }	
            }
            }
            else if (type == 2) //发布到2.0平台
            {
                for(ProgramTargetSyn targetsysSyn:programTargetSynList)
                {
                    int targettype =targetsysSyn.getTargettype();
            		boolean isstatus = true;
            		if (targetsysSyn.getTargetstatus()!=0)
            		{
                        resultPubStr.append(TARGETSYSTEM
               	    	+targetsysSyn.getTargetid()+": "+"目标系统状态不正确"+"  ");    
                	    isstatus = false;
            		}
            		if (isstatus)
                {
    				boolean ispub = true;
    				boolean isxml = true;
                	if ((targettype==0||targettype==10)&&(targetsysSyn.getStatus() == 300 || targetsysSyn.getStatus() == 500 ))
                	{
    				        initBindresult =initCheckproBind(programindex,targetsysSyn.getTargetindex()); //判断发布需满足的基本条件
    	                    if(!ProgramSynConstants.SUCCESS.equals(initBindresult))
    	                    {
                    	        resultPubStr.append(TARGETSYSTEM
                   	    		 +targetsysSyn.getTargetid()+": "+
                                   initBindresult+"  ");    
                    	         ispub = false;   
                	        }
    	                    if (ispub)
                            {
                                String	wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, targettype, 3, 3);
                                if (null == wg18xml)
                                {
                                    isxml = false;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                          	            +targetsysSyn.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                        +"  ");                               
                                } 
                                if (isxml)
                                {
                                    String correlateidtarget2 = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =targetsysSyn.getTargetindex();
                                    String objectcode = programtemp.getCpcontentid();
                                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                    Long objectindex = targetsysSyn.getProgramindex();
                                    String objectid = targetsysSyn.getProgramid();
                                    insertObjectRecord( syncindex, index, objectindex, objectid, 3,targetindex, synType,objectcode,2);
                                    for (int i =0; i<movieTmpList.size(); i++)
                                    {
                                        Long syncindexMovie = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        Long objectindexMovie = movieTmpList.get(i).getMovieindex();
                                        String objectidMovie = movieTmpList.get(i).getMovieid();
                                        objectcode = movieTmpList.get(i).getCpcontentid();
                                        insertObjectRecord( syncindexMovie, index, objectindexMovie, objectidMovie, 7,targetindex, synType,objectcode,2);
                                        
                                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        String objectidMapping = movieTmpList.get(i).getMappingid();
                                        String elementid = objectidMovie;
                                        String  parentid = objectid;  
                                        String elementcode = objectcode;
                                        String parentcode = programtemp.getCpcontentid();
                                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindexMovie,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                    } 
                                    Long batchid_plat2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                                                                        
                                    insertSyncTask(index,wg18xml,correlateidtarget2,targetindex,batchid_plat2,stat);
                                    
                                    cntTargetSyncTemp.setSyncindex(targetsysSyn.getSyncindex());
                                    cntTargetSyncTemp.setOperresult(70);
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态     
                                    CommonLogUtil.insertOperatorLog(programtemp.getProgramid()+","+targetsysSyn.getTargetid(), 
                                            CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                            CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAM_PUBLISH_DEL_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub=true;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                                  	    	+targetsysSyn.getTargetid()+delPub+": "
                                            +operSucess+"  ");
                                }
                            }
                    	 }

                 }
            }
            }
            
            // 更新平台发布状态         
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(Long.parseLong(programindex));
                cntPlatformSyncTmp.setSyncindex(programPlatformSynTmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(200);
                cntPlatformSyncTmp.setPlatform(platformType);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
    	}
    	catch (Exception e) 
    	{
            e.printStackTrace();
            throw e;
        } 
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;    	
    }    

    /**
     * 预下线
     */
    public String preOfflineProgram(String programindex,int type)
    {
        log.debug("preOfflineProgram starting...");
        String result = "0:操作成功";
        try
        {
            // 获取取消发布的内容对象
            CmsProgram program = new CmsProgram();
            program.setProgramindex(Long.parseLong(programindex));
            program = cmsProgramDS.getCmsProgram(program);
            if (program == null)
            {
                return "1:内容不存在";
            }
            else if (program.getStatus() != ProgramSynConstants.programAuditSucess)
            {
                return "1:操作失败,内容状态不正确";
            }
            
            ProgramPlatformSyn programPlatformSyn=new ProgramPlatformSyn();
            programPlatformSyn.setObjectindex(Long.parseLong(programindex));
	    	if (type ==1)  //点播发内容在3.0平台预下线
	    	{
                programPlatformSyn=programPlatformSynDS.getProgramPlatformSyn3(programPlatformSyn);
                if(programPlatformSyn==null)
                {
                    return "1:操作失败,该发布记录已不存在";
                }
	    	}            

	    	if (type ==2) //点播发内容在2.0平台预下线
	    	{
                programPlatformSyn=programPlatformSynDS.getProgramPlatformSyn2(programPlatformSyn);
                if(programPlatformSyn==null)
                {
                    return "1:操作失败,该发布记录已不存在";
                }
	    	}

    	    if (programPlatformSyn.getStatus()!=ProgramSynConstants.programPubSUCESS && programPlatformSyn.getStatus()!=ProgramSynConstants.programModifyPubFail)
    	    {
                return "1:操作失败,该发布记录状态不正确";
    	    }

    	    ProgramTargetSyn programTargetSyn = new ProgramTargetSyn();
            CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
            categoryProgramMap.setProgramid(program.getProgramid());
            List<CategoryProgramMap> categoryProgramMapList = ctgpgmapDS
                    .getCategoryProgramMapByCond(categoryProgramMap);

            if (categoryProgramMapList != null && categoryProgramMapList.size() > 0)
            {
            	for (CategoryProgramMap categoryProMap:categoryProgramMapList)
            	{
            		
            		programTargetSyn.setObjectindex(categoryProMap.getMapindex());
            		programTargetSyn.setObjecttype(26);
                    List<ProgramTargetSyn>  programTargetSynTempList = new ArrayList<ProgramTargetSyn>();
                    if(type == 1)
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);
                    }
                    else
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);
                    }
            		if (programTargetSynTempList!=null &&programTargetSynTempList.size()==0)
            		{
            			for (ProgramTargetSyn rogramTargetSyntmp:programTargetSynTempList)
            			{
            				if (rogramTargetSyntmp.getStatus()!=0&&rogramTargetSyntmp.getStatus()!=400) //关联关系在各个网元的发布状态不为待发布或发布失败
            				{
            	                return "1:内容已经和栏目关联,且已发布，请先取消发布";
            				}
            			}
            		}
            	}
            }
            ServiceProgramMap serviceProgramMap = new ServiceProgramMap();
            serviceProgramMap.setProgramid(program.getProgramid());
            List<ServiceProgramMap> serviceProgramMapList = serviceProgramMapDS
                    .getServiceProgramMapByCond(serviceProgramMap);
            if (serviceProgramMapList != null && serviceProgramMapList.size() > 0)
            {
            	for (ServiceProgramMap serviceProMap:serviceProgramMapList)
            	{
            		programTargetSyn.setObjectindex(serviceProMap.getMapindex());
            		programTargetSyn.setObjecttype(21);
                    List<ProgramTargetSyn>  programTargetSynTempList = new ArrayList<ProgramTargetSyn>();
                    if (type ==1)
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);
                    }
                    else
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);
                    }
            		if (programTargetSynTempList!=null &&programTargetSynTempList.size()==0)
            		{
            			for (ProgramTargetSyn programTargetSyntemp:programTargetSynTempList)
            			{
            				if (programTargetSyntemp.getStatus()!=0&&programTargetSyntemp.getStatus()!=400) //关联关系在各个网元的发布状态不为待发布或发布失败
            				{
            	                return "1:内容已经和服务关联,且已发布，请先取消发布";
            				}
            			}
            		}
            	}
            }
            ProgramCrmMap programCrmMap = new ProgramCrmMap();
            programCrmMap.setProgramid(program.getProgramid());
            List<ProgramCrmMap> programCrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
            if (programCrmMapList != null && programCrmMapList.size() > 0)
            {
            	for (ProgramCrmMap proCrmMap:programCrmMapList)
            	{
            		programTargetSyn.setObjectindex(proCrmMap.getMapindex());
            		programTargetSyn.setObjecttype(21);
                    List<ProgramTargetSyn>  programTargetSynTempList = new ArrayList<ProgramTargetSyn>();
                    if (type ==1)
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);
                    }
                    else
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);
                    }
            		if (programTargetSynTempList!=null &&programTargetSynTempList.size()==0)
            		{
            			for (ProgramTargetSyn programTargetSyntmp:programTargetSynTempList)
            			{
            				if (programTargetSyntmp.getStatus()!=0&&programTargetSyntmp.getStatus()!=400) //关联关系在各个网元的发布状态不为待发布或发布失败
            				{
            	                return "1:内容已经和角色关联,且已发布，请先取消发布";
            				}
            			}
            		}
            	}
            }
            
            ProgramPictureMap programPictureMap = new ProgramPictureMap();
            programPictureMap.setProgramid(program.getProgramid());
            List<ProgramPictureMap> pictureMapList = programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
            if (pictureMapList != null && pictureMapList.size() > 0)
            {
                for (ProgramPictureMap pictureMap : pictureMapList)
                {
                	programTargetSyn.setObjectindex(pictureMap.getMapindex());
                	programTargetSyn.setObjecttype(35);
                    List<ProgramTargetSyn>  programTargetSynTempList = new ArrayList<ProgramTargetSyn>();
                    if (type ==1)
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);
                    }
                    else
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);
                    }
            		if (programTargetSynTempList!=null &&programTargetSynTempList.size()==0)
            		{
            			for (ProgramTargetSyn programTargetSyntmp:programTargetSynTempList)
            			{
            				if (programTargetSyntmp.getStatus()!=0&&programTargetSyntmp.getStatus()!=400) //关联关系在各个网元的发布状态不为待发布或发布失败
            				{
            	                return "1:内容下有海报已经发布,请先取消发布海报";
            				}
            			}
            		}   
                }
            }
            
            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setProgramid(program.getProgramid());
            List<SeriesProgramMap> seriesProgramMapList = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
            if (seriesProgramMapList != null && seriesProgramMapList.size() > 0)
            {
                for (SeriesProgramMap seriesProMap : seriesProgramMapList)
                {
                	programTargetSyn.setObjectindex(seriesProMap.getMapindex());
                	programTargetSyn.setObjecttype(21);
                    List<ProgramTargetSyn>  programTargetSynTempList = new ArrayList<ProgramTargetSyn>();
                    if (type == 1)
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);
                    }
                    else
                    {
                        programTargetSynTempList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);
                    }
            		if (programTargetSynTempList!=null &&programTargetSynTempList.size()==0)
            		{
            			for (ProgramTargetSyn programTargetSyntemp:programTargetSynTempList)
            			{
            				if (programTargetSyntemp.getStatus()!=0&&programTargetSyntemp.getStatus()!=400) //关联关系在各个网元的发布状态不为待发布或发布失败
            				{
            	                return "1:内容已经被连续剧打包,且已发布，请先取消发布";
            				}
            			}
            		}   
                }
            }
            ProgramTargetSyn programTargetSynObj = new ProgramTargetSyn();
            List<ProgramTargetSyn>  cntTargetSyncTmpList = new ArrayList<ProgramTargetSyn>();
            programTargetSynObj.setObjectindex(Long.parseLong(programindex));
            if (type ==1)
            {
                cntTargetSyncTmpList =programTargetSynDS.getProgramTargetSynByCondIPTV3(programTargetSyn);

            }
            else
            {
                cntTargetSyncTmpList =programTargetSynDS.getProgramTargetSynByCondIPTV2(programTargetSyn);

            }
            
            if (cntTargetSyncTmpList==null||cntTargetSyncTmpList.size()==0)
            {
                return "1:操作失败,获取平台下目标网元失败";

            } 
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            
            for (ProgramTargetSyn programTargetSyntmp:cntTargetSyncTmpList) //更新网元状态为预下线
            {
            	cntTargetSyncTemp.setSyncindex(programTargetSyntmp.getSyncindex());
            	cntTargetSyncTemp.setStatus(600);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
            }
            
            CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
            cntPlatformSyncTmp.setSyncindex(programPlatformSyn.getSyncindex());
            cntPlatformSyncTmp.setStatus(600);
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp); 

            // 写操作日志
            CommonLogUtil.insertOperatorLog(program.getProgramid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                    CommonLogConstant.OPERTYPE_PROGRAM_PREOFFLINE, CommonLogConstant.OPERTYPE_PROGRAM_PREOFFLINE_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e);
        }

        log.debug("preOfflineProgram end");

        return result;
    }
 
    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid,int status)
    {
    	CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(status);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
 
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype, Long targetindex,int synType,String objectcode,int platType )
    {
    	ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncRecord.setSyncindex(syncindex);
        	objectSyncRecord.setTaskindex(taskindex);
        	objectSyncRecord.setObjectindex(objectindex);
        	objectSyncRecord.setObjectid(objectid);
        	objectSyncRecord.setDestindex(targetindex);
        	objectSyncRecord.setObjecttype(objecttype);
        	objectSyncRecord.setActiontype(synType);
        	objectSyncRecord.setStarttime(dateformat.format(new Date()));
        		objectSyncRecord.setObjectcode(objectcode);
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }   
    private void insertObjectMappingRecord(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(32);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
    private void insertObjectMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,String elementcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(32);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elementcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     


    public TableDataInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            programPictureMap.setCreateStarttime(DateUtil2.get14Time(programPictureMap.getCreateStarttime()));
            programPictureMap.setCreateEndtime(DateUtil2.get14Time(programPictureMap.getCreateEndtime()));
            programPictureMap.setOnlinetimeStartDate(DateUtil2.get14Time(programPictureMap.getOnlinetimeStartDate()));
            programPictureMap.setOnlinetimeEndDate(DateUtil2.get14Time(programPictureMap.getOnlinetimeEndDate()));
            programPictureMap.setCntofflinestarttime(DateUtil2.get14Time(programPictureMap.getCntofflinestarttime()));
            programPictureMap.setCntofflineendtime(DateUtil2.get14Time(programPictureMap.getCntofflineendtime()));
            if(programPictureMap.getPictureid()!=null&&!"".equals(programPictureMap.getPictureid())){
            	programPictureMap.setPictureid(EspecialCharMgt.conversion(programPictureMap.getPictureid()));
            }           
            dataInfo = programPictureMapDS.pageInfoQueryproPicMap(programPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }    
    public TableDataInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            dataInfo = programPictureMapDS.pageInfoQueryproPicTargetMap(programPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }    
    public String batchPublishOrDelPublishproPic(List<ProgramPictureMap> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
            if (operType.equals("0"))  //批量发布
            {
                for (ProgramPictureMap programPictureMap : list)
                {
                    message = publishproPicMapPlatformSyn(programPictureMap.getMapindex().toString(), programPictureMap.getProgramid(),programPictureMap.getPictureid(),type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), programPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), programPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                for (ProgramPictureMap programPictureMap : list)
                {
                    message = delPublishproPicMapPlatformSyn(programPictureMap.getMapindex().toString(), programPictureMap.getProgramid(),programPictureMap.getPictureid(),type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), programPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), programPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("ProgramPlatformSynLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
    
    public String publishproPicMapPlatformSyn(String mapindex, String programid,String pictureid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub = false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		ProgramPictureMap programPictureMap = new ProgramPictureMap();
    		programPictureMap.setMapindex(Long.parseLong(mapindex));
    		programPictureMap.setProgramid(programid);
    		programPictureMap.setPictureid(pictureid);
			List<ProgramPictureMap> programPictureMapList = new ArrayList<ProgramPictureMap>();
			programPictureMapList =programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
			if (programPictureMapList == null ||programPictureMapList.size()==0)
			{
				return "1:操作失败,点播内容关联海报记录不存在";
			}
    		String initresult="";
    		initresult =initDataPic(programPictureMapList.get(0),"platform",type,1);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(35);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(programPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}  
	    	propicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(35);
	    	propicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ProgramPictureMap programPictureMapObj=new ProgramPictureMap();
            int  platformType = 1;
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(programPictureMap.getMapindex());
            int stat = 1;
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
    	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	propicmap.put("pictureMap", programPictureMapList);
    	    	int targettype;
    			for (ProgramPictureMap programPictureMapTmp:programPictureMapList )
    			{
    			    targettype = programPictureMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (programPictureMapTmp.getTargetstatus()!=0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                	    		 +programPictureMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                +"  ");
                   	    isstatus = false;
    				}
    				if (isstatus)
    			    {
    				boolean isxml = true;
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    	            synType = 1;
    				boolean ispub = true;
    				cntTargetSync.setObjectindex(programPictureMapTmp.getProgramindex());
    				cntTargetSync.setTargetindex(programPictureMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
               	    		 +programPictureMapTmp.getTargetid()
                               +"  ");    	
                	    ispub = false;
                	}
    				if (ispub)
    				{
                        int synTargetType =1;
                        if (programPictureMapTmp.getStatus()== 500) //修改发布    
                        {
                    		synTargetType =2;
            	            synType = 2;
                        }
                       	if (targettype ==2&&(programPictureMapTmp.getStatus()== 0||
                       			programPictureMapTmp.getStatus()== 400||programPictureMapTmp.getStatus()== 500))  //EPG系统
                       	{
                       		String xml;
                       		if (synTargetType==1)
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 1); //新增发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    		 +programPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		else
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 2); //修改发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    		 +programPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }                                
                       		}
                       		if(isxml)
                       		{
                                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                       		    
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                Long targetindex =programPictureMapTmp.getTargetindex();
                                insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                Long objectPicindex = programPictureMapTmp.getPictureindex();
                                String objectid = programPictureMapTmp.getPictureid();
                                String objectcode = programPictureMapTmp.getPicturecode();
                                insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = programPictureMapTmp.getMappingid();
                                String parentid = programPictureMapTmp.getPictureid();
                                String  elementid = programPictureMapTmp.getProgramid();
                                Long objectindex = programPictureMapTmp.getMapindex();
                                insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                                cntTargetSyncTemp.setSyncindex(programPictureMapTmp.getSyncindex());
                                if (synTargetType ==1)
                                {
                                	cntTargetSyncTemp.setOperresult(10);
                                }
                                else
                                {
                                	cntTargetSyncTemp.setOperresult(40);
                                }                            
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                CommonLogUtil.insertOperatorLog(programPictureMapTmp.getMappingid()+","+programPictureMapTmp.getTargetid(), 
                                		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                        CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                                sucessPub=true;
                                resultPubStr.append(PUBTARGETSYSTEM
                        	    		+programPictureMapTmp.getTargetid()+": "
                                        +operSucess+"  "); 
                       		}
                          }
                       }                       		
    			   } 
    		   }
    		   }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
       			   programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
       	    	   if (programPictureMapList == null || programPictureMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   propicmap.put("pictureMap", programPictureMapList);
       	    	   int targettype;
         			for (ProgramPictureMap programPictureMapTmp:programPictureMapList )
           			{
         			    targettype =programPictureMapTmp.getTargettype();
        				boolean isstatus = true;
        				if (programPictureMapTmp.getTargetstatus()!=0)
        				{
                       	    resultPubStr.append(PUBTARGETSYSTEM
                    	    		 +programPictureMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                    +"  ");
                       	    isstatus = false;
        				}
        				if (isstatus)
        				{
         				boolean isxml = true;
        	            synType = 1;
           	            CntTargetSync cntTargetSync = new CntTargetSync();    		
           				boolean ispub = true;
           				cntTargetSync.setObjectindex(programPictureMapTmp.getProgramindex());
           				cntTargetSync.setTargetindex(programPictureMapTmp.getTargetindex());
        				cntTargetSync.setObjecttype(3);
           				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
           				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
           				{
                       	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
                      	    		 +programPictureMapTmp.getTargetid()
                                      +"  ");    	
                       	    ispub = false;
                       	}
           				if (ispub)
           				{
                           int synTargetType =1;
                           if (programPictureMapTmp.getStatus()== 500) //修改发布    
                           {
                       		synTargetType =2;
            	            synType = 2;
                           }
                             if (programPictureMapTmp.getStatus()== 0||
                              			programPictureMapTmp.getStatus()== 400||programPictureMapTmp.getStatus()== 500)  //2.0平台网元
                              	{
                          		String xml;
                          		if (synTargetType==1)
                          		{
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 1); //新增发布
                                    if (null == xml)
                                    {
                                    	isxml = false;
                                   	    resultPubStr.append(PUBTARGETSYSTEM
                                 	    		 +programPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                 +"  ");
                                    }
                          		}
                          		else
                          		{
                                     xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 2); //修改发布
                                     if (null == xml)
                                     {
                                     	isxml = false;
                                    	    resultPubStr.append(PUBTARGETSYSTEM
                                  	    		 +programPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                  +"  ");
                                     }                              		
                               }
                          	   if (isxml)
                          	   {
                                   Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                          	       
                                   String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                   Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                   Long targetindex =programPictureMapTmp.getTargetindex();
                                   Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                   Long objectPicindex = programPictureMapTmp.getPictureindex();
                                   String objectid = programPictureMapTmp.getPictureid();
                                   String objectcode = programPictureMapTmp.getPicturecode();
                                   insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                                   Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                   String objectidMapping = programPictureMapTmp.getMappingid();
                                   String parentid = programPictureMapTmp.getPictureid();
                                   String  elementid = programPictureMapTmp.getProgramid();
                                   Long objectindex = programPictureMapTmp.getMapindex();
                                   String elementcode = programPictureMapTmp.getCpcontentid();
                                   String parentcode = objectcode;
                                   insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,
                                		   elementcode,parentcode,targetindex,synType);
                                   insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                                   cntTargetSyncTemp.setSyncindex(programPictureMapTmp.getSyncindex());
                                   if (synTargetType ==1)
                                   {
                                   	cntTargetSyncTemp.setOperresult(10);
                                   }
                                   else
                                   {
                                   	cntTargetSyncTemp.setOperresult(40);
                                   }                            
                                   cntTargetSyncTemp.setStatus(200);
                                   cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                   CommonLogUtil.insertOperatorLog(programPictureMapTmp.getMappingid()+","+programPictureMapTmp.getTargetid(), 
                                		   CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                           CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_INFO,
                                           CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                   sucessPub=true;
                                   resultPubStr.append(PUBTARGETSYSTEM
                           	    		+programPictureMapTmp.getTargetid()+": "
                                           +operSucess+"  "); 
                                           
                          	   }

                               }
                          }                          		
           		    } 
    	       }
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    }   
    
    public String delPublishproPicMapPlatformSyn(String mapindex, String programid,String pictureid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		ProgramPictureMap programPictureMap = new ProgramPictureMap();
    		programPictureMap.setMapindex(Long.parseLong(mapindex));
    		programPictureMap.setProgramid(programid);
    		programPictureMap.setPictureid(pictureid);
			List<ProgramPictureMap> programPictureMapList = new ArrayList<ProgramPictureMap>();
			programPictureMapList =programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
			if (programPictureMapList == null ||programPictureMapList.size()==0)
			{
				return "1:操作失败,点播内容关联海报记录不存在";
			}
    		String initresult="";
    		initresult =initDataPic(programPictureMapList.get(0),"platform",type,3);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(35);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(programPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}    		
	    	propicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(35);
	    	propicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ProgramPictureMap programPictureMapObj=new ProgramPictureMap();
            int  platformType = 1;
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(programPictureMap.getMapindex());
			int stat = 1;
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
    	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	propicmap.put("pictureMap", programPictureMapList);
    			for (ProgramPictureMap programPictureMapTmp:programPictureMapList )
    			{
    			    int targettype = programPictureMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (programPictureMapTmp.getTargetstatus() != 0)
    				{
             	       resultPubStr.append(DELPUBTARGETSYSTEM
           	    		 +programPictureMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                           +"  ");  
             	       isstatus = false;
    				}
    				if (isstatus)
    			   {
    				boolean isxml =true ;
                   	if (targettype ==2&&(programPictureMapTmp.getStatus()== 300||programPictureMapTmp.getStatus()== 500))  //EPG系统
                   	{
                   		String xml;
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 3); //取消发布到EPG
                        if (null == xml)
                        {
                        	   isxml = false;
                    	       resultPubStr.append(DELPUBTARGETSYSTEM
                  	    		 +programPictureMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                  +"  ");                               
                    	}  
                        if (isxml)
                        {
                            Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                            
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                            Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                            Long targetindex =programPictureMapTmp.getTargetindex();
                            insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                            Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                            Long objectPicindex = programPictureMapTmp.getPictureindex();
                            String objectid = programPictureMapTmp.getPictureid();
                            String objectcode = programPictureMapTmp.getPicturecode();
                            insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                            Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                            String objectidMapping = programPictureMapTmp.getMappingid();
                            String parentid = programPictureMapTmp.getPictureid();
                            String  elementid = programPictureMapTmp.getProgramid();
                            Long objectindex = programPictureMapTmp.getMapindex();
                            insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                            cntTargetSyncTemp.setSyncindex(programPictureMapTmp.getSyncindex());
                            cntTargetSyncTemp.setOperresult(70);
                            cntTargetSyncTemp.setStatus(200);
                            cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                            CommonLogUtil.insertOperatorLog(programPictureMapTmp.getMappingid()+","+programPictureMapTmp.getTargetid(),
                            		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                    CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                            sucessPub=true;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                    	    	+programPictureMapTmp.getTargetid()+delPub+": "
                                +operSucess+"  ");  
                        }

                   }                       		
    			} 
    		  }
    	    }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
       			   programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
       	    	   if (programPictureMapList == null || programPictureMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   propicmap.put("pictureMap", programPictureMapList);
         			for (ProgramPictureMap programPictureMapTmp:programPictureMapList )
           			{
         			    int targettype =programPictureMapTmp.getTargettype();
        				boolean isstatus = true;
        				if (programPictureMapTmp.getTargetstatus() != 0)
        				{
                 	       resultPubStr.append(DELPUBTARGETSYSTEM
               	    		 +programPictureMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                               +"  ");  
                 	       isstatus = false;
        				}
        				if (isstatus)
        				{
         				boolean isxml =true ;
                        if (programPictureMapTmp.getStatus()== 300||programPictureMapTmp.getStatus()== 500)  //2.0平台网元
                        {
                              String xml;
                              xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 3); //新增发布
                              if (null == xml)
                              {
                              	   isxml = false;
                          	       resultPubStr.append(DELPUBTARGETSYSTEM
                        	    		 +programPictureMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                        +"  ");                               
                          	  } 
                              if (isxml)
                              {
                              Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                    
                              String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                              Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                              Long targetindex =programPictureMapTmp.getTargetindex();
                              insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                              Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                              Long objectPicindex = programPictureMapTmp.getPictureindex();
                              String objectid = programPictureMapTmp.getPictureid();
                              String objectcode = programPictureMapTmp.getPicturecode();
                              insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                              Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                              String objectidMapping = programPictureMapTmp.getMappingid();
                              String parentid = programPictureMapTmp.getPictureid();
                              String  elementid = programPictureMapTmp.getProgramid();
                              Long objectindex = programPictureMapTmp.getMapindex();
                              String elementcode = programPictureMapTmp.getCpcontentid();
                              String parentcode = objectcode;
                              insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                              cntTargetSyncTemp.setSyncindex(programPictureMapTmp.getSyncindex());
                              cntTargetSyncTemp.setOperresult(70);                          
                              cntTargetSyncTemp.setStatus(200);
                              cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                  CommonLogUtil.insertOperatorLog(programPictureMapTmp.getMappingid()+","+programPictureMapTmp.getTargetid(),
                                  		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                      CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO,
                                      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                              sucessPub=true;
                          	     resultPubStr.append(DELPUBTARGETSYSTEM
                          	    	+programPictureMapTmp.getTargetid()+delPub+": "
                                      +operSucess+"  "); 
                              }
                            }
           				} 
    	       }
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    }   
    
    public String publishproPicMapTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{ 
    		boolean isxml = true;
    		ProgramPictureMap programPictureMap = new ProgramPictureMap();
    		programPictureMap.setMapindex(Long.parseLong(mapindex));
    		programPictureMap.setTargetindex(Long.parseLong(targetindex));
    		programPictureMap.setObjecttype(35);
			List<ProgramPictureMap> programPictureMapList = new ArrayList<ProgramPictureMap>();
			programPictureMapList =programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
			if (programPictureMapList == null ||programPictureMapList.size()==0)
			{
				return "1:操作失败,点播内容关联海报记录不存在";
			}
    		int synType = 1;
            String initresult="";
    		initresult =initDataPic(programPictureMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(programPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}     		
	    	propicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(35);
	    	propicmap.put("type", typeList);
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(35);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ProgramPictureMap programPictureMapObj=new ProgramPictureMap();
            int  platformType = 1;
            int stat =1;
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programPictureMapObj.setTargetindex(Long.parseLong(targetindex));
    			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
    	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    			if (programPictureMapList.get(0).getTargetstatus()!=0)
    			{
    				return "1:操作失败,目标系统状态不正确";
    			}
    			int targettype = programPictureMapList.get(0).getTargettype();
    	    	propicmap.put("pictureMap", programPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
				cntTargetSync.setObjectindex(programPictureMapList.get(0).getProgramindex());
				cntTargetSync.setTargetindex(programPictureMapList.get(0).getTargetindex());
				cntTargetSync.setObjecttype(3);
				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
				{
                    return "1:操作失败,该点播内容没有发布到该网元";
            	}
                int synTargetType =1;
                if (programPictureMapList.get(0).getStatus()== 500) //修改发布    
                {
            		synTargetType =2;
            		synType = 2;
                }
               	if (targettype ==2)  //EPG系统
               	{
               		String xml;
               		if (synTargetType==1)
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 1); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 2); //修改发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}    					
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                    
                    insertSyncTask(index,xml,correlateid,Long.valueOf(targetindex),batchid,stat); //插入同步任务
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long objectPicindex = programPictureMapList.get(0).getPictureindex();
                    String objectid = programPictureMapList.get(0).getPictureid();
                    String objectcode = programPictureMapList.get(0).getPicturecode();
                    insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = programPictureMapList.get(0).getMappingid();
                    String parentid = programPictureMapList.get(0).getPictureid();
                    String  elementid = programPictureMapList.get(0).getProgramid();
                    Long objectindex = programPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    cntTargetSyncTemp.setSyncindex(programPictureMapList.get(0).getSyncindex());
                    if (synTargetType ==1)
                    {
                    	cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                    	cntTargetSyncTemp.setOperresult(40);
                    }                            
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programPictureMapList.get(0).getMappingid()+","+
                            programPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			programPictureMapObj.setTargetindex(Long.parseLong(targetindex));
        			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
        	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        			if (programPictureMapList.get(0).getTargetstatus()!=0)
        			{
        				return "1:操作失败,目标系统状态不正确";
        			}
        			int targettype = programPictureMapList.get(0).getTargettype();
        	    	propicmap.put("pictureMap", programPictureMapList);
                    CntTargetSync cntTargetSync = new CntTargetSync();
    				cntTargetSync.setObjectindex(programPictureMapList.get(0).getProgramindex());
    				cntTargetSync.setTargetindex(programPictureMapList.get(0).getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                        return "1:操作失败,该点播内容没有发布到该网元";
                	}
                    int synTargetType =1;
                    if (programPictureMapList.get(0).getStatus()== 500) //修改发布    
                    {
                		synTargetType =2;
        	            synType = 2;
                    }

                   		String xml;
               			if (synTargetType ==1)
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 1); //新增发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }               			}
               			else
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 2); //修改发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }                			
                        }
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                        
                        insertSyncTask(index,xml,correlateid,Long.valueOf(targetindex),batchid,stat); //插入同步任务
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectPicindex = programPictureMapList.get(0).getPictureindex();
                        String objectid = programPictureMapList.get(0).getPictureid();
                        String objectcode = programPictureMapList.get(0).getPicturecode();
                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programPictureMapList.get(0).getMappingid();
                        String parentid = programPictureMapList.get(0).getPictureid();
                        String  elementid = programPictureMapList.get(0).getProgramid();
                        Long objectindex = programPictureMapList.get(0).getMapindex();
                        String elementcode = programPictureMapList.get(0).getCpcontentid();
                        String parentcode = objectcode;
                        insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        cntTargetSyncTemp.setSyncindex(programPictureMapList.get(0).getSyncindex());
                        if (synTargetType ==1)
                        {
                        	cntTargetSyncTemp.setOperresult(10);
                        }
                        else
                        {
                        	cntTargetSyncTemp.setOperresult(40);
                        }                            
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(programPictureMapList.get(0).getMappingid()+","
                                +programPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                    }

   			   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(programPictureMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result; 
    }
    
    public String delPublishproPicMapTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{
    		boolean isxml = true;
    		ProgramPictureMap programPictureMap = new ProgramPictureMap();
    		programPictureMap.setMapindex(Long.parseLong(mapindex));
    		programPictureMap.setTargetindex(Long.parseLong(targetindex));
    		programPictureMap.setObjecttype(35);
			List<ProgramPictureMap> programPictureMapList = new ArrayList<ProgramPictureMap>();
			programPictureMapList =programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
			if (programPictureMapList == null ||programPictureMapList.size()==0)
			{
				return "1:操作失败,点播内容关联海报记录不存在";
			}
            String initresult="";
            int synType = 3;
    		initresult =initDataPic(programPictureMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(programPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());    			
    		} 
	    	propicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(35);
	    	propicmap.put("type", typeList);
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(35);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ProgramPictureMap programPictureMapObj=new ProgramPictureMap();
            int  platformType = 1;
            int stat =1;
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programPictureMapObj.setTargetindex(Long.parseLong(targetindex));
    			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
    	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    			if (programPictureMapList.get(0).getTargetstatus()!=0)
    			{
    				return "1:操作失败,目标系统状态不正确";
    			}
    			int targettype = programPictureMapList.get(0).getTargettype();
    	    	propicmap.put("pictureMap", programPictureMapList);
               	if (programPictureMapList.get(0).getTargettype() ==2)  //EPG系统
               	{
               		String xml;
                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(propicmap, targettype, 10, 3); //新增发布到EPG
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                    
                    insertSyncTask(index,xml,correlateid,Long.valueOf(targetindex),batchid,stat); //插入同步任务
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long objectPicindex = programPictureMapList.get(0).getPictureindex();
                    String objectid = programPictureMapList.get(0).getPictureid();
                    String objectcode = programPictureMapList.get(0).getPicturecode();
                    insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = programPictureMapList.get(0).getMappingid();
                    String parentid = programPictureMapList.get(0).getPictureid();
                    String  elementid = programPictureMapList.get(0).getProgramid();
                    Long objectindex = programPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    cntTargetSyncTemp.setSyncindex(programPictureMapList.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);                          
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programPictureMapList.get(0).getMappingid()+
                    		","+programPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			programPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			programPictureMapObj.setTargetindex(Long.parseLong(targetindex));
        			programPictureMapList = programPictureMapDS.getProgramnormalPictureMapByCond(programPictureMapObj);
        	    	if (programPictureMapList == null || programPictureMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	
        			if (programPictureMapList.get(0).getTargetstatus()!=0)
        			{
        				return "1:操作失败,目标系统状态不正确";
        			}
        		    int targettype = programPictureMapList.get(0).getTargettype();
        	    	propicmap.put("pictureMap", programPictureMapList);

                   		String xml;
                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(propicmap, targettype, 10, 3); //取消发布
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                       
                        insertSyncTask(index,xml,correlateid,Long.valueOf(targetindex),batchid,stat); //插入同步任务
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectPicindex = programPictureMapList.get(0).getPictureindex();
                        String objectid = programPictureMapList.get(0).getPictureid();
                        String objectcode = programPictureMapList.get(0).getPicturecode();
                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programPictureMapList.get(0).getMappingid();
                        String parentid = programPictureMapList.get(0).getPictureid();
                        String  elementid = programPictureMapList.get(0).getProgramid();
                        Long objectindex = programPictureMapList.get(0).getMapindex();
                        String elementcode = programPictureMapList.get(0).getCpcontentid();
                        String parentcode = objectcode;
                        insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        cntTargetSyncTemp.setSyncindex(programPictureMapList.get(0).getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);      
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(programPictureMapList.get(0).getMappingid()+","+
                        		programPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                    }
              
   			   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(programPictureMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result; 
    }
    public String initDataPic(ProgramPictureMap programPictureMap,String targetindex,int platfType,int synType)throws Exception
    {
    	try
    	{
    		CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(35);
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programPictureMap.getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		if (cmsProgram == null)
    		{
                return "1:操作失败,内容不存在";
    		}
    		Picture picture = new Picture();
    		picture.setPictureindex(programPictureMap.getPictureindex());
    		picture=pictureDS.getPicture(picture);
    		if (picture == null)
    		{
                return "1:操作失败,海报不存在";
    		}
    		if(platfType == 1 ||platfType == 2) //发布到3.0平台 或者2.0平台
    		{
        		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
        		if (platfType == 1)
        		{
        			cntPlatformSync.setPlatform(2);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
        		if (platfType == 2)
        		{
        			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
    			if (synType == 1) //发布
    			{
    				if(cntPlatformSynctmp.getStatus()!=0&&cntPlatformSynctmp.getStatus()!=400&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}	
    			}
    			else  //取消发布
    			{   
    				if(cntPlatformSynctmp.getStatus()!=300&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                if (platfType == 1)
                {
                    cntTargetSyncTemp.setTargettype(2); //海报只发布到EPG
                }
                else
                {
                    cntTargetSyncTemp.setPlatform(1); // 海报只发布到2.0网元
                }
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null||cntTargetSyncList.size()==0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType ==1)
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==0||cntTargetSync.getStatus()==400||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";
                	}
                }
                else
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==300||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";
                	}
                }                
    		}
    		if (platfType == 3) //发布到网元
    		{
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
    			CntTargetSync cntTargetSync = new CntTargetSync();
    			cntTargetSync.setObjectindex(programPictureMap.getMapindex());
    			cntTargetSync.setObjecttype(35);
    			cntTargetSync.setTargetindex(Long.valueOf(targetindex));
    			cntTargetSyncListTemp =cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
    			if (cntTargetSyncListTemp==null ||cntTargetSyncListTemp.size()==0)
    			{
                    return "1:操作失败,发布记录不存在";
    			}
    			if (synType == 1) //发布
    			{
    				if(cntTargetSyncListTemp.get(0).getStatus()!=0&&cntTargetSyncListTemp.get(0).getStatus()!=400&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				if(cntTargetSyncListTemp.get(0).getStatus()!=300&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    		}

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return "success";
    }
    private void insertPicMappingRecord(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(35);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
      
    private void insertPicMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,String elementcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(35);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elementcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }  
    
    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setCtgpgmapDS(ICategoryProgramMapDS ctgpgmapDS)
    {
        this.ctgpgmapDS = ctgpgmapDS;
    }
    

	public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
	{
		this.cmsProgramDS = cmsProgramDS;
	}

	public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
	{
		this.cmsMovieDS = cmsMovieDS;
	}	

	public void setProgramTargetSynDS(IProgramTargetSynDS programTargetSynDS)
	{
		this.programTargetSynDS = programTargetSynDS;
	}

	public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
	{
		this.cmsStorageareaLS = cmsStorageareaLS;
	}

	public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
	{
		this.usysConfigDS = usysConfigDS;
	}


    public void setProgramPlatformSynDS(IProgramPlatformSynDS programPlatformSynDS)
	{
		this.programPlatformSynDS = programPlatformSynDS;
	}
	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
	{
		this.objectSyncRecordDS = objectSyncRecordDS;
	}
	public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
	{
		this.cntPlatformSyncDS = cntPlatformSyncDS;
	}
	public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
	{
		this.cntTargetSyncDS = cntTargetSyncDS;
	}
    public void setServiceProgramMapDS(IServiceProgramMapDS serviceProgramMapDS)
	{
		this.serviceProgramMapDS = serviceProgramMapDS;
	}
	public void setProgramPictureMapDS(IProgramPictureMapDS programPictureMapDS)
	{
		this.programPictureMapDS = programPictureMapDS;
	}
	public void setSeriesProgramMapDS(ISeriesProgramMapDS seriesProgramMapDS)
	{
		this.seriesProgramMapDS = seriesProgramMapDS;
	}	
	public void setPictureDS(IPictureDS pictureDS)
	{
		this.pictureDS = pictureDS;
	}
	public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
	{
		this.programCrmMapDS = programCrmMapDS;
	}	

	public void setCmsProgramtypeDS(ICmsProgramtypeDS cmsProgramtypeDS)
	{
		this.cmsProgramtypeDS = cmsProgramtypeDS;
	}
}

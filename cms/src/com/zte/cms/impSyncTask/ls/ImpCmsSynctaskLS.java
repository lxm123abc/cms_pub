package com.zte.cms.impSyncTask.ls;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.zte.cms.common.DateUtil2;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.cms.impSyncTask.service.IImpCmsSynctaskDS;
import com.zte.ismp.common.util.DbUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;

public class ImpCmsSynctaskLS implements IImpCmsSynctaskLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IImpCmsSynctaskDS impsyncDS = null;

    private IUcpBasicDS basicDS = null;

    public void setImpsyncDS(IImpCmsSynctaskDS impsyncDS)
    {
        this.impsyncDS = impsyncDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public ImpCmsSynctask getImpCmsSynctask(Long taskindex) throws Exception
    {
        log.debug("get impCmsSynctask by pk starting...");
        ImpCmsSynctask rsObj = new ImpCmsSynctask();
        rsObj.setTaskindex(taskindex);

        try
        {
            rsObj = impsyncDS.getImpCmsSynctask(rsObj);

            // 将cp index转换为CPID，显示在页面上
            Long cpIndex = rsObj.getCpindex();
            UcpBasic ucpBasic = basicDS.getUcpBasicByIndex(cpIndex);
            if (ucpBasic != null)
            {
                rsObj.setCpid(ucpBasic.getCpid());
            }
            else
            {
                rsObj.setCpid(""); // 通过cpindex找不到对应的cp信息时
            }
        }
        catch (Exception e)
        {
            log.error("get impCmsSynctask exception:", e);
            throw e;
        }
        log.debug("get impCmsSynctaskList by pk end");
        return rsObj;
    }

    public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize) throws Exception
    {
        log.debug("get impCmsSynctask page info by condition starting...");
        TableDataInfo tableInfo = null;
        boolean cpCantFind = false;

        if (impCmsSynctask != null)
        {
            // 进行时间格式的转换,将时间中的空格 冒号 等去掉
            impCmsSynctask.setStarttime1(DateUtil2.get14Time(impCmsSynctask.getStarttime1()));
            impCmsSynctask.setStarttime2(DateUtil2.get14Time(impCmsSynctask.getStarttime2()));
            
            impCmsSynctask.setEndtime1(DateUtil2.get14Time(impCmsSynctask.getEndtime1()));
            impCmsSynctask.setEndtime2(DateUtil2.get14Time(impCmsSynctask.getEndtime2()));
          //对特殊字符的处理
            if(impCmsSynctask.getCpid()!=null && !"".equals(impCmsSynctask.getCpid())){
                impCmsSynctask.setCpid(EspecialCharMgt.conversion(impCmsSynctask.getCpid()));
            }
            if(impCmsSynctask.getCspid()!=null && !"".equals(impCmsSynctask.getCspid())){
                impCmsSynctask.setCspid(EspecialCharMgt.conversion(impCmsSynctask.getCspid()));
            }
            
            if(impCmsSynctask.getDescription()!=null && !"".equals(impCmsSynctask.getDescription())){
                impCmsSynctask.setDescription(EspecialCharMgt.conversion(impCmsSynctask.getDescription()));
            }

            // 根据cpid查询时，需要转换成cpindex
            String cpid = impCmsSynctask.getCpid();
            if ((cpid != null) && !cpid.equals(""))
            {
                DbUtil dbUtil = new DbUtil();

                String sql = "select cpindex from zxdbm_umap.ucp_basic where cpid='" + impCmsSynctask.getCpid()
                        + "' and cptype=1"; // cptype=1时为CP，cptype=2时为牌照方

                List resList = dbUtil.getQuery(sql);

                if (resList != null && resList.size() > 0)
                {
                    Iterator it = resList.iterator();
                    Long index = 0L;
                    while (it.hasNext())
                    {
                        HashMap mymap = (HashMap) it.next();
                        index = Long.parseLong((String) mymap.get("cpindex"));
                    }
                    impCmsSynctask.setCpindex(index);
                }
                else
                {
                    cpCantFind = true;
                }
            }
        }

        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("taskindex");
        impCmsSynctask.setOrderCond("taskindex desc");

        try
        {
            tableInfo = impsyncDS.pageInfoQuery(impCmsSynctask, start, pageSize);

            if (cpCantFind) // 根据条件cpid查询不到cpindex时，页面显示0条数据
            {
                List nullValue = new ArrayList<Object>();
                tableInfo.setData(nullValue);
                tableInfo.setTotalCount(0);
            }
            else
            {
                // 将cpindex转换为cpid，在页面上展示出cpid
                List taskList = tableInfo.getData();
                ImpCmsSynctask taskTemp = null;

                if (taskList != null && taskList.size() > 0)
                {
                    Iterator taskIt = taskList.iterator();

                    while (taskIt.hasNext())
                    {
                        taskTemp = (ImpCmsSynctask) taskIt.next();
                        Long cpIndex = taskTemp.getCpindex();
                        UcpBasic ucpBasic = basicDS.getUcpBasicByIndex(cpIndex);
                        if (ucpBasic != null)
                        {
                            taskTemp.setCpid(ucpBasic.getCpid());
                        }
                        else
                        {
                            taskTemp.setCpid(""); // 通过cpindex找不到对应的cp信息时
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {
            log.error("get impCmsSynctask by condition exception:", e);
            throw e;
        }
        log.debug("get impCmsSynctask page info by condition end");
        return tableInfo;
    }

}

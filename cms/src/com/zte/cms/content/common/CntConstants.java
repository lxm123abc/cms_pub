package com.zte.cms.content.common;

public class CntConstants
{
    public static final String WINDOWS_SEPERATOR = "\\"; // WINDOWS分隔符
    public static final String LINUX_SEPERATOR = "/"; // Linux分隔符
    public static final String FTP_PREFIX = "ftp://";
    public static final Long MAX_FILESIZE = 2L * 1024L * 1024L * 1024L; // HTTP上载文件大小的最大值
    
    // 库区定义
    public static final String STORAGEAREA_ONLINE = "2"; // 在线区
    
    // 内容状态
    public static final int CNT_STATUS_TOAPPLY = 110; // 待审核
    public static final int CNT_STATUS_ONEAUDITING = 120; // 一审中
    public static final int CNT_STATUS_ONEAUDITFAIL = 130; // 一审失败
    public static final int CNT_STATUS_TWOAUDITING = 140; // 二审中
    public static final int CNT_STATUS_TWOAUDITFAIL = 150; // 二审失败
    public static final int CNT_STATUS_RECYCLE = 160; // 回收站
    public static final int CNT_STATUS_THREEAUDITING = 170; // 三审中
    public static final int CNT_STATUS_THREEAUDITFAIL = 180; // 三审失败
    public static final int CNT_STATUS_TOPUBLISH = 0; // 待发布

    // 子内容状态
    public static final int SUBCNT_STATUS_TOPUBLISH = 20; // 待上线

    // 业务主对象类型
    public static final String CNT_PBOTYPE_APPLY = "CMS_CNT_A"; // 新增
    public static final String CNT_PBOTYPE_UPDATE = "CMS_CNT_U"; // 修改

    public static final String SUCESS_COUNT = "成功数量："; // 申请
    public static final String FAIL_COUNT = "失败数量："; // 申请

    public static final String MSG_PREFIX = "msg.info."; // 申请

    // 错误码定义
    public static final String CNT_SUCCESS = "250000"; // 操作成功
    public static final String CNT_NOT_EXIST = "250001"; // 内容已不存在
    public static final String CNT_STATUS_ERROR = "250002"; // 内容状态不正确
    public static final String CNT_WORKFLOW_ERROR = "250003"; // 内容已被提审
    public static final String CNT_BEING_APPLIED = "250004"; // 内容正在被其他操作员提审
    public static final String CNT_APPLYWKFW_EXECERROR = "250005"; // 内容提审流程执行失败
    public static final String CNT_HASNO_SUBCNT = "250006"; // 内容没有子内容,不能提审
    public static final String SUBCNT_STATUS_ERROR = "250007"; // 内容下的子内容状态不正确
    public static final String CNT_AUDITWKFW_ERROR = "250008"; // 内容尚未被提审或审核流程已被处理完成
    public static final String CNT_HASBEEN_AUDITED = "250009"; // 内容已被其他操作员完成当前审核操作
    public static final String CNT_BEING_AUDITED = "250010"; // 内容正在被其他操作员审核
    public static final String CNT_AUDITWKFW_EXECERROR = "250011"; // 内容审核流程执行失败
    public static final String CNT_MODWKFW_ERROR = "250201"; //内容已被提交修改申请
    public static final String CNT_BEING_MODAPPLIED = "250202"; //内容正在被其他操作员提交修改申请
    public static final String CNT_MODAPPLYWKFW_EXECERROR = "250203"; //内容提交修改申请流程执行失败
    public static final String CNT_MODHASNO_SUBCNT = "250204"; // 内容没有子内容，不能提交修改申请
    public static final String CNT_MODAUDITWKFW_ERROR = "250205"; //内容尚未被提交修改申请或审核流程已被处理完成
    public static final String CNT_MODSYNC_FAIL = "250206"; //内容修改同步过程失败，请稍后重试
    public static final String TEMPAREA_NOTFOUND = "250207"; //获取临时区地址失败
    public static final String BMS_NOTFOUND	= "250208"; //获取BMS系统失败
    public static final String EPG_NOTFOUND = "250209"; //获取EGP系统失败
    public static final String CDN_NOTFOUND	= "250210"; //获取CDN系统失败
    public static final String CNTSYNC_FTPADDR_NOTFOUND = "250211"; //获取同步内容实体文件FTP地址失败
    public static final String ONLINEAREA_NOTFOUND = "250212"; //获取在线区地址失败
    public static final String DEST_NOTFOUND = "250213"; //找不到目标地址，请检查存储区绑定
    public static final String SUBCNT_TOOLARGE = "250214"; //文件大小超过2G
    public static final String SUBCNT_UPLOAD_FAIL = "250215"; //文件上传失败
    public static final String CNT_FAIL = "250099"; // 操作失败
    public static final String GET_STORAGEAREA_FAIL = "1056020"; // 获取库区地址失败
    
    public static final String CNT_ENCRYPTIONSTATUS_ERROR = "250012"; // 内容加密状态不正确    
    
    public static final String SUBCNT_NOT_EXIST = "270003"; // 子内容不存在
    public static final String NOCONFIG_WINDOWSHARE = "272201"; // 未配置 系统参数WINDOWS共享访问点
    
    //关联发布平台日志
    public static final String OPERTYPE_PLATFORM_PROGRAM_BIND_ADD = "log.platform.program.bind.add";
    public static final String OPERTYPE_PLATFORM_PROGRAM_BIND_ADD_INFO = "log.platform.program.bind.add.info";
    public static final String OPERTYPE_TARGET_PROGRAM_BIND_ADD = "log.target.program.bind.add";
    public static final String OPERTYPE_TARGET_PROGRAM_BIND_ADD_INFO = "log.target.program.bind.add.info";
    public static final String OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD = "log.platform.pcrmmap.bind.add";
    public static final String OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD_INFO = "log.platform.pcrmmap.bind.add.info";
    public static final String OPERTYPE_TARGET_PCRMMAP_BIND_ADD = "log.target.pcrmmap.bind.add";
    public static final String OPERTYPE_TARGET_PCRMMAP_BIND_ADD_INFO = "log.target.pcrmmap.bind.add.info";
    public static final String OPERTYPE_PLATFORM_CTGPROGMAP_BIND_ADD = "log.platform.ctgprogmap.bind.add";
    public static final String OPERTYPE_PLATFORM_CTGPROGMAP_BIND_ADD_INFO = "log.platform.ctgprogmap.bind.add.info";
    public static final String OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD = "log.target.ctgprogmap.bind.add";
    public static final String OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD_INFO = "log.target.ctgprogmap.bind.add.info";
    public static final String OPERTYPE_PLATFORM_PICPROGMAP_BIND_ADD = "log.platform.picprogmap.bind.add";
    public static final String OPERTYPE_PLATFORM_PICPROGMAP_BIND_ADD_INFO = "log.platform.picprogmap.bind.add.info";
    public static final String OPERTYPE_TARGET_PICPROGMAP_BIND_ADD = "log.target.picprogmap.bind.add";
    public static final String OPERTYPE_TARGET_PICPROGMAP_BIND_ADD_INFO = "log.target.picprogmap.bind.add.info";
    public static final String OPERTYPE_PLATFORM_PICTURE_BIND_ADD = "log.platform.picture.bind.add";
    public static final String OPERTYPE_PLATFORM_PICTURE_BIND_ADD_INFO = "log.platform.picture.bind.add.info";
    public static final String OPERTYPE_TARGET_PICTURE_BIND_ADD = "log.target.picture.bind.add";
    public static final String OPERTYPE_TARGET_PICTURE_BIND_ADD_INFO = "log.target.picture.bind.add.info";
    
    public static final String OPERTYPE_PLATFORM_PROGRAM_BIND_DELETE = "log.platform.program.bind.delete";
    public static final String OPERTYPE_PLATFORM_PROGRAM_BIND_DELETE_INFO = "log.platform.program.bind.delete.info";
    public static final String OPERTYPE_TARGET_PROGRAM_BIND_DELETE = "log.target.program.bind.delete";
    public static final String OPERTYPE_TARGET_PROGRAM_BIND_DELETE_INFO = "log.target.program.bind.delete.info";
    public static final String OPERTYPE_PLATFORM_PCRMMAP_BIND_DELETE = "log.platform.pcrmmap.bind.delete";
    public static final String OPERTYPE_PLATFORM_PCRMMAP_BIND_DELETE_INFO = "log.platform.pcrmmap.bind.delete.info";
    public static final String OPERTYPE_TARGET_PCRMMAP_BIND_DELETE = "log.target.pcrmmap.bind.delete";
    public static final String OPERTYPE_TARGET_PCRMMAP_BIND_DELETE_INFO = "log.target.pcrmmap.bind.delete.info";
    public static final String OPERTYPE_PLATFORM_CTGPROGMAP_BIND_DELETE = "log.platform.ctgprogmap.bind.delete";
    public static final String OPERTYPE_PLATFORM_CTGPROGMAP_BIND_DELETE_INFO = "log.platform.ctgprogmap.bind.delete.info";
    public static final String OPERTYPE_TARGET_CTGPROGMAP_BIND_DELETE = "log.target.ctgprogmap.bind.delete";
    public static final String OPERTYPE_TARGET_CTGPROGMAP_BIND_DELETE_INFO = "log.target.ctgprogmap.bind.delete.info";
    public static final String OPERTYPE_PLATFORM_PICPROGMAP_BIND_DELETE = "log.platform.picprogmap.bind.delete";
    public static final String OPERTYPE_PLATFORM_PICPROGMAP_BIND_DELETE_INFO = "log.platform.picprogmap.bind.delete.info";
    public static final String OPERTYPE_TARGET_PICPROGMAP_BIND_DELETE = "log.target.picprogmap.bind.delete";
    public static final String OPERTYPE_TARGET_PICPROGMAP_BIND_DELETE_INFO = "log.target.picprogmap.bind.delete.info";
    public static final String OPERTYPE_PLATFORM_PICTURE_BIND_DELETE = "log.platform.picture.bind.delete";
    public static final String OPERTYPE_PLATFORM_PICTURE_BIND_DELETE_INFO = "log.platform.picture.bind.delete.info";
    public static final String OPERTYPE_TARGET_PICTURE_BIND_DELETE = "log.target.picture.bind.delete";
    public static final String OPERTYPE_TARGET_PICTURE_BIND_DELETE_INFO = "log.target.picture.bind.delete.info";
    
    public static final String CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL = "msg.bind.succuss";
    public static final String CMS_TARGETSYSTEM_MSG_TARGETSYSTEM = "msg.targetsystem";
    public static final String CMS_TARGETSYSTEM_MSG_HAS_BINDED = "msg.has.binded";
    public static final String CMS_TARGETSYSTEM_MSG_HAS_DELETED = "msg.has.deleted";

}

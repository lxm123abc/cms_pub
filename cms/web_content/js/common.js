
addOnloadEvent(function() {
 	//initRadioCheckbox();
 	//iconButton();
 	initInput();
 	initDatagrid();
 	//initForm();
 	//initPopUp();
 	initToggle();
	Upload_init();
 	//initTextarea();
})

//base jquery
//初始化控件radio,checkbox,select
function initRadioCheckbox(){
		var onStyle = "green";
		//radio选中变色
		$("input[@type=radio][@checked]").parent().toggleClass(onStyle);
	    $("input[@type=radio]").click(function(){$("input[@type=radio]").parent().removeClass(onStyle);$(this).parent().addClass(onStyle)})
		//checkbox选中变色
		$("input[@type=checkbox][@checked]").parent().toggleClass(onStyle);
		$("input[@type=checkbox]").click(function(){$(this).parent().toggleClass(onStyle)})
		//select选中变色
		$("select").find("option[@selected]").toggleClass(onStyle);
		$("select").change(function(){$(this).find("option").removeClass(onStyle);$(this).find("option[@selected]").addClass(onStyle);})
		$("select").find("option").each(function(i){$(this).attr("title",$(this).text())});
		}
//初始化图标按钮,主要用于图标操作按钮
function iconButton(){
		//$(":image").addClass("iconBtn")
		$(":image").focus(function(){$(this).get(0).blur()})
		.hover(function(){$(this).attr("class","iconBtnOver")},function(){$(this).attr("class","iconBtn")})
		.mousedown(function(){$(this).attr("class","iconBtnDown")})     
		.mouseout(function(){$(this).attr("class","iconBtn")});
		$(":image:disabled").addClass("iconBtnDisabled").attr("title","不可用");
		}
//初始化按钮
function initInput(){
$("input[@button],input[@submite],button").hover(function(){ $(this).attr("class","buttonOver")}, function(){$(this).attr("class","inputButton") });
}
//初始化数据表格
function initDatagrid(){
	var dataGrid = ".dataGrid";
	try{
		if($(dataGrid).find("thead>tr>th").eq(0).children().is(":checkbox"))
		{
		$(dataGrid).find("thead").find("th")[0].style.width = "20px" ;	
		}
	}catch(e)
	{
	}
	var editBlock =".editBlock";
	try{
    if(!$(editBlock).find("tbody >tr> th").attr("width"))
		{
		$(editBlock).find("tbody >tr> th").width("120px");
		}
	}catch(e)
	{
	}
	var tableHover = ".tableHover";
	$(tableHover).find("tr").hover(function(){ $(this).addClass("trHover")}, function(){$(this).removeClass("trHover") });
	$(":checkbox[@checkSelector]").click(function(){
		var p = $(this);
		$(":checkbox").filter(function(){return $(this).attr("checkNode") == p.attr("checkSelector") && $(this).attr("disabled") == null}).each(function(){$(this).get(0).checked = p.attr("checked")});
	})
	
	$(":checkbox[@checkNode]").click(function(){
		var p = $(this);
		x = $(":checkbox").filter(function(){return $(this).attr("checkNode") !=null && $(this).attr("disabled") == null});
		var k = 0;
		for(var i=0;i<x.length;i++) {if(x.get(i).checked == true) { k++ };}
		var t = $(":checkbox").filter(function(){return $(this).attr("checkSelector") == p.attr("checkNode")});
		if(k==x.length) { 
			t.each(function(){$(this).get(0).checked = true}) 
		} else {
			t.each(function(){$(this).get(0).checked = false}) 
		}
	})
	
}
//初始化表单	
function initForm(){
		try{
		$(":text,:password").focus(function(){ $(this).addClass("onFocus"); })
		.blur(function(){ $(this).removeClass("onFocus"); })
		.filter(function(){return $(this).attr("readonly") != undefined}).focus(function(){ $(this).removeClass("onFocus"); })
		
		$("textarea").focus(function(){ $(this).addClass("onFocus"); })
		.blur(function(){ $(this).removeClass("onFocus"); })
		$(".page select").removeClass("sele");
		$(":text").filter(function(){return $(this).attr("class") != "Wdate"}).get(0).focus();
	}
	catch(e){}
	}
////////////////////// initForm end ////////////////////////
	//以subtitle作为需要点击的class
 function initToggle()
 {
	$(".subtitle").click(function(){
		 var t = $(this).parent().parent().next("tbody");
		 t.toggle();
         if(t.get(0).style.display == "none")
		 {
			 $(this).get(0).className = "subtitleClose";
		 }else{
			 $(this).get(0).className = "subtitle";
		}
	})
	$(".subtitleClose").click(function(){
		 var t = $(this).parent().parent().next("tbody");
		t.toggle();	
		if(t.get(0).style.display == "none")
		{
			$(this).get(0).className = "subtitleClose";
		}else{
			$(this).get(0).className = "subtitle";
		}
	})
 }
////////////////////// initToggle end ////////////////////////	
function initPopUp()
{
	 if(window.innerHeight)
	 {
	  $(".popbody").height(window.innerHeight - 60 +"px");
	 }
	 if(document.body.offsetHeight)
	 {
	 $(".popbody").height((document.documentElement.offsetHeight ||document.body.offsetHeight) - 84 +"px");
	 }
}
//输入字数限制
function initTextarea()
{
	
	$("textarea[@maxchar]").after("<div class='remain'>&nbsp;&nbsp;您还可以输入<span></span>个字符&nbsp;&nbsp;</div>")
	
	$("textarea[@maxchar]").each(function(){
								var lens,remain,maxchar;
								maxchar=$(this).attr("maxchar")
								lens=$(this).val().replace(/[^\x00-\xff]/gi,'ch').length ;
								remain=parseInt(maxchar)-lens;  
								$(this).next("div[@class='remain']").children("span").text(remain);
								$(this).next("div[@class='remain']").width($(this).width())
								})
	$("textarea[@maxchar]").bind("keyup",charLeft);
	$("textarea[@maxchar]").bind("keydown",charLeft);
	$("textarea[@maxchar]").bind("change",charLeft);
	
	
	}
 function charLeft()
{ 
				  var lens,remain,maxchar;
				  maxchar=$(this).attr("maxchar")
				  lens=$(this).val().replace(/[^\x00-\xff]/gi,'ch').length ;
				  remain=parseInt(maxchar)-lens;  
				  $(this).next("div[@class='remain']").children("span").text(remain);
				  if(remain<0)
				  {
					 $(this).val($(this).val().substring(0,maxchar))
					 $(this).next("div[@class='remain']").children("span").text(0);
				  }
} 

// 计算对象的座标
Number.prototype.NaN0=function(){return isNaN(this)?0:this;}
function getPosition(e){
    var left = 0;
    var top  = 0;
    while (e.offsetParent){
        left += e.offsetLeft + (e.currentStyle?(parseInt(e.currentStyle.borderLeftWidth)).NaN0():0);
		top  += e.offsetTop  + (e.currentStyle?(parseInt(e.currentStyle.borderTopWidth)).NaN0():0);
		e     = e.offsetParent;
    }

    left += e.offsetLeft + (e.currentStyle?(parseInt(e.currentStyle.borderLeftWidth)).NaN0():0);
	top  += e.offsetTop  + (e.currentStyle?(parseInt(e.currentStyle.borderTopWidth)).NaN0():0);

    return {x:left, y:top};

}
//弹出窗口
function popUp(url,width,height,winname,left,top)
{
		var left = (left==''||left==null)?(screen.width - width)/2:left;
		var top = (top==''||top==null)?(screen.height - height)/2:top;
		var winnames = (winname=='')?'popUpWin':winname;
		window.open(url, winnames, 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width='+width+',height='+height+',left='+left+', top='+top+',screenX='+left+',screenY='+top+'');
}
//返回
function goback()
{
	history.back();
}
//最大值
function checkMaxInput(_this,maxleng,leftInforId) 
{ 
	var left=document.getElementById(leftInforId); 
	var len=_this.value.replace(/[^\x00-\xff]/gi,'ch').length; 
	var remainnum =parseInt(maxleng)-len; 
	left.value = remainnum; 
	if(remainnum < 0) 
	{ 
		if(_this.value.length!=len) 
		{ 
			if((len-_this.value.length)>(maxleng/2)) 
			{ 
			_this.value=_this.value.substring(0,maxleng/2); 
			} 
			else 
			{ 
			_this.value=_this.value.substring(0,maxleng-(len-_this.value.length)); 
			} 
		}else{ 
			_this.value=_this.value.substring(0,maxleng); 
		} 
			left.value=0; 
			return; 
	} 
}


var historyStr ="";
function checkMaxInput2(_this,maxleng,leftInforId) 
{
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 

    var remainnum =parseInt(maxleng)-len; 
 
    if(remainnum < 0) 
    {
       _this.value = historyStr;
       return ;
    } 
    historyStr = _this.value;
    remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum;  
}

function checkLengthOnBoard(_this,maxleng,leftInforId)
{
    var oldString = _this.value;
    
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 

    if (window.clipboardData) 
    {
        len = len + utf8_strlen2(window.clipboardData.getData('Text'));
        
     } 
    
    var remainnum =parseInt(maxleng)-len; 
   
    if(remainnum < 0) 
    {
      
       _this.value = "hehe";
       _this.value = "";
       if(trim(oldString).length > 0)
       {
           _this.value = oldString;
           window.clipboardData.setData('text',oldString);
       
       }
       return ;
    } 
   
    remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum;  
}

function makeReadonly(){
	
	$(".editBlock").not(".todo").find(":text,select,textArea")
	.attr("readonly","readonly")
	.addClass("readonlyCtrl")
	.focus(function(){$(this).get(0).blur()});
	
		$(".editBlock").not(".todo").find("select,:radio,:checkbox,:button,:submit,.Wdate").attr("disabled","disabled")


}


var  datacount={};
var storagemanageData={};
function showWkfwData()
{

  var para = {};
  para.nodeName='一审';
  callUrl('cntWkfwLSFacade/getMyNeedHandleWorkflowItemListCnt.ssm',para,'datacount','true');
  var oneaudit= datacount.rtnValue;
  para.nodeName='二审';
  callUrl('cntWkfwLSFacade/getMyNeedHandleWorkflowItemListCnt.ssm',para,'datacount','true');
  var twoaudit = datacount.rtnValue;
  para.nodeName='三审';
  callUrl('cntWkfwLSFacade/getMyNeedHandleWorkflowItemListCnt.ssm',para,'datacount','true');
  var threeaudit = datacount.rtnValue;
  para.nodeName='未读消息';
  callUrl('cmsNoticeBasicDS/unViewNumQuery.ssm',para,'datacount','true');
  var unreadmesnum = datacount.rtnValue;
  
  
   var datastr = '<a href="#" onclick="openWkfw('+1+')">'+$res_entry('label.wait.do.wkfw.one')+"&nbsp;"+oneaudit+'</a>&nbsp;&nbsp;&nbsp;'
                +'<a href="#" onclick="openWkfw('+2+')">'+$res_entry('label.wait.do.wkfw.two')+"&nbsp;"+twoaudit+'</a>&nbsp;&nbsp;&nbsp;'
                +'<a href="#" onclick="openWkfw('+3+')">'+$res_entry('label.wait.do.wkfw.three')+"&nbsp;"+threeaudit+'</a>&nbsp;&nbsp;&nbsp;'
                +'<a href="#" onclick="openUnreadMes()">'+$res_entry('label.wait.do.unreadmes')+"&nbsp;"+unreadmesnum+'</a>&nbsp;&nbsp;&nbsp;';

   document.getElementById ('ShowData').innerHTML= datastr ; //定时刷新index页面的审核数量
}

function openWkfw(number)
{
	var url="";
	 if(number=="2")
	 {
     	//document.getElementById('mainFrame').src="../CMS/content/content_batch_wkfw.html?isLoad=false&&flag="+number;
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.two'),url);
	 }
	 else if(number=="1")
	 {
     	//document.getElementById('mainFrame').src="../CMS/content/content_wkfw.html?isLoad=false&&flag="+number;
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.one'),url);
     }
     else
     {
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.three'),url);
     }
}

function openUnreadMes()
{
	var url = "../CMS/noticemgr/notificationList.html?isLoad=false";
	window.mainFrame.myTab.Cts($res_entry('label.wait.do.unreadmes'),url);
}

function openWkfwFromBulletin(number)
{
	var url="";
	 if(number=="2")
	 {
     	//document.getElementById('mainFrame').src="../CMS/content/content_batch_wkfw.html?isLoad=false&&flag="+number;
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.parent.opener.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.two'),url);
	 }
	 else if(number=="1")
	 {
     	//document.getElementById('mainFrame').src="../CMS/content/content_wkfw.html?isLoad=false&&flag="+number;
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.parent.opener.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.one'),url);
     }
     else
     {
     	url="../CMS/content/program/program_wkfw.html?isLoad=false&&flag="+number;
     	window.parent.opener.mainFrame.myTab.Cts($res_entry('label.wait.do.wkfw.three'),url);
     	
     }
}

function openNopassWkfwFromBulletin(auditlevel)
{
	var url = "../CMS/content/content_wkfw_fail_ucdn.html?isLoad=false";
	window.parent.opener.mainFrame.myTab.Cts("内容审核失败",url);
}

function openStorageInfo(areaindex,areaid){

         url="../CMS/storageArea/storageAreaMain.html?web_key_menu_id=statistic.storage.areamgt&isFromBulletin=yes&areaindex="+areaindex+"&areaid="+areaid;
     	 window.parent.opener.mainFrame.myTab.Cts($res_entry('label.statistic.storage.areamgt'),url);
     	 
}

function openFTPInfo(ftpIndex){
          url="../CMS/basicdata/ftpView.html?infoindex="+ftpIndex;
     	 window.parent.opener.mainFrame.myTab.Cts($res_entry('label.ftp.ftpinfo'),url);
}

//window.setInterval(showWkfwData,60000);
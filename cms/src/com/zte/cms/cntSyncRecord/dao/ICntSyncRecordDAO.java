package com.zte.cms.cntSyncRecord.dao;

import java.util.List;

import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICntSyncRecordDAO
{
    /**
     * 新增CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DAOException dao异常
     */
    public void insertCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException;

    /**
     * 根据主键更新CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DAOException dao异常
     */
    public void updateCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException;

    /**
     * 根据主键删除CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @throws DAOException dao异常
     */
    public void deleteCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException;

    /**
     * 根据主键查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @return 满足条件的CntSyncRecord对象
     * @throws DAOException dao异常
     */
    public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException;

    /**
     * 根据条件查询CntSyncRecord对象
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @return 满足条件的CntSyncRecord对象集
     * @throws DAOException dao异常
     */
    public List<CntSyncRecord> getCntSyncRecordByCond(CntSyncRecord cntSyncRecord) throws DAOException;

    /**
     * 根据条件分页查询CntSyncRecord对象，作为查询条件的参数
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CntSyncRecord对象，作为查询条件的参数
     * 
     * @param cntSyncRecord CntSyncRecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
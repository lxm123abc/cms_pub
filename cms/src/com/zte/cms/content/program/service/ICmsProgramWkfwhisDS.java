package com.zte.cms.content.program.service;

import java.util.List;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramWkfwhisDS
{
    /**
     * 新增CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 新增CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException;

    /**
     * 更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 批量更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException;

    /**
     * 根据条件更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException;

    /**
     * 删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 批量删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException;

    /**
     * 根据条件删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException;

    /**
     * 查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @return CmsProgramWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public CmsProgramWkfwhis getCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException;

    /**
     * 根据条件查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @return 满足条件的CmsProgramWkfwhis对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
/* 页面最外Table定义，定义页头 */
<TABLE class=tb_content cellSpacing=0 cellPadding=0 align=center>

/* 定义导航条 */
<table width="100%">
      <tbody>
        <tr>
          <td width="56%" nowrap>
            <div class=div_subtitle>当前位置： 网上支付<span class="arrow_subtitle">&gt;</span>查询<span class="arrow_subtitle">&gt;</span>按核算体系</div></td>     
          <td width="44%" align="right" valign="bottom" nowrap>&nbsp; </td>
        </tr>
      </tbody>
    </table>
    
/* 页面查询Table定义，定义标题和内容td */
<TABLE class=tb_searchbar cellSpacing=1 width="100%">
.tb_searchBar
    td_head           /*表格标题头*/
    td_title          /*表格标题*/
    td_detail         /*表格内容*/
    td_tail           /*表格尾行,有按钮行*/
   
/* 页面输入区域Table定义，定义标题和内容td */   
 <table  cellspacing="1" class="tb_input" >
.tb_input
    td_head           /*表格标题头*/
    td_title          /*表格标题*/
    td_detail         /*表格内容*/
    td_tail           /*表格尾行,有按钮行*/

/* 页面数据列表Table定义，定义字段标题和交叉行tr */
 <table cellspacing="1" class="tb_datalist" >
 
   /* 页码行标记 */
   <tr class="tr_pagenumber">
.tb_dataList
    tr_head            /*表格标题头*/
    tr_title           /*表格字段行*/
    tr_odd             /*表格奇数行*/  
    tr_even            /*表格偶数行*/
    tr_selected        /*表格选定行*/
    tr_pagenumber      /*表格翻页行*/
    tr_tail            /*表格尾行*/


<!-- 页表格头风格定义 td_head暂时不用了 -->
  <table>
  <table class=tb_titlebar  cellspacing=0 cellpadding=0 width="100%">
    <tbody>
      <tr>
        <td><img src="../images/icon_edit.gif" width="16" height="16" class=icon> 总部借款单据 </td>
        <td align="right"><input class=button type=submit value=新增 name=Submit5232></td>
      </tr>
  </table>                    
  <table cellspacing="1" class="tb_datalist" >
  </table>
  
.tb_titlebar

<!-- 定义开发页面导航条 -->
         <table width="100%">
                <tbody>
                  <tr>
                    <td width="56%" nowrap>
                      <div class=div_subtitle>当前位置： 网上支付<span class="arrow_subtitle">&gt;</span>查询<span class="arrow_subtitle">&gt;</span>按核算体系</div></td>     
                    <td width="44%" align="right" valign="bottom" nowrap>&nbsp; </td>
                  </tr>
                </tbody>
              </table>


/% 新项目必须修改的地方: 
修改cssCn.css文件开始部分的url()地址为项目相对的web地址,以"/"开头 ,例如"/hronline/cn_hronline/css/HTMLComponent.htc"
input,textarea { behavior:url(../css/HTMLComponent.htc); } 
 %/

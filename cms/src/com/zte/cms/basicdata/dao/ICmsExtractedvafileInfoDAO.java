package com.zte.cms.basicdata.dao;

import java.util.List;

import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsExtractedvafileInfoDAO
{
    /**
     * 新增CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DAOException dao异常
     */
    public void insertCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException;

    /**
     * 根据主键更新CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DAOException dao异常
     */
    public void updateCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException;

    /**
     * 根据主键删除CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DAOException dao异常
     */
    public void deleteCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo) throws DAOException;

    /**
     * 根据主键查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @return 满足条件的CmsExtractedvafileInfo对象
     * @throws DAOException dao异常
     */
    public CmsExtractedvafileInfo getCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DAOException;

    /**
     * 根据条件查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @return 满足条件的CmsExtractedvafileInfo对象集
     * @throws DAOException dao异常
     */
    public List<CmsExtractedvafileInfo> getCmsExtractedvafileInfoByCond(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DAOException;

    /**
     * 根据条件分页查询CmsExtractedvafileInfo对象，作为查询条件的参数
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询CmsExtractedvafileInfo对象，作为查询条件的参数
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
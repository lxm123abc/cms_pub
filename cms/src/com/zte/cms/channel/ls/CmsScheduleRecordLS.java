package com.zte.cms.channel.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.channel.common.CmsScheduleConstant;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.cms.content.program.service.IProgramSchedualrecordMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.framework.base.Sequence;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsScheduleRecordLS implements ICmsScheduleRecordLS {
	private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_USER,
			getClass());
	private ICmsScheduleDS cmsScheduleDS;
	private ICmsSchedulerecordDS cmsSchedulerecordDS;
	private IProgramSchedualrecordMapDS programSchedualrecordMapDS;
	private IBatchObjectRecordDS batchObjectDS;
	private ICmsChannelDS channelDS;
	private IPhysicalchannelDS physicalchannelDS;
	private ICntSyncTaskDS icntsyncTaskDS;
	private ITargetsystemDS targetSystemDS;
	private ICntSyncRecordDS cntSyncRecordDS;
	private IUcpBasicDS basicDS;
	private IUsysConfigDS usysConfigDS = null;
	private IPrimaryKeyGenerator primaryKeyGenerator = null;
	// private final static String SCHEDULE_STORE_SUB_PATH="/cntsyncxml/schedule/";
	private final static String SYN_PARENT_FOLDER = "xmlsync";
	private final static String SCHEDULERECORD_FOLDER = "schedulerecord";
	private final static String ACTIONREGIST = "REGIST or UPDATE";
	private final static String ACTIONDELETE = "DELETE";
	private static final String BATCHID = "ucdn_task_batch_id";
	private static final String CORRELATEID = "ucdn_task_correlate_id";

	public String insertScheduleRecordmorePhyBatch(List<String> scheduleList,
			String channelid) throws Exception {
		log.info("insertScheduleRecord start");
		
		ReturnInfo rtnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		int infoCnt = 0;
		String result;
		int success = 0;
		int failed = 0;
		int total = scheduleList.size();
		DbUtil db = new DbUtil();
		List<UsysConfig> usysConfigList = null;
		int isarchive = 0;
		int hotdegree = 0;
		int minutes = 15;

		
		//根据物理频道增加录制计划
		List phylist = new ArrayList();
		Physicalchannel phychannel = new Physicalchannel();
		phychannel.setChannelid(channelid);				
		phylist = physicalchannelDS.getPhysicalchannelByCond(phychannel);
		try {
			
			
			
//			String valiChannel = this.getPhysicChannelByid(physicChannelid);
//			if (valiChannel.equals("success")) {
				UsysConfig usysCfgIsarchive = new UsysConfig();
				usysCfgIsarchive.setCfgkey("cms.schedulerecord.archive.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgIsarchive);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgIsarchive = usysConfigList.get(0);
					isarchive = Integer
							.parseInt(usysCfgIsarchive.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}

				UsysConfig usysCfgHotdegree = new UsysConfig();
				usysCfgHotdegree.setCfgkey("cms.publish2cdn.hotdegree.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgHotdegree);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgHotdegree = usysConfigList.get(0);
					hotdegree = Integer
							.parseInt(usysCfgHotdegree.getCfgvalue());// 发布到融合CDN时使用热度表示 0：不热 1：热
				}

				UsysConfig usysCfgCantedit = new UsysConfig();
				usysCfgCantedit.setCfgkey("cms.schedule.cantedit.time");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgCantedit);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgCantedit = usysConfigList.get(0);
					minutes = Integer.parseInt(usysCfgCantedit.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}
				// 提前minutes分钟下达录制计划
				String sql = "select to_char(sysdate + " + minutes
						+ "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚minutes分钟的时间

				List rtnlist = null;
				rtnlist = db.getQuery(sql);
				Map tmpMap = new HashMap();
				tmpMap = (Map) rtnlist.get(0);
				String currenttime = tmpMap.get("currenttime").toString();
				for (int i = 0; i < phylist.size(); i++) {
					
					
					
					Physicalchannel	physicalchannel = (Physicalchannel)phylist.get(i);
				
					CmsSchedulerecord isExsitedRecord = new CmsSchedulerecord();
					isExsitedRecord.setPhysicalchannelid(physicalchannel.getPhysicalchannelid());
					isExsitedRecord.setScheduleid(scheduleList.get(0));
					List<CmsSchedulerecord> isExsitedRecordList = new ArrayList<CmsSchedulerecord>();
					isExsitedRecordList = cmsSchedulerecordDS
							.getCmsSchedulerecordByCond(isExsitedRecord);
					if (isExsitedRecordList.size() > 0) {
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), scheduleList.get(0),
								"添加收录失败", "该录制计划已存在");
						rtnInfo.appendOperateInfo(operateInfo);
						failed++;
					} else {
						CmsSchedule schedule = new CmsSchedule();
						CmsSchedule schdle = new CmsSchedule();
						schedule.setChannelid(channelid);
						schedule.setScheduleid(scheduleList.get(0));

						List<CmsSchedule> list = cmsScheduleDS
								.getCmsScheduleByCond(schedule);
						if (null != list && list.size() > 0) {
							schdle = list.get(0);
						}
//						if (schdle.getStatus() != 20
//								&& schdle.getStatus() != 50
//								&& schdle.getStatus() != 60) {
//							operateInfo = new OperateInfo(String
//									.valueOf(++infoCnt), scheduleList.get(0),
//									"添加收录失败", "该节目单没有同步成功");
//							rtnInfo.appendOperateInfo(operateInfo);
//							failed++;
//							continue;
//						}
						// 提前15分钟下达录制计划
						/***********************************************************************************************
						 * if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime) > 0) {
						 **********************************************************************************************/
						CmsSchedulerecord cmsScheduleRecord = new CmsSchedulerecord();
						cmsScheduleRecord.setNamecn(schdle.getProgramname());
						cmsScheduleRecord.setStatus(0);
						if (null != physicalchannel.getDomain()
								&& !physicalchannel.getDomain().toString()
										.equals("")) {
							cmsScheduleRecord.setDomain(physicalchannel
									.getDomain());
						}
						cmsScheduleRecord.setScheduleid(schdle.getScheduleid());
						cmsScheduleRecord.setPhysicalchannelid(physicalchannel.getPhysicalchannelid());
						cmsScheduleRecord.setCreatetime(currenttime);
						cmsScheduleRecord.setLastupdatetime(currenttime);
						cmsScheduleRecord.setStartdate(schdle.getStartdate());
						cmsScheduleRecord.setStarttime(schdle.getStarttime());
						cmsScheduleRecord.setDuration(schdle.getDuration());
						cmsScheduleRecord.setCpcontentid(physicalchannel
								.getPhysicalchannelid());
						if (schdle.getDescription() != null
								&& !schdle.getDescription().equals("")) {
							cmsScheduleRecord.setDescription(schdle
									.getDescription());
						}
						if (schdle.getCpcontentid() != null
								&& schdle.getCpcontentid().equals("")) {
							cmsScheduleRecord.setCpcontentid(schdle
									.getCpcontentid());
						}
						cmsScheduleRecord.setIsarchive(isarchive);
						cmsScheduleRecord.setHotdegree(hotdegree);
						cmsSchedulerecordDS
								.insertCmsSchedulerecord(cmsScheduleRecord);
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), schdle
										.getScheduleid(), "添加收录成功", "添加收录任务成功");
						rtnInfo.appendOperateInfo(operateInfo);
						success++;
						CommonLogUtil
								.insertOperatorLog(
										cmsScheduleRecord.getSchedulerecordid(),
										CmsScheduleConstant.MGTTYPE_SCHEDULERECOR,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD_INFO,
										CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
						/***********************************************************************************************
						 * } else if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime)<
						 * 0) { operateInfo = new
						 * OperateInfo(String.valueOf(++infoCnt),schdle.getScheduleid(),"添加收录失败","收录失败，节目单即将开始已经不能收录");
						 * rtnInfo.appendOperateInfo(operateInfo); failed++; }
						 **********************************************************************************************/
					}
				}

				if (success == total) {
					rtnInfo.setReturnMessage("成功数量" + success);
					rtnInfo.setFlag("0");
				} else if (failed == total) {
					rtnInfo.setReturnMessage("失败数量" + failed);
					rtnInfo.setFlag("1");
				} else {
					rtnInfo.setReturnMessage("成功数量" + success + " 失败数量"
							+ failed);
					rtnInfo.setFlag("2");
				}
//			} else {
//				return valiChannel;
//			}
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("insertScheduleRecord exception:" + e);
			operateInfo = new OperateInfo(String.valueOf(++infoCnt), "数据库更新",
					"收录失败", "收录失败：" + e.getMessage());
			rtnInfo.appendOperateInfo(operateInfo);
			rtnInfo.setFlag("1");
		}
		result = rtnInfo.toString();
		log.info("insertScheduleRecord end");
		return result;
	}
	
	public String insertScheduleRecordmorePhy(List<String> scheduleList,
			String channelid, String physicChannelid) throws Exception {
		log.info("insertScheduleRecord start");
		String[] str = physicChannelid.split(",");
		
		ReturnInfo rtnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		int infoCnt = 0;
		String result;
		int success = 0;
		int failed = 0;
		int total = scheduleList.size();
		DbUtil db = new DbUtil();
		List<UsysConfig> usysConfigList = null;
		int isarchive = 0;
		int hotdegree = 0;
		int minutes = 15;

		try {
//			String valiChannel = this.getPhysicChannelByid(physicChannelid);
//			if (valiChannel.equals("success")) {
				UsysConfig usysCfgIsarchive = new UsysConfig();
				usysCfgIsarchive.setCfgkey("cms.schedulerecord.archive.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgIsarchive);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgIsarchive = usysConfigList.get(0);
					isarchive = Integer
							.parseInt(usysCfgIsarchive.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}

				UsysConfig usysCfgHotdegree = new UsysConfig();
				usysCfgHotdegree.setCfgkey("cms.publish2cdn.hotdegree.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgHotdegree);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgHotdegree = usysConfigList.get(0);
					hotdegree = Integer
							.parseInt(usysCfgHotdegree.getCfgvalue());// 发布到融合CDN时使用热度表示 0：不热 1：热
				}

				UsysConfig usysCfgCantedit = new UsysConfig();
				usysCfgCantedit.setCfgkey("cms.schedule.cantedit.time");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgCantedit);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgCantedit = usysConfigList.get(0);
					minutes = Integer.parseInt(usysCfgCantedit.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}
				// 提前minutes分钟下达录制计划
				String sql = "select to_char(sysdate + " + minutes
						+ "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚minutes分钟的时间

				List rtnlist = null;
				rtnlist = db.getQuery(sql);
				Map tmpMap = new HashMap();
				tmpMap = (Map) rtnlist.get(0);
				String currenttime = tmpMap.get("currenttime").toString();
				for (int i = 0; i < str.length; i++) {
					Physicalchannel physicalchannel = new Physicalchannel();
					physicalchannel.setPhysicalchannelid(str[i]);
					List<Physicalchannel> physicalchannelList = physicalchannelDS
							.getPhysicalchannelByCond(physicalchannel);
					if (physicalchannelList != null
							&& physicalchannelList.size() > 0) {
						physicalchannel = physicalchannelList.get(0);
					}
					CmsSchedulerecord isExsitedRecord = new CmsSchedulerecord();
					isExsitedRecord.setPhysicalchannelid(physicChannelid);
					isExsitedRecord.setScheduleid(scheduleList.get(0));
					List<CmsSchedulerecord> isExsitedRecordList = new ArrayList<CmsSchedulerecord>();
					isExsitedRecordList = cmsSchedulerecordDS
							.getCmsSchedulerecordByCond(isExsitedRecord);
					if (isExsitedRecordList.size() > 0) {
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), scheduleList.get(0),
								"添加收录失败", "该录制计划已存在");
						rtnInfo.appendOperateInfo(operateInfo);
						failed++;
					} else {
						CmsSchedule schedule = new CmsSchedule();
						CmsSchedule schdle = new CmsSchedule();
						schedule.setChannelid(channelid);
						schedule.setScheduleid(scheduleList.get(0));

						List<CmsSchedule> list = cmsScheduleDS
								.getCmsScheduleByCond(schedule);
						if (null != list && list.size() > 0) {
							schdle = list.get(0);
						}
//						if (schdle.getStatus() != 20
//								&& schdle.getStatus() != 50
//								&& schdle.getStatus() != 60) {
//							operateInfo = new OperateInfo(String
//									.valueOf(++infoCnt), scheduleList.get(0),
//									"添加收录失败", "该节目单没有同步成功");
//							rtnInfo.appendOperateInfo(operateInfo);
//							failed++;
//							continue;
//						}
						// 提前15分钟下达录制计划
						/***********************************************************************************************
						 * if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime) > 0) {
						 **********************************************************************************************/
						CmsSchedulerecord cmsScheduleRecord = new CmsSchedulerecord();
						cmsScheduleRecord.setNamecn(schdle.getProgramname());
						cmsScheduleRecord.setStatus(0);
						if (null != physicalchannel.getDomain()
								&& !physicalchannel.getDomain().toString()
										.equals("")) {
							cmsScheduleRecord.setDomain(physicalchannel
									.getDomain());
						}
						cmsScheduleRecord.setScheduleid(schdle.getScheduleid());
						cmsScheduleRecord.setPhysicalchannelid(str[i]);
						cmsScheduleRecord.setCreatetime(currenttime);
						cmsScheduleRecord.setLastupdatetime(currenttime);
						cmsScheduleRecord.setStartdate(schdle.getStartdate());
						cmsScheduleRecord.setStarttime(schdle.getStarttime());
						cmsScheduleRecord.setDuration(schdle.getDuration());
						cmsScheduleRecord.setCpcontentid(physicalchannel
								.getPhysicalchannelid());
						if (schdle.getDescription() != null
								&& !schdle.getDescription().equals("")) {
							cmsScheduleRecord.setDescription(schdle
									.getDescription());
						}
						if (schdle.getCpcontentid() != null
								&& schdle.getCpcontentid().equals("")) {
							cmsScheduleRecord.setCpcontentid(schdle
									.getCpcontentid());
						}
						cmsScheduleRecord.setIsarchive(isarchive);
						cmsScheduleRecord.setHotdegree(hotdegree);
						cmsSchedulerecordDS
								.insertCmsSchedulerecord(cmsScheduleRecord);
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), schdle
										.getScheduleid(), "添加收录成功", "添加收录任务成功");
						rtnInfo.appendOperateInfo(operateInfo);
						success++;
						CommonLogUtil
								.insertOperatorLog(
										cmsScheduleRecord.getSchedulerecordid(),
										CmsScheduleConstant.MGTTYPE_SCHEDULERECOR,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD_INFO,
										CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
						/***********************************************************************************************
						 * } else if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime)<
						 * 0) { operateInfo = new
						 * OperateInfo(String.valueOf(++infoCnt),schdle.getScheduleid(),"添加收录失败","收录失败，节目单即将开始已经不能收录");
						 * rtnInfo.appendOperateInfo(operateInfo); failed++; }
						 **********************************************************************************************/
					}
				}

				if (success == total) {
					rtnInfo.setReturnMessage("成功数量" + success);
					rtnInfo.setFlag("0");
				} else if (failed == total) {
					rtnInfo.setReturnMessage("失败数量" + failed);
					rtnInfo.setFlag("1");
				} else {
					rtnInfo.setReturnMessage("成功数量" + success + " 失败数量"
							+ failed);
					rtnInfo.setFlag("2");
				}
//			} else {
//				return valiChannel;
//			}
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("insertScheduleRecord exception:" + e);
			operateInfo = new OperateInfo(String.valueOf(++infoCnt), "数据库更新",
					"收录失败", "收录失败：" + e.getMessage());
			rtnInfo.appendOperateInfo(operateInfo);
			rtnInfo.setFlag("1");
		}
		result = rtnInfo.toString();
		log.info("insertScheduleRecord end");
		return result;
	}

	/**
	 * insertScheduleRecord新增录制计划
	 * 
	 * @param scheduleList 节目单ID集合
	 * @param channelid 频道ID
	 * @param physicChannelid 物理频道ID
	 * @return CmsSchedule对象
	 * @throws Exception
	 */
	public String insertScheduleRecord(List<String> scheduleList,
			String channelid, String physicChannelid) throws Exception {
		log.info("insertScheduleRecord start");
		ReturnInfo rtnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		int infoCnt = 0;
		String result;
		int success = 0;
		int failed = 0;
		int total = scheduleList.size();
		DbUtil db = new DbUtil();
		List<UsysConfig> usysConfigList = null;
		int isarchive = 0;
		int hotdegree = 0;
		int minutes = 15;

		try {
			String valiChannel = this.getPhysicChannelByid(physicChannelid);
			if (valiChannel.equals("success")) {
				UsysConfig usysCfgIsarchive = new UsysConfig();
				usysCfgIsarchive.setCfgkey("cms.schedulerecord.archive.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgIsarchive);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgIsarchive = usysConfigList.get(0);
					isarchive = Integer
							.parseInt(usysCfgIsarchive.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}

				UsysConfig usysCfgHotdegree = new UsysConfig();
				usysCfgHotdegree.setCfgkey("cms.publish2cdn.hotdegree.switch");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgHotdegree);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgHotdegree = usysConfigList.get(0);
					hotdegree = Integer
							.parseInt(usysCfgHotdegree.getCfgvalue());// 发布到融合CDN时使用热度表示 0：不热 1：热
				}

				UsysConfig usysCfgCantedit = new UsysConfig();
				usysCfgCantedit.setCfgkey("cms.schedule.cantedit.time");
				usysConfigList = usysConfigDS
						.getUsysConfigByCond(usysCfgCantedit);
				if (null != usysConfigList && usysConfigList.size() > 0) {
					usysCfgCantedit = usysConfigList.get(0);
					minutes = Integer.parseInt(usysCfgCantedit.getCfgvalue());// 录制计划录制后是否归档。0-不归档；1-归档
				}
				// 提前minutes分钟下达录制计划
				String sql = "select to_char(sysdate + " + minutes
						+ "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚minutes分钟的时间

				List rtnlist = null;
				rtnlist = db.getQuery(sql);
				Map tmpMap = new HashMap();
				tmpMap = (Map) rtnlist.get(0);
				String currenttime = tmpMap.get("currenttime").toString();
				for (int i = 0; i < scheduleList.size(); i++) {
					Physicalchannel physicalchannel = new Physicalchannel();
					physicalchannel.setPhysicalchannelid(physicChannelid);
					List<Physicalchannel> physicalchannelList = physicalchannelDS
							.getPhysicalchannelByCond(physicalchannel);
					if (physicalchannelList != null
							&& physicalchannelList.size() > 0) {
						physicalchannel = physicalchannelList.get(0);
					}
					CmsSchedulerecord isExsitedRecord = new CmsSchedulerecord();
					isExsitedRecord.setPhysicalchannelid(physicChannelid);
					isExsitedRecord.setScheduleid(scheduleList.get(i));
					List<CmsSchedulerecord> isExsitedRecordList = new ArrayList<CmsSchedulerecord>();
					isExsitedRecordList = cmsSchedulerecordDS
							.getCmsSchedulerecordByCond(isExsitedRecord);
					if (isExsitedRecordList.size() > 0) {
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), scheduleList.get(i),
								"添加收录失败", "该录制计划已存在");
						rtnInfo.appendOperateInfo(operateInfo);
						failed++;
					} else {
						CmsSchedule schedule = new CmsSchedule();
						CmsSchedule schdle = new CmsSchedule();
						schedule.setChannelid(channelid);
						schedule.setScheduleid(scheduleList.get(i));

						List<CmsSchedule> list = cmsScheduleDS
								.getCmsScheduleByCond(schedule);
						if (null != list && list.size() > 0) {
							schdle = list.get(0);
						}
						if (schdle.getStatus() != 20
								&& schdle.getStatus() != 50
								&& schdle.getStatus() != 60) {
							operateInfo = new OperateInfo(String
									.valueOf(++infoCnt), scheduleList.get(i),
									"添加收录失败", "该节目单没有同步成功");
							rtnInfo.appendOperateInfo(operateInfo);
							failed++;
							continue;
						}
						// 提前15分钟下达录制计划
						/***********************************************************************************************
						 * if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime) > 0) {
						 **********************************************************************************************/
						CmsSchedulerecord cmsScheduleRecord = new CmsSchedulerecord();
						cmsScheduleRecord.setNamecn(schdle.getProgramname());
						cmsScheduleRecord.setStatus(0);
						if (null != physicalchannel.getDomain()
								&& !physicalchannel.getDomain().toString()
										.equals("")) {
							cmsScheduleRecord.setDomain(physicalchannel
									.getDomain());
						}
						cmsScheduleRecord.setScheduleid(schdle.getScheduleid());
						cmsScheduleRecord.setPhysicalchannelid(physicChannelid);
						cmsScheduleRecord.setCreatetime(currenttime);
						cmsScheduleRecord.setLastupdatetime(currenttime);
						cmsScheduleRecord.setStartdate(schdle.getStartdate());
						cmsScheduleRecord.setStarttime(schdle.getStarttime());
						cmsScheduleRecord.setDuration(schdle.getDuration());
						cmsScheduleRecord.setCpcontentid(physicalchannel
								.getPhysicalchannelid());
						if (schdle.getDescription() != null
								&& !schdle.getDescription().equals("")) {
							cmsScheduleRecord.setDescription(schdle
									.getDescription());
						}
						if (schdle.getCpcontentid() != null
								&& schdle.getCpcontentid().equals("")) {
							cmsScheduleRecord.setCpcontentid(schdle
									.getCpcontentid());
						}
						cmsScheduleRecord.setIsarchive(isarchive);
						cmsScheduleRecord.setHotdegree(hotdegree);
						cmsSchedulerecordDS
								.insertCmsSchedulerecord(cmsScheduleRecord);
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), schdle
										.getScheduleid(), "添加收录成功", "添加收录任务成功");
						rtnInfo.appendOperateInfo(operateInfo);
						success++;
						CommonLogUtil
								.insertOperatorLog(
										cmsScheduleRecord.getSchedulerecordid(),
										CmsScheduleConstant.MGTTYPE_SCHEDULERECOR,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_ADD_INFO,
										CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
						/***********************************************************************************************
						 * } else if ((schdle.getStartdate()+schdle.getStarttime()).compareToIgnoreCase(currenttime)<
						 * 0) { operateInfo = new
						 * OperateInfo(String.valueOf(++infoCnt),schdle.getScheduleid(),"添加收录失败","收录失败，节目单即将开始已经不能收录");
						 * rtnInfo.appendOperateInfo(operateInfo); failed++; }
						 **********************************************************************************************/
					}
				}

				if (success == total) {
					rtnInfo.setReturnMessage("成功数量" + success);
					rtnInfo.setFlag("0");
				} else if (failed == total) {
					rtnInfo.setReturnMessage("失败数量" + failed);
					rtnInfo.setFlag("1");
				} else {
					rtnInfo.setReturnMessage("成功数量" + success + " 失败数量"
							+ failed);
					rtnInfo.setFlag("2");
				}
			} else {
				return valiChannel;
			}
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("insertScheduleRecord exception:" + e);
			operateInfo = new OperateInfo(String.valueOf(++infoCnt), "数据库更新",
					"收录失败", "收录失败：" + e.getMessage());
			rtnInfo.appendOperateInfo(operateInfo);
			rtnInfo.setFlag("1");
		}
		result = rtnInfo.toString();
		log.info("insertScheduleRecord end");
		return result;
	}

	/**
	 * getScheduleById通过节目单ID查找节目单对象
	 * 
	 * @param scheduleid 节目单ID
	 * @return CmsSchedule对象
	 * @throws Exception
	 */
	public CmsSchedule getScheduleById(String scheduleid) throws Exception {
		CmsSchedule cmsSchedule = new CmsSchedule();
		cmsSchedule.setScheduleid(scheduleid);
		List<CmsSchedule> scheduleList = null;
		try {
			scheduleList = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null != scheduleList && scheduleList.size() > 0) {
			cmsSchedule = scheduleList.get(0);
		}
		return cmsSchedule;
	}

	public String getCmsSchedule(String scheduleid) throws Exception {
		CmsSchedule cmsSchedule = new CmsSchedule();
		cmsSchedule.setScheduleid(scheduleid);
		List<CmsSchedule> scheduleList = cmsScheduleDS
				.getCmsScheduleByCond(cmsSchedule);
		if (null != scheduleList && scheduleList.size() > 0) {
			int status = scheduleList.get(0).getStatus();
			if (!(status != CmsScheduleConstant.STATUS_110
					|| status != CmsScheduleConstant.STATUS_130
					|| status != CmsScheduleConstant.STATUS_0
					|| status != CmsScheduleConstant.STATUS_20
					|| status != CmsScheduleConstant.STATUS_50
					|| status != CmsScheduleConstant.STATUS_60 || status != CmsScheduleConstant.STATUS_80)) {
				return CmsScheduleConstant.RESULT_CODE_118;
			}
		} else if (scheduleList.size() == 0) {
			return CmsScheduleConstant.RESULT_CODE_119;
		}
		return "success";
	}

	/**
	 * 查询物理频道是否存在 状态是否是已同步 及该频道的CP是否不是删除状态
	 * 
	 * @param String physicchannelid
	 * @return 查询结果 sucess为频道可用
	 * @throws Exception
	 */
	public String getPhysicChannelByid(String physicchannelid) throws Exception {// 新增节目单的 判断需要新增节目单的频道是否存在 状态时否正常 及该频道对应的CP状态是否正常
		log
				.debug("getPhysicChannelByid  start  in the class CmsScheduleLS.......................");
		long physicalchannelStatus;
		try {
			Physicalchannel physicalchannel = new Physicalchannel();
			physicalchannel.setPhysicalchannelideq(physicchannelid);
			List<Physicalchannel> PhysicalchannelList = physicalchannelDS
					.getPhysicalchannelByCond(physicalchannel);
			if (PhysicalchannelList != null && PhysicalchannelList.size() > 0) {
				physicalchannel = PhysicalchannelList.get(0);
				physicalchannelStatus = physicalchannel.getStatus();
				if (physicalchannelStatus != 20 && physicalchannelStatus != 50
						&& physicalchannelStatus != 60) {// 物理频道存在 且已同步才可同步录制计划
					return CmsScheduleConstant.RESULT_CODE_110;
				}
			} else {
				return CmsScheduleConstant.RESULT_CODE_114;// 频道已被删除
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		log
				.debug("getPhysicChannelByid  end  in the class CmsScheduleLS......................");
		return "success";
	}

	/**
	 * getScheduleRecordById根据ID查询录制计划
	 * 
	 * @param schedulerecordid 录制计划ID
	 * @return CmsSchedulerecord对象
	 * @throws Exception
	 */
	public CmsSchedulerecord getScheduleRecordById(String schedulerecordid)
			throws Exception {
		CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
		try {
			cmsSchedulerecord.setSchedulerecordid(schedulerecordid);
			List<CmsSchedulerecord> list = cmsSchedulerecordDS
					.getCmsSchedulerecordByCond(cmsSchedulerecord);
			if (list != null && list.size() > 0) {
				cmsSchedulerecord = list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cmsSchedulerecord;
	}

	/**
	 * removeCmsScheduleRecordList根据ID删除录制计划
	 * 
	 * @param schedulerecordid 录制计划ID集合
	 * @return 删除返回信息
	 * @throws Exception
	 */
	public String removeCmsScheduleRecordList(String[] scheduleRecordidList)
			throws Exception {
		log
				.debug("removeCmsScheduleList  start  in the class CmsScheduleLS.......................");
		ReturnInfo returnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		String flag = "";
		int infoCnt = 0;
		String errorcode = "";
		Integer iFail = 0;
		Integer iSuccessed = 0;

		List<CmsSchedulerecord> scheduleRecordList = new ArrayList<CmsSchedulerecord>();
		if (scheduleRecordidList != null && scheduleRecordidList.length > 0) {
			for (int i = 0; i < scheduleRecordidList.length; i++) {
				CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
				cmsSchedulerecord = this
						.getScheduleRecordById(scheduleRecordidList[i]);
				if (null != cmsSchedulerecord) {
					if (cmsSchedulerecord.getStatus() == CmsScheduleConstant.STATUS_0
							|| cmsSchedulerecord.getStatus() == CmsScheduleConstant.STATUS_80) {
						// 0待发布 ?80取消同步成功,100初始,130审核中（这些状态可删除schedulerecord）
						scheduleRecordList.add(cmsSchedulerecord);
						iSuccessed++;
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), cmsSchedulerecord
										.getScheduleid(), "删除录制计划成功",
								"删除录制计划成功");
						returnInfo.appendOperateInfo(operateInfo);
						CommonLogUtil
								.insertOperatorLog(
										scheduleRecordidList[i],
										CmsScheduleConstant.MGTTYPE_SCHEDULERECOR,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_DEL,
										CmsScheduleConstant.OPERTYPE_CMSSCHEDULERECORD_DEL_INFO,
										CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
					} else {
						iFail++;
						operateInfo = new OperateInfo(
								String.valueOf(++infoCnt), cmsSchedulerecord
										.getScheduleid(), "删除录制计划失败",
								"该录制计划于不可删除状态");
						returnInfo.appendOperateInfo(operateInfo);
						continue;
					}
				} else {
					iFail++;
					operateInfo = new OperateInfo(String.valueOf(++infoCnt),
							cmsSchedulerecord.getScheduleid(), "删除录制计划失败",
							"该录制计划已被删除");
					returnInfo.appendOperateInfo(operateInfo);
					continue;
				}
			}
		}
		if (scheduleRecordList != null && scheduleRecordList.size() > 0) {
			cmsSchedulerecordDS.removeCmsSchedulerecordList(scheduleRecordList);
		}
		if (scheduleRecordidList.length == 0
				|| iFail == scheduleRecordidList.length) {
			flag = GlobalConstants.FAIL; // "1"
			errorcode = "失败数量" + iFail + ""; // "350099"
		} else if (iFail > 0 && iFail < scheduleRecordidList.length) {
			flag = GlobalConstants.SUCCESS; // "0"
			errorcode = "成功数量" + iSuccessed + " 失败数量" + iFail + ""; // "350098"
		} else {
			flag = GlobalConstants.SUCCESS; // "0"
			errorcode = "成功数量" + iSuccessed;
		}
		returnInfo.setFlag(flag);
		returnInfo.setReturnMessage(errorcode);
		log
				.debug("removeCmsScheduleList  end  in the class CmsScheduleLS......................");
		return returnInfo.toString();
	}

	public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord,
			int start, int pageSize) throws Exception {
		log
				.debug("pageInfoQuery  starting  in the class CmsScheduleLS....................");
		TableDataInfo dataInfo = new TableDataInfo();
		DateUtil2 dateUtil = new DateUtil2();
		try {
			if (null != cmsSchedulerecord.getStatus()
					&& cmsSchedulerecord.getStatus() == 999) {
				cmsSchedulerecord.setStatus(null);
			}
			if (null != cmsSchedulerecord.getSchedulerecordid()
					&& !"".equals(cmsSchedulerecord.getSchedulerecordid())) {
				cmsSchedulerecord.setSchedulerecordid(EspecialCharMgt
						.conversion(cmsSchedulerecord.getSchedulerecordid()));
			}
			if (null != cmsSchedulerecord.getProgramname()
					&& !"".equals(cmsSchedulerecord.getProgramname())) {
				cmsSchedulerecord.setProgramname(EspecialCharMgt
						.conversion(cmsSchedulerecord.getProgramname()));
			}
			if (null != cmsSchedulerecord.getScheduleid()
					&& !"".equals(cmsSchedulerecord.getScheduleid())) {
				cmsSchedulerecord.setScheduleid(EspecialCharMgt
						.conversion(cmsSchedulerecord.getScheduleid()));
			}
			if (null != cmsSchedulerecord.getStartdate1()
					&& !"".equals(cmsSchedulerecord.getStartdate1())) {
				String time1 = dateUtil.get14Time(cmsSchedulerecord
						.getStartdate1());
				cmsSchedulerecord.setStartdate1(time1.substring(0, 8));
			}
			if (null != cmsSchedulerecord.getStartdate2()
					&& !"".equals(cmsSchedulerecord.getStartdate2())) {
				String time2 = dateUtil.get14Time(cmsSchedulerecord
						.getStartdate2());
				cmsSchedulerecord.setStartdate2((time2.substring(0, 8)));
			}
			dataInfo = cmsSchedulerecordDS.pageInfoQuery(cmsSchedulerecord,
					start, pageSize);
		} catch (DomainServiceException e) {
			log.error(e);
		}
		log
				.debug("pageInfoQuery  end  in the class CmsScheduleLS....................");
		return dataInfo;
	}
	
	
	public TableDataInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord,
            int start, int pageSize) throws Exception {
        log
                .debug("pageInfoQuery  starting  in the class CmsScheduleLS....................");
        TableDataInfo dataInfo = new TableDataInfo();
        DateUtil2 dateUtil = new DateUtil2();
        try {
            if (null != cmsSchedulerecord.getStatus()
                    && cmsSchedulerecord.getStatus() == 999) {
                cmsSchedulerecord.setStatus(null);
            }
            if (null != cmsSchedulerecord.getSchedulerecordid()
                    && !"".equals(cmsSchedulerecord.getSchedulerecordid())) {
                cmsSchedulerecord.setSchedulerecordid(EspecialCharMgt
                        .conversion(cmsSchedulerecord.getSchedulerecordid()));
            }
            if (null != cmsSchedulerecord.getProgramname()
                    && !"".equals(cmsSchedulerecord.getProgramname())) {
                cmsSchedulerecord.setProgramname(EspecialCharMgt
                        .conversion(cmsSchedulerecord.getProgramname()));
            }
            if (null != cmsSchedulerecord.getScheduleid()
                    && !"".equals(cmsSchedulerecord.getScheduleid())) {
                cmsSchedulerecord.setScheduleid(EspecialCharMgt
                        .conversion(cmsSchedulerecord.getScheduleid()));
            }
            if (null != cmsSchedulerecord.getStartdate1()
                    && !"".equals(cmsSchedulerecord.getStartdate1())) {
                String time1 = dateUtil.get14Time(cmsSchedulerecord
                        .getStartdate1());
                cmsSchedulerecord.setStartdate1(time1.substring(0, 8));
            }
            if (null != cmsSchedulerecord.getStartdate2()
                    && !"".equals(cmsSchedulerecord.getStartdate2())) {
                String time2 = dateUtil.get14Time(cmsSchedulerecord
                        .getStartdate2());
                cmsSchedulerecord.setStartdate2((time2.substring(0, 8)));
            }
            dataInfo = cmsSchedulerecordDS.pageInfoQuerychannel(cmsSchedulerecord,
                    start, pageSize);
        } catch (DomainServiceException e) {
            log.error(e);
        }
        log
                .debug("pageInfoQuery  end  in the class CmsScheduleLS....................");
        return dataInfo;
    }

	public void setLog(Logger log) {
		this.log = log;
	}

	public void setCmsScheduleDS(ICmsScheduleDS cmsScheduleDS) {
		this.cmsScheduleDS = cmsScheduleDS;
	}

	public void setChannelDS(ICmsChannelDS channelDS) {
		this.channelDS = channelDS;
	}

	public void setIcntsyncTaskDS(ICntSyncTaskDS icntsyncTaskDS) {
		this.icntsyncTaskDS = icntsyncTaskDS;
	}

	public void setBasicDS(IUcpBasicDS basicDS) {
		this.basicDS = basicDS;
	}

	public void setUsysConfigDS(IUsysConfigDS usysConfigDS) {
		this.usysConfigDS = usysConfigDS;
	}

	public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator) {
		this.primaryKeyGenerator = primaryKeyGenerator;
	}

	public void setTargetSystemDS(ITargetsystemDS targetSystemDS) {
		this.targetSystemDS = targetSystemDS;
	}

	public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS) {
		this.cntSyncRecordDS = cntSyncRecordDS;
	}

	public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS) {
		this.cmsSchedulerecordDS = cmsSchedulerecordDS;
	}

	public void setBatchObjectDS(IBatchObjectRecordDS batchObjectDS) {
		this.batchObjectDS = batchObjectDS;
	}

	public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS) {
		this.physicalchannelDS = physicalchannelDS;
	}

	public void setProgramSchedualrecordMapDS(
			IProgramSchedualrecordMapDS programSchedualrecordMapDS) {
		this.programSchedualrecordMapDS = programSchedualrecordMapDS;
	}
}

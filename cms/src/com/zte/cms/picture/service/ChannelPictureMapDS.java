package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.picture.dao.IChannelPictureMapDAO;
import com.zte.cms.picture.service.IChannelPictureMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ChannelPictureMapDS extends DynamicObjectBaseDS implements IChannelPictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IChannelPictureMapDAO dao = null;

    public void setDao(IChannelPictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException
    {
        log.debug("insert channelPictureMap starting...");
        try
        {
            if (null == channelPictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_channel_picture_map_index");
                channelPictureMap.setMapindex(mapIndex);
                channelPictureMap.setMappingid(String.format("%02d", ObjectType.CHANNELPICTUREMAP_TYPE)+ String.format("%030d", channelPictureMap.getMapindex()));
            }
            
            dao.insertChannelPictureMap(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert channelPictureMap end");
    }

    public void updateChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException
    {
        log.debug("update channelPictureMap by pk starting...");
        try
        {
            dao.updateChannelPictureMap(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelPictureMap by pk end");
    }

    public void updateChannelPictureMapList(List<ChannelPictureMap> channelPictureMapList)
            throws DomainServiceException
    {
        log.debug("update channelPictureMapList by pk starting...");
        if (null == channelPictureMapList || channelPictureMapList.size() == 0)
        {
            log.debug("there is no datas in channelPictureMapList");
            return;
        }
        try
        {
            for (ChannelPictureMap channelPictureMap : channelPictureMapList)
            {
                dao.updateChannelPictureMap(channelPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelPictureMapList by pk end");
    }

    public void removeChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException
    {
        log.debug("remove channelPictureMap by pk starting...");
        try
        {
            dao.deleteChannelPictureMap(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelPictureMap by pk end");
    }

    public void removeChannelPictureMapList(List<ChannelPictureMap> channelPictureMapList)
            throws DomainServiceException
    {
        log.debug("remove channelPictureMapList by pk starting...");
        if (null == channelPictureMapList || channelPictureMapList.size() == 0)
        {
            log.debug("there is no datas in channelPictureMapList");
            return;
        }
        try
        {
            for (ChannelPictureMap channelPictureMap : channelPictureMapList)
            {
                dao.deleteChannelPictureMap(channelPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelPictureMapList by pk end");
    }

    public ChannelPictureMap getChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException
    {
        log.debug("get channelPictureMap by pk starting...");
        ChannelPictureMap rsObj = null;
        try
        {
            rsObj = dao.getChannelPictureMap(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelPictureMapList by pk end");
        return rsObj;
    }

    public List<ChannelPictureMap> getChannelPictureMapByCond(ChannelPictureMap channelPictureMap)
            throws DomainServiceException
    {
        log.debug("get channelPictureMap by condition starting...");
        List<ChannelPictureMap> rsList = null;
        try
        {
            rsList = dao.getChannelPictureMapByCond(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelPictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get channelPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(channelPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelPictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get channelPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(channelPictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelPictureMap page info by condition end");
        return tableInfo;
    }
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get channelPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerychanPicMap(channelPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelPictureMap page info by condition end");
        return tableInfo;
    }
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get channelPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerypchanPicTargetMap(channelPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelPictureMap page info by condition end");
        return tableInfo;
    }    
    
    
    public List<ChannelPictureMap> getChannelPicturenormalMapByCond(ChannelPictureMap channelPictureMap)
            throws DomainServiceException
    {
        log.debug("get channelPictureMap by condition starting...");
        List<ChannelPictureMap> rsList = null;
        try
        {
            rsList = dao.getChannelPicturenormalMapByCond(channelPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelPictureMap by condition end");
        return rsList;
    }    
}

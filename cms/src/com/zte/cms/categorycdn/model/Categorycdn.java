package com.zte.cms.categorycdn.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Categorycdn extends DynamicBaseObject
{
    private java.lang.Long categoryindex;
    private java.lang.String categoryid;
    private java.lang.String categoryname;
    private java.lang.Long parentindex;
    private java.lang.String parentid;
    private java.lang.Integer sequence;
    private java.lang.String catagorycode;
    private java.lang.Integer activestatus;
    private java.lang.Integer ischildexists;
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String modtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private Long childnum;
    private java.lang.Integer ismapexists;
    private java.lang.String maptype;
    private java.lang.String objindex;    
    private java.lang.String parentcode;
 // 入库时间
    private String startcreatetime;
    private String endcreatetime;
    
    
    public String getStartcreatetime()
    {
        return startcreatetime;
    }

    public void setStartcreatetime(String startcreatetime)
    {
        this.startcreatetime = startcreatetime;
    }

    public String getEndcreatetime()
    {
        return endcreatetime;
    }

    public void setEndcreatetime(String endcreatetime)
    {
        this.endcreatetime = endcreatetime;
    }

    public java.lang.String getParentcode() {
		return parentcode;
	}

	public void setParentcode(java.lang.String parentcode) {
		this.parentcode = parentcode;
	}

	public java.lang.String getMaptype()
    {
        return maptype;
    }

    public void setMaptype(java.lang.String maptype)
    {
        this.maptype = maptype;
    }

    public java.lang.String getObjindex()
    {
        return objindex;
    }

    public void setObjindex(java.lang.String objindex)
    {
        this.objindex = objindex;
    }

    public java.lang.Integer getIsmapexists()
    {
        return ismapexists;
    }

    public void setIsmapexists(java.lang.Integer ismapexists)
    {
        this.ismapexists = ismapexists;
    }

    public Long getChildnum()
    {
        return childnum;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.Integer getIschildexists()
    {
        return ischildexists;
    }

    public void setIschildexists(java.lang.Integer ischildexists)
    {
        this.ischildexists = ischildexists;
    }

    public void setChildnum(Long childnum)
    {
        this.childnum = childnum;
    }

    public java.lang.Long getCategoryindex()
    {
        return categoryindex;
    }

    public void setCategoryindex(java.lang.Long categoryindex)
    {
        this.categoryindex = categoryindex;
    }

    public java.lang.String getCategoryid()
    {
        return categoryid;
    }

    public void setCategoryid(java.lang.String categoryid)
    {
        this.categoryid = categoryid;
    }

    public java.lang.String getCategoryname()
    {
        return categoryname;
    }

    public void setCategoryname(java.lang.String categoryname)
    {
        this.categoryname = categoryname;
    }

    public java.lang.Long getParentindex()
    {
        return parentindex;
    }

    public void setParentindex(java.lang.Long parentindex)
    {
        this.parentindex = parentindex;
    }

    public java.lang.String getParentid()
    {
        return parentid;
    }

    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.String getCatagorycode()
    {
        return catagorycode;
    }

    public void setCatagorycode(java.lang.String catagorycode)
    {
        this.catagorycode = catagorycode;
    }

    public java.lang.Integer getActivestatus()
    {
        return activestatus;
    }

    public void setActivestatus(java.lang.Integer activestatus)
    {
        this.activestatus = activestatus;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getModtime()
    {
        return modtime;
    }

    public void setModtime(java.lang.String modtime)
    {
        this.modtime = modtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub

    }

}

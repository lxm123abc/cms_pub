package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IScheduleWkfwhisDAO
{
    /**
     * 新增ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DAOException dao异常
     */
    public void insertScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException;

    /**
     * 根据主键更新ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DAOException dao异常
     */
    public void updateScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException;

    /**
     * 根据主键删除ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DAOException dao异常
     */
    public void deleteScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException;

    /**
     * 根据主键查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @return 满足条件的ChannelWkfwhis对象
     * @throws DAOException dao异常
     */
    public ScheduleWkfwhis getScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException;

    /**
     * 根据条件查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @return 满足条件的ChannelWkfwhis对象集
     * @throws DAOException dao异常
     */
    public List<ScheduleWkfwhis> getScheduleWkfwhisByCond(ScheduleWkfwhis scheduleWkfwhis) throws DAOException;

    /**
     * 根据条件分页查询ChannelWkfwhis对象，作为查询条件的参数
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ChannelWkfwhis对象，作为查询条件的参数
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
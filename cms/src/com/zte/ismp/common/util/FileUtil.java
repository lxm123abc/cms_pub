package com.zte.ismp.common.util;

import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.io.OutputStream;
import java.io.File;
import org.apache.log4j.Logger;
import org.apache.tools.zip.ZipOutputStream;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import java.io.FileOutputStream;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileWriter;

import java.io.BufferedInputStream;   
import java.io.BufferedOutputStream;   
 
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;   
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;   
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream; 

/**
 * <p>
 * 文件名称: FileUtil.java
 * </p>
 * <p>
 * 文件描述: 文件操作工具类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 包括各种文件操作，如文件解压（zip格式）、文件读取、文件基本操作（创建、删除、复制）
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class FileUtil
{

    // ****** 代码段: 常量定义 *******************************************************************************/

    // ****** 代码段: 变量定义 *******************************************************************************/
    private Logger logger = Logger.getLogger("com.zte.iptv.app.util.FileUtil");

    private static final int BUFFER = 2048;

    // ****** 代码段: 构造方法 *******************************************************************************/
    public FileUtil()
    {

    }

    // ****** 代码段: 文件解压*********************************************************************************/
    /**
     * 输入要进行解压的文件名，解压到unzipDir目录下
     * 
     * @param zipFilePath String
     * @param unzipDir String
     * @return String
     * @throws Exception
     */
    public int unZipFile(String zipFilePath, String unzipDir)
    {

        if (zipFilePath == null || unzipDir == null)
        {
            return 1;
        }

        try
        {
            ZipFile zf = new ZipFile(zipFilePath);
            Enumeration enu = zf.entries();
            String result = "";
            while (enu.hasMoreElements())
            {
                ZipEntry entry = (ZipEntry) enu.nextElement();
                String name = entry.getName();
                // 如果解压entry是目录,直接生成目录即可,不用写入,如果是文件,要将文件写入
                String path = unzipDir + "/" + name;
                result = result + path + "<br/>";
                File file = new File(path);
                if (entry.isDirectory())
                {
                    file.mkdirs();
                }
                else
                {
                    // 使用如下方式创建流和读取字节,不然会有乱码(当然要根据具体环境来定)
                    InputStream is = zf.getInputStream(entry);
                    byte[] buf = new byte[BUFFER];
                    int len;

                    if (!file.exists())
                    {
                        file.getParentFile().mkdirs();
                        file.createNewFile();
                    }
                    OutputStream out = new FileOutputStream(file);
                    while ((len = is.read(buf)) > 0)
                    {
                        String buf1 = new String(buf, 0, len);
                        out.write(buf, 0, len);
                    }
                    is.close();
                    out.close();
                }
            }
            zf.close();
            result = "文件解压成功,解压文件:" + result;
            logger.debug("unzip msg = " + result);
            return 0;
        }
        catch (Exception ex)
        {
            logger.debug("[UNZIP-ERROR]: " + ex);
            return 1;
        }
    }

    // ****** 代码段: 文件读取*********************************************************************************/
    /**
     * 读取资源文件
     * 
     * @param url String
     * @return String
     */
    public String readPropFile(String url)
    {

        Properties prop = null;
        String strRtn = null;
        try
        {
            File file = new File(url);
            FileInputStream is = new FileInputStream(file);
            prop = new Properties();
            prop.load(is);
            Enumeration e = prop.keys();
            while (e.hasMoreElements())
            {
                String key = (String) e.nextElement();
                strRtn = strRtn + key + "=" + prop.getProperty(key) + "&";
            }
            prop.clear();
            is.close();
            logger.debug(strRtn);
        }
        catch (Exception ex)
        {
            logger.error("读取配置文件发生异常：", ex);
        }
        return strRtn;
    }

    /**
     * 读取JAD文件
     * 
     * @param url String
     * @return String
     */
    public Properties readJadFile(String url)
    {

        Properties prop = null;
        try
        {
            File file = new File(url);
            FileInputStream is = new FileInputStream(file);
            prop = new Properties();
            prop.load(is);
            logger.debug(prop);
        }
        catch (Exception e)
        {
            logger.error("读取配置文件发生异常：", e);
        }

        return prop;
    }

    /**
     * 从zip压缩文件中读取JAD文件
     * 
     * @param url String
     * @return String
     */
    public Properties readJadFileFromZip(String url)
    {

        logger.debug("url= " + url);
        Properties prop = null;
        try
        {
            ZipFile zf = new ZipFile(url);
            Enumeration enu = zf.entries();
            while (enu.hasMoreElements())
            {
                ZipEntry entry = (ZipEntry) enu.nextElement();
                String name = entry.getName();
                logger.debug("name= " + name);

                // 压缩文件中不能存在文件夹
                if (entry.isDirectory())
                {
                    return null;
                }

                // 文件必须为指定的格式
                if (!(name.endsWith(".jar") || name.endsWith(".JAR") || name.endsWith(".jad") || name.endsWith(".JAD")))
                {
                    return null;
                }

                // 当其为文件，且以.jad或.JAD结尾时读取属性
                if (name.endsWith(".jad") || name.endsWith(".JAD"))
                {
                    // 使用如下方式创建流和读取字节,不然会有乱码(当然要根据具体环境来定)
                    InputStream is = zf.getInputStream(entry);
                    prop = new Properties();
                    prop.load(is);
                    is.close();
                    logger.debug(prop);
                }
            }
            zf.close();
        }
        catch (Exception ex)
        {
            logger.debug("[READZIP-ERROR]: " + ex);
        }
        return prop;
    }

    // ****** 代码段: 文件基本操作*********************************************************************************/
    /**
     * 新建目录
     * 
     * @param folderPath String 如 c:/fqf
     * @return boolean
     */
    public int newFolder(String folderPath)
    {

        try
        {
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            if (!myFilePath.exists())
            {
                myFilePath.mkdirs();
            }
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("新建目录操作出错：", ex);
            return 1;
        }
    }

    /**
     * 新建目录
     * 
     * @param Path String 如 c:/fqf
     * @param fileName String 如 zzz.zip
     * @return boolean
     */
    public File makeDir(String Path, String fileName)
    {

        File file = null;
        try
        {
            file = new File(Path);
            if (!file.exists())
            {// 如果目录不存在的话
                if (file.mkdirs())
                {// 如果创建目录成功的话
                    file = new File(Path, fileName);
                }
            }
            else if (file.exists())
            {// 如果目录存在的话
                file = new File(Path, fileName);
            }
        }
        catch (Exception ex)
        {
            logger.error("目录和文件出错：", ex);
        }
        return file;
    }

    /**
     * 新建文件
     * 
     * @param filePathAndName String 文件路径及名称 如c:/fqf.txt
     * @param fileContent String 文件内容
     * @return boolean
     */
    public int newFile(String filePathAndName, String fileContent)
    {

        try
        {
            String filePath = filePathAndName;
            filePath = filePath.toString();
            File myFilePath = new File(filePath);
            if (!myFilePath.exists())
            {
                myFilePath.createNewFile();
            }
            FileWriter resultFile = new FileWriter(myFilePath);
            PrintWriter myFile = new PrintWriter(resultFile);
            String strContent = fileContent;
            myFile.println(strContent);
            resultFile.close();
            myFile.close();
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("新建目录操作出错：", ex);
            return 1;
        }

    }

    /**
     * 删除文件
     * 
     * @param filePathAndName String 文件路径及名称 如c:/fqf.txt
     * @param fileContent String
     * @return boolean
     */
    public int delFile(String filePathAndName)
    {

        try
        {
            String filePath = filePathAndName;
            filePath = filePath.toString();
            java.io.File myDelFile = new java.io.File(filePath);
            myDelFile.delete();
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("删除文件操作出错：", ex);
            return 1;
        }
    }

    /**
     * 删除文件夹
     * 
     * @param filePathAndName String 文件夹路径及名称 如c:/fqf
     * @param fileContent String
     * @return boolean
     */
    public int delFolder(String folderPath)
    {

        try
        {
            delAllFile(folderPath); // 删除完里面所有内容
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            myFilePath.delete(); // 删除空文件夹
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("删除文件夹操作出错：", ex);
            return 1;
        }

    }

    /**
     * 删除文件夹里面的所有文件
     * 
     * @param path String 文件夹路径 如 c:/fqf
     */
    public int delAllFile(String path)
    {

        try
        {
            File file = new File(path);
            if (!file.exists())
            {
                return 2;
            }
            if (!file.isDirectory())
            {
                return 2;
            }
            String[] tempList = file.list();
            File temp = null;
            for (int i = 0; i < tempList.length; i++)
            {
                if (path.endsWith(File.separator))
                {
                    temp = new File(path + tempList);
                }
                else
                {
                    temp = new File(path + File.separator + tempList[i]);
                }
                if (temp.isFile())
                {
                    temp.delete();
                }
                if (temp.isDirectory())
                {
                    delAllFile(path + "/" + tempList[i]); // 先删除文件夹里面的文件
                    delFolder(path + "/" + tempList[i]); // 再删除空文件夹
                }
            }
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("删除文件出错", ex);
            return 1;
        }
    }

    /**
     * 复制单个文件
     * 
     * @param oldPath String 原文件路径 如：c:/fqf.txt
     * @param newPath String 复制后路径 如：f:/fqf.txt
     * @return boolean
     */
    public int copyFile(String oldPath, String newPath)
    {

        try
        {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldPath);
            if (oldfile.exists())
            { // 文件存在时
                InputStream inStream = new FileInputStream(oldPath); // 读入原文件
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];
                int length;
                while ((byteread = inStream.read(buffer)) != -1)
                {
                    bytesum += byteread; // 字节数 文件大小
                    // System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
                fs.close();
            }
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("复制单个文件操作出错", ex);
            return 1;
        }

    }

    /**
     * 复制整个文件夹内容
     * 
     * @param oldPath String 原文件路径 如：c:/fqf
     * @param newPath String 复制后路径 如：f:/fqf/ff
     * @return boolean
     */
    public int copyFolder(String oldPath, String newPath)
    {

        try
        {
            (new File(newPath)).mkdirs(); // 如果文件夹不存在 则建立新文件夹
            File a = new File(oldPath);
            String[] file = a.list();
            File temp = null;
            for (int i = 0; i < file.length; i++)
            {
                if (oldPath.endsWith(File.separator))
                {
                    temp = new File(oldPath + file);
                }
                else
                {
                    temp = new File(oldPath + File.separator + file);
                }

                if (temp.isFile())
                {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(newPath + "/" + (temp.getName()).toString());
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ((len = input.read(b)) != -1)
                    {
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory())
                { // 如果是子文件夹
                    copyFolder(oldPath + "/" + file, newPath + "/" + file);
                }
            }
            return 0;
        }
        catch (Exception ex)
        {
            logger.error("复制整个文件夹内容操作出错", ex);
            return 1;
        }
    }

    /**
     * 移动文件到指定目录
     * 
     * @param oldPath String 如：c:/fqf.txt
     * @param newPath String 如：d:/fqf.txt
     */
    public int moveFile(String oldPath, String newPath)
    {

        if (copyFile(oldPath, newPath) != 0)
        {
            return 1;
        }
        if (delFile(oldPath) != 0)
        {
            return 1;
        }
        return 0;
    }

    /**
     * 移动文件到指定目录
     * 
     * @param oldPath String 如：c:/fqf.txt
     * @param newPath String 如：d:/fqf.txt
     */
    public int moveFolder(String oldPath, String newPath)
    {

        if (copyFolder(oldPath, newPath) != 0)
        {
            return 1;
        }
        if (delFolder(oldPath) != 0)
        {
            return 1;
        }
        return 0;
    }

    /**
     * 获取文件的大小
     * 
     * @param url String
     * @return long
     */
    public long getFileSize(String url)
    {

        File file = new File(url);
        return file.length();
    }

    /**
     * 判断文件是否存在
     * 
     * @param fileName String 完整路径
     * @return boolean
     */
    public boolean isExistFile(String fileName)
    {

        boolean ret = false;
        File file = new File(fileName);
        if (file.exists())
        {
            ret = true;
        }
        return ret;
    }

    /**
     * 文件或文件夹更名
     * 
     * @param sourcefile String 源文件或文件夹名称 格式 c:/fqf.zip 或 c:/fqf
     * @param destfile String 目标文件或文件夹名称 格式 c:/fqf.zip 或 c:/fqf
     * @return boolean
     */
    public boolean rename(String sourcefile, String destfile)
    {

        boolean ret = false;
        File file = new File(sourcefile);
        ret = file.renameTo(new File(destfile));
        return ret;
    }

    /**
     * 打包文件
     * 
     * @throws IOException
     */
    public void zipFile(String filepath, String zipname) throws IOException
    {

        File d = new File(filepath);
        if (!d.isDirectory())
        {
            throw new IllegalArgumentException("Not a directory:  " + filepath);
        }

        String[] entries = d.list();
        byte[] buffer = new byte[4096]; // Create a buffer for copying
        int bytesRead;

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(filepath + "/" + zipname));

        for (int i = 0; i < entries.length; i++)
        {
            File f = new File(d, entries[i]);

            FileInputStream in = new FileInputStream(f);
            org.apache.tools.zip.ZipEntry entry = new org.apache.tools.zip.ZipEntry(f.getName());
            out.putNextEntry(entry);
            while ((bytesRead = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytesRead);
            }
            in.close();
        }
        out.close();
    }

    /**
     * 列出参数指定路径下的文件和文件夹列表
     * 
     * @param filepath String 路径
     * @return String[] 文件或文件夹的String数组
     */
    public String[] list(String filepath) throws IOException
    {
        File f = new File(filepath);
        String[] list = f.list();
        if (list != null && list.length > 0)
        {
            return list;
        }
        else
        {
            return null;
        }
    }

    /**
     * 遍历给定的目录以及所有的子目录，掉用相应的方法，把符合条件 的文件夹删除,仅供deleteFolderBeforeAnhour方法调用
     * 
     * @param listFiles:File类型的数组 return:void
     */
    private void iteratorDir(String compname, File[] listFiles)
    {
        if (listFiles == null)
        {
            listFiles = new File[0];
        }
        for (File file : listFiles)
        {
            String filename = file.getName();
            if (filename.indexOf(".") == -1)
            {
                filename = filename.substring(filename.length() - 17);
                if (compname.compareTo(filename) >= 0)
                {
                    if (!deleteDir(filename, file.getName(), file.getAbsolutePath()))
                    {
                        iteratorDir(filename, file.listFiles());
                    }
                }

            }

        }
    }

    /**
     * 把符合条件的目录删除，仅供iteratorDir方法调用
     * 
     * @param dirName:文件夹名
     * @param dirAbsolute return boolean:删除成功返回true,否则返回false
     */
    private boolean deleteDir(String likename, String dirName, String dirAbsolute)
    {
        Pattern pattern = Pattern.compile("\\w*(" + likename + ")\\w*");
        if (pattern.matcher(dirName).find())
        {
            FileUtil fileUtil = new FileUtil();
            fileUtil.delFolder(dirAbsolute);
        }
        return false;
    }

    /**
     * 删除一小时前的文件夹，文件夹名最后17位为创建文件夹时的时间，格式是yyyyMMddHHmmssSSS
     * 
     */

    public void deleteFolderBeforeAnhour(String filepath)
    {
        // 获取当前时间的前一小时
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.HOUR_OF_DAY, -1);
        Date d = cal.getTime();
        String compname = sdf.format(d);
        File file = new File(filepath);
        iteratorDir(compname, file.listFiles());
    }

    /**
     * 获取web路径，构建文件存放地址
     * 
     * @throws Exception
     */
    public String getLocalPath()
    {
        String localPath = RIAContext.getCurrentInstance().getRealPath("/");
        localPath = localPath.replace("//", "/");

        return localPath;
    }

    // ****** 代码段: 调试信息 *******************************************************************************/
    public static void main(String[] args)
    {

        String path = "c:/yyy";
        File f = new File(path);
        FileUtil fileUtil = new FileUtil();
        try
        {
            // fileUtil.unZipFile(path, "c:/aa/");
            System.out.println(fileUtil.rename(path, "c:/xxxx"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        // fileUtil.delFolder("c:/iptvapp/");
        // fileUtil.readJadFile("c:/suite.jad");

        // fileUtil.readJadFileFromZip(path);

    }
    
    private static final String BASE_DIR = "";     
    private static final String PATH = "/";   
    private static final String EXT1 = ".tar";  
    public static final String EXT =  ".zip";
    /**  
     * 归档  
     *   
     * @param srcPath  
     * @param destPath  
     * @throws Exception  
     */  
    public static void archive(String srcPath, String destPath)   
            throws Exception {   
  
        File srcFile = new File(srcPath);   
  
        archive(srcFile, destPath);   
  
    }  
    /**  
     * 归档文件  
     *   
     * @param srcFile  
     * @param destPath  
     * @throws Exception  
     */  
    public static void archive(File srcFile, String destPath) throws Exception {     	
         archive(srcFile, new File(destPath));
    } 
    
    /**  
     * 归档  
     *   
     * @param srcFile  
     *            源路径  
     * @param destPath  
     *            目标路径  
     * @throws Exception  
     */  
    public static void archive(File srcFile, File destFile) throws Exception {  
  
        TarArchiveOutputStream taos = new TarArchiveOutputStream(   
                new FileOutputStream(destFile));   
        File[] files = srcFile.listFiles();  
        for (File file : files) {          	  
        	archive(file, taos, BASE_DIR);   
        } 
           
        taos.flush();   
        taos.close();   
    } 
    
    private static void archive(File srcFile, TarArchiveOutputStream taos,   
            String basePath) throws Exception {   
        if (srcFile.isDirectory()) {   
            archiveDir(srcFile, taos, basePath);   
        } else {   
            archiveFile(srcFile, taos, basePath);   
        }   
    }  
    
    private static void archiveDir(File dir, TarArchiveOutputStream taos,   
            String basePath) throws Exception {   
  
        File[] files = dir.listFiles();  

        if (files.length < 1) {   
            TarArchiveEntry entry = new TarArchiveEntry(basePath   
                    + dir.getName() + PATH);   
  
            taos.putArchiveEntry(entry);   
            taos.closeArchiveEntry();   
        }   
  
        for (File file : files) {   
  
            // 递归归档   
            archive(file, taos, basePath + dir.getName() + PATH);   
  
        }   
    }   
    private static void archiveFile(File file, TarArchiveOutputStream taos,   
            String dir) throws Exception {   
  
        /**  
         * 归档内文件名定义  
         *   
         * <pre>  
         * 如果有多级目录，那么这里就需要给出包含目录的文件名  
         * 如果用WinRAR打开归档包，中文名将显示为乱码  
         * </pre>  
         */  
        TarArchiveEntry entry = new TarArchiveEntry(dir + file.getName());   
  
        entry.setSize(file.length());   
  
        taos.putArchiveEntry(entry);   
  
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(   
                file));   
        int count;   
        byte data[] = new byte[BUFFER];   
        while ((count = bis.read(data, 0, BUFFER)) != -1) {   
            taos.write(data, 0, count);   
        }   
  
        bis.close();   
  
        taos.closeArchiveEntry();   
    } 
    /**  
     * 归档  
     *   
     * @param srcFile  
     * @throws Exception  
     */  
    public static void archive(File srcFile) throws Exception {   
        String name = srcFile.getName();   
        String basePath = srcFile.getParent();   
        String destPath = basePath + name + EXT1;   
        archive(srcFile, destPath);   
    }   
  
  
  
    /**  
     * 归档  
     *   
     * @param srcPath  
     * @throws Exception  
     */  
    public static void archive(String srcPath) throws Exception {   
        File srcFile = new File(srcPath);   
  
        archive(srcFile);   
    } 
 
}

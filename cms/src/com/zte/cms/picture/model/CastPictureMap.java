package com.zte.cms.picture.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CastPictureMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.Long castindex;
    private java.lang.String castid;
    private java.lang.Long pictureindex;
    private java.lang.String pictureid;
    private java.lang.Integer maptype;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private java.lang.String mappingid;
    
    private java.lang.String castname;
    private java.lang.String onlinetimeStartDate;
    private java.lang.String onlinetimeEndDate;
    private java.lang.String cntofflinestarttime;
    private java.lang.String cntofflineendtime;
    private java.lang.String createStarttime;
    private java.lang.String createEndtime;
    
    private java.lang.Long syncindex;
    private java.lang.Long relateindex;
    private java.lang.Integer objecttype;
    private java.lang.Long objectindex;
    private java.lang.String objectid;
    private java.lang.String elementid;
    private java.lang.String parentid;
    private java.lang.Long targetindex;
    private java.lang.Integer operresult;
    private java.lang.Integer typetarget;   
    private java.lang.String typePlatform;
    private java.lang.Integer platform;
    private java.lang.Integer isfiledelete;
    
    private java.lang.Integer targettype;
    private java.lang.String targetname;
    private java.lang.Integer statuses;

    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getCastindex()
    {
        return castindex;
    }

    public void setCastindex(java.lang.Long castindex)
    {
        this.castindex = castindex;
    }

    public java.lang.String getCastid()
    {
        return castid;
    }

    public void setCastid(java.lang.String castid)
    {
        this.castid = castid;
    }

    public java.lang.Long getPictureindex()
    {
        return pictureindex;
    }

    public void setPictureindex(java.lang.Long pictureindex)
    {
        this.pictureindex = pictureindex;
    }

    public java.lang.String getPictureid()
    {
        return pictureid;
    }

    public void setPictureid(java.lang.String pictureid)
    {
        this.pictureid = pictureid;
    }

    public java.lang.Integer getMaptype()
    {
        return maptype;
    }

    public void setMaptype(java.lang.Integer maptype)
    {
        this.maptype = maptype;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public java.lang.String getCastname()
    {
        return castname;
    }

    public void setCastname(java.lang.String castname)
    {
        this.castname = castname;
    }

    public java.lang.String getOnlinetimeStartDate()
    {
        return onlinetimeStartDate;
    }

    public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
    {
        this.onlinetimeStartDate = onlinetimeStartDate;
    }

    public java.lang.String getOnlinetimeEndDate()
    {
        return onlinetimeEndDate;
    }

    public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
    {
        this.onlinetimeEndDate = onlinetimeEndDate;
    }

    public java.lang.String getCntofflinestarttime()
    {
        return cntofflinestarttime;
    }

    public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
    {
        this.cntofflinestarttime = cntofflinestarttime;
    }

    public java.lang.String getCntofflineendtime()
    {
        return cntofflineendtime;
    }

    public void setCntofflineendtime(java.lang.String cntofflineendtime)
    {
        this.cntofflineendtime = cntofflineendtime;
    }

    public java.lang.String getCreateStarttime()
    {
        return createStarttime;
    }

    public void setCreateStarttime(java.lang.String createStarttime)
    {
        this.createStarttime = createStarttime;
    }

    public java.lang.String getCreateEndtime()
    {
        return createEndtime;
    }

    public void setCreateEndtime(java.lang.String createEndtime)
    {
        this.createEndtime = createEndtime;
    }

    public java.lang.Long getSyncindex()
    {
        return syncindex;
    }

    public void setSyncindex(java.lang.Long syncindex)
    {
        this.syncindex = syncindex;
    }

    public java.lang.Long getRelateindex()
    {
        return relateindex;
    }

    public void setRelateindex(java.lang.Long relateindex)
    {
        this.relateindex = relateindex;
    }

    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    }

    public void setObjecttype(java.lang.Integer objecttype)
    {
        this.objecttype = objecttype;
    }

    public java.lang.Long getObjectindex()
    {
        return objectindex;
    }

    public void setObjectindex(java.lang.Long objectindex)
    {
        this.objectindex = objectindex;
    }

    public java.lang.String getObjectid()
    {
        return objectid;
    }

    public void setObjectid(java.lang.String objectid)
    {
        this.objectid = objectid;
    }

    public java.lang.String getElementid()
    {
        return elementid;
    }

    public void setElementid(java.lang.String elementid)
    {
        this.elementid = elementid;
    }

    public java.lang.String getParentid()
    {
        return parentid;
    }

    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }

    public java.lang.Long getTargetindex()
    {
        return targetindex;
    }

    public void setTargetindex(java.lang.Long targetindex)
    {
        this.targetindex = targetindex;
    }

    public java.lang.Integer getOperresult()
    {
        return operresult;
    }

    public void setOperresult(java.lang.Integer operresult)
    {
        this.operresult = operresult;
    }

    public java.lang.Integer getTypetarget()
    {
        return typetarget;
    }

    public void setTypetarget(java.lang.Integer typetarget)
    {
        this.typetarget = typetarget;
    }

    public java.lang.String getTypePlatform()
    {
        return typePlatform;
    }

    public void setTypePlatform(java.lang.String typePlatform)
    {
        this.typePlatform = typePlatform;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.Integer getIsfiledelete()
    {
        return isfiledelete;
    }

    public void setIsfiledelete(java.lang.Integer isfiledelete)
    {
        this.isfiledelete = isfiledelete;
    }

    public java.lang.Integer getTargettype()
    {
        return targettype;
    }

    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }
    public java.lang.Integer getStatuses()
    {
        return statuses;
    }

    public void setStatuses(java.lang.Integer statuses)
    {
        this.statuses = statuses;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("castindex", "CASTINDEX");
        this.addRelation("castid", "CASTID");
        this.addRelation("pictureindex", "PICTUREINDEX");
        this.addRelation("pictureid", "PICTUREID");
        this.addRelation("maptype", "MAPTYPE");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
    }
}

package com.zte.cms.channel.wkfw.service;

import java.util.List;

import com.zte.cms.channel.wkfw.dao.IChannelWkfwTaskForMeDAO;
import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.model.ChannelWkfwTaskForMe;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ChannelWkfwTaskForMeDS implements IChannelWkfwTaskForMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IChannelWkfwTaskForMeDAO channelWkfwTaskForMedao = null;

    public IChannelWkfwTaskForMeDAO getChannelWkfwTaskForMedao()
    {
        return channelWkfwTaskForMedao;
    }

    public void setChannelWkfwTaskForMedao(IChannelWkfwTaskForMeDAO channelWkfwTaskForMedao)
    {
        this.channelWkfwTaskForMedao = channelWkfwTaskForMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#queryStrategyWkfwTaskForMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryChannelWkfwTaskForMeListCntByCond(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        log.debug("query ChannelWkfwTaskForMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(channelWkfwTaskForMeCondition);
            totalCnt = channelWkfwTaskForMedao.querychannelWkfwTaskForMeListCntByCond(channelWkfwTaskForMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query ChannelWkfwTaskForMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get ChannelWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(channelWkfwTaskForMeCondition);
            pageInfo = channelWkfwTaskForMedao.pageInfoQuery(channelWkfwTaskForMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get ChannelWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get ChannelWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(channelWkfwTaskForMeCondition);
            pageInfo = channelWkfwTaskForMedao.pageInfoQuery(channelWkfwTaskForMeCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ChannelWkfwCondition channelWkfwTaskForMeCondition)
    {
        channelWkfwTaskForMeCondition.setChannelid(EspecialCharMgt.conversion(channelWkfwTaskForMeCondition
                .getChannelid()));

        channelWkfwTaskForMeCondition.setChannelname(EspecialCharMgt.conversion(channelWkfwTaskForMeCondition
                .getChannelname()));
    }
}

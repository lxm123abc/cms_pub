
var classObject = {}; // 实体对象
var tableObject = {}; // 表对象
var methodObject = {}; //方法列表
var orderObject = {}; // 排序条件对象，方法参数
var jsObject = {}; // JS对象，方法参数
var jsObject0 = {};
var jsObject1 = {};
var jsObject2 = {};
var jsObject3 = {};
var jsObject4 = {};
var jsObject5 = {};
var jsObject6 = {};
var jsObject7 = {};
var jsObject8 = {};
var jsObject9 = {};
var resultObject = {}; // 查询结果

function initDatabaseTypeList() {
	classObject = {};
	callUrl('classQueryDS/queryDatabaseType.ssm', null, 'classObject', true);
	if (!isEmpty(classObject)) $("#serviceBody").find("select[id='sltDatabaseType']").val(classObject.rtnValue);
}

function doBeanUserDefined() {
	var chk = $("#serviceBody").find("input[id='chkBeanUserDefined']").attr("checked");
	var tdc = $("#serviceBody").find("td[id='tdComponent']"); if (chk) {tdc.hide();} else {tdc.show();}
	var tdcud = $("#serviceBody").find("td[id='tdComponentUd']"); if (chk) {tdcud.show();} else {tdcud.hide();}
	if (chk) $("#serviceBody").find("input[id='sltComponentUd']").val("此项忽略，请点击加载接口按钮");
	var tdb = $("#serviceBody").find("td[id='tdBean']"); if (chk) {tdb.hide();} else {tdb.show();}
	var tdbud = $("#serviceBody").find("td[id='tdBeanUd']"); if (chk) {tdbud.show();} else {tdbud.hide();}
	var btn = $("#serviceBody").find("input[id='btnQueryMethod']"); if (chk) {btn.show();} else {btn.hide();}
	if (chk) $("#serviceBody").find("select[id='sltParam']").attr("length", 0); else changeBeanList();
	var tdp = $("#serviceBody").find("td[id='tdParam']").prev(); if (chk) {tdp.html("接口");} else {tdp.html("实体");}
	var tdqm = $("#serviceBody").find("td[id='tdQueryModel']"); if (chk) {tdqm.hide();} else {tdqm.show();}
	var tdqp = $("#serviceBody").find("td[id='tdQueryParam']"); if (chk) {tdqp.show();} else {tdqp.hide();}
}

function doQueryModelByClass(obj) {
	classObject = {};
	callUrl('classQueryDS/queryClassObject.ssm', '[' + obj + ']', 'classObject', true);
	clearQueryHTML();
	buildQueryHTMLByClass(classObject);
	buildQueryButton();
}

function getTableNameFormClassName(fullname) {
	var clsname = fullname.substr(fullname.lastIndexOf(".") + 1);
	var tname = "";
	for (var i=0; i<clsname.length; i++) {
		var tchr = clsname.charAt(i);
		if (is_english(tchr) && tchr == tchr.toUpperCase()) {
			tname += "_" + clsname.charAt(i).toLowerCase();
		}
		else {
			tname += tchr;
		}
	}
	return tname.substr(1);
}

function doQueryModelByTable(obj) {
	tableObject = {};
	var tableInfo = {};
	tableInfo.dtype = $("#serviceBody").find("select[id='sltDatabaseType']").val();
	tableInfo.tname = getTableNameFormClassName(obj);
	callUrl('ujdbcTableColumnDS/queryUjdbcTableColumnListByCond.ssm', tableInfo, 'tableObject', true);
	clearQueryHTML();
	buildQueryHTMLByTable(tableObject);
	buildQueryButton();
}

function doQueryModel() {
	var val = $("#serviceBody").find("select[id='sltParam']").val();
	var chk = $("#serviceBody").find("input[id='chkQueryTable']").attr("checked");
	if (chk) {doQueryModelByTable(val);} else {doQueryModelByClass(val);}
}

function doQueryMethod() {
	methodObject = {};
	var val = $("#serviceBody").find("input[id='sltBeanUd']").val();
	if (isEmpty(val)) {
		alert("请输入服务名称"); return;
	}
	if (val.indexOf("/") != -1) val = val.substring(0, val.indexOf("/"));
	callUrl('classQueryDS/queryClassMethod.ssm', '[' + val + ']', 'methodObject', true);
	appendParamHTMLByMethod(methodObject);
}

function doQueryParam() {
	var val = $("#serviceBody").find("select[id='sltParam']").val();
	
	if (isEmpty(val)) {
		alert("请先选择接口"); return;
	}
	clearQueryHTML();
	buildUserDefinedButton();
	buildQueryHTMLByParam(val);
}

function doTypeExpress(pvalue, ptype, chk) {
    
	clearQueryHTML();
	if (!chk) {
		buildReturnButton();
		newQueryProp(false, false, true, ptype, pvalue);
		return;
	}
	if (ptype.charAt(0) == '&') {
		pvalue = getJsonVal(pvalue);
		buildReturnButton(false, ptype);
		for (var pv in pvalue) {
			if (pv == "trim" || pv == "startWith" || pv == "endWith") {
				continue;
			}
			newQueryProp(true, true, is_numcode(pv), pv, createJsonStr(pvalue[pv]), ptype);
		}
		return;
	}
	if (ptype.charAt(0) == '[') {
		pvalue = getJsonVal(pvalue);
		ptype = ptype.substring(2, ptype.indexOf(";"));
		buildReturnButton(true, ptype);
		for (var i=0; i<pvalue.length; i++)
			newQueryProp(true, true, true, ptype, createJsonStr(pvalue[i]), ptype);
		return;
	}
	if (ptype.startWith("java.util.List")) {
		pvalue = getJsonVal(pvalue);
		if (ptype.indexOf(">") == -1) {
			ptype = "object";
		}
		else {
			ptype = ptype.substring(ptype.indexOf("<") + 1, ptype.indexOf(">"));
		}
		buildReturnButton(true, ptype);
		for (var i=0; i<pvalue.length; i++)
			newQueryProp(true, true, true, ptype, createJsonStr(pvalue[i]), ptype);
		return;
	}
	classObject = {};
	callUrl('classQueryDS/queryClassObject.ssm', '[' + ptype + ']', 'classObject', true);
	buildQueryHTMLByClass(classObject, getJsonVal(pvalue));
	buildReturnButton();
}

function getURL(actor, ext, ignoreext) {
	var chk = $("#serviceBody").find("input[id='chkBeanUserDefined']").attr("checked");
	var valb = chk ? $("#serviceBody").find("input[id='sltBeanUd']").val() : 
		$("#serviceBody").find("select[id='sltBean']").val();
	if (valb.indexOf("/") != -1) valb = valb.substr(valb.indexOf("/") + 1);
	var valp = ignoreext ? "" : document.getElementById("sltParam").value;
	if (!ignoreext) valp = chk ? valp.substring(0, valp.indexOf("(")) : valp.substr(valp.lastIndexOf(".") + 1);
	return valb + "/" + actor + valp + ext + ".ssm";
}

function getQueryCondByPK() {
	var obj = {};
	$("#queryBody").find("input[name='qrychk'][checked]").each(function(){
		var tid = $(this).prev().attr("id");
		var val = $(this).prev().val(); 
		if (tid == "pname") {
			var pval = $(this).parent().parent().find("input[id='pvalue']").val();
			var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
			var pobj = getJsonVal(pval);
			if (!pchk) {
				if (isEmpty(val) || is_numcode(val)) return false;
				obj.temptemp = pobj; 
				eval("obj." + val + "=obj.temptemp");
			}
			else {
				obj = pobj;
			}
		}
		else {
			tid = tid.substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			eval("obj." + tid + "=\"" + val + "\"");
		}
	});
	if (obj.temptemp != undefined) obj.temptemp = undefined;
	return obj;
}

function getQueryCond() {
	var objs = [];
	objs[0] = {};
	var i = 1;
	var hasShift = false;
	$("#queryBody").find("input[id^='txt']").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		if (!isEmpty(val)) {
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		}
		hasShift = true;
	});
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
		var pobj = getJsonVal(pval);
		if (!pchk || (i>9)) {
			if (!isEmpty(val) && !is_numcode(val)) {
				objs[0].temptemp = pobj; 
				eval("objs[0]." + val + "=objs[0].temptemp");
			}
			hasShift = true;
		}
		else {
			objs[i++] = pobj;
		}
	});
	if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
	if (!hasShift) objs.shift();
	return objs;
}

function getReturnObject(isrecursion) {
	var objs = [];
	objs[0] = {};
	var i = 1;
	var hasShift = false;
	$("#queryBody").find("input[id^='txt']").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		if (!isEmpty(val)) {
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		}
		hasShift = true;
	});
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
		var pobj = getJsonVal(pval);
		if (!pchk) {
			if (!isEmpty(val) && !is_numcode(val)) {
				objs[0].temptemp = pobj; 
				eval("objs[0]." + val + "=objs[0].temptemp");
			}
			hasShift = true;
		}
		else {
			objs[i++] = pobj;
		}
	});
	if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
	if (hasShift) {return objs[0];} else {objs.shift(); return isrecursion ? objs : objs[0];}
}

function getParamsFromQueryCond(objs) {
	var params = "";
	for (var i=0; i<objs.length; i++) {
		jsObject = objs[i];
		if (typeof(jsObject) == "string" && jsObject == "[]") jsObject = [];
		if (typeof(jsObject) == "string" && jsObject == "nvl") jsObject = null;
		eval("jsObject" + i + "=jsObject");
		params += ",jsObject" + i;
	}
	return params.substr(1);
}

function doClear() {
	$("#queryBody").find("input[id^='txt']").each(function(){
		$(this).val("");
	});
}

function doGetByUserDefined() {
	resultObject = {};
	var params = getParamsFromQueryCond(getQueryCond());
	
	callUrl(getURL("", ""), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doGetByPk() {
	resultObject = {};
	jsObject = getQueryCondByPK();
	if (isEmpty(jsObject)) {
		alert("请选择主键查询,自定义对象必须输入对象名称"); return;
	}
	callUrl(getURL("get", ""), '[jsObject]', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doGetByCond() {
	resultObject = {};
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("get", "ByCond"), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doPage(page) {
	resultObject = {};
	var pageSize = $("#tableBody").find("select[id='pageSize']").val();
	if (pageSize == undefined) pageSize = 10;
	var start = (page - 1) * pageSize;
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("pageInfoQuery", "", true) , 
		'[' + (isEmpty(params) ? '' : (params + ',')) + start + ',' + pageSize + ']', 'resultObject', true);
	buildTableHTML(resultObject,start,pageSize,null,true);
}

function doPageOrder(page,orderByColumn,isAsc) {
	resultObject = {};
	var pageSize = $("#tableBody").find("select[id='pageSize']").val();
	if (pageSize == undefined) pageSize = 10;
	var start = (page - 1) * pageSize;
	var params = getParamsFromQueryCond(getQueryCond());
	orderObject = {};
	orderObject.orderByColumn = orderByColumn;
	orderObject.isAsc = isAsc;
	callUrl(getURL("pageInfoQuery", "", true) , 
		'[' + (isEmpty(params) ? '' : (params + ',')) + start + ',' + pageSize + ',orderObject]', 'resultObject', true);
	buildTableHTML(resultObject,start,pageSize,orderByColumn,isAsc);
}

function doInsert() {
	var err = 0;
	$("#queryBody").find("input[id^='txt'][datatype]").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		var type = $(this).attr("datatype");
		var maxchar = $(this).attr("maxlength");
		if ((type == "numeric" || type == "number") && !is_numcode(val)) {
			alert(tid + "必须为数字类型"); err = 1; return false;
		}
		if ((type == "varchar" || type == "varchar2") && (getLens(val) > parseInt(maxchar))) {
			alert(tid + "不能输入超过" + maxchar + "个字符或者" + getZchLens(maxchar) + "个汉字"); err = 1; return false;
		}
	});
	if (err) return;
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("insert", ""), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	alert("调用增加操作成功");
}

function doSelectAll() {
	var tabchk = $("#xtable").contents().find("input[id='tabchk']").attr("checked");
	$("#xtable").contents().find("input[id='trchk']").each(function(){
		$(this).attr("checked", tabchk);
	});
}

function getTrObjs(isbatch) {
	var trobjs = [];
	var i = 0;
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		var trobj = {};
		$(this).parent().parent().find("input[id^='txt']").each(function(){
			var tid = $(this).attr("id").substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			var val = $(this).val();
			var obj = getJsonVal(val);
			if (!isEmpty(obj))	{
				trobj.temptemp = obj;
				eval("trobj." + tid + "=trobj.temptemp");
			}
		});
		if (trobj.temptemp != undefined) trobj.temptemp = undefined;
		trobjs[i] = trobj;
		if (!isbatch) return false;
		i++;
	});
	return trobjs;
}

function doCopyToClipBoard() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要复制的记录"); return;
	}
	jsObject = jsObject[0];
	var clipBoardContent = '';
	clipBoardContent += createJsonStr(jsObject);
	window.clipboardData.setData("Text", clipBoardContent); 
}

function doCopyListToClipBoard() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要复制的记录"); return;
	}
	var clipBoardContent = '';
	clipBoardContent += createJsonStr(jsObject);
	window.clipboardData.setData("Text", clipBoardContent); 
}

function doUpdate() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要更新的记录"); return;
	}
	jsObject = jsObject[0];
	callUrl(getURL("update", ""), '[jsObject]', 'resultObject', true);
	alert("调用修改操作成功");
}

function doUpdateList() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要更新的记录"); return;
	}
	callUrl(getURL("update", "List"), '[jsObject]', 'resultObject', true);
	alert("调用批量更新操作成功");
}

function doRemove() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要删除的记录"); return;
	}
	jsObject = jsObject[0];
	callUrl(getURL("remove", ""), '[jsObject]', 'resultObject', true);
	alert("调用删除操作成功");
}

function doRemoveList() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要删除的记录"); return;
	}
	callUrl(getURL("remove", "List"), '[jsObject]', 'resultObject', true);
	alert("调用批量删除操作成功");
}

function savePageObject() {
	var obj = {};
	var val = $("#serviceBody").find("select[id='sltDatabaseType']").val(); obj.sltDatabaseType = val;
	val = $("#serviceBody").find("input[id='chkBeanUserDefined']").attr("checked"); obj.chkBeanUserDefined = val;
	val = $("#serviceBody").find("input[id='chkQueryTable']").attr("checked"); obj.chkQueryTable = val;
	val = $("#serviceBody").find("select[id='sltComponent']").val(); obj.sltComponent = val;
	val = $("#serviceBody").find("select[id='sltBean']").val(); obj.sltBean = val;
	val = $("#serviceBody").find("input[id='sltBeanUd']").val(); obj.sltBeanUd = val;
	val = $("#serviceBody").find("select[id='sltParam']").val(); obj.sltParam = val;
	$("#queryBody").find("input[name='qrychk'][checked]").each(function(){
		var tid = $(this).prev().attr("id");
		var val = $(this).prev().val(); 
		if (tid == "pname") {
			tid = val;
		}
		else {
			tid = tid.substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		}
		obj.qrychk = tid;
	});
	if (document.getElementById("queryBody").firstChild) {
		var objs = [];
		objs[0] = {};
		objs[1] = {};
		var i = 2;
		$("#queryBody").find("input[id^='txt']").each(function(){
			var tid = $(this).attr("id").substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			var val = $(this).val();
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		});
		$("#queryBody").find("input[id='pname']").each(function(){
			var val = $(this).val();
			var pval = $(this).parent().parent().find("input[id='pvalue']").val();
			var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
			var pobj = getJsonVal(pval);
			if (!pchk) {
				if (!isEmpty(val) && !is_numcode(val)) {
					objs[1].temptemp = pobj; 
					eval("objs[1]." + val + "=objs[1].temptemp");
				}
			}
			else {
				objs[i++] = pobj;
			}
		});
		if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
		if (objs[1].temptemp != undefined) objs[1].temptemp = undefined;
		obj.querycond = objs;
	}
	$("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val(createJsonStr(obj));
}

function readPageObject() {
	var obj = getJsonVal($("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val());
	if (obj.sltDatabaseType == undefined) return;
	$("#serviceBody").find("select[id='sltDatabaseType']").val(obj.sltDatabaseType);
	$("#serviceBody").find("input[id='chkBeanUserDefined']").attr("checked", obj.chkBeanUserDefined);
	$("#serviceBody").find("input[id='chkQueryTable']").attr("checked", obj.chkQueryTable);
	if (isEmpty(obj.sltComponent)) {
		$("#serviceBody").find("select[id='sltComponent']").attr("selectedIndex", 0);
	}
	else {
		$("#serviceBody").find("select[id='sltComponent']").val(obj.sltComponent);
	}
	changeComponentList();
	$("#serviceBody").find("select[id='sltBean']").val(obj.sltBean);
	$("#serviceBody").find("input[id='sltBeanUd']").val(obj.sltBeanUd); doBeanUserDefined();
	if (isEmpty(obj.sltParam)) return;
	if (obj.chkBeanUserDefined) doQueryMethod(); else changeBeanList();
	$("#serviceBody").find("select[id='sltParam']").val(obj.sltParam);
	if (obj.querycond == undefined) return;
	var objs = obj.querycond;
	if (obj.chkBeanUserDefined) {
		doQueryParam();
		var i = 2;
		$("#queryBody").find("input[id='pname']").each(function(){
			$(this).parent().parent().find("input[id='pvalue']").val(createJsonStr(objs[i++]));
		});
		return;
	}
	doQueryModel();
	for (var tid in objs[0]) {
		$("#queryBody").find("input[id='txt" + tid.charAt(0).toUpperCase() + tid.substr(1) + "']").val(objs[0][tid]);
	}
	for (var tid in objs[1]) {
		newQueryProp(true, true, false, tid, objs[1][tid]);
	}
	for (var i=2; i<objs.length; i++) {
		newQueryProp(true, true, false, "arg" + (i-2), createJsonStr(objs[i]));
	}
}
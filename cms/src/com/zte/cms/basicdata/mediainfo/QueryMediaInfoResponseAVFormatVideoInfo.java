/**
 * QueryMediaInfoResponseAVFormatVideoInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoResponseAVFormatVideoInfo implements java.io.Serializable
{
    /* 视频编码类型，参见枚举说明文档 */
    private long videoCodingFormat;

    /* 视频编解码的子类型，参见枚举说明文档 */
    private long videoSubType;

    /* 画面X尺寸 */
    private long CXSize;

    /* 画面Y尺寸 */
    private long CYSize;

    /* 基本帧频 */
    private long standardRate;

    /* 系数（用于帧序号计算） */
    private long standardScale;

    /* 扫描模式，参见枚举说明文档 */
    private long scanMode;

    /* 视频颜色格式，参见枚举说明文档 */
    private long colorFormat;

    /* 视频码率 */
    private long videoBitrate;

    /* 是否为恒定码率编码 */
    private long isConstantRate;

    /* IBP帧压缩类型时的IBP帧组大小 */
    private long GOPSize;

    /* IBP帧压缩类型时IP帧的间隔周期 */
    private long referencePeriod;

    /* 1:Y[16-235]---normal, 0:Y[0-255]---KEY */
    private long isY16_235;

    public QueryMediaInfoResponseAVFormatVideoInfo()
    {
    }

    public QueryMediaInfoResponseAVFormatVideoInfo(long videoCodingFormat, long videoSubType, long CXSize, long CYSize,
            long standardRate, long standardScale, long scanMode, long colorFormat, long videoBitrate,
            long isConstantRate, long GOPSize, long referencePeriod, long isY16_235)
    {
        this.videoCodingFormat = videoCodingFormat;
        this.videoSubType = videoSubType;
        this.CXSize = CXSize;
        this.CYSize = CYSize;
        this.standardRate = standardRate;
        this.standardScale = standardScale;
        this.scanMode = scanMode;
        this.colorFormat = colorFormat;
        this.videoBitrate = videoBitrate;
        this.isConstantRate = isConstantRate;
        this.GOPSize = GOPSize;
        this.referencePeriod = referencePeriod;
        this.isY16_235 = isY16_235;
    }

    /**
     * Gets the videoCodingFormat value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return videoCodingFormat * 视频编码类型，参见枚举说明文档
     */
    public long getVideoCodingFormat()
    {
        return videoCodingFormat;
    }

    /**
     * Sets the videoCodingFormat value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param videoCodingFormat * 视频编码类型，参见枚举说明文档
     */
    public void setVideoCodingFormat(long videoCodingFormat)
    {
        this.videoCodingFormat = videoCodingFormat;
    }

    /**
     * Gets the videoSubType value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return videoSubType * 视频编解码的子类型，参见枚举说明文档
     */
    public long getVideoSubType()
    {
        return videoSubType;
    }

    /**
     * Sets the videoSubType value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param videoSubType * 视频编解码的子类型，参见枚举说明文档
     */
    public void setVideoSubType(long videoSubType)
    {
        this.videoSubType = videoSubType;
    }

    /**
     * Gets the CXSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return CXSize * 画面X尺寸
     */
    public long getCXSize()
    {
        return CXSize;
    }

    /**
     * Sets the CXSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param CXSize * 画面X尺寸
     */
    public void setCXSize(long CXSize)
    {
        this.CXSize = CXSize;
    }

    /**
     * Gets the CYSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return CYSize * 画面Y尺寸
     */
    public long getCYSize()
    {
        return CYSize;
    }

    /**
     * Sets the CYSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param CYSize * 画面Y尺寸
     */
    public void setCYSize(long CYSize)
    {
        this.CYSize = CYSize;
    }

    /**
     * Gets the standardRate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return standardRate * 基本帧频
     */
    public long getStandardRate()
    {
        return standardRate;
    }

    /**
     * Sets the standardRate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param standardRate * 基本帧频
     */
    public void setStandardRate(long standardRate)
    {
        this.standardRate = standardRate;
    }

    /**
     * Gets the standardScale value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return standardScale * 系数（用于帧序号计算）
     */
    public long getStandardScale()
    {
        return standardScale;
    }

    /**
     * Sets the standardScale value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param standardScale * 系数（用于帧序号计算）
     */
    public void setStandardScale(long standardScale)
    {
        this.standardScale = standardScale;
    }

    /**
     * Gets the scanMode value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return scanMode * 扫描模式，参见枚举说明文档
     */
    public long getScanMode()
    {
        return scanMode;
    }

    /**
     * Sets the scanMode value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param scanMode * 扫描模式，参见枚举说明文档
     */
    public void setScanMode(long scanMode)
    {
        this.scanMode = scanMode;
    }

    /**
     * Gets the colorFormat value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return colorFormat * 视频颜色格式，参见枚举说明文档
     */
    public long getColorFormat()
    {
        return colorFormat;
    }

    /**
     * Sets the colorFormat value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param colorFormat * 视频颜色格式，参见枚举说明文档
     */
    public void setColorFormat(long colorFormat)
    {
        this.colorFormat = colorFormat;
    }

    /**
     * Gets the videoBitrate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return videoBitrate * 视频码率
     */
    public long getVideoBitrate()
    {
        return videoBitrate;
    }

    /**
     * Sets the videoBitrate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param videoBitrate * 视频码率
     */
    public void setVideoBitrate(long videoBitrate)
    {
        this.videoBitrate = videoBitrate;
    }

    /**
     * Gets the isConstantRate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return isConstantRate * 是否为恒定码率编码
     */
    public long getIsConstantRate()
    {
        return isConstantRate;
    }

    /**
     * Sets the isConstantRate value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param isConstantRate * 是否为恒定码率编码
     */
    public void setIsConstantRate(long isConstantRate)
    {
        this.isConstantRate = isConstantRate;
    }

    /**
     * Gets the GOPSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return GOPSize * IBP帧压缩类型时的IBP帧组大小
     */
    public long getGOPSize()
    {
        return GOPSize;
    }

    /**
     * Sets the GOPSize value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param GOPSize * IBP帧压缩类型时的IBP帧组大小
     */
    public void setGOPSize(long GOPSize)
    {
        this.GOPSize = GOPSize;
    }

    /**
     * Gets the referencePeriod value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return referencePeriod * IBP帧压缩类型时IP帧的间隔周期
     */
    public long getReferencePeriod()
    {
        return referencePeriod;
    }

    /**
     * Sets the referencePeriod value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param referencePeriod * IBP帧压缩类型时IP帧的间隔周期
     */
    public void setReferencePeriod(long referencePeriod)
    {
        this.referencePeriod = referencePeriod;
    }

    /**
     * Gets the isY16_235 value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @return isY16_235 * 1:Y[16-235]---normal, 0:Y[0-255]---KEY
     */
    public long getIsY16_235()
    {
        return isY16_235;
    }

    /**
     * Sets the isY16_235 value for this QueryMediaInfoResponseAVFormatVideoInfo.
     * 
     * @param isY16_235 * 1:Y[16-235]---normal, 0:Y[0-255]---KEY
     */
    public void setIsY16_235(long isY16_235)
    {
        this.isY16_235 = isY16_235;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj)
    {
        if (!(obj instanceof QueryMediaInfoResponseAVFormatVideoInfo))
            return false;
        QueryMediaInfoResponseAVFormatVideoInfo other = (QueryMediaInfoResponseAVFormatVideoInfo) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null)
        {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && this.videoCodingFormat == other.getVideoCodingFormat()
                && this.videoSubType == other.getVideoSubType() && this.CXSize == other.getCXSize()
                && this.CYSize == other.getCYSize() && this.standardRate == other.getStandardRate()
                && this.standardScale == other.getStandardScale() && this.scanMode == other.getScanMode()
                && this.colorFormat == other.getColorFormat() && this.videoBitrate == other.getVideoBitrate()
                && this.isConstantRate == other.getIsConstantRate() && this.GOPSize == other.getGOPSize()
                && this.referencePeriod == other.getReferencePeriod() && this.isY16_235 == other.getIsY16_235();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode()
    {
        if (__hashCodeCalc)
        {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getVideoCodingFormat();
        _hashCode += getVideoSubType();
        _hashCode += getCXSize();
        _hashCode += getCYSize();
        _hashCode += getStandardRate();
        _hashCode += getStandardScale();
        _hashCode += getScanMode();
        _hashCode += getColorFormat();
        _hashCode += getVideoBitrate();
        _hashCode += getIsConstantRate();
        _hashCode += getGOPSize();
        _hashCode += getReferencePeriod();
        _hashCode += getIsY16_235();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            QueryMediaInfoResponseAVFormatVideoInfo.class, true);

    static
    {
        typeDesc
                .setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>>QueryMediaInfoResponse>AVFormat>VideoInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoCodingFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "VideoCodingFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoSubType");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "VideoSubType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CXSize");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "CXSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CYSize");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "CYSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("standardRate");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "StandardRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("standardScale");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "StandardScale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scanMode");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "ScanMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colorFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "ColorFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoBitrate");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "VideoBitrate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isConstantRate");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "IsConstantRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GOPSize");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "GOPSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencePeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "ReferencePeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isY16_235");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "IsY16_235"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}

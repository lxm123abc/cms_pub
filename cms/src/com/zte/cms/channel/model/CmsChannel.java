package com.zte.cms.channel.model;

import net.sf.cglib.beans.BeanCopier;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsChannel extends DynamicBaseObject
{
    private java.lang.Long channelindex;
    private java.lang.String channelid;
    private java.lang.Integer channelnumber;
    private java.lang.String channelname;
    private java.lang.String callsign;
    private java.lang.Integer timeshift;
    private java.lang.Integer channeltype;
    private java.lang.Integer isvalid;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.String cpcontentid;
    private java.lang.Long storageduration;
    private java.lang.Long timeshiftduration;
    private java.lang.String description;
    private java.lang.String country;
    private java.lang.String zipcode;
    private java.lang.Integer subtype;
    private java.lang.String language;
    private java.lang.Integer macrovision;
    private java.lang.String videotype;
    private java.lang.String audiotype;
    private java.lang.Integer streamtype;
    private java.lang.Integer bilingual;
    private java.lang.String weburl;
    private java.lang.String unicontentid;
    private java.lang.Long cpindex;
    private java.lang.String cpid;
    private java.lang.String sourceurl;
    private java.lang.Long licenseindex;
    private java.lang.String licenseid;
    private java.lang.String originalnamecn;
    private java.lang.Integer contenttype;
    private java.lang.String contenttypeid;
    private java.lang.String keywords;
    private java.lang.String licensingstart;
    private java.lang.String licensingend;
    private java.lang.String copyrightcn;
    private java.lang.String copyrighten;
    private java.lang.String provid;
    private java.lang.String cityid;
    private java.lang.Integer tradetype;
    private java.lang.Long exclusivevalidity;
    private java.lang.Integer copyrightexpiration;
    private java.lang.String llicensofflinetime;
    private java.lang.String deletetime;
    private java.lang.Integer platform;
    private java.lang.String nameen;
    private java.lang.String riginalnameen;
    private java.lang.String firstletter;
    private java.lang.String descen;
    private java.lang.String effectivedate;
    private java.lang.String expirydate;
    private java.lang.String title;
    private java.lang.Long clicknumber;
    private java.lang.Double pricetaxin;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String creatoropername;
    private java.lang.String submittername;
    private java.lang.Integer templatetype;
    private java.lang.String wkfwroute;
    private java.lang.String wkfwpriority;
    private java.lang.String effecttime;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;
    private String state;
    private String city;
    

    private String cpcnshortname;

    // 上线时间
    private String startOnlinetime;
    private String endOnlinetime;
 // 入库时间
    private String startcreatetime;
    private String endcreatetime;

    // 下线时间
    private String startOfflinetime;
    private String endOfflinetime;
    private java.lang.String statuses;
    
    //操作员id
    private String operid;
    
    
    public String getStartcreatetime()
    {
        return startcreatetime;
    }
    public void setStartcreatetime(String startcreatetime)
    {
        this.startcreatetime = startcreatetime;
    }
    public String getEndcreatetime()
    {
        return endcreatetime;
    }
    public void setEndcreatetime(String endcreatetime)
    {
        this.endcreatetime = endcreatetime;
    }
    public String getOperid()
    {
        return operid;
    }
    public void setOperid(String operid)
    {
        this.operid = operid;
    }
    public java.lang.String getStatuses() {
		return statuses;
	}
	public void setStatuses(java.lang.String statuses) {
		this.statuses = statuses;
	}

    public ChannelWkfwhis newStrategyWkfwhis()
    {
        ChannelWkfwhis channelWkfwhis = new ChannelWkfwhis();
        BeanCopier Copier = BeanCopier.create(CmsChannel.class, ChannelWkfwhis.class, false);
        Copier.copy(this, channelWkfwhis, null);
        return channelWkfwhis;
    }

    public String getStartOfflinetime()
    {
        return startOfflinetime;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public void setStartOfflinetime(String startOfflinetime)
    {
        this.startOfflinetime = startOfflinetime;
    }

    public String getEndOfflinetime()
    {
        return endOfflinetime;
    }

    public void setEndOfflinetime(String endOfflinetime)
    {
        this.endOfflinetime = endOfflinetime;
    }

    public String getStartOnlinetime()
    {
        return startOnlinetime;
    }

    public void setStartOnlinetime(String startOnlinetime)
    {
        this.startOnlinetime = startOnlinetime;
    }

    public String getEndOnlinetime()
    {
        return endOnlinetime;
    }

    public void setEndOnlinetime(String endOnlinetime)
    {
        this.endOnlinetime = endOnlinetime;
    }

    public String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.Long getChannelindex()
    {
        return channelindex;
    }

    public void setChannelindex(java.lang.Long channelindex)
    {
        this.channelindex = channelindex;
    }

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.Integer getChannelnumber()
    {
        return channelnumber;
    }

    public void setChannelnumber(java.lang.Integer channelnumber)
    {
        this.channelnumber = channelnumber;
    }

    public java.lang.String getChannelname()
    {
        return channelname;
    }

    public void setChannelname(java.lang.String channelname)
    {
        this.channelname = channelname;
    }

    public java.lang.String getCallsign()
    {
        return callsign;
    }

    public void setCallsign(java.lang.String callsign)
    {
        this.callsign = callsign;
    }

    public java.lang.Integer getTimeshift()
    {
        return timeshift;
    }

    public void setTimeshift(java.lang.Integer timeshift)
    {
        this.timeshift = timeshift;
    }

    public java.lang.Integer getChanneltype()
    {
        return channeltype;
    }

    public void setChanneltype(java.lang.Integer channeltype)
    {
        this.channeltype = channeltype;
    }

    public java.lang.Integer getIsvalid()
    {
        return isvalid;
    }

    public void setIsvalid(java.lang.Integer isvalid)
    {
        this.isvalid = isvalid;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.Long getStorageduration()
    {
        return storageduration;
    }

    public void setStorageduration(java.lang.Long storageduration)
    {
        this.storageduration = storageduration;
    }

    public java.lang.Long getTimeshiftduration()
    {
        return timeshiftduration;
    }

    public void setTimeshiftduration(java.lang.Long timeshiftduration)
    {
        this.timeshiftduration = timeshiftduration;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getCountry()
    {
        return country;
    }

    public void setCountry(java.lang.String country)
    {
        this.country = country;
    }

    public java.lang.String getZipcode()
    {
        return zipcode;
    }

    public void setZipcode(java.lang.String zipcode)
    {
        this.zipcode = zipcode;
    }

    public java.lang.Integer getSubtype()
    {
        return subtype;
    }

    public void setSubtype(java.lang.Integer subtype)
    {
        this.subtype = subtype;
    }

    public java.lang.String getLanguage()
    {
        return language;
    }

    public void setLanguage(java.lang.String language)
    {
        this.language = language;
    }

    public java.lang.Integer getMacrovision()
    {
        return macrovision;
    }

    public void setMacrovision(java.lang.Integer macrovision)
    {
        this.macrovision = macrovision;
    }

    public java.lang.String getVideotype()
    {
        return videotype;
    }

    public void setVideotype(java.lang.String videotype)
    {
        this.videotype = videotype;
    }

    public java.lang.String getAudiotype()
    {
        return audiotype;
    }

    public void setAudiotype(java.lang.String audiotype)
    {
        this.audiotype = audiotype;
    }

    public java.lang.Integer getStreamtype()
    {
        return streamtype;
    }

    public void setStreamtype(java.lang.Integer streamtype)
    {
        this.streamtype = streamtype;
    }

    public java.lang.Integer getBilingual()
    {
        return bilingual;
    }

    public void setBilingual(java.lang.Integer bilingual)
    {
        this.bilingual = bilingual;
    }

    public java.lang.String getWeburl()
    {
        return weburl;
    }

    public void setWeburl(java.lang.String weburl)
    {
        this.weburl = weburl;
    }

    public java.lang.String getUnicontentid()
    {
        return unicontentid;
    }

    public void setUnicontentid(java.lang.String unicontentid)
    {
        this.unicontentid = unicontentid;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getSourceurl()
    {
        return sourceurl;
    }

    public void setSourceurl(java.lang.String sourceurl)
    {
        this.sourceurl = sourceurl;
    }

    public java.lang.Long getLicenseindex()
    {
        return licenseindex;
    }

    public void setLicenseindex(java.lang.Long licenseindex)
    {
        this.licenseindex = licenseindex;
    }

    public java.lang.String getLicenseid()
    {
        return licenseid;
    }

    public void setLicenseid(java.lang.String licenseid)
    {
        this.licenseid = licenseid;
    }

    public java.lang.String getOriginalnamecn()
    {
        return originalnamecn;
    }

    public void setOriginalnamecn(java.lang.String originalnamecn)
    {
        this.originalnamecn = originalnamecn;
    }

    public java.lang.Integer getContenttype()
    {
        return contenttype;
    }

    public void setContenttype(java.lang.Integer contenttype)
    {
        this.contenttype = contenttype;
    }

    public java.lang.String getContenttypeid()
    {
        return contenttypeid;
    }

    public void setContenttypeid(java.lang.String contenttypeid)
    {
        this.contenttypeid = contenttypeid;
    }

    public java.lang.String getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.String keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.String getLicensingstart()
    {
        return licensingstart;
    }

    public void setLicensingstart(java.lang.String licensingstart)
    {
        this.licensingstart = licensingstart;
    }

    public java.lang.String getLicensingend()
    {
        return licensingend;
    }

    public void setLicensingend(java.lang.String licensingend)
    {
        this.licensingend = licensingend;
    }

    public java.lang.String getCopyrightcn()
    {
        return copyrightcn;
    }

    public void setCopyrightcn(java.lang.String copyrightcn)
    {
        this.copyrightcn = copyrightcn;
    }

    public java.lang.String getCopyrighten()
    {
        return copyrighten;
    }

    public void setCopyrighten(java.lang.String copyrighten)
    {
        this.copyrighten = copyrighten;
    }

    public java.lang.String getProvid()
    {
        return provid;
    }

    public void setProvid(java.lang.String provid)
    {
        this.provid = provid;
    }

    public java.lang.String getCityid()
    {
        return cityid;
    }

    public void setCityid(java.lang.String cityid)
    {
        this.cityid = cityid;
    }

    public java.lang.Integer getTradetype()
    {
        return tradetype;
    }

    public void setTradetype(java.lang.Integer tradetype)
    {
        this.tradetype = tradetype;
    }

    public java.lang.Long getExclusivevalidity()
    {
        return exclusivevalidity;
    }

    public void setExclusivevalidity(java.lang.Long exclusivevalidity)
    {
        this.exclusivevalidity = exclusivevalidity;
    }

    public java.lang.Integer getCopyrightexpiration()
    {
        return copyrightexpiration;
    }

    public void setCopyrightexpiration(java.lang.Integer copyrightexpiration)
    {
        this.copyrightexpiration = copyrightexpiration;
    }

    public java.lang.String getLlicensofflinetime()
    {
        return llicensofflinetime;
    }

    public void setLlicensofflinetime(java.lang.String llicensofflinetime)
    {
        this.llicensofflinetime = llicensofflinetime;
    }

    public java.lang.String getDeletetime()
    {
        return deletetime;
    }

    public void setDeletetime(java.lang.String deletetime)
    {
        this.deletetime = deletetime;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getRiginalnameen()
    {
        return riginalnameen;
    }

    public void setRiginalnameen(java.lang.String riginalnameen)
    {
        this.riginalnameen = riginalnameen;
    }

    public java.lang.String getFirstletter()
    {
        return firstletter;
    }

    public void setFirstletter(java.lang.String firstletter)
    {
        this.firstletter = firstletter;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.String getEffectivedate()
    {
        return effectivedate;
    }

    public void setEffectivedate(java.lang.String effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public java.lang.String getExpirydate()
    {
        return expirydate;
    }

    public void setExpirydate(java.lang.String expirydate)
    {
        this.expirydate = expirydate;
    }

    public java.lang.String getTitle()
    {
        return title;
    }

    public void setTitle(java.lang.String title)
    {
        this.title = title;
    }

    public java.lang.Long getClicknumber()
    {
        return clicknumber;
    }

    public void setClicknumber(java.lang.Long clicknumber)
    {
        this.clicknumber = clicknumber;
    }

    public java.lang.Double getPricetaxin()
    {
        return pricetaxin;
    }

    public void setPricetaxin(java.lang.Double pricetaxin)
    {
        this.pricetaxin = pricetaxin;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public void initRelation()
    {
        this.addRelation("channelindex", "CHANNELINDEX");
        this.addRelation("channelid", "CHANNELID");
        this.addRelation("channelnumber", "CHANNELNUMBER");
        this.addRelation("channelname", "CHANNELNAME");
        this.addRelation("callsign", "CALLSIGN");
        this.addRelation("timeshift", "TIMESHIFT");
        this.addRelation("channeltype", "CHANNELTYPE");
        this.addRelation("isvalid", "ISVALID");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("storageduration", "STORAGEDURATION");
        this.addRelation("timeshiftduration", "TIMESHIFTDURATION");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("country", "COUNTRY");
        this.addRelation("zipcode", "ZIPCODE");
        this.addRelation("subtype", "SUBTYPE");
        this.addRelation("language", "LANGUAGE");
        this.addRelation("macrovision", "MACROVISION");
        this.addRelation("videotype", "VIDEOTYPE");
        this.addRelation("audiotype", "AUDIOTYPE");
        this.addRelation("streamtype", "STREAMTYPE");
        this.addRelation("bilingual", "BILINGUAL");
        this.addRelation("weburl", "WEBURL");
        this.addRelation("unicontentid", "UNICONTENTID");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("sourceurl", "SOURCEURL");
        this.addRelation("licenseindex", "LICENSEINDEX");
        this.addRelation("licenseid", "LICENSEID");
        this.addRelation("originalnamecn", "ORIGINALNAMECN");
        this.addRelation("contenttype", "CONTENTTYPE");
        this.addRelation("contenttypeid", "CONTENTTYPEID");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("licensingstart", "LICENSINGSTART");
        this.addRelation("licensingend", "LICENSINGEND");
        this.addRelation("copyrightcn", "COPYRIGHTCN");
        this.addRelation("copyrighten", "COPYRIGHTEN");
        this.addRelation("provid", "PROVID");
        this.addRelation("cityid", "CITYID");
        this.addRelation("tradetype", "TRADETYPE");
        this.addRelation("exclusivevalidity", "EXCLUSIVEVALIDITY");
        this.addRelation("copyrightexpiration", "COPYRIGHTEXPIRATION");
        this.addRelation("llicensofflinetime", "LLICENSOFFLINETIME");
        this.addRelation("deletetime", "DELETETIME");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("riginalnameen", "RIGINALNAMEEN");
        this.addRelation("firstletter", "FIRSTLETTER");
        this.addRelation("descen", "DESCEN");
        this.addRelation("effectivedate", "EFFECTIVEDATE");
        this.addRelation("expirydate", "EXPIRYDATE");
        this.addRelation("title", "TITLE");
        this.addRelation("clicknumber", "CLICKNUMBER");
        this.addRelation("pricetaxin", "PRICETAXIN");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
    }
}


  package com.zte.cms.content.program.programsync.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ProgramPlatformSyn extends DynamicBaseObject
{
	private java.lang.Long programindex;
	private java.lang.Long targetindex;
	private java.lang.Integer status;
	private java.lang.String targetcode;
	private java.lang.String createtime;
	private java.lang.String publishtime;
	private java.lang.String cancelpubtime;

	private java.lang.String onlinetimeStartDate;
	private java.lang.String onlinetimeEndDate;
	private java.lang.String cntofflinestarttime;
	private java.lang.String cntofflineendtime;
	private java.lang.String createStarttime;
	private java.lang.String createEndtime;
	
	private java.lang.String programid;
	private java.lang.String namecn;
	private java.lang.String targetname;
	private java.lang.String targetid;
	
	private java.lang.String statuses;
	private java.lang.String targetids;
	
	private java.lang.String cpid;
	private java.lang.String cpcnshortname;
	private java.lang.String cpids;
	
	private java.lang.Long syncindex;
	private java.lang.Integer objecttype;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String elementid;
	private java.lang.String parentid;
	private java.lang.Integer platform;
	private java.lang.Integer isfiledelete;
	private java.lang.Long reserve01;
	private java.lang.String reserve02;
	
	private java.lang.String typePlatform;
	private java.lang.Integer seriesflag;
	
	private java.lang.String operid;


	public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.Integer getSeriesflag()
	{
		return seriesflag;
	}

	public void setSeriesflag(java.lang.Integer seriesflag)
	{
		this.seriesflag = seriesflag;
	}

	public void initRelation()
    {
        this.addRelation("programindex","PROGRAMINDEX");   
        this.addRelation("targetindex","TARGETINDEX");   
        this.addRelation("programid","PROGRAMID"); 
        this.addRelation("status","STATUS");   
        this.addRelation("targetcode","TARGETCODE");   
        this.addRelation("createtime","CREATETIME");   
        this.addRelation("publishtime","PUBLISHTIME");   
        this.addRelation("cancelpubtime","CANCELPUBTIME");   
        this.addRelation("reserve01","RESERVE01");   
        this.addRelation("reserve02","RESERVE02");   
        this.addRelation("syncindex","SYNCINDEX");   
        this.addRelation("objecttype","OBJECTTYPE");   
        this.addRelation("platform","PLATFORM");   
        this.addRelation("elementid","ELEMENTID");   
    }	
	
	public java.lang.Long getSyncindex()
	{
		return syncindex;
	}

	public void setSyncindex(java.lang.Long syncindex)
	{
		this.syncindex = syncindex;
	}

	public java.lang.Integer getObjecttype()
	{
		return objecttype;
	}

	public void setObjecttype(java.lang.Integer objecttype)
	{
		this.objecttype = objecttype;
	}

	public java.lang.Long getObjectindex()
	{
		return objectindex;
	}

	public void setObjectindex(java.lang.Long objectindex)
	{
		this.objectindex = objectindex;
	}

	public java.lang.String getObjectid()
	{
		return objectid;
	}

	public void setObjectid(java.lang.String objectid)
	{
		this.objectid = objectid;
	}

	public java.lang.String getElementid()
	{
		return elementid;
	}

	public void setElementid(java.lang.String elementid)
	{
		this.elementid = elementid;
	}

	public java.lang.String getParentid()
	{
		return parentid;
	}

	public void setParentid(java.lang.String parentid)
	{
		this.parentid = parentid;
	}

	public java.lang.Integer getPlatform()
	{
		return platform;
	}

	public void setPlatform(java.lang.Integer platform)
	{
		this.platform = platform;
	}

	public java.lang.Integer getIsfiledelete()
	{
		return isfiledelete;
	}

	public void setIsfiledelete(java.lang.Integer isfiledelete)
	{
		this.isfiledelete = isfiledelete;
	}

	public java.lang.Long getReserve01()
	{
		return reserve01;
	}

	public void setReserve01(java.lang.Long reserve01)
	{
		this.reserve01 = reserve01;
	}

	public java.lang.String getReserve02()
	{
		return reserve02;
	}

	public void setReserve02(java.lang.String reserve02)
	{
		this.reserve02 = reserve02;
	}

 
	
	


    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.String getStatuses()
    {
        return statuses;
    }

    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }

    public java.lang.Long getProgramindex()
    {
        return programindex;
    } 
         
    public void setProgramindex(java.lang.Long programindex) 
    {
        this.programindex = programindex;
    }
    
    public java.lang.Long getTargetindex()
    {
        return targetindex;
    } 
         
    public void setTargetindex(java.lang.Long targetindex) 
    {
        this.targetindex = targetindex;
    }
    
    public java.lang.Integer getStatus()
    {
        return status;
    } 
         
    public void setStatus(java.lang.Integer status) 
    {
        this.status = status;
    }
    
    public java.lang.String getTargetcode()
    {
        return targetcode;
    } 
         
    public void setTargetcode(java.lang.String targetcode) 
    {
        this.targetcode = targetcode;
    }
    
    public java.lang.String getCreatetime()
    {
        return createtime;
    } 
         
    public void setCreatetime(java.lang.String createtime) 
    {
        this.createtime = createtime;
    }
    
    public java.lang.String getPublishtime()
    {
        return publishtime;
    } 
         
    public void setPublishtime(java.lang.String publishtime) 
    {
        this.publishtime = publishtime;
    }
    
    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    } 
         
    public void setCancelpubtime(java.lang.String cancelpubtime) 
    {
        this.cancelpubtime = cancelpubtime;
    }


    public java.lang.String getProgramid()
    {
        return programid;
    }

    public void setProgramid(java.lang.String programid)
    {
        this.programid = programid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }
    public java.lang.String getOnlinetimeStartDate()
    {
        return onlinetimeStartDate;
    }

    public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
    {
        this.onlinetimeStartDate = onlinetimeStartDate;
    }

    public java.lang.String getOnlinetimeEndDate()
    {
        return onlinetimeEndDate;
    }

    public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
    {
        this.onlinetimeEndDate = onlinetimeEndDate;
    }

    public java.lang.String getCntofflinestarttime()
    {
        return cntofflinestarttime;
    }

    public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
    {
        this.cntofflinestarttime = cntofflinestarttime;
    }

    public java.lang.String getCntofflineendtime()
    {
        return cntofflineendtime;
    }

    public void setCntofflineendtime(java.lang.String cntofflineendtime)
    {
        this.cntofflineendtime = cntofflineendtime;
    }

    public java.lang.String getCreateStarttime()
    {
        return createStarttime;
    }

    public void setCreateStarttime(java.lang.String createStarttime)
    {
        this.createStarttime = createStarttime;
    }

    public java.lang.String getCreateEndtime()
    {
        return createEndtime;
    }

    public void setCreateEndtime(java.lang.String createEndtime)
    {
        this.createEndtime = createEndtime;
    }
    public java.lang.String getTargetid()
    {
        return targetid;
    }

    public void setTargetid(java.lang.String targetid)
    {
        this.targetid = targetid;
    }

    public java.lang.String getTargetids()
    {
        return targetids;
    }

    public void setTargetids(java.lang.String targetids)
    {
        this.targetids = targetids;
    }

    public java.lang.String getCpids()
    {
        return cpids;
    }

    public void setCpids(java.lang.String cpids)
    {
        this.cpids = cpids;
    } 
    
	public java.lang.String getTypePlatform()
	{
		return typePlatform;
	}

	public void setTypePlatform(java.lang.String typePlatform)
	{
		this.typePlatform = typePlatform;
	}

}

package com.zte.cms.notice.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zte.ucm.util.ToEmailUtil;
import org.apache.commons.lang.StringUtils;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.notice.model.Noticemap;
import com.zte.cms.notice.service.INoticemapService;
import com.zte.ismp.common.ConfigUtil;
import com.zte.purview.access.dao.OperInformationDAO;
import com.zte.purview.access.model.OperInformation;
import com.zte.purview.business.service.OperInformationDS;
import com.zte.purview.util.ContextUtils;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ucm.util.EmailServiceUtil;
import com.zte.ucm.util.KwCheck;
import com.zte.ucm.util.SmsServiceUtil;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;

public class NoticemapLS extends DynamicObjectBaseDS implements INoticemapLS
{
    private Log log = SSBBus.getLog(getClass());

    private INoticemapService noticeDS;

    private static List operGrpUselists;

    private static String SMSServiceUrl = StringUtils.trimToEmpty(ConfigUtil.get("sms_send_info"));

    private static String emailInfo = StringUtils.trimToEmpty(ConfigUtil.get("email_smtp_info"));

    private static String contractPhone = StringUtils.trimToEmpty(ConfigUtil.get("contract_Phone"));

    public void setNoticeDS(INoticemapService noticeDS)
    {
        this.noticeDS = noticeDS;
    }

    public INoticemapService getNoticeDS()
    {
        return noticeDS;
    }

    /**
     * 获取消息列表对象
     */
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        log.info("pageInfoQuery start");
        try
        {
            noticemap.setMessageid(noticemap.getMessageid());
            noticemap.setTitle(EspecialCharMgt.conversion(noticemap.getTitle()));
            noticemap.setMessagetype(noticemap.getMessagetype());
            String startCreateTime = noticemap.getCreatetime();
            if (StringUtils.isNotEmpty(startCreateTime))
            {
                startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetime(startCreateTime);
            }
            String endCreateTime = noticemap.getCreatetimeend();

            if (StringUtils.isNotEmpty(endCreateTime))
            {
                endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetimeend(endCreateTime);
            }

            if (StringUtils.isNotBlank(operId) && !"1".endsWith(operId))
            {
                noticemap.setOperatorid(operId);
            }
            else
            {
                noticemap.setOperatorid("");
            }
            tableInfo = noticeDS.pageInfoQuery(noticemap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("pageInfoQuery end");
        return tableInfo;

    }

    /**
     * 获取消息列表对象
     */
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("pageInfoQuery start");
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            noticemap.setMessageid(noticemap.getMessageid());
            noticemap.setTitle(EspecialCharMgt.conversion(noticemap.getTitle()));
            noticemap.setMessagetype(noticemap.getMessagetype());
            String startCreateTime = noticemap.getCreatetime();
            if (StringUtils.isNotEmpty(startCreateTime))
            {
                startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetime(startCreateTime);
            }

            String endCreateTime = noticemap.getCreatetimeend();
            if (StringUtils.isNotEmpty(endCreateTime))
            {
                endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetimeend(endCreateTime);
            }

            if (StringUtils.isNotBlank(operId) && !"1".endsWith(operId))
            {
                noticemap.setOperatorid(operId);
            }
            else
            {
                noticemap.setOperatorid("");
            }
            tableInfo = noticeDS.pageInfoQuery(noticemap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("pageInfoQuery end");
        return tableInfo;
    }

    /**
     *根据消息的id获取消息对象
     */
    public Noticemap getNoticemap(Integer messageid)
    {
        Noticemap noticemap = new Noticemap();
        noticemap.setMessageid(messageid);
        try
        {
            noticemap = noticeDS.getNoticemap(noticemap);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getNoticemap method:", e);
            return null;
        }
        return noticemap;
    }

    /**
     *根据消息的id获取私信对象
     */
    public Noticemap getNoticeLettermap(Integer messageid)
    {
        Noticemap noticemap = new Noticemap();
        noticemap.setMessageid(messageid);
        try
        {
            noticemap = noticeDS.getNoticeLettermap(noticemap);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getNoticemap method:", e);
            return null;
        }
        return noticemap;
    }

    /**
     * 增加消息
     */
    public String insertNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("insertNoticemap start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        String cp_message_messageid = "";
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        //在公号的末尾加上到cpoa提示
        noticemap.setDescription(noticemap.getDescription()+"请登录内容管理平台查收。有疑问请联络" +contractPhone+"。" );
        Noticemap noticemapobj = new Noticemap();
        noticemapobj.setOperatorid(operId);
        noticemapobj.setCreateid(operId);
        noticemapobj.setTitle(noticemap.getTitle());
        noticemapobj.setDescription(noticemap.getDescription());
        noticemapobj.setIssms(noticemap.getIssms());
        noticemapobj.setIsemail(noticemap.getIsemail());
        noticemapobj.setMessagetype(noticemap.getMessagetype());
        noticemapobj.setAttachname(noticemap.getAttachname());
        noticemapobj.setMessageStatus("0");
        boolean issms = false;
        boolean isemail = false;
        String title = noticemap.getTitle();
        String content =noticemap.getDescription();
        if(noticemap.getIssms() != null && noticemap.getIssms().equals("1")&&!SMSServiceUrl.equals("-1")){
            issms=true;
        }
        if(noticemap.getIsemail() != null && noticemap.getIsemail().equals("1")&&!emailInfo.equals("-1")){
            isemail=true;
        }
        //公告和私信分开入口，公告messagetype为1私信为2，
        //公告receiverid传cpid
        //私信receiverid传operatorid
        try
        {

            if (noticemap.getMessagetype() == 1)
            {
                log.info("insertNoticemap for announcement ");

                String cpidList = noticemap.getCpidlist();
                String[] cpids = cpidList.split(",");
                int length = cpids.length;
                noticemapobj.setViewnum(length);
                noticemapobj.setReceivernum(length);
                cp_message_messageid = noticeDS.insertNoticemap(noticemapobj);
                StringBuilder operatorids = new StringBuilder();
                if (StringUtils.isNotBlank(cpidList))
                {
                    for (int i = 0; i < length; i++)
                    {
                        //公告receiverid传cpid
                        noticeDS.insertCmsMessageCp(Integer.parseInt(cp_message_messageid),
                                cpids[i],
                                noticemap.getMessagetype());
                        String sql = "select operatorid from zxdbm_umap.cms_operator_power where cpid=" + cpids[i];
                        DbUtil db = new DbUtil();
                        List rtnlist = db.getQuery(sql);
                        for (int j = 0; j < rtnlist.size(); j++) {
                            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                            String operatorid=tmpMap.get("operatorid").toString();

                            noticeDS.insertCmsMessageView(cp_message_messageid,
                                    cpids[i],
                                    operatorid);
                            operatorids.append(operatorid).append(",");//把所有的id都放在一个StringBuilder里，用于后面进行查询用户的邮箱
                            //发送短信和邮件
                            if(issms){
                                sendSms(title, content, operatorid, SMSServiceUrl);
                            }

                        }
                    }
                    if(isemail){
                        sendEmail(title, content, operatorids, emailInfo);
                    }
                }
                CommonLogUtil.insertOperatorLog(cp_message_messageid,
                        "log.noticemgr",
                        "log.noticemgr.add",
                        "log.noticemgr.add.info",
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                rtnInfo.setFlag("0");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
            }
            else if (noticemap.getMessagetype() == 2)
            {//私信新增接口
                log.info("insertNoticemap for private letter ");
                String operatoridList = noticemap.getOperatoridlist();
                String[] operatorids = operatoridList.split(",");
                int length = operatorids.length;
                noticemapobj.setViewnum(length);
                noticemapobj.setReceivernum(length);
                //多选操作员 则一个操作员对应一条私信
                for (String id : operatorids)
                {
                    noticemapobj.setMessage_receiverid(id);
                    cp_message_messageid = noticeDS.insertNoticemap(noticemapobj);
                    //公告receiverid传operatorid
                    noticeDS.insertCmsMessageCp(Integer.parseInt(cp_message_messageid), id, noticemap.getMessagetype());
                    String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + id;
                    DbUtil db = new DbUtil();
                    List rtnlist = db.getQuery(sql);
                    //如果操作员绑定了cp传入cp，否则传入cp为"1"
                    if (rtnlist.size() > 0)
                    {
                        Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(0);
                        noticeDS.insertCmsMessageView(cp_message_messageid, tmpMap.get("cpid").toString(), id);
                    }
                    else
                    {
                        noticeDS.insertCmsMessageView(cp_message_messageid, "1", id);
                    }
                    //-1为私信创建人标识，用于接受私信回复消息的提示
                    noticeDS.insertCmsMessageCp(Integer.parseInt(cp_message_messageid),
                            operId,
                            noticemap.getMessagetype());
                    noticeDS.insertCmsMessageView(cp_message_messageid, "-1", operId);
                    //发送短信和邮件
                    if(issms){
                        sendSms(title, content, id, SMSServiceUrl);
                    }

                }

                if(isemail){
                    sendEmail(title, content,new StringBuilder(operatoridList) , emailInfo);//修改使用邮箱的方法，用所有id进行查询
                }
                CommonLogUtil.insertOperatorLog(cp_message_messageid,
                        "log.noticemgr",
                        "log.noticemgr.add",
                        "log.noticemgr.add.info",
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                rtnInfo.setFlag("0");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in insertNoticemap method", e);
            throw new DomainServiceException();
        }
        log.info("insertNoticemap end ");
        return rtnInfo.toString();
    }

    /**
     * 转发消息
     */
    public String forwardNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("forwardNoticemap start ");

        String resultCode = "";
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        String cp_message_messageid = "";
        int oldMessageId = noticemap.getMessageid();
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        Noticemap noticemapobj = new Noticemap();

        noticemapobj.setOperatorid(operId);
        noticemapobj.setCreateid(operId);
        noticemapobj.setTitle(noticemap.getTitle());
        noticemapobj.setDescription(noticemap.getDescription());
        noticemapobj.setIssms(noticemap.getIssms());
        noticemapobj.setIsemail(noticemap.getIsemail());
        noticemapobj.setMessagetype(2);
        noticemapobj.setAttachname(noticemap.getAttachname());
        noticemapobj.setMessageStatus("0");
        boolean issms = false;
        boolean isemail = false;
        String title = "forward message:"+noticemap.getTitle();
        String content =noticemap.getDescription();
        if(noticemap.getIssms() != null && noticemap.getIssms().equals("1")){
            issms=true;
        }
        if(noticemap.getIsemail() != null && noticemap.getIsemail().equals("1")){
            isemail=true;
        }
        //私信receiverid传operatorid
        try
        {
            //私信新增接口
            log.info("forwardNoticemap for private letter ");
            String operatoridList = noticemap.getOperatoridlist();
            String[] operatorids = operatoridList.split(",");
            int length = operatorids.length;
            noticemapobj.setViewnum(length);
            noticemapobj.setReceivernum(length);
            //多选操作员 则一个操作员对应一条私信
            for (String id : operatorids)
            {
                noticemapobj.setMessage_receiverid(id);
                cp_message_messageid = noticeDS.insertNoticemap(noticemapobj);
                //公告receiverid传operatorid
                noticeDS.insertCmsMessageCp(Integer.parseInt(cp_message_messageid), id, 2);
                String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + id;
                DbUtil db = new DbUtil();
                List rtnlist = db.getQuery(sql);
                //如果操作员绑定了cp传入cp，否则传入cp为"1"
                if (rtnlist.size() > 0)
                {
                    Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(0);
                    noticeDS.insertCmsMessageView(cp_message_messageid, tmpMap.get("cpid").toString(), id);
                }
                else
                {
                    noticeDS.insertCmsMessageView(cp_message_messageid, "1", id);
                }
                //-1为私信创建人标识，用于接受私信回复消息的提示
                noticeDS.insertCmsMessageCp(Integer.parseInt(cp_message_messageid), operId, 2);
                noticeDS.insertCmsMessageView(cp_message_messageid, "-1", operId);
                noticeDS.insertCmsMessageForward(Integer.valueOf(cp_message_messageid), oldMessageId);

                //发送短信和邮件
                if(issms){
                    sendSms(title, content, id, SMSServiceUrl);
                }

            }
            if(isemail){
                sendEmail(title, content, new StringBuilder(operatoridList), emailInfo);
            }
            CommonLogUtil.insertOperatorLog(cp_message_messageid,
                    "log.noticemgr",
                    "log.noticemgr.add",
                    "log.noticemgr.add.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            resultCode = "0:" + ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in forwardNoticemap method", e);
            resultCode = "1:" + ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
            throw new DomainServiceException();
        }
        log.info("forwardNoticemap end ");
        return resultCode;
    }

    /**
     * 增加消息与CP对应关系
     */
    public String insertCmsMessageCp(Integer messageid, String cpid) throws DomainServiceException
    {
        log.info("insertCmsMessageCp start ");
        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        Integer essagetype = 1;
        try
        {
            noticeDS.insertCmsMessageCp(messageid, cpid, essagetype);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
            // CommonLogUtil.insertOperatorLog(messageid + "", "", "", "", CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in insertCmsMessageCp method", e);
            throw new DomainServiceException();
        }
        log.info("insertCmsMessageCp end ");
        return rtnInfo.toString();
    }

    /**
     * 删除消息
     */
    public String deleteNoticemap(Noticemap noticemap) throws DomainServiceException
    {

        log.info(" deleteNoticemap start ");
        ReturnInfo rtnInfo;
        try
        {
            Noticemap noticemapmapobj = noticeDS.getNoticemap(noticemap);
            rtnInfo = new ReturnInfo();
            if (noticemapmapobj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("log.noticemgr.title.notexist"));//"消息不存在");
                return rtnInfo.toString();
            }
            if (noticemapmapobj.getAttachid() != null)
            {
                noticeDS.deleteAttach(noticemapmapobj);
            }
            noticeDS.deleteNoticemap(noticemapmapobj);
            //删除消息时，删除消息与CP对应关系 CMS_MESSAGE_CP
            noticeDS.deleteNoticeMessageCp(noticemapmapobj);
            //删除消息时，删除消息回复记录表 CMS_REPLAY
            noticeDS.deleteNoticeCmsReplay(noticemapmapobj);
            //删除消息时，删除消息查看情况记录表 cms_message_view
            noticeDS.deleteNoticeCmsMessageView(noticemapmapobj);
            CommonLogUtil.insertOperatorLog(noticemapmapobj.getMessageid() + "",
                    "log.noticemgr",
                    "log.noticemgr.del",
                    "log.noticemgr.del.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in deleteNoticemap method:", e);
            throw new DomainServiceException();
        }

        log.info(" deleteNoticemap end ");
        return rtnInfo.toString();
    }

    /**
     * 撤回消息
     */
    public String revokeNoticemap(Noticemap noticemap) throws DomainServiceException
    {

        log.info(" revokeNoticemap start ");
        ReturnInfo rtnInfo;
        try
        {
            Noticemap noticemapmapobj = noticeDS.getNoticemap(noticemap);
            rtnInfo = new ReturnInfo();
            if (noticemapmapobj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("log.noticemgr.title.notexist"));//"消息不存在");
                return rtnInfo.toString();
            }
            noticemapmapobj.setMessageStatus("1");
            noticeDS.revokeNoticemap(noticemapmapobj);
//            //删除消息时，删除消息与CP对应关系 CMS_MESSAGE_CP
//            noticeDS.deleteNoticeMessageCp(noticemapmapobj);
//            //删除消息时，删除消息回复记录表 CMS_REPLAY
//            noticeDS.deleteNoticeCmsReplay(noticemapmapobj);
//            //删除消息时，删除消息查看情况记录表 cms_message_view
//            noticeDS.deleteNoticeCmsMessageView(noticemapmapobj);
            CommonLogUtil.insertOperatorLog(noticemapmapobj.getMessageid() + "",
                    "log.noticemgr",
                    "log.noticemgr.revoke",
                    "log.noticemgr.revoke.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in deleteNoticemap method:", e);
            throw new DomainServiceException();
        }

        log.info(" revokeNoticemap end ");
        return rtnInfo.toString();
    }

    /**
     * 获取消息回复列表对象：表CMS_REPLAY
     */
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("pageInfoQuery start");
            noticemap.setCpid(EspecialCharMgt.conversion(noticemap.getCpid()));
            noticemap.setCpname(EspecialCharMgt.conversion(noticemap.getCpname()));
            noticemap.setOpername(EspecialCharMgt.conversion(noticemap.getOpername()));
            String startCreateTime = noticemap.getCreatetime();
            if (StringUtils.isNotEmpty(startCreateTime))
            {
                startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetime(startCreateTime);
            }

            String endCreateTime = noticemap.getCreatetimeend();
            if (StringUtils.isNotEmpty(endCreateTime))
            {
                endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetimeend(endCreateTime);
            }

            tableInfo = noticeDS.pageInfoQueryReplay(noticemap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("pageInfoQuery end");
        return tableInfo;

    }

    /**
     * 获取消息回复列表对象：表CMS_REPLAY
     */
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("pageInfoQuery start");
            noticemap.setCpid(EspecialCharMgt.conversion(noticemap.getCpid()));
            noticemap.setCpname(EspecialCharMgt.conversion(noticemap.getCpname()));
            noticemap.setOpername(EspecialCharMgt.conversion(noticemap.getOpername()));
            String startCreateTime = noticemap.getCreatetime();
            if (StringUtils.isNotEmpty(startCreateTime))
            {
                startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetime(startCreateTime);
            }

            String endCreateTime = noticemap.getCreatetimeend();
            if (StringUtils.isNotEmpty(endCreateTime))
            {
                endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                noticemap.setCreatetimeend(endCreateTime);
            }
            tableInfo = noticeDS.pageInfoQueryReplay(noticemap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method:", e);
            throw new DomainServiceException(e);
        }

        log.info("pageInfoQuery end");
        return tableInfo;
    }

    /**
     * 消息查看情况列表对象：表cms_message_view
     */
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("pageInfoQuery start");
            noticemap.setCpid(EspecialCharMgt.conversion(noticemap.getCpid()));
            noticemap.setCpname(noticemap.getCpname());
            noticemap.setStatus(noticemap.getStatus());
            noticemap.setSmsstatus(noticemap.getSmsstatus());
            tableInfo = noticeDS.pageInfoQueryView(noticemap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method", e);
            //
            throw new DomainServiceException(e);
        }

        log.info("pageInfoQuery end");
        return tableInfo;

    }

    /**
     * 消息查看情况列表对象：表cms_message_view
     */
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("pageInfoQuery start");
            noticemap.setCpid(EspecialCharMgt.conversion(noticemap.getCpid()));
            noticemap.setCpname(EspecialCharMgt.conversion(noticemap.getCpname()));
            noticemap.setOpername(EspecialCharMgt.conversion(noticemap.getOpername()));
            noticemap.setStatus(noticemap.getStatus());
            noticemap.setSmsstatus(noticemap.getSmsstatus());
            tableInfo = noticeDS.pageInfoQueryView(noticemap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in pageInfoQuery method", e);
            throw new DomainServiceException(e);
        }
        log.info("pageInfoQuery end");
        return tableInfo;
    }

    /**
     * 修改消息对象的属性值
     */
    public String updateNoticemap(Noticemap noticemap) throws DomainServiceException
    {
        log.info("updateNoticemap start....................");
        Noticemap tempnotice = new Noticemap();
        tempnotice.setMessageid(noticemap.getMessageid());
        tempnotice = noticeDS.getNoticemap(tempnotice);
        if (tempnotice == null)
        {
            return "1:" + ResourceMgt.findDefaultText("notice.modify.notexist");
        }

        String createtime = noticemap.getCreatetime();
        if (StringUtils.isNotEmpty(createtime))
        {
            createtime = createtime.trim().replace(" ", "").replace(":", "").replace("-", "");
            noticemap.setCreatetime(createtime);
        }
        noticeDS.updateNoticemap(noticemap);
        noticeDS.updateAttach(noticemap);

        // 写操作日志//
        CommonLogUtil.insertOperatorLog(noticemap.getMessageid() + "",
                "log.noticemgr",
                "log.noticemgr.modify",
                "log.noticemgr.modify.info",
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        log.info("updateNoticemap end......................");
        return "0:" + ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
    }

    /**
     * 点击未读消息，根据当前操作员所属cpid查询消息列表
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.info("notificationInfoQuery start");

            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + operId;
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            if (rtnlist.size() > 0)
            {
                Map<String, String> tmpMap = new HashMap<String, String>();
                tmpMap = (Map<String, String>) rtnlist.get(0);
                String cpid = tmpMap.get("cpid").toString();
                noticemap.setMessagetype(noticemap.getMessagetype());
                noticemap.setCpid(cpid);
                noticemap.setOperatorid(operId);
                tableInfo = noticeDS.notificationInfoQuery(noticemap, start, pageSize);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in notificationInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("notificationInfoQuery end");
        return tableInfo;

    }

    /**
     * 获取当前操作员所属cpid的未读消息数
     */
    @SuppressWarnings("unchecked")
    public int unViewNumQuery(Noticemap noticemap) throws DomainServiceException
    {
        log.info("unViewNumQuery start ");
        int result = 0;
        try
        {
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid='" + operId + "'";
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            if (rtnlist.size() > 0)
            {
                Map<String, String> tmpMap = new HashMap<String, String>();
                tmpMap = (Map<String, String>) rtnlist.get(0);
                String cpid = tmpMap.get("cpid").toString();
                noticemap.setCpid(cpid);
                noticemap.setOperatorid(operId);
                result = noticeDS.getNumOfUnView(noticemap);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in notificationInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("unViewNumQuery end ");
        return result;
    }

    /**
     * 通过messageid和cpid更新消息查看状态,更新未读消息CP数(自动)
     */
    public void updateView2(Noticemap noticemap) throws DomainServiceException
    {
        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        Noticemap noticemapobj = new Noticemap();
        noticemapobj.setMessageid(noticemap.getMessageid());
        try
        {
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            noticemapobj.setOperatorid(operId);
            noticeDS.updateView(noticemapobj);
            rtnInfo.setFlag("0");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in updateReplay method", e);
            throw new DomainServiceException();
        }
        log.info("updateReplay end ");
    }

    /**
     * 通过messageid和cpid更新消息查看状态,更新未读消息CP数(手动)
     */
    @SuppressWarnings("unchecked")
    public String updateView(Noticemap noticemap) throws DomainServiceException
    {
        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        int MessageType = noticemap.getMessagetype();
        Noticemap noticemapobj = new Noticemap();
        noticemapobj.setMessageid(noticemap.getMessageid());
        noticemapobj.setStatus(noticemap.getStatus());

        List rtnlist = null;
        DbUtil db = new DbUtil();
        int viewcpnum; //cms_message表中，对应cpnum - viewcpnum
        int cpnum; //cms_message表中，对应cpnum
        try
        {
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();

            String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + operId;
            rtnlist = db.getQuery(sql);
            Map<String, String> tmpMap = new HashMap<String, String>();
            tmpMap = (Map<String, String>) rtnlist.get(0);
            String cpid = String.valueOf(tmpMap.get("cpid"));
            //2012.12.10更新后，不修改私信的已读
            if (MessageType == 1)
            {
                Noticemap tempNoticemap = noticeDS.getNoticemap(noticemap);
                //获取已读消息CP数
                viewcpnum = tempNoticemap.getViewnum();
                cpnum = tempNoticemap.getReceivernum();

                tempNoticemap.setViewnum(cpnum - (viewcpnum + 1));
                //判断是私信还是公告
                if (MessageType == 1)
                {
                    //查询view表中公告是否被操作员阅读过，如果阅读过则不更新message表中的查看数
                    String sql1 = "select messageid from ZXDBM_CMS.CMS_MESSAGE_VIEW  where messageid = " + noticemap
                            .getMessageid()
                            + " and cpid = "
                            + cpid
                            + " and status=1";
                    int rtnsize = db.getQuery(sql1).size();
                    if (rtnsize == 0)
                    {
                        //更新未读消息cp的数量
                        noticeDS.updateNoticemap(tempNoticemap);
                    }
                }
                else
                {
                    //更新未读消息操作员的数量
                    noticeDS.updateNoticemap(tempNoticemap);
                }
            }
            noticemapobj.setOperatorid(operId);
            noticemapobj.setCpid(cpid);
            noticeDS.updateView(noticemapobj);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in updateReplay method", e);
            throw new DomainServiceException();
        }
        log.info("updateReplay end ");
        return rtnInfo.toString();
    }

    /**
     * 通过messageid查看消息详细内容(旧版单回复)
     */
    public Noticemap getNotificationDetail(Noticemap noticemap)
    {
        Noticemap noticeobj = new Noticemap();
        try
        {

            noticeobj = noticeDS.getNotificationDetail(noticemap);
            noticeobj.setCpid(noticemap.getCpid());
            noticeobj.setOperatorid(noticemap.getOperatorid());
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getNotificationDetail method", e);
            return null;
        }
        return noticeobj;
    }

    /**
     * 通过messageid查看消息详细内容(新版滚动多回复)
     */
    public List<Noticemap> getNotificationDetails(Noticemap noticemap)
    {
        List<Noticemap> noticeobj = null;
        try
        {
            noticeobj = noticeDS.getNotificationDetails(noticemap);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getNotificationDetail method", e);
            return null;
        }
        return noticeobj;
    }

    /**
     * 二次及多次回复时更新回复信息
     */
    public String updateReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("updateReplay start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        try
        {
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            noticemap.setOperatorid(operId);
            Noticemap tempNoticemap = noticeDS.getCmsOperatorPowerInfo(noticemap);
            noticemap.setCpid(tempNoticemap.getCpid());
            noticeDS.updateReplay(noticemap);

            //更新消息表中最后回复人以及最后回复时间
            noticemap.setLastReplayid(noticemap.getOperatorid());
            noticeDS.updateNoticeReplay(noticemap);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in updateReplay method", e);
            throw new DomainServiceException();
        }
        log.info("updateReplay end ");
        return rtnInfo.toString();
    }

    /**
     * 标记状态已读(手动)
     */
    public String setStatusRead(Noticemap noticemap) throws DomainServiceException
    {
        log.info("setStatusRead start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        //先判断是否有未读的数据，如果没有直接返回成功
        if (noticemap.getAttachname().equals("") || noticemap.getAttachurl().equals(""))
        {
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
            return rtnInfo.toString();
        }
        String[] messageids = noticemap.getAttachname().split(",");
        String[] messagetypes = noticemap.getAttachurl().split(",");
        try
        {
            //遍历更新数据的查看情况
            for (int i = 0, length = messageids.length; i < length; i++)
            {
                noticemap.setMessageid(Integer.valueOf(messageids[i]));
                int MessageType = Integer.valueOf(messagetypes[i]);
                Noticemap noticemapobj = new Noticemap();
                noticemapobj.setMessageid(noticemap.getMessageid());
                noticemapobj.setStatus(noticemap.getStatus());

                List rtnlist = null;
                DbUtil db = new DbUtil();
                int viewcpnum; //cms_message表中，对应cpnum - viewcpnum
                int cpnum; //cms_message表中，对应cpnum

                OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
                // 操作员编码
                String operId = loginUser.getOperid();
                String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + operId;
                //2012.12.10更新后，不修改私信的已读
                if (MessageType == 1)
                {
                    Noticemap tempNoticemap = noticeDS.getNoticemap(noticemap);
                    //获取已读消息CP数
                    viewcpnum = tempNoticemap.getViewnum();
                    cpnum = tempNoticemap.getReceivernum();

                    tempNoticemap.setViewnum(cpnum - (viewcpnum + 1));
                    //判断是私信还是公告
                    if (MessageType == 1)
                    {
                        //查询view表中公告是否被操作员阅读过，如果阅读过则不更新message表中的查看数
                        String sql1 = "select messageid from ZXDBM_CMS.CMS_MESSAGE_VIEW  where messageid = " + noticemap
                                .getMessageid()
                                + " and cpid = "
                                + noticemap.getCpid()
                                + " and status=1";
                        int rtnsize = db.getQuery(sql1).size();
                        if (rtnsize == 0)
                        {
                            //更新未读消息cp的数量
                            noticeDS.updateNoticemap(tempNoticemap);
                        }
                    }
                    else
                    {
                        //更新未读消息操作员的数量
                        noticeDS.updateNoticemap(tempNoticemap);
                    }
                }
                rtnlist = db.getQuery(sql);
                Map<String, String> tmpMap = new HashMap<String, String>();
                tmpMap = (Map<String, String>) rtnlist.get(0);
                String cpid = String.valueOf(tmpMap.get("cpid"));
                noticemapobj.setOperatorid(operId);
                noticemapobj.setCpid(cpid);
                noticeDS.updateView(noticemapobj);
            }
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in setStatusRead method", e);
            throw new DomainServiceException();
        }
        log.info("setStatusRead end ");
        return rtnInfo.toString();
    }
    /**
     * 操作员通过未读私信页面删除私信
     */
    public String messageDel(Noticemap noticemap) throws DomainServiceException
    {
        log.info("messageDel start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        Noticemap noticemapobj = new Noticemap();
        noticemapobj.setMessageid(noticemap.getMessageid());
        noticemapobj.setMessagetype(noticemap.getMessagetype());
        noticemapobj.setCpid(noticemap.getCpid());
        try
        {
            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            noticemapobj.setOperatorid(operId);
            noticeDS.updateView(noticemapobj);
            noticeDS.updateReceiver(noticemapobj);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in messageDel method", e);
            throw new DomainServiceException();
        }
        log.info("messageDel end ");
        return rtnInfo.toString();
    }
    /**
     * 初次回复时向CMS_REPLAY中插入回复记录
     */
    @SuppressWarnings("unchecked")
    public String insertReplay(Noticemap noticemap) throws DomainServiceException
    {
        log.info("insertReplay start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        String cp_message_replayid = "";
        try
        {
            Noticemap noticemapobj = new Noticemap();
            noticemapobj.setMessageid(noticemap.getMessageid());
            noticemapobj.setReplaydescription(noticemap.getReplaydescription());

            OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
            // 操作员编码
            String operId = loginUser.getOperid();
            //如果现在登录的是创建者
            if (operId.equals(noticemap.getCreateid()))
            {
                noticemapobj.setOperatorid(operId);
                //给view更新借用Createid
                noticemapobj.setCreateid(noticemap.getMessage_receiverid());
            }
            //如果现在登录的是接受者
            else if (operId.equals(noticemap.getMessage_receiverid()))
            {
                noticemapobj.setRepaly_receiverid(operId);
                //给view更新借用Createid
                noticemapobj.setCreateid(noticemap.getCreateid());

            }
            noticemapobj.setLastReplayid(operId);
            //            String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + operId;
            //            List rtnlist = null;
            //
            //            DbUtil db = new DbUtil();
            //            rtnlist = db.getQuery(sql);
            //            Map<String, String> tmpMap = new HashMap<String, String>();
            //            tmpMap = (Map<String, String>) rtnlist.get(0);
            //            String cpid = tmpMap.get("cpid").toString();
            //            noticemapobj.setCpid(cpid);
            cp_message_replayid = noticeDS.insertReplay(noticemapobj);
            //更新消息表中最后回复人以及最后回复时间
            //            noticemapobj.setLastReplayid(noticemapobj.getOperatorid());
            //            noticeDS.updateNoticeReplay(noticemapobj);

            CommonLogUtil.insertOperatorLog(cp_message_replayid,
                    "log.noticemgr",
                    "log.noticemgr.addreplay",
                    "log.noticemgr.addreplay.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in insertReplay method", e);
            noticemap = new Noticemap();
            if (StringUtils.isNotBlank(cp_message_replayid))
            {
                noticemap.setReplayid(Integer.parseInt(cp_message_replayid));
                noticeDS.deleteReplay(noticemap);
            }
            throw new DomainServiceException();
        }
        log.info("insertReplay end ");
        return rtnInfo.toString();
    }

    /**
     * 获取用户的权限组
     */
    @SuppressWarnings("unchecked")
    public boolean getUserOperRights() throws DomainServiceException
    {
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        log.info("Get OperId:" + operId + " OperRights Start");
        boolean result = false;
        try
        {
            //super直接通过
            if (operId.equals("1"))
            {
                return true;
            }
            String sql = "select * from  zxinsys.oper_rights where operid = " + operId;
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            for (int j = 0; j < rtnlist.size(); j++)
            {
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                //判断操作员下是否有系统管理组权限
                if (tmpMap.get("opergrpid").toString().equals("1000"))
                {
                    result = true;
                    break;
                }
            }
            log.info("Get OperId:" + operId + " OperRights Success");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Get OperId:" + operId + " OperRights Fail", e);
        }
        return result;
    }

    /**
     * 获取所有有系统管理员组权限的操作员
     * @return
     */
    @SuppressWarnings("rawtypes")
    private static List getOperGrpUselists()
    {
        NoticemapLS.setOperGrpUselists();
        return NoticemapLS.operGrpUselists;
    }

    public static void setOperGrpUselists()
    {
        String sql = "select operid from  zxinsys.oper_rights where opergrpid = 1000";
        DbUtil db = new DbUtil();
        try
        {
            operGrpUselists = db.getQuery(sql);
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 查询操作员列表
     */
    public TableDataInfo operInfoQuery(OperInformation operInfo, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        String sql = "select * from  zxinsys.oper_rights where opergrpid = 1000 and operid = " + operId;
        DbUtil db = new DbUtil();
        List rsList = null;
        try
        {
            rsList = db.getQuery(sql);
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
            log.error("Error occurred in isAdminQuery method", e1);
        }
        log.info("operInfoQuery start");
        try
        {
            if (!rsList.isEmpty())
            {
                //说明是管理员，可以发送私信给所有操作员
                tableInfo = noticeDS.operInfoQuery(operInfo, start, pageSize);
            }
            else
            {
                //说明是cp操作员，只能发送私信给管理员
                tableInfo = noticeDS.adminInfoQuery(operInfo, start, pageSize);
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in operInfoQuery method", e);
            throw new DomainServiceException(e);
        }

        log.info("operInfoQuery end");
        return tableInfo;
    }

    /**
     * 通过creatid查询账号信息（暂时未用）
     */
    public TableDataInfo getOrderedChildOperatorsDataInfo(long operId, String filter, int rangeStart, int fetchSize)
    {
        TableDataInfo tableInfo = new TableDataInfo();
        // 操作员编码
        String sql = "select * from  zxinsys.oper_rights where opergrpid = 1000 and operid = " + operId;
        DbUtil db = new DbUtil();
        List rsList = null;
        try
        {
            rsList = db.getQuery(sql);
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
            log.error("Error occurred in isAdminQuery method", e1);
        }
        log.info("getOrderedChildOperatorsDataInfo start");
        try
        {

            if (!rsList.isEmpty())
            {
                System.out.println("是管理员，rangeStart：" + rangeStart + ",fetchSize:" + fetchSize + ",filter:" + filter);
                //说明是管理员，查看所有的账号信息
                String serviceKey = (String) ContextUtils.getSessionAttribute("SERVICEKEY");
                String operinfoSql = "select o.* from  zxinsys.oper_information o where creatorid != 0 and reserve3='"
                        + serviceKey + "'";
                //String operinfoSql = "select * from(select o.*, ROWNUM RN  from  zxinsys.oper_information o where creatorid != 0 and reserve3='" + serviceKey + "' and ROWNUM <= "+rangeStart+fetchSize+" order by operid desc)  WHERE RN >= "+rangeStart;
                List<OperInformation> rsListOperinfo = db.getQuery(operinfoSql);
                for (int i = 0; i < rsListOperinfo.size(); i++)
                {
                    //int operstatus = rsListOperinfo.get(i).getOperstatus().intValue();
                    OperInformation child = (OperInformation) rsListOperinfo.get(i);
                    //rsListOperinfo.get(i).setOperStatusText(updateOperStatusText(operstatus));
                }
                String cnt = "select * from  zxinsys.oper_information where creatorid != 0 and reserve3='" + serviceKey
                        + "'";
                List rsListcnt = db.getQuery(cnt);
                PageInfo pageInfo = new PageInfo(rangeStart, rsListcnt.size(), fetchSize, rsListOperinfo);
                tableInfo.setData((List<OperInformation>) pageInfo.getResult());
                tableInfo.setTotalCount((int) pageInfo.getTotalCount());

            }
            else
            {
                System.out.println("是操作员");
                //说明是cp操作员，只能查看自己
                OperInformationDS opifmdDs = new OperInformationDS();

                tableInfo = opifmdDs.getOrderedChildOperatorsDataInfo(operId, filter, rangeStart, fetchSize);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in operInfoQuery method", e);
        }

        log.info("getOrderedChildOperatorsDataInfo end");
        return tableInfo;

    }

    private String updateOperStatusText(int operstatus)
    {
        String operStatusText = null;
        String locale_lang = RIAContext.getCurrentInstance()
                .getRequest()
                .getSession()
                .getServletContext()
                .getInitParameter("com.zte.ssb.ria.webdeploylanguage");
        if (locale_lang.equalsIgnoreCase("zh"))
        {
            if (operstatus == 0)
            {
                operStatusText = "正常";
            }
            else if (operstatus == 1)
            {
                operStatusText = "永久锁定";
            }
            else if (operstatus == 2)
            {
                operStatusText = "黑名单";
            }
            else if (operstatus == 3)
            {
                operStatusText = "禁止";
            }
            else if (operstatus == 4)
            {
                operStatusText = "永久锁定/禁止";
            }
            else if (operstatus == 5)
            {
                operStatusText = "限时锁定";
            }
            else if (operstatus == 8)
            {
                operStatusText = "限时锁定/禁止";
            }
            else if (operstatus > 9)
            {
                operStatusText = "黑名单";
            }
            else
            {
                operStatusText = String.valueOf(operstatus) + ": 未知状态";
            }
        }
        if (locale_lang.equalsIgnoreCase("en"))
        {
            if (operstatus == 0)
            {
                operStatusText = "normal";
            }
            else if (operstatus == 1)
            {
                operStatusText = "lock";
            }
            else if (operstatus == 2)
            {
                operStatusText = "blacklist";
            }
            else if (operstatus == 3)
            {
                operStatusText = "forbiden";
            }
            else if (operstatus == 4)
            {
                operStatusText = "lock/forbiden";
            }
            else if (operstatus == 5)
            {
                operStatusText = "limitedLock";
            }
            else if (operstatus == 8)
            {
                operStatusText = "limitedLock/forbiden";
            }
            else if (operstatus > 9)
            {
                operStatusText = "blacklist";
            }
            else
            {
                operStatusText = String.valueOf(operstatus) + ": unknow status";
            }
        }
        return operStatusText;
    }

    /**
     * 新增操作员之前把设置creatid为1
     */
    public boolean changeOperId(Map<String, String> params)
    {
        boolean success = false;
        try
        {
            RIAContext context = RIAContext.getCurrentInstance();
            ISession session = context.getSession();
            HttpSession httpSession = session.getHttpSession();
            String oldSession = (String) httpSession.getAttribute("OPERID");
            httpSession.setAttribute("OPERID", "1");
            httpSession.setAttribute("OLDOPERID", oldSession);
        }
        catch (Exception exp)
        {
            log.error(exp);
        }
        return success;
    }

    /**
     * 新增操作员之后还原creatid
     */
    public boolean changeOperIdB(Map<String, String> params)
    {
        boolean success = false;
        try
        {
            RIAContext context = RIAContext.getCurrentInstance();
            ISession session = context.getSession();
            HttpSession httpSession = session.getHttpSession();
            httpSession.setAttribute("OPERID", httpSession.getAttribute("OLDOPERID"));
        }
        catch (Exception exp)
        {
            log.error(exp);
        }
        return success;
    }

    /**
     * 发送短信
     * @param title 短信标题
     * @param content   短信内容
     * @param operatorid    收件人
     * @param SMSServiceUrl     短信接口信息
     */
    public void sendSms(String title,String content,String operatorid,String SMSServiceUrl)
    {
        try
        {
            String sql = "select phonenumber from zxinsys.oper_information where operid = " + operatorid;
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(0);
            String phonenumber = KwCheck.checkNull(tmpMap.get("phonenumber")).toString();
            if(phonenumber != null && phonenumber != ""){
                //发送短信
                SmsServiceUtil.SmsSendPost(title,content,phonenumber,SMSServiceUrl);

            }else{
                System.out.println("操作员："+operatorid+"没有配置手机号！");
            }
        }
        catch (Exception exp)
        {
            log.error(exp);
        }
    }

    /**
     * 发送邮件
     * @param title 邮件标题
     * @param content   邮件内容
     * @param operatorid    收件人
     * @param SMSServiceUrl    邮箱服务器信息
     */
    public void sendEmail(String title,String content,StringBuilder operatorid,String emailInfo)
    {

        try
        {
            //判断cp最后一位是否有“，”，没有就用传进来的id，否则就截取掉最后一位的符号“，”
            if(",".equals(operatorid.substring(operatorid.length()-1))){
                operatorid = operatorid.deleteCharAt(operatorid.length()-1);
            }

            String sql = "select email from zxinsys.oper_information where operid in( " + operatorid + ")";
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            StringBuilder emails = new StringBuilder();

            for(int i=0;i<rtnlist.size();i++){
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(i);
                emails.append(KwCheck.checkNull(tmpMap.get("email")).toString()).append(",");
            }
            if(emails != null &&  !"".equals(emails)){
                //发送邮件，在线程中发送邮件，解决一次性发送多封邮件导致的数据库连接中断
                ToEmailUtil toEmailUtil = new ToEmailUtil(emails.toString(),title,content,emailInfo);
                Thread thread = new Thread(toEmailUtil);
                thread.start();
                //EmailServiceUtil.send(title, content, emails.toString(), emailInfo);
            }else{
                System.out.println("操作员："+operatorid+"没有配置邮箱！");
            }
        }
        catch (Exception exp)
        {
            log.error(exp);
        }
    }
}

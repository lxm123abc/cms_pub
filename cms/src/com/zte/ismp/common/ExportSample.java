package com.zte.ismp.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.tools.zip.ZipOutputStream;

import com.zte.ssb.servicecontainer.business.server.RIAContext;

/**
 * 导出Excel示例类
 * 
 * @author caohongbin *
 */
public class ExportSample
{

    /**
     * 示例方法，数据分Excel文件导出并打包成zip文件，Map对象列表方式
     * 
     * @return
     * @throws Exception
     */
    public String exportExcelZipByMap() throws Exception
    {
        String returnStr = null;

        try
        {
            // 约定导出excel的列数，4列如下：
            // 第一列：excel显示名称--编号，数据字段名称--samid
            // 第二列：excel显示名称--名称，数据字段名称--samname
            // 第三列：excel显示名称--描述，数据字段名称--samdesc
            // 第四列：excel显示名称--创建日期，数据字段名称--createtime
            // 根据上面约定，则表头headList如下：
            List<ExcelHead> headList = new ArrayList<ExcelHead>();
            headList.add(new ExcelHead("samid", "编号"));
            headList.add(new ExcelHead("samname", "名称"));
            headList.add(new ExcelHead("samdesc", "描述"));
            headList.add(new ExcelHead("createtime", "创建日期"));
            headList.add(new ExcelHead("samno", "No"));

            // 组织dataList的数据，20000条
            int len = 20000;
            List<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> hashMap = null;
            for (int i = 0; i < len; i++)
            {
                hashMap = new HashMap<String, String>();
                hashMap.put("samid", String.valueOf(i + 1) + " 编号");
                hashMap.put("samname", String.valueOf(i + 1) + " 名称");
                // hashMap.put("samdesc", String.valueOf(i + 1) + " 描述信息");
                // hashMap.put("samdesc", null);
                hashMap.put("samdesc", "000" + String.valueOf(i + 1));
                hashMap.put("createtime", "2010-6-17");
                hashMap.put("samno", String.valueOf(i + 1));

                dataList.add(hashMap);
            }

            // 调用示例代码段
            ExportToExcel exportToExcel = new ExportToExcel();

            // 基于ZXJOS-F的RIA框架取到的本地应用路径， 使用时根据实际情况修改
            String localPath = RIAContext.getCurrentInstance().getRealPath("/");
            // 相对路径
            String relatePath = "/upload/testExport/";
            // excel文件名称、sheet名称，zip包名
            String xlsName = "Excel导出示例";
            // 最大行数
            int maxRowNum = 3000;

            returnStr = exportToExcel
                    .exportExcelZipByMap(dataList, headList, maxRowNum, xlsName, localPath, relatePath);
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 示例方法，数据分Excel文件导出并打包成zip文件，POJO对象列表方式
     * 
     * @return
     * @throws Exception
     */
    public String exportExcelZipByObject() throws Exception
    {
        String returnStr = null;

        try
        {
            // 约定导出excel的列数，4列如下：
            // 第一列：excel显示名称--编号，数据字段名称--samid
            // 第二列：excel显示名称--名称，数据字段名称--samname
            // 第三列：excel显示名称--描述，数据字段名称--samdesc
            // 第四列：excel显示名称--创建日期，数据字段名称--createtime
            // 根据上面约定，则表头headList如下：
            List<ExcelHead> headList = new ArrayList<ExcelHead>();
            headList.add(new ExcelHead("samid", "编号"));
            headList.add(new ExcelHead("samname", "名称"));
            headList.add(new ExcelHead("samdesc", "描述"));
            headList.add(new ExcelHead("createtime", "创建日期"));
            headList.add(new ExcelHead("samno", "No"));

            // 组织dataList的数据，20000条
            int len = 20000;
            List<Object> dataList = new ArrayList<Object>();
            SampleObject sampleObject = null;
            Object obj = null;
            for (int i = 0; i < len; i++)
            {
                sampleObject = new SampleObject();
                sampleObject.setSamid(String.valueOf(i + 1) + " 编号");
                sampleObject.setSamname(String.valueOf(i + 1) + " 名称");
                // sampleObject.setSamdesc(String.valueOf(i + 1) + " 描述信息");
                // sampleObject.setSamdesc(null);
                sampleObject.setSamdesc("000" + String.valueOf(i + 1));
                sampleObject.setCreatetime("2010-7-17");
                sampleObject.setSamno(new Integer(i + 1));

                obj = sampleObject;
                dataList.add(obj);
            }

            // 调用示例代码段
            ExportToExcel exportToExcel = new ExportToExcel();

            // 基于ZXJOS-F的RIA框架取到的本地应用路径， 使用时根据实际情况修改
            String localPath = RIAContext.getCurrentInstance().getRealPath("/");
            // 相对路径
            String relatePath = "/upload/testExport/";
            // excel文件名称、sheet名称，zip包名
            String xlsName = "Excel导出示例";
            // 对象类型名称
            String className = "com.zte.ismp.common.SampleObject";
            // 最大行数
            int maxRowNum = 3000;

            returnStr = exportToExcel.exportExcelZipByObject(dataList, className, headList, maxRowNum, xlsName,
                    localPath, relatePath);
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 示例方法，数据分Excel文件导出至指定文件目录，Map对象列表方式
     * 
     * @return
     * @throws Exception
     */
    public String exportExcelByMap() throws Exception
    {
        String returnStr = null;

        try
        {
            // 约定导出excel的列数，4列如下：
            // 第一列：excel显示名称--编号，数据字段名称--samid
            // 第二列：excel显示名称--名称，数据字段名称--samname
            // 第三列：excel显示名称--描述，数据字段名称--samdesc
            // 第四列：excel显示名称--创建日期，数据字段名称--createtime
            // 根据上面约定，则表头headList如下：
            List<ExcelHead> headList = new ArrayList<ExcelHead>();
            headList.add(new ExcelHead("samid", "编号"));
            headList.add(new ExcelHead("samname", "名称"));
            headList.add(new ExcelHead("samdesc", "描述"));
            headList.add(new ExcelHead("createtime", "创建日期"));
            headList.add(new ExcelHead("samno", "No"));

            // 需要导出记录总数
            int totalCount = 50000;
            // 分批获取数据，每批获取记录数
            int perCount = 5000;
            // 每个excel导出最大行数
            int maxRowNum = 2500;

            // 计算获取数据次数
            int cycleTimes = totalCount / perCount;
            if (totalCount % perCount != 0)
            {
                cycleTimes += 1;
            }

            // 导出excel工具类实例
            ExportToExcel exportToExcel = new ExportToExcel();

            // 导出数据列表对象
            List<HashMap<String, String>> dataList = null;
            HashMap<String, String> hashMap = null;

            // excel文件名称、sheet名称，zip包名
            String xlsName = "Excel导出示例";
            // 循环导出文件的开始序号
            int startNo = 1;
            // 结束序号
            int endNo = 0;

            // 当前时间
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名
            // 基于ZXJOS-F的RIA框架取到的本地应用路径， 使用时根据实际情况修改
            String localPath = RIAContext.getCurrentInstance().getRealPath("/");
            // 相对路径
            String relatePath = "/upload/testExport/" + currTime + "/";
            // 导出Excel文件存储路径
            String xlsPath = localPath + relatePath;
            xlsPath = xlsPath.replace("//", "/");

            // 循环分批次获取数据，并分批导出数据至excel文件到指定目录下
            for (int i = 0; i < cycleTimes; i++)
            {
                // 此处模拟每批次获取perCount条数据
                dataList = new ArrayList<HashMap<String, String>>();
                for (int j = 0; j < perCount; j++)
                {
                    hashMap = new HashMap<String, String>();
                    hashMap.put("samid", String.valueOf(perCount * i + j + 1) + " 编号");
                    hashMap.put("samname", String.valueOf(perCount * i + j + 1) + " 名称");
                    // hashMap.put("samdesc", String.valueOf(perCount*i + j + 1) + " 描述信息");
                    // hashMap.put("samdesc", null);
                    hashMap.put("samdesc", "000" + String.valueOf(perCount * i + j + 1));
                    hashMap.put("createtime", "2010-7-17");
                    hashMap.put("samno", String.valueOf(perCount * i + j + 1));

                    dataList.add(hashMap);
                }

                // 数据分Excel文件导出至指定文件目录
                endNo = exportToExcel.exportExcelByMap(dataList, headList, maxRowNum, xlsName, startNo, xlsPath);

                startNo = endNo;
            }

            // excel文件打包
            String zipname = xlsName + currTime + ".zip";
            zipFile(xlsPath, zipname);
            returnStr = relatePath + "/" + zipname;
            returnStr.replace("//", "/");
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 示例方法，数据分Excel文件导出至指定文件目录，POJO对象列表方式
     * 
     * @return
     * @throws Exception
     */
    public String exportExcelByOjbect() throws Exception
    {
        String returnStr = null;

        try
        {
            // 约定导出excel的列数，4列如下：
            // 第一列：excel显示名称--编号，数据字段名称--samid
            // 第二列：excel显示名称--名称，数据字段名称--samname
            // 第三列：excel显示名称--描述，数据字段名称--samdesc
            // 第四列：excel显示名称--创建日期，数据字段名称--createtime
            // 根据上面约定，则表头headList如下：
            List<ExcelHead> headList = new ArrayList<ExcelHead>();
            headList.add(new ExcelHead("samid", "编号"));
            headList.add(new ExcelHead("samname", "名称"));
            headList.add(new ExcelHead("samdesc", "描述"));
            headList.add(new ExcelHead("createtime", "创建日期"));
            headList.add(new ExcelHead("samno", "No"));

            // 需要导出记录总数
            int totalCount = 50000;
            // 分批获取数据，每批获取记录数
            int perCount = 5000;
            // 每个excel导出最大行数
            int maxRowNum = 2500;

            // 计算获取数据次数
            int cycleTimes = totalCount / perCount;
            if (totalCount % perCount != 0)
            {
                cycleTimes += 1;
            }

            // 导出excel工具类实例
            ExportToExcel exportToExcel = new ExportToExcel();

            // 对象类型名称
            String className = "com.zte.ismp.common.SampleObject";

            // 导出数据列表对象
            List<Object> dataList = null;
            SampleObject sampleObject = null;
            Object obj = null;

            // excel文件名称、sheet名称，zip包名
            String xlsName = "Excel导出示例";
            // 循环导出文件的开始序号
            int startNo = 1;
            // 结束序号
            int endNo = 0;

            // 当前时间
            Date currDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
            String currTime = format.format(currDate); // 精确到毫秒，用于excel文件命名
            // 基于ZXJOS-F的RIA框架取到的本地应用路径， 使用时根据实际情况修改
            String localPath = RIAContext.getCurrentInstance().getRealPath("/");
            // 相对路径
            String relatePath = "/upload/testExport/" + currTime + "/";
            // 导出Excel文件存储路径
            String xlsPath = localPath + relatePath;
            xlsPath = xlsPath.replace("//", "/");

            // 循环分批次获取数据，并分批导出数据至excel文件到指定目录下
            for (int i = 0; i < cycleTimes; i++)
            {
                // 此处模拟每批次获取perCount条数据
                dataList = new ArrayList<Object>();
                for (int j = 0; j < perCount; j++)
                {
                    sampleObject = new SampleObject();
                    sampleObject.setSamid(String.valueOf(perCount * i + j + 1) + " 编号");
                    sampleObject.setSamname(String.valueOf(perCount * i + j + 1) + " 名称");
                    // sampleObject.setSamdesc(String.valueOf(perCount*i + j + 1) + " 描述信息");
                    // sampleObject.setSamdesc(null);
                    sampleObject.setSamdesc("000" + String.valueOf(perCount * i + j + 1));
                    sampleObject.setCreatetime("2010-7-17");
                    sampleObject.setSamno(new Integer(perCount * i + j + 1));

                    obj = sampleObject;
                    dataList.add(obj);
                }

                // 数据分Excel文件导出至指定文件目录
                endNo = exportToExcel.exportExcelByOjbect(dataList, className, headList, maxRowNum, xlsName, startNo,
                        xlsPath);

                startNo = endNo;
            }

            // excel文件打包
            String zipname = xlsName + currTime + ".zip";
            zipFile(xlsPath, zipname);
            returnStr = relatePath + "/" + zipname;
            returnStr.replace("//", "/");
        }
        catch (Exception e)
        {
            throw e;
        }

        return returnStr;
    }

    /**
     * 打包文件
     * 
     * @throws IOException
     */
    private void zipFile(String filePath, String zipName) throws IOException
    {

        File file = new File(filePath);
        if (!file.isDirectory())
        {
            throw new IllegalArgumentException("Not a directory:  " + filePath);
        }

        String[] entries = file.list();
        byte[] buffer = new byte[4096]; // Create a buffer for copying
        int bytesRead;

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(filePath + "/" + zipName));

        for (int i = 0; i < entries.length; i++)
        {
            File f = new File(file, entries[i]);

            FileInputStream in = new FileInputStream(f);
            org.apache.tools.zip.ZipEntry entry = new org.apache.tools.zip.ZipEntry(f.getName());
            out.putNextEntry(entry);
            while ((bytesRead = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytesRead);
            }
            in.close();
        }
        out.close();
    }

}

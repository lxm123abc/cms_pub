/**
 * QueryMediaInfoResponseAVFormat.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoResponseAVFormat implements java.io.Serializable
{
    /* 视音频文件格式类型，参见枚举说明文档 */
    private long streamMediaType;

    /* 视音频文件格式子类型，参见枚举说明文档 */
    private long streamMediaSubType;

    /* 是否包含视频 */
    private long isIncludeVideo;

    /* 是否包含音频 */
    private long isIncludeAudio;

    /* 视频格式信息 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatVideoInfo videoInfo;

    /* 音频格式信息 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatAudioInfo audioInfo;

    public QueryMediaInfoResponseAVFormat()
    {
    }

    public QueryMediaInfoResponseAVFormat(long streamMediaType, long streamMediaSubType, long isIncludeVideo,
            long isIncludeAudio, com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatVideoInfo videoInfo,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatAudioInfo audioInfo)
    {
        this.streamMediaType = streamMediaType;
        this.streamMediaSubType = streamMediaSubType;
        this.isIncludeVideo = isIncludeVideo;
        this.isIncludeAudio = isIncludeAudio;
        this.videoInfo = videoInfo;
        this.audioInfo = audioInfo;
    }

    /**
     * Gets the streamMediaType value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return streamMediaType * 视音频文件格式类型，参见枚举说明文档
     */
    public long getStreamMediaType()
    {
        return streamMediaType;
    }

    /**
     * Sets the streamMediaType value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param streamMediaType * 视音频文件格式类型，参见枚举说明文档
     */
    public void setStreamMediaType(long streamMediaType)
    {
        this.streamMediaType = streamMediaType;
    }

    /**
     * Gets the streamMediaSubType value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return streamMediaSubType * 视音频文件格式子类型，参见枚举说明文档
     */
    public long getStreamMediaSubType()
    {
        return streamMediaSubType;
    }

    /**
     * Sets the streamMediaSubType value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param streamMediaSubType * 视音频文件格式子类型，参见枚举说明文档
     */
    public void setStreamMediaSubType(long streamMediaSubType)
    {
        this.streamMediaSubType = streamMediaSubType;
    }

    /**
     * Gets the isIncludeVideo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return isIncludeVideo * 是否包含视频
     */
    public long getIsIncludeVideo()
    {
        return isIncludeVideo;
    }

    /**
     * Sets the isIncludeVideo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param isIncludeVideo * 是否包含视频
     */
    public void setIsIncludeVideo(long isIncludeVideo)
    {
        this.isIncludeVideo = isIncludeVideo;
    }

    /**
     * Gets the isIncludeAudio value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return isIncludeAudio * 是否包含音频
     */
    public long getIsIncludeAudio()
    {
        return isIncludeAudio;
    }

    /**
     * Sets the isIncludeAudio value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param isIncludeAudio * 是否包含音频
     */
    public void setIsIncludeAudio(long isIncludeAudio)
    {
        this.isIncludeAudio = isIncludeAudio;
    }

    /**
     * Gets the videoInfo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return videoInfo * 视频格式信息
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatVideoInfo getVideoInfo()
    {
        return videoInfo;
    }

    /**
     * Sets the videoInfo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param videoInfo * 视频格式信息
     */
    public void setVideoInfo(com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatVideoInfo videoInfo)
    {
        this.videoInfo = videoInfo;
    }

    /**
     * Gets the audioInfo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @return audioInfo * 音频格式信息
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatAudioInfo getAudioInfo()
    {
        return audioInfo;
    }

    /**
     * Sets the audioInfo value for this QueryMediaInfoResponseAVFormat.
     * 
     * @param audioInfo * 音频格式信息
     */
    public void setAudioInfo(com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatAudioInfo audioInfo)
    {
        this.audioInfo = audioInfo;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj)
    {
        if (!(obj instanceof QueryMediaInfoResponseAVFormat))
            return false;
        QueryMediaInfoResponseAVFormat other = (QueryMediaInfoResponseAVFormat) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null)
        {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && this.streamMediaType == other.getStreamMediaType()
                && this.streamMediaSubType == other.getStreamMediaSubType()
                && this.isIncludeVideo == other.getIsIncludeVideo()
                && this.isIncludeAudio == other.getIsIncludeAudio()
                && ((this.videoInfo == null && other.getVideoInfo() == null) || (this.videoInfo != null && this.videoInfo
                        .equals(other.getVideoInfo())))
                && ((this.audioInfo == null && other.getAudioInfo() == null) || (this.audioInfo != null && this.audioInfo
                        .equals(other.getAudioInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode()
    {
        if (__hashCodeCalc)
        {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getStreamMediaType();
        _hashCode += getStreamMediaSubType();
        _hashCode += getIsIncludeVideo();
        _hashCode += getIsIncludeAudio();
        if (getVideoInfo() != null)
        {
            _hashCode += getVideoInfo().hashCode();
        }
        if (getAudioInfo() != null)
        {
            _hashCode += getAudioInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            QueryMediaInfoResponseAVFormat.class, true);

    static
    {
        typeDesc.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>QueryMediaInfoResponse>AVFormat"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streamMediaType");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "StreamMediaType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streamMediaSubType");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "StreamMediaSubType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isIncludeVideo");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "IsIncludeVideo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isIncludeAudio");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "IsIncludeAudio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "VideoInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS",
                ">>>QueryMediaInfoResponse>AVFormat>VideoInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS",
                ">>>QueryMediaInfoResponse>AVFormat>AudioInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}

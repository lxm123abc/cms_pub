package com.zte.cms.content.program.ls;

import java.util.List;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramLS
{

    /**
     * 新增CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public String insertCmsProgram(CmsProgram cmsProgram) throws Exception;

    /**
     * 查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @return CmsProgram对象
     * @throws Exception
     */
    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws Exception;

    /**
     * 根据条件查询内容
     * 
     * @param cmsProgram
     * @return
     * @throws Exception
     */
    public CmsProgram getCmsProgramByCond(CmsProgram cmsProgram) throws Exception;

    /**
     * 修改 CmsProgram对象
     * 
     * @param cmsProgram
     * @return 操作结果
     * @throws Exception
     */
    public String cntModApply(CmsProgram cmsProgram) throws Exception;

    /**
     * 根据内容编码删除内容
     * 
     * @param programid
     * @return
     * @throws Exception
     */
    public String deleteCmsProgram(String programIndex) throws Exception;

    /**
     * 批量删除内容
     * 
     * @param list
     * @return 操作结果
     * @throws Exception
     */
    public String batchDeleteCmsProgram(List<CmsProgram> list) throws Exception;

    /**
     * 根据条件分页查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws Exception;

    /**
     * 批量提审内容
     * 
     * @param programidList 内容编码列表
     * @return
     * @throws Exception
     */
    public String batchApply(List<String> programidList) throws Exception;

    /**
     * 审核内容方法
     * 
     * @param wkfwParam 工作流参数：内容ID、工单任务号、工单流程号、工单节点名称、审核意见、审核结果
     * @return
     * @throws Exception
     */
    public String cntAudit(String[] wkfwParam) throws Exception;

    public String cntModAudit(String[] wkfwParam) throws Exception;

    /**
     * 批量修改内容
     * 
     * @param programindexs
     * @param program
     * @return
     * @throws Exception
     */
    public String batchUpdateProgram(String programindexs, CmsProgram program) throws Exception;
    
    /**
     * 送入回收站
     * @param programindex
     * @return
     * @throws Exception
     */
    public String recycleProgram(Long programindex ) throws Exception;
    /**
     * 根据连续index获取内容对象
     * 
     * @param programindex 內容Index
     * @return 内容对象
     */
    public CmsProgram getCmsProgramByIndex(Long programindex) throws DomainServiceException;
    
    public String bindBatchProgramTargetsync(List<Targetsystem> platformList,List<CmsProgram> cmsProgramList);
    
    public String bindProgramTargetsync(List<Targetsystem> platformList,CmsProgram cmsProgram);
    
    //删除点播内容与网元关联关系
    public String deleteProgramBindTarget(Long programindex,Long targetindex) throws Exception;
}

package com.zte.cms.egptemplt.dao;

import java.util.List;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class EpgtempltDAO extends DynamicObjectBaseDao implements IEpgtempltDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertEpgtemplt(Epgtemplt epgtemplt) throws DAOException
    {
        log.debug("insert epgtemplt starting...");
        super.insert("insertEpgtemplt", epgtemplt);
        log.debug("insert epgtemplt end");
    }

    public void updateEpgtemplt(Epgtemplt epgtemplt) throws DAOException
    {
        log.debug("update epgtemplt by pk starting...");
        super.update("updateEpgtemplt", epgtemplt);
        log.debug("update epgtemplt by pk end");
    }

    public void deleteEpgtemplt(Epgtemplt epgtemplt) throws DAOException
    {
        log.debug("delete epgtemplt by pk starting...");
        super.delete("deleteEpgtemplt", epgtemplt);
        log.debug("delete epgtemplt by pk end");
    }

    public Epgtemplt getEpgtemplt(Epgtemplt epgtemplt) throws DAOException
    {
        log.debug("query epgtemplt starting...");
        Epgtemplt resultObj = (Epgtemplt) super.queryForObject("getEpgtemplt", epgtemplt);
        log.debug("query epgtemplt end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Epgtemplt> getEpgtempltByCond(Epgtemplt epgtemplt) throws DAOException
    {
        log.debug("query epgtemplt by condition starting...");
        List<Epgtemplt> rList = (List<Epgtemplt>) super.queryForList("queryEpgtempltListByCond", epgtemplt);
        log.debug("query epgtemplt by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize) throws DAOException
    {
        log.debug("page query epgtemplt by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryEpgtempltListCntByCond", epgtemplt)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Epgtemplt> rsList = (List<Epgtemplt>) super.pageQuery("queryEpgtempltListByCond", epgtemplt, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query epgtemplt by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryEpgtempltListByCond", "queryEpgtempltListCntByCond", epgtemplt, start,
                pageSize, puEntity);
    }

}
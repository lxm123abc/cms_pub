package com.zte.cms.ftptask.service;

import java.util.List;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.model.CmsFtptaskAndHis;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsFtptaskDS
{
    /**
     * 新增CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 新增CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException;

    /**
     * 更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 批量更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException;

    /**
     * 根据条件更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException;

    /**
     * 删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 批量删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException;

    /**
     * 根据条件删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DomainServiceException;

    /**
     * 查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @return CmsFtptask对象
     * @throws DomainServiceException ds异常
     */
    public CmsFtptask getCmsFtptask(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 根据条件查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @return 满足条件的CmsFtptask对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    public void removeCmsFtptaskHis(CmsFtptask cmsFtptask) throws DomainServiceException;

    public CmsFtptask getCmsFtptaskHis(CmsFtptask cmsFtptask) throws DomainServiceException;

    public TableDataInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskAndHis, int start, int pageSize)
            throws DomainServiceException;
}
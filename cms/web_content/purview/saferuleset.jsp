<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import = "java.util.*"%>
<%@ include file="JavaFun.jsp" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    String flag = request.getParameter("flag");
    if(flag !=null){
	    String levelindex = request.getParameter("levelindex");
	    if("0".equals(flag)){ //from self page
	      pwdrule.delSafeRule(levelindex);
	    }
    }
%>

<html>
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script language = "JavaScript">

function doLoad() {
  document.theForm.action = "saferuleset.jsp";
  document.theForm.submit();
}
function doAdd() {
  document.theForm.action = "saferuleadd.jsp";
  document.theForm.levelindex.value="";  
  document.theForm.submit();
}
function doEdit(levelindex) {
  document.theForm.action = "saferuleadd.jsp";
  document.theForm.levelindex.value= levelindex;
  document.theForm.submit();
}
function doDelete(levelindex) {
  document.theForm.action = "saferuleset.jsp";
  document.theForm.levelindex.value=levelindex;
  document.theForm.flag.value=0;
  document.theForm.submit();  
}
        </script>
    </head>

    <body bgcolor = "#ffffff">
        <table width = "100%" height = "98%" border = "0" align = "center" cellpadding = "0" cellspacing = "0">

            <tr>
                <td valign = "top">


                    <table width = "95%" border = "0" cellpadding = "1" cellspacing = "1" bgcolor = "#CCCCCC" class="table-style2" height=hei>
                        <tr bgcolor = "#BDE6F7">
                            <td width="20%" height = "20">
                                <fmt:message key="saferuleset.securitylevel"/></td>

                            <td width="50%"height = "20">
                                <fmt:message key="saferuleset.description"/></td>

							              <td width="15%"height = "10">
                                <fmt:message key="saferuleset.edit"/></td>

							              <td width="15%" height = "10">
                                <fmt:message key="saferuleset.delete"/></td>
                        </tr>

                        <%
                          ArrayList list = pwdrule.getSafeRule();
                          
													if (list.size()>0) {
														for (int i=0; i<list.size(); i++) {
														      HashMap map = (HashMap)list.get(i);

						%>
                            			<tr bgcolor = "#FFFFFF">
                                			<td><%= map.get("levelindex") %>&nbsp;</td>
                                			<td><%= map.get("description") %>&nbsp;</td>
                                			<td>
                                     		<input type = "button"  name="btn" value = "<fmt:message key="saferuleset.edit"/>" onclick="return doEdit('<%= map.get("levelindex") %>');"></td>
 											                <td>
                                     		<input type = "button"  name="btn" value = "<fmt:message key="saferuleset.delete"/>" onclick="return doDelete('<%= map.get("levelindex") %>');"></td>
                            			</tr>
                        <%
								          }
							         }
                        %>
                    </table>
                    
                    <hr> 
                    
                    <form name="theForm" action = "" method = "post">
                        <input type="hidden" name="levelindex" value="">
                        <input type="hidden" name="flag" value="">
                        <table width = "95%" border = "0" cellpadding = "1" cellspacing = "1">
													<tr>
														<td colspan=2 align="center">
															<input type="button"  name="btn" value="<fmt:message key="saferuleset.refresh"/>" onclick="return doLoad();">
						                  <input type="button"  name="btn" value="<fmt:message key="saferuleset.add"/>" onclick="return doAdd();">
														</td>
						              </tr>
                        </table>
		                </form>
                </td>
            </tr>
        </table>
    </body>
</html>
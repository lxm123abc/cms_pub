package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.CastPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICastPictureMapDAO
{
    /**
     * 新增CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DAOException dao异常
     */
    public void insertCastPictureMap(CastPictureMap castPictureMap) throws DAOException;

    /**
     * 根据主键更新CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DAOException dao异常
     */
    public void updateCastPictureMap(CastPictureMap castPictureMap) throws DAOException;

    /**
     * 根据主键删除CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteCastPictureMap(CastPictureMap castPictureMap) throws DAOException;

    /**
     * 根据主键查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @return 满足条件的CastPictureMap对象
     * @throws DAOException dao异常
     */
    public CastPictureMap getCastPictureMap(CastPictureMap castPictureMap) throws DAOException;

    /**
     * 根据条件查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @return 满足条件的CastPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<CastPictureMap> getCastPictureMapByCond(CastPictureMap castPictureMap) throws DAOException;

    /**
     * 根据条件分页查询CastPictureMap对象，作为查询条件的参数
     * 
     * @param castPictureMap CastPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CastPictureMap对象，作为查询条件的参数
     * 
     * @param castPictureMap CastPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    
    public PageInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException;
    
    public PageInfo pageInfoQueryCastPicTargetMap(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException;

}
package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgram;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsProgramDAO extends DynamicObjectBaseDao implements ICmsProgramDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsProgram(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("insert cmsProgram starting...");
        super.insert("insertCmsProgram", cmsProgram);
        log.debug("insert cmsProgram end");
    }

    public void updateCmsProgram(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("update cmsProgram by pk starting...");
        super.update("updateCmsProgram", cmsProgram);
        log.debug("update cmsProgram by pk end");
    }

    public void updateCmsProgramByCond(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("update cmsProgram by conditions starting...");
        super.update("updateCmsProgramByCond", cmsProgram);
        log.debug("update cmsProgram by conditions end");
    }

    public void deleteCmsProgram(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("delete cmsProgram by pk starting...");
        super.delete("deleteCmsProgram", cmsProgram);
        log.debug("delete cmsProgram by pk end");
    }

    public void deleteCmsProgramByCond(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("delete cmsProgram by conditions starting...");
        super.delete("deleteCmsProgramByCond", cmsProgram);
        log.debug("update cmsProgram by conditions end");
    }

    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("query cmsProgram starting...");
        CmsProgram resultObj = (CmsProgram) super.queryForObject("getCmsProgram", cmsProgram);
        log.debug("query cmsProgram end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgram> getCmsProgramByCond(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("query cmsProgram by condition starting...");
        List<CmsProgram> rList = (List<CmsProgram>) super.queryForList("queryCmsProgramListByCond", cmsProgram);
        log.debug("query cmsProgram by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgram by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramListCntByCond", cmsProgram)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgram> rsList = (List<CmsProgram>) super.pageQuery("queryCmsProgramListByCond", cmsProgram,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgram by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsProgramListByCond", "queryCmsProgramListCntByCond", cmsProgram, start,
                pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public int getCmsProgramCount(CmsProgram cmsProgram) throws DAOException
    {
        log.debug("query cmsProgram count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramListCntByCond", cmsProgram)).intValue();
        log.debug("query cmsProgram count by condition end");
        return totalCnt;
    }

    public CmsProgram getCmsProgramById(CmsProgram cmsProgram) throws DAOException
    {  
        log.debug("query getCmsProgramById starting...");
       CmsProgram resultObj = (CmsProgram) super.queryForObject("getCmsProgramById", cmsProgram);
        log.debug("query getCmsProgramById end");
        return resultObj;
    }
    
  //super查询时使用
    public PageInfo pageInfoQueryForPage(CmsProgram cmsProgram, int start, int pageSize) throws DAOException
    {
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramPageListCnt", cmsProgram)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgram> rsList = (List<CmsProgram>) super.pageQuery("queryCmsProgramPageList", cmsProgram,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgram by condition end");
        return pageInfo;
    }
    //其他操作员查询时使用
    public PageInfo pageInfoQueryForPageForOper(CmsProgram cmsProgram, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryForPage starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramPageListCntForOper", cmsProgram)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgram> rsList = (List<CmsProgram>) super.pageQuery("queryCmsProgramPageListForOper", cmsProgram, start,fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryForPage end");
        return pageInfo;
    }
}
package com.zte.cms.channel.wkfw.service;

import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IScheduleWkfwProcessWithMeDS
{

    public abstract int queryScheduleWkfwProcessWithMeListCntByCond(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start,
            int pageSize) throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

}
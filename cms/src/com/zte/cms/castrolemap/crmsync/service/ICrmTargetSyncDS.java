package com.zte.cms.castrolemap.crmsync.service;

import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICrmTargetSyncDS
{

    /**
     * 根据条件分页查询CrmTargetSync对象 
     *
     * @param crmTargetSync CrmTargetSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CrmTargetSync crmTargetSync, int start, int pageSize)throws DomainServiceException;

}

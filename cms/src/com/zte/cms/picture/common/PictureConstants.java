package com.zte.cms.picture.common;

public class PictureConstants
{
    public static final String WINDOWS_SEPERATOR = "\\"; // WINDOWS分隔符
    public static final String LINUX_SEPERATOR = "/"; // Linux分隔符
    // 共享访问点
    public static final String HTTP_PREWIEW_PATH_CFG = "cms.httppreview.path"; // 公网http方式文件预览路径
    public static final String CMS_PREWIEW_PATH_CFG = "cms.preview.path"; // WINDOWS共享预览路径
    public static final String CNTSYN_FTPADDRESS_PREFIX_CFG = "cms.cntsyn.ftpaddress"; // 同步内容实体文件FTP地址
    // 同步xml在临时区存放的目录名
    public static final String UCDN_CNT_SYNCXML_DIR = "xmlsync";
    public static final String PIC_OBJECT_NAME = "Picture";

    // 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 10-picture 11-Cast
    public static final int SERVIEC = 1;
    public static final int CATAGORY = 2;
    public static final int PROGRAM = 3;
    public static final int SERIES = 4;
    public static final int CHANNEL = 5;
    public static final int PICTURE = 10;
    public static final int CAST = 11;

    // 同步任务类型
    public static final int SYNC_TASK_ACTION_REGIST = 1;
    public static final int SYNC_TASK_ACTION_UPDATE = 2;
    public static final int SYNC_TASK_ACTION_DELETE = 3;

    // XML Action
    public static final String SYNC_XML_ACTION_REGORUPD = "REGIST or UPDATE";
    public static final String SYNC_XML_ACTION_DELETE = "DELETE";

    // 目标系统类型
    public static final int TARGET_SYS_TYPE_BMS = 1;
    public static final int TARGET_SYS_TYPE_EPG = 2;
    public static final int TARGET_SYS_TYPE_CDN = 3;

    // 海报状态
    public static final int PIC_STATUS_INITIAL = 100; // 初始
    public static final int PIC_STATUS_TOPUBLISH = 0; // 待发布
    public static final int PIC_STATUS_ADDPUBLISHING = 10; // 新增同步中
    public static final int PIC_STATUS_ADDPUBLISH_SUCCESS = 20; // 新增同步成功
    public static final int PIC_STATUS_ADDPUBLISH_FAIL = 30; // 新增同步失败
    public static final int PIC_STATUS_EDITPUBLISHING = 40; // 修改同步中
    public static final int PIC_STATUS_EDITPUBLISH_SUCCESS = 50; // 修改同步成功
    public static final int PIC_STATUS_EDITPUBLISH_FAIL = 60; // 修改同步失败
    public static final int PIC_STATUS_DELPUBLISHING = 70; // 取消同步中
    public static final int PIC_STATUS_DELPUBLISH_SUCCESS = 80; // 取消同步成功
    public static final int PIC_STATUS_DELPUBLISH_FAIL = 90; // 取消同步失败

    // 库区定义
    public static final String STORAGEAREA_TEMP = "1"; // 临时区
    public static final String STORAGEAREA_ONLINE = "2"; // 在线区
    public static final Long MAX_PIC_FILESIZE = 10L * 1024L * 1024L; // HTTP上载海报文件大小的最大值

    // 错误码定义
    public static final String PIC_SUCCESS = "100000"; // 操作成功
    public static final String PIC_FAILED = "1000001"; // 操作失败
    public static final String PIC_ONLINEAREA_NOTFOUND = "100001"; // 获取在线区地址失败
    public static final String PIC_DEST_NOTFOUND = "100002"; // 找不到目标地址，请检查存储区绑定
    public static final String PIC_SOURCEFILE_NOTEXIST = "100003"; // 海报源文件不存在
    public static final String PIC_SIZE_TOOLARGE = "100004"; // 海报文件大小超过10M
    public static final String PIC_UPLOAD_FAIL = "100005"; // 海报文件上传失败
    public static final String PIC_OBJECT_NOTEXIST = "100006"; // 对象已经不存在
    public static final String PIC_STATUS_NOTRIGHT = "100007"; // 海报状态不正确
    public static final String PIC_NOCONFIG_WINDOWSHARE = "100008"; // 未配置系统参数WINDOWS共享访问点
    public static final String PIC_DATAERRO_MOREMAP1PIC = "100009"; // 数据异常,一个海报无关联或关联多个对象
    public static final String PIC_TEMPAREA_NOTFOUND = "1000010"; // 获取临时区地址失败
    public static final String PIC_MOUNTPOINT_NOTFOUND = "1000011"; // 获取挂载点失败
    public static final String PIC_CREATE_SYSNXML_FAILED = "1000012"; // 生成同步xml文件失败
    public static final String PIC_GET_TARGETSYS_FAILED = "1000013"; // 获取目标系统失败
    public static final String PIC_CREATE_SYSNTASK_FAILED = "1000014"; // 插入同步任务失败
    public static final String PIC_GETFTPADDR_PREFIX_FAILED = "1000015"; // 获取同步内容实体文件FTP地址失败

    public static final String PIC_GET_STORAGEAREA_ERROR = "1056020"; // 获取库区地址失败

    // 操作日志资源字符串定义
    public static final String MGTTYPE_PICTURE_MGT = "log.picture.mgt";
    public static final String OPERTYPE_PICTURE_ADD = "log.picture.add";
    public static final String OPERTYPE_PICTURE_ADD_INFO = "log.picture.add.info";

    public static final String OPERTYPE_PICTURE_DEL = "log.picture.del";
    public static final String OPERTYPE_PICTURE_DEL_INFO = "log.picture.del.info";

    public static final String OPERTYPE_PICTURE_PUB = "log.picture.pub";
    public static final String OPERTYPE_PICTURE_PUB_INFO = "log.picture.pub.info";

    public static final String OPERTYPE_PICTURE_CANCELPUB = "log.picture.cancelpub";
    public static final String OPERTYPE_PICTURE_CANCELPUB_INFO = "log.picture.cancelpub.info";
    
    //MapObjtype
    public static final int SERVIEC_PIC_MAP_OBJType = 25;
    public static final int CATEGORY_PIC_MAP_OBJType = 31;
    public static final int PROGRAM_PIC_MAP_OBJType = 35;
    public static final int SERIES_PIC_MAP_OBJType = 38;
    public static final int CHANNEL_PIC_MAP_OBJType = 39;
    public static final int CAST_PIC_MAP_OBJType = 41;
    
    //已发布的状态
    public static final int STATUS_ADDPUBLISH_SUCCESS = 20; // 新增同步成功
    public static final int STATUS_EDITPUBLISH_SUCCESS = 50; // 修改同步成功
    public static final int STATUS_EDITPUBLISH_FAIL = 60; // 修改同步失败
    public static final int STATUS_DELPUBLISH_FAIL = 90; // 取消同步失败
    
    //对象状态不正确
    public static final String SERVICE_STATUS_NOTRIGHT = "1000071"; //海报所属服务状态不正确
    public static final String CATEGORY_STATUS_NOTRIGHT = "1000072"; //海报所属栏目状态不正确
    public static final String PROGRAM_STATUS_NOTRIGHT = "1000073"; //海报所属点播内容状态不正确
    public static final String SERIES_STATUS_NOTRIGHT = "1000074"; //海报所属连续剧状态不正确
    public static final String CHANNEL_STATUS_NOTRIGHT = "1000075"; //海报所属直播频道状态不正确
    public static final String CAST_STATUS_NOTRIGHT = "1000076"; //海报所属人物状态不正确
    
    //对象状态正确
    public static final String STATUS_RIGHT = "0"; //海报属主状态正确
}

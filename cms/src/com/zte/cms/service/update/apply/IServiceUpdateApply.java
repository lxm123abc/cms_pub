package com.zte.cms.service.update.apply;

import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceUpdateApply
{

    /**
     * 修改工作流申请
     * 
     * @param serviceindex
     * @return
     * @throws DomainServiceException
     */
    public abstract String apply(Service service) throws DomainServiceException;

    public abstract String apply(Service service, String priority) throws DomainServiceException;

    /**
     * 修改工作流审核
     * 
     * @param service
     * @param taskId
     * @param handleMsg
     * @param handleEvent
     * @param workflowStatusCode
     * @return
     * @throws DomainServiceException
     */
    public abstract String audit(ServiceBuf servicebuf, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException;

}
package com.zte.cms.egptemplt.service;

import java.util.List;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IEpgtempltDS
{
    /**
     * 新增Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public void insertEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 更新Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public void updateEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 批量更新Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public void updateEpgtempltList(List<Epgtemplt> epgtempltList) throws DomainServiceException;

    /**
     * 删除Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public void removeEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 批量删除Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public void removeEpgtempltList(List<Epgtemplt> epgtempltList) throws DomainServiceException;

    /**
     * 查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @return Epgtemplt对象
     * @throws DomainServiceException ds异常
     */
    public Epgtemplt getEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 根据条件查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @return 满足条件的Epgtemplt对象集
     * @throws DomainServiceException ds异常
     */
    public List<Epgtemplt> getEpgtempltByCond(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 根据条件分页查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

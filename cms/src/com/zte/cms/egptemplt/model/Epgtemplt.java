package com.zte.cms.egptemplt.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Epgtemplt extends DynamicBaseObject
{
    private java.lang.Long templtindex;
    private java.lang.String templtid;
    private java.lang.String templtver;
    private java.lang.Integer templtdftflag;
    private java.lang.String addresspath;
    private java.lang.String begintime;
    private java.lang.String expiredtime;
    private java.lang.Integer status;
    private java.lang.Integer source;
    private java.lang.String description;
    private java.lang.String epggroup;
    private java.lang.String sendto20addr;
    private java.lang.String sendto30addr;
    private java.lang.Integer sendto20flag;
    private java.lang.Integer standard20;
    private java.lang.Integer sendto30flag;
    private java.lang.Integer standard30;
    
    public java.lang.Integer getSource() {
		return source;
	}
	public void setSource(java.lang.Integer source) {
		this.source = source;
	}

    public java.lang.Long getTempltindex()
    {
        return templtindex;
    }

    public void setTempltindex(java.lang.Long templtindex)
    {
        this.templtindex = templtindex;
    }

    public java.lang.String getTempltid()
    {
        return templtid;
    }

    public void setTempltid(java.lang.String templtid)
    {
        this.templtid = templtid;
    }

    public java.lang.String getTempltver()
    {
        return templtver;
    }

    public void setTempltver(java.lang.String templtver)
    {
        this.templtver = templtver;
    }

    public java.lang.Integer getTempltdftflag()
    {
        return templtdftflag;
    }

    public void setTempltdftflag(java.lang.Integer templtdftflag)
    {
        this.templtdftflag = templtdftflag;
    }

    public java.lang.String getAddresspath()
    {
        return addresspath;
    }

    public void setAddresspath(java.lang.String addresspath)
    {
        this.addresspath = addresspath;
    }

    public java.lang.String getBegintime()
    {
        return begintime;
    }

    public void setBegintime(java.lang.String begintime)
    {
        this.begintime = begintime;
    }

    public java.lang.String getExpiredtime()
    {
        return expiredtime;
    }

    public void setExpiredtime(java.lang.String expiredtime)
    {
        this.expiredtime = expiredtime;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub
        this.addRelation("templtindex", "TEMPLTINDEX");
        this.addRelation("templtid", "TEMPLTID");
        this.addRelation("templtver", "TEMPLTVER");
        this.addRelation("templtdftflag", "TEMPLTDFTFLAG");
        this.addRelation("addresspath", "ADDRESSPATH");
        this.addRelation("begintime", "BEGINTIME");
        this.addRelation("expiredtime", "EXPIREDTIME");
        this.addRelation("status", "STATUS");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("source", "SOURCE");
        this.addRelation("epggroup", "EPGGROUP");
        this.addRelation("sendto20flag", "SENDTO20FLAG");
        this.addRelation("sendto20addr", "SENDTO20ADDR");
        this.addRelation("standard20", "STANDARD20");
        this.addRelation("sendto30flag", "SENDTO30FLAG");
        this.addRelation("sendto30addr", "SENDTO30ADDR");
        this.addRelation("standard30", "STANDARD30");
    }
	public java.lang.String getEpggroup() {
		return epggroup;
	}
	public void setEpggroup(java.lang.String epggroup) {
		this.epggroup = epggroup;
	}
	public java.lang.String getSendto20addr() {
		return sendto20addr;
	}
	public void setSendto20addr(java.lang.String sendto20addr) {
		this.sendto20addr = sendto20addr;
	}
	public java.lang.String getSendto30addr() {
		return sendto30addr;
	}
	public void setSendto30addr(java.lang.String sendto30addr) {
		this.sendto30addr = sendto30addr;
	}
	public java.lang.Integer getSendto20flag() {
		return sendto20flag;
	}
	public void setSendto20flag(java.lang.Integer sendto20flag) {
		this.sendto20flag = sendto20flag;
	}
	public java.lang.Integer getStandard20() {
		return standard20;
	}
	public void setStandard20(java.lang.Integer standard20) {
		this.standard20 = standard20;
	}
	public java.lang.Integer getSendto30flag() {
		return sendto30flag;
	}
	public void setSendto30flag(java.lang.Integer sendto30flag) {
		this.sendto30flag = sendto30flag;
	}
	public java.lang.Integer getStandard30() {
		return standard30;
	}
	public void setStandard30(java.lang.Integer standard30) {
		this.standard30 = standard30;
	}

}

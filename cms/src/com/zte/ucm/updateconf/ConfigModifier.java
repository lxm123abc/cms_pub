package com.zte.ucm.updateconf;

import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class ConfigModifier
{
    Logger log = LoggerFactory.getLogger(ConfigModifier.class);

    public void modifyXMLConfig(Config paramConfig) throws Exception
    {
        String str1 = "";
        File localFile = new File(paramConfig.getFile());
        Resultlog localResultlog = new Resultlog();
        if (!localFile.exists())
        {
            this.log.error("config file[" + paramConfig.getFile() + "] is not exist!!!");
            str1 = str1 + "1|Operation ERROR:config file[" + paramConfig.getFile() + "] is not exist!!!" + "\n";
            localResultlog.appendLog(str1);
            return;
        }
        try
        {
            SAXReader localSAXReader = new SAXReader();
            // 对dom4j的SAXReader进行设置，不去下载外部dtd文件来对xml进行验证
            localSAXReader.setValidation(false);
            localSAXReader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            Document localDocument = localSAXReader.read(localFile);
            String str2 = localDocument.getXMLEncoding();
            List localList = localDocument.selectNodes(paramConfig.getXpath());
            Object localObject2;
            Object localObject3;
            Object localObject4;
            if ((localList != null) && (localList.size() > 0))
            {
                String localObject1 = paramConfig.getType().trim();
                localObject2 = "";
                if (("0".equals(localObject1)) || ("".equals(localObject1)))
                {
                    localObject3 = (Element) localList.get(0);
                    localObject2 = ((Element) localObject3).getText();
                    ((Element) localObject3).setText(paramConfig.getValue());
                    this.log.info("Operation success:config file[" + paramConfig.getFile() + "] node["
                            + paramConfig.getXpath() + "] from value[" + (String) localObject2 + "] to value["
                            + paramConfig.getValue() + "]");
                }
                else if ("1".equals(localObject1))
                {
                    localObject3 = (Element) localList.get(0);
                    localObject4 = ((Element) localObject3).attribute(paramConfig.getAttrname());
                    if (localObject4 != null)
                    {
                        localObject2 = ((Attribute) localObject4).getValue();
                        ((Attribute) localObject4).setValue(paramConfig.getValue());
                        this.log.info("Operation success:config file[" + paramConfig.getFile() + "] node["
                                + paramConfig.getXpath() + "] attribute[" + paramConfig.getAttrname() + "] from value["
                                + (String) localObject2 + "] to value[" + paramConfig.getValue() + "]");
                    }
                    else
                    {
                        this.log.error("Operation ERROR:Cannot find the attribute name[" + paramConfig.getAttrname()
                                + "]");
                        str1 = str1 + "1|Operation ERROR:Cannot find the attribute name[" + paramConfig.getAttrname()
                                + "]" + "\n";
                        localResultlog.appendLog(str1);
                    }
                }

            }
            else
            {
                this.log.error("Operation ERROR:Cannot find the config, please check the xpath["
                        + paramConfig.getXpath() + "] of the item[" + paramConfig.getTag() + "] in config file["
                        + paramConfig.getFile() + "]");

                str1 = str1 + "1|Operation ERROR:Cannot find the config, please check the xpath["
                        + paramConfig.getXpath() + "] of the item[" + paramConfig.getTag() + "] in config file["
                        + paramConfig.getFile() + "]" + "\n";

                localResultlog.appendLog(str1);
            }

            Object localObject1 = null;
            try
            {
                localObject2 = new FileOutputStream(localFile);
                localObject3 = new OutputStreamWriter((OutputStream) localObject2, str2);

                localObject4 = OutputFormat.createPrettyPrint();
                ((OutputFormat) localObject4).setEncoding(str2);
                localObject1 = new XMLWriter((Writer) localObject3, (OutputFormat) localObject4);
                ((XMLWriter) localObject1).write(localDocument);
            }
            catch (IOException localIOException)
            {
                this.log.error("Operation ERROR:Save config file[" + paramConfig.getFile() + "] error!!!");
                str1 = str1 + "1|Operation ERROR:Save config file[" + paramConfig.getFile() + "] error!!!" + "\n";
                localResultlog.appendLog(str1);

                throw localIOException;
            }
            finally
            {
                if (localObject1 != null)
                    try
                    {
                        ((XMLWriter) localObject1).close();
                    }
                    catch (Exception localException2)
                    {
                    }
            }
        }
        catch (DocumentException localDocumentException)
        {
            this.log.error("Operation ERROR:ConfigParser getConfig error");
            str1 = str1 + "Operation ERROR:ConfigParser getConfig error\n";
            localResultlog.appendLog(str1);

            throw localDocumentException;
        }
    }
}

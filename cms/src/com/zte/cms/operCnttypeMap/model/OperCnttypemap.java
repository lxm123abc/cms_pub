package com.zte.cms.operCnttypeMap.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class OperCnttypemap extends DynamicBaseObject
{
    private java.lang.Long operid;
    private java.lang.String typeid;
    private java.lang.String typeflag;
    private java.lang.String servicekey;

    public java.lang.Long getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.Long operid)
    {
        this.operid = operid;
    }

    public java.lang.String getTypeid()
    {
        return typeid;
    }

    public void setTypeid(java.lang.String typeid)
    {
        this.typeid = typeid;
    }

    public java.lang.String getTypeflag()
    {
        return typeflag;
    }

    public void setTypeflag(java.lang.String typeflag)
    {
        this.typeflag = typeflag;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public void initRelation()
    {
        this.addRelation("operid", "OPERID");
        this.addRelation("typeid", "TYPEID");
        this.addRelation("typeflag", "TYPEFLAG");
        this.addRelation("servicekey", "SERVICEKEY");
    }
}

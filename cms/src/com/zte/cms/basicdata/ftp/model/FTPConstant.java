package com.zte.cms.basicdata.ftp.model;

/**
 * 常量类,用于添加日志操作
 * 
 */
public class FTPConstant
{

    // mgttype 管理类型 ftp管理

    public static final String MGTTYPE_FTP = "log.ftp.mgt";

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_FTP_ADD = "log.ftp.add";// ftp新增
    public static final String OPERTYPE_FTP_MOD = "log.ftp.modify";// ftp修改
    public static final String OPERTYPE_FTP_DEL = "log.ftp.delete";// ftp删除
    public static final String OPERTYPE_FTP_NOTEXIST = "log.ftp.notexist";// ftp信息不存在或已被删除
    public static final String OPERTYPE_FTP_CANNOTDELETE = "log.ftp.connotdelete";// ftp信息被sp引用不能删除

    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_FTP_ADD_INFO = "log.ftp.add.info";
    public static final String OPERTYPE_FTP_MOD_INFO = "log.ftp.modify.info";
    public static final String OPERTYPE_FTP_DEL_INFO = "log.ftp.delete.info";

}

package com.zte.ucm.util;

import java.io.BufferedInputStream;  
import java.io.BufferedReader;  
import java.io.ByteArrayOutputStream;  
import java.io.InputStream;  
import java.io.InputStreamReader;  
import java.io.OutputStreamWriter;  
import java.net.URL;  
import java.net.URLConnection;  
  
import org.apache.commons.httpclient.HttpClient;  
import org.apache.commons.httpclient.HttpStatus;  
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

import com.zte.ismp.common.ConfigUtil;  
  
/** 
 * 测试调用一些meeting第三方接口 
 * @author Jack.Song 
 */  
public class SmsServiceUtil
{ 
    
    /** 
     * @param args 
     */  
    public static void main(String[] args) {  
        
        String url = "http://localhost:8080/slcshttp/services/AuthenticationService";  
        
        SmsServiceUtil tmi = new SmsServiceUtil();  
        tmi.SmsSendPost("","","",url);  
    }           
      
    /**
     * 发送短信
     * @param title 短信标题
     * @param content   短信内容
     * @param phonenumber    电话号码
     * @param SMSServiceUrl     短信接口信息
     */ 
    public static void SmsSendPost(String title,String content,String phonenumber,String SMSServiceUrl) {  
//        System.out.println("发送短信给："+phonenumber+"，标题是："+title+"，内容是："+content+",接口是："+SMSServiceUrl);
//        String xmlInfo1 = getXmlInfo(title,content,phonenumber);  
//        System.out.println("SMSServiceUrl=" + SMSServiceUrl); 
//        System.out.println("xmlInfo=" + xmlInfo1);        
        try {  
            URL url = new URL(SMSServiceUrl);  
            URLConnection con = url.openConnection();  
            con.setDoOutput(true);  
            con.setDoInput(true);
//          con.setRequestProperty("Pragma:", "no-cache");  
//          con.setRequestProperty("Cache-Control", "no-cache");  
            con.setRequestProperty("Content-Type", "application/xml");  
          
            con.setRequestProperty("Charset", "utf-8");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)"); 
            con.setRequestProperty("accept","*/*");
            OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());      
            String xmlInfo = getXmlInfo(title,content,phonenumber);  
            System.out.println("SMSServiceUrl=" + SMSServiceUrl);  
            out.write(new String(xmlInfo.getBytes("UTF-8")));  
            out.flush();  
            out.close();  
            BufferedReader br = new BufferedReader(new InputStreamReader(con  
                    .getInputStream(),"UTF-8"));  
            String line = "";  
            for (line = br.readLine(); line != null; line = br.readLine()) {  
                System.out.println(line);  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
  
    private static String getXmlInfo(String title,String content,String phonenumber) {       
        String SYSID = StringUtils.trimToEmpty(ConfigUtil.get("sms_info_SYSID"));
        String SEQ = StringUtils.trimToEmpty(ConfigUtil.get("sms_info_SEQ"));
        String TEMPLATEID = StringUtils.trimToEmpty(ConfigUtil.get("sms_info_TEMPLATEID"));
        String LOGINNO = StringUtils.trimToEmpty(ConfigUtil.get("sms_info_LOGINNO"));

        StringBuilder sb = new StringBuilder();        
        sb.append("![CDATA[<?xml version=\"1.0\" encoding=\"GBK\"?>\n" +
                   "<ROOT>\n");
        sb.append("<SYSID>"+SYSID+"</SYSID>");
        sb.append("<SEQ>"+SEQ+"</SEQ>");
        sb.append("<TEMPLATEID>"+TEMPLATEID+"</TEMPLATEID>");
        sb.append("<PARAMS>{\"msg\":\""+ 
                        title + "：" + content +
                        "\"}</PARAMS>");
        sb.append("<PHONENO>" + phonenumber + 
                        "</PHONENO>");
        sb.append("<LOGINNO>"+LOGINNO+"</LOGINNO>\n" +
                "</ROOT>]]>");
        return sb.toString();  
    }  
}
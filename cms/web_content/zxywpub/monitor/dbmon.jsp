<%@page language="java" contentType="text/html"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="com.zte.jservadm.SessionCounter" %>
<jsp:useBean id="db" class="com.zte.zxywpub.smapadm" scope="application" />

<%!
  private String timedsp(long ltime){
    Calendar cal=Calendar.getInstance();
    String sTime = "Free";
    cal.setTimeInMillis(ltime);
    if(ltime>0L){
      sTime = "" + cal.get(Calendar.YEAR) +"."+ (cal.get(Calendar.MONTH)+1);
      sTime = sTime +"."+cal.get(Calendar.DAY_OF_MONTH);
      sTime = sTime +" "+cal.get(Calendar.HOUR_OF_DAY) +":"+cal.get(Calendar.MINUTE);
      sTime = sTime +":"+cal.get(Calendar.SECOND);
    }
    return sTime;
  }
  
  private String statdsp(int i){
    String dspstr="Idle";
    if(i==1){
      dspstr="False";
    }else if(i==2){
      dspstr="True";
    }
    return dspstr;
  }
%>

<%
//本模块通过SMP链接直接访问
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");

String Strmsg="";
int i=0,j=0,k=0,cmdint=0;
if(request.getParameter("cmd")!=null)cmdint=java.lang.Integer.parseInt(request.getParameter("cmd"));
int Statid=0;
String Remoteoper=(String)session.getAttribute("OPERNAME");
String Refreshtime="5";
String servletPath = request.getServletPath();
String rootPath = (servletPath.split("zxywpub"))[0];
if(Remoteoper==null){
  if(cmdint>0){
  %><script language="javascript1.2">
    alert("Please login first");
	top.location.href = ' <%=rootPath%>' + "zxywpub";
  </script><%
    cmdint=0;
  }
}

%><!-- JSP programs end -->
<HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<TITLE>SMP--Home Page</TITLE>
<link REL="STYLESHEET" HREF="inc/style.css" TYPE="text/css">
</HEAD>
<BODY topmargin="0" leftmargin="0" style="background-color: rgb(249,250,244)">

<table border="0" cellPadding="5" cellSpacing="1" width="100%" height="95%" bgcolor="#C0C0C0">
   <tr height="95%">
      <td vAlign="top" bgcolor="#FFFFFF" background="images/bg01.jpg">
<!-- Main display info -->
<%
if(cmdint==98){
  String Dbname=request.getParameter("dbname");
  db.webdbrestart(Dbname);
  cmdint=99;

} 
if(cmdint==66){
  if(request.getParameter("statid")!=null) Statid=java.lang.Integer.parseInt(request.getParameter("statid"));
  db.setstatlevel(Statid);
  cmdint=99;
} 
if(cmdint==99){
//预留给连接池显示
  long freeMomory = Runtime.getRuntime().freeMemory()/1024;
  long totalMomory = Runtime.getRuntime().totalMemory()/1024;

  if(request.getParameter("refreshtime")!=null)Refreshtime=request.getParameter("refreshtime");
  out.print("Now server: "+java.net.InetAddress.getLocalHost().getHostName());
  out.print("["+java.net.InetAddress.getLocalHost().getHostAddress()+"] Java memory:");
  out.print("["+totalMomory+"K] Free memory:["+freeMomory+"K] ");
  out.println("online: "+ SessionCounter.getActiveSessions());
  Statid=db.getstatlevel(); 
if(System.getProperty("os.name").toLowerCase().indexOf("win")>=0){
 out.println("CPU: "+ com.zte.zxywpub.cpusagent.getInstance().getCpuUsageRate());
}else{
 float fl=com.zte.zxywpub.BsdPerformance.getInstance().getCpuUsageRate();
 out.println("CPU: "+ fl);
}
%>
<form method="post" name="inForm" action="dbmon.jsp">
<input type="hidden" name="cmd" value="<%=cmdint%>">
<input type="hidden" name="dbname" value="all">

<div align="center"><center>
  <table border="0" width="400" cellspacing="0" cellpadding="2" style=" font-size: 9pt">
    <tr>
    <td><select name="refreshtime" size="1">
      <OPTION value='3'> 3s </OPTION>
      <OPTION value='5'> 5s </OPTION>
      <OPTION value='10'> 10s </OPTION>
      <OPTION value='15'> 15s </OPTION>
      <OPTION value='30'> 30s </OPTION>
      <OPTION value='60'> 60s </OPTION>
      </select> 
      <span onclick="cleardb('all')" onmouseover="this.style.cursor='hand'"> [Rs] </span>
      </td>
      <td>Stat<select name="statid" size="1" onchange="setstat()">
      <OPTION value='0'> No </OPTION>
      <OPTION value='1'> Yes </OPTION>
      </select>
      </td>
      <td align="right"><input type="button" onclick="renew()" value="Refresh" name="B11" tabindex="91"></td>
    </tr>
  </table><hr size="1" width="500">
<%
  Hashtable hsPools = db.getPoolinfo();
  Enumeration PoolInfos = hsPools.elements();
  ArrayList poolinfo;
  long ltime = 0L;
  String sName="",sTmp=null;
  while (PoolInfos.hasMoreElements()) {
    poolinfo = (ArrayList) PoolInfos.nextElement();
    j=poolinfo.size();
   if(j>0){
    sTmp="";
    sName=poolinfo.get(0).toString();
    out.println("<table border='1' width='100%' cellspacing='0' cellpadding='2' style=' font-size: 9pt'>");
    out.println("<tr><td style='background-color:#cceeff;' align='center'> "+sName+" Pool &nbsp; [<span onclick=\"cleardb('"+sName+"')\" onmouseover=\"this.style.cursor='hand'\">Rs</span>] </td><td style='background-color:#eeeeff;'>State</td><td width='10%' style='background-color:#eeeeff;'>STMT</td><td style='background-color:#eeffee;'>First Create Time</td><td style='background-color:#eeffee;'>Last Create Time</td><td style='background-color:#eeffee;'>Recreate times</td><td style='background-color:#eeffee;'>Control STMT</td></tr>");
    k=0;
    for(i=0;i<j;i=i+7){
        k=k+1;
    	ltime=java.lang.Long.parseLong(poolinfo.get(i+1).toString());
    	sTmp= sTmp + "<tr><td> "+poolinfo.get(i).toString()+"["+k+"] </td><td>"+timedsp(ltime)+" </td><td>"+poolinfo.get(i+2).toString()+"</td>";
    	ltime=java.lang.Long.parseLong(poolinfo.get(i+3).toString());
    	sTmp = sTmp + "<td>"+timedsp(ltime)+"</td>";
    	ltime=java.lang.Long.parseLong(poolinfo.get(i+4).toString());
    	sTmp = sTmp + "<td>"+timedsp(ltime)+"</td>";
    	ltime=java.lang.Integer.parseInt(poolinfo.get(i+5).toString());
    	sTmp = sTmp + "<td>"+ltime+"</td>";
    	sTmp = sTmp +"<td> "+ statdsp(Integer.parseInt(poolinfo.get(i+6).toString()))+"</td></tr>\n";
    }
    out.println(sTmp);
 } 
 }
%></center></div>
</form>
<script language="JavaScript1.2">
function cleardb(dbname){
  document.inForm.cmd.value="98";
  document.inForm.dbname.value=dbname;
  document.inForm.submit();
}
function renew(){
  document.inForm.submit();
}
function setstat(){
  document.inForm.cmd.value="66";
  document.inForm.submit();
}
document.inForm.refreshtime.value="<%=Refreshtime%>";
document.inForm.statid.value="<%=Statid %>";
setTimeout('renew()',<%=Refreshtime%>*1000);
</script>
<%
}%>
<!-- Main display info end -->
      </td>
   </tr>
<!-- Messages -->
   <tr height="5%"><td bgcolor="#f4f9fa" align="right"><%=Strmsg%></td></tr>
<!-- Messages end -->
</table>
</BODY>
</HTML>

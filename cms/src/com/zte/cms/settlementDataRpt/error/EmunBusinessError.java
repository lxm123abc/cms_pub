package com.zte.cms.settlementDataRpt.error;

/**
 * ProjectName: miaosha-com.miaoshaDemo.error
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 16:20 2019/5/6
 * @Description:
 */
public enum EmunBusinessError implements CommonError{
    //通用错误类型
    PARAMETER_VALIDATION_ERROR(00001,"参数不合法"),
    NUKNOWN_ERROR(00002,"未知错误"),

    USER_ERROR_NOT_FIND(10001,"找不到该用户"),

    FILE_CONTENT_FORMAT_ERROR(30001,"文件内容格式错误"),
    FILE_FORMAT_ERROR(30001,"文件格式错误"),

    ERROR_EXCEL_IMPORT(5001,"Excel导入错误"),
    ERROR_EXCEL_EXPORT(5002,"Excel导出错误")
    ;

    private EmunBusinessError(int errorCode,String errorMsg){
        this.errorCode=errorCode;
        this.errorMsg=errorMsg;
    }

    private int errorCode;
    private String errorMsg;

    @Override
    public int getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMsg;
    }

    @Override
    public CommonError setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }
}

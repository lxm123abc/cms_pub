package com.zte.cms.notice.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Noticemap extends DynamicBaseObject
{
    private java.lang.Integer messageid;
    private java.lang.Integer refermessageid;
    private java.lang.String title;
    private java.lang.Integer receivernum; // 消息关联的CP数
    private java.lang.Integer viewnum;// 未读消息CP数
    private java.lang.String createtime;
    private java.lang.String createtimeend; //
    private java.lang.String updatetime;
    private java.lang.String description;
    private java.lang.Integer replayid;
    private java.lang.String replaydescription;
    private java.lang.String replaytime;
    private java.lang.String issms; // 是否发短信，0否，1 是
    private java.lang.String isemail; // 是否发邮箱，0否，1 是
    private java.lang.String cpidlist;
    private java.lang.String cpid;
    private java.lang.String cpname;
    private java.lang.String operatorid;
    private java.lang.String operatoridlist;
    private java.lang.String opername;
    private java.lang.String viewtime;
    private java.lang.String sendtime;
    private java.lang.Integer status;
    private java.lang.String smsstatus;//短信发送状态
    private java.lang.String cpcreditlevel;//信用等级   
    private java.lang.Integer messagetype;//消息类型 1：公告  2;私信
    private java.lang.String receiverid;//接受消息与cp或者操作员关联ID
    private java.lang.Integer receivertype;//接受消息类型 1：公告  2;私信
    private java.lang.Integer attachid;
	private java.lang.String attachname;//附件本身名字
    private java.lang.String attachurl;//附件存入FTP服务器中的新文件名
    private java.lang.String message_receiverid;//消息表接受者id,表中对应receiverid
    private java.lang.String repaly_receiverid;//回复表接受者id,表中对应receiverid
    private java.lang.String repaly_receivername;//回复表接受者姓名
    private java.lang.String message_receivername;//私信收件人名字
    private java.lang.String createid;//私信创建者id
    private java.lang.String createname;//创建者name
    private java.lang.String lastReplaytime;//最后一次回复时间
    private java.lang.String lastReplayid;//最后一次回复人
    private java.lang.String lastReplayname;//最后一次回复人账户名
    private java.lang.String messageStatus;//消息状态：0正常，1撤回
    private java.lang.Integer step2;
    
    public java.lang.String getIsemail()
    {
        return isemail;
    }

    public void setIsemail(java.lang.String isemail)
    {
        this.isemail = isemail;
    }

    public java.lang.String getMessageStatus()
    {
        return messageStatus;
    }

    public void setMessageStatus(java.lang.String messageStatus)
    {
        this.messageStatus = messageStatus;
    }

    public java.lang.Integer getRefermessageid()
    {
        return refermessageid;
    }

    public void setRefermessageid(java.lang.Integer refermessageid)
    {
        this.refermessageid = refermessageid;
    }
    
    public java.lang.String getLastReplaytime()
    {
        return lastReplaytime;
    }

    public void setLastReplaytime(java.lang.String lastReplaytime)
    {
        this.lastReplaytime = lastReplaytime;
    }
    public java.lang.String getLastReplayid()
    {
        return lastReplayid;
    }

    public void setLastReplayid(java.lang.String lastReplayid)
    {
        this.lastReplayid = lastReplayid;
    }

    public java.lang.String getMessage_receiverid()
    {
        return message_receiverid;
    }

    public void setMessage_receiverid(java.lang.String message_receiverid)
    {
        this.message_receiverid = message_receiverid;
    }

    public java.lang.String getRepaly_receiverid()
    {
        return repaly_receiverid;
    }

    public void setRepaly_receiverid(java.lang.String repaly_receiverid)
    {
        this.repaly_receiverid = repaly_receiverid;
    }

    public java.lang.String getCreateid()
    {
        return createid;
    }

    public void setCreateid(java.lang.String createid)
    {
        this.createid = createid;
    }

    public java.lang.String getOperatoridlist()
    {
        return operatoridlist;
    }

    public void setOperatoridlist(java.lang.String operatoridlist)
    {
        this.operatoridlist = operatoridlist;
    }

    public java.lang.Integer getMessagetype()
    {
        return messagetype;
    }

    public void setMessagetype(java.lang.Integer messagetype)
    {
        this.messagetype = messagetype;
    }

    public java.lang.String getReceiverid()
    {
        return receiverid;
    }

    public void setReceiverid(java.lang.String receiverid)
    {
        this.receiverid = receiverid;
    }

    public java.lang.Integer getReceivertype()
    {
        return receivertype;
    }

    public void setReceivertype(java.lang.Integer receivertype)
    {
        this.receivertype = receivertype;
    }

    public java.lang.Integer getMessageid()
    {
        return messageid;
    }

    public void setMessageid(java.lang.Integer messageid)
    {
        this.messageid = messageid;
    }

    public java.lang.String getTitle()
    {
        return title;
    }

    public void setTitle(java.lang.String title)
    {
        this.title = title;
    }

    public java.lang.Integer getReceivernum()
    {
        return receivernum;
    }

    public void setReceivernum(java.lang.Integer receivernum)
    {
        this.receivernum = receivernum;
    }

    public java.lang.Integer getViewnum()
    {
        return viewnum;
    }

    public void setViewnum(java.lang.Integer viewnum)
    {
        this.viewnum = viewnum;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getUpdatetime()
    {
        return updatetime;
    }

    public void setUpdatetime(java.lang.String updatetime)
    {
        this.updatetime = updatetime;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getIssms()
    {
        return issms;
    }

    public void setIssms(java.lang.String issms)
    {
        this.issms = issms;
    }

    public java.lang.String getCreatetimeend()
    {
        return createtimeend;
    }

    public void setCreatetimeend(java.lang.String createtimeend)
    {
        this.createtimeend = createtimeend;
    }

    public java.lang.String getReplaydescription()
    {
        return replaydescription;
    }

    public void setReplaydescription(java.lang.String replaydescription)
    {
        this.replaydescription = replaydescription;
    }

    public java.lang.String getCpidlist()
    {
        return cpidlist;
    }

    public void setCpidlist(java.lang.String cpidlist)
    {
        this.cpidlist = cpidlist;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpname()
    {
        return cpname;
    }

    public void setCpname(java.lang.String cpname)
    {
        this.cpname = cpname;
    }

    public java.lang.String getOperatorid()
    {
        return operatorid;
    }

    public void setOperatorid(java.lang.String operatorid)
    {
        this.operatorid = operatorid;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getSmsstatus()
    {
        return smsstatus;
    }

    public void setSmsstatus(java.lang.String smsstatus)
    {
        this.smsstatus = smsstatus;
    }

    public java.lang.String getViewtime()
    {
        return viewtime;
    }

    public void setViewtime(java.lang.String viewtime)
    {
        this.viewtime = viewtime;
    }

    public java.lang.String getSendtime()
    {
        return sendtime;
    }

    public void setSendtime(java.lang.String sendtime)
    {
        this.sendtime = sendtime;
    }

    public java.lang.String getCpcreditlevel()
    {
        return cpcreditlevel;
    }

    public void setCpcreditlevel(java.lang.String cpcreditlevel)
    {
        this.cpcreditlevel = cpcreditlevel;
    }

    public java.lang.Integer getReplayid()
    {
        return replayid;
    }

    public void setReplayid(java.lang.Integer replayid)
    {
        this.replayid = replayid;
    }

    public java.lang.String getReplaytime()
    {
        return replaytime;
    }

    public void setReplaytime(java.lang.String replaytime)
    {
        this.replaytime = replaytime;
    }
    
    public java.lang.Integer getAttachid() {
		return attachid;
	}

	public void setAttachid(java.lang.Integer attachid) {
		this.attachid = attachid;
	}

	public java.lang.String getAttachname() {
		return attachname;
	}

	public void setAttachname(java.lang.String attachname) {
		this.attachname = attachname;
	}

	public java.lang.String getAttachurl() {
		return attachurl;
	}

	public void setAttachurl(java.lang.String attachurl) {
		this.attachurl = attachurl;
	}

	public java.lang.String getMessage_receivername() {
		return message_receivername;
	}

	public void setMessage_receivername(java.lang.String message_receivername) {
		this.message_receivername = message_receivername;
	}
	
	public java.lang.String getCreatename() {
		return createname;
	}

	public void setCreatename(java.lang.String createname) {
		this.createname = createname;
	}
	
	public java.lang.String getLastReplayname() {
		return lastReplayname;
	}

	public void setLastReplayname(java.lang.String lastReplayname) {
		this.lastReplayname = lastReplayname;
	}

	public java.lang.String getRepaly_receivername() {
		return repaly_receivername;
	}

	public void setRepaly_receivername(java.lang.String repaly_receivername) {
		this.repaly_receivername = repaly_receivername;
	}
	

    public java.lang.Integer getStep2() {
		return step2;
	}

	public void setStep2(java.lang.Integer step2) {
		this.step2 = step2;
	}

	public void initRelation()
    {
        this.addRelation("messageid", "MESSAGEID");
        this.addRelation("title", "TITLE");
        this.addRelation("receivernum", "RECEIVERNUM");
        this.addRelation("viewnum", "VIEWNUM");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("updatetime", "UPDATETIME");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("viewtime", "VIEWTIME");
        this.addRelation("sendtime", "SENDTIME");
        this.addRelation("issms", "ISSMS");
        this.addRelation("cpid", "CPID");
        this.addRelation("cpname", "CPNAME");
        this.addRelation("operatorid", "OPERATORID");
        this.addRelation("opername", "OPERNAME");
        this.addRelation("status", "STATUS");
        this.addRelation("smsstatus", "SMSSTATUS");
        this.addRelation("cpcreditlevel", "CPCREDITLEVEL");
        this.addRelation("messagetype", "MESSAGETYPE");
        this.addRelation("receiverid", "RECEIVERID");
        this.addRelation("receivertype", "RECEIVERTYPE");
    }

}

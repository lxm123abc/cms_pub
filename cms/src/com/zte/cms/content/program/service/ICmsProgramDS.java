package com.zte.cms.content.program.service;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgram;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramDS
{
    /**
     * 新增CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgram(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgram(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 批量更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramList(List<CmsProgram> cmsProgramList) throws DomainServiceException;

    /**
     * 根据条件更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramListByCond(List<CmsProgram> cmsProgramList) throws DomainServiceException;

    /**
     * 删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgram(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 批量删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramList(List<CmsProgram> cmsProgramList) throws DomainServiceException;

    /**
     * 根据条件删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramListByCond(List<CmsProgram> cmsProgramList) throws DomainServiceException;

    /**
     * 查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @return CmsProgram对象
     * @throws DomainServiceException ds异常
     */
    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws DomainServiceException;
    public CmsProgram getCmsProgramById(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 根据条件查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @return 满足条件的CmsProgram对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsProgram> getCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsProgram对象数量
     * 
     * @param cmsProgram
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public int getCmsProgramCount(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * <p>
     * 根据内容信息获取内容新增工作流路由信息
     * </p>
     * 
     * @param cmsProgram 内容对象
     * @return 返回路由信息
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String getRoute(CmsProgram cmsProgram) throws DomainServiceException;

    /**
     * <p>
     * 根据内容信息获取内容修改工作流路由信息
     * </p>
     * 
     * @param cmsProgram 内容对象
     * @return 返回路由信息
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String getModRoute(CmsProgram cmsProgram) throws DomainServiceException;
    
    /**
     * 根据模糊条件查询并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryForPage(CmsProgram cmsProgram, int start, int pageSize)
            throws DomainServiceException;
}
package com.zte.ismp.common.util;

import java.io.*;
import java.net.*;
import org.apache.log4j.Logger;

/**
 * <p>
 * 文件名称: TcpUtil.java
 * </p>
 * <p>
 * 文件描述: Tcp操作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: Tcp操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class TcpUtil
{
    // ****** 代码段: 常量定义 *******************************************************************************/
    public static final int STATE_SUCCESS = 0;
    public static final int STATE_FAIL = 1;
    public static final int TIMEOUT = 3000;

    // ****** 代码段: 变量定义 *******************************************************************************/
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Socket clientSocket = null; // 用来存储客户单的socket信息
    private BufferedReader in = null;
    private PrintWriter out = null;

    // ****** 代码段: 构造方法 *******************************************************************************/
    public TcpUtil()
    {
    }

    // ****** 代码段: 公共方法 *******************************************************************************/
    // 建立Socket连接
    public int connect(String host, int port)
    {
        if (clientSocket == null)
        {
            // 将此套接字连接到具有指定超时值的服务器
            try
            {
                // 初始化 socket 对象，通过系统默认类型的 SocketImpl 创建未连接套接字
                clientSocket = new Socket();
                // 根据 IP 地址和端口号创建套接字地址
                InetSocketAddress socketAddress = new InetSocketAddress(host, port);
                clientSocket.connect(socketAddress, TIMEOUT);
                // 设定接收信息超时时长
                clientSocket.setSoTimeout(TIMEOUT);
            }
            catch (UnknownHostException ex)
            { // 无法解析指定主机，连接失败
                clientSocket = null;
                logger.error("无法解析指定主机，连接失败!", ex);
                return this.STATE_FAIL;
            }
            catch (SocketTimeoutException ex)
            { // 连接超时
                clientSocket = null;
                logger.error("连接服务器[" + host + ":" + port + "]超时！超时时长为：" + TIMEOUT + "毫秒。", ex);
                return this.STATE_FAIL;
            }
            catch (IOException ex)
            {
                clientSocket = null;
                logger.error("连接服务器[" + host + ":" + port + "]失败", ex);
                return this.STATE_FAIL;
            }
        }
        return this.STATE_SUCCESS;
    }

    public void disconnect()
    {
        if (clientSocket != null)
        {
            try
            {
                clientSocket.close();
            }
            catch (IOException ex)
            {
                logger.error("断开Socket连接失败", ex);
            }
        }
    }

    public String sendMessage(String message)
    {
        String returnStr = null;
        if (clientSocket != null)
        {
            try
            {
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())));
                out.println(message);
                out.flush();
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                returnStr = in.readLine();
            }
            catch (SocketTimeoutException ex)
            { // 接收信息超时
                logger.error("指定时间\"" + TIMEOUT + "毫秒\"内未收到服务器的响应！", ex);
                return null;
            }
            catch (IOException ex)
            {
                logger.error("IO错误：", ex);
                return null;
            }
        }
        return returnStr;
    }

    // ****** 代码段: 调试信息 *******************************************************************************/
    public static void main(String[] args)
    {
        TcpUtil s = new TcpUtil();
        if (s.connect("10.40.51.3", 6790) == 0)
        {
            System.out.println("已经连接服务器");
            String rtnStr = s.sendMessage("request 1000\r\n");
            System.out.println("返回消息：" + rtnStr);
            // rtnStr = s.sendMsg("Second Time");
            // System.out.println("返回消息："+rtnStr);
            s.disconnect();
            System.out.println("断开Socket连接");
        }
    }
}

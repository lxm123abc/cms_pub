package com.zte.cms.channel.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsScheduleLS
{
    /**
     * 增加节目单
     * 
     * @param CmsSchedule cmsSchedule对象
     * @param cpid
     * @return 增加结果后详细信息
     * @throws Exception
     */
    public String insertCmsScheduleAndRecord(CmsSchedule cmsSchedule) throws Exception;
    
    /**
     * 查询频道是否存在 状态是否是已同步 及该频道的CP是否不是删除状态
     * 
     * @param String channelid
     * @return 查询结果 sucess为频道可用
     * @throws Exception
     */
    public String getChannelByChannelid(String channelid) throws Exception;

    /**
     * 更新CmsSchedule cmsSchedule对象
     * 
     * @param CmsSchedule cmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public String updateCmsSchedule(CmsSchedule cmsSchedule) throws Exception;
    public String isReapetedSchePub(CmsSchedule cmsSchedule) throws Exception;

    /**
     * 批量删除CmsSchedule cmsSchedule对象
     * 
     * @param scheduleidndx[] 节目单ID集合 channelid 频道编码
     * @throws 异常
     */
    public String removeCmsScheduleList(String scheduleid[], String channelid) throws Exception;

    /**
     * 查询CmsSchedule cmsSchedule对象
     * 
     * @param 节目单ID
     * @return String "true"节目单存在 "false"节目单不存在
     * @throws DomainServiceException ds异常
     */
    public String getCmsSchedule(String scheduleid) throws Exception;

    /**
     * 查询CmsSchedule cmsSchedule对象
     * 
     * @param 节目单ID
     * @return CmsSchedule 对象
     * @throws DomainServiceException ds异常
     */
    public CmsSchedule getScheduleById(String scheduleid) throws Exception;

    /**
     * 根据条件分页查询CmsSchedule cmsSchedule对象
     * 
     * @param CmsSchedule cmsSchedule对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws Exception;

    /**
     * 同步节目单
     * 
     * @param scheduleid[] 节目单ID集合
     * @param channelid 频道编码
     * @return 同步结果信息描述
     * @throws Exception
     */
    public String publishSchedule(List<CntPlatformSync> cntPlatformSynclist) throws Exception;

  

    /*******************************************************************************************************************
     * 获取在线区路径
     * 
     * @param null
     * @return String在线区地址
     * @throws Exception
     ******************************************************************************************************************/
    public String getTempAreAddr() throws Exception;

    /*******************************************************************************************************************
     * 取出同步XML文件FTP地址
     * 
     * @param null
     * @return String 同步XML文件FTP地址
     * @throws Exception
     ******************************************************************************************************************/
    public String getSynXMLFTPAddr() throws Exception;

    /**
     * 获取挂载点
     * 
     * @param null
     * @return String 挂载点
     * @throws Exception
     */
    public String getMountPoint() throws Exception;

  
    /**
     * 批量录入节目单信息
     */
    public String scheduleBatchImport() throws Exception;

    /**
     * 节目单预下线
     * 
     * @param String scheduleid节目单id
     */
    public String schedulePreOffline(String scheduleid) throws Exception;

    /**
     * 节目单时间平移
     * 
     * @param scheduleid节目单ID集合
     * @param channelid频道编码
     * @return 节目单时间平移详细描述
     * @throws Exception
     */
    public String cancelAdjustTime(List<String> scheduleidList, List<String> adjustTimeArr) throws Exception;

    /**
     * 节目单审核
     * 
     * @param scheduleid节目单ID
     * @param oppnion 审核意见
     * @return String 审核结果
     * @throws Exception
     */
    public String auditSchedule(String oppnion, String scheduleid) throws Exception;

    /**
     * pageInfoQuery查询节目单类表
     * 
     * @param CmsSchedule cmsSchedule, int start,int pageSize
     * @return tabledatainfo
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryForRecord(CmsSchedule cmsSchedule, int start, int pageSize) throws Exception;

    /**
     * 通过Index获得节目单
     * @param scheduleindex String
     * @return CmsSchedule
     * @throws Exception
     */
    public CmsSchedule getScheduleByIndex(String scheduleindex) throws Exception;
    
  
	public TableDataInfo popForPubSchedule(CntTargetSync cntTargetSync,
			int start, int pageSize) throws DomainServiceException;
	public String bindBatchScheduleTargetsys(
			List<Targetsystem> targetsystemList,
			List<CmsSchedule> cmsScheduleList) throws DomainServiceException;

	public TableDataInfo queryCtgTarSync(CSCntTargetSync CScntTargetSync,
			int start, int pageSize);
	public TableDataInfo queryCtgPltSyncSche(CSCntPlatformSync cntPlatformSync,
			int start, int pageSize);
	public String cancelpublishSchedulePlt(
			List<CntPlatformSync> cntPlatformSynclist) throws Exception;
	public TableDataInfo queryCtgTarSyncSche(CSCntTargetSync CScntTargetSync,
			int start, int pageSize);
	public HashMap publishScheduleTar(List<CntTargetSync> tarlist)
	throws Exception;
	//节目单网元取消发布
	public HashMap cancelpublishScheduleTar(List<CntTargetSync> tarlist)
			throws Exception ;
	public HashMap deletecntsync(long syncindex);
    public HashMap cancelpublishSchedulePltSingle(List<CntPlatformSync> cntPlatformSynclist) throws Exception;
    public HashMap publishScheduleSingle(List<CntPlatformSync> cntPlatformSynclist) throws Exception;

}

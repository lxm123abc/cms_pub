package com.zte.cms.cntSyncRecord.ls;

import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.EspecialCharMgt;

public class CntSyncRecordLS implements ICntSyncRecordLS {

	private Log log = SSBBus.getLog(getClass());

	private ICntSyncRecordDS cntSyncRecordDS;

	public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start,
			int pageSize) {
		log.debug("pageInfoQuery start.....");

		String starttime = cntSyncRecord.getStartTime();
		if (!starttime.equals("")) {
			String starttime1 = starttime.substring(0, 4)
					+ starttime.substring(5, 7) + starttime.substring(8, 10)
					+ starttime.substring(11, 13) + starttime.substring(14, 16)
					+ starttime.substring(17, 19);
			cntSyncRecord.setStartTime(starttime1);
		
		}
		String starttime2 = cntSyncRecord.getEndTime();
		if (!starttime2.equals("")) {
			String starttime3 = starttime2.substring(0, 4)
					+ starttime2.substring(5, 7) + starttime2.substring(8, 10)
					+ starttime2.substring(11, 13)
					+ starttime2.substring(14, 16)
					+ starttime2.substring(17, 19);
			cntSyncRecord.setEndTime(starttime3);
		}
		
		//
		if (cntSyncRecord.getObjindexStr() != null && !cntSyncRecord.getObjindexStr().equals(""))
        {
            String cpcnshortname = EspecialCharMgt.conversion(cntSyncRecord.getObjindexStr());
            cntSyncRecord.setObjindexStr(cpcnshortname);
        }
		
		
		TableDataInfo tbInfo = null;
		try {
			tbInfo = cntSyncRecordDS.pageInfoQuery(cntSyncRecord, start, pageSize);
		} catch (Exception e) {
			log.error("CntSyncRecordLS pageInfoQuery error:" + e);
		}

		log.debug("pageInfoQuery end");
		return tbInfo;
	}



	public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord) {
		log.debug("get cntSyncRecord getCntSyncRecord starting...");
		CntSyncRecord obj = null;
		try {
			obj = new CntSyncRecord();
			obj = cntSyncRecordDS.getCntSyncRecord(cntSyncRecord);
		} catch (Exception e) {
			log.error("CntSyncRecordLS getCntSyncRecord error:" + e);
		}
		return obj;
	}

	public void removeCntSyncRecordByCond(CntSyncRecord cntSyncRecord) {
		log.debug("delete cntSyncRecord getCntSyncRecord starting...");
		CntSyncRecord objRecord = null;
		try {
			objRecord = new CntSyncRecord();
			cntSyncRecordDS.removeCntSyncRecord(objRecord);
		} catch (Exception e) {
			log.error("CntSyncRecordLS removeCntSyncRecordByCond error:" + e);
		}
	}

	public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS) {
		this.cntSyncRecordDS = cntSyncRecordDS;
	}

}

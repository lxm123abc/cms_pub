package com.zte.cms.ctgsrmap.service;

import java.util.List;

import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategorySeriesMapDS
{
    /**
     * 新增CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException;

    /**
     * 更新CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException;

    /**
     * 批量更新CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategorySeriesMapList(List<CategorySeriesMap> categorySeriesMapList)
            throws DomainServiceException;

    /**
     * 删除CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException;

    /**
     * 批量删除CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategorySeriesMapList(List<CategorySeriesMap> categorySeriesMapList)
            throws DomainServiceException;

    /**
     * 查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @return CategorySeriesMap对象
     * @throws DomainServiceException ds异常
     */
    public CategorySeriesMap getCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException;

    /**
     * 根据条件查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象
     * @return 满足条件的CategorySeriesMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategorySeriesMap> getCategorySeriesMapByCond(CategorySeriesMap categorySeriesMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategorySeriesMap对象
     * 
     * @param categorySeriesMap CategorySeriesMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

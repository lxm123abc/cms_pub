   	var menuNameArrays=[];
    function OpenWin(url)
    {    
      if(url !="common/error.jsp" && url.indexOf("null") ==-1 && url !="")
    {
       var url=getMenuUrl('..',url);
       window.open(url,"","resizable=yes,scrollbars=yes,status=yes,toolbar=no,menubar=no,location=no,dependent,width="+(screen.availWidth-300)+",height="+(screen.availHeight-300)+",left=0,top=0")
     }
    }
   function selectMenu(ShowValue) 
    {
    	PrevSelectedObj =  document.getElementsByName("PrevSelectedValue");    	  
        var LinkValue;
        var strValue;
        window.status = $res_entry('ui.loader.menu.switchover') ;
        if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	
        }
        else
        {
            LinkValue = ShowValue;
        }

       // if (PrevSelectedValue != LinkValue)
     //   {
            var trs = document.getElementsByTagName("td");
            var i ;
            
            strValue = "tdmenu" + PrevSelectedValue;
            if(undefined != document.getElementById(strValue))
            {
            document.getElementById(strValue).className  = "tab";
            }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    if(strValue != "tdmenu10")
                    {
                        trs.item(i-1).className  = "vline";
                    }
                    trs.item(i+1).className = "vline";
                }
            }
            
            strValue = "linkmenu" + PrevSelectedValue;
                        
            strValue = "tdmenu" + LinkValue;
             if(undefined != document.getElementById(strValue))
            {
            document.getElementById(strValue).className = "active";
            }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    trs.item(i-1).className  = "";
                    trs.item(i+1).className = "";
                }
            }
            
            strValue = "linkmenu" + LinkValue;

            PrevSelectedValue = LinkValue;
       // }
        return false;
    }      
   
    //////////////////////////////////////////////////////////////////////////////
    //函数名称： NavigatePage
    //功能说明： 菜单点击后导航各子页面
    //参数定义： ShowValue --  用户点击的当前菜单项
    //相关变量:  PrevSelectedValue -- 定义为上次用户点击菜单项编号,同时设置页面第一次打开的菜单项编号
    //开发人员： 刘建林
    //完成日期： 2005-03-28
    //////////////////////////////////////////////////////////////////////////////
    var PrevSelectedValue = 1;  // 默认显示的菜单项编号        		
    //  NavigatePage(1);
    function NavigatePage(ShowValue) 
    {
    	PrevSelectedObj =  document.getElementsByName("PrevSelectedValue");
    	  
        var LinkValue;
        var strValue;
        window.status = $res_entry('ui.loader.menu.switchover') ;
        setCurrentTopMenu(menuNameArrays[ShowValue-1]);
        if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	//增加刷新功能，胡成龙， 2004-6-9-6-9
        }
        else
        {
            LinkValue = ShowValue;
        }

      //  if (PrevSelectedValue != LinkValue)
     //   {
            var trs = document.getElementsByTagName("td");
            var i ;
            
            //清除以前的选中显示
            strValue = "tdmenu" + PrevSelectedValue;
            if(undefined != document.getElementById(strValue))
            {
            document.getElementById(strValue).className  = "tab";
            }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    if(strValue != "tdmenu10")
                    {
                        trs.item(i-1).className  = "vline";
                    }
                    trs.item(i+1).className = "vline";
                }
            }
            
            strValue = "linkmenu" + PrevSelectedValue;
                        
            strValue = "tdmenu" + LinkValue;
             if(undefined != document.getElementById(strValue))
            {
            document.getElementById(strValue).className = "active";
            }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    trs.item(i-1).className  = "";
                    trs.item(i+1).className = "";
                }
            }
            
            strValue = "linkmenu" + LinkValue;

            PrevSelectedValue = LinkValue;
     //   }
     
       var menuVector = getMenuVector();
       for(var k=0;k<menuVector.length;k++)
       {    
         var menu =  menuVector[k];  
		  if(LinkValue==menu.orderId)
		  {

            window.status = $res_entry('ui.loader.menu.openoject');
            if(menu.gray==false)
            {           
            top.leftFrame.location.href='index/FrameLeftMenu.html?parentMenuKey='+menu.id;
            }            
          //  top.leftdn.location.href='FrameLeftDown.html';
          //  top.rightup.location.href="FrameShowPos.html";
            window.status =  $res_entry('ui.loader.menu.openlevel'); 
            if(menu.uri !="common/error.jsp" && menu.uri.indexOf("null") ==-1 && menu.uri !="")
    	    {
	    	    var url=getMenuUrl('..',menu.uri);
	    	   if(menu.targetPara== "bottomSet")
    	       {
    	          //top.mainFrameSet.cols = "0,*";
				  //top.rightFrameSet.rows = "0,*";
				  top.mainFrame.location.href=url;
    	       }
    	       else
    	       {
    	         // top.mainFrameSet.cols = "141,67%";
				 // top.rightFrameSet.rows = "24,*";
    	       }    	       
	    	    if(menu.targetPara!= "bottomSet" && menu.targetPara!= null && menu.targetPara!="" && eval(menu.targetPara+'.location')!= undefined)
	            {
		    	    var str = menu.targetPara+'.location.href= url';
		    	    eval(str);	
			    }
	            else
	            {
	               top.mainFrame.location.href= url;
	            } 			   
		     }        
           }
        }
       

         //焦点集中
        //top.rightup.window.focus();

        window.status = $res_entry('ui.loader.menu.switchover');
        return false;
    }


  function expandall()
    {
        top.leftup.document.getElementById("BtnExpandAll").click();
    }
    
    function MouseOver(ShowValue) 
    {
        var LinkValue;
        var strValue;
        var tdValue;
        
        LinkValue = ShowValue;
        strValue = "linkmenu" + LinkValue;
        if (PrevSelectedValue != LinkValue)
        {
            document.getElementById(strValue).style.color = "yellow";
        }
        else
        {
            document.getElementById(strValue).style.color = "yellow";
        }
    }

    function MouseOut(ShowValue) 
    {
        var LinkValue;
        var strValue;        

				LinkValue = ShowValue;
        strValue = "linkmenu" + LinkValue;    

        if (PrevSelectedValue != LinkValue)
        {
        	document.getElementById(strValue).style.color = "white";
        }
        else
        {
        	document.getElementById(strValue).style.color = "blue";
        }
    }

    
    function NavigatePage1(ShowValue,var2,var3,var4,var5) 
    {           	  
        var LinkValue;
        var strValue;
        window.status = $res_entry('ui.loader.menu.switchover');
        if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	//增加刷新功能，胡成龙， 2004-6-9-6-9
        }
        else
        {
            LinkValue = ShowValue;
        }
        
        if (PrevSelectedValue != LinkValue)
        {
            var trs = document.getElementsByTagName("td");
            var i ;            
           
            strValue = "tdmenu" + PrevSelectedValue;

             if(undefined != document.getElementById(strValue)){
             document.getElementById(strValue).className  = "tab";
             }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    if(strValue != "tdmenu10")
                    {
                        trs.item(i-1).className  = "vline";
                    }
                    trs.item(i+1).className = "vline";
                }
            }
            
            strValue = "linkmenu" + PrevSelectedValue;
          
            strValue = "tdmenu" + LinkValue;

              if(undefined != document.getElementById(strValue)){
            document.getElementById(strValue).className = "active";
            }
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    trs.item(i-1).className  = "";
                    trs.item(i+1).className = "";
                }
            }
            
            strValue = "linkmenu" + LinkValue;          
            PrevSelectedValue = LinkValue;
        }
  
       var menuVector = getMenuVector();
       for(var k=0;k<menuVector.length;k++)
       {    
         var menu =  menuVector[k];
	     if(ShowValue==menu.orderId)
	     {           
           window.status = $res_entry('ui.loader.menu.openoject');
           //通过以下语句导航到相应的页面
           top.leftup.location.href='FrameLeftMenu.html?parentMenuKey='+menu.id+'&itemId='+var2+'&tdName='+var3+'&url='+var4+'&targetPara='+var5;
       //    top.leftdn.location.href='FrameLeftDown.html';
       //    top.rightup.location.href="FrameShowPos.html";
           window.status = $res_entry('ui.loader.menu.openlevel');        
          }
        }
        if(menu.targetPara != null && menu.targetPara != undefined && menu.targetPara== "bottomSet")
   	     {
   	        top.mainFrameSet.cols = "0,*";
			top.rightFrameSet.rows = "0,*";
   	     }
   	     else
   	     {
   	        top.mainFrameSet.cols = "141,67%";
			top.rightFrameSet.rows = "24,*";
   	     }
        
        window.status = $res_entry('ui.loader.menu.switchover');   
        return false;    
    }
    
    
 function SecondNavigatePage(ShowValue,var2,var3) 
    {           	  
        var LinkValue;
        var strValue;
        window.status = $res_entry('ui.loader.menu.switchover');
        if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	//增加刷新功能，胡成龙， 2004-6-9-6-9
        }
        else
        {
            LinkValue = ShowValue;
        }
        
        if (PrevSelectedValue != LinkValue)
        {
            var trs = document.getElementsByTagName("td");
            var i ;            
           
            strValue = "tdmenu" + PrevSelectedValue;
            document.getElementById(strValue).className  = "tab";
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    if(strValue != "tdmenu10")
                    {
                        trs.item(i-1).className  = "vline";
                    }
                    trs.item(i+1).className = "vline";
                }
            }
            
            strValue = "linkmenu" + PrevSelectedValue;
          
            strValue = "tdmenu" + LinkValue;
            document.getElementById(strValue).className = "active";
            for(i=0;i <trs.length;i++)
            {
                if( trs.item(i).id == strValue )
                {
                    trs.item(i-1).className  = "";
                    trs.item(i+1).className = "";
                }
            }
            
            strValue = "linkmenu" + LinkValue;          
            PrevSelectedValue = LinkValue;
        }
        
       var menuVector = getMenuVector();
       for(var k=0;k<menuVector.length;k++)
       {    
         var menu =  menuVector[k];
		 if(ShowValue==menu.orderId)
		 {           
            window.status = $res_entry('ui.loader.menu.openoject');					//加入了被选中的菜单的序号
            //通过以下语句导航到相应的页面
            top.leftup.location.href='FrameLeftMenu.html?parentMenuKey='+menu.id+'&itemId='+var2+'&url='+var3;
         //   top.leftdn.location.href='FrameLeftDown.html';
          //  top.rightup.location.href="FrameShowPos.html";
            window.status = $res_entry('ui.loader.menu.openlevel');       
          }
        }
        if(menu.targetPara== "bottomSet")
  	     {
  	        top.mainFrameSet.cols = "0,*";
		    top.rightFrameSet.rows = "0,*";
  	     }
  	     else
  	     {
  	         top.mainFrameSet.cols = "141,67%";
		     top.rightFrameSet.rows = "24,*";
  	     }               
        window.status =  $res_entry('ui.loader.menu.switchover');  
        return false;    
    }
  function OnLoadNavigatePage() 
    {
      var ShowValue = "1";
      var menuVector = getMenuVector();   
      if(menuVector != null && menuVector.length > 0) 
      { 
         ShowValue = menuVector[0].orderId;
      }
    	PrevSelectedObj =  document.getElementsByName("PrevSelectedValue");    	  
        var LinkValue;
        var strValue;
        window.status = $res_entry('ui.loader.menu.switchover');
        if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	//增加刷新功能，胡成龙， 2004-6-9-6-9
        }
        else
        {
            LinkValue = ShowValue;
        }

     //   if (PrevSelectedValue != LinkValue)
     //   {
      //       var trs = document.getElementsByTagName("td");
       //      var i ;
            
            //清除以前的选中显示?????
       //      strValue = "tdmenu" + PrevSelectedValue;
       //      if(undefined != document.getElementById(strValue))
        //     {
         //    document.getElementById(strValue).className  = "tab";
        //     }
       //      for(i=0;i <trs.length;i++)
       //      {
        //         if( trs.item(i).id == strValue )
        //         {
        //             if(strValue != "tdmenu10")
        //             {
         //                trs.item(i-1).className  = "vline";
         //            }
         //            trs.item(i+1).className = "vline";
          //       }
         //    }
            
         //    strValue = "linkmenu" + PrevSelectedValue;
                        
            //高亮当前的显示???u???????
         //    strValue = "tdmenu" + LinkValue;
         //     if(undefined != document.getElementById(strValue))
          //   {
           //  document.getElementById(strValue).className = "active";
          //   }
          //   for(i=0;i <trs.length;i++)
          //   {
           //      if( trs.item(i).id == strValue )
           //      {
           //          trs.item(i-1).className  = "";
           //          trs.item(i+1).className = "";
           //      }
          //   }
            
         //    strValue = "linkmenu" + LinkValue;

         //    PrevSelectedValue = LinkValue;
      //  }
              
       for(var k=0;k<menuVector.length;k++)
       {    
         var menu =  menuVector[k];
		  if(LinkValue==menu.orderId)
		  {
            window.status = $res_entry('ui.loader.menu.openoject');
            if(menu.gray==false)
            {
            //	alert("parentMenuKey="+menu.id);
            top.leftup.location.href='index/FrameLeftMenu.html?parentMenuKey='+menu.id;
            }            
          //  top.leftdn.location.href='FrameLeftDown.html';
          //  top.rightup.location.href="FrameShowPos.html";
            window.status =  $res_entry('ui.loader.menu.openlevel'); 
            if(menu.uri.indexOf("common/error.jsp")==-1 && menu.uri.indexOf("null") ==-1 && menu.uri !="")
    	     {
    	     
    	       var url=getMenuUrl('../..',menu.uri);
    	       top.right.location.href= url;
    	     }        
          }
        }
       

         //焦点集中
        top.rightup.window.focus();
        
      //  setTimeout('expandall()',1200);
        	       
        window.status = $res_entry('ui.loader.menu.switchover');
        return false;
    }

function NavigateFirstMenuPage(){

   var ShowValue = "1";
      var menuVector = getMenuVector();   
      if(menuVector != null && menuVector.length > 0) 
      { 
         ShowValue = menuVector[0].orderId;
      }
       if (ShowValue == 99)	
        {
            LinkValue = PrevSelectedValue;	//增加刷新功能，胡成龙， 2004-6-9-6-9
        }
        else
        {
            LinkValue = ShowValue;
        }
        for(var k=0;k<menuVector.length;k++)
       {    
         var menu =  menuVector[k];
		  if(LinkValue==menu.orderId)
		  {
            window.status = $res_entry('ui.loader.menu.openoject');
            if(menu.gray==false)
            {
                NavigatePage(ShowValue);
            }            
           }
        }
}

function pageLeft()
{
    document.all.topMenu.doScroll("pageLeft");
}
function pageRight()
{
    document.all.topMenu.doScroll("pageRight");
}

function displayMenu()
{
  var menustr = '';
  menuNameArrays=[];
  language = $res_entry("webdeploylanguage");
  if(language == 'en')
  {
      menustr = '<TABLE id=topLogo cellSpacing=0><TBODY><TR><TD id=logotd class=logo_en>&nbsp;</TD> ';
  }
  else
  {
      menustr = '<TABLE id=topLogo cellSpacing=0><TBODY><TR><TD id=logotd class=logo>&nbsp;</TD> ';
  }  
  menustr = menustr + '<td width="30px" align="middle" style="vertical-align:middle"><img onclick="pageLeft()" id=leftpic style="cursor:hand" src="./images/menu_left.gif"  style="margin-bottom:-10px"></td><td class="topRight">';
  menustr = menustr + '<div id="topMenu"  onScroll="Scroll.reSize()" style="vertical-align:bottom; position:relative;overflow-x:hidden;">';
  menustr = menustr + '<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" ><tr><td style="vertical-align:bottom"><ul id="ulMenu" style="width:3000px;vertical-align: bottom;overflow-x:hidden;">';
  var menuVector = getMenuVector();
  for(var k=0;k<menuVector.length;k++)
  {    
    var menu =  menuVector[k];
	var description = menu.description;	
	var menuName = menu.name;
	if(menu.display == false)
	{
	   continue;
	}	
	menustr = menustr + "<li id='" + menu.id + "' ";
	if(k==0)
	{
	   menustr = menustr + " class='on'";
	}
	menustr = menustr + ">";
	if(menu.target && menu.enabled )
	{
	  menustr = menustr +' <a href="#" class=A ';
	  menustr = menustr +' onClick="OpenWin(\''+ menu.uri + '\');selectMenu(\'' + menu.orderId + '\');sendUserActionData(\''+ menu.id +'\');" >'+menuName +'</A>'; 
	}
	else if(menu.display ==true && menu.enabled == false)
	{
	   menustr = menustr +' <li><A href="#" id="linkmenu' + menu.orderId + '" >' + menuName + '</A></li>';
	}
	else if(menu.gray == true)
	{
	  menustr = menustr +' <A href="#" id="linkmenu' + menu.orderId + '"  onclick="top.right.location.href=\'../authori.html\';sendUserActionData(\''+ menu.id +'\');><font color="#808080">'+ menuName + '</A>';
	}
	else
	{
	  menustr = menustr +' <A  href="#" id="linkmenu' + menu.orderId + '"';
	  menustr = menustr + ' onclick="NavigatePage(\'' + menu.orderId + '\');sendUserActionData(\'' + menu.id + '\');">' + menuName + '</A>';	  
	  menuNameArrays.push(menuName);	  
	}
	menustr = menustr + '</li>'; 

  }
  menustr = menustr +'</ul></td></tr></table></div></TD><td width="30px" align="middle" style="vertical-align:middle"><img  onclick="pageRight()" id=rightpic style="cursor:hand" src="./images/menu_right.gif"  style="margin-bottom:-10px"></td></TR></TBODY></TABLE>';
  document.getElementById ('menudiv').innerHTML= menustr ;
  //计算多个li的宽度之和赋值给ul的宽度
  initUlMenuWidth();
  Scroll.reSize();
        
 // OnLoadNavigatePage(); 
 NavigateFirstMenuPage();
}

//计算多个li的宽度之和赋值给ul的宽度,就是计算多个一级菜单的市级宽度
function initUlMenuWidth()
{
    var liObjs = document.all.ulMenu.childNodes;
    var liNum = liObjs.length;
    var lisWidth = 0;
    for(var i=0; i<liNum; i++)
    {
        lisWidth = lisWidth + liObjs.item(i).offsetWidth;
    }
    document.all.ulMenu.style.width = lisWidth + 10;
}

function initTopMenuWidth()
{
    //topMenu的宽度
    document.all.topMenu.style.width = document.body.clientWidth-280;
}
var Scroll = {  
  reSize:function(){
      try{
        
        initTopMenuWidth();
                 
        var noww = getWidthNum(document.all.topMenu.style.width);
        var allw = document.all.topMenu.scrollWidth;
        var nowl = document.all.topMenu.scrollLeft;
        var nowr = allw - noww - nowl;
        
        if(nowl>0)
          document.all.leftpic.style.visibility = 'visible';
        else
          document.all.leftpic.style.visibility = 'hidden';
        if(nowr>10)
          document.all.rightpic.style.visibility = 'visible';
        else
          document.all.rightpic.style.visibility = 'hidden';
        
      }catch(E){}
  },
  reSize2:function(){
    Scroll.reSize();
    setTimeout("Scroll.reSize()",500);    
  } 
}

function getWidthNum(s){  
  return parseInt(s.substring(0,s.length-2));
}

var currentTopName;
function setCurrentTopMenu(currentMenuName)
{
	currentTopName=	currentMenuName;
}
function getCurrentMenuName()
{
	return currentTopName;
}

function getTopMenus()
{
	return menuNameArrays;
}
Scroll.reSize2();
window.attachEvent("onresize",Scroll.reSize2);

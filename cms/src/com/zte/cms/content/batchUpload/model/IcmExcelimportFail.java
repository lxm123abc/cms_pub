package com.zte.cms.content.batchUpload.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmExcelimportFail extends DynamicBaseObject
{
    private java.lang.Long importindex;
    private java.lang.Integer excelrownum;
    private java.lang.Integer failedtype;

    public java.lang.Long getImportindex()
    {
        return importindex;
    }

    public void setImportindex(java.lang.Long importindex)
    {
        this.importindex = importindex;
    }

    public java.lang.Integer getExcelrownum()
    {
        return excelrownum;
    }

    public void setExcelrownum(java.lang.Integer excelrownum)
    {
        this.excelrownum = excelrownum;
    }

    public java.lang.Integer getFailedtype()
    {
        return failedtype;
    }

    public void setFailedtype(java.lang.Integer failedtype)
    {
        this.failedtype = failedtype;
    }

    public void initRelation()
    {
        this.addRelation("importindex", "IMPORTINDEX");
        this.addRelation("excelrownum", "EXCELROWNUM");
        this.addRelation("failedtype", "FAILEDTYPE");
    }
}

package com.zte.cms.channel.wkfw.ls;

import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelWkfwCheck implements IChannelWkfwCheck
{

    private ICmsChannelDS channelds = null;
    private IPhysicalchannelDS physicalchannelDS = null;

    // 构造方法
    ChannelWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }

    /**
     * 
     * @return ICmsChannelDS
     */
    public ICmsChannelDS getChannelds()
    {
        return channelds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    /**
     * @param channelindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkChannelWkfw(Long channelindex) throws DomainServiceException
    {
        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(channelindex);
        channel = channelds.getCmsChannel(channel);

        if (channel == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS); // 不存在
        }
        // 状态不为带审核或者审核失败或者待审核,且没有发起工作流
        if ((channel.getStatus() != 110) && (channel.getStatus() != 130))
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG); // 服务状态不正确
        }
        // 如果点播内容下存在“待审核”或“审核失败”的子内容（即物理频道），才可以提交审核
        Physicalchannel physicalchannel = new Physicalchannel();
        physicalchannel.setChannelid(channel.getChannelid());
        List list = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannel);
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                Physicalchannel physical = (Physicalchannel) list.get(i);
                if (physical.getStatus() == 110 || physical.getStatus() == 130)
                {

                }
                else
                {
                    return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_PHCHANNEL_STATUS_WRONG);
                    // 服务状态不正确
                }
            }
        }
        // 已经发起工作流
        if (channel.getWorkflow() != null)
        {
            if (channel.getWorkflow() != 0)
            {
                return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_IN_ALREADY);
                // 策略已发起工作流未结束
            }
        }
        if (channel.getWorkflowlife() != null)
        {
            if (channel.getWorkflowlife() != 0)
            {
                return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG);
                // 工作流流程状态不正确
            }
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    /**
     * @param channelIndex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     * 
     */
    public String checkChannelWkfwAudit(Long channelIndex) throws DomainServiceException
    {
        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(channelIndex);
        channel = channelds.getCmsChannel(channel);

        if (channel == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS);
            // 服务不存在
        }

        // 120：审核中
        if (channel.getStatus() != 120)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG); // 服务态不正确
        }
        if (channel.getWorkflow() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_NOT_IN_WKFW_OR_WRONG_WKFW);
            // 服务不在工作流中或所处工作流不正确
        }
        if (channel.getWorkflowlife() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG);
            // 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

package com.zte.cms.content.series.service;

import java.util.List;

import com.zte.cms.content.series.model.CmsSeries;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsSeriesDS
{
    /**
     * 新增CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsSeries(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 更新CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSeries(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 批量更新CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSeriesList(List<CmsSeries> cmsSeriesList) throws DomainServiceException;

    /**
     * 删除CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSeries(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 批量删除CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSeriesList(List<CmsSeries> cmsSeriesList) throws DomainServiceException;

    /**
     * 查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public CmsSeries getCmsSeries(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 根据条件查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return 满足条件的CmsSeries对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSeries> getCmsSeriesByCond(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据模糊条件查询并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据模糊条件查询连续剧平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据模糊条件查询连续剧平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return CmsSeries对象
     * @throws DomainServiceException ds异常
     */
    public CmsSeries getCmsSeriesById(CmsSeries cmsSeries) throws DomainServiceException;
    public TableDataInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException;
    public TableDataInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException;
    public TableDataInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException;
    public TableDataInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException;
    
    /**
     * 根据seriesid查询关联角色表中的角色名和角色类型
     * 
     */
    public List<CmsSeries> getSeriesCastTypeName(CmsSeries cmsSeries) throws DomainServiceException;
    
    
    

}
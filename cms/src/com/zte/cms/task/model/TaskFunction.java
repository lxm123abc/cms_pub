package com.zte.cms.task.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class TaskFunction extends DynamicBaseObject
{
    private java.lang.Integer funcid;
    private java.lang.Integer parentid;
    private java.lang.String funcdescription;
    private java.lang.Integer stepnum;
    private java.lang.Integer opergrpid;
    
	public java.lang.Integer getFuncid() {
		return funcid;
	}
	public void setFuncid(java.lang.Integer funcid) {
		this.funcid = funcid;
	}
	public java.lang.Integer getParentid() {
		return parentid;
	}
	public void setParentid(java.lang.Integer parentid) {
		this.parentid = parentid;
	}
	public java.lang.String getFuncdescription() {
		return funcdescription;
	}
	public void setFuncdescription(java.lang.String funcdescription) {
		this.funcdescription = funcdescription;
	}
	public java.lang.Integer getStepnum() {
		return stepnum;
	}
	public void setStepnum(java.lang.Integer stepnum) {
		this.stepnum = stepnum;
	}
	public java.lang.Integer getOpergrpid() {
		return opergrpid;
	}
	public void setOpergrpid(java.lang.Integer opergrpid) {
		this.opergrpid = opergrpid;
	}
	@Override
	public void initRelation() {
		 this.addRelation("funcid", "funcid");
	}
}

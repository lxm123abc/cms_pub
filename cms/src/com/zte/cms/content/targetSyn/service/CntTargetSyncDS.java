package com.zte.cms.content.targetSyn.service;

import java.util.List;

import com.zte.cms.content.targetSyn.dao.ICntTargetSyncDAO;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CntTargetSyncDS extends DynamicObjectBaseDS implements ICntTargetSyncDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
  	private ICntTargetSyncDAO dao = null;
	
	public void setDao(ICntTargetSyncDAO dao)
	{
	    this.dao = dao;
	}

	public void insertCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug("insert cntTargetSync starting...");
		Long cntTargetSyncindex = null;
		try
		{
			if(cntTargetSync.getSyncindex() == null)
			{
			    cntTargetSyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_target_sync_index");
			    cntTargetSync.setSyncindex(cntTargetSyncindex);
			}
		    dao.insertCntTargetSync( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert cntTargetSync end");
	}
	
    public void updateCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug("update cntTargetSync by pk starting...");
	    try
		{
			dao.updateCntTargetSync( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntTargetSync by pk end");
	}

	public void updateCntTargetSyncList( List<CntTargetSync> cntTargetSyncList )throws DomainServiceException
	{
		log.debug("update cntTargetSyncList by pk starting...");
		if(null == cntTargetSyncList || cntTargetSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntTargetSyncList");
			return;
		}
	    try
		{
			for( CntTargetSync cntTargetSync : cntTargetSyncList )
			{
		    	dao.updateCntTargetSync( cntTargetSync );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cntTargetSyncList by pk end");
	}



	public void removeCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug( "remove cntTargetSync by pk starting..." );
		try
		{
			dao.deleteCntTargetSync( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntTargetSync by pk end" );
	}

	public void removeCntTargetSyncList ( List<CntTargetSync> cntTargetSyncList )throws DomainServiceException
	{
		log.debug( "remove cntTargetSyncList by pk starting..." );
		if(null == cntTargetSyncList || cntTargetSyncList.size() == 0 )
		{
			log.debug("there is no datas in cntTargetSyncList");
			return;
		}
		try
		{
			for( CntTargetSync cntTargetSync : cntTargetSyncList )
			{
				dao.deleteCntTargetSync( cntTargetSync );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cntTargetSyncList by pk end" );
	}



	public CntTargetSync getCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug( "get cntTargetSync by pk starting..." );
		CntTargetSync rsObj = null;
		try
		{
			rsObj = dao.getCntTargetSync( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetSyncList by pk end" );
		return rsObj;
	}

	public List<CntTargetSync> getCntTargetSyncByCond( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug( "get cntTargetSync by condition starting..." );
		List<CntTargetSync> rsList = null;
		try
		{
			rsList = dao.getCntTargetSyncByCond( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetSync by condition end" );
		return rsList;
	}
	public List<CntTargetSync> getcntSynnormaltargetListBycond( CntTargetSync cntTargetSync )throws DomainServiceException
	{
		log.debug( "get cntTargetSync by condition starting..." );
		List<CntTargetSync> rsList = null;
		try
		{
			rsList = dao.getcntSynnormaltargetListBycond( cntTargetSync );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetSync by condition end" );
		return rsList;
	}	
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntTargetSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntTargetSync, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	public void removeCntTargetSynListByObjindex( List<CntTargetSync> cntTargetSynList )throws DomainServiceException
	{
	    log.debug( "remove cntTargetSynList by condition starting..." );
        if(null == cntTargetSynList || cntTargetSynList.size() == 0 )
        {
            log.debug("there is no datas in cntTargetSynList");
            return;
        }
        try
        {
            for( CntTargetSync cntTargetSync : cntTargetSynList )
            {
                dao.deleteCntTargetSyncByObjindex( cntTargetSync );
            }
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "remove cntTargetSynList by condition end" );
	}

	/**
	 * 肖梁军 2012.10.25编辑
	 */
    public int countCntTargetSync4Object(CntTargetSync cntTargetSync) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get exitCntTargetSync number info by condition starting..." );
        int i = 0;
        try
        {
            i=dao.countCntTargetSync4Object(cntTargetSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get exitCntTargetSync number info by condition end" );
        return i;
    }

    public List<CntTargetSync> getCommonTarget4ServiceAndProgram(CntTargetSync cntTargetSync)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get common target for service and program  by condition starting..." );
        List<CntTargetSync> rsList = null;
        try
        {
            rsList = dao.getCommonTarget4ServiceAndProgram(cntTargetSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get common target for service and program  by condition end" );
        return rsList;
    }    
    public List<CntTargetSync> getCommonTarget4ServiceAndSeries(CntTargetSync cntTargetSync)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get common target for service and Series  by condition starting..." );
        List<CntTargetSync> rsList = null;
        try
        {
            rsList = dao.getCommonTarget4ServiceAndSeries(cntTargetSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get common target for service and Series  by condition end" );
        return rsList;
    }    
    public List<CntTargetSync> getCommonTarget4ServiceAndChannel(CntTargetSync cntTargetSync)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get common target for service and Channel  by condition starting..." );
        List<CntTargetSync> rsList = null;
        try
        {
            rsList = dao.getCommonTarget4ServiceAndChannel(cntTargetSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get common target for service and Channel  by condition end" );
        return rsList;
    }    
    
}

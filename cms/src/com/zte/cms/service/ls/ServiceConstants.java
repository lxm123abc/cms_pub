package com.zte.cms.service.ls;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;

public class ServiceConstants
{
    // 资源文件
    // 文件名称
    public static final String RESOURCE_UCDN_SERVICE = "ucdn_service_resource";
    // 要删除的服务不存在或者已经被删除
    // 要删除的服务存在打包关系
    public static final String RES_UCDN_SERV_DEL_HAS_BIND_CNT = "res.ucdn.serv.del.has.bind.cnt";
    public static final String RESOURCE_UCDN_SERVICE_4DEL_DOESNOT_EXIST = "ucdn.service.4del.doesnot.exist";
    // 要修改的服务不存在或者已经被删除
    public static final String RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST = "ucdn.service.4update.doesnot.exist";
    // 要修改的服务已经被修改或者没有进行修改
    public static final String RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_CHANGE = "ucdn.service.4update.doesnot.change";
    // 新增服务成功
    public static final String RESOURCE_UCDN_SERVICE_ADD_SUCCESSFUL = "ucdn.service.service.add.successful";
    // 删除服务成功
    public static final String RESOURCE_UCDN_SERVICE_DEL_SUCCESSFUL = "ucdn.service.service.del.successful";
    // 修改服务成功
    public static final String RESOURCE_UCDN_SERVICE_UPDATE_SUCCESSFUL = "ucdn.service.service.update.successful";
    // 已经有相同的服务名称存在
    public static final String RESOURCE_UCDN_SERVICE_NAME_EXISTS = "ucdn.service.service.name.exists";
    // 已经有相同的服务ID存在
    public static final String RESOURCE_UCDN_SERVICE_ID_EXISTS = "ucdn.service.service.id.exists";
    // 服务不存在

    public static final String RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS = "ucdn.service.wkfw.no.exists";
    // 服务的状态不正确
    public static final String RESOURCE_UCDN_SERVICE_STATUS_WRONG = "ucdn.service.status.wrong";
    // 服务已经在审核中
    public static final String RESOURCE_UCDN_SERVICE_WKFW_IN_ALREADY = "ucdn.service.wkfw.in.already";
    // 服务工作流状态不正确
    public static final String RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG = "ucdn.service.wkfw.status.wrong";
    // 校验成功，可以操作
    public static final String RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK = "ucdn.service.wkfw.check.ok";
    // 服务不在工作流中或者所处工作流不正确
    public static final String RESOURCE_UCDN_SERVICE_NOT_IN_WKFW_OR_WRONG_WKFW = "ucdn.service.not.in.wkfw.or.wrong.wkfw";

    // 服务新增审核提交成功
    public static final String RES_CMS_SERVICE_WKFW_ADD_APPLY_SUCCESS = "cms.service.msg.wkfw.add.apply.success";

    // 服务新增审核成功
    public static final String RES_CMS_SERVICE_WKFW_ADD_AUDIT_SUCCESS = "cms.service.msg.wkfw.add.audit.success";
    /* 工作流类型相关 */
    public static final String RES_CMS_STRA_WKFWTYPENAME_SERVICE_ADD = "cms.service.workflowtype.name.service.add";// 服务新增

    // 操作成功
    public static final String RES_CMS_SERVICE_OPER_SUCCESS = "res.ucdn.service.oper.success";
    // 操作失败
    public static final String RES_CMS_SERVICE_OPER_FAIL = "res.ucdn.service.oper.fail";
    // 服务的状态不正确，不能发布
    public static final String RESOURCE_UCDN_SERVICE_PUB_WRONG_STATUS = "res.ucdn.service.pub.wrong.status";
    // 没有服务被选中
    public static final String RESOURCE_UCDN_SERVICE_PUB_NO_SERVICE_CHOSEN = "res.ucdn.service.pub.no.service.chosen";
    // BMS目标系统不存在
    public static final String RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM = "res.ucdn.service.pub.no.bms.system";
    // EPG目标系统不存在
    public static final String RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM = "res.ucdn.service.pub.no.epg.system";
    // 新增发布成功
    public static final String RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS = "res.ucdn.service.pub.successful";
    // 取消发布成功
    public static final String RESOURCE_UCDN_SERVICE_PUBLISH_DEL_SUCCESS = "res.ucdn.service.pub.del.successful";
    // 获取临时区地址失败
    public static final String RES_UCDN_PUB_GET_TEMP_AREA_FAILED = "res.ucdn.pub.get.temp.are.failed";

    // 获取同步XML文件FTP地址失败
    public static final String RES_UCDN_PUB_GET_FTP_ADDR_FAILED_FOR_XML_FILE = "res.ucdn.pub.get.ftp.addr.failed.for.xml.file";
    // 失败数量
    public static final String RES_UCDN_SERVICE_FAILED_NUM = "res.ucdn.service.failed.num";
    // 成功数量
    public static final String RES_UCDN_SERVICE_SUCC_NUM = "res.ucdn.service.succ.num";
    // 请选择
    public static final String RES_CMS_SERVICE_OPTION_PLEASESELECT = "res.ucdn.service.option.pleaseselect";
    
    /************************************************服务和发布平台绑定关系starts************************************/
    //操作成功
    public static final String RES_UCDN_SERVICE_OPERATE_SUCCESS = "res.ucdn.service.operate.success";
    //成功删除关联的目标系统平台  bind targetsystem delete successfully
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_SUCC = "res.ucdn.service.bind.target.del.succ";
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_OPERATE_SUCC = "res.ucdn.service.bind.target.operate.succ";
    // 服务在网元上存在发布的关联内容，不能删除网元    
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_CNT_PUBLISH = "res.ucdn.service.bind.target.del.cnt.publish";
    // 有与VOD内容绑定关系在此平台发布，不能删除网元    
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_PROGRAM_PUBLISH = "res.ucdn.service.bind.target.del.program.publish";
    // 有与直播内容绑定关系在此平台发布，不能删除网元    
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_CHANNEL_PUBLISH = "res.ucdn.service.bind.target.del.channel.publish";
    // 有与连续剧内容绑定关系在此平台发布，不能删除网元    
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_SERIES_PUBLISH = "res.ucdn.service.bind.target.del.series.publish";
    //网元不存在   targetsystem does not exist
    public static final String RESOURCE_UCDN_SERVICE_BIND_NO_TARGET_CHOSEN = "res.ucdn.service.bind.no.target.chosen";
    //网元关联关系不存在 targetsystem bind does not exist
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_NO_EXISTS = "res.ucdn.service.bind.target.no.exists";
    //添加关联平台成功 add targetsystem binds 
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_SUCC = "res.ucdn.service.bind.target.add.succ";
    //添加关联平台失败
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_FAILED = "res.ucdn.service.bind.target.add.failed";
    //当前服务没有添加新的关联平台
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_NO_TARGET_ADD = "res.ucdn.service.bind.target.no.target.add";
    //当前服务状态不可以进行添加关联平台操作
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_WRONG_STATUS = "res.ucdn.service.bind.target.wrong.status";
    //批量添加关联平台成功   
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_ADDLIST_SUCC = "res.ucdn.service.bind.target.list.add.succ";    
    //批量添加关联平台失败
    public static final String RESOURCE_UCDN_SERVICE_BIND_TARGET_ADDLIST_FAILED = "res.ucdn.service.bind.target.list.add.failed";
    // 要修改的服务不存在或者已经被删除
    public static final String RESOURCE_UCDN_SERVICE_BIND_DOESNOT_EXIST = "res.ucdn.service.bind.doesnot.exist";
          
    /************************************************服务和发布平台绑定关系ends************************************/

    /** *************服务绑定内容模块************************************************************ */
    // 没有内容被选中
    public static final String RESOURCE_UCDN_SERVICE_BIND_NO_CONTENT_CHOSEN = "res.ucdn.service.bind.no.cnt.chosen";
    // 内容不存在
    public static final String RESOURCE_UCDN_SERVICE_CNT_NO_EXISTS = "res.ucdn.serv.cnt.no.exists";
    // 内容不存在
    public static final String RESOURCE_UCDN_SERVICE_CNT_BIND_EXISTS = "res.ucdn.serv.cnt.bind.exists";
    //服务和内容没有公共网元可以关联
    public static final String RESOURCE_UCDN_SERVICE_TARGET_NO_COMMON_EXISTS ="res.ucdn.service.cnt.no.target.common";
    // 绑定内容成功
    public static final String RESOURCE_UCDN_SERVICE_BIND_CNT_SUCC = "res.ucdn.serv.bind.cnt.succ";
    // 内容绑定删除成功
    public static final String RESOURCE_UCDN_SERVICE_BIND_CNT_DEL_SUCC = "res.ucdn.serv.bind.cnt.del.succ";
    // 内容类型有误
    public static final String RESOURCE_UCDN_SERVICE_WRONG_CNT_TYPE = "res.ucdn.serv.wrong.cnt.type";
    // 没有绑定关系被选中
    public static final String RESOURCE_UCDN_SERVICE_BIND_NO_MAP_CHOSEN_TO_DEL = "res.ucdn.serv.bind.no.map.choosen.to.del";
    // 绑定关系不存在
    public static final String RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS = "res.ucdn.serv.bind.map.no.exists";

    /** ****************************服务内容绑定发布功能******************************************** */
    // 没有绑定关系被选中需要发布
    public static final String RES_UCDN_SERV_CNT_BIND_PUB_NO_MAP_CHOSEN = "res.ucdn.serv.cnt.bind.pub.no.map.chosen";
    // 绑定关系不存在
    public static final String RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS = "res.ucdn.serv.cnt.bind.pub.map.no.exists";
    // 绑定关系当前状态不正确
    public static final String RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG = "res.ucdn.serv.cnt.bind.pub.map.status.wrong";
    //该绑定关系已经发布，不可以删除
    public static final String RES_UCDN_SERV_CNT_BIND_MAP_PUB = "res.ucdn.serv.cnt.bind.map.pub";
    // 取消发布成功
    public static final String RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC = "res.ucdn.serv.cnt.bind.pub.del.succ";
    /** ************************************************************************************************** */
    // 状态必须为待发布或者取消发布成功才可以删除
    public static final String RES_UCDN_SERV_BIND_MAP_PUB_DEL_WRONG_STATUS = "res.ucdn.serv.bind.map.pub.del.wrong.status";
    // 服务修改审核提交成功
    public static final String RES_CMS_SERVICE_WKFW_UDPATE_APPLY_SUCCESS = "cms.service.msg.wkfw.update.apply.success";
    // BUF表中数据不存在
    public static final String RESOURCE_UCDN_SERVICE_WKFW_BUF_NO_EXISTS = "res.ucdn.serv.update.wkfw.buf.no.exists";
    // 服务修改审核成功
    public static final String RES_CMS_SERVICE_WKFW_UPDATE_AUDIT_SUCCESS = "res.ucdn.serv.wkfw.update.autid.successful";
    // 服务状态不正确，不能修改
    public static final String RES_UCDN_SERV_UPDATE_WRONG_STATUS = "res.ucdn.serv.update.wrong.status";
    /* 工作流类型相关 */
    // 服务修改
    public static final String RES_UCDN_SERV_WKFWTYPENAME_SERV_UPDATE = "res.ucdn.serv.wkrwtypename.update";
    /** *************服务绑定内容模块ends************************************************************ */
    // 服务目前的状态不允许删除
    public static final String RES_UCDN_SERV_4DEL_WRONG_STATUS = "res.ucdn.serv.4del.wrong.status";
    // 该服务与海报有关联关系，不允许删除
    public static final String RES_UCDN_SERV_4DEL_BIND_WITH_PIC = "res.ucdn.serv.4del.bind.with.pic";
    /** ******************************************************************************************** */
    // 服务存在关联内容，不能取消发布
    public static final String RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CNT = "res.ucdn.serv.pub.del.bind.cnt";
    // 该服务关联的海报已经发布，不能对该服务取消发布
    public static final String RES_UCDN_SERV_DEL_PUB_PIC_BIND_PUBLISHED = "res.ucdn.serv.del.pub.pic.bind.published";
    
    // 服务存在关联栏目，不能取消发布
    public static final String RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CATEGORY = "res.ucdn.serv.pub.del.bind.category";
    /** ********************************************************************************************* */
    // 服务处于发布状态，不能删除 
    public static final String RESOURCE_UCDN_SERVICE_DEL_WRONG_PUB_STATUS = "res.ucdn.service.del.wrong.pub.status";
 // 服务已经在该平台发布
    public static final String RESOURCE_UCDN_SERVICE_DEL_TARGET_WRONG_PUB_STATUS = "res.ucdn.service.del.target.wrong.pub.status";
    /**************************************************************************************************/
    
    /************************************************服务和栏目绑定关系starts************************************/
     //要绑定的服务不存在或者已经被删除
    public static final String RES_UCDN_SERV_CATEGORY_MAP_SERV_NO_EXISTS = "res.ucdn.serv.category.map.serv.no.exists";
    //要绑定的栏目不存在或者已经被删除
    public static final String RES_UCDN_SERV_CATEGORY_MAP_CATE_NO_EXISTS = "res.ucdn.serv.category.map.cate.no.exists";
    //服务的状态不是发布成功，不可关联栏目
    public static final String RES_UCDN_SERV_STATUS_NOT_PUBLISHED = "res.ucdn.serv.status.not.published";
    //栏目的状态不是发布成功，不可关联服务
    public static final String RES_UCDN_SERV_CATE_STATUS_NOT_PUBLISHED = "res.ucdn.serv.cate.status.not.published";
    //服务关联栏目成功
    public static final String RES_UCDN_SERV_CATE_MAP_SUCCESSFUL = "res.ucdn.serv.cate.map.succ";
    //服务栏目关联关系删除成功
    public static final String RES_UCDN_SERV_CATE_MAP_DEL_SUCC = "res.ucdn.serv.cate.map.del.succ";
    
    /************************************************服务和栏目绑定关系ends************************************/
    public static final int WKFWTYPECODE_SERVICE_ADD = 1;// 服务新增
    public static final int WKFWTYPECODE_SERVICE_UPDATE = 2;// 服务修改

    public static final String WKFWTYPE_SERVICE_ALL = "CMS_SERVICE_%";
    public static final String WKFWTYPE_SERVICE_ADD = "CMS_SERVICE_A";// 新增
    public static final String WKFWTYPE_SERVICE_UPDATE = "CMS_SERVICE_U";// 修改

    public static String getWorkflowType(int workflowTypeCode)
    {
        switch (workflowTypeCode)
        {
            case WKFWTYPECODE_SERVICE_ADD:
                return WKFWTYPE_SERVICE_ADD;
            case WKFWTYPECODE_SERVICE_UPDATE:
                return WKFWTYPE_SERVICE_UPDATE;
            default:
                return null;
        }
    }

    public static String getWorkflowTypeName(int workflowTypeCode)
    {
        ResourceMgt.addDefaultResourceBundle(RESOURCE_UCDN_SERVICE);
        switch (workflowTypeCode)
        {
            case WKFWTYPECODE_SERVICE_ADD:
                return ResourceMgt.findDefaultText(RES_CMS_STRA_WKFWTYPENAME_SERVICE_ADD);
            case WKFWTYPECODE_SERVICE_UPDATE:
                return ResourceMgt.findDefaultText(RES_UCDN_SERV_WKFWTYPENAME_SERV_UPDATE);
            default:
                return null;
        }
    }

    public static String getWorkflowTypeName(String workflowType)
    {
        ResourceMgt.addDefaultResourceBundle(RESOURCE_UCDN_SERVICE);
        if (WKFWTYPE_SERVICE_ADD.equals(workflowType))
        {
            return ResourceMgt.findDefaultText(RES_CMS_STRA_WKFWTYPENAME_SERVICE_ADD);
        }
        else if (WKFWTYPE_SERVICE_UPDATE.equals(workflowType))
        {
            return ResourceMgt.findDefaultText(RES_UCDN_SERV_WKFWTYPENAME_SERV_UPDATE);
        }
        return null;
    }

    public static String getUserid()
    {
        UserInfo ui = LoginMgt.getUserInfo();
        return ui == null ? "" : ui.getUserId();
    }

    /**
     * 得到14的 字符串日期
     */
    public static String getCurrentTimeString()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        return sdf.format(date);
    }

}

// @version 3.00.04

var _PIEBASIC_TAG="piechart";
var _ZPIEBASIC_TAG="z:piechart";
SSB_PIE.pieObjs={};


/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
 var pieObj = {};
function Pies_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_PIEBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZPIEBASIC_TAG);
	}
	if(tags && tags.length > 0) {	
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			pieObj = new SSB_PIE(id);
			pieObj.init();					
		}
	}
}
addOnloadEvent(Pies_init);

/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_PIE(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mattribute= document.getElementById(this.id).getAttribute("attribute");
}

SSB_PIE.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
          if(this.mattribute==null||this.mattribute=="")
        {
			this.mattribute="divshow:true; divper:false;divvalue:false;divtitle:true;pieshow:true; pieper:true;pievalue:false;pietitle:false";
        }
        
		 if(this.mdivid != null&&this.mdivid != "")
		 {
		  if(this.id==this.mdivid)
		 {
			this.mdivid="pie"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		}
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    pieObj.setValue(this);
	    }
	    
	    SSB_PIE.pieObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	
	
	setValue:function(init) {
	
	var options, placeholder,option,datas;
   
    findData(eval(init.mdatasrc));
   
    placeholder = $("#"+init.mdivid);
    
    var fina="{ pies:{show: true},legend:{"+ legendgroup(init.mlegend)+"},attribute:{"+attributegroup(init.mattribute)+"}}";
    
    option=eval("["+fina+"]");

	options = option[0];

	datas=eval("["+totaldata+"]");
	
    $.plot(placeholder,datas,options); 

	
  }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

attributegroup = function (attribute){

		var group=attribute.split(";");
		var attributestring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					attributestring +=group[i];
				}
				else
				{
					attributestring +=group[i]+',';
				}
			}
		}

		return attributestring;
};


var totaldata="";

findData =function(initvalue)
{
		totaldata="";

		
		for(var i=0;i<initvalue.length;i++)
		{
			var info="";
			
			if(i == initvalue.length-1)
			
			info="{label:'"+initvalue[i].label+"',value:["+i+","+initvalue[i].value+"],color:'"+initvalue[i].color+"'}";
			
			else
			
			info="{label:'"+initvalue[i].label+"',value:["+i+","+initvalue[i].value+"],color:'"+initvalue[i].color+"'},";
			
			totaldata+=info;
		}
}
	
	//\u8c03\u670d\u52a1
setpiesValue = function(ids,initvalue) {
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="pie"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
	
	var mattribute= document.getElementById(ids).getAttribute("attribute");

	var options, placeholder,option,datas;
   
    findData(eval(initvalue));
   
    placeholder = $("#"+mdivid);
    
     var fina="{ pies:{show: true},legend:{"+ legendgroup(mlegend)+"},attribute:{"+attributegroup(mattribute)+"}}";
    option=eval("["+fina+"]");

	options = option[0];

	datas=eval("["+totaldata+"]");
	
    $.plot(placeholder,datas,options);
   
	};
	
	

var _LINEBASIC_TAG="lineschart";
var _ZLINEBASIC_TAG="z:lineschart";
SSB_LINE.lineObjs={};

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
 var lineObj = {};
function Lines_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_LINEBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZLINEBASIC_TAG);
	}
	if(tags && tags.length > 0) {	
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			lineObj = new SSB_LINE(id);
			lineObj.init();					
		}
	}
}
addOnloadEvent(Lines_init);


var placeholder,datas,options;
/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_LINE(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mxaxis= document.getElementById(this.id).getAttribute("xaxis");
    this.myaxis= document.getElementById(this.id).getAttribute("yaxis");
    this.mgrid= document.getElementById(this.id).getAttribute("grid");
}

SSB_LINE.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
		 if(this.mxaxis==null||this.mxaxis=="")
        {
			this.mxaxis="tickDecimals: 0";
        }
   
		 if(this.myaxis==null||this.myaxis=="")
        {
			this.myaxis="min: 0";
        }
        
		 if(this.mgrid==null||this.mgrid=="")
        {
			this.mgrid="color:'#000000'";
        }
        
		 if(this.mdivid != null&&this.mdivid != "")
		 {
		  if(this.id==this.mdivid)
		 {
			this.mdivid="line"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    lineObj.setValue(this);
	    }
	    
	    SSB_LINE.lineObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	setValue:function(init) {	
	var options, placeholder,datas,option;
	
	 	//\u6570\u636e\u6e90
		datas =eval(init.mdatasrc);
		//\u663e\u793a\u6837\u5f0f
		var fina="{lines:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroup(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
 }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

xaxisgroup = function (xaxis){

		var group=xaxis.split(";");
		var xaxisstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					xaxisstring +=group[i];
				}
				else
				{
					xaxisstring +=group[i]+',';
				}
			}
		}

		return xaxisstring;
};

gridgroup = function (grid){

		var group=grid.split(";");
		var gridstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					gridstring +=group[i];
				}
				else
				{
					gridstring +=group[i]+',';
				}
			}
		}

		return gridstring;
};

yaxisgroup = function (yaxis){

		var group=yaxis.split(";");
		var yaxisstring="";
		for(var i=0;i<group.length;i++)
		{
			if(i==group.length-1)
			{
				yaxisstring +=group[i];
			}
			else
			{
				yaxisstring +=group[i]+',';
			}
		}
		return yaxisstring;
};

	
	//\u8c03\u670d\u52a1
setlinesValue = function(ids,initvalue) {
	var options, placeholder,datas,option;
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="line"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
    var mxaxis= document.getElementById(ids).getAttribute("xaxis");
    var myaxis= document.getElementById(ids).getAttribute("yaxis");
    var mgrid= document.getElementById(ids).getAttribute("grid");

	 	//\u6570\u636e\u6e90
		datas =eval(initvalue);
		//\u663e\u793a\u6837\u5f0f
		var fina="{lines:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroup(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
   
	};
	
	

var _BARBASIC_TAG="barchart";
var _ZBARBASIC_TAG="z:barchart";
SSB_BAR.barObjs={};


//\u51b3\u5b9a\u521d\u59cb\u5316\u88c5\u8f7dria \uff0cinit \u521d\u59cb\u5316 \uff0c\u7528\u6237\u521d\u59cb\u5316\uff0c

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */

function Bars_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_BARBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZBARBASIC_TAG);
	}
	if(tags && tags.length > 0) {
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			barObj = new SSB_BAR(id);
			barObj.init();		
			
		}
	}
}
addOnloadEvent(Bars_init);

/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_BAR(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mxaxis= document.getElementById(this.id).getAttribute("xaxis");
    this.myaxis= document.getElementById(this.id).getAttribute("yaxis");
    this.mgrid= document.getElementById(this.id).getAttribute("grid");
    this.misgroup= document.getElementById(this.id).getAttribute("isgroup");

}

SSB_BAR.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }

		 if(this.misgroup==null||this.misgroup=="")
        {
			this.misgroup="false";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
		 if(this.mxaxis==null||this.mxaxis=="")
        {
			this.mxaxis="tickDecimals: 0";
        }
   
		 if(this.myaxis==null||this.myaxis=="")
        {
			this.myaxis="min: 0";
        }
        
		 if(this.mgrid==null||this.mgrid=="")
        {
			this.mgrid="color:'#000000'";
        }

		 if(this.mdivid != null&&this.mdivid != "")
		 {
		 if(this.id==this.mdivid)
		 {
			this.mdivid="bar"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div><div id="'+this.mdivid+'2'+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    barObj.setValue(this);
	    }
	    
	   // SSB_BAR.barObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	setValue:function(init) {
	
	var options, placeholder,datas,option;
	
	if(init.misgroup=="true"){
		ticks="";
		isGroup(eval(init.mdatasrc));
	 	//\u6570\u636e\u6e90
		datas =eval("["+totals+"]");
		
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroupbar(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
	}
	else
	{
		ticks="";
	 	//\u6570\u636e\u6e90
		datas =eval(init.mdatasrc);
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroupbar(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
   }
 }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

xaxisgroupbar = function (xaxis){
		
		var group=xaxis.split(";");
		var xaxisstring="";
		if(group.length!=0)
		{
			if(xaxis.search("ticks")!=-1)
			{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
				
					if(ticks!=null&&ticks!="")
					{
						if(group[i].search("ticks") != -1)
						{
							group[i]="ticks:["+ticks+"]";
						}
					}
					xaxisstring +=group[i];
				}
				else
				{
					if(ticks!=null&&ticks!="")
					{
						if(group[i].search("ticks") != -1)
						{
							group[i]="ticks:["+ticks+"]";
						}
					}
					xaxisstring +=group[i]+',';
				}
			}
			}
			else
			{
				for(var i=0;i<group.length;i++)
				{	
					xaxisstring +=group[i]+',';	
				}
				xaxisstring += "ticks:["+ticks+"]";
			}
		}

		return xaxisstring;
};

gridgroup = function (grid){

		var group=grid.split(";");
		var gridstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					gridstring +=group[i];
				}
				else
				{
					gridstring +=group[i]+',';
				}
			}
		}

		return gridstring;
};

yaxisgroup = function (yaxis){

		var group=yaxis.split(";");
		var yaxisstring="";
		for(var i=0;i<group.length;i++)
		{
			if(i==group.length-1)
			{
				yaxisstring +=group[i];
			}
			else
			{
				yaxisstring +=group[i]+',';
			}
		}
		return yaxisstring;
};

	//\u6574\u5408\u7684\u6570\u636e
	var totals="";
	//X\u8f74\u7684\u663e\u793a\u6587\u5b57
	var ticks="";
	
	//\u5206\u7ec4\u65b9\u6cd5
isGroup = function(inputs){
		totals="";
		ticks="";
		var count=1;
		
		var retrn=inputs[0].datas;
		
		var xtick=inputs[0].xyticks;
		
		var leng=retrn.length;
		
		for(var i=0;i<leng;i++)
		{
			var info="";
			
			info="{label:'"+retrn[i].label+"',data:[";
			
			var resul="";
			
			for(var j=0;j<leng;j++)
			{		
			var ints=count+(leng+1)*j;
			
			//\u5904\u7406X\u8f74\u7684\u663e\u793a
			if(i==j)
			{
			
			var tickint=(leng+1)*j+(leng/2+1);
			
				if(i == leng-1)
				ticks +="["+tickint+",'"+xtick[i]+"']";
				else
				ticks +="["+tickint+",'"+xtick[i]+"'],";
			}
			
			//\u5904\u7406\u6570\u636e\u5bfc\u51fa\u7ed3\u679c
			if(j == leng-1)
			resul += "["+ints+","+retrn[i].data[j]+"]";
			else
			resul += "["+ints+","+retrn[i].data[j]+"],";
			}
			
			if(i==leng-1)
			info=info+resul+"]}";
			else
			info=info+resul+"]},";

			totals+=info;
			count++;
		}
	};
	
	//\u8c03\u670d\u52a1
setbarsValue = function(ids,initvalue) {
	var options, placeholder,datas,option;
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="bar"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
    var mxaxis= document.getElementById(ids).getAttribute("xaxis");
    var myaxis= document.getElementById(ids).getAttribute("yaxis");
    var mgrid= document.getElementById(ids).getAttribute("grid");
    var misgroup= document.getElementById(ids).getAttribute("isgroup");
   if(misgroup=="true"){
		ticks="";
		isGroup(eval(initvalue));
	 	//\u6570\u636e\u6e90
		datas =eval("["+totals+"]");
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroupbar(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
    
	    option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
	}
	else
	{
		 ticks="";
	 	//\u6570\u636e\u6e90
		datas =eval(initvalue);
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroupbar(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
	 }
   
	};
	

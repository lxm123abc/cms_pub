package com.zte.cms.cpsp.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsLpopNoaudit extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.String lpid;
    private java.lang.Integer lptype;
    private java.lang.String cpid;
    private java.lang.String cpname;
    private java.lang.String operid;


    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.String getLpid()
    {
        return lpid;
    }

    public void setLpid(java.lang.String lpid)
    {
        this.lpid = lpid;
    }

    public java.lang.Integer getLptype()
    {
        return lptype;
    }

    public void setLptype(java.lang.Integer lptype)
    {
        this.lptype = lptype;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpname()
    {
        return cpname;
    }

    public void setCpname(java.lang.String cpname)
    {
        this.cpname = cpname;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "mapindex");
        this.addRelation("lpid", "LPID");
        this.addRelation("lptype", "LPTYPE");
        this.addRelation("cpid", "CPID");
        this.addRelation("cpname", "CPNAME");
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }
}

package com.zte.cms.impSyncTask.ls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.zte.cms.common.DateUtil2;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.cms.impSyncTask.service.IImpCmsSynctaskDS;
import com.zte.cms.impSyncTask.service.IObjectCpcntRecordDS;
import com.zte.ismp.common.util.DbUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.EspecialCharMgt;

public class ObjectCpcntRecordLS implements IObjectCpcntRecordLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IObjectCpcntRecordDS cpcntDS = null;

    private IImpCmsSynctaskDS impsyncDS = null;

    public void setCpcntDS(IObjectCpcntRecordDS cpcntDS)
    {
        this.cpcntDS = cpcntDS;
    }

    public void setImpsyncDS(IImpCmsSynctaskDS impsyncDS)
    {
        this.impsyncDS = impsyncDS;
    }

    public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize) throws Exception
    {
        log.debug("get objectCpcntRecord page info by condition starting...");

     // 进行时间格式的转换,将时间中的空格 冒号 等去掉      
        objectCpcntRecord.setStarttime1(DateUtil2.get14Time(objectCpcntRecord.getStarttime1()));
        objectCpcntRecord.setStarttime2(DateUtil2.get14Time(objectCpcntRecord.getStarttime2()));
    
      //对特殊字符的处理
        if(objectCpcntRecord.getObjectcode()!=null && !"".equals(objectCpcntRecord.getObjectcode())){
            objectCpcntRecord.setObjectcode(EspecialCharMgt.conversion(objectCpcntRecord.getObjectcode()));
        }
        if(objectCpcntRecord.getParentcode()!=null && !"".equals(objectCpcntRecord.getParentcode())){
            objectCpcntRecord.setParentcode(EspecialCharMgt.conversion(objectCpcntRecord.getParentcode()));
        }

        // 将页面传入的taskindex值转换为对应的correlateid
//        String taskindex = objectCpcntRecord.getCorrelateid();
//        if (taskindex != null && !taskindex.equals(""))
//        {
//            ImpCmsSynctask synctask = new ImpCmsSynctask();
//            String correlateid = "";
//            
//            synctask.setTaskindex(Long.parseLong(taskindex));
//            synctask = impsyncDS.getImpCmsSynctask(synctask);
//            
//            if(synctask != null)
//            {
//                correlateid = synctask.getCorrelateid();
//                objectCpcntRecord.setCorrelateid(correlateid);
//            }
//        }
        objectCpcntRecord.setOrderCond("taskindex desc, starttime desc");

        TableDataInfo tableInfo = null;

        try
        {
            tableInfo = cpcntDS.pageInfoQuery(objectCpcntRecord, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("get objectCpcntRecord by condition exception:", e);
            throw e;
        }

        log.debug("get objectCpcntRecord page info by condition end");
        return tableInfo;
    }

    public HashMap<String, String> getTaskMap() throws Exception
    {
        log.debug("get taskMap starting...");

        HashMap<String, String> map = new HashMap<String, String>();
        DbUtil dbutil = new DbUtil();

        try
        {
            String sqlstr = "select taskindex,correlateid from "
                    + "( select a.taskindex,a.correlateid from zxdbm_cms.imp_cms_synctask a "
                    + "union select b.taskindex,b.correlateid from zxdbm_cms.imp_cms_synctask_his b)";
            List reslist = dbutil.getQuery(sqlstr);

            if (reslist != null && reslist.size() > 0)
            {
                Iterator it = reslist.iterator();
                while (it.hasNext())
                {
                    HashMap mymap = (HashMap) it.next();
                    map.put((String) mymap.get("correlateid"), (String) mymap.get("taskindex"));
                }
            }
        }
        catch (Exception e)
        {
            log.error("get taskMap Exception: ", e);
        }
        log.debug("get taskMap end");
        return map;
    }

    public TableDataInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)
            throws Exception
    {
        log.debug("get objectCpcntRecord page info by condition starting...");
        
        // 进行时间格式的转换,将时间中的空格 冒号 等去掉      
            objectCpcntRecord.setStarttime1(DateUtil2.get14Time(objectCpcntRecord.getStarttime1()));
            objectCpcntRecord.setStarttime2(DateUtil2.get14Time(objectCpcntRecord.getStarttime2()));
        
      //对特殊字符的处理
        if(objectCpcntRecord.getObjectcode()!=null && !"".equals(objectCpcntRecord.getObjectcode())){
            objectCpcntRecord.setObjectcode(EspecialCharMgt.conversion(objectCpcntRecord.getObjectcode()));
        }
        if(objectCpcntRecord.getParentcode()!=null && !"".equals(objectCpcntRecord.getParentcode())){
            objectCpcntRecord.setParentcode(EspecialCharMgt.conversion(objectCpcntRecord.getParentcode()));
        }
        objectCpcntRecord.setOrderCond("taskindex desc, starttime desc");

        TableDataInfo tableInfo = null;

        try
        {
            tableInfo = cpcntDS.getObjectCpcntRecord(objectCpcntRecord, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("get objectCpcntRecord by condition exception:", e);
            throw e;
        }

        log.debug("get objectCpcntRecord page info by condition end");
        return tableInfo;
    }

}



package com.zte.cms.content.program.service;

import java.util.List;
import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramSchedualrecordMapDS
{
    /**
	 * 新增ProgramSchedualrecordMap对象
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;
	  
    /**
	 * 更新ProgramSchedualrecordMap对象
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;

    /**
	 * 批量更新ProgramSchedualrecordMap对象
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateProgramSchedualrecordMapList( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException;

	/**
	 * 根据条件更新ProgramSchedualrecordMap对象  
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap更新条件
	 * @throws DomainServiceException ds异常
	 */ 
	public void updateProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;

	/**
	 * 根据条件批量更新ProgramSchedualrecordMap对象  
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap更新条件
	 * @throws DomainServiceException ds异常
	 */ 
	public void updateProgramSchedualrecordMapListByCond( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException;

	/**
	 * 删除ProgramSchedualrecordMap对象
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;

	/**
	 * 批量删除ProgramSchedualrecordMap对象
	 *
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeProgramSchedualrecordMapList( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException;

    /**
	 * 根据条件删除ProgramSchedualrecordMap对象
	 * 
	 * @param programSchedualrecordMap ProgramSchedualrecordMap删除条件
	 * @throws DomainServiceException ds异常
	 */ 
    public void removeProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;	

    /**
	 * 根据条件批量删除ProgramSchedualrecordMap对象
	 * 
	 * @param programSchedualrecordMap ProgramSchedualrecordMap删除条件
	 * @throws DomainServiceException ds异常
	 */ 
    public void removeProgramSchedualrecordMapListByCond( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException;	

	/**
	 * 查询ProgramSchedualrecordMap对象
	 
	 * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	 * @return ProgramSchedualrecordMap对象
	 * @throws DomainServiceException ds异常 
	 */
	 public ProgramSchedualrecordMap getProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException; 

	 /**
	  * 根据条件查询ProgramSchedualrecordMap对象 
	  * 
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @return 满足条件的ProgramSchedualrecordMap对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<ProgramSchedualrecordMap> getProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException;

	 /**
	  * 根据条件分页查询ProgramSchedualrecordMap对象 
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询ProgramSchedualrecordMap对象 
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
}
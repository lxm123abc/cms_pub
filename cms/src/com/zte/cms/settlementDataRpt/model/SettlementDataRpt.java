package com.zte.cms.settlementDataRpt.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SettlementDataRpt extends DynamicBaseObject implements Serializable{
    private String id;
    private String spId;
    private String spName;

    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;

    /**
     * 税率
     */
    private BigDecimal taxRate;

    /**
     *  不含税额
     */
    private BigDecimal taxNotIncluded;

    /**
     * 税额
     */
    private BigDecimal taxAmount;

    private String projectType;
    private Date rptDt;
    private Date creDt;
    private String remark1;
    private String remark2;
    private String remark3;
    private Date updDt;
    private String isconfirm;

    public String getIsconfirm() {
        return isconfirm;
    }

    public void setIsconfirm(String isconfirm) {
        this.isconfirm = isconfirm;
    }

    public Date getUpdDt() {
        return updDt;
    }

    public void setUpdDt(Date updDt) {
        this.updDt = updDt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId == null ? null : spId.trim();
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName == null ? null : spName.trim();
    }

    public BigDecimal getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(BigDecimal settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxNotIncluded() {
        return taxNotIncluded;
    }

    public void setTaxNotIncluded(BigDecimal taxNotIncluded) {
        this.taxNotIncluded = taxNotIncluded;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public Date getRptDt() {
        return rptDt;
    }

    public void setRptDt(Date rptDt) {
        this.rptDt = rptDt;
    }

    public Date getCreDt() {
        return creDt;
    }

    public void setCreDt(Date creDt) {
        this.creDt = creDt;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    @Override
    public void initRelation() {
        this.addRelation("id", "ID");
        this.addRelation("spId", "SPID");
        this.addRelation("spName", "SPNAME");
        this.addRelation("settlementAmount", "SETTLEMENTAMOUNT");
        this.addRelation("taxRate", "TAXRATE");
        this.addRelation("taxNotIncluded", "TAXNOTINCLUDED");
        this.addRelation("taxAmount", "TAXAMOUNT");
        this.addRelation("projectType", "PROJECTTYPE");
        this.addRelation("rptDt", "RPTDT");
        this.addRelation("creDt", "CREDT");
        this.addRelation("remark1", "REMARK1");
        this.addRelation("remark2", "REMARK2");
        this.addRelation("remark3", "REMARK3");
        this.addRelation("updDt", "UPDDT");
        this.addRelation("isconfirm", "ISCONFIRM");
    }
}
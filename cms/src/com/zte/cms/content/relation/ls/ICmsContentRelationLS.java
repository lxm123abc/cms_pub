package com.zte.cms.content.relation.ls;

import java.util.List;

import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICmsContentRelationLS
{

    public TableDataInfo pageInfoQueryRelation(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws Exception;

    public TableDataInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize) throws Exception;

    public List<CmsContentRelation> getContentIdByCgid(String cgid) throws Exception;

    public TableDataInfo pageInfoQueryforStr(String condition, int start, int pagesize) throws Exception;

    public String insertRelationContent(String cgid, String cgname, String description, String selectList)
            throws Exception;

    public String deleteRelationContentByContentId(String contentid) throws Exception;

    public String deleteRelationContentByCgId(String cgid) throws Exception;

    public String updateRelationContent(String cgid, String cgname, String selectList) throws Exception;

    public String queryValidContent(String cgid) throws Exception;

    public List<CmsContentRelation> querySelectedContentList(String cgid) throws Exception;

    public List<CmsContentRelationInfo> pageInfoQueryList(String[] condition) throws Exception;

}

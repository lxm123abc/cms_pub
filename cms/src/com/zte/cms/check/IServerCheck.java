package com.zte.cms.check;

import java.util.List;
import com.zte.cms.server.model.CmsServer;
import com.zte.cms.server.model.DbServer;
import com.zte.cms.server.model.DiskInfo;

public interface IServerCheck
{

    public DbServer checkall() throws Exception;

    public String check() throws Exception;

    public DbServer checkDbServer() throws Exception;

    public String getFolderSize(String devicename) throws Exception;

    // public String getServerInfo(CmsServer cmsserver) throws Exception;
    public List<DiskInfo> getServerInfo(CmsServer cmsserver) throws Exception;
}

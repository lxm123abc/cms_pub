package com.zte.cms.cntSyncTask.ls;

import java.util.List;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICntSyncTaskLS
{
    /**
     * 根据index查询服务
     * 
     * @param cntSyncTask
     * @return
     * @throws Exception
     */
	public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws Exception;
	 
    /**
     * 查询服务列表 参数为三个，调用四个参数的方法
     * 
     * @param cntSyncTask CntSyncTask 查询的条件对象
     * @param start int起始行
     * @param pageSize  int每页显示行数
     * @return TableDataInfo返回查询的列表
     * @throws Exception Exception
     */ 
    public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize) throws Exception;

    /**
     * 查询服务列表 参数为四个参数
     * 
     * @param cntSyncTask CntSyncTask传入对象
     * @param start int起始行
     * @param pageSize int每页显示行数
     * @param puEntity PageUtilEntity默认排序
     * @return TableDataInfo查询得到的列表
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;
    
    
    public String getPreviewXMLPath(Long movieindex) throws Exception; 
    
    public String insertRerun(Long taskindex); 
    public String batchRerun(List<CntSyncTask> cntSyncTaskList); 
    
    /*
     * 查询同一批次号任务方法！
     * 
     */
    public TableDataInfo  getSameBatchidTask(CntSyncTask cntSyncTask, int start, int pageSize) throws Exception;
}
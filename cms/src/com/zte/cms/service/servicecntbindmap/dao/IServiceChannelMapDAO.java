package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceChannelMapDAO
{
    /**
     * 新增ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DAOException dao异常
     */
    public void insertServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException;

    /**
     * 根据主键更新ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DAOException dao异常
     */
    public void updateServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException;

    /**
     * 根据主键删除ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DAOException dao异常
     */
    public void deleteServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException;

    /**
     * 根据主键查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @return 满足条件的ServiceChannelMap对象
     * @throws DAOException dao异常
     */
    public ServiceChannelMap getServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException;

    /**
     * 根据条件查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @return 满足条件的ServiceChannelMap对象集
     * @throws DAOException dao异常
     */
    public List<ServiceChannelMap> getServiceChannelMapByCond(ServiceChannelMap serviceChannelMap) throws DAOException;

    /**
     * 根据条件分页查询ServiceChannelMap对象，作为查询条件的参数
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceChannelMap对象，作为查询条件的参数
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
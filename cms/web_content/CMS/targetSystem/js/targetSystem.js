var message = {};
//判断目标系统id是否合法
function checktargetSystemID(){
   var targetSysteID = $("#targetid").val();
   if(!isNumbbwt3(targetSysteID))
      {
	       document.getElementById("targetid_msg").innerHTML = '请输入1-8位的数字、字母、下划线或者小数点';
	       document.all.targetid.focus();
	       return false;
      }
    callUrl('targetSystemFacade/checkTargetsystemID.ssm',targetSysteID , 'message', true);
    
     var rtn = message.rtnValue;
     if (typeof(rtn) != "undefined") { 
        if(rtn == false){
           document.getElementById("targetid_msg").innerHTML = '目标系统编号重复';
	       document.all.targetid.focus();
	       return false;
        }else{
          return true;
        }
     } else {
         return false;
     }
}

 function isNumbbwt3(ex)
 {
     ex = ex.trim();
     if(ex == ""){
       return false;
     }
     var regExp = new RegExp("^([0-9. a-zA-Z_])+$"); 
	 if(regExp.test(ex)){
	   return true;
	 }else {
	   return false;
	 }
 }


//判断输入的目标系统信息是否合法
function checktargetSystemInput(type){
    var targetnameVal = $("#targetname").val();;
    targetnameVal = targetnameVal.trim();
    
    if(checkNull(targetnameVal))
	 {
       document.getElementById("targetname_msg").innerHTML = '此项不能为空';
       document.all.targetname.focus();
       return  false;
     }
     else{
        error = isMaxLength(targetnameVal, "targetname_msg", 255);
        
        if (error == false)
        {
            return false;
        }  
     }
     if(type == "insert"){
    	 
     	 //验证平台类型下拉框是否选择
	     var platformVal = $("#platform").val();
	     platformVal = platformVal.trim();
	     //alert("选择的平台类型为"+platformVal);
	     if(checkNull(platformVal))
		 {
	       document.getElementById("platform_msg").innerHTML = '此项不能为空';
	       document.all.platform.focus();
	       return  false;
	     }
    	 
    	 //验证目标系统类型下拉框是否选择
	     var targettypeVal = $("#targettype").val();
	     targettypeVal = targettypeVal.trim();
	     if(checkNull(targettypeVal))
		 {
	       document.getElementById("targettype_msg").innerHTML = '此项不能为空';
	       document.all.targettype.focus();
	       return  false;
	     }
	     
	     if(platformVal==1&&(targettypeVal==1||targettypeVal==2||targettypeVal==3)){
		       document.getElementById("platform_msg").innerHTML = '2.0平台不能选择3.0的网元';
		       document.all.platform.focus();
		       return  false;
	     }
       if(platformVal==2&&!(targettypeVal ==1||targettypeVal==2||targettypeVal==3)){
    	   alert(platformVal);
    	   alert(targettypeVal);
	       document.getElementById("platform_msg").innerHTML = '3.0平台不能选择2.0网元';
	       document.all.platform.focus();
	       return  false;
	     }    
     }
     var msgurlVal = $("#msgurl").val();
     msgurlVal = msgurlVal.trim();
     
     if(checkNull(msgurlVal)){
         document.getElementById("msgurl_msg").innerHTML = '此项不能为空';
         document.all.msgurl.focus();
         return  false;
     }else{
	       var  error = isMaxLength(msgurlVal, "msgurl_msg", 255);
	        if (error == false)
	        {
	            return false;
	        }
	        if(!isURLFormat("msgurl","msgurl_msg")){
	             return false;
	        }
       }
     return true;
}
//判断是不是接口地址
function isURLFormat(input,input_msg)
{
  var inputValue=document.getElementById(input).value;
  if(trim(inputValue) != '' && inputValue != null)
  {
    var strRegex = /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/; 
    var exp = new RegExp(strRegex);
    var inputvalue = inputValue.toLowerCase().trim();
    var reg = inputvalue.match(exp);
    if(reg == null)
    {
        document.getElementById(input_msg).innerText = "请输入正确的接口地址"; 
        document.getElementById(input).onfocus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;   
    }      
  }
}

  //检查是否为空
  function checkNull(inputStr)
   {
       return ( Trim(inputStr).length==0);
   }
   
     function isMaxLength(inputValue, input_msg, len)
	{	
    	if (utf8_strlen2(inputValue) > len)
    	{
        	document.getElementById(input_msg).innerText = "此项长度不能超过" + len + " 个字符"; 
        	return false;
    	}
    	else
    	{
        	document.getElementById(input_msg).innerText = "";   
        	return true;   
    	}
	}
	 
	 function utf8_strlen2(str) 
 { 
	var cnt = 0; 
	for( i=0; i<str.length; i++) 
	{ 
	    var value = str.charCodeAt(i); 
	    if( value < 0x080) 
	    { 
	        cnt += 1; 
	    } 
	    else if( value < 0x0800) 
	    { 
	        cnt += 2; 
	    } 
	    else  if(value < 0x010000)
	    { 
	        cnt += 3; 
	    }
		else
		{
			cnt += 4;
		}
	} 
	return cnt; 
 } 
	 
	function get19Time(time14)
	{
		if(time14 != undefined && time14 != null && strTrim(time14) != "")
	    {
	     	   var  tmpTime = strTrim(time14);
	     	   if(tmpTime.length == 14)
	     	   {
	     	   	   var year = tmpTime.substring(0,4);
	     	   	   var month = tmpTime.substring(4,6);
	     	   	   var day = tmpTime.substring(6,8);
	     	   	   var hour = tmpTime.substring(8,10);
	     	   	   var minutes = tmpTime.substring(10,12);
	     	   	   var seconds = tmpTime.substring(12,14);
	     	   	   return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
	     	   }
	     	   else
	     	   {
	     	   	  var year = tmpTime.substring(0,4);
	     	   	   var month = tmpTime.substring(4,6);
	     	   	   var day = tmpTime.substring(6,8);
	     	   	  
	     	   	   return year + "-" + month + "-" + day;
	     	   }
	   	}
	   	else
	   	{
	   		  return "";
	   	}
	}
	// 查询条件/新增下拉框联动
	function chosePlatform(platfrom) 
	{
	    var index = platfrom.value;
	    callUrl("sysCode/getSysCode.ssm",'["ucdn_cms_targetsys_type","",true]',"$M.targettypeList",true);
	    var targettypes = document.getElementById("targettype");
		var len = targettypes.options.length;
	      for(var i=1;i< len; i++){
	    	  
	    	if(index == 1 && (targettypes.options[i].value == 1 || targettypes.options[i].value == 2 || targettypes.options[i].value == 3)){
	    		targettypes.options.remove(i);
	    		i--;
	    	}
	    	if(index == 2 && targettypes.options[i].value != 1 && targettypes.options[i].value != 2 && targettypes.options[i].value != 3){
	    		targettypes.options.remove(i);
	    		i--;
	    	}	    	 
	       }
	}	

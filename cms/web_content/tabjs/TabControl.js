
var _stylePath = null;
var _curentFrame = null;
var _curentUrl = null; 
var _isShowCloseBtn = null;
var _selectedTab = ".selectedTab";
var _preSelectedTab = ".preSelectedTab";
var _needShow = true;
var _tabNum = 4;
var _maxTabNumber = 8;

var _sunqi_prewindow_id = null;
var _sunqi_array = {};

Number.prototype.NaN0=function(){return isNaN(this)?0:this;}

function HTabControl(tabSkinId,isShowCloseBtn,isShowDropList)
{
    //this.tabSkinId = tabSkinId;
    _isShowCloseBtn = isShowCloseBtn;
    _isShowDropList = isShowDropList;
    _stylePath = _path + "/Tab"+ tabSkinId +"/";
    
    this.init = function()
    {
        loadStyle(tabSkinId,"tab.css");
        return "<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' id='content'>" +
        "<tr>" +
        "<td style='height:21px;' valign='bottom' class='tabCtrl'><div class='tabCtrlDiv' id='tabContainer' style='overflow:hidden;float:left;display:block;'><div id='aa' style='width:auto;float:left;'>" +
        "<table height='100%' id='tabFrameTitle' border='0' cellspacing='0' cellpadding='0'>" +
        "<tr>" +
        "<td valign='bottom'>&nbsp;</td>" +
        "</tr>"+
        "</table></div>" +
        "<div class='dropIcon' id='dropIcon' onclick='showDropList(this)' style='display:none'><span></span></div>" +
        "</div>" +
        "</td>" +
        "</tr>" +
        "<tr><td class='tabpage' valign='top' id='tdFramePane'></td></tr>" + 
        "</table>";
    }

    this.Cts = function(title,url,isRef)
    {
        if(!getId(title) && hasEnoughTab()){
            alert("已经打开了" + _maxTabNumber + "标签，请先关闭其中一些。");
            return;
        }
    	//alert($("#tdFramePane").html());
        var objTitle = getId("tabFrameTitle");
        var objIframe = getId("tdFramePane");
	
		if(getId(title))
        {
           if(title=="存储区管理"){
              closeTab(title);
           }else{
        	setIframeBackToDefaultPage(title);
           }
        }
        if(!getId(title))
        {
            //ShowLoading();
            CreateTitle(objTitle,title);
            if(isRef == true)
            {
                CreateIframe(objIframe,title,'auto');
            }
            else
            {
                CreateIframe(objIframe,title,'auto',url);
            }
            
            if(objTitle.offsetHeight > 30 )
            {

            }          
        }   
        FoucsPage(title,url,isRef);

        _curentUrl = url;

       /* if($("#"+currentMenuName)[0])
        {
            $("#"+currentMenuName).siblings().each(function(i, n) {
                if($(n).attr("id"))
                    closeTab(currentMenuName);
            });
        }*/
        needShowDropListIcon();
        
        _sunqi_array[title] = title;
    }
}

function setIframeBackToDefaultPage(id)
{
	var objIframe = getId("tdFramePane"); 
    for(i=0;i<objIframe.childNodes.length;i++)
    {
        if(objIframe.childNodes[i].id == "F_"+id)
        {      
               objIframe.childNodes[i].src =  objIframe.childNodes[i].src;
               break;
        }
      
    }
}

function needShowDropListIcon(){
    var containerWidth = null;
    if($("#indexContainer")[0] && $("#leftBar")[0])
        containerWidth = $("#indexContainer")[0].offsetWidth - $("#leftBar")[0].offsetWidth;
    else
        return;
    var tabWidth = $("#aa")[0].offsetWidth;
    if(containerWidth < tabWidth){
        $("#dropIcon").show();
    }else{
        $("#dropIcon").hide();
    }
}

function hasEnoughTab(){
    var curTabNum = $("#tabFrameTitle")[0].rows[0].cells.length;
    if(curTabNum && (curTabNum - 1) == _maxTabNumber)
        return true;
    return false;
}


function closeAllTabs(){
    if(!confirm("Do you want to close all tabs?")) return;
    var tabs = $("#tabFrameTitle")[0].rows[0].cells;
    $(tabs).each(function(i, tab) {
        if($(tab).attr("id"))
            closeTab($(tab).attr("id"));
    });
}

function closeAllTabs_new(){
    var tabs = $("#tabFrameTitle")[0].rows[0].cells;
    $(tabs).each(function(i, tab) {
        if($(tab).attr("id"))
            closeTab($(tab).attr("id"));
    });
}


function initDropList(){
    var bbHtml = "";
    $(_selectedTab).parent().find("td").each(function(i, td) {
        if(!$(td).attr("id")) return;
        bbHtml += "<span>" + $(td).attr("id") + "</span>\n";
    });
    if(bbHtml != ""){
        $("#bb").html(bbHtml);
        $("#bb").append("<span id='closeAllTabs'>Close all</span>");
        var iFrame = '<div id="shadowIFrame" style="display:none;background:#fff;position:absolute;top:0;left:0;z-index:999;' + 
            'filter:alpha(opacity=80);-moz-opacity:0.8;"></div><iframe id="mask" frameborder="0" style="position:absolute;visibility:inherit; top:0px; left:0px;' + 
            ' width:100%; height:100%; display:block;z-index:-998;border-bottom:1px solid #92c3d7"></iframe>';
        $("#bb").append(iFrame);
        $("#bb span").each(function(i, span) {
            $(span).click(function() {
                if($(this).attr("id"))
                    closeAllTabs();
                else
                    selectTabWithDropList($(this).text());
                closeDropList();
            });
            $(span).hover(function() {
                $(this).addClass("over");
            }, function(){
                $(this).removeClass("over");
            });
        });
    }
}

function showDropList(_this){
    if(_needShow){
        _needShow = false;
        initDropList();
        var offset = getPosition(_this);
        $("#bb")[0].style.top = (offset.y + 15) + "px";
        $("#bb")[0].style.left = (offset.x - 105) + "px";
        $("#shadowIFrame")[0].style.top = (offset.y + 15) + "px";
        $("#shadowIFrame")[0].style.left = (offset.x - 105) + "px";
        $("#shadowIFrame").show();
	//	alert( $("#shadowIFrame").attr("height"));
	    $("#mask").css({height: $("#tabFrameTitle")[0].rows[0].cells.length*22+'px'});
        $("#bb").fadeIn("slow");
        $("#bb")[0].focus();
        $("#bb").blur(function() {
            closeDropList();
        });
    }else{
        closeDropList();
    }
}

function closeDropList(){
    _needShow = true;
    $("#shadowIFrame").hide();
    $("#bb").fadeOut("slow");
}

function getPosition(e){
    var left = 0;
    var top  = 0;
    while (e.offsetParent){
        left += e.offsetLeft + (e.currentStyle?(parseInt(e.currentStyle.borderLeftWidth)).NaN0():0);
        top  += e.offsetTop  + (e.currentStyle?(parseInt(e.currentStyle.borderTopWidth)).NaN0():0);
        e     = e.offsetParent;
    }
    
    left += e.offsetLeft + (e.currentStyle?(parseInt(e.currentStyle.borderLeftWidth)).NaN0():0);
    top  += e.offsetTop  + (e.currentStyle?(parseInt(e.currentStyle.borderTopWidth)).NaN0():0);
    return {x:left, y:top};
}


function selectTabWithDropList(tabId){
    var curTab = $(_selectedTab);
    var selTab = $("#" + tabId);
    if(curTab[0] == selTab[0]) return;
    if(_tabNum){
        var preTab = new Array(_tabNum);
        var nextTab = new Array(_tabNum);
        for (var i = 0; i < preTab.length; i++) {
            preTab[i] = getPreTab(selTab, i + 1);
        }
        for (var i = 0; i < nextTab.length; i++) {
            nextTab[i] = getNextTab(selTab, i + 1);
        }
        var firstTab = $(_selectedTab).parent().find("td").eq(1);
        for (var i = 0; i < preTab.length; i++) {
            if(firstTab[0] == preTab[i][0])
                firstTab = selTab;
        }
        for (var i = preTab.length - 1; i >= 0; i--) {
            if(preTab[i] && preTab[i].attr("id"))
                firstTab.before(preTab[i]);
        }
        if (firstTab[0] != selTab[0]) 
            firstTab.before($("#" + tabId));
        for (var i = preTab.length - 1; i >= 0; i--){
            if(nextTab[i] && nextTab[i].attr("id"))
                selTab.after(nextTab[i]);
        }
    }else{
        curTab.before(selTab);
    }
    FoucsPage(tabId);
}

function getPreTab(selTab, number){
    if(!number) return null;
    var obj = selTab;
    for (var i = 0; i < number; i++){
        obj = obj.prev();
    }
    return obj;
}

function getNextTab(selTab, number){
    if(!number) return null;
    var obj = selTab;
    for (var i = 0; i < number; i++){
        obj = obj.next();
    }
    return obj;
}

function loadStyle(skinId,cssName)
{
    var head = document.getElementsByTagName('head').item(0);
    css = document.createElement('link');
    css.href = _stylePath + cssName;
    css.rel = 'stylesheet';
    css.type = 'text/css';
    css.id = 'loadCss';
    head.appendChild(css);
}



function CreateTitle(obj,title)
{	
 
    var titleTable = document.createElement("table");
    titleTable.setAttribute("border", "0");
    titleTable.setAttribute("cellPadding","0");
    titleTable.setAttribute("cellSpacing","0");
   
    var titleBody = document.createElement("tbody");
    
    var titleRow = document.createElement("tr");
    
    var tdLeft = document.createElement("td");
        tdLeft.className = "tabTitleFoucs_l";
		var blankImg = new Image();
			 blankImg.src = _stylePath+"tab_close1.gif";
			// blankImg.style.width = "11px";
			 tdLeft.appendChild(blankImg);  
    
    var tdMid = document.createElement("td");
        tdMid.innerHTML = title;
        tdMid.className = "tabTitleFoucs_m";
        if(_isShowCloseBtn) tdMid.title = "双击关闭此标签";
        tdMid.onselectstart = function(){return false};
  
    var tdClose = document.createElement("td");
        tdClose.className = "tabTitleFoucs_m";
        tdClose.style.width = "8px";
        
        if(_isShowCloseBtn)
        {
            CreateCloseBtn(tdClose,title);
        }
		
    var tdRight = document.createElement("td");
        tdRight.className = "tabTitleFoucs_r";
		var blankImg1 = new Image();
			 blankImg1.src = _stylePath+"tab_close1.gif";
			// blankImg1.style.width = "11px";
			 tdRight.appendChild(blankImg1);  
    
  
    titleRow.appendChild(tdLeft);
    titleRow.appendChild(tdMid);
    titleRow.appendChild(tdClose);
    titleRow.appendChild(tdRight);
    titleBody.appendChild(titleRow);
    
    titleTable.appendChild(titleBody);
    titleTable.id = "It_"+title;
    titleTable.onclick = function()
    {
        FoucsPage(title);
    };
    if(_isShowCloseBtn){
        titleTable.ondblclick = function()
        {

            closeTab(title);
        }
    }
    
    var mainTd = document.createElement("td");
		mainTd.id = title;
        mainTd.setAttribute("height","24");
        mainTd.appendChild(titleTable); 
    
    var selectedTab = $(_selectedTab);
    if(selectedTab[0]){
    	selectedTab.after(mainTd);
    }else{
        obj.rows[0].appendChild(mainTd);
    }
}

function closeTab(id){	
    if(FindOtherFrameId(id) != null)
    {
    
    
      //alert("000_sunqi_prewindow_id=" + _sunqi_prewindow_id + ",id=" + id);    
       
      var count=0;
      if(_sunqi_prewindow_id == id)
      {
      		delete _sunqi_array[id];
      		
      		for( var key in _sunqi_array ) 
      		{
      			_sunqi_prewindow_id = _sunqi_array[key];
      			count = count +1;
      		}
          //alert("111_sunqi_prewindow_id=" + _sunqi_prewindow_id + ",id=" + id + ",count=" + count);   			
      		FoucsPage(_sunqi_prewindow_id);
      }else
      {
      	delete _sunqi_array[id];
      }
 	
            
        CloseTitle(id);
        CloseIframe("F_"+id);
        // HiddenLoading();
        needShowDropListIcon();
    }
    else
    {
    	  delete _sunqi_array[id];
    	
    	  FoucsPage(FindOtherFrameId(id));
        CloseTitle(id);
        CloseIframe("F_"+id);
       	if(currentMenuName)
       	{
        	myTab.Cts(currentMenuName,"welcome.html");
        }
    }
    
     //alert("_sunqi_prewindow_id=" + _sunqi_prewindow_id + ",id=" + id);
}

function CreateIframe(obj,title,canScoll,url)
{
    var iFrame = document.createElement("iframe");
    iFrame.id = "F_"+title;
    iFrame.name = "F_"+title;
    if(url != null) iFrame.src = url;
    iFrame.frameBorder = "0";
    iFrame.scrolling = "yes";
    iFrame.className = "Frame";
    iFrame.style.width = "100%";
    iFrame.style.height = "580px";
    obj.appendChild(iFrame);
}

function hasNext(_this){
    return $(_this).next("iframe")[0];
}


function GetIframe(id)
{
    // return document.frames[id];
    return $(id).get(0);
}

function CreateCloseBtn(parent,id)
{
    var closeBtn = new Image();
    closeBtn.id = "btn"+id;
	
    closeBtn.src = _stylePath+"tab_close.gif";
	
    closeBtn.style.cursor = "pointer";
    closeBtn.onclick = function()
    {
        closeTab(id);
    }
    parent.appendChild(closeBtn);    
}

function CloseCurPage()
{
    getId("btn"+_curentFrame).click();
}


function FindOtherFrameId(id)
{
    var lastIframeId = null;
    var objTitle = getId("tabFrameTitle");
    var cellCount = objTitle.rows[0].cells.length;
    var currentCellIndex = 0;
    for (var i = 0 ; i < cellCount ; ++i)
    {
        if (objTitle.rows[0].cells[i].id == id)
        {
            currentCellIndex = i ;
        }
    }
    if(cellCount == 2)
    {
        return null;
    }
    if(currentCellIndex == 1 &&  currentCellIndex+1 <= cellCount)
    {
        lastIframeId = objTitle.rows[0].cells[currentCellIndex+1].id;
    }
    else
    {
        lastIframeId = objTitle.rows[0].cells[currentCellIndex-1].id;
    }
    if(lastIframeId == "") lastIframeId=null;
    return lastIframeId;
}

function CloseTitle(id)
{
    if(id != null)
    {
        var objTitle = getId("tabFrameTitle");
        for (var i = 0 ; i < objTitle.rows[0].cells.length ; ++i)
        {
	        if (objTitle.rows[0].cells[i].id == id)
	        {
	            if(GetBrowser() == "IE")
	            {
		            objTitle.rows[0].cells[i].removeNode(true);
		        }
		        else
		        {
		            node = objTitle.rows[0].cells[i];
		            node.parentNode.removeChild(node); 
		        }
	        }
        }
    }
}

function CloseIframe(id)
{
    if(GetBrowser() == "IE")
    {
        var obj = getId(id);
        obj.removeNode(true)
    }
    else
    {
        node = getId(id);
        node.parentNode.removeChild(node); 
    }
}


function FoucsPage(id,url,isRef)
{
	  _sunqi_prewindow_id = id;

/*
	  if(id!="Welcome")
	  {	  	
	  	_sunqi_array[id] = id;
	  }
*/	  
	  
    if(id == null) 
    {
        _curentFrame = null;
        return;
    }

    if(_curentFrame == id){ 
	      return;
    }
    _curentFrame = id;
    ShowTitle(id);
    ShowIframe(id,url,isRef);
    //if(_isShowCloseBtn) changeMenuFocus(id);
   setHoverForTabs(id);
}

function setHoverForTabs(id){
    var curTab = $("#" + id);
    curTab.hover(function() {}, function() {});
    curTab.siblings().each(function(i, n) {
        $(n).hover(function() {
            $(this).addClass("over");
        },function() {
            $(this).removeClass("over");
        });
    });
}

function GetIframeInnerDocument(objIFrame)
{
    var doc = null;
    if (objIFrame.contentDocument) 
    {
        // For NS6
        doc = objIFrame.contentDocument; 
    } 
    else if (objIFrame.contentWindow) 
    {
        // For IE5.5 and IE6
        doc = objIFrame.contentWindow;
    } 
    else if (objIFrame.document) 
    {
        // For IE5
        doc = objIFrame.document;
    }
    return doc;
}


function ShowTitle(id)
{
    var objTitle = getId("tabFrameTitle");
    for (var i = 0 ; i < objTitle.rows[0].cells.length ; ++i)
    {
        curId = objTitle.rows[0].cells[i].id;
        if(curId != "")
        {
        	if($("#" + curId).attr("class") == "selectedTab")
        		$("#" + curId).attr("class", "preSelectedTab");
        	else
        		$("#" + curId).removeAttr("class");
            getId("It_"+curId).rows[0].cells[0].className = "tabTitleUnFoucs_l";
            getId("It_"+curId).rows[0].cells[1].className = "tabTitleUnFoucs_m";
            getId("It_"+curId).rows[0].cells[2].className = "tabTitleUnFoucs_m";
            getId("It_"+curId).rows[0].cells[3].className = "tabTitleUnFoucs_r";
        }
    }
    $("#" + id).attr("class", "selectedTab");
    getId("It_"+id).rows[0].cells[0].className = "tabTitleFoucs_l";
    getId("It_"+id).rows[0].cells[1].className = "tabTitleFoucs_m";
    getId("It_"+id).rows[0].cells[2].className = "tabTitleFoucs_m";
    getId("It_"+id).rows[0].cells[3].className = "tabTitleFoucs_r";    
}


function ShowIframe(id,url,isRef)
{
    var objIframe = getId("tdFramePane"); 
    for(i=0;i<objIframe.childNodes.length;i++)
    {
        if(objIframe.childNodes[i].id == "F_"+id)
        {
            objIframe.childNodes[i].style.display = '';
            if(isRef == true)
            {
                objIframe.childNodes[i].src = url;
            }
        }
        else
        {
            objIframe.childNodes[i].style.display = 'none';
        }
    }
}

function GetBrowser()
{
    if (document.all)
        return "IE" ;
    if (document.layers)
        return "NS" ;	
    return "OTHER" ;
}

function showScrollBtn(){
	var cw = $("#tabContainer").width();
	var fw = $("#aa").width();
      	if(fw > cw)
	{
	$("#scrollCtrl").show();
	//scrollBtn();
	}
	else
	{
		$("#scrollCtrl").hide();
	}
}

function scrollBtn(){
	
}

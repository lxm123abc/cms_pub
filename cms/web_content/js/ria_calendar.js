// @version 3.00.04

var DEFAULT_DATE_FORMAT = "yyyy-MM-dd";  //\u65e5\u671f\u9ed8\u8ba4\u683c\u5f0f

var DEFAULT_DAY_OFFSET = 0;  //\u65e5\u671f\u9762\u677f\u8d77\u59cb\u661f\u671f\u504f\u79fb\uff0c\u9ed8\u8ba4\u4e3a\u661f\u671f\u65e5
var startDayFormatMap = {};

addStartDay = function(calID, startDay) {
	startDayFormatMap[calID] = startDay;
}

/**
 *\u683c\u5f0f\u5316\u65e5\u671f\u683c\u5f0f\u4e3a\u6807\u51c6\u683c\u5f0fyyyy,MM,dd,HH,mm,ss
 **/
getCalendarFormat = function(format)
{
	if(format == "undefined" || format == "null" || format == null || Trim(format) == "")
	{
		return DEFAULT_DATE_FORMAT;
	}
	
	var dateFormat = Trim(format.toLowerCase());
	
	if(dateFormat.indexOf("mm") >= 0 && (dateFormat.indexOf("mm") != dateFormat.lastIndexOf("mm") || (dateFormat.indexOf("hh") < 0 && dateFormat.indexOf("ss") < 0)))
	{
		dateFormat = dateFormat.replace("mm", "MM");
	}
	
	if(dateFormat.indexOf("hh") >= 0)
	{
		dateFormat = dateFormat.replace("hh", "HH");
	}
	
	return dateFormat;
}

/***************************************
 * \u4f20\u5165\u4e00\u4e2aUTC\u65f6\u95f4 \u8fd4\u56de\u4e00\u4e2a\u65e5\u671f\u3002
 * @author \u5f20\u5b8f\u4f1f
 * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
 * @param utcTime UTC\u65f6\u95f4
 ***************************************/
var TIME_OFFSET = new Date().getTimezoneOffset() * 60 * 1000;//\u65f6\u533a\u5dee

getCalendarTimefromUTC = function(format, utcTime)
{   
    if (utcTime == null || utcTime == '')
    {
        return '';
    }
		
	if(format == null || Trim(format) == "")
	{
		format = DEFAULT_DATE_FORMAT;
	}
	
	var returnTime = getCalendarFormat(format);
	
	
	var paraDate = new Date(utcTime);
	var year = "" + paraDate.getFullYear();
	var month = paraDate.getMonth() >= 9 ? "" + (paraDate.getMonth() + 1) : "0" + (paraDate.getMonth() + 1);
	var date = paraDate.getDate() > 9 ? "" + paraDate.getDate() : "0" + paraDate.getDate();
	var hour = paraDate.getHours() > 9 ? "" + paraDate.getHours() : "0" + paraDate.getHours();
	var minute = paraDate.getMinutes() > 9 ? "" + paraDate.getMinutes() : "0" + paraDate.getMinutes();
	var second = paraDate.getSeconds() > 9 ? "" + paraDate.getSeconds() : "0" + paraDate.getSeconds();
	
	if(returnTime.indexOf("yyyy") >= 0)
	{
		returnTime = returnTime.replace("yyyy", year);
    }		
	else if(returnTime.indexOf("yy") >= 0)
	{
		returnTime = returnTime.replace("yy", year.substring(2));
	}
	
	if(returnTime.indexOf("MM") >= 0)
	{
		returnTime = returnTime.replace("MM", month);
	}
	
	if(returnTime.indexOf("dd") >= 0)
	{
		returnTime = returnTime.replace("dd", date);	
	}
	
	if(returnTime.indexOf("HH") >= 0)
	{
		returnTime = returnTime.replace("HH", hour);	
	}
	
	if(returnTime.indexOf("mm") >= 0)
	{
		returnTime = returnTime.replace("mm", minute);	
	}
	
	if(returnTime.indexOf("ss") >= 0)
	{
		returnTime = returnTime.replace("ss", second);	
	}
	
	return returnTime;
};

/***************************************
 * \u5c06\u65e5\u671f\u683c\u5f0f\u7684\u6570\u636e\u8f6c\u6362\u6210UTC\u3002
 * @author \u5f20\u5b8f\u4f1f
 * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
 * @param Time \u8981\u8f6c\u6362\u6210UTC\u65f6\u95f4\u7684\u65e5\u671f
 ***************************************/

getCalendarUTCfromTime = function(format, time)
{
    if (time == null || Trim(time) == '')
    {
        return null;
    }
	
	if(format == null || Trim(format) == "")
	{
		format = DEFAULT_DATE_FORMAT;
	}
	
	format = Trim(format);
	time = Trim(time);
	
	if(time.length != format.length)
	{
		return null;
	}
	
	var dateFormat = getCalendarFormat(format);
	
	var year, month, date, hour, minute, second;
	var currentDate = new Date();
	
	if(dateFormat.indexOf("yyyy") >= 0)
	{
		year = parseInt(time.substring(dateFormat.indexOf("yyyy"), dateFormat.indexOf("yyyy") + 4), 10);
    }		
	else if(dateFormat.indexOf("yy") >= 0)
	{
		year = Math.floor(new Date().getFullYear() / 100) * 100 + parseInt(time.substring(dateFormat.indexOf("yy"), dateFormat.indexOf("yy") + 2), 10);
	}
	
	if(dateFormat.indexOf("MM") >= 0)
	{
		month = parseInt(time.substring(dateFormat.indexOf("MM"), dateFormat.indexOf("MM") + 2), 10) - 1;
	}
	
	if(dateFormat.indexOf("dd") >= 0)
	{
		date = parseInt(time.substring(dateFormat.indexOf("dd"), dateFormat.indexOf("dd") + 2), 10);
	}
	
	if(dateFormat.indexOf("HH") >= 0)
	{
		hour = parseInt(time.substring(dateFormat.indexOf("HH"), dateFormat.indexOf("HH") + 2), 10);
	}
	
	if(dateFormat.indexOf("mm") >= 0)
	{
		minute = parseInt(time.substring(dateFormat.indexOf("mm"), dateFormat.indexOf("mm") + 2), 10);	
	}
	
	if(dateFormat.indexOf("ss") >= 0)
	{
		second = parseInt(time.substring(dateFormat.indexOf("ss"), dateFormat.indexOf("ss") + 2), 10);	
	}
	
	return new Date(year != null ? year : 0, month != null ? month : 0, date != null ? date : 0, hour != null ? hour : 0, minute != null ? minute : 0, second != null ? second : 0).getTime();

};

/**
   * \u4e3a\u65e5\u5386\u63a7\u4ef6\u8d4b\u503c,\u540e\u53f0\u4f20\u56de\u6765\u4e00\u4e2aUTC\u65f6\u95f4\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param {Object} targetid
   * @param value UTC\u65f6\u95f4
   */  
  function setCalendarValue(id, value)
  {
  	if(typeof value == 'number')
  	{
	  	var obj = document.getElementById(id);
		var targetId = obj.getAttribute("target");
		var obj1 = document.getElementById(targetId);
        var obj_two = document.getElementById(targetId+"_"+id);

		var format = obj.getAttribute("formate");
	    var time = UTC2Date(format, value);
	    obj1.value = time;
		if(obj.getAttribute('isFullDate') == "false")
		{
			arr = time.split(" ");
			obj_two.value = arr[1];
		}else{
			obj_two.value = time;
		}
  	}  
	else 
	{   
  	    var obj = document.getElementById(id);
		var targetId = obj.getAttribute("target");
		var obj1 = document.getElementById(targetId);
        var obj_two = document.getElementById(targetId+"_"+id);
	   
		var tmpValue = "" ;
		if ( value == null || value ==undefined  )
		{
			tmpValue = "" ;
		} else {
			tmpValue = value ;
		}

	    obj1.value = tmpValue;

		if(obj.getAttribute('isFullDate') == "false")
		{
			arr = value.split(" ");
			obj_two.value = arr[1];
		}else{
			obj_two.value = tmpValue;
		}
	}
  };
 

function RIACalendarFactory(UTCTime)
{
	this.utctime = UTCTime;
	this.fullDate;
	
	this.year;
	this.month;
	this.date;
	this.hours;
	this.minutes;
	this.seconds;
	this.milliseconds;
	this.timezoneOffset;
	this.c = ":";
	
	this.init();	
};
RIACalendarFactory.prototype.init = function()
{
	if(this.utctime == "" || this.utctime == null)
	this.fullDate = new Date();
		
	else this.fullDate = new Date(this.utctime);

	this.year = this.fullDate.getFullYear();

	this.month = this.fullDate.getMonth()+1;

	this.date= this.fullDate.getDate(); 

	this.hours = this.fullDate.getHours();

	this.minutes = this.fullDate.getMinutes();

	this.seconds = this.fullDate.getSeconds();

	this.milliseconds = this.fullDate.getMilliseconds();

	this.timezoneOffset = new Date().getTimezoneOffset()/60;		

};
RIACalendarFactory.prototype.getHHmmss = function()
{
	var HH_MM_SS = this.hours+this.c+this.minutes+this.c+this.seconds;
	return HH_MM_SS;
};
RIACalendarFactory.prototype.getMilliseconds = function()
{
	return this.milliseconds;
};
RIACalendarFactory.prototype.getFullYear = function()
{
	return this.year;
};
RIACalendarFactory.prototype.getMonth = function()
{
	return this.month;
};
RIACalendarFactory.prototype.getDate = function()
{
	return this.date;
};
RIACalendarFactory.prototype.getZoneOffset = function()
{
	return this.timezoneOffset;
};
 /**
   * \u53d6\u65e5\u5386\u63a7\u4ef6\u503c\uff0c\u8f6c\u6362\u6210UTC\u65f6\u95f4\u56de\u4f20\u5230\u540e\u53f0
   * @author \u5f20\u5b8f\u4f1f
   * @param {Object} targetid
   */   	
  function getCalendarValue(id)
  {
    var obj = document.getElementById(id);
    var targetId = obj.getAttribute("target");
	var obj1 = document.getElementById(targetId);
    var time = obj1.value;
	var format = obj.getAttribute("formate");
    return Date2UTC(format, time);
  };
  
   var canlendarObj = new Object();//\u63a7\u4ef6\u7684\u6a21\u578b\u6570\u7ec4

  UTC2Date = function(format, utcTime)
  {
	return getCalendarTimefromUTC(format, utcTime);
  };
  
  /***************************************
   * \u5c06\u65e5\u671f\u683c\u5f0f\u7684\u6570\u636e\u8f6c\u6362\u6210UTC\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
   * @param Time \u8981\u8f6c\u6362\u6210UTC\u65f6\u95f4\u7684\u65e5\u671f
   ***************************************/
  
  Date2UTC = function(format, time)
  {
	return getCalendarUTCfromTime(format, time);
  };
/**
 * \u521d\u59cb\u5316\u65e5\u5386\u63a7\u4ef6
 * 
 * @copyright
 * @author \u5f20\u5b8f\u4f1f
 */
var ERROR_TYPE_ALERT = "alert";
var ERROR_TYPE_LABLE = "lable";
 
function Calendar_init()
{
    var tagNames ;
	
	if(isIE())
	{
		tagNames = document.getElementsByTagName('Calendar'); 
	}else{
		tagNames = document.getElementsByTagName('z:Calendar'); 
	}
   
	for(var i = 0; i < tagNames.length; i++)
	{
		if(tagNames[i].parentNode.tagName.toLowerCase() == "z:column" || tagNames[i].parentNode.tagName.toLowerCase() == "column")
		{
			continue;
		}
		
		init_SSB_calendar(tagNames[i]);
	}
  
};

function init_SSB_calendar(calendarObj)
{
	return init_SSB_calendar(calendarObj, null);	
}

function init_SSB_calendar(calendarObj, dateValue)
{	
    var date_formate = calendarObj.getAttribute('formate'); 
     
    if (date_formate == null || date_formate =="") {
		date_formate = DEFAULT_DATE_FORMAT;
    } 
    var mid = 	calendarObj.getAttribute('id');
    var imgurl = calendarObj.getAttribute("imgurl");
    var _calendar_WIDTH = calendarObj.getAttribute("width");
    var _calendar_size = "16";
	var _calendar_readOnly = calendarObj.getAttribute("readonly");  //\u589e\u52a0\u65e5\u671finput\u7684readonly\u5c5e\u6027
    var _onchange_event = (calendarObj.getAttribute("onchange")!=undefined) ? calendarObj.getAttribute("onchange") : "";
	var _calendar_year_range = calendarObj.getAttribute("range");
	var _inputStyle = calendarObj.getAttribute('inputStyle');
	var _imgStyle = calendarObj.getAttribute('imgStyle');
	var _errMsgType = calendarObj.getAttribute('errMsgType');
	var _calendar_start_day = calendarObj.getAttribute('startDay');	
	
	var yearRange =  _calendar_year_range != null ? _calendar_year_range : 100;  //\u5e74\u4efd\u76f8\u5bf9\u5f53\u524d\u65f6\u95f4\u524d\u540e\u9009\u62e9\u8303\u56f4
	
	var startDay = _calendar_start_day != null ?  _calendar_start_day : 'Sunday';  //\u65e5\u671f\u9762\u677f\u8d77\u59cb\u661f\u671f
    addStartDay(mid, startDay);  

	if(date_formate != null && date_formate != "")
	{
		canlendarObj.formate = date_formate; 
	}
		
	if(_calendar_WIDTH != null && _calendar_WIDTH != "")
	{
		_calendar_size = _calendar_WIDTH;
	} 
	
	var inputTarget = calendarObj.getAttribute('target');	//\u83b7\u53d6\u5185\u5b58\u8f93\u5165\u6846\u7684ID	     
	var inputText = (calendarObj.getAttribute('value') != undefined) ? calendarObj.getAttribute("value") : "";//\u83b7\u53d6\u9ed8\u8ba4\u503c
	
	//add by daniel , add defalut value for Calenar Control like  Calenar Control of table 
    var defaultFun = calendarObj.getAttribute('default');
    if ( defaultFun != null && defaultFun != undefined  ) {
     	var defaultValue = eval(defaultFun); 
     	if ( defaultValue > 0 && date_formate != null && date_formate.length > 0 ) 
		{
     	    //document.getElementById(inputTarget).value = getCalendarTimefromUTC(date_formate,defaultValue);
			inputText = getCalendarTimefromUTC(date_formate, defaultValue);
		}
     	else  
		{
     	    //document.getElementById(inputTarget).value = "";
			inputText = "";
		}
    }
	
	if(dateValue != null)
	{
		inputText = getCalendarTimefromUTC(date_formate, dateValue);
	}
	
	var type1 = "text";
	var type2 = "hidden";
	canlendarObj.isFullDate = calendarObj.getAttribute('isFullDate');	
	var inputTarget_show = inputTarget+"_"+mid;
			
	var html = '<input type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'" class="calInputClass" style="'+_inputStyle+'"';
	if(!(_calendar_readOnly != null && _calendar_readOnly.toLowerCase() == "false"))
	{
		html += ' readonly="readonly" ';
	}
	html += ' value="'+inputText+'"';
	if(_errMsgType == ERROR_TYPE_LABLE)
	{
		html += ' onchange="dateValidator(this.value,\''+date_formate+'\',this.id);';
	}
	else
	{
		html += ' onblur="dateValidator(this.value,\''+date_formate+'\',this.id)" onchange="';
	}
	if(_onchange_event != null && Trim(_onchange_event) != "")
	{
		html += _onchange_event;
	}
	html += '" size="'+_calendar_size+'" isDate="true"/>';
	html += '<input type="'+type2+'" id="'+inputTarget+"_"+mid+'" readonly="readonly" ';
	html += ' value="'+inputText+'"  size="'+_calendar_size+'" isDate="false"/>&nbsp; ';
	
	/*
	if(canlendarObj.isFullDate == "false")
	{
		type1 = "hidden";
		type2 = "text";
		inputTarget_show = inputTarget+"_"+mid;
		html = '<input type="'+type2+'" id="'+inputTarget_show+'" style="'+_inputStyle+'"';
		if(!(_calendar_readOnly != null && _calendar_readOnly.toLowerCase() == "false"))
		{
			html += ' readonly="readonly" ';
		}
		html += ' value="'+inputText+'" onchange="'+_onchange_event+'" onblur="dateValidator(this.value,\''+date_formate+'\',this.id)" size="'+_calendar_size+'" isDate="false"/> ';
		html += '<input type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'" readonly="readonly" ';		
		html += ' value="'+inputText+'" size="'+_calendar_size+'" isDate="true"/>&nbsp;';
	}  
	*/
		
	if(imgurl == null || imgurl == "")
	{
		var imgurl = 'images/calendar.gif';
		var pagePath = location.pathname;
		var directoryNum = pagePath.split("/").length - 3;
		while (directoryNum > 0) {
		    imgurl = '../' + imgurl;
		    directoryNum --;
		}
	}

    html+='<img  id="calendar_img_id" class="imgCalendar" style="' + _imgStyle + '" src="'+imgurl+'" width="16" height="15"  onclick="fPopCalendar(\''+inputTarget+'\',\''+inputTarget+'\',\''+date_formate+'\',\''+yearRange+'\',\''+inputTarget_show+'\',\''+ mid +'\');return false" /> ';
    calendarObj.innerHTML = html; 

	return calendarObj;
};

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

var gdCtrl = new Object();
var gdCtrl_tow = new Object();
var goSelectTag = new Array();
var gcEmpty = '';
var previousObject = null;
var gdCurDate = new Date();
var giYear = gdCurDate.getFullYear();
var giMonth = gdCurDate.getMonth()+1;
var giDay = gdCurDate.getDate();
var curYear = gdCurDate.getFullYear();
var curMonth = gdCurDate.getMonth()+1;
var curDay = gdCurDate.getDate();
curMonth= (curMonth.toString().length==1)?'0'+curMonth.toString():curMonth.toString();
curDay= (curDay.toString().length==1)?'0'+curDay.toString():curDay.toString();
var giHh = gdCurDate.getHours();
var giMm = gdCurDate.getMinutes();
var giSs = gdCurDate.getSeconds();
var giTime = giHh+':'+giMm+':'+giSs;
var Hid = true;
/**
 * 
 * @param {Object} num
 */
function fCheckIsNumber(num)
{
	var i,j,strTemp;
	strTemp="0123456789";
	if (num.length==0)
	{
		return false;
	}
	for (i=0;i<num.length;i++)
	{
		j=strTemp.indexOf(num.charAt(i)); 
		if (j==-1)
		{
			return false;
		}
	}
	return true;
};
/**
 * 
 * @param {Object} str
 */
function LTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function RTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function Trim(str) 
{
	return str.replace(/(^[\s]*)|([\s]*$)/g, "");
};

function dateValidator(dateInputValue, dateFormat, inputId)
{   
	var errorLable = document.getElementById(inputId + "ErrLable");
	var calendarObj = document.getElementById(inputId).parentNode; 
	var errMsgType = calendarObj.getAttribute("errMsgType");
	var errMsg = calendarObj.getAttribute("errMsg");

	if(errorLable != null)
	{
		calendarObj.removeChild(errorLable);
	}
	
	if(dateInputValue == null || Trim(dateInputValue) == "")
	{
		return true;
	}
	
	if(dateFormat == null || Trim(dateFormat) == "")
	{
		dateFormat = DEFAULT_DATE_FORMAT;
	}
	
	dateInputValue = Trim(dateInputValue);
	
	var regularExpForDate = getCalendarFormat(dateFormat);
	
	var dateType = new Array("");
	
	if(regularExpForDate.indexOf("yyyy") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("yyyy", "(\\d{4})");
		dateType.push("y");
	}
	else if(regularExpForDate.indexOf("yy") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("yy", "(\\d{2})");
		dateType.push("y");
	}
	
	if(regularExpForDate.indexOf("MM") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("MM", "(\\d{1,2})");
		dateType.push("M");
	}
	
	if(regularExpForDate.indexOf("dd") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("dd", "(\\d{1,2})");
		dateType.push("d");
	}
	
	if(regularExpForDate.indexOf("HH") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("HH", "(\\d{1,2})");
		dateType.push("H");
	}
	
	if(regularExpForDate.indexOf("mm") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("mm", "(\\d{1,2})");
		dateType.push("m");
	}
	
	if(regularExpForDate.indexOf("ss") >= 0)
	{
		regularExpForDate = regularExpForDate.replace("ss", "(\\d{1,2})");
		dateType.push("s");
	}
	
	regularExpForDate = "^" + regularExpForDate + "$";
	
	var regResult = dateInputValue.match(new RegExp(regularExpForDate));
	
	var isErrorDate = false;
    
	if(regResult != null)
	{
		var tmpDate = new Date();
		for(var i = 1; !isErrorDate && i < dateType.length && i < regResult.length; i++)
		{
			switch(dateType[i])
			{
				case "y" : 
					tmpDate.setFullYear(parseInt(regResult[i], 10)); 
					if(tmpDate.getFullYear() != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
				case "M" : 
					tmpDate.setMonth(parseInt(regResult[i], 10) - 1); 
					if(tmpDate.getMonth() + 1 != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
				case "d" : 
					tmpDate.setDate(parseInt(regResult[i], 10)); 
					if(tmpDate.getDate() != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
				case "H" : 
					tmpDate.setHours(parseInt(regResult[i], 10));
					if(tmpDate.getHours() != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
				case "m" : 
					tmpDate.setMinutes(parseInt(regResult[i], 10));
					if(tmpDate.getMinutes() != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
				case "s" : 
					tmpDate.setSeconds(parseInt(regResult[i], 10));
					if(tmpDate.getSeconds() != parseInt(regResult[i], 10))
					{
						isErrorDate = true;
					}
					break;
			}
		}
	}
	else{
		isErrorDate = true;
	}
	
	if (isErrorDate)
	{
		if(errMsg == null || Trim(errMsg) == "")
		{
			errMsg = $res_entry('ui.control.calendar.fomatError1','\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\u8bf7\u6309"') + dateFormat + $res_entry('ui.control.calendar.fomatError2','"\u683c\u5f0f\u8f93\u5165\u6b63\u786e\u7684\u65e5\u671f!');
		}
 
		if(errMsgType == ERROR_TYPE_LABLE)
		{
			errorLable = document.createElement("lable");
			errorLable.setAttribute('id', inputId + "ErrLable");
			errorLable.className = "errorMsg";
			errorLable.innerHTML = errMsg;
			calendarObj.appendChild(errorLable);
		}
		else
		{
			alert(errMsg);
		}
		if(isIE()){
			document.getElementById(inputId).select();
		} else {
			window.setTimeout(function(){document.getElementById(inputId).select();},0);  
		}
	}

}

/**
 * 
 */
function fCheckDateTime()
{
	var inputValue = document.getElementById("dateStart").value;
	inputValue=Trim(inputValue);
	var len=inputValue.length;
	if (len!=10)
	{
		if(len!=16)
		{
			if (len!=19)
			{
				alert("'"+$res_entry('ui.control.calendar.dateFomatError','\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd\u683c\u5f0f\u8f93\u5165!')+"'");
				return;
			}
		}
	}
	var iDate= inputValue.substring(0,10);
	var iTime= inputValue.substring(10,len);
	if (fCheckDate(iDate)== false)
	{
		alert("'"+$res_entry('ui.control.calendar.dateFomatError','\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd\u683c\u5f0f\u8f93\u5165!')+"'");
		return;
	}
	if (len!=10)
	{
		if (fCheckTime(iTime)== false)
		{
			alert("'"+$res_entry('ui.control.calendar.fullDateFomatError','\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd\u683c\u5f0f\u8f93\u5165!')+"'");
			return;
		}
	}
};
/**
 * 
 * @param {Object} inputdate
 */
function fCheckDate(inputdate)
{
	inputdate=inputdate.split('-');
	var year=inputdate[0];
	var month=inputdate[1];
	var day=inputdate[2];
	var Date1 = new Date(year,month-1,day); 
	if (Date1.getMonth()+1!=month||Date1.getDate()!=day||Date1.getFullYear()!=year||year.length!=4)
	{
		return false;
	}
	else
	{
		if (month.indexOf('0')==0) 
		{
			month=month.substr(1,month.length); 
		}
		if (day.indexOf('0')==0) 
		{
			day=day.substr(1,day.length); 
		}
		if ((month==4 || month==6 || month==9 || month==11) && (day>30))
		{
			return false; 
		}
		if (month==2) 
		{
			if (LeapYear(year)) 
			{
				if (day>29 || day<1)
				{
					return false;
				}
			}
			else
			{
				if (day>28 || day<1)
				{
					return false; 
				}
			}
		}
	}
	return true;
};
function fCheckTime(inputTime)
{
	inputTime=inputTime.split(':')
	if(inputTime.length!=2)
	{
		if(inputTime.length!=3)
		{
			return false;
		}
	}
	var hh=Trim(inputTime[0])
	if (isNaN(hh))
	{
		return false;
	}
	if (hh.indexOf('0')==0) 
	{
		hh=hh.substr(1,hh.length); 
	}
	if ((hh<0) || (hh>23)) 
	{
		return false;
	}
	var mi=Trim(inputTime[1]);
	if (isNaN(mi))
	{
		return false;
	}
	if (mi.indexOf('0')==0) 
	{
		mi=mi.substr(1,mi.length); 
	}
	if((mi<0) || (mi>59)) 
	{
		return false;
	}
	if(inputTime.length==3)
	{
		var ss=Trim(inputTime[2]);
		if (isNaN(ss))
		{
			return false;
		}
		if (ss.indexOf('0')==0) 
		{
			ss=ss.substr(1,ss.length); 
		}
		if ((ss<0) || (ss>59)) 
		{
			return false;
		}
	}
	return true
};
function LeapYear(intYear)
{
	if (intYear % 100 == 0)
	{
		if (intYear % 400 == 0)
		{
			return true;
		}
	}
	else
	{
		if ((intYear % 4) == 0) 
		{
			return true; 
		}
	}
	return false;
};

function fSetDateTime(iYear,iMonth,iDay)
{
	giHh=document.getElementById("dpHour").value;
	giMm=document.getElementById("dpMinute").value;
	giSs=document.getElementById("dpSecon").value;
	giHh=(giHh.length==1)?'0'+giHh:giHh;
	giMm=(giMm.length==1)?'0'+giMm:giMm;
	giSs=(giSs.length==1)?'0'+giSs:giSs;
	giTime=giHh+':'+giMm+':'+giSs;
	VicPopCal.style.visibility = "hidden";
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		gdCtrl_tow.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{
		iMonth=(iMonth.toString().length==1)?'0'+iMonth.toString():iMonth;
		iDay=(iDay.toString().length==1)?'0'+iDay.toString():iDay;
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
		else
		{
			gdCtrl.innerText = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
	}
//	for (i in goSelectTag)
//	{
//		goSelectTag[i].style.visibility = "visible";
//	}
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function getNowDate()
{
	var nn = new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var monstr1;
	var datestr1
	if(mon1<10)
	{
		monstr1="0"+mon1;
	}
	else
	{
		monstr1=""+mon1;
	}
	if(date1<10)
	{
		datestr1="0"+date1;
	}
	else
	{
		datestr1=""+date1;
	}
	year1+"-"+monstr1+"-"+datestr1;
};
function getlastweekDate()
{
	var nn=new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var mm=new Date(year1,mon1-1,date1);
	var tmp1=new Date(2000,1,1);
	var tmp2=new Date(2000,1,15);
	var ne=tmp2-tmp1;
	var mm2=new Date();
	mm2.setTime(mm.getTime()-ne);
	year2=mm2.getYear();
	mon2=mm2.getMonth()+1;
	date2=mm2.getDate();
	if(mon2<10)
	{
		monstr2="0"+mon2;
	}
	else
	{
		monstr2=""+mon2;
	}
	if(date2<10) 
	{
		datestr2="0"+date2;
	}
	else
	{
		datestr2=""+date2;
	}
	return year2+"-"+monstr2+"-"+datestr2;
};

function selectQuickTime(format, o)
{
	var tFormat = format != null && format != "" ? format : "HH:mm:ss";
	var tmpTime = "";
	if(isIE())
	{
		tmpTime = Trim(o.innerText);
	}
	else
	{
		tmpTime = Trim(o.textContent);
	}
	
	var tmpUTCTime = getCalendarUTCfromTime(format, tmpTime);
	var tmpDate = new Date(tmpUTCTime != null ? tmpUTCTime : 0);
	
	fSetQuickTime(format, tmpDate.getHours(), tmpDate.getMinutes(), tmpDate.getSeconds());
}

function confirmQuickTime(format)
{
	var tFormat = format != null && format != "" ? format : "HH:mm:ss";
	var tHour = document.getElementById("quickSelect_HH") != null ? document.getElementById("quickSelect_HH").value : 0;
	var tMinute = document.getElementById("quickSelect_mm") != null ? document.getElementById("quickSelect_mm").value : 0;
	var tSecond = document.getElementById("quickSelect_ss") != null ? document.getElementById("quickSelect_ss").value : 0;
	
	fSetQuickTime(format, tHour, tMinute, tSecond);
}

function fSetQuickTime(format, hour, minute, second)
{
	var tmpTime = "";

	var tFormat = format != null && format != "" ? format : "HH:mm:ss";
	
	if(hour != -1 && minute != -1 && second != -1)
	{
		var tHour = !isNaN(parseInt(hour)) ? parseInt(hour, 10) : 0;
		var tMinute = !isNaN(parseInt(minute)) ? parseInt(minute, 10) : 0;
		var tSecond = !isNaN(parseInt(second)) ? parseInt(second, 10) : 0;
		
		tHour = tHour < 0 ? 0 : (tHour > 23 ? 23 : tHour);
		tMinute = tMinute < 0 ? 0 : (tMinute > 59 ? 59 : tMinute);
		tSecond = tSecond < 0 ? 0 : (tSecond > 59 ? 59 : tSecond);
		
		if(tHour != -1 && tMinute != -1 && tSecond != -1)
		{
			tmpTime = getCalendarTimefromUTC(format, new Date(0, 0, 0, tHour, tMinute, tSecond).getTime());
		}
	}
	
	var VicPopCal = document.getElementById("VicPopCal");
	VicPopCal.style.visibility = "hidden";
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	
	if(gdCtrl.tagName == "INPUT")
	{
		gdCtrl.value = tmpTime;
	}
	else
	{
		gdCtrl.innerText = tmpTime;
	}
	
	gdCtrl.onchange();
	goSelectTag.length = 0;
	window.returnValue = gdCtrl.value;
}

function fSetDate(iYear, iMonth, iDay)
{   
	var VicPopCal = document.getElementById("VicPopCal");
	var format = canlendarObj.formate;
	format = getCalendarFormat(format);
	
	var isFull = canlendarObj.isFullDate;
	
	VicPopCal.style.visibility = "hidden";
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		gdCtrl_tow.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{			
		var hhOfTime = document.getElementById("ssb_HH") ? parseInt(document.getElementById("ssb_HH").value, 10) : "0";
		var mmOfTime = document.getElementById("ssb_mm") ? parseInt(document.getElementById("ssb_mm").value, 10) : "0";
		var ssOfTime = document.getElementById("ssb_ss") ? parseInt(document.getElementById("ssb_ss").value, 10) : "0";
		
		hhOfTime = !isNaN(hhOfTime) ?  (hhOfTime < 0 ? "0" : (hhOfTime > 23 ? 23 : hhOfTime)) : "0";
		mmOfTime = !isNaN(mmOfTime) ? (mmOfTime < 0 ? "0" : (mmOfTime > 59 ? 59 : mmOfTime)) : "0";
		ssOfTime = !isNaN(ssOfTime) ? (ssOfTime < 0 ? "0" : (ssOfTime > 59 ? 59 : ssOfTime)) : "0";	
		
		
		var valueOfSetted = getCalendarTimefromUTC(format, new Date(iYear, iMonth - 1, iDay, hhOfTime, mmOfTime, ssOfTime).getTime());
		
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = valueOfSetted;
		}
		else
		{
			gdCtrl.innerText = valueOfSetted;
		}
		
		if(isFull == "false")
		{
			gdCtrl_tow.value = (getHhMmSs() == undefined) ? "" : getHhMmSs();
		}else
		{
			gdCtrl_tow.value = valueOfSetted;
		}
	}
	
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	
	gdCtrl.onchange();
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function HiddenDiv()
{
	var i;
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	document.getElementById('VicPopCal').style.visibility = "hidden";
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	goSelectTag.length = 0;
};

/**
 * \u83b7\u5f97\u9009\u62e9\u503c
 * @param {Object} aCell
 */
function fSetSelected(aCell)
{   
	Hid = true;
	var iOffset = 0;
	giYear = parseInt(document.getElementById("tbSelYear").value);
	giMonth = parseInt(document.getElementById("tbSelMonth").value);
	var iYear = giYear;
	var iMonth = giMonth;
	aCell.className = "tdOnMouseOut";
    
	var nodeobj="";
    
    if(!isIE()){	
	  nodeobj=aCell.childNodes;

		var iDay = parseInt(nodeobj[0].textContent);//innerText
		
		if (nodeobj[0].className == "fontOutMonth")
		{
			iOffset = (nodeobj[0].getAttribute("Victor")<10)?-1:1;//zhanghongwei 2007-10-11
		}
		
		iMonth += iOffset;
		if (iMonth<1)
		{
			iYear--;
			iMonth = 12;
		}
		else if (iMonth>12)
		{
			iYear++;
			iMonth = 1;
		}

	}
	else{
      	nodeobj=aCell.children["cellText"];
      	
		with (nodeobj)
		{
			var iDay = parseInt(innerText);
			
			if (nodeobj.className == "fontOutMonth")
			{
				iOffset = (Victor<10)?-1:1;				
			}
			
			iMonth += iOffset;
			if (iMonth<1)
			{
				iYear--;
				iMonth = 12;
			}
			else if (iMonth>12)
			{
				iYear++;
				iMonth = 1;
			}
		}
	}


	fSetDate(iYear, iMonth, iDay);
};

/**
 * \u8bbe\u7f6e\u9009\u62e9\u65e5\u671f
 * @param {Object} aCell
 */
function fSetSelectedDate(aCell)
{	
	var selectedObj = document.getElementById('divOnSelect');
	if(selectedObj != null)
	{
		var parentDivObj = selectedObj.parentNode;
		var childDivObjs = selectedObj.childNodes;
		parentDivObj.removeChild(selectedObj);
		for(var i = 0; i < (childDivObjs != null ? childDivObjs.length : 0); i++)
		{
			parentDivObj.appendChild(childDivObjs[i]);
		}
	}
	
	var fontObj = null;
	var childObjs = aCell.childNodes;
	var divObj = document.createElement("div");
	divObj.setAttribute('id',"divOnSelect");
	if(isIE())
	{
		divObj.setAttribute('className',"divOnSelect");
	}
	else
	{
		divObj.setAttribute('class',"divOnSelect");
	}
	
	for(var i = 0; i < (childObjs != null ? childObjs.length : 0); i++)
	{
		if(childObjs[i].tagName == "FONT")
		{
			fontObj = childObjs[i];
		}
		divObj.appendChild(childObjs[i]);
	}
	
	aCell.appendChild(divObj);	
	
	giYear = isNaN(parseInt(document.getElementById("tbSelYear").value)) ? 0 : parseInt(document.getElementById("tbSelYear").value);
	giMonth = isNaN(parseInt(document.getElementById("tbSelMonth").value)) ? 0 : parseInt(document.getElementById("tbSelMonth").value);
	giDay = isNaN(parseInt(isIE() ? aCell.innerText : aCell.textContent)) ? 0 : parseInt(isIE() ? aCell.innerText : aCell.textContent);
	
	var iOffset = 0;
	if (fontObj.className == "fontOutMonth")
	{
		iOffset = (fontObj.getAttribute("Victor") < 10) ? -1 : 1;				
	}
			
	giMonth += iOffset;
	if (giMonth < 1)
	{
		giYear--;
		giMonth = 12;
	}
	else if (giMonth > 12)
	{
		giYear++;
		giMonth = 1;
	}
	
}

function Point(iX, iY)
{
	this.x = iX;
	this.y = iY;
};

function fBuildCal(iYear, iMonth, calID)
{	
	var aMonth=new Array();
	for(i=1;i<7;i++)
	{
		aMonth[i]=new Array(i);
	}
	var startDay = startDayFormatMap[calID];
	var startDayOffset = getPanelStartDayOffset(startDay);
	var dCalDate=new Date(iYear, iMonth-1, 1);
	var iDayOfFirst=dCalDate.getDay()<startDayOffset ? dCalDate.getDay()-startDayOffset+7 : dCalDate.getDay()-startDayOffset;
	var iDaysInMonth=new Date(iYear, iMonth, 0).getDate();
	var iOffsetLast=new Date(iYear, iMonth-1, 0).getDate()-iDayOfFirst+1;
	var iDate = 1;
	var iNext = 1;
	for (d = 0; d < 7; d++)
	{
		aMonth[1][d] = (d<iDayOfFirst)?-(iOffsetLast+d):iDate++;
	}
	for (w = 2; w < 7; w++)
	{
		for (d = 0; d < 7; d++)
		{
			aMonth[w][d] = (iDate<=iDaysInMonth)?iDate++:-(iNext++);
		}
	}
	return aMonth;
};

function getPanelStartDayOffset(startDay)
{
	if (startDay != null && startDay != "")
	{
		startDay = startDay.toLowerCase();
		switch(startDay)
		{
			case "sunday" : 	
				return 0;
			case "monday" : 	
				return 1;
			case "tuesday" : 	
				return 2;
			case "wednesday" : 	
				return 3;
			case "thursday" : 	
				return 4;
			case "friday" : 	
				return 5;
			case "saturday" : 	
				return 6;
			default :			
				return 0;
		}
	}
	else
	{
		return 0;
	}
};

function fDrawCal(iYear, iMonth, enableSelect, calID)
{
	//\u662f\u5426\u5141\u8bb8\u70b9\u51fb\u65e5\u671f\u5b8c\u6210\u65e5\u671f\u65f6\u95f4\u9009\u62e9
	var enable = enableSelect != null ? (enableSelect == true || enableSelect == "true" ? true : false) : ture;
	
	var divHtml = '';
	var WeekDay = new Array($res_entry('ui.control.calendar.Sunday','\u65e5'),$res_entry('ui.control.calendar.Monday','\u4e00'),$res_entry('ui.control.calendar.Tuesday','\u4e8c'),$res_entry('ui.control.calendar.Wednesday','\u4e09'),$res_entry('ui.control.calendar.Thursday','\u56db'),$res_entry('ui.control.calendar.Friday','\u4e94'),$res_entry('ui.control.calendar.Saturday','\u516d'));
	var startDay = startDayFormatMap[calID];
	var startDayOffset = getPanelStartDayOffset(startDay);	
	for (var k = startDayOffset; k > 0; k--)
	{
		WeekDay.push(WeekDay.shift());
	}
	
	divHtml = divHtml + "<tr>";
	for(i=0; i<7; i++)
	{
		divHtml = divHtml + "<td class='tdCalendarPanelHead'>"+ WeekDay[i] + "</td>";
	}
	divHtml = divHtml + "</tr>";
	for (w = 0; w < 6; w++)
	{
		divHtml = divHtml +  "<tr>";
		for (d = 0; d < 7; d++)
		{
			divHtml = divHtml + "<td id='calCell' class='tdCalendarPanelCell' onmouseover='mouseOverTDAction(this)' onmouseout='mouseOutTDAction(this)'";
			//enable\u4e3atrue\uff0c\u5219\u70b9\u51fb\u65e5\u671f\u9762\u677f\u5373\u53ef\u5b8c\u6210\u65f6\u95f4\u9009\u62e9\uff0c\u8c03\u7528\u51fd\u6570onclick='fSetSelected(this)'
			if(enable)
			{
				divHtml = divHtml + "onclick='fSetSelected(this)'>";
			}
			//enable\u4e3afalse\uff0c\u5219\u70b9\u51fb\u65e5\u671f\u9762\u677f\u53ea\u663e\u793a\u9009\u62e9\u65e5\u671f\u80cc\u666f\uff0c\u8c03\u7528\u51fd\u6570onclick='fSetSelectedDate(this)'
			else
			{
				divHtml = divHtml + "onclick='fSetSelectedDate(this)'>";
			}
			divHtml = divHtml + "<font id='cellText' name='cellText' Victor='Liming Weng'> </font>";
			divHtml = divHtml + "</td>";
		}
		divHtml = divHtml + "</tr>";
	}
	return divHtml;
};

function mouseOverQuickSelectTDAction(obj)
{
	obj.className = "tdOnMouseOverQuickSelect";
}

function mouseOutQuickSelectTDAction(obj)
{
	obj.className = "tdOnMouseOutQuickSelect";
}

function mouseOverTDAction(obj)
{	
	obj.className = "tdOnMouseOver";
}

function mouseOutTDAction(obj)
{
	obj.className = "tdOnMouseOut";
}

var onfocusobj =null;
function selectHour(obj)
{
	onfocusobj =obj;
	obj.select();
};
function addTime()
{
	if(onfocusobj==undefined)
	{
		onfocusobj =document.getElementsByName('dpHour');
	}
	intTe=parseInt(onfocusobj.value);
	if(onfocusobj.id == 'dpHour') 
	{
		 if(intTe==23) 
		{
			intTe=0;   
		}
		else
		{
			intTe = intTe +1;   
		}
	}
	else
	{
		if(intTe==59)
		{
			intTe=0;
		}
		else
		{
			intTe = intTe +1;
		}
	}
	onfocusobj.value =intTe ;
};
function jianTime()
{
	 if(onfocusobj==null)
	{ 
		onfocusobj =document.getElementsByName('dpHour');  
	}
	intTe=parseInt(onfocusobj.value);  
	 if(intTe>1)
	{
		 intTe =intTe- 1;
	}
	else
	{
		 if(onfocusobj.id == 'dpHour')
		{
			intTe = 23;
		}
		else
		{
			intTe = 59; 
		}
	}
	onfocusobj.value = intTe;
};
function NotHid()
{
	Hid = false;
};
function fUpdateCal(iYear, iMonth, calID)
{   
	Hid = false
	myMonth = fBuildCal(iYear, iMonth, calID);
	 var i = 0;
	 var nn = new Date();
	 var cYear = nn.getFullYear();
	 var startDay = startDayFormatMap[calID];
	 var startDayOffset = getPanelStartDayOffset(startDay);
	 var cDay = nn.getDate();
	 var cMonth = nn.getMonth()+1;
	 var Marked = false;
	 var Started = false;
	
	for (w = 0; w < 6; w++)
	{
		for (d = 0; d < 7; d++)
		{	
			if(isIE()){
				cellText[(7*w)+d].Victor = i++;
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].className = "fontOutMonth";
					cellText[(7*w)+d].innerText = -myMonth[w+1][d];
				}
				else
				{
					if (startDayOffset == 0) {
						cellText[(7*w)+d].className = ((d==startDayOffset)||(d==6-startDayOffset))? "fontWeekend" : "fontInMonth";
						cellText[(7*w)+d].innerText = myMonth[w+1][d];
					} else {
						cellText[(7*w)+d].className = ((d==7-startDayOffset)||(d==6-startDayOffset))? "fontWeekend" : "fontInMonth";
						cellText[(7*w)+d].innerText = myMonth[w+1][d];
					}
				}
				if (cellText[(7*w)+d].innerText == 1) 
				{
					Started = true;
				}
				
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if ((cDay != giDay || (cDay == giDay && (cMonth != giMonth || cYear != giYear))) && parseInt(giDay) == cellText[(7*w)+d].innerText && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
					Marked = true; color = "red"; 
					cellText[(7*w)+d].innerHTML = "<div class=\"divLastSelectDay\">" + cellText[(7*w)+d].innerText + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{
					color = "red"; cellText[(7*w)+d].innerHTML = "<div class=\"divToday\">" + cellText[(7*w)+d].innerText + "</div>";
				}
			
			}
			else {  //edit by chris on 2007-10-09,on firefox
				cellText=document.getElementsByName("cellText"); 
				cellText[(7*w)+d].setAttribute("Victor",i++);
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].className = "fontOutMonth";
					cellText[(7*w)+d].textContent = -myMonth[w+1][d]; 							
				} else {
					if (startDayOffset == 0) {
						cellText[(7*w)+d].className = ((d==startDayOffset)||(d==6-startDayOffset))? "fontWeekend" : "fontInMonth";
						cellText[(7*w)+d].textContent = myMonth[w+1][d];
					} else {
						cellText[(7*w)+d].className = ((d==7-startDayOffset)||(d==6-startDayOffset))? "fontWeekend" : "fontInMonth";
						cellText[(7*w)+d].textContent = myMonth[w+1][d];
					}
				}
				if (cellText[(7*w)+d].textContent == 1) 
				{
					Started = true;
				}
				
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if ((cDay != giDay || (cDay == giDay && (cMonth != giMonth || cYear != giYear))) && parseInt(giDay) == cellText[(7*w)+d].textContent && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
				    Marked = true; color = "red"; 
				    cellText[(7*w)+d].innerHTML = "<div class=\"divLastSelectDay\">" + cellText[(7*w)+d].textContent + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{ 
					color = "red"; cellText[(7*w)+d].innerHTML = "<div class=\"divToday\">" + cellText[(7*w)+d].textContent + "</div>";
				}
			}  
		}
	}
};

function setQuickSelectTime(format, hour, minute, second)
{
	if(format == null || Trim(format) == "")
	{
		format = "HH:mm:ss";
	}
	
	var tHour = !isNaN(parseInt(hour, 10)) ? (parseInt(hour, 10) < 10 ? "0" + parseInt(hour, 10) : hour) : "00";
	var tMinute = !isNaN(parseInt(minute, 10)) ? (parseInt(minute, 10) < 10 ? "0" + parseInt(minute, 10) : minute) : "00";
    var tMinute1 = "00";
	var tMinute2 = "15"; 
	var tMinute3 = "30"; 
	var tMinute4 = "45"; 
	var tMinute5 = "59"; 
	var tSecond = !isNaN(parseInt(second, 10)) ? (parseInt(second, 10) < 10 ? "0" + parseInt(second, 10) : second) : "00";
	
	var quickSelectTime1 = format;
	var quickSelectTime2 = format;
	var quickSelectTime3 = format;
	var quickSelectTime4 = format;
	var quickSelectTime5 = format;
	
	if(format.indexOf("HH") >= 0)
	{
		quickSelectTime1 = quickSelectTime1.replace("HH", tHour);
		quickSelectTime2 = quickSelectTime2.replace("HH", tHour);
		quickSelectTime3 = quickSelectTime3.replace("HH", tHour);
		quickSelectTime4 = quickSelectTime4.replace("HH", tHour);
		quickSelectTime5 = quickSelectTime5.replace("HH", tHour);
	}
	if(format.indexOf("mm") >= 0)
	{
		quickSelectTime1 = quickSelectTime1.replace("mm", tMinute1);
		quickSelectTime2 = quickSelectTime2.replace("mm", tMinute2);
		quickSelectTime3 = quickSelectTime3.replace("mm", tMinute3);
		quickSelectTime4 = quickSelectTime4.replace("mm", tMinute4);
		quickSelectTime5 = quickSelectTime5.replace("mm", tMinute5);
	}
	if(format.indexOf("ss") >= 0)
	{
		quickSelectTime1 = quickSelectTime1.replace("ss", tSecond);
		quickSelectTime2 = quickSelectTime2.replace("ss", tSecond);
		quickSelectTime3 = quickSelectTime3.replace("ss", tSecond);
		quickSelectTime4 = quickSelectTime4.replace("ss", tSecond);
		quickSelectTime5 = quickSelectTime5.replace("ss", tSecond);
	}
	document.getElementById("quickSelectTime1").innerHTML = "&nbsp;&nbsp;" + quickSelectTime1;
	document.getElementById("quickSelectTime2").innerHTML = "&nbsp;&nbsp;" + quickSelectTime2; 
	document.getElementById("quickSelectTime3").innerHTML = "&nbsp;&nbsp;" + quickSelectTime3; 
	document.getElementById("quickSelectTime4").innerHTML = "&nbsp;&nbsp;" + quickSelectTime4; 
	document.getElementById("quickSelectTime5").innerHTML = "&nbsp;&nbsp;" + quickSelectTime5; 
	
	if(!timeInitialized) {
		if(document.getElementById("quickSelect_HH") != null)
		{
			document.getElementById("quickSelect_HH").value = tHour;
		}
		if(document.getElementById("quickSelect_mm") != null)
		{
			document.getElementById("quickSelect_mm").value = tMinute;
		}
		if(document.getElementById("quickSelect_ss"))
		{
			document.getElementById("quickSelect_ss").value = tSecond;
		}
		timeInitialized = true;
	}
}
var timeInitialized = false;

function fSetYearMon(iYear, iMon, calID)
{	
	if(document.getElementById("tbSelMonth") == null || document.getElementById("tbSelYear") == null)
	{
		return false;
	}
	
	document.getElementById("tbSelMonth").options[iMon-1].selected = true;
	
	for (var i = 0; i < document.getElementById("tbSelYear").length; i++)
	{
		if (document.getElementById("tbSelYear").options[i].value == iYear)
		{
			document.getElementById("tbSelYear").options[i].selected = true;
		}
	}
	fUpdateCal(iYear, iMon, calID);
};
function fPrevMonth(calID)
{//alert(calID);//debugger;
	Hid = false;
	var iMon =  document.getElementById("tbSelMonth").value;
	var iYear = document.getElementById("tbSelYear").value;
	
	
	if (--iMon<1) 
	{
			iMon = 12;
			iYear--;
	}
	fSetYearMon(iYear, iMon, calID);
};
function fNextMonth(calID)
{//alert(calID)
	Hid = false;
	
	var iMon =  document.getElementById("tbSelMonth").value;//zhanghongwei
    var iYear = document.getElementById("tbSelYear").value;//zhanghongwei
	if (++iMon>12) 
	{
			iMon = 1;
			iYear++;
	}
	fSetYearMon(iYear, iMon, calID);
};
function fToggleTags()
{
	with (document.getElementsByTagName("select"))//
	{
		var temp=null;
		for (i=0; i<length; i++) {
			temp=item(i).getAttribute("Victor");//edit by chris on 2007-10-09
				//\u5e94\u5f53\u5224\u65adtemp\u662f\u5426\u4e3anull\uff0c\u5426\u5219\u5b83\u4f1a\u628adiv\u8986\u76d6\u5230\u7684\u4e0b\u62c9\u6846\u5168\u90fd\u9690\u85cf\uff01wxr 2008-12-23
				if ((temp!=null)&&(temp!="Won")&&fTagInBound(item(i))) {
						item(i).style.visibility = "hidden";
						goSelectTag[goSelectTag.length] = item(i);
				}
		}
	}
};
function fTagInBound(aTag)
{
	var VicPopCal = document.getElementById('VicPopCal');
	with (VicPopCal.style)
	{
			var l = parseInt(left);
			var t = parseInt(top);
			var r = l+parseInt(width);
			var b = t+parseInt(height);
			var ptLT = fGetXY(aTag);
			return !((ptLT.x>r)||(ptLT.x+aTag.offsetWidth<l)||(ptLT.y>b)||(ptLT.y+aTag.offsetHeight<t));
	}
};

function fGetXY(aTag)
{
	var oTmp1 = aTag;
	var oTmp2 = aTag;
	var pt = new Point(0,0);
	pt.x = oTmp1.offsetLeft;
    pt.y = oTmp1.offsetTop;

	while (oTmp1 = oTmp1.offsetParent) {
        var oTmp3 = oTmp1.parentNode;
        while (oTmp3 != (oTmp2 = oTmp2.parentNode)) {
            pt.x -= oTmp2.scrollLeft;
            pt.y -= oTmp2.scrollTop;
        }
        pt.x += oTmp1.offsetLeft;
        pt.y += oTmp1.offsetTop;
        oTmp2=oTmp1;
    }
	
	return pt;
}; 

/*
function fGetXY(aTag)
{

	var oTmp = aTag;
	
	var pt = new Point(0,0);
	do 
	{
			pt.x += oTmp.offsetLeft;
			pt.y += oTmp.offsetTop;
			oTmp = oTmp.offsetParent;
	}
	while(oTmp!=null && oTmp.tagName!="BODY");
	
	return pt;
};
*/

divHtml = '';
function HidChange(boVis)
{
	Hid = boVis;
};

function setHhMmSs()
{
	var dataObject = new Date();
	dataObject.setHours(giHh, giMm, giSs);
	try{
	document.getElementById("ssb_HH").value = new String(dataObject.getHours()).length == 1? "0"+dataObject.getHours():dataObject.getHours();
	document.getElementById("ssb_mm").value = new String(dataObject.getMinutes()).length == 1? "0"+dataObject.getMinutes():dataObject.getMinutes();
	document.getElementById("ssb_ss").value = new String(dataObject.getSeconds()).length == 1? "0"+dataObject.getSeconds():dataObject.getSeconds();
	}catch(e)
	{
		return ;
	}
}
function getHhMmSs()
{
	try{
	var hh_mm_ss = "";
	var hh = document.getElementById("ssb_HH").value;
	var mm = document.getElementById("ssb_mm").value;
	var ss = document.getElementById("ssb_ss").value;
	
	hh = parseInt(hh, 10) < 0 ? "00" : (parseInt(hh, 10) > 23 ? "23" : hh);
	mm = parseInt(mm, 10) < 0 ? "00" : (parseInt(mm, 10) > 59 ? "59" : mm);
	ss = parseInt(ss, 10) < 0 ? "00" : (parseInt(ss, 10) > 59 ? "59" : ss);
	
	hh = hh.length ==1? "0"+hh:hh;
	mm = mm.length ==1? "0"+mm:mm;
	ss = ss.length ==1? "0"+ss:ss;
	hh_mm_ss = hh_mm_ss+hh+":"+mm+":"+ss;
	return hh_mm_ss;
	}catch(e){
		return ;
	}
}

function checkHours(o)
{	
	if(isNaN(parseInt(o.value, 10)))
	{
		o.value = "00";
		o.select();
	}
	else if(parseInt(o.value, 10) > 23 || parseInt(o.value, 10) < 0)
	{
		o.value = parseInt(o.value, 10) > 23 ? "23" : "00";
		o.select();
	}
	else if(parseInt(o.value, 10) < 10)
	{
		o.value = "0" + parseInt(o.value, 10);
	}
}
function checkMinutes(o)
{
	if(isNaN(parseInt(o.value)))
	{
		o.value = "00";
		o.select();
	}
	else if(parseInt(o.value) > 59 || parseInt(o.value) < 0)
	{
		o.value = parseInt(o.value) > 59 ? "59" : "00";
		o.select();
	}
	else if(parseInt(o.value, 10) < 10)
	{
		o.value = "0" + parseInt(o.value, 10);
	}
}
function checkSeconds(o)
{
	if(isNaN(parseInt(o.value)))
	{
		o.value = "00";
		o.select();
	}
	else if(parseInt(o.value) > 59 || parseInt(o.value) < 0)
	{
		o.value = parseInt(o.value) > 59 ? "59" : "00";
		o.select();
	}
	else if(parseInt(o.value, 10) < 10)
	{
		o.value = "0" + parseInt(o.value, 10);
	}
}

function modifyQuickSelect(format)
{ 
	var hour = document.getElementById("quickSelect_HH") != null ? document.getElementById("quickSelect_HH").value : 0;
	var minute = document.getElementById("quickSelect_mm") != null ? document.getElementById("quickSelect_mm").value : 0;
	var second = document.getElementById("quickSelect_ss") != null ? document.getElementById("quickSelect_ss").value : 0;
	setQuickSelectTime(format, hour, minute, second);
}

var Timer_name = "";

function fPopCalendar(popCtrl, dateCtrl, formate, yearRange, ctrl2, calID)
{  
	timeInitialized = false;
	if(formate == "null" || formate == "undefined" || formate == null || formate == "")
	{	
		formate = DEFAULT_DATE_FORMAT;
	}
	
	formate = getCalendarFormat(formate);
	
	//\u66f4\u65b0\u5f53\u524d\u65f6\u95f4
	curYear = new Date().getFullYear();
	curMonth = new Date().getMonth()+1;
	curDay = new Date().getDate();

	//wanghao4
	canlendarObj.formate = formate;
	popCtrl =document.getElementById(ctrl2);
	while(popCtrl.tagName == undefined || popCtrl.tagName.toLowerCase() != "img")
	{
		popCtrl = popCtrl.nextSibling;
	}
	var strDate = null;
	strDate = Trim(document.getElementById(dateCtrl).value)
	
	if(strDate != null && strDate != "")
	{
		var utcTime = getCalendarUTCfromTime(formate, strDate);
		if(utcTime != null)
		{
			var tmpDate = new Date(utcTime);
			giYear = tmpDate.getFullYear();
			giMonth = tmpDate.getMonth() + 1;
			giDay = tmpDate.getDate();
			giHh = tmpDate.getHours();
			giMm = tmpDate.getMinutes();
			giSs = tmpDate.getSeconds();
		}
	}
	else
	{
		var curDate = new Date();
		
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay= curDate.getDate();
		
		giHh = curDate.getHours();
		giMm = curDate.getMinutes();
		giSs = curDate.getSeconds();
	}
	var c = document.getElementById('VicPopCal');
	//if(c == null)
	//{
		var divHtml = '';
		var iframe = document.getElementById("iframe_id");
		if (iframe == null ) {
			var iframe = document.createElement("iframe");
			iframe.setAttribute('id',"iframe_id");
			document.body.appendChild(iframe);	
			
			var divCal = document.createElement("div");
			divCal.id="VicPopCal";
			document.body.appendChild(divCal);
		}
		
			
		
		

		document.onclick = function (event)
		{	
			var e = event || window.event;
			var elem = e.srcElement||e.target;
			if(elem.tagName.toLowerCase() == "img" && elem.id == "calendar_img_id")
			{
				return;
			}	
			while(elem)
			{ 
				if(elem.id == "VicPopCal")
				{
						return;
				}
				elem = elem.parentNode;		
			}
			
			HiddenDiv();
		}
		//\u65e5\u5386\u63a7\u4ef6\u53ea\u6709\u65f6\u5206\u79d2\u7684DIV
		divHtml = divHtml + "<DIV id='onlyTime'><TABLE class='tbQuickSelectTimePanel'><TBODY>";
		divHtml = divHtml + "<TR><TD class='tdQuickSelectHead'><DIV class='divQuickSelectHead'>" + $res_entry('ui.control.calendar.time.quickSelect','\u5feb\u901f\u9009\u62e9') + "</DIV></TD></TR>";
		divHtml = divHtml + "<TR><TD><TABLE class='tbQuickSelectTime'><TBODY>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectCell1' class='tdQuickSelectCell' onmousedown='' onmouseover='mouseOverQuickSelectTDAction(this)' onmouseout='mouseOutQuickSelectTDAction(this)' noWrap><DIV id='quickSelectTime1'></IDV></TD></TR>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectCell2' class='tdQuickSelectCell' onmousedown='' onmouseover='mouseOverQuickSelectTDAction(this)' onmouseout='mouseOutQuickSelectTDAction(this)' noWrap><DIV id='quickSelectTime2'></IDV></TD></TR>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectCell3' class='tdQuickSelectCell' onmousedown='' onmouseover='mouseOverQuickSelectTDAction(this)' onmouseout='mouseOutQuickSelectTDAction(this)' noWrap><DIV id='quickSelectTime3'></IDV></TD></TR>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectCell4' class='tdQuickSelectCell' onmousedown='' onmouseover='mouseOverQuickSelectTDAction(this)' onmouseout='mouseOutQuickSelectTDAction(this)' noWrap><DIV id='quickSelectTime4'></IDV></TD></TR>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectCell5' class='tdQuickSelectCell' onmousedown='' onmouseover='mouseOverQuickSelectTDAction(this)' onmouseout='mouseOutQuickSelectTDAction(this)' noWrap><DIV id='quickSelectTime5'></IDV></TD></TR>";
		divHtml = divHtml + "</TBODY></TABLE></TD></TR>";
		divHtml = divHtml + "<TR><TD id='tdQuickSelectHHmmss' class='tdQuickSelectHHmmss' noWrap>";
		divHtml = divHtml + "</TD></TR><TR><TD id='tdQuickSelectBtn' class='tdQuickSelectBtn' noWrap>";
		divHtml = divHtml + "<input id='quickSelectClearBtn' class='button' type='button' value='" + $res_entry('ui.control.calendar.clear','\u6e05\u7a7a') + "' onClick=''/>";
		
		divHtml = divHtml + "<input id='quickSelectConfirmBtn' class='button' type='button' value='" + $res_entry('ui.control.calendar.confirm','\u786e\u5b9a') + "' onClick=''/>";
		divHtml = divHtml + "</TD></TR></TBODY></TABLE></DIV>";
		document.getElementById("VicPopCal").innerHTML = divHtml;
		//\u65e5\u5386\u63a7\u4ef6\u62e5\u6709\u5e74\u6708\u65e5\uff0c\u53ef\u4ee5\u62e5\u6709\u65f6\u5206\u79d2\u7684DIV
		divHtml = divHtml + "<DIV id='dateAndTime'><table class='tableCalendar'>";
		divHtml = divHtml + "<tr>";	
		divHtml = divHtml + "<td class='tdCenter'><input type='button' css='button' name='PrevMonth' value='<' class='prevMonthBtn' onclick='fPrevMonth(\"" + calID + "\")' />";
		divHtml = divHtml + "&nbsp;<select id='tbSelYear' name='tbSelYear'onchange='fUpdateCal(document.getElementById(\"tbSelYear\").value, document.getElementById(\"tbSelMonth\").value,\"" + calID +"\")' Victor='Won' class='yearSelect'>";
		divHtml = divHtml + "</select>";
		divHtml = divHtml + "&nbsp;<select id='tbSelMonth' name='tbSelMonth' onchange='fUpdateCal(document.getElementById(\"tbSelYear\").value, document.getElementById(\"tbSelMonth\").value,\"" + calID +"\")' Victor='Won' class='monthSelect'>";
		for (i=0; i<12; i++)
		{
			divHtml = divHtml + "<option value='"+(i+1)+"'>"+gMonths[i]+"</option>";
		}
		divHtml = divHtml + "</select>";
		divHtml = divHtml + "&nbsp;<input type='button' css='button' name='PrevMonth' value='>' class='nextMonthBtn' onclick='fNextMonth(\"" + calID + "\")' />";
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</tr><tr>";
		divHtml = divHtml + "<td class='tdCenter'>";
	
		divHtml = divHtml + "<div id='divCalendarPanel'></div>"
		
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</tr><tr><td class='tdCenter'>";
		divHtml = divHtml + "<table id='tbhms' class='tbTodayAndTimePanel'>";
		divHtml = divHtml + "<tr><td class='tdCenter'>";
		divHtml = divHtml + "</td></tr>";
		
		divHtml = divHtml + "</table>";
		divHtml = divHtml + "</td></tr>";
		//\u589e\u52a0\u786e\u5b9a\u6309\u94ae\uff0c\u66f4\u6539\u6e05\u7a7a\u3001\u4eca\u5929\u7684\u8d85\u94fe\u63a5\u65b9\u5f0f\u4e3a\u6309\u94ae\u65b9\u5f0f
		divHtml = divHtml + "<tr><td id='tdOperationBtn' class='tdOperationBtn'>";
		divHtml = divHtml + "</td></tr>";
		divHtml = divHtml + "</table></DIV>";
		document.getElementById("VicPopCal").innerHTML = divHtml;
		
		//\u4e0d\u540c\u65e5\u5386\u9700\u8981\u6709\u4e0d\u540c\u7684\u9009\u62e9\u5e74\u9650\u7684\u8303\u56f4
		var yearNow = new Date().getFullYear();
		var range = !isNaN(parseInt(yearRange)) ? parseInt(yearRange) : 100;
		var yearSelect = document.getElementById("tbSelYear");
		if(yearSelect != null)
		{
			yearSelect.options.length = 0;
			for(i = yearNow - range; i <= yearNow + range; i++)
			{
				yearSelect.options.add(new Option(i+$res_entry('ui.control.calendar.year','\u5e74'), i));
			}
		}
	//}
	
	//\u5982\u679cformat\u53ea\u6709\u65f6\u95f4\u90e8\u5206\uff0c\u5219\u663e\u793aid\u4e3aonlyTime\u7684DIV
	if(formate.indexOf("YY") < 0 && formate.indexOf("MM") < 0 && formate.indexOf("dd") < 0 && (formate.indexOf("HH") >= 0 || formate.indexOf("mm") >= 0 || formate.indexOf("ss") >= 0))
	{
		document.getElementById('onlyTime').style.display = "";
		document.getElementById('dateAndTime').style.display = "none";
		
		var divTimeInputHtml = $res_entry('ui.control.calendar.time.time','\u65f6\u95f4') + "&nbsp&nbsp";

		if(formate.indexOf("HH") >= 0)
		{
			divTimeInputHtml = divTimeInputHtml + "<input id='quickSelect_HH' ondblclick='this.selected=true' type='text' value='00' size='2' maxlength='2' onblur='checkHours(this);modifyQuickSelect(\""+formate+"\");' />:";
		}
		if(formate.indexOf("mm") >= 0)
		{
			divTimeInputHtml = divTimeInputHtml + "<input id='quickSelect_mm' ondblclick='this.selected=true' type='text' value='00' size='2' maxlength='2' onblur='checkMinutes(this);modifyQuickSelect(\""+formate+"\");' />";
		}
		if(formate.indexOf("ss") >= 0)
		{
			divTimeInputHtml = divTimeInputHtml + ":<input id='quickSelect_ss' ondblclick='this.selected=true' type='text' value='00' size='2' maxlength='2' onblur='checkSeconds(this);modifyQuickSelect(\""+formate+"\")' />";
		}
		document.getElementById("tdQuickSelectHHmmss").innerHTML = divTimeInputHtml;
		
		var functionMethod ="fSetQuickTime('"+formate+"',-1,-1,-1);";
		document.getElementById("quickSelectClearBtn").onclick = Function(functionMethod);
		
		functionMethod ="confirmQuickTime('"+formate+"');";
		document.getElementById("quickSelectConfirmBtn").onclick = Function(functionMethod);
		
		functionMethod = "selectQuickTime('"+formate+"',this);";
		document.getElementById("tdQuickSelectCell1").onmousedown = Function(functionMethod);
		document.getElementById("tdQuickSelectCell2").onmousedown = Function(functionMethod);
		document.getElementById("tdQuickSelectCell3").onmousedown = Function(functionMethod);
		document.getElementById("tdQuickSelectCell4").onmousedown = Function(functionMethod);
		document.getElementById("tdQuickSelectCell5").onmousedown = Function(functionMethod);
		
		setQuickSelectTime(formate, giHh, giMm, giSs);
	}
	//\u5982\u679cformat\u62e5\u6709\u65e5\u671f\u90e8\u5206\uff0c\u53ef\u4ee5\u62e5\u6709\u65f6\u95f4\u90e8\u5206\uff0c\u5219\u663e\u793aid\u4e3adateAndTime\u7684DIV
	else
	{
		document.getElementById('onlyTime').style.display = "none";
		document.getElementById('dateAndTime').style.display = "";
		
		//\u4e3a\u4e86\u4e0d\u540c\u7684\u65e5\u5386\u4f1a\u6709\u4e0d\u540c\u7684\u663e\u793a\u98ce\u683c, \u6240\u4ee5\u4e0d\u5e94\u8be5\u628a\u662f\u5426\u663e\u793a\u65f6\u5206\u79d2\u653e\u5728\u5176\u5b83\u5224\u65ad\u91cc\u9762, \u5b83\u53ea\u4e0eformate\u6709\u5173
		if(formate.indexOf("HH") >= 0 || formate.indexOf("mm") >= 0 || formate.indexOf("ss") >= 0)
		{
			if(document.getElementById("trhms") == null)
			{
				hms = "<tr id='trhms'><td align='center'><span class='tdQuickSelectHHmmss'>" + $res_entry('ui.control.calendar.time.time','\u65f6\u95f4') + "&nbsp&nbsp</span>";
				hms = hms + "<input id='ssb_HH' type='text' value='00' name='ssb_HH' size='2' maxlength='2' onblur='checkHours(this)' />:";
				hms = hms + "<input id='ssb_mm' type='text' value='00' name='ssb_mm' size='2' maxlength='2' onblur='checkMinutes(this)' />:";
				hms = hms + "<input id='ssb_ss' type='text' value='00' name='ssb_ss' size='2' maxlength='2' onblur='checkSeconds(this)' />";
				hms = hms + "</td></tr>";	
				$(hms).appendTo("#tbhms");
			}
			setHhMmSs();
		}
		else if($("#trhms"))
		{
			$("#trhms").remove();
		}
		
		var divCalendarPanelHtml = "<table id='tableCalendarPanel' class='tableCalendarPanel'>";
		//\u65e5\u671f\u9009\u62e9\u9762\u677f\u53ea\u62e5\u6709\u65e5\u671f\u65f6\uff0c\u652f\u6301\u70b9\u51fb\u65e5\u671f\u9009\u62e9\u9762\u677f\u6765\u9009\u62e9\u65f6\u95f4
		if((formate.indexOf("HH") >= 0 || formate.indexOf("mm") >= 0 || formate.indexOf("ss") >= 0) && (formate.indexOf("YY") >= 0 || formate.indexOf("MM") >= 0 || formate.indexOf("dd") >= 0))
		{
			divCalendarPanelHtml = divCalendarPanelHtml + fDrawCal(giYear, giMonth, false, calID);
		}
		//\u65e5\u671f\u9009\u62e9\u9762\u677f\u65e2\u62e5\u6709\u65e5\u671f\uff0c\u53c8\u62e5\u6709\u65f6\u5206\u79d2\u65f6\uff0c\u4e0d\u652f\u6301\u70b9\u51fb\u65e5\u671f\u9009\u62e9\u9762\u677f\u6765\u9009\u62e9\u65f6\u95f4
		else
		{
			divCalendarPanelHtml = divCalendarPanelHtml + fDrawCal(giYear, giMonth, true, calID);
		}
		divCalendarPanelHtml = divCalendarPanelHtml + "</table>";
		document.getElementById('divCalendarPanel').innerHTML = divCalendarPanelHtml;
		
		//\u589e\u52a0\u786e\u5b9a\u6309\u94ae\uff0c\u66f4\u6539\u6e05\u7a7a\u3001\u4eca\u5929\u7684\u8d85\u94fe\u63a5\u65b9\u5f0f\u4e3a\u6309\u94ae\u65b9\u5f0f
		var btnHtml = "<input id='clearBtn' class='button' type='button' value='" + $res_entry('ui.control.calendar.clear','\u6e05\u7a7a') + "' onClick='fSetDate(0,0,0)'/>";
		btnHtml = btnHtml + "<input id='todayBtn' class='button' type='button' value='" + $res_entry('ui.control.calendar.today','\u4eca\u5929') + "' onClick='fSetDate(new Date().getFullYear(),new Date().getMonth()+1,new Date().getDate())'/>";
		//\u5982\u679c\u65e5\u671f\u683c\u5f0f\u53ea\u5305\u542b\u65e5\u671f\u4e0d\u5305\u542b\u65f6\u95f4\uff0c\u4e0d\u73b0\u5b9e\u786e\u5b9a\u6309\u94ae
		if((formate.indexOf("HH") >= 0 || formate.indexOf("mm") >= 0 || formate.indexOf("ss") >= 0) && (formate.indexOf("YY") >= 0 || formate.indexOf("MM") >= 0 || formate.indexOf("dd") >= 0))
		{
			btnHtml = btnHtml + "<input id='confirmBtn' class='button' type='button' value='" + $res_entry('ui.control.calendar.confirm','\u786e\u5b9a') + "' onClick='fSetDate(giYear, giMonth, giDay)'/>";
		}
		
		document.getElementById('tdOperationBtn').innerHTML = btnHtml;
		
		fSetYearMon(giYear, giMonth, calID); 
	}
	
	var onlyNumInputIds = new Array("ssb_HH", "ssb_mm", "ssb_ss", "quickSelect_HH", "quickSelect_mm", "quickSelect_ss");
	var onlyNumRegExps = new Array("^((\\d)|((0|1)(\\d))|(2(0|1|2|3)))$","^((\\d)|((0|1|2|3|4)(\\d))|(5(0|1|2|3|4|5|6|7|8|9)))$","^((\\d)|((0|1|2|3|4)(\\d))|(5(0|1|2|3|4|5|6|7|8|9)))$","^((\\d)|((0|1)(\\d))|(2(0|1|2|3)))$","^((\\d)|((0|1|2|3|4)(\\d))|(5(0|1|2|3|4|5|6|7|8|9)))$","^((\\d)|((0|1|2|3|4)(\\d))|(5(0|1|2|3|4|5|6|7|8|9)))$");

	onlyInputNumber(onlyNumInputIds, onlyNumRegExps);
	
	if (popCtrl == previousObject)
	{
		var VicPopCal = document.getElementById('VicPopCal');
		if (VicPopCal.style.visibility == "visible")
		{
			HiddenDiv();
			return true;
		}
	}
	
	previousObject = popCtrl;
	
	gdCtrl = document.getElementById(dateCtrl);
	gdCtrl_tow = document.getElementById(ctrl2);

	var point = fGetXY(popCtrl);
	
	var VicPopCal = document.getElementById('VicPopCal');
	var iframe = document.getElementById("iframe_id");
	with (VicPopCal.style) 
	{
		if(point.x > 240) 
		{
			left = point.x+popCtrl.offsetWidth-255+"px";
		}
		else 
		{
			left = point.x+popCtrl.offsetWidth+"px";
		}
		top  = point.y+popCtrl.offsetHeight+"px";

		fToggleTags(point); 	//comment by chris on 2007-10-09,\u8fd9\u4e2a\u51fd\u6570\u4ec0\u4e48\u529f\u80fd\uff0c\u6ca1\u592a\u660e\u767d
		visibility = 'visible';
		iframe.style.display = "block";
	}
};

function onlyInputNumber(onlyNumInputIds, onlyNumRegExps){
	
	if(onlyNumInputIds == null || onlyNumRegExps == null)
	{
		return;
	}
	
	for ( var vii = 0; vii < onlyNumInputIds.length && vii < onlyNumRegExps.length; vii++ )
	{
		
		var inputId = onlyNumInputIds[vii];
		var regExpString = onlyNumRegExps[vii];
		if (inputId) 
		{
			if (o = document.getElementById(inputId)) 
			{
				o.setAttribute("regExp", regExpString);
				with (o) 
				{
					if(isIE())    //ie\u6d4f\u89c8\u5668 
					{	
						style.imeMode = "disabled";
						onchange = function (){
							value = value.replace(/\D/g,'');
						}/*		
						onkeydown = function (){
							return event.keyCode < 35 || event.keyCode > 39 ;
						}*/
						ondrop = function (){
							return /\D/g.test(event.dataTransfer.getData("text")) ? false : true;
						}
						onpaste = function (){
							return /\D/g.test(clipboardData.getData('text')) ? false : true;
						}
					}
					else    //\u975eie\u6d4f\u89c8\u5668\uff0c\u6bd4\u5982Firefox 
					{
						style.imeMode = "disabled";
						onchange = function (){
							value = value.replace(/\D/g,'');
						}
						onpaste = function (){
							return false;
						}
					}
				}
			}	
		}
	}
}

function mouseOverBAction(obj)
{
	obj.className = "linkOnMouseOver";
}

function mouseOutBAction(obj)
{
	obj.className = "linkOnMouseOut";
}

function fSetHhMmSs()
{
	document.getElementById("dpHour").value=giHh;
	document.getElementById("dpMinute").value=giMm;
	if (giSs!=null && giSs!='')
	{
		document.getElementById("dpSecon").value=giSs;
	}
	else
	{
		document.getElementById("dpSecon").value='0';
	}
};

function fInitialDate(format, strDate)
{
	if( strDate == null || Trim(strDate)== "")
	{
		return false;
	}
	
	var nYear, nMonth, nDay;
	var utcTime = getCalendarUTCfromTime(format, strDate);
	if(utcTime != null)
	{
		var tmpdate = new Date(utcTime);
		nYear = tmpdate.getFullYear();
		giMonth = tmpdate.getMonth() + 1;
		nDay = tmpdate.getDate();
	}
	
	var arrMon = new Array(12);
	arrMon[ 0] = 31;	arrMon[ 1] = nYear % 4 == 0 ? 29:28;
	arrMon[ 2] = 31;	arrMon[ 3] = 30;
	arrMon[ 4] = 31;	arrMon[ 5] = 30;
	arrMon[ 6] = 31;	arrMon[ 7] = 31;
	arrMon[ 8] = 30;	arrMon[ 9] = 31;
	arrMon[10] = 30;	arrMon[11] = 31;
	
	if ( nMonth < 1 || nMonth > 12 )				return false;
	if ( nDay < 1 || nDay > arrMon[nMonth - 1] )	return false;
	
	giYear  = nYear;
	giMonth = nMonth;
	giDay   = nDay;

	return true;
};

var gMonths = new Array($res_entry('ui.control.calendar.January','\u4e00\u6708'),$res_entry('ui.control.calendar.February','\u4e8c\u6708'),$res_entry('ui.control.calendar.March','\u4e09\u6708'),$res_entry('ui.control.calendar.April','\u56db\u6708'),$res_entry('ui.control.calendar.May','\u4e94\u6708'),$res_entry('ui.control.calendar.June','\u516d\u6708'),$res_entry('ui.control.calendar.July','\u4e03\u6708'),$res_entry('ui.control.calendar.August','\u516b\u6708'),$res_entry('ui.control.calendar.September','\u4e5d\u6708'),$res_entry('ui.control.calendar.October','\u5341\u6708'),$res_entry('ui.control.calendar.November','\u5341\u4e00\u6708'),$res_entry('ui.control.calendar.December','\u5341\u4e8c\u6708'));
function maskN()
{
	var masKey = 78;
	if(event.keyCode==masKey&&event.ctrlKey)
	{
		 event.returnValue=0;
		 window.event.returnValue=0;
	}
};
function isIE()
{
	browser_name = navigator.appName;
	if (browser_name == "Microsoft Internet Explorer") 
	{
			return true;
	}
	return false;
};
function isIE2()
{
	if(document.all)
	{
			return true;
	}
	return false;
};
function isNC()
{
	browser_name = navigator.appName;
	if(browser_name == "Netscape")
	{
			return true;
	}
	return false;
};
function isNC2()
{
	if(document.layers)
	{
			return true;
	}
	return false;
};
function version()
{
	 return parseFloat(navigator.appVersion);
};
var LastTabObjectName="";
var FirstTabObjectName="";
function handleEnter()
{
};
function myKeyEvent(e)
{
	var hotkey=13;
	var rightKey = 39;
	var leftKey = 41;
	if (isNC())
	{
			if(e.which==hotkey)
		{
					e.which=9;
					return true;
		}
	}
	else if (isIE())
	{
		if (event.keyCode==hotkey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						if(event.srcElement.type!="textarea")
				{
								event.keyCode=9;
				}
						return true;
			}
		}
		if (event.keyCode==rightKey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						event.keyCode=9;
						return true;
			}
		}
	}
};
handleEnter();
function closeCalendarWin()
{

	try
	{
		var closeSelectDateWindow= Hid;
		if(previousObject == null)
		{
				return;
		}
		if(closeSelectDateWindow == true)
		{
				HiddenDiv();
		}
	}
	catch(e)
	{
			return;
	}
};
function dateCompare(fromDate,toDate,promptInfo)
{
	var calendar1Value = fromDate.value;
	var calendar2Value = toDate.value;
	if(calendar1Value!="" && calendar2Value!="")
	{
			if(calendar1Value > calendar2Value)
		{
				   alert(promptInfo);
					return false;
		}
	}
	return true;			
};
addOnloadEvent(Calendar_init);

package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.dao.IServiceSchedualMapDAO;
import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceSchedualMapDS extends DynamicObjectBaseDS implements IServiceSchedualMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceSchedualMapDAO dao = null;

    public void setDao(IServiceSchedualMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException
    {
        log.debug("insert serviceSchedualMap starting...");
        Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_service_schedual_map_index");
        serviceSchedualMap.setMapindex(id);
        try
        {
            dao.insertServiceSchedualMap(serviceSchedualMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceSchedualMap end");
    }

    public void updateServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException
    {
        log.debug("update serviceSchedualMap by pk starting...");
        try
        {
            dao.updateServiceSchedualMap(serviceSchedualMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceSchedualMap by pk end");
    }

    public void updateServiceSchedualMapList(List<ServiceSchedualMap> serviceSchedualMapList)
            throws DomainServiceException
    {
        log.debug("update serviceSchedualMapList by pk starting...");
        if (null == serviceSchedualMapList || serviceSchedualMapList.size() == 0)
        {
            log.debug("there is no datas in serviceSchedualMapList");
            return;
        }
        try
        {
            for (ServiceSchedualMap serviceSchedualMap : serviceSchedualMapList)
            {
                dao.updateServiceSchedualMap(serviceSchedualMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceSchedualMapList by pk end");
    }

    public void removeServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException
    {
        log.debug("remove serviceSchedualMap by pk starting...");
        try
        {
            dao.deleteServiceSchedualMap(serviceSchedualMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceSchedualMap by pk end");
    }

    public void removeServiceSchedualMapList(List<ServiceSchedualMap> serviceSchedualMapList)
            throws DomainServiceException
    {
        log.debug("remove serviceSchedualMapList by pk starting...");
        if (null == serviceSchedualMapList || serviceSchedualMapList.size() == 0)
        {
            log.debug("there is no datas in serviceSchedualMapList");
            return;
        }
        try
        {
            for (ServiceSchedualMap serviceSchedualMap : serviceSchedualMapList)
            {
                dao.deleteServiceSchedualMap(serviceSchedualMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceSchedualMapList by pk end");
    }

    public ServiceSchedualMap getServiceSchedualMap(ServiceSchedualMap serviceSchedualMap)
            throws DomainServiceException
    {
        log.debug("get serviceSchedualMap by pk starting...");
        ServiceSchedualMap rsObj = null;
        try
        {
            rsObj = dao.getServiceSchedualMap(serviceSchedualMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceSchedualMapList by pk end");
        return rsObj;
    }

    public List<ServiceSchedualMap> getServiceSchedualMapByCond(ServiceSchedualMap serviceSchedualMap)
            throws DomainServiceException
    {
        log.debug("get serviceSchedualMap by condition starting...");
        List<ServiceSchedualMap> rsList = null;
        try
        {
            rsList = dao.getServiceSchedualMapByCond(serviceSchedualMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceSchedualMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceSchedualMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceSchedualMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceSchedualMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceSchedualMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceSchedualMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceSchedualMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceSchedualMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceSchedualMap page info by condition end");
        return tableInfo;
    }
}

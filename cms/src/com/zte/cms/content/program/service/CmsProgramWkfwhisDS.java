package com.zte.cms.content.program.service;

import java.util.List;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.dao.ICmsProgramWkfwhisDAO;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramWkfwhisDS implements ICmsProgramWkfwhisDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramWkfwhisDAO dao = null;
    private IPrimaryKeyGenerator primaryKeyGenerator;

    public void setDao(ICmsProgramWkfwhisDAO dao)
    {
        this.dao = dao;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public void insertCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("insert cmsProgramWkfwhis starting...");
        try
        {
            Long historyindex = (Long) primaryKeyGenerator.getPrimarykey("cms_programwkfwhis");
            cmsProgramWkfwhis.setHistoryindex(historyindex);
            dao.insertCmsProgramWkfwhis(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgramWkfwhis end");
    }

    public void insertCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException
    {
        log.debug("insert cmsProgramWkfwhisList by pk starting...");
        if (null == cmsProgramWkfwhisList || cmsProgramWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramWkfwhisList");
            return;
        }
        try
        {
            dao.insertCmsProgramWkfwhisList(cmsProgramWkfwhisList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgramWkfwhisList by pk end");
    }

    public void updateCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("update cmsProgramWkfwhis by pk starting...");
        try
        {
            dao.updateCmsProgramWkfwhis(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramWkfwhis by pk end");
    }

    public void updateCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException
    {
        log.debug("update cmsProgramWkfwhisList by pk starting...");
        if (null == cmsProgramWkfwhisList || cmsProgramWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramWkfwhisList");
            return;
        }
        try
        {
            dao.updateCmsProgramWkfwhisList(cmsProgramWkfwhisList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramWkfwhisList by pk end");
    }

    public void updateCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("update cmsProgramWkfwhis by condition starting...");
        try
        {
            dao.updateCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramWkfwhis by condition end");
    }

    public void updateCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException
    {
        log.debug("update cmsProgramWkfwhisList by condition starting...");
        if (null == cmsProgramWkfwhisList || cmsProgramWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramWkfwhisList");
            return;
        }
        try
        {
            dao.updateCmsProgramWkfwhisListByCond(cmsProgramWkfwhisList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramWkfwhisList by condition end");
    }

    public void removeCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("remove cmsProgramWkfwhis by pk starting...");
        try
        {
            dao.deleteCmsProgramWkfwhis(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramWkfwhis by pk end");
    }

    public void removeCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException
    {
        log.debug("remove cmsProgramWkfwhisList by pk starting...");
        if (null == cmsProgramWkfwhisList || cmsProgramWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramWkfwhisList");
            return;
        }
        try
        {
            dao.deleteCmsProgramWkfwhisList(cmsProgramWkfwhisList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramWkfwhisList by pk end");
    }

    public void removeCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("remove cmsProgramWkfwhis by condition starting...");
        try
        {
            dao.deleteCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramWkfwhis by condition end");
    }

    public void removeCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList)
            throws DomainServiceException
    {
        log.debug("remove cmsProgramWkfwhisList by condition starting...");
        if (null == cmsProgramWkfwhisList || cmsProgramWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramWkfwhisList");
            return;
        }
        try
        {
            dao.deleteCmsProgramWkfwhisListByCond(cmsProgramWkfwhisList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramWkfwhisList by condition end");
    }

    public CmsProgramWkfwhis getCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DomainServiceException
    {
        log.debug("get cmsProgramWkfwhis by pk starting...");
        CmsProgramWkfwhis rsObj = null;
        try
        {
            rsObj = dao.getCmsProgramWkfwhis(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramWkfwhisList by pk end");
        return rsObj;
    }

    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis)
            throws DomainServiceException
    {
        log.debug("get cmsProgramWkfwhis by condition starting...");
        List<CmsProgramWkfwhis> rsList = null;
        try
        {
            rsList = dao.getCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramWkfwhis by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsProgramWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramWkfwhis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramWkfwhis page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsProgramWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramWkfwhis, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramWkfwhis page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.channel.apply;

import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelApplyValid extends CommonValid
{
    // 日志
    protected ICmsChannelDS channelds = null;
    private Log log = SSBBus.getLog(getClass());

    private IPhysicalchannelDS physicalchannelDS = null;

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(index);
        try
        {
            channel = channelds.getCmsChannel(channel);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        String optObjecttype = ChannelConstants.getWorkflowTypeName(ChannelConstants.WKFWTYPECODE_CHANNEL_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(String.valueOf(index));

        String info = ResourceMgt.findDefaultText("ucdn.channel.log.wkfw.cntdistribute.add.valid");
        if (null != info)
        {
            info = info.replace("[channelname]", channel.getChannelname());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void operate(Long index)
    {
        // 修改状态
        try
        {
            CmsChannel channel = new CmsChannel();
            channel.setChannelindex(index);
            channel = channelds.getCmsChannel(channel);

            channel.setWorkflow(0); // 未启动
            channel.setStatus(0); // 审核通过后，状态修改成待发布
            channelds.updateCmsChannel(channel);
            Physicalchannel physicalchannel = new Physicalchannel();
            physicalchannel.setChannelid(channel.getChannelid());
            List list = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannel);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Physicalchannel physical = (Physicalchannel) list.get(i);
                    physical.setStatus(0); // 状态改成审核失败
                    physical.setWorkflow(0); // 未启动
                    physicalchannelDS.updatePhysicalchannel(physical);

                }
            }

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }
}

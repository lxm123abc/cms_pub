package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.settlementDataRpt.dao.OperatorsMapper;
import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

import java.util.List;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:01 2019/6/27
 * @Description:
 */
public class OperatorsServiceImpl extends DynamicObjectBaseDS implements OperatorsService {

    private OperatorsMapper dao = null;

    public void setDao(OperatorsMapper dao) {
        this.dao = dao;
    }

    @Override
    public TableDataInfo pageInfoQuery(OperInformation operInformation) {

        PageInfo pageInfo = null;
        try {
            pageInfo = dao.selectOperators(operInformation);
        } catch (Exception e) {
            pageInfo = new PageInfo();
            e.printStackTrace();
        }
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setData((List<OperInformation>) pageInfo.getResult());
        tableDataInfo.setTotalCount((int) pageInfo.getTotalCount());

        return tableDataInfo;
    }
}

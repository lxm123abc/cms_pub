CMS门户配置纳入UCM系统操作步骤：

1.在CMS门户服务器上安装驻留程序，驻留程序版本号为ZXINOS-RESV1.01.28

2.创建UCM-CMS-WEB模块号，以oracle用户登陆UCM数据库服务器，并以ascll方式上传文件create_cms_moduleno.sql至/home/setup/目录下，执行如下命令
>chmod 755 create_cms_moduleno.sql
>dos2unix create_cms_moduleno.sql

3.以oracle用户登陆UCM门户服务器，并以zxdbm_ucm用户登陆数据库，执行如下命令
SQL>@/home/setup/create_cms_moduleno.sql

3.部署UCM接收程序：
以root用户登录CMS门户服务器，创建目录/home/UCM/CMS_WEB/shell，创建目录命令为：mkdir -p /home/UCM/CMS_WEB/shell
并将文件config_check.sh和update_config.sh 以ascll方式ftp上传至上述目录下，将jar包cmsucm.jar,dom4j-1.6.1.jar,jaxen-1.1.1.jar以bin方式ftp上传至上述目录下，并执行如下命令：
~>chmod 755 *.sh
~>dos2unix *.sh


！！！注意： 备机只需执行步骤1和步骤4！！！
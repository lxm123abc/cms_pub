package com.zte.cms.operlimit.model;

public class OperatorLimit {
	
	int operid;
	int funcid;
	public int getOperid() {
		return operid;
	}
	public void setOperid(int operid) {
		this.operid = operid;
	}
	public int getFuncid() {
		return funcid;
	}
	public void setFuncid(int funcid) {
		this.funcid = funcid;
	}

}

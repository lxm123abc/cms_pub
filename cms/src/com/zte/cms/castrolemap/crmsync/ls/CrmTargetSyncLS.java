package com.zte.cms.castrolemap.crmsync.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.model.CastcdnConstant;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.cms.castrolemap.crmsync.service.ICrmTargetSyncDS;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.model.CastrolemapConstant;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CrmTargetSyncLS extends DynamicObjectBaseDS implements ICrmTargetSyncLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private ICrmTargetSyncDS crmTargetSyncDS;
    private ICastrolemapDS castrolemapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private ICastcdnDS castds;
    private IProgramCrmMapDS programCrmMapDS;
    private ISeriesCrmMapDS seriesCrmMapDS;
    private ITargetsystemDS targetSystemDS;
    
    private Map map = new HashMap<String, List>();
    public final static String CNTSYNCXML = "xmlsync";
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    
    public void setLog(Logger log)
    {
        this.log = log;
    }

    public void setCastds(ICastcdnDS castds)
    {
        this.castds = castds;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setCastrolemapDS(ICastrolemapDS castrolemapDS)
    {
        this.castrolemapDS = castrolemapDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setCrmTargetSyncDS(ICrmTargetSyncDS crmTargetSyncDS)
    {
        this.crmTargetSyncDS = crmTargetSyncDS;
    }

    public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
    {
        this.programCrmMapDS = programCrmMapDS;
    }

    public void setSeriesCrmMapDS(ISeriesCrmMapDS seriesCrmMapDS)
    {
        this.seriesCrmMapDS = seriesCrmMapDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    /**
     * 人物发布到网元
     *
     * @param castrolemapindex 角色index 
     * @param targetindex 目标系统index
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception 异常
     */
    public String publishCrmToTarget(Long castrolemapindex,Long targetindex, int platform ,int syncType) throws Exception
    {
        log.debug("publishCrmToTarget start ");
        
        String logpub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_PUBLISH;
        String loginfopub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_PUBLISH_INFO;
        List<Castrolemap> castrolemapList = new ArrayList<Castrolemap>();
        List<Castcdn> castcodeList = new ArrayList<Castcdn>();
        Castrolemap crmObj = new Castrolemap();
        Castcdn cast = new Castcdn();
        int crmSyncStatus = 0;
        int operresult = 0;
        int targetStatus = 0;
        Long batchid = (Long)this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id");
        
        try{
            crmObj.setCastrolemapindex(castrolemapindex);
            crmObj = castrolemapDS.getCastrolemap(crmObj);
            if(crmObj == null){
                return "1:角色不存在";
            }else{
                cast.setCastindex(crmObj.getCastindex());
                cast = castds.getCastcdn(cast);
                castrolemapList.add(crmObj);
                castcodeList.add(cast);
                map.put("castrolemapList", castrolemapList);
                map.put("castcodeList", castcodeList);
            }
            
            Targetsystem tagertsystem = new Targetsystem();
            tagertsystem.setTargetindex(targetindex);
            tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
            if(tagertsystem.getStatus()==1 || tagertsystem.getStatus()==2)
            {
                return "1:目标系统状态为暂停或者已注销";
            }
            
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(12);
            target.setObjectid(crmObj.getCastrolemapid());
            target.setTargetindex(targetindex);
            List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(crmTargetList != null && crmTargetList.size() > 0 ){
                crmSyncStatus = crmTargetList.get(0).getStatus();
                if(syncType == 1){ //新增同步
                    //0：待发布，400：发布失败
                    if(crmSyncStatus != 0 && crmSyncStatus!= 400){
                        return "1:角色发布状态不正确";
                    }
                }else{
                    if(crmSyncStatus != 300 && crmSyncStatus!= 500 && crmSyncStatus!= 400){
                        return "1:角色发布状态不正确";
                    }
                }
            }else
            {
                return "1:发布记录不存在";
            }
            
            if(syncType == 1){
                target.setObjecttype(11);
                target.setObjectid(crmObj.getCastid());
                target.setTargetindex(targetindex);
                List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                if(castTargetList != null && castTargetList.size() > 0 ){
                    int castSyncStatus = castTargetList.get(0).getStatus();
                    if(castSyncStatus != 300 && castSyncStatus!= 500){
                        return "1:人物发布状态不正确";
                    }
                }
            }else if(syncType == 3)
            {
                CntTargetSync othTarget = new CntTargetSync();
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                programCrmMap.setCastrolemapindex(castrolemapindex);
                List<ProgramCrmMap> pcrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                if(pcrmMapList != null && pcrmMapList.size() > 0 ){
                    for(ProgramCrmMap pcrm:pcrmMapList){
                        othTarget.setObjecttype(34);
                        othTarget.setObjectid(pcrm.getMappingid());
                        othTarget.setTargetindex(targetindex);
                        List<CntTargetSync> otherTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                        if(otherTargetList != null && otherTargetList.size() > 0 )
                        {
                            for(CntTargetSync otherTargetSync:otherTargetList){
                                if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                    return "1:该角色与点播内容的关联关系已经发布，请先取消发布的关联关系";
                                }
                            }
                        }
                    }
                }
                SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
                seriesCrmMap.setCastrolemapindex(castrolemapindex);
                List<SeriesCrmMap> scrmMapList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);
                if(scrmMapList != null && scrmMapList.size() > 0 ){
                    for(SeriesCrmMap scrm:scrmMapList){
                        othTarget.setObjecttype(37);
                        othTarget.setObjectid(scrm.getMappingid());
                        othTarget.setTargetindex(targetindex);
                        List<CntTargetSync> otherTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                        if(otherTargetList != null && otherTargetList.size() > 0 )
                        {
                            for(CntTargetSync otherTargetSync:otherTargetList){
                                if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                    return "1:该角色与连续剧内容的关联关系已经发布，请先取消发布的关联关系";
                                }
                            }
                        }
                    }
                }
            }

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
            
            if(syncType == 3 && crmSyncStatus == 400)
            {
                operresult = 80;
                targetStatus = 0;
                logpub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH;
                loginfopub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH__INFO;
                
            }else
            {
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                if(platform ==1){ //2.0
                    String wg18xml = null;
                    if(syncType == 1){
                        wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 12, 1);
                        if(wg18xml == null)
                        {
                            return "1: 生成同步xml文件失败";
                        }
                    }else if(syncType == 3){
                        wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 12, 3);
                        if(wg18xml == null)
                        {
                            return "1: 生成同步xml文件失败";
                        }
                    }
                    
                    insertSyncTask(index,wg18xml,correlateid,targetindex,batchid);  // 插入同步任务
                    insertObjectRecord( syncindex, index, castrolemapindex, crmObj.getCastrolemapid(), 
                            12,targetindex, syncType,crmObj.getCastrolemapcode(),2);
                    
                }else if(platform ==2) //3.0
                {
                    String castrolemapxml = null;
                    if(syncType == 1){
                        castrolemapxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 12, 1);
                        if(castrolemapxml == null)
                        {
                            return "1: 生成同步xml文件失败";
                        }
                    }else if(syncType == 3){
                        castrolemapxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 12, 3);
                        if(castrolemapxml == null)
                        {
                            return "1: 生成同步xml文件失败";
                        }
                    }
                    
                    insertSyncTask(index,castrolemapxml,correlateid,targetindex,batchid);  // 插入同步任务
                    insertObjectRecord( syncindex, index, castrolemapindex, crmObj.getCastrolemapid(),12,targetindex, syncType,"",1);
                }
            }
            
            //更新网元状态以及发布结果
            target = crmTargetList.get(0);
            if(syncType == 1){
                operresult = 10;
                targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
            }else if(syncType == 2){
                operresult = 40;
                targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
            }else{
                operresult = 70;
                targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                logpub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH;
                loginfopub = CastrolemapConstant.OPERTYPE_CASTROLEMAP_CANCELPUBLISH__INFO;
            }
            target.setStatus(targetStatus);
            target.setOperresult(operresult);
            cntTargetSyncDS.updateCntTargetSync(target);
            
            //更新平台状态
            CntPlatformSync castPlatform = new CntPlatformSync();
            castPlatform.setObjecttype(12);
            castPlatform.setObjectid(crmObj.getCastrolemapid());
            castPlatform.setPlatform(platform);
            List<CntPlatformSync> crmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(castPlatform);
            if(crmPlatformList!=null&&crmPlatformList.size()>0){
                CntTargetSync alltarget = new CntTargetSync();
                alltarget.setRelateindex(crmPlatformList.get(0).getSyncindex());
                List<CntTargetSync> allCrmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(alltarget);
                int[] crmTargetStatus = new int[allCrmTargetList.size()];
                if(allCrmTargetList != null && allCrmTargetList.size() > 0){
                    for(int i = 0; i< allCrmTargetList.size(); i++){
                        crmTargetStatus[i] = allCrmTargetList.get(i).getStatus();
                    }
                }
                int platformStatus = StatusTranslator.getPlatformSyncStatus(crmTargetStatus);
                castPlatform = crmPlatformList.get(0);
                castPlatform.setStatus(platformStatus);
                cntPlatformSyncDS.updateCntPlatformSync(castPlatform);
            }
            
            CommonLogUtil.insertOperatorLog(crmObj.getCastrolemapid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP_SYNC,logpub, loginfopub,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            log.error("CrmTargetSyncLS exception:" + e.getMessage());
            return "1:操作失败";
        }
        
        log.debug("publishCrmToTarget end ");
        return "0:操作成功";
    }
    
    public TableDataInfo pageInfoQuery(CrmTargetSync crmTargetSync, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            dataInfo = crmTargetSyncDS.pageInfoQuery(crmTargetSync, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }
    
    public String deleteCrmBindTarget(Long castrolemapindex,Long targetindex) throws Exception
    {
        Castrolemap castrolemap = new Castrolemap();
        Long relateindex = null;
        
        try{
            castrolemap.setCastrolemapindex(castrolemapindex);
            castrolemap = castrolemapDS.getCastrolemap(castrolemap);
            if(castrolemap == null){
                return "1:角色不存在";
            }
            
            Targetsystem tagertsystem = new Targetsystem();
            tagertsystem.setTargetindex(targetindex);
            tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
            
            CntTargetSync target = new CntTargetSync();
            CntTargetSync allTarget = new CntTargetSync();
            CntTargetSync cntTargetSync = new CntTargetSync();
            CntPlatformSync platform = new CntPlatformSync();
            target.setObjecttype(12);
            target.setObjectid(castrolemap.getCastrolemapid());
            target.setTargetindex(targetindex);
            List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(crmTargetList != null && crmTargetList.size() > 0 ){
                int crmtSyncStatus = crmTargetList.get(0).getStatus();
                if(crmtSyncStatus == 200 || crmtSyncStatus == 300 || crmtSyncStatus == 500){
                    return "1:角色在该网元已发布，关联关系不能删除";
                }
                
                relateindex = crmTargetList.get(0).getRelateindex();
                platform.setSyncindex(relateindex);
                platform = cntPlatformSyncDS.getCntPlatformSync(platform);
                allTarget.setRelateindex(relateindex);
                List<CntTargetSync> onePlatTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                if(onePlatTargetList.size() > 1)
                {
                    //删除网元数据
                    cntTargetSync.setSyncindex(crmTargetList.get(0).getSyncindex());
                    cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                    
                    //更新平台状态
                    allTarget.setRelateindex(relateindex);
                    List<CntTargetSync> allCrmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                    int[] crmTargetStatus = new int[allCrmTargetList.size()];
                    if(allCrmTargetList != null && allCrmTargetList.size() > 0){
                        for(int i = 0; i< allCrmTargetList.size(); i++){
                            crmTargetStatus[i] = allCrmTargetList.get(i).getStatus();
                        }
                    }
                    int platformStatus = StatusTranslator.getPlatformSyncStatus(crmTargetStatus);
                    platform.setStatus(platformStatus);
                    cntPlatformSyncDS.updateCntPlatformSync(platform);
                    
                }else
                {
                    //删除网元数据
                    cntTargetSync.setSyncindex(crmTargetList.get(0).getSyncindex());
                    cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                    //删除平台数据
                    cntPlatformSyncDS.removeCntPlatformSync(platform);
                    CommonLogUtil.insertOperatorLog(castrolemap.getCastrolemapid().toString() + ", " + tagertsystem.getTargetid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP,
                            ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_DELETE), ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_DELETE_INFO),
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CommonLogUtil.insertOperatorLog(castrolemap.getCastrolemapid().toString() + ", " + tagertsystem.getTargetid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP,
                        ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_TARGET_CRM_BIND_DELETE), ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_DELETE_INFO),
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                
            }else
            {
                return "1:与网元关联关系不存在";
            }
            
        }catch (Exception e)
        {
            log.error("CrmTargetSyncLS exception:" + e.getMessage());
            return "1:操作失败";
        }
        return "0:操作成功";
    }
    
    /**
     * 
     * @param taskindex 同步任务index
     * @param xmlAddress 同步xml文件地址
     * @param correlateid 流水号
     * @param targetindex 目标系统index
     */
    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid)
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
 
    /**
     * 
     * @param syncindex 对象发布记录index
     * @param taskindex 同步任务index
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param targetindex 目标系统index
     * @param synType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @param objectcode 发布到2.0平台时对象的文广code
     * @param platType 平台类型： 1:3.0平台，2:3.0平台
     */
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype, Long targetindex,int synType,String objectcode,int platType )
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            if (platType==2)
            {
                objectSyncRecord.setObjectcode(objectcode);
            }
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    } 
}

package com.zte.cms.cmsframe.dao;

import java.util.List;

import com.zte.cms.cmsframe.dao.ICmsFrameDAO;
import com.zte.cms.cmsframe.model.CmsFrame;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsFrameDAO extends DynamicObjectBaseDao implements ICmsFrameDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsFrame(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("insert cmsFrame starting...");
        super.insert("insertCmsFrame", cmsFrame);
        log.debug("insert cmsFrame end");
    }

    public void updateCmsFrame(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("update cmsFrame by pk starting...");
        super.update("updateCmsFrame", cmsFrame);
        log.debug("update cmsFrame by pk end");
    }

    public void deleteCmsFrame(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("delete cmsFrame by pk starting...");
        super.delete("deleteCmsFrame", cmsFrame);
        log.debug("delete cmsFrame by pk end");
    }

    public CmsFrame getCmsFrame(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("query cmsFrame starting...");
        CmsFrame resultObj = (CmsFrame) super.queryForObject("getCmsFrame", cmsFrame);
        log.debug("query cmsFrame end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsFrame> getCmsFrameByCond(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("query cmsFrame by condition starting...");
        List<CmsFrame> rList = (List<CmsFrame>) super.queryForList("queryCmsFrameListByCond", cmsFrame);
        log.debug("query cmsFrame by condition end");
        return rList;
    }
    
    public List<CmsFrame> getCmsFrameByName(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("query cmsFrame by condition starting...");
        List<CmsFrame> rList = (List<CmsFrame>) super.queryForList("queryCmsFrameListCntByName", cmsFrame);
        log.debug("query cmsFrame by condition end");
        return rList;
    }
    
    public List<CmsFrame> queryCmsFrameListCntByNameUpdate(CmsFrame cmsFrame) throws DAOException
    {
        log.debug("query cmsFrame by condition starting...");
        List<CmsFrame> rList = (List<CmsFrame>) super.queryForList("queryCmsFrameListCntByNameUpdate", cmsFrame);
        log.debug("query cmsFrame by condition end");
        return rList;
    }
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsFrame by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsFrameListCntByCond", cmsFrame)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsFrame> rsList = (List<CmsFrame>) super.pageQuery("queryCmsFrameListByCond", cmsFrame, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsFrame by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsFrameListByCond", "queryCmsFrameListCntByCond", cmsFrame, start, pageSize,
                puEntity);
    }

}
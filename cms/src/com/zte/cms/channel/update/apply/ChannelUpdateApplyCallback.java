package com.zte.cms.channel.update.apply;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.cms.channel.service.IChannelWkfwhisDS;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelBufDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import net.sf.cglib.beans.BeanCopier;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelUpdateApplyCallback extends CommonCallback
{
    // 日志
    protected ICmsChannelDS channelds = null;
    protected ICmsChannelBufDS channelBufds = null;
    protected IChannelWkfwhisDS channelWkfwhisDS;

    protected CmsChannel channelBasic;
    protected CmsChannelBuf channelBuf;
    protected ChannelWkfwhis channelWkfwhis;
    private Log log = SSBBus.getLog(getClass());
    private IPhysicalchannelDS physicalchannelDS;
    private IPhysicalchannelBufDS physicalchannelbufDS;
    
 
    
	public IPhysicalchannelBufDS getPhysicalchannelbufDS() {
		return physicalchannelbufDS;
	}

	public void setPhysicalchannelbufDS(IPhysicalchannelBufDS physicalchannelbufDS) {
		this.physicalchannelbufDS = physicalchannelbufDS;
	}

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    /**
     * 
     * @return ICmsChannelDS
     */
    public ICmsChannelDS getChannelds()
    {
        return channelds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return ICmsChannelBufDS
     */
    public ICmsChannelBufDS getChannelBufds()
    {
        return channelBufds;
    }

    /**
     * 
     * @param channelBufds ICmsChannelBufDS
     */
    public void setChannelBufds(ICmsChannelBufDS channelBufds)
    {
        this.channelBufds = channelBufds;
    }

    /**
     * 
     * @return IChannelWkfwhisDS
     */
    public IChannelWkfwhisDS getChannelWkfwhisDS()
    {
        return channelWkfwhisDS;
    }

    /**
     * 
     * @param channelWkfwhisDS IChannelWkfwhisDS
     */
    public void setChannelWkfwhisDS(IChannelWkfwhisDS channelWkfwhisDS)
    {
        this.channelWkfwhisDS = channelWkfwhisDS;
    }

    /**
     * 
     * @return CmsChannel
     */
    public CmsChannel getChannelBasic()
    {
        return channelBasic;
    }

    /**
     * 
     * @param channelBasic CmsChannel
     */
    public void setChannelBasic(CmsChannel channelBasic)
    {
        this.channelBasic = channelBasic;
    }

    /**
     * 
     * @return CmsChannelBuf
     */
    public CmsChannelBuf getChannelBuf()
    {
        return channelBuf;
    }

    /**
     * 
     * @param channelBuf CmsChannelBuf
     */
    public void setChannelBuf(CmsChannelBuf channelBuf)
    {
        this.channelBuf = channelBuf;
    }

    /**
     * 
     * @return ChannelWkfwhis
     */
    public ChannelWkfwhis getChannelWkfwhis()
    {
        return channelWkfwhis;
    }

    /**
     * 
     * @param channelWkfwhis ChannelWkfwhis
     */
    public void setChannelWkfwhis(ChannelWkfwhis channelWkfwhis)
    {
        this.channelWkfwhis = channelWkfwhis;
    }

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        String optObjecttype = ChannelConstants.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_UPDATE);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(wkflItem.getPboIndex().toString());

        String info;
        String logKey = "";
        if (wkflItem.getNodeId().longValue() == 1)
        {
            logKey = "ucdn.channel.log.wkfw.cntdistribute.update.apply";
        }
        else
        {
            logKey = "ucdn.channel.log.wkfw.cntdistribute.update.audit";
        }
        info = ResourceMgt.findDefaultText(logKey);
        if (null != info)
        {
            info = info.replace("[opername]", wkflItem.getExcutorName());
            info = info.replace("[channelname]", channelBasic.getChannelname());
            info = info.replace("[nodename]", wkflItem.getNodeName());
            info = info.replace("[handleevent]", wkflItem.getHandleEvent());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long channelindex = wkflItem.getPboIndex();
        channelBuf = new CmsChannelBuf();
        channelBuf.setChannelindex(channelindex);

        // 查询service对象
        try
        {
            channelBuf = channelBufds.getCmsChannelBuf(channelBuf);
            channelBuf.setWorkflowlife(wkflItem.getPboState());

            channelBasic = new CmsChannel();
            channelBasic.setChannelindex(channelindex);
            channelBasic = channelds.getCmsChannel(channelBasic);
            channelBasic.setWorkflowlife(wkflItem.getPboState());

            if (channelBuf.getEffecttime() == null || "".equals(channelBuf.getEffecttime()))
            {
                wkflItem.setEffectiveTime("0");
            }
            else
            {
                wkflItem.setEffectiveTime("0");
            }
            wkflItem.setPboId(channelBuf.getChannelid());

            // 创建工作流历史表
            channelWkfwhis = new ChannelWkfwhis();
            BeanCopier copier = BeanCopier.create(CmsChannelBuf.class, ChannelWkfwhis.class, false);
            copier.copy(channelBuf, channelWkfwhis, null);

            channelWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
            channelWkfwhis.setTaskindex(new Long(wkflItem.getTaskId()));
            channelWkfwhis.setNodeid(Long.valueOf(wkflItem.getNodeId().longValue()));
            channelWkfwhis.setNodename(wkflItem.getNodeName());
            channelWkfwhis.setOpername(wkflItem.getExcutorId());
            channelWkfwhis.setTemplateindex(Long.valueOf(wkflItem.getTemplateSeqNo().longValue()));
            channelWkfwhis.setOpopinion(wkflItem.getComments());
            channelWkfwhis.setHandleevent(wkflItem.getHandleEvent());

            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            if (wkflItem.getStartTime() != null)
            {
                String workflowstarttime = dateformat.format(wkflItem.getStartTime());
                channelWkfwhis.setWorkflowstarttime(workflowstarttime);
            }
            if (wkflItem.getFinishTime() != null)
            {
                String workflowendtime = dateformat.format(wkflItem.getFinishTime());
                channelWkfwhis.setWorkflowendtime(workflowendtime);
            }

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

    }

    @Override
    public void operate()
    {
        try
        {
            if (wkflItem.getPboState().intValue() == -1) // 审核不通过，根据原来的状态修改审核不通过的状态
            {
                int oldStatus = channelBuf.getStatus();

                channelBasic.setStatus(oldStatus);

                channelBufds.removeCmsChannelBuf(channelBuf);
                //删除全部的物理频道的buf表中数据
                PhysicalchannelBuf buf = new PhysicalchannelBuf();
				buf.setChannelid(channelBuf.getChannelid());
				List list = physicalchannelbufDS.getPhysicalchannelBufByCond(buf);
				if(list!=null&&list.size()>0){
					buf = (PhysicalchannelBuf)list.get(0);
				}
				if(buf!=null){
					physicalchannelbufDS.removePhysicalchannelBuf(buf);
				}
                channelBasic.setWorkflow(0); // 否则会总是在审核中
                channelBasic.setWorkflowlife(0); // 未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                channelBasic.setEffecttime(format.format(new Date()));
                // 将生效时间字段设为当前时间

            }
            else
            {
                channelBufds.updateCmsChannelBuf(channelBuf);
            }
            channelds.updateCmsChannel(channelBasic);
            List list = new ArrayList();
            Physicalchannel ph = new Physicalchannel();
            ph.setChannelid(channelBasic.getChannelid());
            list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Physicalchannel ch = (Physicalchannel) list.get(i);
                    ch.setStatus(channelBasic.getStatus());
                    ch.setWorkflow(0); // 否则会总是在审核中
                    ch.setWorkflowlife(0); // 未启动
                    physicalchannelDS.updatePhysicalchannel(ch);
                    // 修改子内容状态
                }
            }
            channelWkfwhisDS.insertChannelWkfwhis(channelWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

}

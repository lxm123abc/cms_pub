package com.zte.cms.content.util;

import java.util.Vector;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

public class ModAuditPool {
    // 日志记录器
    private Log mLog = SSBBus.getLog(ModAuditPool.class);;

    private static byte[] lockByte = new byte[0];

    // 唯一实例
    private volatile static ModAuditPool instance;

    /** 共享存储池*/
    private static Vector<String> auditPool = null;

    /**
     * 返回唯一实例。如果是第一次调用此方法，则创建实例
     * 
     * @return AuditPool 唯一实例
     */
    public static ModAuditPool getInstance()
    {
        if (instance != null)
        {
            return instance;
        }
        else
        {
            synchronized (lockByte)
            {
                if (instance == null)
                {
                    instance = new ModAuditPool();
                }
                return instance;
            }
        }
    }

    /**
     * 构造函数
     * 
     */
    private ModAuditPool()
    {
        if (auditPool == null)
        {
            auditPool = new Vector<String>();
        }
    }


    /**
     * 判断共享池中是否存在当前内容
     */
    public boolean contains(String contentid)
    {
        synchronized (instance)
        {
            if (auditPool.contains(contentid))
            {
                mLog.error("Content has existed: contentid=" + contentid + ", content count=" + auditPool.size());
                return true;
            }
            else
            {
                auditPool.addElement(contentid);
                mLog.error("Add content: contentid=" + contentid + ", content count=" + auditPool.size());
                return false;
            }
            
        }        
    }

    /**
     * 移除共享存储池中的内容
     * 
     * @param contentid 内容编码
     * @throws Exception
     */
    public void remove(String contentid)
    {
        try
        {
            synchronized (instance)
            {
                if (auditPool.contains(contentid))
                {
                    // 移除共享存储池中的对象
                    auditPool.removeElement(contentid);                
                    mLog.error("Remove content: contentid=" + contentid + ", content count=" + auditPool.size());
                }
            }
        }
        catch (Exception ex)
        {
            mLog.error("Error occurred in remove() method", ex);
        }
    }
}

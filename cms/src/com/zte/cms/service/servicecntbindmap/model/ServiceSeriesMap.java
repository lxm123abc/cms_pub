package com.zte.cms.service.servicecntbindmap.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServiceSeriesMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.Long serviceindex;
    private java.lang.String serviceid;
    private java.lang.Long seriesindex;
    private java.lang.String seriesid;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private java.lang.String mappingid;

    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("serviceindex", "SERVICEINDEX");
        this.addRelation("serviceid", "SERVICEID");
        this.addRelation("seriesindex", "SERIESINDEX");
        this.addRelation("seriesid", "SERIESID");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
        this.addRelation("mappingid", "MAPPINGID");
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    }

    public void setServiceindex(java.lang.Long serviceindex)
    {
        this.serviceindex = serviceindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.Long getSeriesindex()
    {
        return seriesindex;
    }

    public void setSeriesindex(java.lang.Long seriesindex)
    {
        this.seriesindex = seriesindex;
    }

    public java.lang.String getSeriesid()
    {
        return seriesid;
    }

    public void setSeriesid(java.lang.String seriesid)
    {
        this.seriesid = seriesid;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

}

package com.zte.cms.storageManage.service;

import java.util.List;
import com.zte.cms.storageManage.model.CmsDeviceWindiskidBind;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsDeviceWindiskidBindDS
{
    /**
     * 新增CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException;

    /**
     * 更新CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException;

    /**
     * 批量更新CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsDeviceWindiskidBindList(List<CmsDeviceWindiskidBind> cmsDeviceWindiskidBindList)
            throws DomainServiceException;

    /**
     * 删除CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException;

    /**
     * 批量删除CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsDeviceWindiskidBindList(List<CmsDeviceWindiskidBind> cmsDeviceWindiskidBindList)
            throws DomainServiceException;

    /**
     * 查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @return CmsDeviceWindiskidBind对象
     * @throws DomainServiceException ds异常
     */
    public CmsDeviceWindiskidBind getCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @return 满足条件的CmsDeviceWindiskidBind对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsDeviceWindiskidBind> getCmsDeviceWindiskidBindByCond(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
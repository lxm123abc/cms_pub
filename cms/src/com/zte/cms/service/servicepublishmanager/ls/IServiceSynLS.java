package com.zte.cms.service.servicepublishmanager.ls;


import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceSynLS
{
    
    //查询平台中服务与点播内容关联关系发布情况
    public TableDataInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws Exception;
  //查询平台中服务与连续剧关联关系发布情况
    public TableDataInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws Exception;
  //查询平台中服务与直播频道关联关系发布情况
    public TableDataInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws Exception;  
    
/***
 * 
 * 查询总状态表中的服务对象
 *  
 */
    public TableDataInfo pagePlatformInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize) throws Exception;    
    
    
    //查询服务与其绑定关系网元发布mapping
    public TableDataInfo pageSerProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws Exception;
    public TableDataInfo pageSerSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws Exception;
    public TableDataInfo pageSerChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws Exception;
    
    /***
     * 
     * 查询网元表中的服务对象
     *  
      */   
    public TableDataInfo pageTargetInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize) throws Exception;    
    
    /***
     * 
     * 查询网元表中的服务对象
     *  
      */   
    public TableDataInfo pageTargetInfoQueryAll(Service service, int start, int pageSize) throws Exception;   
    
    /***
     * 
     * 通过主键来获取网元服务对象
     *  
      */   
    
    public ServiceTargetSyn getServiceTargetSynByID( Long syncindex ) throws DomainServiceException;

    
    /***
     * 网元发布targetPublishSyn ()
     * 
     * */
    
    public String targetPublishSyn(Long syncindex ) throws Exception;
    /***
     * 平台发布targetPublishSyn()
     * 
     * */
    public String  platformPublishSyn(Long synsindex) throws Exception;
    /***
     *批量发布targetPublishSyn()
     * 
     * */
    public String  batchPublishSyn(List<ServiceTargetSyn> serviceList) throws Exception;
    /***
     * 网元取消发布targetPublishSyn ()
     * 
     * */
    
    public String targetDelPublishSyn(Long syncindex ) throws Exception;
    /***
     * 平台取消发布targetPublishSyn()
     * 
     * */
    public String  platformDelPublishSyn(Long syncindex) throws Exception;
    /***
     *批量取消发布targetPublishSyn()
     * 
     * */
    public String  batchDelPublishSyn(List<ServiceTargetSyn> serviceList) throws Exception;
    /***
     * 修改发布服务
     * 
     * */
    public String  modPublishSyn(String objectid) throws Exception;   
    /**
     * 
     * 判断服务绑定的关系发布状态
     * 如果状态是否存在发布中200的状态，如果存在不允许进行修改发布
     * 返回结果：flase表示该服务的发布状态为发布中    ture表示该服务可以进行服务修改发布操作
     * */  
    public boolean checkServiceStatus(Long objectindex) throws Exception;
    
    //****************************服务绑定关系发布********************************
    public String bingTargetPublish(Long syncindex) throws Exception;
    
    public String bingPlatformPublish(Long syncindex)throws Exception;
    
    public String bingBatchPublish(List<ServiceTargetSyn> mappingList)throws Exception;
    //*************************************************************
    public String bingTargetDelPublish(Long syncindex) throws Exception;
    
    public String bingPlatformDelPublish(Long syncindex) throws Exception;
    
    public String bingBatchDelPublish(List<ServiceTargetSyn>  mappingList) throws Exception;

    

    
    

    
}

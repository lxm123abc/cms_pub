package com.zte.cms.cpContractManagement.service;

import com.zte.cms.cpContractManagement.dao.IContractMapDao;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.cms.notice.dao.INoticemapDAO;
import com.zte.cms.notice.ls.INoticemapLS;
import com.zte.cms.notice.model.Noticemap;
import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public  class ContractMapService extends DynamicObjectBaseDS implements IContractMapService {




    private Log log = SSBBus.getLog(getClass());

    private IContractMapDao dao;

    public void setDao (IContractMapDao dao){
        this.dao = dao;
    }


    private INoticemapLS noticemapLS;

    public void  setNoticemapLS(INoticemapLS noticemapLS){
        this.noticemapLS = noticemapLS;
    }


    private INoticemapDAO noticemapDao = null;

    public void setNoticemapDao(INoticemapDAO noticemapDao) {
        this.noticemapDao = noticemapDao;
    }

    /**
     *分页查询数据service层
     * @param contractMap
     * @param start
     * @param pageSize
     * @return
     * @throws DAOException
     */
    @Override
    public TableDataInfo pageInfoQuery(ContractMap contractMap, int start, int pageSize) throws DomainServiceException {
        log.info("ContractMapService  pageInfoQuery start ");
        PageInfo result = null;
        try {
            result = dao.pageInfoQuery(contractMap,start,pageSize);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        List<ContractMap> list = (List<ContractMap>) result.getResult();
        for (ContractMap map : list) {
            if (map.getBeginDate()!=null){
                map.setShowBeginDate(DateUtils.convertDataToString_fit(map.getBeginDate(),DateUtils.fit_yyyy_MM_DD));
            }
            if (map.getEndDate()!=null){
                map.setShowEndDate(DateUtils.convertDataToString_fit(map.getEndDate(),DateUtils.fit_yyyy_MM_DD));
            }
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List) result.getResult());
        tableInfo.setTotalCount((int) result.getTotalCount());//把数据总量传给前台，否则页面统计，翻页插件无法使用插件
        log.info("ContractMapService  pageInfoQuery end ");
        return tableInfo;
    }


    /**
     * 删除合同
     * @param ids
     * @return
     * @throws DAOException
     */
    @Override
    public String deleteConstractByIds(String[] ids) throws DomainServiceException {
        log.info("ContractMapService  deleteConstractByIds start ");
        dao.deleteConstractByIds(ids);
        log.info("ContractMapService  deleteConstractByIds end ");
        return "删除成功！";
    }


    /**
     * 跳转页面通过ID查询单个记录
     * @param id
     * @return
     * @throws IOException
     */
    @Override
    public ContractMap selectContractById(Integer id) throws DomainServiceException {
        log.info("ContractMapService  selectContractById start ");
        ContractMap contractMap = null;


        try {
             contractMap = dao.selectContractById(id);
            contractMap.setShowBeginDate(DateUtils.convertDataToString_fit(contractMap.getBeginDate(),DateUtils.fit_yyyy_MM_DD));
            contractMap.setShowEndDate(DateUtils.convertDataToString_fit(contractMap.getEndDate(),DateUtils.fit_yyyy_MM_DD));
        } catch (DAOException e) {
            e.printStackTrace();
        }
        log.info("ContractMapService  selectContractById end ");


        return contractMap;
    }

    /**
     * 修改合同和上传合同功能
     * @param contractMap
     * @return
     * @throws IOException
     */
    @Override
    public ContractMap updateContractById(ContractMap contractMap) throws DomainServiceException {
        log.info("ContractMapService  updateContractById start ");
        ContractMap result = null;
        try {
            //修改时间格式
            contractMap.setBeginDate(DateUtils.convert_strToDate(contractMap.getShowBeginDate(),DateUtils.fit_yyyy_MM_DD));
            contractMap.setEndDate(DateUtils.convert_strToDate(contractMap.getShowEndDate(),DateUtils.fit_yyyy_MM_DD));
            result = dao.updateContractById(contractMap);

            insertAttach(contractMap.getId(), null);

            result.setShowBeginDate(DateUtils.convertDataToString_fit(result.getBeginDate(),DateUtils.fit_yyyy_MM_DD));
            result.setShowEndDate(DateUtils.convertDataToString_fit(result.getEndDate(),DateUtils.fit_yyyy_MM_DD));
            result.setMessage("修改成功！");
        } catch (DAOException e) {
            e.printStackTrace();
        }
        log.info("ContractMapService  updateContractById end ");
        return result;
    }

    @Override
    public List<ContractMap> getAttachListById(ContractMap contractMap) throws DomainServiceException {
        log.info("ContractMapService  getAttachListById start ");
        List<ContractMap> list = null;
        try {
             list=dao.getAttachListById(contractMap);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        log.info("ContractMapService  getAttachListById end ");
        return list;
    }

    @Override
    public String deleteContractAttach(String attachid) throws DomainServiceException {
        Noticemap noticemap = new Noticemap();
        String message = null;
        try {
            ContractMap contractMap = dao.getAttachInfoById(attachid);
            System.out.println(Integer.parseInt(contractMap.getAttachid()));
            noticemap.setAttachid(Integer.parseInt(contractMap.getAttachid()));
            noticemap.setAttachurl(contractMap.getAttachurl());
            noticemapDao.deleteAttach(noticemap);
            message = "删除成功！";
        } catch (DAOException e) {
            message = "删除失败！请重新尝试！";
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public String insertContract(ContractMap contractMap,String content) throws DomainServiceException {

        log.info("ContractMapService  insertContract start ");
        String  result = null;
        ContractMap con = null;
        Integer isExist = null;

        try {
            //修改时间格式
            contractMap.setBeginDate(DateUtils.convert_strToDate(contractMap.getShowBeginDate(),DateUtils.fit_yyyy_MM_DD));
            contractMap.setEndDate(DateUtils.convert_strToDate(contractMap.getShowEndDate(),DateUtils.fit_yyyy_MM_DD));
            isExist = dao.queryContractByContractId(contractMap);
            if(isExist != 0){
                result = "500";
                return result;
            }
            Long contractId = (Long) this.getPrimaryKeyGenerator().getPrimarykey("contractId_index");

            contractMap.setId(contractId.intValue());
            dao.insertContractMap(contractMap);

            //获取新的合同信息
            con = dao.getContractMapByContractMap(contractMap);

            //拼接发送公告的信息
            String title = "水印版合同上传通知";
            Noticemap noticemap = new Noticemap();
            noticemap.setTitle(title);
            noticemap.setDescription(contractMap.getCpName()+":"+content);
            noticemap.setIsemail("1");
            noticemap.setIssms("1");
            noticemap.setMessagetype(1);
            noticemap.setCpidlist(contractMap.getCpCode());
            insertAttach(con.getId(), null);
            try {
                noticemapLS.insertNoticemap(noticemap);
                result = "200";
            } catch (DomainServiceException e) {
                e.printStackTrace();
            }

        } catch (DAOException e) {
            e.printStackTrace();
        }
        log.info("ContractMapService  insertContract end ");

        return result;
    }


    /**
     * 文件上传功能
     * @param id
     * @param step2
     * @throws DAOException
     */
    public void insertAttach(Integer id, Integer step2) throws DAOException {
        Long attachid;
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        Iterator<FileItem> iter = fileList.iterator();
        while(iter.hasNext()){
            FileItem tmp = (FileItem) iter.next();
            String attachname = tmp.getName();
            //去除掉由于‘multi’不能去除的路径问题
            String name=null;
            String nameArray []=null;
            if (attachname.lastIndexOf("/") != -1)
            {
                nameArray = attachname.split("/");
            }
            else
            {
                nameArray = attachname.split("\\\\");
            }
            name = nameArray[nameArray.length-1];
            if(StringUtils.isNotEmpty(name)) {
                Noticemap noticemap = new Noticemap();
                noticemap.setMessageid(id);
                attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
                noticemap.setAttachname(name);
                noticemap.setAttachid(Integer.parseInt(attachid + ""));
                noticemap.setAttachurl(attachid + name.substring(name.lastIndexOf(".")));
                noticemapDao.insertAttach(tmp, noticemap);
            }
        }
    }
}

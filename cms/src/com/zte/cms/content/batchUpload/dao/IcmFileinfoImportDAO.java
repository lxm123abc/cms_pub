package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.dao.IIcmFileinfoImportDAO;
import com.zte.cms.content.batchUpload.model.IcmFileinfoImport;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmFileinfoImportDAO extends DynamicObjectBaseDao implements IIcmFileinfoImportDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmFileinfoImport(IcmFileinfoImport icmFileinfoImport) throws DAOException
    {
        log.debug("insert icmFileinfoImport starting...");
        super.insert("insertIcmFileinfoImport", icmFileinfoImport);
        log.debug("insert icmFileinfoImport end");
    }

    @SuppressWarnings("unchecked")
    public List<IcmFileinfoImport> getIcmFileinfoImportByCond(IcmFileinfoImport icmFileinfoImport) throws DAOException
    {
        log.debug("query icmFileinfoImport by condition starting...");
        List<IcmFileinfoImport> rList = (List<IcmFileinfoImport>) super.queryForList(
                "queryIcmFileinfoImportListByCond", icmFileinfoImport);
        log.debug("query icmFileinfoImport by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmFileinfoImport by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmFileinfoImportListCntByCond", icmFileinfoImport))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmFileinfoImport> rsList = (List<IcmFileinfoImport>) super.pageQuery(
                    "queryIcmFileinfoImportListByCond", icmFileinfoImport, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmFileinfoImport by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryIcmFileinfoImportListByCond", "queryIcmFileinfoImportListCntByCond",
                icmFileinfoImport, start, pageSize, puEntity);
    }

}
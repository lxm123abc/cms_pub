package com.zte.cms.channel.ls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;


import com.zte.cms.categorycdn.common.CategoryConstant;
import com.zte.cms.categorycdn.ls.CategoryConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.channel.update.apply.IScheduleUpdateApply;

import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CSplatformSyn.service.ICSCntPlatformSyncDS;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.CStargetSyn.service.ICSCntTargetSyncDS;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;

import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceSchedualMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;
import com.zte.cms.channel.common.CmsScheduleConstant;
import com.zte.cms.channel.common.FileSaveUtil;
import com.zte.cms.channel.common.ScheduleNum;
import com.zte.ismp.common.ResourceManager;

public class CmsScheduleLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsScheduleLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_USER, getClass());
    private ICmsScheduleDS cmsScheduleDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private ICmsChannelDS channelDS;
    private ICntSyncTaskDS icntsyncTaskDS;
    private ITargetsystemDS targetSystemDS;
    private IUcpBasicDS basicDS;
    private IUsysConfigDS usysConfigDS = null;
    private IServiceSchedualMapDS serviceSchedualMapDS;
    // private final static String
    // SCHEDULE_STORE_SUB_PATH="/cntsyncxml/schedule/";
    private final static String SCHEDULE_FOLDER = "schedule";
    private final static String SCHEDULERECORD_FOLDER = "schedulerecord";
    private final static String ACTIONREGIST = "REGIST or UPDATE";
    private final static String ACTIONDELETE = "DELETE";
    private IScheduleUpdateApply apply;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private ICSCntPlatformSyncDS cscntPlatformSyncDS;
    private ICSCntTargetSyncDS CScntTargetSyncDS;
    private static final String SUCCESS = "cmsprogramtype.success";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
    private IPhysicalchannelDS physicalchannelDS;
    private ICmsScheduleRecordLS scheduleRecordLS;
    protected IScheduleWkfwhisDS scheduleWkfwhisDS;

    public IScheduleWkfwhisDS getScheduleWkfwhisDS()
    {
        return scheduleWkfwhisDS;
    }

    public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS)
    {
        this.scheduleWkfwhisDS = scheduleWkfwhisDS;
    }

    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    public ICmsScheduleRecordLS getScheduleRecordLS()
    {
        return scheduleRecordLS;
    }

    public void setScheduleRecordLS(ICmsScheduleRecordLS scheduleRecordLS)
    {
        this.scheduleRecordLS = scheduleRecordLS;
    }

    public ICSCntPlatformSyncDS getCscntPlatformSyncDS()
    {
        return cscntPlatformSyncDS;
    }

    public void setCscntPlatformSyncDS(ICSCntPlatformSyncDS cscntPlatformSyncDS)
    {
        this.cscntPlatformSyncDS = cscntPlatformSyncDS;
    }

    public ICSCntTargetSyncDS getCScntTargetSyncDS()
    {
        return CScntTargetSyncDS;
    }

    public void setCScntTargetSyncDS(ICSCntTargetSyncDS scntTargetSyncDS)
    {
        CScntTargetSyncDS = scntTargetSyncDS;
    }

    public IScheduleUpdateApply getApply()
    {
        return apply;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public void setApply(IScheduleUpdateApply apply)
    {
        this.apply = apply;
    }

    /**
     * getScheduleById通过节目单ID查找节目单对象
     * 
     * @param scheduleid
     *            节目单ID
     * @return CmsSchedule对象
     * @throws Exception
     */
    public CmsSchedule getScheduleById(String scheduleid) throws Exception
    {
        CmsSchedule cmsSchedule = new CmsSchedule();
        cmsSchedule.setScheduleid(scheduleid);
        List<CmsSchedule> scheduleList = null;
        try
        {
            scheduleList = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (null != scheduleList && scheduleList.size() > 0)
        {
            cmsSchedule = scheduleList.get(0);
            // String
            // dati=cmsSchedule.getStartdate()+cmsSchedule.getStarttime();
            // cmsSchedule.setStartdate(dati);
            return cmsSchedule;
        }
        return null;
    }

    /**
     * 通过Index获得节目单
     * 
     * @param scheduleindex
     *            String
     * @return CmsSchedule
     * @throws Exception
     */
    // added by fc
    public CmsSchedule getScheduleByIndex(String scheduleindex) throws Exception
    {
        CmsSchedule cmsSchedule = new CmsSchedule();
        cmsSchedule.setScheduleindex(new Long(scheduleindex));
        List<CmsSchedule> scheduleList = null;
        try
        {
            scheduleList = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (null != scheduleList && scheduleList.size() > 0)
        {
            cmsSchedule = scheduleList.get(0);
            // String
            // dati=cmsSchedule.getStartdate()+cmsSchedule.getStarttime();
            // cmsSchedule.setStartdate(dati);
            return cmsSchedule;
        }
        return null;
    }

    /**
     * getScheduleById通过节目单ID查找节目单对象
     * 
     * @param scheduleid
     *            节目单ID
     * @return CmsSchedule对象
     * @throws Exception
     */
    public String auditSchedule(String oppnion, String scheduleid) throws Exception
    {
        try
        {
            CmsSchedule cmsSchedule = new CmsSchedule();
            CmsSchedule schedule = new CmsSchedule();
            cmsSchedule.setScheduleid(scheduleid);
            List<CmsSchedule> auditScheduleList = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
            if (auditScheduleList != null && auditScheduleList.size() > 0)
            {
                cmsSchedule = auditScheduleList.get(0);
                if (cmsSchedule.getStatus() != 110 && cmsSchedule.getStatus() != 130)
                {
                    return CmsScheduleConstant.RESULT_CODE_118;
                }
                if (oppnion != null && oppnion.equals("通过"))
                {
                    schedule.setStatus(0);
                    schedule.setScheduleindex(cmsSchedule.getScheduleindex());
                    cmsScheduleDS.updateCmsSchedule(schedule);
                    CommonLogUtil.insertOperatorLog(scheduleid, CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE,
                            CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_AUDIT,
                            CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_AUDIT_INFO,
                            CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else if (oppnion != null && oppnion.equals("不通过"))
                {
                    schedule.setStatus(130);
                    schedule.setScheduleindex(cmsSchedule.getScheduleindex());
                    cmsScheduleDS.updateCmsSchedule(schedule);
                    CommonLogUtil.insertOperatorLog(scheduleid, CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE,
                            CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_AUDIT,
                            CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_AUDIT_INFO,
                            CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
                    return "success";
                }
            }
            else
            {
                return CmsScheduleConstant.RESULT_CODE_119;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "fail";
        }

        return "success";
    }

    public String getCmsSchedule(String scheduleid) throws Exception
    {
        CmsSchedule cmsSchedule = new CmsSchedule();
        cmsSchedule.setScheduleid(scheduleid);
        List<CmsSchedule> scheduleList = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
        String status;
        if (null != scheduleList && scheduleList.size() > 0)
        {
            status = scheduleList.get(0).getStatus() + "";
            return status;
        }
        else
        {
            return CmsScheduleConstant.RESULT_CODE_119;
        }
    }

    /**
     * insertCmsSchedule新增节目单，包括增加录制计划
     * 
     * @param CmsSchedule
     *            cmsSchedule对象
     * @return 新增是否成功
     * @throws Exception
     */
    public String insertCmsScheduleAndRecordBatch(CmsSchedule cmsSchedule) throws Exception
    {
        log.debug("insertCmsSchedule  start  in the class CmsScheduleLS.................");
        String valiChannel = "";
        try
        {
            // CmsSchedule cmsSchedule = (CmsSchedule)schelist.get(0);
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            int minutes = 15;
            usysConfig.setCfgkey("cms.schedule.cantoperate.time");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
            }
            //查询节目单免审状态，0--免审，1--需要审核
            int flag = 1;
            UsysConfig auditConfig = new UsysConfig();
            auditConfig.setCfgkey("cms.schedule.noaudit.switch");
            List<UsysConfig> auditConfigList = usysConfigDS.getUsysConfigByCond(auditConfig);
            if (null != auditConfigList && auditConfigList.size() > 0)
            {
                auditConfig = auditConfigList.get(0);
                flag = Integer.parseInt(auditConfig.getCfgvalue());
            }
            
            DbUtil db = new DbUtil();
            // 提前minutes分钟下达录制计划
            String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";
            String sql1 = "select to_char(sysdate, 'yyyymmddhh24miss') currenttime1 from dual";
            List rtnlist = null;
            rtnlist = db.getQuery(sql);
            Map tmpMap = new HashMap();
            tmpMap = (Map) rtnlist.get(0);
            String currenttime = tmpMap.get("currenttime").toString();// 查询数据库较当前时间晚minutes分钟的时间
            List rtnlist1 = null;
            rtnlist1 = db.getQuery(sql1);
            Map tmpMap1 = new HashMap();
            tmpMap1 = (Map) rtnlist1.get(0);
            String currenttime1 = tmpMap1.get("currenttime1").toString();// 查询数据库较当前时间
            valiChannel = getChannelByChannelid(cmsSchedule.getChannelid());
            if (valiChannel.equals("success"))
            {
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(cmsSchedule.getChannelid());
                List<CmsChannel> list = channelDS.getChannelByCond(channel);
                if (list != null && list.size() > 0)
                {
                    channel = list.get(0);
                }
                List<CmsSchedule> isTimeRepeated = this.isTimeRepeated(cmsSchedule);
                if (null != isTimeRepeated && isTimeRepeated.size() > 0)
                {
                    return CmsScheduleConstant.RESULT_CODE_113;
                }
                SimpleDateFormat t1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期
                SimpleDateFormat t2 = new SimpleDateFormat("yyyyMMddHHmmss");// 格式化日期
                // 获得当前用户ID
                UserInfo loginUser = LoginMgt.getUserInfo();
                String executorId = loginUser.getUserId();
                String starttime = cmsSchedule.getStarttime();
                /***************************************************************
                 * if ((DateUtil2.get14Time(starttime)) .compareToIgnoreCase(currenttime) < 0) {//
                 * 现在minutes分钟之内即将开始的节目单不可新增 return CmsScheduleConstant.RESULT_CODE_111; }
                 **************************************************************/
                cmsSchedule.setCpid(channel.getCpid());
                cmsSchedule.setStartdate(DateUtil2.get14Time(starttime).substring(0, 8));
                cmsSchedule.setStarttime(DateUtil2.get14Time(starttime).substring(8, 14));
                cmsSchedule.setCreatoropername(executorId);                
                if(flag == 0){
                    //免审
                    cmsSchedule.setStatus(CmsScheduleConstant.STATUS_0);//审核通过的
                    cmsSchedule.setWorkflow(0);
                    cmsSchedule.setWorkflowlife(0);
                }else{
                    cmsSchedule.setStatus(CmsScheduleConstant.STATUS_110);// 待审核
                }                                  
                cmsSchedule.setIsvalid(1);
                cmsSchedule.setCpcontentid(cmsSchedule.getChannelid());
                cmsSchedule.setCreatetime(currenttime1);// 获得系统当前时间为节目单创建时间
                cmsSchedule.setLastupdatetime(currenttime1);// 获得系统当前时间为节目单最近修改时间
                cmsScheduleDS.insertCmsSchedule(cmsSchedule);
                CommonLogUtil.insertOperatorLog(cmsSchedule.getScheduleid(),
                        CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE, CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_ADD,
                        CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_ADD_INFO,
                        CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);

                List schedulelist = new ArrayList();
                schedulelist.add(cmsSchedule.getScheduleid());

                scheduleRecordLS.insertScheduleRecordmorePhyBatch(schedulelist, cmsSchedule.getChannelid());// 多个物理频道

            }
            else
            {
                return valiChannel;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "sysexception";
        }
        log.debug("insertCmsSchedule  end  in the class CmsScheduleLS......................");
        return valiChannel;
    }

    /**
     * insertCmsSchedule新增节目单，包括增加录制计划
     * 
     * @param CmsSchedule
     *            cmsSchedule对象
     * @return 新增是否成功
     * @throws Exception
     */
    public String insertCmsScheduleAndRecord(CmsSchedule cmsSchedule) throws Exception
    {
        log.debug("insertCmsSchedule  start  in the class CmsScheduleLS.................");
        String valiChannel = "";
        try
        {
            // CmsSchedule cmsSchedule = (CmsSchedule)schelist.get(0);
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            int minutes = 15;
            usysConfig.setCfgkey("cms.schedule.cantoperate.time");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
            }
            DbUtil db = new DbUtil();
            // 提前minutes分钟下达录制计划
            String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";
            String sql1 = "select to_char(sysdate, 'yyyymmddhh24miss') currenttime1 from dual";
            List rtnlist = null;
            rtnlist = db.getQuery(sql);
            Map tmpMap = new HashMap();
            tmpMap = (Map) rtnlist.get(0);
            String currenttime = tmpMap.get("currenttime").toString();// 查询数据库较当前时间晚minutes分钟的时间
            List rtnlist1 = null;
            rtnlist1 = db.getQuery(sql1);
            Map tmpMap1 = new HashMap();
            tmpMap1 = (Map) rtnlist1.get(0);
            String currenttime1 = tmpMap1.get("currenttime1").toString();// 查询数据库较当前时间
            valiChannel = getChannelByChannelid(cmsSchedule.getChannelid());
            if (valiChannel.equals("success"))
            {
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(cmsSchedule.getChannelid());
                List<CmsChannel> list = channelDS.getChannelByCond(channel);
                if (list != null && list.size() > 0)
                {
                    channel = list.get(0);
                }
                List<CmsSchedule> isTimeRepeated = this.isTimeRepeated(cmsSchedule);
                if (null != isTimeRepeated && isTimeRepeated.size() > 0)// 新增的时候如果时间重叠则不允许新增
                {
                    return CmsScheduleConstant.RESULT_CODE_113;
                }
                SimpleDateFormat t1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期
                SimpleDateFormat t2 = new SimpleDateFormat("yyyyMMddHHmmss");// 格式化日期
                // 获得当前用户ID
                UserInfo loginUser = LoginMgt.getUserInfo();
                String executorId = loginUser.getUserId();
                String starttime = cmsSchedule.getStarttime();
                if ((DateUtil2.get14Time(starttime)).compareToIgnoreCase(currenttime) < 0)
                {
                    // 现在minutes分钟之内即将开始的节目单不可新增
                    // return "1:新增节目单失败,节目开始时间应大于当前时间"+minutes+"分钟";
                    return "1:新增节目单失败,节目开始时间应大于当前时间" + minutes + "分钟";
                    // return CmsScheduleConstant.RESULT_CODE_111;
                }
                cmsSchedule.setCpid(channel.getCpid());
                cmsSchedule.setStartdate(DateUtil2.get14Time(starttime).substring(0, 8));
                cmsSchedule.setStarttime(DateUtil2.get14Time(starttime).substring(8, 14));
                cmsSchedule.setCreatoropername(executorId);
                cmsSchedule.setIsvalid(cmsSchedule.getIsvalid());
                cmsSchedule.setCpcontentid(cmsSchedule.getChannelid());
                cmsSchedule.setCreatetime(currenttime1);// 获得系统当前时间为节目单创建时间
                cmsSchedule.setLastupdatetime(currenttime1);// 获得系统当前时间为节目单最近修改时间
                cmsSchedule.setReservecode1(cmsSchedule.getReservecode1());// storageduration
                // 查看是否免审
                int flag = 0;
                usysConfig = new UsysConfig();
                usysConfig.setCfgkey("cms.schedule.noaudit.switch");
                usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                if (null != usysConfigList && usysConfigList.size() > 0)
                {
                    usysConfig = usysConfigList.get(0);
                    flag = Integer.parseInt(usysConfig.getCfgvalue());
                }
                if (flag == 0)
                {
                    // 免审
                    cmsSchedule.setStatus(0);// 审核通过的
                    cmsSchedule.setWorkflow(0);
                    cmsSchedule.setWorkflowlife(0);
                }
                else
                {
                    cmsSchedule.setStatus(CmsScheduleConstant.STATUS_110);// 待审核

                }

                cmsScheduleDS.insertCmsSchedule(cmsSchedule);
                CommonLogUtil.insertOperatorLog(cmsSchedule.getScheduleid(),
                        CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE, CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_ADD,
                        CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_ADD_INFO,
                        CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);

                List schedulelist = new ArrayList();
                schedulelist.add(cmsSchedule.getScheduleid());

                scheduleRecordLS.insertScheduleRecordmorePhy(schedulelist, cmsSchedule.getChannelid(), cmsSchedule
                        .getPhysicallist());// 多个物理频道
            }
            else
            {
                return valiChannel;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "sysexception";
        }
        log.debug("insertCmsSchedule  end  in the class CmsScheduleLS......................");
        return valiChannel;
    }

    public String schedulePreOffline(String scheduleid) throws Exception
    {
        log.debug("schedulePreOffline  start  in the class CmsScheduleLS.................");
        try
        {
            CmsSchedule cmsschedule = this.getScheduleById(scheduleid);
            if (cmsschedule != null)
            {
                if (cmsschedule.getStatus() != 20 && cmsschedule.getStatus() != 50 && cmsschedule.getStatus() != 60)
                {
                    return CmsScheduleConstant.RESULT_CODE_201;
                }

                ServiceSchedualMap serviceSchedualMap = new ServiceSchedualMap();
                serviceSchedualMap.setSchedualid(scheduleid);
                List<ServiceSchedualMap> sscheduleList = serviceSchedualMapDS
                        .getServiceSchedualMapByCond(serviceSchedualMap);// 节目单是否与服务绑定
                if (sscheduleList != null && sscheduleList.size() > 0)
                {
                    return CmsScheduleConstant.RESULT_CODE_202;
                }

                CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
                cmsSchedulerecord.setScheduleid(scheduleid);
                List<CmsSchedulerecord> isRecordExsit = cmsSchedulerecordDS
                        .getCmsSchedulerecordByCond(cmsSchedulerecord);// 节目单是否与物理频道生成收录计划
                if (isRecordExsit != null && isRecordExsit.size() > 0)
                {
                    return CmsScheduleConstant.RESULT_CODE_203;
                }
                int status = cmsschedule.getStatus();
                CmsSchedule schedule = new CmsSchedule();
                schedule.setStatus(65);
                schedule.setReservesel1(status);
                schedule.setScheduleindex(cmsschedule.getScheduleindex());
                cmsScheduleDS.updateCmsSchedule(schedule);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "fail";
        }
        log.debug("schedulePreOffline  end  in the class CmsScheduleLS......................");
        return "success";
    }

    /**
     * getChannelByChannelid根据ID查询频道
     * 
     * @param channelid
     *            频道ID
     * @return 返回频道状态是否为新增同步 频道是否存在 频道CP是否删除
     * @throws Exception
     */
    public String getChannelByChannelid(String channelid) throws Exception
    {// 新增节目单的
     // 判断需要新增节目单的频道是否存在
     // 状态时否正常
     // 及该频道对应的CP状态是否正常
        log.debug("getChannelByChannelid  start  in the class CmsScheduleLS.......................");
        long channelStatus;
        String cpid = "";
        try
        {
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(channelid);
            List<CmsChannel> channelList = channelDS.getChannelByCond(channel);
            if (channelList != null && channelList.size() > 0)
            {
                channel = channelList.get(0);
                channelStatus = channel.getStatus();
                cpid = channel.getCpid();
                if (channelStatus == CmsScheduleConstant.STATUS_100)
                {// 频道状态为100（初始）这些状态均不能操作节目单
                    return CmsScheduleConstant.RESULT_CODE_110;
                }
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cpid);
                ucpBasic.setCptype(1); // 1:CP 2:牌照方
                List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() > 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == 4)
                    {
                        return CmsScheduleConstant.RESULT_CODE_112; // 表示cp已被删除
                    }
                }
            }
            else
            {
                return CmsScheduleConstant.RESULT_CODE_114;// 频道已被删除
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        log.debug("getChannelByChannelid  end  in the class CmsScheduleLS......................");
        return "success";
    }

    /**
     * pageInfoQuery查询节目单类表
     * 
     * @param CmsSchedule
     *            cmsSchedule, int start,int pageSize
     * @return tabledatainfo
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuery  starting  in the class CmsScheduleLS....................");
        TableDataInfo dataInfo = new TableDataInfo();
        DateUtil2 dateUtil = new DateUtil2();
        try
        {
            if (null != cmsSchedule.getStatus() && cmsSchedule.getStatus() == 999)
            {
                cmsSchedule.setStatus(null);
            }
            if (null != cmsSchedule.getProgramname() && !"".equals(cmsSchedule.getProgramname()))
            {
                cmsSchedule.setProgramname(EspecialCharMgt.conversion(cmsSchedule.getProgramname()));
            }
            if (null != cmsSchedule.getScheduleid() && !"".equals(cmsSchedule.getScheduleid()))
            {
                cmsSchedule.setScheduleid(EspecialCharMgt.conversion(cmsSchedule.getScheduleid()));
            }
            if (null != cmsSchedule.getStartdate1() && !"".equals(cmsSchedule.getStartdate1()))
            {

                String time1 = dateUtil.get14Time(cmsSchedule.getStartdate1());
                cmsSchedule.setStartdate1(time1.substring(0, 8));
                // cmsSchedule.setStarttime1((time1.substring(8, 14)));
            }
            if (null != cmsSchedule.getStartdate2() && !"".equals(cmsSchedule.getStartdate2()))
            {
                String time2 = dateUtil.get14Time(cmsSchedule.getStartdate2());
                cmsSchedule.setStartdate2((time2.substring(0, 8)));
                // cmsSchedule.setStarttime2((time2.substring(8, 14)));
            }
            dataInfo = cmsScheduleDS.pageInfoQuery(cmsSchedule, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error(e);
        }
        log.debug("pageInfoQuery  end  in the class CmsScheduleLS....................");
        return dataInfo;
    }

    /**
     * pageInfoQuery查询节目单类表
     * 
     * @param CmsSchedule
     *            cmsSchedule, int start,int pageSize
     * @return tabledatainfo
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryForRecord(CmsSchedule cmsSchedule, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuery  starting  in the class CmsScheduleLS....................");
        TableDataInfo dataInfo = new TableDataInfo();
        DateUtil2 dateUtil = new DateUtil2();
        try
        {

            if (null != cmsSchedule.getProgramname() && !"".equals(cmsSchedule.getProgramname()))
            {
                cmsSchedule.setProgramname(EspecialCharMgt.conversion(cmsSchedule.getProgramname()));
            }
            if (null != cmsSchedule.getScheduleid() && !"".equals(cmsSchedule.getScheduleid()))
            {
                cmsSchedule.setScheduleid(EspecialCharMgt.conversion(cmsSchedule.getScheduleid()));
            }
            if (null != cmsSchedule.getStartdate1() && !"".equals(cmsSchedule.getStartdate1()))
            {

                String time1 = dateUtil.get14Time(cmsSchedule.getStartdate1());
                cmsSchedule.setStartdate1(time1.substring(0, 8));
                // cmsSchedule.setStarttime1((time1.substring(8, 14)));
            }
            if (null != cmsSchedule.getStartdate2() && !"".equals(cmsSchedule.getStartdate2()))
            {
                String time2 = dateUtil.get14Time(cmsSchedule.getStartdate2());
                cmsSchedule.setStartdate2((time2.substring(0, 8)));
                // cmsSchedule.setStarttime2((time2.substring(8, 14)));
            }
            dataInfo = cmsScheduleDS.pageInfoQueryForRecrod(cmsSchedule, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error(e);
        }
        log.debug("pageInfoQuery  end  in the class CmsScheduleLS....................");
        return dataInfo;
    }

    /**
     * removeCmsScheduleList删除某频道下的节目单集合
     * 
     * @param scheduleidList节目单ID集合
     *            *
     * @param channelid频道ID
     * @return 删除返回信息
     * @throws Exception
     */
    public String removeCmsScheduleList(String[] scheduleidList, String channelid) throws Exception
    {
        log.debug("removeCmsScheduleList  start  in the class CmsScheduleLS.......................");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        int infoCnt = 0;
        String errorcode = "";
        Integer iFail = 0;
        Integer iSuccessed = 0;
        List tarlist = new ArrayList();
        List platlist = new ArrayList();
        String vailChannel = this.getChannelByChannelid(channelid);
        if (!"success".equals(vailChannel))
        {
            return vailChannel;
        }
        // 先判断节目单本身是否是审核中状态，若是不允许删除，再判断节目单是否已发布，若是不允许删除
        List<CmsSchedule> cmsScheduleList = new ArrayList<CmsSchedule>();
        if (scheduleidList != null && scheduleidList.length > 0)
        {
            for (int i = 0; i < scheduleidList.length; i++)
            {
                CmsSchedule cmsSchedule = new CmsSchedule();
                cmsSchedule = this.getScheduleById(scheduleidList[i]);
                if (null != cmsSchedule)
                {
                    if (cmsSchedule.getStatus() == CmsScheduleConstant.STATUS_120// 审核中
                    )
                    {
                        iFail++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), scheduleidList[i], "删除节目单失败",
                                "该节目单处于审核中状态");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        // 0待发布 ?80取消同步成功,100初始,130审核中（这些状态可删除节目单）

                        CntTargetSync cntTargetSync = new CntTargetSync();
                        cntTargetSync.setObjectindex(cmsSchedule.getScheduleindex());
                        cntTargetSync.setObjecttype(6);

                        CntPlatformSync cntPlatformSync = new CntPlatformSync();
                        cntPlatformSync.setObjectid(cmsSchedule.getScheduleid());
                        cntPlatformSync.setObjecttype(6);
                        platlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                        try
                        {
                            tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                        }
                        catch (DomainServiceException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        boolean flagboolean = false;// 如果状态节目单为发布中，发布成功，修改发布失败，设置标志位不允许删除
                        if (tarlist != null && tarlist.size() > 0)
                        {
                            for (int num = 0; num < tarlist.size(); num++)
                            {
                                cntTargetSync = (CntTargetSync) tarlist.get(num);
                                int status = cntTargetSync.getStatus();
                                // 发布中，不允许修改
                                if (status == 200 || status == 300 || status == 500)
                                {

                                    // return "1:在该状态下不允许删除";
                                    flagboolean = true;
                                }
                            }
                        }
                        if (flagboolean == true)
                        {
                            iFail++;
                            operateInfo = new OperateInfo(String.valueOf(++infoCnt), scheduleidList[i], "删除节目单失败",
                                    "该节目单已发布不能删除");
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }

                        if (tarlist != null && tarlist.size() > 0)
                        {
                            cntTargetSyncDS.removeCntTargetSyncList(tarlist);
                        }
                        if (platlist != null && platlist.size() > 0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSyncList(platlist);
                        }
                        // 删除节目单工作流历史表记录
                        ScheduleWkfwhis cmsScheduleWkfwhis = new ScheduleWkfwhis();
                        cmsScheduleWkfwhis.setScheduleid(cmsSchedule.getScheduleid());
                        List<ScheduleWkfwhis> cmsScheduleWkfwhisList = scheduleWkfwhisDS
                                .getScheduleWkfwhisByCond(cmsScheduleWkfwhis);
                        if (cmsScheduleWkfwhisList != null && cmsScheduleWkfwhisList.size() > 0)
                        {
                            scheduleWkfwhisDS.removeScheduleWkfwhisList(cmsScheduleWkfwhisList);
                        }
                        cmsScheduleList.add(cmsSchedule);
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(++infoCnt), scheduleidList[i], "删除节目单成功",
                                "删除节目单成功");
                        returnInfo.appendOperateInfo(operateInfo);
                        CommonLogUtil.insertOperatorLog(scheduleidList[i], CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE,
                                CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_DEL,
                                CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_DEL_INFO,
                                CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                }
                else
                {
                    iFail++;
                    operateInfo = new OperateInfo(String.valueOf(++infoCnt), scheduleidList[i], "删除节目单失败", "该节目单已被删除");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
            }
        }
        if (cmsScheduleList != null && cmsScheduleList.size() > 0)
        {
            cmsScheduleDS.removeCmsScheduleList(cmsScheduleList);
        }
        if (scheduleidList.length == 0 || iFail == scheduleidList.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = "失败数量" + iFail + ""; // "350099"
        }
        else if (iFail > 0 && iFail < scheduleidList.length)
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = "成功数量" + iSuccessed + "失败数量" + iFail + ""; // "350098"
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = "成功数量" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("removeCmsScheduleList  end  in the class CmsScheduleLS......................");
        return returnInfo.toString();
    }

    /**
     * updateCmsSchedule更新节目单
     * 
     * @param CmsSchedule
     *            cmsSchedule对象
     * @return 更新返回信息
     * @throws Exception
     */
    public String updateCmsSchedule(CmsSchedule cmsSchedule) throws Exception
    {
        String rtn = "";
        // 查看是否免审的配置项
        int flag = 0;
        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        usysConfig.setCfgkey("cms.schedule.noaudit.switch");
        usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            flag = Integer.parseInt(usysConfig.getCfgvalue());//
        }
        log.debug("updateCmsSchedule  end in the class CmsScheduleLS.....................................");
        String scheduleid = cmsSchedule.getScheduleid();
        String channelid = cmsSchedule.getChannelid();
        CmsSchedule schedule = new CmsSchedule();
        schedule = this.getScheduleById(scheduleid);
        long scheduleindex = schedule.getScheduleindex();

        CntTargetSync sync = new CntTargetSync();
        sync.setObjectid(schedule.getScheduleid());
        sync.setObjecttype(6);
        List schelist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
        boolean flagpubtag = false;
        if (schelist != null && schelist.size() > 0)
        {
            for (int i = 0; i < schelist.size(); i++)
            {
                sync = (CntTargetSync) schelist.get(i);
                int status = sync.getStatus();
                // 发布中，不允许修改
                if (status == 200)
                {
                    // String msg = "1:发布中，不允许修改";
                    // return msg;
                    return CmsScheduleConstant.RESULT_CODE_117;// 该节目状态不可修改
                }
                if (status == 300 || status == 500)
                {
                    // 发布成功,只要发布成功就算成功
                    flagpubtag = true;// 发布成功
                }
            }
        }

        try
        {
            if (schedule != null)
            {
                if (schedule.getStatus() != CmsScheduleConstant.STATUS_110
                        && schedule.getStatus() != CmsScheduleConstant.STATUS_130
                        && schedule.getStatus() != CmsScheduleConstant.STATUS_0)
                {
                    return CmsScheduleConstant.RESULT_CODE_117;// 该节目状态不可修改
                }
                else
                {

                    int minutes = 15;
                    usysConfig = new UsysConfig();
                    usysConfig.setCfgkey("cms.schedule.cantoperate.time");
                    usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                    if (null != usysConfigList && usysConfigList.size() > 0)
                    {
                        usysConfig = usysConfigList.get(0);
                        minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
                    }
                    String vailChannel = this.getChannelByChannelid(channelid);
                    if (!"success".equals(vailChannel))
                    {// 验证该节目单所属的频道是否存在
                        return vailChannel;
                    }
                    DbUtil db = new DbUtil();
                    // 提前15分钟下达录制计划
                    String sql = "select to_char(sysdate +" + minutes
                            + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
                    List rtnlist = null;
                    rtnlist = db.getQuery(sql);
                    Map tmpMap = new HashMap();
                    tmpMap = (Map) rtnlist.get(0);
                    String currenttime = tmpMap.get("currenttime").toString();
                    /***********************************************************
                     * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0)
                     * {// 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
                     **********************************************************/
                    cmsSchedule.setScheduleindex(scheduleindex);
                    String startdate = cmsSchedule.getStartdate();
                    String starttime = cmsSchedule.getStarttime();

                    if ((DateUtil2.get14Time(starttime)).compareToIgnoreCase(currenttime) < 0)
                    {
                        // 修改后的开始时间在15分钟之内的节目单不可修改
                        return "1:修改节目单失败,修改后的开始时间应大于当前时间" + minutes + "分钟";
                    }

                    // CmsSchedule cmsScheduleValidTime=new CmsSchedule();
                    // cmsScheduleValidTime=this.getScheduleById(scheduleid);
                    List<CmsSchedule> isRepeatedList = this.isTimeRepeated(cmsSchedule);
                    if (null != isRepeatedList && isRepeatedList.size() > 0)
                    {
                        /******************************************************* 需要修改的节目单未发布 判断与其时间重叠的节目单以及下面的录制计划是否符合删除条件 ******************************************************/
                        if ((schelist == null) || (flagpubtag == false))// 未关联平台或未发布
                        {// 若影响到的节目单未发布，则删除节目单，否则不允许修改
                            for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                            {

                                CntTargetSync syncaffect = new CntTargetSync();
                                syncaffect.setObjectid(cmsSchedulefortime.getScheduleid());
                                syncaffect.setObjecttype(6);
                                List schelistaffect = cntTargetSyncDS.getCntTargetSyncByCond(syncaffect);
                                if (schelistaffect != null && schelistaffect.size() > 0)
                                {
                                    for (int i = 0; i < schelistaffect.size(); i++)
                                    {
                                        syncaffect = (CntTargetSync) schelistaffect.get(i);
                                        int status = syncaffect.getStatus();
                                        // 发布中，不允许修改
                                        if (status == 200)
                                        {
                                            // String msg = "1:影响到的节目单在发布中，不允许修改";
                                            // return msg;
                                            return CmsScheduleConstant.RESULT_CODE_921;
                                        }
                                        if (status == 300 || status == 500)
                                        {// 影响到的节目单已发布，不允许修改
                                            return CmsScheduleConstant.RESULT_CODE_923;
                                        }
                                    }
                                }

                                if (cmsSchedulefortime.getStatus() == CmsScheduleConstant.STATUS_120)
                                {
                                    // 响到的节目单在审核中，不允许修改
                                    return CmsScheduleConstant.RESULT_CODE_922;
                                }

                            }
                            // 修改的节目单A未发布 如果满足修改条件 则在门户侧删除与其时间重叠的节目单 不走审核流程就在者删除
                            // 走审核流程的状态在审核通过后删除
                            if ((schedule.getStatus() == 110 || schedule.getStatus() == 130) || flag == 0)
                            {
                                cmsScheduleDS.removeCmsScheduleList(isRepeatedList);
                            }
                        }

                        /******************************************************* 需要修改的节目单已发布 判断与其时间重叠的节目单以及下面的录制计划是否符合删除条件 ******************************************************/
                        if ((schelist != null) && (flagpubtag == true)) // 已关联平台并且已发布
                        {
                            // 若已发布则允许修改，不论B是否发布，不用判断。除非B状态在xx中

                            for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                            {

                                CntTargetSync syncaffect = new CntTargetSync();
                                syncaffect.setObjectid(cmsSchedulefortime.getScheduleid());
                                syncaffect.setObjecttype(6);
                                List schelistaffect = cntTargetSyncDS.getCntTargetSyncByCond(syncaffect);
                                if (schelistaffect != null && schelistaffect.size() > 0)
                                {
                                    for (int i = 0; i < schelistaffect.size(); i++)
                                    {
                                        syncaffect = (CntTargetSync) schelistaffect.get(i);
                                        int status = syncaffect.getStatus();
                                        // 发布中，不允许修改
                                        if (status == 200)
                                        {
                                            // String msg = "1:影响到的节目单在发布中，不允许修改";
                                            // return msg;
                                            return CmsScheduleConstant.RESULT_CODE_921;
                                        }

                                    }
                                }

                                if (cmsSchedulefortime.getStatus() == CmsScheduleConstant.STATUS_120)
                                {
                                    // 影响到的节目单在审核中，不允许修改
                                    return CmsScheduleConstant.RESULT_CODE_922;
                                }

                            }
                            // 当前节目单是已同步 那时间重叠的节目单和录制计划就删除 同步的就做修改同步 发步过走审核流程
                            // 在审核通过后将其删除 在此处不做删除操作

                        }
                    }
                    String sql1 = "select to_char(sysdate, 'yyyymmddhh24miss') currenttime1 from dual";// 查询数据库较当前时间
                    List rtnlist1 = null;
                    rtnlist1 = db.getQuery(sql1);
                    Map tmpMap1 = new HashMap();
                    tmpMap1 = (Map) rtnlist1.get(0);
                    String currenttime1 = tmpMap1.get("currenttime1").toString();
                    cmsSchedule.setLastupdatetime(currenttime1);
                    cmsSchedule.setStartdate(starttime.substring(0, 4) + starttime.substring(5, 7)
                            + starttime.substring(8, 10));
                    cmsSchedule.setStarttime(starttime.substring(11, 13) + starttime.substring(14, 16)
                            + starttime.substring(17, 19));
                    cmsSchedule.setChannelid(channelid);
                    // cmsSchedule.setIsvalid(schedule.getIsvalid());
                    cmsSchedule.setScheduleindex(schedule.getScheduleindex());
                    cmsSchedule.setScheduleid(scheduleid);
                    // cmsSchedule.setStarttime(starttime.substring(0,
                    // 2)+starttime.substring(3,
                    // 5)+starttime.substring(6, 8));

                    // 提交审核,如果是已审核过的才需走审核流程,此为未审核过的

                    if ((schedule.getStatus() != 110) && (schedule.getStatus() != 130) && (flag != 0))// 并且不是免审的情况下
                    {
                        // flag==0是免审

                        rtn = apply.apply(cmsSchedule);
                        if (!rtn.equals("success"))
                        {
                            return rtn;
                        }
                    }
                    else
                    {
                        if (flag == 0)
                        {
                            rtn = "successflag";
                        }
                        // 不需提交审核，直接修改数据库,没有发布过的情况
                        cmsScheduleDS.updateCmsSchedule(cmsSchedule);
                        CmsSchedulerecord record = new CmsSchedulerecord();
                        record.setScheduleid(cmsSchedule.getScheduleid());
                        List recordlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(record);
                        if (recordlist != null && recordlist.size() > 0)
                        {
                            for (int ii = 0; ii < recordlist.size(); ii++)
                            {
                                record = (CmsSchedulerecord) recordlist.get(ii);
                                record.setNamecn(cmsSchedule.getProgramname());
                                record.setStartdate(cmsSchedule.getStartdate());
                                record.setStarttime(cmsSchedule.getStarttime());
                                record.setDuration(cmsSchedule.getDuration());
                                cmsSchedulerecordDS.updateCmsSchedulerecord(record);
                            }
                        }
                        // 查询是否发布，发布过后的修改也可能免审核，所以不一定所有方法都在审核内
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        cntTargetSync.setObjectindex(cmsSchedule.getScheduleindex());
                        cntTargetSync.setObjecttype(6);

                        List tarlist = new ArrayList();
                        try
                        {
                            tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);// 查出所有发布过的网元
                        }
                        catch (DomainServiceException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        // 发布过的,调用发布方法
                        List<CntTargetSync> publist = new ArrayList();
                        if (tarlist != null && tarlist.size() > 0)
                        {
                            for (int i = 0; i < tarlist.size(); i++)
                            {
                                cntTargetSync = (CntTargetSync) tarlist.get(i);
                                int status = cntTargetSync.getStatus();
                                if (status == 300 || status == 500)
                                {
                                    publist.add(cntTargetSync);

                                }

                            }
                            try
                            {
                                publishScheduleTar(publist);// 发布方法
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }// 可以发布到的网元
                        }
                    }

                    CommonLogUtil.insertOperatorLog(cmsSchedule.getScheduleid(),
                            CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE,
                            CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_MOD,
                            CmsScheduleConstant.OPERTYPE_CHANNELSCHEDULE_MOD_INFO,
                            CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
            else
            {
                return CmsScheduleConstant.RESULT_CODE_119;// 该节目已被删除
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("updateCmsSchedule  end  in the class CmsScheduleLS......................");
        if (flag != 0)
        {
            rtn = "success";
        }

        return rtn;
    }

    public String isReapetedSchePub(CmsSchedule cmsSchedule) throws Exception
    {
        List<CmsSchedule> isRepeatedList = this.isTimeRepeated(cmsSchedule);
        if (null != isRepeatedList && isRepeatedList.size() > 0)
        {
            for (CmsSchedule s : isRepeatedList)
            {
                if (s.getStatus() == 20 || s.getStatus() == 50 || s.getStatus() == 60)
                {
                    return CmsScheduleConstant.RESULT_CODE_916;// 该节目修改后的时间与已有的发布的节目单时间重叠，页面提示是否继续修改
                }
            }
        }
        return "";
    }

    public String publishToTarget(Long targetindex, long objindex, String objid, int objtype, Long relateindex)
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);
        Targetsystem target = new Targetsystem();
        target.setTargetindex(targetindex);
        List tarlist;
        try
        {
            tarlist = targetSystemDS.getTargetsystemByCond(target);
            if (tarlist != null && tarlist.size() > 0)
            {
                target = (Targetsystem) tarlist.get(0);
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);

                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);

                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
                CommonLogUtil.insertOperatorLog(objid + ", " + target.getTargetid(), "log.channelSchedule.mgt",
                        "log.target.schedule.bind.add", "log.target.schedule.bind.add.info",
                        CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    public String preparetopublish(Targetsystem tar, long objindex, String objid, int objtype)
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTarget(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex);
        return operdesc;
    }

    // 节目单需关联频道已经关联的网元
    public String bindBatchScheduleTargetsys(List<Targetsystem> targetsystemList, List<CmsSchedule> cmsScheduleList)
            throws DomainServiceException
    {

        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalChannelindex = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsScheduleList.size(); i++) // 遍历要操作的内容主键，依次操作要关联的内容
        {
            totalChannelindex++;
            String result = null;
            String operdesc = null;
            String proStatusStr = null;

            CmsSchedule cmsSchedule = (CmsSchedule) cmsScheduleList.get(i);

            if (cmsSchedule == null)
            {
                proStatusStr = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED);
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsSchedule.getScheduleid(), result.substring(2,
                        result.length()), proStatusStr);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else if (cmsSchedule.getStatus() != 0)
            {// 未审核通过不允许关联平台
                proStatusStr = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED);
                failCount++;

                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsSchedule.getScheduleid(), result.substring(2,
                        result.length()), "未审核通过不允许关联平台");
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
            boolean flag = false;
            String msg = "";
            for (int j = 0; j < targetsystemList.size(); j++)
            {

                operdesc = preparetopublish(targetsystemList.get(j), cmsSchedule.getScheduleindex(), cmsSchedule
                        .getScheduleid(), 6);// 添加到网元的数据，节目单

                if (operdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                {
                    flag = true;
                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "关联成功";

                }
                else
                // 内容关联运营平台失败
                {

                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "已经被关联";

                }

            }
            if (flag == true)
            {
                successCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsSchedule.getScheduleid(), "成功", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsSchedule.getScheduleid(), "失败", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            num++;
        }

        if ((successCount) == totalChannelindex)
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalChannelindex)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();
    }

    // 节目单网元取消发布
    public HashMap cancelpublishScheduleTar(List<CntTargetSync> tarlist) throws Exception
    {
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_911);
            return map;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_912);
            return map;// "31:获取临时区失失败";
        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_913);
            return map;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();
        CntPlatformSync platformSync = new CntPlatformSync();
        platformSync.setSyncindex(tarlist.get(0).getRelateindex());
        List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(platformSync);
        if (pltlist != null && pltlist.size() > 0)
        {
            platformSync = (CntPlatformSync) pltlist.get(0);
        }
        // for (int i = 0; i < cntPlatformSynclist.size(); i++) {
        // 判断节目单内容是否存在，判断网元状态,不用判录制计划
        CmsSchedule schedule = new CmsSchedule();
        schedule.setScheduleid(platformSync.getObjectid());
        List list = cmsScheduleDS.getCmsScheduleByCond(schedule);
        if (list == null | list.size() == 0)
        {
            map.put("msg", "1:节目单不存在");

            return map;
        }
        else
        {
            schedule = (CmsSchedule) list.get(0);
            if (schedule.getStatus() != 0)
            {
                map.put("msg", "1:节目单未审核通过");

                return map;
            }
        }

        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        int minutes = 15;
        usysConfig.setCfgkey("cms.schedule.cantoperate.time");
        usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
        }

        DbUtil db = new DbUtil();
        // 提前15分钟下达录制计划
        String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
        List rtnlist = null;
        try
        {
            rtnlist = db.getQuery(sql);
        }
        catch (Exception e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map tmpMap = new HashMap();
        tmpMap = (Map) rtnlist.get(0);
        String currenttime = tmpMap.get("currenttime").toString();
        /***********************************************************
         * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
         * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
         **********************************************************/

        String startdate = schedule.getStartdate();
        String starttime = schedule.getStarttime();

        if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
        {
            // 取消发布节目单失败,节目单的开始时间应大于当前时间15分钟

            map.put("msg", "1:" + "取消发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
            return map;
        }

        CntPlatformSync cntPlatformSync = platformSync;

        if (tarlist != null && tarlist.size() > 0)
        {
            Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
            for (int j = 0; j < tarlist.size(); j++)
            {
                Map<String, List> objMap = new HashMap();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync = (CntTargetSync) tarlist.get(j);
                List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                int status = tarsync.getStatus();

                Targetsystem targetSystem = new Targetsystem();
                targetSystem.setTargetindex(tarsync.getTargetindex());
                List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                if (targetlist != null && targetlist.size() > 0)
                {
                    targetSystem = (Targetsystem) targetlist.get(0);
                }
                if (targetSystem.getStatus() != 0)
                {
                    map.put("msg", "1:目标系统状态不正确");
                    continue;
                }
                if (tarsync.getStatus() == 300 || tarsync.getStatus() == 500)
                {
                    actiontype = 3;// 取消发布
                    tarsync.setOperresult(70);// 取消同步中
                }
                else
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), "操作失败",
                    // "当前状态不能发布");
                    // returnInfo.appendOperateInfo(operateInfo);
                    map.put("msg", "1:发布数据状态不正确");
                    continue;// 中断进入下一个循环
                }
                String xmladdress = "";

                ArrayList newlist = new ArrayList();
                if (list != null && list.size() > 0)
                {
                    schedule = (CmsSchedule) list.get(0);
                    CmsChannel channel = new CmsChannel();
                    channel.setChannelid(schedule.getChannelid());
                    List listchannel = channelDS.getChannelByCond(channel);
                    if (listchannel != null && listchannel.size() > 0)
                    {
                        channel = (CmsChannel) listchannel.get(0);
                    }
                    schedule.setChannelcpcontentid(channel.getCpcontentid());
                    newlist.add(schedule);
                    objMap.put("schedulelist", newlist);
                    CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                    schedulerecord.setScheduleid(schedule.getScheduleid());
                    List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(schedulerecord);
                    objMap.put("schedulerecordlist", srlist);
                }

                if (cntPlatformSync.getPlatform() == 2)
                {// 3.0平台,1个平台生成1个xml
                    xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                            targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目

                }
                else
                {

                    xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                            targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目
                }

                insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                        .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, 1);// 调用同步任务模块DS代码

                tarsync.setStatus(200);// 200发布中
                cntTargetSyncDS.updateCntTargetSync(tarsync);
                // iSuccessed++;
                //
                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), ResourceMgt
                // .findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                // returnInfo.appendOperateInfo(operateInfo);
                map.put("msg", "0:操作成功");
                pubSuccessSchedules.add(schedule);
                CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                        "log.schedulesync.mgt", "log.channelSchedule.nopublish", "log.channelSchedule.nopublish.info",
                        CategoryConstant.RESOURCE_OPERATION_SUCCESS);
            }
            CntTargetSync finaltarget = new CntTargetSync();
            finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

            if (finaltargetlist != null && finaltargetlist.size() > 0)
            {
                int[] targetSyncStatus = new int[finaltargetlist.size()];
                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                {
                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                }
                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                if (originalstatus != statussync)
                {
                    cntPlatformSync.setStatus(statussync);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
            }
        }

        // }

        if (pubSuccessSchedules.size() == 0 || ifail == pubSuccessSchedules.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < pubSuccessSchedules.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    // 节目单平台取消发布
    public HashMap cancelpublishSchedulePltSingle(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_911);
            return map;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_912);
            return map;// "31:获取临时区失失败";
        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_913);
            return map;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();
        CmsChannel channel = new CmsChannel();
        CmsSchedule schedulenew = new CmsSchedule();
        schedulenew.setScheduleid(cntPlatformSynclist.get(0).getObjectid());
        List schedulelist = cmsScheduleDS.getCmsScheduleByCond(schedulenew);
        if (schedulelist != null && schedulelist.size() > 0)
        {
            schedulenew = (CmsSchedule) schedulelist.get(0);
            if (schedulenew.getStatus() != 0)
            {
                map.put("msg", "1:节目单未审核通过");

                return map;
            }
        }
        else
        {
            map.put("msg", "1:节目单不存在");

            return map;
        }

        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        int minutes = 15;
        usysConfig.setCfgkey("cms.schedule.cantoperate.time");
        usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
        }

        DbUtil db = new DbUtil();
        // 提前15分钟下达录制计划
        String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
        List rtnlist = null;
        try
        {
            rtnlist = db.getQuery(sql);
        }
        catch (Exception e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map tmpMap = new HashMap();
        tmpMap = (Map) rtnlist.get(0);
        String currenttime = tmpMap.get("currenttime").toString();
        /***********************************************************
         * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
         * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
         **********************************************************/

        String startdate = schedulenew.getStartdate();
        String starttime = schedulenew.getStarttime();

        if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
        {
            // 取消发布节目单失败,节目单的开始时间应大于当前时间15分钟

            map.put("msg", "1:" + "取消发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
            return map;
        }

        channel.setChannelid(schedulenew.getChannelid());
        List listchannel = channelDS.getChannelByCond(channel);
        if (listchannel != null && listchannel.size() > 0)
        {
            channel = (CmsChannel) listchannel.get(0);
        }
        boolean flagbool = false;
        String msg = "";
        for (int i = 0; i < cntPlatformSynclist.size(); i++)
        {
            // 判断频道内容是否存在，判断网元状态,不用判录制计划

            CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
            List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
            {

            }
            else
            {
                map.put("msg", "1:发布数据不存在");

                return map;
            }
            int statusinit = cntPlatformSync.getStatus();
            if (statusinit == 0 || statusinit == 200 || statusinit == 400)
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "发布数据状态不正确");
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
            List tarlist = new ArrayList();
            CntTargetSync tarsync = new CntTargetSync();
            tarsync.setRelateindex(cntPlatformSync.getSyncindex());
            tarsync.setObjecttype(6);
            tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
            if (tarlist != null && tarlist.size() > 0)
            {
                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int j = 0; j < tarlist.size(); j++)
                {
                    Map<String, List> objMap = new HashMap();
                    tarsync = (CntTargetSync) tarlist.get(j);
                    List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                    {

                    }
                    else
                    {
                        msg = msg + " 发布数据不存在";

                        continue;
                    }

                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                        continue;
                    }
                    if (tarsync.getStatus() == 300 || tarsync.getStatus() == 500)
                    {
                        actiontype = 3;// 取消发布
                        tarsync.setOperresult(70);// 取消同步中
                    }
                    else
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // "失败",
                        // "当前状态不能取消发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                        continue;// 中断进入下一个循环
                    }
                    String xmladdress = "";
                    CmsSchedule schedule = new CmsSchedule();
                    schedule.setScheduleid(tarsync.getObjectid());
                    List list = cmsScheduleDS.getCmsScheduleByCond(schedule);
                    ArrayList newlist = new ArrayList();
                    if (list != null && list.size() > 0)
                    {
                        schedule = (CmsSchedule) list.get(0);
                        schedule.setChannelcpcontentid(channel.getCpcontentid());
                        newlist.add(schedule);
                        objMap.put("schedulelist", newlist);
                        CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                        schedulerecord.setScheduleid(schedule.getScheduleid());
                        List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(schedulerecord);
                        objMap.put("schedulerecordlist", srlist);
                    }

                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 6, actiontype);// 1是2.0平台2是3.0平台

                    }
                    else
                    {
                      //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 6, actiontype);
                    }
                    int pristatus = 1;
                    // bms和egp的情况且有别的网元的情况,status可为0或1
                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                            && (tarlist.size() > 0))
                    {
                        for (int count = 0; count < tarlist.size(); count++)
                        {
                            CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                            {
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if (target != null && target.size() > 0)
                                {
                                    tsnew = (Targetsystem) target.get(0);
                                }
                                if ((tsnew.getTargettype() == 2))
                                {// cdn
                                    pristatus = 0;
                                }
                                else if (tsnew.getTargettype() == 1)
                                {// bms
                                    pristatus = 0;
                                }

                            }
                        }
                    }
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                            .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, pristatus);// 调用同步任务模块DS代码

                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    flagbool = true;
                    msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";
                    // iSuccessed++;

                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                    // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    pubSuccessSchedules.add(schedule);
                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                            "log.schedulesync.mgt", "log.channelSchedule.nopublish",
                            "log.channelSchedule.nopublish.info", CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }
            }
            if (flagbool == true)
            {
                // iSuccessed++;
                // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                // "成功", msg);
                // returnInfo.appendOperateInfo(operateInfo);
                msg = "0:" + msg;
                map.put("msg", msg);
            }
            else
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                // "失败",
                // msg);
                // returnInfo.appendOperateInfo(operateInfo);
                msg = "1:" + msg;
                map.put("msg", msg);
            }

        }

        log.info("cancleschedule end...");
        return map;
    }

    // 节目单平台取消发布
    public String cancelpublishSchedulePlt(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            return CmsScheduleConstant.RESULT_CODE_911;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            return CmsScheduleConstant.RESULT_CODE_912;// "31:获取临时区失失败";
        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            return CmsScheduleConstant.RESULT_CODE_913;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();
        CmsChannel channel = new CmsChannel();
        CmsSchedule schedulenew = new CmsSchedule();
        schedulenew.setScheduleid(cntPlatformSynclist.get(0).getObjectid());
        List schedulelist = cmsScheduleDS.getCmsScheduleByID(schedulenew);
        if (schedulelist != null && schedulelist.size() > 0)
        {
            schedulenew = (CmsSchedule) schedulelist.get(0);
        }
        channel.setChannelid(schedulenew.getChannelid());
        List listchannel = channelDS.getChannelByCond(channel);
        if (listchannel != null && listchannel.size() > 0)
        {
            channel = (CmsChannel) listchannel.get(0);
        }
        boolean flagbool = false;

        for (int i = 0; i < cntPlatformSynclist.size(); i++)
        {
            // 判断频道内容是否存在，判断网元状态,不用判录制计划
            CmsSchedule schedule = new CmsSchedule();
            String msg = "";
            CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
            List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) tarsynclistinit.get(0);

                schedule.setScheduleid(cntPlatformSync.getObjectid());
                List list = cmsScheduleDS.getCmsScheduleByID(schedule);
                if (list == null || list.size() == 0)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "节目单不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    schedule = (CmsSchedule) list.get(0);
                    if (schedule.getStatus() != 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++),
                                cntPlatformSync.getObjectid().toString(), "失败", "节目单未审核通过");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }

                UsysConfig usysConfig = new UsysConfig();
                List<UsysConfig> usysConfigList = null;
                int minutes = 15;
                usysConfig.setCfgkey("cms.schedule.cantoperate.time");
                usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                if (null != usysConfigList && usysConfigList.size() > 0)
                {
                    usysConfig = usysConfigList.get(0);
                    minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
                }

                DbUtil db = new DbUtil();
                // 提前15分钟下达录制计划
                String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
                List rtnlist = null;
                try
                {
                    rtnlist = db.getQuery(sql);
                }
                catch (Exception e1)
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                Map tmpMap = new HashMap();
                tmpMap = (Map) rtnlist.get(0);
                String currenttime = tmpMap.get("currenttime").toString();
                /***********************************************************
                 * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
                 * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
                 **********************************************************/

                String startdate = schedulenew.getStartdate();
                String starttime = schedulenew.getStarttime();

                if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
                {
                    // 取消发布节目单失败,节目单的开始时间应大于当前时间15分钟

                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "取消发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;

                }
            }
            else
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "发布数据不存在");
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
            int statusinit = cntPlatformSync.getStatus();
            if (statusinit == 0 || statusinit == 200 || statusinit == 400)
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "发布数据状态不正确");
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
            List tarlist = new ArrayList();
            CntTargetSync tarsync = new CntTargetSync();
            tarsync.setRelateindex(cntPlatformSync.getSyncindex());
            tarsync.setObjecttype(6);
            tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
            if (tarlist != null && tarlist.size() > 0)
            {
                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int j = 0; j < tarlist.size(); j++)
                {
                    Map<String, List> objMap = new HashMap();
                    tarsync = (CntTargetSync) tarlist.get(j);
                    List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                    {

                    }
                    else
                    {
                        msg = msg + " 发布数据不存在";

                        continue;
                    }
                    int status = tarsync.getStatus();

                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                        continue;
                    }
                    if (tarsync.getStatus() == 300 || tarsync.getStatus() == 500)
                    {
                        actiontype = 3;// 取消发布
                        tarsync.setOperresult(70);// 取消同步中
                    }
                    else
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // "失败",
                        // "当前状态不能取消发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                        continue;// 中断进入下一个循环
                    }
                    String xmladdress = "";

                    ArrayList newlist = new ArrayList();

                    schedule.setChannelcpcontentid(channel.getCpcontentid());
                    newlist.add(schedule);
                    objMap.put("schedulelist", newlist);
                    CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                    schedulerecord.setScheduleid(schedule.getScheduleid());
                    List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByID(schedulerecord);
                    objMap.put("schedulerecordlist", srlist);

                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {
                      //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目
                    }
                    int pristatus = 1;
                    // bms和egp的情况且有别的网元的情况,status可为0或1
                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                            && (tarlist.size() > 0))
                    {
                        for (int count = 0; count < tarlist.size(); count++)
                        {
                            CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                            {
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if (target != null && target.size() > 0)
                                {
                                    tsnew = (Targetsystem) target.get(0);
                                }
                                if ((tsnew.getTargettype() == 2))
                                {// cdn
                                    pristatus = 0;
                                }
                                else if (tsnew.getTargettype() == 1)
                                {// bms
                                    pristatus = 0;
                                }

                            }
                        }
                    }
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                            .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, pristatus);// 调用同步任务模块DS代码

                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    flagbool = true;
                    msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";
                    // iSuccessed++;

                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                    // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    pubSuccessSchedules.add(schedule);
                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                            "log.schedulesync.mgt", "log.channelSchedule.nopublish",
                            "log.channelSchedule.nopublish.info", CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }
            }
            if (flagbool == true)
            {
                iSuccessed++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "成功",
                        msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        msg);
                returnInfo.appendOperateInfo(operateInfo);
            }

        }

        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    // 节目单发布到网元
    public HashMap publishScheduleTar(List<CntTargetSync> tarlist) throws Exception
    {

        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_911);
            return map;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_912);
            return map;// "31:获取临时区失失败";
        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_913);
            return map;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();

        // for (int i = 0; i < cntPlatformSynclist.size(); i++) {
        // 判断频道内容是否存在，判断网元状态,不用判断物理频道

        // List tarlist = new ArrayList();

        // tarsync.setRelateindex(cntPlatformSync.getSyncindex());
        // tarsync.setObjecttype(6);
        // tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
        CmsSchedule schedule = new CmsSchedule();
        if (tarlist != null && tarlist.size() > 0)
        {

            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            int minutes = 15;
            usysConfig.setCfgkey("cms.schedule.cantoperate.time");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
            }

            DbUtil db = new DbUtil();
            // 提前15分钟下达录制计划
            String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
            List rtnlist = null;
            rtnlist = db.getQuery(sql);
            Map tmpMap = new HashMap();
            tmpMap = (Map) rtnlist.get(0);
            String currenttime = tmpMap.get("currenttime").toString();
            /***********************************************************
             * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
             * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
             **********************************************************/

            String startdate = schedule.getStartdate();
            String starttime = schedule.getStarttime();

            if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
            {
                // 修改后的开始时间在15分钟之内的节目单不可修改
                map.put("msg", "1:发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
                return map;
            }
        }
        else
        {
            map.put("msg", "1:发布数据不存在");
            return map;
        }
        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
        for (int j = 0; j < tarlist.size(); j++)
        {

            CntPlatformSync platformSync = new CntPlatformSync();
            platformSync.setSyncindex(tarlist.get(j).getRelateindex());
            List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(platformSync);
            if (pltlist != null && pltlist.size() > 0)
            {
                platformSync = (CntPlatformSync) pltlist.get(0);

                schedule.setScheduleid(platformSync.getObjectid());
                List list = cmsScheduleDS.getCmsScheduleByCond(schedule);
                if (list == null || list.size() == 0)
                {
                    map.put("msg", "1:节目单不存在");
                    return map;
                }
                else
                {
                    schedule = (CmsSchedule) list.get(0);
                    if (schedule.getStatus() != 0)
                    {
                        map.put("msg", "1:节目单未审核通过");
                        return map;
                    }
                }

                CntTargetSync tarsync = new CntTargetSync();
                Map<String, List> objMap = new HashMap();
                tarsync = (CntTargetSync) tarlist.get(j);
                List tarsynclist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarsynclist != null && tarsynclist.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");
                    return map;
                }

                Targetsystem targetSystem = new Targetsystem();
                targetSystem.setTargetindex(tarsync.getTargetindex());
                List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                if (targetlist != null && targetlist.size() > 0)
                {
                    targetSystem = (Targetsystem) targetlist.get(0);
                }
                if (targetSystem.getStatus() != 0)
                {
                    map.put("msg", "1:目标系统状态不正确");
                    return map;
                }

                CmsChannel channel = new CmsChannel();
                channel.setChannelid(schedule.getChannelid());
                List listchannel = channelDS.getChannelByCond(channel);
                if (listchannel != null && listchannel.size() > 0)
                {
                    channel = (CmsChannel) listchannel.get(0);
                }
                CntTargetSync syncchannel = new CntTargetSync();
                syncchannel.setObjecttype(5);
                syncchannel.setObjectid(schedule.getChannelid());
                syncchannel.setTargetindex(tarsync.getTargetindex());
                List syncchannellist = cntTargetSyncDS.getCntTargetSyncByCond(syncchannel);
                if (syncchannellist != null && syncchannellist.size() > 0)
                {
                    syncchannel = (CntTargetSync) syncchannellist.get(0);
                }
                if ((syncchannel.getStatus() != 300) && (syncchannel.getStatus() != 500))
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), "操作失败",
                    // "频道没有发布成功");
                    // returnInfo.appendOperateInfo(operateInfo);
                    map.put("msg", "1:频道没有发布成功");
                    continue;// 中断进入下一个循环
                }

                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                {
                    actiontype = 1;// 新增发布
                    tarsync.setOperresult(10);// 新增同步中
                }
                else if (tarsync.getStatus() == 300 || tarsync.getStatus() == 500)
                {
                    actiontype = 2;// 修改发布
                    tarsync.setOperresult(40);// 修改同步中
                }
                else
                {

                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), "操作失败",
                    // "当前状态不能发布");
                    // returnInfo.appendOperateInfo(operateInfo);
                    map.put("msg", "1:发布数据状态不正确");
                    continue;// 中断进入下一个循环
                }

                String xmladdress = "";
                ArrayList newlist = new ArrayList();

                schedule.setChannelcpcontentid(channel.getCpcontentid());
                newlist.add(schedule);
                objMap.put("schedulelist", newlist);
                CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                schedulerecord.setScheduleid(schedule.getScheduleid());
                List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(schedulerecord);
                objMap.put("schedulerecordlist", srlist);

                if (platformSync.getPlatform() == 2)
                {// 3.0平台,1个平台生成1个xml
                    xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                            targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目

                }
                else
                {
                  //如果是2.0平台就每个网元生成一个不同的batchid
                    batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                    xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                            targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目
                }

                int status = 1;
                // bms和egp的情况且有别的网元的情况,status可为0或1
                if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                        && (tarlist.size() > 0))
                {
                    for (int count = 0; count < tarlist.size(); count++)
                    {
                        CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                        if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                        {
                            Targetsystem tsnew = new Targetsystem();
                            tsnew.setTargetindex(cs.getTargetindex());
                            List target = targetSystemDS.getTargetsystemByCond(tsnew);
                            if (target != null && target.size() > 0)
                            {
                                tsnew = (Targetsystem) target.get(0);
                            }
                            if ((tsnew.getTargettype() == 3))
                            {// cdn
                                status = 0;
                            }
                            else if (tsnew.getTargettype() == 1)
                            {// bms
                                status = 0;
                            }

                        }
                    }
                }
                insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                        .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, status);// 调用同步任务模块DS代码

                tarsync.setStatus(200);// 200发布中
                cntTargetSyncDS.updateCntTargetSync(tarsync);
                // iSuccessed++;
                //
                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), ResourceMgt
                // .findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                // returnInfo.appendOperateInfo(operateInfo);
                map.put("msg", "0:操作成功");
                pubSuccessSchedules.add(schedule);
                CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                        "log.schedulesync.mgt", "log.channelSchedule.publish", "log.channelSchedule.publish.info",
                        CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            }
            CntTargetSync finaltarget = new CntTargetSync();
            finaltarget.setRelateindex(platformSync.getSyncindex());
            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

            if (finaltargetlist != null && finaltargetlist.size() > 0)
            {
                int[] targetSyncStatus = new int[finaltargetlist.size()];
                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                {
                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                }
                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                int originalstatus = platformSync.getStatus();// 平台的原始状态
                if (originalstatus != statussync)
                {
                    platformSync.setStatus(statussync);
                    cntPlatformSyncDS.updateCntPlatformSync(platformSync);
                }
            }

        }

        // }

        if (pubSuccessSchedules.size() == 0 || ifail == pubSuccessSchedules.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < pubSuccessSchedules.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    public List checkCanPublicList(List tarlist)
    {
        ArrayList array = new ArrayList();
        for (int i = 0; i < tarlist.size(); i++)
        {
            Map<String, List> objMap = new HashMap();
            CntTargetSync tarsync = (CntTargetSync) tarlist.get(i);

            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(tarsync.getTargetindex());
            List targetlist = null;
            try
            {
                targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (targetlist != null && targetlist.size() > 0)
            {
                targetSystem = (Targetsystem) targetlist.get(0);
            }
            if (targetSystem.getStatus() != 0)
            {
                continue;
            }
            CmsSchedule schedule = new CmsSchedule();
            schedule.setScheduleid(tarsync.getObjectid());
            List list = null;
            try
            {
                list = cmsScheduleDS.getCmsScheduleByID(schedule);
            }
            catch (DomainServiceException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            if (list != null && list.size() > 0)
            {
                schedule = (CmsSchedule) list.get(0);
            }
            CntTargetSync syncchannel = new CntTargetSync();
            syncchannel.setObjecttype(5);
            syncchannel.setObjectid(schedule.getChannelid());
            syncchannel.setTargetindex(tarsync.getTargetindex());
            List syncchannellist = null;
            try
            {
                syncchannellist = cntTargetSyncDS.getCntTargetSyncByCond(syncchannel);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (syncchannellist != null && syncchannellist.size() > 0)
            {
                syncchannel = (CntTargetSync) syncchannellist.get(0);
            }
            if ((syncchannel.getStatus() != 300) && (syncchannel.getStatus() != 500))
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                // "失败", "频道没有发布成功");
                // returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }

            if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
            {
                
            }
            else if (tarsync.getStatus() == 500)
            {
                
            }
            else
            {

                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                // "操作失败", "当前状态不能发布");
                // returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }
            array.add(tarsync);
        }
        return array;
    }

    /**
     * 节目单同步单个，为提示信息用
     * 
     * @param scheduleid节目单ID
     * @param channelid
     *            频道ID
     * @return 同步结果
     * @throws Exception
     */
    public HashMap publishScheduleSingle(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_911);
            return map;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_912);
            return map;// "31:获取临时区失失败";

        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            map.put("msg", CmsScheduleConstant.RESULT_CODE_913);
            return map;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();
        CmsChannel channel = new CmsChannel();
        CmsSchedule schedulenew = new CmsSchedule();
        schedulenew.setScheduleid(cntPlatformSynclist.get(0).getObjectid());
        List schedulelist = cmsScheduleDS.getCmsScheduleByCond(schedulenew);
        if (schedulelist != null && schedulelist.size() > 0)
        {
            schedulenew = (CmsSchedule) schedulelist.get(0);
            if (schedulenew.getStatus() != 0)
            {
                map.put("msg", "1:节目单未审核通过");

                return map;
            }
        }
        else
        {
            map.put("msg", "1:节目单不存在");

            return map;
        }

        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        int minutes = 15;
        usysConfig.setCfgkey("cms.schedule.cantoperate.time");
        usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
        }

        DbUtil db = new DbUtil();
        // 提前15分钟下达录制计划
        String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
        List rtnlist = null;
        rtnlist = db.getQuery(sql);
        Map tmpMap = new HashMap();
        tmpMap = (Map) rtnlist.get(0);
        String currenttime = tmpMap.get("currenttime").toString();
        /***********************************************************
         * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
         * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
         **********************************************************/

        String startdate = schedulenew.getStartdate();
        String starttime = schedulenew.getStarttime();

        if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
        {
            // 修改后的开始时间在15分钟之内的节目单不可修改
            map.put("msg", "1:发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
            return map;
        }

        channel.setChannelid(schedulenew.getChannelid());
        List listchannel = channelDS.getChannelByCond(channel);
        if (listchannel != null && listchannel.size() > 0)
        {
            channel = (CmsChannel) listchannel.get(0);
        }
        boolean flagbool = false;
        String msg = "";
        for (int i = 0; i < cntPlatformSynclist.size(); i++)
        {
            // 判断频道内容是否存在，判断网元状态,不用判断物理频道

            CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
            List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
            {

            }
            else
            {
                map.put("msg", "1:发布数据不存在");

                return map;
            }
            if (cntPlatformSync.getStatus() == 200 || cntPlatformSync.getStatus() == 300)
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "发布数据状态不正确");
                returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }
            else
            {
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                tarsync.setObjecttype(6);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                tarlist = checkCanPublicList(tarlist);
                if (tarlist != null && tarlist.size() > 0)
                {
                    
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    for (int j = 0; j < tarlist.size(); j++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(j);
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {

                        }
                        else
                        {
                            map.put("msg", "1:发布数据不存在");

                            return map;
                        }
                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                            continue;
                        }
                        CmsSchedule schedule = new CmsSchedule();
                        schedule.setScheduleid(tarsync.getObjectid());
                        List list = cmsScheduleDS.getCmsScheduleByCond(schedule);
                        if (list != null && list.size() > 0)
                        {
                            schedule = (CmsSchedule) list.get(0);
                        }
                        CntTargetSync syncchannel = new CntTargetSync();
                        syncchannel.setObjecttype(5);
                        syncchannel.setObjectid(schedule.getChannelid());
                        syncchannel.setTargetindex(tarsync.getTargetindex());
                        List syncchannellist = cntTargetSyncDS.getCntTargetSyncByCond(syncchannel);
                        if (syncchannellist != null && syncchannellist.size() > 0)
                        {
                            syncchannel = (CntTargetSync) syncchannellist.get(0);
                        }
                        if ((syncchannel.getStatus() != 300) && (syncchannel.getStatus() != 500))
                        {
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // "失败", "频道没有发布成功");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统 " + targetSystem.getTargetid() + "频道没有发布成功";
                            continue;// 中断进入下一个循环
                        }

                        if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                        {
                            actiontype = 1;// 新增发布
                            tarsync.setOperresult(10);// 新增同步中
                        }
                        else if (tarsync.getStatus() == 500)
                        {
                            actiontype = 2;// 修改发布
                            tarsync.setOperresult(40);// 修改同步中
                        }
                        else
                        {

                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // "操作失败", "当前状态不能发布");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统 " + targetSystem.getTargetid() + "发布数据状态不正确";
                            continue;// 中断进入下一个循环
                        }

                        String xmladdress = "";
                        ArrayList newlist = new ArrayList();
                        if (list != null && list.size() > 0)
                        {// 为schedule的xml做准备
                            schedule = (CmsSchedule) list.get(0);

                            schedule.setChannelcpcontentid(channel.getCpcontentid());
                            newlist.add(schedule);
                            objMap.put("schedulelist", newlist);
                            CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                            schedulerecord.setScheduleid(schedule.getScheduleid());
                            List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(schedulerecord);
                            objMap.put("schedulerecordlist", srlist);
                        }

                        if (cntPlatformSync.getPlatform() == 2)
                        {// 3.0平台,1个平台生成1个xml
                            xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目

                        }
                        else
                        {
                          //如果是2.0平台就每个网元生成一个不同的batchid
                            batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                            xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目
                        }

                        int status = 1;
                        // bms和egp的情况且有别的网元的情况,status可为0或1
                        if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                                && (tarlist.size() > 0))
                        {
                            for (int count = 0; count < tarlist.size(); count++)
                            {
                                CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                {
                                    Targetsystem tsnew = new Targetsystem();
                                    tsnew.setTargetindex(cs.getTargetindex());
                                    List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                    if (target != null && target.size() > 0)
                                    {
                                        tsnew = (Targetsystem) target.get(0);
                                    }
                                    if ((tsnew.getTargettype() == 3))
                                    {// cdn
                                        status = 0;
                                    }
                                    else if (tsnew.getTargettype() == 1)
                                    {// bms
                                        status = 0;
                                    }

                                }
                            }
                        }
                        insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                                .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, status);// 调用同步任务模块DS代码

                        tarsync.setStatus(200);// 200发布中
                        cntTargetSyncDS.updateCntTargetSync(tarsync);
                        // iSuccessed++;
                        //
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                        // returnInfo.appendOperateInfo(operateInfo);
                        flagbool = true;
                        msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":操作成功";
                        pubSuccessSchedules.add(schedule);

                        cntPlatformSync.setStatus(200);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                                "log.schedulesync.mgt", "log.channelSchedule.publish",
                                "log.channelSchedule.publish.info", CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }else{
                    msg = msg + " 频道没有发布成功";
                }

            }
            if (flagbool == true)
            {
                // iSuccessed++;
                // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                // "成功",
                // msg);
                // returnInfo.appendOperateInfo(operateInfo);
                msg = "0:" + msg;
                map.put("msg", msg);
            }
            else
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                // "失败",
                // msg);
                // returnInfo.appendOperateInfo(operateInfo);
                msg = "1:" + msg;
                map.put("msg", msg);
            }
        }

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    /**
     * 节目单同步
     * 
     * @param scheduleid节目单ID
     * @param channelid
     *            频道ID
     * @return 同步结果
     * @throws Exception
     */
    public String publishSchedule(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        int actiontype = 0;// 发布操作
        Integer ifail = 0;
        Integer iSuccessed = 0;
        String flag = "";
        String errorcode = "";
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("publishSchedule  starting in the class CmsScheduleLS................");
        String mountPoint = getMountPoint();
        if ((mountPoint == null) || (mountPoint.trim().equals("")))
        {
            return CmsScheduleConstant.RESULT_CODE_911;// "31:获取挂载点失败";
        }
        String tempAreaPath = getTempAreAddr();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            return CmsScheduleConstant.RESULT_CODE_912;// "31:获取临时区失失败";
        }
        String synXMLFTPAdd = getSynXMLFTPAddr();
        if (synXMLFTPAdd == null)
        {
            return CmsScheduleConstant.RESULT_CODE_913;// "31:获取同步XML文件FTP地址失败";
        }
        List pubSuccessSchedules = new ArrayList();
        CmsChannel channel = new CmsChannel();
        CmsSchedule schedulenew = new CmsSchedule();
        schedulenew.setScheduleid(cntPlatformSynclist.get(0).getObjectid());
        List schedulelist = cmsScheduleDS.getCmsScheduleByID(schedulenew);
        if (schedulelist != null && schedulelist.size() > 0)
        {
            schedulenew = (CmsSchedule) schedulelist.get(0);
        }
        channel.setChannelid(schedulenew.getChannelid());
        List listchannel = channelDS.getChannelByCond(channel);
        if (listchannel != null && listchannel.size() > 0)
        {
            channel = (CmsChannel) listchannel.get(0);
        }
        boolean flagbool = false;

        for (int i = 0; i < cntPlatformSynclist.size(); i++)
        {
            // 判断频道内容是否存在，判断网元状态,不用判断物理频道
            CmsSchedule schedule = new CmsSchedule();
            String msg = "";
            CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
            List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
            {

                schedule.setScheduleid(cntPlatformSync.getObjectid());
                List list = cmsScheduleDS.getCmsScheduleByID(schedule);
                if (list != null && list.size() > 0)
                {
                    schedule = (CmsSchedule) list.get(0);
                    if (schedule.getStatus() != 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++),
                                cntPlatformSync.getObjectid().toString(), "失败", "节目单未审核通过");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;// 中断进入下一个循环
                    }
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "节目单不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }

                UsysConfig usysConfig = new UsysConfig();
                List<UsysConfig> usysConfigList = null;
                int minutes = 15;
                usysConfig.setCfgkey("cms.schedule.cantoperate.time");
                usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                if (null != usysConfigList && usysConfigList.size() > 0)
                {
                    usysConfig = usysConfigList.get(0);
                    minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
                }

                DbUtil db = new DbUtil();
                // 提前15分钟下达录制计划
                String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
                List rtnlist = null;
                rtnlist = db.getQuery(sql);
                Map tmpMap = new HashMap();
                tmpMap = (Map) rtnlist.get(0);
                String currenttime = tmpMap.get("currenttime").toString();
                /***********************************************************
                 * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
                 * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
                 **********************************************************/

                String startdate = schedulenew.getStartdate();
                String starttime = schedulenew.getStarttime();

                if ((DateUtil2.get14Time(startdate + starttime)).compareToIgnoreCase(currenttime) < 0)
                {
                    // 修改后的开始时间在15分钟之内的节目单不可修改
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "发布节目单失败,节目单的开始时间应大于当前时间" + minutes + "分钟");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
            }
            else
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "发布数据不存在");
                returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }
            if (cntPlatformSync.getStatus() == 200 || cntPlatformSync.getStatus() == 300)
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        "节目单当前状态不能发布");
                returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }
            else
            {
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                tarsync.setObjecttype(6);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {
                    tarlist = checkCanPublicList(tarlist);
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    for (int j = 0; j < tarlist.size(); j++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(j);
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        
                        if (tarsynclistinittar == null || tarsynclistinittar.size() <= 0)
                        {                        
                            msg = msg + " 发布数据不存在";
                            continue;
                        }
                        /*
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {

                        }
                        else
                        {
                            msg = msg + " 发布数据不存在";

                            continue;
                        }
                        */
                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                            continue;
                        }

                        CntTargetSync syncchannel = new CntTargetSync();
                        syncchannel.setObjecttype(5);
                        syncchannel.setObjectid(schedule.getChannelid());
                        syncchannel.setTargetindex(tarsync.getTargetindex());
                        List syncchannellist = cntTargetSyncDS.getCntTargetSyncByCond(syncchannel);
                        if (syncchannellist != null && syncchannellist.size() > 0)
                        {
                            syncchannel = (CntTargetSync) syncchannellist.get(0);
                        }
                        if ((syncchannel.getStatus() != 300) && (syncchannel.getStatus() != 500))
                        {
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // "失败", "频道没有发布成功");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统 " + targetSystem.getTargetid() + "频道没有发布成功";
                            continue;// 中断进入下一个循环
                        }

                        if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                        {
                            actiontype = 1;// 新增发布
                            tarsync.setOperresult(10);// 新增同步中
                        }
                        else if (tarsync.getStatus() == 500)
                        {
                            actiontype = 2;// 修改发布
                            tarsync.setOperresult(40);// 修改同步中
                        }
                        else
                        {

                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // "操作失败", "当前状态不能发布");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统 " + targetSystem.getTargetid() + "发布数据状态不正确";
                            continue;// 中断进入下一个循环
                        }

                        String xmladdress = "";
                        ArrayList newlist = new ArrayList();

                        schedule.setChannelcpcontentid(channel.getCpcontentid());
                        newlist.add(schedule);
                        objMap.put("schedulelist", newlist);
                        CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                        schedulerecord.setScheduleid(schedule.getScheduleid());
                        List srlist = cmsSchedulerecordDS.getCmsSchedulerecordByID(schedulerecord);
                        objMap.put("schedulerecordlist", srlist);

                        if (cntPlatformSync.getPlatform() == 2)
                        {// 3.0平台,1个平台生成1个xml
                            xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目

                        }
                        else
                        {
                          //如果是2.0平台就每个网元生成一个不同的batchid
                            batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                            xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 6, actiontype);// 文广1是regist2是栏目
                        }
                        int status = 1;
                        // bms和egp的情况且有别的网元的情况,status可为0或1
                        if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                                && (tarlist.size() > 0))
                        {
                            for (int count = 0; count < tarlist.size(); count++)
                            {
                                CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                {
                                    Targetsystem tsnew = new Targetsystem();
                                    tsnew.setTargetindex(cs.getTargetindex());
                                    List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                    if (target != null && target.size() > 0)
                                    {
                                        tsnew = (Targetsystem) target.get(0);
                                    }
                                    if ((tsnew.getTargettype() == 3))
                                    {// cdn
                                        status = 0;
                                    }
                                    else if (tsnew.getTargettype() == 1)
                                    {// bms
                                        status = 0;
                                    }

                                }
                            }
                        }
                        insertSyncTask(tarsync.getTargetindex(), xmladdress, schedule.getScheduleindex(), schedule
                                .getScheduleid(), 6, actiontype, schedule.getCpcontentid(), batchid, status);// 调用同步任务模块DS代码

                        tarsync.setStatus(200);// 200发布中
                        cntTargetSyncDS.updateCntTargetSync(tarsync);
                        // iSuccessed++;
                        //
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                        // returnInfo.appendOperateInfo(operateInfo);
                        flagbool = true;
                        msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":操作成功";
                        pubSuccessSchedules.add(schedule);

                        cntPlatformSync.setStatus(200);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);

                        CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                                "log.schedulesync.mgt", "log.channelSchedule.publish",
                                "log.channelSchedule.publish.info", CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }

            }
            if (flagbool == true)
            {
                iSuccessed++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "成功",
                        msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                ifail++;
                operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(), "失败",
                        msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
        }

        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    /**
     * 节目单时间平移
     * 
     * @param scheduleid节目单ID集合
     * @param channelid频道编码
     * @return 节目单时间平移详细描述
     * @throws Exception
     */
    public String cancelAdjustTime(List<String> scheduleidList, List<String> adjustTimeArr) throws Exception
    {
        log.debug("cancelPublishSchedule  starting.................................");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int ok = 0;
        int error = 0;
        int index = 1;
        boolean isAllowAdjust = true;
        List<CmsSchedule> isRepeatedList = new ArrayList<CmsSchedule>();
        try
        {
            boolean flag = true;
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            int minutes = 15;
            usysConfig.setCfgkey("cms.schedule.cantoperate.time");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
            }
            DbUtil db = new DbUtil();
            // 提前15分钟下达录制计划
            String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
            List rtnlist = null;
            rtnlist = db.getQuery(sql);
            Map tmpMap = new HashMap();
            tmpMap = (Map) rtnlist.get(0);
            String currenttime = tmpMap.get("currenttime").toString();

            String channelid = adjustTimeArr.get(2);
            String incordec = adjustTimeArr.get(0);
            String duration = this.formatDuration(adjustTimeArr.get(1));
            String valiChannel = this.getChannelByChannelid(channelid);
            if (valiChannel.equals("success"))
            {
                // 验证选择的节目单是否连续 避免页面未刷新导致节目单不连续
                CmsSchedule lastSchedule = this.getScheduleById(scheduleidList.get(0));
                CmsSchedule firstSchedule = this.getScheduleById(scheduleidList.get(scheduleidList.size() - 1));
                String countSchSql = "select count(*) schcount from cms_schedule where channelid=" + channelid
                        + " and concat(startdate,starttime)>= "
                        + (firstSchedule.getStartdate() + firstSchedule.getStarttime())
                        + " and concat(startdate,starttime) <="
                        + (lastSchedule.getStartdate() + lastSchedule.getStarttime());
                List countSchlist = null;
                countSchlist = db.getQuery(countSchSql);
                Map countSchMap = new HashMap();
                countSchMap = (Map) countSchlist.get(0);
                String count = countSchMap.get("schcount").toString();
                if (!(scheduleidList.size() + "").equals(count))
                {
                    return "930";
                }

                for (String scheduleid : scheduleidList)
                {
                    CmsSchedule cmsSchedule = new CmsSchedule();
                    cmsSchedule = this.getScheduleById(scheduleid);
                    if (cmsSchedule != null)
                    {
                        if (cmsSchedule.getStatus() != 110)
                        {
                            return "921";
                        }
                        /**
                         * if
                         * ((cmsSchedule.getStartdate()+cmsSchedule.getStarttime()).compareToIgnoreCase(currenttime)<0){
                         * //现在15分钟之内即将开始的节目单不可修改 //return CmsScheduleConstant.RESULT_CODE_111; return "926"; }
                         */
                        String startdate = cmsSchedule.getStartdate();
                        String starttime = cmsSchedule.getStarttime();
                        String newtime;
                        if (incordec.equals("increase"))
                        {
                            newtime = FileSaveUtil.getDuration((startdate + starttime), String.valueOf(duration));
                        }
                        else
                        {
                            newtime = FileSaveUtil.getDuration((startdate + starttime), String.valueOf("-" + duration));
                        }
                        cmsSchedule.setStartdate(newtime.substring(0, 8));
                        cmsSchedule.setStarttime(newtime.substring(8, 14));

                        /*******************************************************
                         * if
                         * (DateUtil2.get14Time(newtime).compareToIgnoreCase(currenttime)<0){//修改后的开始时间在15分钟之内的节目单不可修改
                         * ereturn "927"; }
                         ******************************************************/

                        isRepeatedList = this.isTimeRepeated(cmsSchedule);
                        // 排除时间重叠的节目单集合中待平移的节目单
                        for (int i = 0; i < isRepeatedList.size(); i++)
                        {
                            CmsSchedule schedule = isRepeatedList.get(i);
                            for (String id : scheduleidList)
                            {
                                if (schedule.getScheduleid().equals(id))
                                {
                                    isRepeatedList.remove(i);
                                }
                            }
                        }
                        if (null != isRepeatedList && isRepeatedList.size() > 0)
                        {
                            for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                            {
                                CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
                                cmsSchedulerecord.setScheduleid(cmsSchedulefortime.getScheduleid());
                                List<CmsSchedulerecord> schedulerecordList = cmsSchedulerecordDS
                                        .getCmsSchedulerecordByCond(cmsSchedulerecord);
                                // 需要平移时间的当前节目影响到的其他节目的状态不是"待审核","审核失败","待发布"的，不可平移
                                if (cmsSchedulefortime.getStatus() != CmsScheduleConstant.STATUS_110
                                        && cmsSchedulefortime.getStatus() != CmsScheduleConstant.STATUS_130
                                        && cmsSchedulefortime.getStatus() != CmsScheduleConstant.STATUS_0)
                                {
                                    return "922";
                                    /**
                                     * }else if((cmsSchedulefortime.getStartdate()+cmsSchedulefortime.getStarttime()).
                                     * compareToIgnoreCase(currenttime)<0){ //本次修改影响到的schedual的播出时间距离当前时间小于X分钟，都不允许实施修改
                                     * return "928";
                                     */
                                }
                                else
                                {
                                    if (schedulerecordList != null && schedulerecordList.size() > 0)
                                    {
                                        for (CmsSchedulerecord cmsSchedulerecord1 : schedulerecordList)
                                        {
                                            if (cmsSchedulerecord1.getStatus() == 10
                                                    && cmsSchedulerecord1.getStatus() == 40
                                                    && cmsSchedulerecord1.getStatus() == 70)
                                            {
                                                // 需要修改的当前节目影响到的其他节目的收录计划的状态在“xx中”，不可平移
                                                // return
                                                // CmsScheduleConstant.RESULT_CODE_914;
                                                return "923";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        error++;
                        operateInfo = new OperateInfo(String.valueOf(index++), scheduleid, "操作失败", "该节目单已被删除");
                        returnInfo.appendOperateInfo(operateInfo);
                        // continue;
                        return "929";
                    }
                }
                if (isAllowAdjust)
                {
                    cmsScheduleDS.removeCmsScheduleList(isRepeatedList);
                    for (String scheduleid : scheduleidList)
                    {
                        CmsSchedule cmsSchedule = new CmsSchedule();
                        cmsSchedule = this.getScheduleById(scheduleid);
                        if (cmsSchedule != null)
                        {
                            String startdate = cmsSchedule.getStartdate();
                            String starttime = cmsSchedule.getStarttime();
                            String newtime;
                            if (incordec.equals("increase"))
                            {
                                newtime = FileSaveUtil.getDuration((startdate + starttime), String.valueOf(duration));
                            }
                            else
                            {
                                newtime = FileSaveUtil.getDuration((startdate + starttime), String.valueOf("-"
                                        + duration));
                            }
                            cmsSchedule.setStartdate(newtime.substring(0, 8));
                            cmsSchedule.setStarttime(newtime.substring(8, 14));

                            CmsSchedule schedule = new CmsSchedule();
                            schedule.setScheduleindex(cmsSchedule.getScheduleindex());
                            schedule.setStartdate(newtime.substring(0, 8));
                            schedule.setStarttime(newtime.substring(8, 14));
                            String sql1 = "select to_char(sysdate, 'yyyymmddhh24miss') lastupdatetime from dual";// 查询数据库较当前时间
                            List rtnlist1 = null;
                            rtnlist1 = db.getQuery(sql1);
                            Map tmpMap1 = new HashMap();
                            tmpMap1 = (Map) rtnlist1.get(0);
                            String lastupdatetime = tmpMap1.get("lastupdatetime").toString();
                            schedule.setLastupdatetime(lastupdatetime);
                            ok++;
                            operateInfo = new OperateInfo(String.valueOf(index++), scheduleid, "操作成功", "该节目单开始时间已调整");
                            returnInfo.appendOperateInfo(operateInfo);
                            cmsScheduleDS.updateCmsSchedule(schedule);
                            CommonLogUtil.insertOperatorLog(scheduleid, CmsScheduleConstant.MGTTYPE_CHANNELSCHEDULE,
                                    CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_ADJUSTTIME,
                                    CmsScheduleConstant.OPERTYPE_CMSSCHEDULE_ADJUSTTIME_INFO,
                                    CmsScheduleConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        else
                        {
                            error++;
                            operateInfo = new OperateInfo(String.valueOf(index++), scheduleid, "操作失败", "该节目单已被删除");
                            returnInfo.appendOperateInfo(operateInfo);
                            // continue;
                            return "929";
                        }
                    }
                }
                else
                {
                    return "924";
                }
            }
            if ((ok) == scheduleidList.size())
            {
                returnInfo.setReturnMessage("成功数量" + ok);
                returnInfo.setFlag("0");
            }
            else
            {
                if ((error) == scheduleidList.size())
                {
                    returnInfo.setReturnMessage("失败数量" + error);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量" + ok + " 失败数量" + error);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "fail";
        }
        return "925";// returnInfo.toString();
    }

    /** 批量录入节目单信息, 修改by fc 根据改频道下的所有物理频道生成录制计划 */
    public String scheduleBatchImport() throws Exception
    {
        log.debug("scheduleBatchImport starting...");
        int ok = 0;
        int error = 0;
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileItemList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        FileItem fileItem = fileItemList.get(0);
        String filename = fileItem.getName();// 获得文件名字和后缀
        String fileExt = FileSaveUtil.getFileExt(filename);
        if (fileExt == null || fileExt.equals("") || !fileExt.equalsIgnoreCase("txt"))// 判断文间格式是否合法
        {
            return null;
        }
        if (fileItem.getSize() <= 0)
        {
            return null;
        }

        String name = null;
        String[] nameArray = null;

        if (filename.lastIndexOf("/") != -1)
        {
            nameArray = filename.split("/");
        }
        else
        {
            nameArray = filename.split("\\\\");
        }
        name = nameArray[(nameArray.length - 1)];
        name = name.substring(0, (name.length() - 4));
        String channelname = name.substring(8, name.length());// 获得频道名字
        String timename = name.substring(0, 8);// 第一天节目日期
        CmsChannel cmsChannel = new CmsChannel();// 实例化一个频道对象，用于验证频道名是否存在
        cmsChannel.setChannelname(channelname);
        try
        {
            List<CmsChannel> list = channelDS.getChannelByCond(cmsChannel);
            if (list != null && list.size() > 0)
            {
                boolean flag = false;
                for (CmsChannel channel : list)
                {
                    if (cmsChannel.getChannelname().equals(channelname))
                    {
                        flag = true;
                        cmsChannel = channel;
                    }
                }

                if (!flag)
                {
                    cmsChannel = null;
                }
            }
        }
        catch (Exception e)
        {
            log.debug(e);
        }
        if (cmsChannel == null || cmsChannel.getChannelid() == null || cmsChannel.getChannelid().equals(""))
        {
            return "1:频道不存在";
        }
        // if (cmsChannel.getStatus() != CmsScheduleConstant.STATUS_20
        // && cmsChannel.getStatus() != CmsScheduleConstant.STATUS_50
        // && cmsChannel.getStatus() != CmsScheduleConstant.STATUS_60) {
        // return "1:当前频道未同步成功,无法录入";
        // }
        Physicalchannel phychannel = new Physicalchannel();
        phychannel.setChannelid(cmsChannel.getChannelid());
        List phylist = physicalchannelDS.getPhysicalchannelByCond(phychannel);
        if (phylist != null && phylist.size() > 0)
        {

        }
        else
        {
            return "1:当前频道没有物理频道,无法录入";
        }
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        String cpid = null;
        if (ObjectType.OPER_SUPER_ID.equals(operinfo.getOperid()))// 判断当前操作员是否是super
        {
            cpid = cmsChannel.getCpid();
        }
        else
        {
            RIAContext contexts = RIAContext.getCurrentInstance();
            ISession session = contexts.getSession();
            HttpSession httpSession = session.getHttpSession();// 冲session中获得当前CPID
            String id = (String) httpSession.getAttribute("CPID");
            if (cmsChannel.getCpid().equals(id))
            {// 频道cpid是否属于当前操作员的CPID
                cpid = cmsChannel.getCpid();
            }
            else if (id == null)
            {
                return "1:当前操作员无法录入";
            }
            else
            {
                return "1:频道不属于当前CP";
            }
        }
        if (null != cpid && !cpid.equals(""))
        { // 判断cp有没有被删除，如果被删除则不能执行插入操作
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCpid(cpid);
            ucpBasic.setCptype(1);
            try
            {
                List list = basicDS.getUcpBasicByCond(ucpBasic);
                if (null != list)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == 4)
                    {
                        return "1:内容提供商已经被删除"; // 表示cp已被删除
                    }
                }
            }
            catch (DomainServiceException e)
            {
                log.error(e);
            }
        }
        List<ScheduleNum> list = new ArrayList<ScheduleNum>();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {// 读取节目信息
            BufferedReader br = new BufferedReader(new InputStreamReader(fileItem.getInputStream()));
            String msg = null;
            String[] dateTimesArray = null;
            String date = null;
            int t = 1;// 行标号
            while ((msg = br.readLine()) != null && msg != "")
            {
                if (t > 500)
                {
                    return "1:数据超过500行，请分批上传";
                }
                String balk = "1:第" + t + "行不符合要求";
                if (msg.length() < 5)
                {
                    return balk;
                }
                String cost = msg.trim().substring(0, 4);
                CmsSchedule cmsSchedule = new CmsSchedule();
                if (cost.equals("Date"))
                {// 验证日期格式
                    dateTimesArray = msg.split(":");
                    if (dateTimesArray.length == 2)
                    {
                        if (!dateTimesArray[0].trim().equals("Date"))
                        {
                            return balk;
                        }
                        if (!FileSaveUtil.checkint(dateTimesArray[1].trim())
                                || Integer.valueOf(dateTimesArray[1].trim()) > 20501231)
                        {// 节目开始日期数字验证
                            return "1:第" + t + "行开始时间不能大于2050年12月31日";
                        }
                        if (dateTimesArray[1].trim().length() == 8
                                && FileSaveUtil.isShortDate(dateTimesArray[1].trim()))
                        {
                            if (t == 1)
                            {
                                if (!timename.equals(dateTimesArray[1].trim()))
                                {
                                    return "1:第一天节目时间应与节目文件名时间一致";
                                }
                            }
                            date = dateTimesArray[1].trim();
                        }
                        else
                        {
                            return balk;
                        }

                    }
                    else
                    {
                        return balk;
                    }

                }
                else
                {
                    dateTimesArray = msg.split("\\|");
                    if (dateTimesArray.length != 3)
                    {
                        return balk;
                    }
                    else
                    {
                        String time = dateTimesArray[0].trim();// 获得节目开始时间
                        String times = "";
                        String[] stime = time.split(":");// 获得时间
                        if (stime.length != 3)
                        {
                            return balk;
                        }
                        else
                        {
                            for (int f = 0; f < stime.length; f++)
                            {// 验证时间是否合法
                                if (!FileSaveUtil.checkint(stime[f]))
                                {// 节目开始时间数字验证
                                    return balk;
                                }
                                int j = Integer.parseInt(stime[f]);
                                if (((f == 0 && j > 23) || (f == 1 && j > 59) || (f == 2 && j > 59))
                                        || stime[f].length() != 2)
                                {
                                    return balk;
                                }
                                times += stime[f];
                            }

                            cmsSchedule.setStarttime(date + times);// 获得节目开始日期时间
                            String duration = dateTimesArray[1].trim();// 获得节目时长
                            if (!FileSaveUtil.checkint(duration))
                            {// 节目时常数字验证
                                return balk;
                            }
                            if (duration.length() != 6)
                            {
                                return "1:第" + t + "行：节目时长格式是HH24MMSS的6位数";
                            }
                            if (duration.length() > 6)
                            {
                                return "1:第" + t + "行：节目时长不能超过6位数";
                            }
                            if (Long.parseLong(duration) <= 0)
                            {
                                return "1:第" + t + "行：节目时长须大于0秒";
                            }
                            if (Long.parseLong(duration.substring(0, 1)) < 3)
                            {
                                if (Long.parseLong(duration.substring(0, 1)) == 0
                                        && Long.parseLong(duration.substring(1, 2)) <= 9
                                        && Long.parseLong(duration.substring(2, 3)) <= 5
                                        && Long.parseLong(duration.substring(3, 4)) <= 9
                                        && Long.parseLong(duration.substring(4, 5)) <= 5
                                        && Long.parseLong(duration.substring(5, 6)) <= 9)
                                {
                                }
                                else if (Long.parseLong(duration.substring(0, 1)) == 1
                                        && Long.parseLong(duration.substring(1, 2)) <= 9
                                        && Long.parseLong(duration.substring(2, 3)) <= 5
                                        && Long.parseLong(duration.substring(3, 4)) <= 9
                                        && Long.parseLong(duration.substring(4, 5)) <= 5
                                        && Long.parseLong(duration.substring(5, 6)) <= 9)
                                {
                                }
                                else if (Long.parseLong(duration.substring(0, 1)) == 2
                                        && Long.parseLong(duration.substring(1, 2)) <= 3
                                        && Long.parseLong(duration.substring(2, 3)) <= 5
                                        && Long.parseLong(duration.substring(3, 4)) <= 9
                                        && Long.parseLong(duration.substring(4, 5)) <= 5
                                        && Long.parseLong(duration.substring(5, 6)) <= 9)
                                {
                                }
                                else
                                {
                                    return "1:第" + t + "行：节目时长格式是HH24MMSS";
                                }
                            }
                            else
                            {
                                return "1:第" + t + "行：节目时长格式是HH24MMSS";
                            }
                            cmsSchedule.setDuration((duration));// 增加节目时长

                            String valid = "!@#$%^&*;|?()[]{}<>'\"";
                            if (dateTimesArray[2].trim() == null || dateTimesArray[2].trim().equals(""))
                            {
                                return balk;
                            }
                            if (dateTimesArray[2].trim().length() > 128
                                    || FileSaveUtil.exlen(dateTimesArray[2].trim(), 128, "en"))
                            {// 长度验证
                                return "1:第" + t + "行：节目名不能超过128个字符（一个汉字占3个字符）";

                            }
                            else if (FileSaveUtil.checkstring(dateTimesArray[2].trim(), valid))
                            {// 非法字符验证
                                return "1:第" + t + "行：节目名出现非法字符!@#$%^&*;|?()[]{}<>'\"";
                            }
                            else
                            {
                                cmsSchedule.setProgramname(dateTimesArray[2].trim());// 添加节目名称
                            }
                            cmsSchedule.setChannelid(cmsChannel.getChannelid());// 添加频道编码
                            ScheduleNum scheduleNum = new ScheduleNum(t, cmsSchedule);
                            list.add(scheduleNum);// 添加节目单信息
                        }
                    }

                }
                t = t + 1;// 下一行行号
            }
            int index = 1;
            for (ScheduleNum scheduleNum : list)
            {
                String valid = insertCmsScheduleAndRecordBatch(scheduleNum.getCmsSchedule());
                String tips = "";
                String message = "";
                if (valid.equals("success"))
                {
                    ok++;
                    tips = "操作成功";
                    message = "批量导入节目单成功";
                }
                else
                {
                    error++;
                    if (valid.equals(CmsScheduleConstant.RESULT_CODE_110))
                    {
                        tips = "操作失败";
                        message = "频道状态不正确";
                    }
                    else if (valid.equals(CmsScheduleConstant.RESULT_CODE_111))
                    {
                        tips = "操作失败";
                        message = "该节目即将开始";
                    }
                    else if (valid.equals(CmsScheduleConstant.RESULT_CODE_112))
                    {
                        tips = "操作失败";
                        message = "频道所属CP已删除";
                    }
                    else if (valid.equals(CmsScheduleConstant.RESULT_CODE_113))
                    {
                        tips = "操作失败";
                        message = "该节目开始时间与其他节目开始时间重叠";
                    }
                    else if (valid.equals(CmsScheduleConstant.RESULT_CODE_114))
                    {
                        tips = "操作失败";
                        message = "频道已被删除";
                    }
                }
                if (index <= 100)
                {
                    operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(scheduleNum.getNum()), tips,
                            message);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else if (index == 101)
                {
                    index++;
                    String messages = "...";
                    operateInfo = new OperateInfo(messages, messages, messages, messages);
                    returnInfo.appendOperateInfo(operateInfo);
                }
            }

        }
        catch (IOException e)
        {
            log.error(e);
        }
        if ((ok) == list.size())
        {
            returnInfo.setReturnMessage("成功数量：" + ok);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((error) == list.size())
            {
                returnInfo.setReturnMessage("失败数量：" + error);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + ok + "  失败数量：" + error);
                returnInfo.setFlag("2");
            }
        }
        log.debug("scheduleBatchImport end...");
        return returnInfo.toString();

    }

    public String getMountPoint() throws Exception
    {
        String moutPoint = GlobalConstants.getMountPoint();
        return moutPoint;
    }

    public String getSynXMLFTPAddr() throws Exception
    {
        log.debug("getSynXMLFTPAdd starting  in the class CmsScheduleLS................");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig || null == usysConfig.getCfgvalue() || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end in the class CmsScheduleLS.............");
        return synXMLFTPAddress;
    }

    // 获取临时区地址
    public String getTempAreAddr() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1");// 在线区
        return stroageareaPath;
    }

    // 将字符中\\"转换成/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 获取时间 "20111002"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    public List<CmsSchedule> isTimeRepeated(CmsSchedule cmsSchedule)
    {
        List<CmsSchedule> list = new ArrayList<CmsSchedule>();
        List<CmsSchedule> isRepeatedList = new ArrayList<CmsSchedule>();
        try
        {
            String startdate1 = cmsSchedule.getStartdate();
            String starttime1 = cmsSchedule.getStarttime();
            String start = "";
            if (starttime1.length() == 14)
            {
                start = starttime1;
            }
            else if (starttime1.length() == 19)
            {
                start = DateUtil2.get14Time(starttime1);
            }
            else
            {
                start = startdate1 + starttime1;
            }
            String duration = cmsSchedule.getDuration();
            duration = formatDuration(duration);
            String end = "";
            try
            {
                end = FileSaveUtil.getDuration(start, String.valueOf(duration));
            }
            catch (ParseException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            CmsSchedule cmsScheduleTime = new CmsSchedule();
            cmsScheduleTime.setChannelid(cmsSchedule.getChannelid());
            // list=cmsScheduleDS.isTimeRepeated(cmsScheduleTime);
            list = cmsScheduleDS.getCmsScheduleByCond(cmsScheduleTime);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {

                    CmsSchedule cmsScheduleTimechange = new CmsSchedule();
                    cmsScheduleTimechange = list.get(i);
                    String starttime = cmsScheduleTimechange.getStartdate() + cmsScheduleTimechange.getStarttime();
                    String endtime = "";
                    try
                    {
                        starttime = DateUtil2.get14Time(starttime);
                        String durationforvalid = cmsScheduleTimechange.getDuration();
                        durationforvalid = formatDuration(durationforvalid);
                        endtime = FileSaveUtil.getDuration(starttime, String.valueOf(durationforvalid));
                    }
                    catch (ParseException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if ((start.compareToIgnoreCase(starttime) >= 0 && start.compareToIgnoreCase(endtime) < 0)
                            || (end.compareToIgnoreCase(starttime) > 0 && end.compareToIgnoreCase(endtime) <= 0)
                            || (starttime.compareToIgnoreCase(start) >= 0 && starttime.compareToIgnoreCase(end) < 0)
                            || ((endtime.compareToIgnoreCase(start) > 0 && endtime.compareToIgnoreCase(end) <= 0)))
                    {// 当前要修改的节目单和已存在的节目单时间重叠
                        if (!cmsScheduleTimechange.getScheduleid().equals(cmsSchedule.getScheduleid()))
                        {// 排除自己
                            isRepeatedList.add(cmsScheduleTimechange);
                        }
                    }
                }
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isRepeatedList;
    }

    private String formatDuration(String duration)
    {
        int hour;
        int minute;
        int second;
        hour = Integer.parseInt(duration.substring(0, 2));
        minute = Integer.parseInt(duration.substring(2, 4));
        second = Integer.parseInt(duration.substring(4, 6));
        duration = hour * 60 * 60 + minute * 60 + second + "";
        return duration;
    }

    /**
     * 平台关联页面的查询方法，需有频道id,专门为节目单写的方法
     * 
     * @throws DomainServiceException
     */
    public TableDataInfo popForPubSchedule(CntTargetSync cntTargetSync, int start, int pageSize)
            throws DomainServiceException
    {

        Targetsystem targetsystem = new Targetsystem();
        cntTargetSync.setObjecttype(5);
        cntTargetSync.setObjectid(cntTargetSync.getObjectid());
        // targetsystem.setPlatforms("1,2");
        // targetsystem.setTargettypes("0,1,2,3");
        TableDataInfo dataInfo = new TableDataInfo();
        String targetindexes = "";
        List list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                CntTargetSync tarsync = (CntTargetSync) list.get(i);
                Targetsystem system = new Targetsystem();
                system.setTargetindex(tarsync.getTargetindex());
                List listtarget = targetSystemDS.getTargetsystemByCond(system);
                if (listtarget != null && listtarget.size() > 0)
                {
                    system = (Targetsystem) listtarget.get(0);
                    long targetindex = system.getTargetindex();
                    if (!targetindexes.equals(""))
                    {
                        targetindexes = targetindexes + "," + targetindex;
                    }
                    else
                    {
                        targetindexes = targetindexes + targetindex;
                    }

                }
            }

            targetsystem.setTargetindexes(targetindexes);
            targetsystem.setStatus(0);// 0是正常
            log.debug("pageInfoQuery  starting.................................");

            targetsystem.setTargetid(EspecialCharMgt.conversion(targetsystem.getTargetid()));
            targetsystem.setTargetname(EspecialCharMgt.conversion(targetsystem.getTargetname()));
            targetsystem.setOrderCond("changetime desc");
            try
            {
                dataInfo = targetSystemDS.pageInfoQueryPop(targetsystem, start, pageSize);
            }
            catch (Exception e)
            {
                log.error(e);
            }
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;
    }

    public TableDataInfo queryCtgPltSyncSche(CSCntPlatformSync cntPlatformSync, int start, int pageSize)
    {
        log.debug("queryCmsCategory start...");
        TableDataInfo dataInfo = null;

        try
        {
            // 格式化内容带有时间的字段

            cntPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeStartDate()));
            cntPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeEndDate()));
            cntPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(cntPlatformSync.getCntofflinestarttime()));
            cntPlatformSync.setCntofflineendtime(DateUtil2.get14Time(cntPlatformSync.getCntofflineendtime()));
            cntPlatformSync.setObjecttype(6);// schedule
            // cntPlatformSync.setObjectid(objectid);
            dataInfo = cscntPlatformSyncDS.pageInfoQuerySche(cntPlatformSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsCategory error");
        }
        log.debug("queryCmsCategory end");
        return dataInfo;
    }

    public TableDataInfo queryCtgTarSyncSche(CSCntTargetSync CScntTargetSync, int start, int pageSize)
    {
        log.debug("queryCtgTarSync start...");
        TableDataInfo dataInfo = null;
        CScntTargetSync.setObjecttype(6);
        try
        {
            dataInfo = CScntTargetSyncDS.pageInfoQuerySche(CScntTargetSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgTarSync error");
        }
        log.debug("queryCtgTarSync end");
        return dataInfo;
    }

    public TableDataInfo queryCtgTarSync(CSCntTargetSync CScntTargetSync, int start, int pageSize)
    {
        log.debug("queryCtgTarSync start...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = CScntTargetSyncDS.pageInfoQuery(CScntTargetSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgTarSync error");
        }
        log.debug("queryCtgTarSync end");
        return dataInfo;
    }

    public void setLog(Logger log)
    {
        this.log = log;
    }

    public void setCmsScheduleDS(ICmsScheduleDS cmsScheduleDS)
    {
        this.cmsScheduleDS = cmsScheduleDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public void setIcntsyncTaskDS(ICntSyncTaskDS icntsyncTaskDS)
    {
        this.icntsyncTaskDS = icntsyncTaskDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public void setServiceSchedualMapDS(IServiceSchedualMapDS serviceSchedualMapDS)
    {
        this.serviceSchedualMapDS = serviceSchedualMapDS;
    }

    public String insertSyncTask(Long targetindex, String contentmngxmlurl, Long objindex, String objectid,
            int objtype, int actiontype, String cpcontentid, Long batchid, int status) throws DomainServiceException
    {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setBatchid(batchid);
        cntsyncTask.setStatus(status);// 任务状态：0-初始 1-待执行 2-执行中 3-成功结束 4-失败结束
        cntsyncTask.setPriority(5);// 任务优先级：1-高 2-中 3-低
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(contentmngxmlurl);
        cntsyncTask.setDestindex(targetindex);
        cntsyncTask.setSource(1);
        cntsyncTask.setTaskindex(taskindex);
        cntsyncTask.setStarttime(dateformat.format(new Date()));
        cntsyncTask.setRetrytimes(3);
        try
        {
            icntsyncTaskDS.insertCntSyncTask(cntsyncTask);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        toRecordObj(taskindex, objindex, targetindex, objectid, "", objtype, actiontype, cpcontentid);// 记录到发布日志表,2是栏目
        // 栏目

        return null;
    }

    // 不用插入map
    public String toRecordObj(Long taskindex, Long objindex, Long destindex, String objid, String elementid,
            int objtype, int actiontype, String cpcontentid) throws DomainServiceException
    {

        // 查找目标系统类型
        Targetsystem targetsystem = new Targetsystem();
        targetsystem.setTargetindex(destindex);
        List list = targetSystemDS.getTargetsystemByCond(targetsystem);
        if (list != null && list.size() > 0)
        {
            targetsystem = (Targetsystem) list.get(0);
        }
        int targetsystemtype = targetsystem.getTargettype();

        if (targetsystem.getPlatform() == 2 && (targetsystemtype == 3 || targetsystemtype == 2))
        {// 3.0平台,cdn
         // 查出录制计划
            CmsSchedulerecord schedulerecord = new CmsSchedulerecord();// 录制计划
            schedulerecord.setScheduleid(objid);
            List scherelist = cmsSchedulerecordDS.getCmsSchedulerecordByID(schedulerecord);
            if (scherelist != null && scherelist.size() > 0)
            {
                for (int i = 0; i < scherelist.size(); i++)
                {
                    schedulerecord = (CmsSchedulerecord) scherelist.get(i);
                    ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
                    objectSyncRecord.setTaskindex(taskindex);
                    objectSyncRecord.setObjectindex(schedulerecord.getSchedulerecordindex());
                    objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
                    objectSyncRecord.setObjecttype(9);// 对象类型编号：1-service，2-Category，3-Program,9schedulerecord
                    objectSyncRecord.setObjectid(schedulerecord.getSchedulerecordid());
                    objectSyncRecord.setDestindex(destindex);
                    if (targetsystemtype == 0)
                    {// 2.0平台
                        objectSyncRecord.setObjectcode(schedulerecord.getCpcontentid());
                    }
                    objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
                }
            }
        }
        if (targetsystemtype == 1 || targetsystemtype == 2 || targetsystemtype == 0 || targetsystemtype == 10)
        {

            ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objindex);
            objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
            objectSyncRecord.setObjecttype(objtype);// 对象类型编号：1-service，2-Category，3-Program
            if (!elementid.equals(""))
            {
                objectSyncRecord.setElementid(elementid);
                objectSyncRecord.setParentid(objid);// mapping的做法
                objectSyncRecord.setObjectid(objid);
            }
            else
            {
                objectSyncRecord.setObjectid(objid);
            }
            objectSyncRecord.setDestindex(destindex);
            if (targetsystemtype == 0 || targetsystemtype == 10)
            {// 2.0平台
                objectSyncRecord.setObjectcode(cpcontentid);
            }
            try
            {
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    // 删除网元的发布数据，删除网元和平台信息,在网元全部删除后删除平台
    public HashMap deletecntsync(long syncindex)
    {
        log.debug("deleteCategory start...");

        HashMap result = new HashMap();
        String msg = "";
        CntTargetSync sync = new CntTargetSync();
        sync.setSyncindex(syncindex);
        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(sync);
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (list != null && list.size() > 0)
        {

            for (int i = 0; i < list.size(); i++)
            {
                sync = (CntTargetSync) list.get(i);
                int status = sync.getStatus();
                // 发布中，不允许修改
                if (status == 200 || status == 300 || status == 500)
                {
                    result.put("msg", "1:在该状态下不允许删除");
                    return result;
                }
            }

            try
            {
                CntTargetSync cntsync = new CntTargetSync();
                cntsync.setRelateindex(sync.getRelateindex());// 得到相关网元

                CntPlatformSync platsync = new CntPlatformSync();
                platsync.setSyncindex(cntsync.getRelateindex());// 得到平台
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(platsync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    platsync = (CntPlatformSync) pltlist.get(0);
                }
                long targetindex = sync.getTargetindex();// 节目单关联的网元index

                cntTargetSyncDS.removeCntTargetSyncList(list);// 删除网元关联数据

                // 获得有共同平台的所有targetlist
                List alllist = cntTargetSyncDS.getCntTargetSyncByCond(cntsync);
                if (alllist != null && alllist.size() > 0)
                {
                    int[] targetSyncStatus = new int[alllist.size()];
                    for (int count = 0; count < alllist.size(); count++)
                    {
                        targetSyncStatus[count] = ((CntTargetSync) (alllist.get(count))).getStatus();
                    }
                    int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                    int originalstatus = platsync.getStatus();// 平台的原始状态
                    if (originalstatus != statusfinal)
                    {
                        platsync.setStatus(statusfinal);
                        cntPlatformSyncDS.updateCntPlatformSync(platsync);
                    }
                }
                else
                {

                    cntPlatformSyncDS.removeCntPlatformSync(platsync);// 删除平台

                }

            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                result.put("msg", "1:删除失败");
                return result;

            }
            CommonLogUtil.insertOperatorLog(sync.getObjectid(), "log.channelSchedule.mgt",
                    "log.target.schedule.bind.delete", "log.target.schedule.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            result.put("msg", "0:删除成功");
        }
        else
        {
            CommonLogUtil.insertOperatorLog(sync.getObjectid(), "log.channelSchedule.mgt",
                    "log.target.schedule.bind.delete", "log.target.schedule.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_FAIL);
            result.put("msg", "1:此条数据已被删除");
            return result;
        }
        log.debug("deleteCategory end");

        return result;
    }

}

package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.dao.ICmsTasksourceMapDAO;
import com.zte.cms.content.model.CmsTasksourceMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsTasksourceMapDAO extends DynamicObjectBaseDao implements ICmsTasksourceMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("insert cmsTasksourceMap starting...");
        super.insert("insertCmsTasksourceMap", cmsTasksourceMap);
        log.debug("insert cmsTasksourceMap end");
    }

    public void updateCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("update cmsTasksourceMap by pk starting...");
        super.update("updateCmsTasksourceMap", cmsTasksourceMap);
        log.debug("update cmsTasksourceMap by pk end");
    }

    public void updateCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("update cmsTasksourceMap by conditions starting...");
        super.update("updateCmsTasksourceMapByCond", cmsTasksourceMap);
        log.debug("update cmsTasksourceMap by conditions end");
    }

    public void deleteCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("delete cmsTasksourceMap by pk starting...");
        super.delete("deleteCmsTasksourceMap", cmsTasksourceMap);
        log.debug("delete cmsTasksourceMap by pk end");
    }

    public void deleteCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("delete cmsTasksourceMap by conditions starting...");
        super.delete("deleteCmsTasksourceMapByCond", cmsTasksourceMap);
        log.debug("update cmsTasksourceMap by conditions end");
    }

    public CmsTasksourceMap getCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("query cmsTasksourceMap starting...");
        CmsTasksourceMap resultObj = (CmsTasksourceMap) super.queryForObject("getCmsTasksourceMap", cmsTasksourceMap);
        log.debug("query cmsTasksourceMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsTasksourceMap> getCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException
    {
        log.debug("query cmsTasksourceMap by condition starting...");
        List<CmsTasksourceMap> rList = (List<CmsTasksourceMap>) super.queryForList("queryCmsTasksourceMapListByCond",
                cmsTasksourceMap);
        log.debug("query cmsTasksourceMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsTasksourceMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsTasksourceMapListCntByCond", cmsTasksourceMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsTasksourceMap> rsList = (List<CmsTasksourceMap>) super.pageQuery("queryCmsTasksourceMapListByCond",
                    cmsTasksourceMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsTasksourceMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsTasksourceMapListByCond", "queryCmsTasksourceMapListCntByCond",
                cmsTasksourceMap, start, pageSize, puEntity);
    }

}
package com.zte.cms.content.program.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.common.Generator;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.program.dao.ICmsProgramDAO;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.cpsp.dao.ICmsLpopNoauditDAO;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsProgramDS extends DynamicObjectBaseDS implements ICmsProgramDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramDAO dao = null;
    // 内容免审DAO
    private ICmsLpopNoauditDAO cmsLpopNoauditDAO = null;

    public void setDao(ICmsProgramDAO dao)
    {
        this.dao = dao;
    }

    public void setCmsLpopNoauditDAO(ICmsLpopNoauditDAO cmsLpopNoauditDAO)
    {
        this.cmsLpopNoauditDAO = cmsLpopNoauditDAO;
    }

    public void insertCmsProgram(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("insert cmsProgram starting...");
        try
        {
            Long mapinex;
            if (cmsProgram.getProgramindex() == null)
            {
                mapinex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_program_index");
                cmsProgram.setProgramindex(mapinex);
            }
            //String cpcontentid = Generator.getWGCodeByContentId(cmsProgram.getProgramid());
            cmsProgram.setCpcontentid(cmsProgram.getProgramid());
            dao.insertCmsProgram(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgram end");
    }

    public void updateCmsProgram(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("update cmsProgram by pk starting...");
        try
        {
            dao.updateCmsProgram(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgram by pk end");
    }

    public void updateCmsProgramList(List<CmsProgram> cmsProgramList) throws DomainServiceException
    {
        log.debug("update cmsProgramList by pk starting...");
        if (null == cmsProgramList || cmsProgramList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramList");
            return;
        }
        try
        {
            for (CmsProgram cmsProgram : cmsProgramList)
            {
                dao.updateCmsProgram(cmsProgram);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramList by pk end");
    }

    public void updateCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("update cmsProgram by condition starting...");
        try
        {
            dao.updateCmsProgramByCond(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgram by condition end");
    }

    public void updateCmsProgramListByCond(List<CmsProgram> cmsProgramList) throws DomainServiceException
    {
        log.debug("update cmsProgramList by condition starting...");
        if (null == cmsProgramList || cmsProgramList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramList");
            return;
        }
        try
        {
            for (CmsProgram cmsProgram : cmsProgramList)
            {
                dao.updateCmsProgramByCond(cmsProgram);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramList by condition end");
    }

    public void removeCmsProgram(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("remove cmsProgram by pk starting...");
        try
        {
            dao.deleteCmsProgram(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgram by pk end");
    }

    public void removeCmsProgramList(List<CmsProgram> cmsProgramList) throws DomainServiceException
    {
        log.debug("remove cmsProgramList by pk starting...");
        if (null == cmsProgramList || cmsProgramList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramList");
            return;
        }
        try
        {
            for (CmsProgram cmsProgram : cmsProgramList)
            {
                dao.deleteCmsProgram(cmsProgram);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramList by pk end");
    }

    public void removeCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("remove cmsProgram by condition starting...");
        try
        {
            dao.deleteCmsProgramByCond(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgram by condition end");
    }

    public void removeCmsProgramListByCond(List<CmsProgram> cmsProgramList) throws DomainServiceException
    {
        log.debug("remove cmsProgramList by condition starting...");
        if (null == cmsProgramList || cmsProgramList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramList");
            return;
        }
        try
        {
            for (CmsProgram cmsProgram : cmsProgramList)
            {
                dao.deleteCmsProgramByCond(cmsProgram);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramList by condition end");
    }

    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("get cmsProgram by pk starting...");
        CmsProgram rsObj = null;
        try
        {
            rsObj = dao.getCmsProgram(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramList by pk end");
        return rsObj;
    }

    public List<CmsProgram> getCmsProgramByCond(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("get cmsProgram by condition starting...");
        List<CmsProgram> rsList = null;
        try
        {
            rsList = dao.getCmsProgramByCond(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgram by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsProgram page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgram, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgram>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgram page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsProgram page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgram, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgram>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgram page info by condition end");
        return tableInfo;
    }

    public int getCmsProgramCount(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("query cmsProgram count by condition starting...");
        int totalCnt = 0;
        try
        {
            totalCnt = dao.getCmsProgramCount(cmsProgram);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("query cmsProgram count by condition end");
        return totalCnt;
    }

    public String getRoute(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("CmsProgramDS: getRoute starting");
        StringBuffer route = new StringBuffer();
        int r1 = 1; // 走一审
        int r2 = 1; // 走二审

        try
        {
            route.append("|r1,");
            // 判断是否免一审，内容处于回收站状态再次提审后直接到二审
            if (160 == cmsProgram.getStatus())
            {// 160：回收站
                r1 = 0;
            }
            else
            {
                CmsLpopNoaudit noOneaudit = new CmsLpopNoaudit();
                noOneaudit.setCpid(cmsProgram.getCpid());
                noOneaudit.setLptype(1); // 1：CP免审
                List<CmsLpopNoaudit> noOneauditList = cmsLpopNoauditDAO.getCmsLpopNoauditByCond(noOneaudit);
                if (null != noOneauditList && noOneauditList.size() > 0)
                {
                    r1 = 0;
                }
            }
            route.append(r1);

            // 判断内容是否免二审（运营商进行二审）
            CmsLpopNoaudit noTwoaudit = new CmsLpopNoaudit();
            noTwoaudit.setCpid(cmsProgram.getCpid());
            noTwoaudit.setLptype(3); // 3：运营商
            List<CmsLpopNoaudit> noTwoauditList = cmsLpopNoauditDAO.getCmsLpopNoauditByCond(noTwoaudit);

            if (null != noTwoauditList && noTwoauditList.size() > 0)
            {// 运营商免审
                r2 = 0;
            }
            route.append("|r2,");
            route.append(r2);

            route.append("|r3,1");
        }
        catch (Exception e)
        {
            log.error("Error occurred in getRoute method", e);
            throw new DomainServiceException(CntConstants.CNT_FAIL);
        }

        log.debug("CmsProgramDS: getRoute end");
        return route.toString();
    }

    public String getModRoute(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("CmsProgramDS: getRoute starting");
        StringBuffer route = new StringBuffer();
        int r1 = 1; // 走一审
        int r2 = 1; // 走二审

        try
        {
            route.append("|r1,");
            // 判断是否免一审
            CmsLpopNoaudit noOneaudit = new CmsLpopNoaudit();
            noOneaudit.setCpid(cmsProgram.getCpid());
            noOneaudit.setLptype(1); // 1：CP免审
            List<CmsLpopNoaudit> noOneauditList = cmsLpopNoauditDAO.getCmsLpopNoauditByCond(noOneaudit);
            if (null != noOneauditList && noOneauditList.size() > 0)
            {
                r1 = 0;
            }

            route.append(r1);

            // 判断内容是否免二审（运营商进行二审）
            CmsLpopNoaudit noTwoaudit = new CmsLpopNoaudit();
            noTwoaudit.setCpid(cmsProgram.getCpid());
            noTwoaudit.setLptype(3); // 3：运营商
            List<CmsLpopNoaudit> noTwoauditList = cmsLpopNoauditDAO.getCmsLpopNoauditByCond(noTwoaudit);

            if (null != noTwoauditList && noTwoauditList.size() > 0)
            {// 运营商免审
                r2 = 0;
            }
            route.append("|r2,");
            route.append(r2);

            route.append("|r3,1");
        }
        catch (Exception e)
        {
            log.error("Error occurred in getRoute method", e);
            throw new DomainServiceException(CntConstants.CNT_FAIL);
        }

        log.debug("CmsProgramDS: getRoute end");
        return route.toString();

    }

    public CmsProgram getCmsProgramById(CmsProgram cmsProgram) throws DomainServiceException
    {
        log.debug("get getCmsProgramById  ds starting...");
        CmsProgram rsObj = null;
        try
        {
            rsObj = dao.getCmsProgramById(cmsProgram);         
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getCmsProgramById  ds end");
        return rsObj;
    }
    
    public TableDataInfo pageInfoQueryForPage(CmsProgram cmsProgram, int start, int pageSize)
            throws DomainServiceException
            {
                log.debug("get cmsSeries page info by condition starting...");
                PageInfo pageInfo = null;
                try
                {
                    String operid = cmsProgram.getOperid();            
                    //点播内容按操作员进行查询
                    if(operid.equals("1")){
                        pageInfo = dao.pageInfoQueryForPage(cmsProgram, start, pageSize);
                    }else{
                        cmsProgram.setOperid(operid);
                        pageInfo = dao.pageInfoQueryForPageForOper(cmsProgram, start, pageSize);
                        
                    }           
                }
                catch (DAOException daoEx)
                {
                    log.error("dao exception:", daoEx);
                    // TODO 根据实际应用，可以在此处添加异常国际化处理
                    throw new DomainServiceException(daoEx);
                }
                TableDataInfo tableInfo = new TableDataInfo();
                tableInfo.setData((List<CmsProgram>) pageInfo.getResult());
                tableInfo.setTotalCount((int) pageInfo.getTotalCount());
                log.debug("get cmsProgram page info by condition end");
                return tableInfo;
                }
}

package com.zte.cms.castrolemap.crmsync.dao;

import java.util.List;

import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CrmTargetSyncDAO extends DynamicObjectBaseDao implements ICrmTargetSyncDAO
{ 
 // 日志
    private Log log = SSBBus.getLog(getClass());
    
    public PageInfo pageInfoQuery( CrmTargetSync crmTargetSync, int start, int pageSize)throws DAOException
    {
        log.debug("page query crmTargetSync by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCrmTargetSynListTotalByCond",  crmTargetSync)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryCrmTargetSynListByCond" ,  crmTargetSync , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query crmTargetSync by condition end");
        return pageInfo;
    }
}

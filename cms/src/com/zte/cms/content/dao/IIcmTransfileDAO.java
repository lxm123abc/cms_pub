package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.IcmTransfile;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmTransfileDAO
{
    /**
     * 新增IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DAOException dao异常
     */
    public void insertIcmTransfile(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据主键更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DAOException dao异常
     */
    public void updateIcmTransfile(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据条件更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile更新条件
     * @throws DAOException dao异常
     */
    public void updateIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据主键删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DAOException dao异常
     */
    public void deleteIcmTransfile(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据条件删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile删除条件
     * @throws DAOException dao异常
     */
    public void deleteIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据主键查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @return 满足条件的IcmTransfile对象
     * @throws DAOException dao异常
     */
    public IcmTransfile getIcmTransfile(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据条件查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @return 满足条件的IcmTransfile对象集
     * @throws DAOException dao异常
     */
    public List<IcmTransfile> getIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException;

    /**
     * 根据条件分页查询IcmTransfile对象，作为查询条件的参数
     * 
     * @param icmTransfile IcmTransfile对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmTransfile对象，作为查询条件的参数
     * 
     * @param icmTransfile IcmTransfile对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
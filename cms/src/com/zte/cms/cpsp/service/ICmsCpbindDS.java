package com.zte.cms.cpsp.service;

import java.util.List;

import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsCpbindDS
{
    /**
     * 新增CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 批量更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsCpbindList(List<CmsCpbind> cmsCpbindList) throws DomainServiceException;

    /**
     * 根据条件更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsCpbindListByCond(List<CmsCpbind> cmsCpbindList) throws DomainServiceException;

    /**
     * 删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 批量删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsCpbindList(List<CmsCpbind> cmsCpbindList) throws DomainServiceException;

    /**
     * 根据条件删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsCpbindListByCond(List<CmsCpbind> cmsCpbindList) throws DomainServiceException;

    /**
     * 查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @return CmsCpbind对象
     * @throws DomainServiceException ds异常
     */
    public CmsCpbind getCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 根据条件查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @return 满足条件的CmsCpbind对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsCpbind> getCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件删除绑定关系
     * 
     * @param cmsCpbind 绑定关系对象，没有的条件勿set
     * @return 无
     */
    public void removeCpbindBySetCond(CmsCpbind cmsCpbind) throws DomainServiceException;
}
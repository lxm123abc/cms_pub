package com.zte.cms.cast.dao;

import java.util.List;

import com.zte.cms.cast.dao.ICastcdnDAO;
import com.zte.cms.cast.model.Castcdn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CastcdnDAO extends DynamicObjectBaseDao implements ICastcdnDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCastcdn(Castcdn castcdn) throws DAOException
    {
        log.debug("insert castcdn starting...");
        super.insert("insertCast", castcdn);
        log.debug("insert castcdn end");
    }

    public void updateCastcdn(Castcdn castcdn) throws DAOException
    {
        log.debug("update castcdn by pk starting...");
        super.update("updateCast", castcdn);
        log.debug("update castcdn by pk end");
    }

    public void updateCastcdnByCond(Castcdn castcdn) throws DAOException
    {
        log.debug("update castcdn by conditions starting...");
        super.update("updateCastByCond", castcdn);
        log.debug("update castcdn by conditions end");
    }

    public void deleteCastcdn(Castcdn castcdn) throws DAOException
    {
        log.debug("delete castcdn by pk starting...");
        super.delete("deleteCast", castcdn);
        log.debug("delete castcdn by pk end");
    }

    public void deleteCastcdnByCond(Castcdn castcdn) throws DAOException
    {
        log.debug("delete castcdn by conditions starting...");
        super.delete("deleteCastByCond", castcdn);
        log.debug("update castcdn by conditions end");
    }

    public Castcdn getCastcdn(Castcdn castcdn) throws DAOException
    {
        log.debug("query castcdn starting...");
        Castcdn resultObj = (Castcdn) super.queryForObject("getCast", castcdn);
        log.debug("query castcdn end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Castcdn> getCastcdnByCond(Castcdn castcdn) throws DAOException
    {
        log.debug("query castcdn by condition starting...");
        List<Castcdn> rList = (List<Castcdn>) super.queryForList("queryCastListByCond", castcdn);
        log.debug("query castcdn by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DAOException
    {
        log.debug("page query castcdn by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastListCntByCond", castcdn)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Castcdn> rsList = (List<Castcdn>) super.pageQuery("queryCastListByCond", castcdn, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castcdn by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super
                .indexPageQuery("queryCastListByCond", "queryCastListCntByCond", castcdn, start, pageSize, puEntity);
    }

    public Castcdn getCastById(Castcdn castcdn) throws DAOException
    {
        log.debug("query getCastById starting...");
        Castcdn resultObj = (Castcdn) super.queryForObject("getCastById", castcdn);
        log.debug("query getCastById end");
        return resultObj;  
    }
}
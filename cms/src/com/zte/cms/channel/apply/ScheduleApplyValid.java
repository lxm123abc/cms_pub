package com.zte.cms.channel.apply;


import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleApplyValid extends CommonValid
{
    // 日志
    protected ICmsScheduleDS scheduleds = null;
    private Log log = SSBBus.getLog(getClass());  

    public ICmsScheduleDS getScheduleds()
    {
        return scheduleds;
    }

    public void setScheduleds(ICmsScheduleDS scheduleds)
    {
        this.scheduleds = scheduleds;
    }


    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

        CmsSchedule schedule = new CmsSchedule();
        schedule.setScheduleindex(index);
        try
        {
            schedule = scheduleds.getCmsSchedule(schedule);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        String optObjecttype = ChannelConstants.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(String.valueOf(index));

        String info = ResourceMgt.findDefaultText("ucdn.schedule.log.wkfw.cntdistribute.add.valid");
        if (null != info)
        {
            info = info.replace("[programname]", schedule.getProgramname());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void operate(Long index)
    {
        // 修改状态
        try
        {
            CmsSchedule schedule = new CmsSchedule();
            schedule.setScheduleindex(index);
            schedule = scheduleds.getCmsSchedule(schedule);

            schedule.setWorkflow(0); // 未启动
            schedule.setStatus(0); // 审核通过后，状态修改成待发布
            scheduleds.updateCmsSchedule(schedule);       

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }
}

package com.zte.cms.castrolemap.ls;

import java.util.List;

import com.zte.cms.cast.model.CastcdnConstant;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.model.CastrolemapConstant;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CastrolemapLS extends DynamicObjectBaseDS implements ICastrolemapLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CONTENT, getClass());

    private ICastrolemapDS castrolemapds;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ITargetsystemDS targetSystemDS;
    private IProgramCrmMapDS programCrmMapDS;
    private ISeriesCrmMapDS seriesCrmMapDS;
    Long syncindex;

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCastrolemapds(ICastrolemapDS castrolemapds)
    {
        this.castrolemapds = castrolemapds;
    }
    
    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }
    
    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    
    public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
    {
        this.programCrmMapDS = programCrmMapDS;
    }

    public void setSeriesCrmMapDS(ISeriesCrmMapDS seriesCrmMapDS)
    {
        this.seriesCrmMapDS = seriesCrmMapDS;
    }

    public String insertCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("insertCastrolemap start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        try
        {
            Castrolemap castrolemapObj = new Castrolemap();
            castrolemapObj.setCastindex(castrolemap.getCastindex());
            castrolemapObj.setRoletype(castrolemap.getRoletype());
            List<Castrolemap> listcrolemap = castrolemapds.getCastrolemapByCond(castrolemapObj);
            if (listcrolemap != null && listcrolemap.size() > 0)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.role.related.character.please.choose.other"));//"该角色已与该人物关联，请重新选择其他的人物与该角色关联");
                return rtnInfo.toString();
            }

            castrolemap.setStatus(CastrolemapConstant.CASTROLEMAP_STATUS_WAITPUBLISH);
            String castrolemapid = castrolemapds.insertCastrolemap(castrolemap);

            CommonLogUtil.insertOperatorLog(castrolemapid, CastrolemapConstant.MGTTYPE_CASTROLEMAP,
                    CastrolemapConstant.OPERTYPE_CASTROLEMAP_ADD, CastrolemapConstant.OPERTYPE_CASTROLEMAP_ADD_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            log.error("Error occurred in insertCastrolemap method");
            throw new DomainServiceException();
        }
        log.debug("insertCastrolemap end ");
        return rtnInfo.toString();

    }

    public String deleteCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {

        log.debug(" deleteCastrolemap start ");
        ReturnInfo rtnInfo;
        try
        {
            Castrolemap castrolemapObj = castrolemapds.getCastrolemap(castrolemap);
            rtnInfo = new ReturnInfo();
            if (castrolemapObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.role.not.exist"));//"角色不存在");
                return rtnInfo.toString();
            }
            
            ProgramCrmMap programCrmMap = new ProgramCrmMap();
            programCrmMap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());
            List<ProgramCrmMap> pcrmList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
            if(pcrmList != null && pcrmList.size() > 0){
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("castrole.has.binded.program"));
                return rtnInfo.toString();
            }
            SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
            seriesCrmMap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());
            List<SeriesCrmMap> scrmList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);
            if(scrmList != null && scrmList.size() > 0){
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("castrole.has.binded.series"));
                return rtnInfo.toString();
            }
            
            CntPlatformSync platform = new CntPlatformSync();
            platform.setObjecttype(12);
            platform.setObjectid(castrolemapObj.getCastrolemapid());
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(12);
            target.setObjectid(castrolemapObj.getCastrolemapid());
            List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            List<CntPlatformSync> crmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
            if(crmTargetList!=null&&crmTargetList.size()>0){
                for(CntTargetSync targetlist:crmTargetList){
                    int crmSyncStatus = targetlist.getStatus();
                    if(crmSyncStatus==200||crmSyncStatus==300){
                        rtnInfo.setFlag("1");
                        rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));
                        return rtnInfo.toString();
                    }
                }
                cntPlatformSyncDS.removeCntPlatformSynListByObjindex(crmPlatformList);
                cntTargetSyncDS.removeCntTargetSynListByObjindex(crmTargetList);
            }

            castrolemapds.removeCastrolemap(castrolemapObj);
            CommonLogUtil.insertOperatorLog(castrolemapObj.getCastrolemapid(),
                    CastrolemapConstant.MGTTYPE_CASTROLEMAP, CastrolemapConstant.OPERTYPE_CASTROLEMAP_DEL,
                    CastrolemapConstant.OPERTYPE_CASTROLEMAP_DEL_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");         
               
        }
        catch (Exception e)
        {
            log.error("Error occurred in deleteCastrolemap method");
            throw new DomainServiceException();
        }

        log.debug(" deleteCastrolemap end ");
        return rtnInfo.toString();
    }

    public Castrolemap getCastrolemap(Long castrolemapindex)
    {
        Castrolemap castrolemap = new Castrolemap();
        castrolemap.setCastrolemapindex(castrolemapindex);
        try
        {
            castrolemap = castrolemapds.getCastrolemap(castrolemap);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in getCastrolemap method");
            return null;
        }
        return castrolemap;
    }

    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            castrolemap.setCastrole(EspecialCharMgt.conversion(castrolemap.getCastrole()));
            castrolemap.setCastid(EspecialCharMgt.conversion(castrolemap.getCastid()));
            castrolemap.setCastrolemapid((EspecialCharMgt.conversion(castrolemap.getCastrolemapid())));
            tableInfo = castrolemapds.pageInfoQuery(castrolemap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;

    }

    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            castrolemap.setCastrole(EspecialCharMgt.conversion(castrolemap.getCastrole()));
            castrolemap.setCastid(EspecialCharMgt.conversion(castrolemap.getCastid()));
            castrolemap.setCastrolemapid((EspecialCharMgt.conversion(castrolemap.getCastrolemapid())));
            tableInfo = castrolemapds.pageInfoQuery(castrolemap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;
    }

    public String batchDelCastrolemapList(List<String> castrolemapindexs) throws Exception
    {
        log.debug("batchDelCastrolemapList start ");

        Integer ifail = 0;
        Integer iSuccessed = 0;
        Castrolemap castrolemap = new Castrolemap();
        Castrolemap castrolemapObj = null;
        com.zte.cms.content.batchUpload.ls.ReturnInfo rtnInfo = new com.zte.cms.content.batchUpload.ls.ReturnInfo();
        com.zte.cms.content.batchUpload.ls.OperateInfo operateInfo = null;
        String opindex = "";
        String castrolemapindex = "";
        String flag = "";
        boolean isBreak = false;
        for (int i = 0; i < castrolemapindexs.size(); i++)
        {
            opindex = (i + 1) + "";
            try
            {
                castrolemapindex = castrolemapindexs.get(i);
                castrolemap.setCastrolemapindex(Long.valueOf(castrolemapindex));
                castrolemapObj = castrolemapds.getCastrolemap(castrolemap);
                // 角色不存在
                if (castrolemapObj == null || castrolemapObj.getStatus() == null)
                {
                    ifail++;
                    operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castrolemapObj.getCastrolemapid(), ResourceMgt.findDefaultText("do.fail"),
                    		ResourceMgt.findDefaultText("cast.role.not.exist"));//"操作失败""角色不存在");
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                programCrmMap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());
                List<ProgramCrmMap> pcrmList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                if(pcrmList != null && pcrmList.size() > 0){
                    ifail++;
                    operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castrolemapObj.getCastrolemapid(), ResourceMgt.findDefaultText("do.fail"),
                            ResourceMgt.findDefaultText("castrole.has.binded.program"));
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
                seriesCrmMap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());
                List<SeriesCrmMap> scrmList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);
                if(scrmList != null && scrmList.size() > 0){
                    ifail++;
                    operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castrolemapObj.getCastrolemapid(), ResourceMgt.findDefaultText("do.fail"),
                            ResourceMgt.findDefaultText("castrole.has.binded.series"));
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                CntPlatformSync platform = new CntPlatformSync();
                platform.setObjecttype(12);
                platform.setObjectid(castrolemapObj.getCastrolemapid());
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(12);
                target.setObjectid(castrolemapObj.getCastrolemapid());
                List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                List<CntPlatformSync> crmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                if(crmTargetList!=null&&crmTargetList.size()>0){
                    for(CntTargetSync targetlist:crmTargetList){
                        int crmSyncStatus = targetlist.getStatus();
                        if(crmSyncStatus==200||crmSyncStatus==300){
                            ifail++;
                            operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castrolemapObj.getCastrolemapid(), ResourceMgt.findDefaultText("do.fail"),
                                    ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));
                            rtnInfo.appendOperateInfo(operateInfo);
                            isBreak = true;
                            break;
                        }
                    }
                    if(isBreak == true){
                        continue;
                    }
                    cntPlatformSyncDS.removeCntPlatformSynListByObjindex(crmPlatformList);
                    cntTargetSyncDS.removeCntTargetSynListByObjindex(crmTargetList);
                }
                
                castrolemapds.removeCastrolemap(castrolemapObj);

                iSuccessed++;
                operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castrolemapObj.getCastrolemapid(), ResourceMgt.findDefaultText("do.success"),
                        ResourceMgt.findDefaultText("do.success"));//"操作成功");
                rtnInfo.appendOperateInfo(operateInfo);

                CommonLogUtil.insertOperatorLog(castrolemapObj.getCastrolemapid(),
                        CastrolemapConstant.MGTTYPE_CASTROLEMAP, CastrolemapConstant.OPERTYPE_CASTROLEMAP_DEL,
                        CastrolemapConstant.OPERTYPE_CASTROLEMAP_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                
            }
            catch (Exception e)
            {
                ifail++;
                operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"), ResourceMgt.findDefaultText("cast.role.delete.fail"));//"操作失败""角色删除失败"
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (castrolemapindexs.size() == 0 || ifail == castrolemapindexs.size())
        {
            flag = CastcdnConstant.FALSE; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = CastcdnConstant.PARTIALSUCCESS; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + castrolemapindexs.size());
        }

        log.debug("batchDelCastrolemapList end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }
    
    public String bindBatchCrmTargetsync(List<Targetsystem> platformList,List<Castrolemap> crmindexs)
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCastindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < crmindexs.size(); i++)  //遍历要操作的人物主键，依次操作要关联的内容
            {
                totalCastindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
                
                Castrolemap crmObj = new Castrolemap();
                crmObj.setCastrolemapindex(crmindexs.get(i).getCastrolemapindex());
                crmObj = castrolemapds.getCastrolemap(crmObj);
                if (crmObj == null ){
                   proStatusStr = CastcdnConstant.CAST_ISNOTEXIST;
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), crmindexs.get(i).getCastrolemapindex().toString(), 
                           result.substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }else{
                    operdesc = bindCrmTargetsync(platformList,crmObj);  //单个内容关联运营平台
                    if (operdesc.substring(0, 1).equals("0"))  //内容关联运营平台成功
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), crmObj.getCastrolemapid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), crmObj.getCastrolemapid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        if ((successCount) == totalCastindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCastindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
    }
    
    public String bindCrmTargetsync(List<Targetsystem> platformList,Castrolemap castrolemap)
    {
        log.debug("bindCrmTargetsync starting...");
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        try
        {
            for (int i = 0; i < platformList.size(); i++)  //遍历要关联的平台
            {
                Targetsystem targtemp = new Targetsystem();
                targtemp.setTargetindex(platformList.get(i).getTargetindex());
                targtemp = targetSystemDS.getTargetsystem(targtemp);                 
                if (targtemp != null && targtemp.getStatus() == 0)  //0:正常
                {
                    CntTargetSync target = new CntTargetSync();
                    target.setObjecttype(12);
                    target.setObjectid(castrolemap.getCastrolemapid());
                    target.setTargetindex(platformList.get(i).getTargetindex());
                    //运营平台存在
                    List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    if(targetSyncList !=null && targetSyncList.size()>0)
                    {
                        resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                             + platformList.get(i).getTargetid()+ ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)+"  ");
                                        
                        continue;
                    } 
                    target.setObjecttype(11);
                    target.setObjectid(castrolemap.getCastid());
                    target.setTargetindex(platformList.get(i).getTargetindex());
                    List<CntTargetSync> casttargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    //人物未关联改平台
                    if(casttargetSyncList.size()<=0)
                    {
                        resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                             + platformList.get(i).getTargetid()+ "人物"+castrolemap.getCastid()+"未关联"+"  ");
                                        
                        continue;
                    } 
                    
                    CntPlatformSync platform = new CntPlatformSync();
                    platform.setObjecttype(12);
                    platform.setObjectid(castrolemap.getCastrolemapid());
                    platform.setPlatform(platformList.get(i).getPlatform());
                    List<CntPlatformSync> platformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                    if(platformSyncList !=null && platformSyncList.size()>0)  
                    {
                        syncindex = platformSyncList.get(0).getSyncindex();
                        platform = platformSyncList.get(0);
                        platform.setStatus(0);
                        cntPlatformSyncDS.updateCntPlatformSync(platform);   
                    }else{
                        insertPlatformSyncTask(12,castrolemap.getCastrolemapindex(),castrolemap.getCastrolemapid(),"","",
                                platformList.get(i).getPlatform(),0);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(castrolemap.getCastrolemapid().toString() + ", " + platformList.get(i).getTargetid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP,
                                ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_ADD), ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_ADD_INFO),
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    
                    insertTargetSyncTask(syncindex,12,castrolemap.getCastrolemapindex(),castrolemap.getCastrolemapid(),"","",
                            targtemp.getTargetindex(),0,0);
                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(castrolemap.getCastrolemapid().toString() + ", " + platformList.get(i).getTargetid(), CastrolemapConstant.MGTTYPE_CASTROLEMAP,
                            ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_TARGET_CRM_BIND_ADD), ResourceManager.getResourceText(CastrolemapConstant.OPERTYPE_PLATFORM_CRM_BIND_ADD_INFO),
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    
                    sucess = true;
                    resultStr.append(ResourceMgt.findDefaultText(CntConstants
                            .CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                            + ResourceMgt.findDefaultText(CntConstants
                                    .CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)+"  ");
                        
                    
                }else{
                    resultStr.append(ResourceMgt.findDefaultText(CntConstants
                            .CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                            + ResourceMgt.findDefaultText(CntConstants
                                    .CMS_TARGETSYSTEM_MSG_HAS_DELETED)+"  ");
                }
            }
        }catch(Exception e){
            log.error("CastcdnLS exception:" + e.getMessage());
        }
        log.debug("bindCrmTargetsync end...");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }
    
    /**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

}

package com.zte.cms.ftptask.ls;

import com.zte.cms.ftptask.model.CmsFtptaskAndHis;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsFtptaskLS
{

    public String restartFtptask(Long taskindex) throws DomainServiceException;

    public TableDataInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskAndHis, int start, int pageSize)
            throws DomainServiceException;
}

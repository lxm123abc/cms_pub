package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceWkfwhisDAO
{
    /**
     * 新增ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DAOException dao异常
     */
    public void insertServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException;

    /**
     * 根据主键更新ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DAOException dao异常
     */
    public void updateServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException;

    /**
     * 根据主键删除ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DAOException dao异常
     */
    public void deleteServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException;

    /**
     * 根据主键查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @return 满足条件的ServiceWkfwhis对象
     * @throws DAOException dao异常
     */
    public ServiceWkfwhis getServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException;

    /**
     * 根据条件查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @return 满足条件的ServiceWkfwhis对象集
     * @throws DAOException dao异常
     */
    public List<ServiceWkfwhis> getServiceWkfwhisByCond(ServiceWkfwhis serviceWkfwhis) throws DAOException;

    /**
     * 根据条件分页查询ServiceWkfwhis对象，作为查询条件的参数
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceWkfwhis对象，作为查询条件的参数
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
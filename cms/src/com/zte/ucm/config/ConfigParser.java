package com.zte.ucm.config;

import com.zte.ucm.Constants;
import com.zte.ucm.Resultlog;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;
import com.zte.ucm.util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class ConfigParser
{
  Logger log = LoggerFactory.getLogger(ConfigParser.class);

  public List<Config> getConfig(ConfigItem paramConfigItem)
    throws Exception
  {
    String str1 = paramConfigItem.getItem_name();
    String str2 = paramConfigItem.getItem_current_value();
    String str3 = paramConfigItem.getItem_code();

    ArrayList localArrayList = new ArrayList();
    String str4 = "";
    Resultlog localResultlog = new Resultlog();
    File localFile = new File(Constants.CONFIGFILEPATH + Constants.CONFIGFILENAME);
    try
    {
      SAXReader localSAXReader = new SAXReader();
      Document localDocument = localSAXReader.read(localFile);

      List localList = localDocument.selectNodes("/configs/config[tag='" + str1.trim() + "']");

      if ((localList != null) && (localList.size() > 0))
      {
        for (int i = 0; i < localList.size(); i++)
        {
          Config localConfig = new Config();
          localConfig.setTag(str1);
          localConfig.setCode(str3);
          Element localElement1 = (Element)localList.get(i);

          Iterator localIterator = localElement1.elementIterator("file");
          Element localElement2;
          while (localIterator.hasNext())
          {
            localElement2 = (Element)localIterator.next();
            localConfig.setFile(localElement2.getTextTrim());
          }

          localIterator = localElement1.elementIterator("xpath");
          while (localIterator.hasNext())
          {
            localElement2 = (Element)localIterator.next();
            localConfig.setXpath(localElement2.getTextTrim());
          }

          localIterator = localElement1.elementIterator("key");
          while (localIterator.hasNext()) {
            localElement2 = (Element)localIterator.next();
            localConfig.setKey(localElement2.getTextTrim());
          }

          localIterator = localElement1.elementIterator("type");
          while (localIterator.hasNext())
          {
            localElement2 = (Element)localIterator.next();
            localConfig.setType(localElement2.getTextTrim());
          }

          localIterator = localElement1.elementIterator("attrname");
          while (localIterator.hasNext())
          {
            localElement2 = (Element)localIterator.next();
            localConfig.setAttrname(localElement2.getTextTrim());
          }

          localIterator = localElement1.elementIterator("value");
          while (localIterator.hasNext())
          {
            localElement2 = (Element)localIterator.next();
            if (FileUtil.isXml(localConfig.getFile()))
            {
              localConfig.setValue(localElement2.getTextTrim().replaceAll("\\$\\{value\\}", Matcher.quoteReplacement(str2)));
            }
            else
            {
              localConfig.setValue(localElement2.getTextTrim().replaceAll("\\$\\{value\\}", Matcher.quoteReplacement(localConfig.getKey() + "=" + str2)));
            }

          }

          this.log.info("Config Info:[Tag:" + localConfig.getTag() + " File:" + localConfig.getFile() + " Xpath:" + localConfig.getXpath() + " Type:" + localConfig.getType() + " Attrname:" + localConfig.getAttrname() + "  Value:" + localConfig.getValue() + "]");

          localArrayList.add(localConfig);
        }
      }
      else
      {
        this.log.error("Operation ERROR:Cannot find the config, please check the input param[" + str1 + "]");
        str4 = str4 + "1|Operation ERROR:" + str1 + "|" + "Cannot find the config, please check the input param[" + str1 + "]" + "\n";

        localResultlog.appendLog(str4);
      }
    }
    catch (DocumentException localDocumentException)
    {
      this.log.error("Operation ERROR:ConfigParser getConfig error");
      str4 = str4 + "1|Operation ERROR:ConfigParser getConfig error\n";
      localResultlog.appendLog(str4);

      throw localDocumentException;
    }
    return localArrayList;
  }
}


package com.zte.cms.content.CStargetSyn.service;

import java.util.List;

import com.zte.cms.content.CStargetSyn.dao.ICSCntTargetSyncDAO;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CSCntTargetSyncDS extends DynamicObjectBaseDS implements ICSCntTargetSyncDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
  	private ICSCntTargetSyncDAO dao = null;
	
	public void setDao(ICSCntTargetSyncDAO dao)
	{
	    this.dao = dao;
	}


	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntTargetSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQueryCha(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQueryCha(cntTargetSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQueryCach(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQueryCach(cntTargetSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQuerySche(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuerySche(cntTargetSync, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get cntTargetSync page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cntTargetSync, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CSCntTargetSync>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cntTargetSync page info by condition end" );
		return tableInfo;
	}
	
	

	

}

function dealResult(errorcode, des, identity) 
{
	    if (typeof(des) != "undefined") 
	    {
		  errorcode = errorcode + ":" + $res_entry("msg.system.error");
	    }
		openMsg(errorcode, null, false);
	    
}

function validSearchTime(beginID,endID,input_msg)
{
	    var beginTime = document.getElementById(beginID);
	    var endTime   = document.getElementById(endID);
	    return beginTimeLEndTime(beginTime,endTime,input_msg);

}

function startSmallestEndBiggest(beginTime,endTime,input_msg,msg1,msg2){
	  document.getElementById(input_msg).innerText = "";   
	  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
	    if(beginTime.value > endTime.value){
	        document.getElementById(input_msg).innerText = msg1+$res_entry("msg.info.content.203")+msg2+$res_entry("msg.info.content.204") ; 
	        document.getElementById(beginTime.id).focus();
	        return false;
	    }else{
	        return true;               
	    }
	  }
	}

function validBeginAndEndTime(beginID,endID,input_msg,msg1,msg2)
{
		var beginTime = document.getElementById(beginID);
	    var endTime   = document.getElementById(endID);
	    return startSmallestEndBiggest(beginTime,endTime,input_msg,msg1,msg2);
	
}


function beginTimeLEndTime(beginTime,endTime,input_msg){
  document.getElementById(input_msg).innerText = "";   
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.12"); 
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        return true;               
    }
  }
}



//清除提示标签信息
function clearInputMsg(input_msg)
{
	  document.getElementById(input_msg).innerText = ""; 
}

//清除提示文本框信息
function clearInputText(input_id)
{
	  document.getElementById(input_id).value = ""; 
}

//下拉框列表默认请选择
function clearSelect(select_id)
{
	  document.getElementById(select_id).selectedIndex=0;
}


function getTime(str)
{
	if (str.length > 8)
	{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	}else{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	}
}



//页面校验
function isAboveZero(input,input_msg)
{
   	var inputValue=$.trim(input.value);
   	if(inputValue=="")
   	{
   		$("#"+input_msg).text("");
   		return true;
   	}
   	else
   	{
		var integerRex=/^[1-9]+[0-9]*$/;
		var rtnValue=integerRex.test(inputValue);
		if(rtnValue==false)
		{
				$("#"+input_msg).text($res_entry("msg.info.content.201"));
				return false;
		}
		else
		{
			var intValue=parseInt(inputValue);
			if(intValue<1)
			{
				$("#"+input_msg).text($res_entry("msg.info.content.201"));
				return false;
			}
			else
			{
				$("#"+input_msg).text("");
				return true;
			}
		}
   	}
}
    
 function checkpRicetaxin(input,input_msg)
 {
 	
 	var isFLoasee=/^((0)|([1-9]+[0-9]*))((\.[0-9]{1,2})?)$/;
 	var pricetaxinValue=$.trim(input.value);
 
 	if(pricetaxinValue!="")
 	{
 		var flag=isFLoasee.test(pricetaxinValue);
 		if(!flag)
 		{
	        $("#"+input_msg).text($res_entry("msg.info.content.202"));
			return false;
 		}
 		else
 		{
 			if(pricetaxinValue.indexOf(".")==-1){
	 			if(pricetaxinValue.length>10){
	 				$("#"+input_msg).text($res_entry("msg.info.content.202"));
					return false;
	 			} 				
 			}else{
 			   if(pricetaxinValue.indexOf(".")==0||pricetaxinValue.indexOf(".")==pricetaxinValue.length){
 			   		$("#"+input_msg).text($res_entry("msg.info.content.202"));
					return false;
 			   }
 			   if(pricetaxinValue.substr(0,pricetaxinValue.indexOf(".")).length>10){
 			   		$("#"+input_msg).text($res_entry("msg.info.content.202"));
					return false;
 			   }
 			}
			$("#"+input_msg).text("");
			return true;
 		}
 	}
 	return true;
 }
    
    
//日期校验,所选日期应早于当前系统日期时间
function validatedate(datetarget,date_msg){
         var date=formatDBDate(document.getElementById(datetarget).value,14);
         var nowdate=getTimefromUTC("yyyy-mm-dd hh:mm:ss", new Date());
         var date1=formatDBDate(nowdate,14);
         if((date-date1)<0){
         	$("#"+date_msg).html($res_entry("msg.info.cast.23"));
         	return false;
         }else{
         	$("#"+date_msg).html("");
         	return true;
         }
}

  function isPlusInteger(input,input_msg)
  {
  	var inputValue=$.trim(input.value);
  	if(inputValue=="")
  	{
  		$("#"+input_msg).text("");
  		return true;
  	}
  	else
  	{
	var integerRex=/^((0)|([1-9]+[0-9]*))$/;
	var rtnValue=integerRex.test(inputValue);
	if(rtnValue==false)
	{
			$("#"+input_msg).text($res_entry("msg.info.content.201"));
			return false;
	}
	else
	{
		$("#"+input_msg).text("");
		return true;
	}
  	}
  }
    
   //验证输入框中字数是否超过最大长度
function isMaxLength(input,input_msg,len)
{

 		if(utf8_strlen2(trim(input.value)) > len){
     		document.getElementById(input_msg).innerText = $res_entry("msg.check.string.prefix")+len+$res_entry("msg.check.string.suffix");//"超过最大长度限制:";
    		// document.getElementById(input.id).focus();
     		return false;
 		}else{
      		document.getElementById(input_msg).innerText = "";   
      		return true;   
		}
}
    
   //判断不可为空
function isNull(input,input_msg)
{
	if(trim(input.value) == '' || input.value == null){
      	 	document.getElementById(input_msg).innerText =$res_entry("msg.info.basicdata.033"); //"此项不可为空";
      	 	document.getElementById(input.id).focus();
       return false;
   	}
   	else
   	{
        document.getElementById(input_msg).innerText = "";
       	return true;
  	}
}
    
var specialStr="!@#$%^&*;|?()[]{}<>\'\"";
function isSpecialStr(checkstr,msgBox_id,specialStrParam)
{
var valSpecialStr=specialStr;
if(specialStrParam!="undefined"&&specialStrParam!= null)
{
	valSpecialStr=specialStrParam;
}
var allValid = true;
for (i = 0;i<valSpecialStr.length;i++)
{
	ch = valSpecialStr.charAt(i);
 	if(checkstr.indexOf(ch) > -1)
 	{
		allValid = false;
		break;
 	}
}
if(!allValid)
{
	document.getElementById(msgBox_id).innerText=$res_entry("msg.info.channel.023")+valSpecialStr;
}
return allValid;
}



var specialStr1=",!@#$%^&*;|?()[]{}<>\'\"";
function isSpecialStr1(checkstr,msgBox_id,specialStrParam)
{
var valSpecialStr=specialStr1;
if(specialStrParam!="undefined"&&specialStrParam!= null)
{
	valSpecialStr=specialStrParam;
}
var allValid = true;
for (i = 0;i<valSpecialStr.length;i++)
{
	ch = valSpecialStr.charAt(i);
 	if(checkstr.indexOf(ch) > -1)
 	{
		allValid = false;
		break;
 	}
}
if(!allValid)
{
	document.getElementById(msgBox_id).innerText=$res_entry("msg.info.channel.023")+valSpecialStr;
}
return allValid;
}



 //开始时间必须小于结束时间      
function betweenTimeValidate(startTimeTarget,endTimeTarget,date_msg)
{
 	startTimeTarget=document.getElementById(startTimeTarget).value;
	endTimeTarget=document.getElementById(endTimeTarget).value;
	 if(startTimeTarget!="" && endTimeTarget!="")
	 {
	 	if(startTimeTarget>endTimeTarget){
	  	$("#"+date_msg).html($res_entry("msg.info.channel.007"));
	  	return false;
		}else{
		 	return true;
		}
	 
	 }
	 return true;
 }

//操作结果转换
//0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
function getOperresult(keyvalue){
	if(keyvalue['operresult'] == 0){
		keyvalue['operresult'] ="待发布";		
	}
	if(keyvalue['operresult'] == 10){
		keyvalue['operresult'] ="新增同步中";		
	}
	if(keyvalue['operresult'] == 20){
		keyvalue['operresult'] ="新增同步成功";		
	}
	if(keyvalue['operresult'] == 30){
		keyvalue['operresult'] ="新增同步失败";		
	}
	if(keyvalue['operresult'] == 40){
		keyvalue['operresult'] ="修改同步中";		
	}
	if(keyvalue['operresult'] == 50){
		keyvalue['operresult'] ="修改同步成功";		
	}
	if(keyvalue['operresult'] == 60){
		keyvalue['operresult'] ="修改同步失败";		
	}
	if(keyvalue['operresult'] == 70){
		keyvalue['operresult'] ="取消同步中";		
	}
	if(keyvalue['operresult'] == 80){
		keyvalue['operresult'] ="取消同步成功";		
	}
	if(keyvalue['operresult'] == 90){
		keyvalue['operresult'] ="取消同步失败";		
	}	
	  return keyvalue;
}

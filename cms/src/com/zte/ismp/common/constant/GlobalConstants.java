package com.zte.ismp.common.constant;

/**
 * 
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class GlobalConstants
{
    /** 应用业务键 */
    public static final String SYSTEM_SERVICE_KEY = "CMS";
    /** vind 用户 * */
    public static final String SYS_USER_VIND = "vind"; // vind 用户

    /** DB用户前缀 */
    public static final String DB_USER_AD = "zxdbm_cms.";
    public static final String DB_USER_UMAP = "zxdbm_umap.";
    public static final String DB_USER_ZXINSYS = "zxinsys.";

    /** 服务器类型 =4 下载服务器 */
    public static final Integer SERVER_TYPE_DOWNLOAD = 4;

    /** 结果返回，页面提示时使用 0 ：操作成功 ， 1：操作失败 2:部分操作成功 */
    public static final String SUCCESS = "0";
    public static final String FAIL = "1";
    public static final String PARTIAL_SUCCESS = "2";
    public static final String DOSUCCESS = "操作成功";
    public static final String DOFAIL = "操作失败";

}

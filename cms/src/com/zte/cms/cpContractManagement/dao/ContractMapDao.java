package com.zte.cms.cpContractManagement.dao;

import com.zte.cms.cpContractManagement.model.ContractEmailSendLog;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

import java.util.List;

public class ContractMapDao extends DynamicObjectBaseDao implements IContractMapDao {
    private Log log = SSBBus.getLog(getClass());


    /**
     *分页查询数据dao层
     * @param contractMap
     * @param start
     * @param pageSize
     * @return
     * @throws DAOException
     */
    @Override
    public PageInfo pageInfoQuery(ContractMap contractMap, int start, int pageSize) throws DAOException {
        log.info("ContractMapDao  pageInfoQuery start ");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryContractCount", contractMap)).intValue();
        if (totalCnt > 0) {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ContractMap> rsList = (List<ContractMap>) super.pageQuery("queryContractByCpcode",
                    contractMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        } else {
            pageInfo = new PageInfo();
        }
        log.info("ContractMapDao  pageInfoQuery end ");

        return pageInfo;
    }

    @Override
    public void insertContractMap(ContractMap contractMap) {
        logger.info("delete insertContractMap.deleteByPrimaryKey starting...");
        super.insert("insertContractMap",contractMap);
        logger.info("delete insertContractMap.deleteByPrimaryKey  end");
    }

    @Override
    public void updateByContractId(ContractMap contractMap) {
        logger.info("delete updateByContractId.deleteByPrimaryKey starting...");
        super.update("updateContractByContractId",contractMap);
        logger.info("delete updateByContractId.deleteByPrimaryKey  end");
    }

    @Override
    public Integer queryContractByContractId(ContractMap contractMap) {
        logger.info("delete queryContractByContractId.deleteByPrimaryKey starting...");
        Integer count = (Integer) super.queryForObject("queryContractByContractId",contractMap);
        logger.info("delete queryContractByContractId.deleteByPrimaryKey  end");
        return count;
    }

    @Override
    public void deleteConstractByIds(String[] ids) {
        logger.info("delete deleteConstractByIds.deleteByPrimaryKey starting...");
        super.delete("deleteConstractByIds",ids);
        super.delete("deleteAttachByContractIds",ids);
        logger.info("delete deleteConstractByIds.deleteByPrimaryKey starting...");
    }

    @Override
    public ContractMap selectContractById(Integer id) throws DAOException {
        log.info("ContractMapDao  selectContractById start ");
        ContractMap contractMap = (ContractMap) super.queryForObject("selectContractById",id);
        log.info("ContractMapDao  selectContractById end ");
        return  contractMap;
    }

    @Override
    public ContractMap updateContractById(ContractMap contractMap) throws DAOException {
        ContractMap result = null;
        log.info("ContractMapDao  selectContractById start ");
        super.update("updateContractById",contractMap);
        result = (ContractMap) super.queryForObject("selectContractById",contractMap.getId());
        log.info("ContractMapDao  selectContractById end ");
        return result;
    }

    @Override
    public List<ContractMap> getAttachListById(ContractMap contractMap) throws DAOException {
        log.info("ContractMapDao  getAttachListById start ");
        List<ContractMap> list = null;
        list = (List<ContractMap>) super.queryForList("getAttachListById",contractMap);
        log.info("ContractMapDao  getAttachListById end ");
        return list;
    }

    @Override
    public ContractMap getAttachInfoById(String attachid) throws DAOException {
        log.info("ContractMapDao  getAttachInfoById start ");
        ContractMap result = (ContractMap) super.queryForObject("getAttachInfoById",Integer.parseInt(attachid));
        log.info("ContractMapDao  getAttachInfoById end ");
        return result;
    }

    @Override
    public ContractMap getContractMapByContractMap(ContractMap contractMap) throws DAOException {
        log.info("ContractMapDao  getContractMapByContractMapId start ");
        ContractMap result = (ContractMap) super.queryForObject("getContractMapByContractMap",contractMap);
        log.info("ContractMapDao  getContractMapByContractMapId end ");
        return result;
    }

    @Override
    public List<ContractMap> queryWillOver(int day) throws DAOException {
        List<ContractMap> list = (List<ContractMap>) super.queryForList("getContractByRemainingDay",day);
        return list;
    }

    @Override
    public Integer queryContractSendEmailLogByOidAndGrade(ContractEmailSendLog contractEmailSendLog) throws DAOException {
        Integer count = (Integer) super.queryForObject("queryContractSendEmailLogByOidAndGrade",contractEmailSendLog);
        return count;
    }

    public void insertContractSendEmailLog(ContractEmailSendLog record){
        super.insert("insertContractSendEmailLog",record);
    }


}

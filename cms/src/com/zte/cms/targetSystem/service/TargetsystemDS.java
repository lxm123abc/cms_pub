package com.zte.cms.targetSystem.service;

import java.util.List;
import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.dao.ITargetsystemDAO;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class TargetsystemDS extends DynamicObjectBaseDS implements
		ITargetsystemDS {
	// 日志
	private Log log = SSBBus.getLog(getClass());

	private ITargetsystemDAO dao = null;

	public void setDao(ITargetsystemDAO dao) {
		this.dao = dao;
	}

	public void insertTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("insert targetsystem starting...");
		try {
			Long targetindex = (Long) this.getPrimaryKeyGenerator()
					.getPrimarykey("cms_targetsystem");
			targetsystem.setTargetindex(targetindex);
			dao.insertTargetsystem(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("insert targetsystem end");
	}

	public void updateTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("update targetsystem by pk starting...");
		try {
			dao.updateTargetsystem(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update targetsystem by pk end");
	}

	public void updateTargetsystemList(List<Targetsystem> targetsystemList)
			throws DomainServiceException {
		log.debug("update targetsystemList by pk starting...");
		if (null == targetsystemList || targetsystemList.size() == 0) {
			log.debug("there is no datas in targetsystemList");
			return;
		}
		try {
			for (Targetsystem targetsystem : targetsystemList) {
				dao.updateTargetsystem(targetsystem);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update targetsystemList by pk end");
	}

	public void updateTargetsystemByCond(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("update targetsystem by condition starting...");
		try {
			dao.updateTargetsystemByCond(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update targetsystem by condition end");
	}

	public void updateTargetsystemListByCond(List<Targetsystem> targetsystemList)
			throws DomainServiceException {
		log.debug("update targetsystemList by condition starting...");
		if (null == targetsystemList || targetsystemList.size() == 0) {
			log.debug("there is no datas in targetsystemList");
			return;
		}
		try {
			for (Targetsystem targetsystem : targetsystemList) {
				dao.updateTargetsystemByCond(targetsystem);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("update targetsystemList by condition end");
	}

	public void removeTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("remove targetsystem by pk starting...");
		try {
			dao.deleteTargetsystem(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove targetsystem by pk end");
	}

	public void removeTargetsystemList(List<Targetsystem> targetsystemList)
			throws DomainServiceException {
		log.debug("remove targetsystemList by pk starting...");
		if (null == targetsystemList || targetsystemList.size() == 0) {
			log.debug("there is no datas in targetsystemList");
			return;
		}
		try {
			for (Targetsystem targetsystem : targetsystemList) {
				dao.deleteTargetsystem(targetsystem);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove targetsystemList by pk end");
	}

	public void removeTargetsystemByCond(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("remove targetsystem by condition starting...");
		try {
			dao.deleteTargetsystemByCond(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove targetsystem by condition end");
	}

	public void removeTargetsystemListByCond(List<Targetsystem> targetsystemList)
			throws DomainServiceException {
		log.debug("remove targetsystemList by condition starting...");
		if (null == targetsystemList || targetsystemList.size() == 0) {
			log.debug("there is no datas in targetsystemList");
			return;
		}
		try {
			for (Targetsystem targetsystem : targetsystemList) {
				dao.deleteTargetsystemByCond(targetsystem);
			}
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("remove targetsystemList by condition end");
	}

	public Targetsystem getTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("get targetsystem by pk starting...");
		Targetsystem rsObj = null;
		try {
			rsObj = dao.getTargetsystem(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("get targetsystemList by pk end");
		return rsObj;
	}

	public List<Targetsystem> getTargetsystemByCond(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("get targetsystem by condition starting...");
		List<Targetsystem> rsList = null;
		try {
			rsList = dao.getTargetsystemByCond(targetsystem);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		log.debug("get targetsystem by condition end");
		return rsList;
	}

	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start,
			int pageSize) throws DomainServiceException {
		log.debug("get targetsystem page info by condition starting...");
		PageInfo pageInfo = null;
		try {
			pageInfo = dao.pageInfoQuery(targetsystem, start, pageSize);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<Targetsystem>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug("get targetsystem page info by condition end");
		return tableInfo;
	}

	public TableDataInfo pageInfoQueryPop(Targetsystem targetsystem, int start,
			int pageSize) throws DomainServiceException {
		log.debug("get targetsystem page info by condition starting...");
		PageInfo pageInfo = null;
		try {
			pageInfo = dao.pageInfoQueryPop(targetsystem, start, pageSize);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<Targetsystem>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug("get targetsystem page info by condition end");
		return tableInfo;
	}

	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start,
			int pageSize, PageUtilEntity puEntity)
			throws DomainServiceException {
		log.debug("get targetsystem page info by condition starting...");
		PageInfo pageInfo = null;
		try {
			pageInfo = dao.pageInfoQuery(targetsystem, start, pageSize,
					puEntity);
		} catch (DAOException daoEx) {
			log.error("dao exception:", daoEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(daoEx);
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<Targetsystem>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug("get targetsystem page info by condition end");
		return tableInfo;
	}

	/**
	 * 检查目标系统的id是否重复
	 * @param targetsystemID
	 * @return boolean
	 * @throws DomainServiceException
	 */
	public boolean checkTargetsystemID(String targetsystemID)
			throws DomainServiceException {
		log.debug("checkTargetsystemID starting...");
		boolean status = true;

		try {
			int count = dao.checkTargetsystemID(targetsystemID);
			if (count > 0) { //表示数据库中的目标系统ID有重复的
				status = false;
			}
		} catch (DAOException e) {
			log.error("dao exception:", e);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(e);
		}

		log.debug("checkTargetsystemID end...");
		return status;
	}
	
	/**
	 * 查询服务关联目标系统
	 */
    public TableDataInfo queryServiceBindTargetListByCond(Service service, int start, int pageSize) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug("get targetsystem list for service starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryServiceBindTargetListByCond(service, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Targetsystem>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get targetsystem list for service end");
        return tableInfo;
    }    
}

<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ include file="JavaFun.jsp" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>

<%
    String serviceKey = (String)session.getAttribute("SERVICEKEY");
    purview.setServiceKey(serviceKey); 
    String operID = (String)session.getAttribute("OPERID");
    String operName = (String)session.getAttribute("OPERNAME");
    String spIndex = (String)session.getAttribute("SPINDEX");
    String spCode = (String)session.getAttribute("SPCODE");
    System.out.println("SERVICEKEY==="+serviceKey+"    operID =========="+operID);
    Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
    String sysTime = "";
    Vector sysInfo = (Vector)application.getAttribute("SYSINFO");
    try {
       String  errmsg = "";
       boolean flag =true;
       if (operID  == null){
          errmsg = i18n.getMessage("operGrpManage.cl.loginfirst");
          flag = false;
       }

       if(flag){

        String  optOperGroup = "";
        String  optServiceList = "";
        ArrayList serviceList = purview.getServiceList(serviceKey);
        String selectedServiceKey = (String)request.getParameter("serviceKey");
        String operGrpID = request.getParameter("operGroup") == null ? "" : (String)request.getParameter("operGroup");
        String operate = request.getParameter("operate") == null ? "" : (String)request.getParameter("operate");        // ?�����¡䨲o?
        String grpScript = request.getParameter("grpScript") == null ? "" : transferString((String)request.getParameter("grpScript"));  // ��a???����?2������?��?����?
        HashMap map = new HashMap();
        // ?D?????����?����???����?��?1???��D��??33??��
        if (selectedServiceKey == null)
        {
            if (serviceList.size() > 0)
                selectedServiceKey = (String)((HashMap)serviceList.get(0)).get("SERVICEKEY");
            else
                selectedServiceKey = serviceKey;
        }
        if (selectedServiceKey.equalsIgnoreCase("all"))
            selectedServiceKey = (String)((HashMap)serviceList.get(0)).get("SERVICEKEY");

        //???����???2������?������
        if (operate.equalsIgnoreCase("add")){
            HashMap addMap = new HashMap();
            addMap.put("SERVICEKEY",selectedServiceKey);
            addMap.put("GRPSCRIPT",grpScript);
            addMap.put("SESSIONOPERID",operID); 
            addMap.put("SESSIONOPERNAME",operName);                        
            purview.addOperGrp(addMap);
        }
        //DT??��???2������?������
        if (operate.equalsIgnoreCase("mod")){
            HashMap modMap = new HashMap();
            modMap.put("SERVICEKEY",selectedServiceKey);
            modMap.put("OPERGRPID",operGrpID);
            modMap.put("GRPSCRIPT",grpScript);
            modMap.put("SESSIONOPERID",operID); 
            modMap.put("SESSIONOPERNAME",operName);              
            purview.updateOperGrp(modMap);
        }
        // ��?3y��???2������?������
        if (operate.equalsIgnoreCase("del")){
            HashMap delMap = new HashMap();
            delMap.put("SERVICEKEY",selectedServiceKey);
            delMap.put("OPERGRPID",operGrpID);
            delMap.put("SESSIONOPERID",operID); 
            delMap.put("SESSIONOPERNAME",operName);               
            purview.delOperGrp(delMap);
        }

        //ArrayList funcGrp = purview.getFuncGrp(selectedServiceKey);
        Vector lststr=new Vector();
        lststr = purview.oper_grpscriptqry(selectedServiceKey,operID);
        int colcount = purview.oper_grpscriptqryrows();
        int size = lststr.size()/colcount;
        for(int i=0;i<lststr.size();i=i+colcount){
           optOperGroup = optOperGroup + "<option value='" + lststr.get(i) +"' >" + lststr.get(i+1) + "</option>";
        }

        for (int i = 0; i < serviceList.size(); i++) {
            map = (HashMap)serviceList.get(i);
            String serv = (String)map.get("SERVICEKEY");
            if(serv.equals(selectedServiceKey))
              optServiceList = optServiceList + "<option value=\"" +  (String)map.get("SERVICEKEY") +"\" selected >" + (String)map.get("DESCRIPTION") + "</option>";
            else
              optServiceList = optServiceList + "<option value=\"" +  (String)map.get("SERVICEKEY") +"\" >" + (String)map.get("DESCRIPTION") + "</option>";
        }
  %>


<HTML>
<head>
   <title>Untitled</title>
<link rel="stylesheet" href="style.css" type="text/css">
<jsp:include page="JsFun.jsp" flush="" />
</head>
<body topmargin="0" leftmargin="0" onload="initform(this.document.forms[0])"  class="body-style1">
<script language="JavaScript">
	if(parent.frames.length>0)
		//parent.document.all.main.style.height="400";
</script>

<Script language="javascript">
   var v_grpscript = new Array(<%= size + "" %>);
   var v_creatorid = new Array(<%= size + "" %>);
  <%
        int j = 0;
        for(int i=0;i<lststr.size();i=i+colcount){
  %>
    v_grpscript[<%= j + "" %>] = '<%= lststr.get(i+1) %>';
    v_creatorid[<%= j + "" %>] = '<%= lststr.get(i+2) %>';
  <%
    j = j+1;

  } %>
  function initform(pform){
	 document.forms[0].bModify.disabled = true;
	 document.forms[0].bDelete.disabled = true;
	 document.forms[0].bConfirm.disabled = true;
	 document.forms[0].bCancel.disabled = true;
	 document.forms[0].grpScript.disabled = true;
	 if(document.forms[0].operGroup.length>0){
	  document.forms[0].operGroup.selectedIndex = 0;
	  onOperGroup();
	 }
  }
  function actAdd(){

	 document.forms[0].operate.value = "add";
	 document.forms[0].grpIndex.value = document.forms[0].operGroup.selectedIndex;
	 document.forms[0].bAdd.disabled = true;
	 document.forms[0].bConfirm.disabled = false;
	 document.forms[0].bModify.disabled = true;
	 document.forms[0].bDelete.disabled = true;
	 document.forms[0].bCancel.disabled = false;
	 document.forms[0].grpScript.value = "";
	 document.forms[0].grpScript.innertext = "";
	 document.forms[0].operGroup.selectedIndex = -1;
	 document.forms[0].grpScript.disabled = false;
	 document.forms[0].grpScript.focus();
	 return true;
  }

  function actModify(){
     var select = parseInt(document.forms[0].operGroup.selectedIndex);
     document.forms[0].grpIndex.value = select;
     if(select == -1){
         alert("<fmt:message key="operGrpManage.grpupdatecandidate"/>");
     	return false;
     }
     document.forms[0].operate.value = "mod";
     document.forms[0].bAdd.disabled = true;
     document.forms[0].bConfirm.disabled = false;
     document.forms[0].bModify.disabled = true;
     document.forms[0].bDelete.disabled = true;
     document.forms[0].bCancel.disabled = false;
     document.forms[0].grpScript.disabled = false;
     document.forms[0].grpScript.focus();
     return true;
  }

  function actDelete() {
      var select = parseInt(document.forms[0].operGroup.selectedIndex);
	  if(select == -1)
	  {
	      alert("<fmt:message key="operGrpManage.choosegrpfordel"/>");
	  	return false;
	  }
	  if(confirm("<fmt:message key="operGrpManage.suretodelgrp"/>"))
	  {
	      document.forms[0].operate.value = "del";
	  	  document.forms[0].submit();
	  }
  }
  function actConfirm() {

	  if(!checkInput())
	     return false;
	  document.forms[0].submit();
  }

  function onServiceKey(){
    document.forms[0].submit();
  }

  function onOperGroup(){
	 var select = parseInt(document.forms[0].operGroup.selectedIndex);
	 if(select==-1)
	    return;
	 var createid = v_creatorid[select];
	 document.forms[0].grpScript.value = v_grpscript[select] ;
	 if(createid>0){
	    document.forms[0].grpScript.disabled = false;
	    document.forms[0].bModify.disabled = false;
	    document.forms[0].bDelete.disabled = false;
	    document.forms[0].bAdd.disabled = false;
        document.forms[0].bCancel.disabled = true;
	    document.forms[0].bConfirm.disabled = true;
	}
	else{
	   document.forms[0].grpScript.disabled = true;
       document.forms[0].bModify.disabled = true;
       document.forms[0].bDelete.disabled = true;
       document.forms[0].bAdd.disabled = false;
       document.forms[0].bCancel.disabled = true;
       document.forms[0].bConfirm.disabled = true;
	}
  }

  function checkInput(){
     var value = document.forms[0].grpScript.value;
     if( value == ""){
	     alert("<fmt:message key="operGrpManage.inputgrpname"/>");
	     document.forms[0].grpScript.focus();
	     return false;
	 }
        if (!CheckInputStr(document.forms[0].grpScript,'<fmt:message key="operGroupList.grpname"/>')){
           document.forms[0].grpScript.focus();
           return false;
        }
	 if(strlength(value)>40){
	     alert("<fmt:message key="operGrpManage.grpnameexceed"/>");
	     document.forms[0].grpScript.focus();
	     return false;
	 }
	 var select  = document.forms[0].operGroup.selectedIndex;
         var flag = 0;
        if(select ==-1){  //����?��2������?������
          for(var index=0; index<v_grpscript.length;index++){
           if(v_grpscript[index] == value){
             flag = 1;
             break;
          }
        }
    }
    else   //DT??
      for(var index=0; index<v_grpscript.length;index++){
        if(v_grpscript[index] == value ){
            flag = 1;
            break;
        }
       }
     if(flag == 1){
        alert("<fmt:message key="operGrpManage.grpexisted"/>");
        document.forms[0].grpScript.focus();
        return false;
     }
     return true;

  }

  function actCancel(){
    document.forms[0].operGroup.selectedIndex = parseInt(document.forms[0].grpIndex.value);
    onOperGroup();
    document.forms[0].bAdd.disabled = false;
	document.forms[0].bConfirm.disabled = true;
	document.forms[0].bModify.disabled = true;
	document.forms[0].bDelete.disabled = true;
	document.forms[0].bCancel.disabled = true;
	document.forms[0].grpScript.disabled = true;
  }
</script>

<form name="inForm" method="post" action="operGrpManage.jsp">
  <table height="400" align="center" cellSpacing="0" cellPadding="0" border="0" >
    <tr>
	<td width="100%" valign="middle">
	<table width="100%" border="0">
	<tr>
      <td colspan="2" height="26" align="center" class="text-title" valign="middle" background="button/n-9.gif" ><fmt:message key="operGrpManage.grpmanage"/></td>
    </tr>
    <tr>
      <td width="39%" align="center" valign="top" >
	  <table border="0" class="table-style5">
          <tr>
            <td align="center" class="table-style2"><fmt:message key="operGrpManage.grplist"/></td>
          </tr>
          <tr>
            <td align="center" height="100%"> <select name="operGroup" style="WIDTH: 260px; HEIGHT: 280px" size=40  onchange="onOperGroup()">
                <% out.print(optOperGroup); %>
              </select> </td>
          </tr>
      </table></td>
      <td width="61%" valign="top">
	  <table height="305" border="0" bordercolor="#E1F5FD"  class="table-style5">
          <tr>
            <td width="87" height="30" valign="bottom"> <fmt:message key="allotrights.managesystem"/> </td>
            <td width="161" HEIGHT="30" valign="bottom"> <select name="serviceKey" class="input-style1" onchange="onServiceKey()">
                <% out.println(optServiceList); %>
              </select> </td>
          </tr>

          <tr>
            <td height="46"  valign="bottom"> <fmt:message key="operGrpManage.operatorgrp"/> </td>
            <TD HEIGHT="46" valign="bottom"> <input name="grpScript" size="20" Maxlength="40"  class="input-style1">
            </td>
          </tr>

          <tr >
            <TD HEIGHT="104"   colspan="2" valign="bottom"> <input type="button" name="bAdd" value="<fmt:message key="operGrpManage.add"/>" onClick="actAdd()">
              <input type="button" name="bModify" value="<fmt:message key="operGrpManage.update"/>" onClick="actModify()">
              <input type="button" name="bDelete" value="<fmt:message key="operGrpManage.del"/>" onClick="actDelete()">
              <input type="button" name="bConfirm" value="<fmt:message key="operDel.yes"/>" onClick="actConfirm()">
              <input type="button" name="bCancel" value="<fmt:message key="operDel.cancel"/>" onClick="actCancel()">
            <input type="hidden" name="operate" value="-1"> <input type="hidden" name="grpIndex" value="-1"></td>
          </tr>
        </table></td>
    </tr>
	</table>
	</td></tr>
  </table>
</center>
</form>
</body>
</html>
<%
        }
        else {
            if(operID == null){
              %>
              <script language="javascript">
                    alert( "<fmt:message key="allotrights.loginfirst"/>");
                    document.URL = '../enter.jsp';
              </script>
              <%
                      }
                      else{
               %>
              <script language="javascript">
                   alert( "<fmt:message key="allotrights.forbidden"/>");
              </script>
              <%

              }
         }
    }
   catch (Exception e) {
%>
<html>
<body>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style1">
  <tr>
    <td colspan="2" align="center" ><fmt:message key="allotrights.erroroccured"/><%= e.getMessage() %></td>
	<td colspan="2" align="center" ><input type="button" value="OK" onclick="javascript:location.href='operGrpManage.jsp'" ></td>
  </tr>
</table>
</body>
</html>
<%
    }
%>
</body>
</html>

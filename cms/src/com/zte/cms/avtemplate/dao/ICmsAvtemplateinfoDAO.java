package com.zte.cms.avtemplate.dao;

import java.util.List;

import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsAvtemplateinfoDAO
{
    /**
     * 新增CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DAOException dao异常
     */
    public void insertCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;

    /**
     * 根据主键更新CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DAOException dao异常
     */
    public void updateCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;

    /**
     * 根据主键删除CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DAOException dao异常
     */
    public void deleteCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;

    /**
     * 根据主键查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @return 满足条件的CmsAvtemplateinfo对象
     * @throws DAOException dao异常
     */
    public CmsAvtemplateinfo getCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;

    /**
     * 根据条件查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @return 满足条件的CmsAvtemplateinfo对象集
     * @throws DAOException dao异常
     */
    public List<CmsAvtemplateinfo> getCmsAvtemplateinfoByCond(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;

    /**
     * 根据条件分页查询CmsAvtemplateinfo对象，作为查询条件的参数
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsAvtemplateinfo对象，作为查询条件的参数
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public List<CmsAvtemplateinfo> getTemplateByName(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException;
}
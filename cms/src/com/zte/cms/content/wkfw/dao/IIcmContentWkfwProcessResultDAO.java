package com.zte.cms.content.wkfw.dao;

import java.util.List;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwProcessResult;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmContentWkfwProcessResultDAO
{

    /**
     * <p>
     * 根据条件查询我申请的工单数量
     * </p>
     * 
     * @param uappablWkfwCondition UappablWkfwCondition对象，查询条件
     * @return 查询结果
     * @throws DomainServiceException抛出异常
     * 
     */
    public long getMyStartWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition) throws DAOException;

    /**
     * 根据条件查询UappablWkfwProcessResult对象
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象集
     * @return 满足条件的UappablWkfwProcessResult对象集
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public List<IcmContentWkfwProcessResult> getIcmContentWkfwProcessResultByCond(
            IcmContentWkfwCondition contentWkfwCondition) throws DAOException;

    /**
     * 根据条件分页查询UappablWkfwProcessResult对象，作为查询条件的参数
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询UappablWkfwProcessResult对象，作为查询条件的参数
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @param puEntity 排序对象
     * @return 查询结果
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwProcessResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
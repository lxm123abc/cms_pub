package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsCpbindDAO extends DynamicObjectBaseDao implements ICmsCpbindDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsCpbind(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("insert cmsCpbind starting...");
        super.insert("insertCmsCpbind", cmsCpbind);
        log.debug("insert cmsCpbind end");
    }

    public void updateCmsCpbind(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("update cmsCpbind by pk starting...");
        super.update("updateCmsCpbind", cmsCpbind);
        log.debug("update cmsCpbind by pk end");
    }

    public void updateCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("update cmsCpbind by conditions starting...");
        super.update("updateCmsCpbindByCond", cmsCpbind);
        log.debug("update cmsCpbind by conditions end");
    }

    public void deleteCmsCpbind(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("delete cmsCpbind by pk starting...");
        super.delete("deleteCmsCpbind", cmsCpbind);
        log.debug("delete cmsCpbind by pk end");
    }

    public void deleteCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("delete cmsCpbind by conditions starting...");
        super.delete("deleteCmsCpbindByCond", cmsCpbind);
        log.debug("update cmsCpbind by conditions end");
    }

    public CmsCpbind getCmsCpbind(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("query cmsCpbind starting...");
        CmsCpbind resultObj = (CmsCpbind) super.queryForObject("getCmsCpbind", cmsCpbind);
        log.debug("query cmsCpbind end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsCpbind> getCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("query cmsCpbind by condition starting...");
        List<CmsCpbind> rList = (List<CmsCpbind>) super.queryForList("queryCmsCpbindListByCond", cmsCpbind);
        log.debug("query cmsCpbind by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsCpbind by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsCpbindListCntByCond", cmsCpbind)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsCpbind> rsList = (List<CmsCpbind>) super.pageQuery("queryCmsCpbindListByCond", cmsCpbind, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsCpbind by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsCpbindListByCond", "queryCmsCpbindListCntByCond", cmsCpbind, start,
                pageSize, puEntity);
    }

    public void removeCpbindBySetCond(CmsCpbind cmsCpbind) throws DAOException
    {
        log.debug("remove cmsCpbind by set condition starting...");
        super.delete("deleteCmsCpbindBySetCond", cmsCpbind);
        log.debug("remove cmsCpbind by set condition end");
    }

}
package com.zte.cms.cpsp;

public class CpspLogConstant
{

    // 角色管理
    public static final String MGTTYPE_ROLE = "log.role.mgt";
    public static final String MGTYPE_DATAMGT = "log.data.mgt";
    public static final String OPERTYPE_LISENCE_ADD = "log.lisence.add";// 牌照方新增
    public static final String OPERTYPE_LISENCE_MOD = "log.lisence.modify";// 牌照方修改
    public static final String OPERTYPE_LISENCE_DEL = "log.lisence.delete";// 牌照方删除
    public static final String OPERTYPE_LISENCE_ID_ADD = "log.lisenceid.add";// 牌照方ID增加
    public static final String OPERTYPE_LISENCE_ID_DEL = "log.lisenceid.delete";// 牌照方ID删除
    public static final String OPERTYPE_CP_ID_ADD = "log.cpid.add";// CPID增加
    public static final String OPERTYPE_CP_ID_DEL = "log.cpid.delete";// CPID删除
    public static final String OPERTYPE_LICES_SYN_ADD = "log.licessyn.add";// 牌照方同步新增
    public static final String OPERTYPE_LICES_SYN_DEL = "log.licessyn.del";// 牌照方同步删除
    public static final String OPERTYPE_LISENCE_POUSE = "log.lisence.pouse";// 牌照方暂停
    public static final String OPERTYPE_LISENCE_REWORK = "log.lisence.rework";// 牌照方恢复
    // 操作
    public static final String OPERTYPE_LISENCE_ADD_INFO = "log.lisence.add.info";// 牌照方新增
    public static final String OPERTYPE_LISENCE_MOD_INFO = "log.lisence.modify.info";// 牌照方修改
    public static final String OPERTYPE_LISENCE_DEL_INFO = "log.lisence.delete.info";// 牌照方删除
    public static final String OPERTYPE_LISENCE_ID_ADD_INFO = "log.lisenceid.add.info";// 牌照方ID增加
    public static final String OPERTYPE_LISENCE_ID_DEL_INFO = "log.lisenceid.delete.info";// 牌照方ID删除
    public static final String OPERTYPE_CP_ID_ADD_INFO = "log.cpid.add.info";// CPID增加
    public static final String OPERTYPE_CP_ID_DEL_INFO = "log.cpid.delete.info";// CPID删除
    public static final String OPERTYPE_LICES_SYN_ADD_INFO = "log.licessyn.add.info";// 牌照方同步新增
    public static final String OPERTYPE_LICES_SYN_DEL_INFO = "log.licessyn.del.info";// 牌照方同步删除
    public static final String OPERTYPE_LISENCE_POUSE_INFO = "log.lisence.pouse.info";// 牌照方暂停
    public static final String OPERTYPE_LISENCE_REWORK_INFO = "log.lisence.rework.info";// 牌照方恢复
    // 操作结果
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

    public static final String OPERTYPE_OP_NONAUDIT_MGT = "log.operatoroper.nonanudit.mgt"; // 运营商免审设置管理
    public static final String OPERTYPE_OP_NONAUDIT_SET = "log.operatoroper.nonanudit.set"; // 运营商免审设置
    public static final String OPERTYPE_OP_NONAUDIT_CANCEL = "log.operatoroper.nonaudit.cancel"; // 运营商管理员免审设置取消
    public static final String OPERTYPE_OP_NONAUDIT_SET_INFO = "log.operatoroper.nonaudit.set.info"; // 运营商免审设置
    public static final String OPERTYPE_OP_NONAUDIT_CANCEL_INFO = "log.operatoroper.nonaudit.cancel.info";

    public static final String OPERTYPE_LP_NONAUDIT_MGT = "log.lisence.nonaudit.mgt"; // 牌照方免审设置管理
    public static final String OPERTYPE_LP_NONAUDIT_SET = "log.lisence.nonaudit.set"; // 牌照方免审设置
    public static final String OPERTYPE_LP_NONAUDIT_CANCEL = "log.lisence.nonaudit.cancel"; // 牌照方免审设置取消
    public static final String OPERTYPE_LP_NONAUDIT_SET_INFO = "log.lisence.nonaudit.set.info"; // 牌照方免审设置
    public static final String OPERTYPE_LP_NONAUDIT_CANCEL_INFO = "log.lisence.nonaudit.cancel.info";
}

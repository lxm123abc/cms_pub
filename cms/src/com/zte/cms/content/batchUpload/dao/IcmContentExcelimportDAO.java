package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.dao.IIcmContentExcelimportDAO;
import com.zte.cms.content.batchUpload.model.IcmContentExcelimport;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmContentExcelimportDAO extends DynamicObjectBaseDao implements IIcmContentExcelimportDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmContentExcelimport(IcmContentExcelimport icmContentExcelimport) throws DAOException
    {
        log.debug("insert icmContentExcelimport starting...");
        super.insert("insertIcmContentExcelimport", icmContentExcelimport);
        log.debug("insert icmContentExcelimport end");
    }

    @SuppressWarnings("unchecked")
    public List<IcmContentExcelimport> getIcmContentExcelimportByCond(IcmContentExcelimport icmContentExcelimport)
            throws DAOException
    {
        log.debug("query icmContentExcelimport by condition starting...");
        List<IcmContentExcelimport> rList = (List<IcmContentExcelimport>) super.queryForList(
                "queryIcmContentExcelimportListByCond", icmContentExcelimport);
        log.debug("query icmContentExcelimport by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query icmContentExcelimport by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super
                .queryForObject("queryIcmContentExcelimportListCntByCond", icmContentExcelimport)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmContentExcelimport> rsList = (List<IcmContentExcelimport>) super.pageQuery(
                    "queryIcmContentExcelimportListByCond", icmContentExcelimport, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmContentExcelimport by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryIcmContentExcelimportListByCond", "queryIcmContentExcelimportListCntByCond",
                icmContentExcelimport, start, pageSize, puEntity);
    }

}
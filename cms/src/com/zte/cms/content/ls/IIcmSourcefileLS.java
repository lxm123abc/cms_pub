package com.zte.cms.content.ls;

import com.zte.cms.content.model.IcmSourcefile;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmSourcefileLS
{
    /**
     * 增加母片
     * 
     * @param icmSourcefile 母片
     * @return 操作结果
     * @throws Exception
     */
    public String addSourcefile(IcmSourcefile icmSourcefile) throws Exception;

    /**
     * 删除母片
     * 
     * @param icmSourcefile 母片
     * @return 操作结果
     * @throws Exception
     */
    public String deleteSourcefile(IcmSourcefile icmSourcefile) throws Exception;

    /**
     * 获取子文件预览路径
     * 
     * @param fileindex 子文件索引
     * @return 子文件预览路径
     * @throws Exception
     */
    public String getPreviewPath(Long fileindex) throws Exception;

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryDesc(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException;
}

package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.settlementDataRpt.common.CommonReturnType;
import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import java.io.IOException;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:27 2019/6/28
 * @Description:
 */
public interface SettlementEntDownFileController {

    /**
     *  新增企业文件
     */
    CommonReturnType insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile, String uploadResFileCtrl) throws DAOException;

    /**
     *  修改企业文件
     */
    CommonReturnType updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile,String uploadResFileCtrl) throws DAOException;

    /**
     *  通过spid单查
     */
    SettlementEntDownFile selectSettlementEntDownFileBySpid(String spid) throws DAOException;

    /**
     *  删除文件
     */
    CommonReturnType deleteSettlementEntDownFileName(SettlementEntDownFile settlementEntDownFile) throws DAOException;

    /**
     *  获取企业公共文件
     */
    CommonReturnType getSettlementCommonFile() throws DomainServiceException;

    /**
     *  修改公共文件
     */
    CommonReturnType uploadCommonFile() throws IOException, DomainServiceException;

}

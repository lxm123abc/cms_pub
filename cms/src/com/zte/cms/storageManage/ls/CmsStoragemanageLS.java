package com.zte.cms.storageManage.ls;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.check.IServerCheck;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.cms.storageManage.service.ICmsStoragemanageDS;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

public class CmsStoragemanageLS implements ICmsStoragemanageLS {
	// 日志
	private Log log = SSBBus.getLog(getClass());

	private ICmsStoragemanageDS ds = null;
	private IServerCheck checkserverls = null;

	// private String rootAddress = "d:\\storageManage\\";

	public void setDs(ICmsStoragemanageDS ds) {
		this.ds = ds;
	}

	public Map addSpace(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		String rtn = "";
		Map map = new HashMap();
		int hasSameID = 0; // 是否包含相同的空间标识
		int hasSameName = 0; // 是否存在相同的空间名称
		String storageID = ""; // 本节点标识
		storageID = cmsStoragemanage.getStorageid();
		// System.out.println("space id" + storageID);
		cmsStoragemanage.setCurrentcapacity(cmsStoragemanage
				.getStoragecapacity());
		cmsStoragemanage.setPriority(cmsStoragemanage.getPriority());// 空间自己有优先级
		String parentID = ""; // 父节点的标识（用于建立文件夹）
		// int parentPriority = 0; //父节点的优先级
		// 首先将父节点（查询出来）
		CmsStoragemanage parentWithIndex = new CmsStoragemanage();
		parentWithIndex.setStorageindex(cmsStoragemanage.getParentindex());
		CmsStoragemanage parentInfo = new CmsStoragemanage();

		parentInfo = getCmsStoragemanage(parentWithIndex);
		if (parentInfo == null) {
			rtn = "64";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		}

		long parentCurrentCapacity = 0L; // 保存父节点的可用空间

		long spaceCapacity = 0L; // 本节点的空间大小
		CmsStoragemanage parent = new CmsStoragemanage();
		parent.setParentindex(cmsStoragemanage.getParentindex());
		// System.out.println("parentDevice index is
		// :"+parent.getStorageindex());
		// System.out.println("cmsStoragemanage.getParentindex():" +
		// cmsStoragemanage.getParentindex());
		List<CmsStoragemanage> otherStorageSpaces = getCmsStoragemanageByCond(parent);
		// System.out.println("the length of other spaces" +
		// otherStorageSpaces.size());

		parentID = parentInfo.getStorageid();
		// parentPriority = parentInfo.getPriority();
		// cmsStoragemanage.setPriority(parentPriority); //将父节点的优先级赋给空间
		parentCurrentCapacity = Long.parseLong(parentInfo.getCurrentcapacity());
		spaceCapacity = Long.parseLong(cmsStoragemanage.getStoragecapacity());
		for (int i = 0; i < otherStorageSpaces.size(); i++) {

			if (otherStorageSpaces.get(i).getStorageid().equals(storageID)) {

				hasSameID = 1;
			}
			if (otherStorageSpaces.get(i).getStoragename().equals(
					cmsStoragemanage.getStoragename())) {

				hasSameName = 1;
			}

		}
		// 存在相同的标识
		if (hasSameID == 1) {
			rtn = "11";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		}
		// 存在相同的名称
		else if (hasSameName == 1) {
			rtn = "63";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		}
		// 不存在相同标识,判断空间大小是否小于设备的可用空间
		else if (spaceCapacity > parentCurrentCapacity) {
			rtn = "13";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		}
		// 可用空间足够大，则新增空间节点，且修改设备的可用空间
		else {
			String spacePath = ""; // 空间的创建路径
			String devicePath = ""; // 设备的路径，首先判断此路径是否存在，存在则创建，不存在，则不创建

			if (GlobalConstants.getMountPoint().endsWith("\\")) {
				spacePath = GlobalConstants.getMountPoint() + parentID + "\\"
						+ storageID;

			} else if (GlobalConstants.getMountPoint().endsWith("/")) {
				spacePath = GlobalConstants.getMountPoint() + parentID + "/"
						+ storageID;
			}
			devicePath = GlobalConstants.getMountPoint() + parentID;
			// 判断设备的路径是否存在
			File DeviceExit = new File(devicePath);
			if (DeviceExit.exists()) {
				File newDevice = new File(spacePath);
				newDevice.mkdir();
			} else {
				rtn = "66";
				CmsStoragemanage checkpath = new CmsStoragemanage();
				checkpath.setCurrentcapacity(devicePath);
				map.put("rtn", rtn);
				map.put("cmsStoragemanage", checkpath);
				return map;
			}

			// 修改设备的可分配空间大小
			parentCurrentCapacity = parentCurrentCapacity - spaceCapacity;
			parentInfo
					.setCurrentcapacity(String.valueOf(parentCurrentCapacity));
			log
					.debug("update the currentcapcacity of device after insert a new space");
			try {
				ds.updateCmsStoragemanage(parentInfo);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception1:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			log.debug("update the currentcity of device ends.");

			log.debug("addSpace cmsStoragemanage by pk starting...");
			try {
				ds.insertCmsStoragemanage(cmsStoragemanage);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception2:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			// 增加节点成功
			log.debug("addSpace cmsStoragemanage by pk end");

			rtn = "12";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", cmsStoragemanage);

			// 写日志
			AppLogInfoEntity loginfo = new AppLogInfoEntity();
			// 操作对象类型
			String optType = ResourceManager
					.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
			String optObject = String.valueOf(cmsStoragemanage
					.getStorageindex());
			String optObjecttype = ResourceManager
					.getResourceText("log.new.space.add"); // 新增空间

			OperInfo loginUser = LoginMgt.getOperInfo("CMS");
			// 操作员编码
			String operId = loginUser.getOperid();
			// 操作员名称
			String operName = loginUser.getUserId();

			String optDetail = ResourceManager
					.getResourceText("log.space.add.msg"); // 操作员[opername]新增了空间[storageindex]
			optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
					.filterDollarStr(operName));
			optDetail = optDetail.replaceAll("\\[storageindex\\]", String
					.valueOf(cmsStoragemanage.getStorageindex()));

			loginfo.setUserId(operId);
			loginfo.setOptType(optType);
			loginfo.setOptObject(optObject);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptDetail(optDetail);
			loginfo.setOptTime("");
			loginfo.setServicekey("CMS");

			LogInfoMgt.doLog(loginfo);

			return map;

		}

	}

	public Map addDevice(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		Map map = new HashMap();
		String rtn = "";
		int hasSameID = 0; // 是否包含相同的设备标识
		int hasSameName = 0;
		String storageID = ""; // 本节点的标识

		storageID = cmsStoragemanage.getStorageid();

		// 通过父节点查询其他的设备
		CmsStoragemanage parent = new CmsStoragemanage();
		parent.setParentindex(cmsStoragemanage.getParentindex());

		List<CmsStoragemanage> otherDevices = getCmsStoragemanageByCond(parent);

		// System.out.println("the length if otherDevices is" +
		// otherDevices.size());

		for (int i = 0; i < otherDevices.size(); i++) {
			// System.out.println("the length if otherDevices is" +
			// otherDevices.get(i).getStorageid());
			if (otherDevices.get(i).getStorageid().equals(storageID)) {
				hasSameID = 1;
			}
			if (otherDevices.get(i).getStoragename().equals(
					cmsStoragemanage.getStoragename())) {
				hasSameName = 1;
			}
		}
		// 有相同的标识
		if (hasSameID == 1) {
			rtn = "9";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		}
		// 有相同的名称
		else if (hasSameName == 1) {
			rtn = "62";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", null);
			return map;
		} else {
			// 设备已经存在，不用新创建文件夹,只需判断设备是否存在
			// String GlobalAdress = GlobalConstants.getMountPoint();
			// File newDevice = new File(GlobalAdress+storageID);
			// if(!newDevice.exists())
			// {
			// rtn = "60";
			// map.put("rtn", rtn);
			// map.put("cmsStoragemanage", null);
			// return map;
			// }
			// newDevice.mkdir();
			// set可用容量
			// 对设备的大小进行扫描，如果该设备不存在，则返回，如果存在，则将设备大小写入
			String devicesize = "";
			try {
				devicesize = checkserverls.getFolderSize(storageID);
			} catch (Exception e) {
				log.error(e);

			}

			if (devicesize.equals("0")) {

				rtn = "61";
				map.put("rtn", rtn);
				map.put("cmsStoragemanage", null);
				return map;
			}
			String[] arr = devicesize.split(";");
			String availsize = "";// 可用容量
			String mountsize = "";// 最大容量,挂载容量
			String iptransfer = "";
			if (arr != null && arr.length > 0) {
				if (arr.length == 3) {
					availsize = arr[0];
					mountsize = arr[1];
					iptransfer = arr[2];
				} else {
					availsize = arr[0];
					mountsize = arr[1];
					iptransfer = "";
				}
			}
			// 设备存在
			cmsStoragemanage.setStoragecapacity(availsize);
			cmsStoragemanage.setCurrentcapacity(availsize);
			cmsStoragemanage.setMountcapacity(mountsize);// 挂载区间总大小
			cmsStoragemanage.setIpaddress(iptransfer);// ip地址
			log.debug("addDevice cmsStoragemanage by pk starting...");
			try {
				ds.insertCmsStoragemanage(cmsStoragemanage);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			// 增加节点成功
			log.debug("addDevice cmsStoragemanage by pk end");
			rtn = "10";
			map.put("rtn", rtn);
			map.put("cmsStoragemanage", cmsStoragemanage);

			// 写日志
			AppLogInfoEntity loginfo = new AppLogInfoEntity();
			// 操作对象类型
			String optType = ResourceManager
					.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
			String optObject = String.valueOf(cmsStoragemanage
					.getStorageindex());
			String optObjecttype = ResourceManager
					.getResourceText("log.new.device.add"); // 新增设备

			OperInfo loginUser = LoginMgt.getOperInfo("CMS");
			// 操作员编码
			String operId = loginUser.getOperid();
			// 操作员名称
			String operName = loginUser.getUserId();

			String optDetail = ResourceManager
					.getResourceText("log.device.add.msg"); // 操作员[opername]新增了设备[storageindex]
			optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
					.filterDollarStr(operName));
			optDetail = optDetail.replaceAll("\\[storageindex\\]", String
					.valueOf(cmsStoragemanage.getStorageindex()));

			loginfo.setUserId(operId);
			loginfo.setOptType(optType);
			loginfo.setOptObject(optObject);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptDetail(optDetail);
			loginfo.setOptTime("");
			loginfo.setServicekey("CMS");

			LogInfoMgt.doLog(loginfo);
			return map;
		}

	}

	public Map insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		Map map = new HashMap();
		String rtn = "";
		int storagemanageType = -1;
		storagemanageType = cmsStoragemanage.getStoragetype();
		// 设备节点
		if (storagemanageType == 1) {
			map = addDevice(cmsStoragemanage);
		}
		// 空间节点
		else {
			map = addSpace(cmsStoragemanage);
		}
		return map;
	}

	public String updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		ResourceMgt
				.addDefaultResourceBundle(storagemanageConstants.CMS_STORAGEMANAGE_RES_FILE_NAME);
		// 判断设备是否被删除
		CmsStoragemanage parentCmsStoragemanage = new CmsStoragemanage();
		parentCmsStoragemanage.setStorageindex(cmsStoragemanage
				.getParentindex());
		// 查询设备节点

		CmsStoragemanage parent = getCmsStoragemanage(parentCmsStoragemanage);

		if (parent == null) {
			return "65";
		}
		// 首先判断需要修改的空间是否被删除
		CmsStoragemanage spaceifexit = new CmsStoragemanage();
		spaceifexit.setStorageindex(cmsStoragemanage.getStorageindex());
		spaceifexit = ds.getCmsStoragemanage(spaceifexit);
		if (spaceifexit == null) {
			return ResourceMgt
					.findDefaultText(storagemanageConstants.CMS_STORAGEMANAGE_MSG_UPDATE_NO_EXIT);
		}
        //其次判断空间是否被绑定
        if(!spaceifexit.getStoragearea().equals("0"))
        {
        	return ResourceMgt.findDefaultText(storagemanageConstants.CMS_STORAGEMANAGE_MSG_UPDATE_SPACE_BIND_ALREADY);
        }

		// 判断修改后的同一个设备下空间名称是否已经存在
		CmsStoragemanage sameNamecheck = new CmsStoragemanage();
		sameNamecheck.setStoragename(cmsStoragemanage.getStoragename());
		sameNamecheck.setParentindex(cmsStoragemanage.getParentindex());
		List<CmsStoragemanage> SameNameList = ds
				.getCmsStoragemanageByCond(sameNamecheck);
		if (SameNameList.size() > 0) {
			for (int i = 0; i < SameNameList.size(); i++) {
				if (SameNameList.get(i).getStorageindex().longValue() != cmsStoragemanage
						.getStorageindex().longValue()) {
					return ResourceMgt
							.findDefaultText(storagemanageConstants.CMS_STORAGEMANAGE_MSG_UPDATE_SAMENAME_EXIT);
				}
			}
		}

		cmsStoragemanage.setStatus(spaceifexit.getStatus());
		long storagecapacitylong = 0L;
		// long parentThreshold = 0; //父节点的监控门限

		CmsStoragemanage oldcmsStoragemanage = new CmsStoragemanage();
		oldcmsStoragemanage.setParentindex(cmsStoragemanage.getParentindex());
		// 查询相同设备下的所有空间
		List<CmsStoragemanage> brothersUpdated = getCmsStoragemanageByCond(oldcmsStoragemanage);

		// parentThreshold = Long.parseLong(parent.getMonitorthreshold());
		// //得到设备监控门限

		// 将所有空间的大小相加和设备容量进行比较
		for (int i = 0; i < brothersUpdated.size(); i++) {
			if (brothersUpdated.get(i).getStorageindex().longValue() != cmsStoragemanage
					.getStorageindex().longValue()) {
				storagecapacitylong = storagecapacitylong
						+ Long.parseLong(brothersUpdated.get(i)
								.getStoragecapacity());
			}

		}
		storagecapacitylong = storagecapacitylong
				+ Long.parseLong(cmsStoragemanage.getStoragecapacity());

		// 如果修改后的空间和>设备的总的可用空间
		if (storagecapacitylong > Long.parseLong(parent.getStoragecapacity())) {
			return "0";
		} else {
			// 更新修改后的设备的可用空间
			long currentCapacity = Long.parseLong(parent.getStoragecapacity())
					- storagecapacitylong;
			parent.setCurrentcapacity(String.valueOf(currentCapacity));
			log
					.debug("upadte the current capacity of device when update the space");
			try {
				ds.updateCmsStoragemanage(parent);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception1:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			log
					.debug("upadte the current capacity of device when update the space ends");

			log.debug("update cmsStoragemanage by pk starting...");
			try {
				cmsStoragemanage.setStoragearea("0");
				cmsStoragemanage.setCurrentcapacity(cmsStoragemanage
						.getStoragecapacity());
				ds.updateCmsStoragemanage(cmsStoragemanage);

			} catch (DomainServiceException dsEx) {
				log.error("ds exception2:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			log.debug("update cmsStoragemanage by pk end");

			// 写日志,修改空间
			AppLogInfoEntity loginfo = new AppLogInfoEntity();
			// 操作对象类型
			String optType = ResourceManager
					.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
			String optObject = String.valueOf(cmsStoragemanage
					.getStorageindex());
			String optObjecttype = ResourceManager
					.getResourceText("log.space.update"); // 修改空间

			OperInfo loginUser = LoginMgt.getOperInfo("CMS");
			// 操作员编码
			String operId = loginUser.getOperid();
			// 操作员名称
			String operName = loginUser.getUserId();

			String optDetail = ResourceManager
					.getResourceText("log.space.update.msg"); // 操作员[opername]修改了空间[storageindex]
			optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
					.filterDollarStr(operName));
			optDetail = optDetail.replaceAll("\\[storageindex\\]", String
					.valueOf(cmsStoragemanage.getStorageindex()));

			loginfo.setUserId(operId);
			loginfo.setOptType(optType);
			loginfo.setOptObject(optObject);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptDetail(optDetail);
			loginfo.setOptTime("");
			loginfo.setServicekey("CMS");

			LogInfoMgt.doLog(loginfo);

			return "1";
		}

	}

	public void updateCmsStoragemanageList(
			List<CmsStoragemanage> cmsStoragemanageList)
			throws DomainServiceException {
		log.debug("update cmsStoragemanageList by pk starting...");
		if (null == cmsStoragemanageList || cmsStoragemanageList.size() == 0) {
			log.debug("there is no datas in cmsStoragemanageList");
			return;
		}
		try {
			for (CmsStoragemanage cmsStoragemanage : cmsStoragemanageList) {
				ds.updateCmsStoragemanage(cmsStoragemanage);
			}
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}
		log.debug("update cmsStoragemanageList by pk end");
	}

	// 定义设备的删除方法
	public Map deviceRemove(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		String rtn = "";
		Map map = new HashMap();
		String storageID = cmsStoragemanage.getStorageid();// 该设备的标识
		int hasbindStorageArea = 0; // 标志位，是否有空间存在绑定关系
		// 得到该设备下所有的空间
		CmsStoragemanage oldcmsStoragemanage = new CmsStoragemanage();
		oldcmsStoragemanage.setParentindex(cmsStoragemanage.getStorageindex());
		List<CmsStoragemanage> spaceCmsStoragemanages = getCmsStoragemanageByCond(oldcmsStoragemanage);
		// System.out.println("the length of spaceCmsStoragemanages is "
		// +spaceCmsStoragemanages.size());
		// 判断该设备是否有空间
		if (spaceCmsStoragemanages.size() > 0) {

			for (int i = 0; i < spaceCmsStoragemanages.size(); i++) {
				// System.out.println("the spaces of device:
				// "+spaceCmsStoragemanages.get(i).getStorageid());
				// 查看每个空间的绑定关系
				// 如果有绑定关系
				if (!spaceCmsStoragemanages.get(i).getStoragearea().equals("0")) {
					hasbindStorageArea = 1; // 绑定标志位设为1
				}
			}
			if (hasbindStorageArea == 1) // 如果有空间绑定关系，则直接返回
			{
				rtn = "5";
				map.put("cmsStoragemanage", null);
				map.put("rtn", rtn);
				return map;
			}
			// 无绑定关系,先以此删除空间，然后删除设备
			else {

				for (int i = 0; i < spaceCmsStoragemanages.size(); i++) {

					// 先删除该空间在磁盘上的文件夹
					String spaceStorageID = spaceCmsStoragemanages.get(i)
							.getStorageid();
					if (GlobalConstants.getMountPoint().endsWith("\\")) {
						File delf = new File(GlobalConstants.getMountPoint()
								+ storageID + "\\" + spaceStorageID);
						delf.delete();
					} else if (GlobalConstants.getMountPoint().endsWith("/")) {
						File delf = new File(GlobalConstants.getMountPoint()
								+ storageID + "/" + spaceStorageID);
						delf.delete();
					}

					try {
						ds
								.removeCmsStoragemanage(spaceCmsStoragemanages
										.get(i));
					} catch (DomainServiceException dsEx) {
						log.error("ds exception1:", dsEx);
						throw new DomainServiceException(dsEx);
					}

					// 删除空间的日志

					AppLogInfoEntity loginfo = new AppLogInfoEntity();
					// 操作对象类型
					String optType = ResourceManager
							.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
					String optObject = String.valueOf(spaceCmsStoragemanages
							.get(i).getStorageindex());
					String optObjecttype = ResourceManager
							.getResourceText("log.space.delete"); // 删除空间

					OperInfo loginUser = LoginMgt.getOperInfo("CMS");
					// 操作员编码
					String operId = loginUser.getOperid();
					// 操作员名称
					String operName = loginUser.getUserId();

					String optDetail = ResourceManager
							.getResourceText("log.space.delete.msg"); // 操作员[opername]删除了空间[storageindex]
					optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
							.filterDollarStr(operName));
					optDetail = optDetail.replaceAll("\\[storageindex\\]",
							String.valueOf(spaceCmsStoragemanages.get(i)
									.getStorageindex()));

					loginfo.setUserId(operId);
					loginfo.setOptType(optType);
					loginfo.setOptObject(optObject);
					loginfo.setOptObjecttype(optObjecttype);
					loginfo.setOptDetail(optDetail);
					loginfo.setOptTime("");
					loginfo.setServicekey("CMS");

					LogInfoMgt.doLog(loginfo);

				}

				try {
					ds.removeCmsStoragemanage(cmsStoragemanage);
				} catch (DomainServiceException dsEx) {
					log.error("ds exception2:", dsEx);
					throw new DomainServiceException(dsEx);
				}
				rtn = "6";
				map.put("cmsStoragemanage", cmsStoragemanage);
				map.put("rtn", rtn);

				// 写日志 删除设备
				AppLogInfoEntity loginfo = new AppLogInfoEntity();
				// 操作对象类型
				String optType = ResourceManager
						.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
				String optObject = String.valueOf(cmsStoragemanage
						.getStorageindex());
				String optObjecttype = ResourceManager
						.getResourceText("log.device.delete"); // 删除设备

				OperInfo loginUser = LoginMgt.getOperInfo("CMS");
				// 操作员编码
				String operId = loginUser.getOperid();
				// 操作员名称
				String operName = loginUser.getUserId();

				String optDetail = ResourceManager
						.getResourceText("log.detail.device.delete.msg"); // 操作员[opername]删除了设备[storageindex]
				optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
						.filterDollarStr(operName));
				optDetail = optDetail.replaceAll("\\[storageindex\\]", String
						.valueOf(cmsStoragemanage.getStorageindex()));

				loginfo.setUserId(operId);
				loginfo.setOptType(optType);
				loginfo.setOptObject(optObject);
				loginfo.setOptObjecttype(optObjecttype);
				loginfo.setOptDetail(optDetail);
				loginfo.setOptTime("");
				loginfo.setServicekey("CMS");

				LogInfoMgt.doLog(loginfo);
				return map;
			}
		}
		// 没有空间，直接删除，提示删除成功
		else {
			// 拼接路径,删除该设备在磁盘上的逻辑文件夹

			// File delf = new File(ds.getRootAddress()+storageID);
			// delf.delete();

			try {
				ds.removeCmsStoragemanage(cmsStoragemanage);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception3:", dsEx);
				throw new DomainServiceException(dsEx);
			}
			rtn = "6";
			map.put("cmsStoragemanage", cmsStoragemanage);
			map.put("rtn", rtn);
			return map;
		}

	}

	// 定义空间的删除方法
	public Map spaceRemove(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		Map map = new HashMap();
		String rtn = "";
		String storageArea = cmsStoragemanage.getStoragearea(); // 得到库区绑定关系情况

		// 如果空间有库区绑定关系，直接返回
		if (!storageArea.equals("0")) {
			rtn = "3";
			map.put("cmsStoragemanage", null);
			map.put("rtn", rtn);
			return map;
		}
		// 没有绑定关系
		else {

			String parentID = ""; // 保存设备的ID，用来拼接路径
			String storageID = ""; // 自己的ID，拼接路径
			String storageCapacity = cmsStoragemanage.getStoragecapacity(); // 得到要删除的节点空间大小
			long parentCapacity = 0L; // 保存父设备节点的现有可用空间
			CmsStoragemanage parentCmsStoragemanage = new CmsStoragemanage();
			parentCmsStoragemanage.setStorageindex(cmsStoragemanage
					.getParentindex());

			CmsStoragemanage parent = getCmsStoragemanage(parentCmsStoragemanage);

			parentCapacity = Long.parseLong(parent.getCurrentcapacity());

			parentCapacity = parentCapacity + Long.parseLong(storageCapacity);

			parent.setCurrentcapacity(String.valueOf(parentCapacity));

			// 拼接路径，删除该空间在磁盘上的文件夹
			parentID = parent.getStorageid();
			storageID = cmsStoragemanage.getStorageid();
			if (GlobalConstants.getMountPoint().endsWith("\\")) {
				File delf = new File(GlobalConstants.getMountPoint() + parentID
						+ "\\" + storageID);
				delf.delete();
			} else if (GlobalConstants.getMountPoint().endsWith("/")) {
				File delf = new File(GlobalConstants.getMountPoint() + parentID
						+ "/" + storageID);
				delf.delete();
			}

			log
					.debug("update the device currentCapacity before remove space... ");
			try {
				ds.updateCmsStoragemanage(parent);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception1:", dsEx);
				throw new DomainServiceException(dsEx);
			}
			log.debug("update device currentCapacity  by pk end");

			log.debug("remove cmsStoragemanage by pk starting...");
			try {
				ds.removeCmsStoragemanage(cmsStoragemanage);
			} catch (DomainServiceException dsEx) {
				log.error("ds exception:", dsEx);
				// TODO 根据实际应用，可以在此处添加异常国际化处理
				throw new DomainServiceException(dsEx);
			}
			log.debug("remove cmsStoragemanage by pk end");
			rtn = "4";
			map.put("cmsStoragemanage", cmsStoragemanage);
			map.put("rtn", rtn);
			AppLogInfoEntity loginfo = new AppLogInfoEntity();
			// 操作对象类型
			String optType = ResourceManager
					.getResourceText("log.storagemanage.manage.mgt"); // 设备管理
			String optObject = String.valueOf(cmsStoragemanage
					.getStorageindex());
			String optObjecttype = ResourceManager
					.getResourceText("log.space.delete"); // 删除空间

			OperInfo loginUser = LoginMgt.getOperInfo("CMS");
			// 操作员编码
			String operId = loginUser.getOperid();
			// 操作员名称
			String operName = loginUser.getUserId();

			String optDetail = ResourceManager
					.getResourceText("log.space.delete.msg"); // 操作员[opername]删除了空间[storageindex]
			optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil
					.filterDollarStr(operName));
			optDetail = optDetail.replaceAll("\\[storageindex\\]", String
					.valueOf(cmsStoragemanage.getStorageindex()));

			loginfo.setUserId(operId);
			loginfo.setOptType(optType);
			loginfo.setOptObject(optObject);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptDetail(optDetail);
			loginfo.setOptTime("");
			loginfo.setServicekey("CMS");

			LogInfoMgt.doLog(loginfo);

			return map;
		}

	}

	public Map removeCmsStoragemanage(CmsStoragemanage cmsStoragemanage)
			throws DomainServiceException {
		ResourceMgt
				.addDefaultResourceBundle(storagemanageConstants.CMS_STORAGEMANAGE_RES_FILE_NAME);
		Map map = new HashMap();

		String rtn = "";

		int storageType = -1;

		CmsStoragemanage localcmsStoragemanage = new CmsStoragemanage();
		localcmsStoragemanage.setStorageindex(cmsStoragemanage
				.getStorageindex());

		CmsStoragemanage cmsStoragemanageType = getCmsStoragemanage(localcmsStoragemanage);
		if (cmsStoragemanageType == null) {
			rtn = ResourceMgt
					.findDefaultText(storagemanageConstants.CMS_STORAGEMANAGE_MSG_DEL_NO_EXIT);
			map.put("cmsStoragemanage", null);
			map.put("rtn", rtn);
			return map;

		}

		else {
			storageType = cmsStoragemanageType.getStoragetype().intValue();

			// 如果是空间节点
			if (storageType == 2) {
				map = spaceRemove(cmsStoragemanageType);
				return map;
			}
			// 如果是设备节点
			else {
				map = deviceRemove(cmsStoragemanageType);
				return map;
			}
		}

	}

	public void removeCmsStoragemanageList(
			List<CmsStoragemanage> cmsStoragemanageList)
			throws DomainServiceException {
		log.debug("remove cmsStoragemanageList by pk starting...");
		if (null == cmsStoragemanageList || cmsStoragemanageList.size() == 0) {
			log.debug("there is no datas in cmsStoragemanageList");
			return;
		}
		try {
			for (CmsStoragemanage cmsStoragemanage : cmsStoragemanageList) {
				ds.removeCmsStoragemanageList(cmsStoragemanageList);
			}
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}
		log.debug("remove cmsStoragemanageList by pk end");
	}

	public CmsStoragemanage getCmsStoragemanage(
			CmsStoragemanage cmsStoragemanage) throws DomainServiceException {// 2是空间，1是设备
		log.debug("get cmsStoragemanage by pk starting...");
		CmsStoragemanage rsObj = null;
		try {
			rsObj = ds.getCmsStoragemanage(cmsStoragemanage);
			if (rsObj.getStoragetype() == 2) {
				CmsStoragemanage rsObjpar = new CmsStoragemanage();
				rsObjpar.setStorageindex(rsObj.getParentindex());
				rsObjpar = ds.getCmsStoragemanage(rsObjpar);
				rsObj.setStoragepath(GlobalConstants.getMountPoint()
						+ rsObjpar.getStorageid() + "/" + rsObj.getStorageid());
			} else {
				rsObj.setStoragepath(GlobalConstants.getMountPoint()
						+ rsObj.getStorageid());
			}
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}
		log.debug("get cmsStoragemanageList by pk end");
		return rsObj;
	}

	public List<CmsStoragemanage> getCmsStoragemanageByCond(
			CmsStoragemanage cmsStoragemanage) throws DomainServiceException {
		log.debug("get cmsStoragemanage by condition starting...");
		List<CmsStoragemanage> rsList = null;
		try {
			rsList = ds.getCmsStoragemanageByCond(cmsStoragemanage);
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}
		log.debug("get cmsStoragemanage by condition end");
		return rsList;
	}

	// /查询已经得到优先级的空间列表，给库区绑定选择
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQueryPriority(
			CmsStoragemanage cmsStoragemanage, int start, int pageSize)
			throws DomainServiceException {
		log.debug("get cmsStoragemanage page info by condition starting...");
		TableDataInfo tableInfo = null;
		try {
			tableInfo = ds.pageInfoQueryPriority(cmsStoragemanage, start,
					pageSize);
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}

		return tableInfo;
	}

	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage,
			int start, int pageSize) throws DomainServiceException {
		log.debug("get cmsStoragemanage page info by condition starting...");
		TableDataInfo tableInfo = null;
		try {
			tableInfo = ds.pageInfoQuery(cmsStoragemanage, start, pageSize);
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}

		return tableInfo;
	}

	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage,
			int start, int pageSize, PageUtilEntity puEntity)
			throws DomainServiceException {
		log.debug("get cmsStoragemanage page info by condition starting...");
		TableDataInfo tableInfo = null;
		try {
			tableInfo = ds.pageInfoQuery(cmsStoragemanage, start, pageSize,
					puEntity);
		} catch (DomainServiceException dsEx) {
			log.error("ds exception:", dsEx);
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException(dsEx);
		}

		return tableInfo;
	}

	public void setCheckserverls(IServerCheck checkserverls) {
		this.checkserverls = checkserverls;
	}

	/***************************************************************************
	 * 查询存储区可用空间大小
	 * 
	 **************************************************************************/
	public List<CmsStoragemanage> getCmsStoragemanageList(
			CmsStoragemanage cmsStoragmanage) throws DomainServiceException {
		List<CmsStoragemanage> cmsStorageManageList = ds
				.getCmsStoragemanageList();
		return cmsStorageManageList;
	}
}

package com.zte.cms.channel.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Physicalchannel extends DynamicBaseObject
{
    private java.lang.Long physicalchannelindex;
    private java.lang.String physicalchannelid;
    private java.lang.String destcasttype;
    private java.lang.String srccasttype;
    private java.lang.String channelid;
    private java.lang.Integer bitratetype;
    private java.lang.String cpcontentid;
    private java.lang.String multicastip;
    private java.lang.Integer multicastport;
    private java.lang.String unicasturl;
    private java.lang.Integer videotype;
    private java.lang.Integer audiotype;
    private java.lang.Integer resolution;
    private java.lang.Integer videoprofile;
    private java.lang.Integer systemlayer;
    private java.lang.Long domain;
    private java.lang.Integer hotdegree;
    private java.lang.Integer timeshift;
    private java.lang.Long storageduration;
    private java.lang.Long timeshiftduration;
    private java.lang.String namecn;
    private java.lang.String nameen;
    private java.lang.String desccn;
    private java.lang.String descen;
    private java.lang.Long reservecode1;
    private java.lang.Long reservecode2;
    private java.lang.Integer reservesel1;
    private java.lang.Integer reservesel2;
    private java.lang.String reservetime1;
    private java.lang.String reservetime2;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String creatoropername;
    private java.lang.String submittername;
    private java.lang.Integer templatetype;
    private java.lang.String wkfwroute;
    private java.lang.String wkfwpriority;
    private java.lang.String effecttime;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;

    private String channelname;

    private String physicalchannelideq;
    private String statuses;
    public String getStatuses() {
		return statuses;
	}
	public void setStatuses(String statuses) {
		this.statuses = statuses;
	}

    public String getPhysicalchannelideq()
    {
        return physicalchannelideq;
    }

    public void setPhysicalchannelideq(String physicalchannelideq)
    {
        this.physicalchannelideq = physicalchannelideq;
    }

    public String getChannelname()
    {
        return channelname;
    }

    public void setChannelname(String channelname)
    {
        this.channelname = channelname;
    }

    public java.lang.Long getPhysicalchannelindex()
    {
        return physicalchannelindex;
    }

    public void setPhysicalchannelindex(java.lang.Long physicalchannelindex)
    {
        this.physicalchannelindex = physicalchannelindex;
    }

    public java.lang.String getPhysicalchannelid()
    {
        return physicalchannelid;
    }

    public void setPhysicalchannelid(java.lang.String physicalchannelid)
    {
        this.physicalchannelid = physicalchannelid;
    }

    public java.lang.String getDestcasttype()
    {
        return destcasttype;
    }

    public void setDestcasttype(java.lang.String destcasttype)
    {
        this.destcasttype = destcasttype;
    }

    public java.lang.String getSrccasttype()
    {
        return srccasttype;
    }

    public void setSrccasttype(java.lang.String srccasttype)
    {
        this.srccasttype = srccasttype;
    }

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.Integer getBitratetype()
    {
        return bitratetype;
    }

    public void setBitratetype(java.lang.Integer bitratetype)
    {
        this.bitratetype = bitratetype;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.String getMulticastip()
    {
        return multicastip;
    }

    public void setMulticastip(java.lang.String multicastip)
    {
        this.multicastip = multicastip;
    }

    public java.lang.Integer getMulticastport()
    {
        return multicastport;
    }

    public void setMulticastport(java.lang.Integer multicastport)
    {
        this.multicastport = multicastport;
    }

    public java.lang.String getUnicasturl()
    {
        return unicasturl;
    }

    public void setUnicasturl(java.lang.String unicasturl)
    {
        this.unicasturl = unicasturl;
    }

    public java.lang.Integer getVideotype()
    {
        return videotype;
    }

    public void setVideotype(java.lang.Integer videotype)
    {
        this.videotype = videotype;
    }

    public java.lang.Integer getAudiotype()
    {
        return audiotype;
    }

    public void setAudiotype(java.lang.Integer audiotype)
    {
        this.audiotype = audiotype;
    }

    public java.lang.Integer getResolution()
    {
        return resolution;
    }

    public void setResolution(java.lang.Integer resolution)
    {
        this.resolution = resolution;
    }

    public java.lang.Integer getVideoprofile()
    {
        return videoprofile;
    }

    public void setVideoprofile(java.lang.Integer videoprofile)
    {
        this.videoprofile = videoprofile;
    }

    public java.lang.Integer getSystemlayer()
    {
        return systemlayer;
    }

    public void setSystemlayer(java.lang.Integer systemlayer)
    {
        this.systemlayer = systemlayer;
    }

    public java.lang.Long getDomain()
    {
        return domain;
    }
    public void setDomain(java.lang.Long domain)
    {
        this.domain = domain;
    }

    public java.lang.Integer getHotdegree()
    {
        return hotdegree;
    }

    public void setHotdegree(java.lang.Integer hotdegree)
    {
        this.hotdegree = hotdegree;
    }

    public java.lang.Integer getTimeshift()
    {
        return timeshift;
    }

    public void setTimeshift(java.lang.Integer timeshift)
    {
        this.timeshift = timeshift;
    }

    public java.lang.Long getStorageduration()
    {
        return storageduration;
    }

    public void setStorageduration(java.lang.Long storageduration)
    {
        this.storageduration = storageduration;
    }

    public java.lang.Long getTimeshiftduration()
    {
        return timeshiftduration;
    }

    public void setTimeshiftduration(java.lang.Long timeshiftduration)
    {
        this.timeshiftduration = timeshiftduration;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getDesccn()
    {
        return desccn;
    }

    public void setDesccn(java.lang.String desccn)
    {
        this.desccn = desccn;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.Long getReservecode1()
    {
        return reservecode1;
    }

    public void setReservecode1(java.lang.Long reservecode1)
    {
        this.reservecode1 = reservecode1;
    }

    public java.lang.Long getReservecode2()
    {
        return reservecode2;
    }

    public void setReservecode2(java.lang.Long reservecode2)
    {
        this.reservecode2 = reservecode2;
    }

    public java.lang.Integer getReservesel1()
    {
        return reservesel1;
    }

    public void setReservesel1(java.lang.Integer reservesel1)
    {
        this.reservesel1 = reservesel1;
    }

    public java.lang.Integer getReservesel2()
    {
        return reservesel2;
    }

    public void setReservesel2(java.lang.Integer reservesel2)
    {
        this.reservesel2 = reservesel2;
    }

    public java.lang.String getReservetime1()
    {
        return reservetime1;
    }

    public void setReservetime1(java.lang.String reservetime1)
    {
        this.reservetime1 = reservetime1;
    }

    public java.lang.String getReservetime2()
    {
        return reservetime2;
    }

    public void setReservetime2(java.lang.String reservetime2)
    {
        this.reservetime2 = reservetime2;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public void initRelation()
    {
        this.addRelation("physicalchannelindex", "PHYSICALCHANNELINDEX");
        this.addRelation("physicalchannelid", "PHYSICALCHANNELID");
        this.addRelation("destcasttype", "DESTCASTTYPE");
        this.addRelation("srccasttype", "SRCCASTTYPE");
        this.addRelation("channelid", "CHANNELID");
        this.addRelation("bitratetype", "BITRATETYPE");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("multicastip", "MULTICASTIP");
        this.addRelation("multicastport", "MULTICASTPORT");
        this.addRelation("unicasturl", "UNICASTURL");
        this.addRelation("videotype", "VIDEOTYPE");
        this.addRelation("audiotype", "AUDIOTYPE");
        this.addRelation("resolution", "RESOLUTION");
        this.addRelation("videoprofile", "VIDEOPROFILE");
        this.addRelation("systemlayer", "SYSTEMLAYER");
        this.addRelation("domain", "DOMAIN");
        this.addRelation("hotdegree", "HOTDEGREE");
        this.addRelation("timeshift", "TIMESHIFT");
        this.addRelation("storageduration", "STORAGEDURATION");
        this.addRelation("timeshiftduration", "TIMESHIFTDURATION");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("desccn", "DESCCN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("reservecode1", "RESERVECODE1");
        this.addRelation("reservecode2", "RESERVECODE2");
        this.addRelation("reservesel1", "RESERVESEL1");
        this.addRelation("reservesel2", "RESERVESEL2");
        this.addRelation("reservetime1", "RESERVETIME1");
        this.addRelation("reservetime2", "RESERVETIME2");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
    }
}

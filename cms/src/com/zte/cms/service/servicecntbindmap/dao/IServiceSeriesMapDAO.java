package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceSeriesMapDAO
{
    /**
     * 新增ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DAOException dao异常
     */
    public void insertServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException;

    /**
     * 根据主键更新ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DAOException dao异常
     */
    public void updateServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException;

    /**
     * 根据主键删除ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @throws DAOException dao异常
     */
    public void deleteServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException;

    /**
     * 根据主键查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @return 满足条件的ServiceSeriesMap对象
     * @throws DAOException dao异常
     */
    public ServiceSeriesMap getServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException;

    /**
     * 根据条件查询ServiceSeriesMap对象
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @return 满足条件的ServiceSeriesMap对象集
     * @throws DAOException dao异常
     */
    public List<ServiceSeriesMap> getServiceSeriesMapByCond(ServiceSeriesMap serviceSeriesMap) throws DAOException;

    /**
     * 根据条件分页查询ServiceSeriesMap对象，作为查询条件的参数
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceSeriesMap对象，作为查询条件的参数
     * 
     * @param serviceSeriesMap ServiceSeriesMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
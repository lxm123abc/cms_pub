package com.zte.cms.notice.dao;

import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.zte.cms.notice.model.Noticemap;
import com.zte.purview.access.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface INoticemapDAO
{

    public PageInfo pageInfoQuery(Noticemap notictmap, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQuery(Noticemap notictmap, int start, int pageSize, PageUtilEntity puEntity) throws DAOException;

    public PageInfo pageInfoQueryReplay(Noticemap notictmap, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryReplay(Noticemap notictmap, int start, int pageSize, PageUtilEntity puEntity) throws DAOException;

    public PageInfo pageInfoQueryView(Noticemap notictmap, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryView(Noticemap notictmap, int start, int pageSize, PageUtilEntity puEntity) throws DAOException;

    public Noticemap getNoticemap(Noticemap notictmap) throws DAOException;
    
    public Noticemap getNoticeLettermap(Noticemap notictmap) throws DAOException;
    
    public Noticemap getCmsOperatorPowerInfo(Noticemap notictmap) throws DAOException;

    public void insertNoticemap(Noticemap notictmap) throws DAOException;

    public void insertCmsMessageCp(Noticemap notictmap) throws DAOException;

    public void insertCmsMessageView(Noticemap notictmap) throws DAOException;

    public void deleteNoticemap(Noticemap notictmap) throws DAOException;
    
    public void revokeNoticemap(Noticemap notictmap) throws DAOException;

    public void deleteNoticeMessageCp(Noticemap notictmap) throws DAOException;

    public void deleteNoticeCmsMessageView(Noticemap notictmap) throws DAOException;
    
    public void deleteNoticeCmsReplay(Noticemap notictmap) throws DAOException;

    public void updateNoticemap(Noticemap notictmap) throws DAOException;

    public PageInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize) throws DAOException;

    public int getNumOfUnView(Noticemap noticemap);

    public Noticemap getNotificationDetail(Noticemap noticemap) throws DAOException;
    
    public List<Noticemap> getNotificationDetails(Noticemap noticemap) throws DAOException;

    public Noticemap getNotificationDetailOnlyMes(Noticemap noticemap) throws DAOException;
    
    public List<Noticemap> getNotificationDetailOnlyMess(Noticemap noticemap) throws DAOException;

    public void updateView(Noticemap noticemap) throws DAOException;
    
    public void updateReceiver(Noticemap noticemap) throws DAOException;

    public void updateReplay(Noticemap noticemap) throws DAOException;
    
    public void setStatusRead(Noticemap noticemap) throws DAOException;
    
    public void setNoticeViewcpnum(Noticemap noticemap) throws DAOException;

    public void insertReplay(Noticemap noticemap) throws DAOException;

    public void deleteReplay(Noticemap noticemap) throws DAOException;
    
    public void insertAttach(Noticemap noticemap) throws DAOException;
    
    public void insertAttach(FileItem tmp, Noticemap noticemap) throws DAOException;
    
    //public void insertAttach2(FileItem tmp, Noticemap noticemap) throws DAOException;
    
    public void insertMessageForward(Noticemap noticemap) throws DAOException;

    public void deleteAttach(Noticemap noticemap) throws DAOException;
    
    public PageInfo operInfoQuery(OperInformation operInfo,int start, int pageSize) throws DAOException;
    
    public PageInfo adminInfoQuery(OperInformation operInfo,int start, int pageSize) throws DAOException;

	public void updateNoticeReplay(Noticemap noticemap) throws DAOException;
}
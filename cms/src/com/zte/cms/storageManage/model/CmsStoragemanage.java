package com.zte.cms.storageManage.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsStoragemanage extends DynamicBaseObject
{
    private java.lang.Long storageindex;
    private java.lang.String storagename;
    private java.lang.Long parentindex;
    private java.lang.String storageid;
    private java.lang.String storagecapacity;
    private java.lang.String mountcapacity;
    private java.lang.String currentcapacity;
    private java.lang.Integer priority;
    private java.lang.Integer status;
    private java.lang.Integer accessmethod;
    private java.lang.String storageaddress;
    private java.lang.String storagepercentage;
    private java.lang.String monitorthreshold;
    private java.lang.String ipaddress;
    private java.lang.String storagearea;
    private java.lang.String description;
    private java.lang.Integer storagetype;
    private java.lang.String varbackup1;
    private java.lang.String varbackup2;
    private java.lang.Long numbackup1;
    private java.lang.Long numbackup2;
    private java.lang.String storagepath;
    
    
    public java.lang.String getStoragepath() {
		return storagepath;
	}

	public void setStoragepath(java.lang.String storagepath) {
		this.storagepath = storagepath;
	}

	public java.lang.String getMountcapacity() {
		return mountcapacity;
	}

	public void setMountcapacity(java.lang.String mountcapacity) {
		this.mountcapacity = mountcapacity;
	}

	public java.lang.String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(java.lang.String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public java.lang.Long getStorageindex()
    {
        return storageindex;
    }

    public void setStorageindex(java.lang.Long storageindex)
    {
        this.storageindex = storageindex;
    }

    public java.lang.String getStoragename()
    {
        return storagename;
    }

    public void setStoragename(java.lang.String storagename)
    {
        this.storagename = storagename;
    }

    public java.lang.Long getParentindex()
    {
        return parentindex;
    }

    public void setParentindex(java.lang.Long parentindex)
    {
        this.parentindex = parentindex;
    }

    public java.lang.String getStorageid()
    {
        return storageid;
    }

    public void setStorageid(java.lang.String storageid)
    {
        this.storageid = storageid;
    }

    public java.lang.String getStoragecapacity()
    {
        return storagecapacity;
    }

    public void setStoragecapacity(java.lang.String storagecapacity)
    {
        this.storagecapacity = storagecapacity;
    }

    public java.lang.String getCurrentcapacity()
    {
        return currentcapacity;
    }

    public void setCurrentcapacity(java.lang.String currentcapacity)
    {
        this.currentcapacity = currentcapacity;
    }

    public java.lang.Integer getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.Integer priority)
    {
        this.priority = priority;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getAccessmethod()
    {
        return accessmethod;
    }

    public void setAccessmethod(java.lang.Integer accessmethod)
    {
        this.accessmethod = accessmethod;
    }

    public java.lang.String getStorageaddress()
    {
        return storageaddress;
    }

    public void setStorageaddress(java.lang.String storageaddress)
    {
        this.storageaddress = storageaddress;
    }

    public java.lang.String getStoragepercentage()
    {
        return storagepercentage;
    }

    public void setStoragepercentage(java.lang.String storagepercentage)
    {
        this.storagepercentage = storagepercentage;
    }

    public java.lang.String getMonitorthreshold()
    {
        return monitorthreshold;
    }

    public void setMonitorthreshold(java.lang.String monitorthreshold)
    {
        this.monitorthreshold = monitorthreshold;
    }

    public java.lang.String getStoragearea()
    {
        return storagearea;
    }

    public void setStoragearea(java.lang.String storagearea)
    {
        this.storagearea = storagearea;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStoragetype()
    {
        return storagetype;
    }

    public void setStoragetype(java.lang.Integer storagetype)
    {
        this.storagetype = storagetype;
    }

    public java.lang.String getVarbackup1()
    {
        return varbackup1;
    }

    public void setVarbackup1(java.lang.String varbackup1)
    {
        this.varbackup1 = varbackup1;
    }

    public java.lang.String getVarbackup2()
    {
        return varbackup2;
    }

    public void setVarbackup2(java.lang.String varbackup2)
    {
        this.varbackup2 = varbackup2;
    }

    public java.lang.Long getNumbackup1()
    {
        return numbackup1;
    }

    public void setNumbackup1(java.lang.Long numbackup1)
    {
        this.numbackup1 = numbackup1;
    }

    public java.lang.Long getNumbackup2()
    {
        return numbackup2;
    }

    public void setNumbackup2(java.lang.Long numbackup2)
    {
        this.numbackup2 = numbackup2;
    }

    public void initRelation()
    {
        this.addRelation("storageindex", "STORAGEINDEX");
        this.addRelation("storagename", "STORAGENAME");
        this.addRelation("parentindex", "PARENTINDEX");
        this.addRelation("storageid", "STORAGEID");
        this.addRelation("storagecapacity", "STORAGECAPACITY");
        this.addRelation("currentcapacity", "CURRENTCAPACITY");
        this.addRelation("priority", "PRIORITY");
        this.addRelation("status", "STATUS");
        this.addRelation("accessmethod", "ACCESSMETHOD");
        this.addRelation("storageaddress", "STORAGEADDRESS");
        this.addRelation("storagepercentage", "STORAGEPERCENTAGE");
        this.addRelation("monitorthreshold", "MONITORTHRESHOLD");
        this.addRelation("storagearea", "STORAGEAREA");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("storagetype", "STORAGETYPE");
        this.addRelation("varbackup1", "VARBACKUP1");
        this.addRelation("varbackup2", "VARBACKUP2");
        this.addRelation("numbackup1", "NUMBACKUP1");
        this.addRelation("numbackup2", "NUMBACKUP2");
    }
}

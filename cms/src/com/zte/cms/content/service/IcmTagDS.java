package com.zte.cms.content.service;

import java.util.List;

import com.zte.cms.content.dao.IIcmTagDAO;
import com.zte.cms.content.model.IcmTag;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class IcmTagDS extends DynamicObjectBaseDS implements IIcmTagDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmTagDAO dao = null;

    public void setDao(IIcmTagDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("insert icmTag starting...");
        try
        {
            Long tagindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_tag");
            icmTag.setTagindex(tagindex);
            dao.insertIcmTag(icmTag);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmTag end");
    }

    public void updateIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("update icmTag by pk starting...");
        try
        {
            dao.updateIcmTag(icmTag);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTag by pk end");
    }

    public void updateIcmTagList(List<IcmTag> icmTagList) throws DomainServiceException
    {
        log.debug("update icmTagList by pk starting...");
        if (null == icmTagList || icmTagList.size() == 0)
        {
            log.debug("there is no datas in icmTagList");
            return;
        }
        try
        {
            for (IcmTag icmTag : icmTagList)
            {
                dao.updateIcmTag(icmTag);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTagList by pk end");
    }

    public void removeIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("remove icmTag by pk starting...");
        try
        {
            dao.deleteIcmTag(icmTag);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTag by pk end");
    }

    public void removeIcmTagList(List<IcmTag> icmTagList) throws DomainServiceException
    {
        log.debug("remove icmTagList by pk starting...");
        if (null == icmTagList || icmTagList.size() == 0)
        {
            log.debug("there is no datas in icmTagList");
            return;
        }
        try
        {
            for (IcmTag icmTag : icmTagList)
            {
                dao.deleteIcmTag(icmTag);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTagList by pk end");
    }

    public IcmTag getIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("get icmTag by pk starting...");
        IcmTag rsObj = null;
        try
        {
            rsObj = dao.getIcmTag(icmTag);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTagList by pk end");
        return rsObj;
    }

    public List<IcmTag> getIcmTagByCond(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("get icmTag by condition starting...");
        List<IcmTag> rsList = null;
        try
        {
            rsList = dao.getIcmTagByCond(icmTag);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTag by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get icmTag page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmTag, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTag>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmTag page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get icmTag page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmTag, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTag>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmTag page info by condition end");
        return tableInfo;
    }

    public List<IcmTag> getIcmTagList() throws DomainServiceException
    {
        log.debug("get IcmTag List starting...");
        IcmTag icmTag = null;
        List<IcmTag> icmTagList = null;
        try
        {
            icmTag = new IcmTag();
            icmTagList = dao.getIcmTagList(icmTag);

        }
        catch (DAOException daoEx)
        {
            log.error("get IcmTag List exception:" + daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("get IcmTag List end");
        return icmTagList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoUnclearQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException
    {
        log.debug("pageInfoUnclearQuery starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoUnclearQuery(icmTag, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTag>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("pageInfoUnclearQuery end");
        return tableInfo;
    }
}

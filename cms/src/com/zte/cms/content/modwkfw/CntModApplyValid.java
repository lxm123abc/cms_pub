package com.zte.cms.content.modwkfw;

import java.util.List;
import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.cms.content.program.service.ICmsProgramBufDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class CntModApplyValid extends CommonValid
{

    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected ICmsProgramBufDS cmsProgramBufDS = null;
    protected ICmsProgramDS cmsProgramDS = null;
    protected ICmsMovieBufDS cmsMovieBufDS = null;
    protected ICmsMovieDS cmsMovieDS = null;

    // protected ISchedulerService schedulerService = null;

    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {
        // 查询内容缓存表信息
        CmsProgram cmsProgram = null;
        try
        {
            CmsProgram program = new CmsProgram();
            program.setProgramindex(index);
            cmsProgram = cmsProgramDS.getCmsProgram(program);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        if (null != cmsProgram)
        {
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
            ResourceMgt.addDefaultResourceBundle("log_program_resource");

            // 操作对象类型
            String optType = ResourceMgt.findDefaultText("log.opttype.cnt.auditmgt"); // 内容审核管理
            String optObject = String.valueOf(cmsProgram.getProgramid());
            String optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.modauditvalid"); // 内容修改审核
            String optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.modapplyvalid"); // 内容修改申请生效

            if (null != optDetail)
            {
                optDetail = optDetail.replaceAll("\\[programid\\]", cmsProgram.getProgramid());
            }

            loginfo.setOptType(optType);
            loginfo.setOptObject(optObject);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptDetail(optDetail);
            loginfo.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);

            return loginfo;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void operate(Long index)
    {
        // String wkfwpbotype = "";
        try
        {
            CmsProgram oldProgram = new CmsProgram();
            oldProgram.setProgramindex(index);
            oldProgram = cmsProgramDS.getCmsProgram(oldProgram);
            // 将缓存表中的内容信息放到基本表中，并删除
            CmsProgramBuf cmsProgramBuf = new CmsProgramBuf();
            cmsProgramBuf.setProgramindex(index);
            cmsProgramBuf = cmsProgramBufDS.getCmsProgramBuf(cmsProgramBuf);

            CmsProgram cmsProgram = new CmsProgram();
            BeanCopier copier = BeanCopier.create(CmsProgramBuf.class, CmsProgram.class, true);
            copier.copy(cmsProgramBuf, cmsProgram, new BeanPropertyConvert());

            cmsProgram.setWorkflow(0);// 未启动
            cmsProgram.setStatus(cmsProgramBuf.getStatus()); // 更新为修改前的状态
            cmsProgramDS.updateCmsProgram(cmsProgram);
            // 删除内容buf表中的数据
            cmsProgramBufDS.removeCmsProgramBuf(cmsProgramBuf);

            if (cmsMovieBufDS != null && cmsMovieDS != null)
            {
                // 获得子内容buf表中的记录
                CmsMovieBuf cmsMovieBuf = new CmsMovieBuf();
                cmsMovieBuf.setProgramindex(index);
                List<CmsMovieBuf> cmsMovieBufList = cmsMovieBufDS.getCmsMovieBufByCond(cmsMovieBuf);
                // 获得子内容信息表中的子内容
                CmsMovie oldMovie = new CmsMovie();
                oldMovie.setProgramindex(index);
                List<CmsMovie> oldMovieList = cmsMovieDS.getCmsMovieByCond(oldMovie);
                // 将子内容buf表中的信息放到正式表中，并删除buf表中的相应记录
                for (CmsMovieBuf cmsMovieBufObj : cmsMovieBufList)
                {
                    CmsMovie movie = new CmsMovie();
                    BeanCopier movieCopier = BeanCopier.create(CmsMovieBuf.class, CmsMovie.class, true);
                    movieCopier.copy(cmsMovieBufObj, movie, new BeanPropertyConvert());
                    // 判断子内容基本信息表中是否存在记录，如是则更新，否则新增
                    CmsMovie cmsMovie = cmsMovieDS.getCmsMovie(movie);
                    if (cmsMovie != null)
                    {
                        cmsMovieDS.updateCmsMovie(movie);
                    }
                    else
                    {
                        cmsMovieDS.insertCmsMovie(movie);
                    }
                }
                // 查询出子内容正式表中的数据，如果在缓存表中没有这条记录，那么删除该记录
                for (CmsMovie oldMovieObj : oldMovieList)
                {
                    CmsMovieBuf movieBuf = new CmsMovieBuf();
                    movieBuf.setMovieindex(oldMovieObj.getMovieindex());
                    CmsMovieBuf newMovieBuf = cmsMovieBufDS.getCmsMovieBuf(movieBuf);
                    if (newMovieBuf == null)
                    {
                        cmsMovieDS.removeCmsMovie(oldMovieObj);
                    }
                }
                // 批量删除缓存表中的contact信息
                cmsMovieBufDS.removeCmsMovieBufList(cmsMovieBufList);
            }

            // 更新子内容状态为当前内容的状态
            CmsMovie cmsMovie = new CmsMovie();
            List<CmsMovie> cmsMovieList = null;
            cmsMovie.setProgramid(cmsProgram.getProgramid());
            cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
            if (null != cmsMovieList && cmsMovieList.size() > 0)
            {
                for (int i = 0; i < cmsMovieList.size(); i++)
                {
                    cmsMovieList.get(i).setStatus(cmsProgram.getStatus());
                }
                cmsMovieDS.updateCmsMovieList(cmsMovieList);
            }
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        // 删除定时任务
        // if(schedulerService != null){
        // try {
        // schedulerService.deleteJob(String.valueOf(index),wkfwpbotype+"U");
        // } catch (SchedulerException shEx) {
        // log.error("scheduler exception:" + shEx);
        // throw new RuntimeException(shEx);
        // }
        // }
    }

    public void setCmsProgramBufDS(ICmsProgramBufDS cmsProgramBufDS)
    {
        this.cmsProgramBufDS = cmsProgramBufDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsMovieBufDS(ICmsMovieBufDS cmsMovieBufDS)
    {
        this.cmsMovieBufDS = cmsMovieBufDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    // public void setSchedulerService(ISchedulerService schedulerService) {
    // this.schedulerService = schedulerService;
    // }
    //	
}

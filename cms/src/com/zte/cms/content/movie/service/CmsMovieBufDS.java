package com.zte.cms.content.movie.service;

import java.util.List;

import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.dao.ICmsMovieBufDAO;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsMovieBufDS implements ICmsMovieBufDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsMovieBufDAO dao = null;
    private IPrimaryKeyGenerator primaryKeyGenerator;
    private IUsysConfigDS usysConfigDS;

    public void setDao(ICmsMovieBufDAO dao)
    {
        this.dao = dao;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void insertCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("insert cmsMovieBuf starting...");
        try
        {
            Long movieindex = (Long) primaryKeyGenerator.getPrimarykey("cms_movie_buf");
            cmsMovieBuf.setMovieindex(movieindex);
            dao.insertCmsMovieBuf(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsMovieBuf end");
    }

    public void insertCmsMovieBufNoPK(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("insert cmsMovieBuf starting...");
        try
        {
            dao.insertCmsMovieBuf(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsMovieBuf end");
    }

    public void insertCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException
    {
        log.debug("insert cmsMovieBufList by pk starting...");
        if (null == cmsMovieBufList || cmsMovieBufList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieBufList");
            return;
        }
        try
        {
            dao.insertCmsMovieBufList(cmsMovieBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsMovieBufList by pk end");
    }

    public void updateCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("update cmsMovieBuf by pk starting...");
        try
        {
            dao.updateCmsMovieBuf(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieBuf by pk end");
    }

    public void updateCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException
    {
        log.debug("update cmsMovieBufList by pk starting...");
        if (null == cmsMovieBufList || cmsMovieBufList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieBufList");
            return;
        }
        try
        {
            dao.updateCmsMovieBufList(cmsMovieBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieBufList by pk end");
    }

    public void updateCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("update cmsMovieBuf by condition starting...");
        try
        {
            dao.updateCmsMovieBufByCond(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieBuf by condition end");
    }

    public void updateCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException
    {
        log.debug("update cmsMovieBufList by condition starting...");
        if (null == cmsMovieBufList || cmsMovieBufList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieBufList");
            return;
        }
        try
        {
            dao.updateCmsMovieBufListByCond(cmsMovieBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsMovieBufList by condition end");
    }

    public void removeCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("remove cmsMovieBuf by pk starting...");
        try
        {
            dao.deleteCmsMovieBuf(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieBuf by pk end");
    }

    public void removeCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException
    {
        log.debug("remove cmsMovieBufList by pk starting...");
        if (null == cmsMovieBufList || cmsMovieBufList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieBufList");
            return;
        }
        try
        {
            dao.deleteCmsMovieBufList(cmsMovieBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieBufList by pk end");
    }

    public void removeCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("remove cmsMovieBuf by condition starting...");
        try
        {
            dao.deleteCmsMovieBufByCond(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieBuf by condition end");
    }

    public void removeCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException
    {
        log.debug("remove cmsMovieBufList by condition starting...");
        if (null == cmsMovieBufList || cmsMovieBufList.size() == 0)
        {
            log.debug("there is no datas in cmsMovieBufList");
            return;
        }
        try
        {
            dao.deleteCmsMovieBufListByCond(cmsMovieBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsMovieBufList by condition end");
    }

    public CmsMovieBuf getCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("get cmsMovieBuf by pk starting...");
        CmsMovieBuf rsObj = null;
        try
        {
            rsObj = dao.getCmsMovieBuf(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsMovieBufList by pk end");
        return rsObj;
    }

    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException
    {
        log.debug("get cmsMovieBuf by condition starting...");
        List<CmsMovieBuf> rsList = null;
  
        try
        {
            rsList = dao.getCmsMovieBufByCond(cmsMovieBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsMovieBuf by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsMovieBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsMovieBuf, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsMovieBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsMovieBuf page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsMovieBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsMovieBuf, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsMovieBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsMovieBuf page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.cntSyncTask.service;

import java.util.List;

import com.zte.cms.cntSyncTask.dao.ICntSyncTaskDAO;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CntSyncTaskDS extends DynamicObjectBaseDS implements ICntSyncTaskDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICntSyncTaskDAO dao = null;

    public void setDao(ICntSyncTaskDAO dao)
    {
        this.dao = dao;
    }

    public void insertCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("insert cntSyncTask starting...");
        try
        {
            Long index;
 			if (cntSyncTask.getTaskindex() == null)
			{
 				index = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
				"cnt_sync_task");
 	            cntSyncTask.setTaskindex(index);
			}
            dao.insertCntSyncTask(cntSyncTask);
        	
        	/*
            Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
            cntSyncTask.setTaskindex(index);

            dao.insertCntSyncTask(cntSyncTask);
            */
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cntSyncTask end");
    }

    public void updateCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("update cntSyncTask by pk starting...");
        try
        {
            dao.updateCntSyncTask(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncTask by pk end");
    }

    public void updateCntSyncTaskList(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException
    {
        log.debug("update cntSyncTaskList by pk starting...");
        if (null == cntSyncTaskList || cntSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in cntSyncTaskList");
            return;
        }
        try
        {
            for (CntSyncTask cntSyncTask : cntSyncTaskList)
            {
                dao.updateCntSyncTask(cntSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncTaskList by pk end");
    }

    public void updateCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("update cntSyncTask by condition starting...");
        try
        {
            dao.updateCntSyncTaskByCond(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncTask by condition end");
    }

    public void updateCntSyncTaskListByCond(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException
    {
        log.debug("update cntSyncTaskList by condition starting...");
        if (null == cntSyncTaskList || cntSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in cntSyncTaskList");
            return;
        }
        try
        {
            for (CntSyncTask cntSyncTask : cntSyncTaskList)
            {
                dao.updateCntSyncTaskByCond(cntSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cntSyncTaskList by condition end");
    }

    public void removeCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("remove cntSyncTask by pk starting...");
        try
        {
            dao.deleteCntSyncTask(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncTask by pk end");
    }

    public void removeCntSyncTaskList(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException
    {
        log.debug("remove cntSyncTaskList by pk starting...");
        if (null == cntSyncTaskList || cntSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in cntSyncTaskList");
            return;
        }
        try
        {
            for (CntSyncTask cntSyncTask : cntSyncTaskList)
            {
                dao.deleteCntSyncTask(cntSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncTaskList by pk end");
    }

    public void removeCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("remove cntSyncTask by condition starting...");
        try
        {
            dao.deleteCntSyncTaskByCond(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncTask by condition end");
    }

    public void removeCntSyncTaskListByCond(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException
    {
        log.debug("remove cntSyncTaskList by condition starting...");
        if (null == cntSyncTaskList || cntSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in cntSyncTaskList");
            return;
        }
        try
        {
            for (CntSyncTask cntSyncTask : cntSyncTaskList)
            {
                dao.deleteCntSyncTaskByCond(cntSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cntSyncTaskList by condition end");
    }

    public CntSyncTask getCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("get cntSyncTask by pk starting...");
        CntSyncTask rsObj = null;
        try
        {
            rsObj = dao.getCntSyncTask(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncTaskList by pk end");
        return rsObj;
    }

    public List<CntSyncTask> getCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("get cntSyncTask by condition starting...");
        List<CntSyncTask> rsList = null;
        try
        {
            rsList = dao.getCntSyncTaskByCond(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncTask by condition end");
        return rsList;
    }

    public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("get cntSyncTask by pk starting...");
        CntSyncTask rsObj = null;
        try
        {
            rsObj = dao.getCntSynchorTask(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncTaskList by pk end");
        return rsObj;
    }
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cntSyncTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cntSyncTask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntSyncTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cntSyncTask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cntSyncTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cntSyncTask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntSyncTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cntSyncTask page info by condition end");
        return tableInfo;
    }
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
    throws DomainServiceException
    {
    	log.debug("get cntSyncTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerycntsync(cntSyncTask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntSyncTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cntSyncTask page info by condition end");
        return tableInfo;	
    }
    public List<CntSyncTask> getCntSynhisListBybatchid(CntSyncTask cntSyncTask) throws DomainServiceException
    {
        log.debug("get cntSyncTask by batchid starting...");
        List<CntSyncTask> rsObj = null;
        try
        {
            rsObj = dao.getCntSynhisListBybatchid(cntSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cntSyncTaskList by batchid end");
        return rsObj;
    }
}

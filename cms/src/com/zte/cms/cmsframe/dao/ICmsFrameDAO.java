package com.zte.cms.cmsframe.dao;

import java.util.List;

import com.zte.cms.cmsframe.model.CmsFrame;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsFrameDAO
{
    /**
     * 新增CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DAOException dao异常
     */
    public void insertCmsFrame(CmsFrame cmsFrame) throws DAOException;

    /**
     * 根据主键更新CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DAOException dao异常
     */
    public void updateCmsFrame(CmsFrame cmsFrame) throws DAOException;

    /**
     * 根据主键删除CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DAOException dao异常
     */
    public void deleteCmsFrame(CmsFrame cmsFrame) throws DAOException;

    /**
     * 根据主键查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @return 满足条件的CmsFrame对象
     * @throws DAOException dao异常
     */
    public CmsFrame getCmsFrame(CmsFrame cmsFrame) throws DAOException;

    /**
     * 根据条件查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @return 满足条件的CmsFrame对象集
     * @throws DAOException dao异常
     */
    public List<CmsFrame> getCmsFrameByCond(CmsFrame cmsFrame) throws DAOException;

    /**
     * 根据条件分页查询CmsFrame对象，作为查询条件的参数
     * 
     * @param cmsFrame CmsFrame对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsFrame对象，作为查询条件的参数
     * 
     * @param cmsFrame CmsFrame对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    public List<CmsFrame> getCmsFrameByName(CmsFrame cmsFrame) throws DAOException;
    public List<CmsFrame> queryCmsFrameListCntByNameUpdate(CmsFrame cmsFrame) throws DAOException;

}
package com.zte.cms.server.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsServer extends DynamicBaseObject
{
    private java.lang.Long serverindex;
    private java.lang.Integer servertype;
    private java.lang.String servername;
    private java.lang.String description;
    private java.lang.String useraccount;
    private java.lang.String userpassword;
    private java.lang.String inipaddress;
    private java.lang.String outipaddress;
    private java.lang.String port;
    private java.lang.String servicekey;
    private java.lang.String cpuusage;
    private java.lang.String totalmemary;
    private java.lang.String usedmemary;
    private java.lang.String freememary;
    private java.lang.String tablespace;
    private java.lang.String usedconnect;
    private java.lang.String totalconnect;
    private java.lang.String param1;
    private java.lang.String param2;
    private java.lang.String param3;
    private java.lang.String param4;
    private java.lang.String param5;

    public java.lang.Long getServerindex()
    {
        return serverindex;
    }

    public void setServerindex(java.lang.Long serverindex)
    {
        this.serverindex = serverindex;
    }

    public java.lang.Integer getServertype()
    {
        return servertype;
    }

    public void setServertype(java.lang.Integer servertype)
    {
        this.servertype = servertype;
    }

    public java.lang.String getServername()
    {
        return servername;
    }

    public void setServername(java.lang.String servername)
    {
        this.servername = servername;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getUseraccount()
    {
        return useraccount;
    }

    public void setUseraccount(java.lang.String useraccount)
    {
        this.useraccount = useraccount;
    }

    public java.lang.String getUserpassword()
    {
        return userpassword;
    }

    public void setUserpassword(java.lang.String userpassword)
    {
        this.userpassword = userpassword;
    }

    public java.lang.String getInipaddress()
    {
        return inipaddress;
    }

    public void setInipaddress(java.lang.String inipaddress)
    {
        this.inipaddress = inipaddress;
    }

    public java.lang.String getOutipaddress()
    {
        return outipaddress;
    }

    public void setOutipaddress(java.lang.String outipaddress)
    {
        this.outipaddress = outipaddress;
    }

    public java.lang.String getPort()
    {
        return port;
    }

    public void setPort(java.lang.String port)
    {
        this.port = port;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.String getCpuusage()
    {
        return cpuusage;
    }

    public void setCpuusage(java.lang.String cpuusage)
    {
        this.cpuusage = cpuusage;
    }

    public java.lang.String getTotalmemary()
    {
        return totalmemary;
    }

    public void setTotalmemary(java.lang.String totalmemary)
    {
        this.totalmemary = totalmemary;
    }

    public java.lang.String getUsedmemary()
    {
        return usedmemary;
    }

    public void setUsedmemary(java.lang.String usedmemary)
    {
        this.usedmemary = usedmemary;
    }

    public java.lang.String getFreememary()
    {
        return freememary;
    }

    public void setFreememary(java.lang.String freememary)
    {
        this.freememary = freememary;
    }

    public java.lang.String getTablespace()
    {
        return tablespace;
    }

    public void setTablespace(java.lang.String tablespace)
    {
        this.tablespace = tablespace;
    }

    public java.lang.String getUsedconnect()
    {
        return usedconnect;
    }

    public void setUsedconnect(java.lang.String usedconnect)
    {
        this.usedconnect = usedconnect;
    }

    public java.lang.String getTotalconnect()
    {
        return totalconnect;
    }

    public void setTotalconnect(java.lang.String totalconnect)
    {
        this.totalconnect = totalconnect;
    }

    public java.lang.String getParam1()
    {
        return param1;
    }

    public void setParam1(java.lang.String param1)
    {
        this.param1 = param1;
    }

    public java.lang.String getParam2()
    {
        return param2;
    }

    public void setParam2(java.lang.String param2)
    {
        this.param2 = param2;
    }

    public java.lang.String getParam3()
    {
        return param3;
    }

    public void setParam3(java.lang.String param3)
    {
        this.param3 = param3;
    }

    public java.lang.String getParam4()
    {
        return param4;
    }

    public void setParam4(java.lang.String param4)
    {
        this.param4 = param4;
    }

    public java.lang.String getParam5()
    {
        return param5;
    }

    public void setParam5(java.lang.String param5)
    {
        this.param5 = param5;
    }

    public void initRelation()
    {
        this.addRelation("serverindex", "SERVERINDEX");
        this.addRelation("servertype", "SERVERTYPE");
        this.addRelation("servername", "SERVERNAME");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("useraccount", "USERACCOUNT");
        this.addRelation("userpassword", "USERPASSWORD");
        this.addRelation("inipaddress", "INIPADDRESS");
        this.addRelation("outipaddress", "OUTIPADDRESS");
        this.addRelation("port", "PORT");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("cpuusage", "CPUUSAGE");
        this.addRelation("totalmemary", "TOTALMEMARY");
        this.addRelation("usedmemary", "USEDMEMARY");
        this.addRelation("freememary", "FREEMEMARY");
        this.addRelation("tablespace", "TABLESPACE");
        this.addRelation("usedconnect", "USEDCONNECT");
        this.addRelation("totalconnect", "TOTALCONNECT");
        this.addRelation("param1", "PARAM1");
        this.addRelation("param2", "PARAM2");
        this.addRelation("param3", "PARAM3");
        this.addRelation("param4", "PARAM4");
        this.addRelation("param5", "PARAM5");
    }
}

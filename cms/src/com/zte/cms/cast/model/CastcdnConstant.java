package com.zte.cms.cast.model;

public class CastcdnConstant
{
    // mgttype 管理类型 人物管理

    public static final String MGTTYPE_CAST = "log.cast.mgt";
    public static final String MGTTYPE_CAST_SYNC = "log.cast.mgt.sync";

    public final static int CAST_STATUS_WAITPUBLISH = 0;// 待发布
    public final static int CAST_STATUS_ADDSYNCHRONIZING = 10;// 新增同步中
    public final static int CAST_STATUS_ADDSYNCHRONIZED_SUCCESS = 20;// 新增同步成功
    public final static int CAST_STATUS_ADDSYNCHRONIZED_FAIL = 30;// 新增同步失败
    public final static int CAST_STATUS_UPDATESYNCHRONIZING = 40;// 修改同步中
    public final static int CAST_STATUS_UPDATESYNCHRONIZED_SUCCESS = 50;// 修改同步成功
    public final static int CAST_STATUS_UPDATESYNCHRONIZED_FAIL = 60;// 修改同步失败
    public final static int CAST_STATUS_CANCELSYNCHRONIZING = 70;// 取消同步中
    public final static int CAST_STATUS_CANCELSYNCHRONIZED_SUCCESS = 80;// 取消同步成功
    public final static int CAST_STATUS_CANCELSYNCHRONIZED_FAIL = 90;// 取消同步失败

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_CAST_ADD = "log.cast.add";// 人物新增
    public static final String OPERTYPE_CAST_MOD = "log.cast.modify";// 人物修改
    public static final String OPERTYPE_CAST_DEL = "log.cast.delete";// 人物删除
    public static final String OPERTYPE_CAST_PUBLISH = "log.cast.publish";// 人物同步
    public static final String OPERTYPE_CAST_CANCELPUBLISH = "log.cast.cancelpublish";// 人物取消同步
    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_CAST_ADD_INFO = "log.cast.add.info";
    public static final String OPERTYPE_CAST_MOD_INFO = "log.cast.modify.info";
    public static final String OPERTYPE_CAST_DEL_INFO = "log.cast.delete.info";
    public static final String OPERTYPE_CAST_PUBLISH_INFO = "log.cast.publish.info";
    public static final String OPERTYPE_CAST_CANCELPUBLISH_INFO = "log.cast.cancelpublish.info";

    public static final String SUCCESS = "0";
    public static final String FALSE = "1";
    public static final String PARTIALSUCCESS = "2";

    // private static final String PREFIX="cast.";
    public static final String OPER_SUCCESS = "cast.0000010";// 操作成功
    public static final String OPER_PARTIALSUCCESS = "cast.0000020";// 操作部分成功
    public static final String OPER_FALSE = "cast.0000030";// 操作失败
    public static final String CAST_ISNOTEXIST = "cast.0000040";// 人物不存在
    public static final String CAST_STATUS_ERROR = "cast.0000050";// 人物的状态不正确
    public static final String DATABASEEXCEPTION = "cast.0000060";// 数据库异常
    public static final String TARGET_ISNOTEXIST = "cast.0000070";// 同步目标系统不存在
    public static final String TEMPSPACE_ISNOTEXIST = "cast.0000080";// 获取临时区目录失败
    public static final String SYNFTPADDR_ISNOTEXIST = "cast.0000090";// 获取同步XML文件FTP地址失败
    public static final String MOUNTPOINTFAIL = "cast.00001000";// 获取挂载点失败
    public static final String CAST_PICTUREMAP = "cast.00001001";// 该人物关联的有海报，请先清除海报
    public static final String CAST_SYNEDPICTUREMAP = "cast.00001002";// 该人物关联的有已发布的海报，请先取消发布海报
    public static final String CAST_CASTROLEMAP = "cast.00001003"; // 该人物关联的有角色，请先清除关联关系
    public static final String CAST_CREATEXMLFAIL = "cast.00001004"; // 生成同步xml文件失败
    
    public static final String OPERTYPE_PLATFORM_CAST_BIND_ADD = "log.platform.cast.bind.add";
    public static final String OPERTYPE_PLATFORM_CAST_BIND_ADD_INFO = "log.platform.cast.bind.add.info";
    public static final String OPERTYPE_TARGET_CAST_BIND_ADD = "log.target.cast.bind.add";
    public static final String OPERTYPE_TARGET_CAST_BIND_ADD_INFO = "log.target.cast.bind.add.info";
    public static final String OPERTYPE_PLATFORM_CASTPICMAP_BIND_ADD = "log.platform.castpicmap.bind.add";
    public static final String OPERTYPE_PLATFORM_CASTPICMAP_BIND_ADD_INFO = "log.platform.castpicmap.bind.add.info";
    public static final String OPERTYPE_TARGET_CASTPICMAP_BIND_ADD = "log.target.castpicmap.bind.add";
    public static final String OPERTYPE_TARGET_CASTPICMAP_BIND_ADD_INFO = "log.target.castpicmap.bind.add.info";
    
    public static final String OPERTYPE_PLATFORM_CAST_BIND_DELETE = "log.platform.cast.bind.delete";
    public static final String OPERTYPE_PLATFORM_CAST_BIND_DELETE_INFO = "log.platform.cast.bind.delete.info";
    public static final String OPERTYPE_TARGET_CAST_BIND_DELETE = "log.target.cast.bind.delete";
    public static final String OPERTYPE_TARGET_CAST_BIND_DELETE_INFO = "log.target.cast.bind.delete.info";
    public static final String OPERTYPE_PLATFORM_CASTPICMAP_BIND_DELETE = "log.platform.castpicmap.bind.delete";
    public static final String OPERTYPE_PLATFORM_CASTPICMAP_BIND_DELETE_INFO = "log.platform.castpicmap.bind.delete.info";
    public static final String OPERTYPE_TARGET_CASTPICMAP_BIND_DELETE = "log.target.castpicmap.bind.delete";
    public static final String OPERTYPE_TARGET_CASTPICMAP_BIND_DELETE_INFO = "log.target.castpicmap.bind.delete.info";}

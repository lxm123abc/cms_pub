package com.zte.cms.cast.castsync.ls;

import com.zte.cms.cast.castsync.model.CastTargetSync;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastTargetSyncLS
{
    /**
     * 根据条件分页查询CastTargetSync对象 
     *
     * @param castTargetSync CastTargetSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws Exception ds异常
     */
    public TableDataInfo pageInfoQuery(CastTargetSync castTargetSync, int start, int pageSize) throws Exception;
    
    /**
     * 人物发布到网元
     *
     * @param castindex 人物index 
     * @param targetindex 目标系统index
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCastToTarget(Long castindex,Long targetindex, int platform ,int syncType) throws Exception;
    
    public TableDataInfo pageInfoQueryCastPicTargetMap(CastPictureMap castPictureMap, int start, int pageSize) throws Exception;
    
    public String publishCastPicMapToTarget(Long mapindex,Long targetindex,int platform,int syncType) throws Exception;

    //删除人物与网元关联关系
    public String deleteCastBindTarget(Long castindex,Long targetindex) throws Exception;
}

package com.zte.cms.storageManage.dao;

import java.util.ArrayList;
import java.util.List;

import com.zte.cms.storageManage.dao.ICmsStoragemanageDAO;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsStoragemanageDAO extends DynamicObjectBaseDao implements ICmsStoragemanageDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException
    {
        log.debug("insert cmsStoragemanage starting...");
        super.insert("insertCmsStoragemanage", cmsStoragemanage);
        log.debug("insert cmsStoragemanage end");
    }

    public void updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException
    {
        log.debug("update cmsStoragemanage by pk starting...");
        super.update("updateCmsStoragemanage", cmsStoragemanage);
        log.debug("update cmsStoragemanage by pk end");
    }

    public void deleteCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException
    {
        log.debug("delete cmsStoragemanage by pk starting...");
        super.delete("deleteCmsStoragemanage", cmsStoragemanage);
        log.debug("delete cmsStoragemanage by pk end");
    }

    public CmsStoragemanage getCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException
    {
        log.debug("query cmsStoragemanage starting...");
        CmsStoragemanage resultObj = (CmsStoragemanage) super.queryForObject("getCmsStoragemanage", cmsStoragemanage);
        log.debug("query cmsStoragemanage end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsStoragemanage> getCmsStoragemanageByCond(CmsStoragemanage cmsStoragemanage) throws DAOException
    {
        log.debug("query cmsStoragemanage by condition starting...");
        List<CmsStoragemanage> rList = (List<CmsStoragemanage>) super.queryForList("queryCmsStoragemanageListByCond",
                cmsStoragemanage);
        log.debug("query cmsStoragemanage by condition end");
        return rList;
    }

    // 得到设备的优先级 赋给空间
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryPriority(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsStoragemanage by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsStoragemanageCountByCondPriority", cmsStoragemanage))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsStoragemanage> rsList = (List<CmsStoragemanage>) super.pageQuery(
                    "queryCmsStorListbindedSpace", cmsStoragemanage, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsStoragemanage by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsStoragemanage by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsStoragemanageListCntByCond", cmsStoragemanage))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsStoragemanage> rsList = (List<CmsStoragemanage>) super.pageQuery("queryCmsStoragemanageListByCond",
                    cmsStoragemanage, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsStoragemanage by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsStoragemanageListByCond", "queryCmsStoragemanageListCntByCond",
                cmsStoragemanage, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    // 通过调用连表查询操作，返回空间所对应的设备优先级
    public List<CmsStoragemanage> getCmsStoragemanageByCondPriority(CmsStoragemanage cmsStoragemanage)
            throws DAOException
    {
        log.debug("query cmsStoragemanage by condition starting...");
        List<CmsStoragemanage> rList = (List<CmsStoragemanage>) super.queryForList(
                "queryCmsStoragemanageListByCondPriority", cmsStoragemanage);
        log.debug("query cmsStoragemanage by condition end");
        return rList;
    }

    // 查找绑定的空间中优先级最高，容量最大，且可用的空间
    public List<CmsStoragemanage> getCmsStoragemanageofbindedSpace(CmsStoragemanage cmsStoragemanage)
            throws DAOException
    {
        log.debug("query cmsStoragemanage by condition starting...");
        List<CmsStoragemanage> rList = (List<CmsStoragemanage>) super.queryForList(
                "queryCmsStorListbindedSpace", cmsStoragemanage);               
        log.debug("query cmsStoragemanage by condition end");
        return rList;
    }

    /*******************************************************************************************************************
     * 查询存储区可用空间大小
     * 
     ******************************************************************************************************************/
    public List<CmsStoragemanage> getCmsStoragemanageLists() throws DomainServiceException
    {
        List<CmsStoragemanage> allStorageManageList = new ArrayList();
        CmsStoragemanage cmsStoragemanageTem = getCmsStoragemanageList("1");// 1标示临时区
        CmsStoragemanage cmsStoragemanageOnline = getCmsStoragemanageList("2");// 2标示在线区
        CmsStoragemanage cmsStoragemanageOffline = getCmsStoragemanageList("3");// 3标示离线区
        CmsStoragemanage cmsStoragemanageProduct = getCmsStoragemanageList("4");// 4标示制作区
        if (cmsStoragemanageTem == null)
        {
            cmsStoragemanageTem.setCurrentcapacity("");
            cmsStoragemanageTem.setStoragecapacity("");
            cmsStoragemanageTem.setStoragepercentage("");
            allStorageManageList.add(0, cmsStoragemanageTem);
        }
        else
        {
            allStorageManageList.add(0, cmsStoragemanageTem);
        }
        if (cmsStoragemanageOnline == null)
        {
            cmsStoragemanageOnline.setCurrentcapacity("");
            cmsStoragemanageOnline.setStoragecapacity("");
            cmsStoragemanageOnline.setStoragepercentage("");
            allStorageManageList.add(1, cmsStoragemanageOnline);
        }
        else
        {
            allStorageManageList.add(1, cmsStoragemanageOnline);
        }
        if (cmsStoragemanageOffline == null)
        {
            cmsStoragemanageOffline.setCurrentcapacity("");
            cmsStoragemanageOffline.setStoragecapacity("");
            cmsStoragemanageOffline.setStoragepercentage("");
            allStorageManageList.add(2, cmsStoragemanageOffline);
        }
        else
        {
            allStorageManageList.add(2, cmsStoragemanageOffline);
        }
        if (cmsStoragemanageProduct == null)
        {
            cmsStoragemanageProduct.setCurrentcapacity("");
            cmsStoragemanageProduct.setStoragecapacity("");
            cmsStoragemanageProduct.setStoragepercentage("");
            allStorageManageList.add(3, cmsStoragemanageProduct);
        }
        else
        {
            allStorageManageList.add(3, cmsStoragemanageProduct);
        }
        /**
         * allStorageManageList.add(cmsStoragemanageTem); allStorageManageList.add(cmsStoragemanageOnline);
         * allStorageManageList.add(cmsStoragemanageOffline); allStorageManageList.add(cmsStoragemanageProduct);
         */
        return allStorageManageList;
    }

    public CmsStoragemanage getCmsStoragemanageList(String storagearea) throws DomainServiceException
    {
        CmsStoragemanage cmsStoragemanage = new CmsStoragemanage();
        float Currentcapacity = 0;
        float Storagecapacity = 0;
        cmsStoragemanage.setStoragearea(storagearea);
        List<CmsStoragemanage> cmsStorageManageList = (List<CmsStoragemanage>) super.queryForList(
                "queryCmsStoragemanageListByCondStoragearea", cmsStoragemanage);
        for (int i = 0; i < cmsStorageManageList.size(); i++)
        {
            Currentcapacity += Float.parseFloat(cmsStorageManageList.get(i).getCurrentcapacity());
            Storagecapacity += Float.parseFloat(cmsStorageManageList.get(i).getStoragecapacity());
        }
        cmsStoragemanage.setCurrentcapacity(("" + Currentcapacity));
        cmsStoragemanage.setStoragecapacity(("" + Storagecapacity));

        return cmsStoragemanage;
    }
}

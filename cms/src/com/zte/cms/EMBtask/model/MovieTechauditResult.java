package com.zte.cms.EMBtask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class MovieTechauditResult extends DynamicBaseObject
{
    private java.lang.Long movieindex;
    private java.lang.Integer infotype;
    private java.lang.Long channel;
    private java.lang.Long length;
    private java.lang.Long pos;
    private java.lang.String type;
    private java.lang.String createtime;

    public java.lang.Long getMovieindex()
    {
        return movieindex;
    }

    public void setMovieindex(java.lang.Long movieindex)
    {
        this.movieindex = movieindex;
    }

    public java.lang.Integer getInfotype()
    {
        return infotype;
    }

    public void setInfotype(java.lang.Integer infotype)
    {
        this.infotype = infotype;
    }

    public java.lang.Long getChannel()
    {
        return channel;
    }

    public void setChannel(java.lang.Long channel)
    {
        this.channel = channel;
    }

    public java.lang.Long getLength()
    {
        return length;
    }

    public void setLength(java.lang.Long length)
    {
        this.length = length;
    }

    public java.lang.Long getPos()
    {
        return pos;
    }

    public void setPos(java.lang.Long pos)
    {
        this.pos = pos;
    }

    public java.lang.String getType()
    {
        return type;
    }

    public void setType(java.lang.String type)
    {
        this.type = type;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public void initRelation()
    {
        this.addRelation("movieindex", "MOVIEINDEX");
        this.addRelation("infotype", "INFOTYPE");
        this.addRelation("channel", "CHANNEL");
        this.addRelation("length", "LENGTH");
        this.addRelation("pos", "POS");
        this.addRelation("type", "TYPE");
        this.addRelation("createtime", "CREATETIME");
    }
}

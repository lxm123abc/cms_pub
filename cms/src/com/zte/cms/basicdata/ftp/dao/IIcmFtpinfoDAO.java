package com.zte.cms.basicdata.ftp.dao;

import java.util.List;

import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmFtpinfoDAO
{

    /**
     * 根据主键IcmFtpinfo对象
     * 
     * @param infoindex IcmFtpinfo的主键
     * @return IcmFtpinfo对象
     * @throws DAOException dao异常
     */
    IcmFtpinfo getIcmFtpinfoByinfoindex(Long infoindex) throws DAOException;

    /**
     * 新增IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @throws DAOException dao异常
     */
    public void insertIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据主键更新IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @throws DAOException dao异常
     */
    public void updateIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据条件更新IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo更新条件
     * @throws DAOException dao异常
     */
    public void updateIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据主键删除IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @throws DAOException dao异常
     */
    public void deleteIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据条件删除IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo删除条件
     * @throws DAOException dao异常
     */
    public void deleteIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据主键查询IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @return 满足条件的IcmFtpinfo对象
     * @throws DAOException dao异常
     */
    public IcmFtpinfo getIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据条件查询IcmFtpinfo对象
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @return 满足条件的IcmFtpinfo对象集
     * @throws DAOException dao异常
     */
    public List<IcmFtpinfo> getIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException;

    /**
     * 根据条件分页查询IcmFtpinfo对象，作为查询条件的参数
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmFtpinfo对象，作为查询条件的参数
     * 
     * @param icmFtpinfo IcmFtpinfo对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
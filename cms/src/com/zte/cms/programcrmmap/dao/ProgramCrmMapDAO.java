package com.zte.cms.programcrmmap.dao;

import java.util.List;

import com.zte.cms.programcrmmap.dao.IProgramCrmMapDAO;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ProgramCrmMapDAO extends DynamicObjectBaseDao implements IProgramCrmMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("insert programCrmMap starting...");
        super.insert("insertProgramCrmMap", programCrmMap);
        log.debug("insert programCrmMap end");
    }

    public void updateProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("update programCrmMap by pk starting...");
        super.update("updateProgramCrmMap", programCrmMap);
        log.debug("update programCrmMap by pk end");
    }

    public void updateProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("update programCrmMap by conditions starting...");
        super.update("updateProgramCrmMapByCond", programCrmMap);
        log.debug("update programCrmMap by conditions end");
    }

    public void deleteProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("delete programCrmMap by pk starting...");
        super.delete("deleteProgramCrmMap", programCrmMap);
        log.debug("delete programCrmMap by pk end");
    }

    public void deleteProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("delete programCrmMap by conditions starting...");
        super.delete("deleteProgramCrmMapByCond", programCrmMap);
        log.debug("update programCrmMap by conditions end");
    }

    public ProgramCrmMap getProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("query programCrmMap starting...");
        ProgramCrmMap resultObj = (ProgramCrmMap) super.queryForObject("getProgramCrmMap", programCrmMap);
        log.debug("query programCrmMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ProgramCrmMap> getProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("query programCrmMap by condition starting...");
        List<ProgramCrmMap> rList = (List<ProgramCrmMap>) super.queryForList("queryProgramCrmMapListByCond",
                programCrmMap);
        log.debug("query programCrmMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programCrmMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryProgramCrmMapListCntByCond", programCrmMap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramCrmMap> rsList = (List<ProgramCrmMap>) super.pageQuery("queryProgramCrmMapListByCond",
                    programCrmMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programCrmMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryProgramCrmMapListByCond", "queryProgramCrmMapListCntByCond", programCrmMap,
                start, pageSize, puEntity);
    }

/************************* publish management begin********************/
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programCrmMap by condition starting...");
        PageInfo pageInfo = null;
     	if (programCrmMap.getTypePlatform().equals("iptv3") )
     	{
            int totalCnt = ((Integer) super.queryForObject("queryProgramCrmMapSynListCntByCond", programCrmMap)).intValue();
            if (totalCnt > 0)
            {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
                List<ProgramCrmMap> rsList = (List<ProgramCrmMap>) super.pageQuery("queryProgramCrmMapSynListByCond",
                        programCrmMap, start, fetchSize);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
            }
            else
            {
                pageInfo = new PageInfo();
            }
     	}
     	if (programCrmMap.getTypePlatform().equals("iptv2") )
     	{
            int totalCnt = ((Integer) super.queryForObject("queryProgramCrmMap2SynListCntByCond", programCrmMap)).intValue();
            if (totalCnt > 0)
            {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
                List<ProgramCrmMap> rsList = (List<ProgramCrmMap>) super.pageQuery("queryProgramCrmMap2SynListByCond",
                        programCrmMap, start, fetchSize);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
            }
            else
            {
                pageInfo = new PageInfo();
            }
     	}

        log.debug("page query programCrmMap by condition end");
        return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programCrmMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querynormalProCrmMaptSynListCntByCond", programCrmMap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramCrmMap> rsList = (List<ProgramCrmMap>) super.pageQuery("querynormalProCrmMaptSynListByCond",
                    programCrmMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programCrmMap by condition end");
        return pageInfo;
    }    
   
    @SuppressWarnings("unchecked")
    public List<ProgramCrmMap> getProgramnormalCrmMapSynByCond(ProgramCrmMap programCrmMap) throws DAOException
    {
        log.debug("query programCrmMap by condition starting...");
        List<ProgramCrmMap> rList = (List<ProgramCrmMap>) super.queryForList("getnormalProCrmMaptSynListByCond",
                programCrmMap);
        log.debug("query programCrmMap by condition end");
        return rList;
    }   
    
    /************************* publish management begin********************/
}
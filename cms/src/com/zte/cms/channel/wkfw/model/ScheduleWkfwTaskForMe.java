package com.zte.cms.channel.wkfw.model;

/**
 * 工作流任务查询结果
 * 
 * @author 唐秀华 tang.xiuhua@zte.com.cn
 */

public class ScheduleWkfwTaskForMe
{
    /**
     * 进程ID
     */
    private java.lang.Long processId;
    /**
     * 进程名称
     */
    private java.lang.String processName;
    /**
     * 进程状态
     */
    private java.lang.String processStatus;
    /**
     * 进程创建者
     */
    private java.lang.String creator;
    /**
     * 进程开始时间
     */
    private java.util.Date processStartTime;
    /**
     * 工作流对象编号，此处为strategyindex
     */
    private java.lang.Long pboId;
    /**
     * 工作流对象名称
     */
    private java.lang.String pboName;
    /**
     * 工作流类型
     */
    private java.lang.String pboType;
    /**
     * 工作流优先级
     */
    private java.lang.String priority;
    /**
     * 工作流流程状态描述：审核中、成功结束、失败结束
     */
    private java.lang.String nodeStatus;
    /**
     * 工作流流程状态编码
     */
    private java.lang.String nodeStatusCode;

    /**
     * 任务id
     */
    private java.lang.Long taskId;
    /**
     * 任务开始时间
     */
    private java.util.Date taskStartTime;
    /**
     * 参与者ID
     */
    private java.lang.String executorId;
    /**
     * 任务状态
     */
    private java.lang.String taskStatus;
    /**
     * 节点名称
     */
    private java.lang.String nodeName;
    /**
     * 接受状态
     */
    private java.lang.String acceptStatus;

    /**
     * 服务编码
     */
    private java.lang.String scheduleid;
    /**
     * 服务名称
     */
    private java.lang.String programname;
    
    private java.lang.String channelid;
    

    public java.lang.String getChannelid() {
		return channelid;
	}

	public void setChannelid(java.lang.String channelid) {
		this.channelid = channelid;
	}

	public java.lang.Long getProcessId()
    {
        return processId;
    }

    public void setProcessId(java.lang.Long processId)
    {
        this.processId = processId;
    }

    public java.lang.String getProcessName()
    {
        return processName;
    }

    public void setProcessName(java.lang.String processName)
    {
        this.processName = processName;
    }

    public java.lang.String getCreator()
    {
        return creator;
    }

    public void setCreator(java.lang.String creator)
    {
        this.creator = creator;
    }

    public java.util.Date getProcessStartTime()
    {
        return processStartTime;
    }

    public void setProcessStartTime(java.util.Date processStartTime)
    {
        this.processStartTime = processStartTime;
    }

    public java.lang.Long getPboId()
    {
        return pboId;
    }

    public void setPboId(java.lang.Long pboId)
    {
        this.pboId = pboId;
    }

    public java.lang.String getPboName()
    {
        return pboName;
    }

    public void setPboName(java.lang.String pboName)
    {
        this.pboName = pboName;
    }

    public java.lang.String getPboType()
    {
        return pboType;
    }

    public void setPboType(java.lang.String pboType)
    {
        this.pboType = pboType;
    }

    public java.lang.String getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.String priority)
    {
        this.priority = priority;
    }

    public java.lang.String getNodeStatus()
    {
        return nodeStatus;
    }

    public void setNodeStatus(java.lang.String nodeStatus)
    {
        this.nodeStatus = nodeStatus;
    }

    public java.lang.String getNodeStatusCode()
    {
        return nodeStatusCode;
    }

    public void setNodeStatusCode(java.lang.String nodeStatusCode)
    {
        this.nodeStatusCode = nodeStatusCode;
    }

    public java.lang.String getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(java.lang.String processStatus)
    {
        this.processStatus = processStatus;
    }

    public java.lang.Long getTaskId()
    {
        return taskId;
    }

    public void setTaskId(java.lang.Long taskId)
    {
        this.taskId = taskId;
    }

    public java.util.Date getTaskStartTime()
    {
        return taskStartTime;
    }

    public void setTaskStartTime(java.util.Date taskStartTime)
    {
        this.taskStartTime = taskStartTime;
    }

    public java.lang.String getExecutorId()
    {
        return executorId;
    }

    public void setExecutorId(java.lang.String executorId)
    {
        this.executorId = executorId;
    }

    public java.lang.String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(java.lang.String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public java.lang.String getNodeName()
    {
        return nodeName;
    }

    public void setNodeName(java.lang.String nodeName)
    {
        this.nodeName = nodeName;
    }

    public java.lang.String getAcceptStatus()
    {
        return acceptStatus;
    }

    public void setAcceptStatus(java.lang.String acceptStatus)
    {
        this.acceptStatus = acceptStatus;
    }

    public java.lang.String getScheduleid()
    {
        return scheduleid;
    }

    public void setScheduleid(java.lang.String scheduleid)
    {
        this.scheduleid = scheduleid;
    }

	public java.lang.String getProgramname() {
		return programname;
	}

	public void setProgramname(java.lang.String programname) {
		this.programname = programname;
	}
    

   

}

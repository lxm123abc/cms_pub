package com.zte.cms.service.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.cms.service.categoryservicemap.service.ICategoryServiceMapDS;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.service.IServiceWkfwhisDS;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceSchedualMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceSeriesMapDS;
import com.zte.cms.service.servicepublishmanager.ls.IServiceSynLS;
import com.zte.cms.service.update.apply.IServiceUpdateApply;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 服务模块的管理，包括模块的增删改查、审核、发布、取消发布、打包内容操作
 * 
 * @author shaoyan
 * 
 */

public class ServiceLS extends DynamicObjectBaseDS implements IServiceLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private IServiceDS serviceds = null;
    private IServiceWkfwhisDS servicewkfwhisds = null;
    private ITargetsystemDS targetSystemDS = null;
    private ICntSyncTaskDS cntSyncTaskDS = null;
    private ICntSyncRecordDS cntSyncRecordDS = null;
    private IServiceUpdateApply serviceupdateApply = null;
    private IServiceBufDS serviceBufds = null;
    private IPictureDS pictureds = null;

    private IServiceSchedualMapDS serviceschedulemapds = null;
    private IServiceProgramMapDS serviceprogrammapds = null;
    private IServiceChannelMapDS servicechannelmapds = null;
    private IServiceSeriesMapDS serviceseriesmapds = null;
    
    private ICategoryServiceMapDS categoryServiceMapDS=null;
    private ICntPlatformSyncDS cntPlatformSyncds = null;
    private ICntTargetSyncDS cntTargetSyncds = null;
    private IUsysConfigDS usysConfigDS = null;
    private IServiceSynLS servicesynls = null;
    
    public void setServicesynls(IServiceSynLS servicesynls) {
        this.servicesynls = servicesynls;
    }
    
    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setCategoryServiceMapDS(ICategoryServiceMapDS categoryServiceMapDS)
    {
        this.categoryServiceMapDS = categoryServiceMapDS;
    }
    // 构造方法//
    ServiceLS()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    }

    
    public void setCntPlatformSyncds(ICntPlatformSyncDS cntPlatformSyncds)
    {
        this.cntPlatformSyncds = cntPlatformSyncds;
    }

    public void setCntTargetSyncds(ICntTargetSyncDS cntTargetSyncds)
    {
        this.cntTargetSyncds = cntTargetSyncds;
    }

    /**
     * 
     * @param serviceschedulemapds IServiceSchedualMapDS
     */
    public void setServiceschedulemapds(IServiceSchedualMapDS serviceschedulemapds)
    {
        this.serviceschedulemapds = serviceschedulemapds;
    }

    /**
     * 
     * @param serviceprogrammapds IServiceProgramMapDS
     */
    public void setServiceprogrammapds(IServiceProgramMapDS serviceprogrammapds)
    {
        this.serviceprogrammapds = serviceprogrammapds;
    }

    /**
     * 
     * @param servicechannelmapds IServiceChannelMapDS
     */
    public void setServicechannelmapds(IServiceChannelMapDS servicechannelmapds)
    {
        this.servicechannelmapds = servicechannelmapds;
    }

    /**
     * 
     * @param serviceseriesmapds IServiceSeriesMapDS
     */
    public void setServiceseriesmapds(IServiceSeriesMapDS serviceseriesmapds)
    {
        this.serviceseriesmapds = serviceseriesmapds;
    }

    /**
     * 
     * @param serviceBufds IServiceBufDS
     */
    public void setServiceBufds(IServiceBufDS serviceBufds)
    {
        this.serviceBufds = serviceBufds;
    }

    /**
     * 
     * @param serviceupdateApply IServiceUpdateApply
     */
    public void setServiceupdateApply(IServiceUpdateApply serviceupdateApply)
    {
        this.serviceupdateApply = serviceupdateApply;
    }

    /**
     * 
     * @param cntSyncRecordDS ICntSyncRecordDS
     */
    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    /**
     * 
     * @return ICntSyncTaskDS
     */
    public ICntSyncTaskDS getCntSyncTaskDS()
    {
        return cntSyncTaskDS;
    }

    /**
     * 
     * @param targetSystemDS ITargetsystemDS
     */
    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    /**
     * 
     * @param servicewkfwhisds IServiceWkfwhisDS
     */
    public void setServicewkfwhisds(IServiceWkfwhisDS servicewkfwhisds)
    {
        this.servicewkfwhisds = servicewkfwhisds;
    }

    /**
     * 
     * @return IServiceDS
     */
    public IServiceDS getServiceds()
    {
        return serviceds;
    }

    /**
     * 
     * @param serviceds IServiceDS
     */
    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    /**
     * 
     * @param cntSyncTaskDS ICntSyncTaskDS
     */
    public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
    {
        this.cntSyncTaskDS = cntSyncTaskDS;
    }

   

    /**
     * 新增服务
     * 
     * @param service Service
     * @throws Exception Exception
     */

    public void insertService(Service service) throws Exception
    {
        log.debug("insert service starting...");
        try
        {
            serviceds.insertService(service);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理//
            throw new Exception(dsEx.getMessage());
        }
        log.debug("insert service end");
    }

    /**
     * 查询服务列表 参数为三个，调用四个参数的方法
     * 
     * @param service Service 查询的条件对象
     * @param start int起始行
     * @param pageSize  int每页显示行数
     * @return TableDataInfo返回查询的列表
     * @throws Exception Exception
     */ 

    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize) throws Exception
    {
        // 先进行时间格式的转换//
        log.debug("query servicelist starting...");
        service.setServiceid(EspecialCharMgt.conversion(service.getServiceid()));
        service.setServicename(EspecialCharMgt.conversion(service.getServicename()));
        String startCreateTime = service.getCreatetime();
        startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setCreatetime(startCreateTime);

        String endCreateTime = service.getEffecttime();
        endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setEffecttime(endCreateTime);

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("serviceindex");

        try
        {
            tableInfo = pageInfoQuery(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query service list ends");

        return tableInfo;
    }

    /**
     * 查询服务列表 参数为四个参数
     * 
     * @param service Service传入对象
     * @param start int起始行
     * @param pageSize int每页显示行数
     * @param puEntity PageUtilEntity默认排序
     * @return TableDataInfo查询得到的列表
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception
    {
        log.debug("get service page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = serviceds.pageInfoQuery(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理//
            throw new DomainServiceException(dsEx);
        }
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * @param service Service
     * @return Service
     * @throws Exception Exception
     */
    public Service getService(Service service) throws Exception
    {

        Service rsObj = null;
        try
        {
            rsObj = serviceds.getService(service);
        }
        catch (Exception dsEx)
        {
            throw new Exception(dsEx.getMessage());
        }
        log.debug("get serviceList by pk end");
        return rsObj;
    }

    /**
     * 审核页面展示的信息为修改后的信息
     * 
     * @param servicebuf ServiceBuf
     * @return ServiceBuf
     * @throws Exception Exception
     */ 
    public ServiceBuf getServiceBuf(ServiceBuf servicebuf) throws Exception
    {
        ServiceBuf rsObj = null;
        try
        {
            rsObj = serviceBufds.getServiceBuf(servicebuf);
        }
        catch (Exception dsEx)
        {
            throw new Exception(dsEx.getMessage());
        }
        log.debug("get serviceList by pk end");
        return rsObj;
    }

    /**
     * @param service Service
     * @throws Exception Exception
     * @return String
     * @throws Exception Exception
     */
    public String removeService(Service service) throws Exception
    {

        // 首先要删除的判断服务是否存在//
        Service serviceCheck = null;
        serviceCheck = serviceds.getService(service);
        if (serviceCheck == null)
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4DEL_DOESNOT_EXIST);
        }

        // 判断服务的状态是否允许删除 110待审核 130审核失败  0 待发布 //
        if (serviceCheck.getStatus() != 110
                && serviceCheck.getStatus() != 130
                && serviceCheck.getStatus() != 0 ) 
        {
            // 状态不正确，不允许删除//
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_4DEL_WRONG_STATUS); 
        }
        
        if (isServicePublished(serviceCheck))
        {
            // 目标系统发布状态不正确，不允许删除//
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_DEL_WRONG_PUB_STATUS); 
        }
        try
        {
            // 删除服务的相关关联关系的发布信息//
            removeServicePublishInfWithNoCheck(serviceCheck, ObjectType.SERVICE_TYPE);
            // 删除服务与点播（VOD）内容相关关联关系的发布信息//
            removeServicePublishInfWithNoCheck(serviceCheck, ObjectType.SERVICEPROGRAMMAP_TYPE);
            // 删除服务与点播（VOD）内容的打包关系//
            ServiceProgramMap serviceProgramMap = new ServiceProgramMap();
            serviceProgramMap.setServiceindex(serviceCheck.getServiceindex());
            serviceProgramMap.setServiceid(serviceCheck.getServiceid());
            List<ServiceProgramMap> serviceProgramMapList = serviceprogrammapds
                    .getServiceProgramMapByCond(serviceProgramMap);
            serviceprogrammapds.removeServiceProgramMapList(serviceProgramMapList);
            // 删除服务与连续剧内容相关关联关系的发布信息//
            removeServicePublishInfWithNoCheck(serviceCheck, ObjectType.SERVICESERIESMAP_TYPE);
            // 删除服务与连续剧内容的打包关系//
            ServiceSeriesMap serviceSeriesMap = new ServiceSeriesMap();
            serviceSeriesMap.setServiceindex(serviceCheck.getServiceindex());
            serviceSeriesMap.setServiceid(serviceCheck.getServiceid());
            List<ServiceSeriesMap> serviceSeriesMapList = serviceseriesmapds
                    .getServiceSeriesMapByCond(serviceSeriesMap);
            serviceseriesmapds.removeServiceSeriesMapList(serviceSeriesMapList);
            // 删除服务与直播内容相关关联关系的发布信息//
            removeServicePublishInfWithNoCheck(serviceCheck, ObjectType.SERVICECHANNELMAP_TYPE);
            // 删除服务与直播内容的打包关系//
            ServiceChannelMap serviceChannelMap = new ServiceChannelMap();
            serviceChannelMap.setServiceindex(serviceCheck.getServiceindex());
            serviceChannelMap.setServiceid(serviceCheck.getServiceid());
            List<ServiceChannelMap> serviceChannelMapList = servicechannelmapds
                    .getServiceChannelMapByCond(serviceChannelMap);
            servicechannelmapds.removeServiceChannelMapList(serviceChannelMapList);
            // 删除服务//
            serviceds.removeService(serviceCheck);
            // 删除工作流//
            ServiceWkfwhis serviceWkfwhis = new ServiceWkfwhis() ;
            serviceWkfwhis.setServiceindex(serviceCheck.getServiceindex());
            serviceWkfwhis.setServiceid(serviceCheck.getServiceid());
            List<ServiceWkfwhis> serviceWkfwhisList = servicewkfwhisds.getServiceWkfwhisByCond(serviceWkfwhis);
            servicewkfwhisds.removeServiceWkfwhisList(serviceWkfwhisList);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        // 写操作日志//
        CommonLogUtil.insertOperatorLog(serviceCheck.getServiceid(), CommonLogConstant.MGTTYPE_SERVICE,
                CommonLogConstant.OPERTYPE_SERVICE_DEL, CommonLogConstant.OPERTYPE_SERVICE_DEL_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_DEL_SUCCESSFUL);

    }

    /** 删除服务在发布平台和发布网元上 对应关联对象类型的发布信息。
     * 
     * @param service
     * @param objecttype
     * @return
     * @throws DomainServiceException */
    private boolean removeServicePublishInfWithNoCheck(Service service, int objecttype) throws DomainServiceException
    {
        List<CntPlatformSync> servicePlatformList = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();

        cntPlatformSync.setObjecttype(objecttype);
        if (objecttype == ObjectType.SERVICE_TYPE)
        {
            cntPlatformSync.setObjectindex(service.getServiceindex());
            cntPlatformSync.setObjectid(service.getServiceid());
        }
        else if ((objecttype == ObjectType.SERVICECHANNELMAP_TYPE) || (objecttype == ObjectType.SERVICESERIESMAP_TYPE)
                || (objecttype == ObjectType.SERVICEPROGRAMMAP_TYPE))
        {
            cntPlatformSync.setParentid(service.getServiceid());
        }
        else
        {
            //输入类型不正确就返回错误，如果将此方法应用到其他模块需要将此判断删除//
            return false;
        }

        servicePlatformList = cntPlatformSyncds.getCntPlatformSyncByCond(cntPlatformSync);
        // 删除服务关联平台//
        for (int i = 0; i < servicePlatformList.size(); i++)
        {
            cntPlatformSync = servicePlatformList.get(i);
            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setRelateindex(cntPlatformSync.getSyncindex());
            cntTargetSync.setObjectid(cntPlatformSync.getObjectid());
            List<CntTargetSync> cntTargetSyncList = cntTargetSyncds.getcntSynnormaltargetListBycond(cntTargetSync);
            cntTargetSyncds.removeCntTargetSyncList(cntTargetSyncList);            
            cntPlatformSyncds.removeCntPlatformSync(cntPlatformSync);
        }
        return true;
    }

    /** 判断该服务是否已经关联平台并处于发布状态（发布成功、修改发布失败，发布中）,如果有返回true，如果没有就返回false
     * 
     * @param service
     * @return
     * @throws DomainServiceException */
    private boolean isServicePublished(Service service) throws DomainServiceException
    {
        if (service.getStatus() != 0)
        {
            return false;
        }

        List<CntPlatformSync> servicePlatformList = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        List<CntTargetSync> serviceTargetList = null;
        // 获取服务的网元发布状态，如果在发布状态就返回无法删除//
        cntPlatformSync.setObjecttype(1);
        cntPlatformSync.setObjectindex(service.getServiceindex());
        cntPlatformSync.setObjectid(service.getServiceid());
        servicePlatformList = cntPlatformSyncds.getCntPlatformSyncByCond(cntPlatformSync);
        for (int i = 0; i < servicePlatformList.size(); i++)
        {
            cntPlatformSync = servicePlatformList.get(i);
            if (cntPlatformSync.getStatus() != 0 && cntPlatformSync.getStatus() != 400)
            {
                return true;
            }
            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setObjecttype(1);
            cntTargetSync.setObjectindex(service.getServiceindex());
            cntTargetSync.setObjectid(service.getServiceid());
            serviceTargetList = cntTargetSyncds.getcntSynnormaltargetListBycond(cntTargetSync);
            for (int j = 0; j < serviceTargetList.size(); j++)
            {
                cntTargetSync = serviceTargetList.get(j);
                if (cntTargetSync.getStatus() != 0 && cntTargetSync.getStatus() != 400)
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * 新增服务
     * @param service Service
     * @return String
     * @throws Exception Exception
     */
    public String addService(Service service) throws Exception
    {
        // 判断是否有重复的名称//
        Service serviceCheckName = new Service();
        serviceCheckName.setServicename(service.getServicename());
        List<Service> serviceNameList = serviceds.getServiceByCond(serviceCheckName);
        if (serviceNameList.size() > 0)
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_NAME_EXISTS);
        }
        
        String isServiceAudit = null;
        UsysConfig usysConfig = new UsysConfig();
        // 获取服务是否审核配置
        log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
        usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.service.noaudit.switch");
        log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
        if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().equals("")))
        {
            return "1:获取服务是否审核配置参数失败";
        }
        else
        {
            isServiceAudit = usysConfig.getCfgvalue();
        }
        
        // 不免审时初始状态为110-待审核，免审为0-审核通过
        if(isServiceAudit.equals("0"))
        {
            service.setStatus(0);//0-审核通过
        }
        else
        {
            service.setStatus(110);//110-待审核
        }
        
        try
        {
            serviceds.insertService(service);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        // 写日志//
        CommonLogUtil.insertOperatorLog(service.getServiceid(), CommonLogConstant.MGTTYPE_SERVICE,
                CommonLogConstant.OPERTYPE_SERVICE_ADD, CommonLogConstant.OPERTYPE_SERVICE_ADD_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_ADD_SUCCESSFUL);
    }
    
    /**
     * // 修改服务,分为一般修改以及做工作流的修改，根据所在状态判断下一步流程
     * @param service Service 
     * @return String
     * @throws Exception Exception
     */
    public String updateService(Service service) throws Exception
    {
        try
        {            
        // 判断修改的服务是否存在//
            Service serviceCheck = serviceds.getService(service);
            if (serviceCheck == null)
            {
                return "1:"+ ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);
            }
            int serviceStatus = serviceCheck.getStatus();
            
            // 判断修改后的名称是否有重复//
            Service serviceNameCheck = new Service();
            serviceNameCheck.setServicename(service.getServicename());
            List<Service> serviceNameList = serviceds.getServiceByCond(serviceNameCheck);
            int serviceSameNameSize = serviceNameList.size();
            if (serviceSameNameSize > 0)
            {
                for (int i = 0; i < serviceSameNameSize; i++)
                {
                    if (service.getServiceindex().longValue() != serviceNameList.get(i).getServiceindex().longValue())
                    {
                        return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_NAME_EXISTS);
                    }
                }
            }
            
            String isServiceAudit = null;
            UsysConfig usysConfig = new UsysConfig();
            // 获取服务是否审核配置
            log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.service.noaudit.switch");
            log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().equals("")))
            {
                return "1:获取服务是否审核配置参数失败";
            }
            else
            {
                isServiceAudit = usysConfig.getCfgvalue();
            }
            
            // 直接走修改流程//
            if (serviceStatus == 110 || serviceStatus == 130)
            {
                if(isServiceAudit.equals("0"))
                {
                    service.setStatus(0);
                }
                
                serviceds.updateService(service);
                
                // 写操作日志//
                CommonLogUtil.insertOperatorLog(service.getServiceid(), CommonLogConstant.MGTTYPE_SERVICE,
                        CommonLogConstant.OPERTYPE_SERVICE_UPDATE, CommonLogConstant.OPERTYPE_SERVICE_UPDATE_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
    
                return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_UPDATE_SUCCESSFUL);
            }
            else if (serviceStatus == 0)
            {
                //获取服务发布状态//
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjecttype(1);
                cntTargetSync.setObjectid(service.getServiceid());
                cntTargetSync.setObjectindex(service.getServiceindex());
                List<CntTargetSync> cntTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(cntTargetSync);
                
                if(cntTargetSyncList.size() > 0 )
                {
                    for (int i = 0; i < cntTargetSyncList.size(); i++)
                    {
                        if(cntTargetSyncList.get(i).getStatus() == 200)
                        {
                            // 服务在发布中，不能修改 //
                            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_UPDATE_WRONG_STATUS);
                        }
                    }
                }
                
                if(isServiceAudit.equals("0"))
                {
                    serviceds.updateService(service);
                    servicesynls.modPublishSyn(serviceCheck.getServiceid());
                    return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_UPDATE_SUCCESSFUL);
                }
                
                return serviceupdateApply.apply(service);
            }
            else 
            {         
             // 服务状态不正确，不能修改//
                return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_UPDATE_WRONG_STATUS);
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            log.error("updateService exception:" + e);
            throw new Exception(e.getMessage());
        }
    }
   
    /**
     * // 查询历史表
     * @param serviceWkfwhis ServiceWkfwhis
     * @return ServiceWkfwhis
     * @throws Exception Exception
     */
    public ServiceWkfwhis getServiceWkfwHis(ServiceWkfwhis serviceWkfwhis) throws Exception
    {
        log.debug("get serviceWkfwHis by workflwoindex and serviceindex and nodeid starting....");
        List<ServiceWkfwhis> rsList = null;
        try
        {
            rsList = servicewkfwhisds.getServiceWkfwhisByCond(serviceWkfwhis);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());

        }

        if (rsList != null && rsList.size() > 0)
        {
            log.debug("get serviceWkfwHis by workflwoindex and serviceindex and nodeid ends");
            return rsList.get(0);
        }
        return null;
    }

    /**
     * // 服务的发布操作,批量发布
     * @param serviceIndexAll String
     * @return String
     * @throws Exception Exception
     */
    
    public String servicePublish(String serviceIndexAll) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; 
        Integer iSuccessed = 0;
        int index = 1;
      //需要发布的所有内容的serviceindex存放到数组indexArray中//
        String indexArray[] = serviceIndexAll.split(":");
        List<Service> serviceList = new ArrayList<Service>();
      //判断是否选中了改服务的//
        if (indexArray.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RESOURCE_UCDN_SERVICE_PUB_NO_SERVICE_CHOSEN)); 
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; 
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; 
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        else
        {
          //按照serviceindex找出选中的服务对象//
            for (int i = 0; i < indexArray.length; i++)
            {
                Service serviceArr = new Service();
                serviceArr.setServiceindex(Long.valueOf(indexArray[i]));
                serviceArr = serviceds.getService(serviceArr);
                //存放选中的所有服务对象//
                serviceList.add(serviceArr);
            }

        }
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
      //获取选中的服务数量//
        int serviceSize = serviceList.size();

        // 获取目标系统的index//
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
         // 没有bms目标系统//
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); 
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; 
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; 
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 没有EPG目标系统
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        Service service = new Service();
        String tempAreaPath = getTempAreAdd();//获取临时区地址
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 获取临时区地址错误
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        try
        {
            for (int i = 0; i < serviceSize; i++)//对选中的每个服务进行发布操作
            {
                service = serviceds.getService(serviceList.get(i));

                if (service == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), serviceList.get(i).getServiceindex()
                            .toString(), ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                            ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS)); // 服务不存在
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {

                    if (service.getStatus() != 0 // 待发布
                            && service.getStatus() != 30 // 新增同步失败
                            && service.getStatus() != 80 // 取消同步成功
                            && service.getStatus() != 90 // 取消同步失败
                            && service.getStatus() != 60) //修改同步失败
                    	    
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_WRONG_STATUS)); // 服务当前状态不能发布
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    // 状态正常，可以发布//
                    {

                        // 判断服务状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录//
                        if (service.getStatus() == 0 || service.getStatus() == 80)
                        {
                            // 生成服务xml，返回生成文件路径//
                            String fileName = createServiceXml(service, tempAreaPath, "publishService"); // 发布//

                            // 获取任务流水号和流水批次号//
                            String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                    .toString();
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTask = new CntSyncTask();
                            cntsyncTask.setStatus(1);//该状态为待发布//
                            cntsyncTask.setPriority(2);
                            
                            cntsyncTask.setCorrelateid(correlateid);
                            cntsyncTask.setContentmngxmlurl(fileName);
   
                            cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 设置目标系统index//
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                            // 查EPG的任务
                            correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                            cntsyncTaskEPG.setStatus(1);
                            cntsyncTaskEPG.setPriority(2);

                            cntsyncTaskEPG.setCorrelateid(correlateid);
                            cntsyncTaskEPG.setContentmngxmlurl(fileName);
       
                            cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index//
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                            service.setStatus(10);

                        }
                        else if(service.getStatus() == 60)  //修改同步失败//
                        {
                        	 // 生成服务xml，返回生成文件路径//
                            String fileName = createServiceXml(service, tempAreaPath, "publishService"); // 发布//

                            // 获取任务流水号和流水批次号//
                            String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                    .toString();
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTask = new CntSyncTask();
                            cntsyncTask.setStatus(1);
                            cntsyncTask.setPriority(2);
                            
                            cntsyncTask.setCorrelateid(correlateid);
                            cntsyncTask.setContentmngxmlurl(fileName);
                           
                            cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                            // 查EPG的任务
                            correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                            cntsyncTaskEPG.setStatus(1);
                            cntsyncTaskEPG.setPriority(2);
                           
                            cntsyncTaskEPG.setCorrelateid(correlateid);
                            cntsyncTaskEPG.setContentmngxmlurl(fileName);
                           
                            cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                            service.setStatus(40);  //修改同步中

                        }
                        else
                        // 新增同步失败 或者取消同步失败
                        {
                            // 查询分发记录表
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecord.setObjindex(service.getServiceindex());
                            cntSyncRecord.setObjtype(1);
                            cntSyncRecord.setDesttype(1);
                            List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);
                            int bmsSize = cntSyncRecordList.size();
                            // 是否存在EPG
                            CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecordepg.setObjindex(service.getServiceindex());
                            cntSyncRecordepg.setObjtype(1);
                            cntSyncRecordepg.setDesttype(2); // EPG
                            List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecordepg);
                            int epgSize = cntSyncRecordepgList.size();
                            // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                            if ((bmsSize > 0) && (epgSize > 0))
                            {
                                service.setStatus(20); // 新增同步成功
                            }
                            else if ((0 == bmsSize) && (epgSize > 0))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bms = createServiceXml(service, tempAreaPath, "publishService"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bms);
                                
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                service.setStatus(10); // 新增同步中
                            }
                            else if ((bmsSize > 0) && (0 == epgSize))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4epg = createServiceXml(service, tempAreaPath, "publishService"); // 发布
                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4epg);
                               
                                cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                service.setStatus(10); // 新增同步中
                            }
                            else
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bmsEpg = createServiceXml(service, tempAreaPath, "publishService"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 查EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                               
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                service.setStatus(10);// 新增同步中

                            }
                        }
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(service.getServiceid(), CommonLogConstant.MGTTYPE_SERVICE,
                                CommonLogConstant.OPERTYPE_SERVICE_PUBLISH,
                                CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                        returnInfo.appendOperateInfo(operateInfo);

                        serviceds.updateService(service); // 更新服务的状态

                    }
                }
            }

        }
        catch (Exception e)
        {
            log.error(e);
            throw e;
        }

        if (serviceSize == 0 || ifail == serviceSize)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < serviceSize)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
                                                                                                                // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publishColumn end...");
        return returnInfo.toString();

    }

    private String getTempAreAdd() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1");// 临时区
        return stroageareaPath;
    }

    private static final String XMLADDR = "xmlsync"; // 所有的xml文件都放在临时区的该目录下

    // 生成服务xml,返回生成文件全路径。
    private String createServiceXml(Service service, String tempAreaPath, String type)// tempAreaPath
            // /NAS/临时区
            throws Exception
    {
        log.debug("create service xml starting...");

        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint();// CMS
        String fileName = date.getTime() + "_service.xml";// 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date());

        String tempFilePath = tempAreaPath + File.separator + XMLADDR + File.separator + fileFolder4xml
                + File.separator + "service" + File.separator;// xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName;// synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element objectElement = null;
        Element propertyElement = null;
        String actionType = null; // 操作类型

        objectElement = objectsElement.addElement("Object");

        if (type != null && type.equals("publishService")) // 如果操作类型为新增发布或者修改发布
        {
            actionType = "REGIST or UPDATE";
        }
        else
        {
            actionType = "DELETE"; // 类型为取消发布
        }
        objectElement.addAttribute("ElementType", "Service");
        objectElement.addAttribute("ServiceId", service.getServiceid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Name");
        propertyElement.addCDATA(service.getServicename());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FeeType");
        propertyElement.addText(service.getFeetype().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FeeCode");
        propertyElement.addText(service.getFeecode().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FixedFee");
        propertyElement.addText(service.getFixedfee().toString());

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {
                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 查询可以用来发布的服务
     * 
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */

    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize) throws Exception
    {
        // 先进行时间格式的转换
        log.debug("query servicelist starting...");

        service.setServiceid(EspecialCharMgt.conversion(service.getServiceid()));
        service.setServicename(EspecialCharMgt.conversion(service.getServicename()));
        String startCreateTime = service.getCreatetime();
        startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setCreatetime(startCreateTime);

        String endCreateTime = service.getEffecttime();
        endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setEffecttime(endCreateTime);

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("serviceindex");

        try
        {
            tableInfo = pageInfoQueryPublish(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query service list ends");

        return tableInfo;
    }

    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception
    {
        log.debug("get service page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = serviceds.pageInfoQueryPublish(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 查询可以取消发布的操作
     * 
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize) throws Exception
    {
        // 先进行时间格式的转换//
        log.debug("query servicelist starting...");

        service.setServiceid(EspecialCharMgt.conversion(service.getServiceid()));
        service.setServicename(EspecialCharMgt.conversion(service.getServicename()));
        String startCreateTime = service.getCreatetime();
        startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setCreatetime(startCreateTime);

        String endCreateTime = service.getEffecttime();
        endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
        service.setEffecttime(endCreateTime);

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("serviceindex");

        try
        {
            tableInfo = pageInfoQueryDelPublish(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query service list ends");

        return tableInfo;
    }

    /**
     * 查询可以取消发布的服务
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception
    {
        log.debug("get service page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = serviceds.pageInfoQueryDelPublish(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 服务取消发布操作，取消新增发布
     * 
     * @param serviceIndexAll 页面选择的所有希望取消发布的服务的index拼接而成的字符串，以“：”分隔
     * @return
     * @throws Exception
     */
    // 服务的取消发布操作,批量取消发布//
    public String serviceDelPublish(String serviceIndexAll) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 服务发布失败个数//
        Integer iSuccessed = 0;
        int index = 1;

        String indexArray[] = serviceIndexAll.split(":");
        List<Service> serviceList = new ArrayList<Service>();
        if (indexArray.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_SERVICE_CHOSEN)); // 没有服务被选中//
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："//
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        else
        {
            for (int i = 0; i < indexArray.length; i++)
            {
                Service serviceArr = new Service();
                serviceArr.setServiceindex(Long.valueOf(indexArray[i]));
                serviceArr = serviceds.getService(serviceArr);
                serviceList.add(serviceArr);
            }

        }
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
        int serviceSize = serviceList.size();

        // 获取目标系统的index
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); // 没有bms目标系统//
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："//
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }
        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 没有EPG目标系统//
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："//
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        Service service = new Service();

        String tempAreaPath = getTempAreAdd();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 没有绑定关系被选中//
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："//
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        try
        {
            for (int i = 0; i < serviceSize; i++)
            {
                service = serviceds.getService(serviceList.get(i));

                if (service == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS)); // 服务不存在//
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    // 先判断该服务下面是否有打包的内容，查询四张map表，//
                    ServiceProgramMap servPrgMap = new ServiceProgramMap();
                    servPrgMap.setServiceindex(service.getServiceindex());
                    List<ServiceProgramMap> progList = serviceprogrammapds.getServiceProgramMapByCond(servPrgMap);
                    if (progList.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CNT)); // 服务存在打包内容//
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    ServiceSeriesMap servSeriesMap = new ServiceSeriesMap();
                    servSeriesMap.setServiceindex(service.getServiceindex());
                    List<ServiceSeriesMap> seriesList = serviceseriesmapds.getServiceSeriesMapByCond(servSeriesMap);
                    if (seriesList.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CNT)); // 服务存在打包内容//
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    ServiceSchedualMap servScheMap = new ServiceSchedualMap();
                    servScheMap.setServiceindex(service.getServiceindex());
                    List<ServiceSchedualMap> schedualList = serviceschedulemapds
                            .getServiceSchedualMapByCond(servScheMap);
                    if (schedualList.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CNT)); // 服务存在打包内容//
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    ServiceChannelMap servChannMap = new ServiceChannelMap();
                    servChannMap.setServiceindex(service.getServiceindex());
                    List<ServiceChannelMap> channList = servicechannelmapds.getServiceChannelMapByCond(servChannMap);
                    if (channList.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CNT)); // 服务存在打包内容//
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    
                    CategoryServiceMap categoryServiceMap=new CategoryServiceMap();
                    categoryServiceMap.setServiceindex(service.getServiceindex());
                    List list=categoryServiceMapDS.getCategoryServiceMapByCond(categoryServiceMap);
                    if (list!=null&&list.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_DEL_BIND_CATEGORY)); // 服务关联了栏目//
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    
                    // 判断服务下面关联的内容是否已经发布如果已经发布，则不能取消发布//
                    // 判断是否有海报关联关系//
                    List<Picture> servPicMapList = pictureds.getPictureByServiceIndex(service.getServiceindex());
                    int bindPicSize = servPicMapList.size();
                    // 判断每一个海报是否已经发布//
                    int isPicPub = 0;
                    if (bindPicSize > 0)
                    {
                        for (int picidx = 0; picidx < bindPicSize; picidx++)
                        {
                            if (servPicMapList.get(picidx).getStatus() == 10 ||servPicMapList.get(picidx).getStatus() == 20 ||servPicMapList.get(picidx).getStatus() == 40 || servPicMapList.get(picidx).getStatus() == 50
                                    || servPicMapList.get(picidx).getStatus() == 60||servPicMapList.get(picidx).getStatus() == 70 ||servPicMapList.get(picidx).getStatus() == 90)
                            {
                                isPicPub = 1;
                                break;
                            }
                        }
                    }
                    if (isPicPub == 1)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_DEL_PUB_PIC_BIND_PUBLISHED)); // 该服务关联的海报已经发布，不能对服务取消发布
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 判断状态是否可以取消发布
                    if (service.getStatus() != 20// 新增同步成功
                            && service.getStatus() != 30// 新增同步失败
                            && service.getStatus() != 50// 修改同步成功
                            && service.getStatus() != 60// 修改同步失败
                            && service.getStatus() != 90)// 取消同步失败

                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_WRONG_STATUS)); // 服务当前状态不能取消发布
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    // 状态正常，可以取消发布//
                    {

                        // 判断服务状态，下面的状态向分发任务表中插入两条记录//
                        if (service.getStatus() == 20 || service.getStatus() == 50 || service.getStatus() == 60)
                        {
                            // 生成服务xml，返回生成文件路径//
                            String fileName = createServiceXml(service, tempAreaPath, "publishDelService"); // 发布

                            // 获取任务流水号和流水批次号//
                            String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                    .toString();
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTask = new CntSyncTask();
                            cntsyncTask.setStatus(1);
                            cntsyncTask.setPriority(2);
                            
                            cntsyncTask.setCorrelateid(correlateid);
                            cntsyncTask.setContentmngxmlurl(fileName);
                            
                            cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index//
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                            // 查EPG的任务//
                            correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                            cntsyncTaskEPG.setStatus(1);
                            cntsyncTaskEPG.setPriority(2);
                           
                            cntsyncTaskEPG.setCorrelateid(correlateid);
                            cntsyncTaskEPG.setContentmngxmlurl(fileName);
                           
                            cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index//
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                            service.setStatus(70); // 取消同步中//

                        }
                        else
                        // 新增同步失败 或者取消同步失败//
                        {

                            // 查询分发记录表//
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            // 是否存在BMS记录//
                            cntSyncRecord.setObjindex(service.getServiceindex());
                            cntSyncRecord.setObjtype(1);
                            cntSyncRecord.setDesttype(1);

                            List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);
                            int bmsSize = cntSyncRecordList.size();
                            // 是否存在EPG//
                            CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                            // 是否存在BMS记录//
                            cntSyncRecordepg.setObjindex(service.getServiceindex());
                            cntSyncRecordepg.setObjtype(1);
                            cntSyncRecordepg.setDesttype(2); // EPG
                            List<CntSyncRecord> cntSyncRecordepglsit = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecordepg);
                            int epgSize = cntSyncRecordepglsit.size();
                            // 如果两个都不在记录表里面，则直接修改状态为取消同步成功//
                            if ((0 == bmsSize) && (0 == epgSize))
                            {
                                service.setStatus(80); // 取消同步成功//
                            }
                            else if ((0 == bmsSize) && (epgSize > 0))
                            {
                                // 生成服务xml，返回生成文件路径//
                                String fileName4bms = createServiceXml(service, tempAreaPath, "publishDelService"); // 发布//

                                // 获取任务流水号和流水批次号//
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                               
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bms);
                               
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                service.setStatus(70); // 取消同步中//
                            }
                            else if ((bmsSize > 0) && (0 == epgSize))
                            {
                                // 生成服务xml，返回生成文件路径//
                                String fileName4epg = createServiceXml(service, tempAreaPath, "publishDelService"); // 发布//
                                // 获取任务流水号和流水批次号//
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4epg);
                                
                                cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index//
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                service.setStatus(70); // 取消同步中//
                            }
                            else
                            {
                                // 生成服务xml，返回生成文件路径//
                                String fileName4bmsEpg = createServiceXml(service, tempAreaPath, "publishService"); // 发布//

                                // 获取任务流水号和流水批次号//
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                               
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index//
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 查EPG的任务//
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                               
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                               
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index//
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                service.setStatus(70);// 取消同步中//
                                // service.setPublishtime(dateFormat.format(new Date()));
                            }

                            // 写操作日志//
                            CommonLogUtil.insertOperatorLog(service.getServiceid(), CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_PUBLISH,
                                    CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }

                        // 返回信息//
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_DEL_SUCCESS)); // 取消发布成功//
                        returnInfo.appendOperateInfo(operateInfo);

                        serviceds.updateService(service); // 更新服务的状态//

                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error(e);
            throw e;
        }

        if (ifail == serviceSize)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < serviceSize)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
                                                                                                                // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publish delColumn end...");
        return returnInfo.toString();

    }

    /**
     * 修改工作流审核通过后，生成发布任务
     * 
     * @param serviceIndex
     * @throws Exception
     */
    public void serviceUpdatePublish(Long serviceIndex) throws Exception
    {
        Service servicepub = new Service();
        servicepub.setServiceindex(serviceIndex);
        try
        {
            servicepub = serviceds.getService(servicepub);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        String tempAreaPath = getTempAreAdd();
        // 生成服务xml，返回生成文件路径//
        String fileName = createServiceXml(servicepub, tempAreaPath, "publishService"); // 发布

        // 获取目标系统的index//
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        targetSystembms = listtsystembms.get(0);

        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        targetSystemepg = listtsystemepg.get(0);

        // 获取任务流水号和流水批次号//
        String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString();
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setStatus(1);
        cntsyncTask.setPriority(2);
        
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(fileName);
        
        cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index//
        cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

        // 查EPG的任务//
        correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTaskEPG = new CntSyncTask();
        cntsyncTaskEPG.setStatus(1);
        cntsyncTaskEPG.setPriority(2);
        
        cntsyncTaskEPG.setCorrelateid(correlateid);
        cntsyncTaskEPG.setContentmngxmlurl(fileName);
       
        cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index//
        cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

    }

    public void setPictureds(IPictureDS pictureds)
    {
        this.pictureds = pictureds;
    }

    public String insertTargetBindMap(List<Targetsystem> targetsystemList, List<Service> serviceList) throws Exception
    {
        try
        {            
            // 用于返回的信息//
            ReturnInfo returnInfo = new ReturnInfo();
            OperateInfo operateInfo = null;
            int totalServiceindex = 0;//有效的输入服务数目//
            String addSuccessTargetSystem = "";
            
            boolean isAddTargetSystem = false; // 当前服务添加网元目标系统是否成功//
            int successServiceNumber = 0; // 添加目标系统成功的服务数量//
            int failServiceNumber = 0; // 添加目标系统失败的服务数量//
    
            log.debug("bind Services and TargetSystems start...");
            Service service = new Service(); // 当前服务Service//
            int serviceStatus = 0; // 当前服务状态//
            Targetsystem targetsystem = new Targetsystem(); // 当前目标系统targetsystem//
    
            CntTargetSync exitCntTargetSync = new CntTargetSync();// 用于检测关联是否存在的目标系统关联//
    
            for (int i = 0; i < serviceList.size(); i++)
            {
                service = serviceList.get(i);// 获取输入的service//
                if (service.getServiceindex() == null)
                {
                    continue;// 如果服务不存在就跳过本次循环//
                }
                totalServiceindex++;
                service = getService(service); 
                
                //检测服务是否被删除//
                if (service == null )
                {
                    failServiceNumber++;
                    operateInfo = new OperateInfo(totalServiceindex + "", serviceList.get(i).getServiceid(), ResourceMgt
                          .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_FAILED),
                          ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_DOESNOT_EXIST));
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 如果服务不存在就跳过本次循环//
                }
                
                isAddTargetSystem = false;// 当前没有添加服务//
                exitCntTargetSync.setObjectindex(service.getServiceindex());
                // 获取服务状态并检测//
                serviceStatus = service.getStatus();
                // 可以进行目标系统操作的状态是待发布0，发布失败300，发布成功400，修改发布失败500//
                if (serviceStatus == 0 || serviceStatus == 300 || serviceStatus == 400 || serviceStatus == 500)
                {
                    // 开始循环读取目标系统//
                    addSuccessTargetSystem = "";
                    for (int j = 0; j < targetsystemList.size(); j++)
                    {
                        targetsystem = targetsystemList.get(j);
                        targetsystem = targetSystemDS.getTargetsystem(targetsystem);
                        if (targetsystem == null || targetsystem.getTargetindex() == null)
                        {
                            addSuccessTargetSystem = addSuccessTargetSystem + "目标系统" + targetsystem.getTargetid()
                                    + "不存在,不能关联     ";
                            continue;// 如果目标系统不存在或者不正常就跳过本次循环//
                        }
                        if (targetsystem.getStatus() != 0)
                        {
                            addSuccessTargetSystem = addSuccessTargetSystem + "目标系统" + targetsystem.getTargetid()
                                    + "状态不正确,不能关联     ";
                            continue;// 如果目标系统不存在或者不正常就跳过本次循环//
                        }
                        // 检测当前目标系统与当前服务是否存在
                        exitCntTargetSync.setTargetindex(targetsystem.getTargetindex());
                        exitCntTargetSync.setObjecttype(1);
                        int exitBindsNumber = cntTargetSyncds.countCntTargetSync4Object(exitCntTargetSync);
    
                        if (exitBindsNumber < 1)
                        {
                            //尝试添加当前目标系统信息//
                            try
                            {
                                // 向发布总状态表中添加信息//
                                CntPlatformSync writeCntPlatformSync = new CntPlatformSync();
                                writeCntPlatformSync.setObjecttype(1);// 对象类型，1 服务//
                                writeCntPlatformSync.setObjectindex(service.getServiceindex());// 对象index，服务的index//
                                writeCntPlatformSync.setObjectid(service.getServiceid());// 对象编码，服务的id//
                                writeCntPlatformSync.setPlatform(targetsystem.getPlatform());// 目标系统平台类型//
                                writeCntPlatformSync.setStatus(0);// 发布总状态默认为0//
                                // 判断总状态关联是否存在的(此处需要添加自己的方法)//
                                CntPlatformSync checkCntPlatformSync = cntPlatformSyncds
                                        .getCntPlatformSync4Object(writeCntPlatformSync);
                                if (checkCntPlatformSync == null)
                                {
                                    cntPlatformSyncds.insertCntPlatformSync(writeCntPlatformSync);
                                    writeCntPlatformSync = cntPlatformSyncds
                                            .getCntPlatformSync4Object(writeCntPlatformSync);
                                 // 写日志   添加了关联平台//
                                    CommonLogUtil.insertOperatorLog(service.getServiceid() + "," + targetsystem.getTargetid(),
                                            CommonLogConstant.MGTTYPE_SERVICE,
                                            CommonLogConstant.OPERTYPE_SERVICE_ADD_PLATFORM,
                                            CommonLogConstant.OPERTYPE_SERVICE_ADD_PLATFORM_INF,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                }
                                else
                                {
                                    writeCntPlatformSync = checkCntPlatformSync;
                                    writeCntPlatformSync.setStatus(0);
                                    cntPlatformSyncds.updateCntPlatformSync(writeCntPlatformSync);
                                }
                                // 向网元发布状态表中添加信息//
                                CntTargetSync writeCntTargetSync = new CntTargetSync();
                                writeCntTargetSync.setRelateindex(writeCntPlatformSync.getSyncindex());// 发布总状态index//
                                writeCntTargetSync.setObjecttype(1);// 对象类型，1 服务//
                                writeCntTargetSync.setObjectindex(service.getServiceindex());// 对象index，服务的index//
                                writeCntTargetSync.setObjectid(service.getServiceid());// 对象编码，服务的id//
                                writeCntTargetSync.setTargetindex(targetsystem.getTargetindex());// 目标系统的主键//
                                writeCntTargetSync.setStatus(0);// 发布总状态,默认待发布0//
                                writeCntTargetSync.setOperresult(0);// 操作结果，默认0//
                                cntTargetSyncds.insertCntTargetSync(writeCntTargetSync);
                                
                                addServiceProgramMapInAddTargetsystem(service, targetsystem);
                                addServiceChannelMapInAddTargetsystem(service, targetsystem);
                                addServiceSeriesMapInAddTargetsystem(service, targetsystem);
                                // 写日志   添加了关联目标系统//
                                CommonLogUtil.insertOperatorLog(service.getServiceid() + "," + targetsystem.getTargetid(),
                                        CommonLogConstant.MGTTYPE_SERVICE,
                                        CommonLogConstant.OPERTYPE_SERVICE_ADD_TARGETSYSTEM,
                                        CommonLogConstant.OPERTYPE_SERVICE_ADD_TARGETSYSTEM_INF,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
  
                                // 添加当前目标系统成功后
                                isAddTargetSystem = true;
                                addSuccessTargetSystem = addSuccessTargetSystem + "目标系统" + targetsystem.getTargetid()
                                        + "关联成功     ";
                            }
                            catch (Exception e)
                            {
                                // TODO: handle exception
                            }
                            //添加当前目标系统信息失败。//
                        }
                        else
                        {
                            // 更新服务与打包内容的mapping与平台的关联。//
                            addServiceProgramMapInAddTargetsystem(service, targetsystem);
                            addServiceChannelMapInAddTargetsystem(service, targetsystem);
                            addServiceSeriesMapInAddTargetsystem(service, targetsystem);
                            // 当前服务与当前目标网元关联已经存在//
                            addSuccessTargetSystem = addSuccessTargetSystem + "目标系统" + targetsystem.getTargetid()
                                    + "已关联     ";
                        }
                        // 当前服务与当前目标网元关联操作结束。//
                    }
                    // 循环读取targetsystemMaps目标系统结束//
                    //当前服务添关联平台结果//
                    if (isAddTargetSystem)
                    {
                        successServiceNumber++;
                        operateInfo = new OperateInfo(totalServiceindex + "", service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_SUCC),
                                addSuccessTargetSystem);
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        failServiceNumber++;
                        operateInfo = new OperateInfo(totalServiceindex + "", service.getServiceid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_FAILED),
                                addSuccessTargetSystem);
    
                        returnInfo.appendOperateInfo(operateInfo);
                    }
    
                }
                else
                {
                    // 当前服务状态错误，不可以进行添加目标系统操作//
                    failServiceNumber++;
                    operateInfo = new OperateInfo(totalServiceindex + "", service.getServiceid(), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_ADD_FAILED), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_WRONG_STATUS));// 当前服务状态不可以进行添加关联平台操作
                    returnInfo.appendOperateInfo(operateInfo);
                }
    
            }
            
            // 循环读取服务serviceMaps结束//
            if (successServiceNumber < 1)
            {
                returnInfo.setFlag("1");
                returnInfo.setReturnMessage(
                        ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM)+ ": "
                        + failServiceNumber);
            }
            else if (failServiceNumber < 1 ) {
                returnInfo.setFlag("0");
                returnInfo.setReturnMessage(
                        ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": "
                        + successServiceNumber );
            }
            else
            {
                returnInfo.setFlag("0");
                returnInfo.setReturnMessage(
                        ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": "
                        + successServiceNumber + ";" + "  "
                        + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM)+ ": "
                        + failServiceNumber);
            }
    
            log.debug("bind Services and TargetSystems end...");
            return returnInfo.toString();
        }
        catch (Exception e)
        {
            // TODO: handle exception
            log.error("insertTargetBindMap exception:" + e);
            throw new Exception(e.getMessage());
        }
        
    }
    
    
    public void addServiceChannelMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception
    { 
      //检测绑定内容，添加绑定服务的关联关系（确定的服务service和网元targetsystem）//
        //读取关联内容//
        ServiceChannelMap serviceChannelMap = new ServiceChannelMap();
        serviceChannelMap.setServiceindex(service.getServiceindex());
        serviceChannelMap.setServiceid(service.getServiceid());
        List<ServiceChannelMap> serviceChannelMapList = servicechannelmapds
                .getServiceChannelMapByCond(serviceChannelMap);

        for (int i = 0; i < serviceChannelMapList.size(); i++)
        {
            serviceChannelMap = serviceChannelMapList.get(i);
            //查询对应的点播内容是否在此网元上发布//
            CntTargetSync channelTargetSync = new CntTargetSync();
            channelTargetSync.setObjecttype(5);
            channelTargetSync.setObjectid(serviceChannelMap.getChannelid());
            channelTargetSync.setObjectindex(serviceChannelMap.getChannelindex());
            channelTargetSync.setTargetindex(targetsystem.getTargetindex());
            List<CntTargetSync> channelTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(channelTargetSync);
            
          //如果有就添加//
            if (channelTargetSyncList.size()>0)
            {
                channelTargetSync =channelTargetSyncList.get(0);
              //检测关联是否存在//
                CntTargetSync serviceChannelTargetSync = new CntTargetSync();
                serviceChannelTargetSync.setObjecttype(23);
                serviceChannelTargetSync.setParentid(service.getServiceid());
                serviceChannelTargetSync.setElementid(serviceChannelMap.getChannelid());
                serviceChannelTargetSync.setObjectid(serviceChannelMap.getMappingid());
                serviceChannelTargetSync.setObjectindex(serviceChannelMap.getMapindex());
                serviceChannelTargetSync.setTargetindex(targetsystem.getTargetindex());
                
                List<CntTargetSync> serviceChannelTargetSyncList = cntTargetSyncds
                        .getCntTargetSyncByCond(serviceChannelTargetSync);
                if (serviceChannelTargetSyncList.size()<1)
                {
                  //检测总发布状态表//
                    CntPlatformSync serviceChannelPlatformSync = new CntPlatformSync();
                    serviceChannelPlatformSync.setObjecttype(23);
                    serviceChannelPlatformSync.setObjectid(serviceChannelMap.getMappingid());
                    serviceChannelPlatformSync.setObjectindex(serviceChannelMap.getMapindex());
                    serviceChannelPlatformSync.setPlatform(targetsystem.getPlatform());
                    serviceChannelPlatformSync.setParentid(serviceChannelMap.getServiceid());
                    serviceChannelPlatformSync.setElementid(serviceChannelMap.getChannelid());
                    List<CntPlatformSync> serviceChannelPlatformSyncList = cntPlatformSyncds
                            .getCntPlatformSyncByCond(serviceChannelPlatformSync);
                    if (serviceChannelPlatformSyncList.size()<1)
                    {
                        serviceChannelPlatformSync.setStatus(0);
                        cntPlatformSyncds.insertCntPlatformSync(serviceChannelPlatformSync);
                        serviceChannelPlatformSync = cntPlatformSyncds.getCntPlatformSyncByCond(
                                serviceChannelPlatformSync).get(0);
                        // 写日志  添加了新的关联平台//
                        CommonLogUtil.insertOperatorLog(serviceChannelMap.getMappingid() + ","
                                + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM_INF,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        serviceChannelPlatformSync = serviceChannelPlatformSyncList.get(0);
                        serviceChannelPlatformSync.setStatus(0);
                        cntPlatformSyncds.updateCntPlatformSync(serviceChannelPlatformSync);
                    }
                    
                    //向网元发布状态表写入新的关联信息//
                    serviceChannelTargetSync.setRelateindex(serviceChannelPlatformSync.getSyncindex());
                    serviceChannelTargetSync.setStatus(0);
                    serviceChannelTargetSync.setOperresult(0);
                    cntTargetSyncds.insertCntTargetSync(serviceChannelTargetSync);
                    // 写日志  添加了新的关联目标系统//
                    CommonLogUtil.insertOperatorLog(
                            serviceChannelMap.getMappingid() + "," + targetsystem.getTargetid(),
                            CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
           }
        }
    }
    
    public void addServiceSeriesMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception
    {    //检测绑定内容，添加绑定服务的关联关系（确定的服务service和网元targetsystem）//
        //读取关联内容//
        ServiceSeriesMap serviceSeriesMap = new ServiceSeriesMap();
        serviceSeriesMap.setServiceid(service.getServiceid());
        serviceSeriesMap.setServiceindex(service.getServiceindex());
        List<ServiceSeriesMap> serviceSeriesMapList = serviceseriesmapds.getServiceSeriesMapByCond(serviceSeriesMap);
        
        for (int i = 0; i < serviceSeriesMapList.size(); i++)
        {
            serviceSeriesMap = serviceSeriesMapList.get(i);
          //查询对应的点播内容是否在此网元上发布//
            CntTargetSync seriesTargetSync = new CntTargetSync();
            seriesTargetSync.setObjecttype(4);
            seriesTargetSync.setObjectindex(serviceSeriesMap.getSeriesindex());
            seriesTargetSync.setObjectid(serviceSeriesMap.getSeriesid());
            seriesTargetSync.setTargetindex(targetsystem.getTargetindex());
            List<CntTargetSync> seriesTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(seriesTargetSync);
            
            //如果有就添加//
            if (seriesTargetSyncList.size()>0)
            {
                seriesTargetSync = seriesTargetSyncList.get(0);
              //检测关联是否存在//
                CntTargetSync serviceSeriesTargetSync = new CntTargetSync();
                serviceSeriesTargetSync.setObjecttype(22);
                serviceSeriesTargetSync.setParentid(service.getServiceid());
                serviceSeriesTargetSync.setElementid(serviceSeriesMap.getSeriesid());
                serviceSeriesTargetSync.setObjectid(serviceSeriesMap.getMappingid());
                serviceSeriesTargetSync.setObjectindex(serviceSeriesMap.getMapindex());
                serviceSeriesTargetSync.setTargetindex(targetsystem.getTargetindex());
                
                List<CntTargetSync> serviceSeriesTargetSyncList = cntTargetSyncds
                        .getCntTargetSyncByCond(serviceSeriesTargetSync);
                if (serviceSeriesTargetSyncList.size()<1)
                {
                    //检测总发布状态表//
                    CntPlatformSync serviceSeriesPlatformSync = new CntPlatformSync();
                    serviceSeriesPlatformSync.setObjecttype(22);
                    serviceSeriesPlatformSync.setObjectid(serviceSeriesMap.getMappingid());
                    serviceSeriesPlatformSync.setObjectindex(serviceSeriesMap.getMapindex());
                    serviceSeriesPlatformSync.setPlatform(targetsystem.getPlatform());
                    serviceSeriesPlatformSync.setParentid(serviceSeriesMap.getServiceid());
                    serviceSeriesPlatformSync.setElementid(serviceSeriesMap.getSeriesid());
                    
                    List<CntPlatformSync> serviceSeriesPlatformSyncList = cntPlatformSyncds
                            .getCntPlatformSyncByCond(serviceSeriesPlatformSync);
                    if (serviceSeriesPlatformSyncList.size()<1)
                    {
                        serviceSeriesPlatformSync.setStatus(0);
                        cntPlatformSyncds.insertCntPlatformSync(serviceSeriesPlatformSync);
                        serviceSeriesPlatformSync = cntPlatformSyncds.getCntPlatformSyncByCond(
                                serviceSeriesPlatformSync).get(0);
                        // 写日志  添加了新的关联平台//
                        CommonLogUtil.insertOperatorLog(serviceSeriesMap.getMappingid() + ","
                                + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM_INF,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        serviceSeriesPlatformSync = serviceSeriesPlatformSyncList.get(0);
                        serviceSeriesPlatformSync.setStatus(0);
                        cntPlatformSyncds.updateCntPlatformSync(serviceSeriesPlatformSync);
                    }
                    
                  //向网元发布状态表写入新的关联信息//
                    serviceSeriesTargetSync.setRelateindex(serviceSeriesPlatformSync.getSyncindex());
                    serviceSeriesTargetSync.setStatus(0);
                    serviceSeriesTargetSync.setOperresult(0);
                    cntTargetSyncds.insertCntTargetSync(serviceSeriesTargetSync);
                    // 写日志  添加了新的关联目标系统//
                    CommonLogUtil.insertOperatorLog(serviceSeriesMap.getMappingid() + "," + targetsystem.getTargetid(),
                            CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
        }
        
    }
    
    public void addServiceProgramMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception
    {
        //检测绑定内容，添加绑定服务的关联关系（确定的服务service和网元targetsystem）//
        //读取关联内容//
        ServiceProgramMap serviceProgramMap = new ServiceProgramMap();
        serviceProgramMap.setServiceindex(service.getServiceindex());
        serviceProgramMap.setServiceid(service.getServiceid());
        List<ServiceProgramMap> serviceProgramMapList = serviceprogrammapds
                .getServiceProgramMapByCond(serviceProgramMap);
        
        for (int k = 0; k < serviceProgramMapList.size(); k++)
        {
            serviceProgramMap = serviceProgramMapList.get(k);
            //查询对应的点播内容是否在此网元上发布//
            CntTargetSync programTargetSync = new CntTargetSync();
            programTargetSync.setObjecttype(3);
            programTargetSync.setObjectindex(serviceProgramMap.getProgramindex());
            programTargetSync.setObjectid(serviceProgramMap.getProgramid());
            programTargetSync.setTargetindex(targetsystem.getTargetindex());
            List<CntTargetSync> programTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(programTargetSync);
            
            //如果有就添加//
            if (programTargetSyncList.size()>0)
            {
                programTargetSync = programTargetSyncList.get(0);
                //检测关联是否存在//
                CntTargetSync serviceProgramTargetSync = new CntTargetSync();
                serviceProgramTargetSync.setObjecttype(21);
                serviceProgramTargetSync.setParentid(service.getServiceid());
                serviceProgramTargetSync.setElementid(serviceProgramMap.getProgramid());
                serviceProgramTargetSync.setObjectid(serviceProgramMap.getMappingid());
                serviceProgramTargetSync.setObjectindex(serviceProgramMap.getMapindex());
                serviceProgramTargetSync.setTargetindex(targetsystem.getTargetindex());
                
                List<CntTargetSync> serviceProgramTargetSyncList = cntTargetSyncds
                        .getCntTargetSyncByCond(serviceProgramTargetSync);
                if (serviceProgramTargetSyncList.size()<1)
                {
                    //检测总发布状态表//
                    CntPlatformSync serviceProgramPlatformSync = new CntPlatformSync();
                    serviceProgramPlatformSync.setObjecttype(21);
                    serviceProgramPlatformSync.setObjectid(serviceProgramMap.getMappingid());
                    serviceProgramPlatformSync.setObjectindex(serviceProgramMap.getMapindex());
                    serviceProgramPlatformSync.setPlatform(targetsystem.getPlatform());
                    serviceProgramPlatformSync.setParentid(serviceProgramMap.getServiceid());
                    serviceProgramPlatformSync.setElementid(serviceProgramMap.getProgramid());
                    
                    List<CntPlatformSync> serviceProgramPlatformSyncList = cntPlatformSyncds
                            .getCntPlatformSyncByCond(serviceProgramPlatformSync);
                    if (serviceProgramPlatformSyncList.size()<1)
                    {
                        serviceProgramPlatformSync.setStatus(0);
                        cntPlatformSyncds.insertCntPlatformSync(serviceProgramPlatformSync);
                        serviceProgramPlatformSync = cntPlatformSyncds.getCntPlatformSyncByCond(
                                serviceProgramPlatformSync).get(0);
                        // 写日志  添加了新的关联平台//
                        CommonLogUtil.insertOperatorLog(serviceProgramMap.getMappingid() + ","
                                + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM,
                                CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM_INF,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        serviceProgramPlatformSync = serviceProgramPlatformSyncList.get(0);
                        serviceProgramPlatformSync.setStatus(0);
                        cntPlatformSyncds.updateCntPlatformSync(serviceProgramPlatformSync);
                    }                    
                    
                    //向网元发布状态表写入新的关联信息//
                    serviceProgramTargetSync.setRelateindex(serviceProgramPlatformSync.getSyncindex());
                    serviceProgramTargetSync.setStatus(0);
                    serviceProgramTargetSync.setOperresult(0);
                    cntTargetSyncds.insertCntTargetSync(serviceProgramTargetSync);                    
                    // 写日志 添加了新的关联目标系统//
                    CommonLogUtil.insertOperatorLog(
                            serviceProgramMap.getMappingid() + "," + targetsystem.getTargetid(),
                            CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
        }
    }

    /**
     * 删除服务已经关联的平台
     */
    public String removeTargetFromBind(Long serviceindex,Long targetindex)throws Exception
    {
        try
        {
            String rtnString = null;//返回信息//
            //通过服务索引获取服务的状态//
            //判断服务是否存在//
            Service service = new Service(); // 当前服务Service//
            service.setServiceindex(serviceindex);
            service=getService(service);        
           //服务不存在就返回服务错误信息//
            if (service == null || service.getServiceindex() == null)
            {
                rtnString = "1:"
                        + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);// 服务不存在或已经被删除;//
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            //查看网元是否存在(此处需要添加自己的方法)//
            Targetsystem targetsystem = new Targetsystem();
            targetsystem.setTargetindex(targetindex);
            targetsystem = targetSystemDS.getTargetsystem(targetsystem);        
            //网元不存在就返回服务错误信息//
            if (targetsystem == null || targetsystem.getTargetindex() == null)
            {
                rtnString = "1:"
                        + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_NO_TARGET_CHOSEN);// 网元不存在//
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            //判断服务在网元发布状态//
            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setObjecttype(1);
            cntTargetSync.setObjectindex(serviceindex);
            cntTargetSync.setTargetindex(targetsystem.getTargetindex());
            //获取网元状态(此处需要添加自己的方法)//
            List<CntTargetSync> cntTargetSyncMap = cntTargetSyncds.getCntTargetSyncByCond(cntTargetSync);//此处错误
            //正常情况下以上搜索出来的只含有一条网元关联信息//
            if (cntTargetSyncMap.size() < 1)
            {
             // 关联平台关系不存在，服务于网元平台没有建立关联，不可以进行删除操作//
                rtnString = "1:"
                        + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_NO_EXISTS);
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            cntTargetSync = cntTargetSyncMap.get(0);
            int nowStatus = cntTargetSync.getStatus();
            if (nowStatus == 200 || nowStatus == 300 || nowStatus == 500)
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_DEL_TARGET_WRONG_PUB_STATUS);// 服务已经在该平台发布
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            
            //查询服务与点播内容是否有在此网元中处于发布状态//
            if (isServiceContentBindPublished(targetsystem, service, ObjectType.SERVICEPROGRAMMAP_TYPE))
            {
                rtnString = "1:"
                        + ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_PROGRAM_PUBLISH);// 服务有绑定的内容在此网元平台上处于发布状态，不可以进行删除操作
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            //查询服务与点播内容是否有在此网元中处于发布状态//
            if (isServiceContentBindPublished(targetsystem, service, ObjectType.SERVICESERIESMAP_TYPE))
            {
                rtnString = "1:"
                        + ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_PROGRAM_PUBLISH);// 服务有绑定的内容在此网元平台上处于发布状态，不可以进行删除操作
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            //查询服务与点播内容是否有在此网元中处于发布状态//
            if (isServiceContentBindPublished(targetsystem, service, ObjectType.SERVICECHANNELMAP_TYPE))
            {
                rtnString = "1:"
                        + ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_DEL_FAIL_CHANNEL_PUBLISH);// 服务有绑定的内容在此网元平台上处于发布状态，不可以进行删除操作
                return rtnString;// 如果服务不存在就跳过本次循环//
            }
            
            //从网元操作表中删除//
            cntTargetSyncds.removeCntTargetSync(cntTargetSync);//此删除方法依赖于 WHERE SYNCINDEX = #syncindex#//
    
            // 写日志
            CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetsystem.getTargetid(),
                    CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_DEL_TARGETSYSTEM,
                    CommonLogConstant.OPERTYPE_SERVICE_DEL_TARGETSYSTEM_INF ,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            
            //删除服务与内容在此网元上的绑定关系//
            CntTargetSync exitContentBindInTarget = new CntTargetSync();
            exitContentBindInTarget.setParentid(service.getServiceid());
            exitContentBindInTarget.setTargetindex(targetindex);
            
            List<CntTargetSync> DelContentBindInTargetList = null ;
            CntTargetSync DelContentBindInTarget = new CntTargetSync();
            int i=0;
            //删除VOD内容，programBind 21//
            exitContentBindInTarget.setObjecttype(21);
            DelContentBindInTargetList = cntTargetSyncds.getCntTargetSyncByCond(exitContentBindInTarget);
            for(i=0; i<DelContentBindInTargetList.size();i++){
                DelContentBindInTarget = DelContentBindInTargetList.get(i);
                cntTargetSyncds.removeCntTargetSync(DelContentBindInTarget);
             // 写日志  删除了关联目标系统//
                CommonLogUtil.insertOperatorLog(DelContentBindInTarget.getObjectid() + ","
                        + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM_INF,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
            //删除连续剧内容，series 22//
            exitContentBindInTarget.setObjecttype(22);
            DelContentBindInTargetList = cntTargetSyncds.getCntTargetSyncByCond(exitContentBindInTarget);
            for(i=0; i<DelContentBindInTargetList.size();i++){
                DelContentBindInTarget = DelContentBindInTargetList.get(i);
                cntTargetSyncds.removeCntTargetSync(DelContentBindInTarget);
             // 写日志  删除了关联目标系统//
                CommonLogUtil.insertOperatorLog(DelContentBindInTarget.getObjectid() + ","
                        + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM_INF,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
            //删除直播内容，channel 23//
            exitContentBindInTarget.setObjecttype(23);
            DelContentBindInTargetList = cntTargetSyncds.getCntTargetSyncByCond(exitContentBindInTarget);
            for(i=0; i<DelContentBindInTargetList.size();i++){
                DelContentBindInTarget = DelContentBindInTargetList.get(i);
                cntTargetSyncds.removeCntTargetSync(DelContentBindInTarget);
             // 写日志  删除了关联目标系统//
                CommonLogUtil.insertOperatorLog(DelContentBindInTarget.getObjectid() + ","
                        + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM,
                        CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM_INF,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
            
            //从发布总状态表中删除(注意，此处不一定必须删除总状态表中的数据)//
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectid(service.getServiceid());
            cntPlatformSync.setObjectindex(serviceindex);
            cntPlatformSync.setObjecttype(1);
            cntPlatformSync.setPlatform(targetsystem.getPlatform());
            cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
            //查询相关平台下面还有没有同类的其他网元(此处需要添加自己的方法)  //      
            int targetNumber = cntPlatformSyncds.getCntPlatformSyncChildNumber(cntPlatformSync);
            if( targetNumber < 1 ){            
                cntPlatformSyncds.removeCntPlatformSync(cntPlatformSync);//此删除方法依赖于WHERE SYNCINDEX = #syncindex#//
                // 写日志   删除了关联平台//
                CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetsystem.getTargetid(),
                        CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_DEL_PLATFORM,
                        CommonLogConstant.OPERTYPE_SERVICE_DEL_PLATFORM_INF ,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
            }
            else
            {
                CntTargetSync childTagetSync = new CntTargetSync();
                childTagetSync.setRelateindex(cntTargetSync.getRelateindex());
                childTagetSync.setObjectid(service.getServiceid());
                List<CntTargetSync> childTagetList = cntTargetSyncds.getCntTargetSyncByCond(childTagetSync);//获取子网元状态//
                int[] targetSyncStatus =  new int[childTagetList.size()];
                for (int j = 0; j < childTagetList.size(); j++)
                {
                    targetSyncStatus[j]=childTagetList.get(j).getStatus();
                }
                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(targetSyncStatus));
                cntPlatformSyncds.updateCntPlatformSync(cntPlatformSync);
    
            }
            
            //删除服务与内容在此平台类型上的绑定关系//
            CntPlatformSync exitContentBindInPlatform = new CntPlatformSync();
            exitContentBindInPlatform.setParentid(service.getServiceid());
            exitContentBindInPlatform.setPlatform(targetsystem.getPlatform());
            
            List<CntPlatformSync> DelContentBindInPlatformList = null ;
            CntPlatformSync DelContentBindInPlatform = new CntPlatformSync();
            
            List<CntTargetSync> contentBindChildTargetList = null ;
            CntTargetSync ContentBindChildTarget = new CntTargetSync();
            ContentBindChildTarget.setParentid(service.getServiceid());
            //删除VOD内容，programBind 21//
            exitContentBindInPlatform.setObjecttype(21);
            ContentBindChildTarget.setObjecttype(21);
            DelContentBindInPlatformList = cntPlatformSyncds.getCntPlatformSyncByCond(exitContentBindInPlatform);
            for(i=0; i<DelContentBindInPlatformList.size();i++){
                DelContentBindInPlatform = DelContentBindInPlatformList.get(i);
                //查询相关平台下面还有没有同类的其他网元//
                ContentBindChildTarget.setRelateindex(DelContentBindInPlatform.getSyncindex());  
                contentBindChildTargetList = cntTargetSyncds.getcntSynnormaltargetListBycond(ContentBindChildTarget);
                if (contentBindChildTargetList.size()<1)
                {
                    cntPlatformSyncds.removeCntPlatformSync(DelContentBindInPlatform);
                 // 写日志    删除了关联平台//
                    CommonLogUtil.insertOperatorLog(DelContentBindInPlatform.getObjectid() + ","
                            + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    int[] contentTargetSyncStatus =  new int[contentBindChildTargetList.size()];
                    for (int j = 0; j < contentBindChildTargetList.size(); j++)
                    {
                        contentTargetSyncStatus[j]=contentBindChildTargetList.get(j).getStatus();
                    }
                    DelContentBindInPlatform.setStatus(StatusTranslator.getPlatformSyncStatus(contentTargetSyncStatus));
                    cntPlatformSyncds.updateCntPlatformSync(DelContentBindInPlatform);
                }
            }
            //删除连续剧内容，series 22//
            exitContentBindInPlatform.setObjecttype(22);
            ContentBindChildTarget.setObjecttype(22);
            DelContentBindInPlatformList = cntPlatformSyncds.getCntPlatformSyncByCond(exitContentBindInPlatform);
            for(i=0; i<DelContentBindInPlatformList.size();i++){
                DelContentBindInPlatform = DelContentBindInPlatformList.get(i);
                //查询相关平台下面还有没有同类的其他网元//
                ContentBindChildTarget.setRelateindex(DelContentBindInPlatform.getSyncindex());            
                contentBindChildTargetList = cntTargetSyncds.getcntSynnormaltargetListBycond(ContentBindChildTarget);
                if (contentBindChildTargetList.size()<1)
                {
                    cntPlatformSyncds.removeCntPlatformSync(DelContentBindInPlatform);
                 // 写日志   删除了关联平台//
                    CommonLogUtil.insertOperatorLog(DelContentBindInPlatform.getObjectid() + ","
                            + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    int[] contentTargetSyncStatus =  new int[contentBindChildTargetList.size()];
                    for (int j = 0; j < contentBindChildTargetList.size(); j++)
                    {
                        contentTargetSyncStatus[j]=contentBindChildTargetList.get(j).getStatus();
                    }
                    DelContentBindInPlatform.setStatus(StatusTranslator.getPlatformSyncStatus(contentTargetSyncStatus));
                    cntPlatformSyncds.updateCntPlatformSync(DelContentBindInPlatform);
                }
            }
            
            //删除直播内容，channel 23//
            exitContentBindInPlatform.setObjecttype(23);
            ContentBindChildTarget.setObjecttype(23);
            DelContentBindInPlatformList = cntPlatformSyncds.getCntPlatformSyncByCond(exitContentBindInPlatform);
            for(i=0; i<DelContentBindInPlatformList.size();i++){
                DelContentBindInPlatform = DelContentBindInPlatformList.get(i);
                //查询相关平台下面还有没有同类的其他网元//
                ContentBindChildTarget.setRelateindex(DelContentBindInPlatform.getSyncindex());            
                contentBindChildTargetList = cntTargetSyncds.getcntSynnormaltargetListBycond(ContentBindChildTarget);
                if (contentBindChildTargetList.size()<1)
                {
                    cntPlatformSyncds.removeCntPlatformSync(DelContentBindInPlatform);
                 // 写日志   删除了关联平台//
                    CommonLogUtil.insertOperatorLog(DelContentBindInPlatform.getObjectid() + ","
                            + targetsystem.getTargetid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM,
                            CommonLogConstant.OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM_INF,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    int[] contentTargetSyncStatus =  new int[contentBindChildTargetList.size()];
                    for (int j = 0; j < contentBindChildTargetList.size(); j++)
                    {
                        contentTargetSyncStatus[j]=contentBindChildTargetList.get(j).getStatus();
                    }
                    DelContentBindInPlatform.setStatus(StatusTranslator.getPlatformSyncStatus(contentTargetSyncStatus));
                    cntPlatformSyncds.updateCntPlatformSync(DelContentBindInPlatform);
                }
            }
    
            // 删除服务与内容的绑定关系(暂不执行)//
            
            rtnString = "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_TARGET_OPERATE_SUCC);
            return rtnString;
        }
        catch (Exception e)
        {
            // TODO: handle exception
            log.error( e );
            throw new Exception(e.getMessage());
        }
    }
    
    /***检测服务和其打包内容的mapping是否在本网元上有处于发布状态的。
     * 
     * @param targetsystem 网元平台Targetindex
     * @param service 服务
     * @param objecttype 关联内容的类型编号
     * @return
     * @throws DomainServiceException
     */
    private boolean isServiceContentBindPublished(Targetsystem targetsystem , Service service , int objecttype) throws DomainServiceException{
        CntTargetSync cntTargetSync = new CntTargetSync();
        cntTargetSync.setObjecttype(objecttype);
        cntTargetSync.setTargetindex(targetsystem.getTargetindex());
        cntTargetSync.setParentid(service.getServiceid());
        List<CntTargetSync> cntTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(cntTargetSync);
        int cntTargetSyncStatus = 0;
        if (cntTargetSyncList.size() > 0)
        {
            // 是否在此网元上与栏目的关联处于发布中//
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                cntTargetSync = cntTargetSyncList.get(0);
                cntTargetSyncStatus = cntTargetSync.getStatus();
                if ((cntTargetSyncStatus == 200) || (cntTargetSyncStatus == 300) || (cntTargetSyncStatus == 500))
                {
                    return true;
                }
            }
        }
        return false ;
    }
}

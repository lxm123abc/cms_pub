package com.zte.cms.castrolemap.service;

import java.util.List;

import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastrolemapDS
{
    /**
     * 新增Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public String insertCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 批量更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastrolemapList(List<Castrolemap> castrolemapList) throws DomainServiceException;

    /**
     * 根据条件更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 根据条件批量更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCastrolemapListByCond(List<Castrolemap> castrolemapList) throws DomainServiceException;

    /**
     * 删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 批量删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastrolemapList(List<Castrolemap> castrolemapList) throws DomainServiceException;

    /**
     * 根据条件删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 根据条件批量删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCastrolemapListByCond(List<Castrolemap> castrolemapList) throws DomainServiceException;

    /**
     * 查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @return Castrolemap对象
     * @throws DomainServiceException ds异常
     */
    public Castrolemap getCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 根据条件查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @return 满足条件的Castrolemap对象集
     * @throws DomainServiceException ds异常
     */
    public List<Castrolemap> getCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 根据条件分页查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
package com.zte.cms.channel.update.apply;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.channel.common.CmsScheduleConstant;
import com.zte.cms.channel.common.FileSaveUtil;
import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleBufDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.update.wkfw.ls.IScheduleUpdateWkfwCheck;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleUpdateApply extends WorkflowBase implements IScheduleUpdateApply
{
    protected IScheduleUpdateWkfwCheck scheduleUpdateWkfwCheckLS = null;
    private Log log = SSBBus.getLog(getClass());
    private ICmsScheduleDS scheduleds = null;
    private ICmsScheduleBufDS scheduleBufds = null;
    private IUsysConfigDS usysConfigDS = null;
    private IPhysicalchannelDS physicalchannelDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private IUcpBasicDS basicDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    public final static String passwords = "通过";
    DbUtil db = new DbUtil();
    private ICmsChannelDS channelDS;
    private String callbackProcess;
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public IUcpBasicDS getBasicDS()
    {
        return basicDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public ICmsChannelDS getChannelDS()
    {
        return channelDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public ICmsSchedulerecordDS getCmsSchedulerecordDS()
    {
        return cmsSchedulerecordDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    // 构造方法
    ScheduleUpdateApply()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS
     *            IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    public IScheduleUpdateWkfwCheck getScheduleUpdateWkfwCheckLS()
    {
        return scheduleUpdateWkfwCheckLS;
    }

    public void setScheduleUpdateWkfwCheckLS(IScheduleUpdateWkfwCheck scheduleUpdateWkfwCheckLS)
    {
        this.scheduleUpdateWkfwCheckLS = scheduleUpdateWkfwCheckLS;
    }

    public ICmsScheduleDS getScheduleds()
    {
        return scheduleds;
    }

    public void setScheduleds(ICmsScheduleDS scheduleds)
    {
        this.scheduleds = scheduleds;
    }

    public ICmsScheduleBufDS getScheduleBufds()
    {
        return scheduleBufds;
    }

    public void setScheduleBufds(ICmsScheduleBufDS scheduleBufds)
    {
        this.scheduleBufds = scheduleBufds;
    }

    /**
     * @param channel
     *            CmsChannel
     * @return String
     */
    public String apply(CmsSchedule schedule)
    {
        String checkResult = "";
        try
        {
            checkResult = scheduleUpdateWkfwCheckLS.checkScheduleUpdateWkfw(schedule.getScheduleindex());// 再做一遍校验
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (checkResult != "success")
        {
            return checkResult;
        }
        return apply(schedule, null);
    }

    /**
     * @param channel
     *            CmsChannel
     * @param priority
     *            String
     * @return String
     */
    public String apply(CmsSchedule schedule, String priority)
    {
        log.debug("workflow for service update starting...");
        CmsSchedule scheduleBasic = new CmsSchedule();
        scheduleBasic.setScheduleindex(schedule.getScheduleindex());
        try
        {
            scheduleBasic = scheduleds.getCmsSchedule(scheduleBasic);
            if (scheduleBasic == null)
            {
                return "119";// 节目单不存在
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int basicStatus = scheduleBasic.getStatus(); // 在修改状态之前得到基本表的状态

        scheduleBasic.setStatus(120); // 审核中
        scheduleBasic.setWorkflow(2); // 1：新增，2：修改
        scheduleBasic.setWorkflowlife(0);
        try
        {
            scheduleds.updateCmsSchedule(scheduleBasic);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // 把修改后的数据写入buf表
        CmsScheduleBuf scheduleDBBuf = new CmsScheduleBuf();
        BeanCopier basicObjDBCopy = BeanCopier.create(CmsSchedule.class, CmsScheduleBuf.class, true);
        basicObjDBCopy.copy(scheduleBasic, scheduleDBBuf, new BeanPropertyConvert());

        try
        {
            scheduleBufds.insertCmsScheduleBuf(scheduleDBBuf);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } // 不需要index，直接写进去

        CmsScheduleBuf schedulebuf = new CmsScheduleBuf();
        BeanCopier basicCopy = BeanCopier.create(CmsSchedule.class, CmsScheduleBuf.class, false);
        basicCopy.copy(schedule, schedulebuf, null);// 在这里将修改的节目单的数据存入buf表
        schedulebuf.setWorkflow(Integer.valueOf(2));
        schedulebuf.setStatus(basicStatus); // 把基本表的状态存在buf表中
        try
        {
            scheduleBufds.updateCmsScheduleBuf(schedulebuf);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // 发起工作流参数
        String pboName = schedulebuf.getProgramname() + "[" + schedulebuf.getChannelid() + "]";
        String pboId = String.valueOf(schedule.getScheduleindex());
        String pboType = "CMS_SCHEDULE_U";
        String processName = pboType + ":" + pboName;

        String callbackBean = "";
        if (null != this.getCallbackProcess())
        {
            callbackBean = "callbackProcess," + this.getCallbackProcess();

        }

        WorkflowService wkflService = new WorkflowService();
        int flag = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType, "",
                callbackBean, priority);
        if (flag <= 0)
        {
            try
            {
                throw new DomainServiceException();
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        log.debug("workflow for service update ends...");
        return "success";
        // + ResourceMgt
        // .findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_UPDATE_APPLY_SUCCESS);
    }

    /**
     * @param channelBuf
     *            CmsChannelBuf
     * @param taskId
     *            String
     * @param handleMsg
     *            String
     * @param handleEvent
     *            String
     * @param workflowStatusCode
     *            String
     * @return String
     * @throws DomainServiceException
     *             DomainServiceException
     */
    public String audit(CmsScheduleBuf scheduleBuf, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException
    {

        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        int minutes = 15;
        usysConfig.setCfgkey("cms.schedule.cantoperate.time");
        usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            minutes = Integer.parseInt(usysConfig.getCfgvalue());// 离当前时间X分钟之内的节目单不允许修改,单位分钟
        }

        DbUtil db = new DbUtil();
        // 提前15分钟下达录制计划
        String sql = "select to_char(sysdate +" + minutes + "/1440, 'yyyymmddhh24miss') currenttime from dual";// 查询数据库较当前时间晚15分钟的时间
        List rtnlist = null;
        try
        {
            rtnlist = db.getQuery(sql);
        }
        catch (Exception e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map tmpMap = new HashMap();
        tmpMap = (Map) rtnlist.get(0);
        String currenttime = tmpMap.get("currenttime").toString();
        /***********************************************************
         * if ((schedule.getStartdate() + schedule.getStarttime()) .compareToIgnoreCase(currenttime) < 0) {//
         * 现在15分钟之内即将开始的节目单不可修改 return CmsScheduleConstant.RESULT_CODE_111; }
         **********************************************************/

        String startdate = scheduleBuf.getStartdate();
        String starttime = scheduleBuf.getStarttime();

        if ((DateUtil2.get14Time(starttime)).compareToIgnoreCase(currenttime) < 0)
        {
            // 修改后的开始时间在15分钟之内的节目单不可修改
            return "1:" + "修改节目单失败,修改后的开始时间应大于当前时间" + minutes + "分钟";
        }

        CmsScheduleBuf schori = scheduleBufds.getCmsScheduleBuf(scheduleBuf);
        if (schori == null)
        {
            return "1:" + ResourceManager.getResourceText("msg.scheduleaudit.notexist");// 节目单已被审核
        }
        CntTargetSync sync = new CntTargetSync();
        sync.setObjectid(schori.getScheduleid());
        sync.setObjecttype(6);
        List schelist = cntTargetSyncDS.getCntTargetSyncByCond(sync);

        // 若节目单已发布，则频道必须已发布
        // if (schori.getStatus() == CmsScheduleConstant.STATUS_20
        // || schori.getStatus() == CmsScheduleConstant.STATUS_50
        // || schori.getStatus() == CmsScheduleConstant.STATUS_60) {
        // CmsChannel ch = new CmsChannel();
        // ch.setChannelid(schori.getChannelid());
        // List list = channelDS.getChannelByCond(ch);
        // if (list != null && list.size() > 0) {
        // ch = (CmsChannel) list.get(0);
        // }
        // if (!(ch.getStatus() == 20 || ch.getStatus() == 50 || ch
        // .getStatus() == 60)) {
        // return "1:"+ResourceMgt
        // .findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG);
        // }
        // }
        String checkResult = scheduleUpdateWkfwCheckLS.checkScheduleUpdateWkfwAudit(scheduleBuf.getScheduleindex());
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }

        String vailChannel;
        try
        {
            vailChannel = getChannelByChannelid(scheduleBuf.getChannelid());
            if (!"success".equals(vailChannel))
            {// 验证该节目单所属的频道是否存在
                return vailChannel;
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (handleEvent.equals(passwords))
        {

            List<CmsSchedule> isRepeatedList = this.isTimeRepeated(scheduleBuf);
            if (null != isRepeatedList && isRepeatedList.size() > 0)
            {
                /***************************************************************
                 * 需要修改的节目单未发布 判断与其时间重叠的节目单以及下面的录制计划是否符合删除条件
                 **************************************************************/
                if (schelist != null && schelist.size() > 0)
                {
                    for (int num = 0; num < schelist.size(); num++)
                    {
                        CntTargetSync schesync = (CntTargetSync) schelist.get(num);
                        if (schesync.getStatus() != 300 && schesync.getStatus() != 500// 新增同步成功和修改同步失败
                        )
                        {
                            for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                            {
                                CntTargetSync syncnew = new CntTargetSync();
                                syncnew.setObjectid(cmsSchedulefortime.getScheduleid());
                                syncnew.setObjecttype(6);
                                syncnew.setTargetindex(schesync.getTargetindex());
                                List listnew = cntTargetSyncDS.getCntTargetSyncByCond(syncnew);
                                if (listnew != null && listnew.size() > 0)
                                {
                                    syncnew = (CntTargetSync) listnew.get(0);
                                    if (cmsSchedulefortime.getStatus() == CmsScheduleConstant.STATUS_120)
                                    {
                                        // 1:影响到的节目单在审核中，不允许修改
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_auditing);

                                    }
                                    else if (syncnew.getStatus() == 200// 已发布和修改发布失败
                                    )
                                    {
                                        // 1:影响到的节目单在发布中，不允许修改
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_pubing);
                                    }
                                    else if (syncnew.getStatus() == 300 || syncnew.getStatus() == 500// 已发布和修改发布失败
                                    )
                                    {

                                        // 影响到的节目单已发布，不允许修改
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_pubsuccess);
                                    }
                                    else
                                    {
                                        cntTargetSyncDS.removeCntTargetSyncList(listnew);// 删除发布数据
                                    }
                                }
                                // CmsSchedulerecord cmsSchedulerecord = new
                                // CmsSchedulerecord();
                                // cmsSchedulerecord
                                // .setScheduleid(cmsSchedulefortime
                                // .getScheduleid());
                                // List<CmsSchedulerecord> schedulerecordList =
                                // cmsSchedulerecordDS
                                // .getCmsSchedulerecordByCond(cmsSchedulerecord);

                                // else {// 录制计划同节目单状态一致
                                // if (schedulerecordList != null
                                // && schedulerecordList.size() > 0) {
                                // for (CmsSchedulerecord cmsSchedulerecord1 :
                                // schedulerecordList) {
                                // if (cmsSchedulerecord1.getStatus() == 10
                                // && cmsSchedulerecord1.getStatus() == 40
                                // && cmsSchedulerecord1.getStatus() == 70) {
                                // // 需要修改的当前节目影响到的其他节目的收录计划的状态在“xx中”，不可修改
                                // return ResourceMgt
                                // .findDefaultText(ChannelConstants.ucdn_schedulerecord_updateapplydeployfail);//
                                // "1:需要修改的当前节目生成的录制计划都未发布
                                // }
                                // }
                                // }
                                // }
                            }
                            // 修改的节目单A未发布 如果满足修改条件 则在门户侧删除与其时间重叠的节目单B
                            scheduleds.removeCmsScheduleList(isRepeatedList);

                        }

                        /*******************************************************
                         * 需要修改的节目单已发布 判断与其时间重叠的节目单以及下面的录制计划是否符合删除条件
                         ******************************************************/
                        boolean isPublish = false;
                        if (schesync.getStatus() == 300 || schesync.getStatus() == 500)
                        {// A发布成功B为xx中,则A不能修改

                            /***************************************************
                             * 根据表2中判断 如果需要修改的节目单自身的录制计划状态有xx中的 则不允许修改
                             **************************************************/
                            CmsSchedulerecord schedulerecord = new CmsSchedulerecord();
                            schedulerecord.setScheduleid(schori.getScheduleid());
                            List<CmsSchedulerecord> list = new ArrayList<CmsSchedulerecord>();
                            list = cmsSchedulerecordDS.getCmsSchedulerecordByCond(schedulerecord);
                            if (list != null && list.size() > 0)
                            {
                                for (CmsSchedulerecord schedulerecord1 : list)
                                {
                                    if (schedulerecord1.getStatus() == 10 || schedulerecord1.getStatus() == 40
                                            || schedulerecord1.getStatus() == 70)
                                    {
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedulerecord_updateapplyprocessingfail);// "1:需要修改的节目单自身的录制计划状态有xx中的
                                    }
                                    if (schedulerecord1.getStatus() == 20 || schedulerecord1.getStatus() == 50
                                            || schedulerecord1.getStatus() == 60)
                                    {
                                        isPublish = true;
                                    }
                                }
                            }

                            List<CmsSchedulerecord> recordList = new ArrayList<CmsSchedulerecord>();
                            for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                            {

                                CntTargetSync syncnew = new CntTargetSync();
                                syncnew.setObjectid(cmsSchedulefortime.getScheduleid());
                                syncnew.setObjecttype(6);
                                syncnew.setTargetindex(schesync.getTargetindex());
                                List listnew = cntTargetSyncDS.getCntTargetSyncByCond(syncnew);
                                if (listnew != null && listnew.size() > 0)
                                {
                                    syncnew = (CntTargetSync) listnew.get(0);
                                    CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
                                    cmsSchedulerecord.setScheduleid(cmsSchedulefortime.getScheduleid());
                                    List<CmsSchedulerecord> schedulerecordList = cmsSchedulerecordDS
                                            .getCmsSchedulerecordByCond(cmsSchedulerecord);
                                    if (cmsSchedulefortime.getStatus() == CmsScheduleConstant.STATUS_120)
                                    {
                                        // 1:影响到的节目单在审核中，不允许修改
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_auditing);
                                    }
                                    else if (syncnew.getStatus() == 200)
                                    {
                                        // 1:影响到的节目单在发布中，不允许修改
                                        return ResourceMgt
                                                .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_pubing);
                                    }
                                    else
                                    {
                                        if (schedulerecordList != null && schedulerecordList.size() > 0)
                                        {
                                            for (CmsSchedulerecord cmsSchedulerecord1 : schedulerecordList)
                                            {
                                                if (cmsSchedulerecord1.getStatus() == 10
                                                        || cmsSchedulerecord1.getStatus() == 40
                                                        || cmsSchedulerecord1.getStatus() == 70)
                                                {
                                                    // 需要修改的当前节目影响到的其他节目的收录计划的状态在“xx中”，不可修改
                                                    return ResourceMgt
                                                            .findDefaultText(ChannelConstants.ucdn_schedulerecord_updateapplyprocessingundeployfail);// "1:需要修改的当前节目生成的录制计划都未发布
                                                }
                                                if (list.size() == 0)
                                                {
                                                    if (cmsSchedulerecord1.getStatus() == 20
                                                            || cmsSchedulerecord1.getStatus() == 50
                                                            || cmsSchedulerecord1.getStatus() == 60)
                                                    {
                                                        // 需要修改的当前节目没有生成录制计划
                                                        // 但影响到的节目单的录制计划已发布，不可修改
                                                        return ResourceMgt
                                                                .findDefaultText(ChannelConstants.ucdn_schedulerecordno_updateapplydeployfail);// "1:需要修改的当前节目没有生成录制计划
                                                    }
                                                    // A没有录制计划 与其时间重叠的B的录制计划 未同步
                                                    // 则需要门户删除B的录制计划
                                                    recordList.add(cmsSchedulerecord1);
                                                }
                                                if (!isPublish)
                                                {
                                                    if (cmsSchedulerecord1.getStatus() == 20
                                                            || cmsSchedulerecord1.getStatus() == 50
                                                            || cmsSchedulerecord1.getStatus() == 60)
                                                    {
                                                        // 需要修改的当前节目生成的录制计划都未发布
                                                        // 但影响到的节目单的录制计划已发布，不可修改
                                                        return ResourceMgt
                                                                .findDefaultText(ChannelConstants.ucdn_schedulerecordundepoy_updateapplydeployfail);// "1:需要修改的当前节目生成的录制计划都未发布
                                                    }
                                                    // A的录制计划未同步 与其时间重叠的B的录制计划
                                                    // 未同步
                                                    // 则需要门户删除B的录制计划
                                                    recordList.add(cmsSchedulerecord1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // 当前节目单是已同步 那时间重叠的节目单和录制计划就删除 同步的就做修改同步
                            if (recordList != null && recordList.size() > 0)
                            {
                                cmsSchedulerecordDS.removeCmsSchedulerecordList(recordList);
                            }
                            
                        }

                    }
                }
                else
                {
                    // 未关联平台
                    for (CmsSchedule cmsSchedulefortime : isRepeatedList)
                    {

                        CntTargetSync syncaffect = new CntTargetSync();
                        syncaffect.setObjectid(cmsSchedulefortime.getScheduleid());
                        syncaffect.setObjecttype(6);
                        List schelistaffect = cntTargetSyncDS.getCntTargetSyncByCond(syncaffect);
                        if (schelistaffect != null && schelistaffect.size() > 0)
                        {
                            for (int i = 0; i < schelistaffect.size(); i++)
                            {
                                syncaffect = (CntTargetSync) schelistaffect.get(i);
                                int status = syncaffect.getStatus();

                                if (status == 200)
                                {
                                    // "1:影响到的节目单在发布中，不允许修改";
                                    return ResourceMgt
                                            .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_pubing);
                                }
                                if (status == 300 || status == 500)
                                {// 发布成功
                                 // 影响到的节目单已发布，不允许修改
                                    return ResourceMgt
                                            .findDefaultText(ChannelConstants.ucdn_schedule_affectothers_pubsuccess);
                                }
                            }
                        }

                        if (cmsSchedulefortime.getStatus() == CmsScheduleConstant.STATUS_120)
                        {
                            // "1:影响到的节目单在审核中，不允许修改";
                            return ResourceMgt.findDefaultText(ChannelConstants.ucdn_schedule_affectothers_auditing);
                        }

                    }
                    // 修改的节目单A未发布 如果满足修改条件 则在门户侧删除与其时间重叠的节目单 不走审核流程就在者删除
                    // 走审核流程的状态在审核通过后删除

                    scheduleds.removeCmsScheduleList(isRepeatedList);

                }

            }
        }

        scheduleBuf.setStarttime(scheduleBuf.getStarttime().substring(11, 13)
                + scheduleBuf.getStarttime().substring(14, 16) + scheduleBuf.getStarttime().substring(17, 19));
        // 审核时，有可能在页面做了修改，因此需要更新
        scheduleBufds.updateCmsScheduleBuf(scheduleBuf);

        // 工作流处理
        WorkflowService wkflService = new WorkflowService();
        wkflService.completeTask(taskId, handleMsg, handleEvent);
        
        return ResourceMgt.findDefaultText(ChannelConstants.ucdn_schedule_updateapplysuccess);// "0:节目修改审核成功";

        

    }

    /**
     * 审核页面展示的信息为修改后的信息
     * 
     * @param channelbuf
     *            CmsChannelBuf
     * @return CmsChannelBuf
     * @throws Exception
     *             Exception
     */
    public CmsScheduleBuf getScheduleBuf(CmsScheduleBuf schedulebuf) throws Exception
    {
        CmsScheduleBuf rsObj = null;
        try
        {
            rsObj = scheduleBufds.getCmsScheduleBuf(schedulebuf);
        }
        catch (Exception dsEx)
        {
            throw new Exception(dsEx.getMessage());
        }
        log.debug("get serviceList by pk end");
        return rsObj;
    }

    public List<CmsSchedule> isTimeRepeated(CmsScheduleBuf cmsSchedule)
    {
        List<CmsSchedule> list = new ArrayList<CmsSchedule>();
        List<CmsSchedule> isRepeatedList = new ArrayList<CmsSchedule>();
        try
        {
            String startdate1 = cmsSchedule.getStartdate();
            String starttime1 = cmsSchedule.getStarttime();
            String start = "";
            if (starttime1.length() == 14)
            {
                start = starttime1;
            }
            else if (starttime1.length() == 19)
            {
                start = DateUtil2.get14Time(starttime1);
            }
            else
            {
                start = startdate1 + starttime1;
            }
            String duration = cmsSchedule.getDuration();
            duration = formatDuration(duration);
            String end = "";
            try
            {
                end = FileSaveUtil.getDuration(start, String.valueOf(duration));
            }
            catch (ParseException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            CmsSchedule cmsScheduleTime = new CmsSchedule();
            cmsScheduleTime.setChannelid(cmsSchedule.getChannelid());
            // list=cmsScheduleDS.isTimeRepeated(cmsScheduleTime);
            list = scheduleds.getCmsScheduleByCond(cmsScheduleTime);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {

                    CmsSchedule cmsScheduleTimechange = new CmsSchedule();
                    cmsScheduleTimechange = list.get(i);
                    String starttime = cmsScheduleTimechange.getStartdate() + cmsScheduleTimechange.getStarttime();
                    String endtime = "";
                    try
                    {
                        starttime = DateUtil2.get14Time(starttime);
                        String durationforvalid = cmsScheduleTimechange.getDuration();
                        durationforvalid = formatDuration(durationforvalid);
                        endtime = FileSaveUtil.getDuration(starttime, String.valueOf(durationforvalid));
                    }
                    catch (ParseException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if ((start.compareToIgnoreCase(starttime) >= 0 && start.compareToIgnoreCase(endtime) < 0)
                            || (end.compareToIgnoreCase(starttime) > 0 && end.compareToIgnoreCase(endtime) <= 0)
                            || (starttime.compareToIgnoreCase(start) >= 0 && starttime.compareToIgnoreCase(end) < 0)
                            || ((endtime.compareToIgnoreCase(start) > 0 && endtime.compareToIgnoreCase(end) <= 0)))
                    {// 当前要修改的节目单和已存在的节目单时间重叠
                        if (!cmsScheduleTimechange.getScheduleid().equals(cmsSchedule.getScheduleid()))
                        {// 排除自己
                            isRepeatedList.add(cmsScheduleTimechange);
                        }
                    }
                }
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isRepeatedList;
    }

    private String formatDuration(String duration)
    {
        int hour;
        int minute;
        int second;
        hour = Integer.parseInt(duration.substring(0, 2));
        minute = Integer.parseInt(duration.substring(2, 4));
        second = Integer.parseInt(duration.substring(4, 6));
        duration = hour * 60 * 60 + minute * 60 + second + "";
        return duration;
    }

    public String getChannelByChannelid(String channelid) throws Exception
    {// 新增节目单的
        // 判断需要新增节目单的频道是否存在
        // 状态时否正常
        // 及该频道对应的CP状态是否正常
        log.debug("getChannelByChannelid  start  in the class CmsScheduleLS.......................");
        long channelStatus;
        String cpid = "";
        try
        {
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(channelid);
            List<CmsChannel> channelList = channelDS.getChannelByCond(channel);
            if (channelList != null && channelList.size() > 0)
            {
                channel = channelList.get(0);
                channelStatus = channel.getStatus();
                cpid = channel.getCpid();
                if (channelStatus == CmsScheduleConstant.STATUS_100)
                {// 频道状态为100（初始）这些状态均不能操作节目单
                    return ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG);// "1:频道状态不正确";
                }
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cpid);
                List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() > 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == 4)
                    {
                        return ResourceMgt.findDefaultText(ChannelConstants.ucdn_schedule_cpmissing);// "1:cp已被删除";
                    }
                }
            }
            else
            {
                return ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS);// "1:频道已被删除";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        log.debug("getChannelByChannelid  end  in the class CmsScheduleLS......................");
        return "success";
    }

    public CmsSchedule getScheduleById(String scheduleid) throws Exception
    {
        CmsSchedule cmsSchedule = new CmsSchedule();
        cmsSchedule.setScheduleid(scheduleid);
        List<CmsSchedule> scheduleList = null;
        try
        {
            scheduleList = scheduleds.getCmsScheduleByCond(cmsSchedule);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (null != scheduleList && scheduleList.size() > 0)
        {
            cmsSchedule = scheduleList.get(0);
            // String
            // dati=cmsSchedule.getStartdate()+cmsSchedule.getStarttime();
            // cmsSchedule.setStartdate(dati);
        }
        return cmsSchedule;
    }
}
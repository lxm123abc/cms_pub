package com.zte.cms.cast.ls;

import java.util.List;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastcdnLS
{

    /**
     * 人物删除方法
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String deleteCastcdn(String castindex) throws DomainServiceException;

    /**
     * 人物查询方法
     * 
     * @param castindex 主键
     * @return
     * @throws Exception
     */
    Castcdn getCastcdn(Long castindex);

    /**
     * 人物新增方法
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String insertCastcdn(Castcdn castcdn) throws DomainServiceException;

    /**
     * 人物修改方法
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String updateCastcdn(Castcdn castcdn) throws Exception;

    /**
     * 根据条件分页查询Cast对象
     * 
     * @param castcdn对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Cast对象 支持排序
     * 
     * @param castcdn对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 批量删除
     * 
     * @param castindexs 主键集合
     * @throws Exception
     */
    public String batchDelCastcdnList(List<String> castindexs) throws Exception;
    
    public String bindBatchCastTargetsync(List<Targetsystem> platformList,List<Castcdn> castindexs);
}
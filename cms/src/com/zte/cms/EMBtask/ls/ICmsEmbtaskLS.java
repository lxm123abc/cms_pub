package com.zte.cms.EMBtask.ls;

import java.util.List;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsEmbtaskLS
{
    /**
     * 新增CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 根据条件查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return 满足条件的CmsEmbtask对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    // 新增CmsEmbtask对象
    public String insertByHandBatch(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException;

    // 查询母片表，从母片的index得到母片名称
    public IcmSourcefile getSrcFileName(IcmSourcefile icmsourcefile) throws DomainServiceException;

    // 插入技审任务
    public String insertTechAudit(Long movieindex) throws DomainServiceException;

    // 手动插入一条粗编任务
    public String insertByHand(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    // 查询数据的时候，判断来源，如果来源已经不存在，则在来源的index那里显示来源不存在
    public CmsEmbtask getCmsEmbtaskWthChkSrcIdx(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    // 查询数据，关联子内容表，查询子内容的编码和名称
    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException;

    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    //子内容批量加密
    public String insertEncryptionByBatch(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException;
    
    //子内容加密
    public String insertEncryption(CmsEmbtask cmsEmbtask) throws DomainServiceException;
    
    //单独一个子内容加密
    public String insertOneEncryption(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException;
    
    //根据子内容index获取子内容信息
    public CmsMovie getCmsMovieByIndex(Long movieindex)throws DomainServiceException; 
    //查询加密任务
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException;

    //查询加密任务
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException;
}

package com.zte.cms.content.relation.service;

import java.util.List;

import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsContentRelationDS
{
    /**
     * 新增CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException;

    public void insertCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException;

    /**
     * 更新CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException;

    /**
     * 批量更新CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException;

    /**
     * 删除CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException;

    /**
     * 批量删除CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException;

    /**
     * 查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @return CmsContentRelation对象
     * @throws DomainServiceException ds异常
     */
    public CmsContentRelation getCmsContentRelation(CmsContentRelation cmsContentRelation)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象
     * @return 满足条件的CmsContentRelation对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsContentRelation> getCmsContentRelationByCond(CmsContentRelation cmsContentRelation)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsContentRelation对象
     * 
     * @param cmsContentRelation CmsContentRelation对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public TableDataInfo pageInfoQuery2(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws DomainServiceException;

    public TableDataInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize)
            throws DomainServiceException;
    
    public TableDataInfo pageInfoQuery(String sql, String sqlcount, int start, int pageSize)
            throws DomainServiceException;

    public List<CmsContentRelation> querySelectedContentList(CmsContentRelation cmsContentRelation)
            throws DomainServiceException;

    public List<CmsContentRelationInfo> pageInfoQueryList(String sql, String sqlcount) throws DomainServiceException;

}
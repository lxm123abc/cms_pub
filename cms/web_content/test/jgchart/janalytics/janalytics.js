/* 
 * $Date:2009-02-16 16:32:26 +0100 (lun, 16 feb 2009) $
 * $Rev:44M $
 */
 
/*
 * Copyright (c) 2008 Massimiliano Balestrieri
 * 
 * $Date:2009-02-16 16:32:26 +0100 (lun, 16 feb 2009) $
 * $Rev:44M $
 * @requires jQuery v1.2.6
 * 
 * Copyright (c) 2008 Massimiliano Balestrieri
 * Examples and docs at: http://maxb.net/blog/
 * Licensed GPL licenses:
 * http://www.gnu.org/licenses/gpl.html
 */ 
(function(){
	var _gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	var _url = _gaJsHost + "google-analytics.com/ga.js";
	jQuery.getScript(_url);
})();

jQuery(window).bind("load" , function(){
	var _analytics = jQuery("#analytics").attr("class");
	if(_analytics){
	    try {
			var pageTracker = _gat._getTracker(_analytics);
			pageTracker._trackPageview();
			//console.log("tracking");
			jQuery(".tracking").click(function(){
				if(jQuery.metadata)
					var _options = jQuery(this).metadata();
				else
					var _options = {label : this.href};
				//console.log(_options);
				//console.log("tracking");
				if(_options.label){
					pageTracker._trackPageview(_options.label);
				}
				//return false;
			});
		} catch(err) {
			//alert(err);
		}
	}
});
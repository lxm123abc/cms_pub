package com.zte.cms.content.wkfw.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmContentWkfwCondition extends DynamicBaseObject
{
    /**
     * 进程创建者
     */
    private java.lang.String creator;
    /**
     * 进程启动时间
     */
    private java.util.Date taskStarttime;
    /**
     * 进程启动时间
     */
    private java.util.Date processStarttime;
    /**
     * 进程启动时间开始时间
     */
    private java.lang.String processStartTimeStart;
    /**
     * 进程启动时间结束时间
     */
    private java.lang.String processStartTimeEnd;
    /**
     * 工作流类型
     */
    private java.lang.String pboType;
    /**
     * 工作流任务优先级
     */
    private java.lang.String priority;
    /**
     * 参与者ID
     */
    private java.lang.String executorId;
    /**
     * 单据状态代码
     */
    private java.lang.String nodeStatusCode;
    /**
     * 任务状态
     */
    private java.lang.String taskStatus;
    /**
     * 进程状态
     */
    private java.lang.String processStatus;
    /**
     * 接受状态
     */
    private java.lang.String acceptStatus;
    /**
     * 审核节点名称
     */
    private java.lang.String nodeName;

    /**
     * 内容id
     */
    private java.lang.String contentid;
    /**
     * 内容名称
     */
    private java.lang.String namecn;

    /**
     * cpid
     */
    private java.lang.String cpid;
    /**
     * cp名称
     */
    private java.lang.String cpcnshortname;
    /**
     * 操作员ID
     */
    private java.lang.Long operid;

    /**
     * 操作员所属权限组：1000, '系统管理员组'；1001, '运营商管理员组'；1002, '牌照方管理员组'；1003, 'CP管理员组'
     */
    private java.lang.String roleid;

    public java.lang.String getCreator()
    {
        return creator;
    }

    public void setCreator(java.lang.String creator)
    {
        this.creator = creator;
    }

    public java.lang.String getPboType()
    {
        return pboType;
    }

    public void setPboType(java.lang.String pboType)
    {
        this.pboType = pboType;
    }

    public java.lang.String getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.String priority)
    {
        this.priority = priority;
    }

    public java.lang.String getExecutorId()
    {
        return executorId;
    }

    public void setExecutorId(java.lang.String executorId)
    {
        this.executorId = executorId;
    }

    public java.lang.String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(java.lang.String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public java.lang.String getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(java.lang.String processStatus)
    {
        this.processStatus = processStatus;
    }

    public java.lang.String getAcceptStatus()
    {
        return acceptStatus;
    }

    public void setAcceptStatus(java.lang.String acceptStatus)
    {
        this.acceptStatus = acceptStatus;
    }

    public java.lang.String getNodeStatusCode()
    {
        return nodeStatusCode;
    }

    public void setNodeStatusCode(java.lang.String nodeStatusCode)
    {
        this.nodeStatusCode = nodeStatusCode;
    }

    public java.lang.Long getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.Long operid)
    {
        this.operid = operid;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub

    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.util.Date getTaskStarttime()
    {
        return taskStarttime;
    }

    public void setTaskStarttime(java.util.Date taskStarttime)
    {
        this.taskStarttime = taskStarttime;
    }

    public java.util.Date getProcessStarttime()
    {
        return processStarttime;
    }

    public void setProcessStarttime(java.util.Date processStarttime)
    {
        this.processStarttime = processStarttime;
    }

    public java.lang.String getRoleid()
    {
        return roleid;
    }

    public void setRoleid(java.lang.String roleid)
    {
        this.roleid = roleid;
    }

    public java.lang.String getNodeName()
    {
        return nodeName;
    }

    public void setNodeName(java.lang.String nodeName)
    {
        this.nodeName = nodeName;
    }

    public java.lang.String getProcessStartTimeStart()
    {
        return processStartTimeStart;
    }

    public void setProcessStartTimeStart(java.lang.String processStartTimeStart)
    {
        this.processStartTimeStart = processStartTimeStart;
    }

    public java.lang.String getProcessStartTimeEnd()
    {
        return processStartTimeEnd;
    }

    public void setProcessStartTimeEnd(java.lang.String processStartTimeEnd)
    {
        this.processStartTimeEnd = processStartTimeEnd;
    }
}

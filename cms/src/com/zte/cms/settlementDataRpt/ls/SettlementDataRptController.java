package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.settlementDataRpt.common.CommonReturnType;
import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import org.quartz.SchedulerException;

import java.io.IOException;

/**
 * ProjectName: cms-com.zte.cms.settlementDataRpt.ls
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 16:55 2019/5/7
 * @Description:
 */
public interface SettlementDataRptController {

    /**
     * 条件查询
     * @param settlementDataRptVO
     * @return
     */
    public TableDataInfo pageInfoQuery(SettlementDataRptVO settlementDataRptVO, int arg1, int arg2) throws Exception;

    /**
     * 导入excel
     * @param
     * @return
     * @throws IOException
     */
    public CommonReturnType importRpt(String file) throws Exception;

    /**
     * 导出excel
     * @return
     */
    public SettlementDataRptVO exportRpt() ;

    /**
     * 获取项目类型列表
     * @return
     */
    public CommonReturnType getProjectType();

    /**
     *  删除
     * @param ids
     * @return
     */
    public CommonReturnType deleteByIds(String[] ids);

    /**
     *  修改
     * @param settlementDataRptVO
     * @return
     */
    public CommonReturnType updateById(SettlementDataRptVO settlementDataRptVO);

    CommonReturnType updateIsConfirmById(String id);

    /**
     *  单查
     * @param id
     * @return
     */
    public CommonReturnType selectOneById(String id);

    /**
     *  新增
     * @param settlementDataRptVO
     * @return
     */
    public CommonReturnType addSettlementDataRpt(SettlementDataRptVO settlementDataRptVO);

    /**
     *  获取当前登录人userId
     * @return
     */
    public CommonReturnType getThisLoginingRole();

}

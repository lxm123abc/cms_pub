package com.zte.cms.cpsp.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.zte.cms.basicdata.ftp.ds.IIcmFtpinfoDS;
import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.cms.basicdata.service.ICmsProgramtypeDS;
import com.zte.cms.basicdata.service.ICmsProgramtypecpMapDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.MD5;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.cms.cpsp.service.FtpUploadService;
import com.zte.cms.cpsp.service.ICmsCpbindDS;
import com.zte.cms.cpsp.service.ICmsUcpBasicDS;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.purview.access.model.OperInformation;
import com.zte.purview.access.model.OperObjectinfo;
import com.zte.purview.access.model.OperService;
import com.zte.purview.business.service.IOperInformationDS;
import com.zte.purview.business.service.IOperObjectinfoDS;
import com.zte.purview.business.service.IOperServiceDS;
import com.zte.purview.util.PurviewUtils;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.apply.CpRegister;
import com.zte.umap.cpsp.common.ls.IUcpManageLS;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.model.UcpContact;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.cpsp.common.service.IUcpContactDS;
import com.zte.umap.cpsp.wkfw.common.CpspWkfwConstants;
import com.zte.umap.sys.model.UsysProvince;
import com.zte.umap.sys.service.IUsysProvinceDS;
import com.zte.zxywpub.encrypt;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

public class UcpBasicLS extends CpRegister implements IUcpBasicLS {
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private IUcpContactDS contactDS = null;
    private IUcpBasicDS basicDS = null;
    private IUcpManageLS passUcpManageLS = null;
    private IUsysProvinceDS usysProvinceDS = null;
    private IOperServiceDS operServiceDS = null;
    private ICmsCpbindDS cmsCpbindDS = null;
    private IIcmFtpinfoDS icmFtpinfoDS = null;
    private IOperInformationDS operInformationDS;
    private ICmsUcpBasicDS cmsUcpBasicDS;
    private FtpUploadService ftpUploadService;

    public void setFtpUploadService(FtpUploadService ftpUploadService) {
        this.ftpUploadService = ftpUploadService;
    }

    public void setCmsUcpBasicDS(ICmsUcpBasicDS cmsUcpBasicDS) {
        this.cmsUcpBasicDS = cmsUcpBasicDS;
    }

    private ICmsProgramtypeDS cmsProgramTypeDS;

    private ICmsProgramtypecpMapDS cmsProgramTypecpMapDS;

    public void setCmsProgramTypecpMapDS(ICmsProgramtypecpMapDS cmsProgramTypecpMapDS) {
        this.cmsProgramTypecpMapDS = cmsProgramTypecpMapDS;
    }

    public void setCmsProgramTypeDS(ICmsProgramtypeDS cmsProgramTypeDS) {
        this.cmsProgramTypeDS = cmsProgramTypeDS;
    }


    /**
     * 申请CP
     *
     * @param ucpContactList 联系人信息集合
     * @param ucpBasicModel  CP基本信息
     * @return 操作结果
     */
    public String saveUcpBasic(String uploadResFileCtrl, List<UcpContact> ucpContactList, UcpBasicModel ucpBasicModel,
                               List<CmsProgramtypecpMap> programtypecpMap) {
        log.debug("saveUcpBasic starting...");
        String result; // 操作结果
        try {
            if (!checkCpName(ucpBasicModel)) // 校验CP名称是否使用
            {
                return "070005"; // CP名称已使用
            }
            if (!checkCpID(ucpBasicModel)) {
                return "070003"; // CPID已使用
            }            
            /*
            if (!checkFtp(ucpBasic.getCpsharedkey()))
            {
                return "070007";
            }

            
            // 获得FTP对象
            IcmFtpinfo icmFtpinfo = icmFtpinfoDS.getIcmFtpinfo(Long.parseLong(ucpBasic.getCpsharedkey()));
            icmFtpinfo.setCpid(ucpBasic.getCpid()); // 修改FTP服务器的CPID
            icmFtpinfoDS.updateIcmFtpinfo(icmFtpinfo);
             */

            ucpBasicModel.setServicekey("CMS");
            IOperObjectinfoDS operObjectinfoDS = (IOperObjectinfoDS) SSBBus.findDomainService("operObjectinfoDS");
            IOperServiceDS operServiceDS = (IOperServiceDS) SSBBus.findDomainService("operServiceDS");
            ucpBasicModel.setStatus(CpspWkfwConstants.CPSP_STATUS_OK);
            Long cpindex = getPrimaryKeyMax();
            ucpBasicModel.setCpindex(cpindex);
            //上传FTP
            String qualificationsurl = ftpUploadService.upload(0L);
            ucpBasicModel.setQualificationsurl(qualificationsurl);
            cmsUcpBasicDS.insertUcpBasic(ucpBasicModel);


            UcpBasic ucpBasic = cmsUcpBasicDS.getUcpBasicByIndex(getModelByCpindex(cpindex));
            if (ucpContactList != null && ucpContactList.size() > 0) {
                UcpContact ucpContactObj;
                for (Iterator i$ = ucpContactList.iterator(); i$.hasNext(); contactDS.insertUcpContact(ucpContactObj)) {
                    ucpContactObj = (UcpContact) i$.next();
                    ucpContactObj.setCpindex(ucpBasic.getCpindex());
                }
            }
            OperObjectinfo operObjectinfo = new OperObjectinfo();
            operObjectinfo.setObjectid(ucpBasic.getCpindex());
            operObjectinfo.setObjecttype(ucpBasic.getCptype().longValue());
            operObjectinfo.setObjectname(ucpBasic.getCpcnshortname());
            operObjectinfo.setServicekey(ucpBasic.getServicekey());
            operObjectinfoDS.insertOperObjectinfo(operObjectinfo);
            OperInfo operInfo = LoginMgt.getOperInfo("CMS");
            if (operInfo.getOperid() != null && Long.parseLong(operInfo.getOperid()) != 1L) {
                OperService operService = new OperService();
                operService.setServicekey(ucpBasic.getServicekey());
                operService.setOperid(new Long(operInfo.getOperid()));
                operService.setSerflag(ucpBasic.getCptype().longValue());
                operService.setSerindex(ucpBasic.getCpindex());
                operServiceDS.insertOperService(operService);
            }

            for (CmsProgramtypecpMap cprogramcpMap : programtypecpMap) {

                cmsProgramTypecpMapDS.insertProgramtypecpMap(cprogramcpMap);
            }

            // 保存操作日志
            /*CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CP,
                    CommonLogConstant.OPERTYPE_CP_ADD, CommonLogConstant.OPERTYPE_CP_ADD_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);*/
            result = "070000";//申请成功

        } catch (Exception e) {
            e.printStackTrace();
            log.error("dao exception:" + e.getMessage());
            result = "070001"; // 申请失败
            return result;
        }
        log.debug("saveUcpBasic end");
        return result;
    }

    /**
     * 修改CP信息
     *
     * @param ucpContactList 联系人信息集合
     * @param model          CP基本信息
     * @return 操作结果
     */
    public String updateUcpBasic(String uploadResFileCtrl, List<UcpContact> ucpContactList, UcpBasicModel model,
                                 List<CmsProgramtypecpMap> programtypecpMap) {
        log.debug("updateUcpBasic starting...");
        String result = "070000"; // 操作结果
        String operIsPcOrmanager = this.getOperIsPcOrmanager();

        if (operIsPcOrmanager.equals("cp") && uploadResFileCtrl.length() == 12 && StringUtils.isEmpty(model.getQualificationsurl())) {
            return "070010"; // 文件必填项
        }

        try {
            if (!checkCpName(model))// 验证CP名称是否使用
            {
                return "070005"; // CP名称已使用
            }

            /*
            IcmFtpinfo icmFtpinfo = icmFtpinfoDS.getIcmFtpinfo(Long.parseLong(ucpBasic.getCpsharedkey()));
            if (icmFtpinfo == null
                    || (!icmFtpinfo.getCpid().equals("0") && !icmFtpinfo.getCpid().equals(ucpBasic.getCpid())))
            {
                return "070007";
            }
            */
            // 根据cpindex查询CP信息
            UcpBasicModel validBasic = cmsUcpBasicDS.getUcpBasicByIndex(model);
            // 验证CP是否存在
            if (validBasic == null || validBasic.getStatus() == 4) {
                return "070002";
            } else if (validBasic.getStatus() == 21 || validBasic.getStatus() == 40 || validBasic.getStatus() == 42) {
                return "070004"; // CP 同步或同步中 不能修改
            } else {
                //上传FTP
                String qualificationsurl = ftpUploadService.upload(0L);
                if (StringUtils.isNotEmpty(model.getQualificationsurl())){
                    model.setQualificationsurl(model.getQualificationsurl() + qualificationsurl);
                }else {
                    model.setQualificationsurl(qualificationsurl);
                }
                cmsUcpBasicDS.updateUcpBasic(model); // 修改CP基本信息
                //contactDS.updateUcpContactList(ucpContactList); // 修改CP联系人信息

                /*
                // 查询当前CP绑定的FTP服务器
                IcmFtpinfo ftpinfo = new IcmFtpinfo();
                ftpinfo.setCpid(ucpBasic.getCpid());
                List<IcmFtpinfo> ftpList = icmFtpinfoDS.getIcmFtpinfoByCond(ftpinfo);
                if (ftpList != null && ftpList.size() > 0)
                {
                    ftpinfo = ftpList.get(0);
                    ftpinfo.setCpid("0"); // 设置FTP服务器未使用
                    icmFtpinfoDS.updateIcmFtpinfo(ftpinfo);
                }

                icmFtpinfo.setCpid(ucpBasic.getCpid()); // 修改FTP服务器的CPID
                icmFtpinfoDS.updateIcmFtpinfo(icmFtpinfo);
                */
                // 设置关联对象查询条件
                IOperObjectinfoDS operObjectinfoDS = (IOperObjectinfoDS) SSBBus.findDomainService("operObjectinfoDS");
                OperObjectinfo operObjectinfo = new OperObjectinfo();
                operObjectinfo.setObjectid(model.getCpindex());
                operObjectinfo.setObjecttype(Long.parseLong("1"));
                operObjectinfo.setServicekey("CMS");

                // 如果关联对象存在修改关联对象中的CP名称
                List<OperObjectinfo> list = operObjectinfoDS.findOperObjectinfos(operObjectinfo);
                if (list != null && list.size() > 0) {
                    operObjectinfo = list.get(0);
                    operObjectinfo.setObjectname(model.getCpcnshortname());
                    operObjectinfoDS.updateOperObjectinfo(operObjectinfo);
                }
                // 保存支持的内容类型
                CmsProgramtypecpMap cmsProgramtypecpMap = new CmsProgramtypecpMap();
                cmsProgramtypecpMap.setCpid(validBasic.getCpid());
                List<CmsProgramtypecpMap> programtypecpMapList = cmsProgramTypecpMapDS
                        .getCmsProgramtypecpMapCond(cmsProgramtypecpMap);
                if (programtypecpMapList != null && programtypecpMapList.size() > 0) {
                    for (CmsProgramtypecpMap programcpmap : programtypecpMapList) {
                        cmsProgramTypecpMapDS.deleteProgramtypecpMap(programcpmap);
                    }
                }
                if (programtypecpMap.size() > 0) {
                    for (CmsProgramtypecpMap programcpmap : programtypecpMap) {
                        cmsProgramTypecpMapDS.insertProgramtypecpMap(programcpmap);
                    }
                }
                // 保存操作日志
                /*CommonLogUtil.insertOperatorLog(model.getCpid(), CommonLogConstant.MGTTYPE_CP,
                        CommonLogConstant.OPERTYPE_CP_MOD, CommonLogConstant.OPERTYPE_CP_MOD_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);*/
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
            result = "070001"; // 修改失败
            return result;
        }
        log.debug("updateUcpBasic end");
        return result;
    }


    /**
     * 获取省份Map
     *
     * @return 省份Map
     */
    public HashMap getUsysProvinceMap() {
        log.debug("getUsysProvinceMap starting...");
        HashMap<String, Object> map = new HashMap<String, Object>();
        List<String> optionList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        try {
            UsysProvince usysProvince = new UsysProvince();
            List<UsysProvince> list = usysProvinceDS.getUsysProvinceByCond(usysProvince);// 获取省份集合
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) { // 循环省份集合
                    optionList.add(list.get(i).getProvname());
                    valueList.add(list.get(i).getProvid());
                }
            }

            map.put("textList", optionList);
            map.put("valueList", valueList);
            map.put("defaultValue", "0");

        } catch (Exception e) {

            log.error("dao exception:" + e.getMessage());
        }
        log.debug("getUsysProvinceMap end");
        return map;
    }

    /**
     * 根据CPID查询该CP支持的内容类型
     */
    public HashMap getProgramTypeByCPID(String cpid, Boolean needSelect) {
        log.debug("getProgramTypeByCPID starting...");
        HashMap<String, Object> map = new HashMap<String, Object>();
        List<String> optionList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        sb
                .append(" select ctype.typeid typeid,ctype.typename typename from zxdbm_cms.cms_programtype ctype,zxdbm_cms.cms_programtypecp_map  cpm,ucp_basic cp ");
        sb.append(" where cpm.cpid=cp.cpid  and ctype.typeid=cpm.typeid  ");
        sb.append(" and cp.cptype=1 and cp.status=1  and cp.cpid='");
        sb.append(cpid);
        sb.append("'");
        try {
            if (needSelect) {
                optionList.add("--请选择--");
                valueList.add("");
            }
            DbUtil dbutil = new DbUtil();
            List reslist = dbutil.getQuery(sb.toString());
            if (reslist != null && reslist.size() > 0) {
                Iterator it = reslist.iterator();
                while (it.hasNext()) {
                    HashMap mymap = (HashMap) it.next();
                    optionList.add((String) mymap.get("typename"));
                    valueList.add((String) mymap.get("typeid"));
                }
            }
            map.put("codeText", optionList);
            map.put("codeValue", valueList);
            map.put("defaultValue", "");

        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("getProgramTypeByCPID end");
        return map;
    }

    /*******************************************************************************************************************
     * 获取CP绑定的牌照方
     *
     * @param cpid
     * @return
     */
    @SuppressWarnings("unchecked")
    public HashMap getUsysCpBash(String cpid) {
        log.debug("getUsysCpBash starting....");
        // 初始sql语句
        String sql = "select ucp_basic.* from ucp_basic where cptype=2 and cpid in(select lpid from cms_cpbind where cpid='"
                + cpid + "' ) ";
        // 实例化DbUtil对象
        DbUtil db = new DbUtil();
        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            List<HashMap> list = db.getQuery(sql);
            List<String> optionList = new ArrayList<String>();
            List<String> valueList = new ArrayList<String>();
            optionList.add("--请选择--");
            valueList.add("");
            for (int i = 0; i < list.size(); i++) { // 循环CPID集合
                optionList.add(list.get(i).get("cpcnshortname").toString());
                valueList.add(list.get(i).get("cpid").toString());
            }
            map.put("textList", optionList);
            map.put("valueList", valueList);
            map.put("defaultValue", "0");
        } catch (Exception e) {
            log.error("dao exception" + e.getMessage());
        }
        log.debug("getUsysCpBash end");
        return map;
    }

    @Override
    public UcpBasicModel getUcpBasic(UcpBasicModel model) {
        UcpBasicModel ucpBasicByIndex = cmsUcpBasicDS.getUcpBasicByIndex(model);
        String qualificationsurl = ucpBasicByIndex.getQualificationsurl();
        List<String> list = new ArrayList<String>();
        if (qualificationsurl != null && qualificationsurl.length() > 0) {
            String[] split = qualificationsurl.split("\\|");
            for (String newName : split) {
                StringBuffer sb = new StringBuffer();
                sb.append("select * from zxdbm_cms.cms_attach where attachid='");
                sb.append(newName.substring(0, newName.indexOf(".")));
                sb.append("'");
                DbUtil dbutil = new DbUtil();
                List<HashMap<String, String>> reslist = null;
                try {
                    reslist = dbutil.getQuery(sb.toString());
                    for (HashMap<String, String> stringStringHashMap : reslist) {
                        list.add(stringStringHashMap.get("attachname"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String[] strings = list.toArray(new String[list.size()]);
            ucpBasicByIndex.setQualificationsurlArr(strings);
            ucpBasicByIndex.setQualificationsurlNewNameArr(split);
        }
        return ucpBasicByIndex;
    }

    @Override
    public UcpBasicModel deleteUcpBasicFile(String name, String cpindex) {
        UcpBasicModel model = new UcpBasicModel();
        model.setCpindex(Long.parseLong(cpindex));
        model = this.getUcpBasic(model);
        String qualificationsurl = model.getQualificationsurl();
        if (qualificationsurl != null && qualificationsurl.length() > 0) {
            qualificationsurl = qualificationsurl.replaceAll(name + "\\|", "");
        }
        model.setQualificationsurl(qualificationsurl);
        try {
            cmsUcpBasicDS.deleteUcpBasicFile(model);
            ftpUploadService.delete(Integer.parseInt(name.substring(0, name.lastIndexOf("."))), name);
            if (qualificationsurl != null && qualificationsurl.length() > 0) {
                String[] split2 = qualificationsurl.split("\\|");
                List<String> list = new ArrayList<String>();
                for (String newName : split2) {
                    StringBuffer sb = new StringBuffer();
                    sb.append("select * from zxdbm_cms.cms_attach where attachid='");
                    sb.append(newName.substring(0, newName.indexOf(".")));
                    sb.append("'");
                    DbUtil dbutil = new DbUtil();
                    List<HashMap<String, String>> reslist = null;
                    try {
                        reslist = dbutil.getQuery(sb.toString());
                        for (HashMap<String, String> stringStringHashMap : reslist) {
                            list.add(stringStringHashMap.get("attachname"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                String[] strings = list.toArray(new String[list.size()]);
                model.setQualificationsurlArr(strings);
                model.setQualificationsurlNewNameArr(split2);
            } else {
                model.setQualificationsurlArr(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return model;
    }

    @Override
    public String getOperIsPcOrmanager() {
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operid = loginUser.getOperid();
        if (UserInfoUtils.checkLoginRole(operid)) {
            return "manager";
        } else {
            return "cp";
        }
    }

    @Override
    public String checkQualificationDoc() {
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operid = loginUser.getOperid();
        List<UcpBasicModel> list = cmsUcpBasicDS.checkQualificationDoc(operid);
        if (list != null && list.size() > 0) {
            UcpBasicModel model = list.get(0);
            if (StringUtils.isEmpty(model.getQualificationsurl()) || StringUtils.isEmpty(model.getBlstartdt())
                    || StringUtils.isEmpty(model.getBlenddt()) || StringUtils.isEmpty(model.getOpstartdt())
                    || StringUtils.isEmpty(model.getOpenddt())){
                return model.getCpindex()+"";
            }
        }
        return "OK";
    }

    @Override
    public String indexUpdate(String uploadResFileCtrl,UcpBasicModel model) throws DAOException, DomainServiceException {
        if (uploadResFileCtrl.length() == 12 && StringUtils.isEmpty(model.getQualificationsurl())) {
            return "070010"; // 文件必填项
        }
        //上传FTP
        String qualificationsurl = ftpUploadService.upload(0L);
        if (StringUtils.isNotEmpty(model.getQualificationsurl())){
            model.setQualificationsurl(model.getQualificationsurl() + qualificationsurl);
        }else {
            model.setQualificationsurl(qualificationsurl);
        }
        cmsUcpBasicDS.updateUcpBasic(model); // 修改CP基本信息
        return "070000";
    }

    /**
     * 验证CP下是否有操作员
     */
    public String validOper(String cpindex) {
        log.debug("validOper starting....");
        String result = "success";
        try {
            OperService operService1 = new OperService();
            operService1.setSerindex(Long.parseLong(cpindex));
            List<OperService> list1 = operServiceDS.getOperServices(operService1); // 根据cpindex查询CP和操作员关联对象集合
            if (list1 != null && list1.size() > 0) { // CP下有操作员
                result = "fail";
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
            result = "fail";
            return result;
        }
        log.debug("validOper end");
        return result;
    }

    /**
     * 根据cpindex删除CP
     *
     * @param cpindex CP主键
     * @return 操作结果
     * @throws Exception
     */
    public String deleteUcpBasic(String cpindex) throws Exception {
        log.debug("deleteUcpBasic starting....");
        String result = "070000";
        UcpBasic ucpBasic = basicDS.getUcpBasicByIndex(Long.parseLong(cpindex)); // 通过cpindex获得CP
        try {
            if (ucpBasic != null && (ucpBasic.getStatus() == 41 || ucpBasic.getStatus() == 1)) // 该CP未删除
            {
                // String valid = validOper(cpindex); //校验CP下是否有操作员
                // if (valid.equals("fail")) { //CP下有操作员不能删除
                // return "070006";
                // }

                // 根据CPID查询CP是否和牌照方绑定
                CmsCpbind cmsCpbind = new CmsCpbind();
                cmsCpbind.setCpid(ucpBasic.getCpid());
                List<CmsCpbind> list = cmsCpbindDS.getCmsCpbindByCond(cmsCpbind);
                if (list != null && list.size() > 0) {
                    return "070007"; // CP和牌照方绑定 不能删除
                }
                /*
                // 查询FTP服务器
                IcmFtpinfo icmFtpinfo = icmFtpinfoDS.getIcmFtpinfo(Long.parseLong(ucpBasic.getCpsharedkey()));
                if (icmFtpinfo != null)
                {
                    icmFtpinfo.setCpid("0"); // 设置FTP未使用
                    icmFtpinfoDS.updateIcmFtpinfo(icmFtpinfo);
                }
                */
                ucpBasic.setCpsharedkey("");
                ucpBasic.setStatus(4); // 设置CP删除状态
                basicDS.updateUcpBasic(ucpBasic);

                // 解除该cp与操作员的绑定关系
                OperService operService = new OperService();
                operService.setSerindex(Long.parseLong(cpindex));
                List<OperService> listOService = operServiceDS.getOperServices(operService);
                if (listOService != null && listOService.size() > 0) {
                    for (OperService service : listOService) {
                        operServiceDS.removeOperService(service);
                    }
                    cmsUcpBasicDS.deleteCmsOperPower(ucpBasic.getCpid());
                }

                // 获取关联对象DS服务
                IOperObjectinfoDS operObjectinfoDS = (IOperObjectinfoDS) SSBBus.findDomainService("operObjectinfoDS");
                // 设置关联对象查询条件
                OperObjectinfo operObjectinfo = new OperObjectinfo();
                operObjectinfo.setObjectid(ucpBasic.getCpindex());
                operObjectinfo.setObjecttype(Long.parseLong("1"));
                operObjectinfo.setServicekey("CMS");

                // 获取CP关联对象
                List<OperObjectinfo> operObjectinfoList = operObjectinfoDS.findOperObjectinfos(operObjectinfo);
                if (operObjectinfoList != null && operObjectinfoList.size() > 0) { // CP关联对象存在并循环删除
                    for (int i = 0; i < operObjectinfoList.size(); i++) {
                        operObjectinfo = operObjectinfoList.get(i);
                        operObjectinfoDS.removeOperObjectinfo(operObjectinfo);
                    }
                }

                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CP,
                        CommonLogConstant.OPERTYPE_CP_DEL, CommonLogConstant.OPERTYPE_CP_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            } else if (ucpBasic.getStatus() == 21 || ucpBasic.getStatus() == 40 || ucpBasic.getStatus() == 42) {
                return "070004"; // CP同步中或已同步 不能删除
            } else if (ucpBasic.getStatus() == 2) {
                return "071001"; // CP已暂停,不能删除
            } else {
                return "070002"; // CP不存在
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
            result = "070001";
            return result;
        }
        log.debug("deleteUcpBasic end....");
        return result;
    }

    /**
     * CP绑定审核牌照方
     *
     * @param cmsCpbindList Cp绑定审核牌照方关联对象
     * @param cpid          需要绑定的CPID
     * @return 操作结果
     * @throws Exception
     */
    public String saveCmsCpbindByCond(List<CmsCpbind> cmsCpbindList, String cpid) {
        log.debug("saveCmsCpbindByCond starting....");
        String result = "070000";// 设置操作结果为真
        try {

            UcpBasic cp = new UcpBasic();
            cp.setCpid(cpid);
            cp.setCptype(1);
            List<UcpBasic> cpList = basicDS.getUcpBasicByCond(cp);
            if (cpList != null && cpList.size() > 0) {
                cp = cpList.get(0);
                if (cp == null || cp.getStatus() == 4 || cp.getStatus() == 21) {
                    return "070002";
                }
            } else {

                return "070002";
            }

            CmsCpbind delCmsCpbind = new CmsCpbind();
            delCmsCpbind.setCpid(cpid);
            List<CmsCpbind> list = cmsCpbindDS.getCmsCpbindByCond(delCmsCpbind); // 根据CPID获取已存在的CP绑定牌照方关系对象集合
            if (list != null && list.size() > 0) // 如果存在
            {
                cmsCpbindDS.removeCmsCpbindListByCond(list); // 将已存在关系删除
            }

            for (CmsCpbind cmsCpbind : cmsCpbindList) {// 循环重新保存CP绑定牌照方关系对象
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cmsCpbind.getLpid());
                ucpBasic.setCptype(2);

                // 查询牌照方是否存在
                List<UcpBasic> ucpBasiclist = basicDS.getUcpBasicByCond(ucpBasic);
                if (ucpBasiclist != null && ucpBasiclist.size() > 0 && ucpBasiclist.get(0).getStatus() == 1) {
                    cmsCpbindDS.insertCmsCpbind(cmsCpbind);
                }
            }
            CommonLogUtil.insertOperatorLog(cpid, CommonLogConstant.MGTTYPE_CP, CommonLogConstant.OPERTYPE_CP_BIND,
                    CommonLogConstant.OPERTYPE_CP_BIND_INFO, CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        } catch (DomainServiceException e) {
            result = "070001"; // 设置操作结果为假
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("saveCmsCpbindByCond end");
        return result;
    }

    /**
     * 暂停、恢得
     */
    public String pauseAndRework(String cpindex, String opertype) {
        log.debug("pauseAndRework starting....");
        String result = "070000";// 设置操作结果为真
        try {
            UcpBasic ucpBasic = basicDS.getUcpBasicByIndex(Long.parseLong(cpindex));
            if (ucpBasic != null && (ucpBasic.getStatus() == 1 || ucpBasic.getStatus() == 2)) {
                if ("1".equals(opertype)) {
                    if (ucpBasic.getStatus() == 2) {
                        return "0711113"; // CP的状态不正确
                    }
                    ucpBasic.setPortstatus(1);
                    ucpBasic.setStatus(2);// 暂停
                } else {
                    if (ucpBasic.getStatus() == 1) {
                        return "0711113"; // CP的状态不正确
                    }
                    ucpBasic.setPortstatus(0);
                    ucpBasic.setStatus(1); // 正常
                }

                OperService operService = new OperService();
                operService.setSerindex(Long.parseLong(cpindex));
                List<OperService> list = operServiceDS.getOperServices(operService);

                if (list != null && list.size() > 0) {
                    for (OperService oservice : list) {
                        long operid = oservice.getOperid();
                        OperInformation operinfo = operInformationDS.getOperInformationById(operid);// 密码为原始密码
                        if (operinfo != null) {
                            operinfo.setOperpwd(encrypt.VarLenEncrypt(PurviewUtils.clearPwd(operinfo.getOperpwd()
                                    + "11"), 30));// 密码重新加密
                            if ("1".equals(opertype)) {
                                operinfo.setOperstatus(3); // 3:禁止
                            } else {
                                operinfo.setOperstatus(0);// 0:正常
                            }
                            operInformationDS.updateOperInformation(operinfo);
                        }
                    }
                    basicDS.updateUcpBasic(ucpBasic);
                    if ("1".equals(opertype)) {
                        CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CP,
                                CommonLogConstant.OPERTYPE_CP_POUSE, CommonLogConstant.OPERTYPE_CP_POUSE_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    } else {
                        CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CP,
                                CommonLogConstant.OPERTYPE_CP_REWORK, CommonLogConstant.OPERTYPE_CP_REWORK_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                } else {
                    if ("1".equals(opertype)) {
                        return "0711111";// CP没有关联操作员，暂停无效
                    } else {
                        return "0711112";// CP没有关联操作员，恢复无效
                    }
                }
            } else {
                return "0711113"; // CP的状态不正确
            }
        } catch (DomainServiceException e) {
            result = "070001"; // 设置操作结果为假
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pauseAndRework end");
        return result;
    }

    /**
     * 通过FTP infoindex 验证FTP是否有CP绑定
     *
     * @param infoindex
     * @return 验证结果
     */
    public String validFtp(String infoindex) {
        log.debug("validFtp starting....");
        String result = "1"; // 验证结果为假
        try {
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCpsharedkey(infoindex); // 通过infoindex查询是否有CP
            List<UcpBasic> list = basicDS.getUcpBasicByCond(ucpBasic);
            if (list != null && list.size() > 0) // 有查询结果
            {
                result = "1"; // 设置验证结果为假
            } else {
                result = "0"; // 设置验证结果为真
            }
        } catch (DomainServiceException e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("validFtp end");
        return result;
    }


    /**
     * 从会话中获取CP操作员ID
     *
     * @return 返回CP操作员ID
     * @throws Exception
     */
    public String getCPIDFromSession() throws DomainServiceException, NumberFormatException {
        log.debug("getCPIDFromSession starting...");
        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        String cpid = (String) httpSession.getAttribute("CPID");
        log.debug("getCPIDFromSession end");
        return cpid;
    }

    /**
     * 查询CP可以绑定的牌照方,要求为和cp区域相同的或是区域为"全国"的牌照方
     *
     * @param ucpBasic 查询条件对象
     * @param arg1     开始页数
     * @param arg2     条数
     * @return CP信息
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryByProv(UcpBasic ucpBasic, int arg1, int arg2) throws Exception {

        log.debug("pageInfoQueryByProv starting...");
        TableDataInfo tableInfo = new TableDataInfo();
        DbUtil db = new DbUtil();
        PageInfo pageInfo = new PageInfo();
        StringBuffer sb = new StringBuffer();
        String cpid = EspecialCharMgt.conversion(ucpBasic.getCpid());
        String cpcnshortname = EspecialCharMgt.conversion(ucpBasic.getCpcnshortname());
        String businessscope = ucpBasic.getBusinessscope();
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");

        sb.append(" select distinct cpindex,cpid,cpcnshortname,cpposition,cpcreditlevel,businessscope,status  from ucp_basic cp ");
        sb.append(" left join zxinsys.oper_service os on cp.cpindex = os.serindex and os.serflag = 2 ");
        sb.append(" where cptype=2 and status=1 and cp.servicekey = 'CMS' ");
        if (cpid != null && !"".equals(cpid)) {
            sb.append(" and cpid like '%");
            sb.append(cpid);
            sb.append("%'");
        }
        if (cpcnshortname != null && !"".equals(cpcnshortname)) {
            sb.append(" and cpcnshortname like '%");
            sb.append(cpcnshortname);
            sb.append("%'");
        }
        if (businessscope != null && !"".equals(businessscope)) {
            sb.append(" and businessscope in (");
            sb.append(businessscope);
            sb.append(" , 0)");
        }

        if (operinfo != null && operinfo.getOperid() != null && !operinfo.getOperid().equals("1")) {
            sb.append(" and os.operid = ");
            sb.append(operinfo.getOperid());
        }

        try {

            pageInfo = db.pageSplitOracle(sb.toString(), arg1, arg2);
            tableInfo.setData((List) pageInfo.getResult());
            tableInfo.setTotalCount((int) pageInfo.getTotalCount());
            return tableInfo;
        } catch (DomainServiceException e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pageInfoQueryByProv end");
        return tableInfo;

    }

    /**
     * 查询CP信息
     *
     * @param ucpBasic 查询条件对象
     * @param arg1     开始页数
     * @param arg2     条数
     * @return CP信息
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int arg1, int arg2) {
        log.debug("pageInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try {
            PageUtilEntity puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("cpid");
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");

            //判断操作员是否拥有系统管理员权限，如果有给sagid=1，否则sadid=operid。
            //sagid=1的时候，查询所有的cp信息，否者只查询操作员所属的cp
            String sql = "select opergrpid from  zxinsys.oper_rights where operid = " + operinfo.getOperid();
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            for (int j = 0; j < rtnlist.size(); j++) {
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                //判断操作员下是否有系统管理组权限
                if (tmpMap.get("opergrpid").toString().equals("1000")) {
                    ucpBasic.setSagid("1");
                    break;
                }
            }
            if (ucpBasic.getSagid() == null || ucpBasic.getSagid().equals("")) {
                ucpBasic.setSagid(operinfo.getOperid());
            }
            if (ucpBasic.getCpid() != null && !ucpBasic.getCpid().equals("")) {
                ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
            }
            if (ucpBasic.getCpcnshortname() != null && !ucpBasic.getCpcnshortname().equals("")) {
                ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
            }
            dataInfo = cmsUcpBasicDS.pageInfoQuery(ucpBasic, arg1, arg2, puEntity);

        } catch (DomainServiceException e) {
            log.error("dao exception:" + e.getMessage());
        } catch (NumberFormatException e) {
            log.error("dao exception:" + e.getMessage());
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return dataInfo;

    }

    /**
     * 校验CP名称是否使用
     *
     * @param ucpBasic
     * @return 校验结果
     * @throws DomainServiceException
     */
    private boolean checkCpName(UcpBasic ucpBasic) throws DomainServiceException {
        log.debug("checkCpName start...");
        boolean flag = false;
        try {
            UcpBasic tempBasic = new UcpBasic();
            tempBasic.setCpcnshortname(ucpBasic.getCpcnshortname());
            tempBasic.setCptype(ucpBasic.getCptype());
            List<UcpBasic> ucpBasicList = basicDS.getUcpBasicExactByCond(tempBasic);
            if (ucpBasicList == null || ucpBasicList.size() == 0) {
                flag = true;
            } else if (ucpBasicList.size() == 1 && ucpBasicList.get(0).getCpid().equals(ucpBasic.getCpid())) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("checkCpName end");
        return flag;
    }

    /**
     * 校验FTP服务器是否存在
     *
     * @param infoindex FTP服务器index
     * @return 校验结果
     * @throws DomainServiceException
     */
    private boolean checkFtp(String infoindex) throws DomainServiceException {
        log.debug("checkFtp start...");
        boolean flag = false;
        try {
            IcmFtpinfo icmFtpinfo = icmFtpinfoDS.getIcmFtpinfo(Long.parseLong(infoindex));

            if (icmFtpinfo != null && icmFtpinfo.getCpid().equals("0")) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("checkFtp end");
        return flag;

    }

    /**
     * 通过CPID和类型获取CP或牌照方
     */
    public UcpBasic getUcpBasicByCPIDAndCptype(String cpid, int cptype) {
        log.debug("getUcpBasicByCPIDAndCptype starting...");
        UcpBasic ucpBasic = null;
        try {
            ucpBasic = new UcpBasic();
            ucpBasic.setCpid(cpid);
            ucpBasic.setCptype(cptype);
            List<UcpBasic> list = basicDS.getUcpBasicByCond(ucpBasic);
            if (list != null && list.size() == 1) {
                ucpBasic = list.get(0);
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("getUcpBasicByCPIDAndCptype end");
        return ucpBasic;

    }

    /**
     * 查询CP的CPID和牌照方的CPID相同的CP信息
     *
     * @param ucpBasic 查询条件对象
     * @param start    开始页数
     * @param pageSize 条数
     * @return CP信息
     * @throws Exception
     */
    public TableDataInfo queryUcpbasicBySameCPID(UcpBasic ucpBasic, int start, int pageSize) throws Exception {
        log.debug("queryUcpbasicBySameCPID starting...");
        TableDataInfo tableInfo = new TableDataInfo();
        DbUtil db = new DbUtil();
        PageInfo pageInfo = new PageInfo();
        StringBuffer sb = new StringBuffer();
        String cpid = EspecialCharMgt.conversion(ucpBasic.getCpid());
        String cpcnshortname = EspecialCharMgt.conversion(ucpBasic.getCpcnshortname());
        Integer cpcreditlevel = ucpBasic.getCpcreditlevel();
        Integer status = ucpBasic.getStatus();

        sb
                .append(" select a.cpindex,a.cpid,a.cpcnshortname,a.cpposition,a.cpcreditlevel,a.businessscope,a.status  from ");
        sb
                .append(" ( select cpindex,cpid,cpcnshortname,cpposition,cpcreditlevel,businessscope,status from ucp_basic  where cptype=1  ");
        if (cpid != null && !"".equals(cpid)) {
            sb.append(" and cpid like '%");
            sb.append(cpid);
            sb.append("%'");
        }
        if (cpcnshortname != null && !"".equals(cpcnshortname)) {
            sb.append(" and cpcnshortname like '%");
            sb.append(cpcnshortname);
            sb.append("%'");
        }
        if (cpcreditlevel != null && !"".equals(cpcreditlevel)) {
            sb.append(" and cpcreditlevel=");
            sb.append(cpcreditlevel);
        }
        if (status != null && !"".equals(status)) {
            sb.append(" and status=");
            sb.append(status);
        }
        sb.append(" ) a, ");
        sb.append(" ( select  cpindex,cpid,cpcnshortname,status from ucp_basic where cptype=2  and status <> 4");
        sb.append(" ) b where a.cpid=b.cpid ");
        try {

            pageInfo = db.pageSplitOracle(sb.toString(), start, pageSize);
            tableInfo.setData((List) pageInfo.getResult());
            tableInfo.setTotalCount((int) pageInfo.getTotalCount());
            return tableInfo;
        } catch (DomainServiceException e) {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return tableInfo;

    }

    /**
     * 检查数据库中是否存在相同的cpid
     *
     * @param ucpBasic
     * @return String 检查结果
     */
    private boolean checkCpID(UcpBasic ucpBasic) {
        UcpBasic ucpBasicCheck = new UcpBasic();
        ucpBasicCheck.setCpid(ucpBasic.getCpid());
        ucpBasicCheck.setCptype(1);
        ucpBasicCheck.setServicekey("CMS");

        List<UcpBasic> sameNameCpList = null;
        try {
            sameNameCpList = basicDS.getUcpBasicByCond(ucpBasicCheck);
        } catch (Exception e) {
            log.error("ds exception:" + e.getMessage());
        }
        if (sameNameCpList.size() > 0) //已经存在相同的cpid
        {
            return false;
        }

        return true;

    }

    public void setOperInformationDS(IOperInformationDS operInformationDS) {
        this.operInformationDS = operInformationDS;
    }

    public void setUsysProvinceDS(IUsysProvinceDS usysProvinceDS) {
        this.usysProvinceDS = usysProvinceDS;
    }

    public void setContactDS(IUcpContactDS contactDS) {
        this.contactDS = contactDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS) {
        this.basicDS = basicDS;
    }

    public void setPassUcpManageLS(IUcpManageLS passUcpManageLS) {
        this.passUcpManageLS = passUcpManageLS;
    }

    public void setOperServiceDS(IOperServiceDS operServiceDS) {
        this.operServiceDS = operServiceDS;
    }

    public void setCmsCpbindDS(ICmsCpbindDS cmsCpbindDS) {
        this.cmsCpbindDS = cmsCpbindDS;
    }

    public void setIcmFtpinfoDS(IIcmFtpinfoDS icmFtpinfoDS) {
        this.icmFtpinfoDS = icmFtpinfoDS;
    }


    /**
     * 获取index最大值
     *
     * @return
     */
    private Long getPrimaryKeyMax() {
        String sql1 = "select max(cpindex) as cpindex from zxdbm_umap.ucp_basic";
        String sql2 = "select max(cpindex) as cpindex from zxdbm_umap.ucp_contact";
        DbUtil dbutil = new DbUtil();
        List reslist1 = null;
        List reslist2 = null;
        try {
            reslist1 = dbutil.getQuery(sql1);
            reslist2 = dbutil.getQuery(sql2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String cpindex1 = null;
        String cpindex2 = null;
        if (reslist1 != null && reslist1.size() > 0) {
            Map<String, String> map1 = (Map<String, String>) reslist1.get(0);
            cpindex1 = map1.get("cpindex");
        }
        if (reslist2 != null && reslist2.size() > 0) {
            Map<String, String> map2 = (Map<String, String>) reslist2.get(0);
            cpindex2 = map2.get("cpindex");
        }
        boolean notEmpty1 = StringUtils.isNotEmpty(cpindex1);
        boolean notEmpty2 = StringUtils.isNotEmpty(cpindex2);
        if (notEmpty1 && notEmpty2) {
            long l1 = Long.parseLong(cpindex1) + 1;
            long l2 = Long.parseLong(cpindex2) + 1;
            return l1 > l2 ? l1 : l2;
        } else if (!notEmpty1 && !notEmpty2) {
            return 1L;
        } else {
            if (notEmpty1) {
                return Long.parseLong(cpindex1) + 1;
            } else if (notEmpty2) {
                return Long.parseLong(cpindex2) + 1;
            } else {
                return 1L;
            }
        }
    }

    UcpBasicModel getModelByCpindex(long index) {
        UcpBasicModel ucpBasicModel = new UcpBasicModel();
        ucpBasicModel.setCpindex(index);
        return ucpBasicModel;
    }
}

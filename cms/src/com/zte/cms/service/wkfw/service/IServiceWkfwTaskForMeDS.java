package com.zte.cms.service.wkfw.service;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceWkfwTaskForMeDS
{

    public abstract int queryServiceWkfwTaskForMeListCntByCond(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start,
            int pageSize) throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

}
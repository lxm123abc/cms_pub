package com.zte.cms.basicdata.service;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramtypeDS
{

    public CmsProgramtype getCmsProgramtype(CmsProgramtype cmsProgramtype) throws DomainServiceException;

    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DomainServiceException;

    public TableDataInfo pageInfoQuery(CmsProgramtype cmsProgramtype, int start, int pageSize)
            throws DomainServiceException;

    public List<CmsProgramtype> queryForTree() throws DomainServiceException;

    public CmsProgramtype getCmsProgramtypeById(Long id);

    public TableDataInfo queryCmsProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize)
            throws DomainServiceException;

    public void deleteProgramtype(Long id);

    public List<CmsProgramtype> queryChildCmsProgramtype(CmsProgramtype programtype);

    public CmsProgramtype programtypeUpdate(CmsProgramtype cmsProgramtype);

    public CmsProgramtype programtypeInsert(CmsProgramtype cmsProgramtype);

    public Integer queryByTypeid(CmsProgramtype cmsProgramtype);

    public Integer queryByTypename(CmsProgramtype cmsProgramtype);

    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype);

    public CmsProgramtype queryByParentid(String typeid);

    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype);

    public CmsProgramtype getCmsProgramtype(String typeid);

    public Integer isHasChild(String typeid);

    public List<CmsProgramtype> listMenuTreeByParentid(String typeid);

    public CmsProgramtype getCmsProgramtype(Long typeindex);

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex);

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException;
}
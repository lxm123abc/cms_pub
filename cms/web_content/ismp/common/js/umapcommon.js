/**
 * obj:     后台返回的数据对象。例如:$M.zteTable
 * tableId: 表格ID
 */
var __purviewMap = {};
var __contextPath = {};

function noDataTrips(obj, tableId)
{
    
    //分页数据返回的是PageDataInfo类型，其data属性为数据数组
    if((obj.data == undefined && obj.length==0)||obj.data.length == 0)
    {
        //设置所关联的表格ID
        var ztableid = tableId;
        //显示出来的表格
        var displayTB = getTableById(ztableid).table;
        //显示出来的表格的列数
        var colNum = displayTB.rows[0].cells.length;
        //因为没有数据，新增一空白提示行
        var tipRow = displayTB.insertRow(-1);
        //新建提示单元，设置其属性以及要显示的文本
        var tipTd = document.createElement("td");
        tipTd.style.cssText = "color:red; text-align:center; font-size:12px";
        tipTd.setAttribute("colSpan",colNum);
        tipTd.insertBefore(document.createTextNode($res_entry("label.norecord","")),null);
        //将提示单元加入行
        tipRow.insertBefore(tipTd,null);
    }
    
}

function getError(errordes){
    var reg = /(\w+):?(.*)/;
    var rtn = errordes.match(reg);
    var error = {};
    error.code = rtn[1];
    error.des = rtn[2];
    return error;
}

/**
 * @errordes 错误描述 格式为 code:des
 * @url 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 */
function openMsg(errordes, url, errorback){
    var _url = getContextPath() + '/ismp/common/msg.html';
    var p_closeFlag = true;
    var error = getError(errordes);
    if (url != undefined && url != null && (errorback || errorback == undefined || (!errorback && error.code == "0"))) {
        _url = _url + '?url=' + url;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrorcode").value = error.code;
    document.getElementById("txtErrordes").value = error.des;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    
    popdialog.openWithIframe($res_entry("label.promptinfo",""), _url, 310, 200, p_closeFlag);
}

/**
 * @returnInfo 返回值对象
 * @url 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 */
function openMsgBatch(returnInfo, url, errorback){
	//returnInfo 格式({main:'0:msg.info.errorcode',list:'([({id:\"0001\",name:\"测试名称一\",flag:\"msg.info.0\",message:\"msg.info.测试名称一测试名称一\"}),({id:\"0002\",name:\"测试名称二\",flag:\"msg.info.0\",message:\"msg.info.测试名称一测试名称一\"})])'})
	//使用eval()解析returnInfo
	var rtnInfo = eval(returnInfo);
	var errordes = rtnInfo.main;
	var rtnList = eval(rtnInfo.list);
    var _url = getContextPath() + '/ismp/common/msgBatch.html';
    var p_closeFlag = true;
    var error = getError(errordes);
    if (url != undefined && url != null && (errorback || errorback == undefined || (!errorback && error.code == "0"))) {
        _url = _url + '?url=' + url;
        p_closeFlag = false;
    }
    var batchInfoHTML = getBatchInfoHTML(rtnList);
    document.getElementById("txtErrorcode").value = error.code;
    document.getElementById("txtErrordes").value = error.des;
    document.getElementById("batchInfoHTML").value = batchInfoHTML;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    
    popdialog.openWithIframe($res_entry("label.promptinfo",""), _url, 380, 200, p_closeFlag);
}

/**
 * @operateInfoList service方法返回对象中的操作信息list
 */
function getBatchInfoHTML(operateInfoList)
{
	if(operateInfoList == undefined)
	{
	    return '';
	}
    var outString = '';
	var len = operateInfoList.length;
    if(len > 0)
    {
        var xh = $res_entry("label.num#caption","");
        var name = $res_entry("track.log.name#caption","");
        var res = $res_entry("track.log.oprresult#caption","");
        var desc = $res_entry("label.desc","");
        outString = outString + '<div align="left" class="editBlock" style=" width:352px;" >';
	    outString = outString + '<table width="100%" align="left">';
	    outString = outString + '<tr bgcolor="#F7F9FE">';
	    outString = outString + '<td valign="top" align="center" width="25%">'+xh+'</td>';
	    outString = outString + '<td valign="top" align="center" width="25%">'+name+'</td>';
	    outString = outString + '<td valign="top" align="center" width="20%">'+res+'</td>';
	    outString = outString + '<td valign="top" align="center" width="30%">'+desc+'</td>';
	    outString = outString + '</tr>';
	    for(var i=0; i<len; i++)
	    {
	        operateInfo = operateInfoList[i];
	        outString = outString + '<tr>';
	        outString = outString + '<td valign="top" align="left">&nbsp;'+ operateInfo.id + '</td>';
	        outString = outString + '<td valign="top" align="left">'+ operateInfo.name + '</td>';
	        outString = outString + '<td valign="top" align="left">'+ operateInfo.flag + '</td>';
	        outString = outString + '<td valign="top" align="left">'+ operateInfo.message + '</td>';
	        outString = outString + '</tr>';
	    }
	    outString = outString + '</table>';
        outString = outString + '</div>';
    }
    return outString;
}

/**
 * @info 显示信息，任意字符串
 * @url 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 */
function openMsgBox(errordes, url, errorback){
    var _url = getContextPath() + '/ismp/common/msg1.html';
    var p_closeFlag = true;

    if (url != undefined && url != null && errorback) {
        _url = _url + '?url=' + url;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrordes").value = errordes;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    
    var srceenwidth= window.screen.width;
    var msgwidth = srceenwidth / 3.2 ;
    if(msgwidth<320)
    	msgwidth = 320;
    popdialog.openWithIframe($res_entry("label.promptinfo",""), _url, msgwidth, 200, p_closeFlag);
}


/**
 * @info 显示信息，任意字符串
 * @myurl 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 * @width  提示框宽
 * @heigth 提示框高
 */
function openMsgBoxSize(errordes, windowwidth,windowheigth,myurl, errorback){
    var _url = getContextPath() + '/ismp/common/msg1.html';
    var p_closeFlag = true; 

    if (myurl != undefined && myurl != null && errorback) {
        _url = _url + '?url=' + myurl;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrordes").value = errordes;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    

    popdialog.openWithIframe($res_entry("label.promptinfo",""), _url, windowwidth, windowheigth, p_closeFlag);
}


function getContextPath() {
    // 在index中已做了处理
    var txtContextpath = parent.document.getElementById("txtContextpath");
    if (txtContextpath != null) {
        return txtContextpath.value;
    }
    if (__contextPath.rtnValue == undefined) {
        callUrl('systemMgt/getContextPath.ssm', null, '__contextPath', true);
    }
    if (__contextPath.rtnValue != undefined) {
        return __contextPath.rtnValue;
    }
    return "";
}

/**
 * @value 权限值，funcname
 * @return 如果有权限返回值为1，否则为null
 */
function getPurview(value) {
    // 在index中已做了处理
    if (parent._purviewMap != undefined) {
        return parent._purviewMap[value];
    }
    if (__purviewMap.rtnValue == undefined) {
        callUrl('umapLoginMgt/getPurview.ssm', null, '__purviewMap', true);
    }
    if (__purviewMap.rtnValue != undefined) {
        return __purviewMap.rtnValue[value];
    }
    return null;
}

function canOpen(value) {
    if (getPurview(value) != "1") {
        var l = parent.location;
        if (l == undefined){
            l = location;
        }
        l.href = getContextPath() + '/uiloader/login.html';
        return false;
    }
    return true;
}



//操作弹出菜单js
function menuFix() {
    var DivRef = document.all('PopupDiv');
    var IfrRef = document.all('DivShim');
    var sfEls = [];
    var k = 1;
    var count=0;
    var container = [];
    
    while(k <= _primary_key) {
		for(var m = 1; m <= _primary_key; m++) {
		    var sfEl = document.getElementById("nav" + m +k);
		    var sfId="nav" + m +k;
		    if(sfEl == "undefined" || sfEl == null)
		    {
			    continue;
			}
		    else
		    {
		    	var flag = false;
		    	for(var c=0;c<container.length;c++)
		    	{
		    		if(sfId == container[c])		    		
		    		{
		    			flag = true;
		    			break;
		    		}
		    	}
		    	
		    	if(	flag == false )	
		    	{			    	
			    	count++;
			    	container[count-1] = sfId;
			    	sfEls[count-1] = sfEl;
				    break;
				}
			}
	    }
	    	    
	    k ++;
    }

    for (var i = 0; i < sfEls.length; i ++) {
        var ifr = document.getElementById(IfrRef[i].id);
        var divR = document.getElementById(DivRef[i].id);

        document.getElementById(sfEls[i].id).onmouseover = function() {
            var ii=0;
		    var len=sfEls.length;
		    for(var j=0; j<len; j++){
			    if(document.getElementById(sfEls[j].id) == this){
				    ii=j;
				    break;
			    }
		    }

	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	        divR = document.getElementById(DivRef[ii].id);
	        ifr = document.getElementById(IfrRef[ii].id);
	        ifr.style.display = "block";
	        ifr.style.width = divR.offsetWidth;
	        ifr.style.height = divR.offsetHeight;
	        ifr.style.top = divR.style.top;
	        ifr.style.left = divR.style.left;
	        ifr.style.zIndex = divR.style.zIndex + 1;
	        
	    }
	
	    document.getElementById(sfEls[i].id).onMouseDown = function() {
	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	    }
	
	    document.getElementById(sfEls[i].id).onMouseUp = function() {
	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	    }
	
	    document.getElementById(sfEls[i].id).onmouseout=function() {
	        this.className = this.className.replace(new RegExp("( ?|^)sfhover\\b"), "");
	        ifr.style.display = "none";
	    }
    }
}

//<z:service>标签onerror事件方法
//非批量
function clean(errorCode,rtn_expDesc)
{
    openMsg("1:"+rtn_expDesc);
}
//批量
function cleanBatch(errorCode,rtn_expDesc)
{
    openMsgBatch(rtn_expDesc);
}

function translateStrToDate(datestr)
{
   var year;
   var month;
   var day;
   var hour;
   var minute;
   var second;
   var date_str;
   
   if(datestr.length==8 || datestr.length==14)
   {
       year=datestr.substring(0,4);

       month=datestr.substring(4,6);

       day=datestr.substring(6,8);
       date_str=year+"-"+month +"-"+day;
       if (datestr.length==14)
       {
          hour=datestr.substring(8,10);
          minute=datestr.substring(10,12);
          second=datestr.substring(12,14);
          date_str +=" "+hour+":"+minute+":"+second;
       }
   }
   else
   {
     alert($res_entry("msg.info.trans.date"));
   }
   return date_str;
}


//以下方法用于页面状态保存
//查询历史状态   ref要绑定数据的控件如: "$M.obj" , 传空的时候不进行绑定， statusSaveObj 在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
function searchHisData(ref, statusSaveObj)
{
	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		statusSaveObj = "QueryConObj";
	}
	var tableHisStatus = parentObj[statusSaveObj];
	if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
	{
		var condition = tableHisStatus.condition;
		//将查询条件绑定到控件上
		if(typeof(ref) != "undefined" && ref != null)
		{
			setBindObject(ref, condition[0]);
		}
		getTurnPageData(tableHisStatus.tid, condition,tableHisStatus.pageIndex, tableHisStatus.pagesize);
	}
}

//得到最顶层页面的对象
function getWindowParent()
{
	var parentObj = window.parent;
	var indexUrl = parentObj.location.href;
	while(indexUrl != undefined && indexUrl !=null && indexUrl.indexOf('/uiloader/index.html') == -1)
	{
		parentObj = parentObj.parent;
		indexUrl = parentObj.location.href;
	}
	return parentObj;
}

//保存table历史状态的类 ，
function TableHisStatus(tid, condition, pageIndex, pagesize)
{
	this.tid = tid;
	this.pageIndex = pageIndex;
	this.pagesize = pagesize;
	this.condition = condition;
}

//进去其他链接时 如进入修改或者查询等页面前 调用此方法保存状态  tableID 为<z:table> 的id  ， statusSaveObj 为在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
function savePageStatus(tableID, statusSaveObj)
{
	var ztable = getTableById(tableID);
	var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		parentObj['QueryConObj']=hisStatus;
	}
	else
	{
		parentObj[statusSaveObj]=hisStatus;
	}
}

//页面输入框输入字符数相关JS方法
var language = $res_entry("webdeploylanguage");
function remainLength(obj,maxLen)  
{
    var len = getStrLength(obj.value);
    if (len > maxLen)
    {
        obj.value = getMaxStr(obj,maxLen);
    }
    var curr = maxLen - getStrLength(obj.value);
    document.getElementById(obj.id+"_msg").innerHTML = curr.toString(); 
}
function getMaxStr(obj,maxLen)
{
    var str = obj.value;
    var len = 0;
    if(language == 'en')
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            {
                len += 1;
            }
            else if( value < 0x0800)
            {
                len += 2;
            }
            else if(value < 0x010000)
            {
                len += 3;
            }
		    else
		    {
		        len += 4;
		    }
            if(len > maxLen)
            {
               return str.substring(0,i);
            }
        }
    }
    else
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            {
                len += 1;
            }
            else if( value < 0x0800)
            {
                len += 2;
            }
            else if(value < 0x010000)
            { 
                len += 2;
            }
		    else
		    {
		        len += 4; //超出GBK范围
		    }
            if(len > maxLen)
            {
               return str.substring(0,i);
            }
        }
    }
}
function getStrLength(str)
{
    len = 0;
    if(language == 'en')
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            { 
                len += 1; 
            }
            else if( value < 0x0800) 
            {
                len += 2; 
            }
            else if(value < 0x010000)
            {
                len += 3; 
            }
		    else
		    {
		        len += 4;
		    }
        }
    }
    else
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            {
                len += 1;
            }
            else if( value < 0x0800)
            {
                len += 2;
            }
            else if(value < 0x010000)
            {
                len += 2;
            }
		    else
		    {
		        len += 4; //超出GBK范围
		    }
        }
    }
    return len;
}
function checkLength(obj,maxLen,msgid)
{
    str = obj.value;
    len = 0;
    if(language == 'en')
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            {
                len += 1;
            }
            else if( value < 0x0800)
            {
                len += 2; 
            }
            else if(value < 0x010000)
            {
                len += 3;
            }
		    else
		    {
		        len += 4;
		    }
        }
    }
    else
    {
        for(i=0;i<str.length;i++)
        {
            value = str.charCodeAt(i);
            if( value < 0x080)
            {
                len += 1; 
            }
            else if( value < 0x0800)
            {
                len += 2; 
            }
            else if(value < 0x010000)
            {
                len += 2; 
            }
		    else
		    {
		        len += 4; //超出GBK范围
		    }
        }
    }
    var v_obj;
    if(msgid == undefined || msgid == null)
    {
        v_obj = document.getElementById(obj.id+ "_msg");
    }
    else
    {
        v_obj = document.getElementById(msgid);
    }
    if(len>maxLen)
    {
        prefix = $res_entry("msg.info.length.prefix");
        postfix = $res_entry("msg.info.length.postfix");
        v_obj.innerText = prefix + maxLen + postfix;
        obj.focus();
        return false;
    }
    //v_obj.innerText = ''; //2009-09-14注释掉，原因：页面所有项统一校验后统一给出提示时，该项清空会清掉其他校验的提示
    return true;
}

//检验手机号码
function isMobile(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
//检验邮编
function isZipCode(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
//校验温度格式
function isTem(tem)
{
    var reg = /(-?\d+?([\u2103])?\/-?\d+?[\u2103]$)|(-?\d+?([\u2109])?\/-?\d+?[\u2109]$)/;
    if(tem.match(reg))
    {
        return true;
    }
    else
    {
        return false;
    }
}
function clearAllMsgLabel()
{
	$("label[@id$='_msg']").html("");
}

function Trim(inputStr)
{
    return inputStr.replace(/(^\s*)|(\s*$)/g,"");
}

function checkNull(inputStr)
{
    return ( Trim(inputStr).length==0)
}

function checkLen(inputStr,min,max)
{
    var tempStr=Trim(inputStr);
    if((tempStr.length<min)||(tempStr.length>max))
    {
        return false;
    }
    return true;
}

function IsDigit(cCheck)
{
    return (('0'<=cCheck) && (cCheck<='9'));
}

function checkNum(sCheck){
  var regexp= /^\d+$/; 
  return sCheck.match(regexp); 
}

function isZeroStart(sCheck){
  var regexp= /^0/; 
  return sCheck.match(regexp); 
}

function checkIP(inputStr)
{
    var regIP=/\b((25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)\.){3}(25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)\b/;
    return inputStr.match(regIP); 
}

/**
 * 检验字符串是否为IP地址
 * @param ipaddr
 * @return   true 是IP地址， false 不是IP地址
 */
function checkIPaddr(ipaddr)
{
    if(ipaddr!=""&&ipaddr!=null)
    {
    	var reg1=/^(((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d)))$/;
        return reg1.test(ipaddr);
    }
}

function checkDomain(inputStr)
{
    var regDomain=/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/;
    return inputStr.match(regDomain); 
}

function checkUrl(inputStr)
{
    //var regUrl=/^(http:[/][/]|www.)(|[a-z]|[A-Z]|[0-9]|[/.]|[~]|[:])*$/;
    //var regUrl=/^(http:[/][/]|www.)([a-z]|[A-Z]|[0-9]|[/.]|[~]|[:]|[\?]|[=])*$/;
    var regUrl=/^(http:[/][/]|www.)/;
    return inputStr.match(regUrl); 
}

function IsAlpha(cCheck)
{
    return ((('a'<=cCheck) && (cCheck<='z')) || (('A'<=cCheck) && (cCheck<='Z')))
}

function IsaNull(cCheck)
{
    return(cCheck != " ")
}

function checkMobileCC(inputStr)
{
    var Pattern=/^13[0-9]{9}$/; 
    var Pattern1=/^8613[0-9]{9}$/; 

	if(Pattern.test(Trim(inputStr))==false && Pattern1.test(Trim(inputStr))==false)
	{
		return false;
	}
	else
	{
		return true;
	}

}

function checkMobile(inputStr)
{
    var Pattern=/^13[0-9]{9}$/; 

	if(Pattern.test(Trim(inputStr))==false)
	{
		return false;
	}
	else
	{
		return true;
	}

}

function checkPhone(inputStr)
{
    var regPhone=/^[0-9]+\-[0-9]+$/;
    return (regPhone.test(Trim(inputStr)));
}

function checkCN(inputStr)
{
    var cnPattern=/[\u4E00-\u9FA5]/;
    return (cnPattern.test(Trim(inputStr)) || Trim(inputStr).length == 0);
}


function checkMoney(inputStr)
{
    var regPhone=/^[0-9]+\.[0-9]{0,2}$/;
    if (regPhone.test(Trim(inputStr))) return true;
    if (checkNum(Trim(inputStr))) return true;
    return false;
}

 function checkDate(year,month,day)
 {
     var yStr=new String(year);
     var mStr=new String(month);
     var dStr=new String(day);
     if((yStr.length!=4)||(!checkLen(month,1,2))||(!checkLen(day,1,2)))
     {
            return false;
     }
     for(i=0;i<yStr.length;i++)
     {
         cCheck = yStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                     return false;
             }
     }
     for(i=0;i<mStr.length;i++)
     {
         cCheck = mStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                      return false;
             }
     }
     for(i=0;i<dStr.length;i++)
     {
         cCheck = dStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                          return false;
             }
     }
     return true;
}

function checkstring(checkstr,userinput)
{
	var allValid = false;
	for (i = 0;i<userinput.length;i++)
	{
		ch = userinput.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = true;
			break;
	 	}
  	}

	return allValid;
}

//输入框不包含特殊字符
function CheckSpecialString(input,input_msg)
{
   if(checkstring("$%'/\\:*&?\"<>|",input.value))
   {
      document.getElementById(input_msg).innerText =$res_entry("msg.info.notinclude") +"$%'/\\:*&?\"<>|";
      input.focus();
      return false;

   }else
   {
     return true;
   }
}

function CheckSpecialString2(input,input_msg)
{
   if(checkstring("$%'/\\:*&?\"<>|,;",input.value))
   {
      document.getElementById(input_msg).innerText =$res_entry("msg.info.notinclude") +"$%'/\\:*&?\"<>|,;";
      input.focus();
      return false;

   }
   else
   {
     return true;
   }
}

//验证必填字段是否已填
function checkNotNull(input,input_msg){

    if(trim(input.value) == '' || input.value == null){

        document.getElementById(input_msg).innerText = $res_entry("label.msg.info.notnull"); //"此项不可为空";
        input.focus();
        return false;
    }
    else
    {
        return true;
    }
}


package com.zte.cms.task.dao;

import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.cms.task.model.TaskCmsauditRecord;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

import java.util.Date;
import java.util.List;

/**
 * @Author: Liangxiaomin
 * @Date Created in 16:16 2019/7/10
 * @Description:
 */
public class CmsTaskAuditRecordDAO extends DynamicObjectBaseDao implements ICmsTaskAuditRecordDAO {

    // 日志
    private Log log = SSBBus.getLog(getClass());

    public PageInfo pageInfoQuery(TaskCmsauditRecord record, int start, int pageSize) throws DAOException {
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("selectcmsTaskAuditRecordCount", record)).intValue();
        if (totalCnt > 0) {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<TaskCmsauditRecord> rsList = (List<TaskCmsauditRecord>) super.pageQuery("selectcmsTaskAuditRecord",
                    record, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public boolean insert(TaskCmsauditRecord record) {
        record.setAuditcredt(DateUtils.convertDataToString_fit(new Date(),DateUtils.fit_a));
        try {
            super.insert("insertCmsTaskAuditRecord",record);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


}

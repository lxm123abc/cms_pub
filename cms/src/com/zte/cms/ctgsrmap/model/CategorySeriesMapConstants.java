package com.zte.cms.ctgsrmap.model;

public class CategorySeriesMapConstants {
    
	public static final String MGTTYPE_CTGSRMAP = "log.ctgsr.mgt";
	public static final String OPERTYPE_CTGSR_ADD = "log.ctgsr.mgt.add";// 新增
	public static final String OPERTYPE_CTGSR_ADD_INFO = "log.ctgsr.mgt.add.info";// 新增
	
	public static final String OPERTYPE_CTGSR_DELETE = "log.ctgsr.mgt.delete";// 删除
	public static final String OPERTYPE_CTGSR_DELETE_INFO = "log.ctgsr.mgt.delete.info";// 删除
	
	public static final String OPERTYPE_CTGSR_PUBLISH = "log.ctgsr.mgt.publish";// 发布
	public static final String OPERTYPE_CTGSR_PUBLISH_INFO = "log.ctgsr.mgt.publish.info";// 发布
	
	public static final String OPERTYPE_CTGSR_UNPUBLISH = "log.ctgsr.mgt.unpublish";// 取消发布
	public static final String OPERTYPE_CTGSR_UNPUBLISH_INFO = "log.ctgsr.mgt.unpublish.info";// 取消发布

	public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
	public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
}

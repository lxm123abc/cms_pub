package com.zte.cms.service.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServiceWkfwhis extends DynamicBaseObject
{
    private java.lang.Long historyindex;
    private java.lang.String historydate;
    private java.lang.Long workflowindex;
    private java.lang.Long taskindex;
    private java.lang.Long nodeid;
    private java.lang.String nodename;
    private java.lang.String workflowstarttime;
    private java.lang.String workflowendtime;
    private java.lang.String opopinion;
    private java.lang.String opername;
    private java.lang.Long templateindex;
    private java.lang.String handleevent;
    private java.lang.Long serviceindex;
    private java.lang.String serviceid;
    private java.lang.String servicename;
    private java.lang.Integer feetype;
    private java.lang.Integer feecode;
    private java.lang.Integer fixedfee;
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String effecttime;
    private java.lang.String createtime;
    private java.lang.String modtime;
    private java.lang.String publishtime;
    private java.lang.String corpmode;
    private java.lang.String cntrange;
    private java.lang.String servicecode;
    private java.lang.Long cpindex;
    private java.lang.String cancelpubtime;

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.Long getHistoryindex()
    {
        return historyindex;
    }

    public void setHistoryindex(java.lang.Long historyindex)
    {
        this.historyindex = historyindex;
    }

    public java.lang.String getHistorydate()
    {
        return historydate;
    }

    public void setHistorydate(java.lang.String historydate)
    {
        this.historydate = historydate;
    }

    public java.lang.Long getWorkflowindex()
    {
        return workflowindex;
    }

    public void setWorkflowindex(java.lang.Long workflowindex)
    {
        this.workflowindex = workflowindex;
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.Long getNodeid()
    {
        return nodeid;
    }

    public void setNodeid(java.lang.Long nodeid)
    {
        this.nodeid = nodeid;
    }

    public java.lang.String getNodename()
    {
        return nodename;
    }

    public void setNodename(java.lang.String nodename)
    {
        this.nodename = nodename;
    }

    public java.lang.String getWorkflowstarttime()
    {
        return workflowstarttime;
    }

    public void setWorkflowstarttime(java.lang.String workflowstarttime)
    {
        this.workflowstarttime = workflowstarttime;
    }

    public java.lang.String getWorkflowendtime()
    {
        return workflowendtime;
    }

    public void setWorkflowendtime(java.lang.String workflowendtime)
    {
        this.workflowendtime = workflowendtime;
    }

    public java.lang.String getOpopinion()
    {
        return opopinion;
    }

    public void setOpopinion(java.lang.String opopinion)
    {
        this.opopinion = opopinion;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public java.lang.Long getTemplateindex()
    {
        return templateindex;
    }

    public void setTemplateindex(java.lang.Long templateindex)
    {
        this.templateindex = templateindex;
    }

    public java.lang.String getHandleevent()
    {
        return handleevent;
    }

    public void setHandleevent(java.lang.String handleevent)
    {
        this.handleevent = handleevent;
    }

    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    }

    public void setServiceindex(java.lang.Long serviceindex)
    {
        this.serviceindex = serviceindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.String getServicename()
    {
        return servicename;
    }

    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }

    public java.lang.Integer getFeetype()
    {
        return feetype;
    }

    public void setFeetype(java.lang.Integer feetype)
    {
        this.feetype = feetype;
    }

    public java.lang.Integer getFeecode()
    {
        return feecode;
    }

    public void setFeecode(java.lang.Integer feecode)
    {
        this.feecode = feecode;
    }

    public java.lang.Integer getFixedfee()
    {
        return fixedfee;
    }

    public void setFixedfee(java.lang.Integer fixedfee)
    {
        this.fixedfee = fixedfee;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getModtime()
    {
        return modtime;
    }

    public void setModtime(java.lang.String modtime)
    {
        this.modtime = modtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getCorpmode()
    {
        return corpmode;
    }

    public void setCorpmode(java.lang.String corpmode)
    {
        this.corpmode = corpmode;
    }

    public java.lang.String getCntrange()
    {
        return cntrange;
    }

    public void setCntrange(java.lang.String cntrange)
    {
        this.cntrange = cntrange;
    }

    public java.lang.String getServicecode()
    {
        return servicecode;
    }

    public void setServicecode(java.lang.String servicecode)
    {
        this.servicecode = servicecode;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public void initRelation()
    {

        this.addRelation("historyindex", "HISTORYINDEX");
        this.addRelation("historydate", "HISTORYDATE");
        this.addRelation("workflowindex", "WORKFLOWINDEX");
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("nodeid", "NODEID");
        this.addRelation("nodename", "NODENAME");
        this.addRelation("workflowstarttime", "WORKFLOWSTARTTIME");
        this.addRelation("workflowendtime", "WORKFLOWENDTIME");
        this.addRelation("opopinion", "OPOPINION");
        this.addRelation("opername", "OPERNAME");
        this.addRelation("templateindex", "TEMPLATEINDEX");
        this.addRelation("handleevent", "HANDLEEVENT");
        this.addRelation("serviceindex", "SERVICEINDEX");
        this.addRelation(serviceid, "SERVICEID");
        this.addRelation("SERVICENAME", "SERVICENAME");
        this.addRelation("feetype", "FEETYPE");
        this.addRelation("feecode", "FEECODE");

        this.addRelation("fixedfee", "FIXEDFEE");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("status", "STATUS");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("modtime", "MODTIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("corpmode", "CORPMODE");
        this.addRelation("cntrange", "CNTRANGE");
        this.addRelation("servicecode", "SERVICECODE");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");

    }

}

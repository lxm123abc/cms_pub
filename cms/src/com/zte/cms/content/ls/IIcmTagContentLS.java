package com.zte.cms.content.ls;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmTagContentLS
{
    /**
     * 删除内容关联
     * 
     * @param tagindex 内容标签主键
     * @param contentid 内容编码
     * @return 操作返回值
     */
    public String removeRelationByTindexAndConid(Long tagindex, String contentid) throws DomainServiceException;

    /**
     * 
     * 新增内容关联
     * 
     * @param tagindex 关联index
     * @param contentids 内容ID数组
     * @return 操作返回值
     */
    public String insertNewContentByIndexAndConID(Long tagindex, String[] contentids) throws DomainServiceException;

    /**
     * 批量删除内容关联记录
     * 
     * @param tagindex 内容标签主键
     * @param contentids 内容ID数组
     * @return 操作返回值
     */
    public String removeBatchRelationByTagindexAndConIDs(Long tagindex, String[] contentids)
            throws DomainServiceException;
}

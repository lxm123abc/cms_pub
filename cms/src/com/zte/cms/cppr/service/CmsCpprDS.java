package com.zte.cms.cppr.service;

import java.util.List;
import java.util.Map;

import com.zte.cms.cpsp.dao.ICmsUcpBasicDAO;
import com.zte.cms.cppr.dao.CmsCpprDAO;
import com.zte.cms.cppr.model.CmsCppr;
import com.zte.cms.cppr.model.CmsUcpBasic;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CmsCpprDS implements ICmsCpprDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IPrimaryKeyGenerator primaryKeyGenerator = null;

    private CmsCpprDAO prDao = null;
    private ICmsUcpBasicDAO cbDao = null;
    
    public void setCbDao(ICmsUcpBasicDAO cbDao)
    {
        this.cbDao = cbDao;
    }

    public void setPrDao(CmsCpprDAO prDao)
    {
        this.prDao = prDao;
    }
    
    @Override
    public List<CmsUcpBasic> getAllCmsUcpBasicListByServiceKey(String serviceKey) throws DomainServiceException
    {
        CmsUcpBasic cmsUcpBasic= new CmsUcpBasic();
        cmsUcpBasic.setServicekey(serviceKey);
        List<CmsUcpBasic> list=null;
        try
        {
            list =cbDao.getCmsUcpBasicListByServiceKey(cmsUcpBasic);

        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error("dao exception:", e);
            e.printStackTrace();         
        }
        return list;
        
    }
    
    @Override
    public List<CmsCppr> getAllLocahostCmsUcpBasicListByServiceKey(String serviceKey, long operId) throws DomainServiceException
    {
        CmsCppr cmsCppr= new CmsCppr();
        cmsCppr.setServicekey(serviceKey);
        cmsCppr.setOperatorid(operId);
        List<CmsCppr> list=null;
        try
        {
            list=prDao.queryCmsCpprListByCond(cmsCppr);
            
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error("dao exception:", e);
            e.printStackTrace();
        }
        return list;
    }
    
    @Override
    public boolean authorizeOperCp(Map paramMap){
        CmsCppr cmsCppr= new CmsCppr();      
        cmsCppr.setCpindex(Long.valueOf(String.valueOf(paramMap.get("OPERGRPINDEX"))));
        cmsCppr.setOperatorid(Long.valueOf(String.valueOf(paramMap.get("OPERID"))));
        cmsCppr.setOperatortype(Long.valueOf("1"));
        cmsCppr.setCpid(String.valueOf(paramMap.get("OPERGRPID")));
        cmsCppr.setCpcnshortname(String.valueOf(paramMap.get("OPERGRPNAME")));
        cmsCppr.setCptype(Long.valueOf(String.valueOf(paramMap.get("OPERGRPTYPE"))));
        cmsCppr.setServicekey(String.valueOf(paramMap.get("SERVICEKEY")));
        try
        {
            prDao.insertCmsCppr(cmsCppr);
            return true;
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error("dao exception:", e);
            e.printStackTrace();
            return false;
        }
       
        
    }
    @Override
    public boolean dismissOperCp(Map paramMap){
        CmsCppr cmsCppr= new CmsCppr();
        cmsCppr.setOperatorid(Long.valueOf(String.valueOf(paramMap.get("OPERID"))));
        cmsCppr.setOperatortype(Long.valueOf("1"));
        cmsCppr.setCpindex(Long.valueOf(String.valueOf(paramMap.get("OPERGRPINDEX"))));
        cmsCppr.setCpid(String.valueOf(paramMap.get("OPERGRPID")));
        cmsCppr.setCpcnshortname(String.valueOf(paramMap.get("OPERGRPNAME")));
        cmsCppr.setServicekey(String.valueOf(paramMap.get("SERVICEKEY")));
        cmsCppr.setCptype(Long.valueOf(String.valueOf(paramMap.get("OPERGRPTYPE"))));

        try
        {
            prDao.deleteCmsCppr(cmsCppr);
            return true;
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error("dao exception:", e);
            e.printStackTrace();
            return false;
        }
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }


}

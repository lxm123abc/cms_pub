package com.zte.cms.content.modwkfw;

import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICntModApply
{
    /**
     * <p>
     * cpsp修改申请，发起修改申请工作流，该方法是修改工作流的申请方法，目前提供了一步和两步的工作流模板
     * </p>
     * <p>
     * 用到的开关有：uniqueNameFlag:名称唯一性校验开关;uniqueIdFlag:编码唯一性校验开关;
     * checkCpindexFlag:CPSP基本信息存在性校验开关;checkContactindexFlag:CPSP联系人信息存在性校验开关;
     * checkFileindexFlag:CPSP文件信息存在性校验开关;wkfwCanStartFlag:是否可以发起工作流开关;bpsErrorCodeChangeFlag:是返回转换后的BSP工作流错误码校验开关
     * </p>
     * <p>
     * 处理逻辑：首先获得CPSP基本信息组件开关类对象cpspCommonProperty，如果不为空，则调用checkUcpCondAll(arg0)方法进行校验，如果校验不通过，返回相应错误码；
     * 否则如果cpspCommonProperty不为空且wkfwCanStartFlag开关打开，则校验工作流是否可以发起，如果校验不通过，则返回070105；查询修改前的cpsp基本信息；设置CPSP的工作流状态为“修改”，并更新该信息；保存CPSP基本信息为缓存信息；若联系人信息列表不为空，循环取出联系人信息，
     * 如果是新增联系人信息，则调用主键新增入联系人缓存表，如果是更新联系人，则不调用主键新增入联系人缓存表；文件信息列表处理方式同联系人信息；工作流处理；发起工作流
     * </p>
     * 
     * @param ucpBasic cpsp基本信息实体，不能为null
     * @param ucpContactList cpsp动态联系人信息列表，即所增加的联系人信息
     * @param ucpFileList cpsp动态文件上传信息列表，即全部文件上传信息
     * @return 返回工作流修改申请结果 <br/><a href=/umap_errorcode.html#070000>070000</a>：成功 <br/><a
     *         href=/umap_errorcode.html#070100>070100</a>：CPSP不存在 <br/><a href=/umap_errorcode.html#070104>070104</a>：CPID已经存在
     *         <br/><a href=/umap_errorcode.html#070105>070105</a>：CPSP已发起工作流未结束 <br/><a
     *         href=/umap_errorcode.html#070107>070107</a>：CP中文名简称已经存在 <br/><a href=/umap_errorcode.html#070108>070108</a>：联系人信息不存在
     *         <br/><a href=/umap_errorcode.html#070109>070109</a>：文件信息不存在 <br/><a
     *         href=/umap_errorcode.html#070112>070112</a>：必填入参为空 <br/><a href=/umap_errorcode.html#010010>010010</a>：发起工作流时，找不到工作流模板
     *         <br/><a href=/umap_errorcode.html#010011>010011</a>：发起工作流时，找不到参与者 <br/><a
     *         href=/umap_errorcode.html#010012>010012</a>：发起工作流时，工作流模板某个节点没有设置参与者 <br/><a
     *         href=/umap_errorcode.html#010013>010013</a>：发起工作流时，执行存储过程出错 <br/><a
     *         href=/umap_errorcode.html#010014>010014</a>：发起工作流时，进程创建者不存在 <br/><a
     *         href=/umap_errorcode.html#010015>010015</a>：发起工作流时，回调bean没有设置 <br/><a
     *         href=/umap_errorcode.html#010016>010016</a>：发起工作流时，回调webservice错误 <br/><a
     *         href=/umap_errorcode.html#010017>010017</a>：发起工作流时，BPS未知错误
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public String runModApply(CmsProgram cmsProgram) throws Exception;

    public String runModApply(CmsMovie cmsMovie, String isUpload) throws Exception;

    public String runModAudit(String[] wkfwParam) throws Exception;

    /**
     * <p>
     * 审核/配置待处理的cpsp工单，该方法是修改工作流的审核/配置方法，目前提供了一步和两步的工作流模板
     * </p>
     * <p>
     * 用到的开关有：uniqueNameFlag:名称唯一性校验开关;uniqueIdFlag:编码唯一性校验开关;
     * checkCpindexFlag:CPSP基本信息存在性校验开关;checkContactindexFlag:CPSP联系人信息存在性校验开关; checkFileindexFlag:CPSP文件信息存在性校验开关;
     * wkfwHasDoneFlag:工作流是否已经处理工作流校验开关; bpsErrorCodeChangeFlag:是返回转换后的BSP工作流错误码校验开关
     * </p>
     * <p>
     * 处理逻辑：首先获得CPSP基本信息组件开关类对象cpspCommonProperty，如果不为空且wkfwHasDoneFlag开关打开，进行工作流是否已经处理的校验，如果校验不通过，返回010034；
     * 如果cpspCommonProperty不为空且checkCpindexFlag开关打开，校验更新的CPSP缓存信息对象是否存在，如果不存在，则返回070118；
     * 如果cpspCommonProperty不为空且checkContactindexFlag开关打开，则循环对联系人缓存信息列表做更新对象存在性的校验，如果不存在，则返回070119；
     * 如果cpspCommonProperty不为空且checkFileindexFlag开关打开，则循环对文件缓存信息列表做更新对象存在性的校验，如果不存在，则返回070120；
     * 如果cpspCommonProperty不为空，调用checkUcpBasicAll(arg0)方法校验CPSP缓存信息的名称和编码唯一性，如果校验不通过，返回相应错误码；
     * 最后更新cpsp基本信息缓存信息、更新联系人、文件缓存信息；工作流处理
     * </p>
     * 
     * @param basicBuf cpsp基本buf信息实体，不能为null
     * @param ucpContactBufList cpsp联系人缓存信息
     * @param ucpFileBufList cpsp文件缓存信息
     * @param taskId 任务ID
     * @param handleMsg 审核/配置意见，可为空
     * @param handleEvent 审核/配置结果：通过/不通过，不可为空
     * @return 返回审核/配置工单结果 <br/><a href=/umap_errorcode.html#070000>070000</a>: 成功 <br/><a
     *         href=/umap_errorcode.html#070104>070104</a>：CPID已经存在 <br/><a href=/umap_errorcode.html#070107>070107</a>：CP中文名简称已经存在
     *         <br/><a href=/umap_errorcode.html#070112>070112</a>：必填入参为空 <br/><a
     *         href=/umap_errorcode.html#070118>070118</a>：CPSP缓存信息不存在 <br/><a href=/umap_errorcode.html#070119>070119</a>：CPSP联系人缓存信息不存在
     *         <br/><a href=/umap_errorcode.html#070120>070120</a>：CPSP文件缓存信息不存在 <br/><a
     *         href=/umap_errorcode.html#010030>010030</a>：审核工作流时，找不到参与者 <br/><a
     *         href=/umap_errorcode.html#010031>010031</a>：审核工作流时，审核节点没有设置“进程启动者”或者“带权限的资源角色”之类的参与者 <br/><a
     *         href=/umap_errorcode.html#010032>010032</a>：审核工作流时，执行存储过程出错 <br/><a
     *         href=/umap_errorcode.html#010033>010033</a>：审核工作流时，进程参数不存在 <br/><a
     *         href=/umap_errorcode.html#010034>010034</a>：审核工作流时，工单已经被处理了 <br/><a
     *         href=/umap_errorcode.html#010035>010035</a>：审核工作流时，回调bean没有设置 <br/><a
     *         href=/umap_errorcode.html#010036>010036</a>：审核工作流时，任务执行人错误 <br/><a
     *         href=/umap_errorcode.html#010037>010037</a>：审核工作流时，回调webservice错误 <br/><a
     *         href=/umap_errorcode.html#010038>010038</a>：审核工作流时，任务不存在 <br/><a
     *         href=/umap_errorcode.html#010039>010039</a>：审核工作流时，任务路由错误 <br/><a
     *         href=/umap_errorcode.html#010040>010040</a>：审核工作流时，BPS未知错误
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXMCISMP-UMAPV2.01.01
     */

    /**
     * <p>
     * cpsp修改申请，发起修改申请工作流，该方法是带路由的修改工作流的申请方法，提供了四步的工作流模板，每步都可以由用户选择是否需要
     * </p>
     * <p>
     * 用到的开关有：uniqueNameFlag:名称唯一性校验开关;uniqueIdFlag:编码唯一性校验开关;uniqueAccesscodeFlag:接入码唯一性校验开关;
     * checkCpindexFlag:CPSP基本信息存在性校验开关;checkContactindexFlag:CPSP联系人信息存在性校验开关;checkAccesscodeindexFlag:CPSP接入码信息存在性校验开关;
     * checkFileindexFlag:CPSP文件信息存在性校验开关;wkfwCanStartFlag:是否可以发起工作流开关; bpsErrorCodeChangeFlag:是返回转换后的BSP工作流错误码校验开关
     * </p>
     * <p>
     * 处理逻辑：
     * </p>
     * <p>
     * 首先获得CPSP基本信息组件开关类对象cpspCommonProperty，如果不为空，则调用checkUcpCondAll(arg0)方法进行校验，如果校验不通过，返回相应错误码；
     * 否则如果cpspCommonProperty不为空且wkfwCanStartFlag开关打开，则校验工作流是否可以发起，如果校验不通过，则返回070105；解析工作流路由条件,如果不符合要求，
     * 返回010001;如果路由符合要求，则获取CPSP相关数据；设置CPSP的工作流状态为“修改”，并更新该信息；保存CPSP基本信息为缓存信息；
     * 若联系人信息列表不为空，循环取出联系人信息，如果是新增联系人信息，则调用主键新增入联系人缓存表，如果是更新联系人，则不调用主键新增入联系人缓存表； 文件信息和接入码信息的处理方式同联系人信息;工作流处理；发起工作流
     * </p>
     * 
     * @param ucpCond
     *            cpsp相关信息管理对象，其中的ucpbasic对象不能为null，其中wkfwobjectname、wkfwpbotype由上层传入，必填,且wkfwpbotype的后缀有限制,修改工作流以_U结尾;若wkfwobjectname不传入，则默认为"CPSP";若wkfwpbotype不传入，则默认为"CP_U"
     * @param templateRoute 工作流路由条件，输入为4位数字的字符串，每位必须为1或者0
     * @return 返回工作流修改申请结果 <br/><a href=/umap_errorcode.html#070000>070000</a>：成功 <br/><a
     *         href=/umap_errorcode.html#070100>070100</a>：CPSP不存在 <br/><a href=/umap_errorcode.html#070104>070104</a>：CPID已经存在
     *         <br/><a href=/umap_errorcode.html#070105>070105</a>：CPSP已发起工作流未结束 <br/><a
     *         href=/umap_errorcode.html#070107>070107</a>：CP中文名简称已经存在 <br/><a href=/umap_errorcode.html#070108>070108</a>：联系人信息不存在
     *         <br/><a href=/umap_errorcode.html#070109>070109</a>：文件信息不存在 <br/><a
     *         href=/umap_errorcode.html#070110>070110</a>：该CPSP接入码已被前向匹配 <br/><a
     *         href=/umap_errorcode.html#070111>070111</a>：该CPSP接入码不存在 <br/><a href=/umap_errorcode.html#070112>070112</a>：必填入参为空
     *         <br/><a href=/umap_errorcode.html#070114>070114</a>：已发起CPSP修改工作流 <br/><a
     *         href=/umap_errorcode.html#010001>010001</a>：工作流路由不正确 <br/><a href=/umap_errorcode.html#010010>010010</a>：发起工作流时，找不到工作流模板
     *         <br/><a href=/umap_errorcode.html#010011>010011</a>：发起工作流时，找不到参与者 <br/><a
     *         href=/umap_errorcode.html#010012>010012</a>：发起工作流时，工作流模板某个节点没有设置参与者 <br/><a
     *         href=/umap_errorcode.html#010013>010013</a>：发起工作流时，执行存储过程出错 <br/><a
     *         href=/umap_errorcode.html#010014>010014</a>：发起工作流时，进程创建者不存在 <br/><a
     *         href=/umap_errorcode.html#010015>010015</a>：发起工作流时，回调bean没有设置 <br/><a
     *         href=/umap_errorcode.html#010016>010016</a>：发起工作流时，回调webservice错误 <br/><a
     *         href=/umap_errorcode.html#010017>010017</a>：发起工作流时，BPS未知错误
     *         ，类型String，070000为成功，010001:工作流路由不符合要求，070100:CPSP不存在，070114：已发起修改工作流，未结束
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXUMAPV2.02.01
     */
    /**
     * <p>
     * 审核/配置待处理的cpsp工单，该方法是带路由的修改工作流的审核/配置方法，提供了四步的工作流模板
     * </p>
     * <p>
     * 用到的开关有：uniqueNameFlag:名称唯一性校验开关;uniqueIdFlag:编码唯一性校验开关;uniqueAccesscodeFlag:接入码唯一性校验开关;
     * checkCpindexFlag:CPSP基本信息存在性校验开关;wkfwHasDoneFlag:工作流是否已经处理工作流校验开关; checkContactindexFlag:CPSP联系人信息存在性校验开关;
     * checkFileindexFlag:CPSP文件信息存在性校验开关; checkAccesscodeindexFlag:CPSP接入码信息存在性校验开关;
     * bpsErrorCodeChangeFlag:是返回转换后的BSP工作流错误码校验开关
     * </p>
     * <p>
     * 处理逻辑：
     * </p>
     * <p>
     * 获取CPSP相关缓存信息；若CPSP基本缓存信息不为空，获取CPSP基本信息组件的开关类对象cpspCommonProperty，如果cpspCommonProperty不为空且wkfwCanStartFlag开关打开，
     * 则校验工作流是否可以发起，如果校验不通过，则返回070105；如果cpspCommonProperty不为空且CheckCpindexFlag开关打开，则校验修改的缓存对象是否存在，如果不存在，则返回070118；
     * 如果cpspCommonProperty不为空且checkContactindexFlag开关打开，则循环对联系人缓存信息列表做更新对象存在性的校验，如果不存在，则返回070119；
     * 如果cpspCommonProperty不为空且checkFileindexFlag开关打开，则循环对文件缓存信息列表做更新对象存在性的校验，如果不存在，则返回070120；
     * 如果cpspCommonProperty不为空且checkAccesscodeindexFlag开关打开，则循环对接入码缓存信息列表做更新对象存在性的校验，如果不存在，则返回070121；
     * 如果cpspCommonProperty不为空且uniqueAccesscodeFlag开关打开则对接入码进行校验，校验不通过，返回相应错误码；如果cpspCommonProperty不为空，
     * 调用checkUcpBasicAll(arg0)方法校验CPSP缓存信息的名称和编码唯一性，如果校验不通过，，返回相应错误码；否则更新该CPSP基本缓存信息、更新联系人、文件、接入码缓存信息；工作流处理
     * </p>
     * 
     * @param ucpCondBuf cpsp相关缓存信息管理对象，若ucpCondBuf不为空，则其中ucpBasicBuf不能为空
     * @param taskId 任务ID
     * @param handleMsg 审核/配置意见，可为空
     * @param handleEvent 审核/配置结果：通过/不通过，不可为空
     * @param templateRoute 工作流路由条件，输入为4位数字的字符串，每位必须为1或者0
     * @return 返回审核/配置工单结果 <br/><a href=/umap_errorcode.html#070000>070000</a>: 成功 <br/><a
     *         href=/umap_errorcode.html#070104>070104</a>：CPID已经存在 <br/><a href=/umap_errorcode.html#070107>070107</a>：CP中文名简称已经存在
     *         <br/><a href=/umap_errorcode.html#070110>070110</a>：该CPSP接入码已被前向匹配 <br/><a
     *         href=/umap_errorcode.html#070112>070112</a>：必填入参为空 <br/><a href=/umap_errorcode.html#070118>070118</a>：CPSP缓存信息不存在
     *         <br/><a href=/umap_errorcode.html#070119>070119</a>：CPSP联系人缓存信息不存在 <br/><a
     *         href=/umap_errorcode.html#070120>070120</a>：CPSP文件缓存信息不存在 <br/><a
     *         href=/umap_errorcode.html#070121>070121</a>：CPSP接入码缓存信息不存在 <br/><a
     *         href=/umap_errorcode.html#010030>010030</a>：审核工作流时，找不到参与者 <br/><a
     *         href=/umap_errorcode.html#010031>010031</a>：审核工作流时，审核节点没有设置“进程启动者”或者“带权限的资源角色”之类的参与者 <br/><a
     *         href=/umap_errorcode.html#010032>010032</a>：审核工作流时，执行存储过程出错 <br/><a
     *         href=/umap_errorcode.html#010033>010033</a>：审核工作流时，进程参数不存在 <br/><a
     *         href=/umap_errorcode.html#010034>010034</a>：审核工作流时，工单已经被处理了 <br/><a
     *         href=/umap_errorcode.html#010035>010035</a>：审核工作流时，回调bean没有设置 <br/><a
     *         href=/umap_errorcode.html#010036>010036</a>：审核工作流时，任务执行人错误 <br/><a
     *         href=/umap_errorcode.html#010037>010037</a>：审核工作流时，回调webservice错误 <br/><a
     *         href=/umap_errorcode.html#010038>010038</a>：审核工作流时，任务不存在 <br/><a
     *         href=/umap_errorcode.html#010039>010039</a>：审核工作流时，任务路由错误 <br/><a
     *         href=/umap_errorcode.html#010040>010040</a>：审核工作流时，BPS未知错误
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXUMAPV2.02.01
     */
}

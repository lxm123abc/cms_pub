 <%@page language="java" contentType = "text/html; charset=UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="com.zte.zxywpub.*"%>
<% 
String Remoteoper=(String)session.getAttribute("OPERNAME");
String servletPath = request.getServletPath();
String rootPath = (servletPath.split("zxywpub"))[0];
if(Remoteoper==null){
  %><script language="javascript1.2">
    alert("Please login first");
	top.location.href = ' <%=rootPath%>' + "zxywpub";
  </script>
 <%
}
  %>
<jsp:useBean id="dblink" class="com.zte.zxywpub.DbLinkAnalyser"
	scope="application" />
<%if (session.getAttribute("OPERNAME") == null) {
			//response.sendRedirect("../login.jsp");
		}

		%>

<html>
<head>
<title>Database Test</title>
<link rel="stylesheet" type="text/css" href="inc/style.css">
<style>
<!--
body {
margin-left: 0px;
margin-top: 0px;
margin-right: 5px;
margin-bottom: 0px;
}
.style1 {
color: #FFFFFF;
font-weight: bold;
}
-->
</style>

<script language="JavaScript">

function clearAll(){
   document.theForm.sql.value="";
   document.theForm.result.value="";
	document.theForm.times.value="";
}

function formatStr(str){
	String.prototype.trim   =   function(){   
          //   用正则表达式将前后空格   
          //   用空字符串替代。   
          return   this.replace(/(^\s*)|(\s*$)/g,   "");   
	};
	str = str.trim();
	return str.toLowerCase();
}

function doAction(){

	if(document.theForm.times.value.length == 0){
		alert("must input cycle times.");
		return;
	}
   if(document.theForm.pool.checked){
       document.theForm.usepool.value="1";
   }
   else{
       document.theForm.usepool.value="0";
   }
   if(formatStr(document.theForm.sql.value).search("^update") >= 0 ){
	   alert("Can't execute update operation.");
		return;
   }
   if( formatStr(document.theForm.sql.value).search("^delete") >= 0  ){
	   alert("Can't execute delete operation.");
		return;
   }
      if( formatStr(document.theForm.sql.value).search("^call") >= 0 ){
	   alert("Can't execute call operation.");
		return;
   }
      if( formatStr(document.theForm.sql.value).search("^exec") >= 0 ){
	   alert("Can't execute exec operation.");
		return;
   }
   document.theForm.submit();
   
}

</script>
</head>

<body>
<form name="theForm" method="post">
<input type="hidden" name="usepool" value="0">
<%String sql = (request.getParameter("sql") == null ? "" : request
				.getParameter("sql").trim());
		String usePool = "";
		String times = "";
		String alias = "";
		String result = "";
		if (sql.trim().length() > 0) {
			System.out.println();
			alias = request.getParameter("alias");
			usePool = request.getParameter("usepool");
			times = request.getParameter("times");
			result = dblink.linkCheck(alias, sql, usePool.equals("1"),Integer.parseInt(times),(String)(request.getLocalAddr()),(String)(session.getValue("OPERNAME")));
		}

		%>

<table border="0" cellPadding="5" cellSpacing="1" width="100%"
	height="95%">
	<tr>

		<td align="center">

		<table border="1" width="400" cellspacing="0" cellpadding="2"
			style=" font-size: 9pt" bgcolor="#FFFFFF">
			<tr>
				<td height="28" width="30%">module</td>

				<td height="28"><select name="alias">
					<%DBConnectionManager man = DBConnectionManager.getInstance();
		Object names[] = man.getAllPoolName();
		for (int i = 0; i < names.length; i++) {
			if (!alias.equals((String) names[i])) {
				out.println("<option value=" + names[i] + ">" + names[i]
						+ "</option>");
			} else {
				out.println("<option value=" + names[i] + " selected >"
						+ names[i] + "</option>");
			}
		}
		%>
				</select></td>
			</tr>

			<tr>
				<td height="28" width="30%">pattern</td>
				<td height="28"><input type="checkbox" name="pool"
					<%= usePool.equals("1")? "checked":"" %>>use db pool</td>
			</tr>

			<tr>
				<td height="28" width="30%">cycle</td>
				<td height="28"><input type="text" name="times" value="<%="".equals(times)?"1":times%>"> times</td>
			</tr>

			<tr>
				<td width="30%">SQL</td>

				<td><textarea name="sql" cols="80" rows="4" value=""><%= sql%></textarea></td>
			</tr>

			<tr>
				<td width="30%">result</td>
				<td><textarea name="result" cols="80" rows="4" value=""><%= result%></textarea></td>
			</tr>

			<tr>
				<td colspan="2" align="center">
					<input type="button" class="button"
					value="execute" onclick="return doAction();"> 
					<input type="button"
					class="button" value="reset" onclick="return clearAll();"></td>
			</tr>
		</table>
</form>
</body>

</html>

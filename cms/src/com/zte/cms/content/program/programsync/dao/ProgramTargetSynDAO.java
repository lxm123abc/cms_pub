
  package com.zte.cms.content.program.programsync.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ProgramTargetSynDAO extends DynamicObjectBaseDao implements IProgramTargetSynDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCntTargetsysSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("insert programTargetSyn starting...");
    	super.insert( "insertCntTargetsysSyn", programTargetSyn );
    	log.debug("insert programTargetSyn end");
    }
     
    public void updateCntTargetsysSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("update programTargetSyn by pk starting...");
       	super.update( "updateCntTargetsysSyn", programTargetSyn );
       	log.debug("update programTargetSyn by pk end");
    }

    public void updateCntTargetsysSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("update programTargetSyn by conditions starting...");
    	super.update( "updateCntTargetsysSynByCond", programTargetSyn );
    	log.debug("update programTargetSyn by conditions end");
    }   

    public void deleteCntBindTgsysSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("delete programTargetSyn by pk starting...");
       	super.delete( "deleteCntBindTgsysSyn", programTargetSyn );
       	log.debug("delete programTargetSyn by pk end");
    }
    
    public void deleteCntTargetsysSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("delete programTargetSyn by pk starting...");
       	super.delete( "deleteCntTargetsysSyn", programTargetSyn );
       	log.debug("delete programTargetSyn by pk end");
    }

    public void deleteCntTargetsysSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("delete programTargetSyn by conditions starting...");
    	super.delete( "deleteCntTargetsysSynByCond", programTargetSyn );
    	log.debug("update programTargetSyn by conditions end");
    } 
    
    public void deleteCntTargetsysSynByProgramindex( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("delete programTargetSyn by conditions starting...");
    	super.delete( "deleteCntTargetsysSynByProgramindex", programTargetSyn );
    	log.debug("update programTargetSyn by conditions end");
    }   

    public ProgramTargetSyn getProgramTargetSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn starting...");
       	ProgramTargetSyn resultObj = (ProgramTargetSyn)super.queryForObject( "getProgramTargetsysSyn",programTargetSyn);
       	log.debug("query programTargetSyn end");
       	return resultObj;
    }

    public ProgramTargetSyn getProgramTargetBindSyn( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn starting...");
       	ProgramTargetSyn resultObj = (ProgramTargetSyn)super.queryForObject( "getCntTargetsysBindSyn",programTargetSyn);
       	log.debug("query programTargetSyn end");
       	return resultObj;
    }   
    //按条件查询
    public int getCntTargetsysBindSynPub( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynPub",programTargetSyn);
       	log.debug("query programTargetSyn end");
       	return result;
    } 
    
    public int getCntTargetsysBindSynCnt( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynCnt",programTargetSyn);
       	log.debug("query programTargetSyn end");
       	return result;
    } 
    
    //完全发布成功
    public int getCntTargetsysBindSynCntPubSuccess( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynCntPubSuccess starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynCntPubSuccess",programTargetSyn);
       	log.debug("query getCntTargetsysBindSynCntPubSuccess end");
       	return result;
    }
    
	@SuppressWarnings("unchecked")
    public List<ProgramTargetSyn> getProgramTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn by condition starting...");
    	List<ProgramTargetSyn> rList = (List<ProgramTargetSyn>)super.queryForList( "getProgramTargetSynListByCond",programTargetSyn);
    	log.debug("query programTargetSyn by condition end");
    	return rList;
    }
	
	public PageInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DAOException
    {
        log.debug("page query programTargetSyn by condition starting...");
        PageInfo pageInfo = null;
        
        int totalCnt = ((Integer) super.queryForObject("getProgramTargetSynListByCondCount",  programTargetSyn)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "getProgramTargetSynListByCond" ,  programTargetSyn , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programTargetSyn by condition end");
        return pageInfo;
    }	
	
	@SuppressWarnings("unchecked")
    public List<ProgramTargetSyn> getProTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query programTargetSyn by condition starting...");
    	List<ProgramTargetSyn> rList = (List<ProgramTargetSyn>)super.queryForList( "getProTargetSynListByCond",programTargetSyn);
    	log.debug("query programTargetSyn by condition end");
    	return rList;
    }
	
    public List<ProgramTargetSyn> getCntTargetsysBindSynPubSuccess( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition starting...");
    	List<ProgramTargetSyn> rList = (List<ProgramTargetSyn>)super.queryForList( "getCntTargetsysBindSynPubSuccess",programTargetSyn);
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition end");
    	return rList;
    }
 
    public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV3( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition starting...");
    	List<ProgramTargetSyn> rList = (List<ProgramTargetSyn>)super.queryForList( "getProgramTargetSynByCondIPTV3",programTargetSyn);
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition end");
    	return rList;
    }
    
    public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV2( ProgramTargetSyn programTargetSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition starting...");
    	List<ProgramTargetSyn> rList = (List<ProgramTargetSyn>)super.queryForList( "getProgramTargetSynByCondIPTV2",programTargetSyn);
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition end");
    	return rList;
    }
    
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DAOException
    {
    	log.debug("page query programTargetSyn by condition starting...");
     	PageInfo pageInfo = null;
     	if (programTargetSyn.getTypetarget()== 0)
     	{
        	int totalCnt = ((Integer) super.queryForObject("queryProgramTarget2SynListCntByCond",  programTargetSyn)).intValue();
        	if( totalCnt > 0 )
        	{
        		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
        		List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryProgramTarget2SynListByCond" ,  programTargetSyn , start , fetchSize );
        		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        	}
        	else
        	{
        		pageInfo = new PageInfo();
        	}
     	}
     	if (programTargetSyn.getTypetarget()== 1)
     	{
    	int totalCnt = ((Integer) super.queryForObject("queryProgramTargetSynListCntByCond",  programTargetSyn)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryProgramTargetSynListByCond" ,  programTargetSyn , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
        	}
    	}
    	log.debug("page query programTargetSyn by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryProgramTargetSynListByCond", "queryCntTargetsysSynListCntByCond", programTargetSyn, start, pageSize, puEntity);
    }
    
    
    
} 
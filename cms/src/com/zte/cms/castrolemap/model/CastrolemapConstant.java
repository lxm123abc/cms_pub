package com.zte.cms.castrolemap.model;

public class CastrolemapConstant
{
    // mgttype 管理类型 角色管理

    public static final String MGTTYPE_CASTROLEMAP = "log.castrolemap.mgt";
    public static final String MGTTYPE_CASTROLEMAP_SYNC = "log.castrolemap.mgt_sync";

    public final static int CASTROLEMAP_STATUS_WAITPUBLISH = 0;// 待发布
    public final static int CASTROLEMAP_STATUS_ADDSYNCHRONIZING = 10;// 新增同步中
    public final static int CASTROLEMAP_STATUS_ADDSYNCHRONIZED_SUCCESS = 20;// 新增同步成功
    public final static int CASTROLEMAP_STATUS_ADDSYNCHRONIZED_FAIL = 30;// 新增同步失败
    // public final static int CASTROLEMAP_STATUS_UPDATESYNCHRONIZING= 40;//修改同步中
    // public final static int CASTROLEMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS = 50;//修改同步成功
    // public final static int CASTROLEMAP_STATUS_UPDATESYNCHRONIZED_FAIL = 60;//修改同步失败
    public final static int CASTROLEMAP_STATUS_CANCELSYNCHRONIZING = 70;// 取消同步中
    public final static int CASTROLEMAP_STATUS_CANCELSYNCHRONIZED_SUCCESS = 80;// 取消同步成功
    public final static int CASTROLEMAP_STATUS_CANCELSYNCHRONIZED_FAIL = 90;// 取消同步失败

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_CASTROLEMAP_ADD = "log.castrolemap.add";// 角色新增
    public static final String OPERTYPE_CASTROLEMAP_MOD = "log.castrolemap.modify";// 角色修改
    public static final String OPERTYPE_CASTROLEMAP_DEL = "log.castrolemap.delete";// 角色删除
    public static final String OPERTYPE_CASTROLEMAP_PUBLISH = "log.castrolemap.publish";// 角色同步
    public static final String OPERTYPE_CASTROLEMAP_CANCELPUBLISH = "log.castrolemap.cancelpublish";// 角色取消同步
    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_CASTROLEMAP_ADD_INFO = "log.castrolemap.add.info";
    public static final String OPERTYPE_CASTROLEMAP_MOD_INFO = "log.castrolemap.modify.info";
    public static final String OPERTYPE_CASTROLEMAP_DEL_INFO = "log.castrolemap.delete.info";
    public static final String OPERTYPE_CASTROLEMAP_PUBLISH_INFO = "log.castrolemap.publish.info";
    public static final String OPERTYPE_CASTROLEMAP_CANCELPUBLISH__INFO = "log.castrolemap.cancelpublish.info";
    
    public static final String OPERTYPE_PLATFORM_CRM_BIND_ADD = "log.platform.crm.bind.add";
    public static final String OPERTYPE_PLATFORM_CRM_BIND_ADD_INFO = "log.platform.crm.bind.add.info";
    public static final String OPERTYPE_TARGET_CRM_BIND_ADD = "log.target.crm.bind.add";
    public static final String OPERTYPE_TARGET_CRM_BIND_ADD_INFO = "log.target.crm.bind.add.info";

    public static final String OPERTYPE_PLATFORM_CRM_BIND_DELETE = "log.platform.crm.bind.delete";
    public static final String OPERTYPE_PLATFORM_CRM_BIND_DELETE_INFO = "log.platform.crm.bind.delete.info";
    public static final String OPERTYPE_TARGET_CRM_BIND_DELETE = "log.target.crm.bind.delete";
    public static final String OPERTYPE_TARGET_CRM_BIND_DELETE_INFO = "log.target.crm.bind.delete.info";
}

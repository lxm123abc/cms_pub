package com.zte.cms.content.apply;

/**
 * <p>
 * 文件名称: ICntApply.java
 * </p>
 * <p>
 * 文件描述: 内容提审接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容订单批量提交审核、批量审核接口
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：2011年06月10日
 *    版 本 号：ZXIN10 CMSV2.01.03
 *    修 改 人：李梅
 *    修改内容：按照编程规范对代码进行了优化
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public interface ICntApply
{

    /**
     * <p>
     * 批量提交审核，发起新增申请工作流
     * </p>
     * 
     * @param cntorderList 订单编号列表
     * @return 返回工作流新增申请结果，类型String，显示资源文件信息
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.cntorder
     * @see com.zte.cms.cntorder.model.Cntorder
     * @since ZXIN10 CMSV2.01.01
     */
    public String runApply(String programid) throws Exception;

    /**
     * <p>
     * 批量审核待处理的内容订单新增申请
     * </p>
     * 
     * @param cntorderList 订单编号列表
     * @param taskidList 任务ID列表
     * @param wkfwParam 审核信息，包括审核意见（不可为空）、审核结果（通过/不通过，不可为空）
     * @return 返回审核工单结果，类型String，显示资源文件信息
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.cntorder
     * @see com.zte.cms.cntorder.model.Cntorder
     * @since ZXIN10 CMSV2.01.01
     */
    public String runAudit(String[] wkfwParam) throws Exception;

}

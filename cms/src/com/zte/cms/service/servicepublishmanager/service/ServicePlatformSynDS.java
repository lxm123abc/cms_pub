package com.zte.cms.service.servicepublishmanager.service;

import java.util.List;

import com.zte.cms.service.servicepublishmanager.dao.IServicePlatformSynDAO;
import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class ServicePlatformSynDS implements IServicePlatformSynDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IServicePlatformSynDAO dao = null;
	
	public void setDao(IServicePlatformSynDAO dao)
	{
	    this.dao = dao;
	}

    public List<ServicePlatformSyn> getCntTargetsysSynByCond(ServicePlatformSyn servicePlatformSyn)
            throws DomainServiceException
    {
        log.debug("get ServicePlatformSyn list by condition starting...");
        List<ServicePlatformSyn> rsList = null;
        try {
            rsList = dao.getServicePlatformSynByCond(servicePlatformSyn);
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get ServicePlatformSyn list by condition end");
        return rsList;
    }


    public TableDataInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug( "get servicePlatformSyn page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(servicePlatformSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePlatformSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get servicePlatformSyn page info by condition end" );
        return tableInfo;
    }


    public TableDataInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        return null;
    }


    public String updateServicePlatformSyn(ServicePlatformSyn servicePlatformSyn) throws DomainServiceException
    {

        log.debug("update servicePlatformSyn by pk starting...");
        try {
            dao.updateServicePlatformSyn(servicePlatformSyn);
           } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update servicePlatformSyn by pk end");
          return "DS success";
    }

    public ServicePlatformSyn getServicePlatformSynByIndex (ServicePlatformSyn servicePlatformSyn)
            throws DomainServiceException
    {
        log.debug("get servicePlatformSyn by pk start...");        
        ServicePlatformSyn rsObj = new ServicePlatformSyn();
        try
        {
            rsObj =  dao.getServicePlatformSynByIndex(servicePlatformSyn);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       
        log.debug("get servicePlatformSyn by pk end...");
        return rsObj;
    }

    public TableDataInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws DomainServiceException
    {

        log.debug( "pageInfoQueryforServiceProgramBing starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryforServiceProgramBing(servicePlatformSyn, start, pageSize);
            
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePlatformSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "pageInfoQueryforServiceProgramBing end..." );
        return tableInfo;
    
    }

    public TableDataInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws DomainServiceException
    {

        log.debug( "pageInfoQueryforServiceSeriesBing starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryforServiceSeriesBing(servicePlatformSyn, start, pageSize);
            
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePlatformSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "pageInfoQueryforServiceSeriesBing end..." );
        return tableInfo;
    
    }

    public TableDataInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws DomainServiceException
    {


        log.debug( "pageInfoQueryforChannelSeriesBing starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryforServiceChannelBing(servicePlatformSyn, start, pageSize);
            
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePlatformSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "pageInfoQueryforChannelSeriesBing end..." );
        return tableInfo; 
    }

    public List<ServicePlatformSyn> getSameParentPlatformSyn(ServicePlatformSyn servicePlatformSyn)
            throws DomainServiceException
    {
        log.debug("get getSameParentPlatformSyn list by condition starting...");
        List<ServicePlatformSyn> rsList = null;
        try {
            rsList = dao.getSameParentPlatformSyn(servicePlatformSyn);
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getSameParentPlatformSyn list by condition end");
        return rsList;
    }


}

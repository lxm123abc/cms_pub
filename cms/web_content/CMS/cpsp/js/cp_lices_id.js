var specialStr="!@#$%^&*;|?()[]{}<>\'\"";
function isNumFormat(input,input_msg)
{
	if(trim(input.value)!=''&&input.value!=null)
	{
		var strRegex=/^d*$/;
		var exp=new RegExp(strRegex);
		var reg=input.value.match(exp);
		if(reg==null)
		{
			document.getElementById(input_msg).innerText="请输入长度为8位的数字";
			document.getElementById(input.id).focus();
			return false;
		}
		else
		{
			document.getElementById(input_msg).innerText = "";   
        	return true; 
		}
	}
}


//验证是否为FTP格式
function isFTPFormat(input,input_msg)
{
	if(trim(input.value) != '' && input.value != null)
	{
		
    	var strRegex = /^(ftp|FTP):\/\/.*$/; 
    	var exp=new RegExp(strRegex);
    	var inputvalue=trim(input.value.toLowerCase());
    	var reg = inputvalue.match(exp);
    	if(reg == null)
    		{
        		document.getElementById(input_msg).innerText = "FTP格式不正确"; //"FTP格式不正确";
        		document.getElementById(input.id).focus();
        		return false;      
    		}
    	else
    		{
       	 		document.getElementById(input_msg).innerText = "";   
        		return true;   
   	 		} 
    	
	}
	else
	{
		return true;
	}
}

//验证是否为URL格式
function isURLFormat(input,input_msg)
{
	if(trim(input.value) != '' && input.value != null)
	{
		
    	var strRegex =  /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/; 
    	var exp=new RegExp(strRegex);
    	var inputvalue=trim(input.value.toLowerCase());
    	var reg = inputvalue.match(exp);
    	if(reg == null)
    		{
        		document.getElementById(input_msg).innerText = "URL格式不正确,输入格式为 http://ss.cn"; //"URL格式不正确";
        		document.getElementById(input.id).focus();
        		return false;      
    		}
    	else
    		{
       	 		document.getElementById(input_msg).innerText = "";   
        		return true;   
   	 		} 
    	
	}
	else
	{
		return true;
	}
}

//验证是否为电话号码，长度是否大于6位
function isPhoneFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    /*if(trim(input.value).length < 6){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.2"); //"长度必须6位以上";
        document.getElementById(input.id).focus();
        return false;
    }*/
    if(validMobile(input.value.trim()) == false){
        document.getElementById(input_msg).innerText = "请输入7到20位数字"; //"只可包含-,()和数字";
        document.getElementById(input.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true; 
    }  
  }
  else
  {
		return true;
  }
}
function validMobile(str)
{
    if(str.length > 20 || str.length < 7)
    {
        return false;
    }
    else
    {
        var regex=/^[0-9]*$/;
        return regex.test(str);
    }
}

function isphone(str)
{
    var number_chars = "-1234567890";
    var i;
    for (i=0;i<str.length;i++)
    {
    if (number_chars.indexOf(str.charAt(i))==-1) return false;
    }
    return true;
}
//验证是否为email格式
function ismailFormat(input,input_msg) 
{ 
  if(trim(input.value) != '' && input.value != null){
    //email格式
    var exp = new RegExp("[a-z0-9-_]+@[a-z0-9-_]+[\.]{1}(com|net|bta|cn|org|edu|mil)","gi"); 
    var reg = input.value.match(exp);
    if(reg == null){
        document.getElementById(input_msg).innerText = "email格式不正确"; //"email格式不正确";
        document.getElementById(input.id).focus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = ""; 
        return true;     
    }  
  }
  else
  {
  	return true;
  }
}

//判断不可为空
function inputIsNull(input,input_msg)
{
	if(trim(input.value) == '' || input.value == null){
        document.getElementById(input_msg).innerText ="此项不能为空"; //"此项不可为空";
        document.getElementById(input.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";
        return true;
    }
}


//只能为数字
function checkQueryNum(input,input_msg)
{
	
	if(trim(input.value)!=''&&input.value!=null)
	{
		var strRegex=/^\d+$/;
		var exp=new RegExp(strRegex);
		var reg=input.value.match(exp);
		if(reg==null)
		{
			document.getElementById(input_msg).innerText="请输入若干长度的数字";
			//document.getElementById(input.id).focus();
			return false;
		}
		else
		{
			document.getElementById(input_msg).innerText = "";   
        	return true; 
		}
	}
}

//验证输入框中字数是否超过最大长度
function IsMaxLength(input,input_msg,len){
	
  if(utf8_strlen2(trim(input.value)) > len){
      document.getElementById(input_msg).innerText = "此项长度不能超过"+len+"个字符（一个汉字占3个字符）";//"超过最大长度限制:";
     // document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}

function transforCPStatus(status)
{
	var value;
	if(status=="0")
	{
		value="未使用";
	}
	else if(status=="1")
	{
		value="已使用"
	}
	return value;
}

function transforCPType(type)
{
	var value;
	if(type=="1")
	{
		value="CPID";
	}
	else if(type=="2")
	{
		value="牌照方ID";
	}
	return value;
}

function cppositionConver(keyvalue){
	var cppositionArr =keyvalue["cpposition"].split("|");
	var cpposition="";
	var num=0;
	for(var i=0; i<cppositionArr.length;i++){
		if(num==0){
			if(cppositionArr[i]=="PC"){
				cpposition ="互联网视讯"
			}else if(cppositionArr[i]=="CE"){
				cpposition ="天翼视讯"
			}else if(cppositionArr[i]=="TV"){
				cpposition ="IPTV"
			}
		}else{
			if(cppositionArr[i]=="CE"){
				cpposition =cpposition+" | 天翼视讯"
			}else if(cppositionArr[i]=="TV"){
				cpposition =cpposition+" | IPTV"
			}
		}
		num++;
	}
	keyvalue["cpposition"]=cpposition;
	return keyvalue;
}



function BatchResult(result,size)
{

	var wheight= 200 ;
	var wwidth = 400;
	if((result=="undefined")||!result)
	{
		result="系统错误！";
	}
	if(size)
	{
		wheight= 200 + (size)* 14;
	}
	openMsgBoxSize(result,wwidth,wheight);

}
function utf8_strlen2(str) 
{ 
    var cnt = 0; 
    for( i=0; i<str.length; i++) 
    { 
        var value = str.charCodeAt(i); 
        if( value < 0x080) 
        { 
            cnt += 1; 
        } 
        else if( value < 0x0800) 
        { 
            cnt += 2; 
        } 
        else  if(value < 0x010000)
        { 
            cnt += 3; 
        }
		else
		{
			cnt += 4;
		}
    } 
    return cnt; 
}


    function setButtonState(btnID,state)
    {
    		if(!state)
    		{
				document.getElementById(btnID).style.display="none";
    		}
    }
    
    
var historyStr ="";
function checkMaxInput(_this,maxleng,leftInforId) 
{ 
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 
    var remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum; 
    if(remainnum < 0) 
    { 
        if(_this.value.length!=len) 
        { 
            if((len-_this.value.length)>(maxleng/3)) 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng/3); 
            } 
            else 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng-(len-_this.value.length)); 
            } 
        }else{ 
            _this.value=_this.value.substring(0,maxleng); 
        } 
            left.value=0; 
            return; 
    } 
    historyStr = _this.value;
}


function calSigType(keyValue)
{
	var type=keyValue['signaltype'];
	if(type=="1")
	{
		type="rtsp";
	}
	if(type=="2")
	{
		type="http";
	}
	if(type=="3")
	{
		type="UDP";
	}
	if(type=="4")
	{
		type="SDI";
	}
	keyValue['signaltype']=type;
	return keyValue;
}
function calSigStatus(keyValue)
{
	var status=keyValue['status'];
	if(status=="0")
	{
		status="正常";
	}
	if(status=="1")
	{
		status="失效";
	}
	keyValue['status']=status;
	return keyValue;
}
function calSigShd(keyValue)
{
	var sigishd=keyValue['sigishd'];
	if(sigishd=="0")
	{
		sigishd="标清";
	}
	if(sigishd=="1")
	{
		sigishd="高清";
	}
	keyValue['sigishd']=sigishd;
	return keyValue;
}



function checkSpcialStrMsgByConfig(checkstr,msgBox_id,specialStrParam,configMsg)
{
	var valSpecialStr=specialStr;
	if(specialStrParam!="undefined"&&specialStrParam!= null)
	{
		valSpecialStr=specialStrParam;
	}
	var allValid = true;
	for (i = 0;i<valSpecialStr.length;i++)
	{
		ch = valSpecialStr.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = false;
			break;
	 	}
  	}
	if(!allValid)
	{
		document.getElementById(msgBox_id).innerText=configMsg;
	}
	return allValid;
}

function checkSpcialStr(checkstr,msgBox_id,specialStrParam)
{
	var valSpecialStr=specialStr;
	if(specialStrParam!="undefined"&&specialStrParam!= null)
	{
		valSpecialStr=specialStrParam;
	}
	var allValid = true;
	for (i = 0;i<valSpecialStr.length;i++)
	{
		ch = valSpecialStr.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = false;
			break;
	 	}
  	}
	if(!allValid)
	{
		document.getElementById(msgBox_id).innerText="此项不能包含以下特殊符号 "+valSpecialStr;
	}
	return allValid;
}


function selectDynDistribute()
{
	$(".selectedClass").change(function(){
		//alert($(this).attr("id"));
		dynChgSelectWidth($(this).attr("id"));
	});
}

function resetSelectChange()
{
	$(".selectedClass").each(function(){
		dynChgSelectWidth($(this).attr("id"));
	});
}

function dynChgSelectWidth(id)
{
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width = getCharWidth(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}

function getTime(str){  
    if(str!=null){
    	if(str.length==4){
    		return str.substr(0,4);
    	}
    	if(str.length == 8){
	    	return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    }
	    
	    if (str.length == 10){ 
	        return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2)+ " " + str.substr(8,2);
	    }
	    
	    if(str.length ==14){
	       return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	    }else{
	        return "";
	    }  
    }else{
        return "";
    }
}
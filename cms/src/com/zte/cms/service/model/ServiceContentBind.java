package com.zte.cms.service.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServiceContentBind extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.String serviceid;
    private java.lang.String servicename;
    private java.lang.String contentid;
    private java.lang.String namecn;
    private java.lang.String createtime;
    private java.lang.Integer status;

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("serviceid", "SERVICEID");
        this.addRelation("servicename", "SERVICENAME");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("status", "STATUS");

    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public java.lang.String getServicename()
    {
        return servicename;
    }

    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

}

package com.zte.cms.channel.wkfw.service;

import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IChannelWkfwTaskForMeDS
{

    public abstract int queryChannelWkfwTaskForMeListCntByCond(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start,
            int pageSize) throws DomainServiceException;

    @SuppressWarnings("unchecked")
    public abstract TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

}
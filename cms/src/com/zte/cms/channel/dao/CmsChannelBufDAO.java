package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.ICmsChannelBufDAO;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsChannelBufDAO extends DynamicObjectBaseDao implements ICmsChannelBufDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException
    {
        log.debug("insert cmsChannelBuf starting...");
        super.insert("insertCmsChannelBuf", cmsChannelBuf);
        log.debug("insert cmsChannelBuf end");
    }

    public void updateCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException
    {
        log.debug("update cmsChannelBuf by pk starting...");
        super.update("updateCmsChannelBuf", cmsChannelBuf);
        log.debug("update cmsChannelBuf by pk end");
    }

    public void deleteCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException
    {
        log.debug("delete cmsChannelBuf by pk starting...");
        super.delete("deleteCmsChannelBuf", cmsChannelBuf);
        log.debug("delete cmsChannelBuf by pk end");
    }

    public CmsChannelBuf getCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException
    {
        log.debug("query cmsChannelBuf starting...");
        CmsChannelBuf resultObj = (CmsChannelBuf) super.queryForObject("getCmsChannelBuf", cmsChannelBuf);
        log.debug("query cmsChannelBuf end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsChannelBuf> getCmsChannelBufByCond(CmsChannelBuf cmsChannelBuf) throws DAOException
    {
        log.debug("query cmsChannelBuf by condition starting...");
        List<CmsChannelBuf> rList = (List<CmsChannelBuf>) super.queryForList("queryCmsChannelBufListByCond",
                cmsChannelBuf);
        log.debug("query cmsChannelBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsChannelBuf by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsChannelBufListCntByCond", cmsChannelBuf)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsChannelBuf> rsList = (List<CmsChannelBuf>) super.pageQuery("queryCmsChannelBufListByCond",
                    cmsChannelBuf, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsChannelBuf by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsChannelBufListByCond", "queryCmsChannelBufListCntByCond", cmsChannelBuf,
                start, pageSize, puEntity);
    }

}
package com.zte.cms.avtemplate.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 目标系统的LS层
 * 
 * @author Administrator
 * 
 */
public interface ITemplateLS
{

    public abstract CmsAvtemplateinfo getTemplate(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;

    public abstract TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize)
            throws DomainServiceException;

    public abstract TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public String insertTemplate(CmsAvtemplateinfo template) throws DomainServiceException;

    public String updateTemplate(CmsAvtemplateinfo templateinfo) throws DomainServiceException;

    public String removeTemplate(Long templateindex) throws DomainServiceException;

    public String checkExist(Long templateindex) throws DomainServiceException;

    public Map<String, String> getTemplateMap(int servicetype, int templatetype);

    public Map getTemplate(int servicetype, int templatetype, boolean needSelect, String defaultvalue) throws Exception;

    public String checkExistByName(String templatename) throws DomainServiceException;
}
package com.zte.cms.basicdata.dao;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsProgramtypecpMapDAO
{
    public CmsProgramtypecpMap getCmsProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap) throws DAOException;

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapByCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DAOException;

    public PageInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

    public PageInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DAOException;

    public PageInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DAOException;

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap);

    public void insertProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap);

    public void deleteProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap);

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DAOException;
}
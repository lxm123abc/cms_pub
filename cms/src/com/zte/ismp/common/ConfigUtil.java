package com.zte.ismp.common;

import java.util.Hashtable;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ConfigUtil
{

    /** 成功标志 */
    public static final String SUCCESS_FLAG = "0";
    public static final int SUCCESS_FLAG_INT = 0;

    /** 失败标志 */
    public static final String FAILURE_FLAG = "1";
    public static final int FAILURE_FLAG_INT = 1;

    /** 默认配置文件地址。 */
    public static final String DEFAULT_CONFIG_FILE = "app_config.xml";
    /** 默认配置文件地址头。 */
    public static final String DEFAULT_CONFIG_FILE_HEAD = "msg_config";

    private static Hashtable<String, String> hashtable = new Hashtable<String, String>();

    /**
     * 取根结点。
     * 
     * @return 根节点。如果找不到配置文件，返回 null 。
     * @version 直接使用 URL 对象打开输入流进行读取。
     */
    private static Element getRoot()
    {

        Element root = null;
        try
        {
            java.net.URL confURL = ConfigUtil.class.getClassLoader().getResource(DEFAULT_CONFIG_FILE);
            String path = confURL.getPath();
            if ((null != path) && (path.trim().length() > 0))
            {
                SAXBuilder dom = new SAXBuilder();
                Document doc = dom.build(confURL.openStream());
                root = doc.getRootElement();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return root;
    }

    /**
     * 加载存放公共属性的哈希表
     */
    @SuppressWarnings("unchecked")
    private static void loadHashtable()
    {
        Element root = getRoot();
        List<Element> elements = (List<Element>) root.getChildren("info");

        if (elements != null)
        {
            for (int i = 0; i < elements.size(); i++)
            {
                Element element = (Element) elements.get(i);
                String name = element.getAttributeValue("name").toLowerCase();
                String value = element.getAttributeValue("value");
                hashtable.put(name, value);
            }
        }
    }

    /**
     * 取得配置文件ad_config.xml中节点名为name的value值
     * 
     * @param name ：节点名称 ，对大小写不敏感
     * @return ：节点的值
     */
    public static String getConfigValue(String name)
    {
        String value = "";

        if ((hashtable == null) || (hashtable.size() == 0))
        {
            loadHashtable();
        }
        if (hashtable != null || hashtable.size() == 0)
        {
            loadHashtable();
            value = (String) hashtable.get(name.toLowerCase());
        }

        return value;
    }

    /**
     * 重新加载配置文件
     */
    public static void reLoad()
    {
        hashtable = new Hashtable<String, String>();
        loadHashtable();
        /*
         * Enumeration key = ht.keys(); while(key.hasMoreElements()){
         * 
         * System.out.println((String) ht.get((String)key.nextElement())); }
         */

    }

    /**
     * 取得配置文件app_config.xml中节点名为name的value值
     * 
     * @param name ：节点名称 ，对大小写不敏感
     * @return ：节点的值
     */
    public static String get(String name)
    {
        String value = "";

        if ((hashtable == null) || (hashtable.size() == 0))
        {
            loadHashtable();
        }
        if (hashtable != null || hashtable.size() == 0)
        {
            loadHashtable();
            value = (String) hashtable.get(name.toLowerCase());
        }

        return value;
    }

}


function appendParamHTMLByMethod(jsObj) {
	var paramObj = document.getElementById("sltParam");
	paramObj.options.length = 0;
	if (isEmpty(jsObj) || isEmpty(jsObj.rtnValue)) {
		alert("没有找到暴露的服务"); return;
	}
	jsObj = jsObj.rtnValue;
	for (var obj in jsObj) {
		paramObj.options.add(new Option(jsObj[obj], obj));
	}
}

function newQueryProp(showclear, showcreate, pcheck, pname, pvalue, ptype) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr,myTh,myTd;
	myTr = queryBodyObj.insertRow(queryBodyObj.rows.length - 1);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "对象名称";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pname\" value='" + nvl(pname, true) + 
		"' style=\"vertical-align:middle\" />" + 
		"<input type=\"radio\" name=\"qrychk\" style=\"vertical-align:middle\" title=\"isKey\"/>";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showclear ? 
		"<a href=\"#\" onclick=\"clearQueryProp(this.parentNode.parentNode.rowIndex)\"> 删除 </a>" : "");
	myTr.appendChild(myTd);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "值";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pvalue\" value='" + nvl(pvalue, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showcreate ? 
		"<a href=\"#\" onclick=\"hrefJsonStr(this,'" +  nvl(ptype) + "')\"> 生成值 </a>" : "");
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 3);
	myTd.innerHTML = "<input type=\"checkbox\" id=\"pcheck\"" + (!bol(pcheck) ? "" : " checked") + 
		"> 是否为参数 </input>";
	myTr.appendChild(myTd);
}

function buildQueryHTMLByParam(jsObj) {
	jsObj = jsObj.substring(jsObj.indexOf("(") + 1, jsObj.length - 1);
	if (isEmpty(jsObj)) return;
	var jsObj = jsObj.split(",");
	for (var i=0; i<jsObj.length; i++) newQueryProp(false, true, true, "arg" + i, "", jsObj[i]);
}

function buildQueryHTMLByClass(jsObj, valObj) {
	if (isEmpty(jsObj) || isEmpty(jsObj.rtnValue)) {
		alert("没有找到相应的实体"); return;
	}
	jsObj = jsObj.rtnValue;
	var queryBodyObj = document.getElementById("queryBody");
	var myTr, myTh, myTd;
	var cname,ctype;
	var i = 0;
	myTr = document.createElement("tr");
	for (var obj in jsObj) {
		if (obj == "class" || obj == "orderCond" || obj == "relationMap") continue;
		cname = obj;
		ctype = jsObj[obj];
		myTh = document.createElement("th");
		myTh.innerHTML = cname + "<label>(" + getTypeSymbol(ctype) + ")</label>";
		myTr.appendChild(myTh);
		myTd = document.createElement("td");
		myTd.innerHTML = "<input type=\"text\" id=\"txt" + cname.charAt(0).toUpperCase() + cname.substr(1) + 
			"\" value='" + ((valObj == undefined || valObj[cname] === undefined) ? 
					getTypeDefVal(ctype) : createJsonStr(valObj[cname], true)) + 
			"' defval='" + getTypeDefVal(ctype) + 
			"' style=\"vertical-align:middle\" />" +
			"<input type=\"radio\" name=\"qrychk\" style=\"vertical-align:middle\" title=\"isKey\"/>";
		myTr.appendChild(myTd);
		myTd = document.createElement("td");
		myTd.innerHTML = "<a href=\"#\" onclick=\"hrefJsonStr(this,'" + ctype + "')\"> 生成值 </a>";
		myTr.appendChild(myTd);
		i++;
		if (i % 3 == 0) {queryBodyObj.appendChild(myTr); myTr = document.createElement("tr");}
	}
	if (i % 3 != 0) queryBodyObj.appendChild(myTr);
}

function buildQueryHTMLByTable(jsObj) {
	if (isEmpty(jsObj) || isEmpty(jsObj.rtnValue)) {
		alert("没有找到相应的表"); return;
	}
	jsObj = jsObj.rtnValue;
	var queryBodyObj = document.getElementById("queryBody");
	var myTr, myTh, myTd;
	var cname,cstatus,cdefault,ctype,clength;
	var count = jsObj.length;
	myTr = document.createElement("tr");
	for (var i=0; i<count; i++) {
		cname = jsObj[i]["cname"]; cstatus = jsObj[i]["cstatus"]; cdefault = jsObj[i]["cdefault"];
		ctype = jsObj[i]["ctype"] ; clength = (ctype == "char" ? 19 : jsObj[i]["clength"]);
		myTh = document.createElement("th");
		myTh.innerHTML =  cname + "<label>(" + getTypeSymbol(ctype, cstatus) + ")</label>";
		myTr.appendChild(myTh);
		myTd = document.createElement("td");
		myTd.innerHTML = "<input type=\"text\" id=\"txt" + cname.charAt(0).toUpperCase() + cname.substr(1) + 
			"\" value='" + getTypeDefVal(ctype, cdefault) + 
			"' defval='" + getTypeDefVal(ctype, cdefault) +
			"' maxlength=\"" + clength + "\"  datatype=\"" + ctype + 
			"\" style=\"vertical-align:middle\" />" +
			"<input type=\"radio\" name=\"qrychk\" style=\"vertical-align:middle\" title=\"isKey\"/>";
		myTr.appendChild(myTd);
		myTd = document.createElement("td");
		myTd.innerHTML = "<a href=\"#\" onclick=\"hrefJsonStr(this,'" + ctype + "')\"> 生成值 </a>";
		myTr.appendChild(myTd);
		if ((i+1) % 3 == 0) {queryBodyObj.appendChild(myTr);myTr = document.createElement("tr");}
	}
	if (i % 3 != 0) queryBodyObj.appendChild(myTr);
}

function hrefJsonStr(href, ptype) {
    var obj = {};
   
	obj.pvalue = href.parentNode.previousSibling.firstChild.value;
	obj.ptype = ptype;
    var retObj = showModalDialog("./xpop.html", obj, 
		"dialogWidth:1152px;dialogHeight:640px;scrolling:yes;toolbar:no;location:no;" +
		"directories:no;status:no;menubar:no;scrollbars:yes;resizable:yes");
	if (retObj == undefined) return;
	
	href.parentNode.previousSibling.firstChild.value = createJsonStr(retObj);
}

function buildQueryButton() {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = 
		"<input type=\"button\" class=\"inputButton\" value=\" 创 建 对 象 \" onclick=\"newQueryProp(true, true, false)\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 清 空 \" onclick=\"doClear()\" />" + 
		"<input type=\"button\" class=\"inputButton\" value=\" 新 增 \" onclick=\"doInsert()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 主 键 查 询 \" onclick=\"doGetByPk()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 条 件 查 询 \" onclick=\"doGetByCond()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 分 页 查 询 \" onclick=\"doPage(1)\" />" + 
		"<input type=\"button\" class=\"inputButton\" value=\" 保 存 文 件\" onclick=\"parent.frames['xtree'].saveFile('service')\" />";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildUserDefinedButton() {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = 
		"<input type=\"button\" class=\"inputButton\" value=\" 调 用 服 务 \" onclick=\"doGetByUserDefined()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 保 存 文 件\" onclick=\"parent.frames['xtree'].saveFile('service')\" />";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildReturnButton(pcheck, ptype) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = (ptype == undefined ? "" :
		"<input type=\"button\" class=\"inputButton\" value=\" 创 建 对 象 \" " + 
		"onclick=\"newQueryProp(true, true, " + pcheck + ", '" + ptype + "','','" + ptype + "')\" />") +
		"<input type=\"button\" class=\"inputButton\" value=\" 清 空 \" onclick=\"doClear()\" />" + 
		"<input type=\"button\" class=\"inputButton\" value=\" 确 定 \" onclick=\"doReturn()\" />";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildTableHTML(jsObj,start,pageSize,orderByColumn,isAsc) {
	clearTableHTML();
	if (isEmpty(jsObj) || isEmpty(jsObj.rtnValue)) {
		$("#xtable").contents().find("#xtable").html("没有返回结果");
		return;
	}
	jsObj = jsObj.rtnValue;
	jsObj = getObjectArray(jsObj, "data");
	var totalCount = jsObj.length;
	if (totalCount == 0) {
		$("#xtable").contents().find("#xtable").html("没有满足条件的记录");
		return;
	}
	var pageCount = 0;
	var currentPage = 0;
	if (pageSize != 0) {
		pageCount = Math.floor(totalCount / pageSize) + (totalCount % pageSize == 0 ? 0 : 1);
		currentPage = Math.floor(start / pageSize) + 1;
	}
	var count = jsObj.length;
	var xtabTr = new Array(count + 1);
	xtabTr[0] = "<th width=\"45px\"><input type=\"checkbox\" id=\"tabchk\"/ onclick=\"parent.doSelectAll()\"></th>";
	for (var i=1; i<count+1; i++) {
		xtabTr[i] = "<td align=\"center\"><input type=\"checkbox\" id=\"trchk\"/></td>";
	}
	for (var obj in jsObj[0]) {
		if (obj == "class" || obj == "orderCond") {
			continue;
		}
		if (pageSize != 0) {
			xtabTr[0] += "<th><a href=\"#\" onclick=\"parent.doPageOrder('" + currentPage + 
				"','" + obj + "'," + (orderByColumn == obj ? !isAsc : true) + ")\">" + 
				obj + "(" + (orderByColumn == obj ? "*" : "") + (isAsc ? "A" : "D") + ")</a></th>";
		}
		else {
			xtabTr[0] += "<th>" + obj + "</th>";
		}
		for (var i=1; i<count+1; i++) {
			xtabTr[i] += "<td><input type=\"text\" id=\"txt" + obj.charAt(0).toUpperCase() + obj.substr(1) + 
				"\" value='" + createJsonStr(jsObj[i-1][obj], true, "class") + 
				"' style=\"border-style: solid; border-width: 0\"/></td>";
		}
	}
	var xtab = "<table border = 1>";
	for (var i=0; i<count+1; i++) {
		xtab += "<tr>" + xtabTr[i] + "</tr>";
	}
	xtab += "</table>";
	$("#xtable").contents().find("#xtable").html(xtab);
	buildActorButton(totalCount,pageSize,pageCount,currentPage,orderByColumn,isAsc);
}

function getActorName(page,orderByColumn,isAsc) {
	if (orderByColumn == null) {
		return "doPage(" + page + ")";
	}
	else {
		return "doPageOrder(" + page + ",'" + orderByColumn + "'," + isAsc + ")";
	}
}

function buildActorButton(totalCount,pageSize,pageCount,currentPage,orderByColumn,isAsc) {
	var tableBodyObj = document.getElementById("tableBody");
	var myTr = tableBodyObj.insertRow();
	var myTd = myTr.insertCell();
	myTd.setAttribute("align", "left");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 修 改 \" onclick=\"doUpdate()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 批 量 修 改 \" onclick=\"doUpdateList()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 删 除 \" onclick=\"doRemove()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 批 量 删 除 \" onclick=\"doRemoveList()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 复 制 \" onclick=\"doCopyToClipBoard()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 批 量 复 制 \" onclick=\"doCopyListToClipBoard()\" />";
	myTd = myTr.insertCell();
	myTd.setAttribute("align", "right");
	myTd.setAttribute("class", "toolbar");
	var sHTML = "totalCount:<span>" + totalCount + "</span>&nbsp;&nbsp;" + (pageSize == 0 ? "" : 
		("pageSize:<select id=\"pageSize\" onchange=\"" + getActorName(currentPage,orderByColumn,isAsc) + 
		"\" style=\"vertical-align: middle; border-style: solid; border-width: 0; width: 45px\">" +
		"<option value=5" + (pageSize==5 ? " selected" : "") + ">5</option>" +
		"<option value=10" + (pageSize==10 ? " selected" : "") + ">10</option>" + 
		"<option value=15" + (pageSize==15 ? " selected" : "") + ">15</option></select>&nbsp;&nbsp;" + 
		"currentPage:<span>" + currentPage + "</span>/<span>" + pageCount + "</span>&nbsp;&nbsp;" + 
		"<input type=\"button\" class=\"pageNav firstPage\" onclick=\"" + 
		getActorName(1,orderByColumn,isAsc) + "\"/>" +
		"<input type=\"button\" class=\"pageNav prevPage\" onclick=\"" + 
		getActorName(currentPage <= 1 ? 1 : currentPage-1,orderByColumn,isAsc) + "\"/>" +
		"<input type=\"text\" id=\"toPage\" value=\"" + currentPage + 
		"\" style=\"vertical-align: middle; border-style: solid; border-width: 1; width: 20px\"/>&nbsp;" + 
		"<input type=\"button\" class=\"showPage\" value=\"Go\" onclick=\"" +
		getActorName("this.previousSibling.previousSibling.value",orderByColumn,isAsc) + "\"/>" + 
		"<input type=\"button\" class=\"pageNav nextPage\" onclick=\"" + 
		getActorName(currentPage >= pageCount ? pageCount : currentPage+1,orderByColumn,isAsc) + "\"/>" +
		"<input type=\"button\" class=\"pageNav lastPage\" onclick=\"" + 
		getActorName(pageCount,orderByColumn,isAsc) + "\"/>"));
	myTd.innerHTML = sHTML;
}
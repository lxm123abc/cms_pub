package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.dao.IServiceSeriesMapDAO;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceSeriesMapDAO extends DynamicObjectBaseDao implements IServiceSeriesMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException
    {
        log.debug("insert serviceSeriesMap starting...");
        super.insert("insertServiceSeriesMap", serviceSeriesMap);
        log.debug("insert serviceSeriesMap end");
    }

    public void updateServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException
    {
        log.debug("update serviceSeriesMap by pk starting...");
        super.update("updateServiceSeriesMap", serviceSeriesMap);
        log.debug("update serviceSeriesMap by pk end");
    }

    public void deleteServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException
    {
        log.debug("delete serviceSeriesMap by pk starting...");
        super.delete("deleteServiceSeriesMap", serviceSeriesMap);
        log.debug("delete serviceSeriesMap by pk end");
    }

    public ServiceSeriesMap getServiceSeriesMap(ServiceSeriesMap serviceSeriesMap) throws DAOException
    {
        log.debug("query serviceSeriesMap starting...");
        ServiceSeriesMap resultObj = (ServiceSeriesMap) super.queryForObject("getServiceSeriesMap", serviceSeriesMap);
        log.debug("query serviceSeriesMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceSeriesMap> getServiceSeriesMapByCond(ServiceSeriesMap serviceSeriesMap) throws DAOException
    {
        log.debug("query serviceSeriesMap by condition starting...");
        List<ServiceSeriesMap> rList = (List<ServiceSeriesMap>) super.queryForList("queryServiceSeriesMapListByCond",
                serviceSeriesMap);
        log.debug("query serviceSeriesMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceSeriesMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceSeriesMapListCntByCond", serviceSeriesMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceSeriesMap> rsList = (List<ServiceSeriesMap>) super.pageQuery("queryServiceSeriesMapListByCond",
                    serviceSeriesMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceSeriesMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceSeriesMap serviceSeriesMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceSeriesMapListByCond", "queryServiceSeriesMapListCntByCond",
                serviceSeriesMap, start, pageSize, puEntity);
    }

}
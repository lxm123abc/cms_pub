package com.zte.cms.cppr.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsUcpBasic extends DynamicBaseObject
{
    private Long cpindex;
    private java.lang.String cpid;
    private Long cptype;
    private java.lang.String cpcnshortname;
    private java.lang.String servicekey;
    
    public Long getCpindex()
    {
        return cpindex;
    }
    public void setCpindex(Long cpindex)
    {
        this.cpindex = cpindex;
    }
    public java.lang.String getCpid()
    {
        return cpid;
    }
    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }
    
    public Long getCptype()
    {
        return cptype;
    }
    public void setCptype(Long cptype)
    {
        this.cptype = cptype;
    }
    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }
    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }
    public java.lang.String getServicekey()
    {
        return servicekey;
    }
    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }
    public void initRelation()
    {
        this.addRelation("cpindex", "cpindex");
        this.addRelation("cpid", "cpid");
        this.addRelation("cptype", "cptype");
        this.addRelation("cpcnshortname", "cpcnshortname");
        this.addRelation("servicekey", "servicekey");      
    }
}

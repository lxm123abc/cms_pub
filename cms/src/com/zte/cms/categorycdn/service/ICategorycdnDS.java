package com.zte.cms.categorycdn.service;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategorycdnDS {
	/**
	 * 新增Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public void insertCategorycdn(Categorycdn categorycdn)
			throws DomainServiceException;

	/**
	 * 更新Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public void updateCategorycdn(Categorycdn categorycdn)
			throws DomainServiceException;

	/**
	 * 批量更新Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public void updateCategorycdnList(List<Categorycdn> categorycdnList)
			throws DomainServiceException;

	/**
	 * 删除Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public void removeCategorycdn(Categorycdn categorycdn)
			throws DomainServiceException;

	/**
	 * 批量删除Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public void removeCategorycdnList(List<Categorycdn> categorycdnList)
			throws DomainServiceException;

	/**
	 * 查询Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @return Categorycdn对象
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public Categorycdn getCategorycdn(Categorycdn categorycdn)
			throws DomainServiceException;

	/**
	 * 根据条件查询Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象
	 * @return 满足条件的Categorycdn对象集
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public List<Categorycdn> getCategorycdnByCond(Categorycdn categorycdn)
			throws DomainServiceException;

	/**
	 * 根据条件分页查询Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象，作为查询条件的参数
	 * @param start
	 *            起始行
	 * @param pageSize
	 *            页面大小
	 * @param puEntity
	 *            排序空置参数@see PageUtilEntity
	 * @return 查询结果
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public TableDataInfo pageInfoQuery(Categorycdn categorycdn, int start,
			int pageSize) throws DomainServiceException;

	/**
	 * 根据条件分页查询Categorycdn对象
	 * 
	 * @param categorycdn
	 *            Categorycdn对象，作为查询条件的参数
	 * @param start
	 *            起始行
	 * @param pageSize
	 *            页面大小
	 * @param puEntity
	 *            排序空置参数@see PageUtilEntity
	 * @return 查询结果
	 * @throws DomainServiceException
	 *             ds异常
	 */
	public TableDataInfo pageInfoQuery(Categorycdn categorycdn, int start,
			int pageSize, PageUtilEntity puEntity)
			throws DomainServiceException;

	public List<Categorycdn> listMenuTreeByParentid(String typeid);

	public TableDataInfo queryCmsCategory(Categorycdn categorycdn, int start,
			int pageSize) throws DomainServiceException;
	public Categorycdn getCmsCategoryById(Long id);
	public Integer queryByCategoryid(Categorycdn categorycdn) ;
	public Integer queryByCategoryname(Categorycdn categorycdn) ;
	public Categorycdn getCmsCategory(String categoryid) ;
	public Categorycdn categoryInsert(Categorycdn categorycdn) ;
	public List<Categorycdn> queryChildCmsCategory(Long id) ;
	public Categorycdn queryByParentid(String typeid) ;
	public Long queryCategoryindexByCategoryname(Categorycdn categorycdn) ;
    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex, String type, String obindex);
    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex);
    public List<Categorycdn> selectByCategoryindex(Categorycdn categorycdn) throws DomainServiceException;
    
    
    public Categorycdn getCategoryByCategoryId(Categorycdn categorycdn)throws DomainServiceException;
    
    public List<Categorycdn> listMenuTreeByParentidcategoryAll(String typeid);
    
    
    /**
     * 
     * @param CategorycdnMap categorycdnMap
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return PageInfo
     * @throws DAOException DAOException
     */
    public TableDataInfo getCategoryCntMapQuery( CategorycdnMap categorycdnMap, int start, int pageSize, PageUtilEntity puEntity)
    throws DomainServiceException;
}

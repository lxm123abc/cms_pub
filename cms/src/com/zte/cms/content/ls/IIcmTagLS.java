package com.zte.cms.content.ls;

import java.util.List;

import com.zte.cms.content.model.IcmTag;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmTagLS
{
    /**
     * 删除IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 查询IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @return IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public IcmTag getIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTag对象
     * 
     * @param icmTag IcmTag对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException;

    /**
     * 返回内容标签列表
     */
    public List<IcmTag> getIcmTagList() throws DomainServiceException;

    /**
     * 新增标签
     * 
     * @param icmTag IcmTag对象
     * @param contentIndex 内容index数组
     * @return 操作返回值
     */
    public String insertIcmTag(IcmTag icmTag, String[] contentid) throws DomainServiceException;

    /**
     * 
     * 根据index删除标签
     * 
     * @param tagindex 标签主键
     * @return 操作返回值
     */
    public String removeTagByIndex(Long tagindex) throws DomainServiceException;

    /**
     * 根据请求模式分发修改，新增函数
     * 
     * @param icmTag 内容标签对象
     * @param contentid 内容ID数组，
     * @param mode 提交选择关键字 add/mod
     * @return 操作返回值
     */
    public String applyMode(IcmTag icmTag, String[] contentid, String mode) throws DomainServiceException;

    /**
     * 修改内容标签对象
     * 
     * @param icmTag IcmTag 对象
     * @return 操作返回值
     */
    public String updateIcmTagByObj(IcmTag icmTag) throws DomainServiceException;

    /**
     * 由标签的index得到标签的名称
     * 
     * @param indexAll，表签index由“：”拼接而成的字符串
     * @return
     * @throws Exception
     */
    public String getName(String indexAll) throws Exception;

}

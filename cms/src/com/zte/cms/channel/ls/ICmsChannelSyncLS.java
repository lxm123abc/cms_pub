package com.zte.cms.channel.ls;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsChannelSyncLS {

	public String publishChannelcontentTar(List<CntTargetSync> tarlist)
			throws DomainServiceException;
	
	
}
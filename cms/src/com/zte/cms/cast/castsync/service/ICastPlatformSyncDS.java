package com.zte.cms.cast.castsync.service;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastPlatformSyncDS
{
    /**
     * 根据条件分页查询CastPlatformSync对象 
     *
     * @param castPlatformSync CastPlatformSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery( CastPlatformSync castPlatformSync, int start, int pageSize)throws DomainServiceException;    
    
    
}

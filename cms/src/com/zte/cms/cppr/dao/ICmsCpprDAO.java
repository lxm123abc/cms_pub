package com.zte.cms.cppr.dao;

import java.util.List;

import com.zte.cms.cppr.model.CmsCppr;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsCpprDAO
{

    public List<CmsCppr> queryCmsCpprListByCond(CmsCppr cmsCppr) throws DAOException;
    /**
     * 新增到power和service表
     * @param cmsCppr
     * @throws DAOException
     */
    public void insertCmsCppr(CmsCppr cmsCppr) throws DAOException;
    /**
     * 删除power和service表数据
     * @param cmsCppr
     * @throws DAOException
     */
    public void deleteCmsCppr(CmsCppr cmsCppr) throws DAOException;

       
}
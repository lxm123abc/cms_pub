package com.zte.cms.service.service;

import java.util.List;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceBufDS
{
    /**
     * 新增ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException;

    /**
     * 更新ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException;

    /**
     * 批量更新ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceBufList(List<ServiceBuf> serviceBufList) throws DomainServiceException;

    /**
     * 删除ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException;

    /**
     * 批量删除ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceBufList(List<ServiceBuf> serviceBufList) throws DomainServiceException;

    /**
     * 查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @return ServiceBuf对象
     * @throws DomainServiceException ds异常
     */
    public ServiceBuf getServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException;

    /**
     * 根据条件查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @return 满足条件的ServiceBuf对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceBuf> getServiceBufByCond(ServiceBuf serviceBuf) throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

<%@ page import="java.util.*" %>
<%!
  private String blankdsp(String str){
       return ((str.equals(""))?"&nbsp;":str);
  }
  private String getgbstr(String str){
    try{
      return new String(str.getBytes("ISO8859_1"),"GB2312");
    } catch(Exception e){
      return "";
    }
  }
  private String turninstr(String str,int Changelang){
    if(Changelang==2||Changelang==3)str=getgbstr(str);
    return str;
  }
  private String turnoutstr(String str,int Changelang){
    if(Changelang==1||Changelang==3)str=getgbstr(str);
    return str;
  }
  private int lenb(String str){
    int il=str.length();
    int in=il;
    for(int ii=0;ii<il;ii++)if(str.charAt(ii)<0||str.charAt(ii)>255)in++;
    return in;
  }
  private String dspstrfmt(String str,int dsplength){
    String sRet="";
    String sEnd="";
    int ii=0;
    int il=lenb(str);
    if(il<dsplength){
      sRet=str;
      int spaclength=dsplength-il;
      for(ii=0;ii<spaclength;ii++)sRet=sRet+"&nbsp;";
      sEnd="&nbsp;";
    }else{
      il=0;
      for(ii=0;ii<dsplength;ii++){
        sRet=sRet+str.charAt(ii);
        if(lenb(sRet)==dsplength){
          ii=dsplength;
          sEnd="&nbsp;";
        }else if(lenb(sRet)>dsplength){
          ii=dsplength;
        }
      }
    }
    return sRet+sEnd;
  }
  private String putspace(int dsplength){
    String sRet="";
    for(int ii=0;ii<dsplength;ii++)sRet=sRet+"&nbsp;";
    return sRet;
  }
  private String serkcomp(String ywserkey,String nowserkey){
    String sRet="";
    if(ywserkey==null||nowserkey==null){
      sRet="User time out! ";
    }else{
      if(ywserkey.equals(nowserkey)){
      }else{
        sRet="Muntiple service at this client! ";
      }
    }
    return sRet;
  }

  private Vector DevideStr(String Full_string, String sComm){
    Vector vTmp = new Vector();
    if(sComm.length()>1)sComm=sComm.substring(0,1);
    StringTokenizer stString = new StringTokenizer(Full_string,sComm);
    while (stString.hasMoreTokens())vTmp.addElement(stString.nextToken());
    return vTmp;
  }
//按指定长度在字符串前补0
  private String addPreZero(String varstr, int ilen){
    String sRet="";
    String sTmp=varstr.trim();
    if (sTmp.length()<ilen)for(int i=0;i<ilen-sTmp.length();i++)sRet="0"+sRet;
    return (sRet+sTmp);
  }
//按指定长度在字符串前补空格
  private String addPreBlank(String varstr, int ilen){
    String sRet="";
    String sTmp=varstr.trim();
    if (sTmp.length()<ilen)for(int i=0;i<ilen-sTmp.length();i++)sRet=" "+sRet;
    return (sRet+sTmp);
  }
%>
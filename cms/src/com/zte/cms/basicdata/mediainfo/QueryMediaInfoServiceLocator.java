/**
 * QueryMediaInfoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoServiceLocator extends org.apache.axis.client.Service implements
        com.zte.cms.basicdata.mediainfo.QueryMediaInfoService
{

    public QueryMediaInfoServiceLocator()
    {
    }

    public QueryMediaInfoServiceLocator(org.apache.axis.EngineConfiguration config)
    {
        super(config);
    }

    public QueryMediaInfoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
            throws javax.xml.rpc.ServiceException
    {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for QueryMediaInfoServiceHttpPort
    private java.lang.String QueryMediaInfoServiceHttpPort_address = "http://Server:8080/Sys/services/QueryMediaInfoService";

    public java.lang.String getQueryMediaInfoServiceHttpPortAddress()
    {
        return QueryMediaInfoServiceHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String QueryMediaInfoServiceHttpPortWSDDServiceName = "QueryMediaInfoServiceHttpPort";

    public java.lang.String getQueryMediaInfoServiceHttpPortWSDDServiceName()
    {
        return QueryMediaInfoServiceHttpPortWSDDServiceName;
    }

    public void setQueryMediaInfoServiceHttpPortWSDDServiceName(java.lang.String name)
    {
        QueryMediaInfoServiceHttpPortWSDDServiceName = name;
    }

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort getQueryMediaInfoServiceHttpPort()
            throws javax.xml.rpc.ServiceException
    {
        java.net.URL endpoint;
        try
        {
            endpoint = new java.net.URL(QueryMediaInfoServiceHttpPort_address);
        }
        catch (java.net.MalformedURLException e)
        {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getQueryMediaInfoServiceHttpPort(endpoint);
    }

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort getQueryMediaInfoServiceHttpPort(java.net.URL portAddress)
            throws javax.xml.rpc.ServiceException
    {
        try
        {
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoServiceBindingStub _stub = new com.zte.cms.basicdata.mediainfo.QueryMediaInfoServiceBindingStub(
                    portAddress, this);
            _stub.setPortName(getQueryMediaInfoServiceHttpPortWSDDServiceName());
            _stub.setTimeout(60000);// 超时设置1分钟没相应就做超时处理
            return _stub;
        }
        catch (org.apache.axis.AxisFault e)
        {
            return null;
        }
    }

    public void setQueryMediaInfoServiceHttpPortEndpointAddress(java.lang.String address)
    {
        QueryMediaInfoServiceHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation. If this service has no port for the given interface, then
     * ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException
    {
        try
        {
            if (com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort.class.isAssignableFrom(serviceEndpointInterface))
            {
                com.zte.cms.basicdata.mediainfo.QueryMediaInfoServiceBindingStub _stub = new com.zte.cms.basicdata.mediainfo.QueryMediaInfoServiceBindingStub(
                        new java.net.URL(QueryMediaInfoServiceHttpPort_address), this);
                _stub.setPortName(getQueryMediaInfoServiceHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t)
        {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
                + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation. If this service has no port for the given interface, then
     * ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
            throws javax.xml.rpc.ServiceException
    {
        if (portName == null)
        {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("QueryMediaInfoServiceHttpPort".equals(inputPortName))
        {
            return getQueryMediaInfoServiceHttpPort();
        }
        else
        {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName()
    {
        return new javax.xml.namespace.QName("http://NewMediaCMS/QueryMediaInfoService", "QueryMediaInfoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts()
    {
        if (ports == null)
        {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://NewMediaCMS/QueryMediaInfoService",
                    "QueryMediaInfoServiceHttpPort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address)
            throws javax.xml.rpc.ServiceException
    {

        if ("QueryMediaInfoServiceHttpPort".equals(portName))
        {
            setQueryMediaInfoServiceHttpPortEndpointAddress(address);
        }
        else
        { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
            throws javax.xml.rpc.ServiceException
    {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.web.multipart.MultipartFile;

import java.util.List;

/**
 * ProjectName: cms_demo-com.zte.cms_demo.settlementDataRpt.service
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 11:36 2019/5/6
 * @Description:
 */
public interface SettlementDataRptService {

    /**
     *      全查
     * @param settlementDataRptVO
     * @return
     */
    TableDataInfo pageInfoQuery(SettlementDataRptVO settlementDataRptVO);

    //导入
    String importRpt(MultipartFile file) throws Exception;

    //导出
    SettlementDataRptVO exportRpt(SettlementDataRptVO settlementDataRptVo, String userId);

    List<String> getProjectType();


    //新增
    String insertSettlementDataRpt(SettlementDataRptVO settlementDataRpt) throws Exception;

    //删除
    String deleteByIds(String[] ids);

    //单查
    SettlementDataRpt seletcOneById(String id);

    //修改
    void updateById(SettlementDataRptVO vo);

    void updateIsConfirmById(String id);

}

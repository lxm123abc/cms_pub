package com.zte.ismp.systemconfig.common;

import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.log.OperatorLog;
import com.zte.umap.common.ResourceMgt;

public class LogCommon
{
    /**
     * 调用OperatorLog记录日志
     * 
     * @param index 操作对象编号
     * @param opertype 操作类型 1-新增，2-修改，3-删除
     * @param objtype 操作对象类型 1-厂商，2-厂商资源，3-厂商频道列表，4-厂商业务入口，5-服务器，6-配置管理，7-省份，8-城市，9-业务区,10-CPSP
     * @param optresut 操作结果 true-成功，false-失败
     * @param msg 操作信息
     */
    public static void addAppLog(String id, int opertype, int objtype, String msg, boolean optresut)
    {

        String objectId = id;
        String perType = null;
        String optObjectType = null;

        switch (opertype)
        {
            case 1:
                perType = ResourceManager.getResourceText("log.opertype.sys.add");
                break;
            case 2:
                perType = ResourceManager.getResourceText("log.opertype.sys.edit");
                break;
            case 3:
                perType = ResourceManager.getResourceText("log.opertype.sys.del");
                break;
        }

        switch (objtype)
        {
            case 6:
                optObjectType = ResourceManager.getResourceText("log.objtype.sys.config");
                break;

        }

        perType = perType + optObjectType;

        String logDesc = null;

        String result = ResourceManager.getResourceText("log.result.sys.success");

        if (!optresut)
        {
            result = ResourceManager.getResourceText("log.result.sys.fail");
        }

        if (msg.equals("") || msg == null)
        {
            logDesc = perType + result;

        }
        else
        {
            logDesc = perType + "[" + msg + "]" + result;
        }

        OperatorLog.insertOperatorLog(objectId, perType, optObjectType, logDesc);
    }

}

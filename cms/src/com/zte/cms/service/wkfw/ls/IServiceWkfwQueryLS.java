package com.zte.cms.service.wkfw.ls;

import java.util.HashMap;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceWkfwQueryLS
{

    public abstract int queryServiceWkfwTaskForMeListCnt(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException;

    public abstract TableDataInfo queryServiceWkfwTaskForMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    public abstract TableDataInfo queryServiceWkfwTaskForMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    public abstract int queryServiceWkfwProcessWithMeListCnt(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException;

    public abstract TableDataInfo queryServiceWkfwProcessWithMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    public abstract TableDataInfo queryServiceWkfwProcessWithMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect);

    public HashMap<String, String> getWorkflowTypeMap();
}
package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.dao.ISeriesPictureMapDAO;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class SeriesPictureMapDS extends DynamicObjectBaseDS implements ISeriesPictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ISeriesPictureMapDAO dao = null;

    public void setDao(ISeriesPictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException
    {
        log.debug("insert seriesPictureMap starting...");
        try
        {
            if (null == seriesPictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_series_picture_map_index");
                seriesPictureMap.setMapindex(mapIndex);
                seriesPictureMap.setMappingid(String.format("%02d", ObjectType.SERIESPICTUREMAP_TYPE)+ String.format("%030d", seriesPictureMap.getMapindex()));
            }
            
            dao.insertSeriesPictureMap(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert seriesPictureMap end");
    }

    public void updateSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException
    {
        log.debug("update seriesPictureMap by pk starting...");
        try
        {
            dao.updateSeriesPictureMap(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesPictureMap by pk end");
    }

    public void updateSeriesPictureMapList(List<SeriesPictureMap> seriesPictureMapList) throws DomainServiceException
    {
        log.debug("update seriesPictureMapList by pk starting...");
        if (null == seriesPictureMapList || seriesPictureMapList.size() == 0)
        {
            log.debug("there is no datas in seriesPictureMapList");
            return;
        }
        try
        {
            for (SeriesPictureMap seriesPictureMap : seriesPictureMapList)
            {
                dao.updateSeriesPictureMap(seriesPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesPictureMapList by pk end");
    }

    public void removeSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException
    {
        log.debug("remove seriesPictureMap by pk starting...");
        try
        {
            dao.deleteSeriesPictureMap(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesPictureMap by pk end");
    }

    public void removeSeriesPictureMapList(List<SeriesPictureMap> seriesPictureMapList) throws DomainServiceException
    {
        log.debug("remove seriesPictureMapList by pk starting...");
        if (null == seriesPictureMapList || seriesPictureMapList.size() == 0)
        {
            log.debug("there is no datas in seriesPictureMapList");
            return;
        }
        try
        {
            for (SeriesPictureMap seriesPictureMap : seriesPictureMapList)
            {
                dao.deleteSeriesPictureMap(seriesPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesPictureMapList by pk end");
    }

    public SeriesPictureMap getSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException
    {
        log.debug("get seriesPictureMap by pk starting...");
        SeriesPictureMap rsObj = null;
        try
        {
            rsObj = dao.getSeriesPictureMap(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesPictureMapList by pk end");
        return rsObj;
    }

    public List<SeriesPictureMap> getSeriesPictureMapByCond(SeriesPictureMap seriesPictureMap)
            throws DomainServiceException
    {
        log.debug("get seriesPictureMap by condition starting...");
        List<SeriesPictureMap> rsList = null;
        try
        {
            rsList = dao.getSeriesPictureMapByCond(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesPictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesPictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get seriesPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesPictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesPictureMap page info by condition end");
        return tableInfo;
    }

    public Integer getPublishedMapBySeriesIndexCnt(SeriesPictureMap seriesPictureMap) throws DomainServiceException
    {
        Integer publishedCount = 0;
        log.debug("getPublishedMapBySeriesIndexCnt starting...");
        try
        {
            publishedCount = dao.getPublishedMapBySeriesIndexCnt(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("getPublishedMapBySeriesIndexCnt end");
        return publishedCount;
    }
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryseriesPicMap(seriesPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesPictureMap page info by condition end");
        return tableInfo;
    }
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerypseriesPicTargetMap(seriesPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesPictureMap page info by condition end");
        return tableInfo;
    }    
    
    public List<SeriesPictureMap> getSeriesnormalPictureMapByCond(SeriesPictureMap seriesPictureMap)
            throws DomainServiceException
    {
        log.debug("get seriesPictureMap by condition starting...");
        List<SeriesPictureMap> rsList = null;
        try
        {
            rsList = dao.getSeriesnormalPictureMapByCond(seriesPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesPictureMap by condition end");
        return rsList;
    }    
}

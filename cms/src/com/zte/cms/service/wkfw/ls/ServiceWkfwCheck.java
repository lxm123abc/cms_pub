package com.zte.cms.service.wkfw.ls;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.wkfw.ls.IServiceWkfwCheck;
import com.zte.cms.common.ResourceCodeParser;

public class ServiceWkfwCheck implements IServiceWkfwCheck
{

    private IServiceDS serviceds = null;

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    // 构造方法
    ServiceWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
    }

    public String checkServiceWkfw(Long serviceindex) throws DomainServiceException
    {
        Service service = new Service();
        service.setServiceindex(serviceindex);
        service = serviceds.getService(service);

        if (service == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS); // 服务不存在
        }
        // 状态不为带审核或者审核失败或者待审核,且没有发起工作流
        if ((service.getStatus() != 110) && (service.getStatus() != 130))
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_STATUS_WRONG); // 服务状态不正确
        }
        // 已经发起工作流
        if (service.getWorkflow() != 0)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_IN_ALREADY);// 策略已发起工作流未结束
        }
        if (service.getWorkflowlife() != 0)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);// 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    public String checkServiceWkfwAudit(Long serviceIndex) throws DomainServiceException
    {
        Service service = new Service();
        service.setServiceindex(serviceIndex);
        service = serviceds.getService(service);

        if (service == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS);// 服务不存在
        }
        // 120：审核中
        if (service.getStatus() != 120)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_STATUS_WRONG); // 服务态不正确
        }
        if (service.getWorkflow() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_NOT_IN_WKFW_OR_WRONG_WKFW); // 服务不在工作流中或所处工作流不正确
        }
        if (service.getWorkflowlife() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);// 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ISeriesPictureMapDAO
{
    /**
     * 新增SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DAOException dao异常
     */
    public void insertSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException;

    /**
     * 根据主键更新SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DAOException dao异常
     */
    public void updateSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException;

    /**
     * 根据主键删除SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException;

    /**
     * 根据主键查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @return 满足条件的SeriesPictureMap对象
     * @throws DAOException dao异常
     */
    public SeriesPictureMap getSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException;

    /**
     * 根据条件查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @return 满足条件的SeriesPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<SeriesPictureMap> getSeriesPictureMapByCond(SeriesPictureMap seriesPictureMap) throws DAOException;

    /**
     * 根据条件分页查询SeriesPictureMap对象，作为查询条件的参数
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询SeriesPictureMap对象，作为查询条件的参数
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据连续剧Index查询其下面已发布海报的数量
     * 
     * @seriesPictureMap SeriesPictureMap对象
     * @return 该连续剧下已发布的海报的数量
     * @throws DAOException dao 异常
     * 
     */
    public Integer getPublishedMapBySeriesIndexCnt(SeriesPictureMap seriesPictureMap) throws DAOException;
    /**
     * 根据条件分页查询SeriesPictureMap对象，作为查询条件的参数
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询SeriesPictureMap对象，作为查询条件的参数
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException;
    
    /**
     * 根据条件查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @return 满足条件的SeriesPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<SeriesPictureMap> getSeriesnormalPictureMapByCond(SeriesPictureMap seriesPictureMap) throws DAOException;
}
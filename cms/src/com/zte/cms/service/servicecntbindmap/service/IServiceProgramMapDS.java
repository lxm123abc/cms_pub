package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceProgramMapDS
{
    /**
     * 新增ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException;

    /**
     * 更新ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException;

    /**
     * 批量更新ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceProgramMapList(List<ServiceProgramMap> serviceProgramMapList)
            throws DomainServiceException;

    /**
     * 删除ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException;

    /**
     * 批量删除ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceProgramMapList(List<ServiceProgramMap> serviceProgramMapList)
            throws DomainServiceException;

    /**
     * 查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @return ServiceProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public ServiceProgramMap getServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException;

    /**
     * 根据条件查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @return 满足条件的ServiceProgramMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceProgramMap> getServiceProgramMapByCond(ServiceProgramMap serviceProgramMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

package com.zte.cms.content.CStargetSyn.dao;


import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICSCntTargetSyncDAO
{
 
     /**
	  * 根据条件分页查询CntTargetSync对象，作为查询条件的参数
	  *
	  * @param cntTargetSync CntTargetSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetSync对象，作为查询条件的参数
	  *
	  * @param cntTargetSync CntTargetSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
 	 public PageInfo pageInfoQueryCha(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException;
 	 public PageInfo pageInfoQuerySche(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException;
 	 public PageInfo pageInfoQueryCach(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException;

}
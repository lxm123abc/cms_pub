package com.zte.cms.EMBtask.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.cms.EMBtask.service.ICmsEmbtaskDS;
import com.zte.cms.EMBtask.service.IMovieTechauditResultDS;
import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.cms.avtemplate.service.ICmsAvtemplateinfoDS;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.cms.content.service.IIcmTransfileDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

public class CmsEmbtaskLS extends DynamicObjectBaseDS implements ICmsEmbtaskLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsEmbtaskDS ds = null;
    private ICmsAvtemplateinfoDS cmsAvtemplateDs;
    private ICmsStorageareaLS cmsStoragearealS;  
    private IIcmSourcefileDS icmSourcefileds; // 母片
    private IIcmTransfileDS iceTransfileds; // 转码后的文件
    private ICmsMovieDS cmsMovieds;
    private IMovieTechauditResultDS movieTechauditResultDS;
    private ICmsProgramDS cmsProgramDS;
	private IPrimaryKeyGenerator primaryKeyGenerator = null;
    public void setDs(ICmsEmbtaskDS ds)
    {
        this.ds = ds;
    }

    public void setIcmSourcefileds(IIcmSourcefileDS icmSourcefileds)
    {
        this.icmSourcefileds = icmSourcefileds;
    }

    public void setIceTransfileds(IIcmTransfileDS iceTransfileds)
    {
        this.iceTransfileds = iceTransfileds;
    }

    public void setCmsAvtemplateDs(ICmsAvtemplateinfoDS cmsAvtemplateDs)
    {
        this.cmsAvtemplateDs = cmsAvtemplateDs;
    }

    public void setCmsStoragearealS(ICmsStorageareaLS cmsStoragearealS)
    {
        this.cmsStoragearealS = cmsStoragearealS;
    }

    // 新增技审任务
    // 入参cmsEmbtask中的movieindex为子内容的index
    // 出参为插入的结果
    public String insertTechAudit(Long movieindex) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(EMBtaskConstants.CMS_EMBTASK_RES_FILE_NAME);
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setMovieindex(movieindex);
        cmsMovie = cmsMovieds.getCmsMovie(cmsMovie);
        if (cmsMovie == null)
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CMSMOVIE_DOESNOT_EXIST);
        }
        // 判断在表中是否存在该子内容的技审任务，
        CmsEmbtask cmsEmbtask1 = new CmsEmbtask();
        cmsEmbtask1.setSubcontentindex(movieindex); // 相同的母片
        cmsEmbtask1.setTasktype(4);
        cmsEmbtask1.setStatus(1);
        List<CmsEmbtask> cmsEmbtaskList1 = ds.getCmsEmbtaskByCond(cmsEmbtask1);
        cmsEmbtask1.setStatus(2);
        List<CmsEmbtask> cmsEmbtaskList2 = ds.getCmsEmbtaskByCond(cmsEmbtask1);
        if (cmsEmbtaskList1.size() > 0 || cmsEmbtaskList2.size() > 0)
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SAME_TECHAUDITTASK_EXISTS);
        }

        String address = cmsMovie.getFileurl();
        Long programIndex = cmsMovie.getProgramindex();
        String str[];
        if (address.contains("/"))
        {
            str = address.split("/");
        }
        else if (address.contains("\\"))
        {
            str = address.split("\\");
        }
        else
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SOURCEFILE_PATH_WRONG_FORMAT);
        }
        String fileName = str[str.length - 1];
        String path = address.substring(0, address.length() - fileName.length() - 1);

        CmsEmbtask cmsEmbtask = new CmsEmbtask();
        cmsEmbtask.setContentindex(programIndex);
        cmsEmbtask.setSubcontentindex(movieindex);
        cmsEmbtask.setPriority(2);
        cmsEmbtask.setStatus(1);
        cmsEmbtask.setTasktype(4);
        cmsEmbtask.setSourcetype(5);
        cmsEmbtask.setMdfivegenerateoption(1);
        cmsEmbtask.setSrcfilegrouptype(4);
        cmsEmbtask.setSrcfilephyfilename(fileName);
        cmsEmbtask.setSrcfilestoragetype(1);
        cmsEmbtask.setSrcfilepath(path);
        ds.insertCmsEmbtask(cmsEmbtask);
        MovieTechauditResult movieTechauditResult = new MovieTechauditResult();
        movieTechauditResult.setMovieindex(movieindex);
        List<MovieTechauditResult> rsList = null;
        rsList = movieTechauditResultDS.getMovieTechauditResultByCond(movieTechauditResult);
        if (rsList.size() > 0)
        {
            movieTechauditResultDS.removeMovieTechauditResultByCond(movieTechauditResult);
        }
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.techaudit.manage.mgt"); // 技审任务管理
        String optObject = String.valueOf(cmsEmbtask.getTaskindex());
        String optObjecttype = ResourceManager.getResourceText("log.new.techaudit.add"); // 新增技审任务

        OperInfo loginUser = LoginMgt.getOperInfo("CMS");
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.detail.techaudit.add.msg"); // 操作员[opername]新增了技审任务[taskindex]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey("CMS");
        LogInfoMgt.doLog(loginfo);
        return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);

    }

    // 新增粗编任务
    // 入参cmsEmbtask中的contentindex为母片的index
    // 出参为插入的结果
    public String insertByHand(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(EMBtaskConstants.CMS_EMBTASK_RES_FILE_NAME);
        boolean flag1 = true;
        boolean flag2 = true;
        boolean flag3 = true;

        // 定义一个母片对象，获取希望粗编的母片信息
        IcmSourcefile sourceFile = new IcmSourcefile();
        sourceFile.setFileindex(cmsEmbtask.getSubcontentindex());
        sourceFile = icmSourcefileds.getIcmSourcefile(sourceFile);
        if (sourceFile == null)
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SOURCEFILE_DOESNOT_EXIST);
        }

        // 判断模板是否存在
        // 定义一个模板对象，存储视频模板的信息（groupType）
        CmsAvtemplateinfo cmsAvtemplate = new CmsAvtemplateinfo();
        cmsAvtemplate.setTemplateindex(cmsEmbtask.getTemplateindex());
        cmsAvtemplate = cmsAvtemplateDs.getCmsAvtemplateinfo(cmsAvtemplate);
        if (cmsAvtemplate == null)
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_TEMPLATE_DOESNOT_EXIST);
        }

        // 判断在表中是否存在相同转码后的文件，如果相同，则不转
        IcmSourcefile transedfile = new IcmSourcefile();
        transedfile.setParentindex(cmsEmbtask.getSubcontentindex()); // 相同的母片
        transedfile.setTempletid(cmsEmbtask.getTemplateindex()); // 相同的模板
        transedfile.setCmsid(4);
        List<IcmSourcefile> sametransfileList = icmSourcefileds.getIcmSourcefileByCond(transedfile);
        if (sametransfileList.size() > 0)
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SAME_TRANSFILE_EXISTS);
        }

        // 判断任务表中是否存在相同的任务
        CmsEmbtask checkDupTask = new CmsEmbtask();
        checkDupTask.setSubcontentindex(cmsEmbtask.getSubcontentindex());
        checkDupTask.setTasktype(cmsEmbtask.getTasktype());
        checkDupTask.setTemplateindex(cmsEmbtask.getTemplateindex());
        List<CmsEmbtask> dupTask = ds.getCmsEmbtaskByCond(checkDupTask);
        if (dupTask.size() > 0)
        {
            for (int i = 0; i < dupTask.size(); i++)
            {
                if (dupTask.get(i).getStatus() != 4)
                {
                    return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_DUPLICATED_TASK);
                }
            }

        }
        // 判断母片格式和模板格式是否匹配
        flag1 = flag1
                && (sourceFile.getDefinition() == 3)
                && ((cmsAvtemplate.getGrouptype() == 3) || (cmsAvtemplate.getGrouptype() == 4) || (cmsAvtemplate
                        .getGrouptype() == 2));
        flag2 = flag2 && (sourceFile.getDefinition() == 4)
                && (cmsAvtemplate.getGrouptype() == 2 || cmsAvtemplate.getGrouptype() == 4);
        flag3 = flag3 && (sourceFile.getDefinition() == 2) && (cmsAvtemplate.getGrouptype() == 2);

        if (flag1 || flag2 || flag3) // 匹配
        {
            String sourcepathName = sourceFile.getAddresspath(); // 路径+名称
            String path; // 存放路径
            String name; // 存放名称
            String desName; // 目标文件名称
            String desPath; // 目标文件路径
            String str[];
            if (sourcepathName.contains("/"))
            {
                str = sourcepathName.split("/");
            }
            else if (sourcepathName.contains("\\"))
            {
                str = sourcepathName.split("\\");
            }
            else
            {
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SOURCEFILE_PATH_WRONG_FORMAT);
            }
            name = str[str.length - 1]; // 得到名字
            path = sourcepathName.substring(0, sourcepathName.length() - name.length() - 1);
            // 判断源文件名称是否有后缀
            if (name.lastIndexOf(".") == -1)
            {
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SOURCEFILE_WRONG_POSTFIX);
            }
            else
            {
                desName = name.substring(0, name.lastIndexOf(".")) + cmsAvtemplate.getTemplatename()
                        + "." + cmsAvtemplate.getVideoformat();
            }
            desPath = cmsStoragearealS.getAddress("4"); // 制作区
            if (desPath.equals("1056020")) // 没有得到制作区地址
            {
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CANTNOT_GET_MAKEAREA_ADDRESS);
            }
            cmsEmbtask.setStatus(1);
            cmsEmbtask.setSrcfilegrouptype(sourceFile.getDefinition());
            cmsEmbtask.setSrcfilephyfilename(sourceFile.getFilename());// 物理文件名
            cmsEmbtask.setSrcfilepath(path);
            cmsEmbtask.setDestfilegrouptype(cmsAvtemplate.getGrouptype());
            cmsEmbtask.setDestfilephyfilename(desName);
            cmsEmbtask.setDestfilepath(desPath);
            cmsEmbtask.setSrcfilestoragetype(1);
            cmsEmbtask.setDestfilestoragetype(1);
            cmsEmbtask.setMdfivegenerateoption(0);
            cmsEmbtask.setTasktype(2);
            insertCmsEmbtask(cmsEmbtask);

            // 写日志
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
            // 操作对象类型
            String optType = ResourceManager.getResourceText("log.embtask.manage.mgt"); // 粗编任务管理
            String optObject = String.valueOf(cmsEmbtask.getTaskindex());
            String optObjecttype = ResourceManager.getResourceText("log.new.embtask.add"); // 新增粗编任务

            OperInfo loginUser = LoginMgt.getOperInfo("CMS");
            // 操作员编码
            String operId = loginUser.getOperid();
            // 操作员名称
            String operName = loginUser.getUserId();

            String optDetail = ResourceManager.getResourceText("log.detail.embtask.add.msg"); // 操作员[opername]新增了粗编任务[taskindex]
            optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
            optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

            loginfo.setUserId(operId);
            loginfo.setOptType(optType);
            loginfo.setOptObject(optObject);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptDetail(optDetail);
            loginfo.setOptTime("");
            loginfo.setServicekey("CMS");
            LogInfoMgt.doLog(loginfo);

            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);
        }
        else
        {
            return ResourceMgt
                    .findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_GROUPTYPE_DEESNOT_MATCH_AVGROUPTYPE);
        }

    }

    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("insert cmsEmbtask starting...");
        try
        {
            ds.insertCmsEmbtask(cmsEmbtask);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("insert cmsEmbtask end");
    }

    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("get cmsEmbtask by pk starting...");
        CmsEmbtask rsObj = null;
        try
        {
            rsObj = ds.getCmsEmbtask(cmsEmbtask);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }

        log.debug("get cmsEmbtaskList by pk end");
        return rsObj;
    }

    // 查询任务信息，如果查询出来之后，判断来源是否存在，如果存在则直接显示，如果不存在，则在来源index中显示来源不存在或者被删除
    public CmsEmbtask getCmsEmbtaskWthChkSrcIdx(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {

        log.debug("get cmsEmbtask by pk starting...");
        CmsEmbtask rsObj = null;
        try
        {
            rsObj = ds.getCmsEmbtask(cmsEmbtask);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtaskList by pk end");
        return rsObj;

    }

    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        log.debug("get cmsEmbtask by condition starting...");
        List<CmsEmbtask> rsList = null;
        try
        {
            rsList = ds.getCmsEmbtaskByCond(cmsEmbtask);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException
    {

        // 先进行时间格式的转换
        String starttime = cmsEmbtask.getStarttime();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            // System.out.println(starttime1);
            cmsEmbtask.setStarttime(starttime1);
        }
        String starttime2 = cmsEmbtask.getEndtime();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            // System.out.println(starttime3);
            cmsEmbtask.setEndtime(starttime3);
        }

        String endtime1 = cmsEmbtask.getSrcfilephyfilename();
        if (!endtime1.equals(""))
        {
            String endtime2 = endtime1.substring(0, 4) + endtime1.substring(5, 7) + endtime1.substring(8, 10)
                    + endtime1.substring(11, 13) + endtime1.substring(14, 16) + endtime1.substring(17, 19);
            cmsEmbtask.setSrcfilephyfilename(endtime2);
        }
        String endtime3 = cmsEmbtask.getDestfilephyfilename();
        if (!endtime3.equals(""))
        {
            String endtime4 = endtime3.substring(0, 4) + endtime3.substring(5, 7) + endtime3.substring(8, 10)
                    + endtime3.substring(11, 13) + endtime3.substring(14, 16) + endtime3.substring(17, 19);
            cmsEmbtask.setDestfilephyfilename(endtime4);
        }

        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = ds.pageInfoQuery(cmsEmbtask, start, pageSize);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException
    {

        // 先进行时间格式的转换
        String starttime = cmsEmbtask.getStarttime();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            // System.out.println(starttime1);
            cmsEmbtask.setStarttime(starttime1);
        }
        String starttime2 = cmsEmbtask.getEndtime();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            // System.out.println(starttime3);
            cmsEmbtask.setEndtime(starttime3);
        }

        String endtime1 = cmsEmbtask.getSrcfilephyfilename();
        if (!endtime1.equals(""))
        {
            String endtime2 = endtime1.substring(0, 4) + endtime1.substring(5, 7) + endtime1.substring(8, 10)
                    + endtime1.substring(11, 13) + endtime1.substring(14, 16) + endtime1.substring(17, 19);
            cmsEmbtask.setSrcfilephyfilename(endtime2);
        }
        String endtime3 = cmsEmbtask.getDestfilephyfilename();
        if (!endtime3.equals(""))
        {
            String endtime4 = endtime3.substring(0, 4) + endtime3.substring(5, 7) + endtime3.substring(8, 10)
                    + endtime3.substring(11, 13) + endtime3.substring(14, 16) + endtime3.substring(17, 19);
            cmsEmbtask.setDestfilephyfilename(endtime4);
        }

        if (null != cmsEmbtask.getResultdesc() && !cmsEmbtask.getResultdesc().equals(""))
        {
            String resultdesc = EspecialCharMgt.conversion(cmsEmbtask.getResultdesc());
            cmsEmbtask.setResultdesc(resultdesc);
        }

        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("taskindex");
        try
        {
            tableInfo = pageInfoQuerySub(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = ds.pageInfoQuerySub(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException
    {

        // 先进行时间格式的转换
		String starttime1 = cmsEmbtask.getStarttime1();
        if (!starttime1.equals(""))
        {
		starttime1 = starttime1.trim().replace(" ", "").replace(":", "").replace("-", "");
		cmsEmbtask.setStarttime1(starttime1);
        }
		String starttime2 = cmsEmbtask.getStarttime2();
        if (!starttime2.equals(""))
        {
		starttime2 = starttime2.trim().replace(" ", "").replace(":", "").replace("-", "");
		cmsEmbtask.setStarttime2(starttime2);
        }
		String endtime1 = cmsEmbtask.getEndtime1();
        if (!endtime1.equals(""))
        {
		endtime1 = endtime1.trim().replace(" ", "").replace(":", "").replace("-", "");
		cmsEmbtask.setEndtime1(endtime1);
        }		
		String endtime2 = cmsEmbtask.getEndtime2();
        if (!endtime2.equals(""))
        {
		endtime2 = endtime2.trim().replace(" ", "").replace(":", "").replace("-", "");
		cmsEmbtask.setEndtime2(endtime2);
        }			

        if (null != cmsEmbtask.getMoviename() && !cmsEmbtask.getMoviename().equals(""))
        {
            String moviename = EspecialCharMgt.conversion(cmsEmbtask.getMoviename());
            cmsEmbtask.setMoviename(moviename);
        }

        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("taskindex");
        try
        {
            tableInfo = pageInfoQueryEncryption(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }
   
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = ds.pageInfoQueryEncryption(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }   
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsEmbtask page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = ds.pageInfoQuery(cmsEmbtask, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("dao exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    // 查询母片表，从母片的index得到母片名称
    public IcmSourcefile getSrcFileName(IcmSourcefile icmsourcefile) throws DomainServiceException
    {
        IcmSourcefile srcFileobj = null;
        log.debug("get source file name from sourcefile table by index ");
        try
        {
            srcFileobj = icmSourcefileds.getIcmSourcefile(icmsourcefile);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }

        return srcFileobj;
    }

    public String insertByHandBatch(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsEmbtasklist.size(); i++)
        {
            String message = null;
            String result = null;
            String valid = null;
            valid = insertByHand(cmsEmbtasklist.get(i));
            if (valid.equals("0:操作成功"))
            {
                successCount++;
                result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);

                operateInfo = new OperateInfo(String.valueOf(num), cmsEmbtasklist.get(i).getSourcefilename(), result
                        .substring(2, result.length()), valid.substring(2, valid.length()));
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                failCount++;
                result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsEmbtasklist.get(i).getSourcefilename(), result
                        .substring(2, result.length()), valid.substring(2, valid.length()));
                returnInfo.appendOperateInfo(operateInfo);
            }

            num++;
        }

        if ((successCount) == cmsEmbtasklist.size())
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == cmsEmbtasklist.size())
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();
    }

    //子内容批量加密
    public String insertEncryptionByBatch(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsEmbtasklist.size(); i++)
        {
            String result = null;
            String valid = null;
            valid = insertEncryption(cmsEmbtasklist.get(i));
            if (valid.equals("0:操作成功"))
            {
                successCount++;
                result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);

                operateInfo = new OperateInfo(String.valueOf(num), cmsEmbtasklist.get(i).getMoviename(), result
                        .substring(2, result.length()), valid.substring(2, valid.length()));
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                failCount++;
                result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsEmbtasklist.get(i).getMoviename(), result
                        .substring(2, result.length()), valid.substring(2, valid.length()));
                returnInfo.appendOperateInfo(operateInfo);
            }

            num++;
        }

        if ((successCount) == cmsEmbtasklist.size())
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == cmsEmbtasklist.size())
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();
    	
    }
    // 新增加密任务

    public String insertEncryption(CmsEmbtask cmsEmbtask) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(EMBtaskConstants.CMS_EMBTASK_RES_FILE_NAME);
        String result = null;
        String desPath; // 目标文件路径
        String destfilename ; //目标文件物理名
        boolean flag1 = false; //判断有无插入加密任务
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setMovieindex(cmsEmbtask.getSubcontentindex());
        cmsMovie = cmsMovieds.getCmsMovie(cmsMovie);
        if (cmsMovie == null)  //该子内容不存在
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CMSMOVIE_DOESNOT_EXIST);
        }
        else
        {
        	if (cmsMovie.getEncryptionstatus() != 0 && cmsMovie.getEncryptionstatus()!=4) //该子内容加密状态不为未加密或加密失败
        	{
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_ENCRYPTION_STATUS_DOESNOT_CORRECT);
        	}
        	if (cmsMovie.getStatus() == 100)
        	{
        		return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_ENCRP_SUBCONTENT_STATUS_DOESNOT_CORRECT);
        		
        	}
        }

        String address = cmsMovie.getFileurl(); //获取子内容相对路径
        String str[]=null;
        String subcntName;
        if (address.contains("/"))
        {
            str = address.split("/");
        }
        if (address.contains("\\"))
        {
            str = address.split("\\");
        }
        
        String suffix; //子内容文件后缀
        String fileName = str[str.length - 1]; //子内容文件名，包括后缀
        String path = address.substring(0, address.length() - fileName.length() - 1);          
 
        if (fileName.lastIndexOf(".") == -1) //源文件格式不正确
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SOURCEFILE_WRONG_POSTFIX);
        }
        else
        {
        	 subcntName = fileName.substring(0, fileName.lastIndexOf(".")); //子内容文件名，不包括后缀
        	 suffix = fileName.substring(fileName.lastIndexOf(".")); //后缀
        }       
        
  
        
       if (cmsEmbtask.getWorktype() ==1)  //普通加密
       {
           Long index = (Long)primaryKeyGenerator.getPrimarykey("cms_EMBtask_taskindex"); //获取taskindex
           cmsEmbtask.setTaskindex(index);
           destfilename = subcntName + "_" + "common" + suffix; //目标文件物理名
           
           desPath = cmsStoragearealS.getAddress("4"); // 制作区
           if (desPath.equals("1056020")) // 没有得到制作区地址
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CANTNOT_GET_MAKEAREA_ADDRESS);
           }
           desPath = desPath + File.separator +"commdest"+cmsEmbtask.getTaskindex();           
           desPath = desPath.replaceAll("\\\\", "/");
           cmsEmbtask.setStarttime(dateFormat.format(new Date()));
           cmsEmbtask.setWorktype(1);  //普通加密
           cmsEmbtask.setFiletranstype(0);  //普通文件
           cmsEmbtask.setStatus(201);  //待加密
           cmsEmbtask.setSrcfilephyfilename(fileName);  //源文件物理文件名称
           cmsEmbtask.setSrcfilepath(path);  //源文件路径
           cmsEmbtask.setDestfilephyfilename(destfilename);  //目标文件物理文件名称
           cmsEmbtask.setDestfilepath(desPath);  //目标文件路径
           cmsEmbtask.setFiletype(0);
           cmsEmbtask.setTaskname(index.toString());
           cmsEmbtask.setResType("0");
           insertCmsEmbtask(cmsEmbtask);

           // 写日志
           AppLogInfoEntity loginfo = new AppLogInfoEntity();
           // 操作对象类型
           String optType = ResourceManager.getResourceText("log.encryptiontask.manage.mgt"); // 加密任务管理
           String optObject = String.valueOf(cmsEmbtask.getTaskindex());
           String optObjecttype = ResourceManager.getResourceText("log.new.encryptiontask.add"); // 新增加密任务

           OperInfo loginUser = LoginMgt.getOperInfo("CMS");
           // 操作员编码
           String operId = loginUser.getOperid();
           // 操作员名称
           String operName = loginUser.getUserId();

           String optDetail = ResourceManager.getResourceText("log.detail.encryptiontask.add.msg"); // 操作员[opername]新增了加密任务[taskindex]
           optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
           optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

           loginfo.setUserId(operId);
           loginfo.setOptType(optType);
           loginfo.setOptObject(optObject);
           loginfo.setOptObjecttype(optObjecttype);
           loginfo.setOptDetail(optDetail);
           loginfo.setOptTime("");
           loginfo.setServicekey("CMS");
           LogInfoMgt.doLog(loginfo);
           
           //更新子内容加密状态和加密类型
           CmsMovie cmsMovieTemp = new CmsMovie();
           cmsMovieTemp.setMovieindex(cmsEmbtask.getSubcontentindex());
           cmsMovieTemp.setEncryptiontype(1);  
           cmsMovieTemp.setEncryptionstatus(2);
           cmsMovieds.updateCmsMovie(cmsMovieTemp);    
           flag1 = true;
           
       }

       if (cmsEmbtask.getWorktype() == 0)  //HLS加密
       {
           Long index = (Long)primaryKeyGenerator.getPrimarykey("cms_EMBtask_taskindex"); //获取taskindex
           cmsEmbtask.setTaskindex(index);
    	   String  cmsMovieLogaddress;
    	   String  logaddress ="../../../../translog";

           
           String embRule = cmsEmbtask.getEmbRule(); //模板ID
           destfilename = subcntName + "_" + "hls" + suffix; //目标文件物理名
           
           desPath = cmsStoragearealS.getAddress("4"); // 制作区
           if (desPath.equals("1056020")) // 没有得到制作区地址
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CANTNOT_GET_MAKEAREA_ADDRESS);
           }
           desPath = desPath + File.separator +"hlsdest"+cmsEmbtask.getTaskindex();
           desPath = desPath.replaceAll("\\\\", "/");
           cmsEmbtask.setStarttime(dateFormat.format(new Date()));   
           cmsEmbtask.setWorktype(0);  //HLS加密
           cmsEmbtask.setFiletranstype(1);  //HLS文件
           cmsEmbtask.setStatus(101);  //待加密
           cmsEmbtask.setSrcfilephyfilename(fileName);  //源文件物理文件名称
           cmsEmbtask.setSrcfilepath(path);  //源文件路径
           cmsEmbtask.setDestfilephyfilename(destfilename);  //目标文件物理文件名称
           cmsEmbtask.setDestfilepath(desPath);  //目标文件路径
           cmsEmbtask.setFiletype(0);
           cmsEmbtask.setTaskname(index.toString());
           cmsEmbtask.setResType("0");
           cmsEmbtask.setEmbRule(embRule); //模板ID
           cmsEmbtask.setHlsTargetDuration(-1L);
           cmsEmbtask.setResType("0");
           cmsEmbtask.setLogaddress(logaddress); //日志路径
           insertCmsEmbtask(cmsEmbtask);

           // 写日志
           AppLogInfoEntity loginfo = new AppLogInfoEntity();
           // 操作对象类型
           String optType = ResourceManager.getResourceText("log.encryptiontask.manage.mgt"); // 加密任务管理
           String optObject = String.valueOf(cmsEmbtask.getTaskindex());
           String optObjecttype = ResourceManager.getResourceText("log.new.encryptiontask.add"); // 新增加密任务

           OperInfo loginUser = LoginMgt.getOperInfo("CMS");
           // 操作员编码
           String operId = loginUser.getOperid();
           // 操作员名称
           String operName = loginUser.getUserId();

           String optDetail = ResourceManager.getResourceText("log.detail.encryptiontask.add.msg"); // 操作员[opername]新增了加密任务[taskindex]
           optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
           optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

           loginfo.setUserId(operId);
           loginfo.setOptType(optType);
           loginfo.setOptObject(optObject);
           loginfo.setOptObjecttype(optObjecttype);
           loginfo.setOptDetail(optDetail);
           loginfo.setOptTime("");
           loginfo.setServicekey("CMS");
           LogInfoMgt.doLog(loginfo);
           
           //更新子内容加密状态和加密类型并把hls的加密日志路径赋给子内容
           CmsMovie cmsMovieTemp = new CmsMovie();
           cmsMovieTemp.setMovieindex(cmsEmbtask.getSubcontentindex());
           cmsMovieLogaddress =logaddress +File.separator +cmsEmbtask.getTaskindex()+".txt";
           cmsMovieLogaddress = cmsMovieLogaddress.replaceAll("\\\\", "/");// 用 "\" 替换 "/"
           cmsMovieTemp.setEncryptionlogpath(cmsMovieLogaddress);
           cmsMovieTemp.setEncryptiontype(2);  
           cmsMovieTemp.setEncryptionstatus(2);
           cmsMovieds.updateCmsMovie(cmsMovieTemp); 
           flag1 = true;
       }
       //更新内容状态
       if (flag1)
       {
   	       int count = 0; //统计子内容的加密状态
           CmsMovie cmsMovieTmp = new CmsMovie();

           cmsMovieTmp.setProgramindex(cmsEmbtask.getContentindex());
           List<CmsMovie> movieList = null;
           movieList = cmsMovieds.getCmsMovieByCond(cmsMovieTmp);
           if (movieList == null || movieList.size() <= 0)
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_CNT_HASNOT_SUBCONTENT);
           }
           else
           {
               for(CmsMovie movie:movieList)
               {
                  if(movie.getEncryptionstatus() != 1 && movie.getEncryptionstatus() != 2 && movie.getEncryptionstatus() != 3)
                  {
                      count++;
                  }
               } 
               if (count == 0) //所有子内容加密状态不存在未加密和加密失败时，更新内容的状态为加密中
               {
                   CmsProgram program = new CmsProgram();
                   program.setProgramindex(cmsEmbtask.getContentindex());
                   program.setEncryptionstatus(2);
        	       cmsProgramDS.updateCmsProgram(program);
               }
           
           }
           result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);  

       }
       return result.toString();
    }    
    // 单独一个子内容加密
    public String insertOneEncryption(List<CmsEmbtask> cmsEmbtasklist) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(EMBtaskConstants.CMS_EMBTASK_RES_FILE_NAME);
        String result = null;
        String desPath; // 目标文件路径
        String destfilename ; //目标文件物理名
        boolean flag1 = false; //判断有无插入加密任务
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
       
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setMovieindex(cmsEmbtasklist.get(0).getSubcontentindex());
        cmsMovie = cmsMovieds.getCmsMovie(cmsMovie);
        if (cmsMovie == null) //子内容不存在
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_CMSMOVIE_DOESNOT_EXIST);
        }
        else
        {
        	if (cmsMovie.getEncryptionstatus() != 0 && cmsMovie.getEncryptionstatus()!=4) //子内容加密状态不为加密失败和未加密
        	{
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_CMSMOVIE_ENCRYPTION_STATUS_DOESNOT_CORRECT);
        	}
        	if(cmsMovie.getStatus() == 100)
        	{
                return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_STATUS_DOESNOT_CORRECT);
        	}
        }

        String address = cmsMovie.getFileurl(); 
        String str[]=null;
        String subcntName;
        if (address.contains("/"))
        {
            str = address.split("/");
        }
        if (address.contains("\\"))
        {
            str = address.split("\\");
        }
        
        String suffix; //子内容文件后缀
        String fileName = str[str.length - 1]; //子内容文件名，包括后缀
        String path = address.substring(0, address.length() - fileName.length() - 1);          
 
        if (fileName.lastIndexOf(".") == -1) //源文件格式不正确
        {
            return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_SOURCEFILE_WRONG_POSTFIX);
        }
        else
        {
        	 subcntName = fileName.substring(0, fileName.lastIndexOf(".")); //子内容文件名，不包括后缀
        	 suffix = fileName.substring(fileName.lastIndexOf(".")); //后缀
        }       
        
  
        CmsEmbtask cmsEmbtask = new CmsEmbtask();
       
        cmsEmbtask.setContentindex(cmsEmbtasklist.get(0).getContentindex());
        cmsEmbtask.setSubcontentindex(cmsEmbtasklist.get(0).getSubcontentindex());
        cmsEmbtask.setPriority(cmsEmbtasklist.get(0).getPriority());
       if (cmsEmbtasklist.get(0).getWorktype() ==1)  //普通加密
       {
           Long index = (Long) primaryKeyGenerator.getPrimarykey("cms_EMBtask_taskindex");  //获取任务index
           cmsEmbtask.setTaskindex(index);
           destfilename = subcntName + "_" + "common" + suffix; //目标文件物理名
           
           desPath = cmsStoragearealS.getAddress("4"); // 制作区
           if (desPath.equals("1056020")) // 没有得到制作区地址
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_CANTNOT_GET_MAKEAREA_ADDRESS);
           }
           desPath = desPath + File.separator +"commdest"+cmsEmbtask.getTaskindex();                      
           desPath = desPath.replaceAll("\\\\", "/");
           cmsEmbtask.setStarttime(dateFormat.format(new Date()));

           cmsEmbtask.setWorktype(1);  //普通加密
           cmsEmbtask.setFiletranstype(0);  //普通文件
           cmsEmbtask.setStatus(201);  //待加密
           cmsEmbtask.setSrcfilephyfilename(fileName);  //源文件物理文件名称
           cmsEmbtask.setSrcfilepath(path);  //源文件路径
           cmsEmbtask.setDestfilephyfilename(destfilename);  //目标文件物理文件名称
           cmsEmbtask.setDestfilepath(desPath);  //目标文件路径
           cmsEmbtask.setFiletype(0);
           cmsEmbtask.setTaskname(index.toString());
           cmsEmbtask.setResType("0");
           insertCmsEmbtask(cmsEmbtask);

           // 写日志
           AppLogInfoEntity loginfo = new AppLogInfoEntity();
           // 操作对象类型
           String optType = ResourceManager.getResourceText("log.encryptiontask.manage.mgt"); // 加密任务管理
           String optObject = String.valueOf(cmsEmbtask.getTaskindex());
           String optObjecttype = ResourceManager.getResourceText("log.new.encryptiontask.add"); // 新增加密任务

           OperInfo loginUser = LoginMgt.getOperInfo("CMS");
           // 操作员编码
           String operId = loginUser.getOperid();
           // 操作员名称
           String operName = loginUser.getUserId();

           String optDetail = ResourceManager.getResourceText("log.detail.encryptiontask.add.msg"); // 操作员[opername]新增了加密任务[taskindex]
           optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
           optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

           loginfo.setUserId(operId);
           loginfo.setOptType(optType);
           loginfo.setOptObject(optObject);
           loginfo.setOptObjecttype(optObjecttype);
           loginfo.setOptDetail(optDetail);
           loginfo.setOptTime("");
           loginfo.setServicekey("CMS");
           LogInfoMgt.doLog(loginfo);

           //更新子内容加密状态和加密类型
           CmsMovie cmsMovieTemp = new CmsMovie();
           cmsMovieTemp.setMovieindex(cmsEmbtask.getSubcontentindex());
           cmsMovieTemp.setEncryptiontype(1);  
           cmsMovieTemp.setEncryptionstatus(2);
           cmsMovieds.updateCmsMovie(cmsMovieTemp);    
           flag1 = true;  
       }
       if (cmsEmbtasklist.get(0).getWorktype() == 0)  //HLS加密
       {
           Long index = (Long) primaryKeyGenerator.getPrimarykey("cms_EMBtask_taskindex");  //获取任务index
           cmsEmbtask.setTaskindex(index);
    	   String  cmsMovieLogaddress;
    	   String  logaddress ="../../../../translog";


           String embRule = cmsEmbtasklist.get(0).getEmbRule();
           destfilename = subcntName + "_" + "hls" + suffix; //目标文件物理名
           
           desPath = cmsStoragearealS.getAddress("4"); // 制作区
           if (desPath.equals("1056020")) // 没有得到制作区地址
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_CANTNOT_GET_MAKEAREA_ADDRESS);
           }
           desPath = desPath + File.separator +"hlsdest"+cmsEmbtask.getTaskindex();
           desPath = desPath.replaceAll("\\\\", "/");
           cmsEmbtask.setStarttime(dateFormat.format(new Date()));
           cmsEmbtask.setWorktype(0);  //HLS加密
           cmsEmbtask.setFiletranstype(1);  //HLS文件
           cmsEmbtask.setStatus(101);  //待加密
           cmsEmbtask.setSrcfilephyfilename(fileName);  //源文件物理文件名称
           cmsEmbtask.setSrcfilepath(path);  //源文件路径
           cmsEmbtask.setDestfilephyfilename(destfilename);  //目标文件物理文件名称
           cmsEmbtask.setDestfilepath(desPath);  //目标文件路径
           cmsEmbtask.setFiletype(0);
           cmsEmbtask.setTaskname(index.toString());
           cmsEmbtask.setResType("0");
           cmsEmbtask.setEmbRule(embRule);  //模板id
           cmsEmbtask.setHlsTargetDuration(-1L);
           cmsEmbtask.setResType("0");
           cmsEmbtask.setLogaddress(logaddress);
           insertCmsEmbtask(cmsEmbtask);

           // 写日志
           AppLogInfoEntity loginfo = new AppLogInfoEntity();
           // 操作对象类型
           String optType = ResourceManager.getResourceText("log.encryptiontask.manage.mgt"); // 加密任务管理
           String optObject = String.valueOf(cmsEmbtask.getTaskindex());
           String optObjecttype = ResourceManager.getResourceText("log.new.encryptiontask.add"); // 新增加密任务

           OperInfo loginUser = LoginMgt.getOperInfo("CMS");
           // 操作员编码
           String operId = loginUser.getOperid();
           // 操作员名称
           String operName = loginUser.getUserId();

           String optDetail = ResourceManager.getResourceText("log.detail.encryptiontask.add.msg"); // 操作员[opername]新增了加密任务[taskindex]
           optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
           optDetail = optDetail.replaceAll("\\[taskindex\\]", String.valueOf(cmsEmbtask.getTaskindex()));

           loginfo.setUserId(operId);
           loginfo.setOptType(optType);
           loginfo.setOptObject(optObject);
           loginfo.setOptObjecttype(optObjecttype);
           loginfo.setOptDetail(optDetail);
           loginfo.setOptTime("");
           loginfo.setServicekey("CMS");
           LogInfoMgt.doLog(loginfo);
           
           //更新子内容加密状态和加密类型
           CmsMovie cmsMovieTemp = new CmsMovie();
           cmsMovieTemp.setMovieindex(cmsEmbtask.getSubcontentindex());
           cmsMovieLogaddress = logaddress +File.separator +cmsEmbtask.getTaskindex()+".txt";
           cmsMovieLogaddress = cmsMovieLogaddress.replaceAll("\\\\", "/");// 用 "\" 替换 "/"

           cmsMovieTemp.setEncryptionlogpath(cmsMovieLogaddress);
           cmsMovieTemp.setEncryptiontype(2);  
           cmsMovieTemp.setEncryptionstatus(2);
           cmsMovieds.updateCmsMovie(cmsMovieTemp); 
           flag1 = true;  
       }
       //更新内容状态
       if (flag1)
       {
   	       int count = 0; //统计子内容的加密状态
           CmsMovie cmsMovieTmp = new CmsMovie();
   	    
           cmsMovieTmp.setProgramindex(cmsEmbtask.getContentindex());
           List<CmsMovie> movieList = null;
           movieList = cmsMovieds.getCmsMovieByCond(cmsMovieTmp);
           if (movieList == null || movieList.size() <= 0)
           {
               return ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_SUBCONTENT_CNT_HASNOT_SUBCONTENT);
           }
           else
           {
               for(CmsMovie movie:movieList)
               {
                  if(movie.getEncryptionstatus() != 1 && movie.getEncryptionstatus() != 2 && movie.getEncryptionstatus() != 3)
                  {
                      count++;
                  }
               } 
               if (count == 0) //所有子内容加密状态不存在未加密和加密失败时，更新内容的状态为加密中
               {
                   CmsProgram program = new CmsProgram();
                   program.setProgramindex(cmsEmbtask.getContentindex());
                   program.setEncryptionstatus(2);
        	       cmsProgramDS.updateCmsProgram(program);
               }
           
           }
           result = ResourceMgt.findDefaultText(EMBtaskConstants.CMS_EMBTASK_MSG_OPER_SUCCESSFUL);  
       }
    return result.toString();
    }        
    
    /**
     * 根据连续index获取内容对象
     * 
     * @param movieindex 內容Index
     * @return 内容对象
     */
    public CmsMovie getCmsMovieByIndex(Long movieindex)throws DomainServiceException 
    {
        log.debug("getCmsProgramByIndex starting...");
        CmsMovie cmsMovie = new CmsMovie();
        try 
        {
        	cmsMovie.setMovieindex(movieindex);
        	cmsMovie = cmsMovieds.getCmsMovie(cmsMovie);
        }
        catch (DomainServiceException e) 
        {
	        log.error("getCmsProgramByIndex exception:" + e);
	        throw e;
        }
        log.debug("getCmsProgramByIndex end");
        return cmsMovie;
    }    
    public Log getLog()
    {
        return log;
    }

    public void setLog(Log log)
    {
        this.log = log;
    }

    public ICmsEmbtaskDS getDs()
    {
        return ds;
    }

    public ICmsAvtemplateinfoDS getCmsAvtemplateDs()
    {
        return cmsAvtemplateDs;
    }

    public ICmsStorageareaLS getCmsStoragearealS()
    {
        return cmsStoragearealS;
    }

    public ICmsMovieDS getCmsMovieds()
    {
        return cmsMovieds;
    }

    public void setCmsMovieds(ICmsMovieDS cmsMovieds)
    {
        this.cmsMovieds = cmsMovieds;
    }

    public void setMovieTechauditResultDS(IMovieTechauditResultDS movieTechauditResultDS)
    {
        this.movieTechauditResultDS = movieTechauditResultDS;
    }

    public IMovieTechauditResultDS getMovieTechauditResultDS()
    {
        return movieTechauditResultDS;
    }
	public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
	{
		this.cmsProgramDS = cmsProgramDS;
	}
	public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
	{
		this.primaryKeyGenerator = primaryKeyGenerator;
	}
}

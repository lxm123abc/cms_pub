package com.zte.ismp.applog.dao;

import java.util.List;

import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.ssb.logmanager.business.model.VSsblog;

public class AppLogDAO extends DynamicObjectBaseDao implements IAppLogDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize) throws DAOException
    {
        log.debug("page query vSsblog by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryTvmsVSsblogListCntByCond", vSsblog)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<VSsblog> rsList = (List<VSsblog>) super.pageQuery("queryTvmsVSsblogListByCond", vSsblog, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query vSsblog by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryTvmsVSsblogListByCond", "queryTvmsVSsblogListCntByCond", vSsblog, start,
                pageSize, puEntity);
    }

}
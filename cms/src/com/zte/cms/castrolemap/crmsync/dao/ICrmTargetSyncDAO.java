package com.zte.cms.castrolemap.crmsync.dao;

import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICrmTargetSyncDAO
{
    /**
     * 根据条件分页查询CrmTargetSync对象 
     *
     * @param crmTargetSync CrmTargetSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DomainServiceException ds异常
     */
    public PageInfo pageInfoQuery(CrmTargetSync crmTargetSync, int start, int pageSize)throws DAOException;

}


  package com.zte.cms.impSyncTask.dao;

import java.util.List;

import com.zte.cms.impSyncTask.dao.IImpCmsSynctaskDAO;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ImpCmsSynctaskDAO extends DynamicObjectBaseDao implements IImpCmsSynctaskDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("insert impCmsSynctask starting...");
    	super.insert( "insertImpCmsSynctask", impCmsSynctask );
    	log.debug("insert impCmsSynctask end");
    }
	
	public void insertImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException
	{
		log.debug("insert impCmsSynctaskList starting...");
		if( null != impCmsSynctaskList )
		{
		super.insertBatch( "insertImpCmsSynctask", impCmsSynctaskList );
		}
		log.debug("insert impCmsSynctaskList end");
	}
     
    public void updateImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("update impCmsSynctask by pk starting...");
       	super.update( "updateImpCmsSynctask", impCmsSynctask );
       	log.debug("update impCmsSynctask by pk end");
    }
    public void updateImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException
    {
    	log.debug("update impCmsSynctaskList by pk starting...");
	super.updateBatch( "updateImpCmsSynctask", impCmsSynctaskList );
       	log.debug("update impCmsSynctaskList by pk end");
    }

    public void updateImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("update impCmsSynctask by conditions starting...");
    	super.update( "updateImpCmsSynctaskByCond", impCmsSynctask );
    	log.debug("update impCmsSynctask by conditions end");
    }
    public void updateImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException
    {
    	log.debug("update impCmsSynctaskList by conditions starting...");
    	super.updateBatch( "updateImpCmsSynctaskByCond", impCmsSynctaskList );
    	log.debug("update impCmsSynctaskList by conditions end");
    }   

    public void deleteImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("delete impCmsSynctask by pk starting...");
       	super.delete( "deleteImpCmsSynctask", impCmsSynctask );
       	log.debug("delete impCmsSynctask by pk end");
    }
    public void deleteImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException
    {
    	log.debug("delete impCmsSynctaskList by pk starting...");
       	super.deleteBatch( "deleteImpCmsSynctask", impCmsSynctaskList );
       	log.debug("delete impCmsSynctaskList by pk end");
    }

    public void deleteImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("delete impCmsSynctask by conditions starting...");
    	super.delete( "deleteImpCmsSynctaskByCond", impCmsSynctask );
    	log.debug("delete impCmsSynctask by conditions end");
    }
    public void deleteImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException
    {
    	log.debug("delete impCmsSynctaskList by conditions starting...");
    	super.deleteBatch( "deleteImpCmsSynctaskByCond", impCmsSynctaskList );
    	log.debug("delete impCmsSynctaskList by conditions end");
    }  

    public ImpCmsSynctask getImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("query impCmsSynctask starting...");
       	ImpCmsSynctask resultObj = (ImpCmsSynctask)super.queryForObject( "getImpCmsSynctask",impCmsSynctask);
       	log.debug("query impCmsSynctask end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException
    {
    	log.debug("query impCmsSynctask by condition starting...");
    	List<ImpCmsSynctask> rList = (List<ImpCmsSynctask>)super.queryForList( "queryImpCmsSynctaskListByCond",impCmsSynctask);
    	log.debug("query impCmsSynctask by condition end");
    	return rList;
    }
	@SuppressWarnings("unchecked")
    public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask, PageUtilEntity puEntity )throws DAOException
    {
        log.debug("query impCmsSynctask by condition starting...");
        List<ImpCmsSynctask> rList = (List<ImpCmsSynctask>)super.queryForList( "queryImpCmsSynctaskListByCond",impCmsSynctask,puEntity);
        log.debug("query impCmsSynctask by condition end");
        return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize)throws DAOException
    {
    	log.debug("page query impCmsSynctask by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryImpCmsSynctaskListCntByCond",  impCmsSynctask)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<ImpCmsSynctask> rsList = (List<ImpCmsSynctask>)super.pageQuery( "queryImpCmsSynctaskListByCond" ,  impCmsSynctask , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query impCmsSynctask by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryImpCmsSynctaskListByCond", "queryImpCmsSynctaskListCntByCond", impCmsSynctask, start, pageSize, puEntity);
    }
    
} 
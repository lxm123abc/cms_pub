//该js引用要放在riaframework.js引用之后
/**
 * 状态保存方法，查询历史状态  
 * 
 * @param ref               要绑定数据的控件如: "$M.obj" , 传空的时候不进行绑定， 
 * @param statusSaveObj     在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 "QueryConObj" 
 * @return
 */
function searchHisData(ref, statusSaveObj)
{
	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		statusSaveObj = "QueryConObj";
	}
	var tableHisStatus = parentObj[statusSaveObj];
	if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
	{
		var condition = tableHisStatus.condition;
		//将查询条件绑定到控件上
		if(typeof(ref) != "undefined" && ref != null)
		{
			setBindObject(ref, condition[0]);
		}	
		getTurnPageData(tableHisStatus.tid, condition,tableHisStatus.pageIndex, tableHisStatus.pagesize);
		
	}	
}

/**
 * 得到最顶层页面的对象
 * @return
 */
function getWindowParent()
{
	var parentObj = window.parent;
	var indexUrl = parentObj.location.href;
	var count = 0;
	while(indexUrl != undefined && indexUrl !=null && indexUrl.indexOf('/uiloader/index.html') == -1)
	{
		count++;
		parentObj = parentObj.parent;
		indexUrl = parentObj.location.href;
		if(count>20)
		{
			break;
		}
	}
	count = 0;
	return parentObj;
}

/**
 * 保存table历史状态
 * @param tid          table的id
 * @param condition    查询条件
 * @param pageIndex    历史页号
 * @param pagesize     每页展示数据大小
 * @return
 */
function TableHisStatus(tid, condition, pageIndex, pagesize)
{
	this.tid = tid;
	this.pageIndex = pageIndex;
	this.pagesize = pagesize;
	this.condition  = condition;
}

/**
 * 进去其他链接时, 如进入修改或者查询等页面前, 调用此方法保存状态  
 * @param tableID				<z:table> 的id  ， 
 * @param statusSaveObj         在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 "QueryConObj" 
 * @return
 */
function savePageStatus(tableID, statusSaveObj)
{
	var ztable = getTableById(tableID);
	var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		parentObj['QueryConObj']=hisStatus;
	}
	else
	{
		parentObj[statusSaveObj]=hisStatus;
	}
}
/**
 * 用于一个页面有两个服务时
 * 进去其他链接时 如进入修改或者查询等页面前 调用此方法保存状态  
 * @param tableID          <z:table> 的id  ， 
 * @param statusSaveObj    在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 "QueryConObj"
 * @return
 */
function savePageStatus2(tableID, statusSaveObj)
{
	var ztable = getTableById(tableID);
	var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		parentObj['QueryConObj']=hisStatus;
	}
	else
	{
		parentObj['serviceSaveObj']=hisStatus;
	}
}
/**
 * 查询历史状态   
 * @param ref              要绑定数据的控件如: "$M.obj" , 传空的时候不进行绑定， 
 * @param sid              <z:table>的sid 属性
 * @param statusSaveObj    在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 "QueryConObj"
 * @return
 */
function searchHisDataForFrame(ref, sid,statusSaveObj)
{
	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		statusSaveObj = "QueryConObj";
	}
	var tableHisStatus = parentObj[statusSaveObj];
	if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
	{
		var condition = tableHisStatus.condition;
		//将查询条件绑定到控件上
		if(typeof(ref) != "undefined" && ref != null)
		{
			setBindObject(ref, condition[0]);
		}	
         tableModel_arr[tableHisStatus.tid].sid = sid;
		getTurnPageData(tableHisStatus.tid, condition,tableHisStatus.pageIndex, tableHisStatus.pagesize);
		//parentObj[statusSaveObj]={};
	}	
}

/**
 * 使用历史查询时，得到历史查询的查询条件
 * @param statusSaveObj   在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 "QueryConObj"
 * @return        查询条件
 */
function getHisQueryCondition(statusSaveObj)
{
	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		statusSaveObj = "QueryConObj";
	}
	var tableHisStatus = parentObj[statusSaveObj];
	if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
	{
		var condition = tableHisStatus.condition;
		return condition[0];
	}
	else
	{
		return null;
	}
}
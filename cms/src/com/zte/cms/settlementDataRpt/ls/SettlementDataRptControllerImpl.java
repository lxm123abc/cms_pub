package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.settlementDataRpt.common.CommonReturnType;
import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;
import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.cms.settlementDataRpt.service.SettlementDataRptService;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.web.multipart.MultipartFile;
import com.zte.ssb.web.multipart.commons.CommonsMultipartFile;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * 目标系统的LS实现层
 *
 * @author sanya
 */

public class SettlementDataRptControllerImpl implements SettlementDataRptController {

    private Log logger = SSBBus.getLog(SettlementDataRptControllerImpl.class);

    private static Map<String, SettlementDataRptVO> searchMap = new HashMap<String, SettlementDataRptVO>();

    private SettlementDataRptService settlementDataRptService;

    public void setSettlementDataRptService(SettlementDataRptService settlementDataRptService) {
        this.settlementDataRptService = settlementDataRptService;
    }

    public TableDataInfo pageInfoQuery(SettlementDataRptVO settlementDataRptVO, int arg1, int arg2) throws Exception {
        logger.info("SettlementDataRptControllerImpl pageInfoQuery begin...");
        if (StringUtils.isNotEmpty(settlementDataRptVO.getSpName())) {
            settlementDataRptVO.setSpName("%" + settlementDataRptVO.getSpName() + "%");
        }

        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operid = loginUser.getOperid();
        //
        String userId = loginUser.getUserId();

        if (!UserInfoUtils.checkLoginRole(operid)) {
            //如果是CP用户,直接set查询条件为该cp
            settlementDataRptVO.setSpId(userId);
        }

        settlementDataRptVO.setStart(arg1);
        settlementDataRptVO.setPageSize(arg2);
        searchMap.put(operid, settlementDataRptVO);

        TableDataInfo tableDataInfo = settlementDataRptService.pageInfoQuery(settlementDataRptVO);
        return tableDataInfo;
    }

    public CommonReturnType importRpt(String file) throws Exception {
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        if (!UserInfoUtils.checkLoginRole(operId)) {
            return CommonReturnType.create("仅管理员有权限");
        }

        RIAContext currentInstance = RIAContext.getCurrentInstance();
        HttpServletRequest request = currentInstance.getRequest();
        /**
         *  获取文件并转换成 MultipartFile 接收多份文件
         */
        List<FileItem> list = (List<FileItem>) request.getAttribute("FileItemList");
        List<MultipartFile> multipartFiles = new ArrayList<MultipartFile>();
        FileItem tmp = null;
        Iterator iter = list.iterator();
        while (iter.hasNext()) {
            tmp = (FileItem) iter.next();
            multipartFiles.add(new CommonsMultipartFile(tmp));
        }
        /**
         *  循环读取文件 获取总返回值
         */
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < multipartFiles.size(); i++) {
            result.append("第" + (i + 1) + "份excel导入的信息:\n");
            result.append(settlementDataRptService.importRpt(multipartFiles.get(i)));
            result.append("\n");
        }
        return CommonReturnType.create(result.toString());
    }

    /**
     * 导出
     *
     * @param
     * @return
     */
    public SettlementDataRptVO exportRpt() {
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        String userId = loginUser.getUserId();
        if (!UserInfoUtils.checkLoginRole(operId)) {
            System.out.println("仅管理员有权限");
            return null;
        }
        //获取查询条件
        SettlementDataRptVO settlementDataRptVO = searchMap.get(operId);
        try {
            settlementDataRptService.exportRpt(settlementDataRptVO, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return settlementDataRptVO;
    }

    public CommonReturnType getProjectType() {
        List<String> projectType = settlementDataRptService.getProjectType();
        return CommonReturnType.create(projectType);
    }

    @Override
    public CommonReturnType deleteByIds(String[] ids) {
        if (ids == null) {
            return CommonReturnType.create(EmunBusinessError.PARAMETER_VALIDATION_ERROR.getErrorMsg());
        } else if (ids.length == 0) {
            return CommonReturnType.create("无选择数据");
        }
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        if (!UserInfoUtils.checkLoginRole(operId)) {
            return CommonReturnType.create("仅管理员有权限");
        }
        String result = settlementDataRptService.deleteByIds(ids);
        return CommonReturnType.create(result);
    }

    @Override
    public CommonReturnType updateById(SettlementDataRptVO settlementDataRptVO) {
        try {
            settlementDataRptService.updateById(settlementDataRptVO);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonReturnType.create("500");
        }
        return CommonReturnType.create("200");
    }

    @Override
    public CommonReturnType updateIsConfirmById(String id) {

        try {
            settlementDataRptService.updateIsConfirmById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonReturnType.create("确认失败");
        }
        return CommonReturnType.create("确认成功");
    }

    @Override
    public CommonReturnType selectOneById(String id) {
        if (id == null || id.length() == 0) {
            return CommonReturnType.create(EmunBusinessError.PARAMETER_VALIDATION_ERROR.getErrorMsg());
        }
        SettlementDataRpt settlementDataRpt = settlementDataRptService.seletcOneById(id);
        SettlementDataRptVO settlementDataRptVO = new SettlementDataRptVO();
        try {
            PropertyUtils.copyProperties(settlementDataRptVO, settlementDataRpt);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (settlementDataRptVO.getRptDt() != null) {
            settlementDataRptVO.setRptDtStr(DateUtils.convertDataToString_fit(settlementDataRptVO.getRptDt(), DateUtils.fit_yyyy_MM));
        }
        return CommonReturnType.create(settlementDataRptVO);
    }

    @Override
    public CommonReturnType addSettlementDataRpt(SettlementDataRptVO settlementDataRptVO) {
        String result = null;
        try {
            settlementDataRptVO.setRptDt(DateUtils.convert_fit_yyyyMM(settlementDataRptVO.getRptDtStr()));
            result = settlementDataRptService.insertSettlementDataRpt(settlementDataRptVO);
        } catch (Exception e) {
            e.printStackTrace();
            return CommonReturnType.create("500");
        }
        if (result.equals("OK")){
            return CommonReturnType.create(result);
        }else {
            return CommonReturnType.create(result,"fail");
        }

    }

    @Override
    public CommonReturnType getThisLoginingRole() {
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        String userId = loginUser.getUserId();

        if (!UserInfoUtils.checkLoginRole(operId)) {
            return CommonReturnType.create(userId);
        }else {
            return CommonReturnType.create("super");
        }
    }


}

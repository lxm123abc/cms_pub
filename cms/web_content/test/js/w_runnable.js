
function newQueryTask(showclear, showcreate, pcheck, pname, pvalue, ptype) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr,myTh,myTd;
	myTr = queryBodyObj.insertRow(queryBodyObj.rows.length - 1);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "名称";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pname\" value='" + nvl(pname, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showclear ? 
		"<a href=\"#\" onclick=\"clearQueryProp(this.parentNode.parentNode.rowIndex)\"> 删除 </a>" : "");
	myTr.appendChild(myTd);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "值";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pvalue\" value='" + nvl(pvalue, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showcreate ? 
		"<a href=\"#\" onclick=\"hrefJsonStr(this,'&')\"> 生成值 </a>" : "");
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 3);
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
}

function buildQueryHTMLByTask(pclass, ptask, pexpress) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr,myTh,myTd;
	myTr = document.createElement("tr");
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "任务名称";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"ptask\" value='" + nvl(ptask, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "表达式";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pexpress\" value='" + nvl(pexpress, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = "<a href=\"#\" onclick=\"hrefJsonStr(this,'&')\"> 生成值 </a>";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 3);
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildTaskButton(pclass, isTaskEdit) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 清 空 任 务 \"" + 
		" onclick=\"clearTableHTML()\" />";
	if (pclass == "thread") {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 添 加 入 参 \"" +
				" onclick=\"newQueryTask(true, true, false)\" />";
	}
	if (!bol(isTaskEdit)) {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 保 存 任 务 \"" +
			" onclick=\"doTaskInsert('" + pclass + "')\" />";
	}
	else {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 修 改 任 务 \"" + 
			" onclick=\"doTaskEditCallback('" + pclass + "')\" />";
	}
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildQueryHTMLByTaskOut(pid, ptask, pparam, starttime, endtime, costtime) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("align", "center");
	myTd.setAttribute("width", "45px");
	myTd.innerHTML = pid;
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.innerHTML = ptask;
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.innerHTML = pparam;
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.innerHTML = starttime;
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.innerHTML = endtime;
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.innerHTML = costtime;
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildTaskOutButton() {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 清 除 \" onclick=\"clearTaskChart()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 线 图 \" onclick=\"doTaskChart()\" />";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildTableTHHTMLByTask() {
	var xtabTr = "<tr>";
	xtabTr += "<th width=\"30px\"></th>";
	xtabTr += "<th width=\"45px\"><input type=\"checkbox\" id=\"tabchk\" onclick=\"parent.doSelectAll()\" /></th>";
	xtabTr += "<th>this.task</th>";
	xtabTr += "<th>this.class</th>";
	xtabTr += "<th>this.param</th>";
	xtabTr += "<th>this.express</th>";
	xtabTr += "</tr>";
	return xtabTr;
}

function buildTableTRHTMLByTask(jsObj) {
	var xtabTr = "<tr>";
	xtabTr += "<td align=\"center\"></td>";
	xtabTr += "<td align=\"center\"><input type=\"checkbox\" id=\"trchk\"/></td>";
	xtabTr += "<td><label id=\"lbltask\">" + createJsonStr(jsObj.ptask) + "</label></td>";
	xtabTr += "<td><label id=\"lblclass\">" + createJsonStr(jsObj.pclass) + "</label></td>";
	xtabTr += "<td><label id=\"lblparam\">" + createJsonStr(jsObj.pparam) + "</label></td>";
	xtabTr += "<td><label id=\"lblexpress\">" + createJsonStr(jsObj.pexpress) + "</label></td>";
	xtabTr += "</tr>";
	return xtabTr;
}

function buildTableHTMLByTask(jsObj) {
	clearTableHTML();
	var xtab = "<table border = 1>";
	xtab += buildTableTHHTMLByTask();
	for (var i=0; i<jsObj.length; i++) {
		xtab += buildTableTRHTMLByTask(jsObj[i]);
	}	
	xtab += "</table>";
	$("#xtable").contents().find("#xtable").html(xtab);
	buildTableButtonByTask();
}

function buildTableButtonByTask() {
	var tableBodyObj = document.getElementById("tableBody");
	var myTr = tableBodyObj.insertRow();
	var myTd = myTr.insertCell();
	myTd.setAttribute("align", "left");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 删 除 \" onclick=\"doTaskGC()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 修 改 \" onclick=\"doTaskEdit()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 复 制 \" onclick=\"doTaskCopy()\" />";
	myTd = myTr.insertCell();
	myTd.setAttribute("align", "right");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 执 行 \" onclick=\"doTaskRun()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 保 存 文 件 \" onclick=\"parent.frames['xtree'].saveFile('runnable')\" />";
}
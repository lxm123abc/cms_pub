package com.zte.cms.content.modwkfw;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.service.ICmsProgramBufDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;

import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class CntModApplyCallback extends CommonCallback
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    protected ICmsProgramDS cmsProgramDS = null;
    protected ICmsProgramBufDS cmsProgramBufDS = null;
    protected ICmsProgramWkfwhisDS cmsProgramWkfwhisDS = null;
    protected ICmsMovieDS cmsMovieDS = null;
    protected ICmsMovieBufDS cmsMovieBufDS = null;

    protected CmsProgram cmsProgram;
    protected CmsProgramBuf cmsProgramBuf;
    protected CmsProgramWkfwhis cmsProgramWkfwhis;
    protected List<CmsMovieBuf> cmsMovieBufList;

    public AppLogInfoEntity getLogInfo()
    {
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        ResourceMgt.addDefaultResourceBundle("log_program_resource");

        String optType = ""; // 操作类型
        String optObjecttype = ""; // 操作对象类型
        String optObject = cmsProgramBuf.getProgramid();
        String optDetail = ""; // 操作详细说明

        if (wkflItem.getNodeId() == 1)
        {// 申请
            optType = ResourceMgt.findDefaultText("log.opttype.cnt.mgt"); // 点播内容管理
            optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.modapply"); // 内容修改申请
            optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.modapply");
        }
        else
        {
            optType = ResourceMgt.findDefaultText("log.opttype.cnt.auditmgt"); // 内容审核管理
            optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.modaudit"); // 内容修改[
            // nodename
            // ]
            optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.modaudit");
        }

        if (null != optObjecttype)
        {
            optObjecttype = optObjecttype.replaceAll("\\[nodename\\]", wkflItem.getNodeName());
        }

        if (null != optDetail)
        {
            optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(wkflItem.getExcutorName()));
            optDetail = optDetail.replaceAll("\\[programid\\]", String.valueOf(cmsProgram.getProgramid()));
            optDetail = optDetail.replaceAll("\\[nodename\\]", wkflItem.getNodeName());
            optDetail = optDetail.replaceAll("\\[handleevent\\]", wkflItem.getHandleEvent());
        }

        loginfo.setOptType(optType);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(optObject);
        loginfo.setOptDetail(optDetail);
        loginfo.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long programindex = wkflItem.getPboIndex();
        try
        {
            // 根据programindex得到buf表中的数据
            cmsProgramBuf = new CmsProgramBuf();
            cmsProgramBuf.setProgramindex(programindex);
            cmsProgramBuf = cmsProgramBufDS.getCmsProgramBuf(cmsProgramBuf);

            cmsProgramBuf.setWorkflowlife(wkflItem.getPboState());

            // 根据programindex得到内容基本信息
            cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(programindex);
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            cmsProgram.setWorkflowlife(wkflItem.getPboState());

            // 获取内容关联的所有子内容的buf记录
            CmsMovieBuf cmsMovieBuf = new CmsMovieBuf();
            cmsMovieBuf.setProgramindex(programindex);
            cmsMovieBufList = cmsMovieBufDS.getCmsMovieBufByCond(cmsMovieBuf);

            // 工作流对象处理
            wkflItem.setEffectiveTime("0");
            wkflItem.setPboId(String.valueOf(cmsProgramBuf.getProgramindex()));

            // 内容工作流历史对象处理
            cmsProgramWkfwhis = new CmsProgramWkfwhis();
            BeanCopier copier = BeanCopier.create(CmsProgramBuf.class, CmsProgramWkfwhis.class, false);
            copier.copy(cmsProgramBuf, cmsProgramWkfwhis, null);

            cmsProgramWkfwhis.setWorkflowindex(new Long(wkflItem.getFlowInstId()));
            cmsProgramWkfwhis.setTaskindex(new Long(wkflItem.getTaskId()));
            cmsProgramWkfwhis.setNodeid(wkflItem.getNodeId().longValue());
            cmsProgramWkfwhis.setNodename(wkflItem.getNodeName());
            cmsProgramWkfwhis.setOpername(wkflItem.getExcutorId());
            cmsProgramWkfwhis.setTemplateindex(wkflItem.getTemplateSeqNo().longValue());
            cmsProgramWkfwhis.setOpopinion(wkflItem.getComments());
            cmsProgramWkfwhis.setHandleevent(wkflItem.getHandleEvent());
            if (wkflItem.getStartTime() != null)
            {
                cmsProgramWkfwhis
                        .setWorkflowstarttime(EspecialCharMgt.conversionDateToStr(wkflItem.getStartTime(), 14));
            }
            if (wkflItem.getFinishTime() != null)
            {
                cmsProgramWkfwhis.setWorkflowendtime(EspecialCharMgt.conversionDateToStr(wkflItem.getFinishTime(), 14));
            }

        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in initData method of CntModApplyCallback", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void operate()
    {
        try
        {
            if (wkflItem.getPboState() == -1)
            {// 审核不通过
               
                cmsProgram.setStatus(cmsProgramBuf.getStatus());

                // 删除内容信息缓存记录
                cmsProgramBufDS.removeCmsProgramBuf(cmsProgramBuf);
                // 删除子内容buf信息
                if (null != cmsMovieBufList && cmsMovieBufList.size() > 0)
                {
                    cmsMovieBufDS.removeCmsMovieBufList(cmsMovieBufList);
                }

                // 更新内容工作流类型为无
                cmsProgram.setWorkflow(0);
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                // 更新生效时间为当前时间
                cmsProgram.setEffecttime(format.format(new Date()));
            }
            else
            {
                // 由于审核时不允许修改内容及子内容信息，所以此句可去掉
                cmsProgramBufDS.updateCmsProgramBuf(cmsProgramBuf);

                String route = cmsProgramDS.getModRoute(cmsProgram);
                int r1 = Integer.valueOf(route.substring(4, 5)).intValue();
                int r2 = Integer.valueOf(route.substring(9, 10)).intValue();
                if (wkflItem.getNodeId().longValue() == 1)
                {// 提交修改申请
                    if (r1 == 0 && r2 == 0)
                    {// 免一审并且免二审，置为三审中170
                        cmsProgram.setStatus(170);
                    }
                    if (r1 == 0 && r2 == 1)
                    {// 免一审且不免二审，置为二审中140
                        cmsProgram.setStatus(140);
                    }
                    if (r1 == 1 && r2 == 0)
                    {// 走一审且免二审，置为一审中120
                        cmsProgram.setStatus(120);
                    }
                    if (r1 == 1 && r2 == 1)
                    {// 一审、二审都不免 置为一审中120
                        cmsProgram.setStatus(120);
                    }
                }
                else
                {
                    //if ("一审".equals(wkflItem.getNodeName()))
                    if (ResourceMgt.findDefaultText("cnt.first.audit").equals(wkflItem.getNodeName()))
                    {
                        if (r2 == 0)
                        {// 免二审，置为三审中170
                            cmsProgram.setStatus(170); // 170:三审中
                        }
                        else
                        {// 不免二审，置为二审中140
                            cmsProgram.setStatus(140); // 140:二审中
                        }
                    }
                    //else if ("二审".equals(wkflItem.getNodeName()))
                    else if (ResourceMgt.findDefaultText("cnt.second.audit").equals(wkflItem.getNodeName()))
                    {
                        cmsProgram.setStatus(170); // 170:三审中
                    }
                }

            }

            cmsProgramDS.updateCmsProgram(cmsProgram);

            // 更新子内容状态为当前内容的状态
            List<CmsMovie> cmsMovieList = null;
            CmsMovie cmsMovie = new CmsMovie();
            cmsMovie.setProgramindex(cmsProgram.getProgramindex());
            cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
            if (null != cmsMovieList && cmsMovieList.size() > 0)
            {
                for (int i = 0; i < cmsMovieList.size(); i++)
                {
                    cmsMovieList.get(i).setStatus(cmsProgram.getStatus());
                }
                cmsMovieDS.updateCmsMovieList(cmsMovieList);
            }

            cmsProgramWkfwhisDS.insertCmsProgramWkfwhis(cmsProgramWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("Error occurred in operate method of CntModApplyCallback", dsEx);
            throw new RuntimeException(dsEx);
        }
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsProgramBufDS(ICmsProgramBufDS cmsProgramBufDS)
    {
        this.cmsProgramBufDS = cmsProgramBufDS;
    }

    public void setCmsProgramWkfwhisDS(ICmsProgramWkfwhisDS cmsProgramWkfwhisDS)
    {
        this.cmsProgramWkfwhisDS = cmsProgramWkfwhisDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsMovieBufDS(ICmsMovieBufDS cmsMovieBufDS)
    {
        this.cmsMovieBufDS = cmsMovieBufDS;
    }

}

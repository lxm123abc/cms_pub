package com.zte.cms.EMBtask.service;

import java.util.List;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IMovieTechauditResultDS
{
    /**
     * 新增MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @throws DomainServiceException ds异常
     */
    public void insertMovieTechauditResult(MovieTechauditResult movieTechauditResult) throws DomainServiceException;

    /**
     * 根据条件更新MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException;

    /**
     * 根据条件批量更新MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateMovieTechauditResultListByCond(List<MovieTechauditResult> movieTechauditResultList)
            throws DomainServiceException;

    /**
     * 根据条件删除MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException;

    /**
     * 根据条件批量删除MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeMovieTechauditResultListByCond(List<MovieTechauditResult> movieTechauditResultList)
            throws DomainServiceException;

    /**
     * 根据条件查询MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @return 满足条件的MovieTechauditResult对象集
     * @throws DomainServiceException ds异常
     */
    public List<MovieTechauditResult> getMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException;

    /**
     * 根据条件分页查询MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
package com.zte.cms.cast.castsync.service;

import java.util.List;

import com.zte.cms.cast.castsync.dao.ICastPlatformSyncDAO;
import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CastPlatformSyncDS implements ICastPlatformSyncDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    
    private ICastPlatformSyncDAO dao = null;
    
    public void setDao(ICastPlatformSyncDAO dao)
    {
        this.dao = dao;
    }
    
    public TableDataInfo pageInfoQuery( CastPlatformSync castPlatformSync, int start, int pageSize)throws DomainServiceException
    {
        log.debug( "get castPlatformSync page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castPlatformSync, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CastPlatformSync>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get castPlatformSync page info by condition end" );
        return tableInfo;
    }
}

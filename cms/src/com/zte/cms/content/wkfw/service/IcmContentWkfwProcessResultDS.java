package com.zte.cms.content.wkfw.service;

import java.util.List;

import com.zte.cms.content.wkfw.dao.IIcmContentWkfwProcessResultDAO;
import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwProcessResult;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class IcmContentWkfwProcessResultDS implements IIcmContentWkfwProcessResultDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmContentWkfwProcessResultDAO dao = null;

    public void setDao(IIcmContentWkfwProcessResultDAO dao)
    {
        this.dao = dao;
    }

    public List<IcmContentWkfwProcessResult> getUappablWkfwProcessResultByCond(
            IcmContentWkfwCondition contentWkfwCondition) throws DomainServiceException
    {
        log.debug("get IcmContentWkfwProcessResult by condition starting...");
        List<IcmContentWkfwProcessResult> rsList = null;
        try
        {

            rsList = dao.getIcmContentWkfwProcessResultByCond(contentWkfwCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get IcmContentWkfwProcessResult by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get uappablWkfwProcessResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(contentWkfwCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmContentWkfwProcessResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get uappablWkfwProcessResult page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get IcmContentWkfwProcessResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(contentWkfwCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmContentWkfwProcessResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get IcmContentWkfwProcessResult page info by condition end");
        return tableInfo;
    }

    public long getMyStartWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition)
            throws DomainServiceException
    {
        log.debug("get getMyStartWorkflowItemListCount info by  condition starting...");
        long result = 0;
        try
        {

            result = dao.getMyStartWorkflowItemListCount(contentWkfwCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getMyStartWorkflowItemListCount info by condition end");
        return result;
    }

}

<%@ page session="true" import="java.util.*" %>
<%
  session.invalidate();
  response.sendRedirect("index.html");
%>
User login info will be invalid, you must login again! 

package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.cms.channel.dao.IPhysicalchannelBufDAO;
import com.zte.cms.channel.service.IPhysicalchannelBufDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class PhysicalchannelBufDS implements IPhysicalchannelBufDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IPhysicalchannelBufDAO dao = null;
	
	public void setDao(IPhysicalchannelBufDAO dao)
	{
	    this.dao = dao;
	}

	public void insertPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug("insert physicalchannelBuf starting...");
		try
		{
			dao.insertPhysicalchannelBuf( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert physicalchannelBuf end");
	}
	
	public void updatePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug("update physicalchannelBuf by pk starting...");
	    try
		{
			dao.updatePhysicalchannelBuf( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update physicalchannelBuf by pk end");
	}

	public void updatePhysicalchannelBufList( List<PhysicalchannelBuf> physicalchannelBufList )throws DomainServiceException
	{
		log.debug("update physicalchannelBufList by pk starting...");
		if(null == physicalchannelBufList || physicalchannelBufList.size() == 0 )
		{
			log.debug("there is no datas in physicalchannelBufList");
			return;
		}
	    try
		{
			for( PhysicalchannelBuf physicalchannelBuf : physicalchannelBufList )
			{
		    	dao.updatePhysicalchannelBuf( physicalchannelBuf );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update physicalchannelBufList by pk end");
	}



	public void removePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug( "remove physicalchannelBuf by pk starting..." );
		try
		{
			dao.deletePhysicalchannelBuf( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove physicalchannelBuf by pk end" );
	}
	
	public void removePhysicalchannelBufByChId( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug( "remove physicalchannelBuf by pk starting..." );
		try
		{
			dao.deletePhysicalchannelBufByChId( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove physicalchannelBuf by pk end" );
	}

	public void removePhysicalchannelBufList ( List<PhysicalchannelBuf> physicalchannelBufList )throws DomainServiceException
	{
		log.debug( "remove physicalchannelBufList by pk starting..." );
		if(null == physicalchannelBufList || physicalchannelBufList.size() == 0 )
		{
			log.debug("there is no datas in physicalchannelBufList");
			return;
		}
		try
		{
			for( PhysicalchannelBuf physicalchannelBuf : physicalchannelBufList )
			{
				dao.deletePhysicalchannelBuf( physicalchannelBuf );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove physicalchannelBufList by pk end" );
	}



	public PhysicalchannelBuf getPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug( "get physicalchannelBuf by pk starting..." );
		PhysicalchannelBuf rsObj = null;
		try
		{
			rsObj = dao.getPhysicalchannelBuf( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get physicalchannelBufList by pk end" );
		return rsObj;
	}

	public List<PhysicalchannelBuf> getPhysicalchannelBufByCond( PhysicalchannelBuf physicalchannelBuf )throws DomainServiceException
	{
		log.debug( "get physicalchannelBuf by condition starting..." );
		List<PhysicalchannelBuf> rsList = null;
		try
		{
			rsList = dao.getPhysicalchannelBufByCond( physicalchannelBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get physicalchannelBuf by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get physicalchannelBuf page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(physicalchannelBuf, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<PhysicalchannelBuf>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get physicalchannelBuf page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get physicalchannelBuf page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(physicalchannelBuf, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<PhysicalchannelBuf>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get physicalchannelBuf page info by condition end" );
		return tableInfo;
	}
}

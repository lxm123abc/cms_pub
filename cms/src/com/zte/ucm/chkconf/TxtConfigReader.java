package com.zte.ucm.chkconf;

import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TxtConfigReader
{
  Logger log = LoggerFactory.getLogger(TxtConfigReader.class);

  public void readTXTConfig(Config paramConfig)
  {
    String str1 = paramConfig.getKey();
    String str2 = "";
    Resultlog localResultlog = new Resultlog();
    File localFile = new File(paramConfig.getFile());
    if (!localFile.exists())
    {
      this.log.warning("Operation WARN:config file[" + paramConfig.getFile() + "] is not exist!!!");

      paramConfig.setCode(null);

      return;
    }

    String str3 = null;
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(paramConfig.getFile()), "utf-8"));

      String str4 = "";
      while ((str4 = localBufferedReader.readLine()) != null)
      {
        if ((str4.trim().startsWith("#")) || (!str4.contains("=")) || (!str4.contains(str1)))
        {
          continue;
        }
        str3 = getTxtValue(str1, str4);
        this.log.info("Operation success:config file[" + paramConfig.getFile() + "]  key[" + str1 + "] value[" + str3 + "]");
      }

      localBufferedReader.close();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }

    if (str3 == null)
    {
      this.log.warning("Operation success:config file[" + paramConfig.getFile() + "]  key[" + str1 + "] is not exist!!!");

      paramConfig.setCode(null);

      return;
    }

    paramConfig.setValue(str3);
  }

  private String getTxtValue(String paramString1, String paramString2)
  {
    String str1 = "";

    if ((paramString2.trim().startsWith("#")) || (!paramString2.contains("=")) || (!paramString2.contains(paramString1))) {
      return str1;
    }

    paramString2 = paramString2.split("#")[0];

    String[] arrayOfString = paramString2.split(";");
    for (int i = 0; i < arrayOfString.length; i++) {
      String str2 = arrayOfString[i];
      if (!str2.contains(paramString1))
        continue;
      int j = str2.indexOf(paramString1);

      int k = str2.indexOf("=", j);

      str2 = str2.substring(k + 1);

      str1 = str2.trim();
    }

    return str1;
  }
}
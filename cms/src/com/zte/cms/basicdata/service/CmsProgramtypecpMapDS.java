package com.zte.cms.basicdata.service;

import java.util.List;

import com.zte.cms.basicdata.dao.ICmsProgramtypecpMapDAO;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsProgramtypecpMapDS extends DynamicObjectBaseDS implements ICmsProgramtypecpMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramtypecpMapDAO dao = null;

    public void setDao(ICmsProgramtypecpMapDAO dao)
    {
        this.dao = dao;
    }

    public CmsProgramtypecpMap getCmsProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException
    {
        log.debug("get cmsProgramtypecpMap by pk starting...");
        CmsProgramtypecpMap rsObj = null;
        try
        {
            rsObj = dao.getCmsProgramtypecpMap(cmsProgramtypecpMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramtypecpMapList by pk end");
        return rsObj;
    }

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapByCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException
    {
        log.debug("get cmsProgramtypecpMap by condition starting...");
        List<CmsProgramtypecpMap> rsList = null;
        try
        {
            rsList = dao.getCmsProgramtypecpMapByCond(cmsProgramtypecpMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramtypecpMap by condition end");
        return rsList;
    }

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException
    {
        log.debug("get cmsProgramtypecpMap by condition starting...");
        List<CmsProgramtypecpMap> rsList = null;
        try
        {
            rsList = dao.getCmsProgramtypecpMapCond(cmsProgramtypecpMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramtypecpMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsProgramtypecpMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramtypecpMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtypecpMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramtypecpMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsProgramtypecpMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramtypecpMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtypecpMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramtypecpMap page info by condition end");
        return tableInfo;
    }

    // 根据typeid查询CP信息
    public TableDataInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get queryCPByTypid page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryCPByTypid(cmsProgramtypecpMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtypecpMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get queryCPByTypid page info by condition end");
        return tableInfo;
    }

    public TableDataInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get queryCPByTypid page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryCmsProgramtypeByCpId(cmsProgramtypecpMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramtypecpMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get queryCPByTypid page info by condition end");
        return tableInfo;
    }

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        return dao.queryCPByTypidCount(cmsProgramtypecpMap);
    }

    public void insertProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_programtypecp_map");
        cmsProgramtypecpMap.setMapindex(mapindex);
        dao.insertProgramtypecpMap(cmsProgramtypecpMap);
    }

    public void deleteProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        dao.deleteProgramtypecpMap(cmsProgramtypecpMap);
    }

}

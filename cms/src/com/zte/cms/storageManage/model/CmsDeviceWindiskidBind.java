package com.zte.cms.storageManage.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsDeviceWindiskidBind extends DynamicBaseObject
{
    private java.lang.String deviceid;
    private java.lang.String windiskid;
    private java.lang.String reserve1;
    private java.lang.String reserve2;

    public java.lang.String getDeviceid()
    {
        return deviceid;
    }

    public void setDeviceid(java.lang.String deviceid)
    {
        this.deviceid = deviceid;
    }

    public java.lang.String getWindiskid()
    {
        return windiskid;
    }

    public void setWindiskid(java.lang.String windiskid)
    {
        this.windiskid = windiskid;
    }

    public java.lang.String getReserve1()
    {
        return reserve1;
    }

    public void setReserve1(java.lang.String reserve1)
    {
        this.reserve1 = reserve1;
    }

    public java.lang.String getReserve2()
    {
        return reserve2;
    }

    public void setReserve2(java.lang.String reserve2)
    {
        this.reserve2 = reserve2;
    }

    public void initRelation()
    {
        this.addRelation("deviceid", "DEVICEID");
        this.addRelation("windiskid", "WINDISKID");
        this.addRelation("reserve1", "RESERVE1");
        this.addRelation("reserve2", "RESERVE2");
    }
}

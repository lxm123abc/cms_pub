package com.zte.cms.service.wkfw.dao;

import java.util.List;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwProcessWithMe;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceWkfwProcessWithMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ServiceWkfwProcessWithMe> queryServiceWkfwProcessWithMeListByCond(
            ServiceWkfwCondition serviceWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int queryServiceWkfwProcessWithMeListCntByCond(
            ServiceWkfwCondition serviceWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start,
            int pageSize) throws DAOException;

    public abstract PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DAOException;

}
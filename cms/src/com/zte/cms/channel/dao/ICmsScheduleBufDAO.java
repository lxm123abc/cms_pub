package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsScheduleBufDAO
{
     /**
	  * 新增CmsScheduleBuf对象 
	  * 
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @throws DAOException dao异常
	  */	
     public void insertCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException;
     
     /**
	  * 根据主键更新CmsScheduleBuf对象
	  * 
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @throws DAOException dao异常
	  */
     public void updateCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException;


     /**
	  * 根据主键删除CmsScheduleBuf对象
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @throws DAOException dao异常
	  */
     public void deleteCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException;
     
     
     /**
	  * 根据主键查询CmsScheduleBuf对象
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @return 满足条件的CmsScheduleBuf对象
	  * @throws DAOException dao异常
	  */
     public CmsScheduleBuf getCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException;
     
     /**
	  * 根据条件查询CmsScheduleBuf对象 
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @return 满足条件的CmsScheduleBuf对象集
	  * @throws DAOException dao异常
	  */
     public List<CmsScheduleBuf> getCmsScheduleBufByCond( CmsScheduleBuf cmsScheduleBuf )throws DAOException;

     /**
	  * 根据条件分页查询CmsScheduleBuf对象，作为查询条件的参数
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CmsScheduleBuf对象，作为查询条件的参数
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
        
}
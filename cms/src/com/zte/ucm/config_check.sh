#!/bin/bash
## Usage:sh /home/UCM/CMS_WEB/shell/config_check.sh -filename "/home/UCM/CMS_WEB/configdeliver/00320111118123401/config_apply_items.xml"
CONFIGWORKDIR=/home/UCM/CMS_WEB/shell/
CONFIGITEMSFILE=/home/UCM/CMS_WEB/shell/config_apply_items.xml
LOGPATH=/home/UCM/CMS_WEB/shell/log
ERRORFILENAME=/home/UCM/CMS_WEB/shell/config_check.log

ZXJAR=/home/zxin10/was/tomcat/shared/lib
JAVA_HOME=/usr/java/java
JAVABIN=${JAVA_HOME}/bin

LOGFILE=log_`date +%Y%m%d`.log
getTimeStamp()
{
  echo `date +"%Y-%m-%d %H:%M:%S"`
}
log()
{
  if [ ! -d ${LOGPATH} ]; then
  	  mkdir -p ${LOGPATH}
  fi;
  echo "@@@ $1"
  echo "`getTimeStamp` : @@@ $1" >> ${LOGPATH}/${LOGFILE}
}

if [ "$1" == "-filename" ]; then
    CONFIGITEMSFILE=$2
fi; 

if [ ! -d ${CONFIGWORKDIR} ]; then
	log "CONFIGWORKDIR is not exist:${CONFIGWORKDIR}"
	exit 1;
fi;

if [ ! -f ${CONFIGITEMSFILE} ]; then 
	log "CONFIGITEMSFILE is not exist:${CONFIGITEMSFILE}"
	exit 1;
fi;

log "===== START CHECKING CONFIG ====="

dom4jjar=`ls ${ZXJAR}/dom4j-1*.jar`
jaxenjar=`ls ${ZXJAR}/jaxen*.jar`

count=`ls ${CONFIGWORKDIR}*.jar|grep "dom4j-1"|wc -l`
count1=`ls ${CONFIGWORKDIR}*.jar|grep "jaxen"|wc -l`

workdirdom4jjar=`ls ${CONFIGWORKDIR}dom4j-1*.jar > /dev/null  2>&1`
workdirjaxenjar=`ls ${CONFIGWORKDIR}jaxen*.jar > /dev/null  2>&1`

if [ $count -eq 0 -o $count1 -eq 0 ]; then
    ##log "===== execute with dom4j and jaxen in zxjos ====="
	${JAVABIN}/java -jar -Xbootclasspath/a:${dom4jjar}:${jaxenjar} ${CONFIGWORKDIR}/cmsucm.jar 1 ${CONFIGWORKDIR} ${CONFIGITEMSFILE} ${LOGPATH} ${ERRORFILENAME}
else
    ##log "===== execute with dom4j and jaxen in workdir ====="
	${JAVABIN}/java -jar ${CONFIGWORKDIR}/cmsucm.jar 1 ${CONFIGWORKDIR} ${CONFIGITEMSFILE} ${LOGPATH} ${ERRORFILENAME}
fi;

count=`cat ${ERRORFILENAME}`
if [[ $count -eq 0 ]]; then
    log "===== CONFIG CHECKED SUCCESSFUL ====="
	exit 0;
else
	log "===== CONFIG CHECKED FAILURE ====="
	exit 1;
fi;

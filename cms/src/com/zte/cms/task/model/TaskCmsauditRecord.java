package com.zte.cms.task.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class TaskCmsauditRecord extends DynamicBaseObject {

    private Integer id;
    private Integer taskid;
    private String auditdesc;
    private Integer auditstatus;
    private String auditcredt;
    private Integer step1;
    private Integer step2;
    private String remark1;
    private String remark2;


    private String exportNewName;
    private String exportFileName;


    @Override
    public String toString() {
        return "TaskCmsauditRecord{" +
                "id=" + id +
                ", taskid=" + taskid +
                ", auditdesc='" + auditdesc + '\'' +
                ", auditstatus=" + auditstatus +
                ", auditcredt='" + auditcredt + '\'' +
                ", step1=" + step1 +
                ", step2=" + step2 +
                ", remark1='" + remark1 + '\'' +
                ", remark2='" + remark2 + '\'' +
                '}';
    }

    public String getExportNewName() {
        return exportNewName;
    }

    public void setExportNewName(String exportNewName) {
        this.exportNewName = exportNewName;
    }

    public String getExportFileName() {
        return exportFileName;
    }

    public void setExportFileName(String exportFileName) {
        this.exportFileName = exportFileName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskid() {
        return taskid;
    }

    public void setTaskid(Integer taskid) {
        this.taskid = taskid;
    }

    public String getAuditdesc() {
        return auditdesc;
    }

    public void setAuditdesc(String auditdesc) {
        this.auditdesc = auditdesc;
    }

    public Integer getAuditstatus() {
        return auditstatus;
    }

    public void setAuditstatus(Integer auditstatus) {
        this.auditstatus = auditstatus;
    }

    public String getAuditcredt() {
        return auditcredt;
    }

    public void setAuditcredt(String auditcredt) {
        this.auditcredt = auditcredt;
    }

    public Integer getStep1() {
        return step1;
    }

    public void setStep1(Integer step1) {
        this.step1 = step1;
    }

    public Integer getStep2() {
        return step2;
    }

    public void setStep2(Integer step2) {
        this.step2 = step2;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    @Override
    public void initRelation() {

    }
}

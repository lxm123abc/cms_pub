<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.zte.log.*" %>
<%@ page language="java" contentType="text/html;charset=GBK" %>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%!
    public String transferString(String str) throws Exception {
        try {
            return (new String(str.getBytes("ISO8859_1"),"GBK")).trim();
        }
        catch (UnsupportedEncodingException e) {
            throw new Exception (e.getMessage() + "\r\n使用了不支持的字符类型！");
        }
    }

    public String display(String str, String infoChar) {
        String displayStr = "  <tr>";
        String infoType = "";
        if (str.length() < 20)
            return "";
        if (infoType == null)
            return "";
        infoType = str.substring(0,1);
        if (infoType.equalsIgnoreCase("E") && (infoChar.length() == 0 || infoChar.equalsIgnoreCase("E")))
            displayStr = displayStr + "<td align=\"center\"><img src=\"error.gif\" alt=\"错误\"></td>";
        else if (infoType.equalsIgnoreCase("W") && (infoChar.length() == 0 || infoChar.equalsIgnoreCase("W")))
            displayStr = displayStr + "<td align=\"center\"><img src=\"warning.gif\" alt=\"警告\"></td>";
        else if (infoType.equalsIgnoreCase("I") && (infoChar.length() == 0 || infoChar.equalsIgnoreCase("I")))
            displayStr = displayStr + "<td align=\"center\"><img src=\"info.gif\" alt=\"一般\"></td>";
        else
            return "";
        displayStr = displayStr + "<td>" + str.substring(1,20) + "</td><td>" + str.substring(20) + "</td></tr>\r\n";
        return displayStr;
    }
%>
<%
    String Remoteoper=(String)session.getAttribute("OPERNAME");
    if(Remoteoper==null||(!Remoteoper.equals("super")))
        throw new Exception ("您没有权限使用本系统！");
    Vector vet = null;
    String infoType = request.getParameter("infotype") == null ? "" : (String)request.getParameter("infotype");
    String logFile = request.getParameter("logfile") == null ? "" : (String)request.getParameter("logfile");
    try {
        if (logFile.length() > 0)
            vet = com.zte.log.WebLog.loadWebLog(logFile);
    }
    catch (Exception e) {
System.out.println(e);
    }
%>
<html>
<head>
<title>系统日志查询</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   function pageLoad() {
      var fm = document.inputForm;
      if (! checkDate(fm.logfile.value)) {
         alert('日志文件日期错误！');
         fm.logfile.focus();
         return;
      }
      else
         fm.submit();
   }

   function selectType (infoType) {
      var fm = document.inputForm;
      fm.r1.checked = false;
      fm.r2.checked = false;
      fm.r3.checked = false;
      fm.r4.checked = false;
      fm.infotype.value = infoType;
      if (infoType == '')
         fm.r1.checked = true;
      else if (infoType == 'E')
         fm.r2.checked = true;
      else if (infoType == 'W')
         fm.r3.checked = true;
      else if (infoType == 'I')
         fm.r4.checked = true;
   }

   //判断日期值是否正确
   function checkDate (str) {
      if (str.length == 0)
         return true;
      if (str == null || str == '' || str.length != 8)
         return false;
      year = str.substring(0,4);
      month = str.substring(4,6) - 1;
      day = str.substring(6,8);
      if (isNaN(year) || isNaN(month) || isNaN(day))
         return false;
      var tmpDate = new Date(year,month,day);
      if (tmpDate.getFullYear() != year || tmpDate.getMonth() != month)
         return false;
      return true;
   }
</script>
</head>
<body class="body-style1">
<form name="inputForm" method="post" action="sysLog.jsp">
<input type="hidden" name="infotype" value="<%= infoType %>">
<table border="1" bordercolorlight="#000000" bordercolordark="#ffffff" cellspacing="1" cellpadding="0" width="640" align="center" class="table-style2">
  <tr>
    <td colspan="3" align="center" class="text-title">系统日志查询</td>
  </tr>
  <tr valign="middle">
    <td colspan="3">请输入要查询的日志文件日期（YYYYMMDD）：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="text" name="logfile" value="<%= logFile %>" maxlength="8" class="input-style1">
      <img border="0" src="search.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:pageLoad()">
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <table border="0" bordercolorlight="#000000" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" width="640" align="center" class="table-style2">
        <tr>
          <td width="40%">请选择监控信息类别：</td>
          <td width="15%"><input type="radio" name="r1" onclick="javascript:selectType('')">所有信息</td>
          <td width="15%"><input type="radio" name="r2" onclick="javascript:selectType('E')">错误信息</td>
          <td width="15%"><input type="radio" name="r3" onclick="javascript:selectType('W')">警告信息</td>
          <td width="15%"><input type="radio" name="r4" onclick="javascript:selectType('I')">一般信息</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="16">&nbsp;</td>
    <td align="center" width="120">时间</td>
    <td align="center" width="504">信息内容</td>
  </tr>
<%
  if(vet!=null){ 
    for (int i = vet.size() - 1; i >= 0; i--)
        out.println(display((String)vet.get(i), infoType));
  }      
%>
</table>
<script language="javascript">
   selectType('<%= infoType %>');
</script>
</body>
</html>
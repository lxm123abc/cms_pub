package com.zte.cms.service.service;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.service.model.Service;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceDS
{
    /**
     * 新增Service对象
     * 
     * @param service Service对象
     * @throws DomainServiceException ds异常
     */
    public void insertService(Service service) throws DomainServiceException;

    /**
     * 更新Service对象
     * 
     * @param service Service对象
     * @throws DomainServiceException ds异常
     */
    public void updateService(Service service) throws DomainServiceException;

    /**
     * 批量更新Service对象
     * 
     * @param service Service对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceList(List<Service> serviceList) throws DomainServiceException;

    /**
     * 删除Service对象
     * 
     * @param service Service对象
     * @throws DomainServiceException ds异常
     */
    public void removeService(Service service) throws DomainServiceException;

    /**
     * 批量删除Service对象
     * 
     * @param service Service对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceList(List<Service> serviceList) throws DomainServiceException;

    /**
     * 查询Service对象
     * 
     * @param service Service对象
     * @return Service对象
     * @throws DomainServiceException ds异常
     */
    public Service getService(Service service) throws DomainServiceException;
    
    public Service getServiceById(Service service) throws DomainServiceException;


    /**
     * 根据条件查询Service对象
     * 
     * @param service Service对象
     * @return 满足条件的Service对象集
     * @throws DomainServiceException ds异常
     */
    public List<Service> getServiceByCond(Service service) throws DomainServiceException;

    /**
     * 根据条件分页查询Service对象
     * 
     * @param service Service对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Service对象
     * 
     * @param service Service对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 查询用来发布的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 取消发布
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    /**
     * 根据objectindex和targetindex查询服务是否有内容处于发布中状态
     * @param cntTargetSync
     * @return
     * @throws DomainServiceException
     */
    public int countCntContentPublish(CntTargetSync cntTargetSync)throws DomainServiceException;
}

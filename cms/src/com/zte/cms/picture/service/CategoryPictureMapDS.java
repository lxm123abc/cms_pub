package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.picture.dao.ICategoryPictureMapDAO;
import com.zte.cms.picture.service.ICategoryPictureMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CategoryPictureMapDS extends DynamicObjectBaseDS implements ICategoryPictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICategoryPictureMapDAO dao = null;

    public void setDao(ICategoryPictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException
    {
        log.debug("insert categoryPictureMap starting...");
        try
        {
            if (null == categoryPictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_category_picture_map_index");
                categoryPictureMap.setMapindex(mapIndex);
                categoryPictureMap.setMappingid(String.format("%02d", ObjectType.CATEGORYPICTUREMAP_TYPE)+ String.format("%030d", mapIndex));
            }
            
            dao.insertCategoryPictureMap(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert categoryPictureMap end");
    }

    public void updateCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException
    {
        log.debug("update categoryPictureMap by pk starting...");
        try
        {
            dao.updateCategoryPictureMap(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categoryPictureMap by pk end");
    }

    public void updateCategoryPictureMapList(List<CategoryPictureMap> categoryPictureMapList)
            throws DomainServiceException
    {
        log.debug("update categoryPictureMapList by pk starting...");
        if (null == categoryPictureMapList || categoryPictureMapList.size() == 0)
        {
            log.debug("there is no datas in categoryPictureMapList");
            return;
        }
        try
        {
            for (CategoryPictureMap categoryPictureMap : categoryPictureMapList)
            {
                dao.updateCategoryPictureMap(categoryPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categoryPictureMapList by pk end");
    }

    public void removeCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException
    {
        log.debug("remove categoryPictureMap by pk starting...");
        try
        {
            dao.deleteCategoryPictureMap(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categoryPictureMap by pk end");
    }

    public void removeCategoryPictureMapList(List<CategoryPictureMap> categoryPictureMapList)
            throws DomainServiceException
    {
        log.debug("remove categoryPictureMapList by pk starting...");
        if (null == categoryPictureMapList || categoryPictureMapList.size() == 0)
        {
            log.debug("there is no datas in categoryPictureMapList");
            return;
        }
        try
        {
            for (CategoryPictureMap categoryPictureMap : categoryPictureMapList)
            {
                dao.deleteCategoryPictureMap(categoryPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categoryPictureMapList by pk end");
    }

    public CategoryPictureMap getCategoryPictureMap(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap by pk starting...");
        CategoryPictureMap rsObj = null;
        try
        {
            rsObj = dao.getCategoryPictureMap(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryPictureMapList by pk end");
        return rsObj;
    }

    public List<CategoryPictureMap> getCategoryPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap by condition starting...");
        List<CategoryPictureMap> rsList = null;
        try
        {
            rsList = dao.getCategoryPictureMapByCond(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryPictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categoryPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryPictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get categoryPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categoryPictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryPictureMap page info by condition end");
        return tableInfo;
    }
/**********************************海报发布管理******************************/    
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerycatPicMap(categoryPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryPictureMap page info by condition end");
        return tableInfo;
    }    
    
 
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerypcatPicTargetMap(categoryPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryPictureMap page info by condition end");
        return tableInfo;
    }    
   
    public List<CategoryPictureMap> getCategorynormalPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException
    {
        log.debug("get categoryPictureMap by condition starting...");
        List<CategoryPictureMap> rsList = null;
        try
        {
            rsList = dao.getCategorynormalPictureMapByCond(categoryPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryPictureMap by condition end");
        return rsList;
    }    
    
    
    /**********************************海报发布管理******************************/     
}

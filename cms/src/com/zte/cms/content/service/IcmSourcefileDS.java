package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.dao.IIcmSourcefileDAO;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmSourcefileDS implements IIcmSourcefileDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmSourcefileDAO dao = null;
    private IPrimaryKeyGenerator primaryKeyGenerator = null;

    public void setDao(IIcmSourcefileDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("insert icmSourcefile starting...");
        try
        {
            Long fileindex = (Long) primaryKeyGenerator.getPrimarykey("cms_file");
            icmSourcefile.setFileindex(fileindex);
            dao.insertIcmSourcefile(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmSourcefile end");
    }

    public void updateIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("update icmSourcefile by pk starting...");
        try
        {
            dao.updateIcmSourcefile(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmSourcefile by pk end");
    }

    public void updateIcmSourcefileList(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException
    {
        log.debug("update icmSourcefileList by pk starting...");
        if (null == icmSourcefileList || icmSourcefileList.size() == 0)
        {
            log.debug("there is no datas in icmSourcefileList");
            return;
        }
        try
        {
            for (IcmSourcefile icmSourcefile : icmSourcefileList)
            {
                dao.updateIcmSourcefile(icmSourcefile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmSourcefileList by pk end");
    }

    public void updateIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("update icmSourcefile by condition starting...");
        try
        {
            dao.updateIcmSourcefileByCond(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmSourcefile by condition end");
    }

    public void updateIcmSourcefileListByCond(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException
    {
        log.debug("update icmSourcefileList by condition starting...");
        if (null == icmSourcefileList || icmSourcefileList.size() == 0)
        {
            log.debug("there is no datas in icmSourcefileList");
            return;
        }
        try
        {
            for (IcmSourcefile icmSourcefile : icmSourcefileList)
            {
                dao.updateIcmSourcefileByCond(icmSourcefile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmSourcefileList by condition end");
    }

    public void removeIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("remove icmSourcefile by pk starting...");
        try
        {
            dao.deleteIcmSourcefile(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmSourcefile by pk end");
    }

    public void removeIcmSourcefileList(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException
    {
        log.debug("remove icmSourcefileList by pk starting...");
        if (null == icmSourcefileList || icmSourcefileList.size() == 0)
        {
            log.debug("there is no datas in icmSourcefileList");
            return;
        }
        try
        {
            for (IcmSourcefile icmSourcefile : icmSourcefileList)
            {
                dao.deleteIcmSourcefile(icmSourcefile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmSourcefileList by pk end");
    }

    public void removeIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("remove icmSourcefile by condition starting...");
        try
        {
            dao.deleteIcmSourcefileByCond(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmSourcefile by condition end");
    }

    public void removeIcmSourcefileListByCond(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException
    {
        log.debug("remove icmSourcefileList by condition starting...");
        if (null == icmSourcefileList || icmSourcefileList.size() == 0)
        {
            log.debug("there is no datas in icmSourcefileList");
            return;
        }
        try
        {
            for (IcmSourcefile icmSourcefile : icmSourcefileList)
            {
                dao.deleteIcmSourcefileByCond(icmSourcefile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmSourcefileList by condition end");
    }

    public IcmSourcefile getIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("get icmSourcefile by pk starting...");
        IcmSourcefile rsObj = null;
        try
        {
            rsObj = dao.getIcmSourcefile(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmSourcefileList by pk end");
        return rsObj;
    }

    public List<IcmSourcefile> getIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException
    {
        log.debug("get icmSourcefile by condition starting...");
        List<IcmSourcefile> rsList = null;
        try
        {
            rsList = dao.getIcmSourcefileByCond(icmSourcefile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmSourcefile by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get icmSourcefile page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmSourcefile, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmSourcefile>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmSourcefile page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get icmSourcefile page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmSourcefile, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmSourcefile>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmSourcefile page info by condition end");
        return tableInfo;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }
}

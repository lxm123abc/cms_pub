package com.zte.cms.ctgpgmap.service;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategoryProgramMapDS
{
    /**
     * 新增CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException;

    /**
     * 更新CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException;

    /**
     * 批量更新CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryProgramMapList(List<CategoryProgramMap> categoryProgramMapList)
            throws DomainServiceException;

    /**
     * 删除CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException;

    /**
     * 批量删除CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryProgramMapList(List<CategoryProgramMap> categoryProgramMapList)
            throws DomainServiceException;

    /**
     * 查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @return CategoryProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public CategoryProgramMap getCategoryProgramMap(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException;

    /**
     * 根据条件查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @return 满足条件的CategoryProgramMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategoryProgramMap> getCategoryProgramMapByCond(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    /****** publish management *************/
    
    
	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
    public TableDataInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws Exception;

	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
   public TableDataInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws Exception;
   
    /**
     * 根据条件查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @return 满足条件的CategoryProgramMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategoryProgramMap> getnormaltargetSynListByCond(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException;
    
    /****** publish management end ********/    
}

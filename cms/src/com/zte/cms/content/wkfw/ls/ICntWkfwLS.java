package com.zte.cms.content.wkfw.ls;

import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * <p>
 * 文件名称: ICntorderWkfwLS.java
 * </p>
 * <p>
 * 文件描述: 内容审核查询接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容订单审核查询接口
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public interface ICntWkfwLS
{

    /**
     * <p>
     * 根据内容审核查询条件分页查询待审核的内容订单信息
     * </p>
     * <p>
     * 精确查询字段：contentname、pbo_type；其余为精确查询字段；时间段查询字段：process_start_time
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单审核查询条件类
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 返回查询结果，CntorderWkfwTaskResult内容订单审核查询结果信息列表
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * <p>
     * 根据内容订单工作流相关查询条件分页查询待审核的内容订单信息，支持排序
     * </p>
     * <p>
     * 精确查询字段：contentname、pbo_type；其余为精确查询字段；时间段查询字段：process_start_time
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单审核查询条件类
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 表格中需要排序的字段实体
     * @return 返回查询结果，CntorderWkfwTaskResult，内容订单审核查询结果信息列表
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * <p>
     * 根据内容订单工作流相关查询条件分页查询查询待审核的内容订单数量
     * </p>
     * <p>
     * 精确查询字段：contentname、pbo_type；其余为精确查询字段；时间段查询字段：process_start_time
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单审核查询条件类
     * @return 返回查询结果，待审核的内容订单数量
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntWkfwCondition) throws DomainServiceException;

    /**
     * 查看审核失败信息
     * 
     * @param cntWkfwhis CntWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwis, int start, int pageSize) throws DomainServiceException;

    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis) throws DomainServiceException;

    /**
     * 内容工单历史查询方法
     * 
     * @param cmsprogram
     * @param start 当前页数
     * @param pageSize 每页查询数目
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getMyHaveHandleWorkflowUcdn(CntWkfwhis cntWkfwis, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 内容流程跟踪查询
     * 
     * @param cmsprogram
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getWorkflowTrack(CntWkfwhis cntWkfwis, int start, int pageSize) throws DomainServiceException;

    /**
     * 审核历史信息查看
     * 
     * @param historyindex
     * @return
     * @throws DomainServiceException
     */
    public CntWkfwhis getCmsProgramWkfwhis(long historyindex) throws DomainServiceException;
}

package com.zte.cms.settlementDataRpt.error;

/**
 * ProjectName:
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 16:27 2019/5/6
 * @Description:    包装器业务异常实现
 */
public class BusinessException extends Exception implements CommonError {

    private CommonError commonError;


    //直接接受EmBusinessError的传参
    public BusinessException(CommonError commonError){
        super();
        this.commonError = commonError;
    }

    //接受自定义方式
    public BusinessException(CommonError commonError,String errorMsg){
        super();
        this.commonError = commonError;
        this.setErrorMsg(errorMsg);
    }

    @Override
    public int getErrorCode() {
        return this.commonError.getErrorCode();
    }

    @Override
    public String getErrorMsg() {
        return this.commonError.getErrorMsg();
    }

    @Override
    public CommonError setErrorMsg(String errorMsg) {
        this.commonError.setErrorMsg(errorMsg);
        return this;
    }
}

package com.zte.cms.content.targetSyn.dao;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICntTargetSyncDAO
{
     /**
	  * 新增CntTargetSync对象 
	  * 
	  * @param cntTargetSync CntTargetSync对象
	  * @throws DAOException dao异常
	  */	
     public void insertCntTargetSync( CntTargetSync cntTargetSync )throws DAOException;
     
     /**
	  * 根据主键更新CntTargetSync对象
	  * 
	  * @param cntTargetSync CntTargetSync对象
	  * @throws DAOException dao异常
	  */
     public void updateCntTargetSync( CntTargetSync cntTargetSync )throws DAOException;


     /**
	  * 根据主键删除CntTargetSync对象
	  *
	  * @param cntTargetSync CntTargetSync对象
	  * @throws DAOException dao异常
	  */
     public void deleteCntTargetSync( CntTargetSync cntTargetSync )throws DAOException;
     
     
     /**
	  * 根据主键查询CntTargetSync对象
	  *
	  * @param cntTargetSync CntTargetSync对象
	  * @return 满足条件的CntTargetSync对象
	  * @throws DAOException dao异常
	  */
     public CntTargetSync getCntTargetSync( CntTargetSync cntTargetSync )throws DAOException;
     
     /**
	  * 根据条件查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象
	  * @return 满足条件的CntTargetSync对象集
	  * @throws DAOException dao异常
	  */
     public List<CntTargetSync> getCntTargetSyncByCond( CntTargetSync cntTargetSync )throws DAOException;

     /**
	  * 根据条件分页查询CntTargetSync对象，作为查询条件的参数
	  *
	  * @param cntTargetSync CntTargetSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetSync对象，作为查询条件的参数
	  *
	  * @param cntTargetSync CntTargetSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     /**
      * 根据objecttype、objectindex和targetindex查询对应的关联信息数目
      * @param cntTargetSync
      * @return
      * @throws DAOException
      */
     public int countCntTargetSync4Object(CntTargetSync cntTargetSync)throws DAOException;
        
     /**
      * 根据objecttype和objectindex删除CntTargetSync对象
      *
      * @param cntTargetSync CntTargetSync对象
      * @throws DAOException dao异常
      */
     public void deleteCntTargetSyncByObjindex( CntTargetSync cntTargetSync )throws DAOException;
     /**
      * 根据objectindex和elementid查询已知服务索引和VOD内容id的可以绑定的VOD内容CntTargetSync对象
      *
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象
      * @throws DAOException dao异常
      */
     public List<CntTargetSync>  getCommonTarget4ServiceAndProgram( CntTargetSync cntTargetSync )throws DAOException;
     /**
      * 根据objectindex和elementid查询已知服务索引和连续剧内容id的可以绑定的VOD内容CntTargetSync对象
      *
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象
      * @throws DAOException dao异常
      */
     public List<CntTargetSync>  getCommonTarget4ServiceAndSeries( CntTargetSync cntTargetSync )throws DAOException;
     /**
      * 根据objectindex和elementid查询已知服务索引和直播频道内容id的可以绑定的VOD内容CntTargetSync对象
      *
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象
      * @throws DAOException dao异常
      */
     public List<CntTargetSync>  getCommonTarget4ServiceAndChannel( CntTargetSync cntTargetSync )throws DAOException;

    /**
	  * 根据条件查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象
	  * @return 满足条件的CntTargetSync对象集
	  * @throws DAOException dao异常
	  */
     public List<CntTargetSync> getcntSynnormaltargetListBycond( CntTargetSync cntTargetSync )throws DAOException;
}
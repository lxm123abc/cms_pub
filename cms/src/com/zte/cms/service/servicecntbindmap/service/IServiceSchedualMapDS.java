package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceSchedualMapDS
{
    /**
     * 新增ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException;

    /**
     * 更新ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException;

    /**
     * 批量更新ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceSchedualMapList(List<ServiceSchedualMap> serviceSchedualMapList)
            throws DomainServiceException;

    /**
     * 删除ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DomainServiceException;

    /**
     * 批量删除ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceSchedualMapList(List<ServiceSchedualMap> serviceSchedualMapList)
            throws DomainServiceException;

    /**
     * 查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @return ServiceSchedualMap对象
     * @throws DomainServiceException ds异常
     */
    public ServiceSchedualMap getServiceSchedualMap(ServiceSchedualMap serviceSchedualMap)
            throws DomainServiceException;

    /**
     * 根据条件查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @return 满足条件的ServiceSchedualMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceSchedualMap> getServiceSchedualMapByCond(ServiceSchedualMap serviceSchedualMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

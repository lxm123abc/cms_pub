
  package com.zte.cms.content.objectSynRecord.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;


public class ObjectSyncRecord extends DynamicBaseObject  implements Cloneable
{
	private java.lang.Long syncindex;
	private java.lang.Long taskindex;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String objectcode;
	private java.lang.Integer objecttype;
	private java.lang.String elementid;
	private java.lang.String elementcode;
	private java.lang.String parentid;
	private java.lang.String parentcode;
	private java.lang.Integer actiontype;
	private java.lang.String starttime;
	private java.lang.String endtime;
	private java.lang.Integer resultcode;
	private java.lang.String resultdesc;
	private java.lang.Long destindex;
	private java.lang.Long targetindex; 
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    private java.lang.String endtime1;
    private java.lang.String endtime2;
    
    public Object clone(){
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
            return null;
        }
        
         
     }
	
	public java.lang.String getStarttime1()
    {
        return starttime1;
    }

    public void setStarttime1(java.lang.String starttime1)
    {
        this.starttime1 = starttime1;
    }

    public java.lang.String getStarttime2()
    {
        return starttime2;
    }

    public void setStarttime2(java.lang.String starttime2)
    {
        this.starttime2 = starttime2;
    }

    public java.lang.String getEndtime1()
    {
        return endtime1;
    }

    public void setEndtime1(java.lang.String endtime1)
    {
        this.endtime1 = endtime1;
    }

    public java.lang.String getEndtime2()
    {
        return endtime2;
    }

    public void setEndtime2(java.lang.String endtime2)
    {
        this.endtime2 = endtime2;
    }

    public java.lang.Long getTargetindex()
    {
        return targetindex;
    }

    public void setTargetindex(java.lang.Long targetindex)
    {
        this.targetindex = targetindex;
    }

    public java.lang.Integer getTargettype()
    {
        return targettype;
    }

    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }

    private java.lang.Integer targettype;
	private java.lang.String targetname;
    public java.lang.Long getSyncindex()
    {
        return syncindex;
    } 
         
    public void setSyncindex(java.lang.Long syncindex) 
    {
        this.syncindex = syncindex;
    }
    
    public java.lang.Long getTaskindex()
    {
        return taskindex;
    } 
         
    public void setTaskindex(java.lang.Long taskindex) 
    {
        this.taskindex = taskindex;
    }
    
    public java.lang.Long getObjectindex()
    {
        return objectindex;
    } 
         
    public void setObjectindex(java.lang.Long objectindex) 
    {
        this.objectindex = objectindex;
    }
    
    public java.lang.String getObjectid()
    {
        return objectid;
    } 
         
    public void setObjectid(java.lang.String objectid) 
    {
        this.objectid = objectid;
    }
    
    public java.lang.String getObjectcode()
    {
        return objectcode;
    } 
         
    public void setObjectcode(java.lang.String objectcode) 
    {
        this.objectcode = objectcode;
    }
    
    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    } 
         
    public void setObjecttype(java.lang.Integer objecttype) 
    {
        this.objecttype = objecttype;
    }
    
    public java.lang.String getElementid()
    {
        return elementid;
    } 
         
    public void setElementid(java.lang.String elementid) 
    {
        this.elementid = elementid;
    }
    
    public java.lang.String getElementcode()
    {
        return elementcode;
    } 
         
    public void setElementcode(java.lang.String elementcode) 
    {
        this.elementcode = elementcode;
    }
    
    public java.lang.String getParentid()
    {
        return parentid;
    } 
         
    public void setParentid(java.lang.String parentid) 
    {
        this.parentid = parentid;
    }
    
    public java.lang.String getParentcode()
    {
        return parentcode;
    } 
         
    public void setParentcode(java.lang.String parentcode) 
    {
        this.parentcode = parentcode;
    }
    
    public java.lang.Integer getActiontype()
    {
        return actiontype;
    } 
         
    public void setActiontype(java.lang.Integer actiontype) 
    {
        this.actiontype = actiontype;
    }
    
    public java.lang.String getStarttime()
    {
        return starttime;
    } 
         
    public void setStarttime(java.lang.String starttime) 
    {
        this.starttime = starttime;
    }
    
    public java.lang.String getEndtime()
    {
        return endtime;
    } 
         
    public void setEndtime(java.lang.String endtime) 
    {
        this.endtime = endtime;
    }
    
    public java.lang.Integer getResultcode()
    {
        return resultcode;
    } 
         
    public void setResultcode(java.lang.Integer resultcode) 
    {
        this.resultcode = resultcode;
    }
    
    public java.lang.String getResultdesc()
    {
        return resultdesc;
    } 
         
    public void setResultdesc(java.lang.String resultdesc) 
    {
        this.resultdesc = resultdesc;
    }
    
    public java.lang.Long getDestindex()
    {
        return destindex;
    } 
         
    public void setDestindex(java.lang.Long destindex) 
    {
        this.destindex = destindex;
    }
    
    public void initRelation()
    {       
        this.addRelation("syncindex", "SYNCINDEX");
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("objectindex", "OBJECTINDEX");
        this.addRelation("objectid", "OBJECTID");
        this.addRelation("objectcode", "OBJECTCODE");       
        this.addRelation("objecttype", "OBJECTTYPE");        
        this.addRelation("elementid", "ELEMENTID");
        this.addRelation("elementcode", "ELEMENTCODE");       
        this.addRelation("parentid", "PARENTID");
        this.addRelation("parentcode", "PARENTCODE");
        this.addRelation("actiontype", "ACTIONTYPE");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("resultcode", "RESULTCODE");
        this.addRelation("resultdesc", "RESULTDESC");
        this.addRelation("destindex", "DESTINDEX");       
        this.addRelation("targetindex", "TARGETINDEX");
        this.addRelation("targettype", "TARGETTYPE");
        this.addRelation("targetname", "TARGETNAME");
        this.addRelation("starttime1", "STARTTIME1");
        this.addRelation("endtime1", "ENDTIME1");
        this.addRelation("starttime2", "STARTTIME2");
        this.addRelation("endtime2", "ENDTIME2");
    }
 
}

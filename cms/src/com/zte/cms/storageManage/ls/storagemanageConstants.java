package com.zte.cms.storageManage.ls;

import com.zte.umap.common.ResourceMgt;

public class storagemanageConstants
{

    public static final String CMS_STORAGEMANAGE_RES_FILE_NAME = "cms_storagemanage_resource";// 资源文件名
    public static final String CMS_STORAGEMANAGE_MSG_DEL_NO_EXIT = "cms.storagemanage.msg.del.no.exit";
    public static final String CMS_STORAGEMANAGE_MSG_VIEW_NO_EXIT = "cms.storagemanage.msg.view.no.exits";
    public static final String CMS_STORAGEMANAGE_MSG_UPDATE_NO_EXIT = "cms.storagemanage.msg.spacetoupdate.no.exits";
    public static final String CMS_STORAGEMANAGE_MSG_UPDATE_SAMENAME_EXIT = "cms.storagemanage.msg.spacetoupdate.samename.exit";
    public static final String CMS_STORAGEMANAGE_MSG_UPDATE_SPACE_BIND_ALREADY = "cms.storagemanage.msg.spacetoupdate.space.bind.alredy";
    public static String setSourceFile()
    {
        ResourceMgt.addDefaultResourceBundle(CMS_STORAGEMANAGE_RES_FILE_NAME);
        return null;
    }
}

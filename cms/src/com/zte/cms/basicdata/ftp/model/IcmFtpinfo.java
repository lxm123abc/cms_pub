package com.zte.cms.basicdata.ftp.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmFtpinfo extends DynamicBaseObject
{
    private java.lang.Long infoindex;

    private java.lang.Long spaceindex;

    private java.lang.String cpid;
    private java.lang.Integer infotype;
    private java.lang.String serverip;
    private java.lang.String ftpaccount;
    private java.lang.String ftppassword;
    private java.lang.String ftpport;
    private java.lang.String ftppath;
    private java.lang.Long spacesize;

    private java.lang.String currentcapacity;
    private java.lang.Integer monitorthreshold;
    private java.lang.Integer status;

    private java.lang.String lastupdatedate;
    private java.lang.String servicekey;
    private java.lang.String description;

    public java.lang.Long getInfoindex()
    {
        return infoindex;
    }

    public void setInfoindex(java.lang.Long infoindex)
    {
        this.infoindex = infoindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Integer getInfotype()
    {
        return infotype;
    }

    public void setInfotype(java.lang.Integer infotype)
    {
        this.infotype = infotype;
    }

    public java.lang.String getServerip()
    {
        return serverip;
    }

    public void setServerip(java.lang.String serverip)
    {
        this.serverip = serverip;
    }

    public java.lang.String getFtpaccount()
    {
        return ftpaccount;
    }

    public void setFtpaccount(java.lang.String ftpaccount)
    {
        this.ftpaccount = ftpaccount;
    }

    public java.lang.String getFtppassword()
    {
        return ftppassword;
    }

    public void setFtppassword(java.lang.String ftppassword)
    {
        this.ftppassword = ftppassword;
    }

    public java.lang.String getFtpport()
    {
        return ftpport;
    }

    public void setFtpport(java.lang.String ftpport)
    {
        this.ftpport = ftpport;
    }

    public java.lang.String getFtppath()
    {
        return ftppath;
    }

    public void setFtppath(java.lang.String ftppath)
    {
        this.ftppath = ftppath;
    }

    public java.lang.Long getSpacesize()
    {
        return spacesize;
    }

    public void setSpacesize(java.lang.Long spacesize)
    {
        this.spacesize = spacesize;
    }

    public java.lang.String getLastupdatedate()
    {
        return lastupdatedate;
    }

    public void setLastupdatedate(java.lang.String lastupdatedate)
    {
        this.lastupdatedate = lastupdatedate;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public void initRelation()
    {
        this.addRelation("infoindex", "INFOINDEX");
        this.addRelation("spaceindex", "SPACEINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("infotype", "INFOTYPE");
        this.addRelation("serverip", "SERVERIP");
        this.addRelation("ftpaccount", "FTPACCOUNT");
        this.addRelation("ftppassword", "FTPPASSWORD");
        this.addRelation("ftpport", "FTPPORT");
        this.addRelation("ftppath", "FTPPATH");
        this.addRelation("spacesize", "SPACESIZE");
        this.addRelation("currentcapacity", "CURRENTCAPACITY");
        this.addRelation("monitorthreshold", "MONITORTHRESHOLD");
        this.addRelation("status", "STATUS");
        this.addRelation("lastupdatedate", "LASTUPDATEDATE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("description", "DESCRIPTION");
    }

    public java.lang.Long getSpaceindex()
    {
        return spaceindex;
    }

    public void setSpaceindex(java.lang.Long spaceindex)
    {
        this.spaceindex = spaceindex;
    }

    public java.lang.String getCurrentcapacity()
    {
        return currentcapacity;
    }

    public void setCurrentcapacity(java.lang.String currentcapacity)
    {
        this.currentcapacity = currentcapacity;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getMonitorthreshold()
    {
        return monitorthreshold;
    }

    public void setMonitorthreshold(java.lang.Integer monitorthreshold)
    {
        this.monitorthreshold = monitorthreshold;
    }
}

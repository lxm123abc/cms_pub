package com.zte.cms.programcrmmap.service;

import java.util.List;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramCrmMapDS
{
    /**
     * 新增ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public String insertProgramCrmMapRtn(ProgramCrmMap programCrmMap) throws DomainServiceException;
    
    /**
     * 新增ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 批量更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateProgramCrmMapList(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException;

    /**
     * 根据条件更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 根据条件批量更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateProgramCrmMapListByCond(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException;

    /**
     * 删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 批量删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeProgramCrmMapList(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException;

    /**
     * 根据条件删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 根据条件批量删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeProgramCrmMapListByCond(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException;

    /**
     * 查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @return ProgramCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public ProgramCrmMap getProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 根据条件查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @return 满足条件的ProgramCrmMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ProgramCrmMap> getProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
/*************************publish mangement begin*******************/    
    
    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException;    
        
    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException;      
    
    /**
     * 根据条件查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @return 满足条件的ProgramCrmMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ProgramCrmMap> getProgramnormalCrmMapSynByCond(ProgramCrmMap programCrmMap) throws DomainServiceException;    
    
/*************************publish mangement end*******************/    
}
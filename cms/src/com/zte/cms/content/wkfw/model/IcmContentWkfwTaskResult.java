package com.zte.cms.content.wkfw.model;

public class IcmContentWkfwTaskResult
{
    /**
     * 进程ID
     */
    private java.lang.Long processId;
    /**
     * 进程名称
     */
    private java.lang.String processName;
    /**
     * 进程创建者
     */
    private java.lang.String creator;
    /**
     * 进程开始时间
     */
    private java.util.Date processStarttime;
    /**
     * 工作流id
     */
    private java.lang.Long pboId;
    /**
     * 工作流名称
     */
    private java.lang.String pboName;
    /**
     * 工作流类型
     */
    private java.lang.String pboType;
    /**
     * 工作流优先级
     */
    private java.lang.String priority;
    /**
     * 任务id
     */
    private java.lang.Long taskId;
    /**
     * 任务开始时间
     */
    private java.util.Date taskStarttime;
    /**
     * 参与者ID
     */
    private java.lang.String executorId;
    /**
     * 节点状态
     */
    private java.lang.String nodeStatus;
    /**
     * 节点状态编码
     */
    private java.lang.String nodeStatusCode;
    /**
     * 任务状态
     */
    private java.lang.String taskStatus;
    /**
     * 进程状态
     */
    private java.lang.String processStatus;
    /**
     * 节点名称
     */
    private java.lang.String nodeName;
    /**
     * 接受状态
     */
    private java.lang.String acceptStatus;
    /**
     * 内容index
     */
    private java.lang.Long contentindex;
    /**
     * 内容id
     */
    private java.lang.String contentid;
    /**
     * 内容名称
     */
    private java.lang.String namecn;
    /**
     * CP
     */
    private java.lang.Long cpindex;
    /**
     * cpid
     */
    private java.lang.String cpid;
    /**
     * cp名称
     */
    private java.lang.String cpcnshortname;

    public java.lang.Long getProcessId()
    {
        return processId;
    }

    public void setProcessId(java.lang.Long processId)
    {
        this.processId = processId;
    }

    public java.lang.String getProcessName()
    {
        return processName;
    }

    public void setProcessName(java.lang.String processName)
    {
        this.processName = processName;
    }

    public java.lang.String getCreator()
    {
        return creator;
    }

    public void setCreator(java.lang.String creator)
    {
        this.creator = creator;
    }

    public java.lang.Long getPboId()
    {
        return pboId;
    }

    public void setPboId(java.lang.Long pboId)
    {
        this.pboId = pboId;
    }

    public java.lang.String getPboName()
    {
        return pboName;
    }

    public void setPboName(java.lang.String pboName)
    {
        this.pboName = pboName;
    }

    public java.lang.String getPboType()
    {
        return pboType;
    }

    public void setPboType(java.lang.String pboType)
    {
        this.pboType = pboType;
    }

    public java.lang.String getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.String priority)
    {
        this.priority = priority;
    }

    public java.lang.Long getTaskId()
    {
        return taskId;
    }

    public void setTaskId(java.lang.Long taskId)
    {
        this.taskId = taskId;
    }

    public java.lang.String getExecutorId()
    {
        return executorId;
    }

    public void setExecutorId(java.lang.String executorId)
    {
        this.executorId = executorId;
    }

    public java.lang.String getNodeStatus()
    {
        return nodeStatus;
    }

    public void setNodeStatus(java.lang.String nodeStatus)
    {
        this.nodeStatus = nodeStatus;
    }

    public java.lang.String getNodeStatusCode()
    {
        return nodeStatusCode;
    }

    public void setNodeStatusCode(java.lang.String nodeStatusCode)
    {
        this.nodeStatusCode = nodeStatusCode;
    }

    public java.lang.String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(java.lang.String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public java.lang.String getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(java.lang.String processStatus)
    {
        this.processStatus = processStatus;
    }

    public java.lang.String getNodeName()
    {
        return nodeName;
    }

    public void setNodeName(java.lang.String nodeName)
    {
        this.nodeName = nodeName;
    }

    public java.lang.String getAcceptStatus()
    {
        return acceptStatus;
    }

    public void setAcceptStatus(java.lang.String acceptStatus)
    {
        this.acceptStatus = acceptStatus;
    }

    public java.lang.Long getContentindex()
    {
        return contentindex;
    }

    public void setContentindex(java.lang.Long contentindex)
    {
        this.contentindex = contentindex;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.util.Date getProcessStarttime()
    {
        return processStarttime;
    }

    public void setProcessStarttime(java.util.Date processStarttime)
    {
        this.processStarttime = processStarttime;
    }

    public java.util.Date getTaskStarttime()
    {
        return taskStarttime;
    }

    public void setTaskStarttime(java.util.Date taskStarttime)
    {
        this.taskStarttime = taskStarttime;
    }

}

package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsCpbindDAO
{
    /**
     * 新增CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DAOException dao异常
     */
    public void insertCmsCpbind(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据主键更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DAOException dao异常
     */
    public void updateCmsCpbind(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据条件更新CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据主键删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @throws DAOException dao异常
     */
    public void deleteCmsCpbind(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据条件删除CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据主键查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @return 满足条件的CmsCpbind对象
     * @throws DAOException dao异常
     */
    public CmsCpbind getCmsCpbind(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据条件查询CmsCpbind对象
     * 
     * @param cmsCpbind CmsCpbind对象
     * @return 满足条件的CmsCpbind对象集
     * @throws DAOException dao异常
     */
    public List<CmsCpbind> getCmsCpbindByCond(CmsCpbind cmsCpbind) throws DAOException;

    /**
     * 根据条件分页查询CmsCpbind对象，作为查询条件的参数
     * 
     * @param cmsCpbind CmsCpbind对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsCpbind对象，作为查询条件的参数
     * 
     * @param cmsCpbind CmsCpbind对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件删除绑定关系
     * 
     * @param cmsCpbind 绑定关系对象，没有的条件勿set
     * @return 无
     */
    public void removeCpbindBySetCond(CmsCpbind cmsCpbind) throws DAOException;

}
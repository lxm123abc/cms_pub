package com.zte.cms.ftptask.dao;

import java.util.List;

import com.zte.cms.ftptask.dao.ICmsFtptaskDAO;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.model.CmsFtptaskAndHis;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsFtptaskDAO extends DynamicObjectBaseDao implements ICmsFtptaskDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsFtptask(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("insert cmsFtptask starting...");
        super.insert("insertCmsFtptask", cmsFtptask);
        log.debug("insert cmsFtptask end");
    }

    public void insertCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException
    {
        log.debug("insert cmsFtptaskList starting...");
        if (null != cmsFtptaskList)
        {
            super.insertBatch("insertCmsFtptask", cmsFtptaskList);
        }
        log.debug("insert cmsFtptaskList end");
    }

    public void updateCmsFtptask(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("update cmsFtptask by pk starting...");
        super.update("updateCmsFtptask", cmsFtptask);
        log.debug("update cmsFtptask by pk end");
    }

    public void updateCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException
    {
        log.debug("update cmsFtptaskList by pk starting...");
        super.updateBatch("updateCmsFtptask", cmsFtptaskList);
        log.debug("update cmsFtptaskList by pk end");
    }

    public void updateCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("update cmsFtptask by conditions starting...");
        super.update("updateCmsFtptaskByCond", cmsFtptask);
        log.debug("update cmsFtptask by conditions end");
    }

    public void updateCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DAOException
    {
        log.debug("update cmsFtptaskList by conditions starting...");
        super.updateBatch("updateCmsFtptaskByCond", cmsFtptaskList);
        log.debug("update cmsFtptaskList by conditions end");
    }

    public void deleteCmsFtptask(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("delete cmsFtptask by pk starting...");
        super.delete("deleteCmsFtptask", cmsFtptask);
        log.debug("delete cmsFtptask by pk end");
    }

    public void deleteCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException
    {
        log.debug("delete cmsFtptaskList by pk starting...");
        super.deleteBatch("deleteCmsFtptask", cmsFtptaskList);
        log.debug("delete cmsFtptaskList by pk end");
    }

    public void deleteCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("delete cmsFtptask by conditions starting...");
        super.delete("deleteCmsFtptaskByCond", cmsFtptask);
        log.debug("delete cmsFtptask by conditions end");
    }

    public void deleteCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DAOException
    {
        log.debug("delete cmsFtptaskList by conditions starting...");
        super.deleteBatch("deleteCmsFtptaskByCond", cmsFtptaskList);
        log.debug("delete cmsFtptaskList by conditions end");
    }

    public CmsFtptask getCmsFtptask(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("query cmsFtptask starting...");
        CmsFtptask resultObj = (CmsFtptask) super.queryForObject("getCmsFtptask", cmsFtptask);
        log.debug("query cmsFtptask end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("query cmsFtptask by condition starting...");
        List<CmsFtptask> rList = (List<CmsFtptask>) super.queryForList("queryCmsFtptaskListByCond", cmsFtptask);
        log.debug("query cmsFtptask by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask, PageUtilEntity puEntity) throws DAOException
    {
        log.debug("query cmsFtptask by condition starting...");
        List<CmsFtptask> rList = (List<CmsFtptask>) super.queryForList("queryCmsFtptaskListByCond", cmsFtptask,
                puEntity);
        log.debug("query cmsFtptask by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsFtptask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsFtptaskListCntByCond", cmsFtptask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsFtptask> rsList = (List<CmsFtptask>) super.pageQuery("queryCmsFtptaskListByCond", cmsFtptask,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsFtptask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsFtptaskListByCond", "queryCmsFtptaskListCntByCond", cmsFtptask, start,
                pageSize, puEntity);
    }

    public void deleteCmsFtptaskHis(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("delete cmsFtptaskHis by pk starting...");
        super.delete("deleteCmsFtptaskHis", cmsFtptask);
        log.debug("delete cmsFtptaskHis by pk end");
    }

    public CmsFtptask getCmsFtptaskHis(CmsFtptask cmsFtptask) throws DAOException
    {
        log.debug("query getCmsFtptaskHis starting...");
        CmsFtptask resultObj = (CmsFtptask) super.queryForObject("getCmsFtptaskHis", cmsFtptask);
        log.debug("query getCmsFtptaskHis end");
        return resultObj;
    }

    public PageInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskHis, int start, int pageSize)
            throws DAOException
    {
        log.debug("pageCmsFtptaskHisInfoQuery by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsFtptaskAndHisListCntByCond", cmsFtptaskHis)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsFtptaskAndHis> rsList = (List<CmsFtptaskAndHis>) super.pageQuery("queryCmsFtptaskAndHisListByCond",
                    cmsFtptaskHis, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageCmsFtptaskHisInfoQuery by condition end");
        return pageInfo;
    }
}
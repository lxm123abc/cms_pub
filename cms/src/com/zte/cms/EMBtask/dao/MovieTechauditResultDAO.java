package com.zte.cms.EMBtask.dao;

import java.util.List;

import com.zte.cms.EMBtask.dao.IMovieTechauditResultDAO;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class MovieTechauditResultDAO extends DynamicObjectBaseDao implements IMovieTechauditResultDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertMovieTechauditResult(MovieTechauditResult movieTechauditResult) throws DAOException
    {
        log.debug("insert movieTechauditResult starting...");
        super.insert("insertMovieTechauditResult", movieTechauditResult);
        log.debug("insert movieTechauditResult end");
    }

    public void updateMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult) throws DAOException
    {
        log.debug("update movieTechauditResult by conditions starting...");
        super.update("updateMovieTechauditResultByCond", movieTechauditResult);
        log.debug("update movieTechauditResult by conditions end");
    }

    public void deleteMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult) throws DAOException
    {
        log.debug("delete movieTechauditResult by conditions starting...");
        super.delete("deleteMovieTechauditResultByCond", movieTechauditResult);
        log.debug("update movieTechauditResult by conditions end");
    }

    @SuppressWarnings("unchecked")
    public List<MovieTechauditResult> getMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DAOException
    {
        log.debug("query movieTechauditResult by condition starting...");
        List<MovieTechauditResult> rList = (List<MovieTechauditResult>) super.queryForList(
                "queryMovieTechauditResultListByCond", movieTechauditResult);
        log.debug("query movieTechauditResult by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query movieTechauditResult by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryMovieTechauditResultListCntByCond", movieTechauditResult))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<MovieTechauditResult> rsList = (List<MovieTechauditResult>) super.pageQuery(
                    "queryMovieTechauditResultListByCond", movieTechauditResult, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query movieTechauditResult by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryMovieTechauditResultListByCond", "queryMovieTechauditResultListCntByCond",
                movieTechauditResult, start, pageSize, puEntity);
    }

}
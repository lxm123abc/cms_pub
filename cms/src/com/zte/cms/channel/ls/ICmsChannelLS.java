package com.zte.cms.channel.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsChannelLS {

	public String insertChannelContent(CmsChannel channel);

	public TableDataInfo pageInfo(CmsChannel cmsChannel, int start, int pageSize);

	public CmsChannel getChannel(CmsChannel channel);

	public String deleteChannelContent(CmsChannel channel);

	public String deleteChannelWithPhysical(CmsChannel channel);

	public String updateChannelContent(CmsChannel channel);

	public String publishChannelcontent(
			List<CntPlatformSync> cntPlatformSynclist);

	public String canclePublishChannel(List<CntPlatformSync> cntPlatformSynclist);

	

	public String startOffline(CmsChannel channel);

	public String bindBatchChannelTargetsys(
			List<Targetsystem> targetsystemList, List<CmsChannel> cmsChannelList)
			throws DomainServiceException;

	public HashMap publishChannelcontentTar(List<CntTargetSync> tarlist)
			throws DomainServiceException;

	public HashMap cancelpublishChannelcontentTar(List<CntTargetSync> tarlist)
			throws DomainServiceException;

	public TableDataInfo queryCtgPltSyncCha(CSCntPlatformSync cntPlatformSync,
			int start, int pageSize);

	public TableDataInfo queryCtgTarSyncCha(CSCntTargetSync CScntTargetSync,
			int start, int pageSize);
	public TableDataInfo queryCtgPltSyncCach(CSCntPlatformSync cntPlatformSync,
				int start, int pageSize);
	public TableDataInfo queryCtgTarSyncCach(CSCntTargetSync CScntTargetSync,
			int start, int pageSize);
	public String preparetopublishForMap(Targetsystem tar, long objindex,
			String objid, int objtype, String elementid, String parentid)
			throws DomainServiceException;
	public HashMap deletecntsync(long syncindex);
	
	public HashMap publishChannelcontentSingle(List<CntPlatformSync> cntPlatformSynclist);
	public HashMap canclePublishChannelSingle(List<CntPlatformSync> cntPlatformSynclist);
	
	
	/************************海报管理****陈虎********************/
	
	
    public TableDataInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws Exception;
	
    
    public TableDataInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws Exception;

    public String batchPublishOrDelPublishchanPic(List<ChannelPictureMap> list,String operType, int type) throws Exception;

    public String publishchanPicMapPlatformSyn(String mapindex, String channelid,String pictureid,int type,int synType) throws Exception;

    public String delPublishchanPicMapPlatformSyn(String mapindex, String channelid,String pictureid,int type,int synType) throws Exception;

    public String publishchanPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;

    public String delPublishchanPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;
   
    
    
    
	/************************海报管理************************/	
}
package com.zte.cms.server.dao;

import java.util.List;

import com.zte.cms.server.model.CmsServer;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsServerDAO extends DynamicObjectBaseDao implements ICmsServerDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsServer(CmsServer cmsServer) throws DAOException
    {
        log.debug("insert cmsServer starting...");
        super.insert("insertCmsServer", cmsServer);
        log.debug("insert cmsServer end");
    }

    public void insertCmsServerList(List<CmsServer> cmsServerList) throws DAOException
    {
        log.debug("insert cmsServerList starting...");
        if (null != cmsServerList)
        {
            super.insertBatch("insertCmsServer", cmsServerList);
        }
        log.debug("insert cmsServerList end");
    }

    public void updateCmsServer(CmsServer cmsServer) throws DAOException
    {
        log.debug("update cmsServer by pk starting...");
        super.update("updateCmsServer", cmsServer);
        log.debug("update cmsServer by pk end");
    }

    public void updateCmsServerList(List<CmsServer> cmsServerList) throws DAOException
    {
        log.debug("update cmsServerList by pk starting...");
        super.updateBatch("updateCmsServer", cmsServerList);
        log.debug("update cmsServerList by pk end");
    }

    public void updateCmsServerByCond(CmsServer cmsServer) throws DAOException
    {
        log.debug("update cmsServer by conditions starting...");
        super.update("updateCmsServerByCond", cmsServer);
        log.debug("update cmsServer by conditions end");
    }

    public void updateCmsServerListByCond(List<CmsServer> cmsServerList) throws DAOException
    {
        log.debug("update cmsServerList by conditions starting...");
        super.updateBatch("updateCmsServerByCond", cmsServerList);
        log.debug("update cmsServerList by conditions end");
    }

    public void deleteCmsServer(CmsServer cmsServer) throws DAOException
    {
        log.debug("delete cmsServer by pk starting...");
        super.delete("deleteCmsServer", cmsServer);
        log.debug("delete cmsServer by pk end");
    }

    public void deleteCmsServerList(List<CmsServer> cmsServerList) throws DAOException
    {
        log.debug("delete cmsServerList by pk starting...");
        super.deleteBatch("deleteCmsServer", cmsServerList);
        log.debug("delete cmsServerList by pk end");
    }

    public void deleteCmsServerByCond(CmsServer cmsServer) throws DAOException
    {
        log.debug("delete cmsServer by conditions starting...");
        super.delete("deleteCmsServerByCond", cmsServer);
        log.debug("delete cmsServer by conditions end");
    }

    public void deleteCmsServerListByCond(List<CmsServer> cmsServerList) throws DAOException
    {
        log.debug("delete cmsServerList by conditions starting...");
        super.deleteBatch("deleteCmsServerByCond", cmsServerList);
        log.debug("delete cmsServerList by conditions end");
    }

    public CmsServer getCmsServer(CmsServer cmsServer) throws DAOException
    {
        log.debug("query cmsServer starting...");
        CmsServer resultObj = (CmsServer) super.queryForObject("getCmsServer", cmsServer);
        log.debug("query cmsServer end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer) throws DAOException
    {
        log.debug("query cmsServer by condition starting...");
        List<CmsServer> rList = (List<CmsServer>) super.queryForList("queryCmsServerListByCond", cmsServer);
        log.debug("query cmsServer by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer, PageUtilEntity puEntity) throws DAOException
    {
        log.debug("query cmsServer by condition starting...");
        List<CmsServer> rList = (List<CmsServer>) super.queryForList("queryCmsServerListByCond", cmsServer, puEntity);
        log.debug("query cmsServer by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsServer by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsServerListCntByCond", cmsServer)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsServer> rsList = (List<CmsServer>) super.pageQuery("queryCmsServerListByCond", cmsServer, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsServer by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsServerListByCond", "queryCmsServerListCntByCond", cmsServer, start,
                pageSize, puEntity);
    }

}
﻿
function initComponentList() {
	
    var obj = document.getElementById("sltComponent");
	obj.options.length=0;
	obj.options.add(new Option("请选择", ""));
	obj.options.add(new Option("-------------", ""));
	// 集团
	obj.options.add(new Option("集团管理组件","umap_corp_common"));	
	obj.options.add(new Option("-------------", ""));

	// 系统
	obj.options.add(new Option("系统参数配置管理组件","umap_sys_config"));
	obj.options.add(new Option("区域管理组件","umap_sys_area"));
	obj.options.add(new Option("公告管理组件","umap_sys_bulletin"));
	obj.options.add(new Option("日志管理组件","umap_sys_log"));
	obj.options.add(new Option("终端信息管理组件","umap_sys_terminal"));
	obj.options.add(new Option("设备管理组件","umap_device_common"));
	obj.options.add(new Option("投诉单管理组件","umap_customer_advice"));
	obj.options.add(new Option("-------------", ""));

	// CPSP
	obj.options.add(new Option("CPSP考核管理组件","umap_cpsp_assess"));
	obj.options.add(new Option("CPSP基本信息管理组件","umap_cpsp_common"));
	obj.options.add(new Option("CPSP违规管理组件","umap_cpsp_violation"));
	obj.options.add(new Option("CPSP黑名单管理组件","umap_cpsp_wkfwblacklist"));
	obj.options.add(new Option("系统颜色名单管理组件","umap_sys_colorlist"));
	obj.options.add(new Option("-------------", ""));

	// 业务能力
	obj.options.add(new Option("业务能力签约管理组件", "umap_appabl_common"));
	obj.options.add(new Option("系统业务能力管理组件", "umap_appabl_sysappabl"));
	obj.options.add(new Option("系统SLA信息管理组件", "umap_appabl_syssla"));
	obj.options.add(new Option("业务能力黑名单管理组件", "umap_appabl_wkfwblacklist"));
	obj.options.add(new Option("-------------", ""));

	// 业务
	obj.options.add(new Option("业务信息管理组件", "umap_app_common"));
	obj.options.add(new Option("业务类别管理组件", "umap_app_group"));
	obj.options.add(new Option("业务目录管理组件", "umap_app_syscatalog"));
	obj.options.add(new Option("业务黑名单管理组件", "umap_app_wkfwblacklist"));
	obj.options.add(new Option("-------------", ""));

	//产品
	obj.options.add(new Option("产品信息管理组件", "umap_product_common"));
	obj.options.add(new Option("产品绑定关系管理组件", "umap_product_bind"));
	obj.options.add(new Option("-------------", ""));

	// 计费
	obj.options.add(new Option("计费方案管理组件", "umap_jf_charge"));
	obj.options.add(new Option("折扣方案管理组件", "umap_jf_discount"));
	obj.options.add(new Option("-------------", ""));

	// 套餐
	obj.options.add(new Option("套餐管理组件", "umap_pkg_common"));
	obj.options.add(new Option("-------------", ""));

	// 用户
	obj.options.add(new Option("用户组管理组件", "umap_user_group"));
	obj.options.add(new Option("用户基本信息管理组件", "umap_user_common"));
	obj.options.add(new Option("用户订购关系管理组件", "umap_order_common"));
	obj.options.add(new Option("-------------", ""));

	// 内容
	obj.options.add(new Option("内容人物角色管理组件", "ucmp_actor"));
	obj.options.add(new Option("内容目录管理组件", "ucmp_cntcpdirectory"));
	obj.options.add(new Option("服务器管理组件", "ucmp_server"));
	obj.options.add(new Option("内容版权管理组件", "ucmp_rights"));
	obj.options.add(new Option("临时空间管理组件", "ucmp_tempspace"));
	obj.options.add(new Option("内容基本信息管理组件", "ucmp_content"));
	obj.options.add(new Option("-------------", ""));
	
    	obj.options.add(new Option("同步组件", "umap_common_datasync"));
}

function changeComponentList() {
	var val = document.getElementById("objComponent").value;
	if (val == "") return;
	initBeanList(val);
	changeBeanList();
}

function changeBeanList() {
	var val = document.getElementById("objBean").value;
	if (val == "") return;
	initParamList(val);
	changeParamList();
}

function changeParamList() {
	var val = document.getElementById("objParam").value;
	if (val == "") return;
	doQuery(val);
}

function initBeanList(comp) {
	var obj = document.getElementById("sltBean");
	obj.options.length=0;
	obj.options.add(new Option("请选择", ""));
	obj.options.add(new Option("-------------", ""));
    // 集团
	if (comp == "umap_corp_common")	{
		obj.options.add(new Option("集团基本信息", "ucorpBasicFacade"));
		obj.options.add(new Option("集团联系人", "ucorpContactFacade"));
	}
	// 系统
	if (comp == "umap_sys_config")	{
		obj.options.add(new Option("系统参数配置管理", "usysConfigFacade"));
	}
	if (comp == "umap_sys_area")	{
		obj.options.add(new Option("请选择", ""));
		obj.options.add(new Option("省份管理", "usysProvinceFacade"));
		obj.options.add(new Option("区域管理", "usysAreaFacade"));
        obj.options.add(new Option("城市管理", "usysCityFacade"));
        obj.options.add(new Option("业务区管理", "usysSmallareaFacade"));
		obj.options.add(new Option("号段管理", "usysPhoneFacade"));
	}
	if (comp == "umap_sys_bulletin")	{
		obj.options.add(new Option("公告管理", "usysBulletinFacade"));
	}
	if (comp == "umap_sys_log")	{
		obj.options.add(new Option("日志01信息管理", "ssbLog01Facade"));
		obj.options.add(new Option("日志02信息管理", "ssbLog02Facade"));
		obj.options.add(new Option("日志03信息管理", "ssbLog03Facade"));
		obj.options.add(new Option("日志04信息管理", "ssbLog04Facade"));
		obj.options.add(new Option("日志05信息管理", "ssbLog05Facade"));
		obj.options.add(new Option("日志06信息管理", "ssbLog06Facade"));
		obj.options.add(new Option("日志07信息管理", "ssbLog07Facade"));
		obj.options.add(new Option("日志08信息管理", "ssbLog08Facade"));
		obj.options.add(new Option("日志09信息管理", "ssbLog09Facade"));
		obj.options.add(new Option("日志10信息管理", "ssbLog10Facade"));
		obj.options.add(new Option("日志11信息管理", "ssbLog11Facade"));
		obj.options.add(new Option("日志12信息管理", "ssbLog12Facade"));
		obj.options.add(new Option("日志01历史信息管理", "ssbLogHis01Facade"));
		obj.options.add(new Option("日志02历史信息管理", "ssbLogHis02Facade"));
		obj.options.add(new Option("日志03历史信息管理", "ssbLogHis03Facade"));
		obj.options.add(new Option("日志04历史信息管理", "ssbLogHis04Facade"));
		obj.options.add(new Option("日志05历史信息管理", "ssbLogHis05Facade"));
		obj.options.add(new Option("日志06历史信息管理", "ssbLogHis06Facade"));
		obj.options.add(new Option("日志07历史信息管理", "ssbLogHis07Facade"));
		obj.options.add(new Option("日志08历史信息管理", "ssbLogHis08Facade"));
		obj.options.add(new Option("日志09历史信息管理", "ssbLogHis09Facade"));
		obj.options.add(new Option("日志10历史信息管理", "ssbLogHis10Facade"));
		obj.options.add(new Option("日志11历史信息管理", "ssbLogHis11Facade"));
		obj.options.add(new Option("日志12历史信息管理", "ssbLogHis12Facade"));
	}
	if (comp == "umap_sys_terminal")	{
		obj.options.add(new Option("终端信息管理", "uhandsetFacade"));
        obj.options.add(new Option("终端型号映射管理", "uhandsetModelFacade"));
        obj.options.add(new Option("终端url映射管理", "uhandsetUaFacade"));
	}
	if (comp == "umap_device_common")	{
		obj.options.add(new Option("FTP服务器设备管理", "udevFtpserverFacade"));
        obj.options.add(new Option("设备管理", "udevInfoFacade"));
		obj.options.add(new Option("ISMP平台信息管理", "udevIsmpFacade"));
		obj.options.add(new Option("设备与号段关联关系管理", "udevPhoneFacade"));
		obj.options.add(new Option("业务门户配置管理", "udevPortalFacade"));
		obj.options.add(new Option("SCP设备管理", "udevScpFacade"));
		obj.options.add(new Option("业务引擎配置管理", "udevSengFacade"));
	}
	if (comp == "umap_customer_advice")	{
		obj.options.add(new Option("投诉单管理", "uusrAdviceFacade"));
		obj.options.add(new Option("指派单管理", "uusrAdviceAssignFacade"));
		obj.options.add(new Option("投诉单历史信息管理", "uusrAdviceWkfwFacade"));
		obj.options.add(new Option("指派单历史信息管理", "uusrAdviceAssignWkfwFacade"));
	}

	// CPSP
	if(comp == "umap_cpsp_assess"){
	    obj.options.add(new Option("考核结果管理","ucpAssessFacade"));
	    obj.options.add(new Option("考核结果历史管理","ucpAssessHisFacade"));
	    obj.options.add(new Option("考核规则管理","ucpAssessRuleFacade"));
	    obj.options.add(new Option("考核规则历史管理","ucpAssessRuleHisFacade"));
	}
    else if(comp == "umap_cpsp_common"){
        obj.options.add(new Option("CPSP基本信息管理","ucpBasicFacade"));
        obj.options.add(new Option("CPSP基本缓存信息管理","ucpBasicBufFacade"));
        obj.options.add(new Option("CPSP基本工作流历史信息管理","ucpBasicWkfwhisFacade"));
        obj.options.add(new Option("CPSP联系人信息管理","ucpContactFacade"));
        obj.options.add(new Option("CPSP联系人缓存信息管理","ucpContactBufFacade"));
        obj.options.add(new Option("CPSP联系人工作流历史信息管理","ucpContactWkfwhisFacade"));
        obj.options.add(new Option("CPSP文件信息管理","ucpFileFacade"));
        obj.options.add(new Option("CPSP文件缓存信息管理","ucpFileBufFacade"));
        obj.options.add(new Option("CPSP文件工作流历史信息管理","ucpFileWkfwhisFacade"));
    }
    else if(comp == "umap_cpsp_violation"){
        obj.options.add(new Option("CPSP违规信息管理","ucpViolationFacade"));    
    }
    else if(comp == "umap_cpsp_wkfwblacklist"){
        obj.options.add(new Option("CPSP黑名单管理","ucpWkfwblacklistFacade"));     
    }
    else if(comp == "umap_sys_colorlist"){
        obj.options.add(new Option("系统颜色名单管理","usysColorlistFacade"));       
    }

	// 业务能力
	else if (comp == "umap_appabl_common")	{
		obj.options.add(new Option("业务能力接入码信息管理", "uappablAccesscodeDS"));
		obj.options.add(new Option("业务能力接入码缓存信息管理", "uappablAccesscodeBufDS"));
		obj.options.add(new Option("业务能力接入码历史信息管理", "uappablAccesscodeWkfwhisDS"));
		obj.options.add(new Option("业务能力sla信息管理", "uappablSlaDS"));
		obj.options.add(new Option("业务能力sla缓存信息管理", "uappablSlaBufDS"));
		obj.options.add(new Option("业务能力sla历史信息管理", "uappablSlaWkfwhisDS"));
		obj.options.add(new Option("业务能力信息管理", "uappablSignupDS"));
		obj.options.add(new Option("业务能力缓存信息管理", "uappablSignupBufDS"));
		obj.options.add(new Option("业务能力历史信息管理", "uappablSignupWkfwhisDS"));
	}
	else if (comp == "umap_appabl_sysappabl")	{
		obj.options.add(new Option("业务能力依赖关系管理", "usysAppablDependDS"));
		obj.options.add(new Option("系统业务能力url管理", "usysApptypeUrlDS"));
		obj.options.add(new Option("系统业务流程类型管理", "usysAppFlowDS"));
		obj.options.add(new Option("系统业务能力管理", "usysAppablDS"));
	}
	else if (comp == "umap_appabl_syssla")	{
		obj.options.add(new Option("系统sla信息管理", "usysSlaPolicyDS"));
	}
	else if (comp == "umap_appabl_wkfwblacklist")	{
		obj.options.add(new Option("业务能力黑名单管理", "uappablSignupWkfwblackDS"));
	}

	// 业务
	else if (comp == "umap_app_common")	{
		obj.options.add(new Option("业务临时信息管理", "uappBasicBufDS"));
		obj.options.add(new Option("业务信息管理", "uappBasicDS"));
		obj.options.add(new Option("业务历史信息管理", "uappBasicWkfwhisDS"));
		obj.options.add(new Option("业务所属类别临时信息管理", "uappGroupMapbufDS"));
		obj.options.add(new Option("业务所属类别信息管理", "uappGroupMapDS"));
		obj.options.add(new Option("业务所属类别历史信息管理", "uappGroupMapwkfwhisDS"));
		obj.options.add(new Option("业务签约SLA临时信息管理", "uappSlaInfoBufDS"));
		obj.options.add(new Option("业务签约SLA信息管理", "uappSlaInfoDS"));
		obj.options.add(new Option("业务签约SLA历史信息管理", "uappSlaInfoWkfwhisDS"));
	}
	else if (comp == "umap_app_group")	{
		obj.options.add(new Option("业务类别管理", "uappGroupDS"));
	}
	else if (comp == "umap_app_syscatalog")	{
		obj.options.add(new Option("业务目录管理", "usysCatalogDS"));
	}
	else if (comp == "umap_app_wkfwblacklist")	{
		obj.options.add(new Option("业务黑名单管理", "uappBlacklistDS"));
	}

	//产品
	else if (comp == "umap_product_common")	{
		obj.options.add(new Option("产品临时信息管理", "uproductBufDS"));
		obj.options.add(new Option("产品信息管理", "uproductDS"));
		obj.options.add(new Option("产品信息关联关系", "uprdtChargeMapDS"));
		obj.options.add(new Option("产品历史信息管理", "uproductWkfwhisDS"));
		obj.options.add(new Option("产品临时指令信息管理", "uproductOrderBufDS"));
		obj.options.add(new Option("产品指令信息管理", "uproductOrderDS"));
		obj.options.add(new Option("产品历史指令信息管理", "uproductOrderWkfwhisDS"));
	}
	else if (comp == "umap_product_bind")	{
		obj.options.add(new Option("产品临时绑定关系管理", "umapPrdtBindMapbufDS"));
		obj.options.add(new Option("产品绑定关系管理", "umapPrdtBindMapDS"));
		obj.options.add(new Option("产品历史绑定关系管理", "umapPrdtBindMapwkfwhisDS"));
	}

	// 套餐
	else if (comp == "umap_pkg_common")	{
		obj.options.add(new Option("套餐产品绑定关系管理", "uproductPkgMapDS"));
		obj.options.add(new Option("套餐产品绑定关系缓存信息管理", "uproductPkgMapBufDS"));
		obj.options.add(new Option("套餐产品绑定关系历史信息管理", "uproductPkgMapWkfwhisDS"));
		obj.options.add(new Option("套餐管理", "uproductPkgDS"));
		obj.options.add(new Option("套餐缓存信息管理", "uproductPkgBufDS"));
		obj.options.add(new Option("套餐历史信息管理", "uproductPkgWkfwhisDS"));
	}

	//计费
	else if (comp == "umap_jf_charge")	{
		obj.options.add(new Option("货币信息管理", "ujfCurrencyDS"));
		obj.options.add(new Option("计量单位管理", "ujfUnitDS"));
		obj.options.add(new Option("计费方案管理", "ujfChargeSchemeDS"));
		obj.options.add(new Option("业务能力类型和计费类型关联关系管理", "ujfAppablChargemodeDS"));
		obj.options.add(new Option("业务能力类型和计量单位关联关系管理", "ujfAppablUnitDS"));
	}
	else if (comp == "umap_jf_discount")	{
		obj.options.add(new Option("节假日折扣管理", "ujfDiscountHolidayDetailDS"));
		obj.options.add(new Option("节假日折扣类别管理", "ujfDiscountHolidayDS"));
		obj.options.add(new Option("时段折扣管理", "ujfDiscountPeriodDetailDS"));
		obj.options.add(new Option("时段折扣类别管理", "ujfDiscountPeriodDS"));
		obj.options.add(new Option("折扣方案管理", "ujfDiscountSchemeDS"));
		obj.options.add(new Option("时间折扣管理", "ujfDiscountTimeDS"));
		obj.options.add(new Option("累计折扣管理", "ujfDiscountTotalDetailDS"));
		obj.options.add(new Option("累计折扣类别管理", "ujfDiscountTotalDS"));
		obj.options.add(new Option("星期折扣管理", "ujfDiscountWeekDetailDS"));
		obj.options.add(new Option("星期折扣类别管理", "ujfDiscountWeekDS"));
	}

	// 用户
	else if (comp == "umap_user_group")	{
		obj.options.add(new Option("用户组信息管理", "uusrGroupFacade"));
		obj.options.add(new Option("用户组成员信息管理", "uusrGroupMemberFacade"));
	}
	else if (comp == "umap_user_common")	{
		obj.options.add(new Option("用户00基本信息管理", "uusrBasic00Facade"));
		obj.options.add(new Option("用户01基本信息管理", "uusrBasic01Facade"));
		obj.options.add(new Option("用户02基本信息管理", "uusrBasic02Facade"));
		obj.options.add(new Option("用户03基本信息管理", "uusrBasic03Facade"));
		obj.options.add(new Option("用户04基本信息管理", "uusrBasic04Facade"));
		obj.options.add(new Option("用户05基本信息管理", "uusrBasic05Facade"));
		obj.options.add(new Option("用户06基本信息管理", "uusrBasic06Facade"));
		obj.options.add(new Option("用户07基本信息管理", "uusrBasic07Facade"));
		obj.options.add(new Option("用户08基本信息管理", "uusrBasic08Facade"));
		obj.options.add(new Option("用户09基本信息管理", "uusrBasic09Facade"));
		obj.options.add(new Option("客户00信息管理", "uusrInfo00Facade"));
		obj.options.add(new Option("客户01信息管理", "uusrInfo01Facade"));
		obj.options.add(new Option("客户02信息管理", "uusrInfo02Facade"));
		obj.options.add(new Option("客户03信息管理", "uusrInfo03Facade"));
		obj.options.add(new Option("客户04信息管理", "uusrInfo04Facade"));
		obj.options.add(new Option("客户05信息管理", "uusrInfo05Facade"));
		obj.options.add(new Option("客户06信息管理", "uusrInfo06Facade"));
		obj.options.add(new Option("客户07信息管理", "uusrInfo07Facade"));
		obj.options.add(new Option("客户08信息管理", "uusrInfo08Facade"));
		obj.options.add(new Option("客户09信息管理", "uusrInfo09Facade"));
	}
	else if (comp == "umap_order_common")	{
		obj.options.add(new Option("订购产品00基本信息管理", "usubscription00Facade"));
		obj.options.add(new Option("订购产品01基本信息管理", "usubscription01Facade"));
		obj.options.add(new Option("订购产品02基本信息管理", "usubscription02Facade"));
		obj.options.add(new Option("订购产品03基本信息管理", "usubscription03Facade"));
		obj.options.add(new Option("订购产品04基本信息管理", "usubscription04Facade"));
		obj.options.add(new Option("订购产品05基本信息管理", "usubscription05Facade"));
		obj.options.add(new Option("订购产品06基本信息管理", "usubscription06Facade"));
		obj.options.add(new Option("订购产品07基本信息管理", "usubscription07Facade"));
		obj.options.add(new Option("订购产品08基本信息管理", "usubscription08Facade"));
		obj.options.add(new Option("订购产品09基本信息管理", "usubscription09Facade"));
		obj.options.add(new Option("订购产品历史01基本信息管理", "usubscriptionHis01Facade"));
		obj.options.add(new Option("订购产品历史02基本信息管理", "usubscriptionHis02Facade"));
		obj.options.add(new Option("订购产品历史03基本信息管理", "usubscriptionHis03Facade"));
		obj.options.add(new Option("订购产品历史04基本信息管理", "usubscriptionHis04Facade"));
		obj.options.add(new Option("订购产品历史05基本信息管理", "usubscriptionHis05Facade"));
		obj.options.add(new Option("订购产品历史06基本信息管理", "usubscriptionHis06Facade"));
		obj.options.add(new Option("订购产品历史07基本信息管理", "usubscriptionHis07Facade"));
		obj.options.add(new Option("订购产品历史08基本信息管理", "usubscriptionHis08Facade"));
		obj.options.add(new Option("订购产品历史09基本信息管理", "usubscriptionHis09Facade"));
		obj.options.add(new Option("订购产品历史10基本信息管理", "usubscriptionHis10Facade"));
		obj.options.add(new Option("订购产品历史11基本信息管理", "usubscriptionHis11Facade"));
		obj.options.add(new Option("订购产品历史12基本信息管理", "usubscriptionHis12Facade"));
	}

	// 内容
	else if (comp == "ucmp_actor")	{
		obj.options.add(new Option("人物基本信息", "ucmCntActorFacade"));
		obj.options.add(new Option("人物基本信息缓存", "ucmCntActorBufFacade"));
		obj.options.add(new Option("内容和人物关联信息", "ucmCntActorMapFacade"));
		obj.options.add(new Option("内容和人物关联信息缓存", "ucmCntActorMapBufFacade"));
		obj.options.add(new Option("内容和人物关联信息历史", "ucmCntActorMapWkfwhisFacade"));
	}
	else if (comp == "ucmp_cntcpdirectory")	{
		obj.options.add(new Option("内容目录基本信息", "ucmCntCpDirectoryFacade"));
	}
	else if (comp == "ucmp_server")	{
		obj.options.add(new Option("内容服务器", "ucmServerFacade"));
	}
	else if (comp == "ucmp_rights")	{
		obj.options.add(new Option("内容版权规则", "ucmRightsRuleFacade"));
		obj.options.add(new Option("内容版权操作", "ucmRightsActionFacade"));
	}
	else if (comp == "ucmp_tempspace")	{
		obj.options.add(new Option("内容临时空间基本信息", "ucmTempspaceFacade"));
		obj.options.add(new Option("内容临时空间缓存信息", "ucmTempspaceBufFacade"));
		obj.options.add(new Option("内容临时空间工作流信息", "ucmTempspaceWkfwhisFacade"));
	}
	else if (comp == "ucmp_content")	{
		obj.options.add(new Option("音频类型内容信息", "ucmAudioinfoFacade"));
		obj.options.add(new Option("内容和业务能力关联", "ucmCntAppablMapFacade"));
		obj.options.add(new Option("内容类型的层次关系", "ucmCntCatalogFacade"));
		obj.options.add(new Option("内容与分类map关系", "ucmCntCatalogMapFacade")); 
		obj.options.add(new Option("内容与实体文件关联", "ucmCntFileMapFacade"));
		obj.options.add(new Option("内容与实体文件关联缓存", "ucmCntFileMapBufFacade"));
		obj.options.add(new Option("内容与实体文件关联历史", "ucmCntFileMapWkfwhisFacade"));
		obj.options.add(new Option("内容使用类型", "ucmCntUsetypeFacade"));
		obj.options.add(new Option("内容基本信息", "ucmContentFacade"));
		obj.options.add(new Option("内容缓存", "ucmContentBufFacade"));
		obj.options.add(new Option("内容工作流历史", "ucmContentWkfwhisFacade"));
		obj.options.add(new Option("卡拉OK内容", "ucmContentKalaokFacade"));
		obj.options.add(new Option("卡拉OK内容缓存", "ucmContentKalaokBufFacade"));
		obj.options.add(new Option("卡拉OK内容工作流历史表", "ucmContentKalaokWkfwhisFacade"));
		obj.options.add(new Option("内容所属地区编码", "ucmCountryFacade"));
		obj.options.add(new Option("文件基本信息", "ucmFileFacade"));
		obj.options.add(new Option("音频类型子内容信息", "ucmFileAudioinfoFacade"));
		obj.options.add(new Option("文件缓存", "ucmFileBufFacade"));
		obj.options.add(new Option("文件工作流历史", "ucmFileWkfwhisFacade"));
		obj.options.add(new Option("系统支持的内容文件类型的后缀", "ucmFileFormatFacade"));
		obj.options.add(new Option("内容mimetype信息", "ucmFileMimetypeFacade"));
		obj.options.add(new Option("图片类型子内容信息", "ucmFilePicinfoFacade"));
		obj.options.add(new Option("内容和终端绑定关系", "ucmFileTerminalmapFacade"));
		obj.options.add(new Option("视频类型子内容信息", "ucmFileVideoinfoFacade"));
		obj.options.add(new Option("文件工作流历史表", "ucmFileWkfwhisFacade"));
		obj.options.add(new Option("版权规则表", "ucmPreviewfileFacade"));
		obj.options.add(new Option("视频类型内容信息", "ucmVideoinfoFacade"));
		obj.options.add(new Option("同步任务信息", "ucmFtptaskFacade"));
		obj.options.add(new Option("同步任务历史信息", "ucmFtptaskHisFacade"));
	}
	else if (comp == "umap_common_datasync")	{
		obj.options.add(new Option("同步任务基本信息", "usyncTaskFacade"));
		obj.options.add(new Option("同步任务历史信息", "usyncTaskResultFacade"));
		obj.options.add(new Option("信息类型和设备信息", "usyncInfoDevMapFacade"));
		obj.options.add(new Option("流水号信息", "usysTransactionnoFacade"));
	}
}

function initParamList(bean) {
	var obj = document.getElementById("sltParam");
	obj.options.length=0;
	obj.options.add(new Option("请选择", ""));
	obj.options.add(new Option("-------------", ""));

    // 集团
	if (bean == "ucorpBasicFacade")	{
		obj.options.add(new Option("集团基本信息实体", "com.zte.umap.corp.common.model.UcorpBasic"));
	}
	else if(bean == "ucorpContactFacade")	
	{
         obj.options.add(new Option("集团联系人实体", "com.zte.umap.corp.common.model.UcorpContact"));
	}
	// 系统
	else if (bean == "usysConfigFacade")	{
		obj.options.add(new Option("系统参数配置实体", "com.zte.umap.sys.model.UsysConfig"));
	}
	else if (bean == "usysProvinceFacade")	{
		obj.options.add(new Option("省份实体", "com.zte.umap.sys.model.UsysProvince"));
	}
    else if (bean == "usysAreaFacade")	     {
		obj.options.add(new Option("区域实体", "com.zte.umap.sys.model.UsysArea"));
	}
    else if (bean == "usysCityFacade")	     {
		obj.options.add(new Option("城市实体", "com.zte.umap.sys.model.UsysCity"));
	}
    else if (bean == "usysSmallareaFacade")	     {
		obj.options.add(new Option("业务区实体", "com.zte.umap.sys.model.UsysSmallarea"));
	}
    else if (bean == "usysPhoneFacade")	     {
		obj.options.add(new Option("号段实体", "com.zte.umap.sys.model.UsysPhone"));
	}
	else if (bean == "usysBulletinFacade")	{
		obj.options.add(new Option("公告实体", "com.zte.umap.sys.model.UsysBulletin"));
	}
	else if (bean == "ssbLog01Facade")	{
		obj.options.add(new Option("日志01信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog01"));
	}
	else if (bean == "ssbLog02Facade")	{
		obj.options.add(new Option("日志02信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog02"));
	}
	else if (bean == "ssbLog03Facade")	{
		obj.options.add(new Option("日志03信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog03"));
	}
	else if (bean == "ssbLog04Facade")	{
		obj.options.add(new Option("日志04信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog04"));
	}
	else if (bean == "ssbLog05Facade")	{
		obj.options.add(new Option("日志05信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog05"));
	}
	else if (bean == "ssbLog06Facade")	{
		obj.options.add(new Option("日志06信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog06"));
	}
	else if (bean == "ssbLog07Facade")	{
		obj.options.add(new Option("日志07信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog07"));
	}
	else if (bean == "ssbLog08Facade")	{
		obj.options.add(new Option("日志08信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog08"));
	}
	else if (bean == "ssbLog09Facade")	{
		obj.options.add(new Option("日志09信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog09"));
	}
	else if (bean == "ssbLog10Facade")	{
		obj.options.add(new Option("日志10信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog10"));
	}
	else if (bean == "ssbLog11Facade")	{
		obj.options.add(new Option("日志11信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog11"));
	}
	else if (bean == "ssbLog12Facade")	{
		obj.options.add(new Option("日志12信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLog12"));
	}
	else if (bean == "ssbLogHis01Facade")	{
		obj.options.add(new Option("日志01历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis01"));
	}
	else if (bean == "ssbLogHis02Facade")	{
		obj.options.add(new Option("日志02历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis02"));
	}
	else if (bean == "ssbLogHis03Facade")	{
		obj.options.add(new Option("日志03历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis03"));
	}
	else if (bean == "ssbLogHis04Facade")	{
		obj.options.add(new Option("日志04历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis04"));
	}
	else if (bean == "ssbLogHis05Facade")	{
		obj.options.add(new Option("日志05历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis05"));
	}
	else if (bean == "ssbLogHis06Facade")	{
		obj.options.add(new Option("日志06历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis06"));
	}
	else if (bean == "ssbLogHis07Facade")	{
		obj.options.add(new Option("日志07历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis07"));
	}
	else if (bean == "ssbLogHis08Facade")	{
		obj.options.add(new Option("日志08历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis08"));
	}
	else if (bean == "ssbLogHis09Facade")	{
		obj.options.add(new Option("日志09历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis09"));
	}
	else if (bean == "ssbLogHis10Facade")	{
		obj.options.add(new Option("日志10历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis10"));
	}
	else if (bean == "ssbLogHis11Facade")	{
		obj.options.add(new Option("日志11历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis11"));
	}
	else if (bean == "ssbLogHis12Facade")	{
		obj.options.add(new Option("日志12历史信息实体", "com.zte.umap.ssb.logmanager.business.model.SsbLogHis12"));
	}
	else if (bean == "uhandsetFacade")	{
		obj.options.add(new Option("终端信息实体", "com.zte.umap.terminal.model.Uhandset"));
	}
    else if (bean == "uhandsetModelFacade")	{
		obj.options.add(new Option("终端型号映射实体", "com.zte.umap.terminal.model.UhandsetModel"));
	}
    else if (bean == "uhandsetUaFacade")	{
		obj.options.add(new Option("终端url映射实体", "com.zte.umap.terminal.model.UhandsetUa"));
	}
	else if (bean == "udevFtpserverFacade")	{
		obj.options.add(new Option("FTP服务器设备实体", "com.zte.umap.device.model.UdevFtpserver"));
	}
	else if (bean == "udevInfoFacade")	     {
		obj.options.add(new Option("设备实体", "com.zte.umap.device.model.UdevInfo"));
	}
    else if (bean == "udevIsmpFacade")	     {
		obj.options.add(new Option("ISMP平台信息实体", "com.zte.umap.device.model.UdevIsmp"));
	}
    else if (bean == "udevPhoneFacade")	     {
		obj.options.add(new Option("设备与号段关联关系实体", "com.zte.umap.device.model.UdevPhone"));
	}
    else if (bean == "udevPortalFacade")	     {
		obj.options.add(new Option("业务门户配置实体", "com.zte.umap.device.model.UdevPortal"));
	}
    else if (bean == "udevScpFacade")	     {
		obj.options.add(new Option("SCP设备实体", "com.zte.umap.device.model.UdevScp"));
	}
	else if (bean == "udevSengFacade")	     {
		obj.options.add(new Option("业务引擎配置实体", "com.zte.umap.device.model.UdevSeng"));
	}
	else if (bean == "uusrAdviceFacade")	{
		obj.options.add(new Option("投诉单实体", "com.zte.umap.advice.model.UusrAdvice"));
	}
    else if (bean == "uusrAdviceAssignFacade")	     {
		obj.options.add(new Option("指派单实体", "com.zte.umap.advice.model.UusrAdviceAssign"));
	}
    else if (bean == "uusrAdviceWkfwFacade")	     {
		obj.options.add(new Option("投诉单历史信息实体", "com.zte.umap.advice.model.UusrAdviceWkfw"));
	}
    else if (bean == "uusrAdviceAssignWkfwFacade")	     {
		obj.options.add(new Option("指派单历史信息实体", "com.zte.umap.advice.model.UusrAdviceAssignWkfw"));
	}

	// CPSP
	else if(bean == "ucpAssessFacade"){
		obj.options.add(new Option("考核结果实体", "com.zte.umap.cpsp.model.UcpAssess"));	
	}
	else if(bean == "ucpAssessHisFacade"){
		obj.options.add(new Option("考核结果历史实体", "com.zte.umap.cpsp.model.UcpAssessHis"));	
	}
	else if(bean == "ucpAssessRuleFacade"){
		obj.options.add(new Option("考核规则实体", "com.zte.umap.cpsp.model.UcpAssessRule"));	
	}

	else if(bean == "ucpAssessRuleHisFacade"){
		obj.options.add(new Option("考核规则历史实体", "com.zte.umap.cpsp.model.UcpAssessRuleHis"));	
	}
	else if(bean == "ucpBasicFacade"){
		obj.options.add(new Option("CPSP基本信息实体", "com.zte.umap.cpsp.model.UcpBasic"));	
	}
	else if(bean == "ucpBasicBufFacade"){
		obj.options.add(new Option("CPSP基本缓存信息实体", "com.zte.umap.cpsp.model.UcpBasicBuf"));	
	}
	else if(bean == "ucpBasicWkfwhisFacade"){
		obj.options.add(new Option("CPSP基本工作流历史信息实体", "com.zte.umap.cpsp.model.UcpBasicWkfwhis"));	
	}
	else if(bean == "ucpContactFacade"){
		obj.options.add(new Option("CPSP联系人信息实体", "com.zte.umap.cpsp.model.UcpContact"));	
	}
	else if(bean == "ucpContactBufFacade"){
		obj.options.add(new Option("CPSP联系人缓存信息实体", "com.zte.umap.cpsp.model.UcpContactBuf"));	
	}
	else if(bean == "ucpContactWkfwhisFacade"){
		obj.options.add(new Option("CPSP联系人工作流历史信息实体", "com.zte.umap.cpsp.model.UcpContactWkfwhis"));	
	}
	else if(bean == "ucpFileFacade"){
		obj.options.add(new Option("CPSP文件信息实体", "com.zte.umap.cpsp.model.UcpFile"));	
	}
	else if(bean == "ucpFileBufFacade"){
		obj.options.add(new Option("CPSP文件缓存信息实体", "com.zte.umap.cpsp.model.UcpFileBuf"));	
	}
	else if(bean == "ucpFileWkfwhisFacade"){
		obj.options.add(new Option("CPSP文件工作流历史信息实体", "com.zte.umap.cpsp.model.UcpFileWkfwhis"));	
	}

	else if(bean == "ucpViolationFacade"){
		obj.options.add(new Option("CPSP违规信息实体", "com.zte.umap.cpsp.model.UcpViolation"));	
	}
	else if(bean == "ucpWkfwblacklistFacade"){
		obj.options.add(new Option("CPSP黑名单信息实体", "com.zte.umap.cpsp.model.UcpWkfwblacklist"));	
	}
	else if(bean == "usysColorlistFacade"){
		obj.options.add(new Option("系统颜色名单信息实体", "com.zte.umap.sys.model.UsysColorlist"));	
	}

	// 业务能力
	else if (bean == "uappablAccesscodeBufDS")	{
		obj.options.add(new Option("业务能力接入码缓存信息实体", "com.zte.umap.appabl.common.model.UappablAccesscodeBuf"));
	}
	else if (bean == "uappablAccesscodeDS")	{
		obj.options.add(new Option("业务能力接入码信息实体", "com.zte.umap.appabl.common.model.UappablAccesscode"));
	}
	else if (bean == "uappablAccesscodeWkfwhisDS")	{
		obj.options.add(new Option("业务能力接入码历史信息实体", "com.zte.umap.appabl.common.model.UappablAccesscodeWkfwhis"));
	}
	else if (bean == "uappablSlaBufDS")	{
		obj.options.add(new Option("业务能力sla缓存信息实体", "com.zte.umap.appabl.common.model.UappablSlaBuf"));
	}
	else if (bean == "uappablSlaDS")	{
		obj.options.add(new Option("业务能力sla信息实体", "com.zte.umap.appabl.common.model.UappablSla"));
	}
	else if (bean == "uappablSlaWkfwhisDS")	{
		obj.options.add(new Option("业务能力sla历史信息实体", "com.zte.umap.appabl.common.model.UappablSlaWkfwhis"));
	}
	else if (bean == "uappablSignupBufDS")	{
		obj.options.add(new Option("业务能力缓存信息实体", "com.zte.umap.appabl.common.model.UappablSignupBuf"));
	}
	else if (bean == "uappablSignupDS")	{
		obj.options.add(new Option("业务能力信息实体", "com.zte.umap.appabl.common.model.UappablSignup"));
	}
	else if (bean == "uappablSignupWkfwhisDS")	{
		obj.options.add(new Option("业务能力历史信息实体", "com.zte.umap.appabl.common.model.UappablSignupWkfwhis"));
	}

	else if (bean == "usysAppablDependDS")	{
		obj.options.add(new Option("业务能力依赖关系实体", "com.zte.umap.appabl.sysappabl.model.UsysAppablDepend"));
	}
	else if (bean == "usysApptypeUrlDS")	{
		obj.options.add(new Option("系统业务能力url实体", "com.zte.umap.appabl.sysappabl.model.UsysApptypeUrl"));
	}
	else if (bean == "usysAppFlowDS")	{
		obj.options.add(new Option("系统业务流程类型实体", "com.zte.umap.appabl.sysappabl.model.UsysAppFlow"));
	}
	else if (bean == "usysAppablDS")	{
		obj.options.add(new Option("系统业务能力实体", "com.zte.umap.appabl.sysappabl.model.UsysAppabl"));
	}

	else if (bean == "usysSlaPolicyDS")	{
		obj.options.add(new Option("系统sla信息实体", "com.zte.umap.appabl.syssla.model.UsysSlaPolicy"));
	}
	else if (bean == "uappablSignupWkfwblackDS")	{
		obj.options.add(new Option("业务能力黑名单实体", "com.zte.umap.appabl.wkfwblacklist.model.UappablSignupWkfwblack"));
	}

	// 业务
	else if (bean == "uappBasicBufDS")	{
		obj.options.add(new Option("业务临时信息实体", "com.zte.umap.app.common.model.UappBasicBuf"));
	}
	else if (bean == "uappBasicDS")	{
		obj.options.add(new Option("业务信息实体", "com.zte.umap.app.common.model.UappBasic"));
	}
	else if (bean == "uappBasicWkfwhisDS")	{
		obj.options.add(new Option("业务历史信息实体", "com.zte.umap.app.common.model.UappBasicWkfwhis"));
	}
	else if (bean == "uappGroupMapbufDS")	{
		obj.options.add(new Option("业务所属类别临时信息实体", "com.zte.umap.app.common.model.UappGroupMapbuf"));
	}
	else if (bean == "uappGroupMapDS")	{
		obj.options.add(new Option("业务所属类别信息实体", "com.zte.umap.app.common.model.UappGroupMap"));
	}
	else if (bean == "uappGroupMapwkfwhisDS")	{
		obj.options.add(new Option("业务所属类别历史信息实体", "com.zte.umap.app.common.model.UappGroupMapwkfwhis"));
	}
	else if (bean == "uappSlaInfoBufDS")	{
		obj.options.add(new Option("业务签约SLA临时信息实体", "com.zte.umap.app.common.model.UappSlaInfoBuf"));
	}
	else if (bean == "uappSlaInfoDS")	{
		obj.options.add(new Option("业务签约SLA信息实体", "com.zte.umap.app.common.model.UappSlaInfo"));
	}
	else if (bean == "uappSlaInfoWkfwhisDS")	{
		obj.options.add(new Option("业务签约SLA历史信息实体", "com.zte.umap.app.common.model.UappSlaInfoWkfwhis"));
	}

	else if (bean == "uappGroupDS")	{
		obj.options.add(new Option("业务类别信息实体", "com.zte.umap.app.group.model.UappGroup"));
	}
	else if (bean == "usysCatalogDS")	{
		obj.options.add(new Option("业务目录信息实体", "com.zte.umap.app.syscatalog.model.UsysCatalog"));
	}
	else if (bean == "uappBlacklistDS")	{
		obj.options.add(new Option("业务黑名单信息实体", "com.zte.umap.app.wkfwblacklist.model.UappBlacklist"));
	}

	//产品
	else if (bean == "uproductBufDS")	{
		obj.options.add(new Option("产品临时信息实体", "com.zte.umap.product.common.model.UproductBuf"));
	}
	else if (bean == "uproductDS")	{
		obj.options.add(new Option("产品信息实体", "com.zte.umap.product.common.model.Uproduct"));
	}
	else if (bean == "uprdtChargeMapDS") {
	    obj.options.add(new Option("产品信息关联关系",  "com.zte.umap.product.common.model.UprdtChargeMap"));
	}
	else if (bean == "uproductWkfwhisDS")	{
		obj.options.add(new Option("产品历史信息实体", "com.zte.umap.product.common.model.UproductWkfwhis"));
	}
	else if (bean == "uproductOrderBufDS")	{
		obj.options.add(new Option("产品临时指令信息实体", "com.zte.umap.product.common.model.UproductOrderBuf"));
	}
	else if (bean == "uproductOrderDS")	{
		obj.options.add(new Option("产品指令信息实体", "com.zte.umap.product.common.model.UproductOrder"));
	}
	else if (bean == "uproductOrderWkfwhisDS")	{
		obj.options.add(new Option("产品历史指令信息实体", "com.zte.umap.product.common.model.UproductOrderWkfwhis"));
	}

	else if (bean == "umapPrdtBindMapbufDS")	{
		obj.options.add(new Option("产品临时绑定关系实体", "com.zte.umap.product.bind.model.UmapPrdtBindMapbuf"));
	}
	else if (bean == "umapPrdtBindMapDS")	{
		obj.options.add(new Option("产品绑定关系实体", "com.zte.umap.product.bind.model.UmapPrdtBindMap"));
	}
	else if (bean == "umapPrdtBindMapwkfwhisDS")	{
		obj.options.add(new Option("产品历史绑定关系实体", "com.zte.umap.product.bind.model.UmapPrdtBindMapwkfwhis"));
	}

	// 套餐
	else if (bean == "uproductPkgMapDS")	{
		obj.options.add(new Option("套餐产品绑定关系实体", "com.zte.umap.pkg.common.model.UproductPkgMap"));
	}
	else if (bean == "uproductPkgMapBufDS")	{
		obj.options.add(new Option("套餐产品绑定关系缓存信息实体", "com.zte.umap.pkg.common.model.UproductPkgMapBuf"));
	}
	else if (bean == "uproductPkgMapWkfwhisDS")	{
		obj.options.add(new Option("套餐产品绑定关系历史信息实体", "com.zte.umap.pkg.common.model.UproductPkgMapWkfwhis"));
	}
	else if (bean == "uproductPkgDS")	{
		obj.options.add(new Option("套餐实体", "com.zte.umap.pkg.common.model.UproductPkg"));
	}
	else if (bean == "uproductPkgBufDS")	{
		obj.options.add(new Option("套餐缓存信息实体", "com.zte.umap.pkg.common.model.UproductPkgBuf"));
	}
	else if (bean == "uproductPkgWkfwhisDS")	{
		obj.options.add(new Option("套餐历史信息实体", "com.zte.umap.pkg.common.model.UproductPkgWkfwhis"));
	}

	// 计费
	else if (bean == "ujfCurrencyDS")	{
		obj.options.add(new Option("货币实体", "com.zte.umap.jf.charge.model.UjfCurrency"));
	}
	else if (bean == "ujfUnitDS")	{
		obj.options.add(new Option("计量单位实体", "com.zte.umap.jf.charge.model.UjfUnit"));
	}
	else if (bean == "ujfChargeSchemeDS")	{
		obj.options.add(new Option("计费方案实体", "com.zte.umap.jf.charge.model.UjfChargeScheme"));
	}
	else if (bean == "ujfAppablChargemodeDS")	{
		obj.options.add(new Option("业务能力类型和计费类型关联关系实体", "com.zte.umap.jf.charge.model.UjfAppablChargemode"));
	}
	else if (bean == "ujfAppablUnitDS")	{
		obj.options.add(new Option("业务能力类型和计量单位关联关系实体", "com.zte.umap.jf.charge.model.UjfAppablUnit"));
	}

	else if (bean == "ujfDiscountHolidayDetailDS")	{
		obj.options.add(new Option("节假日折扣实体", "com.zte.umap.jf.discount.model.UjfDiscountHolidayDetail"));
	}
	else if (bean == "ujfDiscountHolidayDS")	{
		obj.options.add(new Option("节假日折扣类别实体", "com.zte.umap.jf.discount.model.UjfDiscountHoliday"));
	}
	else if (bean == "ujfDiscountPeriodDetailDS")	{
		obj.options.add(new Option("时段折扣实体", "com.zte.umap.jf.discount.model.UjfDiscountPeriodDetail"));
	}
	else if (bean == "ujfDiscountPeriodDS")	{
		obj.options.add(new Option("时段折扣类别实体", "com.zte.umap.jf.discount.model.UjfDiscountPeriod"));
	}
	else if (bean == "ujfDiscountSchemeDS")	{
		obj.options.add(new Option("折扣方案实体", "com.zte.umap.jf.discount.model.UjfDiscountScheme"));
	}
	else if (bean == "ujfDiscountTimeDS")	{
		obj.options.add(new Option("时间折扣实体", "com.zte.umap.jf.discount.model.UjfDiscountTime"));
	}
	else if (bean == "ujfDiscountTotalDetailDS")	{
		obj.options.add(new Option("累计折扣实体", "com.zte.umap.jf.discount.model.UjfDiscountTotalDetail"));
	}
	else if (bean == "ujfDiscountTotalDS")	{
		obj.options.add(new Option("累计折扣类别实体", "com.zte.umap.jf.discount.model.UjfDiscountTotal"));
	}
	else if (bean == "ujfDiscountWeekDetailDS")	{
		obj.options.add(new Option("星期折扣实体", "com.zte.umap.jf.discount.model.UjfDiscountWeekDetail"));
	}
	else if (bean == "ujfDiscountWeekDS")	{
		obj.options.add(new Option("星期折扣类别实体", "com.zte.umap.jf.discount.model.UjfDiscountWeek"));
	}

	// 用户
	else if (bean == "uusrGroupFacade")	{
		obj.options.add(new Option("用户组实体", "com.zte.umap.user.group.model.UusrGroup"));
	}
	else if (bean == "uusrGroupMemberFacade")	{
		obj.options.add(new Option("用户组成员实体", "com.zte.umap.user.group.model.UusrGroupMember"));
	}

	else if (bean == "uusrBasic00Facade")	{
		obj.options.add(new Option("用户00实体", "com.zte.umap.user.model.UusrBasic00"));
	}
	else if (bean == "uusrBasic01Facade")	{
		obj.options.add(new Option("用户01实体", "com.zte.umap.user.model.UusrBasic01"));
	}
	else if (bean == "uusrBasic02Facade")	{
		obj.options.add(new Option("用户02实体", "com.zte.umap.user.model.UusrBasic02"));
	}
	else if (bean == "uusrBasic03Facade")	{
		obj.options.add(new Option("用户03实体", "com.zte.umap.user.model.UusrBasic03"));
	}
	else if (bean == "uusrBasic04Facade")	{
		obj.options.add(new Option("用户04实体", "com.zte.umap.user.model.UusrBasic04"));
	}
	else if (bean == "uusrBasic05Facade")	{
		obj.options.add(new Option("用户05实体", "com.zte.umap.user.model.UusrBasic05"));
	}
	else if (bean == "uusrBasic06Facade")	{
		obj.options.add(new Option("用户06实体", "com.zte.umap.user.model.UusrBasic06"));
	}
	else if (bean == "uusrBasic07Facade")	{
		obj.options.add(new Option("用户07实体", "com.zte.umap.user.model.UusrBasic07"));
	}
	else if (bean == "uusrBasic08Facade")	{
		obj.options.add(new Option("用户08实体", "com.zte.umap.user.model.UusrBasic08"));
	}
	else if (bean == "uusrBasic09Facade")	{
		obj.options.add(new Option("用户09实体", "com.zte.umap.user.model.UusrBasic09"));
	}
	else if (bean == "uusrInfo00Facade")	{
		obj.options.add(new Option("客户00实体", "com.zte.umap.user.model.UusrInfo00"));
	}
	else if (bean == "uusrInfo01Facade")	{
		obj.options.add(new Option("客户01实体", "com.zte.umap.user.model.UusrInfo01"));
	}
	else if (bean == "uusrInfo02Facade")	{
		obj.options.add(new Option("客户02实体", "com.zte.umap.user.model.UusrInfo02"));
	}
	else if (bean == "uusrInfo03Facade")	{
		obj.options.add(new Option("客户03实体", "com.zte.umap.user.model.UusrInfo03"));
	}
	else if (bean == "uusrInfo04Facade")	{
		obj.options.add(new Option("客户04实体", "com.zte.umap.user.model.UusrInfo04"));
	}
	else if (bean == "uusrInfo05Facade")	{
		obj.options.add(new Option("客户05实体", "com.zte.umap.user.model.UusrInfo05"));
	}
	else if (bean == "uusrInfo06Facade")	{
		obj.options.add(new Option("客户06实体", "com.zte.umap.user.model.UusrInfo06"));
	}
	else if (bean == "uusrInfo07Facade")	{
		obj.options.add(new Option("客户07实体", "com.zte.umap.user.model.UusrInfo07"));
	}
	else if (bean == "uusrInfo08Facade")	{
		obj.options.add(new Option("客户08实体", "com.zte.umap.user.model.UusrInfo08"));
	}
	else if (bean == "uusrInfo09Facade")	{
		obj.options.add(new Option("客户09实体", "com.zte.umap.user.model.UusrInfo09"));
	}

	else if (bean == "usubscription00Facade")	{
		obj.options.add(new Option("订购产品00实体", "com.zte.umap.order.model.Usubscription00"));
	}
	else if (bean == "usubscription01Facade")	{
		obj.options.add(new Option("订购产品01实体", "com.zte.umap.order.model.Usubscription01"));
	}
	else if (bean == "usubscription02Facade")	{
		obj.options.add(new Option("订购产品02实体", "com.zte.umap.order.model.Usubscription02"));
	}
	else if (bean == "usubscription03Facade")	{
		obj.options.add(new Option("订购产品03实体", "com.zte.umap.order.model.Usubscription03"));
	}
	else if (bean == "usubscription04Facade")	{
		obj.options.add(new Option("订购产品04实体", "com.zte.umap.order.model.Usubscription04"));
	}
	else if (bean == "usubscription05Facade")	{
		obj.options.add(new Option("订购产品05实体", "com.zte.umap.order.model.Usubscription05"));
	}
	else if (bean == "usubscription06Facade")	{
		obj.options.add(new Option("订购产品06实体", "com.zte.umap.order.model.Usubscription06"));
	}
	else if (bean == "usubscription07Facade")	{
		obj.options.add(new Option("订购产品07实体", "com.zte.umap.order.model.Usubscription07"));
	}
	else if (bean == "usubscription08Facade")	{
		obj.options.add(new Option("订购产品08实体", "com.zte.umap.order.model.Usubscription08"));
	}
	else if (bean == "usubscription09Facade")	{
		obj.options.add(new Option("订购产品09实体", "com.zte.umap.order.model.Usubscription09"));
	}
	else if (bean == "usubscriptionHis01Facade")	{
		obj.options.add(new Option("订购产品历史01实体", "com.zte.umap.order.model.UsubscriptionHis01"));
	}
	else if (bean == "usubscriptionHis02Facade")	{
		obj.options.add(new Option("订购产品历史02实体", "com.zte.umap.order.model.UsubscriptionHis02"));
	}
	else if (bean == "usubscriptionHis03Facade")	{
		obj.options.add(new Option("订购产品历史03实体", "com.zte.umap.order.model.UsubscriptionHis03"));
	}
	else if (bean == "usubscriptionHis04Facade")	{
		obj.options.add(new Option("订购产品历史04实体", "com.zte.umap.order.model.UsubscriptionHis04"));
	}
	else if (bean == "usubscriptionHis05Facade")	{
		obj.options.add(new Option("订购产品历史05实体", "com.zte.umap.order.model.UsubscriptionHis05"));
	}
	else if (bean == "usubscriptionHis06Facade")	{
		obj.options.add(new Option("订购产品历史06实体", "com.zte.umap.order.model.UsubscriptionHis06"));
	}
	else if (bean == "usubscriptionHis07Facade")	{
		obj.options.add(new Option("订购产品历史07实体", "com.zte.umap.order.model.UsubscriptionHis07"));
	}
	else if (bean == "usubscriptionHis08Facade")	{
		obj.options.add(new Option("订购产品历史08实体", "com.zte.umap.order.model.UsubscriptionHis08"));
	}
	else if (bean == "usubscriptionHis09Facade")	{
		obj.options.add(new Option("订购产品历史09实体", "com.zte.umap.order.model.UsubscriptionHis09"));
	}
	else if (bean == "usubscriptionHis10Facade")	{
		obj.options.add(new Option("订购产品历史10实体", "com.zte.umap.order.model.UsubscriptionHis10"));
	}
	else if (bean == "usubscriptionHis11Facade")	{
		obj.options.add(new Option("订购产品历史11实体", "com.zte.umap.order.model.UsubscriptionHis11"));
	}
	else if (bean == "usubscriptionHis12Facade")	{
		obj.options.add(new Option("订购产品历史12实体", "com.zte.umap.order.model.UsubscriptionHis12"));
	}

	// 内容
	else if (bean == "ucmCntActorFacade")	{
		obj.options.add(new Option("内容人物信息", "com.zte.umap.ucms.model.UcmCntActor"));
	}
	else if (bean == "ucmCntActorBufFacade") {
		obj.options.add(new Option("内容人物信息缓存", "com.zte.umap.ucms.model.UcmCntActorBuf"));
	}
	else if (bean == "ucmCntActorMapFacade")	{
		obj.options.add(new Option("内容和人物关系", "com.zte.umap.ucms.model.UcmCntActorMap"));
	}
	else if (bean == "ucmCntActorMapBufFacade")	{
		obj.options.add(new Option("内容和人物关系缓存", "com.zte.umap.ucms.model.UcmCntActorMapBuf"));
	}
	else if (bean == "ucmCntActorMapWkfwhisFacade")	{
		obj.options.add(new Option("内容和人物关系缓存历史表", "com.zte.umap.ucms.model.UcmCntActorMapWkfwhis"));
	}
	else if (bean == "ucmCntCpDirectoryFacade")	{
		obj.options.add(new Option("内容目录信息", "com.zte.umap.ucms.model.UcmCntCpDirectory"));
	}
	else if (bean == "ucmServerFacade")	{
		obj.options.add(new Option("内容服务器信息", "com.zte.umap.ucms.model.UcmServer"));
	}
	else if (bean == "ucmRightsRuleFacade")	{
		obj.options.add(new Option("内容版权规则", "com.zte.umap.ucms.model.UcmRightsRule"));
	}
	else if (bean == "ucmRightsActionFacade")	{
		obj.options.add(new Option("内容版权操作", "com.zte.umap.ucms.model.UcmRightsAction"));
	}
	else if (bean == "ucmTempspaceFacade")	{
		obj.options.add(new Option("临时空间信息", "com.zte.umap.ucms.model.UcmTempspace"));
	}
	else if (bean == "ucmTempspaceBufFacade")	{
		obj.options.add(new Option("临时空间缓存", "com.zte.umap.ucms.model.UcmTempspaceBuf"));
	}
	else if (bean == "ucmTempspaceWkfwhisFacade")	{
		obj.options.add(new Option("临时空间历史", "com.zte.umap.ucms.model.UcmTempspaceWkfwhis"));
	}
	else if (bean == "ucmAudioinfoFacade")	{
		obj.options.add(new Option("音频内容信息", "com.zte.umap.ucms.model.UcmAudioinfo"));
	}
	else if (bean == "ucmCntAppablMapFacade")	{
		obj.options.add(new Option("内容和业务能力关联", "com.zte.umap.ucms.model.UcmCntAppablMap"));
	}
	else if (bean == "ucmCntCatalogFacade")	{
		obj.options.add(new Option("内容类型的层次关系", "com.zte.umap.ucms.model.UcmCntCatalog"));
	}
	else if (bean == "ucmCntCatalogMapFacade")	{
		obj.options.add(new Option("内容与分类map关系", "com.zte.umap.ucms.model.UcmCntCatalogMap"));
	}
	else if (bean == "ucmCntFileMapFacade")	{
		obj.options.add(new Option("内容与实体文件关联", "com.zte.umap.ucms.model.UcmCntFileMap"));
	}
	else if (bean == "ucmCntFileMapBufFacade")	{
		obj.options.add(new Option("内容与实体文件关联缓存", "com.zte.umap.ucms.model.UcmCntFileMapBuf"));
	}
	else if (bean == "ucmCntFileMapWkfwhisFacade")	{
		obj.options.add(new Option("内容与实体文件关联历史", "com.zte.umap.ucms.model.UcmCntFileMapWkfwhis"));
	}
	else if (bean == "ucmCntUsetypeFacade")	{
		obj.options.add(new Option("内容使用类型", "com.zte.umap.ucms.model.UcmCntUsetype"));
	}
	else if (bean == "ucmContentFacade")	{
		obj.options.add(new Option("内容基本信息", "com.zte.umap.ucms.model.UcmContent"));
	}
	else if (bean == "ucmContentBufFacade")	{
		obj.options.add(new Option("内容缓存", "com.zte.umap.ucms.model.UcmContentBuf"));
	}
	else if (bean == "ucmContentWkfwhisFacade")	{
		obj.options.add(new Option("内容工作流历史", "com.zte.umap.ucms.model.UcmContentWkfwhis"));
	}
	else if (bean == "ucmContentKalaokFacade")	{
		obj.options.add(new Option("卡拉OK内容", "com.zte.umap.ucms.model.UcmContentKalaok"));
	}
	else if (bean == "ucmContentKalaokBufFacade")	{
		obj.options.add(new Option("卡拉OK内容缓存", "com.zte.umap.ucms.model.UcmContentKalaokBuf"));
	}
	else if (bean == "ucmContentKalaokWkfwhisFacade")	{
		obj.options.add(new Option("卡拉OK内容工作流历史表", "com.zte.umap.ucms.model.UcmContentKalaokWkfwhis"));
	}
	else if (bean == "ucmCountryFacade")	{
		obj.options.add(new Option("内容所属地区编码", "com.zte.umap.ucms.model.UcmCountry"));
	}
	else if (bean == "ucmFileFacade")	{
		obj.options.add(new Option("文件基本信息", "com.zte.umap.ucms.model.UcmFile"));
	}
	else if (bean == "ucmFileAudioinfoFacade")	{
		obj.options.add(new Option("音频类型子内容信息", "com.zte.umap.ucms.model.UcmFileAudioinfo"));
	}
	else if (bean == "ucmFileBufFacade")	{
		obj.options.add(new Option("文件缓存", "com.zte.umap.ucms.model.UcmFileBuf"));
	}
	else if (bean == "ucmFileWkfwhisFacade")	{
		obj.options.add(new Option("文件工作流历史", "com.zte.umap.ucms.model.UcmFileWkfwhis"));
	}
	else if (bean == "ucmFileFormatFacade")	{
		obj.options.add(new Option("系统支持的内容文件类型的后缀", "com.zte.umap.ucms.model.UcmFileFormat"));
	}
	else if (bean == "ucmFileMimetypeFacade")	{
		obj.options.add(new Option("内容mimetype信息", "com.zte.umap.ucms.model.UcmFileMimetype"));
	}
	else if (bean == "ucmFilePicinfoFacade")	{
		obj.options.add(new Option("图片类型子内容信息", "com.zte.umap.ucms.model.UcmFilePicinfo"));
	}
	else if (bean == "ucmFileTerminalmapFacade")	{
		obj.options.add(new Option("内容和终端绑定关系", "com.zte.umap.ucms.model.UcmFileTerminalmap"));
	}
	else if (bean == "ucmFileVideoinfoFacade")	{
		obj.options.add(new Option("视频类型子内容信息", "com.zte.umap.ucms.model.UcmFileVideoinfo"));
	}
	else if (bean == "ucmFileWkfwhisFacade")	{
		obj.options.add(new Option("文件工作流历史表", "com.zte.umap.ucms.model.UcmFileWkfwhis"));
	}
	else if (bean == "ucmPreviewfileFacade")	{
		obj.options.add(new Option("版权规则表", "com.zte.umap.ucms.model.UcmPreviewfile"));
	}
	else if (bean == "ucmVideoinfoFacade")	{
		obj.options.add(new Option("视频类型内容信息", "com.zte.umap.ucms.model.UcmVideoinfo"));
	}
	else if (bean == "ucmFtptaskFacade")	{
		obj.options.add(new Option("同步任务信息实体", "com.zte.umap.ucms.upload.model.UcmFtptask"));
	}
	else if (bean == "ucmFtptaskHisFacade")	{
		obj.options.add(new Option("同步任务历史信息实体", "com.zte.umap.ucms.upload.model.UcmFtptaskHis"));
	}
	
		else if (bean == "usysTransactionnoFacade")	{
		obj.options.add(new Option("流水号信息", "com.zte.umap.common.sync.model.UsysTransactionno"));
	}
else if (bean == "usyncInfoDevMapFacade")	{
		obj.options.add(new Option("信息类型和设备信息", "com.zte.umap.common.sync.model.UsyncInfoDevMap"));
	}
else if (bean == "usyncTaskResultFacade")	{
		obj.options.add(new Option("同步任务历史信息", "com.zte.umap.common.sync.model.UsyncTaskResult"));
	}
else if (bean == "usyncTaskFacade")	{
		obj.options.add(new Option("同步任务基本信息", "com.zte.umap.common.sync.model.UsyncTask"));
	}
}
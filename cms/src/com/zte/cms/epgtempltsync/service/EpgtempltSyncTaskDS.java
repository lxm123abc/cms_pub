package com.zte.cms.epgtempltsync.service;

import java.util.List;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.cms.epgtempltsync.dao.IEpgtempltSyncTaskDAO;
import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class EpgtempltSyncTaskDS extends DynamicObjectBaseDS implements IEpgtempltSyncTaskDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IEpgtempltSyncTaskDAO dao = null;

    public void setDao(IEpgtempltSyncTaskDAO dao)
    {
        this.dao = dao;
    }

    public void insertEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException
    {
        log.debug("insert epgtempltSyncTask starting...");
        try
        {
            Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_epgtempltsync_seq");
            epgtempltSyncTask.setTaskindex(id);
            // Integer intvalue = Integer.parseInt(id.toString());
            int length = id.toString().length();
            StringBuffer correlateid = new StringBuffer();
            // String makeup = "";
            if (length < 20)
            {
                for (int i = 0; correlateid.length() + length < 20; i++)
                {
                    correlateid = correlateid.append("0");
                }
            }
            correlateid = correlateid.append(id);
            if (correlateid != null)
            {
                epgtempltSyncTask.setCorrelateid(correlateid.toString());
            }
            dao.insertEpgtempltSyncTask(epgtempltSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert epgtempltSyncTask end");
    }

    public void updateEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException
    {
        log.debug("update epgtempltSyncTask by pk starting...");
        try
        {

            int length = epgtempltSyncTask.getTaskindex().toString().length();
            StringBuffer correlateid = new StringBuffer();

            if (length < 20)
            {
                for (int i = 0; correlateid.length() + length < 20; i++)
                {
                    correlateid = correlateid.append("0");
                }
            }
            correlateid = correlateid.append(epgtempltSyncTask.getTaskindex());
            if (correlateid != null)
            {
                epgtempltSyncTask.setCorrelateid(correlateid.toString());
            }

            dao.updateEpgtempltSyncTask(epgtempltSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update epgtempltSyncTask by pk end");
    }

    public String checkExist(Long templateindex) throws DomainServiceException
    {
        String result = "0";
        EpgtempltSyncTask epgtemplt = new EpgtempltSyncTask();
        epgtemplt.setTaskindex(templateindex);
        List<EpgtempltSyncTask> list = getEpgtempltSyncTaskByCond(epgtemplt);
        if (list.size() < 1)
        {
            result = "1";
        }
        return result;
    }

    public void updateEpgtempltSyncTaskList(List<EpgtempltSyncTask> epgtempltSyncTaskList)
            throws DomainServiceException
    {
        log.debug("update epgtempltSyncTaskList by pk starting...");
        if (null == epgtempltSyncTaskList || epgtempltSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in epgtempltSyncTaskList");
            return;
        }
        try
        {
            for (EpgtempltSyncTask epgtempltSyncTask : epgtempltSyncTaskList)
            {
                dao.updateEpgtempltSyncTask(epgtempltSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update epgtempltSyncTaskList by pk end");
    }

    public void removeEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException
    {
        log.debug("remove epgtempltSyncTask by pk starting...");
        try
        {
            dao.deleteEpgtempltSyncTask(epgtempltSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove epgtempltSyncTask by pk end");
    }

    public void removeEpgtempltSyncTaskList(List<EpgtempltSyncTask> epgtempltSyncTaskList)
            throws DomainServiceException
    {
        log.debug("remove epgtempltSyncTaskList by pk starting...");
        if (null == epgtempltSyncTaskList || epgtempltSyncTaskList.size() == 0)
        {
            log.debug("there is no datas in epgtempltSyncTaskList");
            return;
        }
        try
        {
            for (EpgtempltSyncTask epgtempltSyncTask : epgtempltSyncTaskList)
            {
                dao.deleteEpgtempltSyncTask(epgtempltSyncTask);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove epgtempltSyncTaskList by pk end");
    }

    public EpgtempltSyncTask getEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException
    {
        log.debug("get epgtempltSyncTask by pk starting...");
        EpgtempltSyncTask rsObj = null;
        try
        {
            rsObj = dao.getEpgtempltSyncTask(epgtempltSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get epgtempltSyncTaskList by pk end");
        return rsObj;
    }

    public List<EpgtempltSyncTask> getEpgtempltSyncTaskByCond(EpgtempltSyncTask epgtempltSyncTask)
            throws DomainServiceException
    {
        log.debug("get epgtempltSyncTask by condition starting...");
        List<EpgtempltSyncTask> rsList = null;
        try
        {
            rsList = dao.getEpgtempltSyncTaskByCond(epgtempltSyncTask);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get epgtempltSyncTask by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get epgtempltSyncTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            if (epgtempltSyncTask.getStarttime() != null && !"".equals(epgtempltSyncTask.getStarttime()))
            {
                String starttime1 = epgtempltSyncTask.getStarttime().substring(0, 4)
                        + epgtempltSyncTask.getStarttime().substring(5, 7)
                        + epgtempltSyncTask.getStarttime().substring(8, 10)
                        + epgtempltSyncTask.getStarttime().substring(11, 13)
                        + epgtempltSyncTask.getStarttime().substring(14, 16)
                        + epgtempltSyncTask.getStarttime().substring(17, 19);
                epgtempltSyncTask.setStarttime(starttime1);
            }
            if (epgtempltSyncTask.getEndtime()!= null && !"".equals(epgtempltSyncTask.getEndtime()))
            {
                String expiredtime1 = epgtempltSyncTask.getEndtime().substring(0, 4)
                        + epgtempltSyncTask.getEndtime().substring(5, 7)
                        + epgtempltSyncTask.getEndtime().substring(8, 10)
                        + epgtempltSyncTask.getEndtime().substring(11, 13)
                        + epgtempltSyncTask.getEndtime().substring(14, 16)
                        + epgtempltSyncTask.getEndtime().substring(17, 19);
                epgtempltSyncTask.setEndtime(expiredtime1);
            }
            pageInfo = dao.pageInfoQuery(epgtempltSyncTask, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<EpgtempltSyncTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get epgtempltSyncTask page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get epgtempltSyncTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(epgtempltSyncTask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<EpgtempltSyncTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get epgtempltSyncTask page info by condition end");
        return tableInfo;
    }

    public TableDataInfo pageInfoQueryUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug("get upstreamEPGTask page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryUpstreamEPGTask(upstreamEPGTask, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<UpstreamEPGTask>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get upstreamEPGTask page info by condition end");
        return tableInfo;
    }

    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail(UpstreamEPGTaskDetail rsObj) throws DomainServiceException
    {
        log.debug("get UpstreamEPGTask by pk starting...");
        try
        {
            rsObj = dao.getUpstreamEPGTaskDetail(rsObj);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get UpstreamEPGTask by pk end");
        return rsObj;
    }
}

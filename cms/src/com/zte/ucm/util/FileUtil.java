package com.zte.ucm.util;

import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class FileUtil
{
  private static Logger log = LoggerFactory.getLogger(FileUtil.class);

  public static boolean isXml(String paramString)
  {
    return (paramString != null) && (paramString.endsWith(".xml"));
  }

  public boolean copyFile(String paramString1, String paramString2)
    throws Exception
  {
	boolean i = false;
    try
    {
      int j = 0;
      int k = 0;
      File localFile = new File(paramString1);
      if (localFile.exists())
      {
        FileInputStream localFileInputStream = new FileInputStream(paramString1);
        FileOutputStream localFileOutputStream = new FileOutputStream(paramString2);
        byte[] arrayOfByte = new byte[1444];
        while ((k = localFileInputStream.read(arrayOfByte)) != -1)
        {
          j += k;
          localFileOutputStream.write(arrayOfByte, 0, k);
        }
        i = true;
        localFileOutputStream.close();
        localFileInputStream.close();
      }
    }
    catch (Exception localException)
    {
      log.error(localException);
      throw localException;
    }
    return true;
  }
}

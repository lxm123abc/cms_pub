#!/bin/bash
## Usage:sh /home/UCM/IAG/shell/update_config.sh -filename "/home/UCM/IAG/configdeliver/00320111118123401/config_apply_items.xml"
CONFIGWORKDIR=/home/UCM/CMS_WEB/shell/
CONFIGITEMSFILE=/home/UCM/CMS_WEB/shell/config_apply_items.xml
LOGPATH=/home/UCM/CMS_WEB/shell/log
ERRORFILENAME=/home/UCM/CMS_WEB/shell/update_config.log

ZXJAR=/home/zxin10/was/tomcat/shared/lib
JAVA_HOME=/usr/java/java
JAVABIN=${JAVA_HOME}/bin

LOGFILE=log_`date +%Y%m%d`.log
getTimeStamp()
{
  echo `date +"%Y-%m-%d %H:%M:%S"`
}
log()
{
  if [ ! -d ${LOGPATH} ]; then
  	  mkdir -p ${LOGPATH}
  fi;
  echo "@@@ $1"
  echo "`getTimeStamp` : @@@ $1" >> ${LOGPATH}/${LOGFILE}
}

function changeCBT()
{
   SYSINFO=/home/zxin10/was/tomcat/shared/classes/sysinfo.ini
   CLASS=/home/zxin10/was/tomcat/webapps/iptvslcs/WEB-INF/classes
   
   DISLANG=`cat ${SYSINFO} | grep DISLANG |awk -F= '{print $2}' | tr -d " "`
   log "DISLANG=${DISLANG}"
   #0:COMMON 21:CBT
   AREA_VERSION=`cat ${CLASS}/cmsInfo.xml | grep "slpex.area_version" |awk -F\" '{print $4}' |tr -d " "`
   log "AREA_VERSION=${AREA_VERSION}"
   
   #backup 
   cp -rf ${CLASS}/ApplicationResources_cbt.properties  ${CLASS}/ApplicationResources_cbt.properties.bak
   cp -rf ${CLASS}/resource_cbt.properties  ${CLASS}/resource_cbt.properties.bak
   if [ ${AREA_VERSION} -eq 21 ] 
   then
      log  " cp -rf ${CLASS}/ApplicationResources_cbt.properties.bak  ${CLASS}/ApplicationResources.properties"
      cp -rf ${CLASS}/ApplicationResources_cbt.properties.bak  ${CLASS}/ApplicationResources.properties
	  log " cp -rf ${CLASS}/resource_cbt.properties.bak  ${CLASS}/resource.properties"
      cp -rf ${CLASS}/resource_cbt.properties.bak  ${CLASS}/resource.properties
   else
      if [ "${DISLANG}" = "CN" ]
	  then 
	    log "cp -rf  ${CLASS}/ApplicationResources.properties.bak  ${CLASS}/ApplicationResources.properties"
	    cp -rf  ${CLASS}/ApplicationResources.properties.bak  ${CLASS}/ApplicationResources.properties
		log "cp -rf ${CLASS}/resource.properties.bak   ${CLASS}/resource.properties"
		cp -rf ${CLASS}/resource.properties.bak   ${CLASS}/resource.properties
	  else
	     log "cp -rf  ${CLASS}/ApplicationResources_en.properties.bak  ${CLASS}/ApplicationResources.properties"
	     cp -rf  ${CLASS}/ApplicationResources_en.properties.bak  ${CLASS}/ApplicationResources.properties
		 log "cp -rf ${CLASS}/resource_en.properties.bak   ${CLASS}/resource.properties"
		 cp -rf ${CLASS}/resource_en.properties.bak   ${CLASS}/resource.properties
	  fi	  
   fi   
}
if [ "$1" == "-filename" ]; then
    CONFIGITEMSFILE=$2
fi; 

if [ ! -d ${CONFIGWORKDIR} ]; then
	log "CONFIGWORKDIR is not exist:${CONFIGWORKDIR}"
	exit 1;
fi;

if [ ! -f ${CONFIGITEMSFILE} ]; then 
	log "CONFIGITEMSFILE is not exist:${CONFIGITEMSFILE}"
	exit 1;
fi;

log "===== START UPDATING CONFIG ====="

dom4jjar=`ls ${ZXJAR}/dom4j-1*.jar`
jaxenjar=`ls ${ZXJAR}/jaxen*.jar`

count=`ls ${CONFIGWORKDIR}*.jar|grep "dom4j-1"|wc -l`
count1=`ls ${CONFIGWORKDIR}*.jar|grep "jaxen"|wc -l`

workdirdom4jjar=`ls ${CONFIGWORKDIR}dom4j-1*.jar > /dev/null  2>&1`
workdirjaxenjar=`ls ${CONFIGWORKDIR}jaxen*.jar > /dev/null  2>&1`

chattr -i /etc/init.d/tomcat > /dev/null  2>&1
  
if [ $count -eq 0 -o $count1 -eq 0 ]; then
    ##log "===== execute with dom4j and jaxen in zxjos ====="
	count=`${JAVABIN}/java -jar -Xbootclasspath/a:${dom4jjar}:${jaxenjar} ${CONFIGWORKDIR}/cmsucm.jar 0 ${CONFIGWORKDIR} ${CONFIGITEMSFILE} ${LOGPATH} ${ERRORFILENAME}|grep "*#06#"|wc -l`
else
    ##log "===== execute with dom4j and jaxen in workdir ====="
	count=`${JAVABIN}/java -jar ${CONFIGWORKDIR}/cmsucm.jar 0 ${CONFIGWORKDIR} ${CONFIGITEMSFILE} ${LOGPATH} ${ERRORFILENAME}|grep "*#06#"|wc -l`
fi;

if [[ $count -eq 0 ]]; then
	log "===== CONFIG UPDATED FAILURE ====="
	exit 1;
else    
#   changeCBT
#	wget "http://127.0.0.1:8081/slcshttp/resourcematch"
#	wget "http://127.0.0.1:8080/iptvslcs/initcmsinfo" > /dev/null  2>&1
#	rm -rf ${CONFIGWORKDIR}initcmsinfo*	
#	rm -rf {CONFIGWORKDIR}resourcematch*
    log "===== CONFIG UPDATED SUCCESSFUL ====="
	exit 0;
fi;

package com.zte.cms.content.batchUpload.service;

import java.util.List;
import com.zte.cms.content.batchUpload.model.IcmFileinfoImport;
import com.zte.cms.content.batchUpload.dao.IIcmFileinfoImportDAO;
import com.zte.cms.content.batchUpload.service.IIcmFileinfoImportDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmFileinfoImportDS implements IIcmFileinfoImportDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmFileinfoImportDAO dao = null;

    public void setDao(IIcmFileinfoImportDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmFileinfoImport(IcmFileinfoImport icmFileinfoImport) throws DomainServiceException
    {
        log.debug("insert icmFileinfoImport starting...");
        try
        {
            dao.insertIcmFileinfoImport(icmFileinfoImport);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmFileinfoImport end");
    }

    public List<IcmFileinfoImport> getIcmFileinfoImportByCond(IcmFileinfoImport icmFileinfoImport)
            throws DomainServiceException
    {
        log.debug("get icmFileinfoImport by condition starting...");
        List<IcmFileinfoImport> rsList = null;
        try
        {
            rsList = dao.getIcmFileinfoImportByCond(icmFileinfoImport);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmFileinfoImport by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get icmFileinfoImport page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmFileinfoImport, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmFileinfoImport>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmFileinfoImport page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get icmFileinfoImport page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmFileinfoImport, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmFileinfoImport>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmFileinfoImport page info by condition end");
        return tableInfo;
    }
}

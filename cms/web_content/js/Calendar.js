// @version 1.1.4
/**
   * \u4e3a\u65e5\u5386\u63a7\u4ef6\u8d4b\u503c,\u540e\u53f0\u4f20\u56de\u6765\u4e00\u4e2aUTC\u65f6\u95f4\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param {Object} targetid
   * @param value UTC\u65f6\u95f4
   */  
  function setCalendarValue(id, value)
  {
  	if(typeof value == 'number')
  	{
	  	var obj = document.getElementById(id);
		var targetId = obj.getAttribute("target");
		var obj1 = document.getElementById(targetId);
        var obj_two = document.getElementById(targetId+"_"+id);

	    var format = canlendarObj.formate;	
	    var time = UTC2Date(format, value);
	    obj1.value = time;
		if(obj.getAttribute('isFullDate') == "false")
		{
			arr = time.split(" ");
			obj_two.value = arr[1];
		}else{
			obj_two.value = time;
		}
  	}  else {   
  	// add by Daniel  The Calendar has an attribute like datatype.
  	    var obj = document.getElementById(id);
		var targetId = obj.getAttribute("target");
		var obj1 = document.getElementById(targetId);
        var obj_two = document.getElementById(targetId+"_"+id);
	   
		var tmpValue = "" ;
		if ( value == null || value ==undefined  )
		{
			tmpValue = "" ;
		} else {
			tmpValue = value ;
		}

	    obj1.value = tmpValue;

		if(obj.getAttribute('isFullDate') == "false")
		{
			arr = value.split(" ");
			obj_two.value = arr[1];
		}else{
			obj_two.value = tmpValue;
		}
	}
  };
 

function RIACalendarFactory(UTCTime)
{
	//debugger;
	this.utctime = UTCTime;
	this.fullDate;
	
	this.year;
	this.month;
	this.date;
	this.hours;
	this.minutes;
	this.seconds;
	this.milliseconds;
	this.timezoneOffset;
	this.c = ":";
	
	this.init();	
};
RIACalendarFactory.prototype.init = function()
{
	if(this.utctime == "" || this.utctime == null)
	this.fullDate = new Date();
		
	else this.fullDate = new Date(this.utctime);

	this.year = this.fullDate.getFullYear();

	this.month = this.fullDate.getMonth()+1;

	this.date= this.fullDate.getDate(); 

	this.hours = this.fullDate.getHours();

	this.minutes = this.fullDate.getMinutes();

	this.seconds = this.fullDate.getSeconds();

	this.milliseconds = this.fullDate.getMilliseconds();

	this.timezoneOffset = new Date().getTimezoneOffset()/60;		

};
RIACalendarFactory.prototype.getHHmmss = function()
{
	var HH_MM_SS = this.hours+this.c+this.minutes+this.c+this.seconds;
	return HH_MM_SS;
};
RIACalendarFactory.prototype.getMilliseconds = function()
{
	return this.milliseconds;
};
RIACalendarFactory.prototype.getFullYear = function()
{
	return this.year;
};
RIACalendarFactory.prototype.getMonth = function()
{
	return this.month;
};
RIACalendarFactory.prototype.getDate = function()
{
	return this.date;
};
RIACalendarFactory.prototype.getZoneOffset = function()
{
	return this.timezoneOffset;
};
 /**
   * \u53d6\u65e5\u5386\u63a7\u4ef6\u503c\uff0c\u8f6c\u6362\u6210UTC\u65f6\u95f4\u56de\u4f20\u5230\u540e\u53f0
   * @author \u5f20\u5b8f\u4f1f
   * @param {Object} targetid
   */   	
  function getCalendarValue(id)
  {
    var obj = document.getElementById(id);
    var targetId = obj.getAttribute("target");
	  var obj1 = document.getElementById(targetId);
    var time = obj1.value;
    var format = canlendarObj.formate;
    return Date2UTC(format, time);
  };
  
   var canlendarObj = new Object();//\u63a7\u4ef6\u7684\u6a21\u578b\u6570\u7ec4

  UTC2Date = function(format, utcTime)
  {
  	if(utcTime == null || utcTime == '')
  	{
  		return '';
  	}
	
	var nd = new Date(utcTime);
	var yy = "-";
	var mm = "-";
	var _HH_MM_SS = "";

	if(format != null)
	{
		format = format.toLowerCase();
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		yy = format.substring(y_endindex+1,y_endindex+2);
	   
	    var m_index = format.indexOf("m");		
		mm = format.substring(m_index+2, m_index+3);
		var hour = new String(nd.getHours()).length > 1? nd.getHours():"0"+nd.getHours();
		var minutes = new String(nd.getMinutes()).length > 1? nd.getMinutes():"0"+nd.getMinutes();
		var seconds = new String(nd.getSeconds()).length > 1? nd.getSeconds():"0"+nd.getSeconds();
		
		if(format.indexOf("hh") != -1)
		{
			_HH_MM_SS = _HH_MM_SS + " " + hour;			
		}
		
		if(format.lastIndexOf("mm") != m_index)
		{
			if(_HH_MM_SS == "")
			  _HH_MM_SS = _HH_MM_SS + " " + minutes;
			else 
			  _HH_MM_SS = _HH_MM_SS + ":" + minutes;
		}
		
		if(format.indexOf("ss") != -1)
		{
			if(_HH_MM_SS == "")
			  _HH_MM_SS = _HH_MM_SS + " " + seconds;
			else 
			  _HH_MM_SS = _HH_MM_SS + ":" + seconds;			
		}
	}
    var month = nd.getMonth()+1+"";
    if(month.length == 1)
    {
    	month = "0" + month; 	
    }
    var date  = nd.getDate()+"";
    if(date.length == 1)
    {
    	date  = "0" + date;
    }
    
	var time = nd.getFullYear() + yy + month + mm + date + _HH_MM_SS;
	
	return time;
  };
  
  /***************************************
   * \u5c06\u65e5\u671f\u683c\u5f0f\u7684\u6570\u636e\u8f6c\u6362\u6210UTC\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
   * @param Time \u8981\u8f6c\u6362\u6210UTC\u65f6\u95f4\u7684\u65e5\u671f
   ***************************************/
  
  Date2UTC = function(format, time)
  {
  	if(time == null || time == '')
  	{
  		return null;
  	}
  	var utc = "";
	var yy = "-";
	var mm = "-";
	var date_format = new Array();
	var date_format1 = "";
	var date_format2 = "";
	var time_arr = new Array();
	var time1 = time;
	var time2 = "";
	if(format != null)
	{
		if(format.length > 10)
		{
		    date_format = format.split(" ");
		    date_format1 = date_format[0];
		    date_format2 = date_format[1];
		    
		    time_arr = time.split(" ");
		    time1 = time_arr[0];
		    time2 = time_arr[1];
		}else{
			date_format1 = format;
			time1 = time;
		}
		var y_index = date_format1.indexOf("y");
		var y_endindex = date_format1.lastIndexOf("y");
		yy = date_format1.substring(y_endindex+1,y_endindex+2);	

	    var m_index = date_format1.indexOf("m");
		var m_endindex = date_format1.lastIndexOf("m");
		mm = date_format1.substring(m_endindex+1, m_endindex+2);
	}
	var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
	var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);

	time1 = time1.replace(YM,',');
	time1 = time1.replace(MD,',');
    var tt = time1.split(",");
  
    if(time2.length < 1){
    	var date = new Date(tt[0],parseInt(tt[1]-1),tt[2]);
    	utc = date.getTime();
    }else{
    	var HH = time2.split(":");
    	 var date = new Date(tt[0],parseInt(tt[1]-1),tt[2],HH[0],HH[1],HH[2]);
    	 utc = date.getTime();
    }
	return utc;
  };
/**
 * \u521d\u59cb\u5316\u65e5\u5386\u63a7\u4ef6
 * 
 * @copyright
 * @author \u5f20\u5b8f\u4f1f
 */
function Calendar_init()
{
    var tagNames ;
	
	if(isIE())
	{
		tagNames = document.getElementsByTagName('Calendar'); 
	}else{
		tagNames = document.getElementsByTagName('z:Calendar'); 
	}
   
   init_SSB_calendar(tagNames);
  
};

function init_SSB_calendar(tagNames)
{
     for(var i = 0; i < tagNames.length; i++)
     {
     	if(tagNames[i].parentNode.tagName.toLowerCase() == "z:column" || tagNames[i].parentNode.tagName.toLowerCase() == "column")
     	  continue;
         var date_formate = tagNames[i].getAttribute('formate'); //\u83b7\u53d6\u65e5\u671f\u683c\u5f0f
          //zzd \u8868\u683c\u4e2d\u7684\u65e5\u5386\u521d\u59cb\u5316\u65f6\u4e0d\u4f1a\u8c03\u7528\u8be5\u65b9\u6cd5, \u9ed8\u8ba4\u4e0d\u663e\u793a\u65f6\u5206\u79d2, \u4e3a\u4e86\u7edf\u4e00, \u8868\u683c\u5916\u7684\u65e5\u5386\u4e5f\u5e94\u5f53\u4e0d\u663e\u793a\u65f6\u5206\u79d2
         if (date_formate == null || date_formate =="") {
         	date_formate = "yyyy-mm-dd"
         } 
         var mid = 	tagNames[i].getAttribute('id');
         var imgurl = tagNames[i].getAttribute("imgurl");
         var _calendar_WIDTH = tagNames[i].getAttribute("width");
         var _calendar_size = "16";
         var _onchange_event = (tagNames[i].getAttribute("onchange")!=undefined)?tagNames[i].getAttribute("onchange"):"";
         
		if(date_formate != null && date_formate != "")
		{
			 canlendarObj.formate = date_formate; 
		}
		if(_calendar_WIDTH != null && _calendar_WIDTH != "")
		{
			_calendar_size = _calendar_WIDTH;
		} 
		var inputTarget = tagNames[i].getAttribute('target');	//\u83b7\u53d6\u5185\u5b58\u8f93\u5165\u6846\u7684ID	     
		 var inputText = (tagNames[i].getAttribute('value')!=undefined)?tagNames[i].getAttribute("value"):"";//\u83b7\u53d6\u9ed8\u8ba4\u503c
		var type1 = "text";
		var type2 = "hidden";
		 canlendarObj.isFullDate = tagNames[i].getAttribute('isFullDate');	
		 var inputTarget_show = inputTarget+"_"+mid;
		  var html = '<INPUT type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'" readonly="readonly" ';		
			html += ' value="'+inputText+'"  onchange="'+_onchange_event+'" size="'+_calendar_size+'">';
			html += '<INPUT type="'+type2+'" id="'+inputTarget+"_"+mid+'" readonly="readonly" ';
			html += ' value="'+inputText+'"  size="'+_calendar_size+'">&nbsp; ';
		 
		 if(canlendarObj.isFullDate == "false"){
		 	type1 = "hidden";
		 	type2 = "text";
		 	inputTarget_show = inputTarget+"_"+mid;
		 	    html = '<INPUT type="'+type2+'" id="'+inputTarget_show+'" readonly="readonly" ';
				html += ' value="'+inputText+'"  size="'+_calendar_size+'"> ';
		 	    html += '<INPUT type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'" readonly="readonly" ';		
				html += ' value="'+inputText+'"  onchange="'+_onchange_event+'" size="'+_calendar_size+'">&nbsp;';
				
		 }         
		if(imgurl == null || imgurl == "")
		{
			//zzd
		    var imgurl = 'images/calendar.gif';
		    var pagePath = location.pathname;
		    var directoryNum = pagePath.split("/").length - 3;
		    while (directoryNum > 0) {
		    	imgurl = '../' + imgurl;
		    	directoryNum --;
		    }
			//imgurl = '../images/calendar.gif';
		}
		//wanghao2
      	html+='<img  id="calendar_img_id" style="cursor:pointer;" border="0" src="'+imgurl+'" width="16" height="15"  onClick="fPopCalendar(\''+inputTarget+'\',\''+inputTarget+'\',\''+date_formate+'\',\''+inputTarget_show+'\');return false"> ';
       
     	 tagNames[i].innerHTML=html; 
        //add by daniel , add defalut value for Calenar Control like  Calenar Control of table 
     	var defaultFun = tagNames[i].getAttribute('default');
     	if ( defaultFun != null && defaultFun !=undefined  ) {
     	    var defaultValue = eval(defaultFun); 
     	    if ( defaultValue > 0 && date_formate != null && date_formate.length > 0 ) 
     	        document.getElementById(inputTarget).value = getTimefromUTC(date_formate,defaultValue);
     	    else      	 
     	        document.getElementById(inputTarget).value = "";
     	}
     }
};
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

var gdCtrl = new Object();
var gdCtrl_tow = new Object();
var goSelectTag = new Array();
var gcGray   = "#808080";
var gcToggle = "#FB8664";
var gcEmpty = '';
var gcBG = "#e5e6ec";
var previousObject = null;
var gdCurDate = new Date();
var giYear = gdCurDate.getFullYear();
var giMonth = gdCurDate.getMonth()+1;
var giDay = gdCurDate.getDate();
var curYear = gdCurDate.getFullYear();
var curMonth = gdCurDate.getMonth()+1;
var curDay = gdCurDate.getDate();
curMonth= (curMonth.toString().length==1)?'0'+curMonth.toString():curMonth.toString();
curDay= (curDay.toString().length==1)?'0'+curDay.toString():curDay.toString();
var giHh = gdCurDate.getHours();
var giMm = gdCurDate.getMinutes();
var giSs = gdCurDate.getSeconds();
var giTime = giHh+':'+giMm+':'+giSs;
var Hid = true;
/**
 * 
 * @param {Object} num
 */
function fCheckIsNumber(num)
{
	var i,j,strTemp;
	strTemp="0123456789";
	if (num.length==0)
	{
		return false;
	}
	for (i=0;i<num.length;i++)
	{
		j=strTemp.indexOf(num.charAt(i)); 
		if (j==-1)
		{
			return false;
		}
	}
	return true;
};
/**
 * 
 * @param {Object} str
 */
function LTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function RTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function Trim(str) 
{
	return RTrim(LTrim(str));
};
/**
 * 
 */
function fCheckDateTime()
{
	//var inputValue=document.all.dateStart.value;
	var inputValue = document.getElementById("dateStart").value;
	inputValue=Trim(inputValue);
	var len=inputValue.length;
	if (len!=10)
	{
		if(len!=16)
		{
			if (len!=19)
			{
				//alert('\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd\u683c\u5f0f\u8f93\u5165!');
				alert("'"+$res_entry('ui.control.calendar.dateFomatError')+"'");
				return;
			}
		}
	}
	var iDate= inputValue.substring(0,10);
	var iTime= inputValue.substring(10,len);
	if (fCheckDate(iDate)== false)
	{
		//alert('\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd\u683c\u5f0f\u8f93\u5165!');
		alert("'"+$res_entry('ui.control.calendar.dateFomatError')+"'");
		return;
	}
	if (len!=10)
	{
		if (fCheckTime(iTime)== false)
		{
			//alert('\u8f93\u5165\u65e5\u671f\u4e0d\u5408\u6cd5,\n\u8bf7\u6309yyyy-MM-dd hh:mm:ss\u683c\u5f0f\u8f93\u5165!');
			alert("'"+$res_entry('ui.control.calendar.fullDateFomatError')+"'");
			return;
		}
	}
};
/**
 * 
 * @param {Object} inputdate
 */
function fCheckDate(inputdate)
{
	inputdate=inputdate.split('-');
	var year=inputdate[0];
	var month=inputdate[1];
	var day=inputdate[2];
	var Date1 = new Date(year,month-1,day); 
	if (Date1.getMonth()+1!=month||Date1.getDate()!=day||Date1.getFullYear()!=year||year.length!=4)
	{
		return false;
	}
	else
	{
		if (month.indexOf('0')==0) 
		{
			month=month.substr(1,month.length); 
		}
		if (day.indexOf('0')==0) 
		{
			day=day.substr(1,day.length); 
		}
		if ((month==4 || month==6 || month==9 || month==11) && (day>30))
		{
			return false; 
		}
		if (month==2) 
		{
			if (LeapYear(year)) 
			{
				if (day>29 || day<1)
				{
					return false;
				}
			}
			else
			{
				if (day>28 || day<1)
				{
					return false; 
				}
			}
		}
	}
	return true;
};
function fCheckTime(inputTime)
{
	inputTime=inputTime.split(':')
	if(inputTime.length!=2)
	{
		if(inputTime.length!=3)
		{
			return false;
		}
	}
	var hh=Trim(inputTime[0])
	if (isNaN(hh))
	{
		return false;
	}
	if (hh.indexOf('0')==0) 
	{
		hh=hh.substr(1,hh.length); 
	}
	if ((hh<0) || (hh>23)) 
	{
		return false;
	}
	var mi=Trim(inputTime[1]);
	if (isNaN(mi))
	{
		return false;
	}
	if (mi.indexOf('0')==0) 
	{
		mi=mi.substr(1,mi.length); 
	}
	if((mi<0) || (mi>59)) 
	{
		return false;
	}
	if(inputTime.length==3)
	{
		var ss=Trim(inputTime[2]);
		if (isNaN(ss))
		{
			return false;
		}
		if (ss.indexOf('0')==0) 
		{
			ss=ss.substr(1,ss.length); 
		}
		if ((ss<0) || (ss>59)) 
		{
			return false;
		}
	}
	return true
};
function LeapYear(intYear)
{
	if (intYear % 100 == 0)
	{
		if (intYear % 400 == 0)
		{
			return true;
		}
	}
	else
	{
		if ((intYear % 4) == 0) 
		{
			return true; 
		}
	}
	return false;
};
function fSetDateTime(iYear,iMonth,iDay)
{
	//giHh=document.all.dpHour.value;
	giHh = document.getElementById("dpHour").value;
	//giMm=document.all.dpMinute.value;
	giMm=document.getElementById("dpMinute").value;
	//giSs=document.all.dpSecon.value;
	giSs=document.getElementById("dpSecon").value;
	giHh=(giHh.length==1)?'0'+giHh:giHh;
	giMm=(giMm.length==1)?'0'+giMm:giMm;
	giSs=(giSs.length==1)?'0'+giSs:giSs;
	giTime=giHh+':'+giMm+':'+giSs;
	VicPopCal.style.visibility = "hidden";
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		gdCtrl_tow.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{
		iMonth=(iMonth.toString().length==1)?'0'+iMonth.toString():iMonth;
		iDay=(iDay.toString().length==1)?'0'+iDay.toString():iDay;
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
		else
		{
			gdCtrl.innerText = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
	}
//	for (i in goSelectTag)
//	{
//		goSelectTag[i].style.visibility = "visible";
//	}
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function getNowDate()
{
	var nn = new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var monstr1;
	var datestr1
	if(mon1<10)
	{
		monstr1="0"+mon1;
	}
	else
	{
		monstr1=""+mon1;
	}
	if(date1<10)
	{
		datestr1="0"+date1;
	}
	else
	{
		datestr1=""+date1;
	}
	year1+"-"+monstr1+"-"+datestr1;
};
function getlastweekDate()
{
	var nn=new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var mm=new Date(year1,mon1-1,date1);
	var tmp1=new Date(2000,1,1);
	var tmp2=new Date(2000,1,15);
	var ne=tmp2-tmp1;
	var mm2=new Date();
	mm2.setTime(mm.getTime()-ne);
	year2=mm2.getYear();
	mon2=mm2.getMonth()+1;
	date2=mm2.getDate();
	if(mon2<10)
	{
		monstr2="0"+mon2;
	}
	else
	{
		monstr2=""+mon2;
	}
	if(date2<10) 
	{
		datestr2="0"+date2;
	}
	else
	{
		datestr2=""+date2;
	}
	return year2+"-"+monstr2+"-"+datestr2;
};
function fSetDate(iYear, iMonth, iDay)
{
	
	var VicPopCal = document.getElementById("VicPopCal");
	//
	var format = canlendarObj.formate;
	format = format.toLowerCase(); //\u9632\u6b62\u5bf9yyyy-MM-dd hh:mm:ss\u683c\u5f0f\u7684\u9519\u8bef\u5904\u7406
	var mm = "-";	
	var yy = "-";
	var Hh_mm_ss = "";
	var isFull = canlendarObj.isFullDate;
	//if(format != null)	//wxr 2008-08-25: \u8be5\u5224\u65ad\u4e0d\u4e25\u8c28\uff0cformat\u4e3a\u5b57\u7b26\u4e32\uff0c\u5b83\u5b58\u5728\u4e3a"null"\u7684\u60c5\u51b5\uff0c\u8fd8\u6709\u5e94\u5224\u65ad\u5176\u957f\u5ea6\uff1b\u8be5\u5224\u65ad\u4f1a\u5f15\u8d77\u4e71\u7801\u95ee\u9898
	if(format != "null" && format != null && format.length > 6)
	{
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		
		yy = format.substring(y_endindex+1,y_endindex+2);
	    var m_index = format.indexOf("m");
		//var m_endindex = format.lastIndexOf("m");	
		mm = format.substring(m_index+2, m_index+3);
		
		if(format.length > 10 && format.indexOf(':') != -1)
		{
			Hh_mm_ss = (getHhMmSs() == undefined)?"":" "+getHhMmSs();			
		}
	}

	//

	VicPopCal.style.visibility = "hidden";
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		gdCtrl_tow.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{
		iMonth=(iMonth.toString().length==1)?'0'+iMonth.toString():iMonth;
		iDay=(iDay.toString().length==1)?'0'+iDay.toString():iDay;
		
		//\u5904\u7406\u65e5\u671f\u683c\u5f0f 
	
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = iYear+yy+iMonth+mm+iDay+Hh_mm_ss;
		}
		else
		{
			gdCtrl.innerText = iYear+yy+iMonth+mm+iDay+Hh_mm_ss;
		}
		
		if(isFull == "false")
		{
			gdCtrl_tow.value = Hh_mm_ss;
		}else{
			gdCtrl_tow.value = iYear+yy+iMonth+mm+iDay+Hh_mm_ss;
		}
	}
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	gdCtrl.onchange();
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function HiddenDiv()
{
	var i;
	var iframe = document.getElementById("iframe_id");
	iframe.style.display = "none";
	document.getElementById('VicPopCal').style.visibility = "hidden";
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	goSelectTag.length = 0;
};

/**
 * \u83b7\u5f97\u9009\u62e9\u503c
 * @param {Object} aCell
 */
function fSetSelected(aCell)
{
	
	Hid = true;
	var iOffset = 0;
	giYear = parseInt(document.getElementById("tbSelYear").value);
	giMonth = parseInt(document.getElementById("tbSelMonth").value);
	var iYear = giYear;
	var iMonth = giMonth;
	aCell.style.bgColor = gcBG;
    
	var nodeobj="";
    
	//edit by chris on 2007-10-09
    if(!isIE()){
	
	  nodeobj=aCell.childNodes;

		var iDay = parseInt(nodeobj[0].textContent);//innerText
		if (nodeobj[0].color==gcGray)
		{
			iOffset = (nodeobj[0].getAttribute("Victor")<10)?-1:1;//zhanghongwei 2007-10-11
		}
		iMonth += iOffset;
		if (iMonth<1)
		{
			iYear--;
			iMonth = 12;
		}
		else if (iMonth>12)
		{
			iYear++;
			iMonth = 1;
		}

	}
	else{
      nodeobj=aCell.children["cellText"];
      // alert(nodeobj.Victor);
		with (nodeobj)
		{
	
			var iDay = parseInt(innerText);
			if (color==gcGray)
			{
				iOffset = (Victor<10)?-1:1;				
			}
			iMonth += iOffset;
			if (iMonth<1)
			{
				iYear--;
				iMonth = 12;
			}
			else if (iMonth>12)
			{
				iYear++;
				iMonth = 1;
			}
		}
	}


	fSetDate(iYear, iMonth, iDay);
};
function Point(iX, iY)
{
	this.x = iX;
	this.y = iY;
};
function fBuildCal(iYear, iMonth)
{
	var aMonth=new Array();
	for(i=1;i<7;i++)
	{
		aMonth[i]=new Array(i);
	}
	var dCalDate=new Date(iYear, iMonth-1, 1);
	var iDayOfFirst=dCalDate.getDay();
	var iDaysInMonth=new Date(iYear, iMonth, 0).getDate();
	var iOffsetLast=new Date(iYear, iMonth-1, 0).getDate()-iDayOfFirst+1;
	var iDate = 1;
	var iNext = 1;
	for (d = 0; d < 7; d++)
	{
		aMonth[1][d] = (d<iDayOfFirst)?-(iOffsetLast+d):iDate++;
	}
	for (w = 2; w < 7; w++)
	{
		for (d = 0; d < 7; d++)
		{
			aMonth[w][d] = (iDate<=iDaysInMonth)?iDate++:-(iNext++);
		}
	}
	return aMonth;
};
function fDrawCal(iYear, iMonth, iCellHeight, sDateTextSize)
{
	var divHtml = '';
	//var WeekDay = new Array("\u65e5","\u4e00","\u4e8c","\u4e09","\u56db","\u4e94","\u516d");
	var WeekDay = new Array($res_entry('ui.control.calendar.Sunday'),$res_entry('ui.control.calendar.Monday'),$res_entry('ui.control.calendar.Tuesday'),$res_entry('ui.control.calendar.Wednesday'),$res_entry('ui.control.calendar.Thursday'),$res_entry('ui.control.calendar.Friday'),$res_entry('ui.control.calendar.Saturday'));
	var styleTD = " bgcolor='"+gcBG+"' bordercolor='"+gcBG+"' valign='middle' align='center' height='"+iCellHeight+"' style='font:bold arial "+sDateTextSize+";"; 
	var hmsStyleTD = " bgcolor='white' bordercolor='"+gcBG+"' valign='middle' align='center' height='"+iCellHeight+"' style='font:bold arial "+sDateTextSize+";"; 
	var txtHmsStyle="style=WIDTH:15px;COLOR:black;BORDER-TOP-STYLE:none;BORDER-RIGHT-STYLE:none;BORDER-LEFT-STYLE:none;HEIGHT:100%;BACKGROUND-COLOR:white;BORDER-BOTTOM-STYLE:none;background-color:Transparent;"; 
	divHtml = divHtml + "<tr>";
	for(i=0; i<7; i++)
	{
		divHtml = divHtml + "<td width='14%'"+styleTD+"color:#990099' >"+ WeekDay[i] + "</td>";
	}
	divHtml = divHtml + "<tr>";
	for (w = 0; w < 6; w++)
	{
		divHtml = divHtml +  "<tr>";
		for (d = 0; d < 7; d++)
		{
			divHtml = divHtml + "<td id=calCell "+styleTD+"cursor:pointer;' onMouseOver='this.bgColor=gcToggle' onMouseOut='this.bgColor=gcBG' onclick='fSetSelected(this)'>";
			divHtml = divHtml + "<font id=cellText name=cellText Victor='Liming Weng'> </font>";
			divHtml = divHtml + "</td>";
		}
	}
	return divHtml;
};
var onfocusobj =null;
function selectHour(obj)
{
	onfocusobj =obj;
	obj.select();
};
function addTime()
{
	if(onfocusobj==undefined)
	{
		 //onfocusobj =document.all('dpHour');
		 onfocusobj = document.getElementsByName('dpHour');
	}
	intTe=parseInt(onfocusobj.value);
	if(onfocusobj.id == 'dpHour') 
	{
		 if(intTe==23) 
		{
			intTe=0;   
		}
		else
		{
			intTe = intTe +1;   
		}
	}
	else
	{
		if(intTe==59)
		{
			intTe=0;
		}
		else
		{
			intTe = intTe +1;
		}
	}
	onfocusobj.value =intTe ;
};
function jianTime()
{
	 if(onfocusobj==null)
	{
		//onfocusobj =document.all('dpHour'); 
		onfocusobj = document.getElementsByName('dpHour');   
	}
	intTe=parseInt(onfocusobj.value);  
	 if(intTe>1)
	{
		 intTe =intTe- 1;
	}
	else
	{
		 if(onfocusobj.id == 'dpHour')
		{
			intTe = 23;
		}
		else
		{
			intTe = 59; 
		}
	}
	onfocusobj.value = intTe;
};
function NotHid()
{
	Hid = false;
};
function fUpdateCal(iYear, iMonth)
{
	Hid = false
	myMonth = fBuildCal(iYear, iMonth);
	 var i = 0;
	 var nn = new Date();
	 var cYear = nn.getFullYear();	
	
	 var cDay = nn.getDate();
	 var cMonth = nn.getMonth()+1;
	 var Marked = false;
	 var Started = false;
	
	for (w = 0; w < 6; w++)
	{
		for (d = 0; d < 7; d++)
		{
		
		
			if(isIE()){
				cellText[(7*w)+d].Victor = i++;
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].color = gcGray;
					//alert(cellText[(7*w)+d]);
					
					cellText[(7*w)+d].innerText = -myMonth[w+1][d];
					
				}
				else
				{
					cellText[(7*w)+d].color = ((d==0)||(d==6))?"blue":"black";
					cellText[(7*w)+d].innerText = myMonth[w+1][d];
				}
				if (cellText[(7*w)+d].innerText == 1) 
				{
					Started = true;
				}
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if (cDay != giDay && parseInt(giDay) == cellText[(7*w)+d].innerText && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
					Marked = true; color = "red"; 
					cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: buttonface'>" + cellText[(7*w)+d].innerText + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{
					color = "red"; cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: yellow'>" + cellText[(7*w)+d].innerText + "</div>";
				}
			
			}
			else{  //edit by chris on 2007-10-09,on firefox
				
				cellText=document.getElementsByName("cellText"); 
				cellText[(7*w)+d].setAttribute("Victor",i++);
				
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].color = gcGray;
					

					cellText[(7*w)+d].textContent = -myMonth[w+1][d]; 					
					
				}
				else
				{
					cellText[(7*w)+d].color = ((d==0)||(d==6))?"blue":"black";
					cellText[(7*w)+d].textContent = myMonth[w+1][d];
				}
				if (cellText[(7*w)+d].textContent == 1) 
				{
					Started = true;
				}
				
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if (cDay != giDay && parseInt(giDay) == cellText[(7*w)+d].textContent && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
				    Marked = true; color = "red"; 
				    cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: buttonface'>" + cellText[(7*w)+d].textContent + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{ 
					color = "red"; cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: yellow'>" + cellText[(7*w)+d].textContent + "</div>";
				}
			}
		}
	}
};
function fSetYearMon(iYear, iMon)
{
	//if(isIE())	document.all.tbSelMonth.options[iMon-1].selected = true;
	//else  document.getElementById("tbSelMonth").options[iMon-1].selected = true;
	document.getElementById("tbSelMonth").options[iMon-1].selected = true;
	
	//for (i = 0; i < document.all.tbSelYear.length; i++)
	for (i = 0; i < document.getElementById("tbSelYear").length; i++)
	{
		//if (document.all.tbSelYear.options[i].value == iYear)
		if (document.getElementById("tbSelYear").options[i].value == iYear)
		{
			//document.all.tbSelYear.options[i].selected = true;
			document.getElementById("tbSelYear").options[i].selected = true;
		}
	}
	fUpdateCal(iYear, iMon);
};
function fPrevMonth()
{
	Hid = false;
	/*
	if(isIE())
	{
		var iMon = document.all.tbSelMonth.value;
		var iYear = document.all.tbSelYear.value;
		
	}	
	else{
		var iMon =  document.getElementById("tbSelMonth").value;
		var iYear = document.getElementById("tbSelYear").value;
		
	} 
	*/
	var iMon =  document.getElementById("tbSelMonth").value;
	var iYear = document.getElementById("tbSelYear").value;
	
	
	if (--iMon<1) 
	{
			iMon = 12;
			iYear--;
	}
	fSetYearMon(iYear, iMon);
};
function fNextMonth()
{
	Hid = false;
	
	var iMon =  document.getElementById("tbSelMonth").value;//zhanghongwei
    var iYear = document.getElementById("tbSelYear").value;//zhanghongwei
	if (++iMon>12) 
	{
			iMon = 1;
			iYear++;
	}
	fSetYearMon(iYear, iMon);
};
function fToggleTags()
{
	
	with (document.getElementsByTagName("select"))//
	{
		    var temp=null;
			for (i=0; i<length; i++)
		{
				temp=item(i).getAttribute("Victor");//edit by chris on 2007-10-09

					if ((temp!="Won")&&fTagInBound(item(i)))
			{
							item(i).style.visibility = "hidden";
							goSelectTag[goSelectTag.length] = item(i);
			}
		}
	}
};
function fTagInBound(aTag)
{
	var VicPopCal = document.getElementById('VicPopCal');
	with (VicPopCal.style)
	{
			var l = parseInt(left);
			var t = parseInt(top);
			var r = l+parseInt(width);
			var b = t+parseInt(height);
			var ptLT = fGetXY(aTag);
			return !((ptLT.x>r)||(ptLT.x+aTag.offsetWidth<l)||(ptLT.y>b)||(ptLT.y+aTag.offsetHeight<t));
	}
};
function fGetXY(aTag)
{

	var oTmp = aTag;
	
	var pt = new Point(0,0);
	do 
	{
			pt.x += oTmp.offsetLeft;
			pt.y += oTmp.offsetTop;
			oTmp = oTmp.offsetParent;
	}
	while(oTmp!=null && oTmp.tagName!="BODY");
	
	return pt;
};
divHtml = '';
function HidChange(boVis)
{
	Hid = boVis;
};

function setHhMmSs()
{
	var dataObject = new Date();
	dataObject.setHours(giHh, giMm, giSs);
	try{
	document.getElementById("ssb_HH").value = new String(dataObject.getHours()).length == 1? "0"+dataObject.getHours():dataObject.getHours();
	document.getElementById("ssb_mm").value = new String(dataObject.getMinutes()).length == 1? "0"+dataObject.getMinutes():dataObject.getMinutes();
	document.getElementById("ssb_ss").value = new String(dataObject.getSeconds()).length == 1? "0"+dataObject.getSeconds():dataObject.getSeconds();
	}catch(e)
	{
		return ;
	}
}
function getHhMmSs()
{
	try{
	var hh_mm_ss = "";
	var hh = document.getElementById("ssb_HH").value;
	var mm = document.getElementById("ssb_mm").value;
	var ss = document.getElementById("ssb_ss").value;
	hh = hh.length ==1? "0"+hh:hh;
	mm = mm.length ==1? "0"+mm:mm;
	ss = ss.length ==1? "0"+ss:ss;
	hh_mm_ss = hh_mm_ss+hh+":"+mm+":"+ss;
	return hh_mm_ss;
	}catch(e){
		return ;
	}
}

function checkHours(o)
{
	if(isNaN(parseInt(o.value)))
	{
		o.value = 00;
		o.select();
	}
	else if(parseInt(o.value) > 23 || parseInt(o.value) < 0)
	{
		o.value = o.value > 23? 23:0;
		o.select();
	}
}
function checkMinutes(o)
{
	if(isNaN(parseInt(o.value)))
	{
		o.value = 00;
		o.select();
	}
	else if(parseInt(o.value) > 59 || parseInt(o.value) < 0)
	{
		o.value = o.value >59? 59:0;
		o.select();
	}
}
function checkSeconds(o)
{
	if(isNaN(parseInt(o.value)))
	{
		o.value = 00;
		o.select();
	}
	else if(parseInt(o.value) > 59 || parseInt(o.value) < 0)
	{
		o.value = o.value > 59? 59:0;
		o.select();
	}
}
var Timer_name = "";
function fPopCalendar(popCtrl, dateCtrl, formate, ctrl2)
{
	//wanghao4
	canlendarObj.formate = formate;
	popCtrl =document.getElementById(ctrl2);
	while(popCtrl.tagName == undefined || popCtrl.tagName.toLowerCase() != "img")
	{
		popCtrl = popCtrl.nextSibling;
	}
	var strDate = null;
	strDate=Trim(document.getElementById(dateCtrl).value)
	if(strDate.length>10)
	{
		giYear = strDate.substring(0,4);
		giMonth = strDate.substring(5,7);
		giDay= strDate.substring(8,10);
//		var iTime=strDate.substring(10,strDate.Length) ; \u8c01\u628alength\u5927\u5199\u4e86\u7684?
		var iTime=strDate.substring(10,strDate.length) ;
		iTime=Trim(iTime).split(':');
		giHh=iTime[0];
		giMm=iTime[1];
		giSs=iTime[2];
	}
	else
	{
		var curDate = new Date();
		
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay= curDate.getDate();
		
		giHh = curDate.getHours();
		giMm = curDate.getMinutes();
		giSs = curDate.getSeconds();
	}
	var c = document.getElementById('VicPopCal');
	if(c == null)
	{
		var divHtml = '';
		//\u4fee\u6539\u8bb0\u5f551 \u738b\u6d69\u4fee\u6539
		var iframe = document.createElement("iframe");
		iframe.style.cssText = "position:absolute;z-index:9;width:expression(this.nextSibling.offsetWidth);height:expression(this.nextSibling.offsetHeight);top:expression(this.nextSibling.offsetTop);left:expression(this.nextSibling.offsetLeft);";
		iframe.frameborder = 0;
		iframe.setAttribute('id',"iframe_id");
		iframe.setAttribute('frameBorder',0);
		document.body.appendChild(iframe);		
		//\u4fee\u6539\u8bb0\u5f551
		
		var divCal = document.createElement("div");
		divCal.id="VicPopCal";
		
		//\u4fee\u6539\u8bb0\u5f551 \u738b\u6d69\u4fee\u6539
		divCal.style.cssText="ALIGN:center;POSITION:absolute;VISIBILITY:hidden;border:1px ridge;z-index:10;";		
		//document.body.insertBefore(divCal,null);
		document.body.appendChild(divCal);
		//\u4fee\u6539\u8bb0\u5f551
		
		//wanghao2
		document.onclick = function (event)
		{
			var e = event || window.event;
			var elem = e.srcElement||e.target;
			//wanghao3
			if(elem.tagName.toLowerCase() == "img" && elem.id == "calendar_img_id")
			{
				return;
			}	
			
			while(elem)
			{
				if(elem.id == "VicPopCal")
					return;
				elem=elem.parentNode;
			}
			
			HiddenDiv();
		}
		
		divHtml = divHtml + "<table border='0' width=100% bgcolor='#cccccc'>";
		divHtml = divHtml + "<TR>";
		divHtml = divHtml + "<td valign='middle' align='center'><input type='button' css='button' name='PrevMonth' value='<' style='height:20;width:10%;FONT:bold' onClick='fPrevMonth()'>";
		divHtml = divHtml + "&nbsp;<SELECT id='tbSelYear' name='tbSelYear'onChange='fUpdateCal(tbSelYear.value, tbSelMonth.value)' Victor='Won' style='height:20;width:36%;'>";
		for(i=1900;i<=2050;i++)
		{
				//divHtml = divHtml + "<OPTION value='"+i+"'>"+i+"\u5e74</OPTION>";
				divHtml = divHtml + "<OPTION value='"+i+"'>"+i+$res_entry('ui.control.calendar.year')+"</OPTION>";
		}
		divHtml = divHtml + "</SELECT>";
		divHtml = divHtml + "&nbsp;<select id='tbSelMonth' name='tbSelMonth' onChange='fUpdateCal(tbSelYear.value, tbSelMonth.value)' Victor='Won' style='height:20;width:33%;'>";
		for (i=0; i<12; i++)
		{
				divHtml = divHtml + "<option value='"+(i+1)+"'>"+gMonths[i]+"</option>";
		}
		divHtml = divHtml + "</SELECT>";
		divHtml = divHtml + "&nbsp;<input type='button' css='button' name='PrevMonth' value='>' style='height:20;width:10%;FONT:bold' onclick='fNextMonth()'>";
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</TR><TR>";
		divHtml = divHtml + "<td align='center'>";
		divHtml = divHtml + "<DIV style='background-color:#3366CC'><table width='98%' border='0'>";
		divHtml = divHtml + fDrawCal(giYear, giMonth, 20, '12');
		divHtml = divHtml + "</table></DIV>";
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</TR><TR><TD align='center'>";
		divHtml = divHtml + "<TABLE id='tbhms' width='100%' bgcolor='#cccccc'>";
		divHtml = divHtml + "<TR><TD align='center'>";
		//divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(0,0,0)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>\u6e05\u7a7a</B>";
		divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(0,0,0)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>"+$res_entry('ui.control.calendar.clear')+"</B>";
		divHtml = divHtml + "     ";
		//divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(curYear,curMonth,curDay)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>\u4eca\u5929:"+curYear+"-"+curMonth+"-"+curDay+"</B>";
		divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(curYear,curMonth,curDay)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>"+$res_entry('ui.control.calendar.today')+":"+curYear+"-"+curMonth+"-"+curDay+"</B>";
		divHtml = divHtml + "</td></tr>";
		//\u4e3a\u4e86\u4e0d\u540c\u7684\u65e5\u5386\u4f1a\u6709\u4e0d\u540c\u7684\u663e\u793a\u98ce\u683c, \u6240\u4ee5\u4e0d\u5e94\u8be5\u628a\u662f\u5426\u663e\u793a\u65f6\u5206\u79d2\u653e\u5728\u5176\u5b83\u5224\u65ad\u91cc\u9762
		/*
		if(formate != null && formate.length > 10)
		{
			divHtml = divHtml + "<TR><TD align='center'>";
			divHtml = divHtml + "<input type='text' value='00' id='ssb_HH' name='ssb_HH' size='2' onblur='checkHours(this)'/>:";
			divHtml = divHtml + "<input type='text' value='00' id='ssb_mm' name='ssb_mm' size='2' onblur='checkMinutes(this)'/>:";
			divHtml = divHtml + "<input type='text' value='00' id='ssb_ss' name='ssb_ss' size='2' onblur='checkSeconds(this)'/>";
			divHtml = divHtml + "</td></tr>";	
		}
		*/
		divHtml = divHtml + "</table>";
		divHtml = divHtml + "</TD></TR>";
		divHtml = divHtml + "</TABLE>";
		document.getElementById("VicPopCal").innerHTML = divHtml;
	}
	//\u4e3a\u4e86\u4e0d\u540c\u7684\u65e5\u5386\u4f1a\u6709\u4e0d\u540c\u7684\u663e\u793a\u98ce\u683c, \u6240\u4ee5\u4e0d\u5e94\u8be5\u628a\u662f\u5426\u663e\u793a\u65f6\u5206\u79d2\u653e\u5728\u5176\u5b83\u5224\u65ad\u91cc\u9762, \u5b83\u53ea\u4e0eformate\u6709\u5173
	if(formate != null && formate.length > 10)
	{
		if(document.getElementById("trhms") == null)
		{
			hms = "<TR id='trhms'><TD align='center'>";
			//\u5e94\u8be5\u8bbe\u7f6eid\u800c\u4e0d\u4ec5\u4ec5\u662fname
			hms = hms + "<input id='ssb_HH' type='text' value='00' name='ssb_HH' size='2' onblur='checkHours(this)'/>:";
			hms = hms + "<input id='ssb_mm' type='text' value='00' name='ssb_mm' size='2' onblur='checkMinutes(this)'/>:";
			hms = hms + "<input id='ssb_ss' type='text' value='00' name='ssb_ss' size='2' onblur='checkSeconds(this)'/>";
			hms = hms + "</td></tr>";	
			$(hms).appendTo("#tbhms");
		}
		setHhMmSs();
	}
	else if($("#trhms"))
	{
		$("#trhms").remove();
	}
	
	//Timer_name = setInterval("setHhMmSs()",1000);
	//setHhMmSs();
	if (popCtrl == previousObject)
	{
		var VicPopCal = document.getElementById('VicPopCal');
			if (VicPopCal.style.visibility == "visible")
		{
					HiddenDiv();
					return true;
		}
	}
	
	previousObject = popCtrl;
	
	gdCtrl = document.getElementById(dateCtrl);
	gdCtrl_tow = document.getElementById(ctrl2);
	fInitialDate(strDate);
	//alert("ok1");
	fSetYearMon(giYear, giMonth); 
	//alert("ok2");

	var point = fGetXY(popCtrl);
	
	var VicPopCal = document.getElementById('VicPopCal');
	var iframe = document.getElementById("iframe_id");
	with (VicPopCal.style) 
	{
		    if(point.x > 240) left = point.x+popCtrl.offsetWidth-255;
			else left = point.x+popCtrl.offsetWidth;
			top  = point.y+popCtrl.offsetHeight;
			width = VicPopCal.offsetWidth;
			width = 240;
			//height = VicPopCal.offsetHeight;
			//fToggleTags(point); 	//comment by chris on 2007-10-09,\u8fd9\u4e2a\u51fd\u6570\u4ec0\u4e48\u529f\u80fd\uff0c\u6ca1\u592a\u660e\u767d
			visibility = 'visible';
			iframe.style.display = "block";
	}
};
function fSetHhMmSs()
{
	//document.all.dpHour.value=giHh;
	document.getElementById("dpHour").value = giHh;
	//document.all.dpMinute.value=giMm;
	document.getElementById("dpMinute").value = giMm;
	if (giSs!=null && giSs!='')
	{
		//document.all.dpSecon.value=giSs;
		document.getElementById("dpSecon").value = giSs;
	}
	else
	{
		//document.all.dpSecon.value='0';
		document.getElementById("dpSecon").value = '0';
	}
};
function fInitialDate(strDate)
{
	if( strDate == null || strDate.length != 10 )
	{
		return false;
	}
	var sYear  = strDate.substring(0,4);
	var sMonth = strDate.substring(5,7);
	var sDay   = strDate.substring(8,10);
	if( sMonth.charAt(0) == '0' ) { sMonth = sMonth.substring(1,2); }
	if( sDay.charAt(0)   == '0' ) { sDay   = sDay.substring(1,2);   }
	var nYear  = parseInt(sYear );
	var nMonth = parseInt(sMonth);
	var nDay   = parseInt(sDay);
	if ( isNaN(nYear ) )	return false;
	if ( isNaN(nMonth) )	return false;
	if ( isNaN(nDay  ) )	return false;
	var arrMon = new Array(12);
	arrMon[ 0] = 31;	arrMon[ 1] = nYear % 4 == 0 ? 29:28;
	arrMon[ 2] = 31;	arrMon[ 3] = 30;
	arrMon[ 4] = 31;	arrMon[ 5] = 30;
	arrMon[ 6] = 31;	arrMon[ 7] = 31;
	arrMon[ 8] = 30;	arrMon[ 9] = 31;
	arrMon[10] = 30;	arrMon[11] = 31;
	if ( nYear  < 1900 || nYear > 2050 )	return false;
	if ( nMonth < 1 || nMonth > 12 )				return false;
	if ( nDay < 1 || nDay > arrMon[nMonth - 1] )	return false;
	giYear  = nYear;
	giMonth = nMonth;
	giDay   = nDay;
	return true;
};
//var gMonths = new Array("\u4e00\u6708","\u4e8c\u6708","\u4e09\u6708","\u56db\u6708","\u4e94\u6708","\u516d\u6708","\u4e03\u6708","\u516b\u6708","\u4e5d\u6708","\u5341\u6708","\u5341\u4e00\u6708","\u5341\u4e8c\u6708");
var gMonths = new Array($res_entry('ui.control.calendar.January'),$res_entry('ui.control.calendar.February'),$res_entry('ui.control.calendar.March'),$res_entry('ui.control.calendar.April'),$res_entry('ui.control.calendar.May'),$res_entry('ui.control.calendar.June'),$res_entry('ui.control.calendar.July'),$res_entry('ui.control.calendar.August'),$res_entry('ui.control.calendar.September'),$res_entry('ui.control.calendar.October'),$res_entry('ui.control.calendar.November'),$res_entry('ui.control.calendar.December'));
function maskN()
{
	var masKey = 78;
	if(event.keyCode==masKey&&event.ctrlKey)
	{
		 event.returnValue=0;
		 window.event.returnValue=0;
	}
};
function isIE()
{
	browser_name = navigator.appName;
	if (browser_name == "Microsoft Internet Explorer") 
	{
			return true;
	}
	return false;
};
function isIE2()
{
	if(document.all)
	{
			return true;
	}
	return false;
};
function isNC()
{
	browser_name = navigator.appName;
	if(browser_name == "Netscape")
	{
			return true;
	}
	return false;
};
function isNC2()
{
	if(document.layers)
	{
			return true;
	}
	return false;
};
function version()
{
	 return parseFloat(navigator.appVersion);
};
var LastTabObjectName="";
var FirstTabObjectName="";
function handleEnter()
{
};
function myKeyEvent(e)
{
	var hotkey=13;
	var rightKey = 39;
	var leftKey = 41;
	if (isNC())
	{
			if(e.which==hotkey)
		{
					e.which=9;
					return true;
		}
	}
	else if (isIE())
	{
		if (event.keyCode==hotkey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						if(event.srcElement.type!="textarea")
				{
								event.keyCode=9;
				}
						return true;
			}
		}
		if (event.keyCode==rightKey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						event.keyCode=9;
						return true;
			}
		}
	}
};
handleEnter();
function closeCalendarWin()
{

	try
	{
		var closeSelectDateWindow= Hid;
		if(previousObject == null)
		{
				return;
		}
		if(closeSelectDateWindow == true)
		{
				HiddenDiv();
		}
	}
	catch(e)
	{
			return;
	}
};
function dateCompare(fromDate,toDate,promptInfo)
{
	var calendar1Value = fromDate.value;
	var calendar2Value = toDate.value;
	if(calendar1Value!="" && calendar2Value!="")
	{
			if(calendar1Value > calendar2Value)
		{
				   alert(promptInfo);
					return false;
		}
	}
	return true;			
};
addOnloadEvent(Calendar_init);

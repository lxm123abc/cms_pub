package com.zte.cms.EMBtask.ls;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.cms.EMBtask.service.IMovieTechauditResultDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IMovieTechauditResultLS
{

    /**
     * 根据条件分页查询CmsEmbtask对象
     * 
     * 
     * @throws DomainServiceException 异常
     */
    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DomainServiceException;

}

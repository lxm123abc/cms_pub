package com.zte.cms.basicdata.area.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.umap.sys.model.Tree;
import com.zte.umap.sys.model.UsysProvince;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 提供了区域数据管理中省份管理的一套接口
 * 
 * 
 */
public interface IIsysProvinceLS
{

    public Tree insertUsysProvinceInTree(UsysProvince usysProvince) throws DomainServiceException;

    public Tree updateUsysProvinceInTree(UsysProvince usysProvince) throws DomainServiceException;

    public Tree removeUsysProvinceFromIptvTree(UsysProvince usysProvince) throws DomainServiceException;

    public UsysProvince getUsysProvince(UsysProvince usysProvince) throws DomainServiceException;

    public List<UsysProvince> getUsysProvinceByCond(UsysProvince usysProvince) throws DomainServiceException;

    public List<Tree> getUsysProvinceInIptvTree(Tree tree) throws DomainServiceException;

    public UsysProvince getUsysProvinceById(UsysProvince usysProvince) throws DomainServiceException;

    public String getUsysProvinceByName(UsysProvince usysProvince) throws DomainServiceException;

    public HashMap<String, Object> getSysProvince(String selectedValue, Boolean needSelect)
            throws DomainServiceException;

    public String operSynByProvdesc(String provdesc, String provindex) throws DomainServiceException;
}

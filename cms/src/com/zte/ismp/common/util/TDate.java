package com.zte.ismp.common.util;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * <p>
 * 文件名称: TDate.java
 * </p>
 * <p>
 * 文件描述: 日期类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 日期类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class TDate
{

    private GregorianCalendar firstDay;
    public int year;
    public int month;
    public int day;

    TDate(GregorianCalendar gcDate)
    {
        year = gcDate.get(1);
        month = gcDate.get(2);
        day = gcDate.get(5);
        firstDay = new GregorianCalendar(year, month, day);
    }

    public TDate()
    {
        this(new GregorianCalendar());
    }

    public void addDay(int v)
    {
        firstDay.add(5, v);
    }

    public void addWeek(int v)
    {
        firstDay.add(5, 7 * v);
    }

    public void addMonth(int v)
    {
        firstDay.add(2, v);
    }

    public void addQuarter(int v)
    {
        firstDay.add(2, 3 * v);
    }

    public void addYear(int v)
    {
        firstDay.add(1, v);
    }

    public String formatDate(String formatStr)
    {
        SimpleDateFormat bartDateFormat = new SimpleDateFormat(formatStr);
        return bartDateFormat.format(firstDay.getTime());
    }

    public int getIntYear()
    {
        return StrUtil.getInt(getStringYear());
    }

    public int getIntMonth()
    {
        return StrUtil.getInt(getStringMonth());
    }

    public int getIntDay()
    {
        return StrUtil.getInt(getStringDay());
    }

    public String getStringYear()
    {
        return formatDate("yyyy");
    }

    public String getStringMonth()
    {
        return formatDate("M");
    }

    public String getStringDay()
    {
        return formatDate("d");
    }

    public static void main(String args[])
    {
        TDate tDate = new TDate();
        tDate.addYear(1);
        System.out.println(tDate.getStringYear() + " " + tDate.getStringMonth() + " " + tDate.getStringDay());
    }
}

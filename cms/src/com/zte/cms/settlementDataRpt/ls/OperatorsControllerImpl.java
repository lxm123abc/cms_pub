package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.cms.settlementDataRpt.service.OperatorsService;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import org.apache.commons.lang.StringUtils;

/**
 * @Author: Liangxiaomin
 * @Date Created in 14:54 2019/6/27
 * @Description:
 */
public class OperatorsControllerImpl implements OperatorsController {


    private OperatorsService operatorsService;

    public void setOperatorsService(OperatorsService operatorsService) {
        this.operatorsService = operatorsService;
    }

    @Override
    public TableDataInfo pageInfoQuery(OperInformation operInformation, int arg1, int arg2) throws Exception {

        if (StringUtils.isNotEmpty(operInformation.getOperallname())){
            operInformation.setOperallname("%"+operInformation.getOperallname()+"%");
        }
        operInformation.setStart(arg1);
        operInformation.setPageSize(arg2);
        TableDataInfo tableDataInfo = operatorsService.pageInfoQuery(operInformation);

        return tableDataInfo;
    }
}

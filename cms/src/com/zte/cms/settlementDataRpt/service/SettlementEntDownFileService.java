package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;
import com.zte.ssb.framework.exception.exceptions.DAOException;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:23 2019/6/28
 * @Description:
 */
public interface SettlementEntDownFileService {

    void insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) throws DAOException;

    void updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) throws DAOException;

    SettlementEntDownFile selectSettlementEntDownFileBySpid(String spid) throws DAOException;

    void deleteAttach(SettlementEntDownFile settlementEntDownFile) throws DAOException;

}

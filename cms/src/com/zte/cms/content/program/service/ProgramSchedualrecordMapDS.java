package com.zte.cms.content.program.service;

import java.util.List;

import com.zte.cms.content.program.dao.IProgramSchedualrecordMapDAO;
import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ProgramSchedualrecordMapDS extends DynamicObjectBaseDS implements IProgramSchedualrecordMapDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IProgramSchedualrecordMapDAO dao = null;
	
	public void setDao(IProgramSchedualrecordMapDAO dao)
	{
	    this.dao = dao;
	}

	public void insertProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug("insert programSchedualrecordMap starting...");
		try
		{

            if (programSchedualrecordMap.getMapindex() == null)
            {
                Long mapinex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_program_index");
                programSchedualrecordMap.setMapindex(mapinex);
            }
			dao.insertProgramSchedualrecordMap( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert programSchedualrecordMap end");
	}
	
	public void updateProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug("update programSchedualrecordMap by pk starting...");
	    try
		{
			dao.updateProgramSchedualrecordMap( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update programSchedualrecordMap by pk end");
	}

	public void updateProgramSchedualrecordMapList( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException
	{
		log.debug("update programSchedualrecordMapList by pk starting...");
		if(null == programSchedualrecordMapList || programSchedualrecordMapList.size() == 0 )
		{
			log.debug("there is no datas in programSchedualrecordMapList");
			return;
		}
	    try
		{
			for( ProgramSchedualrecordMap programSchedualrecordMap : programSchedualrecordMapList )
			{
		    	dao.updateProgramSchedualrecordMap( programSchedualrecordMap );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update programSchedualrecordMapList by pk end");
	}

	public void updateProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug("update programSchedualrecordMap by condition starting...");
		try
		{
			dao.updateProgramSchedualrecordMapByCond( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update programSchedualrecordMap by condition end");
	}

	public void updateProgramSchedualrecordMapListByCond( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException
	{
		log.debug("update programSchedualrecordMapList by condition starting...");
		if(null == programSchedualrecordMapList || programSchedualrecordMapList.size() == 0 )
		{
			log.debug("there is no datas in programSchedualrecordMapList");
			return;
		}
		try
		{
			for( ProgramSchedualrecordMap programSchedualrecordMap : programSchedualrecordMapList )
			{
				dao.updateProgramSchedualrecordMapByCond( programSchedualrecordMap );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update programSchedualrecordMapList by condition end");
	}

	public void removeProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug( "remove programSchedualrecordMap by pk starting..." );
		try
		{
			dao.deleteProgramSchedualrecordMap( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove programSchedualrecordMap by pk end" );
	}

	public void removeProgramSchedualrecordMapList ( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException
	{
		log.debug( "remove programSchedualrecordMapList by pk starting..." );
		if(null == programSchedualrecordMapList || programSchedualrecordMapList.size() == 0 )
		{
			log.debug("there is no datas in programSchedualrecordMapList");
			return;
		}
		try
		{
			for( ProgramSchedualrecordMap programSchedualrecordMap : programSchedualrecordMapList )
			{
				dao.deleteProgramSchedualrecordMap( programSchedualrecordMap );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove programSchedualrecordMapList by pk end" );
	}

	public void removeProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug( "remove programSchedualrecordMap by condition starting..." );
		try
		{
			dao.deleteProgramSchedualrecordMapByCond( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove programSchedualrecordMap by condition end" );
	}

	public void removeProgramSchedualrecordMapListByCond( List<ProgramSchedualrecordMap> programSchedualrecordMapList )throws DomainServiceException
	{
		log.debug( "remove programSchedualrecordMapList by condition starting..." );
		if(null == programSchedualrecordMapList || programSchedualrecordMapList.size() == 0 )
		{
			log.debug("there is no datas in programSchedualrecordMapList");
			return;
		}
		try
		{
			for( ProgramSchedualrecordMap programSchedualrecordMap : programSchedualrecordMapList )
			{
				dao.deleteProgramSchedualrecordMapByCond( programSchedualrecordMap );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove programSchedualrecordMapList by condition end" );
	}

	public ProgramSchedualrecordMap getProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug( "get programSchedualrecordMap by pk starting..." );
		ProgramSchedualrecordMap rsObj = null;
		try
		{
			rsObj = dao.getProgramSchedualrecordMap( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programSchedualrecordMapList by pk end" );
		return rsObj;
	}

	public List<ProgramSchedualrecordMap> getProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DomainServiceException
	{
		log.debug( "get programSchedualrecordMap by condition starting..." );
		List<ProgramSchedualrecordMap> rsList = null;
		try
		{
			rsList = dao.getProgramSchedualrecordMapByCond( programSchedualrecordMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programSchedualrecordMap by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get programSchedualrecordMap page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programSchedualrecordMap, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramSchedualrecordMap>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programSchedualrecordMap page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get programSchedualrecordMap page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programSchedualrecordMap, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramSchedualrecordMap>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programSchedualrecordMap page info by condition end" );
		return tableInfo;
	}
}

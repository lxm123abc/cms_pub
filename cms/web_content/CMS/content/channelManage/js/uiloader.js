var USER_INFO={};
var VAILD_CODE="";
var  assist={};

function getAssistant()
{
  callUrl('$/ssb/uiloader/menuMgt/getAssistant.ssm',null,'assist','true');
  return assist.rtnValue;
}

var  vector1={};
function getMenuVector()
{
  callUrl('$/ssb/uiloader/menuMgt/getMenuVector.ssm',null,'vector1','true');
  //alert("menuVectorqqqqq="+vector1.rtnValue.toJSONString());
  return vector1.rtnValue;
}

var vector2={};
function getChildMenuList(fatherMenuId)
{
  callUrl('$/ssb/uiloader/menuMgt/getChildMenuList.ssm',fatherMenuId,'vector2','true');
  return vector2.rtnValue; 
}

var userinfo={};
var getUserInfo = function ()
{
  callUrl('$/ssb/uiloader/loginMgt/getUserInfo.ssm',null,'userinfo','true');
  return userinfo.rtnValue;
}

var returnCode={};
function login(userId,password,vaildCode)
{
  var returnStr=$res_entry("login.error.password.is.wrong");
  //var UserInfo={};
  USER_INFO.userId=userId;
  USER_INFO.password=password;
  VAILD_CODE=vaildCode;
  callUrl('$/ssb/uiloader/loginMgt/login.ssm','[USER_INFO,VAILD_CODE]','returnCode','true');
  if("10000"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.validCode.error");
  }
  if("0003"==returnCode.rtnValue||"902"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.notexist");
  }
 if("0004"==returnCode.rtnValue||"905"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.cannot.user");
  }
 if("0008"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.password.is.wrong");
  }
  if("901"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.disabled");
  }
 if("0009"==returnCode.rtnValue||"906"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.error.no.agent");
  }
 if("701"==returnCode.rtnValue||"702"==returnCode.rtnValue)
  {
    returnStr=$res_entry("login.system.error");
  }
  if("907"==returnCode.rtnValue)
  {
  	returnStr=$res_entry("login.error.disabled");
  }
 if("0000"==returnCode.rtnValue||"9999"==returnCode.rtnValue||"0"==returnCode.rtnValue)
  {
    window.open("index.html","_self");
  }
  if(returnCode.rtnValue !="0000"&&"9999"!=returnCode.rtnValue&&"0"!=returnCode.rtnValue)
   
  return returnStr;
}

function logout()
{
   callUrl('$/ssb/uiloader/loginMgt/logout.ssm');
   window.open("../login.html","_parent");
}

function logoutUmap()
{
   callUrl('$/ssb/uiloader/loginMgt/logout.ssm');
   window.open("login.html","_parent");
}

function languageChange(locale)
{
   callUrl('$/ssb/uiloader/loginMgt/languageChange.ssm',locale,"",'true');
   window.location.href="login.html";
  // window.location.reload();
}

var locale={};
function getLocaleInfo()
{
    callUrl('$/ssb/uiloader/loginMgt/getLocaleInfo.ssm',null,'locale','true');
    return locale.rtnValue;
}

String.prototype.sub = function(n,symbol)
{ 
  var r = /[^\x00-\xff]/g; 
  if(this.replace(r, "mm").length <= n) 
     return this;
  var m = Math.floor(n/2); 
  for(var i=m; i<this.length; i++) 
  { 
     if(this.substr(0, i).replace(r, "mm").length>=n) 
     { 
       if(this.substr(0, i).replace(r, "mm").length > n)
       {
          return this.substr(0, i-1) +symbol;
       }
       return this.substr(0, i) + symbol; 
     }
  } 
  return this;
 };
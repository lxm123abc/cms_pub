package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.CmsTasksourceMap;
import com.zte.cms.content.dao.ICmsTasksourceMapDAO;
import com.zte.cms.content.service.ICmsTasksourceMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsTasksourceMapDS extends DynamicObjectBaseDS implements ICmsTasksourceMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsTasksourceMapDAO dao = null;

    public void setDao(ICmsTasksourceMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("insert cmsTasksourceMap starting...");
        try
        {
            Long srcmapindex;
            if (cmsTasksourceMap.getSrcmapindex() == null)
            {
                srcmapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_tasksource_map");
                cmsTasksourceMap.setSrcmapindex(srcmapindex);
            }
            dao.insertCmsTasksourceMap(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsTasksourceMap end");
    }

    public void updateCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("update cmsTasksourceMap by pk starting...");
        try
        {
            dao.updateCmsTasksourceMap(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsTasksourceMap by pk end");
    }

    public void updateCmsTasksourceMapList(List<CmsTasksourceMap> cmsTasksourceMapList) throws DomainServiceException
    {
        log.debug("update cmsTasksourceMapList by pk starting...");
        if (null == cmsTasksourceMapList || cmsTasksourceMapList.size() == 0)
        {
            log.debug("there is no datas in cmsTasksourceMapList");
            return;
        }
        try
        {
            for (CmsTasksourceMap cmsTasksourceMap : cmsTasksourceMapList)
            {
                dao.updateCmsTasksourceMap(cmsTasksourceMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsTasksourceMapList by pk end");
    }

    public void updateCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("update cmsTasksourceMap by condition starting...");
        try
        {
            dao.updateCmsTasksourceMapByCond(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsTasksourceMap by condition end");
    }

    public void updateCmsTasksourceMapListByCond(List<CmsTasksourceMap> cmsTasksourceMapList)
            throws DomainServiceException
    {
        log.debug("update cmsTasksourceMapList by condition starting...");
        if (null == cmsTasksourceMapList || cmsTasksourceMapList.size() == 0)
        {
            log.debug("there is no datas in cmsTasksourceMapList");
            return;
        }
        try
        {
            for (CmsTasksourceMap cmsTasksourceMap : cmsTasksourceMapList)
            {
                dao.updateCmsTasksourceMapByCond(cmsTasksourceMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsTasksourceMapList by condition end");
    }

    public void removeCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("remove cmsTasksourceMap by pk starting...");
        try
        {
            dao.deleteCmsTasksourceMap(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsTasksourceMap by pk end");
    }

    public void removeCmsTasksourceMapList(List<CmsTasksourceMap> cmsTasksourceMapList) throws DomainServiceException
    {
        log.debug("remove cmsTasksourceMapList by pk starting...");
        if (null == cmsTasksourceMapList || cmsTasksourceMapList.size() == 0)
        {
            log.debug("there is no datas in cmsTasksourceMapList");
            return;
        }
        try
        {
            for (CmsTasksourceMap cmsTasksourceMap : cmsTasksourceMapList)
            {
                dao.deleteCmsTasksourceMap(cmsTasksourceMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsTasksourceMapList by pk end");
    }

    public void removeCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("remove cmsTasksourceMap by condition starting...");
        try
        {
            dao.deleteCmsTasksourceMapByCond(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsTasksourceMap by condition end");
    }

    public void removeCmsTasksourceMapListByCond(List<CmsTasksourceMap> cmsTasksourceMapList)
            throws DomainServiceException
    {
        log.debug("remove cmsTasksourceMapList by condition starting...");
        if (null == cmsTasksourceMapList || cmsTasksourceMapList.size() == 0)
        {
            log.debug("there is no datas in cmsTasksourceMapList");
            return;
        }
        try
        {
            for (CmsTasksourceMap cmsTasksourceMap : cmsTasksourceMapList)
            {
                dao.deleteCmsTasksourceMapByCond(cmsTasksourceMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsTasksourceMapList by condition end");
    }

    public CmsTasksourceMap getCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException
    {
        log.debug("get cmsTasksourceMap by pk starting...");
        CmsTasksourceMap rsObj = null;
        try
        {
            rsObj = dao.getCmsTasksourceMap(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsTasksourceMapList by pk end");
        return rsObj;
    }

    public List<CmsTasksourceMap> getCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap)
            throws DomainServiceException
    {
        log.debug("get cmsTasksourceMap by condition starting...");
        List<CmsTasksourceMap> rsList = null;
        try
        {
            rsList = dao.getCmsTasksourceMapByCond(cmsTasksourceMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsTasksourceMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsTasksourceMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsTasksourceMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsTasksourceMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsTasksourceMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsTasksourceMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsTasksourceMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsTasksourceMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsTasksourceMap page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.content.wkfw.service;

import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 内容订单工作流查询的一系列接口
 * 
 * @author 李梅 li.mei3@zte.com.cn
 * @version ZXCMS
 */
public interface ICntWkfwDS
{

    /**
     * <p>
     * 根据查询条件分页查询需要处理的内容订单工单信息
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单工作流查询条件公共类
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 表格中需要排序的字段实体
     * @return 返回查询结果，内容订单信息列表
     * @throws DomainServiceException
     * @since ZXCMS
     */
    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntorderWkfwCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * <p>
     * 根据查询条件查询需要处理的内容订单工单数量
     * </p>
     * 
     * @param cntorderWkfwCondition 内容订单工作流查询条件公共类
     * @return 返回查询结果，需要处理的内容订单工单数量
     * @throws DomainServiceException
     * @since ZXCMS
     */
    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntorderWkfwCondition)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象
     * 
     * @param cntWkfwhis CntWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 内容工单流程跟踪
     * 
     * @param cntWkfwhis
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwhis, int start, int pageSize) throws DomainServiceException;

    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis) throws DomainServiceException;

    /**
     * 审核历史信息查看
     * 
     * @param cntWkfwhis
     * @return
     * @throws DomainServiceException
     */
    public CntWkfwhis getCmsProgramWkfwhis(CntWkfwhis cntWkfwhis) throws DomainServiceException;
}

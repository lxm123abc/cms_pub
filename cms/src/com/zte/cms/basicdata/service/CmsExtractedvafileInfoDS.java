package com.zte.cms.basicdata.service;

import java.util.List;
import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;
import com.zte.cms.basicdata.dao.ICmsExtractedvafileInfoDAO;
import com.zte.cms.basicdata.service.ICmsExtractedvafileInfoDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsExtractedvafileInfoDS extends DynamicObjectBaseDS implements ICmsExtractedvafileInfoDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsExtractedvafileInfoDAO dao = null;

    public void setDao(ICmsExtractedvafileInfoDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException
    {
        log.debug("insert cmsExtractedvafileInfo starting...");
        try
        {
            dao.insertCmsExtractedvafileInfo(cmsExtractedvafileInfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsExtractedvafileInfo end");
    }

    public void updateCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException
    {
        log.debug("update cmsExtractedvafileInfo by pk starting...");
        try
        {
            dao.updateCmsExtractedvafileInfo(cmsExtractedvafileInfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsExtractedvafileInfo by pk end");
    }

    public void updateCmsExtractedvafileInfoList(List<CmsExtractedvafileInfo> cmsExtractedvafileInfoList)
            throws DomainServiceException
    {
        log.debug("update cmsExtractedvafileInfoList by pk starting...");
        if (null == cmsExtractedvafileInfoList || cmsExtractedvafileInfoList.size() == 0)
        {
            log.debug("there is no datas in cmsExtractedvafileInfoList");
            return;
        }
        try
        {
            for (CmsExtractedvafileInfo cmsExtractedvafileInfo : cmsExtractedvafileInfoList)
            {
                dao.updateCmsExtractedvafileInfo(cmsExtractedvafileInfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsExtractedvafileInfoList by pk end");
    }

    public void removeCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException
    {
        log.debug("remove cmsExtractedvafileInfo by pk starting...");
        try
        {
            dao.deleteCmsExtractedvafileInfo(cmsExtractedvafileInfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsExtractedvafileInfo by pk end");
    }

    public void removeCmsExtractedvafileInfoList(List<CmsExtractedvafileInfo> cmsExtractedvafileInfoList)
            throws DomainServiceException
    {
        log.debug("remove cmsExtractedvafileInfoList by pk starting...");
        if (null == cmsExtractedvafileInfoList || cmsExtractedvafileInfoList.size() == 0)
        {
            log.debug("there is no datas in cmsExtractedvafileInfoList");
            return;
        }
        try
        {
            for (CmsExtractedvafileInfo cmsExtractedvafileInfo : cmsExtractedvafileInfoList)
            {
                dao.deleteCmsExtractedvafileInfo(cmsExtractedvafileInfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsExtractedvafileInfoList by pk end");
    }

    public CmsExtractedvafileInfo getCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException
    {
        log.debug("get cmsExtractedvafileInfo by pk starting...");
        CmsExtractedvafileInfo rsObj = null;
        try
        {
            rsObj = dao.getCmsExtractedvafileInfo(cmsExtractedvafileInfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsExtractedvafileInfoList by pk end");
        return rsObj;
    }

    public List<CmsExtractedvafileInfo> getCmsExtractedvafileInfoByCond(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException
    {
        log.debug("get cmsExtractedvafileInfo by condition starting...");
        List<CmsExtractedvafileInfo> rsList = null;
        try
        {
            rsList = dao.getCmsExtractedvafileInfoByCond(cmsExtractedvafileInfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsExtractedvafileInfo by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsExtractedvafileInfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsExtractedvafileInfo, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsExtractedvafileInfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsExtractedvafileInfo page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsExtractedvafileInfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsExtractedvafileInfo, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsExtractedvafileInfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsExtractedvafileInfo page info by condition end");
        return tableInfo;
    }
}

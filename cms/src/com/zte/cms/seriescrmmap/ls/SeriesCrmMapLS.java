package com.zte.cms.seriescrmmap.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.series.common.SeriesConfig;
import com.zte.cms.content.series.ls.ICmsSeriesLS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.model.SeriesCrmMapConstant;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class SeriesCrmMapLS extends DynamicObjectBaseDS implements ISeriesCrmMapLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CONTENT, getClass());

    private ITargetsystemDS targetSystemDS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private ICmsSeriesDS cmsSeriesDS;
    private ISeriesCrmMapDS seriesCrmMapDS;
    private ICastrolemapDS castrolemapds;
    //新增注入
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;

    private static final String TEMPAREA_ID = "1"; // 临时区ID
    private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    private final static String SERIESCRMMAP_CNTSYNCXML = "xmlsync";
    private final static String SERIESCRMMAP_PROGRAMTYPE = "seriescrmmap";

    private static final int TARGETTYPE = 2;// EPG 类型
    private static final String BATCHID = "ucdn_task_batch_id";// 任务批次号
    private static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    private static final int SERIESCRMMAP_SYNOBJTYPE = 37;// series_castrolemap_map 对象类型

    private static final int SERIESCRMMAP_TASKTYPE_REGIST = 1;
    private static final int SERIESCRMMAP_TASKTYPE_DELETE = 3;
    
    

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public void setCastrolemapds(ICastrolemapDS castrolemapds)
    {
        this.castrolemapds = castrolemapds;
    }

    public void setCmsSeriesDS(ICmsSeriesDS cmsSeriesDS)
    {
        this.cmsSeriesDS = cmsSeriesDS;
    }

    public void setSeriesCrmMapDS(ISeriesCrmMapDS seriesCrmMapDS)
    {
        this.seriesCrmMapDS = seriesCrmMapDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public SeriesCrmMap getSeriescrmmap(Long mapindex)
    {
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
        seriesCrmMap.setMapindex(mapindex);
        try
        {
            seriesCrmMap = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in getSeriescrmmap method");
            return null;
        }
        return seriesCrmMap;
    }
    public String insertSeriescrmmap(Long[] castrolemapindexList, String seriesindex) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        Castrolemap castrolemap = new Castrolemap();
        Castrolemap castrolemapObj = null;
        OperateInfo operateInfo = null;
        String opindex = "";
        String castrolemapindex = null;
        String flag = "";
        SeriesCrmMap seriescrmmap = null;

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        if (CmsSeriesObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.not.exist"));//该连续剧内容不存在
            return rtnInfo.toString();
        }
        
        for (int i = 0; i < castrolemapindexList.length; i++)
        {
            try
            {

                opindex = (i + 1) + "";
                castrolemapindex = String.valueOf(castrolemapindexList[i]);

                castrolemap.setCastrolemapindex(castrolemapindexList[i]);
                castrolemapObj = castrolemapds.getCastrolemap(castrolemap);
                if (castrolemapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"), 
                    		ResourceMgt.findDefaultText("cast.role.not.exist"));//"操作失败"   角色不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                // 判断角色状态是否已发生改变，不为已发布状态
                else 
                {

                    // 判断连续剧内容是与角色是否已存在绑定关系
                    seriescrmmap = new SeriesCrmMap();
                    seriescrmmap.setSeriesindex(Long.valueOf(seriesindex));
                    seriescrmmap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());

                    List<SeriesCrmMap> listscrmmap = seriesCrmMapDS.getSeriesCrmMapByCond(seriescrmmap);
                    if (listscrmmap != null && listscrmmap.size() > 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"),ResourceMgt.findDefaultText("bind.series.with.role.exist"));//"操作失败"   该连续剧内容与角色已存在绑定关系
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        seriescrmmap.setSeriesid(CmsSeriesObj.getSeriesid());
                        seriescrmmap.setCastrolemapid(castrolemapObj.getCastrolemapid());
                        seriescrmmap.setStatus(SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH);

                        String mapindex = seriesCrmMapDS.insertSeriesCrmMap(seriescrmmap);
                        iSuccessed++;
                        operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.success"), ResourceMgt.findDefaultText("do.success"));//" 操作成功"
                        rtnInfo.appendOperateInfo(operateInfo);

                        CommonLogUtil.insertOperatorLog(mapindex, SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                                SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_ADD,
                                SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_ADD_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        CntTargetSync seriesTargetSync = new CntTargetSync();
                        seriesTargetSync.setObjectid(CmsSeriesObj.getSeriesid());
                        seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
                        List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
                        
                        if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
                        {
                            CntTargetSync crmTargetSync = new CntTargetSync();
                            crmTargetSync.setObjectid(castrolemapObj.getCastrolemapid());
                            crmTargetSync.setObjecttype(ObjectType.CASTROLEMAP_TYPE);
                            List<CntTargetSync> crmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(crmTargetSync);
                            if (null != crmTargetSyncList && crmTargetSyncList.size() > 0)
                            {
                                for (CntTargetSync srTargetSync:seriesTargetSyncList)
                                {
                                    for (CntTargetSync crmTSync:crmTargetSyncList)
                                    {
                                        if (srTargetSync.getTargetindex().intValue() == crmTSync.getTargetindex().intValue())
                                        {
                                            ICmsSeriesLS cmsSeriesLS = (ICmsSeriesLS) SSBBus.findDomainService("cmsSeriesLS");
                                            Targetsystem target = new Targetsystem();
                                            target.setTargetindex(srTargetSync.getTargetindex());
                                            target.setStatus(0);//正常状态
                                            List<Targetsystem> targetList = targetSystemDS.getTargetsystemByCond(target);
                                            if (null != targetList && targetList.size() > 0)
                                            {
                                                
                                                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                                                cntPlatformSync.setObjectindex(seriescrmmap.getMapindex());
                                                cntPlatformSync.setObjectid(seriescrmmap.getMappingid());
                                                cntPlatformSync.setElementid(castrolemapObj.getCastrolemapid());
                                                cntPlatformSync.setParentid(CmsSeriesObj.getSeriesid());
                                                cntPlatformSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                                                String categorySeriesMapOperdesc = cmsSeriesLS.bind2Platform(targetList.get(0),cntPlatformSync);
                                                if (categorySeriesMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                                                {
                                                    CommonLogUtil
                                                    .insertOperatorLog(
                                                            seriescrmmap.getMappingid() + ", " + targetList.get(0).getTargetid(),
                                                            SeriesConfig.SERIES_MGT,
                                                            SeriesConfig.SERIESCRM_BINDTARGET,
                                                            SeriesConfig.SERIESCRM_BINDTARGET_INFO,
                                                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }

                    }
                }
            }
            catch (Exception e)
            {

                ifail++;
                operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"), 
                		ResourceMgt.findDefaultText("bind.add.series.with.role.fail"));//"操作失败"   新增连续剧内容与角色的绑定关系失败
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (castrolemapindexList.length == 0 || ifail == castrolemapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count")+ iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + castrolemapindexList.length);
        }

        log.debug("insertProgramcrmmap end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public String batchDeleteSeriescrmmap(Long[] mapindexList, String seriesindex) throws DomainServiceException
    {
        log.debug("batchDeleteSeriescrmmap start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        OperateInfo operateInfo = null;
        String opindex = "";
        String mapindex = null;
        String flag = "";
        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        
        for (int i = 0; i < mapindexList.length; i++)
        {
            try
            {

                opindex = (i + 1) + "";
                mapindex = String.valueOf(mapindexList[i]);

                seriesCrmMap.setMapindex(mapindexList[i]);
                seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);
                
                if (seriesCrmMapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                            ResourceMgt.findDefaultText("bind.relation.notexist"));//"操作失败"   绑定关系不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }else 
                {
                
                    CntTargetSync cntTargetSync = new CntTargetSync();
                    cntTargetSync.setObjectid(seriesCrmMapObj.getMappingid());
                    cntTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                    List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                    boolean isPublished = false;
                    if(null != cntTargetSyncList && cntTargetSyncList.size() > 0)
                    {
                        
                        for (CntTargetSync targetSync:cntTargetSyncList)
                        {
                            if (StatusTranslator.STATUS_PUBLISHING == targetSync.getStatus() || StatusTranslator.STATUS_PUBLISHED == targetSync.getStatus()||StatusTranslator.STATUS_MODIFYPUBFAILED == targetSync.getStatus())
                            {
                                ifail++;
                                operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"),"角色关联已发布不允许删除");
                                rtnInfo.appendOperateInfo(operateInfo);
                                isPublished = true;
                                break;
                            }
                        }
                        
                        if (!isPublished)
                        {
                            for (int j = 0; j < cntTargetSyncList.size(); j++)
                            {
                                cntTargetSync = cntTargetSyncList.get(j);
                                cntTargetSyncDS.removeCntTargetSync(cntTargetSync);

                                // 获取平台发布信息
                                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                                cntPlatformSync.setObjectid(cntTargetSync.getObjectid());
                                cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                                cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                                cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);

                                // 查询子网元发布信息
                                CntTargetSync childTargetSync = new CntTargetSync();
                                childTargetSync.setObjectid(cntTargetSync.getObjectid());
                                childTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                                childTargetSync.setObjecttype(cntTargetSync.getObjecttype());
                                List<CntTargetSync> childTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(childTargetSync);

                                if (childTargetSyncList.size() < 1)
                                {
                                    // 删除主平台关联发布信息
                                    cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                                    // 更新电视剧与其他对象的mapping是否有关联平台发布，没有就删除它们之间的mapping关系（暂不实现）
                                    
                                }
                                else
                                {
                                    // 更改主平台关联发布状态
                                    int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                                    for (int k = 0; k < childTargetSyncList.size(); k++)
                                    {
                                        childTargetSyncStatus[j] = childTargetSyncList.get(k).getStatus();
                                    }
                                    cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                                }
                            }
                        }
                    }
                    
                    if (!isPublished)
                    {
                        seriesCrmMapDS.removeSeriesCrmMap(seriesCrmMapObj);
                        iSuccessed++;
                        operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.success"), ResourceMgt.findDefaultText("do.success"));//" 操作成功"
                        rtnInfo.appendOperateInfo(operateInfo);
        
                        CommonLogUtil.insertOperatorLog(String.valueOf(mapindex), SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                                SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_DEL,
                                SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                }  
            }
            catch (Exception e)
            {
                ifail++;
                operateInfo = new OperateInfo(opindex, mapindex,ResourceMgt.findDefaultText("do.fail"), 
                		ResourceMgt.findDefaultText("bind.delete.series.with.role.fail"));//"操作失败"   删除连续剧内容与角色的绑定关系失败
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (mapindexList.length == 0 || ifail == mapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + mapindexList.length);
        }

        log.debug("batchDeleteSeriescrmmap end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            seriesCrmMap.setSeriesid(EspecialCharMgt.conversion(seriesCrmMap.getSeriesid()));
            seriesCrmMap.setCastrolemapid(EspecialCharMgt.conversion(seriesCrmMap.getCastrolemapid()));

            tableInfo = seriesCrmMapDS.pageInfoQuery(seriesCrmMap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;

    }

    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            seriesCrmMap.setSeriesid(EspecialCharMgt.conversion(seriesCrmMap.getSeriesid()));
            seriesCrmMap.setCastrolemapid(EspecialCharMgt.conversion(seriesCrmMap.getCastrolemapid()));

            tableInfo = seriesCrmMapDS.pageInfoQuery(seriesCrmMap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;
    }

    public String batchPublishList(Long[] mapindexList, String seriesindex) throws Exception
    {
        log.debug("batchPublishList start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        OperateInfo operateInfo = null;
        String opindex = "";
        String mapindex = null;
        String flag = "";

        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        if (CmsSeriesObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.not.exist"));//该连续剧内容不存在
            return rtnInfo.toString();
        }
        // 判断连续剧内容是否已发生改变，不为待发布，新增同步成功，修改同步成功，修改同步失败状态
        else if (CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.status.changed"));//该连续剧内容的状态已发生改变
            return rtnInfo.toString();
        }

        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(TARGETTYPE);
        // 获取发布系统EPG的记录
        List<Targetsystem> listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
        if (listtsystem == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.sync.object.notexist"));//"同步目标不存在"
            return rtnInfo.toString();
        }
        targetSystem = listtsystem.get(0);
        // 获取挂载点
        String mountPoint = GlobalConstants.getMountPoint();
        if (mountPoint == null || mountPoint.trim().equals(""))
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.mountpoint.fail"));//"获取挂载点失败"
            return rtnInfo.toString();
        }
        // 获取临时区绑定空间的路径
        String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
        if ("".equals(tempAreaPath))
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.temporarydirectory.fail"));//"获取临时区目录失败"
            return rtnInfo.toString();
        }

        for (int i = 0; i < mapindexList.length; i++)
        {
            try
            {
                opindex = (i + 1) + "";

                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

                mapindex = String.valueOf(mapindexList[i]);

                seriesCrmMap.setMapindex(mapindexList[i]);
                seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);

                if (seriesCrmMapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"),
                    		ResourceMgt.findDefaultText("bind.series.with.role.notexist"));//"操作失败"   连续剧内容与角色的绑定关系不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                // 只有状态为"待发布" "新增同步失败" "取消同步成功"的绑定关系才能发布
                else if (seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                        || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_FAIL
                        || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_SUCCESS)
                {

                    String fileName = createSynXML(seriesCrmMapObj, SERIESCRMMAP_TASKTYPE_REGIST, tempAreaPath,
                            mountPoint);
                    if (null == fileName)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"),
                        		ResourceMgt.findDefaultText("cast.xmlsyncfile.fail"));//"操作失败"   生成同步xml文件失败
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    CntSyncTask cntSyncTask = new CntSyncTask();
                    cntSyncTask.setStatus(1);
                    cntSyncTask.setPriority(2);
                    cntSyncTask.setCorrelateid(correlateid);
                    cntSyncTask.setContentmngxmlurl(fileName);              
                    cntSyncTask.setDestindex(targetSystem.getTargetindex());

                    cntsyncTaskDS.insertCntSyncTask(cntSyncTask);

                    seriesCrmMapObj.setStatus(SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZING);
                    seriesCrmMapDS.updateSeriesCrmMap(seriesCrmMapObj);

                    iSuccessed++;
                    CommonLogUtil.insertOperatorLog(mapindex, SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_PUBLISH,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.success"), ResourceMgt.findDefaultText("do.success"));//" 操作成功"
                    rtnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                    		ResourceMgt.findDefaultText("bind.relation.status.wrong"));//"操作失败"   绑定关系的状态不正确
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
            }
            catch (Exception e)
            {
                ifail++;
                operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                		ResourceMgt.findDefaultText("pub.series.cnt.bind.with.role.relation.fail"));//"操作失败"   发布连续剧内容与角色的绑定关系失败
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (mapindexList.length == 0 || ifail == mapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + mapindexList.length);
        }

        log.debug("batchPublishList end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public String batchCancelPublishList(Long[] mapindexList, String seriesindex) throws Exception
    {
        log.debug("batchCancelPublishList start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        OperateInfo operateInfo = null;
        String opindex = "";
        String mapindex = null;
        String flag = "";

        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        if (CmsSeriesObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.not.exist"));//该连续剧内容不存在
            return rtnInfo.toString();
        }
        // 判断连续剧内容是否已发生改变，不为待发布,新增同步成功，修改同步成功，修改同步失败状态
        else if (CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.status.changed"));//该连续剧内容的状态已发生改变
            return rtnInfo.toString();
        }

        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(TARGETTYPE);
        // 获取发布系统EPG的记录
        List<Targetsystem> listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
        if (listtsystem == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.sync.object.notexist"));//"同步目标不存在"
            return rtnInfo.toString();
        }
        targetSystem = listtsystem.get(0);
        // 获取挂载点
        String mountPoint = GlobalConstants.getMountPoint();
        if (mountPoint == null || mountPoint.trim().equals(""))
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.mountpoint.fail"));//"获取挂载点失败"
            return rtnInfo.toString();
        }
        // 获取临时区绑定空间的路径
        String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
        if ("".equals(tempAreaPath))
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.temporarydirectory.fail"));//"获取临时区目录失败"
            return rtnInfo.toString();
        }

        for (int i = 0; i < mapindexList.length; i++)
        {
            try
            {
                opindex = (i + 1) + "";

                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

                mapindex = String.valueOf(mapindexList[i]);

                seriesCrmMap.setMapindex(mapindexList[i]);
                seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);

                if (seriesCrmMapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex,ResourceMgt.findDefaultText("do.fail") ,
                    		ResourceMgt.findDefaultText("bind.series.with.role.notexist"));//"操作失败"   该连续剧内容与角色的绑定关系不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                // 只有状态为"新增同步成功" "取消同步同步失败" 的绑定关系取消发布
                else if (seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                        || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_FAIL)
                {

                    String fileName = createSynXML(seriesCrmMapObj, SERIESCRMMAP_TASKTYPE_DELETE, tempAreaPath,
                            mountPoint);
                    if (null == fileName)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                        		ResourceMgt.findDefaultText("cast.xmlsyncfile.fail"));//"操作失败"   生成同步xml文件失败
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    CntSyncTask cntSyncTask = new CntSyncTask();
                    cntSyncTask.setStatus(1);
                    cntSyncTask.setPriority(2);
                    cntSyncTask.setCorrelateid(correlateid);
                    cntSyncTask.setContentmngxmlurl(fileName);         
                    cntSyncTask.setDestindex(targetSystem.getTargetindex());

                    cntsyncTaskDS.insertCntSyncTask(cntSyncTask);

                    seriesCrmMapObj.setStatus(SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZING);
                    seriesCrmMapDS.updateSeriesCrmMap(seriesCrmMapObj);
                    iSuccessed++;

                    CommonLogUtil.insertOperatorLog(mapindex, SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_CANCELPUBLISH,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_CANCELPUBLISH__INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.success"), ResourceMgt.findDefaultText("do.success"));//" 操作成功"
                    rtnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                    		ResourceMgt.findDefaultText("series.cnt.bind.with.role.status.wrong"));//"操作失败"   连续剧内容与角色的绑定关系的状态不正确
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
            }
            catch (Exception e)
            {

                ifail++;
                operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.fail"), 
                		ResourceMgt.findDefaultText("bind.add.series.with.role.fail"));//"操作失败"   取消连续剧内容与角色的绑定关系失败
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (mapindexList.length == 0 || ifail == mapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + mapindexList.length);
        }

        log.debug("batchCancelPublishList end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public String publish(Long mapindex, String seriesindex) throws Exception
    {
        log.debug("publish start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        if (CmsSeriesObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.not.exist"));//该连续剧内容不存在
            return rtnInfo.toString();
        }
        // 判断连续剧内容是否已发生改变，不为待发布,新增同步成功，修改同步成功，修改同步失败状态
        else if (CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.status.changed"));//该连续剧内容的状态已发生改变
            return rtnInfo.toString();
        }

        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(TARGETTYPE);
        try
        {
            // 获取发布系统EPG的记录
            List<Targetsystem> listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.sync.object.notexist"));//"同步目标不存在"
                return rtnInfo.toString();
            }
            targetSystem = listtsystem.get(0);
            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.mountpoint.fail"));//"获取挂载点失败"
                return rtnInfo.toString();
            }
            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.temporarydirectory.fail"));//"获取临时区目录失败"
                return rtnInfo.toString();
            }
            String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();
            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

            seriesCrmMap.setMapindex(mapindex);
            seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);

            if (seriesCrmMapObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.notexist"));//"绑定关系不存在"
                return rtnInfo.toString();
            }
            // 只有状态为"待发布" "新增同步失败" "取消同步成功"的绑定关系才能发布
            else if (seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                    || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_FAIL
                    || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_SUCCESS)
            {

                String fileName = createSynXML(seriesCrmMapObj, SERIESCRMMAP_TASKTYPE_REGIST, tempAreaPath, mountPoint);
                if (null == fileName)
                {
                    rtnInfo.setFlag("1");
                    rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.xmlsyncfile.fail"));//"生成同步xml文件失败"
                    return rtnInfo.toString();
                }

                CntSyncTask cntSyncTask = new CntSyncTask();
                cntSyncTask.setStatus(1);
                cntSyncTask.setPriority(2);
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(fileName);
                cntSyncTask.setDestindex(targetSystem.getTargetindex());
                cntsyncTaskDS.insertCntSyncTask(cntSyncTask);

                seriesCrmMapObj.setStatus(SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZING);
                seriesCrmMapDS.updateSeriesCrmMap(seriesCrmMapObj);

                CommonLogUtil.insertOperatorLog(String.valueOf(mapindex), SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                        SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_PUBLISH,
                        SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_PUBLISH_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                rtnInfo.setFlag("0");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功"
            }
            else
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.status.wrong"));//"绑定关系的状态不正确"
            }

        }
        catch (Exception e)
        {
            log.error("Error occurred in publish method:" + e);
            throw new DomainServiceException(e);
        }

        log.debug("publish end ");
        return rtnInfo.toString();
    }

    public String cancelPublish(Long mapindex, String seriesindex) throws Exception
    {
        log.debug("cancelPublish start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        if (CmsSeriesObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.not.exist"));//该连续剧内容不存在
            return rtnInfo.toString();
        }
        // 判断连续剧内容是否已发生改变，不为待发布，新增同步成功，修改同步成功，修改同步失败状态
        else if (CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_WAITPUBLISH
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS
                && CmsSeriesObj.getStatus() != SeriesCrmMapConstant.SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("series.cnt.status.changed"));//该连续剧内容的状态已发生改变
            return rtnInfo.toString();
        }

        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(TARGETTYPE);
        try
        {
            // 获取发布系统EPG的记录
            List<Targetsystem> listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.sync.object.notexist"));//"同步目标不存在"
                return rtnInfo.toString();
            }
            targetSystem = listtsystem.get(0);
            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.mountpoint.fail"));//"获取挂载点失败"
                return rtnInfo.toString();
            }
            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.get.temporarydirectory.fail"));//"获取临时区目录失败"
                return rtnInfo.toString();
            }
            String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();
            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

            seriesCrmMap.setMapindex(mapindex);
            seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);

            if (seriesCrmMapObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.notexist"));//"绑定关系不存在"
                return rtnInfo.toString();
            }
            // 只有状态为"新增同步成功" "取消同步同步失败" 的绑定关系取消发布
            else if (seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS
                    || seriesCrmMapObj.getStatus() == SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_FAIL)
            {

                String fileName = createSynXML(seriesCrmMapObj, SERIESCRMMAP_TASKTYPE_DELETE, tempAreaPath, mountPoint);
                if (null == fileName)
                {
                    rtnInfo.setFlag("1");
                    rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.xmlsyncfile.fail"));//"生成同步xml文件失败"
                    return rtnInfo.toString();
                }
                CntSyncTask cntSyncTask = new CntSyncTask();
                cntSyncTask.setStatus(1);
                cntSyncTask.setPriority(2);
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(fileName);
                cntSyncTask.setDestindex(targetSystem.getTargetindex());

                cntsyncTaskDS.insertCntSyncTask(cntSyncTask);

                seriesCrmMapObj.setStatus(SeriesCrmMapConstant.SERIESCRMMAP_STATUS_CANCELSYNCHRONIZING);
                seriesCrmMapDS.updateSeriesCrmMap(seriesCrmMapObj);

                CommonLogUtil.insertOperatorLog(String.valueOf(mapindex), SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                        SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_CANCELPUBLISH,
                        SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_CANCELPUBLISH__INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                rtnInfo.setFlag("0");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功"
            }
            else
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.status.wrong"));//"绑定关系的状态不正确"
            }

        }
        catch (Exception e)
        {
            log.error("Error occurred in cancelPublish method:" + e);
            throw new DomainServiceException(e);
        }

        log.debug("cancelPublish end ");
        return rtnInfo.toString();
    }

    public String deleteSeriescrmmap(Long mapindex, String seriesindex) throws DomainServiceException
    {
        log.debug("deleteSeriescrmmap start ");
        ReturnInfo rtnInfo = new ReturnInfo();

        SeriesCrmMap seriesCrmMapObj = null;
        SeriesCrmMap seriesCrmMap = new SeriesCrmMap();

        CmsSeries CmsSeriesObj = null;
        CmsSeries cmsseries = new CmsSeries();
        cmsseries.setSeriesindex(Long.valueOf(seriesindex));
        CmsSeriesObj = cmsSeriesDS.getCmsSeries(cmsseries);
        
        try
        {
            
            seriesCrmMap.setMapindex(mapindex);
            seriesCrmMapObj = seriesCrmMapDS.getSeriesCrmMap(seriesCrmMap);
            
            if (seriesCrmMapObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.notexist"));//"绑定关系不存在"
            }else 
            {
            
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectid(seriesCrmMapObj.getMappingid());
                cntTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                boolean isPublished = false;
                if(null != cntTargetSyncList && cntTargetSyncList.size() > 0)
                {
                    
                    for (CntTargetSync targetSync:cntTargetSyncList)
                    {
                        if (StatusTranslator.STATUS_PUBLISHING == targetSync.getStatus() || StatusTranslator.STATUS_PUBLISHED == targetSync.getStatus()||StatusTranslator.STATUS_MODIFYPUBFAILED == targetSync.getStatus())
                        {
                            rtnInfo.setFlag("1");
                            rtnInfo.setReturnMessage("角色关联已发布不允许删除");//"绑定关系的状态不正确"
                            isPublished = true;
                            break;
                        }
                    }
                    
                    if (!isPublished)
                    {
                        for (int i = 0; i < cntTargetSyncList.size(); i++)
                        {
                            cntTargetSync = cntTargetSyncList.get(i);
                            cntTargetSyncDS.removeCntTargetSync(cntTargetSync);

                            // 获取平台发布信息
                            CntPlatformSync cntPlatformSync = new CntPlatformSync();
                            cntPlatformSync.setObjectid(cntTargetSync.getObjectid());
                            cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                            cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                            cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);

                            // 查询子网元发布信息
                            CntTargetSync childTargetSync = new CntTargetSync();
                            childTargetSync.setObjectid(cntTargetSync.getObjectid());
                            childTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                            childTargetSync.setObjecttype(cntTargetSync.getObjecttype());
                            List<CntTargetSync> childTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(childTargetSync);

                            if (childTargetSyncList.size() < 1)
                            {
                                // 删除主平台关联发布信息
                                cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                                // 更新电视剧与其他对象的mapping是否有关联平台发布，没有就删除它们之间的mapping关系（暂不实现）
                                
                            }
                            else
                            {
                                // 更改主平台关联发布状态
                                int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                                for (int j = 0; j < childTargetSyncList.size(); j++)
                                {
                                    childTargetSyncStatus[j] = childTargetSyncList.get(j).getStatus();
                                }
                                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }
                }
                
                if (!isPublished)
                {
                    seriesCrmMapDS.removeSeriesCrmMap(seriesCrmMapObj);
                    rtnInfo.setFlag("0");
                    rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功"
    
                    CommonLogUtil.insertOperatorLog(String.valueOf(mapindex), SeriesCrmMapConstant.MGTTYPE_SERIESCRMMAP,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_DEL,
                            SeriesCrmMapConstant.OPERTYPE_SERIESCRMMAP_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in deleteSeriescrmmap method:" + e);
            throw new DomainServiceException(e);

        }

        log.debug("deleteSeriescrmmap end ");
        return rtnInfo.toString();
    }

    /**
     * @param SeriesCrmMap 同步对象
     * @param syncType 同步类型 1 新增同步 2修改同步 3 取消删除
     * @param tempAreaPath 临时区目录
     * @param mountPoint //挂载点
     * @return 同步生成的xml文件的全路径
     * @throws Exception
     */
    private String createSynXML(SeriesCrmMap seriescrmmap, int syncType, String tempAreaPath, String mountPoint)
            throws Exception
    {

        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = date.getTime() + "_seriescrmmap.xml";
        String synctype = "";
        String tempFilePath = tempAreaPath + File.separator + SERIESCRMMAP_CNTSYNCXML + File.separator + dir
                + File.separator + SERIESCRMMAP_PROGRAMTYPE + File.separator;
        String mountAndTempfilePath = mountPoint + tempFilePath;
        String retPath = filterSlashStr(tempFilePath + fileName);
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        adiElement.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }
        
        //获得OBJECTID
        String objectId = getObjectId(seriescrmmap.getMapindex().toString());

        // 必填属性
        objectElement = objectsElement.addElement("Mapping");
        objectElement.addAttribute("ParentType", "Series");
        objectElement.addAttribute("ElementType", "CastRoleMap");
        objectElement.addAttribute("ParentID", seriescrmmap.getSeriesid());
        objectElement.addAttribute("ObjectID", objectId);
        objectElement.addAttribute("ElementID", seriescrmmap.getCastrolemapid());
        objectElement.addAttribute("Action", synctype);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (seriescrmmap.getSequence() == null || "".equals(seriescrmmap.getSequence()))
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(seriescrmmap.getSequence().toString());
        }

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("createScheduleXml exception:" + e);
            retPath = null;
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("createScheduleXml exception:" + e);
                    retPath = null;
                    throw e;
                }
            }
        }

        return retPath;
    }
    
    /**
     * 根据mapping对象index生成ObjectId
     * @param index mapping对象index
     * @return ObjectId
     */
    private String getObjectId(String index)
    {
        String objectId="";
        objectId = String.format("%02d",Integer.valueOf(SERIESCRMMAP_SYNOBJTYPE)) + String.format("%030d",Integer.valueOf(index));
        return objectId;
        
    }

    // 获取临时区目录
    private String getTempAreaPath() throws DomainServiceException
    {
        log.debug("getTempAreaPath  start");
        String stroageareaPath = "";
        // 获取临时区路径
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            stroageareaPath = storageareaLs.getAddress(TEMPAREA_ID);// 临时区
            if (null == stroageareaPath || "".equals(stroageareaPath) || (TEMPAREA_ERRORSiGN).equals(stroageareaPath))
            {// 获取临时区地址失败
                return "";
            }
        }
        catch (Exception e)
        {
            log.error("Error occured in getTempAreaPath method");
            throw new DomainServiceException(e);
        }

        log.debug("getTempAreaPath  end");
        return stroageareaPath;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 获取时间 如"20111002"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    
    public String publishSeriesCastTargetMap(Long syncindex) throws Exception
    {
        log.debug("publish series castrolemap mapping start...");  
        String bingObject = "";
        CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
         mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
             return "1:没有可发布的mapping关系 ";
         }
       int type = mapping.getObjecttype();
       if(type == 37)//表示服务与点播内容的绑定关系
       {
           bingObject = "角色";
          SeriesCrmMap scmap = new SeriesCrmMap();
           scmap.setMapindex(mapping.getObjectindex());  
           scmap = seriesCrmMapDS.getSeriesCrmMap(scmap);        
           if(scmap == null){
               return "1:连续剧与角色关联关系不存在 ";
           }
           
           Castrolemap cast = new Castrolemap();          
           cast.setCastrolemapid(mapping.getElementid());           
           cast = castrolemapds.getCastrolemapByCond(cast).get(0);
          if(cast  == null){
              return "1:角色不存在 ";   
          }        
           mapping.setReserve02(cast.getCastrolemapcode());     
           mapping.setReserve01(scmap.getSequence().longValue());          
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getParentid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series == null){
           return "1:连续剧不存在 ";
       }
       String parentcode =series.getCpcontentid();  //添加父节点的文广code
        mapping.setCancelpubtime(series.getCpcontentid());    
              
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:连续剧与角色的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:连续剧与角色的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
       
       CntTargetSync parent = new CntTargetSync();
       parent.setObjectid(mapping.getParentid());
       parent.setTargetindex(mapping.getTargetindex());
       parent =cntTargetSyncDS.getCntTargetSync(parent);
       if(parent == null){
           return "1:连续剧的发布数据不存在 ";
       }
       if(parent.getStatus()!=300 && parent.getStatus()!=500){
           return "1:连续剧在网元"+targetid+"上的发布状态不正确 ";
       }
       CntTargetSync element = new CntTargetSync();
       element.setObjectid(mapping.getElementid());
       element.setTargetindex(mapping.getTargetindex());
       element = cntTargetSyncDS.getCntTargetSync(element);
       if(element == null){
           return "1:角色的发布数据不存在 ";
       }
       if(element.getStatus()!=300 && parent.getStatus()!=500){
           return "1:角色在网元"+targetid+"上的发布状态不正确 ";      
       }

       CntPlatformSync  platformSync = new CntPlatformSync();
       platformSync.setSyncindex(mapping.getRelateindex());
       platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
       int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 0 
               || targetResult == 30 
               || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
       }else {                   
               return  "1:连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ";      
       }
       if(xmlPath == null){
           return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementcode = mapping.getReserve02();
        String  elementid = mapping.getElementid();//获得栏目文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       modTargetSynStatus( mapping, 1);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.SERIES_CAST_MAPPING_PUBLISH,
       SeriesConfig.SERIES_CAST_MAPPING_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("publish  series castrolemap  mapping end...");
       String returnInfo ="0:操作成功";
       return returnInfo;    
    }
    
   private String publishSeriesCastTarget(Long syncindex,Long batchid,int firTargetType)throws Exception{

       log.debug("publish series castrolemap mapping start...");  
       String bingObject = "";
       CntTargetSync mapping = new CntTargetSync();
        mapping.setSyncindex(syncindex);      
        mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
        if(mapping == null){
            return "1:没有可发布的mapping关系 ";
        }
      int type = mapping.getObjecttype();
      if(type == 37)//表示服务与点播内容的绑定关系
      {
          bingObject = "角色";
         SeriesCrmMap scmap = new SeriesCrmMap();
          scmap.setMapindex(mapping.getObjectindex());  
          scmap = seriesCrmMapDS.getSeriesCrmMap(scmap);        
          if(scmap == null){
              return "1:连续剧与角色关联关系不存在 ";
          }
          
          Castrolemap cast = new Castrolemap();          
          cast.setCastrolemapid(mapping.getElementid());           
          cast = castrolemapds.getCastrolemapByCond(cast).get(0);
         if(cast  == null){
             return "1:角色不存在 ";   
         }        
          mapping.setReserve02(cast.getCastrolemapcode());     
          mapping.setReserve01(scmap.getSequence().longValue());          
      }
      CmsSeries series = new CmsSeries();
      series.setSeriesid(mapping.getParentid());
      series = cmsSeriesDS.getCmsSeriesById(series) ;  
      if(series == null){
          return "1:连续剧不存在 ";
      }
      String parentcode =series.getCpcontentid();  //添加父节点的文广code
       mapping.setCancelpubtime(series.getCpcontentid());     
 
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargetindex(mapping.getTargetindex());
        Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
        if(targetSystemExist == null){
            return "1:连续剧与角色的关联关系发布的网元不存在 ";
        }
        if(targetSystemExist.getStatus() !=0){
            return "1:连续剧与角色的关联关系发布的网元状态不正确 ";
        }       
      int targettype =targetSystemExist.getTargettype();
      String targetid = targetSystemExist.getTargetid();
      
      CntTargetSync parent = new CntTargetSync();
      parent.setObjectid(mapping.getParentid());
      parent.setTargetindex(mapping.getTargetindex());
      parent =cntTargetSyncDS.getCntTargetSync(parent);
      if(parent == null){
          return "1:连续剧的发布数据不存在 ";
      }
      if(parent.getStatus()!=300 && parent.getStatus()!=500){
          return "1:连续剧在网元"+targetid+"上的发布状态不正确 ";
      }
      CntTargetSync element = new CntTargetSync();
      element.setObjectid(mapping.getElementid());
      element.setTargetindex(mapping.getTargetindex());
      element = cntTargetSyncDS.getCntTargetSync(element);
      if(element == null){
          return "1:角色的发布数据不存在 ";
      }
      if(element.getStatus()!=300 && parent.getStatus()!=500){            
              return "1:角色在网元"+targetid+"上的发布状态不正确 ";
      }
      
      CntPlatformSync  platformSync = new CntPlatformSync();
      platformSync.setSyncindex(mapping.getRelateindex());
      platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
      int platformNum = (int)platformSync.getPlatform();
      String xmlPath = "";
      int targetResult = mapping.getOperresult();
      Map map = new HashMap<String, List>(); 
      List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
      seriesMapList.add(mapping);
      map.put("seriesmapping", seriesMapList);
      if(targetResult == 0 
              || targetResult == 30 
              || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
      {       //生成xml文件    
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
      }else {                   
      
              return  "1:连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ";  
      }
      if(xmlPath == null){
          return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
      }
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = mapping.getTargetindex();
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = mapping.getObjectindex();
       String objectid = mapping.getObjectid();
       int objecttype = mapping.getObjecttype();
       String parentid = mapping.getParentid();
       String elementcode = mapping.getReserve02();
       String  elementid = mapping.getElementid();//获得栏目文广code
       insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
       if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
           Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
       }else if(firTargetType == targettype){
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       }else{
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
       }
      modTargetSynStatus( mapping, 1);
      CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
      SeriesConfig.SERIES_CAST_MAPPING_PUBLISH,
      SeriesConfig.SERIES_CAST_MAPPING_PUBLISH_INFO,
      SeriesConfig.RESOURCE_OPERATION_SUCCESS);
      log.debug("publish  series castrolemap  mapping end...");
      String returnInfo ="0:发布到目标系统"+targetid+":操作成功 ";
      return returnInfo;    
   
       
   }

    public String publishSeriesCastPlatformMap(Long syncindex) throws Exception
    {
        log.debug("publish platform for series castrolemapmapping start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  

        CntPlatformSync platformsync = new  CntPlatformSync();
        platformsync.setSyncindex(syncindex);
        platformsync = cntPlatformSyncDS.getCntPlatformSync(platformsync);
        if(platformsync == null){
            return  "1:平台发布数据不存在 ";            
        }
        int platformStatus = platformsync.getStatus();
        if(platformStatus== 200 || platformStatus == 300 ){
            return   "1:平台发布状态不正确 ";        
        }  

        CntTargetSync targetSyn = new CntTargetSync();
        targetSyn.setRelateindex(syncindex);
        List<CntTargetSync>  mappingList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyn);
        if(mappingList == null || mappingList.size()==0){
            return  "1:该关联关系没有关联网元 ";
        }
        
        String BMSPub = getBMSPub(mappingList);
        if(BMSPub.substring(0,1).equals("1")){
            return BMSPub;
        }
        
        Long batchid = null;
        
        int firTargetType = findFirTargettype(mappingList,0);
        if(firTargetType != 99)
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        int size = mappingList.size();
        for(int i=0; i<size; i++){
            CntTargetSync map = mappingList.get(i);
            Long index = map.getSyncindex();   
            
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(map.getTargetindex());
            Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
            String targetid = targetSystemExist.getTargetid();

            if(map.getStatus() == 200 || map.getStatus() == 300 ){//对发布中或者发布的成功的网元不做操作
                allresult.append("连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ");  
                continue;
            }
            //String rstInfo= publishSeriesCastTarget(index); 
            String rstInfo= publishSeriesCastTarget(index,batchid,firTargetType);
            if ( rstInfo.substring(0, 1).equals("0"))
            {
                successFlg = true;
                allresult.append(rstInfo.substring(2));
            }else
            {
                allresult.append(rstInfo.substring(2));
         }
            }                       
         log.debug("publish platform for  series castrolemap mapping end...");
        if(successFlg){
            return "0:"+allresult.toString();         
        }else{//全部失败
            return "1:"+allresult.toString();            
        }

    }

    public String publishSeriesCastBatchMap(List<CmsSeries> mappingList) throws Exception
    {
        log.debug("publish batch for series castrolemap mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
        int size = mappingList.size();     
        for(CmsSeries mapping:mappingList){
            Long syncindex = mapping.getSyncindex();
            retInfo=  publishSeriesCastPlatformMap(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量:" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量:" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量:" + iSuccessed + "  失败数量:" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("publish batch for series castrolemap mapping end...");
        return returnInfo.toString();  
    }

    public String delPublishSeriesCastTargetMap(Long syncindex) throws Exception
    {
        log.debug("delete publish series castrolemap mapping  for target start...");  
        String bingObject = "";
       CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
        mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
            return "1:没有可发布的mapping关系";
         }
       int type = mapping.getObjecttype();
       if(type == 37)//表示服务与点播内容的绑定关系
       {
           bingObject = "角色";
           SeriesCrmMap scmap = new SeriesCrmMap();
            scmap.setMapindex(mapping.getObjectindex());  
            scmap = seriesCrmMapDS.getSeriesCrmMap(scmap);        
            if(scmap == null){
                return "1:连续剧与角色关联关系不存在 ";
            }
            
            Castrolemap cast = new Castrolemap();          
            cast.setCastrolemapid(mapping.getElementid());           
            cast = castrolemapds.getCastrolemapByCond(cast).get(0);
           if(cast  != null){             
                mapping.setReserve02(cast.getCastrolemapcode());     
                mapping.setReserve01(scmap.getSequence().longValue());               
           }                                
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getParentid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series!= null){
            mapping.setCancelpubtime(series.getCpcontentid());//把连续剧文广code放到取消发布时间字段中先
       }
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:连续剧与角色的关联关系发布的网元不存在";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:连续剧与角色的关联关系发布的网元状态不正确";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
         CntPlatformSync  platformSync = new CntPlatformSync();
         platformSync.setSyncindex(mapping.getRelateindex());
         platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
         int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 20       
               || targetResult == 90)// 20新增同步成功// 90取消同步失败
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
       }else {                   
             
               return  "1:连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ";  
       }
       if(xmlPath == null){
           return "1:从目标系统"+targetid+"取消发布:生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String elementcode = mapping.getReserve02();//获得栏目文广code
        String parentcode = mapping.getCancelpubtime();//获得连续剧文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       modTargetSynStatus( mapping, 3);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.SERIES_CAST_MAPPING_DEL_PUBLISH,
       SeriesConfig.SERIES_CAST_MAPPING_DEL_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("delete publish  series castrolemap mapping  for target end...");
       String returnInfo ="0: 操作成功 ";
       return returnInfo;   
    }
    
    private  String delPublishSeriesCastTarget(Long syncindex,Long batchid,int firTargetType) throws Exception{

        log.debug("delete publish series castrolemap mapping  for target start...");  
        String bingObject = "";
       CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
        mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
            return "1:没有可发布的mapping关系 ";
         }
       int type = mapping.getObjecttype();
       if(type == 37)//表示服务与点播内容的绑定关系
       {
           bingObject = "角色";
           SeriesCrmMap scmap = new SeriesCrmMap();
            scmap.setMapindex(mapping.getObjectindex());  
            scmap = seriesCrmMapDS.getSeriesCrmMap(scmap);        
            if(scmap == null){
                return "1:连续剧与角色关联关系不存在 ";
            }
            
            Castrolemap cast = new Castrolemap();          
            cast.setCastrolemapid(mapping.getElementid());           
            cast = castrolemapds.getCastrolemapByCond(cast).get(0);
           if(cast  != null){             
                mapping.setReserve02(cast.getCastrolemapcode());     
                mapping.setReserve01(scmap.getSequence().longValue());               
           }                                
       }else{
           return "1:连续剧与角色关联关系类型不匹配 ";
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getParentid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series!= null){
            mapping.setCancelpubtime(series.getCpcontentid());//把连续剧文广code放到取消发布时间字段中先
       }
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:连续剧与角色的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:连续剧与角色的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
         CntPlatformSync  platformSync = new CntPlatformSync();
         platformSync.setSyncindex(mapping.getRelateindex());
         platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
         int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 20       
               || targetResult == 90)// 20新增同步成功// 90取消同步失败
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
       }else {                   
                    
               return  "1:连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ";  
       }
       if(xmlPath == null){
           return "1:从目标系统"+targetid+"取消发布:生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String elementcode = mapping.getReserve02();//获得栏目文广code
        String parentcode = mapping.getCancelpubtime();//获得连续剧文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
       
        if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
            Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
        }else if(firTargetType == targettype){
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
        }else{
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
        }
        //insertSyncTask(taskindex,xmlPath,correlateid,targetindex); //插入同步任务
       
       modTargetSynStatus( mapping, 3);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.SERIES_CAST_MAPPING_DEL_PUBLISH,
       SeriesConfig.SERIES_CAST_MAPPING_DEL_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("delete publish  series castrolemap mapping  for target end...");
       String returnInfo ="0:从目标系统"+targetid+"取消发布: 操作成功 ";
       return returnInfo;   
    
        
    }

    public String delPublishSeriesCastPlatformMap(Long syncindex) throws Exception
    {
        
        log.debug("delete publish platform for series castrolemap mapping start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
        
        CntPlatformSync platformsync = new  CntPlatformSync();
        platformsync.setSyncindex(syncindex);
        platformsync = cntPlatformSyncDS.getCntPlatformSync(platformsync);
        if(platformsync == null){
            return  "1:平台发布数据不存在 ";            
        }
        int platformStatus = platformsync.getStatus();
        if(platformStatus== 0 || platformStatus == 200 || platformStatus == 400 ){
            return   "1:平台发布状态不正确 ";        
        }          
        
        CntTargetSync targetSyn = new CntTargetSync();
        targetSyn.setRelateindex(syncindex);
        List<CntTargetSync>  mappingList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyn);
        if(mappingList == null || mappingList.size()==0){
            return  "1:该关联关系没有关联网元 ";
        }
        
        Long batchid = null;
        
        int firTargetType = findFirTargettype(mappingList,2);
        if(firTargetType != 99)
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        int size = mappingList.size();
        for(int i=0; i<size; i++){
            CntTargetSync map = mappingList.get(i);
            Long index = map.getSyncindex();    
            
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(map.getTargetindex());
            Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
            String targetid = targetSystemExist.getTargetid();
            
            if(map.getStatus() == 0 || map.getStatus() == 200 || map.getStatus() == 400 ){//对待发布，发布中或者发布的失败的网元不做操作
                allresult.append("连续剧与角色关联关系在网元"+targetid+"上的发布状态不正确 ");  
                continue;
            }
            String rstInfo= delPublishSeriesCastTarget(index,batchid,firTargetType);  
            if ( rstInfo.substring(0, 1).equals("0"))
            {
                successFlg = true;
                allresult.append(rstInfo.substring(2));
            }else
            {
                allresult.append(rstInfo.substring(2));
         }
            }                       
         log.debug("delete publish platform for series castrolemap mapping end...");
        if(successFlg){
            return "0:"+allresult.toString();         
        }else{//全部失败
            return "1:"+allresult.toString();            
        }
    }

    public String delPublishSeriesCastBatchMap(List<CmsSeries> mappingList) throws Exception
    {
        log.debug("delete publish batch for series castrolemaps mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
        int size = mappingList.size();     
        for(CmsSeries mapping:mappingList){
            Long syncindex = mapping.getSyncindex();
            retInfo=  delPublishSeriesCastPlatformMap(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量:" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量:" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量:" + iSuccessed + "  失败数量:" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("delete publish batch for series castrolemap mapping end...");
        return returnInfo.toString();   

    }
    private void insertObjectRecordForMap( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype,String parentid, String elementid, Long targetindex,int synType, int platform,String parentcode,String elementcode)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setParentid(parentid);
            objectSyncRecord.setElementid(elementid);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            if(platform == 1)
            {
                objectSyncRecord.setParentcode(parentcode);
                objectSyncRecord.setElementcode(elementcode);
            }
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }  
       catch (Exception e)
       {
           log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    private void insertSyncTask(Long taskindex, String xmlPath, String correlateid, Long targetindex,Long batchid,int status)
    {
            CntSyncTask cntSyncTask = new CntSyncTask();
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            try
            {
                cntSyncTask.setTaskindex(taskindex);
                cntSyncTask.setBatchid(batchid);
                cntSyncTask.setStatus(status);
                cntSyncTask.setPriority(5);
                cntSyncTask.setRetrytimes(3);
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(xmlPath);
                cntSyncTask.setDestindex(Long.valueOf(targetindex));
                cntSyncTask.setSource(1);
                cntSyncTask.setStarttime(dateformat.format(new Date()));
                cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
            }
            catch (Exception e)
            {
                log.error("cmsProgramLS exception:" + e.getMessage());
            }
    }
    private void modTargetSynStatus(CntTargetSync mapping , int action) throws Exception
    {   
        CntTargetSync  targetSyn = new CntTargetSync();
       int result = mapping.getOperresult();
       if(action ==1){      
        if (result== 0)//待发布
        {    
            targetSyn.setOperresult(10);
        }
        else if(result== 60)  //修改同步失败
        {
            targetSyn.setOperresult(40);  //修改同步中
        }
        else        
        {     // 新增同步失败 或者取消同步失败
            targetSyn.setOperresult(10);// 新增同步中
         }
        targetSyn.setStatus(200);
        targetSyn.setSyncindex(mapping.getSyncindex());
        cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态  
       }
       if(action == 2){
           if( result == 20 ||  
                result == 50 || 
                result == 90 || 
                result == 60 )  //20新增同步成功//50修改同步成功 //90取消同步失败  //60改同步失败  
           { 
               targetSyn.setOperresult(40); //修改同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(mapping.getSyncindex());
           cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态     
       }
       if(action == 3){
           if(result == 20 ||  //新增同步成功
              result == 50 ||  //修改同步成功
               result == 60 ||   //修改同步失败
               result == 90  )  //取消同步失败                   
           { 
               targetSyn.setOperresult(70); //取消同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(mapping.getSyncindex());
           cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态     
       }
       CntTargetSync  sameRelateindexTargetSyn = new CntTargetSync();
       Long targetRelateindex = mapping.getRelateindex();
       sameRelateindexTargetSyn.setRelateindex(targetRelateindex);    
       List<CntTargetSync> mappingList = cntTargetSyncDS.getCntTargetSyncByCond(sameRelateindexTargetSyn);
       int size = mappingList.size();
       int targetStatusArrary[] = new int[size];      
       for(int i=0; i<size; i++){
           targetStatusArrary[i] = StatusTranslator.getTargetSyncStatus(mappingList.get(i).getOperresult()); 
       }
       int platformStatus = StatusTranslator.getPlatformSyncStatus(targetStatusArrary);
       CntPlatformSync platformSyn = new  CntPlatformSync();
       platformSyn.setSyncindex(targetRelateindex);
       platformSyn.setStatus(platformStatus);
       cntPlatformSyncDS.updateCntPlatformSync(platformSyn);   
    }
    private int findFirTargettype(List<CntTargetSync> serviceTargetSynList,int optype ) throws DomainServiceException{
        int type=99;
        for (CntTargetSync targetSync : serviceTargetSynList){
            int status = targetSync.getStatus();
            if(optype == 0 || optype ==1){//0,1表示新增和修改
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 0 || status == 400 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 0 || status == 400 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 0 || status == 400 || status == 500))type =2;
            }else{//2表示取消（删除）
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 2 && (status == 300 || status == 500))type =2;
                if(targettype == 1 && (type==99 || type==3) && (status == 300 || status == 500))type =1;
                if(targettype == 3 && type ==99 && (status == 300 || status == 500))type =3;
            }
        }
        return type;
    }
    private String getBMSPub(List<CntTargetSync> mappingList) throws DomainServiceException{
        for (CntTargetSync mappingTarget : mappingList){
            Targetsystem target = new Targetsystem();
            target.setTargetindex(mappingTarget.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            //只有存在EPG网元且能够取消发布的时候才去判断是否有mapping已发布
            if (target.getTargettype() == 1)
            {
                CntTargetSync parent = new CntTargetSync();
                parent.setObjectid(mappingTarget.getParentid());
                parent.setTargetindex(mappingTarget.getTargetindex());
                parent.setObjecttype(ObjectType.SERIES_TYPE);
                List<CntTargetSync> parentList = cntTargetSyncDS.getCntTargetSyncByCond(parent);
                
                if (null != parentList && parentList.size() > 0)
                {
                    for(CntTargetSync targetSync:parentList )
                    {
                        if(targetSync.getStatus() != 300 && targetSync.getStatus()!=500){
                            return "1:连续剧在网元"+target.getTargetid()+"上未发布，不能进行发布操作 ";
                        }
                       
                    }
                }

                //判断被绑定对象的发布状态是否为发布成功
                CntTargetSync element = new CntTargetSync();
                element.setObjectid(mappingTarget.getElementid());
                element.setTargetindex(mappingTarget.getTargetindex());
                element.setObjecttype(ObjectType.CASTROLEMAP_TYPE);
                List<CntTargetSync> elementList = cntTargetSyncDS.getCntTargetSyncByCond(element);
                
                if (null != elementList && elementList.size() > 0)
                {
                    for(CntTargetSync targetSync:elementList )
                    {
                        if(targetSync.getStatus() != 300 && targetSync.getStatus()!=500){
                            return "1:角色在网元"+target.getTargetid()+"上未发布，不能进行发布操作 ";
                        }
                       
                    }
                }
                
            }
        }
        return "0";
        
    }

}

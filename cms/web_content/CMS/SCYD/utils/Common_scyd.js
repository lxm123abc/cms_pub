/**
 * 判断两个天数
 */
function daysBetween(start,end,msg) {
    start = getHtmlCtrlTrimValue(start);
    end = getHtmlCtrlTrimValue(end);
    // 判断开始时间是否小于结束时间
    if (start.length > 0 && end.length > 0) {
        if (Date.parse(start.replace("-", "/")) > Date.parse(end.replace("-", "/"))) {
            alert(msg +" 结束时间不能小于开始时间!");
            return false;
        }else {
            return true;
        }
    }else if(start.length > 0 || end.length > 0){
        alert(msg+" 输入开始(或结束)时间,另一项必输");
        return false;
    }
    return true;
}

function getTime(str){
    if(str!=null){
        if(str.length==4){
            return str.substr(0,4);
        }
        if(str.length == 8){
            return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
        }

        if (str.length == 10){
            return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2)+ " " + str.substr(8,2);
        }

        if(str.length ==14){
            return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
        }else{
            return "";
        }
    }else{
        return "";
    }
}
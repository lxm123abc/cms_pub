package com.zte.cms.commonSync.object;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;

public class CreateXmlFile4Service extends AbstractCreateXmlFile4Object
{
    
    /*
     * 
     * 文广1.8规范
     **/
    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        String actionType = null;
        if (0 != targetType)
        {
        return null;
        }
        // 获取发布Action类型
        if (1 == syncType)
        {
            actionType = "REGIST";
        }else if(2 == syncType)
        {
            actionType = "UPDATE";
        }
        else if (3 == syncType)
        {
            actionType = "DELETE";
        }
        else
        {
            return null;
        }
        Date date = new Date();
         String fileName = "service_"+"20wg"+"_"+actionType.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element objectElement = null;
        Element propertyElement = null;
       

        List<ServiceTargetSyn> serviceList = objectMap.get("service");
        if (null == serviceList || 0 == serviceList.size())
        {
            return null;
        }
        ServiceTargetSyn service = serviceList.get(0);
        
        objectElement = objectsElement.addElement("Object");
        objectElement.addAttribute("ElementType", "Package");
        objectElement.addAttribute("ID", service.getServicecode());
        objectElement.addAttribute("Action", actionType);
        objectElement.addAttribute("Code", service.getServicecode());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Name");
        propertyElement.addCDATA(service.getServicename());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Type");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "SortName");
        propertyElement.addCDATA("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "SearchName");
        propertyElement.addCDATA("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "RentalPeriod");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "OrderNumber");
        propertyElement.addText(service.getServiceid().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "LicensingWindowStart");
        propertyElement.addText(service.getLicensingwindowstart().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "LicensingWindowEnd");
        propertyElement.addText(service.getLicensingwindowend().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Price");
        propertyElement.addText(service.getFeecode().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Status");
        propertyElement.addText("1");
            
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Description");
        propertyElement.addCDATA(service.getDescription()==null?"":service.getDescription().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Keywords");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Tags");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Reserve1");
        propertyElement.addText("");
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Reserve2");
        propertyElement.addText("");
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Reserve3");
        propertyElement.addText("");
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Reserve4");
        propertyElement.addText("");
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Reserve5");
        propertyElement.addText("");
              
        //把对象写入xml文件
        writeDom(dom,fileFullName);       
        return retPath;
    }
    /*
     * 
     * 电信3.0规范
     **/
    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
        String actionType = null;
        // 获取发布Action类型
        if (1 == syncType || 2 == syncType)
        {
            actionType = "REGIST or UPDATE";
        }
        else if (3 == syncType)
        {
            actionType = "DELETE";
        }
        else
        {
            return null;
        }
        Date date = new Date();
        String targetTypeName=null;
        if(targetType==1){
            targetTypeName = "bms";
        }else{
            targetTypeName = "epg";
        }
        String fileName = "service_"+targetTypeName+"_"+actionType.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
   
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element objectElement = null;
        Element propertyElement = null;
        
        
        List<ServiceTargetSyn> serviceList = objectMap.get("service");
        if (null == serviceList || 0 == serviceList.size())
        {
            return null;
        }
        ServiceTargetSyn service = serviceList.get(0); 
        
        objectElement = objectsElement.addElement("Object");
   

        objectElement.addAttribute("ElementType", "Service");
        objectElement.addAttribute("ServiceId", service.getServiceid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Name");
        propertyElement.addCDATA(service.getServicename());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FeeType");
        propertyElement.addText(service.getFeetype().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FeeCode");
        propertyElement.addText(service.getFeecode().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "FixedFee");
        propertyElement.addText(service.getFixedfee().toString());
        
        //把对象写入xml文件
        writeDom(dom,fileFullName);       
        return retPath;
    }

    @Override
    /*
     * 
     *央视2.6规范xml
     **/
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {

        String actionType = null;
        if (10 != targetType)
        {
        return null;
        }
        // 获取发布Action类型
        if (1 == syncType)
        {
            actionType = "REGIST";
        }else if(2 == syncType)
        {
            actionType = "UPDATE";
        }
        else if (3 == syncType)
        {
            actionType = "DELETE";
        }
        else
        {
            return null;
        }
        Date date = new Date();
         String fileName = "service_"+"20ys"+"_"+actionType.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element objectElement = null;
        Element propertyElement = null;
       

        List<ServiceTargetSyn> serviceList = objectMap.get("service");
        if (null == serviceList || 0 == serviceList.size())
        {
            return null;
        }
        ServiceTargetSyn service = serviceList.get(0);
        
        objectElement = objectsElement.addElement("Object");
        objectElement.addAttribute("ElementType", "Package");
        objectElement.addAttribute("ID", service.getServicecode());
        objectElement.addAttribute("Action", actionType);
        objectElement.addAttribute("Code", service.getServicecode());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Name");
        propertyElement.addCDATA(service.getServicename());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Type");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "SortName");
        propertyElement.addCDATA("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "SearchName");
        propertyElement.addCDATA("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "RentalPeriod");
        propertyElement.addText("");
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "OrderNumber");
        propertyElement.addText(service.getServiceid().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "LicensingWindowStart");
        propertyElement.addText(service.getLicensingwindowstart().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "LicensingWindowEnd");
        propertyElement.addText(service.getLicensingwindowend().toString());
        
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Price");
        propertyElement.addText(service.getFeecode().toString());

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Status");
        propertyElement.addText("1");
            
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Description");
        propertyElement.addCDATA(service.getDescription()==null?"":service.getDescription().toString());

              
        //把对象写入xml文件
        writeDom(dom,fileFullName);       
        return retPath;
    
    }

}

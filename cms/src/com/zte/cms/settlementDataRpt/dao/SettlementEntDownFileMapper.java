package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:18 2019/6/28
 * @Description:
 */
public interface SettlementEntDownFileMapper {

    void insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile);

    void updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile);

    SettlementEntDownFile selectSettlementEntDownFileBySpid(SettlementEntDownFile settlementEntDownFile);

    Integer selectSettlementEntDownFileCountBySpid(SettlementEntDownFile settlementEntDownFile);
}

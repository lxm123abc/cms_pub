package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.service.dao.IServiceDAO;
import com.zte.cms.service.model.Service;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceDAO extends DynamicObjectBaseDao implements IServiceDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertService(Service service) throws DAOException
    {
        log.debug("insert service starting...");
        super.insert("insertService", service);
        log.debug("insert service end");
    }

    public void updateService(Service service) throws DAOException
    {
        log.debug("update service by pk starting...");
        super.update("updateService", service);
        log.debug("update service by pk end");
    }

    public void deleteService(Service service) throws DAOException
    {
        log.debug("delete service by pk starting...");
        super.delete("deleteService", service);
        log.debug("delete service by pk end");
    }

    public Service getService(Service service) throws DAOException
    {
        log.debug("query service starting...");
        Service resultObj = (Service) super.queryForObject("getService", service);
        log.debug("query service end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Service> getServiceByCond(Service service) throws DAOException
    {
        log.debug("query service by condition starting...");
        List<Service> rList = (List<Service>) super.queryForList("queryServiceListByCond", service);
        log.debug("query service by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Service service, int start, int pageSize) throws DAOException
    {
        log.debug("page query service by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceListCntByCond", service)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Service> rsList = (List<Service>) super.pageQuery("queryServiceListByCond", service, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query service by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceListByCondForQuery", "queryServiceListCntByCondForQuery", service,
                start, pageSize, puEntity);
    }

    public PageInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceListByCondPublish", "queryServiceListCntByCondPublish", service,
                start, pageSize, puEntity);
    }

    /**
     * 取消发布的查询
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceListByCondDelPublish", "queryServiceListCntByCondDelPublish", service,
                start, pageSize, puEntity);
    }

    public int countCntContentPublish(CntTargetSync cntTargetSync) throws DAOException
    {
        // TODO Auto-generated method stub
        return ((Integer) super.queryForObject("countCntContentPublish",  cntTargetSync)).intValue();
    }

    public Service getServiceById(Service service) throws DAOException
    {
        log.debug("query getServiceById starting...");
        Service resultObj = (Service) super.queryForObject("getServiceById", service);
        log.debug("query getServiceById end");
        return resultObj;
    }
   
}
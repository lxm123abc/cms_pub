package com.zte.cms.EMBtask.service;

import java.util.List;
import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsEmbtaskDS
{
    /**
     * 新增CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 更新CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 批量更新CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsEmbtaskList(List<CmsEmbtask> cmsEmbtaskList) throws DomainServiceException;

    /**
     * 删除CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 批量删除CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsEmbtaskList(List<CmsEmbtask> cmsEmbtaskList) throws DomainServiceException;

    /**
     * 查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return CmsEmbtask对象
     * @throws DomainServiceException ds异常
     */
    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 根据条件查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象
     * @return 满足条件的CmsEmbtask对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsEmbtask对象
     * 
     * @param cmsEmbtask CmsEmbtask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DomainServiceException;

    public TableDataInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    public TableDataInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
    throws DomainServiceException;

}

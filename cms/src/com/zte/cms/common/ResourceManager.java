package com.zte.cms.common;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.util.ApplicationGlobalResource;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;

import com.zte.ssb.ria.parsexml.ParseResource;

/**
 * 资源文件处理类
 * 
 * @author 王蔚
 * @version ZXMCISMP-UMAPV2.01.01
 */

public class ResourceManager
{
    /**
     * 语言信息保存在session中的key
     */
    private static final String SESSION_LOCALE = "web_key_session_locale";

    /** LOCALE */
    public static String LOCALE = "zh";

    /**
     * 错误码前缀
     */
    private static final String ERRORCODE_PREFIX = "msg.info.";

    /**
     * 根据资源key获取资源信息
     * 
     * @param key 资源key
     * @return 资源信息
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public static String getResourceText(String key)
    {

        Locale userLocale = getLocale();

        String returnValue = ParseResource.getResourceValueByKey(key, userLocale.toString());
        if (returnValue == null || "".equals(returnValue.trim()))
        {
            returnValue = key;
        }
        System.out.println(returnValue);
        return returnValue;
    }

    public static String getErrorcodeByException(Exception e)
    {
        String errorcode = "10000";// 操作失败，请稍候再试
        if (e instanceof DomainServiceException)
        {
            errorcode = e.getMessage();
            if (!checkErrorcode(errorcode))
            {
                errorcode = "10001"; // 数据库操作失败
            }
        }
        else if (e instanceof Exception)
        {
            errorcode = e.getMessage();
            if (!checkErrorcode(errorcode))
            {
                errorcode = "10000"; // 操作失败，请稍候再试
            }
        }
        return errorcode;
    }

    /**
     * 根据异常获取多语化后的TEXT
     * 
     * @param e
     * @return
     */
    public static String getResourceText(Exception e)
    {
        String emsg = "";
        String errorcode = "10000";// 操作失败，请稍候再试
        if (e instanceof DomainServiceException)
        {
            errorcode = e.getMessage();
            if (!checkErrorcode(errorcode))
            {
                errorcode = "10001"; // 数据库操作失败
            }
        }
        else if (e instanceof Exception)
        {
            errorcode = e.getMessage();
            if (!checkErrorcode(errorcode))
            {
                errorcode = "10000"; // 操作失败，请稍候再试
            }
        }
        emsg = getResourceTextByCode(errorcode);
        return emsg;
    }

    /**
     * 根据资源key获取资源信息
     * 
     * @param key 资源key
     * @return 资源信息
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public static String getResourceTextByCode(String errorcode)
    {

        Locale userLocale = getLocale();
        errorcode = ERRORCODE_PREFIX + errorcode;

        String returnValue = ParseResource.getResourceValueByKey(errorcode, userLocale.toString());
        if (returnValue == null || "".equals(returnValue.trim()))
        {
            returnValue = errorcode;
        }

        return returnValue;
    }

    public static boolean checkErrorcode(String errorcode)
    {
        boolean rtn = true;
        try
        {
            Long.parseLong(errorcode);
        }
        catch (Exception e)
        {
            rtn = false;
        }
        return rtn;
    }

    /**
     * 获取当前登陆操作员的Locale
     * 
     * @return
     */
    private static Locale getLocale()
    {
        Locale userLocale = null;
        // 先从session中得local
        RIAContext context = RIAContext.getCurrentInstance();
        if (null != context)
        {
            ISession session = context.getSession();
            if (null != session)
            {
                HttpSession httpSession = session.getHttpSession();
                if (null != httpSession)
                {
                    Object o = httpSession.getAttribute(SESSION_LOCALE);
                    if (null != o)
                    {
                        userLocale = (Locale) o;
                    }
                }
            }
        }

        // 再从应用配置中获得
        if (null == userLocale)
        {
            String local = LOCALE;
            userLocale = new Locale(local);
        }
        return userLocale;
    }
}
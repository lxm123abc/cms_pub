package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IChannelPictureMapDS
{
    /**
     * 新增ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException;

    /**
     * 更新ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException;

    /**
     * 批量更新ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateChannelPictureMapList(List<ChannelPictureMap> channelPictureMapList)
            throws DomainServiceException;

    /**
     * 删除ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException;

    /**
     * 批量删除ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeChannelPictureMapList(List<ChannelPictureMap> channelPictureMapList)
            throws DomainServiceException;

    /**
     * 查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return ChannelPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public ChannelPictureMap getChannelPictureMap(ChannelPictureMap channelPictureMap) throws DomainServiceException;

    /**
     * 根据条件查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return 满足条件的ChannelPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ChannelPictureMap> getChannelPictureMapByCond(ChannelPictureMap channelPictureMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    /**
     * 根据条件分页查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException;

    
    /**
     * 根据条件分页查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws DomainServiceException;
 
    
    /**
     * 根据条件查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return 满足条件的ChannelPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ChannelPictureMap> getChannelPicturenormalMapByCond(ChannelPictureMap channelPictureMap)
            throws DomainServiceException;
}

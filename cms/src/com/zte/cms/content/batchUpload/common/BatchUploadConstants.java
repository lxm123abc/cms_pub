package com.zte.cms.content.batchUpload.common;

public class BatchUploadConstants
{
    public static final String WINDOWS_SEPERATOR = "\\"; // WINDOWS分隔符
    public static final String LINUX_SEPERATOR = "/"; // Linux分隔符
    // 共享访问点
    public static final String WINDOWS_SHARE_SITE = "cms.httppreview.path"; // cms.httppreview.path

    // 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
    public static final int SERVIEC = 1;
    public static final int CATAGORY = 2;
    public static final int PROGRAM = 3;
    public static final int SERIES = 4;
    public static final int CHANNEL = 5;
    public static final int CAST = 11;

    // 库区定义
    public static final String STORAGEAREA_ONLINE = "2"; // 在线区
    public static final Long MAX_FILESIZE = 10L * 1024L * 1024L; // HTTP上载海报文件大小的最大值

    // 错误码定义
    public static final String BATCHUPLOAD_SUCCESS = "200000"; // 操作成功
    public static final String BATCHUPLOAD_FAILED = "200001"; // 操作失败
    public static final String BATCHUPLOAD_ONLINEAREA_NOTFOUND = "200002"; // 获取在线区地址失败
    public static final String BATCHUPLOAD_DEST_NOTFOUND = "200003"; // 找不到目标地址，请检查存储区绑定
    public static final String BATCHUPLOAD_WRONG_FORMAT = "200004"; // 导入文件格式错误
    public static final String BATCHUPLOAD_WRONG_COLMNE = "200005"; // 导入文件列数错误
    public static final String BATCHUPLOAD_WRONG_ROW = "200006"; // 导入文件行数超过限制

    public static final String BATCHUPLOAD_GET_STORAGEAREA_ERROR = "1056020";// 获取库区地址失败
}

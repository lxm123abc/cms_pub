package com.zte.cms.check;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.SocketTimeoutException;

import org.apache.commons.net.telnet.TelnetClient;

/**
 * 利用apache net 开源包，使用telnet方式获取AIX主机信息
 * 
 * @version 1.2
 */
public class CheckServer
{

    // Telnet对象
    private TelnetClient telnet = new TelnetClient();

    private InputStream in;

    private PrintStream out;

    // 提示符。具体请telnet到AIX主机查看
    private String prompt = ">";
    private String prompt2 = "$";
    private String prompt3 = "#";

    // telnet端口
    private String port;

    // 用户
    private String user;

    // 密码
    private String password;

    // IP地址
    private String ip;

    // cpu使用率
    private String percent = "";

    // 内存总容量
    private String totalmem = "";

    // 已用容量
    private String usedmem = "";

    // 可用容量
    private String freemem = "";

    private String executeresult = "";

    public CheckServer()
    {

        try
        {
            // AIX主机IP
            this.ip = "";
            this.password = "";
            this.user = "";
            this.port = "23";

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public CheckServer(String ip1, String user1, String password1, String port1)
    {

        try
        {
            // AIX主机IP
            this.ip = ip1;
            this.password = password1;
            this.user = user1;
            this.port = port1;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 读取分析结果
     * 
     * @param pattern
     * @return
     */
    public String readUntil(String pattern, String pattern2, String pattern4, String pattern3) throws Exception
    {
        char lastChar = pattern.charAt(pattern.length() - 1);
        char lastChar2 = ' ';
        char lastChar3 = ' ';
        char lastChar4 = ' ';
        int flag = 0;
        try
        {
            if (pattern2 != null)
            {
                flag = 1;
                lastChar2 = pattern2.charAt(pattern2.length() - 1);
            }
            if (pattern3 != null)
            {
                flag = 2;
                lastChar3 = pattern3.charAt(pattern3.length() - 1);
            }
            if (pattern4 != null)
            {
                flag = 3;
                lastChar4 = pattern4.charAt(pattern4.length() - 1);
            }

            StringBuffer sb = new StringBuffer();
            InputStreamReader br = new InputStreamReader(in, "UTF-8");
            char[] buf = new char[255];

            char ch = (char) br.read();
            while (true)
            {
                // System.out.print(ch);
                sb.append(ch);
                if (ch == lastChar || (flag == 1 && ch == lastChar2) || (flag == 3 && ch == lastChar4))
                {
                    if (sb.toString().endsWith(pattern))
                    {
                        return sb.toString();
                    }
                    else if ((pattern2 != null) && sb.toString().endsWith(pattern2))
                    {
                        return sb.toString();
                    }
                    else if ((pattern4 != null) && sb.toString().endsWith(pattern4))
                    {
                        return sb.toString();
                    }
                    else
                    {
                        sb.delete(0, sb.length());
                    }
                }
                else if (flag == 2 && ch == lastChar3)
                {
                    if (sb.toString().endsWith(pattern3))
                    {
                        return "error";
                    }
                }
                ch = (char) br.read();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

    }

    /**
     * 写
     * 
     * @param value
     */
    public void write(String value)
    {
        try
        {
            out.println(value);
            out.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 匹配到cpu和内存的信息
     * 
     * @return
     */
    public String readCpuandManary()
    {
        try
        {
            char lastChar = ':';
            char lastChar2 = '%';
            char lastChar3 = 'k';
            char lastChar4 = 'M';
            char lastChar5 = 'm';
            int flag = 0;
            int countexe = 0;

            StringBuffer sb = new StringBuffer();
            InputStreamReader br = new InputStreamReader(in, "UTF-8");
            char[] buf = new char[255];

            char ch = (char) br.read();
            while (true)
            {
                // System.out.print(ch);
                sb.append(ch);
                if (ch == lastChar)
                {
                    if (flag == 0 && sb.toString().endsWith("Cpu(s):"))
                    {
                        sb.delete(0, sb.length());
                        flag = 1;
                    }
                    else if (flag == 2 && sb.toString().endsWith("Mem:"))
                    {
                        sb.delete(0, sb.length());
                        flag = 3;
                    }
                    else
                    {
                        sb.delete(0, sb.length());
                    }
                }
                else if (flag == 1 && ch == lastChar2)
                {
                    getUsedpec(sb.toString());
                    sb.delete(0, sb.length());
                    flag = 2;
                    countexe++;
                }
                else if (flag >= 3 && ch == lastChar3)
                {
                    if (flag == 3)
                    {
                        getMemTotal(sb.toString(), 1);
                        sb.delete(0, sb.length());
                        if (!checkIsNm(this.totalmem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        flag++;
                    }
                    else if (flag == 4)
                    {
                        getMemUsed(sb.toString(), 1);
                        sb.delete(0, sb.length());
                        if (!checkIsNm(this.usedmem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        flag++;
                    }
                    else if (flag == 5)
                    {
                        getMemFree(sb.toString(), 1);
                        if (!checkIsNm(this.freemem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        if (countexe == 4)
                        {
                            this.executeresult = String.valueOf(countexe);
                        }
                        return sb.toString();
                    }
                }
                else if (flag >= 3 && (ch == lastChar4 || ch == lastChar5))
                {
                    if (flag == 3)
                    {
                        getMemTotal(sb.toString(), 0);
                        sb.delete(0, sb.length());
                        if (!checkIsNm(this.totalmem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        flag++;
                    }
                    else if (flag == 4)
                    {
                        getMemUsed(sb.toString(), 0);
                        sb.delete(0, sb.length());
                        if (!checkIsNm(this.usedmem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        flag++;
                    }
                    else if (flag == 5)
                    {
                        getMemFree(sb.toString(), 0);
                        if (!checkIsNm(this.freemem))
                        {
                            ch = (char) br.read();
                            continue;
                        }
                        countexe++;
                        flag++;
                        if (countexe == 4)
                        {
                            this.executeresult = String.valueOf(countexe);
                        }
                        return sb.toString();
                    }

                }
                else if (ch == '\n')
                {
                    sb.delete(0, sb.length());

                }
                ch = (char) br.read();

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 检查是否是数字
     * 
     * @param str String类型
     * @return boolean类型
     */
    private boolean checknum(char chr)
    {

        String mask = "0123456789";

        if (mask.indexOf(chr) != -1)
        {
            return true;
        }

        return false;
    }

    /**
     * 检查是否是数字
     * 
     * @param str String类型
     * @return boolean类型
     */
    private boolean checkIsNm(String str)
    {

        String mask = "0123456789";
        char chr = ' ';
        for (int i = 0; i < str.length(); i++)
            chr = str.charAt(i);
        if (mask.indexOf(chr) == -1)
        {
            return false;
        }

        return true;
    }

    /**
     * 获取cpu的已用百分比
     * 
     * @param str
     */
    public void getUsedpec(String str)
    {
        String perc = "";
        char temp;
        str = str.substring(0, str.length() - 1);
        for (int i = str.length() - 1; i >= 0; i--)
        {
            temp = str.charAt(i);
            if (checknum(temp) || temp == '.')
            {
                perc = temp + perc;
            }
            else
            {
                this.percent = perc;
                break;
            }
        }
    }

    /**
     * 获取总内存的大小
     * 
     * @param str
     */
    public void getMemTotal(String str, int isk)
    {
        String MemTotal = "";
        char temp;
        str = str.substring(0, str.length() - 1);
        for (int i = str.length() - 1; i >= 0; i--)
        {
            temp = str.charAt(i);
            if (checknum(temp))
            {
                MemTotal = temp + MemTotal;
            }
            else
            {
                if (isk == 1)
                {
                    this.totalmem = String.valueOf(Long.valueOf(MemTotal) / 1024);
                }
                else
                {
                    this.totalmem = MemTotal;
                }
                break;
            }
        }
    }

    /**
     * 获取已用内存的大小
     * 
     * @param str
     */
    public void getMemUsed(String str, int isk)
    {
        String MemUsed = "";
        char temp;
        str = str.substring(0, str.length() - 1);
        for (int i = str.length() - 1; i >= 0; i--)
        {
            temp = str.charAt(i);
            if (checknum(temp))
            {
                MemUsed = temp + MemUsed;
            }
            else
            {
                if (isk == 1)
                {
                    this.usedmem = String.valueOf(Long.valueOf(MemUsed) / 1024);
                }
                else
                {
                    this.usedmem = MemUsed;
                }
                break;
            }
        }
    }

    /**
     * 获取可用内存的大小
     * 
     * @param str
     */
    public void getMemFree(String str, int isk)
    {
        String MemFree = "";
        char temp;
        str = str.substring(0, str.length() - 1);
        for (int i = str.length() - 1; i >= 0; i--)
        {
            temp = str.charAt(i);
            if (checknum(temp))
            {
                MemFree = temp + MemFree;
            }
            else
            {
                if (isk == 1)
                {
                    this.freemem = String.valueOf(Long.valueOf(MemFree) / 1024);
                }
                else
                {
                    this.freemem = MemFree;
                }
                break;
            }
        }
    }

    /**
     * 向目标发送命令字符串
     * 
     * @param command
     * @return
     */
    public String sendCommand(String command)
    {
        try
        {
            write(command);
            return readCpuandManary();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 关闭连接
     * 
     */
    public void disconnect()
    {
        try
        {
            telnet.disconnect();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * telnet到服务器
     * 
     * @return 返回登录结果
     */
    public String login()
    {
        String result = "";
        this.executeresult = "";
        try
        {
            telnet.setDefaultTimeout(5000);
            // 设置超时时间
            telnet.setConnectTimeout(800);

            telnet.connect(ip, Integer.parseInt(port));
            // System.out.println("开始获取输入流...");
            in = telnet.getInputStream();

            out = new PrintStream(telnet.getOutputStream());
            // 登录
            result = readUntil("login: ", null, null, "disabled.");
            if ("error".equals(result))
            {
                return result;
            }
            write(user);
            readUntil(user + ": ", "Password: ", null, null);
            write(password);
            result = readUntil(prompt, prompt2, prompt3, "Login incorrect");
        }
        catch (Exception e)
        {
            // e.printStackTrace();
            result = "error";
        }
        return result;
    }

    /**
     * 执行top命令，并获取cpu和内存信息
     * 
     * @throws Exception
     */
    public void excute() throws Exception
    {
        try
        {
            this.sendCommand("top");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 执行top命令，并获取cpu和内存信息
     * 
     * @throws Exception
     */
    public String excuteHasReturn(String command) throws Exception
    {
        try
        {
            write(command);
            return readUntil(prompt, null, null, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "";
    }

    // 输入命令，得到命令返回的信息
    public String getResult(String command) throws Exception
    {

        StringBuffer sb = new StringBuffer();
        InputStreamReader br = new InputStreamReader(in, "UTF-8");

        BufferedReader brin = new BufferedReader(br);
        String line;
        String retCmdInfo = null;

        write(command);
        try
        {
            while ((line = brin.readLine()) != null)
            {
                // System.out.println(line);
                if (retCmdInfo == null)
                {
                    retCmdInfo = "";
                    // System.out.println(retCmdInfo);
                }
                else
                {
                    retCmdInfo += (line + "\r\n");
                }

            }
        }
        // 由于无法关闭判断流的结尾，因此在信息读取完后，依然试图读取内容，导致报read time out异常，因此这里catch，并不做处理
        catch (SocketTimeoutException e)
        {

        }
        return retCmdInfo;
    }

    // 测tab
    public static void main(String[] args)
    {
        try
        {
            System.out.println("开始执行telnet......");
            CheckServer telnet = new CheckServer("10.129.170.22", "zxin10", "zxin10", "23");
            String loginres = telnet.login();
            System.out.println("------------------------result=" + loginres);

            // 通过aix的命令“查找主机名称”获取数据
            // 命令是 "hostname"
            // 不熟悉命令的参考<<AIX网络管理手册>>
            if (!"error".equals(loginres))
            {
                System.out.println("开始发送hostname命令");
                // String result = telnet.sendCommand("df -h");
                String result = telnet.getResult("df -h");
                // result = result.replace(" ", "**");
                System.out.println(result);

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public TelnetClient getTelnet()
    {
        return telnet;
    }

    public void setTelnet(TelnetClient telnet)
    {
        this.telnet = telnet;
    }

    public InputStream getIn()
    {
        return in;
    }

    public void setIn(InputStream in)
    {
        this.in = in;
    }

    public PrintStream getOut()
    {
        return out;
    }

    public void setOut(PrintStream out)
    {
        this.out = out;
    }

    public String getPrompt()
    {
        return prompt;
    }

    public void setPrompt(String prompt)
    {
        this.prompt = prompt;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getPercent()
    {
        return percent;
    }

    public void setPercent(String percent)
    {
        this.percent = percent;
    }

    public String getTotalmem()
    {
        return totalmem;
    }

    public void setTotalmem(String totalmem)
    {
        this.totalmem = totalmem;
    }

    public String getUsedmem()
    {
        return usedmem;
    }

    public void setUsedmem(String usedmem)
    {
        this.usedmem = usedmem;
    }

    public String getFreemem()
    {
        return freemem;
    }

    public void setFreemem(String freemem)
    {
        this.freemem = freemem;
    }

    public String getExecuteresult()
    {
        return executeresult;
    }

    public void setExecuteresult(String executeresult)
    {
        this.executeresult = executeresult;
    }
}
package com.zte.cms.content.apply;

import java.util.List;

import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * <p>
 * 文件名称: CntApplyValid.java
 * </p>
 * <p>
 * 文件描述: 内容申请生效类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容申请审核后的生效方法
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntApplyValid extends CommonValid
{
    protected ICmsProgramDS cmsProgramDS = null;
    protected ICmsMovieDS cmsMovieDS = null;
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {
        // 查询订单对象
        CmsProgram cmsProgram = new CmsProgram();
        try
        {
            cmsProgram.setProgramindex(index);
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        if (null != cmsProgram)
        {
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
            ResourceMgt.addDefaultResourceBundle("log_program_resource");

            // 操作对象类型
            String optType = ResourceMgt.findDefaultText("log.opttype.cnt.auditmgt"); // 内容审核管理
            String optObject = String.valueOf(cmsProgram.getProgramid());
            String optObjecttype = ResourceMgt.findDefaultText("log.optobjecttype.cnt.auditvalid"); // 内容审核
            String optDetail = ResourceMgt.findDefaultText("log.optdetail.cnt.applyvalid"); // 内容申请生效

            if (null != optDetail)
            {
                optDetail = optDetail.replaceAll("\\[programid\\]", cmsProgram.getProgramid());
            }

            loginfo.setOptType(optType);
            loginfo.setOptObject(optObject);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptDetail(optDetail);
            loginfo.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);

            return loginfo;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void operate(Long index)
    {
        try
        {
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(index);
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            cmsProgram.setWorkflow(0); // 0:未启动
            cmsProgram.setStatus(0); // 0：待发布

            cmsProgramDS.updateCmsProgram(cmsProgram);

            // 更新内容下的所有子内容为内容的状态
            CmsMovie cmsMovie = new CmsMovie();
            List<CmsMovie> cmsMovieList = null;
            cmsMovie.setProgramid(cmsProgram.getProgramid());
            cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
            if (null != cmsMovieList && cmsMovieList.size() > 0)
            {
                for (int i = 0; i < cmsMovieList.size(); i++)
                {
                    cmsMovieList.get(i).setStatus(cmsProgram.getStatus());
                }
                cmsMovieDS.updateCmsMovieList(cmsMovieList);
            }
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

}

package com.zte.cms.settlementDataRpt.model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * ProjectName: miaosha-com.miaoshaDemo.controller.viewObject
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 15:07 2019/5/6
 * @Description:
 */
public class SettlementDataRptVO extends SettlementDataRpt implements Serializable {
    private String rptDt_a;
    private String startRptDt;
    private String endRptDt;
    private int start;
    private int pageSize;
    private String rptDtStr;
    private String exportNewName;
    private String exportFileName;




    public String getExportNewName() {
        return exportNewName;
    }

    public void setExportNewName(String exportNewName) {
        this.exportNewName = exportNewName;
    }

    public String getExportFileName() {
        return exportFileName;
    }

    public void setExportFileName(String exportFileName) {
        this.exportFileName = exportFileName;
    }

    public String getRptDtStr() {
        return rptDtStr;
    }

    public void setRptDtStr(String rptDtStr) {
        this.rptDtStr = rptDtStr;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartRptDt() {
        return startRptDt;
    }

    public void setStartRptDt(String startRptDt) {
        this.startRptDt = startRptDt;
    }

    public String getEndRptDt() {
        return endRptDt;
    }

    public void setEndRptDt(String endRptDt) {
        this.endRptDt = endRptDt;
    }

    public String getRptDt_a() {
        return rptDt_a;
    }

    public void setRptDt_a(String rptDt_a) {
        this.rptDt_a = rptDt_a;
    }

}
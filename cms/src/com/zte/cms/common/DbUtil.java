package com.zte.cms.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zte.cms.common.ResourceManager;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.JDBCBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.zxywpub.BaseDataSource;

/**
 * 数据库操作类，用于校验是否有关联数据
 * 
 * @author
 * 
 */
public class DbUtil extends JDBCBaseDao
{
    private Log log = SSBBus.getLog(getClass());

    public Connection getConn()
    {
        BaseDataSource dataSource = (BaseDataSource) SSBBus.findDomainService("dataSource");
        super.setDataSource(dataSource);
        Connection conn = super.getConnection();
        return conn;

    }

    public List getQuery(String sql) throws Exception
    {
        Connection conn = null;
        Statement s = null;
        try
        {
            BaseDataSource dataSource = (BaseDataSource) SSBBus.findDomainService("dataSource");
            super.setDataSource(dataSource);
            conn = super.getConnection();
            s = conn.createStatement();
            ResultSet result = s.executeQuery(sql);
            return rsToList(result);
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            try
            {
                if (s != null)
                {
                    s.close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                {
                    releaseJDBCConnection(conn);
                }
            }
        }
    }

    public List rsToList(ResultSet rs) throws SQLException
    {
        if (rs == null)
        {
            return null;
        }
        List list = new ArrayList();
        java.sql.ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (rs.next())
        {
            Map rsMap = new HashMap();
            for (int i = 0; i < columnCount; i++)
            {
                rsMap.put(metaData.getColumnName(i + 1).toLowerCase(), rs.getString(i + 1));
            }
            list.add(rsMap);
        }
        return list;
    }

    /**
     * 增加、修改和删除等操作前，校验是否存在关联数据，返回0则不存在，否则，存在
     * 
     * @param sqlstr
     * @return
     */
    public String checkconflict(String sqlstr) throws Exception
    {
        List list = null;
        Iterator it = null;
        HashMap mymap = null;
        String mystr = "0";
        try
        {
            list = getQuery(sqlstr);
            it = list.iterator();
            mymap = (HashMap) it.next();
            mystr = (String) (mymap.values().iterator().next());

        }
        catch (Exception ex)
        {
            log.error("checkconflict exception:", ex);
            throw new Exception(ResourceManager.getResourceText(ex));
        }

        return mystr;

    }

    public void excute(List sqls) throws Exception
    {
        excuteTrans(sqls);
    }

    public void excute(String sql) throws Exception
    {
        excuteSql(sql);
    }

    public void excuteTrans(List sqls) throws Exception
    {
        Connection conn = null;
        Statement s = null;
        try
        {
            BaseDataSource dataSource = (BaseDataSource) SSBBus.findDomainService("dataSource");
            super.setDataSource(dataSource);
            conn = super.getConnection();
            conn.setAutoCommit(false);
            s = conn.createStatement();
            for (int i = 0; i < sqls.size(); i++)
            {
                String sql = (String) sqls.get(i);
                System.out.println("------" + sql);
                s.execute(sql);
            }
            conn.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            conn.rollback();
            throw e;
        }
        finally
        {
            try
            {
                if (s != null)
                {
                    s.close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                {
                    conn.setAutoCommit(true);
                    super.releaseConnection(conn);
                }
            }
        }
    }

    public void excuteSql(String sql) throws Exception
    {
        Connection conn = null;
        Statement s = null;
        try
        {
            BaseDataSource dataSource = (BaseDataSource) SSBBus.findDomainService("dataSource");
            super.setDataSource(dataSource);
            conn = super.getConnection();
            if (conn.getAutoCommit() == false)
            {
                conn.commit();
                conn.setAutoCommit(true);
            }
            s = conn.createStatement();
            s.execute(sql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        finally
        {
            try
            {
                if (s != null)
                {
                    s.close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn != null)
                {
                    super.releaseConnection(conn);
                }
            }
        }
    }

    public List getQuery(String sql, Connection conn) throws Exception
    {
        Statement s = null;
        try
        {
            s = conn.createStatement();
            System.out.println("------" + sql);
            ResultSet result = s.executeQuery(sql);
            return rsToList(result);
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            try
            {
                if (s != null)
                {
                    s.close();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * 返回查询数目的SQL语句查询结果.
     * 
     * @param sqlstr the sqlstr
     * @return the int
     * @throws Exception the exception
     * @return
     */
    public int queryForCount(String sqlstr) throws Exception
    {
        List list = null;
        Iterator it = null;
        HashMap mymap = null;
        String mystr = "0";
        try
        {
            list = getQuery(sqlstr);
            it = list.iterator();
            mymap = (HashMap) it.next();
            mystr = (String) (mymap.values().iterator().next());

        }
        catch (Exception ex)
        {
            log.error("queryForCount exception:", ex);
            throw new Exception(ResourceManager.getResourceText(ex));
        }

        return Integer.parseInt(mystr);

    }
    @SuppressWarnings("unchecked")
    public PageInfo pageSplitOracle(String sqlStr, int start, int pageSize)
            throws Exception {
        int minRow = start + 1;
        int maxRow = start + pageSize;
        List listCnt = new ArrayList();
        List list = new ArrayList();
        PageInfo pageInfo = null;
        Iterator it = null;
        HashMap map = null;

        String sqlCnt = "SELECT COUNT(1) FROM (" + sqlStr + ")";
        String sql = "SELECT * FROM (SELECT A.*, ROWNUM RN FROM (";
        sql = sql + sqlStr + ") A WHERE ROWNUM <= " + maxRow + ") WHERE RN >= "
                + minRow;
        try {
            log.debug("Executing Statement: " + sqlCnt);
            listCnt = this.getQuery(sqlCnt);
            it = listCnt.iterator();
            map = (HashMap) it.next();
            int totalCnt = Integer.parseInt((String) map.values().iterator()
                    .next());
            if (totalCnt > 0) {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                        : pageSize;
                log.debug("Executing Statement: " + sql);
                list = this.getQuery(sql);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, list);
            } else {
                pageInfo = new PageInfo();
            }
        } catch (Exception e) {
            throw e;
        }
        return pageInfo;
    }
    public int doUpdateBatch(List<String> sqlList) throws Exception {
        if (sqlList == null || sqlList.size() == 0) {
            return 0;
        }
        Connection conn = null;
        try {
            conn = this.getConn();
            if (conn != null) {
                for (String sql : sqlList) {
                    this.doUpdate(sql, conn);
                }
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            if (conn != null) {
                releaseJDBCConnection(conn);
            }
        }
    }
    public int doUpdate(String sql, Connection conn) throws Exception {
        Statement s = null;
        try {
            if (conn != null) {
                s = conn.createStatement();
                if (s != null) {
                    log.debug("Executing Statement: " + sql);
                    int result = s.executeUpdate(sql);
                    return result;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (s != null) {
                    s.close();
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }
}

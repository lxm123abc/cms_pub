package com.zte.cms.channel.ls;

import java.util.List;

import org.dom4j.Element;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface IPhysicaChannellLS
{

    public TableDataInfo pageInfo(Physicalchannel physicalchannel, int start, int pageSize);

    public String insertPhysicalchannel(Physicalchannel physicalchannel);

    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel);

    public String updatePhysicalchannel(Physicalchannel physicalchannel) throws Exception;

    public String deletePhysicalchannel(Physicalchannel physicalchannel);


    public List getPhysicalchannelBychannelindex(Long channelindex);
    /**
     * 列表展示物理频道为节目单
     * added by fc
     */
    public TableDataInfo pageInfoschedule(CmsSchedule cmsSchedule, int start, int pageSize);
}
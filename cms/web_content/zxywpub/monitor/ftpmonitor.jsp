<%@page language="java" contentType="text/html"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="com.zte.jservadm.SessionCounter" %>
<%@ page import="com.zte.jos.monitor.util.FtpSnapshotEntity" %>

<jsp:useBean id="ftp" class="com.zte.jos.monitor.build.FTPPoolBuild" scope="application" />

<%!
  private String timedsp(long ltime){
    Calendar cal=Calendar.getInstance();
    String sTime = "Free";
    cal.setTimeInMillis(ltime);
    if(ltime>0L){
      sTime = "" + cal.get(Calendar.YEAR) +"."+ (cal.get(Calendar.MONTH)+1);
      sTime = sTime +"."+cal.get(Calendar.DAY_OF_MONTH);
      sTime = sTime +" "+cal.get(Calendar.HOUR_OF_DAY) +":"+cal.get(Calendar.MINUTE);
      sTime = sTime +":"+cal.get(Calendar.SECOND);
    }
    return sTime;
  }
  
  private String statdsp(int i){
    String dspstr="Idle";
    if(i==1){
      dspstr="False";
    }else if(i==2){
      dspstr="True";
    }
    return dspstr;
  }
%>

<%
//本模块通过SMP链接直接访问
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");

String Strmsg="";
int k=0,cmdint=0;
if(request.getParameter("cmd")!=null)cmdint=java.lang.Integer.parseInt(request.getParameter("cmd"));
int Statid=0;
String Remoteoper=(String)session.getAttribute("OPERNAME");
String Refreshtime="5";
String servletPath = request.getServletPath();
String rootPath = (servletPath.split("zxywpub"))[0];
if(Remoteoper==null){
  if(cmdint>0){
  %><script language="javascript1.2">
    alert("Please login first");
	top.location.href = ' <%=rootPath%>' + "zxywpub";
  </script><%
    cmdint=0;
  }
}

%><!-- JSP programs end -->
<HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<TITLE>SMP--Home Page</TITLE>
<link REL="STYLESHEET" HREF="inc/style.css" TYPE="text/css">
</HEAD>
<BODY topmargin="0" leftmargin="0" style="background-color: rgb(249,250,244)">

<table border="0" cellPadding="5" cellSpacing="1" width="100%" height="95%" bgcolor="#C0C0C0">
   <tr height="95%">
      <td vAlign="top" bgcolor="#FFFFFF" background="images/bg01.jpg">
<!-- Main display info -->
<%
if(cmdint==98){
  String Dbname=request.getParameter("dbname");

  cmdint=99;

} 
if(cmdint==66){
  if(request.getParameter("statid")!=null) Statid=java.lang.Integer.parseInt(request.getParameter("statid"));

  cmdint=99;
} 
if(cmdint==99){
//预留给连接池显示
  long freeMomory = Runtime.getRuntime().freeMemory()/1024;
  long totalMomory = Runtime.getRuntime().totalMemory()/1024;

  if(request.getParameter("refreshtime")!=null)Refreshtime=request.getParameter("refreshtime");
  out.print("Now server: "+java.net.InetAddress.getLocalHost().getHostName());
  out.print("["+java.net.InetAddress.getLocalHost().getHostAddress()+"] Java memory:");
  out.print("["+totalMomory+"K] Free memory:["+freeMomory+"K] ");
  out.println("online: "+ SessionCounter.getActiveSessions());
/*
if(System.getProperty("os.name").toLowerCase().indexOf("win")>=0){
 out.println("CPU: "+ com.zte.zxywpub.cpusagent.getInstance().getCpuUsageRate());
}else{
 float fl=com.zte.zxywpub.BsdPerformance.getInstance().getCpuUsageRate();
 out.println("CPU: "+ fl);
}*/
%>
<form method="post" name="inForm" action="ftpmonitor.jsp">
<input type="hidden" name="cmd" value="<%=cmdint%>">
<input type="hidden" name="dbname" value="all">

<div align="center"><center>
  <table border="0" width="400" cellspacing="0" cellpadding="2" style=" font-size: 9pt">
    <tr>
    <td><select name="refreshtime" size="1">
    	<OPTION value='0'> 0 </OPTION>
      <OPTION value='3'> 3s </OPTION>
      <OPTION value='5'> 5s </OPTION>
      <OPTION value='10'> 10s </OPTION>
      <OPTION value='15'> 15s </OPTION>
      <OPTION value='30'> 30s </OPTION>
      <OPTION value='60'> 60s </OPTION>
      
      </select> 
      <span onclick="cleardb('all')" onmouseover="this.style.cursor='hand'"> [Rs] </span>
      </td>
      <td>Stat<select name="statid" size="1" onchange="setstat()">
      <OPTION value='0'> No </OPTION>
      <OPTION value='1'> Yes </OPTION>
      </select>
      </td>
      <td align="right"><input type="button" onclick="renew()" value="Refresh" name="B11" tabindex="91"></td>
    </tr>
  </table><hr size="1" width="500">
<%
  List<FtpSnapshotEntity> list = ftp.getSnapshot();

    String sTmp="";
    String hd="";
    FtpSnapshotEntity e = null;
    out.println("<table border='1' width='100%' cellspacing='0' cellpadding='2' style=' font-size: 9pt'>");
    out.println("<tr><td style='background-color:#cceeff;'>Pool</td><td style='background-color:#cceeff;'>IP</td><td style='background-color:#cceeff;'>Port</td><td style='background-color:#cceeff;'>Maxsize</td><td width='10%' style='background-color:#cceeff;'>Free</td><td width='10%' style='background-color:#cceeff;'>Active</td><td style='background-color:#cceeff;'>AlarmNum</td><td style='background-color:#cceeff;'>trignum</td></tr>");
    
    for(int i=0;i<list.size();i++)
    {
    	e = (FtpSnapshotEntity)list.get(i);
    	
    	sTmp= sTmp + "<tr><td> "+e.getId()+"</td><td>"+e.getIp()+"</td><td> "+e.getPort()+"</td><td>"+e.getMaxNum()+" </td><td>"+e.getFreeNum()+"</td>";
    
        sTmp = sTmp + "<td><a href='#' onClick=''>"+e.getActiveNum()+"</a></td>";
    
    	sTmp = sTmp + "<td>"+e.getAlarmNum()+"</td>";
      
    	sTmp = sTmp + "<td>"+e.getTrigNum()+"</td>";
 
    	sTmp = sTmp +"</tr>\n";
    }
    
   
    
    out.println(sTmp);

    if(list.size() != 0)
    {
        sTmp = sTmp + "</table>";

	   String tmp = "<table id='' border='1'   width='100%' cellspacing='0' cellpadding='2' style=' font-size: 9pt'>";
	   tmp = tmp + "<tr><td style='background-color:#cceeff;'>FTP Connection</td><td style='background-color:#cceeff;'>status</td><td style='background-color:#cceeff;'>createTime</td></tr>";
	  
	   for(int i=0;i<list.size();i++)
	   {
	   	e = (FtpSnapshotEntity)list.get(i);
	   	String st[] = e.get_start();
	   	for(int j = 0; j < st.length; j ++)
	   	{
	   	    tmp= tmp + "<tr><td> Pool["+e.getId()+"] connection ["+j+"]</td>";
	   	    
	   	    tmp = tmp + "<td>active</td>";
	       
	   	    tmp = tmp + "<td>"+st[j]+"</td>";
	    
	   	    tmp = tmp +"</tr>\n";
	   	}
	   	
	   }
	   
	   out.println(tmp);
	   
    }else{
        String nf = "<table id='' border='1'   width='100%' cellspacing='0' cellpadding='2' style=' font-size: 9pt'><tr><font color='red'>NO found FTP Pool<tr></table>";
        out.println(nf);
    }
        
  
%>
</center></div>
</form>
<script language="JavaScript1.2">
function cleardb(dbname){
  document.inForm.cmd.value="98";
  document.inForm.dbname.value=dbname;
  document.inForm.submit();
}
function showDetail(a){
  alert(a);
  var table1 = document.getElementById(a);
  alert(table1.style.display);
  table1.style.display = display;
}
function renew(){
  document.inForm.submit();
}
function setstat(){
  document.inForm.cmd.value="66";
  document.inForm.submit();
}
document.inForm.refreshtime.value="<%=Refreshtime%>";
document.inForm.statid.value="<%=Statid %>";
if(<%=Refreshtime%> != 0){
	setTimeout('renew()',<%=Refreshtime%>*1000);
}

</script>
<%
}%>
<!-- Main display info end -->
      </td>
   </tr>
<!-- Messages -->
   <tr height="5%"><td bgcolor="#f4f9fa" align="right"><%=Strmsg%></td></tr>
<!-- Messages end -->
</table>
</BODY>
</HTML>

package com.zte.cms.basicdata.area.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceCodeParser;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.sys.model.Tree;
import com.zte.umap.sys.model.UsysCity;
import com.zte.umap.sys.model.UsysProvince;
import com.zte.umap.sys.service.IUsysCityDS;
import com.zte.umap.sys.service.IUsysProvinceDS;

public class IsysProvinceLS implements IIsysProvinceLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private IUsysProvinceDS ds = null;

    private IUsysCityDS usysCityDS = null;

    private static final String INITIAL = "10";// 初始
    private static final String SYNCHRONIZING = "21";// 同步中
    private static final String SYNCHRONIZED = "40";// 已同步
    private static final String ADDSYNCHRONIZEDERROR = "41";// 新增同步失败
    private static final String DELSYNCHRONIZEDERROR = "42";// 删除同步失败

    private static final String CAN_ADD_SYN = "can_add_syn";
    private static final String CANNOT_ADD_SYN = "cannot_add_syn";
    private static final String CAN_DEL_SYN = "can_del_syn";
    private static final String CANNOT_DEL_SYN = "cannot_del_syn";

    private static final long COUNTRYWIDE = 33; // 全国

    private ResourceCodeParser parser = new ResourceCodeParser(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

    public void setUsysCityDS(IUsysCityDS usysCityDS)
    {
        this.usysCityDS = usysCityDS;
    }

    public void setDs(IUsysProvinceDS ds)
    {
        this.ds = ds;
    }

    public Tree insertUsysProvinceInTree(UsysProvince usysProvince) throws DomainServiceException
    {
        // String operResult =
        // CommonLogConstant.OPTRESUT_SUCCESS+":"+parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        String operObjectId = usysProvince.getProvid() + "";

        log.debug("insertUsysProvinceInTree start ");
        usysProvince.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        usysProvince.setProvdesc(INITIAL);
        Tree provtree = new Tree();
        if (usysProvince != null)
        {
            if (usysProvince.getProvdesc() != null && !"".equals(usysProvince.getProvdesc()))
            {
                try
                {

                    log.debug("insertUsysProvince start ");
                    ds.insertUsysProvince(usysProvince);
                    log.debug("insertUsysProvince end ");

                    provtree.setMenuid(usysProvince.getProvindex());
                    provtree.setMenuname(usysProvince.getProvname());
                    provtree.setParentid(new Long(0));
                    provtree.setOperresult(new Integer(1150000)); // 增加成功
                    provtree.setEnabled("1");
                    provtree.setFlag(1);

                    // 操作成功增加日志操作
                    CommonLogUtil.insertOperatorLog(operObjectId, AreaConstant.MGTTYPE_AREA,
                            AreaConstant.OPERTYPE_AREA_PROVINCE_ADD, AreaConstant.OPERTYPE_AREA_PROVINCE_ADD_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                }
                catch (DomainServiceException e)
                {
                    log.error("during insert usysProvince occur errors ");
                    throw new DomainServiceException(e);
                }

            }
        }

        log.debug("insertUsysProvinceInTree  end");
        return provtree;
    }

    public Tree updateUsysProvinceInTree(UsysProvince usysProvince) throws DomainServiceException
    {

        log.debug("updateUsysProvinceInTree start ");
        String operObjectId = usysProvince.getProvid() + "";
        Tree provtree = new Tree();
        try
        {
            UsysProvince provinfo = new UsysProvince();
            provinfo.setProvid(usysProvince.getProvid());
            UsysProvince provinince = this.getUsysProvinceById(provinfo);
            if (provinince == null)
            {
                provtree.setOperresult(new Integer(1150001)); // 省份不存在或已经删除
            }
            else
            {
                if (INITIAL.equals(provinince.getProvdesc()) || ADDSYNCHRONIZEDERROR.equals(provinince.getProvdesc()))
                {
                    ds.updateUsysProvince(usysProvince);
                    provtree.setMenuid(provinince.getProvindex());
                    provtree.setMenuname(usysProvince.getProvname());
                    provtree.setParentid(new Long(0));
                    provtree.setOperresult(new Integer(1150000)); // 修改成功
                    provtree.setEnabled("1");
                    provtree.setFlag(1);

                    // 操作成功增加日志操作
                    CommonLogUtil.insertOperatorLog(operObjectId, AreaConstant.MGTTYPE_AREA,
                            AreaConstant.OPERTYPE_AREA_PROVINCE_MOD, AreaConstant.OPERTYPE_AREA_PROVINCE_MOD_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    provtree.setOperresult(new Integer(1150002)); // 省份状态不正确,不能修改
                }

            }
        }
        catch (DomainServiceException e)
        {
            log.error("during update usysProvince occur errors ");
            // operResult=CommonLogConstant.OPTRESUT_SUCCESS+":"+parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            throw new DomainServiceException(e);
        }

        log.debug("updateUsysProvinceInTree end");
        return provtree;
    }

    public Tree removeUsysProvinceFromIptvTree(UsysProvince usysProvince) throws DomainServiceException
    {

        log.debug("removeUsysProvinceFromIptvTree start");
        String operObjectId = usysProvince.getProvid() + "";
        Tree provtree = new Tree();
        try
        {
            UsysProvince provinfo = new UsysProvince();
            provinfo.setProvid(usysProvince.getProvid());
            UsysProvince province = this.getUsysProvinceById(provinfo);
            if (province == null)
            {
                provtree.setOperresult(new Integer(1150001)); // 省份不存在或已经删除
            }
            else
            {
                if (INITIAL.equals(province.getProvdesc()))
                {
                    ds.removeUsysProvince(province);
                    provtree.setMenuid(province.getProvindex());
                    provtree.setMenuname(usysProvince.getProvname());
                    provtree.setParentid(new Long(0));
                    provtree.setOperresult(new Integer(1150000)); // 删除成功

                    // 操作增加日志操作
                    CommonLogUtil.insertOperatorLog(operObjectId, AreaConstant.MGTTYPE_AREA,
                            AreaConstant.OPERTYPE_AREA_PROVINCE_DEL, AreaConstant.OPERTYPE_AREA_PROVINCE_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    provtree.setOperresult(new Integer(1150003)); // 省份状态不正确,不能删除
                }
            }
        }
        catch (DomainServiceException e)
        {
            log.error("during remove usysProvince occur errors ");
            throw new DomainServiceException(e);

        }
        log.debug("removeUsysProvinceFromIptvTree end");

        return provtree;
    }

    public UsysProvince getUsysProvince(UsysProvince usysProvince) throws DomainServiceException
    {
        log.debug("getUsysProvince start ");
        UsysProvince rsObj = null;
        rsObj = ds.getUsysProvince(usysProvince);
        log.debug("getUsysProvince end ");
        return rsObj;
    }

    public UsysProvince getUsysProvinceById(UsysProvince usysProvince) throws DomainServiceException
    {
        log.debug("getUsysProvinceById start ");
        UsysProvince rsObj = null;
        UsysProvince temp = new UsysProvince();
        if(usysProvince.getProvid()==null){
            return null;
        }
        temp.setProvid(usysProvince.getProvid());
        temp.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysProvince> provlist = ds.getUsysProvinceByCond(temp);
        if (provlist != null && !provlist.isEmpty())
        {
            rsObj = provlist.get(0);
        }
        log.debug("getUsysProvinceById end");
        return rsObj;
    }

    public String getUsysProvinceByName(UsysProvince usysProvince) throws DomainServiceException
    {
        log.debug("getUsysProvinceByName start ");
        UsysProvince rsObj = null;
        UsysProvince temp = new UsysProvince();
        temp.setProvname(usysProvince.getProvname());
        temp.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysProvince> provlist = ds.getUsysProvinceByCond(temp);
        if (provlist.size() == 0)
        {
            return "0";
        }
        if (provlist.size() == 1)
        {
            rsObj = provlist.get(0);
            if (rsObj.getProvid().equals(usysProvince.getProvid()))
            {
                return "0";
            }
        }

        log.debug("getUsysProvinceByName end");
        return "1";
    }

    public List<UsysProvince> getUsysProvinceByCond(UsysProvince usysProvince) throws DomainServiceException
    {
        log.debug("getUsysProvinceByCond start ");
        usysProvince.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysProvince> rsList = null;
        rsList = ds.getUsysProvinceByCond(usysProvince);
        log.debug("getUsysProvinceByCond end");
        return rsList;
    }

    public List<Tree> getUsysProvinceInIptvTree(Tree tree) throws DomainServiceException
    {

        List<Tree> rsList = new ArrayList<Tree>();
        List<Tree> rsListprov = null;
        List<Tree> rsListcity = null;

        tree.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        log.debug("getUsysProvinceInIptvTree start ");
        rsListprov = ds.getUsysProvinceInIptvTree(tree);
        for (int i = 0; i < rsListprov.size(); i++)
        {
            Tree tree2 = (Tree) rsListprov.get(i);
            if (tree2.getMenuid() == COUNTRYWIDE)
            {
                rsListprov.get(i).setFlag(1);
                // rsListprov.get(i).setParentid(new Long(-1));
            }
            else
            {
                rsListprov.get(i).setFlag(1);
                rsListprov.get(i).setParentid(new Long(0));
            }

        }
        if (tree.getParentid() == 0)
        {
            tree.setParentid(null);
        }

        rsListcity = usysCityDS.getUsysCityInIptvTree(tree);
        for (int i = 0; i < rsListcity.size(); i++)
        {
            rsListcity.get(i).setFlag(2);
        }

        if (rsListprov.size() >= 1)
        {
            for (Tree item : rsListprov)
            {
                rsList.add(item);
            }
        }

        if (rsListcity.size() >= 1)
        {
            for (Tree item : rsListcity)
            {
                rsList.add(item);
            }
        }

        log.debug("getUsysProvinceInIptvTree end ");
        return rsList;
    }

    public HashMap<String, Object> getSysProvince(String selectedValue, Boolean needSelect)
            throws DomainServiceException
    {
        HashMap<String, Object> map = new HashMap<String, Object>();

        UsysProvince usysProvince = new UsysProvince();
        usysProvince.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        if (needSelect)
        {
           // textList.add("--请选择--");
        	textList.add(ResourceMgt.findDefaultText("log.please.choice"));
            valueList.add("");
        }

        List<UsysProvince> list = null;

        list = (List<UsysProvince>) ds.getUsysProvinceByCond(usysProvince);

        for (UsysProvince item : list)
        {
            textList.add(item.getProvname());
            valueList.add(item.getProvid());
        }

        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);

        return map;

    }

    /**
     * 根据省份下所有城市的状态来判断该省份是否有新增同步或删除同步的操作
     */
    public String operSynByProvdesc(String provdesc, String provindex) throws DomainServiceException
    {
        log.debug("getUsysCityByCond  starting...");
        UsysCity usyscity = new UsysCity();
        usyscity.setProvindex(new Long(provindex));
        usyscity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysCity> listUyscity = null;
        listUyscity = usysCityDS.getUsysCityByCond(usyscity);
        boolean b = true;
        // 省份状态为"初始" 或"新增同步失败" 且当城市的状态都为"初始"时,省份才可新增同步
        if (INITIAL.equals(provdesc) || ADDSYNCHRONIZEDERROR.equals(provdesc))
        {
            if (!listUyscity.isEmpty())
            {
                for (UsysCity ucity : listUyscity)
                {
                    if (!INITIAL.equals(ucity.getCitydesc()))
                    {
                        b = false;
                        break;
                    }
                }
                if (b == false)
                {
                    return CANNOT_ADD_SYN;
                }
                return CAN_ADD_SYN;
            }
            else
            {
                return CAN_ADD_SYN;
            }
        }
        // 省份状态为"已同步"或"删除同步失败" 且 当城市的状态都为"初始"或"新增同步失败"时,省份才可删除同步
        if (SYNCHRONIZED.equals(provdesc) || DELSYNCHRONIZEDERROR.equals(provdesc))
        {
            int initCount = 0;
            int addFailCount = 0;
            if (!listUyscity.isEmpty())
            {
                for (UsysCity ucity : listUyscity)
                {
                    if (INITIAL.equals(ucity.getCitydesc()))
                    {
                        initCount++;
                    }
                    if (ADDSYNCHRONIZEDERROR.equals(ucity.getCitydesc()))
                    {
                        addFailCount++;
                    }
                }
                if (initCount == listUyscity.size() || addFailCount == listUyscity.size())
                {
                    return CAN_DEL_SYN;
                }
                return CANNOT_DEL_SYN;
            }
            else
            {
                return CAN_DEL_SYN;
            }
        }

        log.debug("getUsysCityByCond  ending...");
        return null;
    }

}

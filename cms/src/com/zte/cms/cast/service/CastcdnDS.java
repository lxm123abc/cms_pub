package com.zte.cms.cast.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.dao.ICastcdnDAO;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.common.Generator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CastcdnDS extends DynamicObjectBaseDS implements ICastcdnDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICastcdnDAO dao = null;

    public void setDao(ICastcdnDAO dao)
    {
        this.dao = dao;
    }

    public String insertCastcdn(Castcdn castcdn) throws DomainServiceException
    {

        log.debug("insert castcdn starting...");

        Long castindex = null;
        String castid = null;
        try
        {
            castindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_cast");
            castcdn.setCastindex(castindex);
            castid = Generator.getContentId(Long.valueOf(000000), "CAST");
            castcdn.setCastid(castid);
            castcdn.setCastcode(castid);

            dao.insertCastcdn(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert castcdn end");
        return castid;
    }

    public void updateCastcdn(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("update castcdn by pk starting...");
        try
        {
            castcdn.setModtime(getCurrentTime());
            dao.updateCastcdn(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castcdn by pk end");
    }

    public void updateCastcdnList(List<Castcdn> castcdnList) throws DomainServiceException
    {
        log.debug("update castcdnList by pk starting...");
        if (null == castcdnList || castcdnList.size() == 0)
        {
            log.debug("there is no datas in castcdnList");
            return;
        }
        try
        {
            for (Castcdn cast : castcdnList)
            {
                cast.setModtime(getCurrentTime());
                dao.updateCastcdn(cast);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castcdnList by pk end");
    }

    public void updateCastcdnByCond(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("update castcdn by condition starting...");
        try
        {
            castcdn.setModtime(getCurrentTime());
            dao.updateCastcdnByCond(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castcdn by condition end");
    }

    public void updateCastcdnListByCond(List<Castcdn> castcdnList) throws DomainServiceException
    {
        log.debug("update castcdnList by condition starting...");
        if (null == castcdnList || castcdnList.size() == 0)
        {
            log.debug("there is no datas in castcdnList");
            return;
        }
        try
        {
            for (Castcdn cast : castcdnList)
            {
                cast.setModtime(getCurrentTime());
                dao.updateCastcdnByCond(cast);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castcdnList by condition end");
    }

    public void removeCastcdn(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("remove castcdn by pk starting...");
        try
        {
            dao.deleteCastcdn(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castcdn by pk end");
    }

    public void removeCastcdnList(List<Castcdn> castcdnList) throws DomainServiceException
    {
        log.debug("remove castcdnList by pk starting...");
        if (null == castcdnList || castcdnList.size() == 0)
        {
            log.debug("there is no datas in castcdnList");
            return;
        }
        try
        {
            for (Castcdn cast : castcdnList)
            {
                dao.deleteCastcdn(cast);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castcdnList by pk end");
    }

    public void removeCastcdnByCond(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("remove castcdn by condition starting...");
        try
        {
            dao.deleteCastcdnByCond(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castcdn by condition end");
    }

    public void removeCastcdnListByCond(List<Castcdn> castcdnList) throws DomainServiceException
    {
        log.debug("remove castcdnList by condition starting...");
        if (null == castcdnList || castcdnList.size() == 0)
        {
            log.debug("there is no datas in castcdnList");
            return;
        }
        try
        {
            for (Castcdn cast : castcdnList)
            {
                dao.deleteCastcdnByCond(cast);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castcdnList by condition end");
    }

    public Castcdn getCastcdn(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("get castcdn by pk starting...");
        Castcdn rsObj = null;
        try
        {
            rsObj = dao.getCastcdn(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castcdn by pk end");
        return rsObj;
    }

    public List<Castcdn> getCastcdnByCond(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("get castcdn by condition starting...");
        List<Castcdn> rsList = null;
        try
        {
            rsList = dao.getCastcdnByCond(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castcdn by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get castcdn page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castcdn, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Castcdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castcdn page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get castcdn page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castcdn, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Castcdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castcdn page info by condition end");
        return tableInfo;
    }

    public TableDataInfo querySynedCastcdn(Castcdn castcdn, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get castcdn page info by condition starting...");
        PageInfo pageInfo = null;
        if (castcdn.getStatus() == null || "".equals(castcdn.getStatus()))
        {
            castcdn.setStatusList("0");
        }
        try
        {
            pageInfo = dao.pageInfoQuery(castcdn, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Castcdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castcdn page info by condition end");
        return tableInfo;
    }

    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    public Castcdn getCastById(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("get getCastById starting...");
        Castcdn rsObj = null;
        try
        {
            rsObj = dao.getCastById(castcdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getCastById end");
        return rsObj;
  
    }

}

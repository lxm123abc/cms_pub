package com.zte.cms.operCnttypeMap.service;

import java.util.List;
import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IOperCnttypemapDS
{
    /**
     * 新增OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @throws DomainServiceException ds异常
     */
    public void insertOperCnttypemap(OperCnttypemap operCnttypemap) throws DomainServiceException;

    /**
     * 根据条件更新OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException;

    /**
     * 根据条件批量更新OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateOperCnttypemapListByCond(List<OperCnttypemap> operCnttypemapList) throws DomainServiceException;

    /**
     * 根据条件删除OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException;

    /**
     * 根据条件批量删除OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeOperCnttypemapListByCond(List<OperCnttypemap> operCnttypemapList) throws DomainServiceException;

    /**
     * 根据条件查询OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @return 满足条件的OperCnttypemap对象集
     * @throws DomainServiceException ds异常
     */
    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException;

    /**
     * 根据条件分页查询OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
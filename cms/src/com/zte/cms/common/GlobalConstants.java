package com.zte.cms.common;

import com.zte.cms.common.DbUtil;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class GlobalConstants
{
    public static final String IPTV_SERVICE_KEY = "CMS"; // iptv业务键
    public static final String SYSTEM_SERVICE_KEY = "CMS"; // CMS业务键
    public static String SUPER_OPERID = "1"; // super操作员的id
    public static String PORTAL_MOUNT_POINT = getMountPoint(); // 挂载点

    // 定时器的执行周期设定
    public static String CYCLE_SEQUENCE = "cms.cycle.sequence";
    public static final String SUCCESS = "0";
    public static final String FAIL = "1";

    // 将PORTAL_MOUNT_POINT从数据库中取
    public static String getMountPoint()
    {
        DbUtil dbUtil = new DbUtil();
        String mountPoint = "";
        try
        {
            List list = dbUtil.getQuery("select cfgvalue from usys_config where cfgkey='cms.equ.mountpoint'");
            if (list != null && list.size() > 0)
            {
                Map rtnvalue = (Map) list.get(0);
                mountPoint = (String) rtnvalue.get("cfgvalue");
            }
            else
            {
                mountPoint = "/";
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mountPoint;
    }
    
  //获取同步指令文件库区的地址
    public static String getTempAreAdd() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1");// 同步指令文件库区
        return stroageareaPath;
    }
    
    
    //将字符串中的"\"转换成"/"
    public static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {
                rtnStr = str;
            }
        }
        return rtnStr;
    }
    
    public static String getFtpAddressPrefix()
    {
        UsysConfig usysConfig = new UsysConfig();
        IUsysConfigDS usysConfigDS = (IUsysConfigDS) SSBBus.findDomainService("usysConfigDS");;
        if (null == usysConfig || null == usysConfig.getCfgvalue()
                || (usysConfig.getCfgvalue().trim().equals("")))
        {
            return null;
        }
        
        return usysConfig.getCfgvalue();
    }
}

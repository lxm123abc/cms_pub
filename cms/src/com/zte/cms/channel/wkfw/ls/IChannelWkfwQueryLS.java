package com.zte.cms.channel.wkfw.ls;

import java.util.HashMap;

import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IChannelWkfwQueryLS
{
    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public abstract int queryChannelWkfwTaskForMeListCnt(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryChannelWkfwTaskForMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryChannelWkfwTaskForMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public abstract int queryChannelWkfwProcessWithMeListCnt(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryChannelWkfwProcessWithMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo queryChannelWkfwProcessWithMe(ChannelWkfwCondition channelWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 
     * @param selectedValue String
     * @param needSelect Boolean
     * @return HashMap *
     */
    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect);

    /**
     * 
     * @return HashMap
     */
    public HashMap<String, String> getWorkflowTypeMap();
}
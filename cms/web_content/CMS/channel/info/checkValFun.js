

(function($) {
  $.regexpCommon = function(regexpDesc) {
    return $.regexpCommon.regexpPattern[regexpDesc].call();
  };

  $.regexpCommon.regexpPattern = {
    // numbers
    numberInteger : function() {
      return /^[-+]?[1-9]\d*\.?[0]*$/;
    },
    numberFloat : function() {
      return /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/;
    },
    // email
    email : function() {
      return /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    },
    ssn : function() {
      return /^\d{3}-\d{2}-\d{4}$/;
    },
    url : function() {
      return /^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$/;
    },
    phoneNumberUS : function() {
      return /^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$/;
    },
    zipCodeUS : function() {
      return /^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$/;
    },
    currencyUS : function() {
      return /^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/;
    }, 
    htmlHexCode : function() {
      return /^#([a-fA-F0-9]){3}(([a-fA-F0-9]){3})?$/;
    },
    dottedQuadIP : function() {
      return /^(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4] \d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4] \d|25[0-5])$/;
    },
    macAddress : function() {
      return /^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$/;
    }
  };
}) (jQuery);

//判断是否包含有中文
function hasChineseChars(sCheck){
  for (var nIndex=0; nIndex<sCheck.length; nIndex++){
     cCheck = sCheck.charCodeAt(nIndex);
     if (cCheck > 255)
       return true;
  }
  return false;
}

//判断下拉列表是否选中
function checkSelectVal(id){
  var value=$('#'+id).val();
  if(value==""){
    $('#'+id+'_info').text("请选择");
    return false;
  }
  return true;
}

//设置下拉列表选中值
function setSelectVal(id,val){
  var objs=document.getElementById(id);
  for(var i=0;i<objs.options.length;i++){
    if(val==objs.options[i].value){
      objs.options[i].selected=true;
      break;
    }
  }
}

//判断单选按钮是否选中
function checkRadioVal(name,msgId){
  var objs=document.getElementsByName(name);
  var list=new Array();
  for(var i=0;i<objs.length;i++){
    if(objs[i].checked){
	  list.push(objs[i].value);
	}
  }
  if(list.length<1){
	$('#'+msgId).text="此项不能为空";
    return false;
  }
  return true;
}

//设置单选按钮选中值
function setRadioVal(name,val){
  var objs=document.getElementsByName(name);
  for(var i=0;i<objs.length;i++){
    if(objs[i].value==val){
	  objs[i].checked=true;
	  break;
	}
  }
}



//清空所有错误消息
function clearAllMsg(){
  $("label[id$='_info']").html("");
}

//清空所有查询条件
function clearAllCondition(){
  $("input[id$='_soso']").val("");
}

//判断是否是以某字符为开头
function startWithStr(id,str){
  var value=$('#'+id).val();
  var result=false;
  if(str==null||str==""||str.length>this.length||value.length==0)
   return false;
  if(value.substr(0,str.length)==str)
     return true;
  else
     return false;
  return true;
}

//判断数组中有没有某元素
function hashVal(array,val){
  var result=-1;
  for(var i=0;i<array.length;i++){
    if(array[i]==val){
      result=i;
    }
  }
  return result;
}

//移除数组中的元素
function delVal(array,val){
  var i=hashVal(array,val);
  if(i!=-1)
    array.splice(i,1);
}

//去除数组中的重复元素
function distinct(arr){
  var rs=new Array();
  for(var i=0;i<arr.length;i++){
    var s=","+rs.join(",")+",";
	if(!s.match(","+arr[i]+",")){
      rs.push(arr[i]);
    }
  }
  return rs;
}

function getTime2(str){
  if(str!=null){
	if (str.length > 6){
	  return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	}else{
	  return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
    }
  }else{
    return "";
  }
}

function isMaxLengthWide(input_id,len,bool){

	if(utf8_strlenWide(TrimWide($('#'+input_id).val())) > len){
		focusAndSelect(input_id,bool);
		return false;
    }else{
       return true;   
    }
 }
 
function utf8_strlenWide(str) 
{ 
    var cnt = 0; 
    for( i=0; i<str.length; i++) 
    { 
        var value = str.charCodeAt(i); 
        if( value < 0x080) 
        { 
            cnt += 1; 
        } 
        else if( value < 0x0800) 
        { 
            cnt += 2; 
        } 
        else  if(value < 0x010000)
        { 
            cnt += 3; 
        }
		else
		{
			cnt += 4;
		}
    } 
    return cnt; 
}

/**
 * @param {String} input_id
 * @param (boolean) bool
 * Select the current error data 
 */
function focusAndSelect(input_id,bool){
	bool?$('#'+input_id).one('click',function(){$(this).focus();$(this).select();}):0;
	$('#'+input_id).click()
}

/**
 * 
 * @param {Object} str
 */
function TrimWide(str) 
{
	return RTrimWide(LTrimWide(str));
};

function RTrimWide(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};

function LTrimWide(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
}

var historyStr ="";
function checkMaxInput2(_this,maxleng,leftInforId) 
{
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 

    var remainnum =parseInt(maxleng)-len; 
 
    if(remainnum < 0) 
    {
       _this.value = historyStr;
       return ;
    } 
    historyStr = _this.value;
    remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum;  
}

function utf8_strlen2(str) 
{ 
	var cnt = 0; 
	for( i=0; i<str.length; i++) 
	{ 
	    var value = str.charCodeAt(i); 
	    if( value < 0x080) 
	    { 
	        cnt += 1; 
	    } 
	    else if( value < 0x0800) 
	    { 
	        cnt += 2; 
	    } 
	    else  if(value < 0x010000)
	    { 
	        cnt += 3; 
	    }
		else
		{
			cnt += 4;
		}
	} 
	return cnt; 
}
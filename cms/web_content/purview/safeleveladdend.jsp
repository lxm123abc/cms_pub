<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import = "java.util.*"%>
<%@ include file="JavaFun.jsp" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>
<%
      String flag = request.getParameter("flag");
      String servicekey = request.getParameter("servicekey");
      String param = request.getParameter("param");
      
	    if("2".equals(flag)){
	      String paramname = transferString(request.getParameter("paramname"));
        String value = request.getParameter("value");
        String defaultvalue = request.getParameter("defaultvalue");
        String description = transferString(request.getParameter("description"));
	      pwdrule.addSafeLevel(param,paramname,servicekey,value,defaultvalue,description);
	    }
	    else if("3".equals(flag)){ 
	      String oldparam = request.getParameter("oldparam");
	      String oldservicekey = request.getParameter("oldservicekey");
	      String paramname = transferString(request.getParameter("paramname"));
        String value = request.getParameter("value");
        String defaultvalue = request.getParameter("defaultvalue");
        String description = transferString(request.getParameter("description"));
        pwdrule.updateSafeLevel(oldparam,oldservicekey,param,paramname,oldservicekey,value,defaultvalue,description);	      
	    }
	    response.sendRedirect("safelevelset.jsp");

%>
package com.zte.cms.programcrmmap.service;

import java.util.List;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.dao.IProgramCrmMapDAO;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ProgramCrmMapDS extends DynamicObjectBaseDS implements IProgramCrmMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IProgramCrmMapDAO dao = null;

    public void setDao(IProgramCrmMapDAO dao)
    {
        this.dao = dao;
    }

    public String insertProgramCrmMapRtn(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("insert programCrmMap starting...");
        Long mapindex = null;
        String mappingid = null;
        try
        {
            mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("program_crm_map");
            mappingid = String.format("%02d", 34)+ String.format("%030d", mapindex);
            programCrmMap.setMapindex(mapindex);
            programCrmMap.setMappingid(mappingid);
            programCrmMap.setSequence(0);
            dao.insertProgramCrmMap(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert programCrmMap end");
        return mapindex.toString();
    }
    
    public void insertProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("insert programCrmMap starting...");
        Long mapindex = null;
        String mappingid = null;
        try
        {
            if(programCrmMap.getMapindex() == null){
                mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("program_crm_map");
                mappingid = String.format("%02d", 34)+ String.format("%030d", mapindex);
                programCrmMap.setMapindex(mapindex);
                programCrmMap.setMappingid(mappingid);
                programCrmMap.setSequence(0);
            }
            dao.insertProgramCrmMap(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert programCrmMap end");
    }

    public void updateProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("update programCrmMap by pk starting...");
        try
        {
            dao.updateProgramCrmMap(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programCrmMap by pk end");
    }

    public void updateProgramCrmMapList(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException
    {
        log.debug("update programCrmMapList by pk starting...");
        if (null == programCrmMapList || programCrmMapList.size() == 0)
        {
            log.debug("there is no datas in programCrmMapList");
            return;
        }
        try
        {
            for (ProgramCrmMap programCrmMap : programCrmMapList)
            {
                dao.updateProgramCrmMap(programCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programCrmMapList by pk end");
    }

    public void updateProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("update programCrmMap by condition starting...");
        try
        {
            dao.updateProgramCrmMapByCond(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programCrmMap by condition end");
    }

    public void updateProgramCrmMapListByCond(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException
    {
        log.debug("update programCrmMapList by condition starting...");
        if (null == programCrmMapList || programCrmMapList.size() == 0)
        {
            log.debug("there is no datas in programCrmMapList");
            return;
        }
        try
        {
            for (ProgramCrmMap programCrmMap : programCrmMapList)
            {
                dao.updateProgramCrmMapByCond(programCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programCrmMapList by condition end");
    }

    public void removeProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("remove programCrmMap by pk starting...");
        try
        {
            dao.deleteProgramCrmMap(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programCrmMap by pk end");
    }

    public void removeProgramCrmMapList(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException
    {
        log.debug("remove programCrmMapList by pk starting...");
        if (null == programCrmMapList || programCrmMapList.size() == 0)
        {
            log.debug("there is no datas in programCrmMapList");
            return;
        }
        try
        {
            for (ProgramCrmMap programCrmMap : programCrmMapList)
            {
                dao.deleteProgramCrmMap(programCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programCrmMapList by pk end");
    }

    public void removeProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("remove programCrmMap by condition starting...");
        try
        {
            dao.deleteProgramCrmMapByCond(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programCrmMap by condition end");
    }

    public void removeProgramCrmMapListByCond(List<ProgramCrmMap> programCrmMapList) throws DomainServiceException
    {
        log.debug("remove programCrmMapList by condition starting...");
        if (null == programCrmMapList || programCrmMapList.size() == 0)
        {
            log.debug("there is no datas in programCrmMapList");
            return;
        }
        try
        {
            for (ProgramCrmMap programCrmMap : programCrmMapList)
            {
                dao.deleteProgramCrmMapByCond(programCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programCrmMapList by condition end");
    }

    public ProgramCrmMap getProgramCrmMap(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("get programCrmMap by pk starting...");
        ProgramCrmMap rsObj = null;
        try
        {
            rsObj = dao.getProgramCrmMap(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programCrmMapList by pk end");
        return rsObj;
    }

    public List<ProgramCrmMap> getProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("get programCrmMap by condition starting...");
        List<ProgramCrmMap> rsList = null;
        try
        {
            rsList = dao.getProgramCrmMapByCond(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programCrmMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(programCrmMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programCrmMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get programCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(programCrmMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programCrmMap page info by condition end");
        return tableInfo;
    }
    /************************publish mangagement begin*****************/
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryproCrmMap(programCrmMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programCrmMap page info by condition end");
        return tableInfo;
    }   
    
    public TableDataInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryproCrmTargetMap(programCrmMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programCrmMap page info by condition end");
        return tableInfo;
    }      
    
    public List<ProgramCrmMap> getProgramnormalCrmMapSynByCond(ProgramCrmMap programCrmMap) throws DomainServiceException
    {
        log.debug("get programCrmMap by condition starting...");
        List<ProgramCrmMap> rsList = null;
        try
        {
            rsList = dao.getProgramnormalCrmMapSynByCond(programCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programCrmMap by condition end");
        return rsList;
    }    
    
    /************************publish mangagement begin*****************/
}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.IChannelPictureMapDAO;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ChannelPictureMapDAO extends DynamicObjectBaseDao implements IChannelPictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("insert channelPictureMap starting...");
        super.insert("insertChannelPictureMap", channelPictureMap);
        log.debug("insert channelPictureMap end");
    }

    public void updateChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("update channelPictureMap by pk starting...");
        super.update("updateChannelPictureMap", channelPictureMap);
        log.debug("update channelPictureMap by pk end");
    }

    public void deleteChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("delete channelPictureMap by pk starting...");
        super.delete("deleteChannelPictureMap", channelPictureMap);
        log.debug("delete channelPictureMap by pk end");
    }

    public ChannelPictureMap getChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("query channelPictureMap starting...");
        ChannelPictureMap resultObj = (ChannelPictureMap) super.queryForObject("getChannelPictureMap",
                channelPictureMap);
        log.debug("query channelPictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ChannelPictureMap> getChannelPictureMapByCond(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("query channelPictureMap by condition starting...");
        List<ChannelPictureMap> rList = (List<ChannelPictureMap>) super.queryForList(
                "queryChannelPictureMapListByCond", channelPictureMap);
        log.debug("query channelPictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query channelPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryChannelPictureMapListCntByCond", channelPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ChannelPictureMap> rsList = (List<ChannelPictureMap>) super.pageQuery(
                    "queryChannelPictureMapListByCond", channelPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query channelPictureMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryChannelPictureMapListByCond", "queryChannelPictureMapListCntByCond",
                channelPictureMap, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query channelPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryChannePictureMapSynListCntByCond", channelPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ChannelPictureMap> rsList = (List<ChannelPictureMap>) super.pageQuery(
                    "queryChannePictureMapSynListByCond", channelPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query channelPictureMap by condition end");
        return pageInfo;
    }
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query channelPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryChannePictureTargetMapSynListCntByCond", channelPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ChannelPictureMap> rsList = (List<ChannelPictureMap>) super.pageQuery(
                    "queryChannePictureTargetMapSynListByCond", channelPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query channelPictureMap by condition end");
        return pageInfo;
    }    
    @SuppressWarnings("unchecked")
    public List<ChannelPictureMap> getChannelPicturenormalMapByCond(ChannelPictureMap channelPictureMap) throws DAOException
    {
        log.debug("query channelPictureMap by condition starting...");
        List<ChannelPictureMap> rList = (List<ChannelPictureMap>) super.queryForList(
                "getChannePictureTargetMapSynListByCond", channelPictureMap);
        log.debug("query channelPictureMap by condition end");
        return rList;
    }    
}
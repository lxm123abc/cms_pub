package com.zte.cms.server.ls;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import com.zte.ssb.ria.parsexml.ParseResource;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;

public class getErrorMsg
{

    public static String ERRORCODE_PREFIX = "";

    /**
     * 语言信息保存在session中的key
     */
    private static final String SESSION_LOCALE = "web_key_session_locale";

    /** LOCALE */
    public static String LOCALE = "zh";

    /**
     * 根据资源key获取资源信息
     * 
     * @param key 资源key
     * @return 资源信息
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public static String getResourceTextByCode(String errorcode)
    {

        Locale userLocale = getLocale();
        errorcode = ERRORCODE_PREFIX + errorcode;

        String returnValue = ParseResource.getResourceValueByKey(errorcode, userLocale.toString());
        if (returnValue == null || "".equals(returnValue.trim()))
        {
            returnValue = errorcode;
        }

        return returnValue;
    }

    /**
     * 获取当前登陆操作员的Locale
     * 
     * @return
     */
    private static Locale getLocale()
    {
        Locale userLocale = null;
        // 先从session中得local
        RIAContext context = RIAContext.getCurrentInstance();
        if (null != context)
        {
            ISession session = context.getSession();
            if (null != session)
            {
                HttpSession httpSession = session.getHttpSession();
                if (null != httpSession)
                {
                    Object o = httpSession.getAttribute(SESSION_LOCALE);
                    if (null != o)
                    {
                        userLocale = (Locale) o;
                    }
                }
            }
        }

        // 再从应用配置中获得
        if (null == userLocale)
        {
            String local = LOCALE;
            userLocale = new Locale(local);
        }
        return userLocale;
    }
}

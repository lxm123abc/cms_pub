package com.zte.cms.ctgpgmap.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CategoryProgramMapDAO extends DynamicObjectBaseDao implements ICategoryProgramMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException
    {
        log.debug("insert categoryProgramMap starting...");
        super.insert("insertCategoryProgramMap", categoryProgramMap);
        log.debug("insert categoryProgramMap end");
    }

    public void updateCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException
    {
        log.debug("update categoryProgramMap by pk starting...");
        super.update("updateCategoryProgramMap", categoryProgramMap);
        log.debug("update categoryProgramMap by pk end");
    }

    public void deleteCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException
    {
        log.debug("delete categoryProgramMap by pk starting...");
        super.delete("deleteCategoryProgramMap", categoryProgramMap);
        log.debug("delete categoryProgramMap by pk end");
    }

    public CategoryProgramMap getCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException
    {
        log.debug("query categoryProgramMap starting...");
        CategoryProgramMap resultObj = (CategoryProgramMap) super.queryForObject("getCategoryProgramMap",
                categoryProgramMap);
        log.debug("query categoryProgramMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CategoryProgramMap> getCategoryProgramMapByCond(CategoryProgramMap categoryProgramMap)
            throws DAOException
    {
        log.debug("query categoryProgramMap by condition starting...");
        List<CategoryProgramMap> rList = (List<CategoryProgramMap>) super.queryForList(
                "queryCategoryProgramMapListByCond", categoryProgramMap);
        log.debug("query categoryProgramMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query categoryProgramMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCategoryProgramMapListCntByCond", categoryProgramMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CategoryProgramMap> rsList = (List<CategoryProgramMap>) super.pageQuery(
                    "queryCategoryProgramMapListByCond", categoryProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query categoryProgramMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCategoryProgramMapListByCond", "queryCategoryProgramMapListCntByCond",
                categoryProgramMap, start, pageSize, puEntity);
    }

    /****** publish management *************/
    
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DAOException
    {
    	log.debug("page query programPlatformSyn by condition starting...");
     	PageInfo pageInfo = null;
     	if (categoryProgramMap.getTypePlatform().equals("iptv3") )
     	{
        	int totalCnt = ((Integer) super.queryForObject("queryCategoryProgramMapSynListCntByCond",  categoryProgramMap)).intValue();
        	if( totalCnt > 0 )
        	{
        		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
        		List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCategoryProgramMapSynListByCond" ,  categoryProgramMap , start , fetchSize );
        		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        	}
        	else
        	{
        		pageInfo = new PageInfo();
        	}
     		
     	}
     	if (categoryProgramMap.getTypePlatform().equals("iptv2") )
     	{

        	int totalCnt = ((Integer) super.queryForObject("queryCategoryProgramMapSyn2ListCntByCond",  categoryProgramMap)).intValue();
        	if( totalCnt > 0 )
        	{
        		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
        		List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCategoryProgramMapSyn2ListByCond" ,  categoryProgramMap , start , fetchSize );
        		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        	}
        	else
        	{
        		pageInfo = new PageInfo();
        	}
     		
     	}

    	log.debug("page query categoryProgramMap by condition end");
    	return pageInfo;
    }    
  
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DAOException
    {
    	log.debug("page query programPlatformSyn by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("querynormaltargetSynListCntByCond",  categoryProgramMap)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "querynormaltargetSynListByCond" ,  categoryProgramMap , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
 		


    	log.debug("page query categoryProgramMap by condition end");
    	return pageInfo;
    } 	
	
    @SuppressWarnings("unchecked")
    public List<CategoryProgramMap> getnormaltargetSynListByCond(CategoryProgramMap categoryProgramMap)
            throws DAOException
    {
        log.debug("query categoryProgramMap by condition starting...");
        List<CategoryProgramMap> rList = (List<CategoryProgramMap>) super.queryForList(
                "getnormaltargetSynListByCond", categoryProgramMap);
        log.debug("query categoryProgramMap by condition end");
        return rList;
    }	
	
    /****** publish management end ********/    
}
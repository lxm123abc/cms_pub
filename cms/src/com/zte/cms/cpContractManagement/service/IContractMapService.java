package com.zte.cms.cpContractManagement.service;

import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import java.util.List;

public interface IContractMapService {

    //查询
    TableDataInfo pageInfoQuery(ContractMap contractMap,int start, int pageSize) throws DomainServiceException;

    //删除
     String deleteConstractByIds(String[] ids) throws DomainServiceException;

    //根据id查找合同
    ContractMap selectContractById(Integer id)throws DomainServiceException;

    //根据id修改合同
    ContractMap updateContractById(ContractMap contractMap)throws DomainServiceException;

    List<ContractMap> getAttachListById(ContractMap contractMap)throws DomainServiceException;

    String deleteContractAttach(String attachid)throws DomainServiceException;

    String insertContract(ContractMap contractMap,String content)throws DomainServiceException;

}

package com.zte.cms.content.batchUpload.ls;

import java.util.ArrayList;
import java.util.List;

public class ReturnInfo
{

    /** 业务处理操作返回操作标志 0：成功， 1：失败 */
    private String flag;

    /** 业务处理操作返回操作信息 成功=0， 失败返回错误码多语化后的信息 */
    private String returnMessage;

    /** 批量处理时封装单条记录的操作信息 */
    private List<OperateInfo> operateInfoList = new ArrayList<OperateInfo>();

    public ReturnInfo()
    {
        this.flag = "0";
        this.returnMessage = "";
    }

    public ReturnInfo(String flag, String returnMessage)
    {
        this.flag = flag;
        this.returnMessage = returnMessage;
    }

    public String getFlag()
    {
        return flag;
    }

    public void setFlag(String flag)
    {
        this.flag = flag;
    }

    public String getReturnMessage()
    {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage)
    {
        this.returnMessage = returnMessage;
    }

    public List<OperateInfo> getOperateInfoList()
    {
        return operateInfoList;
    }

    public void setOperateInfoList(List<OperateInfo> operateInfoList)
    {
        this.operateInfoList = operateInfoList;
    }

    /**
     * 追加单条记录的错误信息
     * 
     * @param errorInfo
     */
    public void appendOperateInfo(OperateInfo operateInfo)
    {
        this.operateInfoList.add(operateInfo);
    }

    /**
     * 当前对象字符串化 例：对象转化为
     * ({main:'returnMessage',list:'([({id:\"id\",name:\"name\",flag:\"flag\",message:\"message\"}),])'})
     */
    public String toString()
    {
        StringBuffer out = new StringBuffer();
        out.append("({"); // 开始
        out.append("main:"); // 当前操作主信息 0：成功， 其他code多语化后返回
        String rtnMessage = "";
        // 替换英文单引号(')为(\') ，java代码层返回到页面(\'),这样eval()解析时，会把(\')转义为可显示的单引号
        // (\)同上
        // 替换原因：英文单引号(')为JS的eval()方法解析字符串的分隔符
        rtnMessage = replaceChar(this.returnMessage);
        out.append("'" + flag + ":" + rtnMessage + "'");
        out.append(","); // main与list的分隔符
        out.append("list:"); // 批量处理单条操作信息
        out.append("'");
        if (operateInfoList.size() > 0)
        {// "({id:'001',name:'lao5',message:'([({id:99,name:\"info\"}),({})])'})";
            int size = operateInfoList.size();
            out.append("(["); // list 部分开始
            OperateInfo opInfo = null;
            for (int i = 0; i < size; i++)
            {
                opInfo = operateInfoList.get(i);
                out.append("({");
                out.append("id:\\\"" + opInfo.getId() + "\\\",");
                out.append("name:\\\"" + opInfo.getName() + "\\\",");
                out.append("flag:\\\"" + opInfo.getFlag() + "\\\",");
                String opMessage = replaceChar(opInfo.getMessage());
                out.append("message:\\\"" + opMessage + "\\\"");
                out.append("})");

                if (i < size - 1) // 表示非list最后槽,追加分隔符逗号","
                {
                    out.append(","); // list内部对象之间的分隔符
                }
            }
            out.append("");
            out.append("])"); // list 部分结束
        }
        out.append("'");
        out.append("})"); // 结束
        return out.toString();
    }

    /**
     * 转化多语化后字符串中的英文单引号 转化原因：英文单引号(')为JS的eval()方法解析字符串的分隔符
     * 
     * @param oldString
     * @return
     */
    public String replaceChar(String oldString)
    {
        // 转化为char数组
        char[] oldChar = oldString.toCharArray();
        int len = oldChar.length;
        char[] newChar = new char[4 * len]; // 新char数组 长度为old的4倍
        int newCount = 0;
        for (int i = 0; i < len; i++)
        {
            switch ((int) oldChar[i])
            {
                case 39: // 39 英文单引号的整型
                    newChar[newCount] = (char) 92;
                    newCount++;
                    newChar[newCount] = (char) 39;
                    newCount++;
                    break; // ' 变为 \'
                case 92: // 92 \的整型
                    newChar[newCount] = (char) 92;
                    newCount++;
                    newChar[newCount] = (char) 92;
                    newCount++;
                    break; // \ 变为 \\
                default:
                    newChar[newCount] = oldChar[i];
                    newCount++;
            }
        }

        String newString = String.valueOf(newChar);
        newString = newString.trim();
        return newString;
    }
}

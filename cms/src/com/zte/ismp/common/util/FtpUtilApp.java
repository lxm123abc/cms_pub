package com.zte.ismp.common.util;

import sun.net.ftp.FtpClient;
import java.io.*;
import sun.net.TelnetOutputStream;
import org.apache.log4j.Logger;
import sun.net.TelnetInputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * <p>
 * 文件名称: FtpUtil.java
 * </p>
 * <p>
 * 文件描述: FTP操作类
 * </p>
 * FTP远程命令列表<br>
 * USER PORT RETR ALLO DELE SITE XMKD CDUP FEAT<br>
 * PASS PASV STOR REST CWD STAT RMD XCUP OPTS<br>
 * ACCT TYPE APPE RNFR XCWD HELP XRMD STOU AUTH<br>
 * REIN STRU SMNT RNTO LIST NOOP PWD SIZE PBSZ<br>
 * QUIT MODE SYST ABOR NLST MKD XPWD MDTM PROT<br>
 * 在服务器上执行命令,如果用sendServer来执行远程命令(不能执行本地FTP命令)的话，所有FTP命令都要加上\r\n<br>
 * ftpclient.sendServer("XMKD /test/bb\r\n"); //执行服务器上的FTP命令<br>
 * ftpclient.readServerResponse一定要在sendServer后调用<br>
 * nameList("/test")获取指目录下的文件列表<br>
 * XMKD建立目录，当目录存在的情况下再次创建目录时报错<br>
 * XRMD删除目录<br>
 * DELE删除文件<br>
 * ASCII方式：只能传输一些如txt文本文件， zip、jpg等文件需要使用BINARY方式
 * <p>
 * Title: 使用JAVA操作FTP服务器(FTP客户端)
 * </p>
 * <p>
 * Description: 上传文件的类型及文件大小都放到调用此类的方法中去检测，比如放到前台JAVASCRIPT中去检测等 针对FTP中的所有调用使用到文件名的地方请使用完整的路径名（绝对路径开始）。
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: FTP操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class FtpUtilApp
{
    // ****** 代码段: 常量定义 *******************************************************************************/
    public static final int STATE_SUCCESS = 0;
    public static final int STATE_FAIL = 1;

    // ****** 代码段: 变量定义 *******************************************************************************/
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private FtpClient ftpClient;

    // ****** 代码段: 构造方法 *******************************************************************************/
    public FtpUtilApp()
    {
    }

    // ****** 代码段: 公共方法 *******************************************************************************/
    /**
     * 建立FTP连接
     * 
     * @param host String
     * @param user String
     * @param password String
     * @return boolean
     */
    public boolean connect(String host, String user, String password)
    {
        // server：FTP服务器的IP地址；user:登录FTP服务器的用户名
        // password：登录FTP服务器的用户名的口令；path：FTP服务器上的路径
        try
        {
            ftpClient = new FtpClient();
            ftpClient.openServer(host);
            ftpClient.login(user, password);
            // System.out.println("login success!");
            // ftpClient.sendServer("mkdir /servFilePath");
            ftpClient.binary();
        }
        catch (IOException e)
        {
            logger.debug("ftpClient.login error: ", e);
            return false;
        }
        return true;
    }

    /**
     * 断开FTP链接
     */
    public void disconnect()
    {
        try
        {
            ftpClient.closeServer();
        }
        catch (IOException ex)
        {
            logger.error("ftpClient.closeServer() error: ", ex);
        }
    }

    /**
     * 通过FTP上传文件，将路径为localUrl文件保存到FTP服务器的remotePath路径上
     * 
     * @param localUrl String
     * @param remotePath String
     * @return boolean
     */
    public boolean upload(String localUrl, String remotePath)
    {
        java.io.File localFile = new java.io.File(localUrl);
        if (localFile.isFile())
        {
            String filename = localFile.getName();
            // 从filename中分析出文件的名称，作为目标文件的名称,具体方法实现未给出
            if (remotePath.length() != 0)
            {
                try
                {
                    ftpClient.cd(remotePath);
                }
                catch (IOException ex)
                {
                    logger.debug("ftpClient.cd [" + remotePath + "] error: ", ex);
                    return false;
                }
            }

            logger.debug("正在上传文件 [" + localUrl + "], 请等待....");
            try
            {
                TelnetOutputStream os = ftpClient.put(filename);
                FileInputStream is = new FileInputStream(localUrl);
                byte[] bytes = new byte[1024];
                int c;
                while ((c = is.read(bytes)) != -1)
                {
                    os.write(bytes, 0, c);
                }
                is.close();
                os.close();
                logger.debug("上传[" + localUrl + "]文件到[" + remotePath + "]目录成功!");
            }
            catch (IOException ex)
            {
                logger.debug("上传[" + localUrl + "]文件到[" + remotePath + "]目录失败!", ex);
                return false;
            }
        }
        return true;
    }

    /**
     * 通过FTP上传文件，可以更改文件名
     * 
     * @param localPath String
     * @param localName String
     * @param remotePath String
     * @param remoteName String
     * @return boolean
     */
    public boolean upload(String localPath, String localName, String remotePath, String remoteName)
    {
        java.io.File localFile = new java.io.File(localPath + localName);
        if (localFile.isFile())
        {
            // 从filename中分析出文件的名称，作为目标文件的名称,具体方法实现未给出
            if (remotePath != null && remotePath.length() != 0)
            {
                try
                {
                    ftpClient.cd(remotePath);
                }
                catch (IOException ex)
                {
                    logger.debug("ftpClient.cd [" + remotePath + "] error: ", ex);
                    return false;
                }
            }

            logger.debug("正在上传文件 [" + localPath + "/" + localName + "], 请等待....");
            try
            {
                TelnetOutputStream os = ftpClient.put(remoteName);
                FileInputStream is = new FileInputStream(localPath + localName);
                byte[] bytes = new byte[1024];
                int c;
                while ((c = is.read(bytes)) != -1)
                {
                    os.write(bytes, 0, c);
                }
                is.close();
                os.close();
                logger.debug("上传[" + localName + "]文件到[" + remotePath + "]目录成功!");
            }
            catch (IOException ex)
            {
                logger.debug("上传[" + localName + "]文件到[" + remotePath + "]目录失败!", ex);
                return false;
            }
        }
        return true;
    }

    /**
     * 下载文件
     * 
     * @param remotePath String
     * @param remoteName String
     * @param localPath String
     * @param localName String
     * @return boolean
     */
    public boolean download(String remotePath, String remoteName, String localPath, String localName)
    {
        // 远程路经不为空时进行路经的重定向
        if (remotePath != null && remotePath.length() != 0)
        {
            try
            {
                ftpClient.cd(remotePath);
            }
            catch (IOException ex)
            {
                logger.debug("ftpClient.cd error: ", ex);
                return false;
            }
        }

        logger.debug("正在下载文件" + remoteName + ",请等待....");
        try
        {
            TelnetInputStream is = ftpClient.get(remoteName);
            FileOutputStream os = new FileOutputStream(localPath + localName);
            int c;
            byte[] bytes = new byte[1024];
            while ((c = is.read(bytes)) != -1)
            {
                os.write(bytes, 0, c);
            }
            is.close();
            os.close();
            logger.debug("下载[" + remoteName + "]文件到[" + localPath + "]目录成功!");
        }
        catch (IOException ex)
        {
            logger.error("下载[" + remoteName + "]文件到[" + localPath + "]目录失败!", ex);
            return false;
        }
        return true;
    }

    /**
     * 删除文件
     * 
     * @param remotePath String
     * @param remoteName String
     * @param ft FtpUtil
     * @return boolean
     */
    public boolean deleteFile(String fileName)
    {
        // 发送删除命令
        ftpClient.sendServer("DELE " + fileName + "\r\n");
        logger.debug("删除下的文件[" + fileName + "]");
        return true;
    }

    /**
     * 删除目录
     * 
     * @param folderName String
     * @return boolean
     */
    public boolean deleteFolder(String fileName)
    {
        // 发送删除命令
        ftpClient.sendServer("XRMD " + fileName + "\r\n");
        logger.debug("删除下的目录[" + fileName + "]");
        return true;
    }

    /**
     * 返回指定目录的所有文件及文件夹
     * 
     * @return ArrayList
     * @throws IOException
     */
    public ArrayList getFileList(String path)
    {
        ArrayList al = new ArrayList();
        try
        {
            BufferedReader dr = new BufferedReader(new InputStreamReader(ftpClient.nameList(path)));
            String s = "";
            while ((s = dr.readLine()) != null)
            {
                al.add(s);
            }
        }
        catch (IOException ex)
        {
            logger.error("getFileList error: ", ex);
        }
        return al;
    }

    /**
     * 创建目录
     * 
     * @param remotePath String
     * @param dirName String
     * @param ft FtpUtil
     * @return boolean
     */
    public boolean mkDir(String pathName)
    {
        try
        {
            ftpClient.ascii();
            StringTokenizer s = new StringTokenizer(pathName, "/"); // sign

            String tempPathName = "";
            if (pathName.charAt(0) != '/')
            {
                tempPathName = ".";
            }

            while (s.hasMoreElements())
            {
                tempPathName = tempPathName + "/" + (String) s.nextElement();
                try
                {
                    ftpClient.sendServer("XMKD " + tempPathName + "\r\n");
                }
                catch (Exception e)
                {
                    e = null;
                }
                int reply = ftpClient.readServerResponse();
            }
            ftpClient.binary();
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
    }

    /**
     * 判断文件是否存在
     * 
     * @param remotePath String
     * @param remoteName String
     * @return boolean
     */
    public boolean isExistFile(String fileName)
    {
        boolean ret = false;

        // 如果为目录
        if (fileName.charAt(fileName.length() - 1) == '/')
        {
            try
            {
                String oldpath = ftpClient.pwd();
                ftpClient.cd(fileName);
                // 回到原先的目录
                ftpClient.cd(oldpath);
                ret = true;
            }
            catch (Exception ex)
            {
                ex = null;
            }
        }
        // 如果为文件
        else
        {
            try
            {
                String oldpath = ftpClient.pwd();
                String path = fileName.substring(0, fileName.lastIndexOf("/") + 1);
                String name = fileName.substring(fileName.lastIndexOf("/") + 1);

                // 该方法使用时，nameList返回的值对不同的FTP服务器可能不同，
                // 测试中发现对我们的10.40.105.15和Server-U的服务器返回的是名称，但10.40.40.100返回的是带路径的名称
                // 因此，采用list解析文件列表的方式来获取文件名
                // BufferedReader dr = new BufferedReader(new InputStreamReader(ftpClient.nameList(path)));
                // String s = "";
                //
                // while((s = dr.readLine()) != null)
                // {
                // System.out.println(s);
                // if(name.equals(s))
                // {
                // ret = true;
                // break;
                // }
                // }

                ftpClient.cd(path);
                BufferedReader dr = new BufferedReader(new InputStreamReader(ftpClient.list()));
                String s = "";

                while ((s = dr.readLine()) != null)
                {
                    logger.debug(s);
                    // 对文件夹则忽略
                    if (s.charAt(0) == 'd')
                    {
                        continue;
                    }

                    // 判断文件名是否存在文件行信息中
                    int index = s.lastIndexOf(name);
                    if (index > 0)
                    {
                        // 文件名存在
                        ret = true;
                        break;
                    }

                }
                ftpClient.cd(oldpath);
            }
            catch (Exception ex)
            {
                ex = null;
            }
        }
        return ret;
    }

    /**
     * 获取文件的大小 注意：文件的名称中目前暂时不能含有空格
     * 
     * @param fileName String
     * @return long
     */
    public long getFileSize(String fileName)
    {
        long ret = -1L; // 表示不存在
        try
        {
            String oldpath = ftpClient.pwd();
            String path = fileName.substring(0, fileName.lastIndexOf("/") + 1);
            String name = fileName.substring(fileName.lastIndexOf("/") + 1);
            ftpClient.cd(path);

            BufferedReader dr = new BufferedReader(new InputStreamReader(ftpClient.list()));
            String s = "";

            StringTokenizer st = null; // sign
            while ((s = dr.readLine()) != null)
            {
                // 对文件夹则忽略
                if (s.charAt(0) == 'd')
                {
                    continue;
                }

                // 判断文件名是否存在文件行信息中
                int index = s.lastIndexOf(name);
                if (index > 0)
                {
                    // 文件名存在，先将文件名过滤掉
                    s = s.substring(0, index);

                    // 以空格分解剩余的字符
                    st = new StringTokenizer(s, " ");
                    String[] attr = new String[st.countTokens()];
                    int i = 0;
                    while (st.hasMoreElements())
                    {
                        attr[i] = (String) st.nextElement();
                        i++;
                    }
                    ret = Integer.parseInt(attr[4]);
                    break;
                }
            }
            ftpClient.cd(oldpath);
        }
        catch (IOException ex)
        {
            logger.error("isExistFile error: ", ex);
        }

        return ret;
    }

    public boolean rename(String srcFileName, String desFileName)
    {
        boolean ret = false;
        try
        {
            ftpClient.rename(srcFileName, desFileName);
            ret = true;
            logger.debug("更改文件[" + srcFileName + "] 为 [" + desFileName + "]");
        }
        catch (Exception ex)
        {
            ex = null;
        }

        return ret;
    }

    // ****** 代码段: 公共方法,方法内自带FTP建立与释放 *******************************************************************************/
    /**
     * 上传一个文件
     * 
     * @param host ： FTP服务器IP地址
     * @param user ：用户名
     * @param password ：密码
     * @param localPath ：本地路径
     * @param localName ：本地文件名
     * @param remotePath ：文件在ftp服务器上的绝对路径
     * @param remoteName ：文件在ftp服务器上的名称
     */
    public boolean upload(String host, String user, String password, String localPath, String localName,
            String remotePath, String remoteName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);

            if (isconnect)
            {
                if (!isExistFile(remotePath))
                {
                    mkDir(remotePath);
                }

                ret = this.upload(localPath, localName, remotePath, remoteName);
                logger.debug("上传文件" + localName + "为" + remoteName + " ——成功");
            }
            else
            {
                logger.error("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 将remotePath目录的文件下载并保存到localPath目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param remotePath String
     * @param remoteName String
     * @param localPath String
     * @param localName String
     * @return boolean Modify:Dongyang,20060922
     */
    public boolean download(String host, String user, String password, String remotePath, String remoteName,
            String localPath, String localName)
    {
        boolean ret = false;

        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.download(remotePath, remoteName, localPath, localName);
                logger.debug("下载文件" + remoteName + "为" + localName + " ——成功");
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
            ret = false;
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 删除FTP服务器上的文件
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean deleteFile(String host, String user, String password, String fileName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                boolean b = this.isExistFile(fileName);

                if (!b)
                { // 无此文件，就当作删除成功
                    ret = true;
                }
                else
                { // 有此文件，执行删除
                    ret = this.deleteFile(fileName);
                }
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 删除FTP服务器上的目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean deleteFolder(String host, String user, String password, String fileName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                boolean b = this.isExistFile(fileName);

                if (!b)
                { // 无此文件，就当作删除成功
                    ret = true;
                }
                else
                { // 有此文件，执行删除
                    ret = this.deleteFolder(fileName);
                }
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 判断文件是否存在
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean isExistFile(String host, String user, String password, String fileName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.isExistFile(fileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 获取文件的大小
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return long
     */
    public long getFileSize(String host, String user, String password, String fileName)
    {
        long ret = -1;

        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.getFileSize(fileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 重命名文件
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param srcFileName String
     * @param desFileName String
     * @return boolean
     */
    public boolean rename(String host, String user, String password, String srcFileName, String desFileName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.rename(srcFileName, desFileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 创建目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param pathName String
     * @return boolean
     */
    public boolean mkDir(String host, String user, String password, String pathName)
    {
        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.mkDir(pathName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    public static void main(String[] args)
    {
        FtpUtilApp ftpOperation = new FtpUtilApp();
        // ftpOperation.connect("10.40.40.100", "pub", "pub");
        // ftpOperation.upload("C:/core.zip", "/");

        ftpOperation.connect("10.40.113.112", "iptv", "iptv");
        // ftpOperation.connect("10.40.43.17", "123456", "123456");
        // ftpOperation.download("kjh", "Util.java", "c:/", "testtt.mp3");
        // ftpOperation.mkDir("5452");
        // ftpOperation.mkDir("5451/222/111/");
        // ftpOperation.rename("kjh/11.so.java", "12.so");
        // System.out.println(ftpOperation.isExistFile("kjh/123/11/"));
        // System.out.println(ftpOperation.isExistFile("kjh/Util.java"));
        // System.out.println(ftpOperation.deleteFile("kjh/123/Util.java"));
        // System.out.println(ftpOperation.getFileSize("1111/JSP 语法--HTML注释.chm"));
        // System.out.println(ftpOperation.getFileSize("kjh/11.so"));
        // System.out.println(ftpOperation.isExistFile("/home/iptv/PermissionCheckServer.class"));
        ftpOperation.deleteFolder("tomcat/webapps/download/mms/templet/templet1");
        ftpOperation.deleteFile("tomcat/webapps/download/mms/templet/t.zip");
        ftpOperation.download("/home/iptv/./kjh/", "11.so", "c:/", "111.ss");
        ftpOperation.upload("C:/core.zip", "kjh/");

        // ftpOperation.download("D:/FtpServer/hms/伴奏/", "IOIO(剪切).wma", "c:/", "IOIO(剪切)111.wma");

        ftpOperation.disconnect();

        ftpOperation.connect("10.40.40.100", "pub", "pub");
        System.out.println(ftpOperation.isExistFile("./download/entry/10000007.zip"));
        System.out.println(ftpOperation.isExistFile("download/entry/10000007.zip"));
        ftpOperation.disconnect();

    }
}

package com.zte.cms.cpContractManagement.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

/**
 * @Author: Liangxiaomin
 * @Date Created in 17:42 2019/6/24
 * @Description:
 */
public class ContractEmailSendLog  extends DynamicBaseObject {

    private String id;
    private String oid;
    private String senddt;
    private int grade;

    @Override
    public void initRelation() {
        this.addRelation("id", "ID");
        this.addRelation("oid", "OID");
        this.addRelation("senddt", "SENDDT");
        this.addRelation("grade", "GRADE");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSenddt() {
        return senddt;
    }

    public void setSenddt(String senddt) {
        this.senddt = senddt;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }


}

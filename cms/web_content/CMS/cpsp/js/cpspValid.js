
//特殊字符过滤
function filtrate(object){
    object.value = object.value.replace(/[-_`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\<\>\/?]/g,'');
}

function cpspValid(){
    var error = true;
    //CPSP中文名简写
    var cpShortName = document.getElementById('cpShortName');
    error = inputIsNull(cpShortName,'cpShortName_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpShortName,'cpShortName_msg','30');
    if(error == false){
        return false;
    }
    
    //CPSP英文简写
  /*  var cpShortEnName = document.getElementById('cpShortEnName');
    error = IsEnglish(cpShortEnName,'cpShortEnName_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpShortEnName,'cpShortEnName_msg','90');
    if(error == false){
        return false;
    }*/
    //CPSP中文全名
    var cpcnFullName = document.getElementById('cpcnFullName');
    error = inputIsNull(cpcnFullName,'cpcnFullName_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpcnFullName,'cpcnFullName_msg','255');
    if(error == false){
        return false;
    }
    
    //CPSP英文名称
   /* var cpenFullName = document.getElementById('cpenFullName');
    error = IsEnglish(cpenFullName,'cpenFullName_msg');
    if(error == false){
        return false;
    }   
    error = IsMaxLength(cpenFullName,'cpenFullName_msg','255');
    if(error == false){
        return false;
    }*/  
    //cpsp服务类型
    var cpSrvType = document.getElementById('cpSrvType');
    error = inputIsNull(cpSrvType,'cpSrvType_msg');
    if(error == false){
        return false;
    }
    
    //增值业务经营许可证编号
    var icpCode = document.getElementById('icpCode');
    error = inputIsNull(icpCode,'icpCode_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(icpCode,'icpCode_msg','40');
    if(error == false){
        return false;
    }
    
    //增值业务经营许可证颁发地
    var icpAddress = document.getElementById('icpAddress');
    error = IsMaxLength(icpAddress,'icpAddress_msg','300');
    if(error == false){
        return false;
    }
      
    //公司的法人代表
    var legalPerson = document.getElementById('legalPerson');
    error = IsMaxLength(legalPerson,'legalPerson_msg','60');
    if(error == false){
        return false;
    }
    
    //客服电话
    var custSrvTel = document.getElementById('custSrvTel');
    error = inputIsNull(custSrvTel,'custSrvTel_msg');
    if(error == false){
        return false;
    }
    error = isPhoneFormat(custSrvTel,'custSrvTel_msg');
    if(error == false){
        return false;
    }
    
    //客户服务url地址
    var custSrvUrl = document.getElementById('custSrvUrl');
    error = isURLFormat(custSrvUrl,'custSrvUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(custSrvUrl,'custSrvUrl_msg','255');
    if(error == false){
        return false;
    }
    
    //主页
    var homepageUrl = document.getElementById('homepageUrl');
    error = IsMaxLength(homepageUrl,'homepageUrl_msg','255');
    if(error == false){
        return false;
    }    
    
    //邮箱地址
    var emailAddr = document.getElementById('emailAddr');
    error = ismailFormat(emailAddr,'emailAddr_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(emailAddr,'emailAddr_msg','120');
    if(error == false){
        return false;
    }
     
    //开户银行
    var bankName = document.getElementById('bankName');
    error = IsMaxLength(bankName,'bankName_msg','255');
    if(error == false){
        return false;
    }
    
    //开户帐号
    var bankAccount = document.getElementById('bankAccount');
    error = IsMaxLength(bankAccount,'bankAccount_msg','40');
    if(error == false){
        return false;
    }    
    
    //注册资金,单位(万元)
    var capital = document.getElementById('capital');
    error = isPosiint(capital,'capital_msg');
    if(error == false){
        return false;
    } 
    
    //营业执照编号
    var licenseCode = document.getElementById('licenseCode');
    error = IsMaxLength(licenseCode,'licenseCode_msg','40');
    if(error == false){
        return false;
    } 
    
    //营业执照登记机关
    var licenseOrgan = document.getElementById('licenseOrgan');
    error = IsMaxLength(licenseOrgan,'licenseOrgan_msg','300');
    if(error == false){
        return false;
    }
    
    //企业登记经营范围 
    var businessScope = document.getElementById('businessScope');
    error = IsMaxLength(businessScope,'businessScope_msg','300');
    if(error == false){
        return false;
    }
    
    //所在国家
     var countryName = document.getElementById('countryName');
    error = IsMaxLength(countryName,'countryName_msg','90');
    if(error == false){
        return false;
    } 
    
    //公司地址
     var corpaddr = document.getElementById('corpaddr');
    error = IsMaxLength(corpaddr,'corpaddr_msg','255');
    if(error == false){
        return false;
    } 
    
    //通信地址
    var postaddress = document.getElementById('postaddress');
    error = IsMaxLength(postaddress,'postaddress_msg','255');
    if(error == false){
        return false;
    }                  
   
    //公司的联系电话
    var corptel = document.getElementById('corptel');
    error = isPhoneFormat(corptel,'corptel_msg');
    if(error == false){
        return false;
    }
    
    //传真
    var corpfax = document.getElementById('corpfax');
    error = IsMaxLength(corpfax,'corpfax_msg','20');
    if(error == false){
        return false;
    } 
    //CPSP描述
    var description = document.getElementById('description');
    error = IsMaxLength(description,'description_msg','255');
    if(error == false){
        return false;
    } 
    
    //CPSP英文描述
  /*  var descriptionen = document.getElementById('descriptionen');
    error = IsEnglish(descriptionen,'descriptionen_msg');
    if(error == false){
        return false;
    } 
    error = IsMaxLength(descriptionen,'descriptionen_msg','255');
    if(error == false){
        return false;
    }  */         
    
    //CPSP基本信息邮政编码

	var postcode = document.getElementById('postcode');
    error = isPostcodeFormat(postcode,'postcode_msg');
    if(error == false){
        return false;
    }
    
    //CPSP合同开始时间小于CPSP合同结束时间 
    var startDate = document.getElementById('startDate');
    var endDate = document.getElementById('endDate');
    error = beginTimeLEndTime(startDate,endDate,'endDate_msg');
    if(error == false){
        return false;    
    }
    var endTime = GetDataFromCtrl('calendar2');
    error = isGnowTime(endDate,endTime,'endDate_msg');
    if(error == false){
        return false;    
    }
    
    //CPSP订购退订url
 /*   var cpsubscribeUrl = document.getElementById('cpsubscribeUrl');
    error = isURLFormat(cpsubscribeUrl,'cpsubscribeUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpsubscribeUrl,'cpsubscribeUrl_msg','255');
    if(error == false){
        return false;
    } 
    
    //CPSP退订url
    var cpunsubscriberUrl = document.getElementById('cpunsubscriberUrl');
    error = isURLFormat(cpunsubscriberUrl,'cpunsubscriberUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpunsubscriberUrl,'cpunsubscriberUrl_msg','255');
    if(error == false){
        return false;
    } 
    
    //CPSP执行url
    var cpexcuteUrl = document.getElementById('cpexcuteUrl');
    error = isURLFormat(cpexcuteUrl,'cpexcuteUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpexcuteUrl,'cpexcuteUrl_msg','255');
    if(error == false){
        return false;
    } 
    
    //CPSP信息同步url
    var webServiceUrl = document.getElementById('webServiceUrl');
    error = isURLFormat(webServiceUrl,'webServiceUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(webServiceUrl,'webServiceUrl_msg','255');
    if(error == false){
        return false;
    } 
    
    //sp主机IP地址
    var cphostipaddr = document.getElementById('cphostipaddr');
    error = isIpFormat(cphostipaddr,'cphostipaddr_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cphostipaddr,'cphostipaddr_msg','255');
    if(error == false){
        return false;
    }
    //sp的数字网址
    var cpdigitalUrl = document.getElementById('cpdigitalUrl');
    error = isURLFormat(cpdigitalUrl,'cpdigitalUrl_msg');
    if(error == false){
        return false;
    }
    error = IsMaxLength(cpdigitalUrl,'cpdigitalUrl_msg','255');
    if(error == false){
        return false;
    } 
    
    //sp主机所在地址
    var cpposition = document.getElementById('cpposition');
    error = IsMaxLength(cpposition,'cpposition_msg','255');
    if(error == false){
        return false;
    }
    
    //sp设备名称
    var cpdeviceName = document.getElementById('cpdeviceName');
    error = IsMaxLength(cpdeviceName,'cpdeviceName_msg','255');
    if(error == false){
        return false;
    } 
    //业务引擎鉴权帐户
    var authaccount = document.getElementById('authaccount');
    error = IsMaxLength(authaccount,'authaccount_msg','60');
    if(error == false){
        return false;
    }
    //业务引擎鉴权密码
    var authpassword = document.getElementById('authpassword');
    error = IsMaxLength(authpassword,'authpassword_msg','60');
    if(error == false){
        return false;
    } 
    //sag编号
    var sagid = document.getElementById('sagid');
    error = IsMaxLength(sagid,'sagid_msg','20');
    if(error == false){
        return false;
    } 
    //对sp的鉴权密码
    var cppassword = document.getElementById('cppassword');
    error = IsMaxLength(cppassword,'cppassword_msg','40');
    if(error == false){
        return false;
    }
    //回调用户名
    var cprevid = document.getElementById('cprevid');
    error = IsMaxLength(cprevid,'cprevid_msg','40');
    if(error == false){
        return false;
    }    
    //回调密码
    var cprevpassword = document.getElementById('cprevpassword');
    error = IsMaxLength(cprevpassword,'cprevpassword_msg','40');
    if(error == false){
        return false;
    } 
    //SMS回调地址
    var cpsmsurl = document.getElementById('cpsmsurl');
    error = IsMaxLength(cpsmsurl,'cpsmsurl_msg','255');
    if(error == false){
        return false;
    } 
    //USSD回调地址
    var cpussdurl = document.getElementById('cpussdurl');
    error = IsMaxLength(cpussdurl,'cpussdurl_msg','255');
    if(error == false){
        return false;
    } */                                
}

//验证必填字段是否已填
function inputIsNull(input,input_msg){

    if(trim(input.value) == '' || input.value == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.1"); //"此项不可为空";
        document.getElementById(input.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";
        return true;
    }
}

//验证是否为电话号码，长度是否大于6位
function isPhoneFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    /*if(trim(input.value).length < 6){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.2"); //"长度必须6位以上";
        document.getElementById(input.id).focus();
        return false;
    }*/
    if(isphone(input.value.trim()) == false){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.3"); //"只可包含-,()和数字";
        document.getElementById(input.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true; 
    }  
  }
}

//验证是否为邮政编码
function isPostcodeFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    if(isZipCode(input.value.trim()) == false){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.4"); //"只能是长度为6位的数字";
        document.getElementById(input.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";  
        return true;
    }  
  }
  
}

//验证是否为手机号码
function isMobileFormat(input,input_msg){
 if(trim(input.value) != '' && input.value != null){
    if(isMobile(input.value.trim()) == false){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.5"); //"格式不正确";
        document.getElementById(input.id).focus();
        return false;    
    }else{
        document.getElementById(input_msg).innerText = "";  
        return true;
    }
  }
   
}

//验证是否为ip地址
function isIpFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    var aa = input.value.split(";");
    for(var i=0; i<aa.length; i++){
     var exp=/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
     var reg = aa[i].match(exp);
     if(reg == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.6"); //"IP地址格式不正确";
        document.getElementById(input.id).focus();
        return false;         
     }else{
        document.getElementById(input_msg).innerText = ""; 
    }   
    }
   return true; 
  }
}

//验证值应为正整数，且长度不能大于10
function isPosiint(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
      if(isExsitChineseChar(input.value)){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.7"); //"值可能存在全角输入数字，请将输入法调整为半角再重新输入数字";
        document.getElementById(input.id).focus();
        return false;     
    }
    if(isPositiveInteger(input.value.trim()) == false){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.8"); //"值应为正整数";
        document.getElementById(input.id).focus();
        return false;     
    }
    if(input.value.length > 10){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.9"); //"值长度超过限制长度,最多只能为10位";
        document.getElementById(input.id).focus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = "";  
        return true;    
    }
  }
}

//验证是否为email格式
function ismailFormat(input,input_msg) 
{ 
  if(trim(input.value) != '' && input.value != null){
    //email格式
    var exp = new RegExp("[a-z0-9-_]+@[a-z0-9-_]+[\.]{1}(com|net|bta|cn|org|edu|mil)","gi"); 
    var reg = input.value.match(exp);
    if(reg == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.10"); //"email格式不正确";
        document.getElementById(input.id).focus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = ""; 
        return true;     
    }  
  }
} 

//验证是否为URL格式
function isURLFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    var strRegex = /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/; 
    var exp=new RegExp(strRegex);
    var inputvalue = input.value.toLowerCase().trim();
    var reg = inputvalue.match(exp);
    if(reg == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.11"); //"URL格式不正确";
        document.getElementById(input.id).focus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;   
    }      
  }
}
//验证开始时间是否小于结束时间
function beginTimeLEndTime(beginTime,endTime,input_msg){
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.12"); //"开始时间必须小于等于结束时间";
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;               
    }
  }
}

//验证时间是否大于当前时间
function isGnowTime(endDate,time,input_msg){
    if(time != '' && time != null && time <= new Date().getTime()){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.13"); //"所填时间必须大于当前时间";
        document.getElementById(endDate.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;               
    }   
}

//验证输入框中字数是否超过最大长度
function IsMaxLength(input,input_msg,len){
	
  if(utf8_strlen2(trim(input.value)) > len){
      document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.14")+len+$res_entry("msg.cpspmanager.check.20");//"超过最大长度限制:";
      document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}

//得到字符串的长度（汉字按3个字符计）
/*    function strlength(str){
        var l=str.length;
        var n=l;
        for(var i=0;i<l;i++){
    		    if(str.charCodeAt(i)<0 || str.charCodeAt(i)>255){
    		    	n+=2;
    		    }
    	  }
        return n;
    }
    
//验证是否为中文
function IsChinese(input,input_msg){
    if(trim(input.value).length*3 != strlength(trim(input.value))){
      document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.15");//"只能输入中文汉字";
      document.getElementById(input.id).focus();    
      return false;
    }else{
       document.getElementById(input_msg).innerText = "";   
       return true;     
    }
}

//验证是否为非中文
function IsEnglish(input,input_msg){
	var result = isExsitChineseChar(input.value);
    if(result == true){
      document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.16"); //"不能输入中文汉字,可能存在中文输入格式";
      document.getElementById(input.id).focus();    
      return false;	
	}else{
       document.getElementById(input_msg).innerText = "";   
       return true;     
    }
}


 //展开折叠为输入框的sag信息
function closeSag(){
    if(GetDataFromCtrl('needsag') == 1){
	    var sagTable = document.getElementById('sagtable');
	    if(sagTable.style.display == "none"){
	        sagTable.style.display = "block";
	    }else{
	        sagTable.style.display = "none";
	    }
	}
}

//展开折叠为label的sag信息
function closeSagLabel(){
    if(GetDataFromCtrl('needsag') == '是'){
	    var sagTable = document.getElementById('sagtable');
	    if(sagTable.style.display == "none"){
	        sagTable.style.display = "block";
	    }else{
	        sagTable.style.display = "none";
	    }
	}
}
*/
//判断cp代码是否为数字或字符
function checkNumOrChar(input,input_msg){

    var pattern= /[^a-zA-Z0-9]/; 
    var tem = input.value.trim();
    var reg = pattern.test(tem); 
    if(reg){
      document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.17"); //"只能输入数字或字符";
      document.getElementById(input.id).focus(); 
      return false;

    }else{
       document.getElementById(input_msg).innerText = "";   
       return true;     
    }

}

function checkNum(input,input_msg){

    var regexp= /^\d+$/; 
    var tem = input.value;
    var reg = tem.match(regexp); 
    if(reg==null){
      document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.4"); //"只能输入数字";
      document.getElementById(input.id).focus(); 
      return false;

    }else{
       document.getElementById(input_msg).innerText = "";   
       return true;     
    }

}


	function getTime(str)
	{
		if (str.length > 8)
		{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
		}else{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
		}
	}
	
	//改变下拉框的长度
	/*	function chgWidth(obj) {
		var v=obj.options[obj.selectedIndex].text;
		var width=7*getLens(v);
		width = (width<130 ? 130 : width);
		obj.style.width=width;
	}

	function getLens(obj){
		return obj.replace(/[^\x00-\xff]/gi,'zch').length;
	}
	//手机号码格式
 function checkMobilePhone( s ){
    var regu =/^[1][3|5|8][0-9]{9}$/;
    var re = new RegExp(regu);
    if (re.test(s)) {
      return true;
    }else{
      return false;
    }
}*/

package com.zte.cms.service.apply;

import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.wkfw.ls.IServiceWkfwCheck;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class ServiceApply extends WorkflowBase implements IServiceApply
{

    private Log log = SSBBus.getLog(getClass());
    private IServiceDS serviceds = null;
    private IUsysConfigDS usysConfigDS = null;
    protected IServiceWkfwCheck serviceWkfwCheckLS = null;
    private String callbackProcess;
    
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }
    public IServiceDS getServiceds()
    {
        return serviceds;
    }

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }
    
    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setServiceWkfwCheckLS(IServiceWkfwCheck serviceWkfwCheckLS)
    {
        this.serviceWkfwCheckLS = serviceWkfwCheckLS;
    }
    
    // 构造方法
    ServiceApply()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
    }

    public String apply(Long serviceIdx) throws DomainServiceException
    {
        String checkResult = serviceWkfwCheckLS.checkServiceWkfw(serviceIdx);
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }

        Service service = new Service();
        service.setServiceindex(serviceIdx);
        service = serviceds.getService(service);
        String isServiceAudit = null;
        UsysConfig usysConfig = new UsysConfig();
        // 获取服务是否审核配置
        log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
        usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.service.noaudit.switch");
        log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
        if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().equals("")))
        {
            return "1:获取服务是否审核配置参数失败";
        }
        else
        {
            isServiceAudit = usysConfig.getCfgvalue();
        }
        
        // 不免审发起工作流，免审状态更新为0-审核通过
        if(isServiceAudit.equals("0"))
        {
            service.setStatus(0);
            serviceds.updateService(service);
            return "0:服务已免审";
        }
        
        return apply(service, "1", null);
    }

    public String apply(Service service, String templateRoute, String priority) throws DomainServiceException
    {

        service.setStatus(120);
        service.setWorkflow(1);
        service.setWorkflowlife(0);

        // 插入正式表，判断是否已经存在
        try
        {
            serviceds.updateService(service);
        }
        catch (DomainServiceException e)
        {
            throw new DomainServiceException(e.getMessage());
        }
        // 发起工作流参数
        String pboName = service.getServicename() + "[" + service.getServiceid() + "]";
        String pboId = String.valueOf(service.getServiceindex());
        String pboType = ServiceConstants.getWorkflowType(service.getWorkflow());
        String processName = pboType + ":" + pboName;

        String arg6 = "step1," + templateRoute;// 工作流模板路由选择

        String callbackBean = "";
        if (null != this.getCallbackProcess())
        {
            callbackBean = "callbackProcess," + this.getCallbackProcess();
            arg6 = arg6 + "|" + callbackBean;// 回调bean

        }

        WorkflowService wkflService = new WorkflowService();
        String tempcode = this.getTemplateCode();
        int flag = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType, "",
                arg6, priority);
        if (flag <= 0)
        {
            throw new DomainServiceException();
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_ADD_APPLY_SUCCESS);
    }

    public String audit(Service service, String taskId, String handleMsg, String handleEvent, String workflowStatusCode)
            throws DomainServiceException
    {

        String checkResult = serviceWkfwCheckLS.checkServiceWkfwAudit(service.getServiceindex());
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }

        // 修改正式表
        if (service != null && service.getServiceid() != null)
        {
            serviceds.updateService(service);
        }

        // 工作流处理
        WorkflowService wkflService = new WorkflowService();
        wkflService.completeTask(taskId, handleMsg, handleEvent);
      
        if (workflowStatusCode.equals("1"))
        {// 审核
            return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_ADD_AUDIT_SUCCESS);
        }
        else
        {
            return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_ADD_AUDIT_SUCCESS);
        }
    }

}
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html;charset=GBK" %>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="db" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<%
    i18n.setLocale(request);
    String operID = (String)session.getAttribute("OPERID");
    String operName = (String)session.getAttribute("OPERNAME");
    String spIndex = (String)session.getAttribute("SPINDEX");
    String spCode = (String)session.getAttribute("SPCODE");
    Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
    String serviceKey = (String)session.getAttribute("SERVICEKEY");
    db.setServiceKey(serviceKey); 
    String sysTime = "";
    Vector sysInfo = (Vector)application.getAttribute("SYSINFO");
    String strMsg = "";
       String  errmsg = "";
       boolean flag =true;
       if (operID  == null){
          errmsg = i18n.getMessage("checkRingLog.cl.loginfirst");
          flag = false;
       }
       if(flag){

        int cmdint=-1;
        String  optOperGroup = "";
        String  optServiceList = "";
        String selectedServiceKey = (String)request.getParameter("serviceKey");
        if(request.getParameter("cmd")!=null){
          cmdint = Integer.parseInt(request.getParameter("cmd"));
        }
        String  roleID = "1000";
        if(request.getParameter("roleID")!=null) roleID=request.getParameter("roleID");//.split(",")[0];
		String createid = "0";
		if(request.getParameter(roleID+"createid")!=null) createid=request.getParameter(roleID+"createid");
		String serflag = "0";
		if(request.getParameter(roleID+"serflag")!=null) serflag=request.getParameter(roleID+"serflag");
		//out.print(roleID + "," + createid + "," + serflag);
        String operate = request.getParameter("operate") == null ? "" : (String)request.getParameter("operate");        // 动作代号
        String grpScript = request.getParameter("grpScript") == null ? "" : (String)request.getParameter("grpScript");  // 要增加的操作员描述
        HashMap map = new HashMap();
        Vector vFgrp=new Vector();
        Vector lststr=new Vector();
        if(cmdint==2){
          HashMap modMap = new HashMap();
          modMap.put("SERVICEKEY",serviceKey);
          modMap.put("SESSIONOPERID",operID);
          modMap.put("SESSIONOPERNAME",operName);
          modMap.put("REQUEST",request);
          if(db.grp_rightsmod(modMap)){
            strMsg="<font color='#0000FF'>";
          }else{
            strMsg="<font color='#FF0000'>";
          }
          strMsg=strMsg+ db.getStrmsg(db.geterrorCode()) + "</font>";
        }
  %>

<html>
<head>
<title><fmt:message key="operGrpRight.allot"/></title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body topmargin="0" leftmargin="0" onload="initform(this.document.forms[0])">
<script language="JavaScript">
	if(parent.frames.length>0)
	try{
		parent.document.all.main.style.height="600";
	}catch(e){
	}
</script>
<form name="inForm" method="POST" action="roleRight.jsp" onSubmit="return submitchk()">
   <input type="hidden" value="" name="cmd">
   <input type="hidden" value="" name="rgstr">
   <input type="hidden" name="servicekey" value="<%= serviceKey  %>">
   <table border=0 height="480" width="80%" align=center>
   <tr>
   <td>
   <table border=0  width="100%" align=center  class="table-style2">
   <tr>
      <td colspan="2" height="26" align="center" class="text-title" valign="middle" background="../image/n-9.gif" ><fmt:message key="operGrpRight.rightsconfig"/>
   </td>
   <tr >
   <td class="table-style5"> <fmt:message key="operGrpRight.choosegrp"/>&nbsp;
    <select NAME="roleID" size="1" tabindex="4" onChange="getroleinf()" style="width:200px">
    <%

      if(!serviceKey.equals("")){
        lststr=db.roleScriptQry(serviceKey,operID);
        int k=db.roleScriptQryRows();
        String strtmp = "";
        for(int i=0;i<lststr.size();i=i+k){
          if(i==0)
             strtmp = lststr.get(i).toString();
          out.print("<OPTION value='"+lststr.get(i)+"'> "+lststr.get(i+1)+" </OPTION>");
        }
       if(roleID.equals("") && !strtmp.equals("")){
         roleID = strtmp;
       }
       if(lststr.size()>0)
          lststr.clear();
      }
    %>
    </select>
	
	
	<%
	if(!serviceKey.equals("")){
		lststr=db.roleScriptQry(serviceKey,operID);
		int k=db.roleScriptQryRows();
		for(int i=0;i<lststr.size();i=i+k){
			out.print("<input type='hidden' name='" + lststr.get(i) + "createid" +"' value='" + lststr.get(i+2) +"' >\n");
			out.print("<input type='hidden' name='" + lststr.get(i) + "serflag" + "' value='" + lststr.get(i+3) +"' >\n");
		}
	}
	
	%>
   </td>
   <td>
     <input type="button" value="<%= i18n.getMessage("operGrpRight.authenticate") %>" name="Add" onClick="actadd()" >
   </td>
   </tr>
   <tr>
   <td align="center" width="100%" colspan="2">
              <table align="center" class="table-style5" border="1" cellpadding="6" cellspacing="0" width="100%"  bordercolorlight="#77BEEE" bordercolordark="#E0E0E0" height="398">
                <tr>
                  <td width="50%" align="center"><font class="font"><strong><fmt:message key="operGrpRight.funcgrplist"/></strong></font>
                  </td>
                  <td width="50%" align="center"><font class="font"><strong><fmt:message key="operGrpRight.rightsconfig"/></strong></font></td>
      </tr>

      <tr>
      <td align="center" valign="top">
      <select NAME="funcgrpid"  size="22" onChange="getfuncgrpinf()" style="width: 200px">
      <%
         if(serviceKey.equals("")){
            out.println("<OPTION value=''> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </OPTION>");
         }else{
          lststr=db.oper_funcgrpqry(serviceKey,serflag);
          int k = db.oper_funcgrpqryrows();
          for(int i=0;i<lststr.size();i=i+k){
            if(i==0)
              out.print("<OPTION value='"+lststr.get(i)+"' selected > "+lststr.get(i+1)+" </OPTION>");
           else
             out.print("<OPTION value='"+lststr.get(i)+"'> "+lststr.get(i+1)+" </OPTION>");
            vFgrp.addElement(lststr.get(i).toString());
          }
          lststr.clear();
         }
      %>
      </select></td>
      <td bgcolor="#FFFFFF" valign="top">
      <%
         Vector vFunc=new Vector();
         String sTmp="";
         if(serviceKey.equals("")){
         }else{
          int ii=0;
          int ik=db.oper_functionqryrows();
          String sTmp2="";
          for(int i=0;i<vFgrp.size();i++){
            sTmp=vFgrp.get(i).toString();
            out.println("<div id='Func_"+sTmp+"' STYLE='display:none;'>");
            lststr=db.oper_functionqry(serviceKey,sTmp);
            for(ii=0;ii<lststr.size();ii=ii+ik){
              String str = lststr.get(ii).toString()+"-"+lststr.get(ii+1).toString();
              if(purviewList.get(str) == null){
                sTmp2 = "disabled";
              }else{
                sTmp2="";
              }
			
              out.print(" <input type='checkbox' name='Func_"+lststr.get(ii)+"_"+lststr.get(ii+1)+"' value='0'" + ">"+lststr.get(ii+2).toString()+"<br>");
              vFunc.addElement(lststr.get(ii).toString()+"_"+lststr.get(ii+1).toString());
            }
            lststr.clear();
            out.println("</div>");
          }
         }
      %>
      </td>
      </tr>
     </table>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
</form>

<script language="JavaScript1.2">
function getfuncgrpinf(){
  var sTmp=document.inForm.funcgrpid.value;
  document.all('Func_'+sTmp).style.display='';
}

function actadd(){
  if(!(document.inForm.roleID.value==""||document.inForm.servicekey.value=="")){
    var sTmp1="";
    document.inForm.rgstr.value=sTmp1.substring(0,sTmp1.length-1);
    document.inForm.cmd.value="2";
    document.inForm.submit();
  }
}

function initform(pform){

}

function submitchk(){
  if(document.inForm.cmd.value==""){
    return false;
  }else{
    return true;
  }
}


function actadd(){
  if(!(document.inForm.roleID.value==""||document.inForm.servicekey.value=="")){
    var sTmp1="";
<%
  for(int i=0;i<vFunc.size();i++){
    sTmp=vFunc.get(i).toString();
    out.println(" if(document.all('Func_"+sTmp+"').checked==true&&document.all('Func_"+sTmp+"').disabled==false){");
    out.println("  sTmp1=sTmp1+'"+sTmp+"|';");
    out.println(" }");
  }
%>
    document.inForm.rgstr.value=sTmp1.substring(0,sTmp1.length-1);
    document.inForm.cmd.value="2";
    document.inForm.submit();
  }
}

function getfuncgrpinf(){
<%
  for(int i=0;i<vFgrp.size();i++){
    sTmp=vFgrp.get(i).toString();
    out.println("document.all('Func_"+sTmp+"').style.display='none';");
  }
%>
  var sTmp=document.inForm.funcgrpid.value;
  document.all('Func_'+sTmp).style.display='';
}


function getroleinf(){
  if(document.inForm.roleID.value==""){
  }else{
    document.inForm.cmd.value="0";
    document.inForm.submit();
  }
}

//document.inForm.Add.disabled=true;
<%
  if(vFgrp.size()>0)out.println("document.all('Func_"+vFgrp.get(0)+"').style.display='';");
  if(!serviceKey.equals(""))out.println("document.inForm.servicekey.value='"+serviceKey+"';");
  if(!roleID.equals("")){
    lststr=db.oper_grpdef(roleID,serviceKey);
    int k=db.oper_grpdefrows();
    for(int i=0;i<lststr.size();i=i+k){
      out.println("document.all('Func_"+lststr.get(i)+"_"+lststr.get(i+1)+"').value='1';");
      out.println("document.all('Func_"+lststr.get(i)+"_"+lststr.get(i+1)+"').checked=true;");
    }
    lststr.clear();
    if(db.roleInf(serviceKey,roleID)){
      out.println("document.inForm.roleID.value="+roleID+";");
      if(db.getCreatorid()==java.lang.Integer.parseInt(operID)){
        out.println("document.inForm.Add.disabled=false;");
      }
    }

  }
 %>

</script>
</BODY>
</HTML>
<%
        }
%>
</body>
</html>

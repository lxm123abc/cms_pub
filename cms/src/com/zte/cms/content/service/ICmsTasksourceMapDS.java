package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.CmsTasksourceMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsTasksourceMapDS
{
    /**
     * 新增CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 批量更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsTasksourceMapList(List<CmsTasksourceMap> cmsTasksourceMapList) throws DomainServiceException;

    /**
     * 根据条件更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsTasksourceMapListByCond(List<CmsTasksourceMap> cmsTasksourceMapList)
            throws DomainServiceException;

    /**
     * 删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 批量删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsTasksourceMapList(List<CmsTasksourceMap> cmsTasksourceMapList) throws DomainServiceException;

    /**
     * 根据条件删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsTasksourceMapListByCond(List<CmsTasksourceMap> cmsTasksourceMapList)
            throws DomainServiceException;

    /**
     * 查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @return CmsTasksourceMap对象
     * @throws DomainServiceException ds异常
     */
    public CmsTasksourceMap getCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DomainServiceException;

    /**
     * 根据条件查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @return 满足条件的CmsTasksourceMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsTasksourceMap> getCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
package com.zte.cms.settlementDataRpt.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

import java.io.Serializable;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:08 2019/6/28
 * @Description:
 */
public class SettlementEntDownFile  extends DynamicBaseObject implements Serializable {
    @Override
    public void initRelation() {
        this.addRelation("id", "ID");
        this.addRelation("spid", "SPID");
        this.addRelation("spname", "SPNAME");
        this.addRelation("filename", "FILENAME");
    }

    private long id;
    private String spid;
    private String spname;
    private String filename;

    //vo
    private String[] oldname;
    private String[] newname;
    private String operfilename;


    public String getOperfilename() {
        return operfilename;
    }

    public void setOperfilename(String operfilename) {
        this.operfilename = operfilename;
    }

    public String[] getNewname() {
        return newname;
    }

    public void setNewname(String[] newname) {
        this.newname = newname;
    }

    public String[] getOldname() {
        return oldname;
    }

    public void setOldname(String[] oldname) {
        this.oldname = oldname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpid() {
        return spid;
    }

    public void setSpid(String spid) {
        this.spid = spid;
    }

    public String getSpname() {
        return spname;
    }

    public void setSpname(String spname) {
        this.spname = spname;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}

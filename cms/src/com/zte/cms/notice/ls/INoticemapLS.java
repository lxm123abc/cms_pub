package com.zte.cms.notice.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.notice.model.Noticemap;
import com.zte.purview.access.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface INoticemapLS
{

    TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                       throws DomainServiceException;

    TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                             throws DomainServiceException;

    TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                           throws DomainServiceException;

    TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 获取cms_message 信息
     * @param messageid
     * @return
     */
    Noticemap getNoticemap(Integer messageid);
    
    Noticemap getNoticeLettermap(Integer messageid);

    /**
     * 增加公告消息  cms_message 
     * @param noticemap
     * @return
     * @throws DomainServiceException
     */
    String insertNoticemap(Noticemap noticemap) throws DomainServiceException;
    
    /**
     * 转发消息 
     * @param noticemap
     * @return
     * @throws DomainServiceException
     */
    String forwardNoticemap(Noticemap noticemap) throws DomainServiceException;

    /**
     * 增加消息与CP对应关系：CMS_MESSAGE_CP 
     * @param noticemap
     * @return
     * @throws DomainServiceException
     */
    String insertCmsMessageCp(Integer messageid, String cpid) throws DomainServiceException;

    String deleteNoticemap(Noticemap noticemap) throws DomainServiceException;
    
    public String revokeNoticemap(Noticemap noticemap) throws DomainServiceException;

    /**
     * 修改公告消息
     * 
     * @param service
     * @return
     * @throws Exception
     */
    public String updateNoticemap(Noticemap service) throws DomainServiceException;

    public String setStatusRead(Noticemap service) throws DomainServiceException;
    
    public String messageDel(Noticemap service) throws DomainServiceException;
    
    TableDataInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    Noticemap getNotificationDetail(Noticemap noticemap);
    
    public List<Noticemap> getNotificationDetails(Noticemap noticemap);

    void updateView2(Noticemap noticemap) throws DomainServiceException;

    String updateView(Noticemap noticemap) throws DomainServiceException;

    String updateReplay(Noticemap noticemap) throws DomainServiceException;

    String insertReplay(Noticemap noticemap) throws DomainServiceException;

    int unViewNumQuery(Noticemap noticemap) throws DomainServiceException;
    
    boolean getUserOperRights() throws DomainServiceException;

    public TableDataInfo operInfoQuery(OperInformation operInfo,int start, int pageSize) throws DomainServiceException;

    public TableDataInfo getOrderedChildOperatorsDataInfo(long paramLong, String paramString, int paramInt1, int paramInt2);

    public boolean changeOperId(Map<String, String> params);
    
    public boolean changeOperIdB(Map<String, String> params);
}
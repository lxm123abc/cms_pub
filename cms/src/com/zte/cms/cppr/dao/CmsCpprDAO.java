package com.zte.cms.cppr.dao;

import java.util.List;

import com.zte.cms.cppr.model.CmsCppr;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsCpprDAO extends DynamicObjectBaseDao implements ICmsCpprDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @Override
    public List<CmsCppr> queryCmsCpprListByCond(CmsCppr cmsCppr) throws DAOException
    {
        log.debug("query cmsCpprList by cond starting...");
        List<CmsCppr> list =(List<CmsCppr>) super.queryForList("queryCmsCpprListByCond", cmsCppr);
        log.debug("query cmsCpprList by cond end");
        return list;
    }

    @Override
    public void insertCmsCppr(CmsCppr cmsCppr) throws DAOException
    {
        log.debug("insert cmsCppr starting...");
        super.insert("insertCmsCppr", cmsCppr);
        super.insert("insertCmsCpprToService", cmsCppr);
        log.debug("insert cmsCppr end");
    }

    @Override
    public void deleteCmsCppr(CmsCppr cmsCppr) throws DAOException
    {
        log.debug("deletec msCppr starting...");
        super.delete("deleteCmsCppr", cmsCppr);
        super.delete("deleteCmsCpprFromService", cmsCppr);
        log.debug("deletec msCppr end");
    }

}
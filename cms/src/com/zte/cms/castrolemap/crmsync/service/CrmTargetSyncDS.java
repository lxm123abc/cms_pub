package com.zte.cms.castrolemap.crmsync.service;

import java.util.List;

import com.zte.cms.castrolemap.crmsync.dao.ICrmTargetSyncDAO;
import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CrmTargetSyncDS extends DynamicObjectBaseDS implements ICrmTargetSyncDS
{
private Log log = SSBBus.getLog(getClass());
    
    private ICrmTargetSyncDAO dao = null;
    
    public void setDao(ICrmTargetSyncDAO dao)
    {
        this.dao = dao;
    }
    
    public TableDataInfo pageInfoQuery(CrmTargetSync crmTargetSync, int start, int pageSize)throws DomainServiceException
    {
        log.debug( "get crmTargetSync page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(crmTargetSync, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get crmTargetSync page info by condition end" );
        return tableInfo;
    }
}

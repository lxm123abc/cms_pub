package com.zte.cms.service.ls;

import com.zte.cms.service.model.Service;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceContentBindLS
{
    /**
     * 查询和服务绑定的内容信息
     * 
     * @param 利用service类将查询条件传入后台
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize) throws Exception;

    /**
     * 查询和服务绑定的内容信息
     * 
     * @param service 利用service类将查询条件传入后台
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * 查询没有绑定的但是可以绑定的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize,
            PageUtilEntity puEntity) throws Exception;

    /**
     * 查询没有绑定的但是可以绑定的内容
     * 
     * @param 利用service类将查询条件传入后台
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize) throws Exception;

    /**
     * 批量绑定内容
     * 
     * @param cntIdxList ：选中内容的index数组
     * @param serviceIdx ：要绑定的服务的index
     * @param cntBindType ：绑定的内容类型 根据不同的类型，插入不同的map表
     * @return
     * @throws Exception
     */
    public String insertServiceBindMap(String cntIdxList[], Long serviceIdx, int cntBindType) throws Exception;

    /**
     * @mapIndexAll 确认删除map关系的所有mapindex拼接成的字符串
     */
    public String removeServiceBindMapList(String mapIndexAll, int bindCntType) throws Exception;

    
    public String removeServiceBindMap(String mapIndex, int bindCntType) throws Exception;
    
    /**
     * 发布服务和内容的绑定关系
     * 
     * @param servCntBindMapIndexAll 选中的想要发布的所有的绑定关系
     * @param bindCntType 绑定的内容类型
     * @return
     * @throws Exception
     */
    public String publishServiceBindMap(String servCntBindMapIndexAll, int bindCntType) throws Exception;

    /**
     * 取消绑定关系发布
     * 
     * @param servCntBindMapIndexAll
     * @param bindCntType
     * @return
     * @throws Exception
     */
    public String cancelPublishServiceBindMap(String servCntBindMapIndexAll, int bindCntType) throws Exception;
    
    /*******************************下面为服务和栏目的关联关系方法**************************************************/
    /*********************************************************************************************************/
    /**
     * 查询和服务关联的栏目信息
     * 
     * @param service Service利用service类将查询条件传入后台
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize) throws Exception;

    /**
     * 查询和服务绑定的内容信息
     * 
     * @param service Service利用service类将查询条件传入后台
     * @param start int
     * @param pageSize int
     * @param puEntity  PageUtilEntity
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception;
    /**
     * 新增服务和栏目的绑定关系
     * @param serviceIdx String
     * @param categoryIdx String
     * @return String
     * @throws Exception Exception
     */
    public String addServiceCategoryMap(String serviceIdx, String categoryIdx)throws Exception;
    
    /**
     * 批量删除服务和栏目的关联关系
     * @param mapindexAll String 想要删除的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String delServiceCategoryMap(String mapIndexAll)throws Exception;
    
    /**
     * 批量发布服务和栏目的关联关系
     * @param mapIndexAll String 想要发布的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String pubServiceCategoryMap(String mapIndexAll)throws Exception;
    
    /**
     * 批量取消发布服务和栏目的关联关系
     * @param mapIndexAll String 想要发布的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String delPubServiceCategoryMap(String mapIndexAll)throws Exception;
    
    /**
     * 通过给定的内容，查询出改内容绑定的服务
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo serviceContentMapQuery(Service service, int start, int pageSize) throws Exception;

    /**
     * 
     * @param service Service
     * @param start int 
     * @param pageSize int 
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo serviceContentMapQuery(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception;
    
    
}

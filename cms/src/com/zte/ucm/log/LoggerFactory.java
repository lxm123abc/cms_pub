package com.zte.ucm.log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class LoggerFactory
{
  private static HashMap loggerMap = new HashMap();

  public static Logger getLogger(Class paramClass)
  {
    return getLogger("All", paramClass.getName());
  }

  public static Logger getLogger(String paramString1, String paramString2)
  {
    LoggerManager.setModelLevel(paramString1, 2);
    LoggerManager.addModelLevel(paramString1, 8);
    LoggerManager.addModelLevel(paramString1, 1);
    LoggerManager.addModelLevel(paramString1, 4);
    synchronized (loggerMap)
    {
      LoggerImpl localLoggerImpl = (LoggerImpl)loggerMap.get(paramString1);
      if (localLoggerImpl == null)
      {
        localLoggerImpl = new LoggerImpl(paramString1, paramString2);
        localLoggerImpl.setLoggerLevel(LoggerManager.getModelLevel(paramString1));
        localLoggerImpl.setLoggerOutter(LoggerManager.getModelLoggerOutter(paramString1));

        loggerMap.put(paramString1, localLoggerImpl);
      }
      localLoggerImpl.setClassName(paramString2);
      return localLoggerImpl;
    }
  }

  static void validate()
  {
    synchronized (loggerMap)
    {
      Set localSet = loggerMap.entrySet();
      Iterator localIterator = localSet.iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        String str = (String)localEntry.getKey();
        LoggerImpl localLoggerImpl = (LoggerImpl)localEntry.getValue();
        localLoggerImpl.setLoggerLevel(LoggerManager.getModelLevel(str));
        localLoggerImpl.setLoggerOutter(LoggerManager.getModelLoggerOutter(str));
      }
    }
  }
}
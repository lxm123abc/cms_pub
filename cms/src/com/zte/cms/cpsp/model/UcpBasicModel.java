package com.zte.cms.cpsp.model;

import com.zte.umap.cpsp.common.model.UcpBasic;

/**
 * @Author: Liangxiaomin
 * @String Created in 10:20 2019/6/5
 * @Description:
 */
public class UcpBasicModel extends UcpBasic {

    private String blstartdt;
    private String blenddt;
    private String opstartdt;
    private String openddt;
    private String poistartdt;
    private String poienddt;
    private String oqstartdt;
    private String oqenddt;
    private String qualificationsurl;
    private String[] qualificationsurlArr;
    private String[] qualificationsurlNewNameArr;

    public String[] getQualificationsurlNewNameArr() {
        return qualificationsurlNewNameArr;
    }

    public void setQualificationsurlNewNameArr(String[] qualificationsurlNewNameArr) {
        this.qualificationsurlNewNameArr = qualificationsurlNewNameArr;
    }

    public String[] getQualificationsurlArr() {
        return qualificationsurlArr;
    }

    public void setQualificationsurlArr(String[] qualificationsurlArr) {
        this.qualificationsurlArr = qualificationsurlArr;
    }

    public void initRelation() {
        this.addRelation("blstartdt", "blstartdt");
        this.addRelation("blenddt", "blenddt");
        this.addRelation("opstartdt", "opstartdt");
        this.addRelation("openddt", "openddt");
        this.addRelation("poistartdt", "poistartdt");
        this.addRelation("poienddt", "poienddt");
        this.addRelation("oqstartdt", "oqstartdt");
        this.addRelation("oqenddt", "oqenddt");
        this.addRelation("qualificationsurl", "qualificationsurl");
    }
    public String getBlstartdt() {
        return blstartdt;
    }

    public void setBlstartdt(String blstartdt) {
        this.blstartdt = blstartdt;
    }

    public String getBlenddt() {
        return blenddt;
    }

    public void setBlenddt(String blenddt) {
        this.blenddt = blenddt;
    }

    public String getOpstartdt() {
        return opstartdt;
    }

    public void setOpstartdt(String opstartdt) {
        this.opstartdt = opstartdt;
    }

    public String getOpenddt() {
        return openddt;
    }

    public void setOpenddt(String openddt) {
        this.openddt = openddt;
    }

    public String getPoistartdt() {
        return poistartdt;
    }

    public void setPoistartdt(String poistartdt) {
        this.poistartdt = poistartdt;
    }

    public String getPoienddt() {
        return poienddt;
    }

    public void setPoienddt(String poienddt) {
        this.poienddt = poienddt;
    }

    public String getOqstartdt() {
        return oqstartdt;
    }

    public void setOqstartdt(String oqstartdt) {
        this.oqstartdt = oqstartdt;
    }

    public String getOqenddt() {
        return oqenddt;
    }

    public void setOqenddt(String oqenddt) {
        this.oqenddt = oqenddt;
    }

    public String getQualificationsurl() {
        return qualificationsurl;
    }

    public void setQualificationsurl(String qualificationsurl) {
        this.qualificationsurl = qualificationsurl;
    }
}

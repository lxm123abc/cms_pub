package com.zte.cms.content.objectSynRecord.ls;

import java.util.List;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IObjectSyncRecordLS
{
    /**
	 * 新增ObjectSyncRecord对象
	 *
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException;
	  
    /**
	 * 更新ObjectSyncRecord对象
	 *
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException;

    /**
	 * 批量更新ObjectSyncRecord对象
	 *
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateObjectSyncRecordList( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException;



	/**
	 * 删除ObjectSyncRecord对象
	 *
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException;

	/**
	 * 批量删除ObjectSyncRecord对象
	 *
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeObjectSyncRecordList( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException;




	/**
	 * 查询ObjectSyncRecord对象
	 
	 * @param objectSyncRecord ObjectSyncRecord对象
	 * @return ObjectSyncRecord对象
	 * @throws DomainServiceException ds异常 
	 */
	 public ObjectSyncRecord getObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询ObjectSyncRecord对象 
	  * 
	  * @param objectSyncRecord ObjectSyncRecord对象
	  * @return 满足条件的ObjectSyncRecord对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<ObjectSyncRecord> getObjectSyncRecordByCond( ObjectSyncRecord objectSyncRecord )throws DomainServiceException;

	 /**
	  * 根据条件分页查询ObjectSyncRecord对象 
	  *
	  * @param objectSyncRecord ObjectSyncRecord对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询ObjectSyncRecord对象 
	  *
	  * @param objectSyncRecord ObjectSyncRecord对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     
     /**
      * 根据taskindex精确分页查询ObjectSyncRecord对象 
      *
      * @param objectSyncRecord ObjectSyncRecord对象，作为查询条件的参数 
      * @param start 起始行
      * @param pageSize 页面大小
      * @param puEntity 排序空置参数@see PageUtilEntity
      * @return  查询结果
      * @throws DomainServiceException ds异常
      */
     public TableDataInfo getobjectSyncRecord(ObjectSyncRecord objectSyncRecord, int start, int pageSize)throws DomainServiceException;
}

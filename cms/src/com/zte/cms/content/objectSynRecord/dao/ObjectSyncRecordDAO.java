
  package com.zte.cms.content.objectSynRecord.dao;

import java.util.List;

import com.zte.cms.content.objectSynRecord.dao.IObjectSyncRecordDAO;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ObjectSyncRecordDAO extends DynamicObjectBaseDao implements IObjectSyncRecordDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DAOException
    {
    	log.debug("insert objectSyncRecord starting...");
    	super.insert( "insertObjectSyncRecord", objectSyncRecord );
    	log.debug("insert objectSyncRecord end");
    }
     
    public void updateObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DAOException
    {
    	log.debug("update objectSyncRecord by pk starting...");
       	super.update( "updateObjectSyncRecord", objectSyncRecord );
       	log.debug("update objectSyncRecord by pk end");
    }


    public void deleteObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DAOException
    {
    	log.debug("delete objectSyncRecord by pk starting...");
       	super.delete( "deleteObjectSyncRecord", objectSyncRecord );
       	log.debug("delete objectSyncRecord by pk end");
    }


    public ObjectSyncRecord getObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DAOException
    {
    	log.debug("query objectSyncRecord starting...");
       	ObjectSyncRecord resultObj = (ObjectSyncRecord)super.queryForObject( "getObjectSyncRecord",objectSyncRecord);
       	log.debug("query objectSyncRecord end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<ObjectSyncRecord> getObjectSyncRecordByCond( ObjectSyncRecord objectSyncRecord )throws DAOException
    {
    	log.debug("query objectSyncRecord by condition starting...");
    	List<ObjectSyncRecord> rList = (List<ObjectSyncRecord>)super.queryForList( "queryObjectSyncRecordListByCond",objectSyncRecord);
    	log.debug("query objectSyncRecord by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize)throws DAOException
    {
    	log.debug("page query objectSyncRecord by condition starting...");
     	PageInfo pageInfo = null;
    	    
     	   int totalCnt = ((Integer) super.queryForObject("queryObjectSyncRecordListCntByCond",  objectSyncRecord)).intValue();
           if( totalCnt > 0 )
           {
               int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
               List<ObjectSyncRecord> rsList = (List<ObjectSyncRecord>)super.pageQuery( "queryObjectSyncRecordListByCond" ,  objectSyncRecord , start , fetchSize );
               pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
           }
           else
           {
               pageInfo = new PageInfo();
           }
            log.debug("page query objectSyncRecord by condition end");
            return pageInfo;
     	
    }

	public PageInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start,
			int pageSize, PageUtilEntity puEntity) throws DAOException
	{
	       return super.indexPageQuery( "queryObjectSyncRecordListByCond", "queryObjectSyncRecordListCntByCond", objectSyncRecord, start, pageSize, puEntity);

	}

    public List<ObjectSyncRecord> getObjectSyncRecordByTaskindex(ObjectSyncRecord objectSyncRecord) throws DAOException
    {
        log.debug("query objectSyncRecord by taskindex starting...");
        List<ObjectSyncRecord> rList = (List<ObjectSyncRecord>)super.queryForList( "queryObjectSyncRecordListByTaskindex",objectSyncRecord);
        log.debug("query objectSyncRecord by taskindex end");
        return rList;
    }

    public PageInfo getobjectSyncRecord(ObjectSyncRecord objectSyncRecord, int start, int pageSize) throws DAOException
    {
        log.debug("page query objectSyncRecord by condition starting...");
        PageInfo pageInfo = null;
            
           int totalCnt = ((Integer) super.queryForObject("getObjectSyncRecordListCntByTaskindex",  objectSyncRecord)).intValue();
           if( totalCnt > 0 )
           {
               int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
               List<ObjectSyncRecord> rsList = (List<ObjectSyncRecord>)super.pageQuery( "getObjectSyncRecordListByTaskindex" ,  objectSyncRecord , start , fetchSize );
               pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
           }
           else
           {
               pageInfo = new PageInfo();
           }
            log.debug("page query objectSyncRecord by condition end");
            return pageInfo;
    }

    
} 
package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsChannelDAO
{
    /**
     * 新增CmsChannel对象
     * 
     * @param cmsChannel CmsChannel对象
     * @throws DAOException dao异常
     */
    public void insertCmsChannel(CmsChannel cmsChannel) throws DAOException;

    /**
     * 根据主键更新CmsChannel对象
     * 
     * @param cmsChannel CmsChannel对象
     * @throws DAOException dao异常
     */
    public void updateCmsChannel(CmsChannel cmsChannel) throws DAOException;

    /**
     * 根据主键删除CmsChannel对象
     * 
     * @param cmsChannel CmsChannel对象
     * @throws DAOException dao异常
     */
    public void deleteCmsChannel(CmsChannel cmsChannel) throws DAOException;

    /**
     * 根据主键查询CmsChannel对象
     * 
     * @param cmsChannel CmsChannel对象
     * @return 满足条件的CmsChannel对象
     * @throws DAOException dao异常
     */
    public CmsChannel getCmsChannel(CmsChannel cmsChannel) throws DAOException;
    
    public CmsChannel getCmsChannelById(CmsChannel cmsChannel) throws DAOException;


    /**
     * 根据条件分页查询CmsChannel对象，作为查询条件的参数
     * 
     * @param cmsChannel CmsChannel对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsChannel cmsChannel, int start, int pageSize) throws DAOException;

    public List getChannelByCond(CmsChannel channel);

}
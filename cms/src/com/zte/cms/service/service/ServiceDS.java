package com.zte.cms.service.service;

import java.util.List;
import com.zte.cms.common.Generator;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.dao.IServiceDAO;
import com.zte.cms.service.service.IServiceDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceDS extends DynamicObjectBaseDS implements IServiceDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceDAO dao = null;

    public void setDao(IServiceDAO dao)
    {
        this.dao = dao;
    }

    public void insertService(Service service) throws DomainServiceException
    {
        log.debug("insert service starting...");
        try
        {
            Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("ucdn_service_index");
            service.setServiceindex(index);
            String contentid = Generator.getContentId(0L, "PACK");
            service.setServiceid(contentid);
            //String servicecode = Generator.getWGCodeByContentId(contentid);
            service.setServicecode(contentid);
            dao.insertService(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert service end");
    }

    public void updateService(Service service) throws DomainServiceException
    {
        log.debug("update service by pk starting...");
        try
        {
            dao.updateService(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update service by pk end");
    }

    public void updateServiceList(List<Service> serviceList) throws DomainServiceException
    {
        log.debug("update serviceList by pk starting...");
        if (null == serviceList || serviceList.size() == 0)
        {
            log.debug("there is no datas in serviceList");
            return;
        }
        try
        {
            for (Service service : serviceList)
            {
                dao.updateService(service);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceList by pk end");
    }

    public void removeService(Service service) throws DomainServiceException
    {
        log.debug("remove service by pk starting...");
        try
        {
            dao.deleteService(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove service by pk end");
    }

    public void removeServiceList(List<Service> serviceList) throws DomainServiceException
    {
        log.debug("remove serviceList by pk starting...");
        if (null == serviceList || serviceList.size() == 0)
        {
            log.debug("there is no datas in serviceList");
            return;
        }
        try
        {
            for (Service service : serviceList)
            {
                dao.deleteService(service);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceList by pk end");
    }

    public Service getService(Service service) throws DomainServiceException
    {
        log.debug("get service by pk starting...");
        Service rsObj = null;
        try
        {
            rsObj = dao.getService(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceList by pk end");
        return rsObj;
    }

    public List<Service> getServiceByCond(Service service) throws DomainServiceException
    {
        log.debug("get service by condition starting...");
        List<Service> rsList = null;
        try
        {
            rsList = dao.getServiceByCond(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get service by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(service, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Service>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(service, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Service>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryPublish(service, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Service>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 取消发布
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryDelPublish(service, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Service>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    public int countCntContentPublish(CntTargetSync cntTargetSync)throws DomainServiceException
    {
        // TODO Auto-generated method stub
        log.debug( "get publishing  content number info by condition starting..." );
        int i = 0;
        try
        {
            i=dao.countCntContentPublish(cntTargetSync);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get publishing  content number info by condition end" );
        return i;
    }

    public Service getServiceById(Service service) throws DomainServiceException
    {
        log.debug("get getServiceById ds starting...");
        Service rsObj = null;
        try
        {
            rsObj = dao.getServiceById(service);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getServiceById ds end");
        return rsObj;
    }
}

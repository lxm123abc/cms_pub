package com.zte.cms.common;

public class ObjectType
{
    public static final String PROG = "PROG";
    public static final String MOVI = "MOVI";
    public static final String CAST = "CAST";
    public static final String CARM = "CARM";
    public static final String CHAN = "CHAN";
    public static final String PHCH = "PHCH";
    public static final String SCHE = "SCHE";
    public static final String PICT = "PICT";
    public static final String CATE = "CATE";
    public static final String SERI = "SERI";
    public static final String PACK = "PACK";
    public static final String VACT = "VACT";
    
    //对象编码
    public static final int SERVICE_TYPE = 1;
    public static final int CATEGORY_TYPE = 2;
    public static final int PROGRAM_TYPE = 3;
    public static final int SERIES_TYPE = 4;
    public static final int CHANNEL_TYPE = 5;
    public static final int SCHEDULE_TYPE = 6;
    public static final int MOVIE_TYPE = 7;
    public static final int PHYSICALCHANNEL_TYPE = 8;
    public static final int SCHEDULERECORD_TYPE = 9;
    public static final int PICTURE_TYPE = 10;
    public static final int CAST_TYPE = 11;
    public static final int CASTROLEMAP_TYPE = 12;

    public static final int SERVICEPROGRAMMAP_TYPE = 21;
    public static final int SERVICESERIESMAP_TYPE = 22;
    public static final int SERVICECHANNELMAP_TYPE = 23;
    public static final int SERVICESCHEDULEMAP_TYPE = 24;
    public static final int SERVICEPICTUREMAP_TYPE = 25;
    public static final int CATEGORYPROGRAMMAP_TYPE = 26;
    public static final int CATEGORYSERIESMAP_TYPE = 27;
    public static final int CATEGORYCHANNELMAP_TYPE = 28;
    public static final int CATEGORYSCHEDULEMAP_TYPE = 29;
    public static final int CATEGORYSERVICEMAP_TYPE = 30;
    public static final int CATEGORYPICTUREMAP_TYPE = 31;
    public static final int PROGRAMMOVIEMAP_TYPE = 32;
    public static final int PROGRAMSCHEDULERECORDMAP_TYPE = 33;
    public static final int PROGRAMCASTROLEMAPMAP_TYPE = 34;
    public static final int PROGRAMPICTURE_TYPE = 35;
    public static final int SERIESPROGRAMMAP_TYPE = 36;
    public static final int SERIESCASTROLEMAPMAP_TYPE = 37;
    public static final int SERIESPICTUREMAP_TYPE = 38;
    public static final int CHANNELPICTUREMAP_TYPE = 39;
    public static final int SCHEDULESCHEDULERECORDMAP_TYPE = 40;
    public static final int CASTPICTURE_TYPE = 41;
    public static final String OPER_SUPER_ID = "1";

}

package com.zte.cms.content.series.common;

public interface SeriesConfig
{
    public static final String CMS_LOG_RESOURCE_FILE = "cms_log_resource";//公共日志资源文件名称
    public static final String SERIES_CONTENT_TYPE = "SERI";
    public static final String SERIES_CONTENT_TYPE_CODE = "00000010";

    public static final String SERIES_ADD = "series.add";
    public static final String SERIES_MODIFY = "series.modify";
    public static final String SERIES_DELETE = "series.delete";
    public static final String SERIES_PUBLISH = "series.publish";
    public static final String SERIES_CANCEL_PUBLISH = "series.cancelpublish";
    public static final String SERIES_PREOFFLINE = "series.preoffline";

    public static final String SERIES_ADD_INFO = "series.add.info";
    public static final String SERIES_MODIFY_INFO = "series.modify.info";
    public static final String SERIES_DELETE_INFO = "series.delete.info";
    public static final String SERIES_PUBLISH_INFO = "series.publish.info";
    public static final String SERIES_CANCEL_PUBLISH_INFO = "series.cancelpublish.info";
    public static final String SERIES_PREOFFLINE_INFO = "series.preoffline.info";
    
    public static final String SERIES_BINDTARGET = "series.bindtarget";
    public static final String SERIES_BINDTARGET_INFO = "series.bindtarget.info";    
    public static final String SERIESPIC_BINDTARGET = "seriespic.bindtarget";
    public static final String SERIESPIC_BINDTARGET_INFO = "seriespic.bindtarget.info";
    public static final String SERIESCATE_BINDTARGET = "seriescate.bindtarget";
    public static final String SERIESCATE_BINDTARGET_INFO = "seriescate.bindtarget.info";
    public static final String SERIESCRM_BINDTARGET = "seriescrm.bindtarget";
    public static final String SERIESCRM_BINDTARGET_INFO = "seriescrm.bindtarget.info";
    public static final String SERIESPROG_BINDTARGET = "seriesprog.bindtarget";
    public static final String SERIESPROG_BINDTARGET_INFO = "seriesprog.bindtarget.info";
    
    
    public static final String CMS_BINDTARGET_MSG = "cms.bindtarget.msg";
    public static final String CMS_BINDTARGET_MSG_HAS_DELETED = "cms.bindtarget.msg.has.deleted";
    public static final String CMS_BINDTARGET_MSG_HAS_BINDED = "cms.bindtarget.msg.has.binded";
    
    public static final String SERIES_DATA_NOT_EXIST = "series.data.not.exist";
    public static final String SERIES_STATUS_NON_PUBLISHED = "series.status.nonpublished";
    public static final String SERIES_STATUS_PUBLISHED = "series.status.published";
    public static final String SERIES_STATUS_PUBLISHING = "series.status.publishing";
    public static final String SERIES_DBNORMAL = "series.dbnormal";
    
    // 操作结果
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

    // 连续剧内容管理
    public static final String SERIES_MGT = "log.series.mgt";
    
    //连续剧关联关系发布
    public static final String SERIES_MAPPING_PUBLISH_MANAGE = "series.mapping.pubulish.mgt";//连续剧关联关系发布管理
    public static final String CATEGORY_SERIES_MAPPING_PUBLISH = "category.series.mapping.pubulish";//栏目关联连续剧发布
    public static final String  SERIES_CAST_MAPPING_PUBLISH = "series.cast.mapping.pubulish";//连续剧关联角色发布
    public static final String CATEGORY_SERIES_MAPPING_PUBLISH_INFO = "category.series.mapping.pubulish.info";//栏目关联连续剧发布详情
    public static final String  SERIES_CAST_MAPPING_PUBLISH_INFO = "series.cast.mapping.pubulish.info";//连续剧关联角色发布详情
    
    public static final String CATEGORY_SERIES_MAPPING_DEL_PUBLISH = "category.series.mapping.delpubulish";//栏目关联连续剧取消发布
    public static final String  SERIES_CAST_MAPPING_DEL_PUBLISH = "series.cast.mapping.delpubulish";//连续剧关联角色取消发布
    public static final String CATEGORY_SERIES_MAPPING_DEL_PUBLISH_INFO = "category.series.mapping.delpubulish.info";//栏目关联连续剧取消发布详情
    public static final String  SERIES_CAST_MAPPING_DEL_PUBLISH_INFO = "series.cast.mapping.delpubulish.info";//连续剧关联角色取消发布详情
    
    
    //连续剧关联平台
    public static final String TARGET_NOT_EXIST = "log.target.no.exist";//网元不存在
    public static final String SERIES_OPERATE_SUCCEED = "log.series.operate.succeed";//删除关联网元
    public static final String SERIES_BIND_TARGET_DEL = "log.series.bind.target.del";//删除关联网元
    public static final String SERIES_BIND_PALTFORM_DEL = "log.series.bind.paltform.del";//删除关联平台
    public static final String SERIES_BIND_TARGET_NOT_EXIST = "log.series.bind.target.no.exist";//连续剧与网元的关联不存在
    public static final String SERIES_BIND_TARGET_DEL_INF = "log.series.bind.target.del.inf";//删除关联网元详细信息
    public static final String SERIES_BIND_PALTFORM_DEL_INF = "log.series.bind.paltform.del.inf";//删除关联平台详细信息
    public static final String SERIES_BIND_TARGET_DEL_SUCCESS = "log.series.bind.target.del.success";//删除关联网元成功
    public static final String SERIES_BIND_TARGET_DEL_FAILED = "log.series.bind.target.del.fail";//删除关联网元失败
    public static final String SERIES_BIND_NO_TARGET_CAN_DEL = "log.series.bind.no.target.can.del";//没有可以删除的已关联网元
    
    //连续剧mapping关系
    public static final String SERIES_PUBLISHED_IN_TARGETSYSTEM = "log.series.published.in.targetsystem";//连续剧在此网元上已经发布
    public static final String SERIES_BIND_CONTENT_PUBLISHED = "log.series.bind.content.published";//连续剧在此的关联内容已经发布
    public static final String SERIES_PUBLISHED_BY_SERVICE = "log.series.published.by.service";//连续剧被服务在此关联，并处于发布状态
    public static final String SERIES_PUBLISHED_BY_CATEGORY = "log.series.published.by.category";//连续剧被栏目在此关联，并处于发布状态
    public static final String SERIES_PUBLISHED_BY_OTHER = "log.series.published.by.other";//连续剧被栏目或服务在此关联，并处于发布状态
    public static final String SERIES_NOT_EXIST = "log.series.not.exist" ;//连续剧不存在或已经被删除
    public static final String SERIES_BIND_CONTENT_DEL_TARGET = "log.series.bind.content.del.target";//删除电视剧关联内容mapping在网元上的发布
    public static final String SERIES_BIND_CONTENT_DEL_TARGET_INF = "log.series.bind.content.del.target.inf";//删除关联网元详细信息
    public static final String SERIES_BIND_CONTENT_DEL_PALTFORM = "log.series.bind.content.del.paltform";//删除电视剧关联内容mapping在平台上的发布
    public static final String SERIES_BIND_CONTENT_DEL_PALTFORM_INF = "log.series.bind.content.del.paltform.inf";//删除关联平台详细信息
    
}

package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.model.IcmFileinfoImport;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmFileinfoImportDAO
{
    /**
     * 新增IcmFileinfoImport对象
     * 
     * @param icmFileinfoImport IcmFileinfoImport对象
     * @throws DAOException dao异常
     */
    public void insertIcmFileinfoImport(IcmFileinfoImport icmFileinfoImport) throws DAOException;

    /**
     * 根据条件查询IcmFileinfoImport对象
     * 
     * @param icmFileinfoImport IcmFileinfoImport对象
     * @return 满足条件的IcmFileinfoImport对象集
     * @throws DAOException dao异常
     */
    public List<IcmFileinfoImport> getIcmFileinfoImportByCond(IcmFileinfoImport icmFileinfoImport) throws DAOException;

    /**
     * 根据条件分页查询IcmFileinfoImport对象，作为查询条件的参数
     * 
     * @param icmFileinfoImport IcmFileinfoImport对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmFileinfoImport对象，作为查询条件的参数
     * 
     * @param icmFileinfoImport IcmFileinfoImport对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmFileinfoImport icmFileinfoImport, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
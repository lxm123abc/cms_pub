package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.ServicePictureMap;
import com.zte.cms.picture.dao.IServicePictureMapDAO;
import com.zte.cms.picture.service.IServicePictureMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServicePictureMapDS extends DynamicObjectBaseDS implements IServicePictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServicePictureMapDAO dao = null;

    public void setDao(IServicePictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException
    {
        log.debug("insert servicePictureMap starting...");
        try
        {
            if (null == servicePictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_service_picture_map_index");
                servicePictureMap.setMapindex(mapIndex);
                servicePictureMap.setMappingid(String.format("%02d", ObjectType.SERVICEPICTUREMAP_TYPE)+ String.format("%030d", mapIndex));
            }
            
            dao.insertServicePictureMap(servicePictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert servicePictureMap end");
    }

    public void updateServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException
    {
        log.debug("update servicePictureMap by pk starting...");
        try
        {
            dao.updateServicePictureMap(servicePictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update servicePictureMap by pk end");
    }

    public void updateServicePictureMapList(List<ServicePictureMap> servicePictureMapList)
            throws DomainServiceException
    {
        log.debug("update servicePictureMapList by pk starting...");
        if (null == servicePictureMapList || servicePictureMapList.size() == 0)
        {
            log.debug("there is no datas in servicePictureMapList");
            return;
        }
        try
        {
            for (ServicePictureMap servicePictureMap : servicePictureMapList)
            {
                dao.updateServicePictureMap(servicePictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update servicePictureMapList by pk end");
    }

    public void removeServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException
    {
        log.debug("remove servicePictureMap by pk starting...");
        try
        {
            dao.deleteServicePictureMap(servicePictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove servicePictureMap by pk end");
    }

    public void removeServicePictureMapList(List<ServicePictureMap> servicePictureMapList)
            throws DomainServiceException
    {
        log.debug("remove servicePictureMapList by pk starting...");
        if (null == servicePictureMapList || servicePictureMapList.size() == 0)
        {
            log.debug("there is no datas in servicePictureMapList");
            return;
        }
        try
        {
            for (ServicePictureMap servicePictureMap : servicePictureMapList)
            {
                dao.deleteServicePictureMap(servicePictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove servicePictureMapList by pk end");
    }

    public ServicePictureMap getServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException
    {
        log.debug("get servicePictureMap by pk starting...");
        ServicePictureMap rsObj = null;
        try
        {
            rsObj = dao.getServicePictureMap(servicePictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get servicePictureMapList by pk end");
        return rsObj;
    }

    public List<ServicePictureMap> getServicePictureMapByCond(ServicePictureMap servicePictureMap)
            throws DomainServiceException
    {
        log.debug("get servicePictureMap by condition starting...");
        List<ServicePictureMap> rsList = null;
        try
        {
            rsList = dao.getServicePictureMapByCond(servicePictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get servicePictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get servicePictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(servicePictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get servicePictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get servicePictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(servicePictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServicePictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get servicePictureMap page info by condition end");
        return tableInfo;
    }
}

package com.zte.ismp.common.util;

import com.zte.umap.common.ResourceMgt;

/**
 * 用于查询国际化资源文件词条对应的描述信息
 * 
 * @author 唐秀华 tang.xiuhua@zte.com.cn
 * @version ISMP-IPTV2.04.01
 */

public class ResourceCodeParser
{
    private String resourcename;// 资源文件名称
    private int resulttype;// 结果类型（0:成功，1:失败）
    private String prefix;// 词条前缀，格式如"a.b."，注意最后一个"."不可漏掉
    private String errorcode;// 词条编码

    public ResourceCodeParser()
    {
    }

    public ResourceCodeParser(String resourcename)
    {
        this.resourcename = resourcename;
    }

    public ResourceCodeParser(String resourcename, String prefix)
    {
        this.resourcename = resourcename;
        this.prefix = prefix;
    }

    public String getResourcename()
    {
        return resourcename;
    }

    public void setResourcename(String resourcename)
    {
        this.resourcename = resourcename;
    }

    public int getResulttype()
    {
        return resulttype;
    }

    public void setResulttype(int resulttype)
    {
        this.resulttype = resulttype;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getErrorcode()
    {
        return errorcode;
    }

    public void setErrorcode(String errorcode)
    {
        this.errorcode = errorcode;
    }

    /**
     * <p>
     * 根据条件查询国际化资源文件词条对应的描述信息，所有条件参数需要事先set到相关属性中
     * </p>
     * 
     * @return 返回词条对应的描述信息，String类型
     */
    public String getResourceMsg()
    {
        ResourceMgt.addDefaultResourceBundle(resourcename);
        return resulttype + ":" + ResourceMgt.findDefaultText(prefix + errorcode);
    }

    /**
     * <p>
     * 根据条件查询国际化资源文件词条对应的描述信息，资源文件名称和词条前缀事先set到相关属性中，结果类型和词条编码通过入参传入
     * </p>
     * 
     * @param resulttype 结果类型（0:成功，1:失败），int类型
     * @param errorcode 词条编码，String类型
     * @return 返回词条对应的描述信息，String类型
     */
    public String getResourceMsg(int resulttype, String errorcode)
    {
        ResourceMgt.addDefaultResourceBundle(resourcename);
        return resulttype + ":" + ResourceMgt.findDefaultText(prefix + errorcode);
    }

    /**
     * <p>
     * 根据条件查询国际化资源文件词条对应的描述信息，所有条件参数通过入参传入
     * </p>
     * 
     * @param resourcename 资源文件名，String类型
     * @param resulttype 结果类型（0:成功，1:失败），int类型
     * @param prefix 词条前缀，String类型
     * @param errorcode 词条编码，String类型
     * 
     * @return 返回词条对应的描述信息，String类型
     * @since ISMP-IPTV2.04.01
     */
    public static String getResourceMsg(String resourcename, int resulttype, String prefix, String errorcode)
    {
        ResourceMgt.addDefaultResourceBundle(resourcename);
        return resulttype + ":" + ResourceMgt.findDefaultText(prefix + errorcode);
    }

    /**
     * <p>
     * 根据条件查询国际化资源文件词条对应的描述信息，资源文件名称和词条前缀事先set到相关属性中，词条编码通过入参传入
     * </p>
     * 
     * @param code 词条编码，String类型
     * @return 返回词条对应的描述信息，String类型
     */
    public String getResourceString(String code)
    {
        ResourceMgt.addDefaultResourceBundle(resourcename);
        return ResourceMgt.findDefaultText(prefix + code);
    }

    /**
     * <p>
     * 根据条件查询国际化资源文件词条对应的描述信息，资源文件名称事先set到相关属性中，词条通过入参传入
     * </p>
     * 
     * @param item 词条(注意，是整个词条，非词条编码)，String类型
     * @return 返回词条对应的描述信息，String类型
     */
    public String getResourceItem(String item)
    {
        ResourceMgt.addDefaultResourceBundle(resourcename);
        return ResourceMgt.findDefaultText(item);
    }
}
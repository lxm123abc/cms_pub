package com.zte.cms.cpsp.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class UcpCmsLpopNoaudit extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.Long cpindex;
    private java.lang.String cpid;
    private java.lang.String cpcnshortname;
    private java.lang.String lpid;
    private java.lang.Integer cptype;
    private java.lang.Integer lptype;
    private java.lang.Integer status;
    private java.lang.Integer cpcreditlevel;

    public void initRelation()
    {
        this.addRelation("mapindex", "mapindex");
    }

    public java.lang.Integer getCpcreditlevel()
    {
        return cpcreditlevel;
    }

    public void setCpcreditlevel(java.lang.Integer cpcreditlevel)
    {
        this.cpcreditlevel = cpcreditlevel;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.String getLpid()
    {
        return lpid;
    }

    public void setLpid(java.lang.String lpid)
    {
        this.lpid = lpid;
    }

    public java.lang.Integer getLptype()
    {
        return lptype;
    }

    public void setLptype(java.lang.Integer lptype)
    {
        this.lptype = lptype;
    }

    public java.lang.Integer getCptype()
    {
        return cptype;
    }

    public void setCptype(java.lang.Integer cptype)
    {
        this.cptype = cptype;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }
}

package com.zte.ismp.applog.service;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.ssb.logmanager.business.model.VSsblog;

public interface IAppLogDS
{
    /**
     * 根据条件分页查询VSsblog对象
     * 
     * @param vSsblog VSsblog对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询VSsblog对象
     * 
     * @param vSsblog VSsblog对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
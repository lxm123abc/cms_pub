package com.zte.cms.cpContractManagement.ls;

import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import java.util.List;

public interface IContractMapLS {

    //查询
    TableDataInfo pageInfoQuery(ContractMap contractMap, int arg1, int arg2) throws DomainServiceException;

    //删除合同
    String deleteConstractByIds(String[] ids) throws DomainServiceException;

    //根据id查询一个合同
    ContractMap selectContractById(Integer id)throws DomainServiceException;

    //根据id修改一个合同
    ContractMap updateContractById(ContractMap contractMap,String enclosure)throws DomainServiceException;

    List<ContractMap> getAttachListById(Integer id)throws DomainServiceException;

    String deleteContractAttach(String attachid)throws DomainServiceException;

    //获取合同公告模板
    String getContractTemplate(String type) throws DomainServiceException;

    //增加合同
    String insertContract(ContractMap contractMap,String description,String uploadResFileCtrl) throws DomainServiceException;


}

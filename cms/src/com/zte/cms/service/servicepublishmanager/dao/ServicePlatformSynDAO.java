
  package com.zte.cms.service.servicepublishmanager.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ServicePlatformSynDAO extends DynamicObjectBaseDao implements IServicePlatformSynDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	

    public PageInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize) throws DAOException
    {
        log.debug("page query servicePlatformSyn by condition starting...");
        PageInfo pageInfo = null;
  
            int totalCnt = ((Integer) super.queryForObject("queryServicePlatfSynListCntByCond",  servicePlatformSyn)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryServicePlatfSynListByCond" ,  servicePlatformSyn , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }    
        log.debug("page query servicePlatformSyn by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery( "queryCntTargetsysSynListByCond", "queryCntTargetsysSynListCntByCond", servicePlatformSyn, start, pageSize, puEntity);

    }


    public String updateServicePlatformSyn(ServicePlatformSyn servicePlatformSyn) throws DAOException
    {
        log.debug("update servicePlatformSyn by pk starting...");
        super.update("updateservicePlatformSyn", servicePlatformSyn);
        log.debug("update servicePlatformSyn by pk end");       
        return "DAO success";
    }

    public ServicePlatformSyn getServicePlatformSynByIndex(ServicePlatformSyn servicePlatformSyn) throws DAOException
    {
            log.debug("get servicePlatformSyn by pk start...");          
            ServicePlatformSyn resultObj=null;
            resultObj   = (ServicePlatformSyn)super.queryForObject( "getServicePlatformSynByIndex",servicePlatformSyn);                        
            log.debug("get servicePlatformSyn by pk end...");      
        return resultObj;
    }

    public List<ServicePlatformSyn> getServicePlatformSynByCond(ServicePlatformSyn servicePlatformSyn)
            throws DAOException
    {
        
        log.debug("query servicePlatformSyn list starting...");
       List< ServicePlatformSyn> resultObjList =  (List< ServicePlatformSyn>)super.queryForList( "getServicePlatformSynList",servicePlatformSyn);
        log.debug("query servicePlatformSyn list end...");
        return resultObjList;
  
    }

    public PageInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)
            throws DAOException
    {
        log.debug("pageInfoQueryforServiceProgramBing starting...");
        PageInfo pageInfo = null;
  
            int totalCnt = ((Integer) super.queryForObject("queryServiceProgramMapCntByCond",  servicePlatformSyn)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryServiceProgramMapByCond" ,  servicePlatformSyn , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }    
        log.debug("pageInfoQueryforServiceProgramBing end");
        return pageInfo;
    }

    public PageInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)
            throws DAOException
    {

        log.debug("pageInfoQueryforServiceSeriesBing starting...");
        PageInfo pageInfo = null;
  
            int totalCnt = ((Integer) super.queryForObject("queryServiceSeriesMapCntByCond",  servicePlatformSyn)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryServiceSeriesMapByCond" ,  servicePlatformSyn , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }    
        log.debug("pageInfoQueryforServiceSeriesBing end");
        return pageInfo;
    
    }

    public PageInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)
            throws DAOException
    {

        log.debug("pageInfoQueryforChannelSeriesBing starting...");
        PageInfo pageInfo = null;
  
            int totalCnt = ((Integer) super.queryForObject("queryServiceChannelMapCntByCond",  servicePlatformSyn)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryServiceChannelMapByCond" ,  servicePlatformSyn , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }    
        log.debug("pageInfoQueryforChannelSeriesBing end");
        return pageInfo;
    
    }

    public List<ServicePlatformSyn> getSameParentPlatformSyn(ServicePlatformSyn servicePlatformSyn) throws DAOException
    {
        log.debug("query getSameParentPlatformSyn list starting...");
       List< ServicePlatformSyn> resultObjList =  (List< ServicePlatformSyn>)super.queryForList( "getSameParentPlatformSyn",servicePlatformSyn);
        log.debug("query getSameParentPlatformSyn list end...");
        return resultObjList;    
    }

} 
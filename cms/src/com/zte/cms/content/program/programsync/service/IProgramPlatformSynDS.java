

package com.zte.cms.content.program.programsync.service;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramPlatformSynDS
{
   
	/**
	 * 查询CntTargetsysSyn对象
	 
	 * @param programPlatformSyn CntTargetsysSyn对象
	 * @return CntTargetsysSyn对象
	 * @throws DomainServiceException ds异常 
	 */
	 public ProgramPlatformSyn getProgramPlatformSyn3( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException; 

		/**
		 * 查询CntTargetsysSyn对象
		 
		 * @param programPlatformSyn CntTargetsysSyn对象
		 * @return CntTargetsysSyn对象
		 * @throws DomainServiceException ds异常 
		 */
		 public ProgramPlatformSyn getProgramPlatformSyn2( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException; 


	/**
	 * 查询CntTargetsysSyn对象
		 
	 * @param programPlatformSyn CntTargetsysSyn对象
	 * @return CntTargetsysSyn对象
	 * @throws DomainServiceException ds异常 
	 */
	 public ProgramPlatformSyn getCntTargetsysBindSyn( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询CntTargetsysSyn对象 
	  * 
	  * @param programPlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<ProgramPlatformSyn> getCntTargetsysSynByCond( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     //按查询条件查询
 	 

}
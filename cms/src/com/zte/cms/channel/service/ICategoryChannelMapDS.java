package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategoryChannelMapDS
{
    /**
     * 新增CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DomainServiceException;

    /**
     * 更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DomainServiceException;

    /**
     * 批量更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryChannelMapList(List<CategoryChannelMap> categoryChannelMapList)
            throws DomainServiceException;

    /**
     * 根据条件更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap) throws DomainServiceException;

    /**
     * 根据条件批量更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryChannelMapListByCond(List<CategoryChannelMap> categoryChannelMapList)
            throws DomainServiceException;

    /**
     * 删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DomainServiceException;

    /**
     * 批量删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryChannelMapList(List<CategoryChannelMap> categoryChannelMapList)
            throws DomainServiceException;

    /**
     * 根据条件删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap) throws DomainServiceException;

    /**
     * 根据条件批量删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryChannelMapListByCond(List<CategoryChannelMap> categoryChannelMapList)
            throws DomainServiceException;

    /**
     * 查询CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @return CategoryChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public CategoryChannelMap getCategoryChannelMap(CategoryChannelMap categoryChannelMap)
            throws DomainServiceException;

    /**
     * 根据条件查询CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @return 满足条件的CategoryChannelMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategoryChannelMap> getCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategoryChannelMap categoryChannelMap, int start, int pageSize)
            throws DomainServiceException;
    
    public List<CategoryChannelMap> getCategoryChannelMapByCond2(
			CategoryChannelMap categoryChannelMap)
			throws DomainServiceException;


}
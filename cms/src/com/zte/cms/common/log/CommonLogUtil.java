package com.zte.cms.common.log;

import com.zte.ismp.common.util.StrUtil;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;

/**
 * 日志工具类
 * 
 */
public class CommonLogUtil
{

    /**
     * 所有静态字符常量都来自CommonLogConstant
     * 
     * @param objectId 操作对象编码
     * @param OperType 操作类型：内容管理(MGTTYPE_CONTENT)、角色管理(MGTTYPE_ROLE)、数据管理(MGTTYPE_DATA)、存储管理(MGTTYPE_STORAGE)、
     *            直播管理(MGTTYPE_LIVE)、节目单管理(MGTTYPE_PROGRAMlIST)、收录管理(MGTTYPE_lIVERECIEVE)
     * @param optObjectType
     *            操作对象类型：（1）内容管理：OPERTYPE_CONTENT_ADD-内容新增，OPERTYPE_CONTENT_MOD-内容修改，OPERTYPE_CONTENT_DEL-内容删除，
     *            OPERTYPE_CONTENT_OFFLINE-内容下线，OPERTYPE_CONTENT_PUB-内容发布，OPERTYPE_SUBCONTENT_ADD-子内容新增，
     *            OPERTYPE_SUBCONTENT_MOD-子内容修改，OPERTYPE_SUBCONTENT_DEL-子内容删除
     *            (2)角色管理：OPERTYPE_CP_ADD-CP新增，OPERTYPE_CP_MOD-CP修改，OPERTYPE_CP_DEL-CP删除，OPERTYPE_LISENCE_ADD-牌照方新增
     *            OPERTYPE_LISENCE_MOD-牌照方修改，OPERTYPE_LISENCE_DEL-牌照方删除，OPERTYPE_LISENCE_NONAUDIT_SET-牌照方免审设置
     *            OPERTYPE_LISENCE_NONAUDIT_CANCEL-牌照方免审设置取消，OPERTYPE_OPERATOROPER_NONAUDIT_SET-运营商管理员免审设置
     *            OPERTYPE_OPERATOROPER_NONAUDIT_CANCEL-运营商管理员设置取消
     * @param operLogdesc
     *            操作详细描述:在操作对象类型字符串后面添加"_INFO",例如：操作对象类型为OPERTYPE_CONTENT_ADD-内容新增，则操作详细描述就应为OPERTYPE_CONTENT_ADD_INFO
     * @param operResult
     *            操作结果：操作成功（RESOURCE_OPERATION_SUCCESS）、操作失败（RESOURCE_OPERATION_FAIL），部分成功（RESOURCE_OPERATION_PARTIALSUCCESS）
     */
    public static void insertOperatorLog(String objectId, String operType, String optObjectType, String operLogdesc,
            String operResult)
    {

        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String OperTypeDesc = ResourceMgt.findDefaultText(operType);
        String ObjTypeDesc = null;
        String OperRstDesc = null;
        String OperRst = null;
        ObjTypeDesc = ResourceMgt.findDefaultText(optObjectType);
        OperRstDesc = ResourceMgt.findDefaultText(operLogdesc);
        OperRst = ResourceMgt.findDefaultText(operResult);

        OperInfo loginUser = LoginMgt.getOperInfo("CMS");
        // 操作员名称
        String operName = loginUser.getUserId();
        OperRstDesc = OperRstDesc.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        OperRstDesc = OperRstDesc.replaceAll("\\[objectid\\]", String.valueOf(objectId));
        OperRstDesc = OperRstDesc.replaceAll("\\[handleevent\\]", String.valueOf(OperRst));
        OperatorLog.insertOperatorLog(objectId, OperTypeDesc, ObjTypeDesc, OperRstDesc);
    }

}

package com.zte.cms.service.servicepublishmanager.dao;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceTargetSynDAO
{ 
     /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param ServiceTargetSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
     public List<ServiceTargetSyn> getCntTargetsysSynByCond( ServiceTargetSyn serviceTargetSyn )throws DAOException;

     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param ServiceTargetSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DAOException;
     
     /**
      * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
      *
      * @param Service service对象 
      * @param start 起始行
      * @param pageSize 页面大小
      * @return  查询结果
      * @throws DAOException dao异常
      */
     public PageInfo pageInfoQueryAll(Service service, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param ServiceTargetSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     /**
      * 根据主键更新ServiceTargetSyn对象
      * 
      * @param serviceTargetSyn ServiceTargetSyn对象
      * @throws DAOException dao异常
      */
     public void updateServiceTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DAOException;
     
     /**
      * 根据主键获取ServiceTargetSyn对象
      * 
      * @param serviceTargetSyn ServiceTargetSyn对象
      * @throws DAOException dao异常
      */
     
     public ServiceTargetSyn getServiceTargetSyn( ServiceTargetSyn serviceTargetSyn ) throws DomainServiceException;
    
     /**
      * 根据relateindex取ServiceTargetSyn对象
      * 
      * @param serviceTargetSyn ServiceTargetSyn对象
      * @throws DAOException dao异常
      */
     
     public List<ServiceTargetSyn> getServiceTargetSynByCond(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException;
     
     public PageInfo queryforSerProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DAOException;
     public PageInfo queryforSerSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DAOException;
     public PageInfo queryforSerChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DAOException;
     
     //根据syncindex获取网元发布数据
     public ServiceTargetSyn getMappingTargetSyn( ServiceTargetSyn serviceTargetSyn ) throws DAOException;
     //根据objectid和targetindex获得mapping关系在该网元发布状态表数据
     public ServiceTargetSyn getObjectTargetSyn( ServiceTargetSyn serviceTargetSyn ) throws DAOException;
     //通过serviceid查找所有的服务网元
     public List<ServiceTargetSyn>  getAllServiceTargetSynById( ServiceTargetSyn serviceTargetSyn ) throws DAOException;
     //根据parentid和targetindex获得所有具有相同网元和相同父节点的mapping关系
     public List<ServiceTargetSyn> getSameParentTargetMappingSyn(ServiceTargetSyn serviceTargetSyn )  throws DAOException;


     


}
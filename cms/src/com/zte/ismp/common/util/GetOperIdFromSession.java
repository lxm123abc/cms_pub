package com.zte.ismp.common.util;

import javax.servlet.http.HttpSession;

import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;

public class GetOperIdFromSession
{

    /**
     * 获取HttpSession信息
     * 
     * @return HttpSession信息
     * 
     */
    public static HttpSession getHttpSession()
    {
        HttpSession httpSession;
        try
        {
            RIAContext context = RIAContext.getCurrentInstance();
            ISession session = context.getSession();
            httpSession = session.getHttpSession();
        }
        catch (Exception e)
        {
            return null;
        }

        return httpSession;
    }

    /**
     * 从session中取operid
     * 
     * @return
     */
    public static String getOperidFromSession()
    {
        HttpSession session = getHttpSession();
        if (null != session)
        {
            Object o = session.getAttribute("OPERID");
            if (o != null)
            {
                String operid = (String) o;
                return operid;
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    public static String getOperNameFromSession()
    {
        HttpSession session = getHttpSession();
        if (null != session)
        {
            Object o = session.getAttribute("OPERNAME");
            if (o != null)
            {
                String opername = (String) o;
                return opername;
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

}

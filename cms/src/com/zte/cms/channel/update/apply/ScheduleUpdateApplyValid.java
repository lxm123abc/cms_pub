package com.zte.cms.channel.update.apply;

import java.util.ArrayList;
import java.util.List;

import net.sf.cglib.beans.BeanCopier;
import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.ls.ICmsScheduleLS;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.cms.channel.model.CmsSchedulerecord;

import com.zte.cms.channel.service.ICmsScheduleBufDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;


/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleUpdateApplyValid extends CommonValid {
	// 日志
	protected ICmsScheduleDS scheduleds = null;
	protected ICmsScheduleBufDS scheduleBufds = null;
	protected ICmsScheduleLS schedulels = null;
	private ICntTargetSyncDS cntTargetSyncDS;
	private ICmsSchedulerecordDS cmsSchedulerecordDS;

	private Log log = SSBBus.getLog(getClass());

	
	public ICmsSchedulerecordDS getCmsSchedulerecordDS()
    {
        return cmsSchedulerecordDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public ICmsScheduleLS getSchedulels() {
		return schedulels;
	}

	public void setSchedulels(ICmsScheduleLS schedulels) {
		this.schedulels = schedulels;
	}

	public ICmsScheduleDS getScheduleds() {
		return scheduleds;
	}

	public void setScheduleds(ICmsScheduleDS scheduleds) {
		this.scheduleds = scheduleds;
	}

	public ICmsScheduleBufDS getScheduleBufds() {
		return scheduleBufds;
	}

	public void setScheduleBufds(ICmsScheduleBufDS scheduleBufds) {
		this.scheduleBufds = scheduleBufds;
	}

	@Override
	public AppLogInfoEntity getLogInfo(Long index) {

		CmsSchedule schedule = new CmsSchedule();
		schedule.setScheduleindex(index);
		try {
			schedule = scheduleds.getCmsSchedule(schedule);
		} catch (DomainServiceException dsEx) {
			log.error("domain exception:" + dsEx);
			throw new RuntimeException(dsEx);
		}

		if (null != schedule) {
			AppLogInfoEntity loginfo = new AppLogInfoEntity();
			ResourceMgt
					.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

			String optObjecttype = ChannelConstants
					.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_UPDATE);
			loginfo.setOptObjecttype(optObjecttype);
			loginfo.setOptObject(String.valueOf(index));
			String info = ResourceMgt
					.findDefaultText("ucdn.schedule.log.wkfw.cntdistribute.update.valid");

			if (null != info) {
				info = info.replace("[programname]", schedule.getProgramname());
			}
			loginfo.setOptDetail(info);
			loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
			return loginfo;
		} else {
			return null;
		}

	}

	@Override
	public void operate(Long index) {
		// 修改状态
		try {
			CmsSchedule scheduleOld = new CmsSchedule();
			scheduleOld.setScheduleindex(index);
			scheduleOld = scheduleds.getCmsSchedule(scheduleOld);

			CmsScheduleBuf schedulebuf = new CmsScheduleBuf();
			schedulebuf.setScheduleindex(index);
			schedulebuf = scheduleBufds.getCmsScheduleBuf(schedulebuf);

			CmsSchedule scheduleNew = new CmsSchedule();

			BeanCopier copier = BeanCopier.create(CmsScheduleBuf.class,
					CmsSchedule.class, true);
			copier.copy(schedulebuf, scheduleNew, new BeanPropertyConvert());
			scheduleNew.setWorkflow(Integer.valueOf(0));

			CntTargetSync cntTargetSync = new CntTargetSync();
			cntTargetSync.setObjectindex(scheduleNew.getScheduleindex());
			cntTargetSync.setObjecttype(6);

			List tarlist = new ArrayList();
			try {
				tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
			} catch (DomainServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			scheduleds.updateCmsSchedule(scheduleNew);
			CmsSchedulerecord record = new CmsSchedulerecord();
            record.setScheduleid(scheduleNew.getScheduleid());
            List recordlist = cmsSchedulerecordDS.getCmsSchedulerecordByCond(record);
            if(recordlist!=null&&recordlist.size()>0){
                for(int ii=0;ii<recordlist.size();ii++){
                    record = (CmsSchedulerecord)recordlist.get(ii);
                    record.setNamecn(scheduleNew.getProgramname());
                    record.setStartdate(scheduleNew.getStartdate());
                    record.setStarttime(scheduleNew.getStarttime());
                    record.setDuration(scheduleNew.getDuration());
                    cmsSchedulerecordDS.updateCmsSchedulerecord(record);
                }
            }
			List<CntTargetSync> publist = new ArrayList();
			if (tarlist != null && tarlist.size() > 0) {
				for (int i = 0; i < tarlist.size(); i++) {
					cntTargetSync = (CntTargetSync) tarlist.get(i);
					int status = cntTargetSync.getStatus();
					if (status == 300 || status == 500) {
						publist.add(cntTargetSync);
						
					}

				}
				try {
                    schedulels.publishScheduleTar(publist);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }//可以发布到的网元
			}

			// 根据老的状态，修改新的状态
			//int oldStatus = schedulebuf.getStatus();
			//if (oldStatus == 0 || oldStatus == 80) // 待发布或者取消同步成功
			//{

				//scheduleNew.setStatus(oldStatus);
				//scheduleds.updateCmsSchedule(scheduleNew);

			//} else {
				// channelNew.setStatus(40); //修改同步中
				//schedule是查出来的
				//scheduleds.updateCmsSchedule(scheduleNew);
				// 生成同步任务
				try {

//					boolean isPublishRecord = false;
//					if ((!scheduleOld.getStartdate().equals(
//							schedulebuf.getStartdate()))
//							|| (!scheduleOld.getStarttime().equals(
//									schedulebuf.getStarttime()))
//							|| (!scheduleOld.getDuration().equals(
//									schedulebuf.getDuration()))) {
//						isPublishRecord = true;
//					}
//					CmsSchedule[] schedule = new CmsSchedule[1];
//					schedule[0] = scheduleNew;

					//schedulels.publishEdit(scheduleNew, isPublishRecord);
					//scheduleNew.setStatus(40);
					

				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			//}
			scheduleBufds.removeCmsScheduleBuf(schedulebuf);//删除临时表数据
		} catch (DomainServiceException dsEx) {
			log.error("domain exception:" + dsEx);
			throw new RuntimeException(dsEx);
		}
	}

	public ICntTargetSyncDS getCntTargetSyncDS() {
		return cntTargetSyncDS;
	}

	public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS) {
		this.cntTargetSyncDS = cntTargetSyncDS;
	}
}

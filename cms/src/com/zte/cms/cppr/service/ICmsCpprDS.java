package com.zte.cms.cppr.service;

import java.util.List;
import java.util.Map;

import com.zte.cms.cppr.model.CmsCppr;
import com.zte.cms.cppr.model.CmsUcpBasic;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsCpprDS
{
   
    public List<CmsUcpBasic> getAllCmsUcpBasicListByServiceKey(String serviceKey) throws DomainServiceException;
    
    public List<CmsCppr> getAllLocahostCmsUcpBasicListByServiceKey(String serviceKey, long operId) throws DomainServiceException;
    
    public boolean authorizeOperCp(Map paramMap);

    public boolean dismissOperCp(Map paramMap);

}
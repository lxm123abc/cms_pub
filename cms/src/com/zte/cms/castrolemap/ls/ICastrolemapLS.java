package com.zte.cms.castrolemap.ls;

import java.util.List;

import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastrolemapLS
{

    /**
     * 角色删除方法
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String deleteCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 角色查询方法
     * 
     * @param castindex 主键
     * @return
     * @throws Exception
     */
    Castrolemap getCastrolemap(Long castrolemapindex);

    /**
     * 角色新增方法
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String insertCastrolemap(Castrolemap castrolemap) throws DomainServiceException;

    /**
     * 根据条件分页查询Castrolemap对象
     * 
     * @param castrolemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Castrolemap对象 支持排序
     * 
     * @param castrolemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 批量删除
     * 
     * @param castrolemapindexs 主键集合
     * @throws Exception
     */
    public String batchDelCastrolemapList(List<String> castrolemapindexs) throws Exception;
    
    public String bindBatchCrmTargetsync(List<Targetsystem> platformList,List<Castrolemap> crmindexs);
}
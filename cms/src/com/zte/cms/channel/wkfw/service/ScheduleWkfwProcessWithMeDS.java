package com.zte.cms.channel.wkfw.service;

import java.util.List;

import com.zte.cms.channel.wkfw.dao.IScheduleWkfwProcessWithMeDAO;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwProcessWithMe;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ScheduleWkfwProcessWithMeDS implements IScheduleWkfwProcessWithMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IScheduleWkfwProcessWithMeDAO scheduleWkfwProcessWithMedao = null;

    public void setScheduleWkfwProcessWithMedao(IScheduleWkfwProcessWithMeDAO scheduleWkfwProcessWithMedao)
    {
        this.scheduleWkfwProcessWithMedao = scheduleWkfwProcessWithMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#queryStrategyWkfwProcessWithMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryScheduleWkfwProcessWithMeListCntByCond(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition)
            throws DomainServiceException
    {
        log.debug("query ChannelWkfwProcessWithMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(scheduleWkfwProcessWithMeCondition);
            totalCnt = scheduleWkfwProcessWithMedao
                    .queryScheduleWkfwProcessWithMeListCntByCond(scheduleWkfwProcessWithMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query ScheduleWkfwProcessWithMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get ScheduleWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(scheduleWkfwProcessWithMeCondition);
            pageInfo = scheduleWkfwProcessWithMedao.pageInfoQuery(scheduleWkfwProcessWithMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get ScheduleWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get ScheduleWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(scheduleWkfwProcessWithMeCondition);
            pageInfo = scheduleWkfwProcessWithMedao.pageInfoQuery(scheduleWkfwProcessWithMeCondition, start, pageSize,
                    puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition)
    {
        scheduleWkfwProcessWithMeCondition.setScheduleid(EspecialCharMgt.conversion(scheduleWkfwProcessWithMeCondition
                .getScheduleid()));

        scheduleWkfwProcessWithMeCondition.setProgramname(EspecialCharMgt.conversion(scheduleWkfwProcessWithMeCondition
                .getProgramname()));
    }
}

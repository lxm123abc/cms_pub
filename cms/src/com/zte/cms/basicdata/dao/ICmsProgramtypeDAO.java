package com.zte.cms.basicdata.dao;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsProgramtypeDAO
{

    public Integer insertCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException;

    public Integer updateCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException;

    public Integer deleteCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException;

    public CmsProgramtype getCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException;

    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException;

    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype, PageUtilEntity puEntity)
            throws DAOException;

    public PageInfo pageInfoQuery(CmsProgramtype cmsProgramtype, int start, int pageSize) throws DAOException;

    public List<CmsProgramtype> queryForMenuTree(CmsProgramtype cmsProgramtype);

    public CmsProgramtype getCmsProgramtypeById(Long id);

    public Integer deleteProgramtype(Long id) throws DAOException;

    public List<CmsProgramtype> queryChildCmsProgramtype(CmsProgramtype programtype);

    public PageInfo queryParentProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize) throws DAOException;

    public Integer queryByTypeid(CmsProgramtype cmsProgramtype);

    public Integer queryByTypename(CmsProgramtype cmsProgramtype);

    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype);

    public CmsProgramtype queryByParentid(String typeid);

    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype);

    public CmsProgramtype getCmsProgramtype(String typeid);

    public Integer isHasChild(String typeid);

    public List<CmsProgramtype> listMenuTreeByParentid(String typeid);

    public CmsProgramtype getCmsProgramtype(Long typeindex);

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex);

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype);
}
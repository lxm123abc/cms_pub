package com.zte.cms.cpsp.common;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsLpopCheckLS
{

    public boolean checkCmsLpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    public boolean checkCmsOpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    public boolean checkCmsCpExist(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

}

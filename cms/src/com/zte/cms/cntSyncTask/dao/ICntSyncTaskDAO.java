package com.zte.cms.cntSyncTask.dao;

import java.util.List;

import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICntSyncTaskDAO
{
    /**
     * 新增CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DAOException dao异常
     */
    public void insertCntSyncTask(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据主键更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DAOException dao异常
     */
    public void updateCntSyncTask(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据条件更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask更新条件
     * @throws DAOException dao异常
     */
    public void updateCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据主键删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DAOException dao异常
     */
    public void deleteCntSyncTask(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据条件删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask删除条件
     * @throws DAOException dao异常
     */
    public void deleteCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据主键查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return 满足条件的CntSyncTask对象
     * @throws DAOException dao异常
     */
    public CntSyncTask getCntSyncTask(CntSyncTask cntSyncTask) throws DAOException;

    /**
     * 根据条件查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return 满足条件的CntSyncTask对象集
     * @throws DAOException dao异常
     */
    public List<CntSyncTask> getCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException;

    /**T
     * 根据主键查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return 满足条件的CntSyncTask对象
     * @throws DAOException dao异常
     */
    public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws DAOException;
    /**T
     * 根据主键查询batchid对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return 满足条件的CntSyncTask对象
     * @throws DAOException dao异常
     */
    public List<CntSyncTask> getCntSynhisListBybatchid(CntSyncTask cntSyncTask) throws DAOException;
    /**
     * 根据条件分页查询CntSyncTask对象，作为查询条件的参数
     * 
     * @param cntSyncTask CntSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CntSyncTask对象，作为查询条件的参数
     * 
     * @param cntSyncTask CntSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    
    /**
     * 根据条件分页查询CntSyncTask对象，作为查询条件的参数
     * 
     * @param cntSyncTask CntSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
}
// @version 1.1.6

if (!this.JSON) {

// Create a JSON object only if one does not already exist. We create the
// object in a closure to avoid creating global variables.

    JSON = function () {

        function f(n) {
            // Format integers to have at least two digits.
            return n < 10 ? '0' + n : n;
        }

        Date.prototype.toJSON = function (key) {

            return this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z';
        };

        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapeable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap,
            indent,
            meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            },
            rep;


        function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

            escapeable.lastIndex = 0;
            return escapeable.test(string) ?
                '"' + string.replace(escapeable, function (a) {
                    var c = meta[a];
                    if (typeof c === 'string') {
                        return c;
                    }
                    return '\\u' + ('0000' +
                            (+(a.charCodeAt(0))).toString(16)).slice(-4);
                }) + '"' :
                '"' + string + '"';
        }


        function str(key, holder) {

// Produce a string from holder[key].

            var i,          // The loop counter.
                k,          // The member key.
                v,          // The member value.
                length,
                mind = gap,
                partial,
                value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

            if (value && typeof value === 'object' &&
                    typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

            if (typeof rep === 'function') {
                value = rep.call(holder, key, value);
            }

// What happens next depends on the value's type.

            switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

                return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

            case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

                if (!value) {
                    return 'null';
                }

// Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

// If the object has a dontEnum length property, we'll treat it as an array.

                if (typeof value.length === 'number' &&
                        !(value.propertyIsEnumerable('length'))) {

// The object is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                    v = partial.length === 0 ? '[]' :
                        gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                    mind + ']' :
                              '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

// If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        k = rep[i];
                        if (typeof k === 'string') {
                            v = str(k, value, rep);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

// Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value, rep);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

                v = partial.length === 0 ? '{}' :
                    gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
            }
        }

// Return the JSON object containing the stringify and parse methods.

        return {
            stringify: function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

                var i;
                gap = '';
                indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

                if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) {
                        indent += ' ';
                    }

// If the space parameter is a string, it will be used as the indent string.

                } else if (typeof space === 'string') {
                    indent = space;
                }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

                rep = replacer;
                if (replacer && typeof replacer !== 'function' &&
                        (typeof replacer !== 'object' ||
                         typeof replacer.length !== 'number')) {
                    throw new Error('JSON.stringify');
                }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

                return str('', {'': value});
            },


            parse: function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

                var j;

                function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                    var k, v, value = holder[key];
                    if (value && typeof value === 'object') {
                        for (k in value) {
                            if (Object.hasOwnProperty.call(value, k)) {
                                v = walk(value, k);
                                if (v !== undefined) {
                                    value[k] = v;
                                } else {
                                    delete value[k];
                                }
                            }
                        }
                    }
                    return reviver.call(holder, key, value);
                }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

                cx.lastIndex = 0;
                if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' + ('0000' +
                                (+(a.charCodeAt(0))).toString(16)).slice(-4);
                    });
                }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

                if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                    j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                    return typeof reviver === 'function' ?
                        walk({'': j}, '') : j;
                }

// If the text is not JSON parseable, then a SyntaxError is thrown.

                throw new SyntaxError('JSON.parse');
            }
        };
    }();
}


//\u89e3\u6790\u6807\u7b7e\u5f02\u5e38
var RIA_ERROR_PARSETAG = 100;//\u89e3\u6790\u6807\u7b7e
var RIA_ERROR_PARSETAG_SERVICE = 101;//\u89e3\u6790\u670d\u52a1\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_BIND = 102;//\u89e3\u6790\u90a6\u5b9a\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_INIT = 103;//\u89e3\u6790\u521d\u59cb\u5316\u51fa\u9519
var RIA_ERROR_PARSETAG_REQUEST = 104;//\u89e3\u6790\u8bf7\u6c42\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_RESPONSE = 105;//\u89e3\u6790\u8fd4\u56de\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_FORWORD = 106;//\u89e3\u6790\u8df3\u8f6c\u65f6\u51fa\u9519
//\u83b7\u53d6\u6d4f\u89c8\u5668\u51fa\u9519\uff0c\u5224\u65ad\u6d4f\u89c8\u5668\u662f\u5426\u652f\u6301
var RIA_ERROR_SUPPORT_BROWSER = 110;
//\u5c01\u88c5\u9875\u9762\u53c2\u6570\u5931\u8d25
var RIA_ERROR_ENCAPSULATION_URL = 111;
//HTTP\u54cd\u5e94\u5f02\u5e38
var RIA_ERROR_HTTPRES = 200;
var RIA_ERROR_HTTPRES_STATS = 201;
//\u4e1a\u52a1\u5904\u7406\u5f02\u5e38
var RIA_ERROR_OPERATION = 300;
//\u8c03\u7528\u670d\u52a1\u65f6callSid\u662f\u5fc5\u987b\u6307\u5b9a\u4e00\u4e2a\u7533\u660e\u7684\u670d\u52a1
var RIA_ERROR_CALLSID = 301;
var RIA_ERROR_GETCTRL = 302;
var RIA_FRAMEWORK_EXCEPTION = "RiaFrameWork";
//\u670d\u52a1\u56de\u8c03\u5f02\u5e38
var RIA_ERROR_DISPOSE = 401;//\u670d\u52a1\u5bb9\u5668\u5f02\u5e38

//\u7530\u5e94\u6743 2008-5-22 \u6dfb\u52a0\u65e5\u81f3\u5904\u7406\u7c7b
var RiaLog = function() {
};
var showMsgLevel_Ria ="default_ctrl";
RiaLog.prototype.info = function(description) {
};
RiaLog.prototype.debug = function(description){
};
RiaLog.prototype.warn = function(description){
};
RiaLog.prototype.error = function(description) {
  var code_domainService = new Array("20","21","22");
  var code_riaFramework = new Array("00","01","02","03","10","40","60","70");
  var pro_code =arguments[1];
  if(pro_code == "30"||pro_code == 30) alert(description);
  if(showMsgLevel_Ria == constantRia.SHOWDOMAIN_EXCEPTION && pro_code !=undefined){
    for(var i =0;i<code_domainService.length;i++) 
    if(pro_code ==code_domainService[i]) alert(description);
  }
  if(showMsgLevel_Ria == constantRia.SHOWRIA_FRAMEERROR && pro_code !=undefined){
    for(var i=0;i<code_riaFramework.length;i++)
    if(pro_code == code_riaFramework[i]) alert(description);
  }
};
RiaLog.prototype.trace = function(description){
};
RiaLog.prototype.fatal = function(description){
 //alert(description);
};

var logLevel;//\u8bbe\u7f6e\u65e5\u81f3\u7ea7\u522b
var LogFactory = {
    getLog:function() {
        var logjs;
        try {
            logjs = new Log4js.getLogger(RIA_FRAMEWORK_EXCEPTION);
            logLevel = logLevel || Log4js.Level.ALL;
            logjs.setLevel(logLevel);
            logjs.addAppender(new Log4js.ConsoleAppender(false));
            // \u5199\u65e5\u5fd7\u7684\u65b9\u5f0f\u4e0d\u5b89\u5168\uff0c\u6240\u4ee5\uff0c\u8fd8\u662f\u9009\u62e9\u63a7\u5236\u53f0\u6253\u5370
            // var filePath = "C:\\somefile.log";
            //logjs.addAppender(new Log4js.FileAppender(filePath));
        }
        catch(e) {
            logjs = new RiaLog();
        }
        return logjs;
    }
};
var log = new LogFactory.getLog();

//\u7530\u5e94\u6743 2008-5-22 \u6dfb\u52a0\u5f02\u5e38\u5904\u7406\u7c7b
//\u5b9a\u4e49\u5e38\u91cf
var constantRia = {
   EXCEPTION_TYPEHTTP     : "http_error",
   EX_TYPE_HTTPDESC       : 'service interrupt', 
   EXCEPTION_TYPENETWORK  : "nerwork_error",
   EX_TYPE_NETDESC        : 'network interrupt',
   RIA_EXCEPTIONTYPE      : 'exception type:',
   RIA_EXCEPTIONLINENUMBER: 'exception line:',
   RIA_EXCEPTIONCODE      : 'exception code:',
   RIA_EXCEPTIONDESC      : 'exception description:',
   RIA_FRAMEWORKEXCEPTION : 'framework exception',
   RIA_HTTPEXCEPTION      : 'HTTP exception',
   RIA_USEREXCEPTION      : 'user exception',
   RIA_EXCEBACKGROUND     : 'background exception',
   RIA_DEFAULTEXCEPTION   : 'undefined exception',
   RIA_EXCECOMPLETE       : 'service complete exception',
   RIA_EXCE_CTRLDATA      : 'UI control bind exception',
   RIA_EXCE_ORGANIZE      : 'framework organise data exception', 
   RIA_HTTP_INIT          : 'http object instance exception',  
   RIA_RIA                : "ria",
   RIA_HTTP               : "http",
   RIA_USER               : "user",
   RIA_BACKGROUND         : "background",
   SHOWDOMAIN_EXCEPTION   : "SHOWDOMAIN_EXCEPTION",
   SHOWRIA_FRAMEERROR     : "SHOWRIA_FRAMEERROR"     
};
/*
if(!Object.prototype.extend) {
    Object.extend = function(destination, source) {
      for (property in source) {
        destination[property] = source[property];
      }
      return destination;
    };
    
    Object.prototype.extend = function(object) {
      return Object.extend.apply(this, [this, object]);
    };
};
//\u5f02\u5e38\u5904\u7406\u63a5\u53e3
var IRIAException = function(){
};
//\u63a5\u53e3\u65b9\u6cd5
IRIAException.prototye={
  doException:function(exType){
    return;
  }
};*/
function RIAException(locationExceptionID,exceptionCode,exceptionDESC,reservedFiled){
  this.locationExceptionID = locationExceptionID;
  this.exceptionCode = exceptionCode;
  this.exceptionDESC = exceptionDESC;
  this.reservedFiled = reservedFiled;
};
RIAException.prototype = new Error();//\u6846\u67b6\u5b9e\u73b0\u9519\u8bef\u5f02\u5e38
RIAException.prototype = {
    doException:function(exType,product_code){
      switch(exType){
        case constantRia.RIA_RIA : log.error(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_FRAMEWORKEXCEPTION+","+this.exceptionDESC);break;
        case constantRia.RIA_HTTP: alert(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_HTTPEXCEPTION+","+this.exceptionDESC);break;
        case constantRia.RIA_USER: log.error(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_USEREXCEPTION+","+this.exceptionDESC); break;
        case constantRia.RIA_BACKGROUND:
            if(product_code == "30" ) log.error(this.exceptionDESC);
            else log.error(constantRia.RIA_EXCEPTIONLINENUMBER+this.locationExceptionID+"\r\n"+constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_EXCEBACKGROUND+","+this.exceptionDESC,product_code); 
            break;
        default: log.error(constantRia.RIA_EXCEPTIONDESC+this.exceptionDESC);break;
      }
    }
};

var commonException = function(flag,exception){
      flag = flag || "";
     //\u5224\u65ad\u662f\u6d4b\u8bd5\u73af\u5883\u8fd8\u662f\u751f\u4ea7\u73af\u5883
     if(log instanceof RiaLog){//\u751f\u4ea7\u73af\u5883\u901a\u8fc7alert\u7684\u65b9\u5f0f\u6765\u5904\u7406
        var product_code = arguments[2];
        exception.doException(flag,product_code); 
     }//\u6d4b\u8bd5\u73af\u5883\u901a\u8fc7log4js\u7684\u5f39\u51fa\u63a7\u5236\u53f0\u7684\u65b9\u5f0f\u6765\u5199\u65e5\u81f3\u4fe1\u606f       
     else{log.error(constantRia.RIA_EXCEPTIONCODE+exception.exceptionCode+constantRia.RIA_EXCEPTIONDESC+exception.exceptionDESC);}
};
var httpExceptionCode = function(httpstatus){
   var flag;
    try{
       if(httpstatus == 500 ||httpstatus == 503||httpstatus == 504) {flag = constantRia.RIA_HTTP;throw new RIAException("",constantRia.EXCEPTION_TYPEHTTP,constantRia.EX_TYPE_HTTPDESC);}
       if(httpstatus == 12029 ){flag =constantRia.RIA_HTTP; throw new RIAException("",constantRia.EXCEPTION_TYPENETWORK,constantRia.EX_TYPE_HTTPDESC); }
    }catch(e){
       commonException(flag,e);
    }    
};

function IswhichBrower(){
 
};
//function HTTPException(){
//};
//HTTPException.prototype = new Error();//http\u5f02\u5e38\u5904\u7406\u7c7b

/////////////////////////////////////////////////////////////////////
//////////////////////////////\u6570\u636e\u6821\u9a8c///////////////////////////////
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u957f\u5ea6 \u6c49\u5b57\u7b97\uff12\u4e2a\u5b57\u8282*/
function strlen(str)
{   var i;
    var len;
    len = 0;
    if(str != undefined && str != null && str != ""){
        for (i=0;i<str.length;i++)
        {
            if (str.charCodeAt(i)>255) len+=2; else len++;
        }
        return len;
    }
};
/*\u68c0\u6d4b\u662f\u5426\u662f\u5b57\u7b26\u6216\u6570\u5b57*/
function ischar(str)
{
    var s2=/[^A-Za-z0-9]/;
   if(s2.exec(str))
        return false;
    else
        return true;
};
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u7a7a */
function isnull(str)
{
    if(str == null) return true;
    var i;
    for (i=0;i<str.length;i++)
    {
        if (str.charAt(i)!=' ') return false;
    }
    return true;
};
//\u5224\u65ad\u8f93\u5165\u662f\u5426\u4e3a\u5b57\u6bcd
function isLetter(str)
{
   var regex=/^([a-zA-Z])*$/;
   if(regex.test(str))
   {
      return true;
   }
   return false;
}
/*\u5224\u65ad\u662f\u5426\u8f93\u5165\u4e86\u4e2d\u6587\u7684\u5b57\u7b26*/
function isExsitChineseChar(str){
    if( typeof str =="undefined" || isnull(str) || str =="")
        return false;
   if(str.length!=strlen(str))
        return true;
   else
        return false;
};

//\u68c0\u6d4b\u8d27\u5e01
function ismoney(str)
{
    var pos1 = str.indexOf(".");
    var pos2 = str.lastIndexOf(".");
    if ((pos1 != pos2)||!isnumber(str)) {
        return false;
    }
    return true;
};
//\u68c0\u6d4b\u5e74(YYYY\u5e74)
function  isyear(str)
{
  if(!isnumber(str))
     return false;
   //if(str.length != 4 ) {
    //  return false;
    //}

    if((str<'1950')||(str>'2050'))
    {
        return false;
    }

   return true;

};

//\u68c0\u6d4b\u975e\u7a7a
function checklength(val,maxlen)
{
    var str = trim(document.forms[0].elements[val].value);
    if ( str == "" && maxlen==null ) {
        alert ('\u6b64\u9879\u4e0d\u53ef\u4e3a\u7a7a');
        document.forms[0].elements[val].focus();
        document.forms[0].elements[val].select();
        return false;
    }else if(str!="" && maxlen!=null){
        if (str.length>maxlen){
            return false;
        }
    }
    document.forms[0].elements[val].value = str;
    return true;
};
//\u9a8c\u8bc1\u6570\u5b57
function checknum(val)
{
   if(isNaN(document.forms[0].elements[val].value)) {
       alert ('\u8bf7\u8f93\u5165\u6570\u5b57');
       document.forms[0].elements[val].focus();
       document.forms[0].elements[val].select();
       return false;
   }
   return true;
};
//\u53bb\u7a7a\u683c
function trim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
};
//\u68c0\u6d4b\u7535\u8bdd\u53f7\u7801
function isphone(str)
{
    var number_chars = "()-1234567890";
    var i;
    for (i=0;i<str.length;i++)
    {
    if (number_chars.indexOf(str.charAt(i))==-1) return false;
    }
    return true;
};

function replaceAll(str){
    var i;
    for (i=0;i<str.length;i++)
    {/*
        if(str.charAt(i)=='&')
            str=str.replace('&','&amp;');
        if(str.charAt(i)=='<')
            str=str.replace('<','&lt;');
        if(str.charAt(i)=='>')
            str=str.replace('>','&gt;');*/
        if(str.charAt(i)=='\'')
            str=str.replace('\'','\\uff07');
        if(str.charAt(i)=='\"')
            str=str.replace('\"','\\uff02');
    }
            return str;
};
//\u68c0\u67e5\u8868\u5355\u6240\u6709\u5143\u7d20
function verifyAll(myform)
{
    var i;
    for (i=0;i<myform.elements.length;i++)
    {
        /*\u5c06\u534a\u89d2\u8f6c\u6362\u4e3a\u5168\u89d2*///comment by chris:\u4ec5\u4ec5\u662f\u5c06\u4e2d\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7\u6362\u6210\u82f1\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7
        if (myform.elements[i].type=="textarea"||myform.elements[i].type=="text")
        {
            myform.elements[i].value=replaceAll(myform.elements[i].value);
        }

        /* \u975e\u81ea\u5b9a\u4e49\u5c5e\u6027\u7684\u5143\u7d20\u4e0d\u4e88\u7406\u776c */
        if (myform.elements[i].chname+""=="undefined") continue;

        /* \u6821\u9a8c\u5f53\u524d\u5143\u7d20 */
        if (verifyInput(myform.elements[i])==false)
        {
            myform.elements[i].focus();
            myform.elements[i].select();
            return false;
        }
    }
    return true;
};
function isElementDefined(el_ctrl){
  if(el_ctrl == null || el_ctrl == undefined || el_ctrl == "") return false;
  else if(el_ctrl) return true;
};
/* \u68c0\u6d4b\u6307\u5b9a\u6587\u672c\u6846\u8f93\u5165\u662f\u5426\u5408\u6cd5 */
function verifyInput(input,datatype)
{
    var i;
    var error = false;
    var isnotnull=false;
    input.datatype = datatype;
    input.chname = input.name;
    var elementCtrl = input.id;
    var el_ctrl = document.getElementById(elementCtrl+RIA_MSG_LABLE);
    /* \u975e\u7a7a\u6821\u9a8c */
    
    if (isnull(input.value))
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText ='\u4e0d\u80fd\u4e3a\u7a7a';
        else alert("\""+input.chname+"\""+'\u4e0d\u80fd\u4e3a\u7a7a\uff01');
        return false;
    }

    if(input.maxlen+""!="undefined" && strlen(input.value)>input.maxlen)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u8d85\u8fc7\u9650\u5236\u957f\u5ea6!';
        else alert("\""+input.chname+"\""+'\u8d85\u8fc7\u9650\u5236\u957f\u5ea6!');
        error = true;
        return false;
    }
    if(input.datatype+""!="undefined")
    {
     
     var type = input.datatype;
     var datasize = null;
     var size = null;
     if(type.indexOf("(") != -1)
     {
        type = input.datatype.substring(0, type.indexOf("("));
        
        datasize = input.datatype.substring(input.datatype.indexOf("(") + 1, input.datatype.indexOf(")"));
        size = datasize.split(",");
     }
    //\u4e0d\u533a\u5206\u5927\u5c0f\u5199
    type=type.toLowerCase();
    /* \u6570\u636e\u7c7b\u578b\u6821\u9a8c */
        switch(type)
        {
            case "posiint":
                 if(isnull(input.value)) error = true;
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57';
                          else alert("\""+input.chname+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57');

                          error = true;
                     }
                     else{
                         if (isPositiveInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                              if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u5e94\u4e3a\u6b63\u6574\u6570';
                              else alert("\""+input.chname+"\""+'\u503c\u5e94\u4e3a\u6b63\u6574\u6570');
                              error = true;
                         }
                         else if(size+""!="undefined" && size!=null && strlen(input.value)>size)
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6';
                             else alert("\""+input.chname+"\""+'\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6');
                             error = true
                         } 
                     }    
                  }
                  
                  break;
                  
            case "negaint":
                 if(isnull(input.value)) error = true; 
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57';
                          else alert("\""+input.chname+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57');
                          error = true;
                     }
                     else{
                         if (isNegativeNumber(input.value)==false)//\u975e\u6570\u5b57
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u5e94\u4e3a\u8d1f\u6574\u6570';
                             else alert("\""+input.chname+"\""+'\u503c\u5e94\u4e3a\u8d1f\u6574\u6570');
                             error = true;
                         }
                         else if(size+""!="undefined" && size!=null&& strlen(input.value.substring(1))>size)
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6';
                             else alert("\""+input.chname+"\""+'\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6');
                             error = true
                         } 
                     }    
                  }
                  
                  break;  
                  
             case "int":
                 if(isnull(input.value)) error = true; 
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57';
                          else alert("\""+input.chname+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57');
                          error = true;
                     }
                     else{
                         if (isInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u5e94\u4e3a\u6574\u6570';
                             else alert("\""+input.chname+"\""+'\u503c\u5e94\u4e3a\u6574\u6570');
                             error = true;
                         }
                         else if(size+""!="undefined" && size!=null)
                         {
                            var reg1  = "^[0-9]+$";          
                            var regu2 = "^-[0-9]+$";
                            var re1 = new RegExp(reg1);
                            var re2 = new RegExp(regu2);                        
                            if (input.value.search(re2) != -1 && strlen(input.value.substring(1))>size)
                            {
                              if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6';
                              else alert("\""+input.chname+"\""+'\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6');
                               error = true
                            }
                            else if(input.value.search(re1)!=-1 && strlen(input.value)>size)
                            {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6';
                                else alert("\""+input.chname+"\""+'\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6');
                                error = true
                            } 
                         } 
                     }    
                  }
                  
                  break;        
                      
            case "char": 
                 if(isnull(input.value)) error = true;
                 if (ischar(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd';
                        else alert("\""+input.chname+"\""+'\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd');
                        error = true;
                    }
                    
                    break;
                    
            case "mobile":
                 if(isnull(input.value)) error = true; 
                 if(trim(input.value)!="")
                 {            
                    if(checkMobile(trim(input.value))==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u683c\u5f0f\u4e0d\u6b63\u786e';
                        else alert("\""+input.chname+"\""+'\u683c\u5f0f\u4e0d\u6b63\u786e');
                        error = true;
                    }
                 }      
                        
                 break;
                 
            case "postcode":
                if(isnull(input.value)) error = true;
                if(checkPostCode(trim(input.value))==false)
                {
                    if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u53ea\u80fd\u662f\u957f\u5ea6\u4e3a6\u4f4d\u7684\u6570\u5b57';
                    else alert("\""+input.chname+"\""+'\u53ea\u80fd\u662f\u957f\u5ea6\u4e3a6\u4f4d\u7684\u6570\u5b57');
                    error = true;
                }
                
                break;
                
            case "posidecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                if(isnull(input.value)) error = true;
                 input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkPositiveDecimal(input,el_ctrl)==false)
                  {
                      error=true;
                  }
                }  
                
               break;
               
           case "negadecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                if(isnull(input.value)) error = true;
                input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkNegativeDecimal(input,el_ctrl)==false)
                  {
                      error=true;
                  }
                }  
                
               break;    
            
           case "decimal"://\u4e3a\u6b63\u5c0f\u6570\uff0c\u4e5f\u53ef\u4ee5\u4e3a\u8d1f\u5c0f\u6570
               if(isnull(input.value)) error = true;
               input.precision = datasize;
               if(trim(input.value)!="")
               {
                 if(checkDecimal(input,el_ctrl)==false)
                 {
                     error=true;
                 }
               }  
                 break;

           case "numberchar":
              if(isnull(input.value)) error = true;
              if(input.value!="")
              {
                 if(isNumberOrChar(trim(input.value))==false)
                 {
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText =  '\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd';
                     else alert("\""+input.chname+"\""+'\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd');
                     error = true;
                 }
              }
              
                 break;
                 
            case "phone":
                if(isnull(input.value)) error = true;
                if(input.value!="")
                {
                   if((trim(input.value)).length<6){
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u957f\u5ea6\u5fc5\u987b6\u4f4d\u4ee5\u4e0a';
                        else alert("\""+input.chname+"\""+'\u957f\u5ea6\u5fc5\u987b6\u4f4d\u4ee5\u4e0a');
                        error = true;
                        break;
                    }
                   if (isphone(input.value)==false)
                       {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u53ea\u53ef\u5305\u542b-\u3001()\u548c\u6570\u5b57';
                          else alert("\""+input.chname+"\""+'\u53ea\u53ef\u5305\u542b-\u3001()\u548c\u6570\u5b57');
                          error = true;
                       }
                  }
                  
                  break;
                  
             case "money": 
                  if(isnull(input.value)) error = true;
                  if(ismoney(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u53ea\u53ef\u5305\u542b\u4e00\u4e2a.\u548c\u6570\u5b57';
                        else alert("\""+input.chname+"\""+'\u53ea\u53ef\u5305\u542b\u4e00\u4e2a.\u548c\u6570\u5b57');
                        error = true;
                    }
                    
                    break;
                    
             case "year":   
                   if(isnull(input.value)) error = true;
                   if(isyear(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u5e94\u8be5\u662f\u4e00\u4e2a\u5e74\u4efd\u8303\u56f4\u57281950\uff0d2050\u4e4b\u95f4';
                        else alert("\""+input.chname+"\""+'\u5e94\u8be5\u662f\u4e00\u4e2a\u5e74\u4efd\u8303\u56f4\u57281950\uff0d2050\u4e4b\u95f4');
                        error = true;
                    }
                    break;
                    
             case "cardid": 
                    if(isnull(input.value)) error = true;
                    if(input.value!="")
                    {
                        return validIdCard(input.value,input.chname,el_ctrl);
                    }
                    break;
                    
             case "email":  
                   if(isnull(input.value)) error = true;
                   if(input.value!="")
                    {
                        return emailCheck(input.value,input.chname,el_ctrl);
                    }
                    
                    break;
                    
             case "date":
                   if(isnull(input.value)) error = true;             
                   input.precision = datasize;
                   if(isExsitChineseChar(input.value))
                   {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f';
                       else alert("\""+input.chname+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01');
                       error = true;
                   }
                   else
                       if(input.value != "")
                           return checkDate(input,el_ctrl);
                           //return true;
                  
                  break;  
             case "percent" :
                  if(isnull(input.value)) error = true;
                  if(isExsitChineseChar(input.value))
                  {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f';
                       else alert("\""+input.chname+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01');
                       error = true;                    
                  }else if(input.value != "")
                  {
                    if(checkPercent(input.value))
                    {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u5e94\u8be5\u662f\u4e00\u4e2a\u767e\u5206\u6570';
                       else alert("\""+input.chname+"\""+'\u5e94\u8be5\u662f\u4e00\u4e2a\u767e\u5206\u6570');
                       return false;
                    }
                    return true;
                  }else
                  {
                    return true;
                  }
                                  
            case "letter":
                  if(isnull(input.value)) error = true;
                  if(!isLetter(input.value))
                  {
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u8f93\u5165\u7684\u5e94\u8be5\u662f\u5b57\u6bcd\uff01';
                     else alert("\""+input.chname+"\""+'\u8f93\u5165\u7684\u5e94\u8be5\u662f\u5b57\u6bcd\uff01');
                     return false;
                  }
                  else{
                     return true;
                  }
            case "nonempty":
                 if (input.value == null||input.value ==""){
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u4e0d\u80fd\u4e3a\u7a7a';
                     else alert("\""+input.chname+"\""+'\u4e0d\u80fd\u4e3a\u7a7a');
                     error =true;
                     
                 } 
                 break;                       
            /* \u5728\u8fd9\u91cc\u53ef\u4ee5\u6dfb\u52a0\u591a\u4e2a\u81ea\u5b9a\u4e49\u6570\u636e\u7c7b\u578b\u7684\u6821\u9a8c\u5224\u65ad */
            /*  case datatype1: ... ; break;        */
            /*  case datatype2: ... ; break;        */
            /*  ....................................*/
            default :           
                var regu =type;
                var re = new RegExp(regu);
                if (!re.test(input.value)) 
                {
                   error = true;                 
                }   
               break;
        }
    }
    /* \u6839\u636e\u6709\u65e0\u9519\u8bef\u8bbe\u7f6e\u6216\u53d6\u6d88\u8b66\u793a\u6807\u5fd7 */
    if (error)
    {
        return false;
    }
    else
    {
        return true;
    }
};
//\u9a8c\u8bc1percent
function checkPercent(regEx)
{
    var regu =/^-?([1-9]\d*|0)(\.\d+)?%$/;
    var re = new RegExp(regu);
    if (re.test(regEx)) 
    {
       return false;                 
    }
    else
    {
      return true;
    }
}
//\u5c4f\u853d\u952e\u76d8\u5feb\u6377\u952e
function checkKey()
{
    var srce = window.event.srcElement.type;
    var shield = false;
    //\u6587\u672c\u6846\u5185\u4e0d\u5c4f\u853d\u5feb\u6377\u952e
    if(window.event.keyCode == 8 && srce =="password") return;
    if(window.event.keyCode == 8 && srce =="file") return;
    if (window.event.keyCode == 8 && srce =="text") return;
    if (window.event.keyCode == 8 && srce =="textarea") return;

    //\u5c4f\u853d\u6587\u672c\u6846\u5185\u7684\u56de\u8f66
    if (window.event.keyCode == 13 && srce =="password"){
        event.keyCode=0;
        return false;
    }

    if (window.event.keyCode == 13 && srce =="file"){
        event.keyCode=0;
        return false;
    }
    if (window.event.keyCode == 13 && srce =="text"){
        event.keyCode=0;
        return false;
    }
    if (window.event.keyCode == 13 && srce =="textarea"){
        event.keyCode=0;
        return false;
    }

    if ((event.keyCode==8) || //\u5c4f\u853d\u9000\u683c\u5220\u9664\u952e
        (event.keyCode==116)|| //\u5c4f\u853d F5 \u5237\u65b0\u952e
        (event.ctrlKey && event.keyCode==82)){ //Ctrl + R
        event.keyCode=0;
        return false;
    }
    if ((window.event.altKey)&&
        ((window.event.keyCode==37)|| //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2190
        (window.event.keyCode==39))){ //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2192
        alert("\u4e0d\u51c6\u4f60\u4f7f\u7528ALT+\u65b9\u5411\u952e\u524d\u8fdb\u6216\u540e\u9000\u7f51\u9875\uff01");
        return false;
    }
    if ((event.ctrlKey)&&(event.keyCode==78)) //\u5c4f\u853d Ctrl+n
        return false;
    if (window.event.srcElement.tagName == "A" && window.event.shiftKey)
        return false; //\u5c4f\u853d shift \u52a0\u9f20\u6807\u5de6\u952e\u65b0\u5f00\u4e00\u7f51\u9875
};
function compareDate(date1,date2){
    var value;
    if((date1==null||date1=="undefined")&&(date2==null||date2=="undefined"))
        value="d1=d2";
    else if((date1==null||date1=="undefined")&&(date2!=null&&date2!="undefined"))
         value="d1<d2";
    else if((date1!=null&&date1!="undefined")&&(date2==null||date2=="undefined"))
         value="d1>d2";
    else 
        value=date1.localeCompare(date2);
    if(value==0)
        return "d1=d2";
    else if (value>0)
        return "d1>d2";
    else
        return "d1<d2";
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u7684\u65e5\u671f\u662f\u5426\u7b26\u5408 yyyy-MM-dd
\u8f93\u5165\uff1a
    value\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDate(elem)
{
    var date=elem.value;
    var length=date.length;
    var year=date.substr(0,4);
    var month=date.substr(5,2);
    var day=date.substr(8,2);
    var var1=date.substr(4,1);
    var var2=date.substr(7,1);

    if(length == '10' && var1==var2 && (var1 == '-'||var1 == '/'||var1 == '.'))
    {
        if(isNumber(year) && isNumber(month) && isNumber(day))
        {
                    if(month>"12" || month< "01") {
                       alert("\""+elem.chname+"\""+'\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4');
                       return false;
                    }
                    if(day>getMaxDay(year,month) || day< "01") {
                       alert("\""+elem.chname+"\""+'\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408');
                       return false;
                    }
                    
                    return true;
        }
        else {
                   alert("\""+elem.chname+"\""+'\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57');
                   return false;
              }
    }
    else {
         
          
             alert("\"" + elem.chname + "\""+'\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD,YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f');
             return false;
            
    }
};

function checkDate(elem,el_ctrl)
{
    var format=elem.precision;
    var date=elem.value;
    // sujingui  
    var start=/^('|")/;
    var end=/('|")$/;
    if(date!="undefined" && date != null && date != "")
    {       
        date=date.replace(start,"");
        date=date.replace(end,"");  
    }
    if(format!="undefined" && format != null && format != "")
    {
        format=format.replace(start,"");
        format=format.replace(end,"");  
    }
    //end
    var length=date.length;
    var year=date.substr(0,4);
    var month=date.substr(5,2);
    var day=date.substr(8,2);
    var var1=date.substr(4,1);
    var var2=date.substr(7,1);

         
    if(length == '10' && var1==var2 && (var1 == '-'||var1 == '/'||var1 == '.'))
    {
        if(isNumber(year) && isNumber(month) && isNumber(day))
        {
                    if(month>"12" || month< "01") {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4';
                       else alert("\""+elem.chname+"\""+'\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4');
                       return false;
                    }
                    if(day>getMaxDay(year,month) || day< "01") {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408';
                       else alert("\""+elem.chname+"\""+'\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408');
                       return false;
                    }
                    if(format!="undefined" && format != null && format != "")
                    {
                                  
                        if(format=="YYYY/MM/DD")
                        {
                           var reg=/^(\d{4})([/])(\d{2})([/])(\d{2})/;
                           if(!reg.test(date))
                           {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY/MM/DD';
                                else alert("\""+elem.chname+"\""+'\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY/MM/DD');
                                return false;
                           }
                        }
                        else if(format=="YYYY-MM-DD")
                        {
                           var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
                           if(!reg.test(date))
                           {  
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY-MM-DD';
                                else alert("\""+elem.chname+"\""+'\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY-MM-DD');
                                return false;
                           }
                        }
                        else if(format=="YYYY.MM.DD")
                        {
                           var reg=/^(\d{4})([.])(\d{2})([.])(\d{2})/;
                           if(!reg.test(date))
                           {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY.MM.DD';
                                else alert("\""+elem.chname+"\""+'\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY.MM.DD');
                                return false;
                           }
                        }
                      }
                    return true;
        }
        else {
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57\uff01';
                   else alert("\""+elem.chname+"\""+'\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57\uff01');
                   return false;
              }
    }
    else {
         
             if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f';
             else alert("\"" + elem.chname + "\""+'\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f');
             return false;
            
    }
};
function getMaxDay(year,month) {
    if(month==4||month==6||month==9||month==11)
        return "30";
    if(month==2)
        if(year%4==0&&year%100!=0 || year%400==0)
            return "29";
        else
            return "28";
    return "31";
};

//\u8eab\u4efd\u8bc1\u4e2d\u7684\u7c4d\u8d2f\u4fe1\u606f
function IdCardArea(){
    var area={11:"\u5317\u4eac",12:"\u5929\u6d25",13:"\u6cb3\u5317",14:"\u5c71\u897f",15:"\u5185\u8499\u53e4",21:"\u8fbd\u5b81",22:"\u5409\u6797",23:"\u9ed1\u9f99\u6c5f",31:"\u4e0a\u6d77",32:"\u6c5f\u82cf",33:"\u6d59\u6c5f",34:"\u5b89\u5fbd",35:"\u798f\u5efa",36:"\u6c5f\u897f",37:"\u5c71\u4e1c",41:"\u6cb3\u5357",42:"\u6e56\u5317",43:"\u6e56\u5357",44:"\u5e7f\u4e1c",45:"\u5e7f\u897f",46:"\u6d77\u5357",50:"\u91cd\u5e86",51:"\u56db\u5ddd",52:"\u8d35\u5dde",53:"\u4e91\u5357",54:"\u897f\u85cf",61:"\u9655\u897f",62:"\u7518\u8083",63:"\u9752\u6d77",64:"\u5b81\u590f",65:"\u65b0\u7586",71:"\u53f0\u6e7e",81:"\u9999\u6e2f",82:"\u6fb3\u95e8",00:"\u56fd\u5916"};
    return area;
};

//\u8eab\u4efd\u8bc1\u7684\u9a8c\u8bc1      <!--{idcard}-->
function validIdCard(strVal,name,el_ctrl)
{
        //add by chris on 2006-03-20,\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad
        var areaArray=IdCardArea();

    var count = 0;
    var numArray = "0123456789";

    strVal =trim(strVal);
    var strSfzhm = strVal;

        if(isExsitChineseChar(strVal))
        {
           if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165';
           else alert("\""+name+"\""+'\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\uff01');
        }

    if ((strSfzhm.length !=15 ) && (strSfzhm.length!=18))
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u5e94\u8f93\u516515\u4f4d\u621618\u4f4d';
        else alert("["+name+"]" +'\u5e94\u8f93\u516515\u4f4d\u621618\u4f4d');
        return false;
    }
    else
    {
        if (strSfzhm.length == 15)
        {
            for(var i=0;i<15;i++)
            {
                tmpChar = strSfzhm.charAt(i);
                if ( numArray.indexOf(tmpChar) == -1 )
                {
                    count = count+1;
                }
            }
            if(count!=0)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '15\u4f4d\u6570\u5b57\u8f93\u5165\u6709\u8bef!';
                else alert("["+name+"]"+'15\u4f4d\u6570\u5b57\u8f93\u5165\u6709\u8bef!');
                return false;
            }
        }
        else            //18\u4f4d\u7684SFZHM\u6821\u9a8c
        {
            for(i=0;i<17;i++)
            {
                tmpChar = strSfzhm.charAt(i);
                if(numArray.indexOf(tmpChar)==-1)
                {
                    if(isElementDefined(el_ctrl)) el_ctrl.innerText = '18\u4f4d\u65f6\u524d17\u6570\u5b57\u8f93\u5165\u6709\u8bef';
                    else alert("["+name+"]" + '18\u4f4d\u65f6\u524d17\u6570\u5b57\u8f93\u5165\u6709\u8bef' );
                    return false;
                }
            }
            var lastchar = strSfzhm.charAt(17).toLowerCase();
            var denominator='abcdefghijklmnopqrstuvwxyz0123456789';
            if(denominator.indexOf(lastchar) == -1)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '18\u4f4d\u5e94\u4e3a\u6570\u5b57\u6216\u5b57\u6bcd!';
                else alert("["+name+"]" + '18\u4f4d\u5e94\u4e3a\u6570\u5b57\u6216\u5b57\u6bcd!');
                return false;
            }
        }
                    //\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad\uff0cadd by chris on 2006-03-20
                     var sub=strVal.substr(0,2);
                     if((typeof areaArray[sub]) =="undefined"){
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u524d\u4e24\u4f4d\u6570\u5b57\u5bf9\u5e94\u7684\u7c4d\u8d2f\u4e3a\'\u4e0d\u660e\u7701\u4efd\'';
                        else alert("["+name+"]" +'\u524d\u4e24\u4f4d\u6570\u5b57\u5bf9\u5e94\u7684\u7c4d\u8d2f\u4e3a\'\u4e0d\u660e\u7701\u4efd\'' );
                        return false;
                     }
    }
    return true;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u624b\u673a\u53f7\u7801\u662f\u5426\u6b63\u786e
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function checkMobile( s ){
    //sujingui
    var regu =/^[1][3|5][0-9]{9}$/;
    var re = new RegExp(regu);
    if (re.test(s)) {
      return true;
    }else{
      return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u90ae\u7f16\u683c\u5f0f\u662f\u5426\u6b63\u786e
*/
function checkPostCode(s){
   if(s!= null && s.length==6&&isNumber(s)){
      return true;
   }
   else
     return false;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u6b63\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumber( s ){
    var regu = "^[0-9]+$";
    var re = new RegExp(regu);
    if (s!=null && s.search(re) != -1) {
       return true;
    } else {
       return false;
    }
};

function isInteger( s ){
    var regu1 = "^[0-9]+$";
    var regu2 = "^-[0-9]+$";
    var re1 = new RegExp(regu1);
    var re2 = new RegExp(regu2);

    if (s!=null && (s.search(re1) != -1 || s.search(re2) != -1)) {
       return true;
    } else {
       return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u8d1f\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNegativeNumber( s ){
    var regu = "^-[0-9]+$";
    var re = new RegExp(regu);
    if (s!=null && s.search(re) != -1) {
       return true;
    } else {
       return false;
    }
};

/*
  \u7528\u9014:\u65e5\u671f\u8054\u60f3\u8f93\u5165\u6cd5
*/
function smartDay(obj){
  var year,month,day;
  var date=obj.value;
  if(date.length==8){
     if(isNumber(obj.value)){
        year=date.substr(0,4);
    month=date.substr(4,2);
    day=date.substr(6,2);
        obj.value=year+"-"+month+"-"+day;
    }
  }

};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u53ea\u7531\u82f1\u6587\u5b57\u6bcd\u548c\u6570\u5b57\u7ec4\u6210
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumberOrChar( s ){    //\u5224\u65ad\u662f\u5426\u662f\u6570\u5b57\u6216\u5b57\u6bcd
    var regu = "^[0-9a-zA-Z]+$";
    var re = new RegExp(regu);
    if (re.test(s)) {
      return true;
    }else{
      return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u53ef\u4ee5\u662f\u8d1f\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDecimal( str ){
        if(isNumber(str)||isNegativeNumber(str)) return true;
    var re = /^[-]{0,1}(\d+)[\.]+(\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveDecimal( str ){
       if(isNumber(str)) return true;

    var re = /^(\d+)[\.](\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};
function isNegativeDecimal( str ){
       if(isNegativeNumber(str)) return true;

    var re = /^-(\d+)[\.](\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};
/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveInteger( str ){
       if(isNumber(str)) return true;
    var re = /^(\d+)(\d+)$/;
    if (re.test(str)) {
       if(RegExp.$1==0&&RegExp.$2==0) return false;
       return true;
    } else {
       return false;
    }
};

function checkNegativeDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
                if(isNegativeDecimal(obj.value)==false){
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u8f93\u5165\u5fc5\u987b\u4e3a\u8d1f\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u8d1f\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206';
                   else alert("\""+obj.chname+"\""+'\u8f93\u5165\u5fc5\u987b\u4e3a\u8d1f\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u8d1f\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206');
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       if(numType.indexOf(",")!=-1)
                       {
                          numpric = Math.abs( strArr[1] );
                       }
                       else
                       { 
                          numpric=0;
                       }
                      
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function checkPositiveDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
                if(isPositiveDecimal(obj.value)==false){
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u8f93\u5165\u5fc5\u987b\u4e3a\u6b63\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6b63\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206';
                   else alert("\""+obj.chname+"\""+'\u8f93\u5165\u5fc5\u987b\u4e3a\u6b63\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6b63\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206');
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       //\u5f53\u6709\u4e24\u4e2a\u53c2\u6570\u9650\u5236\u65f6
                       if(numType.indexOf(",")!=-1)
                       {
                          numpric = Math.abs( strArr[1] );
                       }
                       else
                       {
                          numpric = 0;
                       }
                                  
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function checkDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
            if(isDecimal(obj.value)==false){
               if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u8f93\u5165\u5fc5\u987b\u4e3a\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206';  
               else alert("\""+obj.chname+"\""+'\u8f93\u5165\u5fc5\u987b\u4e3a\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206\uff01');
               return false;
            }
            if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       if(numType.indexOf(",")!=-1)
                       {
                            numpric = Math.abs( strArr[1] );
                       }
                       else
                       {
                            numpric = 0;
                       }
                      
                      
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function f_checkNumLenPrec(obj, len, pric,el_ctrl){
    var numReg;
    var value = obj.value;
    var strValueTemp, strInt, strDec;
    //alert(value + "=====" + len + "====="+ pric);
    try{
    
        numReg =/[\-]/;
        strValueTemp = value.replace(numReg, "");
        strValueTemp = strValueTemp.replace(numReg, "");
        //\u6574\u6570
        if(pric==0){
            numReg =/[\.]/;
            //alert(numReg.test(value));
            if(numReg.test(value) == true){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u503c\u5e94\u4e3a\u6574\u6570';                
                else alert("\""+obj.chname+"\""+'\u503c\u5e94\u4e3a\u6574\u6570');
                return false;
            }
        }

        if(strValueTemp.indexOf(".") < 0 ){
            //alert("lennth==" + strValueTemp);
            if(strValueTemp.length > len){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u6574\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570';  
                else alert("\""+obj.chname+"\""+'\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7'+ len +'\u4f4d');
                return false;
            }

        }else{
            strInt = strValueTemp.substr( 0, strValueTemp.indexOf(".") );
            //alert("lennth==" + strInt);
            if(strInt.length > len ){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u6574\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570';
                else alert("\""+obj.chname+"\""+'\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7'+ len + '\u4f4d');
                return false;
            }

            strDec = strValueTemp.substr( (strValueTemp.indexOf(".")+1), strValueTemp.length );
            //alert("pric==" + strDec);
            if(strDec.length > pric){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u5c0f\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570';
                else alert("\""+obj.chname+"\""+'\u5c0f\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7' +  pric +'\u4f4d');
                return false;
            }
        }

        return true;
    }catch(e){
        alert("in f_checkNumLenPrec = " + e);
        return false;
    }
};


function emailCheck (emailStr,name,el_ctrl)
{
    var checkTLD=1;
    var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
    var emailPat=/^(.+)@(.+)$/;
    var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
    var validChars="\[^\\s" + specialChars + "\]";
    var quotedUser="(\"[^\"]*\")";
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom=validChars + '+';
    var word="(" + atom + "|" + quotedUser + ")";
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
    var matchArray=emailStr.match(emailPat);

    if (matchArray==null)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u65e0\u6548\u5b57\u7b26\u5fc5\u987b\u542b\u6709\'@\'\u548c\'.\'';
        else alert("["+name+"]" +'\u65e0\u6548,\u5fc5\u987b\u542b\u6709\'@\'\u548c\'.\'');
        return false;
    }
    var user=matchArray[1];
    var domain=matchArray[2];

    for (i=0; i<user.length; i++)
    {
        if (user.charCodeAt(i)>127)
        {
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u542b\u6709\u975e\u6cd5\u5b57\u7b26';
            else alert("["+name+"]" + '\u542b\u6709\u975e\u6cd5\u5b57\u7b26');
            return false;
        }
    }
    for (i=0; i<domain.length; i++) {
        if (domain.charCodeAt(i)>127) {
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01';
            else alert("["+name+"]" + '\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01' ) ;
            return false;
        }
    }

    if (user.match(userPat)==null)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u540d\u79f0\u6709\u8bef';
        else alert("["+name+"]" + '\u540d\u79f0\u6709\u8bef');
        return false;
    }

    var IPArray=domain.match(ipDomainPat);
    if (IPArray!=null)
    {
        for (var i=1;i<=4;i++)
        {
            if (IPArray[i]>255)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u540d\u79f0\u6709\u8bef';
                else alert("["+name+"]" +  '\u8f93\u5165\u6709\u8bef');
                return false;
            }
        }
        return true;
    }

    var atomPat=new RegExp("^" + atom + "$");
    var domArr=domain.split(".");
    var len=domArr.length;
    for (i=0;i<len;i++)
    {
        if (domArr[i].search(atomPat)==-1)
        { 
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u57df\u540d\u6709\u8bef';
            else alert("["+name+"]" + '\u4e2d\u7684\u57df\u540d\u6709\u8bef');
            return false;
        }
    }

    if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u5fc5\u987b\u4ee5\u4e00\u4e2a\u6709\u7528\u57df\u540d\u7ed3\u675f';
        else alert("["+name+"]" + '\u5fc5\u987b\u4ee5\u4e00\u4e2a\u6709\u7528\u57df\u540d\u7ed3\u675f');
        return false;
    }

    if (len<2)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = '\u6ca1\u6709\u8f93\u5165\u4e3b\u673a\u5730\u5740';
        else alert("["+name+"]" + '\u6ca1\u6709\u8f93\u5165\u4e3b\u673a\u5730\u5740');
        return false;
    }
    return true;
} ;
window.addOnloadEvent = function(f) {
    // If already loaded, just invoke f() now.
    if (addOnloadEvent.loaded) f();
    // Otherwise, store it for later
    else addOnloadEvent.funcs.push(f);
};
// The array of functions to call when the document loads
addOnloadEvent.funcs = [];
// The functions have not been run yet.
addOnloadEvent.loaded = false;

// Run all registered functions in the order in which they were registered.
// It is safe to call addOnloadEvent.run() more than once: invocations after the
// first do nothing. It is safe for an initialization function to call
// addOnloadEvent() to register another function.
addOnloadEvent.run = function() {
    // If we've already run, do nothing
    if (addOnloadEvent.loaded) return;
    for(var i = 0; i < addOnloadEvent.funcs.length; i++) {
        try { addOnloadEvent.funcs[i](); }
        catch(e) { /* An exception in one function shouldn't stop the rest */ }
    }
    // Remember that we've already run once.
    addOnloadEvent.loaded = true;
    // But don't remember the functions themselves.
    delete addOnloadEvent.funcs;
    // And forget about this function too!
    delete addOnloadEvent.run;
};

// Register addOnloadEvent.run() as the onload event handler for the window
if (window.addEventListener)
    window.addEventListener("load", addOnloadEvent.run, false);
else if (window.attachEvent) window.attachEvent("onload", addOnloadEvent.run);
else window.onload = addOnloadEvent.run;
function $res_entry(){
  var argument_one = arguments[0];
  var argument_two = arguments[1];
  if(argument_two== 'undefine'||argument_two==null ||argument_two=="")
  return "no value";
  else return argument_two;
};


//----------------------- add by Daniel for event manager--------------begin
if(!Array.prototype.push){
    Array.prototype.push=function(elem){
         this[this.length]=elem;
    }
}
var  EventManager={
    _registry: null,
    Initialise: function(){
         if(this._registry==null){
             this._registry=[];
             //  Register the cleanup handler on page unload. 
             EventManager.Add(window,"unload" ,this.CleanUp);
         }
    },
    /* 
     * Registers an event and handler with the manager.
     *
     * @param  obj         Object handler will be attached to.
     * @param  type        Name of event handler responds to.
     * @param  fn          Handler function.
     * @param  useCapture  Use event capture. False by default.
     *                     If you don't understand this, ignore it.
     *
     * @return True if handler registered, else false.
     */ 
    Add: function(obj, type, fn, useCapture){
         this.Initialise();
         //  If a string was passed in, it's an id. 
         if(typeof obj=="string"){
            obj = document.getElementById(obj);
         }
         if(obj==null || fn==null){
            return  false ;
         }
         // Mozilla/W3C listeners? 
         if(obj.addEventListener){
            obj.addEventListener(type, fn, useCapture);
            this._registry.push({obj: obj, type: type, fn: fn, useCapture: useCapture});
            return  true ;
         }
         //  IE-style listeners? 
         if(obj.attachEvent && obj.attachEvent("on" + type,fn)){
            this._registry.push({obj: obj, type: type, fn: fn, useCapture: false });
            return true ;
         }
         return false ;
    },
    /* *
     * Cleans up all the registered event handlers.
     */ 
    CleanUp: function(){
         for(var i=0;i<EventManager._registry.length;i++){
             with(EventManager._registry[i]) {
                 // Mozilla/W3C listeners? 
                 if(obj.removeEventListener) {
                    obj.removeEventListener(type, fn, useCapture);
                 }
                 else if(obj.detachEvent){//  IE-style listeners? 
                    obj.detachEvent("on"+type,fn);
                 }
             }
         }
         //  Kill off the registry itself to get rid of the last remaining 
         //  references. 
         EventManager._registry = null ;
    }
}; 
//----------------------- add by Daniel for event manager--------------end
//\u6570\u636e\u7ed1\u5b9a\u5230\u63a7\u4ef6
function BindDataToCtrl(id, eleVal,ctrlext,datatype) {
    //\u901a\u8fc7id\u83b7\u53d6\u63a7\u4ef6\u5bf9\u8c61\u6570\u7ec4\u3002
    var obj = $("#" + id) ;

    //\u53ea\u80fd\u9488\u5bf9\u5176\u4e2d\u4e00\u4e2a\uff08id\u552f\u4e00\uff09
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d\u79f0
        var tagNam = getTageName(obj[0]);
            //input
        if ("input" == tagNam) {
            //\u6587\u672c\u6846
            
            if ("text" == obj[0].type) {
                if(datatype&&datatype=="NullString"){
                   obj[0].value = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].value = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }
            } else
                //\u5355\u9009\u6309\u94ae
            if ("radio" == obj[0].type) {
                BindDataToRadioCtrl(obj, eleVal);
            } else 
                //\u590d\u9009\u6309\u94ae
            if ("checkbox" == obj[0].type) {
                BindDataToRadioMultCtrl(obj, eleVal);
            } else 
            if ("hidden" == obj[0].type) {
                obj[0].value = eleVal == null ? "" : eleVal;
            } else
            {
                
                obj[0].value = eleVal;
            }
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            BindDataToSelectCtrl(obj, eleVal,ctrlext);
        }
        //\u6811\u578b\u6309\u94ae
        else if ("tree" == tagNam) {
            var tree = new Tree_Factory(eleVal, id);
            tree.initTree();
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
           SSB_RadioGroup.radioGroupObjs[id].setValue(eleVal);
        }
		//\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
        else if ("slider" == tagNam) {
           SliderCtrObj.setValueObjFromSlider(id,eleVal);
        }
		//\u81ea\u5b9a\u4e49extextarea\u63a7\u4ef6
        else if ("extextarea" == tagNam) {
           SSB_EXTEXTAREA.setTextAreaValue(id,eleVal);
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            setCalendarValue(id, eleVal);
        }
        //table
        else if ("table" == tagNam) {
            setTableValue(id, eleVal);
        }
        //Label
        else if ("label" == tagNam) {
      
            if (!isIE()) {
                  if(datatype&&datatype=="NullString"){
                  obj[0].textContent = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].textContent = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }  
            } else {
                //obj[0].innerText = eleVal;
                if(datatype&&datatype=="NullString"){
                  obj[0].innerText = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].innerText = (eleVal == null||eleVal == "null") ? "" : eleVal;
                } 
            }
        }
        //ip
        else if("ip" == tagNam) {
            SSB_IP.ipObjs[id].setValue(eleVal);
        }
        //textArea
        else if("textarea" == tagNam) {
            if(datatype&&datatype=="NullString"){
                   obj[0].value = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].value = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }
        }
        // add by 
        else if ("barchart" == tagNam) {
            setbarsValue(id,eleVal);
        }
         else if ("lineschart" == tagNam) {
            setlinesValue(id,eleVal);
        }
         else if ("piechart" == tagNam) {
            setpiesValue(id,eleVal);
        }
        //\u9ed8\u8ba4\u60c5\u51b5
        else
        {
            obj[0].value = eleVal;
        }
    }
};

//\u4ece\u63a7\u4ef6\u4e0a\u53d6\u503c
function GetDataFromCtrl(id) {
    //\u63a7\u4ef6\uff08\u6570\u7ec4\uff09
    var obj = $("#" + id) ;
    //id\u552f\u4e00\uff0c\u6240\u4ee5obj\u552f\u4e00
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d
        var tagNam = getTageName(obj[0]);
            //\u6587\u672c\u6846
        if ("input" == tagNam) {
            if ("text" == obj[0].type) {
                return obj[0].value;
            } else if ("radio" == obj[0].type) {
                return GetDataFromRadioCtrl(obj);
            } else if ("checkbox" == obj[0].type) {
                return GetDataFromRadioMultCtrl(obj);
            } else if ("hidden" == obj[0].type) {
                return obj[0].value;
            } else if("file" == obj[0].type){
                __file_List.push(document.getElementById(id));
                return obj[0].value;
            }else{              
                return obj[0].value;
            } 
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            return GetDataFromSelectCtrl(obj[0]);
        }
        //\u81ea\u5b9a\u4e49\u6811
        else if ("tree" == tagNam) {
            var tree = SSB_Tree.trees[obj.attr("id")];
            var ret = tree.getValue();
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
            var ret = SSB_RadioGroup.radioGroupObjs[obj.attr("id")].getValue();
            return ret;
        }
		//\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
        else if ("slider" == tagNam) {
			var ret = SliderCtrObj.getValueObjFromSlider(obj.attr("id")); 				
            return ret;
        }
		//\u81ea\u5b9a\u4e49extextarear\u63a7\u4ef6
        else if ("extextarea" == tagNam) {
			var ret = SSB_EXTEXTAREA.getTextAreaValue(obj.attr("id")); 				
            return ret;
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            var ret = getCalendarValue(id);
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u8868\u683c
        else if ("table" == tagNam) {
            var ret = getTable_Trs(id);
            return ret;
        }
        //Label
        else if ("label" == tagNam) {
            if (!isIE()) {
                return obj[0].textContent;
            } else {
                return obj[0].innerText;
            }
        }else if("upload"==tagNam){
            return GetDataFromUploadCtrl(obj[0]);
        }
//ip
        else if("ip" == tagNam) {
            var ret = SSB_IP.ipObjs[obj.attr("id")].getValue();
            return ret; 
        }else{
            //\u9ed8\u8ba4
            return obj[0].value;
        }
    }
};


//\u53d6\u6807\u7b7e\u540d\u79f0\uff0c\u907f\u514did\u548cfirefox\u6709\u4e0d\u540c\u7684\u6807\u7b7e\u540d\u79f0
function getTageName(obj) {
    //\u6587\u672c\u6846
    if ("input" == (obj.tagName).toLowerCase()) {
        return "input";
    }
    //
    if ("textarea" == (obj.tagName).toLowerCase()) {
        return "textarea";
    }
    //\u4e0b\u62c9
    if ("select" == (obj.tagName).toLowerCase()) {
        return "select";
    }
    //\u81ea\u5b9a\u4e49\u6309\u94ae\u7ec4
    if ("radiogroup" == (obj.tagName).toLowerCase() || "z:radiogroup" == (obj.tagName).toLowerCase()) {
        return "radiogroup";
    }
	//\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
    if ("slider" == (obj.tagName).toLowerCase() || "z:slider" == (obj.tagName).toLowerCase()) {
        return "slider";
    }
	//\u81ea\u5b9a\u4e49extextarea\u63a7\u4ef6
    if ("extextarea" == (obj.tagName).toLowerCase() || "z:extextarea" == (obj.tagName).toLowerCase()) {
        return "extextarea";
    }
    //\u81ea\u5b9a\u4e49\u65e5\u5386\u63a7\u4ef6
    if ("calendar" == (obj.tagName).toLowerCase() || "z:calendar" == (obj.tagName).toLowerCase()) {
        return "calendar";
    }
    //\u81ea\u5b9a\u4e49\u8868\u683c\u63a7\u4ef6
    if ("table" == (obj.tagName).toLowerCase() || "z:table" == (obj.tagName).toLowerCase()) {
        return "table";
    }
    //\u81ea\u5b9a\u4e49\u6811\u578b\u63a7\u4ef6
    if ("tree" == (obj.tagName).toLowerCase() || "z:tree" == (obj.tagName).toLowerCase()) {
        return "tree";
    }
    //label\u6807\u7b7e
    if ("label" == (obj.tagName).toLowerCase()) {
        return "label";
    }
    if("upload"==(obj.tagName).toLowerCase()||"z:upload"==(obj.tagName).toLowerCase()){
        return "upload";
    }
     //ip\u6807\u7b7e
    if("ip" == (obj.tagName).toLowerCase() || "z:ip" == (obj.tagName).toLowerCase()) {
        return "ip";
    }

    if ("barchart" == (obj.tagName).toLowerCase()) {
        return "barchart";
    }
    if ("lineschart" == (obj.tagName).toLowerCase()) {
        return "lineschart";
    }
    if ("piechart" == (obj.tagName).toLowerCase()) {
        return "piechart";
    }
    return "other";
};


//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioMultCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
    //\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
    //\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
   for (var i = 0; i < radioObjs.length && elementvalue !=null ; i++) {
       //\u5982\u679c\u662f\u5355\u4e2a\u503c\uff0c\u800c\u4e0d\u662f\u6570\u7ec4
       var tmpArr = new Array();
       if( elementvalue instanceof Array )
           tmpArr = elementvalue ;
       else 
           tmpArr[0] = elementvalue;
        for (var j = 0; tmpArr != null && j < tmpArr.length; j++) {
            var radioObj = radioObjs[i] ;
            var varV = tmpArr[j];
            //\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9                
            if (varV !=null && varV.toString() == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
        }
    }
};

//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
    //\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
    //\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
    for (var i = 0; i < radioObjs.length; i++) {
       
            var radioObj = radioObjs[i] ;
            var varV = elementvalue;
            //\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9                    
            if (varV == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
       
    }
};
        
//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioMultCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn ;
    var j = 0 ;
    //\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    if ( radioObjs.length == 1 ) {
        rtn = "";
        var radioObj = radioObjs[0];
        if (radioObj.checked) {
            rtn = radioObj.getAttribute("value");
        }
    } else {
        rtn = new Array()   
        for (var i = 0; i < radioObjs.length; i++) {
            var radioObj = radioObjs[i] ;
            if (radioObj.checked) {
                rtn[j++] = radioObj.getAttribute("value");
            }
        }
    }
    return rtn;
};

//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn = "";
    var j = 0 ;
    //\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    for (var i = 0; i < radioObjs.length; i++) {
        var radioObj = radioObjs[i] ;
        if (radioObj.checked) {
            return radioObj.getAttribute("value");
        }
    }
    return rtn;
};

//\u7ed1\u5b9a\u503c\u5230\u4e0b\u62c9\u6846
function BindDataToSelectCtrl(obj, elementvalue,ctrlext ) {

    //\u6709\u91cd\u590d\u64cd\u4f5c\uff0c\u4f18\u5316\uff01
    var selectObj = document.getElementById(obj.attr("id"));
    var refObj = elementvalue ;

    if (undefined == ctrlext )
    {
        for (var i = 0; i < selectObj.length; i++) {
            //\u5224\u65ad\u88ab\u9009\u4e2d\u7684\u4e0b\u62c9\u5143\u7d20
            var optA = selectObj.options[i] ;
            if (!selectObj.getAttribute('multiple') && elementvalue == optA.value) {
                optA.selected = true;
            }
            else if(selectObj.getAttribute('multiple'))
            {
                for(var m = 0;m < elementvalue.length; m++)
                {
                    if(elementvalue[m] == optA.value)
                    {
                        optA.selected = true;
                    }
                }
            }
        }
    } else {
        var textName = ctrlext.text;
        var valueName = ctrlext.value;
        //\u5982\u679c\u6709\u503c
        if ( refObj )
        {
            //\u5982\u679c\u662f\u5bf9\u8c61
            if ( refObj instanceof(Object))
            { 
                 
                var textArr = refObj[textName] ;
                var valueArr = refObj[valueName];
                var defaultValue = refObj[ctrlext.defaultValue];
                        
                var k = 0;
                if ( selectObj )
                {
                    k = selectObj.length;
                }
                for (var j = 0; j < k; j++) {
                    selectObj.remove(0);
                }
                //\u5faa\u73af\u5224\u65ad
                for (var i = 0; i < textArr.length  && textArr.length  == valueArr.length ; i++) {
                    var optA = document.createElement("OPTION");
                    optA.text = textArr[i];
                    optA.value = valueArr[i];
                    selectObj.options.add(optA);
                    //\u5224\u65ad\u88ab\u9009\u62e9\u7684\u4e0b\u62c9\u5143\u7d20
                    if (!selectObj.getAttribute('multiple') && defaultValue == optA.value) {                                
                        optA.selected = true;                
                    }
                    else if(selectObj.getAttribute('multiple') && defaultValue && defaultValue.length && defaultValue.length > 0)
                    {
                        for(var n = 0;n < defaultValue.length; n++)
                        {
                            if(defaultValue[n] == optA.value)
                            {
                                optA.selected = true;
                            }
                        }               
                    }                  
                }
            }
            
        }
        
    }
};


//\u83b7\u53d6\u4e0b\u62c9\u7684\u9009\u4e2d\u503c
function GetDataFromSelectCtrl(obj) {
    var ret = new Array();
    var j = 0 ;
    if (!obj.getAttribute('multiple')) {
        return obj.value;
    }
    //\u4ece\u6240\u6709\u7684\u4e0b\u62c9\u5143\u7d20\u4e2d\u5224\u65ad\u88ab\u9009\u62e9\u7684\u5143\u7d20\u503c
    for (var i = 0; i < obj.length; i++) {
        var optA = obj.options[i] ;
        if (optA.selected) {
            ret[j++] = optA.value;
        }
    }
    return ret;
};

//\u5224\u65adie\u7c7b\u578b
function isIE() {
    return window.navigator.userAgent.indexOf("MSIE") != -1;
};
var __file_List=[];
//\u83b7\u53d6\u4e0a\u4f20\u6587\u4ef6\u63a7\u4ef6\u7684\u503c,\u53ea\u662f\u6587\u4ef6\u8def\u5f84
function GetDataFromUploadCtrl(obj){
    //wanghao
    __file_List = [];

    var values=[];
    var inputs=obj.getElementsByTagName('INPUT');
    for(var i=0;i<inputs.length;i++){
        if(inputs[i].type=='file')
        {   
                    
            __file_List.push(inputs[i]);                
        }

        values.push(inputs[i].value);
    }
    return values;
}

//constant
var RIA_BIND_TAG = "bind";
var RIA_S_TAG = "s";
var RIA_Z_TAG = "z:";
var RIA_INSTANCE_TAG = "instance";
var RIA_SERVICE_TAG = "service";
var RIA_SETVALUE_TAG = "setvalue";
var RIA_INCLUDE_TAG = "include";//\u65b0\u589einclude\u6807\u7b7e
var RIA_INIT_TAG = "init";
var RIA_REQUEST_TAG = "request";
var RIA_RESPONSE_TAG = "response";
var RIA_FORWARD_TAG = "forward";
var RIA_LOADING_TAG = "loading";
var RIA_ZREQUEST_TAG = "Z:REQUEST";
var RIA_ZRESPONSE_TAG = "Z:RESPONSE";
var RIA_ZFORWARD_TAG = "Z:FORWARD";
var RIA_ZLOADING_TAG = "Z:LOADING";
var RIA_DOT_TAG = ".";
//add include tags properties
var INCLUDE_ID_TAG = "id";
var INCLUDE_URL_TAG = "url";
//loadingtag class properties
var LOADING_ID_TAG ="id";
var LOADING_ISSYN_TAG ="isSyn";
var LOADING_DIVID_TAG = "divId";
var LOADING_IMG_TAG ="img";
var LOADING_DELAY_TAG ="delay";
var LOADING_WIDTH_TAG ="width";
var LOADING_HEIGHT_TAG ="height";

//Bindtag class properties
var BIND_ID_TAG = 'id';

var BIND_CTRL_TAG = 'ctrl';

var BIND_REF_TAG = 'ref';

var BIND_AUTOCHECK_TAG = 'autocheck';

var BIND_DATATYPE_TAG = 'datatype';

var BIND_VALIDATE_TAG = 'validate';

var BIND_CTRLEXT_TAG = 'ctrlext';
var BIND_CALCULATE_TAG = 'calculate';
//RequestTag class properties

var REQUEST_ID_TAG = 'id';

var REQUEST_PARA_TAG = 'para';

var REQUEST_PREREQUEST_TAG = 'prerequest';

var REQUEST_POSTREQUEST_TAG = 'postrequest';

//ResponseTag class properties

var RESPONSE_ID_TAG = 'id';

var RESPONSE_RTN_TAG = 'rtn';

var RESPONSE_PRERESPONSE_TAG = 'preResponse';

var RESPONSE_POSTRESPONSE_TAG = 'postResponse';

//InstanceTag class properties

var INSTANCE_OBJNAME_TAG = 'var';

var INSTANCE_OBJTYPE_TAG = 'type';

//ForwardTag class properties

var FORWARD_ID_TAG = 'id';

var FORWARD_TARGET_TAG = 'target';

var FORWARD_TEST_TAG = 'test';

var FORWARD_ACTION_TAG = 'action';

var FORWARD_ONFORWARD_TAG = 'onforward';

//ServiceTag class properties

var SERVICE_ID_TAG = 'id';

var SERVICE_URL_TAG = 'url';

var SERVICE_ISSYN_TAG = 'isSyn';

//add by Daniel
var SERVICE_Enctype_TAG = 'enctype';
//add by Daniel

var SERVICE_CACHE_TAG = 'cache';

var SERVICE_EXPIRES_TAG = 'expires';

var SERVICE_ONERROR_TAG = 'onError';

var SERVICE_REQUESTOBJ_TAG = 'reqeuestObj';

var SERVICE_RESPONSEOBJ_TAG = 'responseObj';

var SERVICE_FORWARDOBJ_TAG = 'forwardObj';

//InitTag class properties

var INIT_ID_TAG = 'id';

var INIT_CTRLID_TAG = 'ctrl';

var INIT_REF_TAG = 'ref';

var INIT_VALUE_TAG = 'value';
//\u6d88\u606f\u63d0\u793a
var RIA_MSG_LABLE = '_msg';
//\u53c2\u6570\u5b9e\u4f53
var _para = {};

//define loading class
var LoadingTag = function()
{
    this.id='';
    this.isSyn='';
    this.divId='';
    this.img='';
    this.delay='';
    this.width='';
    this.height='';

};

//define BindTag class

var BindTag = function ()
{

    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.autocheck = '';

    this.datatype = '';

    this.validate = '';

    this.ctrlext = '';
    this.calculate = ''; 
};

//define RequestTag class

var RequestTag = function ()
{

    this.id = '';

    this.para = '';

    this.preRequest = '';

    this.postRequest = '';
};

//define ResponseTag class

var ResponseTag = function ()
{

    this.id = "";

    this.rnt = '';

    this.preResponse = '';

    this.postResponse = '';

};

//define  InstanceTag class

var InstanceTag = function ()
{

    this.objName = '';

    this.objType = '';
};

//define ForwardTag class 

var ForwardTag = function ()
{

    this.id = '';

    this.target = '';

    this.test = '';

    this.action = '';

    this.onforward = '';

};

//define ServiceTag class

var ServiceTag = function ()
{
    this.id = '';

    this.method = '';

    this.url = '';
    
    this.isSyn = '';

    // add by Daniel begin 
    this.enctype = '' ;
    //add by Daniel end ;
     
    this.expires = '';
    
    this.cache = '';
     
    this.requestObj = '';

    this.responseObj = '';

    this.forwardObj = '';
    
    this.loadingObj ='';
    
    this.onError = "";
};

//define InitTag class 
var InitTag = function()
{
    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.value = '';

};
//define pagecontextCache class
var IncludeTag = function(){
    this.id = '';
    this.url= '';
};
//define PageContextCache class
var PageContextCache = function () {
    PageContextCache.prototype.bindArr = new Array();

    PageContextCache.prototype.instanceArr = new Array();

    PageContextCache.prototype.serviceArr = new Array();

    PageContextCache.prototype.initArr = new Array();
    PageContextCache.prototype.include = new Array();   
};

//define PageContextCache class method getService

PageContextCache.prototype.getService = function(serviceId)
{
    if (serviceId == null || serviceId == "")
        log.info("errorCode is" + RIA_ERROR_CALLSID + " error description:Error encountered when retrieving the service id,please check if there is a corresponding service id declared.");
    var serviceTags = new ServiceTag();
    for (var i = 0; i < PageContextCache.prototype.serviceArr.length; i++) {
        if (PageContextCache.prototype.serviceArr[i].id == serviceId) {
            serviceTags = PageContextCache.prototype.serviceArr[i];
        }
    }
    return serviceTags;
};

//define PageContextCache class method getBind

PageContextCache.prototype.getBind = function(ctrlid)
{
    var bindtags = new BindTag();
    for (var i = 0; i < PageContextCache.prototype.bindArr.length; i++) {
        if (PageContextCache.prototype.bindArr[i].ctrlid == ctrlid) {
            bindtags = PageContextCache.prototype.bindArr[i];
        }
    }

    return bindtags;
};

//define PageContextCache class method getInstance

PageContextCache.prototype.getInstance = function(objName)
{
    var instanceTags = new InstanceTag();
    for (var i = 0; i < PageContextCache.prototype.instanceArr.length; i++) {
        if (PageContextCache.prototype.instanceArr[i].objName == objName) {
            instanceTags = PageContextCache.prototype.instanceArr[i];
        }
    }
    return instanceTags;
};

//define PageContextCache class method getRequest

PageContextCache.prototype.getRequest = function(serviceId)
{
    var requestTags = new RequestTag();
    requestTags = PageContextCache.prototype.getService(serviceId).requestObj;
    return requestTags;
};

//define PageContextCache class method getResponse

PageContextCache.prototype.getResponse = function(serviceId)
{
    var responseTags = new ResponseTag();
    responseTags = PageContextCache.prototype.getService(serviceId).responseObj;
    return responseTags;
};
//define PageContextCache class method getForward

PageContextCache.prototype.getForward = function(serviceId)
{
    var forwardTags = new Array();
    forwardTags = PageContextCache.prototype.getService(serviceId).forwardObj;
    return forwardTags;
};
//define PageContextCache class method getloading
PageContextCache.prototype.getLoading = function(serviceId)
{
    var loadingTags = new Array();
    loadingTags = PageContextCache.prototype.getService(serviceId).loadingObj;
    return loadingTags;
};
//define pageContextCache class menthod getUrl
PageContextCache.prototype.getUrl = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceUrl = serviceTag.url;
    return serviceUrl;
};

//define pageContextCache class menthod getIsSyn
PageContextCache.prototype.getIsSyn = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceIsSyn = serviceTag.isSyn;
    
    //\u8fd4\u56de\u662f\u5426\u5f02\u6b65
    return serviceIsSyn;
};

//\u83b7\u53d6\u670d\u52a1\u6807\u7b7e\u7c7b\u578b
PageContextCache.prototype.getEnctype = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceEnctype = serviceTag.enctype;
    return serviceEnctype;
};

var newinputEvent = function(input) { 
   return function test(){
    input.value = document.getElementById(input.ctrlid).value;
    //add by tianyingquan 2008-8-28
    input.id = input.ctrlid;
    var verifybl = verifyInput(input,input.datatype);
    var ria_div_obj = document.getElementById(input.ctrlid+RIA_MSG_LABLE);
    if(verifybl &&ria_div_obj && ria_div_obj!=null && ria_div_obj!="") {
      var intext = ria_div_obj.innerText;
      if(intext!=""||intext!=null)  ria_div_obj.innerText ="";
    }
    //end 
   }
};
var getBindObjects = function(){
    var binds = getTagValue(RIA_BIND_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < binds.length; i++) {       
        //2008-6-27 modify by tianyingquan
        //var singleBinds = binds[i];     
        //for(var j =0;j<singleBinds.childNodes.length;j++){
        //if(singleBinds.childNodes[j].tagName == RIA_BIND_TAG){
        //var tmp1 = encapsBindTag(singleBinds.childNodes[j]);
        var tmp1 = encapsBindTag(binds[i])
        if (tmp1.autocheck || tmp1.autocheck == "true")
        {
            //datatype \u4e0d\u80fd\u4e3a\u7a7a
            if (tmp1.datatype != null && tmp1.datatype != "" && document.getElementById(tmp1.ctrlid)) {
                var input = {};
                input.datatype = tmp1.datatype;
                input.name = document.getElementById(tmp1.ctrlid).name;
                input.ctrlid =tmp1.ctrlid;
                if(isIEorFF()){
                  document.getElementById(tmp1.ctrlid).attachEvent("onblur",newinputEvent(input));
                }else{
                 document.getElementById(tmp1.ctrlid).addEventListener("blur",newinputEvent(input),false);
                }
            }

        }
          pageContextCache.bindArr.push(tmp1);
     //   }
     //  }
     //end
    }
};
var pageContextCache = new PageContextCache();
//\u5b9a\u4e49\u91cd\u590d\u63d0\u4ea4\u6309\u94ae\u7684\u7f13\u5b58
var cacheMultiUrl = new Array();
var riatags_init = function () {
    //-------includeTag------------------------
    var include = getTagValue(RIA_INCLUDE_TAG,"");
        for(var i = 0;i<include.length;i++){
           pageContextCache.include.push(encapsIncludeTag(include[i],i));
        }   
    //------bindTag----------------------------------
    //var bindtag = new BindTag();
    getBindObjects();
    
//-------instanceTag----------------------------------------
    var instances = getTagValue(RIA_INSTANCE_TAG, RIA_S_TAG);
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < instances.length; i++) {
        pageContextCache.instanceArr.push(encapsInstanceTag(instances[i]));
    }

    
//---------ServiceTag---------------------------------------
    var services = getTagValue(RIA_SERVICE_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < services.length; i++) {
        pageContextCache.serviceArr.push(encapsServiceTag(services[i]));
    }

//-------initTag----------------------------

    //var initTag = new InitTag();
    var inits = getTagValue(RIA_SETVALUE_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < inits.length; i++) {
        pageContextCache.initArr.push(encapsInitTag(inits[i]));
    }
    
    //_para\u5bf9\u8c61
    creatParaBean();
    
    //\u521d\u59cb\u5316init\u6807\u7b7e
    setValues();

    return pageContextCache;
};

var encapsBindTag = function (node) {
    var newbindtag = new BindTag();
    newbindtag.id = node.getAttribute(BIND_ID_TAG);
    newbindtag.ctrlid = node.getAttribute(BIND_CTRL_TAG);
    newbindtag.ref = node.getAttribute(BIND_REF_TAG);
    newbindtag.autocheck = node.getAttribute(BIND_AUTOCHECK_TAG);
    newbindtag.datatype = node.getAttribute(BIND_DATATYPE_TAG);
    newbindtag.validate = node.getAttribute(BIND_VALIDATE_TAG);
    newbindtag.ctrlext = node.getAttribute(BIND_CTRLEXT_TAG);
    newbindtag.calculate = node.getAttribute(BIND_CALCULATE_TAG);
    return newbindtag;
};

var encapsInstanceTag = function (node) {
    var newinstanceTag = new InstanceTag();
    newinstanceTag.objName = node.getAttribute(INSTANCE_OBJNAME_TAG);
    newinstanceTag.objType = node.getAttribute(INSTANCE_OBJTYPE_TAG);
    return newinstanceTag;
};

var encapsInitTag = function (node)
{
    var newinitTag = new InitTag();
    newinitTag.id = node.getAttribute(INIT_ID_TAG);
    newinitTag.ctrlid = node.getAttribute(INIT_CTRLID_TAG);
    newinitTag.ref = node.getAttribute(INIT_REF_TAG);
    newinitTag.value = node.getAttribute(INIT_VALUE_TAG);
    return newinitTag;
};
//\u5c01\u88c5include tag
var encapsIncludeTag = function (node,count){
   try{
       var newincludeTag = new IncludeTag();
       newincludeTag.id  = node.getAttribute(INCLUDE_ID_TAG);
       newincludeTag.url = node.getAttribute(INCLUDE_URL_TAG);
       if(newincludeTag.url!=null ||newincludeTag.url!="") {createDivTag(node,newincludeTag.url,count);}
       /*
       document.body.onunload =function(){
          cleanDivObject(count);
       };*/
       return newincludeTag;
   }
   catch(e){
      alert('error information desc==='+e.description);
   }
};
//\u83b7\u53d6html\u6587\u4ef6\u7684\u5b57\u7b26\u4e32
var getHtmlString = function (URL){
    var sync = true;
    var xmlhttp_include = new FactoryXMLHttpRequest();
    xmlhttp_include.open("GET",URL, !sync);
    var tempstring;
    function includeHtmlBack(){
        if(xmlhttp_include.readyState == 4){
              if(xmlhttp_include.status == 200){ 
                 var xmlString = xmlhttp_include.responseText;
                 xmlhttp_include = null;
                 tempstring = xmlString;
              }               
          }                 
        };  
        xmlhttp_include.send(null);
        if(!sync)xmlhttp_include.onreadystatechange = includeHtmlBack;
        else includeHtmlBack();
        if(tempstring) return tempstring;
};
//\u521b\u5efadiv\u5bf9\u8c61
var createDivTag = function (node,url,count){
     var newdiv = document.createElement("div");
     newdiv.id="divid"+count;
     var stringhtml = getHtmlString(url);
     newdiv.innerHTML =stringhtml;
     $(node).after(newdiv);
     //document.body.appendChild(newdiv);
     //newdiv.style.visibility = "hidden";
};
//\u6e05\u9664div\u5bf9\u8c61
var cleanDivObject = function (count){
    var tempdivid = 'divid'+count;
    if(document.getElementById(tempdivid)){ 
    var divobj = document.getElementById(tempdivid);
    if(divobj&&(divobj!=null||divobj!="")){document.body.removeChild(divobj);}
    if(divobj){
      alert(" div object is exit");
    }
 }
};

var encapsServiceTag = function (node)
{
    var newserviceTag = new ServiceTag();
    newserviceTag.id = node.getAttribute(SERVICE_ID_TAG);
    newserviceTag.url = node.getAttribute(SERVICE_URL_TAG);
    newserviceTag.isSyn = node.getAttribute(SERVICE_ISSYN_TAG);

    //add by Daniel begin
    newserviceTag.enctype = node.getAttribute(SERVICE_Enctype_TAG);
    //add by Daniel end

    newserviceTag.onError = node.getAttribute(SERVICE_ONERROR_TAG);
    //\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
    newserviceTag.cache = node.getAttribute(SERVICE_CACHE_TAG);
    newserviceTag.expires = node.getAttribute(SERVICE_EXPIRES_TAG);
    //\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f 
    if (isIEorFF())
    {
        var forwards = new Array();
        for (var i = 0; i < node.childNodes.length; i++) {
            //request\u5bf9\u8c61
            if (node.childNodes[i].tagName == RIA_REQUEST_TAG)
            {
                newserviceTag.requestObj = encapsRequestTag(node.childNodes[i]);
            }
            //response\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_RESPONSE_TAG)
            {
                newserviceTag.responseObj = encapsResponseTag(node.childNodes[i]);
            }
            //forward\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_FORWARD_TAG)
            {
                forwards.push(encapsForwardTag(node.childNodes[i]));
            }
            //loading\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_LOADING_TAG)
            {
                newserviceTag.loadingObj=encapsLoadingTag(node.childNodes[i]);
            }
        }
        newserviceTag.forwardObj = forwards;
    }

    else {
        var forwords = new Array();
        while (true) {
            //\u5224\u65ad\u662f\u5426\u5b58\u5728\u5bf9\u8c61
            if (node.childNodes[1] == undefined) {
                break;
            } else {
                node = node.childNodes[1];
                
                //request\u5bf9\u8c61
                if (node.tagName == RIA_ZREQUEST_TAG)
                {
                    newserviceTag.requestObj = encapsRequestTag(node);
                }
                //response\u5bf9\u8c61
                else if (node.tagName == RIA_ZRESPONSE_TAG)
                {
                    newserviceTag.responseObj = encapsResponseTag(node);
                }
                //forward\u5bf9\u8c61
                else if (node.tagName == RIA_ZFORWARD_TAG)
                {
                    forwords.push(encapsForwardTag(node));
                }
                //loading\u5bf9\u8c61
                else if (node.tagName == RIA_ZLOADING_TAG)
                {
                    newserviceTag.loadingObj=encapsLoadingTag(node);
                }
            }
        }
        newserviceTag.forwardObj = forwords;
    }

    return newserviceTag;
};

var encapsRequestTag = function (node) {
    var newrequestTag = new RequestTag();
    newrequestTag.id = node.getAttribute(REQUEST_ID_TAG);
    newrequestTag.para = node.getAttribute(REQUEST_PARA_TAG);
    newrequestTag.preRequest = node.getAttribute(REQUEST_PREREQUEST_TAG);
    newrequestTag.postRequest = node.getAttribute(REQUEST_POSTREQUEST_TAG);

    return newrequestTag;
};
var encapsLoadingTag = function (node){
    var newloadingTag = new LoadingTag();
    newloadingTag.id = node.getAttribute(LOADING_ID_TAG);
    newloadingTag.isSyn = node.getAttribute(LOADING_ISSYN_TAG);
    newloadingTag.img = node.getAttribute(LOADING_IMG_TAG);
    newloadingTag.delay = node.getAttribute(LOADING_DELAY_TAG);
    newloadingTag.divId = node.getAttribute(LOADING_DIVID_TAG);
    newloadingTag.height = node.getAttribute(LOADING_HEIGHT_TAG);
    newloadingTag.width = node.getAttribute(LOADING_WIDTH_TAG);
    return newloadingTag;
};

var encapsResponseTag = function (node) {
    var newresponseTag = new ResponseTag();
    newresponseTag.id = node.getAttribute(RESPONSE_ID_TAG);
    newresponseTag.rtn = node.getAttribute(RESPONSE_RTN_TAG);
    newresponseTag.preResponse = node.getAttribute(RESPONSE_PRERESPONSE_TAG);
    newresponseTag.postResponse = node.getAttribute(RESPONSE_POSTRESPONSE_TAG);

    return newresponseTag;
};

var encapsForwardTag = function (node) {
    var newforwardTag = new ForwardTag();
    newforwardTag.id = node.getAttribute(FORWARD_ID_TAG);
    newforwardTag.target = node.getAttribute(FORWARD_TARGET_TAG);
    newforwardTag.test = node.getAttribute(FORWARD_TEST_TAG);
    newforwardTag.action = node.getAttribute(FORWARD_ACTION_TAG);
    newforwardTag.onforward = node.getAttribute(FORWARD_ONFORWARD_TAG);

    return newforwardTag;
};

var getTagValue = function (node, str) {
    try {
        if (isIEorFF())
        {
            return getIETagValue(node + str);
        }
        else {
            return getFFTagVlaue(RIA_Z_TAG + node);
        }
    }
    catch(e) {
        var tempException = new RIAException("",'different oper defined ctrl exception',e.description);
        commonException("ria",tempException);    
        //log.info("errorcode is:" + RIA_ERROR_SUPPORT_BROWSER + "errordescription is:" + e.message);
    }
};

var getIETagValue = function (node) {

    if (document.getElementsByTagName(node)[0] == undefined)
    {
        return new Array();
    }
    var ieNode = document.getElementsByTagName(node)[0].children;
    if (node == RIA_SERVICE_TAG || node == RIA_SETVALUE_TAG ||node ==RIA_INCLUDE_TAG||node == RIA_BIND_TAG) {
        ieNode = document.getElementsByTagName(node);
    }
    return ieNode;
};

var getFFTagVlaue = function (node) {

    var length = document.getElementsByTagName(node).length;
    var foxNode = new Array(length);
    for (var i = 0; i < length; i++) {
        foxNode[i] = document.getElementsByTagName(node).item(i);
    }
    return foxNode;
};

var isIEorFF = function () {
    if (navigator.appName == "Microsoft Internet Explorer")
    {
        return true;
    }
    return false;
};

var setValues = function ()
{
    var initArr = pageContextCache.initArr;

    var initArr_index = initArr.length;
    if (initArr_index > 0)
    {
        for (var i = 0; i < initArr_index; i++)
        {
            var initTag = initArr[i];
            var init_ctrlid = initTag.ctrlid;
            var init_value = initTag.value;
            var init_ref = initTag.ref;
            if (init_ctrlid == null || init_ctrlid == undefined || init_ctrlid == "")
            {
                continue;
            }
            if (init_value)
            {
               BindDataToCtrl(init_ctrlid, init_value);
               // document.getElementById(init_ctrlid).value = init_value;
            }
            else if (init_ref)
            {
                var init_ref_index = init_ref.indexOf(RIA_DOT_TAG);
                var para_value = _para[init_ref.substring(init_ref_index + 1)];
                //document.getElementById(init_ctrlid).value = para_value == undefined ? "" : para_value;
                para_value = para_value == undefined ? "" : para_value;
                BindDataToCtrl(init_ctrlid, para_value);
            }
        }
    }
};

var getRequestParaUrl = function ()
{
    //\u83b7\u53d6url
    var url = decodeURI(document.URL);
    return url;

};

var paraValue = function ()
{
    var str = new String();
    str = getRequestParaUrl();
    var i = 0;
    if (str != "")
    {
        i = str.indexOf("?");
        i = i + 1;
        str = str.substring(i, str.length);
        var arr = str.split("&");
        return arr;
    }
};

var creatParaBean = function ()
{
    try {
        var value = paraValue();
        if (value != "")
        {
            for (var i = 0; i < value.length; i++)
            {
                var name = value[i].split("=");
                _para[name[0]] = name[1];
            }
        }
    } catch(e)
    {
          var tempException = new RIAException("",'instance ctrl exception',e.description);
          commonException("ria",tempException);
        //log.info("errorcode is " + RIA_ERROR_ENCAPSULATION_URL + "errordescription is" + e.message);
    }
};
//\u521d\u59cb\u5316\u6240\u6709\u6570\u636e
try{
  addOnloadEvent(riatags_init);
}catch(e){
     var tempException = new RIAException("",'Ria instance ctrl exception',e.description);
     commonException("ria",tempException);
}


var FactoryXMLHttpRequest = function () {
    if( window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else if( window.ActiveXObject) {
        var msxmls = new Array(
            'Msxml2.XMLHTTP.5.0',
            'Msxml2.XMLHTTP.4.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP');
        for (var i = 0; i < msxmls.length; i++) {
            try {
                return new ActiveXObject(msxmls[i]);
            } catch (e) {
            // modify by Daniel begin
            //   var tempException = new RIAException("",constantRia.RIA_HTTP_INIT,e.message);
            //   commonException(constantRia.RIA_HTTP,tempException);            
            // modify by Daniel end

            }
        }
    }
    throw new Error('Could not instantiate factory');
};
var OrganizeData = function(inParas) {
    this.inParas = inParas;
    this.pageContext = pageContextCache;
    //this.processorFactory = new ProcessorFactory();
};
OrganizeData.prototype.buildData = function(sid,$M)
{
    var jsonvalue = this.parseParas(sid,$M);
    if(jsonvalue === false) return false;
    var requestBySid = this.pageContext.getRequest(sid);
    var preRequestMethod = requestBySid.preRequest; 
    //\u4e8b\u4ef6\u8bf7\u6c42\u524d\u65b9\u6cd5
    if(requestBySid && preRequestMethod && preRequestMethod.trim())
    {
        var requestevent = parseValueById(preRequestMethod);
        evalEvent($M,requestevent);
    } 
    jsonvalue = JSON.stringify(jsonvalue);
    return jsonvalue;
};

OrganizeData.prototype.getInfoByCtrlId = function(ctrlId)
{

};
function isIncludeSymbol(requestPara,organizeData,$M)
{
    var inparasValue;
    var group;
    var typePara = typeof(requestPara);
   if(typePara =="string")
    {
        if (requestPara.indexOf("[") == -1 && requestPara.indexOf("]") == -1)
        {
            inparasValue = "[" + requestPara + "]";
        }
        if (requestPara.indexOf("[") != -1 && requestPara.indexOf("]") != -1)
        {
            inparasValue = requestPara;
        }
        group = organizeData.splitParaValue(inparasValue, organizeData, group,$M);  
        if(group === false) return false;
        return group;       
    }
    /*else if(requestPara instanceof Array)
    {
        var temp = new Array();
        for(var i=0;i<requestPara.length;i++){
          var requestM = requestPara[i];
          if(typeof(requestM)=='string'&&requestM.indexOf("$M.")!=-1) {
              var tempobjpara = organizeData.splitParaValue(requestM,organizeData,group,$M);
              if(tempobjpara === false) return false;
              requestPara[i] =tempobjpara[0] ;
          }
        }
        temp[0] = requestPara;
        return temp;    
    }*/
    else{
        var objArr = new Array();
        objArr[0] = requestPara;
        group = objArr;
        return group;       
    }
};

OrganizeData.prototype.parseParas = function(sid,$M)
{
    var group;
    var organizeData = this;
    if (organizeData.inParas != null && organizeData.inParas != "")
    {       
        group = isIncludeSymbol(organizeData.inParas,organizeData,$M);
        if (group === false) return false;
        return group;
    }
    else{
        var requestBySid = organizeData.pageContext.getRequest(sid);
        //\u83b7\u53d6request\u6807\u7b7e
        if (requestBySid != null && requestBySid != "" && (this.inParas == null || this.inParas == ""))
        {
            var requestPara = requestBySid.para;
            //\u83b7\u53d6request\u6807\u7b7e\u4e0b\u9762\u7684para\u53c2\u6570
            if (requestPara != null && requestPara != "")
            {
              group = isIncludeSymbol(requestPara,organizeData,$M);
              if(group === false) return false;
              return group;
            }else
            {
                return new Array();
            }
            
        }else
        {
            return new Array();
        }
         
    }
   
};

OrganizeData.prototype.invokeFunction = function(elementId)
{
    var result = {};
    try {
        result = GetDataFromCtrl(elementId);
    } catch(e)
    {
       var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
       commonException(constantRia.RIA_RIA,tempException);          
      return;
    }
    return result;
};
OrganizeData.prototype.baseTypeValue = function (requestPara){
    var arrobj = new Array();
    arrobj[0] = requestPara;
    para =  arrobj;
    return para;
};

OrganizeData.prototype.splitParaValue = function(requestPara, organizeData, group,$M)
{
    if(requestPara == null)
    {
        return new Array();
    }
    var group = new Object();
    var splitPara;
    var para;
    //call url\u7684\u65f6\u5019\u83b7\u53d6\u53c2\u6570\u7684\u7c7b\u578b\uff0c$m.obj.name 
    //"number," "string," "boolean," "object," "function," \u548c "undefined."
    var typePara = typeof(requestPara); 
    switch(typePara)
    {
        case "string" :
            var subPara; 
            if(requestPara.indexOf("[")!=-1&&requestPara.indexOf("]")!=-1)
            {
                var paraStart = requestPara.indexOf("[");
                var paraEnd = requestPara.indexOf("]");
                subPara = requestPara.substring(paraStart + 1, paraEnd);
                splitPara = subPara.split(",");                 
            }
            else
            {
                splitPara = requestPara.split(",");
            }
            break;
        case "object" ://\u5982\u679c\u662f\u6570\u7ec4\u5f62\u5f0f\u7684\u4e5f\u5f53\u4f5cobject\u6765\u5904\u7406
            return this.baseTypeValue(requestPara);
        case "number":
            return this.baseTypeValue(requestPara);
        case "boolean"://false\u30010\u3001null\u3001 NaN
            return this.baseTypeValue(requestPara);
        case "undefined":
            return new Array();                 
    }   
    para = new Array(splitPara.length);
    // modify by tianyingquan
    ///var stortBindValue = "";
    ///var f = 0;
    //end 
    //\u5206\u79bb\u591a\u4e2a\u53c2\u6570\u7684\u5f62\u5f0f 
    for (var i = 0; i < splitPara.length; i++)
    {
        var singlePara = splitPara[i];
        var j = singlePara.indexOf("$");
        //\u5982\u679c\u5e26\u6709$\u7b26\u53f7
         //\u5904\u7406\u5bf9\u8c61\u6a21\u578b\uff0c\u5982 $M.obj
        if (j != -1)
        {
            //\u5982\u679c\u6709\u70b9\u53f7
            if (singlePara.indexOf(".") != -1)
            {
                var requestModel = singlePara.substring(singlePara.indexOf(".") + 1, singlePara.length);
                if (requestModel != null && requestModel != "")
                {
                    var requestModelArr = requestModel.split(".");
                    var modelArrLength = requestModelArr.length;
                    requestModelArr[modelArrLength - 1] = new Object();
                    group = requestModelArr[modelArrLength - 1];
                    var bindObject = new Object();
                    //\u5faa\u73afbinds\u6807\u7b7e\u91cc\u9762\u7684ref\u5c5e\u6027\uff0c\u53d6\u51fa\u4e0erequestModel\u503c\u76f8\u540c\u7684\u503c                              
                    var bindTagArr = organizeData.pageContext.bindArr;
                    var d = 0;
                    //var arrAllBindRef = [];
                    //var arrAllBindCtrl = [];
                    //\u5c01\u88c5\u6240\u6709\u5b58\u5728requestModel \u4e2d\u7684bind\u6807\u7b7e
                    var bindTempArr = [];
                    for (var k = 0; k < bindTagArr.length; k++)
                    {
                        var bindRef = bindTagArr[k].ref;
                        var bindCtrl = bindTagArr[k].ctrlid;
                        var bindId = bindTagArr[k].id;
                        var bindAutoCheck = bindTagArr[k].autocheck;
                        var bindDatatype = bindTagArr[k].datatype;
                        var bindValidate = bindTagArr[k].validate;
                        var bindCtrlext = bindTagArr[k].ctrlext;
                        var regexp = new RegExp("^" + requestModel + "[.]");
                        if (regexp.test(bindRef) || bindRef == requestModel)
                        {
                            //1.\u628a\u5728modelRequest\u91cc\u9762\u7684\u503c\u53d6\u51fa\u6765\u653e\u5728\u4e00\u4e2a\u6570\u7ec4\u91cc\u9762
                            //arrAllBindRef[d] = bindRef.replace(requestModel+".","");
                            //arrAllBindCtrl[d] = bindCtrl;
                            var newBind = new BindTag();
                            newBind.ref = bindRef.replace(requestModel + ".", "");
                            newBind.ctrlid = bindCtrl;
                            newBind.datatype = bindDatatype;
                            newBind.autocheck = bindAutoCheck;
                            newBind.ctrlext = bindCtrlext;
                            newBind.id = bindId;
                            newBind.validate = bindValidate;
                            bindTempArr[d] = newBind;
                            d++;
                        }
                    }
                    //2.\u5728\u53d6\u51fa\u6570\u7ec4\u91cc\u9762\u67d0\u4e2a\u5177\u4f53\u7684\u503c\u8fdb\u884c\u5904\u7406
                    for (var q = 0; q < bindTempArr.length; q++)
                    {
                        var singleBindRefArr = (bindTempArr[q].ref).split(".");
                        var singleBindRefCtrl = bindTempArr[q].ctrlid;
                        if (singleBindRefCtrl != null && singleBindRefCtrl != "") {
                            if (singleBindRefArr.length >= 1 && bindTempArr[q].ref == requestModel)
                            {
                                var ctrlResult = "";
                                //ui\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728
                                if(document.getElementById(singleBindRefCtrl) != null) ctrlResult =this.invokeFunction(singleBindRefCtrl);
                                //\u5b9a\u4e49\u6587\u672c\u6846\u7684\u540d\u79f0                             
                                var bind_type = bindTempArr[q].datatype;
                                var setValue_obj = document.getElementById(singleBindRefCtrl+RIA_MSG_LABLE);                              
                                //\u5148\u6267\u884c\u6846\u67b6\u7684\u7c7b\u578b
                                if(bind_type!= null && bind_type!= ""&&document.getElementById(singleBindRefCtrl) != null&&bind_type!="NullString")
                                {
                                    var inputname = document.getElementById(singleBindRefCtrl).name;
                                    var ble = validateDataType(ctrlResult, bindTempArr[q], inputname,singleBindRefCtrl); 
                                    if (!ble)
                                    {
                                        try{
                                           document.getElementById(singleBindRefCtrl).focus();
                                           return false; 
                                        }catch(e){
                                           return false;
                                        }                                                                          
                                    }
                                    //add by tianyingquan2008-8-28 RIA_MSG_LABLE
                                    else if(setValue_obj && setValue_obj!=null && setValue_obj!=""){
                                       var innervalue = setValue_obj.innerText;
                                       if(innervalue!="" || innervalue!= null) setValue_obj.innerText ="";
                                    }                                     
                                    //end                                   
                                }
                                var validatefunction = bindTempArr[q].validate;
                                //\u540e\u6267\u884c\u7528\u6237\u81ea\u5df1\u7684\u6821\u9a8c\u51fd\u6570
                                if (validatefunction != null && validatefunction != "") {
                                   var blvli = eval(validatefunction);
                                   if(!blvli) return false; 
                                }                               
                                if(bind_type!=null && bind_type.indexOf("date")!=-1) bindObject = riaDate2UTC(bind_type,ctrlResult); 
                                else bindObject = ctrlResult;
                                break;
                            }
                           var rtnValue= this.getObject(bindObject, singleBindRefArr, 1, singleBindRefCtrl, bindTempArr[q]);
                           if(rtnValue === false) return false;
                        }
                    }
                    $M[requestModel] = bindObject;
                    para[i] = group = bindObject;
                }
            }
        }
     else 
        {
            para[i] = splitEvalFunction(singlePara);
        }
    }
    return     para;
};
function splitEvalFunction(singlePara){
    var evalparatype = typeof(singlePara);  
    if(evalparatype =='string')
    {
        var nn = new RegExp("\^[0-9]");
        if(nn.test(singlePara))
        {
            return singlePara;
        }else{
            try{
                var evalPara = eval(singlePara);
                if(evalPara == undefined)
                {
                    return singlePara;
                }               
                else
                {
                    return evalPara;
                }
            }
            catch(e){
                return singlePara;
            }
        }       
    }
     else
    {
        var evalPara = eval(singlePara);                
        return evalPara;
    }
};
var validateDataType = function (ctrlResult, bindTemp, inputname,ctrlid)
{
    var datatype = bindTemp.datatype;
    var input = {};
    input.datatype = datatype;
    input.value = ctrlResult;
    input.name = inputname;
    input.id = ctrlid;
    return verifyInput(input,datatype);
};
function validateTypeOfDate(str)
{
  //var regexp = new RegExp(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])$/);
   var regexp = new RegExp(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])(\s?([0-1]?[0-9]|2[0-3]):([0-5]?[0-9]):([0-5]?[0-9]))?$/);
  if(regexp.test(str))
  {
      return true;
  }
  else
  {
      return false;
  } 
};
var  riaDate2UTC = function(format, time){
    var format = format.substring(6,format.length-2);
    if(time == null || time == '')
    {
        return null;
    }
    var utc = "";
    var yy = "-";
    var mm = "-";
    var date_format = new Array();
    var date_format1 = "";
    var date_format2 = "";
    var time_arr = new Array();
    var time1 = time;
    var time2 = "";
    if(format != null)
    {
        if(format.length > 10)
        {
            date_format = format.split(" ");
            date_format1 = date_format[0];
            date_format2 = date_format[1];
            
            time_arr = time.split(" ");
            time1 = time_arr[0];
            time2 = time_arr[1];
        }else{
            date_format1 = format;
            time1 = time;
        }
        var y_index = date_format1.indexOf("y");
        var y_endindex = date_format1.lastIndexOf("y");
        yy = date_format1.substring(y_endindex+1,y_endindex+2); 

        var m_index = date_format1.indexOf("m");
        var m_endindex = date_format1.lastIndexOf("m");
        mm = date_format1.substring(m_endindex+1, m_endindex+2);
    }
    var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
    var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);

    time1 = time1.replace(YM,',');
    time1 = time1.replace(MD,',');
    var tt = time1.split(",");
  
    if(time2.length < 1){
        var date = new Date(tt[0],parseInt(tt[1]-1),tt[2]);
        utc = date.getTime();
    }else{
        var HH = time2.split(":");
         var date = new Date(tt[0],parseInt(tt[1]-1),tt[2],HH[0],HH[1],HH[2]);
         utc = date.getTime();
    }
    return utc;
  };
OrganizeData.prototype.getObject = function(obj, propertyPath, level, ctrlid, bindtemp)
{
    if (level == propertyPath.length)
    {
        var ctrlResult ="";
        //\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728\u7684\u60c5\u51b5
        if(document.getElementById(ctrlid)!=null) ctrlResult=this.invokeFunction(ctrlid);
        
        if(typeof(ctrlResult)=='string')
                ctrlResult=ctrlResult.trim();  
        var bind_type = bindtemp.datatype;
        var div_obj = document.getElementById(ctrlid+RIA_MSG_LABLE);                    
        //\u5148\u6267\u884c\u6846\u67b6\u7684\u6821\u9a8c\u7c7b\u578b
        if(bindtemp&&bind_type != null && bind_type != ""&& document.getElementById(ctrlid)!=null&&bind_type!="NullString")
        {
            var inputname = document.getElementById(ctrlid).name;           
            var bl = validateDataType(ctrlResult, bindtemp, inputname,ctrlid);
            if(!bl) 
            {   
                try{
                    document.getElementById(ctrlid).focus();
                    return false;               
                }catch(e){
                    return false;
                }
            }
            //add by tianyingquan 2008-8-28 RIA_MSG_LABLE
            else if(div_obj&& div_obj!=null && div_obj!=""){
               var ctrlE =div_obj.innerText; 
               if(ctrlE!=""||ctrlE!=null) div_obj.innerText ="";
            }       
            //end
            
            if(ctrlResult==null||ctrlResult=="")
            {
                obj[propertyPath[level - 1]] = null;
                return;
            }
            
            if(validateTypeOfDate(ctrlResult))
            {             
               //var year = parseInt(ctrlResult.substring(0,4));
               //var month = parseInt(ctrlResult.substring(5,7));
               //var day = parseInt(ctrlResult.substring(8,10));               
               //obj[propertyPath[level - 1]] = new Date(year,month,day).getTime();
               obj[propertyPath[level - 1]] = riaDate2UTC(bindtemp.datatype,ctrlResult);
               return ;
            }           
        }
        //\u540e\u6267\u884c\u7528\u6237\u81ea\u5b9a\u4e49\u7684\u6821\u9a8c\u51fd\u6570
        if (bindtemp&&bindtemp.validate != null && bindtemp.validate != "") {
          var vabl = eval(bindtemp.validate);
          if(!vabl) return false;
        }       
        obj[propertyPath[level - 1]] = ctrlResult;
        return;
    }
    if (!obj[propertyPath[level - 1]])
    {
        obj[propertyPath[level - 1]] = {};
    }
    var obj_bl = this.getObject(obj[propertyPath[level - 1]], propertyPath, level + 1, ctrlid, bindtemp);
    if(obj_bl === false) return false;
};


var CallService = function(sid, outParas, autoPara, obj, fun,mm,cacheurl,urlErrorEvent) {
    this._xmlhttp = new FactoryXMLHttpRequest();
    this.username = null;
    this.password = null;
    this.sid = sid;
    this.outParas = outParas;
    this.autoPara = autoPara;
    this.pageContext = pageContextCache;
    this._object = obj;
    this.onSuccess = fun;
    this.mm = mm;
    this.cacheurl = cacheurl;
    this.urlErrorEvent = urlErrorEvent;
};
var processBarHidden = function(serviceId,instance)
{
   if(instance._xmlhttp.readyState == 4){
     //tianyingquan add exception 2008-5-23
     httpExceptionCode(instance._xmlhttp.status); 
     //end 
     if(instance._xmlhttp.status == 12029||instance._xmlhttp.status == 408)
    {   
        if(serviceId)
        {
            var loadingobj = pageContextCache.getLoading(serviceId);
            var issyn = loadingobj.isSyn;
            var divid = loadingobj.divId;
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                hiddendDivImg(divid);
                return;
            }else if(issyn==true||issyn=="true")
            {
               removeObj();          
               return;
            }         
        }
    } 
     }
};

var CallService_call = function(action, url, data,isSyn) {
    var instance = this;
    var hiddensid = this.sid;
    if(isSyn && (isSyn=='true'||isSyn == true))
    {
        //\u540c\u6b65
        isSyn = false;
    }
    else
    {
        //\u5f02\u6b65
        isSyn = true;
    }
    this._xmlhttp.open(action, url, isSyn);
    this.openCallback(this._xmlhttp);
    this._xmlhttp.onreadystatechange = callbackallbrowser;
    function callbackallbrowser() {
    //\u670d\u52a1\u8d85\u65f6\u8bbe\u7f6e\u56fe\u7247\u9690\u85cf
    processBarHidden(hiddensid,instance);
        switch (instance._xmlhttp.readyState) {
            case 1:
                instance.loading();
                break;
            case 2:
                instance.loaded();
                break;
            case 3:
                instance.interactive();
                break;
            case 4:
                if (instance.complete) {
                    try {
                        instance.complete(instance._xmlhttp.status, instance._xmlhttp.statusText,
                                instance._xmlhttp.responseText, instance._xmlhttp.responseXML);
                        instance._xmlhttp.abort();
                        instance._xmlhttp = null ;        
                    }
                    catch (e) {
                      var tempException = new RIAException("",constantRia.RIA_EXCECOMPLETE,e.description);
                      commonException(constantRia.RIA_RIA,tempException);                   
                    }
                }
                break;
        }
    };
    try {
        this._xmlhttp.send(data);
        if(this._xmlhttp.onreadystatechange == null)
            callbackallbrowser();
            //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
       var serviceRequest = pageContextCache.getRequest(this.sid);
       //\u8bf7\u6c42\u76f8\u5e94\u540e\u4e8b\u4ef6
       if(serviceRequest && serviceRequest.postRequest&&serviceRequest.postRequest.trim())
       {
            var requesteventafter = parseValueById(serviceRequest.postRequest);
            evalEvent(this.mm,requesteventafter);
       }
       
    }
    catch(e) {
           var tempException = new RIAException("",e.number,e.description);
           commonException(constantRia.RIA_RIA,tempException);    
    }
};

var CallService_get = function(url) {
    this.call("GET", url, null);
};

var CallService_put = function(url, mimetype, datalength, data) {
    this.openCallback = function(xmlhttp) {
        xmlhttp.setRequestHeader("Content-type", mimetype);
        xmlhttp.setRequestHeader("Content-Length", datalength);
    };
    this.call("PUT", url, data);
};

var CallService_delete = function (url) {
    this.call("DELETE", url, null);
};

var CallService_post = function(url, mimetype, datalength, data,isSyn) {
    var thisReference = this;
    this.userOpenCallback = this.openCallback;
    this.openCallback = function(xmlhttp) {
        //xmlhttp.setRequestHeader( "Content-type", mimetype);
        //xmlhttp.setRequestHeader( "Content-Length", datalength);
        thisReference.userOpenCallback(xmlhttp);
        thisReference.openCallback = thisReference.userOpenCallback;
    };
    this.call("POST", url, data,isSyn);
};

var CallService_openCallback = function (obj) {
};
var CallService_loading = function () {
};
var CallService_loaded = function () {
};
var CallService_interactive = function () {
};

var SHOWDATA_OPRST_TAG = "opRst";
var SHOWDATA_RTN_TAG = "rtn";
var SHOWDATA_EXPMODEL_TAG = "expModel";
var SHOWDATA_DOT_TAG = ".";
var ERR_ERRORCODE ="excepCode";
var ERR_ERRORDESC = "excepDesc";
var ERR_LOCATIONID = "locationId";

var CODE_ERR_CONSOLE = "4";
var RIA_NOLOGIN_OPRST = "6";
var RIA_NOPOPE_OPRST = "7";
var CODE_ERR_SIZELIMITEXCEEDED="8";
 
String.prototype.trim = function()
{
    // \u7528\u6b63\u5219\u8868\u8fbe\u5f0f\u5c06\u524d\u540e\u7a7a\u683c
    // \u7528\u7a7a\u5b57\u7b26\u4e32\u66ff\u4ee3\u3002
    return this.replace(/(^\s*)|(\s*$)/g, "");
};
//\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
//\u5904\u7406\u591a\u6b21\u63d0\u4ea4\u7f13\u5b58
function addCacheUrl(url)
{   
   var cacheUrl ="";
   //\u904d\u5386\u83b7\u53d6\u6240\u6709\u7684\u7f13\u5b58url 
    for(cacheUrl in cacheMultiUrl)
    {   
        //\u5982\u679c\u5728\u7f13\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728\u4e86
        if(url == cacheUrl)
        {
            return false;
        }                               
    }
    //\u5728\u7f13\u5b58\u4e2d\u4e0d\u5b58\u5728\u6dfb\u52a0url
    cacheMultiUrl.push(url);
    return true;
};
//\u79fb\u51fa\u5185\u5b58\u4e2durl\u5730\u5740
function removeCacheUrl(cacheurl)
{
    //\u904d\u5386\u591a\u6b21\u53d6\u5f97url
    for(var i=0;i<cacheMultiUrl.length;i++)
    {
        //\u5982\u679c\u5728\u5185\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728
        if(cacheMultiUrl[i]==cacheurl)
        {
            cacheMultiUrl.splice(i,1);
        }
    }
};
//\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f

var CallService_complete = function (status, statusText, responseText, responseHTML) {
    
    log.info("Call Service complete:status=" + status);
    log.info("Call Service complete:statusText=" + statusText);
    log.info("Call Service Complete:responseText=" + responseText);
    
    var outPara = this.outParas;
    var serviceId = this.sid;
    var autoPara = this.autoPara;
    var _object = this._object;
    var onSuccess = this.onSuccess;
    var urlerror = this.urlErrorEvent;
    
    //session time out
    if(status == 1000)
    {
       //var tempUrl = document.referrer;
        //tempUrl = tempUrl.substring(0,tempUrl.lastIndexOf("/"));
        //top.window.location.href=tempUrl + "/login.html";
		var href = document.location.href;
		var tempArray = href.replace("//", "/").split("/");	
        var tempUrl =  tempArray[0] + "//" + tempArray[1] + "/" + tempArray[2] + "/uiloader/forward.html";
        window.open(tempUrl, "_self");
        return;
    }
    // noauthori
    //wanghao add
    if(status == 403) {
        document.body.innerHTML = responseText;
        return;
     }
    //success
    if (status == 200)
    {       
        removeCacheUrl(this.cacheurl);
        //\u4fee\u6539\u8bb0\u5f552 \u5f00\u59cb
        //\u7528\u6237\u5b58\u5728\u56fe\u7247\u8fdb\u5ea6\u6761\u7684\u663e\u793a
        if(serviceId){

            var loadingobj = pageContextCache.getLoading(serviceId);
            //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
            if(loadingobj)
            {
                var issyn = loadingobj.isSyn;
                var delay = loadingobj.delay;               
                //\u4fee\u6539\u8bb0\u5f556 \u5f00\u59cb
                if(delay == ""||delay==null||delay==undefined) delay="500";             
                //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
                if(issyn==null||issyn==""||issyn=="false"||issyn==false)
                {
                    var divid = loadingobj.divId;
                    var numObj = new Number(delay);
                    //\u5ef6\u65f6\u529f\u80fd
                    if(delay)
                    {
                        window.setTimeout("hiddendDivImg('"+divid+"');",parseInt(delay));
                    }else//\u4e0d\u5ef6\u65f6
                    {
                        hiddendDivImg(divid);
                    }
                    
                }else if(issyn==true||issyn=="true")//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
                {
                    //\u5ef6\u65f6\u529f\u80fd
                    if(delay)
                    {
                        window.setTimeout("removeObj();",parseInt(delay));
                    }else//\u4e0d\u5ef6\u65f6
                    {
                        removeObj();
                    }                               
                }
                //\u4fee\u6539\u8bb0\u5f556 \u7ed3\u675f
            }           
        }
        //\u4fee\u6539\u8bb0\u5f552 \u7ed3\u675f
        
        if (!responseText)
        {
            alert('no data retrieved from the server');
            return;
        }
        var response;
        try{
          response = eval("(" + responseText + ")");  
        }catch(e){
          return;
        }
        //\u4ecejson\u4e2d\u63d0\u53d6\u6570\u636e
        var rtn_value = response[SHOWDATA_RTN_TAG]; 
        //\u4fee\u6539\u8bb0\u5f553 \u5f00\u59cb
        if(serviceId)
        {
            var servicetagid = pageContextCache.getService(serviceId);
            //\u662f\u5426\u9700\u8981cookies\u7f13\u5b58
            //\u4fee\u6539\u8bb0\u5f554 \u5f00\u59cb
            if(servicetagid.cache=="true"||servicetagid.cache==true)
            {
                var expiresvalue = servicetagid.expires;
                
                setCookie(getNameCookie(serviceId),JSON.stringify(rtn_value),expiresvalue);
            }
            //\u4fee\u6539\u8bb0\u5f554 \u7ed3\u675f
        }
        //\u4fee\u6539\u8bb0\u5f553 \u7ed3\u675f    
        //\u8fd4\u56de\u72b6\u6001
        var res_oprst = response[SHOWDATA_OPRST_TAG] + "";
        
        //var rtn_expDesc = response[SHOWDATA_EXPDESC_TAG];
        var res_expModel = response[SHOWDATA_EXPMODEL_TAG];
        switch (res_oprst) {
                case "0":
                    //\u8fd4\u56de\u4fe1\u606f\u663e\u793a
                    response_success(outPara,serviceId,autoPara,_object,onSuccess,rtn_value);                   
                    break;
                case RIA_NOLOGIN_OPRST:
                    //\u6ca1\u6709\u767b\u5f55
                    isLogin(rtn_value);
                    break;
                case RIA_NOPOPE_OPRST:
                    //\u6ca1\u6709\u6743\u9650
                    isPrivilege(rtn_value);
                    break;
                case CODE_ERR_SIZELIMITEXCEEDED:
                    //\u4e0a\u4f20\u8d85\u51fa\u6700\u5927\u503c
                    response_rtn_expDesc(res_oprst,"FileUploadException - "+res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                    log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                    break;
                case CODE_ERR_CONSOLE:
                     log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                     response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                     break; 
                default:
                    //\u8fd4\u56de\u9519\u8bef\u4fe1\u606f
                    log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                    response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                    break;
                                    
        };
    }
};

var response_success = function (outPara,serviceId,autoPara,_object,onSuccess,rtn_value)
{

    //\u82e5\u8c03\u7528\u7684\u662fcallMethod
    if (onSuccess)
    {
        if((typeof _object) != "object")
        {
            _object = this;
        }
        onSuccess.call(_object, 0, rtn_value);
        return;
    }
    
    //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var responseTags = serviceId == null ? {} : pageContextCache.getResponse(serviceId);
    
    var resp_rtn = "";
    
    //\u5224\u65ad\u7528\u6237\u662f\u5426\u914d\u7f6eoutparas\u5c5e\u6027
    if (outPara)
    {
        resp_rtn = outPara;
    }//\u82e5\u6ca1\u914d\u7f6e\u5219\u76f4\u63a5\u89e3\u6790response
    else if(responseTags.rtn){
        resp_rtn = responseTags.rtn;
    }
    
    //\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
    
    //\u521b\u5efa\u5bf9\u8c61
    var $M = {};
    $M[rtn_string] = rtn_value;
    
    //\u670d\u52a1\u8bf7\u6c42\u524d\u54cd\u5e94\u7684\u4e8b\u4ef6
    if (responseTags.preResponse && responseTags.preResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var preResponse_event = parseValueById(responseTags.preResponse);
        evalEvent($M,preResponse_event);
    }
                
    //\u81ea\u52a8\u76f4\u63a5\u56de\u9009
    if (autoPara)
    {
        log.info("Call Service Complete:autoPara=" + autoPara);
        log.info("Call Service Complete:resp_rtn=" + resp_rtn);
        log.info("Call Service Complete:rtn_value=" + rtn_value);
        if(!document.getElementById(resp_rtn))
        {
            var respRtn = eval(resp_rtn);
            if(respRtn){
              respRtn.rtnValue = $M[rtn_string];
            }           
        }else{
            try{
                BindDataToCtrl(resp_rtn, $M[rtn_string]);
            }
            catch(e){
                var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                commonException(constantRia.RIA_RIA,tempException);
            }
        }
    }
                
    //\u5206\u6790\u914d\u7f6e\u8fd4\u56de\u503c\u4fe1\u606f
    else if (resp_rtn != null)
    {
        //\u8fd4\u56de\u503c\u914d\u7f6e\u82e5\u662f\u5e26$M\u7b26\u53f7
        if ("$" == resp_rtn.substring(0,1))
        {
            //\u5224\u65ad\u4f20\u8fc7\u6765\u7684\u6570\u636e\u662f\u4ec0\u4e48\u7c7b\u578b
            if ((typeof rtn_value) == "object")
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string],$M);

            } else if ((typeof rtn_value) == "string" || (typeof rtn_value) == "boolean" || (typeof rtn_value) =='number')
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string],$M);
            } else if ((typeof rtn_value) == "undefined")
            {
                alert('undefined');
            }
            //\u7c7b\u578b\u5224\u65ad\u7ed3\u675f.

        }//\u82e5\u662f\u63a7\u4ef6ID
        else {
            log.info("Call Service Complete:resp_rtn=" + resp_rtn);
            log.info("Call Service Complete:rtn_value=" + $M[rtn_string]);
            
            try{
                BindDataToCtrl(resp_rtn, $M[rtn_string]);
            }
            catch(e){
                var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                commonException(constantRia.RIA_RIA,tempException);         
            }
        }
    }
    
    //\u670d\u52a1\u8bf7\u6c42\u540e\u54cd\u5e94\u7684\u4e8b\u4ef6      
    if (responseTags.postResponse && responseTags.postResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var postResponse_event = parseValueById(responseTags.postResponse);
        evalEvent($M,postResponse_event);
    }
    //forward\u9875\u9762\u8df3\u8f6c
    getForwards(serviceId,$M);
       
};
function setRiaCtrlvalue(ctrid,value,objvalue){
    if(ctrid!=null&&ctrid !=""&&(objvalue!=undefined||ctrid!=undefined)&&objvalue!=null){
      var bind_object = pageContextCache.getBind(ctrid);
        if(bind_object && bind_object !=null){
        var bind_ref = bind_object.ref;
        var bind_arr = bind_ref.split(".");
        var length_ref = bind_arr.length;
        if(length_ref ==1) return objvalue[bind_ref]=value;
        if(length_ref == 2) return objvalue[bind_arr[0]][bind_arr[1]]=value;
        else if(length_ref >=3){
           var tempvar ={};
           for(var i=0;i<length_ref;i++){
              var strProperty = bind_arr[i];
              tempvar = objvalue[strProperty];
           }
          tempvar = value;
        }
     }
    }
};
var evalEvent = function (m,para)
{ 
    try{
        eval("var $M = arguments[0];");
        eval(para);
    }catch(e){
        var tempException = new RIAException("",e.number,e.description);
        commonException(constantRia.RIA_USER,tempException);    
    }
};

var response_rtn_expDesc = function(errorCode,res_expModel,serviceId,onSuccess,urlerror,res_oprst)
{
    
    if (onSuccess)
    {       
        if((typeof _object) != "object")
        {
            _object = this;
        }
        onSuccess.call(_object, 1, res_expModel);
        return;
    }
    
    //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var serviceTags = serviceId == null ? {} : pageContextCache.getService(serviceId);
    if (serviceTags.onError && serviceTags.onError.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var onError_event = parseValueById(serviceTags.onError);
        urlErrorHandel(errorCode,onError_event,res_expModel,res_oprst);
        return;

    }else if(urlerror!=null&&urlerror!=""&&urlerror){
       urlerror(res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
      /// var onError_callUrlevent = parseValueById(urlerror);
       ///urlErrorHandel(errorCode,onError_callUrlevent,res_expModel);
       return;
    }else if((serviceTags.onError==null||serviceTags.onError=="")&&res_expModel){
       var tempException = new RIAException(res_expModel[ERR_LOCATIONID],res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
       commonException(constantRia.RIA_BACKGROUND,tempException,res_oprst);
       //log.error("\u9519\u8bef\u4ee3\u7801:"+res_expModel[ERR_ERRORCODE]+"\u9519\u8bef\u63cf\u8ff0:"+res_expModel[ERR_ERRORDESC]);
       return;
    }

};
var urlErrorHandel = function(errorCode,onError_event,res_expModel,res_oprst){
   var error_code = new Array("3","4","00","01","02","03","10","20","21","22","30","40","60","70");
   for(var i=0;i<error_code.length;i++)
   if(res_oprst == error_code[i]){
      ssb_update_eval(onError_event,res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
      return;
   }
   eval(onError_event);
};
var ssb_update_eval = function()
{
    var onError_event = arguments[0];
    if(onError_event.indexOf("(") != -1 && onError_event.indexOf(")") != -1)
    {
        var eventStr = onError_event.substring(onError_event.indexOf("("),onError_event.indexOf(")"));
        if(eventStr.indexOf("errorCode") != -1)
        {
             onError_event = onError_event.replace("errorCode","arguments[1]");
        }
        if(eventStr.indexOf("rtn_expDesc") != -1)
        {
             onError_event = onError_event.replace("rtn_expDesc","arguments[2]");
        }
    }
    
    eval(onError_event);
};

var parseRtn = function (pageContext, resp_rtn, rtn_values,tempobj)
{
    //\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
    
    //\u904d\u5386\u6240\u6709bind
    for (var j = 0; j < pageContext.bindArr.length; j++)
    {
        var rtn_value = rtn_values;
        var typeValues = new Array();
        var bind_ref = pageContext.bindArr[j].ref;
        var ctrlid = pageContext.bindArr[j].ctrlid;
        var datatype = pageContext.bindArr[j].datatype;
        var ctrlext = pageContext.bindArr[j].ctrlext;
        var calculate = pageContext.bindArr[j].calculate;

        //\u6bd4\u8f83bind\u7684ref\u548crtn\u503c
        var bindre = new RegExp('^' + rtn_string);
        if (bindre.test(bind_ref))
        {
            var bind_refs = bind_ref.substring(rtn_string.length);
            
            var bind_ref_index = bind_refs.indexOf(SHOWDATA_DOT_TAG);
            var bind_ref_values = new Array();      
                
            var bind_array = /^\[/;
            var bind_arrays = /\]$/;
            if(bind_array.test(bind_refs))
            {
                var array_index = bind_refs.substring(bind_refs.indexOf("[")+1,bind_refs.indexOf("]"));
                rtn_value = rtn_values[parseInt(array_index)];
            }
            
            if (bind_ref_index != -1)
            {
                bind_ref_values = bind_refs.substring(bind_ref_index + 1).split(SHOWDATA_DOT_TAG);
                
                for (var n = 0; n < bind_ref_values.length; n++)
                {
                    //\u4fee\u6539\u8bb0\u5f557 \u5f00\u59cb
                    if(rtn_value == null) return;
                    //\u4fee\u6539\u8bb0\u5f557 \u7ed3\u675f
                    rtn_value = rtn_value[bind_ref_values[n]];
                    
                    //\u82e5rtn_value\u4e2d\u6ca1\u6709\u5c01\u88c5\u6b64\u5bf9\u8c61,\u5219\u76f4\u63a5\u8df3\u51fa\u6b64\u65b9\u6cd5
                    if (rtn_value == undefined)
                    {
                        //error\u65e5\u5fd7
                        //log.error("Call Service Complete:rtn_value1=" + rtn_value);
                        break;
                    }
                }
                log.info("Call Service Complete:ctrlid=" + ctrlid);
                log.info("Call Service Complete:rtn_value=" + rtn_value);
                try{
                    //rtn_value = rtnCalendarValue(datatype,rtn_value);
                    if(calculate != null&&calculate != "") rtn_value=this.rtnCalendarValue(calculate,rtn_value,tempobj);
                    BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext),datatype);
                }
                catch(e){
                      var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                      commonException("ria",tempException);
                }
                
            }else
            {
                if(bind_ref == rtn_string || (bind_array.test(bind_refs)&&bind_arrays.test(bind_refs)))
                {
                    log.info("Call Service Complete:ctrlid=" + ctrlid);
                    log.info("Call Service Complete:rtn_value=" + rtn_value);
                    try{
                        //rtn_value = rtnCalendarValue(datatype,rtn_value);
                        if(calculate != null&&calculate != "") rtn_value = this.rtnCalendarValue(calculate,rtn_value,tempobj);
                        BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext),datatype);
                    }
                    catch(e){
                      var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                      commonException(constantRia.RIA_RIA,tempException);           
                    }
                }
            }

        }

    }
};

var isctrlext = function (ctrlext)
{
    var obj = {};
    if(ctrlext)
    {
        var ctrlArr = ctrlext.split(";");
        for(var i =0;i<ctrlArr.length;i++)
        {
            var cArr = ctrlArr[i].split(":");
            obj[cArr[0]] = cArr[1];
        }
        return obj;
    }
    else{
        return null;
    }
            
};

var isLogin = function(rtn_url)
{
    openPath(rtn_url);
};

var isPrivilege = function(rtn_url)
{
    openPath(rtn_url);
};

var openPath = function (rtn_url){
    window.open(rtn_url,"_self");
};

function splitStringDate(format,value){
  var year = parseInt(value.substring(0,4));
  var month =value.substring(4,6)-1;
  var day = parseInt(value.substring(6,8));
  var hour = parseInt(value.substring(8,10));
  var minute = parseInt(value.substring(10,12));
  var sec = parseInt(value.substring(12,14));
  if(value.length ==14){
      return new Date(year,month,day,hour,minute,sec);
  }else if (value.length == 8) return new Date(year,month,day);
};
var temp_rtnObject;
function getRiaCtrlValue(ctrlid,tempobj_clone){
  if(tempobj_clone!= undefined && ctrlid == null) {
    temp_rtnObject = tempobj_clone;
    return;
  }
  if(tempobj_clone == undefined && ctrlid != null){
     var bind_object = pageContextCache.getBind(ctrlid);
     if(bind_object && bind_object !=null){
        var bind_ref = bind_object.ref;
        var bind_arr = bind_ref.split(".");
        var length_ref = bind_arr.length;
        if(length_ref ==1) return temp_rtnObject[bind_ref];
        if(length_ref == 2) return temp_rtnObject[bind_arr[0]][bind_arr[1]] ;
        else if(length_ref >=3){
           var tempvar =temp_rtnObject;
           var strProperty;
           for(var i=0;i<length_ref;i++){
              strProperty= bind_arr[i];
              tempvar = tempvar[strProperty];
           }
          return tempvar;
        }
     }
  }  
};
/*
function riaClone(myObj){
  if(typeof(myObj) != 'object') return myObj;
  if(myObj == null) return myObj;
  
  var myNewObj = new Object();
  
  for(var i in myObj)
     myNewObj[i] = clone(myObj[i]);
  
  return myNewObj;
};*/
var ria_objClone=function(orgin){ 
    var obj={}; 
    if(typeof orgin=="object"){ 
        var cb=arguments.callee; 
        if(orgin instanceof Array){ 
            for(var i=0,obj=[],l=orgin.length;i<l;i++){ 
                obj.push(cb(orgin[i])); 
            } 
            return obj; 
        } 
        for(var i in orgin){ 
            obj[i]=cb(orgin[i]); 
        } 
        return obj; 
    } 
    return orgin; 
};
/*
Object.prototype.Clone = function(){
    var objClone;
    if ( this.constructor == Object ) objClone = new this.constructor(); 
    else objClone = new this.constructor(this.valueOf()); 
    for ( var key in this ){
        if ( objClone[key] != this[key] ){ 
            if ( typeof(this[key]) == 'object' ){ 
                 objClone[key] = this[key].Clone();
             }
            else{
                 objClone[key] = this[key];
             }
         }
     }
     objClone.toString = this.toString;
     objClone.valueOf = this.valueOf;
    return objClone; 
};*/

function rtnCalendarValue(datatype,value,tempobj)
  {
    
    if(datatype)
    {
        var type =  datatype.trim();
        var datatypes = type.indexOf("date");
        
        if(datatypes == -1)
        {
            var tempobj_clone = ria_objClone(tempobj);
            this.getRiaCtrlValue(null,tempobj_clone);
            var userMethod = datatype;
            var userValue = eval(userMethod+"()");          
            temp_rtnObject = null;
            return userValue;
        }
        if(value == null || value == "")
        {
            return "";
        }
        var format = "yyyy-MM-dd";
        if(type.indexOf("(") != -1)
        {
            format = type.substring(type.indexOf("(")+1,type.indexOf(")"));
        }
        


        // \u4fee\u6539\u8bb0\u5f555 \u5f00\u59cb
        format = format.toLowerCase();

        // add by Daniel  del from ' or " from "format" 
        var start=/^('|")/;
        var end=/('|")$/;

        if(format!="undefined" && format != null && format != "")
        {
            format=format.replace(start,"");
            format=format.replace(end,"");  
        }
        //------------------------------

        if(format.indexOf("hh24") != -1)
        {
            format = format.replace("hh24","hh");
        }
        // \u4fee\u6539\u8bb0\u5f555 \u7ed3\u675f
        
        if(typeof(value) =="string" && value!=null &&(value.length ==14||value.length ==8)) time_value=splitStringDate(format,value);
        else time_value = new Date(value);
    
        var yTime = rtn_replace(format,"yyyy",time_value.getFullYear()+"");
        
        var MTime = rtn_replace(yTime,"mm",numTimes(parseInt(time_value.getMonth()+1)+""));
        
        var dTime = rtn_replace(MTime,"dd",numTimes(time_value.getDate())+"");
        
        var hTime = rtn_replace(dTime,"hh",time_value.getHours()+"");
        
        var mTime = rtn_replace(hTime,"mi",time_value.getMinutes()+"");
        
        var sTime = rtn_replace(mTime,"ss",time_value.getSeconds()+"");
   
        return sTime;
    }
    return value;
   
  };

var numTimes = function (num)
{
    if(parseInt(num)<10)
    {
        return "0"+num;
    }
    return num;
};

var rtn_replace = function (format,str,value)
{
    if(format.indexOf(str) != -1)
    {
        if(value == 0) value = "00";
        else if(value.length == 1) value = "0"+value;       
        return  format.replace(str,value);
    }
    return format;
};

CallService.prototype.openCallback = CallService_openCallback;
CallService.prototype.loading = CallService_loading;
CallService.prototype.loaded = CallService_loaded;
CallService.prototype.interactive = CallService_interactive;
CallService.prototype.complete = CallService_complete;
CallService.prototype.get = CallService_get;
CallService.prototype.put = CallService_put;
CallService.prototype.del = CallService_delete;
CallService.prototype.post = CallService_post;

CallService.prototype.call = CallService_call;

CallService.prototype.response_success = response_success;
CallService.prototype.response_rtn_expDesc = response_rtn_expDesc;
CallService.prototype.parseRtn = parseRtn;
var DEFAULT_METHOD = "POST";
var DEFAULT_DOTSSM_TAG = ".ssm";
RIAControl = function(serviceId, inParas, outParas, autoPara, obj, fun,$M,cacheurl,urlErrorEvent) {
    this.sid = serviceId;
    this.inParas = inParas;
    this.outParas = outParas;
    this.pageContext = pageContextCache;
    this.organizedata = new OrganizeData(inParas);
    this.callService = new CallService(serviceId, outParas, autoPara, obj, fun,$M,cacheurl,urlErrorEvent);
};

RIAControl.prototype.formatGetUrl = function(url, paras) {
    return url;
};


RIAControl.prototype.callByUrl = function(url, methods, paras) {
    this.process(false, url, methods, paras);
};

RIAControl.prototype.callBySid = function(sid, methods, paras) {
    var url = this.pageContext.getUrl(sid);
    this.process(url, methods, paras);
};

RIAControl.prototype.process = function(url, methods, paras,isSyn) {
    var meth1 = methods || "post";
    var meth = meth1.toUpperCase();
    switch (meth) {
        case 'POST':
            this.callService.post(url, null, null, paras,isSyn);
            break;
        case 'GET':
            var furl = this.formatGetUrl(url, paras);
            this.callService.get(furl);
            break;
        case 'DELETE':
            this.callService.del(url);
            break;
        case 'PUT':
            this.callService.put(url, null, null, paras);
            break;
        default:
            break;
    }

};

RIAControl.prototype.processUpload = function(paraMap,url){
    var id =new Date().getTime();
    var createForm=function(){
        var oForm=document.createElement('FORM');
        oForm.id='form_'+id;
        oForm.action=url;
        oForm.method='POST';
        if(oForm.encoding){
            oForm.encoding = 'multipart/form-data';
        }
        else{
            oForm.enctype = 'multipart/form-data';
        }
        
        var jsonInput=document.createElement('INPUT');
        jsonInput.value=paraMap;
        jsonInput.name='_JasonParameters';
        jsonInput.type='hidden';
        oForm.appendChild(jsonInput);
        document.body.appendChild(oForm);
    };
    var createIFrame=function(){
        var oIFrame={};
        try{
            oIFrame = document.createElement("<IFRAME name=frame_"+id+" />");
        }catch(e){
            oIFrame=document.createElement("IFRAME");
            oIFrame.name='frame_'+ id;
        }
        oIFrame.name ='frame_'+ id;
        oIFrame.width=0;
        oIFrame.height=0;
        oIFrame.frameBorder=0;
        oIFrame.id='frame_'+id;
        document.body.appendChild(oIFrame);
        return oIFrame;
    };
    createForm();
    var oFrame=createIFrame();
    var oForm=document.getElementById('form_'+id);
    var thisCtrl=this;
    var uploadCallback=function(){
        var thisDocument=oFrame.contentDocument||oFrame.contentWindow.document;
        var html=thisDocument.body.innerHTML;
        if(html.charAt(0)=='{'){
            responseText=html;
        }else if(html.substring(0,5)=='<pre>'){
            responseText=html.substring(5,html.length-6);
        }
        else
            responseText=thisDocument.getElementById('result').value;
        try{
            thisCtrl.callService.complete(200,'success',responseText,'');
        }catch(e){
            throw 'upload exception:'+e;
        }
        setTimeout(function(){try{
                                oForm.parentNode.removeChild(oForm);
                                oFrame.parentNode.removeChild(oFrame);
                                } catch(e){
                                    throw e;
                                }
                              }, 100);
    };
    
    //wanghao
    var oldParentNodes = [];
    var oldNodes = [];
    
    //wanghao
    var clonedNodes = [];
    for(var i=0;i<__file_List.length;i++){
        if(__file_List[i].type!='file'||__file_List[i].value=='') continue;
        var old=__file_List[i];
        var cloned=__file_List[i].cloneNode(true);
        
        //\u4fee\u6539\u8bb0\u5f552: wanghao
        cloned.onchange=old.onchange;
        old.parentNode.insertBefore(cloned,old);
        old.id=id+'_file_'+i;
        if(old.hideFocus != true)
            old.style.visibility = "hidden";
        
        oldNodes.push(old);
        clonedNodes.push(cloned);
        oldParentNodes.push(    old.parentNode);            
        oForm.appendChild(old);
        /*cloned.onchange=old.onchange;
        old.parentNode.insertBefore(cloned,old);
        old.id=id+'_file_'+i;
        old.style.display='none';
        oForm.appendChild(old);*/
        //\u4fee\u6539\u8bb0\u5f551: wanghao
        /*oldNodes.push(old);
        oldParentNodes.push(old.parentNode);            
        oForm.appendChild(old);*/
    }
    if(window.attachEvent){
        //oFrame.attachEvent('onload', uploadCallback);
        EventManager.Add(oFrame,'load',uploadCallback);
    }
    else{
        oFrame.addEventListener('load', uploadCallback, false);
    }
    oForm.setAttribute("target",oFrame.name);
    oForm.submit();
    
    /*for(var j=0;j<oldParentNodes.length;j++)
    {
        oldParentNodes[j].appendChild(oldNodes[j]);
    }*/
    //\u4fee\u6539\u8bb0\u5f552: wanghao
    for(var j=0;j<oldParentNodes.length;j++)
    {
        if(oldNodes[j].hideFocus == true)
        {
            oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
            continue;
        }
        if(oldNodes[j].style.visibility == "hidden")
            oldNodes[j].style.visibility = "visible";
        oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
    }
    
    //\u4fee\u6539\u8bb0\u5f553: wanghao ------IE\u62a5\u9519\u95ee\u9898
    
    //wanghao8
    //__file_List = input_file;
    //document.body.removeChild(document.getElementById("form_"+id));
    //document.body.removeChild(document.getElementById("frame_"+id));
};
function cacheCookies(outParas,jsonvalue,sid)
{   
    var rtnserviec=pageContextCache.getService(sid);
    var rtnsrponse =rtnserviec.responseObj;
    var rtn =rtnsrponse.rtn;
    if(sid){
        if(outParas&&outParas.indexOf("$")!=-1)
        {
            parseRtn(pageContextCache,outParas,eval('('+jsonvalue+')'));
        }else if(!outParas)
        {
            if(rtn&&rtn.indexOf("$")!=-1)
            {
                parseRtn(pageContextCache,rtn,eval('('+jsonvalue+')'));
            }else
            {
                 BindDataToCtrl(rtn,eval('('+jsonvalue+')'));                   
            }
            
        }       
    }   
};
window.callSidQueue = function(sidArr){
    var sidList = sidArr;
    for(var i=0;sidList!=null && i<sidList.length;i++){
        callSid(sidList[i]);
    }
};
window.callSid = function(sid, inParas, outParas) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
    var obj = getCacheCookie(getNameCookie(sid));
    //\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
    if(obj&&sid)
    {
        cacheCookies(outParas,obj,sid); 
        return;
    }
    var $M ={};
    var url = pageContextCache.getUrl(sid);
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        //\u663e\u793a\u8fdb\u5ea6\u6761
        var loadingobj = pageContextCache.getLoading(sid);
        //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
        if(loadingobj)
        {
            var issyn = loadingobj.isSyn;
            //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                displayDivImg(loadingobj);
            }else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
            {
                divalert(loadingobj);
            }
        }
        var control;
        var paraMap;
        try{
             control= new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
             paraMap= control.organizedata.buildData(sid,$M);
             //wanghao modify
             if(paraMap === false) 
             {
                hiddendDivImg(loadingobj.divId);
                removeObj();
                return;
             }
             
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException); 
        }       
        var isSyn = pageContextCache.getIsSyn(sid);
        log.info("call service: url=" + url);
        log.info("call service: input parameter=\n" + paraMap);

        //add by Daniel
        var enctype = pageContextCache.getEnctype(sid);

        if (  enctype=="multipart"&&__file_List.length&&__file_List.length>0 ) {
            control.processUpload(paraMap,url);
            return ;
        }
        
        if (  enctype=="form") {
            control.process(url, DEFAULT_METHOD, paraMap,isSyn);
            return;
        } 
        //add by Daniel
        
        if(__file_List.length&&__file_List.length>0){
            control.processUpload(paraMap,url);
            //\u4fee\u6539:wanghao
            //__file_List=[]; 
            return;
        }
        control.process(url, DEFAULT_METHOD, paraMap,isSyn);        
    }


};

//wanghao5
window.callSidUpload = function(groupId, sid, inParas, outParas)
{
    
    if(!file_group[groupId])
    {
        return;
    }
    else
    {
        __file_List = file_group[groupId];
    }
    
    callSid(sid, inParas, outParas);
};

window.callTSid = function(sid, inParas, outParas,jsonValue) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
    var obj = getCacheCookie(getNameCookie(sid));
    //\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
    if(obj&&sid)
    {
        cacheCookies(outParas,obj,sid); 
        return;
    }
    var $M ={};
    var url = pageContextCache.getUrl(sid);
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        //\u663e\u793a\u8fdb\u5ea6\u6761
        var loadingobj = pageContextCache.getLoading(sid);
        //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
        if(loadingobj)
        {
            var issyn = loadingobj.isSyn;
            //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                displayDivImg(loadingobj);
            }else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
            {
                divalert(loadingobj);
            }
        }       
         var control;
        try{
            control= new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
            var temppara = control.organizedata.buildData(sid,$M);
            if(temppara === false) return;      
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException);     
        }       
        
        var paraMap = jsonValue;
        var isSyn = pageContextCache.getIsSyn(sid);
        log.info("call service: url=" + url);
        log.info("call service: input parameter=\n" + paraMap);

        control.process(url, DEFAULT_METHOD, paraMap,isSyn);        
    }


};

window.setBindObject = function(resp_rtn,rtn_values)
{
    var pagecontext = pageContextCache;
    parseRtn(pagecontext,resp_rtn,rtn_values);
};
window.getBindObject = function(inParas)
{
    var $M ={};
    var control = new RIAControl(null, inParas, null,true,null,null,$M);
    var paraMaps;
    paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);
    return  paraMaps;   
};
window.callUrl = function(url, inParas, outParas,isSyn,urlErrorEvent) {
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        var $M ={};
         var control;
        var paraMaps;
        try{
            control= new RIAControl(null, inParas, outParas,true,null,null,$M,url,urlErrorEvent);   
            paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);       
            if(paraMaps === false) return;
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException);         
        }
        var paraMap = JSON.stringify(paraMaps);
        log.info("call url: url=" + url);
        log.info("call url: input parameter=\n" + paraMap);
        control.process(url, DEFAULT_METHOD, paraMap,isSyn);
    }
};

window.callMethod = function(sidOrUrl, inParas, obj, fun)
{
   
    var index_url = sidOrUrl.indexOf(DEFAULT_DOTSSM_TAG);
    var url = "";
    if (index_url == -1)
    {
        url = pageContextCache.getUrl(sidOrUrl);
    }
    else
    {
        url = sidOrUrl;
    }
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        var control = new RIAControl(null, null, null, null, obj, fun,url);
        var paraMap = JSON.stringify(inParas);
        control.process(url, DEFAULT_METHOD, paraMap);
    }
};
var _timer_names = new Array();

function callSidTimer(sid, timer)
{
   
   var name = setInterval("callSid('"+sid+"')",timer*1000);

   _timer_names[sid] = name;
};

function intTimerServ(sid)
{
   var name = _timer_names[sid];
   clearInterval(name);
};

function intCurrServ()
{
  throw true;
  
};

window.onerror=function()
{
  return true;
};

var getForwards = function (sid,m)
{
    eval("var $M = arguments[1];");
    var forwardTags = pageContextCache.getForward(sid);
    //alert(forwardTags.length);
    var forwardTags_length = forwardTags.length;
    if (forwardTags_length <= 0)
    {
        return;
    }
    
    //\u904d\u5386forward
    for (var i = 0; i < forwardTags_length; i++)
    {
        var forward_onforward = forwardTags[i].onforward;
        var forward_str = parseForwardValueById(forward_onforward);

        //\u5f97\u5230onforward\u4e2d\u7684\u7b97\u5f0f\u662f\u5426\u6210\u7acb
        var forward_s = eval(forward_str);
        if (forward_s)
        {
            var forward_action = parseToString_action(m,forwardTags[i].action);
            var forward_target = parseValueById(forwardTags[i].target);
            window.open(encodeURI(forward_action), forward_target);
            //return;       
        } else {
            //alert(forward_s);
            continue;
        }

    }

};

var parseForwardValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
        }
        else
        {
            id_value = id;
        }
        str = str.replace(strArr[i], id_value);
    }
    return str;
};

var parseValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
            str = str.replace(strArr[i], '"'+id_value+'"');
        }
        else
        {
            str = str.replace(strArr[i], id);
        }
        
    }
    return str;
};

var parseToString_action = function(m, _parsedStr)
{
    eval("var $M = arguments[0];");
    var parsedStr = parseForwardValueById(_parsedStr);
    var parseArr = parseLabel(parsedStr);
    var str = parsedStr;
    if (parseArr.length != 0)
    {
        for (var j = 0; j < parseArr.length; j++)
        {
            var rtn_str_value = eval(parseArr[j]);           
            var str = str.replace(parseArr[j], rtn_str_value);
        }
    }
    return str;
};

var parseLabel = function (str)
{
    var strArr = new Array();
    var str_index = str.indexOf("$M");
    if (str_index != -1)
    {
        strArr = parseArr(str, strArr);
    }

    return strArr;
};

var parseArr = function (str, strArr)
{
    var strArrs = str.split("");
    var parse_str = "";
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i - 1] == "$" && strArrs[i] == "M")
        {
            parse_str = str.substring(i);
            var strings = "$";
            var parse_strArrs = parse_str.split("");
            for (var j = 0; j < parse_strArrs.length; j++)
            {
                if ((/[^0-9a-zA-Z_\[\].]/g).test(parse_strArrs[j]))
                {
                    strArr.push(strings);
                    strings = "$";
                    var parse_string = parse_str.substring(j);
                    strArr = parseArr(parse_string, strArr);
                    return strArr;
                }
                else {
                    strings = strings + parse_strArrs[j];
                }
            }
            strArr.push(strings);
            return strArr;
        }
    }
    return strArr;
};


var rtnValueById = function (str)
{
    var strArr = new Array();
    var strArrs = str.split("");
    var parse_str = "";
    var TORF = false;
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i] == "$" && strArrs[i + 1] == "{")
        {
            TORF = true;
        }
        if (strArrs[i] == "}")
        {
            parse_str = parse_str + strArrs[i];
            strArr.push(parse_str);
            parse_str = "";
            TORF = false;
        }
        if (TORF)
        {
            parse_str = parse_str + strArrs[i];
        }
    }
    return strArr;
};
    function fixWindow()
    {
        var bgObj = document.getElementById('bgDiv');
        if(document.body.offsetWidth > document.body.scrollWidth)
            bgObj.style.width = document.body.offsetWidth + "px";
        else
            bgObj.style.width = document.body.scrollWidth + "px";
            
        if(document.body.scrollHeight > document.body.offsetHeight)
            bgObj.style.height = document.body.scrollHeight + "px";
        else
            bgObj.style.height = document.body.offsetHeight + "px";
        
    }
        
    function divalert(loadingObj)
    {
       var msgw,msgh,bordercolor;
       msgw=400;//\u63d0\u793a\u7a97\u53e3\u7684\u5bbd\u5ea6
       msgh=100;//\u63d0\u793a\u7a97\u53e3\u7684\u9ad8\u5ea6
       titleheight=25;//\u63d0\u793a\u7a97\u53e3\u6807\u9898\u9ad8\u5ea6
       bordercolor="#336699";//\u63d0\u793a\u7a97\u53e3\u7684\u8fb9\u6846\u989c\u8272
       titlecolor="#99CCFF";//\u63d0\u793a\u7a97\u53e3\u7684\u6807\u9898\u989c\u8272
       var sWidth,sHeight;
       
                
       //\u80cc\u666f\u5c42\uff08\u5927\u5c0f\u4e0e\u7a97\u53e3\u6709\u6548\u533a\u57df\u76f8\u540c\uff0c\u5373\u5f53\u5f39\u51fa\u5bf9\u8bdd\u6846\u65f6\uff0c\u80cc\u666f\u663e\u793a\u4e3a\u653e\u5c04\u72b6\u900f\u660e\u7070\u8272  
       var bgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09
       bgObj.setAttribute('id','bgDiv');
       bgObj.style.position="absolute";
       bgObj.style.top="0";
       //bgObj.style.background="#777";
       bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";
       bgObj.style.opacity="0.6";
       bgObj.style.left="0";
       //bgObj.style.width=sWidth + "px";
       //bgObj.style.height=sHeight + "px";
       bgObj.style.zIndex = "10000";
       
       document.body.appendChild(bgObj);//\u5728body\u5185\u6dfb\u52a0\u8be5div\u5bf9\u8c61
        
       //wanghao
       fixWindow();
       window.onresize = fixWindow;
       
       var msgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
       msgObj.setAttribute("id","msgDiv");
       
       msgObj.style.background="white";
       msgObj.style.border="1px solid " + bordercolor;
       msgObj.style.position = "absolute";
       msgObj.style.left = "50%";
       msgObj.style.top = "50%";
       msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";
       
       msgObj.style.marginLeft = "-225px" ;
       msgObj.style.marginTop = -75+document.documentElement.scrollTop+"px";
       /*msgObj.style.width = msgw + "px";
       msgObj.style.height =msgh + "px";
       msgObj.style.textAlign = "center";
       msgObj.style.lineHeight ="25px";
       */
       msgObj.style.zIndex = "10001";
    
       var title=document.createElement("h4");//\u521b\u5efa\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
       title.setAttribute("id","msgTitle");
       title.setAttribute("align","right");
       title.style.filter="progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";
       title.style.opacity="0.75";
       /*      title.style.margin="0";
       title.style.padding="3px";
       title.style.background=bordercolor;     
       title.style.border="1px solid " + bordercolor;
       title.style.height="18px";
       title.style.font="12px Verdana, Geneva, Arial, Helvetica, sans-serif";
       title.style.color="white";
       title.style.cursor="pointer";
       */
       title.innerHTML='It\'s Loading,Please wait...';
    
       var image = document.createElement("IMG");//\u5b9a\u4e49\u4e00\u4e2a\u80cc\u666f\u56fe\u7247
        var imgscr = "";//\u56fe\u7247\u8def\u5f84
        var width = ""; //\u56fe\u7247\u5bbd\u5ea6
        var height =""; //\u56fe\u7247\u9ad8\u5ea6
        //\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
        if(loadingObj)
        {
            imgscr = loadingObj.img;
            width = loadingObj.width;
            height = loadingObj.height;
        }
        
       //wanghao22
       image.setAttribute("id","msgImage");        
       image.setAttribute("src",imgscr);
       image.setAttribute("align","middle");
       image.setAttribute("width",width);
       image.setAttribute("height",height);
       
       //wanghao22
       /*var cancel =  document.createElement("div");//\u5b9a\u4e49\u4e00\u4e2a\u53d6\u6d88\u6309\u94ae
       cancel.setAttribute("id","msg_cancel");
       cancel.innerHTML = "<a onclick='removeObj()' href='javascript:void(0)'>Cancel</a>";
           */
       document.body.appendChild(msgObj);//\u5728body\u5185\u6dfb\u52a0\u63d0\u793a\u6846div\u5bf9\u8c61msgObj
       document.getElementById("msgDiv").appendChild(title);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u6807\u9898\u680f\u5bf9\u8c61title
       document.getElementById("msgDiv").appendChild(image);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u56fe\u7247\u5bf9\u8c61image
       //document.getElementById("msgDiv").appendChild(cancel);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u53d6\u6d88\u6309\u94aecancel
    }; 
    function removeObj()
    {
         //\u70b9\u51fb\u6807\u9898\u680f\u89e6\u53d1\u7684\u4e8b\u4ef6
         var msgObj=document.getElementById("msgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
         var title=document.getElementById("msgTitle");//\u83b7\u53d6\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
         var bgObj=document.getElementById("bgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09 
         
         //wanghao22
         window.onresize = null;
         
         //wanghao22
         if(bgObj)
            document.body.removeChild(bgObj);//\u5220\u9664\u80cc\u666f\u5c42Div
         if(msgObj && title)
            document.getElementById("msgDiv").removeChild(title);//\u5220\u9664\u63d0\u793a\u6846\u7684\u6807\u9898\u680f
         if(msgObj)
            document.body.removeChild(msgObj);//\u5220\u9664\u63d0\u793a\u6846\u5c42
    };

    function displayDivImg(loadingObj)
    {
        var imgscr = "";//\u56fe\u7247\u8def\u5f84
        var width = ""; //\u56fe\u7247\u5bbd\u5ea6
        var height =""; //\u56fe\u7247\u9ad8\u5ea6
        var divid = ''; //\u8bbe\u7f6e\u8981\u663e\u793a\u7684div
        //\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
        if(loadingObj)
        {
            imgscr = loadingObj.img;
            width = loadingObj.width;
            height = loadingObj.height;
            divid = loadingObj.divId;
        }
        //\u8be5div\u662f\u5426\u5b58\u5728
        if(divid)
        {
           document.getElementById(divid).style.display="";  
           document.getElementById(divid).innerHTML="<img src='"+imgscr+"' width='"+width+"' height='"+height+"'/>";
        }
    };

    function hiddendDivImg(divid)
    {
      //\u8be5div\u662f\u5426\u5b58\u5728
      if(divid)
      {
         document.getElementById(divid).style.display="none";
      }
    };
    //\u8fd9\u662f\u6709\u8bbe\u5b9a\u8fc7\u671f\u65f6\u95f4\u7684\u4f7f\u7528\u793a\u4f8b\uff1a
    //s20\u662f\u4ee3\u886820\u79d2
    //h\u662f\u6307\u5c0f\u65f6\uff0c\u598212\u5c0f\u65f6\u5219\u662f\uff1ah12
    //d\u662f\u5929\u6570\uff0c30\u5929\u5219\uff1ad30  
    //\u5199cookies
    function setCookie(name,value,time){
        var strsec = getsec(time);
        document.cookie = name + "="+ encodeURIComponent (value) + ";expires=" + strsec;
    };
    function getNameCookie(sid)
    {
        var path = document.location.pathname.toString();
        path = path+":"+sid;
        return path;        
    };
    //\u83b7\u53d6\u7528\u4e8e\u8fc7\u671f\u7edf\u8ba1
    function getsec(strtime){
        //\u9a8c\u8bc1\u65f6\u95f4\u683c\u5f0fyyyy-mm-dd
        var timearr = strtime.split("-");
        //\u662f\u65f6\u95f4\u683c\u5f0f
        if(timearr.length==3)
        {
            var objdate =new Object();
            objdate.value=strtime;          
            var bl = isDate(objdate);
            if(bl==undefined)
            {
                bl = true;
            }           
            //\u662f\u6b63\u786e\u7684\u65f6\u95f4\u683c\u5f0f
            if(bl)
            {
                var yeararr = strtime.split("-");
                var year =yeararr[0];
                var month = yeararr[1];
                var day = yeararr[2];
                var utc=Date.UTC(year,month-1,day);
                var date = new Date(utc);
                return date.toGMTString();
            }           
        }else if(strtime)
        {
            //\u5982\u679c\u662f\u4e0b\u4e2a\u6708
            if(strtime.indexOf("DAY")!=-1)
            {
                var monthday =strtime.substring(3,strtime.length);
                var nowtime = new Date();
                var nowmonth = nowtime.getMonth();
                var nowyear = nowtime.getFullYear();
                if(nowmonth ==11)
                {
                    nowyear += 1;
                    nowmonth = "01";                    
                }
                var utc=Date.UTC(nowyear,nowmonth,monthday);
                var date = new Date(utc);
                return date.toGMTString();              
                
            }else//\u5982\u679c\u662f\u4e0b\u4e2a\u661f\u671f\u6216\u8005\u662f\u5f53\u5929\u7684\u67d0\u4e2a\u65f6\u523b
            {
                var weekday = new Date();
                var d = weekday.getDay();
                if(d==0)
                {
                    d=7;
                }               
                switch(strtime)
                {
                    case "MON":
                       return getDisDate(d,1);                      
                    case "TUE":
                       return getDisDate(d,2);
                    case "WED":
                       return getDisDate(d,3);
                    case "THU":
                       return getDisDate(d,4);
                    case "FRI":
                       return getDisDate(d,5);
                    case "SAT":
                       return getDisDate(d,6);
                    case "SUN":
                       return getDisDate(d,7);                                                                                      
                }               
            }
        }
    };
    function getDisDate(disDate,passweekday)
    {
        
        var numObj ;
        if(disDate>passweekday)
        {
            numObj = passweekday - disDate+7;
        }else if(disDate==passweekday)
        {
            numObj=7;
        }else if(disDate<passweekday)
        {
            numObj =passweekday - disDate;
        }       
        var utcm = numObj*60*60*24*1000;
        var cdate = new Date();
        var year = cdate.getYear();
        var month = cdate.getMonth();
        var day = cdate.getDate();

        var utc=Date.UTC(year,month,day);
        utc+=utcm;
        var date = new Date(utc);
        return date;        
    };
    //\u8bfb\u53d6cookies
    function getCacheCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        arr=document.cookie.match(reg);
        if(arr){ return decodeURIComponent(arr[2]);}
        else {return null;}
    };
    //\u5220\u9664\u4e00\u4e2acookies
    function iterativeCookie(name)
    {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval=getCacheCookie(name);
        if(cval!=null) {document.cookie= name + "="+cval+";expires="+exp.toGMTString();}            
    };
    //\u591a\u4e2asid\u7f13\u5b58\u7684\u5220\u9664
    function delCookie(name)
    {
        //\u5220\u9664\u6240\u6709\u7684
        //\u5982\u679c\u53ea\u6709\u4e00\u4e2asid
        if(!name)
        {
            var allservice = pageContextCache.serviceArr;
            for(var i=0;i<allservice.length;i++)
            {
                if(allservice[i].cache==true||allservice[i].cache=='true') iterativeCookie(allservice[i].id);
            }
            return;
        }
        if(name.indexOf(";")==-1)
        {
              iterativeCookie(getNameCookie(name));
              return;
        }else//\u5982\u679c\u6709\u591a\u4e2asid
        {
            var namearr=name.split(";");
            for(var i=0;i<namearr.length;i++)
            {
                if(namearr[i]) 
                {
                    iterativeCookie(getNameCookie(namearr[i]));
                }
            }
            return;
        }
    };

function getTableById(tib)
{
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var Mtable = {};//\u5bf9\u8c61\u6a21\u578b
    Mtable.id = tib;
 	//\u5c5e\u6027
    Mtable.table = table;
    Mtable.rowcount = table.rows.length; //\u8868\u683c\u603b\u884c\u6570
    Mtable.pagesize = tableModel_arr[tib].pg.perPageCount; //\u6bcf\u9875\u8bb0\u5f55\u6570
    Mtable.pageIndex = tableModel_arr[tib].pg.page;//\u5f53\u524d\u7b2c\u51e0\u9875
    Mtable.columncount = tableModel_arr[tib].cells.length;//\u603b\u5217\u6570
    Mtable.pagecount = tableModel_arr[tib].pg.pageCount;//\u603b\u9875\u6570
    Mtable.allcount = tableModel_arr[tib].pg.totalrow;//\u603b\u7684\u8bb0\u5f55\u6570
    Mtable.condition = tableModel_arr[tib].condition;//\u5f53\u524d\u67e5\u8be2\u6761\u4ef6
    Mtable.data = tableModel_arr[tib].pageData;//\u8868\u683c\u6570\u636e
    //\u65b9\u6cd5
	//\u83b7\u53d6\u6307\u5b9a\u5750\u6807\u7684\u5355\u5143\u4e2a\u5bf9\u8c61
    Mtable.getCell = function(x, y) {
        return table.rows[x].cells[y];
    };
	//\u83b7\u53d6\u6307\u5b9a\u5e8f\u5217\u7684\u884c\u5bf9\u8c61
    Mtable.getRow = function(m) {
        return table.rows[m];
    };
	//\u5728\u8868\u683c\u672b\u5c3e\u65b0\u589e\u52a0\u4e00\u884c
    /*1.\u65e0\u53c2\u6570\u65f6\u5220\u9664\u8868\u683c\u672b\u5c3e\u884c
      2.\u6709\u53c2\u6570\u65f6 n \u4ee3\u8868\u884c\u5e8f\u53f7*/
    Mtable.insertRow = function() {
        addOneRow(tib);
    };
    Mtable.deleteRow = function(n)
    {
        if (n == null || n == '')
        {
            n = parseInt(table.rows.length - 1);
        }
        var row = table.rows[n];
        deleteOneRow(tib, row);
    };
 	//\u53d6\u5f97\u8868\u683c\u5217\u5bf9\u8c61
    Mtable.getColumn = function(m)
    {
        var tb_arr = new Array();
        var rows = table.rows;
        for (var i = 0; i < rows.length; i++)
        {
            var row = rows[i];
            tb_arr.push(row.cells[m]);
        }
        return tb_arr;
    };
 	//\u53d6\u5f97\u8868\u683c\u884c\u503c\u5bf9\u8c61\u6570\u7ec4
    Mtable.getRowValues = function() {
        return getTable_objTrs(tib);
    };
 	//\u53d6\u5f97\u8868\u683c\u5217\u503c\u5bf9\u8c61\u6570\u7ec4
    Mtable.getColumnValues = function(index) {
        return getTable_Tds(tib, index);
    };
 	//\u53d6\u5f97\u8868\u683c\u884c\u3001\u5217\u4e8c\u7ef4\u503c\u6570\u7ec4
    Mtable.getAllValues = function() {
        return getTable_Trs(tib);
    };
 	//\u53d6\u5f97\u67d0\u4e00\u5217\u9009\u4e2d\u7684\u503c\u6570\u7ec4\uff08\u8be5\u5217\u4e3aradio\u6216checkbox\uff09
    Mtable.getCheckedValues = function(index) {
        return getTable_Td_CheckedValue(tib, index);
    };
 	//\u91cd\u7f6e\u53ef\u7f16\u8f91\u8868\u683c \u6e05\u7a7a\u6240\u6709\u5355\u5143\u683c\u7684\u503c
    Mtable.reset = function()
    {
        var initrow = parseInt(table.rows.length - 1);
        for (var i = initrow; i > 0; i --)
        {
            table.deleteRow(parseInt(i));
        }
        for (var i = 0; i < initrow; i ++)
        {
			//\u6e05\u7a7a\u4e00\u884c\uff0c\u4e5f\u5c31\u662f\u65b0\u5efa\u4e00\u884c
            create_new_EditTableRow(tableModel_arr[tib].tableId, tib);
        }
    };
    Mtable.refresh = function()//\u5237\u65b0\u5206\u9875\u8868\u683c
    {
        var condition = tableModel_arr[tib].condition;
        if (condition == null)
        {
            return;
        }
        if (tableModel_arr[tib].turnpage == "true")
        {
            var pageNum = this.pageIndex;
            var perPageCount = this.pagesize;
            getTurnPageData(tib, condition, pageNum, perPageCount); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
        }
        else if (tableModel_arr[tib].editable == "true")
        {
            getEditPageData(tib, condition); //\u53ef\u7f16\u8f91\u8868\u683c
        }
    };
 	//\u83b7\u53d6\u8868\u683c\u4e2d\u9009\u62e9\u884c\u5bf9\u8c61\u503c\u6570\u7ec4
    Mtable.getCheckedRowValues = function() {
        return getCheckedRowsValue(tib);
    };

    getColumns = function()
    {
        var cells = new Array();
        for (var i = 0; i < Mtable.columncount; i ++)
        {
            cells.push(Mtable.getColumn(i));
        }
        return cells;
    };
 	
 	//\u8868\u683c\u4e2d\u6587\u4ef6\u4e0a\u4f20
    Mtable.upload = function(o, sid, field, anotherField) {
        uploadFile(o, sid, field, anotherField)
    };
 	//\u96c6\u5408
    Mtable.rows = table.rows;//\u5305\u542b\u884c\u5bf9\u8c61\u7684\u6570\u7ec4
    Mtable.columns = eval("getColumns()");//\u5305\u542b\u5217\u5bf9\u8c61\u7684\u6570\u7ec4

    return Mtable;
}
;

////\u53d6\u53ef\u7f16\u8f91\u8868\u683c\u4e2d\u67d0\u4e00\u5217\u4e2d\u7684input \u548c select\u7684value \u503c\u4ee5\u6570\u7ec4\u7684\u5f62\u5f0f\u8fd4\u56de
getTable_Tds = function(tib, td_index)
{

    var td_value_arr = new Array();
  ////////\u83b7\u53d6\u5185\u5b58\u8868
    var table = tableModel_arr[tib].table;

    var table_rows = table.rows;
    var table_cells = table_rows[1].cells;

  //\u53d6\u6821\u9a8c\u51fd\u6570
    var validate = tableModel_arr[tib].cells[td_index - 1].validate;
    var datatype = tableModel_arr[tib].cells[td_index - 1].datatype;
    var formate = tableModel_arr[tib].date_formate;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index - 1];
        var obj_select = obj.getElementsByTagName("SELECT");
        var obj_input = obj.getElementsByTagName("INPUT");
	
	//\u5faa\u73af\u53d6\u503c
        try {
            for (var m = 0; m < obj_select.length; m ++)
            {
                td_value_arr.push(obj_select[m].value);
            }
		//
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + obj_input[n].value + "')");
                }
                else if (datatype != null && datatype != "" && obj_input[n].type == "text")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase() == 'date')
                    {
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
                td_value_arr.push(value);
            }
        } catch(ee) 
		{
            log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
            return false;
        }
    }

    return td_value_arr;
};

getCheckedRowsValue = function(tib)
{
    for (var i = 0; i < tableModel_arr[tib].cells.length; i++)
    {
        var iskey = tableModel_arr[tib].cells[i].getAttribute("iskey");
        if (iskey == "true")
        {
            return getCheckedValues(tib, i);
            break;
        }
    }
};

getCheckedValues = function (tib, td_index)
{
    var table = tableModel_arr[tib].table;
   //\u83b7\u5f97\u5217\u5c5e\u6027\u6570\u7ec4
    var checkedrows = new Array();
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index];
        var obj_input = obj.getElementsByTagName("INPUT");

        if (obj_input.length < 1)
            alert('The' + parseInt(td_index + 1) + 'Column has no options!');

	//\u5faa\u73af\u53d6\u503c
        for (var n = 0; n < obj_input.length; n++)
        {
            if (obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
            {
                if (obj_input[n].checked == true)
                {
                    var oo = getTable_objTr(tib, i);
                    checkedrows.push(oo);
                    break;
                }
            }
        }
    }
    return checkedrows;
};

getTable_Td_CheckedValue = function (tib, td_index)
{
    var table = tableModel_arr[tib].table;
    var td_checkedvalue = new Array();
    var table_rows = table.rows;
    var table_cells = table_rows[1].cells;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index - 1];
        var obj_input = obj.getElementsByTagName("INPUT");

        if (obj_input.length < 1)
            //alert("\u7b2c" + td_index + "\u5217\u6ca1\u6709\u53ef\u9009\u9879!");
			//alert('The ' + parseInt(td_index + 1) + 'Column has no options!');
			continue;
	//\u5faa\u73af\u53d6\u503c
        for (var n = 0; n < obj_input.length; n++)
        {
            if (obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
            {
                if (obj_input[n].checked == true)
                {
                    // alert(obj_input[n].value);
                    td_checkedvalue.push(obj_input[n].value);
                }
            }
        }
    }
    return td_checkedvalue;
};


//\u83b7\u53d6\u53ef\u7f16\u8f91\u8868\u683c\u4e2d\u67d0\u4e00\u884c\u4e2d\u7684input \u548c select \u6807\u7b7e\u7684value \u503c\u4ee5\u6570\u7ec4\u7684\u5f62\u5f0f\u8fd4\u56de
getTable_Tr = function(tib, tr_index)
{
    var tr_value = new Array();
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var props = tableModel_arr[tib].props;
    var formate = tableModel_arr[tib].date_formate;
    var table_row = table.rows[tr_index];
    var table_cells = table_row.cells;

    try {
        for (var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
        {
            var obj = table_row.cells[i];
            var obj_select = obj.getElementsByTagName("SELECT");
            var obj_input = obj.getElementsByTagName("INPUT");
		//\u53d6\u6821\u9a8c\u51fd\u6570
            var validate = tableModel_arr[tib].cells[i].validate;
            var datatype = tableModel_arr[tib].cells[i].datatype;

		//\u5faa\u73af\u53d6\u503c
            for (var m = 0; m < obj_select.length; m++)
            {
                var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + obj_select[m].value + "')");
                }
                else if (datatype != null && datatype != "")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    //\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
                }
                if (rtn == false)
                {
                    obj_select[m].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }

                tr_value.push(obj_select[m].value);
            }
		//
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + value + "')");
                }
                else if (datatype != null && datatype != "" && (obj_input[n].type == "text" || obj_input[n].type == "hidden"))//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    if (obj_input[n].isDate == "false") continue;
                    //2008-10-23 wxr:\u4e3a\u4ec0\u4e48\u4e5f\u8981\u5bf9\u9690\u85cf\u57df\u8fdb\u884c\u6821\u9a8c\u5462? \u9664\u975e\u5b83\u5185\u90e8\u6307\u5b9a\u4e86datatype
                    if(obj_input[n].type == "hidden")
                   	{
                   		var hide_datatype = obj_input[n].getAttribute('datatype');
                   		if(hide_datatype != null && hide_datatype != "")
                   			rtn = verifyInput(obj_input[n], hide_datatype);
                   	}
                   	else
                    rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase().indexOf("date") != -1 && obj_input[n].isDate == "true")
                    {
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
                tr_value.push(value);
            }
        }
    } catch(ee) 
	{
        log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
        return false;
    }
    return tr_value;
};

function getTable_objTr(tib, tr_index, isCheck)
{
    var tr_value = new Object();
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var props = tableModel_arr[tib].props;
    var formate = tableModel_arr[tib].date_formate;
    var table_row = table.rows[tr_index];
    var table_cells = table_row.cells;

    SSB_RIA_initReturnValueObject(tr_value, props);
    try {
        for (var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
        {
            var prop = props[i];
            var obj = table_row.cells[i];
            var obj_select = obj.getElementsByTagName("SELECT");
            var obj_input = obj.getElementsByTagName("INPUT");
            var validate = tableModel_arr[tib].cells[i].validate;
            var datatype = tableModel_arr[tib].cells[i].datatype;
		//\u5faa\u73af\u53d6\u503c
            for (var m = 0; m < obj_select.length; m ++)
            {
                var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "" && isCheck != false)
                {
                    rtn = eval(validate + "('" + obj_select[m].value + "')");
                }
                else if (datatype != null && datatype != "" && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    //\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
                }
                if (rtn == false)
                {
                    obj_select[m].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
                if (obj_select[m].getAttribute("prop") != null && obj_select[m].getAttribute("prop") != "")
                {
                    prop = obj_select[m].getAttribute("prop");
                }
			
			//\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
                if (prop != null && prop != "")
                {
                    eval("tr_value." + prop + "='" + obj_select[m].value + "'");
                }
            }
			
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "" && isCheck != false)
                {
                    rtn = eval(validate + "('" + value + "')");
                }
                else if (datatype != null && datatype != "" && (obj_input[n].type == "text" || obj_input[n].type == "hidden") && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    if (obj_input[n].isDate == "false") continue;
                    //2008-10-23 wxr:\u4e3a\u4ec0\u4e48\u4e5f\u8981\u5bf9\u9690\u85cf\u57df\u8fdb\u884c\u6821\u9a8c\u5462? \u9664\u975e\u5b83\u5185\u90e8\u6307\u5b9a\u4e86datatype
                    if(obj_input[n].type == "hidden")
                   	{
                   		var hide_datatype = obj_input[n].getAttribute('datatype');
                   		if(hide_datatype != null && hide_datatype != "")
                   			rtn = verifyInput(obj_input[n], hide_datatype);
                   	}
                   	else
                    rtn = verifyInput(obj_input[n], datatype);
					//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase().indexOf("date") != -1 && $(obj_input[n]).attr("isdate") == "true")
                    {
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
                if (obj_input[n].getAttribute("prop") != null && obj_input[n].getAttribute("prop") != "")
                {
                    var ctr_prop = obj_input[n].getAttribute("prop");
                    eval("tr_value." + ctr_prop + "='" + value + "'");
                }
                    //\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
                else if (prop != null && prop != "")
                {
                    if (obj_input[n].type == 'radio' || obj_input[n].type == 'checkbox')
                    {
                        if (obj_input[n].checked == true)
                            eval("tr_value." + prop + "='" + value + "'");
                    }
                    else if (datatype && datatype.toLowerCase().indexOf("date") != -1)
                    {
                    	//\u5982\u679c\u662f\u65f6\u95f4\u7c7b\u578b, \u4ee5long\u578b\u8fd4\u56de, \u800c\u4e0d\u662f\u5b57\u7b26\u4e32
                        eval("tr_value." + prop + "=" + value);
                    }
                    else
                    {
                        eval("tr_value." + prop + "='" + value + "'");
                    }
                }
            }

        }
    } catch(ee) {
        //alert("ErrorMessage is:" + ee.message);
        log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
        return false;
    }
    return tr_value;
}
;

getTable_Trs = function(tib)
{
    var trs_arr = new Array();//\u5217\u6570\u7ec4
    var table = tableModel_arr[tib].table;
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i ++)
    {
        var tr_value = getTable_Tr(tib, i);
        if (tr_value == false)
        {
            return false;
            break;
        }
        trs_arr.push(tr_value);
    }

    return trs_arr;
};

getTable_objTrs = function(tib, isCheck)
{
    var trs_arr = new Array();//\u5217\u6570\u7ec4
    var table = tableModel_arr[tib].table;
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i ++)
    {
        var tr_value = getTable_objTr(tib, i, isCheck);
        if (tr_value == false)
        {
            return false;
            break;
        }
        trs_arr.push(tr_value);
    }

    return trs_arr;
};

function getSortTurnPageData(tid, condition, pageNum, perPageCount, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount;
      	  
	 //\u67e5\u8be2\u6761\u4ef6
    var oCondition = condition;
 	 //\u8d77\u59cb\u884c
    var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
    var fetchSize = perPageCount;
    var oCondition = JSON.stringify(condition);
    var json;
		
	//\u5f53\u603b\u6761\u6570\u4e3a0\u6216\u8005\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u603b\u6761\u6570\u65f6\uff0c\u8003\u8651\u517c\u5bb9\u4ee5\u524d\u7684\u67e5\u8be2\u670d\u52a1\uff0c\u4f203\u4e2a\u53c2\u6570	
    if (tableModel_arr[tid].searchSize || tableModel_arr[tid].pg.totalrow == 0)
        json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
    //\u4e0d\u60f3\u6bcf\u6b21\u90fd\u67e5\u8be2\u603b\u6761\u6570\uff0c\u4f20\u56db\u4e2a\u53c2\u6570
    else if (!tableModel_arr[tid].searchSize)
	{
		if(tableModel_arr[tid].isserversort ==true)
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
		else
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + ", false]";
	}
	//add wanghao 
	if(sortColumn_table =="")
	{
		
	}
	else if (tableModel_arr[tid].isserversort ==true)
  {
		json = json.substring(0, json.lastIndexOf("]")) + ", {isAlwaysSearchSize:"+tableModel_arr[tid].searchSize+",orderByColumn:'"+sortColumn_table+"',isAsc:"+isAsc_table[sortColumn_table]+"}]";
	}

	
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callTSid(sid, preCondition, tid, json);
}
;

function getTurnPageData(tid, condition, pageNum, perPageCount, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount;
      	  
	 //\u67e5\u8be2\u6761\u4ef6
    var oCondition = condition;
 	 //\u8d77\u59cb\u884c
    var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
    var fetchSize = perPageCount;
    var oCondition = JSON.stringify(condition);
    var json;
		
	//\u5f53\u603b\u6761\u6570\u4e3a0\u6216\u8005\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u603b\u6761\u6570\u65f6\uff0c\u8003\u8651\u517c\u5bb9\u4ee5\u524d\u7684\u67e5\u8be2\u670d\u52a1\uff0c\u4f203\u4e2a\u53c2\u6570	
    if (tableModel_arr[tid].searchSize || tableModel_arr[tid].pg.totalrow == 0)
        json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
        //\u4e0d\u60f3\u6bcf\u6b21\u90fd\u67e5\u8be2\u603b\u6761\u6570\uff0c\u4f20\u56db\u4e2a\u53c2\u6570
    else if (!tableModel_arr[tid].searchSize)
	{
		if(tableModel_arr[tid].isserversort ==true)
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
		else
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + ", false]";
	}
	
	
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callTSid(sid, preCondition, tid, json);
}
;


//sunqi:20090820:���������ֶ�
function getTurnPageData1(tid, condition, pageNum, perPageCount, sortColumn,isAsc, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount;
      	  
	 //\u67e5\u8be2\u6761\u4ef6
    var oCondition = condition;
 	 //\u8d77\u59cb\u884c
    var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
    var fetchSize = perPageCount;
    var oCondition = JSON.stringify(condition);
    var json;
		
	//\u5f53\u603b\u6761\u6570\u4e3a0\u6216\u8005\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u603b\u6761\u6570\u65f6\uff0c\u8003\u8651\u517c\u5bb9\u4ee5\u524d\u7684\u67e5\u8be2\u670d\u52a1\uff0c\u4f203\u4e2a\u53c2\u6570	
    if (tableModel_arr[tid].searchSize || tableModel_arr[tid].pg.totalrow == 0)
        json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
        //\u4e0d\u60f3\u6bcf\u6b21\u90fd\u67e5\u8be2\u603b\u6761\u6570\uff0c\u4f20\u56db\u4e2a\u53c2\u6570
    else if (!tableModel_arr[tid].searchSize)
	{
		if(tableModel_arr[tid].isserversort ==true)
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
		else
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + ", false]";
	}
	
	//sunqi:20090820:���������ֶ�
	if(sortColumn!=null && sortColumn.length>0)
	{
		json = json.substring(0, json.lastIndexOf("]")) + ", {orderByColumn:'" + sortColumn + "',isAsc:" + isAsc+"}]";
		sortColumn_table = sortColumn;
		isAsc_table[sortColumn_table] = isAsc;
		tableModel_arr[tid].isserversort = true;
	}
	
	
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callTSid(sid, preCondition, tid, json);
}
;






getEditPageData = function(tid, condition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callSid(sid, condition, tid);
};

function searchTableData(sid, condition, tid)
{
    var perPage = 0;
    tableModel_arr[tid].sid = sid;
    if (tableModel_arr[tid].turnpage == "true" || tableModel_arr[tid].editable == "false")
    {
        perPage = tableModel_arr[tid].pg.perPageCount;// \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
        var bindObject = getBindObject(condition);
        
        //\u521d\u59cb\u5316\u6392\u5e8f\u56fe\u6807
        sortColumn_table = "";
        isAsc_table = {};
        $(tableModel_arr[tid].table.rows[0]).find("img[id^='ssbSortImg']").each(function(){
        	if (isIE())
	        	$(this).attr("className","IEsortASC");
	        else 
	            $(this).attr("className","FFsortASC");
        });
        
        getTurnPageData(tid, bindObject, '1', perPage, condition); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
    }
    else if (tableModel_arr[tid].turnpage == "false" || tableModel_arr[tid].editable == "true")
    {
        perPage = tableModel_arr[tid].initrow;
        getEditPageData(tid, condition); //\u53ef\u7f16\u8f91\u8868\u683c
    }
    else if (tableModel_arr[tid].turnpage == tableModel_arr[tid].editable)
    {
        //alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u4e0d\u80fd\u76f8\u540c!");
        alert('Not allow to set turnpage and editable property same!');
    }
    else
    {
        //alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u914d\u7f6e\u6709\u8bef!");
         alert('The configure of turnpage and editable are Wrong!');
    }
}
;


setTableValue = function(mid, pageinfo)
{
    //\u91cd\u7f6e\u8868\u5934\u590d\u9009\u6846
    tableModel_arr[mid].pageData = null;
    var title_checkbox = document.getElementById(mid + "allCheck");
    if (title_checkbox) title_checkbox.checked = false;

	//\u5206\u9875\u8868\u683c\u586b\u5145
    if (tableModel_arr[mid].turnpage == "true")
    {
		 //\u5f53\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u6216\u8005\u5f53\u524d\u603b\u6761\u6570\u4e3a0\uff0c\u5219\u91cd\u65b0\u8bbe\u7f6e\u8bb0\u5f55\u603b\u6761\u6570
        if (tableModel_arr[mid].searchSize || tableModel_arr[mid].pg.totalrow == 0)
            tableModel_arr[mid].pg.totalrow = pageinfo.totalCount; 

        tableModel_arr[mid].pg.printHtml(); // \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
        tableModel_arr[mid].pageData = pageinfo.data;
        pushData2ssbTable(mid, pageinfo.data);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
    }
	//\u53ef\u7f16\u8f91\u8868\u586b\u5145
    else 
    {
        tableModel_arr[mid].pageData = pageinfo;
        pushData2ssbTable(mid, pageinfo);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
    }
};

var checkedAll = function(td, tid)
{
    var id = '';
    if (typeof tid == 'object')
    {
        id = tid.id;
    }
    var input = td;
    while (td.tagName != "TD")
    {
        td = td.parentNode;
    }
    var tdindex = td.cellIndex;
    var table = getTableById(id);
    var columns = table.getColumn(tdindex);
    for (var i = 1; i < columns.length; i++)
    {
        if (isIE())
        {
            var inputs = columns[i].children;
        } else {
            var inputs = columns[i].childNodes;
        }

        for (var j = 0; j < inputs.length; j++)
        {
            if (inputs[j].type.toLowerCase() == "checkbox")
            {
                inputs[j].checked = input.checked;
            }
        }
    }

};

var tableModel_arr = new Array();//\u6a21\u578b\u6570\u7ec4
var ROW_CLASS_EVEN = "even";
var RIA_SCOPENAME = "Z";
var E_RESIZE = "e-resize";
var DISPLAYHEAD = "isDisplayHead";
var RIA_COLUMN = "z:COLUMN";
//\u662f\u5426\u5bfc\u51faExcel\u6587\u4ef6
var IS_EXCEL = "isexcel";
//\u5206\u9875\u9875\u9762\u8df3\u8f6c\u662f\u5426\u91c7\u7528\u4e0b\u62c9\u6846
var IS_LIST = "islist";
var COLUMN_TAGNAME = "column";
//\u662f\u5426\u603b\u662f\u66f4\u65b0\u8bb0\u5f55\u603b\u6570
var SEARCH_SIZE = "alwaysSearchSize";
//\u5bfc\u51faExcel\u6587\u4ef6\u65f6\u662f\u5426\u663e\u793a\u5217\u9009\u62e9\u754c\u9762
var IS_EXCEL_CONFIG = "isexcelconfig";
var IS_EXCEL_PROP = "isexcelprop";
var VSTATCAPTION = "vstatcaption";
var VSTATFORMULA = "vstatformula";
var CAPTIONSTYLE = "captionstyle";
var CAPTIONTIP = "captiontip";
var IS_EXCEL_STYLE = "isexcelstyle";
var EXCEL_TITLE_STYLE = "exceltitlestyle";
var EXCEL_FILE_STYLE = "excelfilestyle";
//add server sort
//add wanghao
var IS_SERVER_SORT = "isserversort";
var init_ssb_tableModels = function(tb)
{
    // \u83b7\u53d6\u8868\u6a21\u578b\u5bf9\u8c61
    var Mobject = document.getElementById(tb);
	//\u5c06\u8868\u6a21\u578b\u5b58\u5165\u6a21\u578b\u6570\u7ec4
    tableModel_arr[tb] = Mobject;
    tableModel_arr[tb].isIE = isIE();
    var style = null;
	//\u83b7\u53d6className\u5c5e\u6027\u548cstyle\u5c5e\u6027
    if (isIE())
    {
        tableModel_arr[tb].MclassName = Mobject.className;
        style = Mobject.getAttribute("style").cssText;
    }
    else {
        tableModel_arr[tb].MclassName = Mobject.getAttribute("class");
        style = Mobject.getAttribute("style");
    }
    if (Mobject.style.display == 'none')
    {
        var Tstyle = style.replace('none', 'DISPLAY');
        tableModel_arr[tb].Mstyle = Tstyle;
    } else {
        Mobject.style.display = "none";//\u9690\u85cf\u6807\u7b7e
        tableModel_arr[tb].Mstyle = style;
    }
    tableModel_arr[tb].isExcel = Mobject.getAttribute(IS_EXCEL);
	////////////////\u5bfc\u51fa\u6587\u4ef6\u662f\u5426\u5e94\u7528\u6837\u5f0f
    if (Mobject.getAttribute(IS_EXCEL_STYLE) == "true")
        tableModel_arr[tb].IS_EXCEL_STYLE = true;
    else
        tableModel_arr[tb].IS_EXCEL_STYLE = false;
	
	////////////////\u9ed8\u8ba4\u663e\u793a\u4e0b\u62c9\u6846
    if (Mobject.getAttribute(IS_LIST) == "false")
        tableModel_arr[tb].isList = false;
    else
        tableModel_arr[tb].isList = true;
	////////////////\u9ed8\u8ba4\u95ee\u9898\u66f4\u65b0\u603b\u8bb0\u5f55\u6570
    if (Mobject.getAttribute(SEARCH_SIZE) == "false")
        tableModel_arr[tb].searchSize = false;
    else
        tableModel_arr[tb].searchSize = true;
	
	//add wanghao	
	if (Mobject.getAttribute(IS_SERVER_SORT) == "true")
        tableModel_arr[tb].isserversort = true;
    else
        tableModel_arr[tb].isserversort = false;
	
    tableModel_arr[tb].isExcelConfig = Mobject.getAttribute(IS_EXCEL_CONFIG);
    var pg = new showPages('pg');//\u5206\u9875
    pg.tid = tb;
    tableModel_arr[tb].pg = pg;
    var props = new Array();//\u5217\u5c5e\u6027\u7684\u6570\u7ec4\u5bf9\u8c61
    var caption = new Array();//\u8868\u683c\u7684\u5217\u540d
    //\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u6a21\u578b
    var col = new Array();
    var clum = new Array();
    var excel_title = new Array();
    var excel_props = new Array();
    //\u8868\u5934\u6837\u5f0f\u6570\u7ec4
    var capstyles =  new Array();
    //\u8868\u5934\u63d0\u793a\u6570\u7ec4
    var captips =  new Array();
    //\u5bfc\u51faexcel\u6587\u4ef6\u6837\u5f0f,\u5982\u679c\u6ca1\u6709\u8bbe\u7f6etitle\u6837\u5f0f\u5219\u5168\u90e8\u4ee5file\u4e3a\u51c6, \u91c7\u7528\u952e\u503c\u5bf9\u7684\u65b9\u5f0f
    var excel_file_title = new Object();
    var excel_file = new Object();
    
    if (isIE())
    {
        // IE\u83b7\u53d6\u8868\u6a21\u578b\u7684\u5217\u6570\u7ec4
        var cd = Mobject.firstChild;
        while (cd != null)
        {
            if (cd.tagName === COLUMN_TAGNAME)
            {
                clum.push(cd);
            }
            cd = cd.nextSibling;
        }
    }
    else {
        clum = Mobject.getElementsByTagName(RIA_COLUMN);//\u706b\u72d0
    }
    for (var i = 0; i < clum.length; i ++)
    {
        if (clum[i].getAttribute("visible") != 'false')
        {
            col.push(clum[i]);
            capstyles.push(clum[i].getAttribute('CAPTIONSTYLE'));
            captips.push(clum[i].getAttribute('CAPTIONTIP'));
        }
        if (clum[i].getAttribute(IS_EXCEL_PROP) != 'false')
        {
			//\u8bbe\u7f6eExcel\u5bfc\u51fa\u65f6\u8981\u663e\u793a\u7684\u5217\u7684\u503c
            excel_title.push(clum[i].getAttribute("caption"));
            excel_props.push(clum[i].getAttribute("prop"));
            if(tableModel_arr[tb].IS_EXCEL_STYLE == true)
            {
            	//\u91c7\u7528\u4e0e\u5c5e\u6027\u7ed1\u5b9a\u7684\u65b9\u5f0f
            	eval('excel_file_title.' + excel_props[i]+ "= (clum[i].getAttribute(EXCEL_TITLE_STYLE) == null)?'':clum[i].getAttribute(EXCEL_TITLE_STYLE)");
            	eval('excel_file.' + excel_props[i]+ "= (clum[i].getAttribute(EXCEL_FILE_STYLE) == null)?'':clum[i].getAttribute(EXCEL_FILE_STYLE)");
    		}
        }
    }
	//\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u5bf9\u8c61\u96c6\u5408
    tableModel_arr[tb].cells = col;
    tableModel_arr[tb].clums = clum;
    tableModel_arr[tb].excel_title = excel_title;
    tableModel_arr[tb].excel_props = excel_props;
    for(var i=0; i<capstyles.length; i++)
    {
    	if(capstyles[i] == null || capstyles[i] == "null") capstyles[i]="";
    	if(captips[i] == null || captips[i] == "null") captips[i]="";
    }
    tableModel_arr[tb].capstyles = capstyles;
    tableModel_arr[tb].captiontips = captips;
	
	if(tableModel_arr[tb].IS_EXCEL_STYLE == true)
	{
		tableModel_arr[tb].excel_file_title = excel_file_title;
	    tableModel_arr[tb].excel_file = excel_file;
    }
    
	//////////////\u5982\u679c\u5217\u6570\u7ec4\u957f\u5ea6\u4e3a0\uff0c \u9000\u51fa\u521d\u59cb\u5316
    if (tableModel_arr[tb].clums.length == 0) {
        return false;
    }
	
	//\u521b\u5efa\u6a21\u578b\u8868\u5217\u7684\u5b50\u7ed3\u70b9\u6a21\u578b
    for (var j = 0; j < tableModel_arr[tb].cells.length; j++)
    {
        //\u83b7\u53d6\u5217\u6570\u503c\u6821\u9a8c\u51fd\u6570
        var checkfunction = tableModel_arr[tb].cells[j].getAttribute("validate");
        tableModel_arr[tb].cells[j].validate = checkfunction;

        var defaultCheck = tableModel_arr[tb].cells[j].getAttribute("datatype");
        tableModel_arr[tb].cells[j].datatype = defaultCheck;

        var td_Node_Children = tableModel_arr[tb].cells[j].childNodes;
        var td_Children = new Array();
        for (var m = 0; m < td_Node_Children.length; m ++)
        {
        	//\u5e94\u5f53\u628a\u6ce8\u91ca\u4e5f\u53bb\u6389nodevalue=8
            if (td_Node_Children[m].nodeType != 8 && td_Node_Children[m].tagName != null && td_Node_Children[m].tagName != "Z:COLUMN")
            {
                td_Children.push(td_Node_Children[m]);
            }
			
			else if(td_Node_Children[m].nodeType == 3 && $.trim(td_Node_Children[m].nodeValue).length > 0)
			{
 				var textvalue = td_Node_Children[m].nodeValue.match(/#nbsp;/gi);
				if(typeof(textvalue) != "undefined" && textvalue != null)
				{
					var space = document.createTextNode(" ");
					for(var i=0; i<textvalue.length; i++) td_Children.push(space);
				}
			}
        }
		
		//\u5c06td\u5b50\u7ed3\u70b9\u7684\u96c6\u5408\u653e\u5230\u6a21\u578b\u5217\u4e2d
        tableModel_arr[tb].cells[j].td_Children = td_Children;

    }
    //\u521b\u5efa\u5185\u5b58\u8868ID\u5c5e\u6027,\u5e76\u653e\u5165\u6a21\u578b\u5bf9\u8c61\u7684tableId\u5c5e\u6027\u4e2d,\u89c4\u5219\u662f\u6a21\u578bID+"_ssb"
    tableModel_arr[tb].tableId = tb + "_ssb";
    //\u6a21\u578bID
    tableModel_arr[tb].MtableId = tb;
	//\u83b7\u53d6SID
    var sid = Mobject.getAttribute("sid");
    tableModel_arr[tb].sid = sid;
    //\u662f\u5426\u5206\u9875
    if (Mobject.getAttribute('turnpage') != null)
        tableModel_arr[tb].turnpage = Mobject.getAttribute('turnpage');
    else if (Mobject.getAttribute('editable') != 'true')tableModel_arr[tb].turnpage = "true";
    else tableModel_arr[tb].turnpage = "false";
    if ((Mobject.getAttribute('turnpage') == null || Mobject.getAttribute('turnpage') == 'false') && (Mobject.getAttribute('editable') != null && Mobject.getAttribute('editable') == "true"))
        tableModel_arr[tb].editable = 'true';
    //\u8bbe\u7f6e\u6bcf\u9875\u663e\u793a\u591a\u5c11\u884c\u7684\u4e0b\u62c9\u5217\u8868\u6846
    if (tableModel_arr[tb].turnpage == "true")
    {
        if (Mobject.getAttribute('pagesizelist') == null || Mobject.getAttribute('pagesizelist') == "")
        {
            tableModel_arr[tb].pageSizeList = new Array("10", "20", "30", "50");
        }
        else {
            var pagessize = Mobject.getAttribute('pagesizelist');
            tableModel_arr[tb].pageSizeList = pagessize.split(',');
            tableModel_arr[tb].pg.perPageCount = tableModel_arr[tb].pageSizeList[0];
        }
    }
    tableModel_arr[tb].props = props;
    tableModel_arr[tb].caption = caption;
	//\u5728\u8868\u683c\u4e2d\u70b9\u51fb\u67d0\u884c\u7684\u9f20\u6807\u4e8b\u4ef6
    if (Mobject.getAttribute('leftButtonEvent'))
        tableModel_arr[tb].leftButtonEvent = Mobject.getAttribute('leftButtonEvent');
    if (Mobject.getAttribute('rightButtonEvent'))
        tableModel_arr[tb].rightButtonEvent = Mobject.getAttribute('rightButtonEvent');
    if (Mobject.getAttribute('dbClickEvent'))
        tableModel_arr[tb].dbClickEvent = Mobject.getAttribute('dbClickEvent');
    if (Mobject.getAttribute(DISPLAYHEAD))
        tableModel_arr[tb].isDisplayHead = Mobject.getAttribute(DISPLAYHEAD);
    if (Mobject.getAttribute(VSTATCAPTION) != null) {
        tableModel_arr[tb].vstext = Mobject.getAttribute(VSTATCAPTION);
        tableModel_arr[tb].vstatcaption = true;
    }
    else tableModel_arr[tb].vstatcaption = false;
	//\u68c0\u67e5\u7528\u6237\u662f\u5426\u8bbe\u7f6e\u4e86\u521d\u59cb\u5316\u5b8c\u6210\u540e\u7684\u5904\u7406	
    if (Mobject.getAttribute("onFinished") && Mobject.getAttribute(VSTATCAPTION) != '')
        tableModel_arr[tb].onFinished = Mobject.getAttribute("onFinished");
};

var init_table_CSS = function(Mobject, table)
{
    if (Mobject["border"] != null)
    {
        table.setAttribute("border", Mobject["border"]);
    }
    else {
        table.setAttribute("border", "0");
    }
    if (Mobject["cellpadding"] != null)
    {
        table.setAttribute("cellPadding", Mobject["cellpadding"]);
    }
    else {
        table.setAttribute("cellPadding", "0");
    }
    if (Mobject["cellspacing"] != null)
    {
        table.setAttribute("cellSpacing", Mobject["cellspacing"]);
    }
    else {
        table.setAttribute("cellSpacing", "1");
    }
    if (Mobject["width"] != null)
    {
        table.setAttribute("width", Mobject["width"]);
    }
    /*
    else {
        table.setAttribute("width", "100%");
    }
    */
    if (Mobject["height"] != null)
    {
        table.setAttribute("height", Mobject["height"]);
    }
};

var init_ssb_table_Tbody = function(tb)
{
    var Mobject = tableModel_arr[tb];
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');
   //\u8868\u683c\u5217\u5bf9\u8c61\u96c6\u5408
    var col = tableModel_arr[tb].cells;
   //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
    var props = tableModel_arr[tb].props;
	//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
    var caption = tableModel_arr[tb].caption;
    var capstyles = tableModel_arr[tb].capstyles;
    var captiontips = tableModel_arr[tb].captiontips;
    
    //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
    if (isIE())//\u5982\u679c\u662fIE
    {
        Mobject.parentElement.appendChild(table);
    }
    else {
        Mobject.parentNode.appendChild(table);
    }
    table.appendChild(tbody);
    tableModel_arr[tb].table = table;
    table.setAttribute('id', tableModel_arr[tb].tableId);//\u8bbe\u7f6e\u8868\u5bf9\u8c61\u7684ID\u503c

    init_table_CSS(Mobject, table);
    //\u8bbe\u7f6e\u8868\u683cstyle\u5c5e\u6027
    table.style.cssText = tableModel_arr[tb].Mstyle;
    var tt = tableModel_arr[tb].table;
    var objRow = tt.insertRow(-1);
    if (Mobject.isDisplayHead == 'false')
    {
        objRow.style.display = "none";
    }
   //\u8bbe\u7f6eCSS\u6837\u5f0f
    if (tableModel_arr[tb].MclassName != null && tableModel_arr[tb].MclassName != "")
    {
        table.className = tableModel_arr[tb].MclassName;
        objRow.className = "tr_detail";
    }
    else {

        table.className = "tb_datalist";//\u8bbe\u7f6e\u6837\u5f0f\u65f6\u7528className.
        objRow.className = "tr_detail";
    }

    for (var i = 0; i < col.length; i++)
    {
        //\u521d\u59cb\u5316\u8868\u5934\u5217\u540d
        var html = '';
        if (col[i].getAttribute("checkall") == 'true' && col[i].getAttribute("iskey") == 'true')
        {
            html += '<input class="checkAll" type="checkbox" onclick="checkedAll(this, ' + tb + ')" name="allChecked" id="' + tb + 'allCheck"/>';
        }
        if (col[i].getAttribute("caption") != null)
        {
            var title = col[i].getAttribute("caption");
            html += title;
            caption[i] = title;
        }
		
        var withd = col[i].getAttribute("width");
        var objCell = objRow.insertCell(-1);
		//\u8868\u6392\u5e8f
        var cellSort = col[i].getAttribute("allowsort");
        var sortHtml = '';
        var sortClass = null;
        if (isIE())
        {
            sortClass = "IEsortASC";
        } else {
            sortClass = "FFsortASC";
        }
        if (cellSort == 'true')
        {
            sortHtml += '<IMG id="ssbSortImg'+ i +'" class="' + sortClass + '" onclick="sortTable(' + tableModel_arr[tb].tableId + ', ' + i + ', this);">';
        }

        objCell.setAttribute("width", withd);
       // objCell.setAttribute("style", "TEXT-ALIGN: left");//style="TEXT-ALIGN: left"

        html += sortHtml;

        if (i == parseInt(col.length - 1))//
        {
            objCell.innerHTML = "<SPAN style='"+capstyles[i]+"' title='"+captiontips[i]+"'>" + html + '</SPAN><span></span>';
        } else {
            objCell.innerHTML = "<SPAN style='"+capstyles[i]+"' title='"+captiontips[i]+"'>" + html + '</SPAN><span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,' + tableModel_arr[tb].tableId + ');"></span>';
            objCell.lastChild.className = "resizeBar";
        }
		//2008-08-28 \u6dfb\u52a0\u6ce8\u91ca\uff1a\u8fd9\u91cc\u8bbe\u7f6e\u8868\u5934caption\u7684\u6837\u5f0f
        objCell.firstChild.className = "resizeTitle";
		
		//\u4fdd\u5b58\u884c\u5bf9\u8c61\u7684\u5c5e\u6027\u540d\u79f0		
        props[i] = col[i].getAttribute('prop');
    }

};


init_ssb_table = function(tb)
{
    //\u521d\u59cb\u5316\u6a21\u578b
    init_ssb_tableModels(tb);
   
   //////////////\u5982\u679c\u5217\u6570\u7ec4\u957f\u5ea6\u4e3a0\uff0c \u9000\u51fa\u521d\u59cb\u5316
    if (tableModel_arr[tb].clums.length == 0) {
        return false;
    }
   
    var Mobject = tableModel_arr[tb];
   //\u521b\u5efa\u8868\u5934\u6807\u9898
    if (Mobject["title"] != null && Mobject["title"] != "")
    {
        //\u5f97\u5230\u6a21\u578b\u5bf9\u8c61
        var htable = document.createElement('table');//\u521b\u5efa\u8868\u683c
        var thead = document.createElement('thead');
        if (isIE())
        {
        	htable.appendChild(thead); 
        	Mobject.parentElement.appendChild(htable);
        }
        else 
        {
        	htable.appendChild(thead); 
            Mobject.parentNode.appendChild(htable);
        }
        
        //\u8bbe\u7f6e\u6837\u5f0f
        init_table_CSS(Mobject, htable);
        var table_title = "";
        if (table_title != null)
        {
            table_title = Mobject["title"];
        }
        var row = htable.insertRow(-1);//insertRow(-1)firfox\u652f\u6301
        //row["align"] = "left";
        var cell1 = row.insertCell(-1);//insertCell(-1) firfox\u652f\u6301
        cell1.innerHTML = table_title;
        //cell1.width = "95%";
		
		//2008-08-28 \u8bfb\u53d6\u8868\u683c\u6807\u9898title\u6837\u5f0f
		/*
        if(isIE())
	   {
	   		cell1.setAttribute("className","tableTitle");
	   }
	   else
	   {
	   		cell1.setAttribute("class","tableTitle");
	   }
	   */
	   
        var addButton = "'Add'";
        addButton = addButton.indexOf("$") == -1 ? addButton : "\u65b0\u589e";
	  //\u5982\u679c\u8868\u6a21\u578b\u5bf9\u8c61\u7684editable\u5c5e\u6027\u4e3a"true"\u5219\u521b\u5efa\u7684\u8868\u683c\u4e3a\u53ef\u7f16\u8f91\u8868\u683c
        if (Mobject["editable"] == "true")
        {
            var cell2 = row.insertCell(-1);
            cell2.innerHTML = "<input class='button' type='button' name='Submit52' onClick=create_new_EditTableRow('" + tableModel_arr[tb].tableId + "','" + tb + "'); return false; value=" + addButton + "  class='button'/>";
            //cell2.width = "5%"
        }
  	 //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
        if (isIE())//\u5982\u679c\u662fIE
        {
            //Mobject.parentElement.appendChild(htable);
            cell1.setAttribute("className","tableTitle");
            if(cell2)
        		cell2.setAttribute("className","newButton");
        }
        else {
            //Mobject.parentNode.appendChild(htable);
           	cell1.setAttribute("class","tableTitle");
        	if(cell2)
        		cell2.setAttribute("class","newButton");
        }
        htable.className = "tb_titlebar";
        htable.style.cssText = tableModel_arr[tb].Mstyle;
    }
   //\u521b\u5efa\u8868\u683c\u4e3b\u4f53
    if (tableModel_arr[tb].table) return;
    init_ssb_table_Tbody(tb);
	//\u5982\u679c\u662f\u4e0d\u53ef\u7f16\u8f91\u8868\u683c
    if (Mobject["turnpage"] == 'true')
    {
        //\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
        var div_table = document.createElement("table");
        var table_body = document.createElement("tbody");
        var thead = document.createElement('thead');
        div_table.appendChild(thead);
        div_table.appendChild(table_body);
        var div_tr = document.createElement("tr");
        table_body.appendChild(div_tr);
        var dir_tr_td = document.createElement('td');
        div_tr.appendChild(dir_tr_td);
        tableModel_arr[tb].toolbarId = tb + "_pages";
        dir_tr_td.setAttribute('id', tableModel_arr[tb].toolbarId);
        dir_tr_td.setAttribute('align', 'right');
		
		 //2008-08-28 \u8bfb\u53d6\u5206\u9875\u680f\u7684\u76f8\u5173\u6837\u5f0f
        if(isIE())
		 {
		 	dir_tr_td.setAttribute("className","tableNavigator");
		 	Mobject.parentElement.appendChild(div_table);
		 }
		 else
		 {
		 	dir_tr_td.setAttribute("class","tableNavigator");
		 	Mobject.parentNode.appendChild(div_table);
		 }
		 //2008-08-28 \u8bfb\u53d6\u5206\u9875\u680f\u7684\u76f8\u5173\u6837\u5f0f end
	
        div_table.id = "div_" + tb;
        div_table.className = "toolbarTable";
        div_table.style.cssText = tableModel_arr[tb].Mstyle;
        tableModel_arr[tb].pg.printHtml();//\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
    }
        //\u5982\u679c\u662f\u53ef\u7f16\u8f91\u8868\u683c
    else if (Mobject["editable"] == 'true')
    {
        //\u521d\u59cb\u5316\u8868\u683c
		//2008-08-27 wxr: \u56e0\u4e3ainitrow\u5e76\u6ca1\u6709\u88ab\u5b9a\u4e49\u4e3a\u8868\u683c\u7684\u4e00\u4e2a\u5168\u5c40\u5c5e\u6027\uff0cMobject["initrow"]\u5728FF\u4e2d\u4e0d\u80fd\u53d6\u5f97\u503c\uff0c\u6240\u4ee5\u91c7\u7528\u901a\u7528\u7684getAttribute\u65b9\u6cd5
		var initrownum = Mobject.getAttribute("initrow");
        if (initrownum != null)
        {
            for (var i = 0; i < initrownum; i ++)
            {
                create_new_EditTableRow(Mobject["tableId"], tb);
            }
        }        
    }

    table_Header_Resize.resizeInit();
};

InsertRow = function(objTable, tb)
{
    var colNum = tableModel_arr[tb].cells.length;
    var objRow = objTable.insertRow(-1);
    for (var i = 0; i < colNum; i++)
    {
        var objCell = objRow.insertCell(-1);
    }
    return objRow;
};

addOneRow = function(Mtid)
{
    var tableId = tableModel_arr[Mtid].tableId; //\u5185\u5b58\u8868
    return create_new_EditTableRow(tableId, Mtid);
};

deleteOneRow = function(tb, e, tf)
{
    var tableId = tableModel_arr[tb].tableId;
    var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
    var tr = e; //\u83b7\u53d6\u884c\u5bf9\u8c61
    while (tr.tagName.toLowerCase() != 'tr')
    {
        tr = tr.parentNode
    }
  //\u7b2c\u4e00\u884c\u662f\u8868\u5934,\u5e94\u8be5\u53bb\u6389
    var index = tr.rowIndex;

    if (tf == undefined || tf != false)
    {
        //if (confirm('\u786e\u5b9a\u5220\u9664\u5417?'))
        if (confirm('Are you sure to delete?'))
        {
            objTable.deleteRow(index);
	  //\u5f53\u8bbe\u7f6e\u603b\u662f\u66f4\u65b0\u603b\u8bb0\u5f55\u6570\u65f6\u624d\u91cd\u7f6e
            if (tableModel_arr[tb].searchSize)
                reSetTableRowNum(tb, index);
            return true;
        }
        else
        {
            return false;
        }
    }
    else {
        objTable.deleteRow(index);
	  //\u5f53\u8bbe\u7f6e\u603b\u662f\u66f4\u65b0\u603b\u8bb0\u5f55\u6570\u65f6\u624d\u91cd\u7f6e
        if (tableModel_arr[tb].searchSize)
            reSetTableRowNum(tb, index);
        return true;
    }
};

function reSetTableRowNum(MtableId, rowIndex)
{
    var tableId = tableModel_arr[MtableId].tableId;
    var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
    var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9

    var rows = objTable.rows;
    if (rows.length == 1)
    {
        var t_inputs = objTable.getElementsByTagName("input");
        for (var i = 0; i < t_inputs.length; i ++)
        {
            if (t_inputs[i].type.toUpperCase() == "CHECKBOX")
                t_inputs[i].checked = false;
        }
    }
    for (var i = rowIndex; i < rows.length; i ++)
    {
        if (rows[i].rowIndex % 2 == 0)
        {
            rows[i].className = "even";
        } else {
            rows[i].className = "";
        }
        for (var j = 0; j < tdnodes.length; j++)
        {
            var prop = tableModel_arr[MtableId].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
            if (tableModel_arr[MtableId].cells[j].td_Children.length == 0)
            {
                if (prop == "$ROWNUM")
                {
                    rows[i].cells[j].innerHTML = rows[i].rowIndex;
                }
            }
        }
    }
    var props = tableModel_arr[MtableId].excel_props;
    for(j=0; j<props.length; j++)
    {
    	if (props[j] == "$ROWNUM")
         {
             break;
         }
    }
    //\u662f\u5426\u6709\u7edf\u8ba1\u884c
    if(tableModel_arr[MtableId].vstatcaption)
    {
    	var pagetable = new PageZTable(MtableId);
    	if(j > 0 && j < pagetable.columnLength)
    		objTable.rows[objTable.rows.length - 1].cells[j].innerHTML = "";
    	else if(j == 0)
    		objTable.rows[objTable.rows.length - 1].cells[j].innerHTML = tableModel_arr[MtableId].vstext;
    }
    	
    if (tableModel_arr[MtableId].turnpage == "true" || tableModel_arr[MtableId].editable == "false")
    {
        tableModel_arr[MtableId].pg.totalrow = tableModel_arr[MtableId].pg.totalrow > 0 ? tableModel_arr[MtableId].pg.totalrow - 1 : 0;
        tableModel_arr[MtableId].pg.getPageCount();
        //\u5982\u679c\u5f53\u524d\u9875\u53ea\u6709\u4e00\u6761\u6570\u636e\u65f6, \u5f53\u5220\u9664\u4e86\u8fd9\u6761\u6570\u636e\u540e,\u5e94\u5f53\u8fd4\u56de\u5230\u4e0a\u4e00\u9875,\u5e76\u67e5\u8be2\u51fa\u4e0a\u4e00\u9875\u7684\u6570\u636e
        update2NewPage(MtableId);
        tableModel_arr[MtableId].pg.printHtml();
    }
}
;
function update2NewPage(tid)
{
	var ztable = getTableById(tid);
	var deadline = 1;
	 //\u662f\u5426\u6709\u7edf\u8ba1\u884c
    if(tableModel_arr[tid].vstatcaption)
    	deadline = 2;
	if(ztable.rowcount == deadline)
		getTurnPageData(ztable.id, ztable.condition, ztable.pageIndex-1, ztable.pagesize);
}

function updateStatisticRow(tid, index, statisticTitle, customTRString)
{
	var pagetable = new PageZTable(tid);
	var tableData = new Array();
	var length_rows = pagetable.rows.length - 1;
	if(!isIE())
	{						
		FF_innerText();
	}
	//1. \u53d6\u6570\u636e
	for(var i = 1; i < pagetable.columnLength; i++)
	{
		var columnDatas = pagetable.getColumnCells(i);
		for(var j = 0; j < columnDatas.length; j++)
		{
			//\u6ce8\u610fFF\u4e0d\u652f\u6301innerText, \u9700\u52a0\u5165\u517c\u5bb9\u8bbe\u7f6e
			columnDatas[j] = columnDatas[j].innerText;
		}
		
		tableData.push(columnDatas);
	}
	columnDatas = null;
	
	var statisdicProps = pagetable.getStatisticProps();
	var rowdata = new Array();
	//\u81ea\u5b9a\u4e49\u7edf\u8ba1\u680f\u548c\u9ed8\u8ba4\u7edf\u8ba1\u680f\u7684\u533a\u522b
	if(statisticTitle == null || statisticTitle == "")
		rowdata.push(pagetable.getStatisticText);
	else
		rowdata.push(statisticTitle);
		
	for(var k = 1; k < pagetable.columnLength; k++)
	{
		rowdata.push(enumerate_value(tableData[k-1],statisdicProps[k]));
	}
	
	
	//2, \u5220\u9664\u7edf\u8ba1\u884c
	if(pagetable.getStatisticText == pagetable.rows[pagetable.rows.length - 1].cells[0].innerText || statisticTitle == pagetable.rows[pagetable.rows.length - 1].cells[0].innerText)
	{
		pagetable.deleteRow(length_rows);
	}
	
	//3, \u65b0\u5efa\u7edf\u8ba1\u884c
	if(statisticTitle == null || statisticTitle == "")
	{
		//\u91c7\u7528\u9ed8\u8ba4\u7684\u65b9\u5f0f
		var statisticRow = pagetable.insertRow(-1);
	    if (statisticRow.rowIndex % 2 == 0)
	    {
	        statisticRow.className = "even";
	    }
		
		var cells = tableModel_arr[tid].cells;//\u6a21\u578b\u8868\u5217
		for (var j = 0; j < cells.length; j++)
	    {
	        var cell = statisticRow.insertCell(-1);
	        cell.innerHTML = rowdata[j];
	        
	        //\u8bbe\u7f6eTD\u6837\u5f0f
	        assignDataCSS(cells[j], cell);
	    }
	    reSetTableRowNum(tid, index);
    }
    else
    	eval(customTRString);
    	
    pagetable= tableData= statisdicProps= rowdata= cells= null;
}

function PageZTable(tid)
{
	var pageTable = new Object();
	pageTable = tableModel_arr[tid].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var cells = tableModel_arr[tid].cells;
	var cells_length = cells.length;
	
	var statisticEnabled =  tableModel_arr[tid].vstatcaption;
	pageTable.columnLength = cells.length;
	pageTable.data = tableModel_arr[tid].pageData;//\u8868\u683c\u6570\u636e
	pageTable.getStatisticText = tableModel_arr[tid].vstext;
	
	//\u83b7\u53d6\u9875\u9762\u8868\u683c\u7684\u67d0\u4e00\u5217, \u4f46\u6392\u9664\u7edf\u8ba1\u884c\u5355\u5143\u683c
	pageTable.getColumnCells = function(mm)
	{
		var tb_arr = new Array();
		
        var rows = pageTable.rows;
        for (var i = 1; i < rows.length; i++)
        {
            var row = rows[i];
            tb_arr.push(row.cells[mm]);
        }
        
        if(statisticEnabled) tb_arr.pop();
        
        return tb_arr;
	}
	//\u83b7\u53d6\u6307\u5b9a\u5217\u4e2dcheckbox, radio\u88ab\u9009\u4e2d\u7684index\u6570\u7ec4
	pageTable.getCheckedIndexs = function(mm)
	{
		var tb_arr = new Array();
		
        var rows = pageTable.rows;
        for (var i = 1; i < rows.length; i++)
        {
            var row = rows[i];
            var inputArr = row.cells[mm].getElementsByTagName('INPUT');
            if(inputArr[0].checked == true)
            	tb_arr.push(i-1);
        }
        
        return tb_arr;
	}
	
	pageTable.getStatisticProps = function()
	{
		var statisticProps = new Array();
		for(var i = 0; i < cells.length; i++)
		{
			statisticProps.push(cells[i].getAttribute('vstatformula'));
		}
		
		return statisticProps;
	}
	
	pageTable.getCell = function(rowIndex, colIndex)
	{
		var rows = pageTable.rows;
		return rows[rowIndex].cells[colIndex];
	}
	/*
	//\u83b7\u53d6\u6307\u5b9a\u5217\u7684\u6240\u6709TD,\u5305\u542b\u8868\u5934
	pageTable.getColumnCells = function(colIndex)
	{
		var rtnValue = new Array();
		var rows = pageTable.rows;
		
		//\u53d6\u5f97\u6570\u636e\u6570\u636e\u7b2c\u4e00\u5217\u88ab\u9009\u4e2dindex
	    var checkedIndexArr = pageTable.getCheckedIndexs(0);
	    var datalength = pageTable.data.length;
	    //\u5982\u679c\u6ca1\u6709\u9009\u5b9a\u884c,\u5219\u8fd4\u56de\u5168\u90e8\u5217\u6570\u636e
	    if(checkedIndexArr.length == 0)
	    {
	    	for(var i=0; i<rows.length; i++)
			{
				rtnValue.push(rows[i].cells[colIndex]);
			}
	    }
	    else
	    {
	    	for(var i=0; i<checkedIndexArr.length; i++)
			{
				rtnValue.push(rows[checkedIndexArr[i]].cells[colIndex]);
			}
	    }
		
		return 	rtnValue;	
	}
	*/
	//\u83b7\u53d6\u6307\u5b9a\u5217\u7684\u503c
	pageTable.getColumnCellsValues = function (tid, td_index)
	{
	    var rtnValue = new Array();
	    //\u53ef\u80fd\u901a\u8fc7outerHTML\u5b9a\u4f4d\u54ea\u4e00\u5217\u4f7f\u7528\u4e86checkbox,\u6682\u65f6\u8ba4\u4e3a\u7b2c\u4e00\u5217\u4e3acheckbox\uff0c\u9700\u6539\u8fdb
	    
	    //\u53d6\u5f97\u6570\u636e\u6570\u636e\u7b2c\u4e00\u5217\u88ab\u9009\u4e2dindex
	    var checkedIndexArr = pageTable.getCheckedIndexs(0);
	    
	    //\u5982\u679c\u6ca1\u6709\u9009\u5b9a\u884c,\u5219\u8fd4\u56de\u5168\u90e8\u5217\u6570\u636e
	    if(checkedIndexArr.length == 0)
	    {
	    	for(var m=0; m<pageTable.rows.length-1; m++) 	{
	    		checkedIndexArr.push(m);
	    	}
	    }
	    
	    var checkedIndex;
	    var targetCell;
	    for(var i=0; i<checkedIndexArr.length; i++)
	    {
	    	checkedIndex =  checkedIndexArr[i];
	    	//getCell\u65b9\u6cd5\u8fd4\u56de\u6307\u5b9a\u7684td,\u5305\u542b\u8868\u5934
	    	targetCell = pageTable.getCell(checkedIndex+1, td_index);
	    	
	    	//\u53ea\u6709\u6587\u672c\u8282\u70b9,\u4e0d\u5305\u542b\u63a7\u4ef6
	    	if(targetCell.childNodes[0].nodeType == 3)
	    	{
	    		if(isIE()) 
	    			rtnValue.push(targetCell.innerText);
	    		else
	    			rtnValue.push(targetCell.textContent);
	    	}
	    	//\u6709\u63a7\u4ef6\u7684\u60c5\u51b5
	    	else
	    	{
	    		var targetCtr = targetCell.childNodes[0];
	    		switch(targetCtr.tagName)
	    		{
	    			case "SELECT":
	    				rtnValue.push(targetCtr[targetCtr.selectedIndex].value);
	    				break;
	    			case "INPUT":
	    				rtnValue.push(targetCtr.value);
	    				break;
	    			case "A":
	    				if(isIE())
	    					rtnValue.push(targetCtr.innerText);
	    				else
	    					rtnValue.push(targetCtr.textContent);
	    				break;
	    		}
	    	}
	    }
	     return rtnValue;
	}
	
	return pageTable;
}

function enumerate_value(tableData, vstat)
{
	if(vstat == null || vstat == 'null') return "";
    var rtn = "";
    var arr = tableData;
   
    switch (vstat.toUpperCase()) {
        case "MAX":
            arr.sort();
            rtn = arr[arr.length - 1];
            break;
        case "MIN":
            arr.sort();
            rtn = arr[0];
            break;
        case "AVG":
            var sum = 0;
            for (var i = 0; i < arr.length; i++)
            {
                sum += arr[i];
            }
            rtn = sum / arr.length;
            break;
        case "SUM":
            var sum = 0;
            for (var i = 0; i < arr.length; i++)
            {
                sum += parseFloat(arr[i]);
            }
            rtn = sum;
            break;
        default:
            break;
    }
    return rtn;
}

//\u8bbe\u7f6e\u884c\u7684css
var setRowCss = function(row, rowClass, index)
{
    var num = row.rowIndex;
    if (index != null)
    {
        num = index + 1;
    }
    if (num % 2 == 0)
    {
        row.className = ROW_CLASS_EVEN;
    } else {
        row.className = "";
    }
};

pushData2ssbTable = function(MtableId, tableData)
{
    var tObject = tableModel_arr[MtableId].table
   
   //\u521d\u59cb\u5316sortCol\u548c\u6392\u5e8f\u6309\u94ae\u56fe\u6807
    if (tableModel_arr[MtableId].table.sortCol != null && tableModel_arr[MtableId].table.sortCol.constructor == Number)
    {
        var titleRow = tableModel_arr[MtableId].table.children[0].children[0];
        var imgs = titleRow.getElementsByTagName('IMG');
        for (var j = 0; j < imgs.length; j++)
        {
            if (isIE())
                imgs[j].className = "IEsortASC";
            else
                imgs[j].className = "FFsortASC";
        }
    }
    tableModel_arr[MtableId].table.sortCol = "";
    tableModel_arr[MtableId].table.softflag = "";
   
   //\u63d2\u5165\u6570\u636e\u524d\u6e05\u7a7a\u8868\u4e2d\u6570\u636e.
    var rows = tObject.rows.length;//\u5f53\u524d\u8868\u7684\u884c\u6570
    for (var j = rows; j > 1; j --)
    {
        tObject.deleteRow(j - 1);
    }
  //\u5faa\u73af\u63d2\u5165\u7a7a\u767d\u884c.
    for (var i = 0; i < tableData.length; i ++)
    {
        var row = InsertRow(tObject, MtableId);
        setRowCss(row, ROW_CLASS_EVEN);
        bindButtonEvent2Ctrl(row, MtableId);
    }
	
	//$(tableModel_arr[MtableId].table).find("tr").hover(	function(){ $(this).addClass("trHover")}, 	function(){$(this).removeClass("trHover") }	);
  // \u7ed9\u8868\u7684\u884c\u586b\u5145\u6570\u636e
    bindData2TableCtrl(MtableId, tableData);
};

//\u8868\u683c\u4e2d\u9f20\u6807\u70b9\u51fb\u4e8b\u4ef6\u5904\u7406
function bindButtonEvent2Ctrl(rowObj, Mtid)
{
    var model = tableModel_arr[Mtid];
    if (model.leftButtonEvent && model.rightButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 1)
                parseFunction(model.leftButtonEvent, rowObj);
            else if (event.button == 2)
                parseFunction(model.rightButtonEvent, rowObj);
        }
    }
    else if (model.leftButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 1)
                parseFunction(model.leftButtonEvent, rowObj);
        }
    }
    else if (model.rightButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 2)
                parseFunction(model.rightButtonEvent, rowObj);
        }
    }
    if (model.dbClickEvent)
    {
        rowObj.ondblclick = function() {
            parseFunction(model.dbClickEvent, rowObj);
        }
    }

}
;

var parseFunction = function (defaultParm, obj)
{
    var def_parms = defaultParm;
    if (def_parms != "") {
        var str_val = unescape(defaultParm);
        var ssbfunction = str_val;
        if (str_val.indexOf('this') != -1) {
            eval("var obj = obj");
            var ssbfunction = str_val.replace("this", "obj");
        }
        return eval(ssbfunction);
    }
};

var TIME_OFFSET = new Date().getTimezoneOffset() * 60 * 1000;//\u65f6\u533a\u5dee
var TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
getTimefromUTC = function(format, utcTime)
{
    if (utcTime == null || utcTime == '')
    {
        return '';
    }
    var utc_number = utcTime;
    var nd = new Date(utc_number);
    var yy = "-";
    var mm = "-";
    var _HH_MM_SS = "";
    if (format != null)
    {
        format = format.toLowerCase();
        var y_index = format.indexOf("y");
        var y_endindex = format.lastIndexOf("y");
        yy = format.substring(y_endindex + 1, y_endindex + 2);
        var m_index = format.indexOf("m");
        mm = format.substring(m_index + 2, m_index + 3);
        var hour = new String(nd.getHours()).length > 1 ? nd.getHours() : "0" + nd.getHours();
        var minutes = new String(nd.getMinutes()).length > 1 ? nd.getMinutes() : "0" + nd.getMinutes();
        var seconds = new String(nd.getSeconds()).length > 1 ? nd.getSeconds() : "0" + nd.getSeconds();

        if (format.indexOf("hh") != -1)
        {
            _HH_MM_SS = _HH_MM_SS + " " + hour;
        }
        if (format.lastIndexOf("mm") != m_index)
        {
            if (_HH_MM_SS == "")
                _HH_MM_SS = _HH_MM_SS + " " + minutes;
            else
                _HH_MM_SS = _HH_MM_SS + ":" + minutes;
        }
        if (format.indexOf("ss") != -1)
        {
            if (_HH_MM_SS == "")
                _HH_MM_SS = _HH_MM_SS + " " + seconds;
            else
                _HH_MM_SS = _HH_MM_SS + ":" + seconds;
        }
    }
    var month = nd.getMonth() + 1 + "";
    if (month.length == 1)
    {
        month = "0" + month;
    }
    var date = nd.getDate() + "";
    if (date.length == 1)
    {
        date = "0" + date;
    }
    var time = nd.getFullYear() + yy + month + mm + date + _HH_MM_SS;
    return time;
};


getUTCfromTime = function(format, time)
{
    if (time == null || time == '')
    {
        return null;
    }
    var utc = "";
    var yy = "-";
    var mm = "-";
    var date_format = new Array();
    var date_format1 = "";
    var date_format2 = "";
    var time_arr = new Array();
    var time1 = time;
    var time2 = "";
    if (format != null)
    {
        if (format.length > 10)
        {
            date_format = format.split(" ");
            date_format1 = date_format[0];
            date_format2 = date_format[1];
            time_arr = time.split(" ");
            time1 = time_arr[0];
            time2 = time_arr[1];
        } else {
            date_format1 = format;
            time1 = time;
        }
        var y_index = date_format1.indexOf("y");
        var y_endindex = date_format1.lastIndexOf("y");
        yy = date_format1.substring(y_endindex + 1, y_endindex + 2);

        var m_index = date_format1.indexOf("m");
        var m_endindex = date_format1.lastIndexOf("m");
        mm = date_format1.substring(m_endindex + 1, m_endindex + 2);
    }
    var YM = time.substring(time.indexOf(yy), time.indexOf(yy) + 1);
    var MD = time.substring(time.indexOf(mm), time.indexOf(mm) + 1);
    time1 = time1.replace(YM, ',');
    time1 = time1.replace(MD, ',');
    var tt = time1.split(",");
    if (time2.length < 1) {
        var date = new Date(tt[0], parseInt(tt[1] - 1), tt[2]);
        utc = date.getTime();
    } else {
        var HH = time2.split(":");
        var date = new Date(tt[0], parseInt(tt[1] - 1), tt[2], HH[0], HH[1], HH[2]);
        utc = date.getTime();
    }
    return parseInt(utc);
};

var SSB_RIA_CALENDAR = "../images/calendar.gif";
Calendar_Ctr_Processor = function(tagObject, value, rowindex, MtableId)
{
    var _onchange_event = (tagObject.getAttribute("onchange") != undefined) ? tagObject.getAttribute("onchange") : "";
    var inputTarget = tagObject.getAttribute('target') + rowindex;	//\u83b7\u53d6\u5185\u5b58\u8f93\u5165\u6846\u7684ID
    var date_formate = tagObject.getAttribute('formate'); //\u83b7\u53d6\u65e5\u671f\u683c\u5f0f
    var inputStyle; //\u8f93\u5165\u6846\u7684\u6837\u5f0f\u8bbe\u7f6e, \u6bd4\u5982width,color\u7b49
    if(isIE())
		inputStyle = tagObject.getAttribute('style').cssText;
	else
		inputStyle = tagObject.getAttribute('style');
		
    var mid = tagObject.getAttribute('id');
    var imgurl = tagObject.getAttribute("imgurl");
    var _calendar_WIDTH = tagObject.getAttribute("width");
    var _calendar_size = "16";
    var inputTarget_show = inputTarget + "_" + mid;
    if (_calendar_WIDTH != null && _calendar_WIDTH != "")
    {
        _calendar_size = _calendar_WIDTH;
    }
    if (imgurl == null || imgurl == "")
    {
        imgurl = SSB_RIA_CALENDAR;
    }
    var nullflag = 'yes';
    var nullable = tagObject.getAttribute('nullable');
    if (nullable != null && nullable != '')
    {
        nullflag = 'no';
    }
	
   	//\u5c06\u65e5\u671f\u7c7b\u578b\u5b58\u5165\u5168\u5c40\u6a21\u578b
    if (tableModel_arr[MtableId].date_formate == null || tableModel_arr[MtableId].date_formate == '')
    {
        tableModel_arr[MtableId].date_formate = date_formate;
    }
    var time = getTimefromUTC(date_formate, value);
    var Hms = time;
    var type1 = "text";
    var type2 = "hidden";
    var html = '<INPUT type="' + type2 + '" id="' + inputTarget + "_" + mid + '" readonly="readonly" ';
    html += ' value="' + Hms + '"  size="' + _calendar_size + '" isDate="false"/> ';
    html += '<INPUT type="' + type1 + '" id="' + inputTarget + '" style="' + inputStyle + '" name="' + inputTarget + '"  onchange="' + _onchange_event + '"';
    html += 'readonly= true  value="' + time + '" isDate="true" size="' + _calendar_size + '"/>&nbsp;';
    if (tagObject.getAttribute("isFullDate") == "false") {
        type1 = "hidden";
        type2 = "text";
        Hms = time.split(' ')[1];
        Hms = Hms || " ";
        inputTarget_show = inputTarget + "_" + mid;
        html = '<INPUT type="' + type1 + '" id="' + inputTarget + '" name="' + inputTarget + '"  onchange="' + _onchange_event + '"';
        html += 'readonly= true  value="' + time + '" isDate="true" size="' + _calendar_size + '">';
        html += '<INPUT type="' + type2 + '" id="' + inputTarget_show + '" readonly="readonly" ';
        html += ' value="' + Hms + '"  size="' + _calendar_size + '" isDate="false"/>&nbsp; ';

    }
    html += '<img style="cursor:pointer;" id="calendar_img_id" border="0" src="' + imgurl + '" width="16" height="15"  ';
    html += 'onClick="fPopCalendar(\'' + inputTarget + '\',\'' + inputTarget + '\',\'' + date_formate + '\',\'' + inputTarget_show + '\');return false"> ';
    return html;
};

Lov_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    var targetId = retagObject.getAttribute('targetCompId');
    var jsevent = retagObject.getAttribute('jsEventId');
    if (jsevent && (jsevent != "" || jsevent != null)) {
        retagObject.setAttribute('jsEventId', retagObject.getAttribute('jsEventId') + rowindex);
    }
    else {
        retagObject.setAttribute('jsEventId', "");
    }
    var nid_arr = targetId.split(",");
    for (var i = 0; i < nid_arr.length; i++)
    {
        nid_arr[i] = nid_arr[i] + rowindex;
    }
    var ntargetId = nid_arr.join(",");
    retagObject.setAttribute('targetCompId', ntargetId);
    return retagObject;
};

autoComplete_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    var nctrId = retagObject.getAttribute('ctrlId') + rowindex;
    var nId = retagObject.getAttribute('id') + rowindex;
    retagObject.setAttribute('ctrlId', nctrId);
    retagObject.setAttribute('id', nId);
    return retagObject;
};

radioGroup_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    retagObject.innerHTML = '';
    return retagObject;
};

upload_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    return retagObject;
};
 
//\u8ba9FF\u4e5f\u80fd\u652f\u6301outerHTML
function FFOuterHTML()
{
    if (window.HTMLElement) {
        HTMLElement.prototype.__defineSetter__("outerHTML", function(sHTML) {
            var r = this.ownerDocument.createRange();
            r.setStartBefore(this);
            var df = r.createContextualFragment(sHTML);
            this.parentNode.replaceChild(df, this);
            return sHTML;
        });

        HTMLElement.prototype.__defineGetter__("outerHTML", function() {
            var attr;
            var attrs = this.attributes;
            var str = "<" + this.tagName.toLowerCase();
            for (var i = 0; i < attrs.length; i++) {
                attr = attrs[i];
                if (attr.specified)
                    str += " " + attr.name + '="' + attr.value + '"';
            }
            if (!this.canHaveChildren)
                return str + ">";
            return str + ">" + this.innerHTML + "</" + this.tagName.toLowerCase() + ">";
        });

        HTMLElement.prototype.__defineGetter__("canHaveChildren", function() {
            switch (this.tagName.toLowerCase()) {
                case "area":
                case "base":
                case "basefont":
                case "col":
                case "frame":
                case "hr":
                case "img":
                case "br":
                case "input":
                case "isindex":
                case "link":
                case "meta":
                case "param":
                    return false;
            }
            return true;
        });
    }
}
//\u8ba9FF\u652f\u6301IE\u7684innerText\u5c5e\u6027
function FF_innerText()
{	
	HTMLElement.prototype.__defineGetter__("innerText",
    function(){
        var anyString = "";
        var childS = this.childNodes;
        for(var i=0; i<childS.length; i++) {
            if(childS[i].nodeType==1)
                anyString += childS[i].innerText;
            else if(childS[i].nodeType==3)
                anyString += childS[i].nodeValue;
        }
        return anyString;
    });
    HTMLElement.prototype.__defineSetter__("innerText",
    function(sText){
        this.textContent=sText;
    });
}
//\u8ba9FF\u652f\u6301IE\u7684children\u5c5e\u6027
function FF_children()
{
	HTMLElement.prototype.__defineGetter__("children", 
	function () 
	{ 
	 var returnValue = new Object(); 
	 var number = 0; 
	 for (var i=0; i<this.childNodes.length; i++) { 
		 if (this.childNodes[i].nodeType == 1) { 
			 returnValue[number] = this.childNodes[i]; 
			 number++; 
		 } 
	 } 
	 returnValue.length = number; 
	 return returnValue; 
	});
}

//\u8868\u8fbe\u5f0f\u5904\u7406\u5668
function Express_Factory(value, data, isAttribute)
{
	//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	this.paramConvert = function(value)
	{
		var exps = value.match(/\${.+?}/g); 
		for(var i=0; i<exps.length; i++)
		{				
			var loc = value.indexOf(exps[i]) ;
			if( value.charAt(loc - 1) != "'")
			{
				if(value.charAt(loc - 1) != "\"")
				{
					value = value.replace(exps[i], "'"+exps[i]+"'");
				}					
			}
		}
		return value;	
	}
	
	//\u53d6\u5f97\u8868\u8fbe\u5f0f\u7684\u503c
	this.getExpValue = function(prop)
	{
		var levels = prop.split(".");
    	var datastring = "data";
    	for(var j=0; j<levels.length; j++)
    	{
    		datastring += "."+levels[j];
    	}
    	
   		return eval(datastring);
	}
	
	this.processor = function()
	{
		if(value != null && value != '')
 		{
			if(isAttribute)
			{
				//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
		    	if(value.indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
		    	{
		    		if(value.indexOf("${") != -1)
		    			value = this.paramConvert(value);
		    	}
		    	else if(value.indexOf("${") == -1)
		    	{
		    		//\u65e2\u6ca1\u6709\u62ec\u53f7\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f\uff0c \u76f4\u63a5\u9000\u51fa
	    			return value;
		    	}
			}
 			
	 		do
	 		{
			    var start = value.indexOf("${");
			    if(start != -1)
			    {
			    	var old = value.substring(start, value.indexOf("}")+1);	//\u6bd4\u5982\uff1a${xxxx.xxx}
			    	var prop = old.substring(2,old.length-1);				//\u6bd4\u5982\uff1axxxx.xxx
			    	prop = this.getExpValue(prop);
			    	value = value.replace(old, prop);
				}
			}
			while(value.indexOf("${") != -1);
			
			if(isAttribute)
			{
				if(value.indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
		    	{
	    			value = eval(value);
		    	}
			}
		}
		
 		return  value;
	}
	
	return this;
}

getOuterHtml = function(retagObject, data, usermethod, pp)
{
     //\u5982\u679c\u662f\u6587\u672c\u8282\u70b9\uff0c\u68c0\u67e5\u8282\u70b9\u503c\u662f\u5426\u662f\u8868\u8fbe\u5f0f\uff0c \u5982\u679c\u662f\u9700\u4f5c\u5904\u7406
 	if(retagObject.nodeName == "#text") 
 	{ 
		if(Trim(retagObject.nodeValue) != "")
		{
 			retagObject.nodeValue = new Express_Factory(retagObject.nodeValue, data).processor();
 		}
 		return;
 	}
 	
 	//\u4ece\u6700\u5e95\u5c42\u5f00\u59cb\u5904\u7406
    for(var j = 0; j < retagObject.childNodes.length; j++) 
    {
    	getOuterHtml(retagObject.childNodes[j], data, usermethod, pp);
    }
    
	//\u8ba9FF\u4e5f\u80fd\u652f\u6301outerHTML
   	if(!isIE()) 	FFOuterHTML();
	
	//\u901a\u8fc7outerHTML\u83b7\u53d6\u5c5e\u6027\u5217\u8868
	var attrs = retagObject.outerHTML.match(/\s+\w+=/g); 
	//\u65e0\u5c5e\u6027\u5904\u7406	
	if(attrs == null) 	return retagObject;
	for(var i=0;i<attrs.length;i++)
	{
		attrs[i] = $.trim(attrs[i]);
		attrs[i] = attrs[i].substring(0,attrs[i].length-1);
	}
	
    
	//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	var paramConvert = function(value)
	{
		var exps = value.match(/\${.+?}/g); 
		for(var i=0; i<exps.length; i++)
		{				
			var loc = value.indexOf(exps[i]) ;
			if( value.charAt(loc - 1) != "'")
			{
				if(value.charAt(loc - 1) != "\"")
				{
					value = value.replace(exps[i], "'"+exps[i]+"'");
				}					
			}
		}
		return value;	
	}
	
	//\u68c0\u67e5\u672c\u5bf9\u8c61\u662f\u5426\u6709\u7528\u6237\u8f6c\u6362\uff0c\u6709\u7684\u8bdd\u4ee5\u6b64\u4e3a\u51c6
	var tempmethod = retagObject.getAttribute('calculate');
	var tempprop = retagObject.getAttribute('prop');
	var isParams = false;
	if(tempmethod != null && tempmethod != "")
	{
		usermethod = tempmethod;
		//\u68c0\u67e5\u81ea\u5b9a\u4e49\u65b9\u6cd5\u662f\u5426\u6709\u53c2\u6570
		if(usermethod.indexOf("(") != -1)
		{
			 if(usermethod.indexOf(")") != -1)				         
			 {				
				contxt = usermethod;				        
				isParams = true;
			 }
			 else
				usermethod = null;
		}
	}
	
	if(tempprop != null && tempprop != "")
		pp = tempprop;
	
	
	//\u53d6\u5f97\u8868\u8fbe\u5f0f\u7684\u503c
	var getExpValue = function(prop)
	{
		var levels = prop.split(".");
		if(data != null && data != "null")
		{
			var datastring = "data";
	    	for(var j=0; j<levels.length; j++)
	    	{
	    		datastring += "."+levels[j];
	    	}
	    	if(isParams)
	    		return eval(datastring);
	    	else
	    		return caculator_processor(usermethod, prop, eval(datastring));
		}
	}
	
	//\u6839\u636e\u5c5e\u6027\u5217\u8868\uff0c\u66ff\u6362\u6389\u8868\u8fbe\u5f0f\uff0c\u8fd9\u4e00\u6b65\u5bf9\u6240\u6709\u5c5e\u6027\u4ee5\u53ca\u5185\u7f6e\u6587\u672c\u8d4b\u503c\uff0c\u5e76\u6ca1\u6709\u5904\u7406\u4e8b\u4ef6
 	for(var i = 0; i < attrs.length; i ++)
 	{ 		
 		var attrname = attrs[i];
		//\u6ce8\u610fINPUT\u5e76\u4e0d\u76f4\u63a5\u652f\u6301Width\u5c5e\u6027\uff0c\u7528\u6237\u8bbe\u7f6eWidth\u5fc5\u987b\u7528Style\uff1b\u76f4\u63a5\u8df3\u8fc7
		if(attrname.toUpperCase() == "WIDTH") continue;
		if(attrname.toUpperCase() == "STYLE") continue;
		if(attrname.toUpperCase() == "TYPE") continue;
		if(attrname.toUpperCase() == "PROP") continue;
		
 		//\u53d6\u51fa\u6bcf\u4e2a\u5c5e\u6027\u7684\u503c\uff0c\u5305\u62econclick\uff01
 		var value = decodeURIComponent(retagObject.getAttribute(attrname));
 		
		//modify by wanghao		
 		if(value.indexOf("about:") == 0)
 		{
 			if(value.indexOf("about:blank") == 0)
 				value = value.substr(11);
 			else
 				value = value.substr(6);
 		}
 					
 		//\u652f\u6301label\u91cc\u9762\u7684\u5c5e\u6027\uff0c\u6bd4\u5982title\u7b49
 		//if(retagObject.nodeName.toUpperCase() == "LABEL") 	  value = retagObject.innerHTML;
 		//\u5faa\u73af\u66ff\u6362\u8868\u8fbe\u5f0f\u7684\u503c
 		if(value != null && value != ''&& typeof value == 'string')
 		{
 			//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	    	if(Trim(value).indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
	    	{
	    		if(value.indexOf("${") != -1)
	    			value = paramConvert(value);
	    	}
	 		do
	 		{
			    var start = value.indexOf("${");
			    if(start != -1)
			    {
			    	var old = value.substring(start, value.indexOf("}")+1);	//\u6bd4\u5982\uff1a${xxxx.xxx}
			    	var prop = old.substring(2,old.length-1);				//\u6bd4\u5982\uff1axxxx.xxx
			    	
			    	value = value.replace(old,getExpValue(prop));
				}
			}
			while(value.indexOf("${") != -1);
		}
 		var onstr = attrname.substring(0,2);
 		//\u5bf9\u4e8b\u4ef6\u5c5e\u6027\u7684\u8bbe\u5b9a, \u5168\u9762\u652f\u630120\u4e2ajs\u4e8b\u4ef6\uff0c\u5bf9IE\u548cFF\u7684\u5904\u7406\u65b9\u5f0f\u4e0d\u540c
 		if(onstr.toUpperCase()=="ON")
 		{
	 		if(isIE())
	 		{
	 			if(value.indexOf("{") != -1)
	 				value = value.substring(value.indexOf("{")+2,value.length-2);
	 			retagObject.setAttribute(attrname,value);
	 		}
			else
			{
 				retagObject.setAttribute(attrname,value);
 			}
 		}
 		else
 		{
 		retagObject.setAttribute(attrname,value);
 		}
 	}
 	//\u4e8b\u4ef6\u7ed1\u5b9a, \u5168\u9762\u652f\u630120\u4e2ajs\u4e8b\u4ef6\uff0c\u5bf9IE\u548cFF\u7684\u5904\u7406\u65b9\u5f0f\u4e0d\u540c
 	/*
	var eventNameArray = attrs.toString().match(/on\w+/g); 
	var eventContentArray = new Array();
	if(eventNameArray != null)
	{	
		if(isIE()) 	retagObject.event = eventContentArray;
		for(var i=0; i<eventNameArray.length; i++)
		{
			eval('eventContentArray.' + eventNameArray[i] + "= retagObject.getAttribute(eventNameArray[i]).toString()");
			if(isIE())
			{	
				retagObject.setAttribute(eventNameArray[i],function()
				{	
					var eventProp = eval("retagObject.event.on"+window.event.type);
					eval(eventProp);window.event.cancelBubble=true;
					if(retagObject.tagName.toUpperCase() == "A")
						window.event.returnValue=false;
				});
			}else{
				retagObject.addEventListener(eventNameArray[i].substr(2),function(e)
				{
				e.stopPropagation();
				if(retagObject.tagName.toUpperCase() == "A")
					e.preventDefault();
				}, false);
			}
			
		}
	}
 	*/
 	
	var eventNameArray = attrs.toString().match(/on\w+/g); 
	if(eventNameArray != null)
	{			
		for (var i = 0; i < eventNameArray.length; i++) 
		{
			//\u95ed\u5305\u4fdd\u5b58\u4e8b\u4ef6\u540d\u548c\u8981\u6267\u884c\u7684\u4e8b\u4ef6\u7684\u5b57\u7b26\u4e32
			(function(){
				var evstr = retagObject.getAttribute(eventNameArray[i]);
				var evname = eventNameArray[i].substring(2);
				//\u91c7\u7528EventManager\u6765\u7ba1\u7406\u4e8b\u4ef6\u7ed1\u5b9a
				EventManager.Add(retagObject, evname, function(ev){
					var tmp = evstr.replace(/(\W)(this)(\W)/gi, "$1retagObject$3");
					if(isIE()) eval(tmp); 
					if(retagObject.tagName.toUpperCase() == "A") //\u53d6\u6d88\u8868\u683c\u4e2d\u8d85\u94fe\u63a5\u7684\u9ed8\u8ba4\u884c\u4e3a, \u5220\u9664\u884c\u65f6\u4e0d\u4f1a\u8dd1\u5230\u9875\u5934
					{
						if(!isIE())
							ev.preventDefault();
						else
							window.event.returnValue=false;
					}
				});
			})();
		}
	}
 	
 	if(retagObject.tagName.toUpperCase() != "SELECT")
 	{ 	
        var text = "";
		if(isIE())
			text = retagObject.innerText;
		else
			text = retagObject.textContent;
		
		if(text != '')
		{
			var start = text.indexOf("${");
		    //\u5982\u679c\u6709\u53c2\u6570
		    if(start != -1)
		    {
		    	var end = text.indexOf("}")+1;
		    	var old = text.substring(start, end);
		    	var prop = text.substring(start+2,end-1);
		    	
			    text =text.replace(old,getExpValue(prop));
				//modify by wanghao
				innerText_function(retagObject, text);
			}
			
		}
 	}
 	//\u63a7\u4ef6\u4e2d\u6709calculate\u7528\u6237\u81ea\u5b9a\u4e49\u65b9\u6cd5\u7684\u7edf\u4e00\u5904\u7406
 	if(usermethod != null && usermethod != "")
 	{	
 		var isCal = false;
 		//1, \u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5, \u5e76\u4e14\u6709\u53c2\u6570
 		if(isParams)
	 	{
	 		usermethod = retagObject.getAttribute('calculate');
	 		value = caculator_processor(usermethod);
	 		isCal = true;
	 	}
	 	//2, \u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5,\u4f46\u6ca1\u6709\u53c2\u6570
	 	else if(tempmethod != null && tempmethod != "")
	 	{
			//\u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86prop\u5c5e\u6027
			if(pp != null && pp != "")
			{
				value = getExpValue(pp);
				isCal = true;
			}
			//\u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49prop\u5c5e\u6027, \u68c0\u67e5\u5728\u5217\u4e2d\u662f\u5426\u5b9a\u4e49\u4e86prop
			else if(prop != null && prop != "")
			{
				value = getExpValue(prop);
				isCal = true;
			}
	 	}
	 	//3, \u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5
	 	else
	 	{
			//\u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86prop\u5c5e\u6027
			if(pp != null && pp != "")
			{
				value = getExpValue(pp);
				isCal = true;
			}
			//\u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49prop\u5c5e\u6027, \u68c0\u67e5\u5728\u5217\u4e2d\u662f\u5426\u5b9a\u4e49\u4e86prop
			else if(prop != null && prop != "")
			{
				value = getExpValue(prop);
				isCal = true;
			}
	 	}
	 	
	 	if(isCal)
	 	{
	 		switch(retagObject.tagName.toUpperCase())
	 		{
	 			case "INPUT" : 
	            	if(isIE())
	 					retagObject.setAttribute('value',value);
		 			else
		 			{	//FF\u4e0d\u652f\u6301\u5728outerHTML\u8bbe\u7f6e\u7684\u5c5e\u6027\u503c
		 				retagObject.setAttribute('value',value);
		 				//retagObject['value'] = value;
		 			}
	            	break;
	            	
				case "LABEL" :
	        	case "A" : 
	             	retagObject.innerHTML = value;
	            	break;
	 		}
	 	}
 	}
 	
  	  	return retagObject;
};

select_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, data)
{
    var option_values = new Array();
    var option_texts = new Array();
    var selectValue = '';
    var ctrlex = reTagObject.getAttribute("ctrlex");		   
   //\u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027
    if (ctrlex != null)
    {
        reTagObject.innerHTML = "";
        var ctrlarr = reTagObject.getAttribute("ctrlex").split(';');//\u83b7\u53d6\u6269\u5c55\u5c5e\u6027
        for (var i = 0; i < ctrlarr.length; i ++)
        {
            var arr = ctrlarr[i].split(':');
            if (arr.length == null)
            {
                break;
            }
	   	  //\u7ed9\u9009\u9879\u7684text\u548cvalue\u8d4b\u503c
            switch (arr[0].toUpperCase())
                    {
                case 'TEXT' :
                    var text_arr = new Array();
                    text_arr = arr[1].split('.');
                    var text_arr_options = evalParse(data, text_arr[0]);
                    text_arr_options = evalParse(text_arr_options, text_arr[1]);
                    for (var m = 0; m < text_arr_options.length; m++)
                    {
                        option_texts[m] = evalParse(text_arr_options[m], text_arr[2]);
                    }

                    break;

                case 'VALUE' :
                    var value_arr = new Array();
                    value_arr = arr[1].split('.');
                    var value_arr_options = evalParse(data, value_arr[0]);
                    value_arr_options = evalParse(value_arr_options, value_arr[1]);
                    for (var n = 0; n < value_arr_options.length; n++)
                    {
                        option_values[n] = evalParse(value_arr_options[n], value_arr[2]);
                    }
                    break;

                case 'DEFAULT' :

                    var select_arr = arr[1].split('.');
                    selectValue = evalParse(data, select_arr[0]);
                    selectValue = evalParse(selectValue, select_arr[1]);
                    break;

                default :

                    break;
            }//end switch
        }//end for

        //\u521b\u5efa\u4e0b\u62c9\u9009\u9879
        for (var i = 0; i < option_values.length; i++)
        {
            var option = document.createElement("option");
            option.value = option_values[i];

            option.innerHTML = option_texts[i];
		  
	   	  //\u8bbe\u7f6e\u9009\u4e2d\u9879
            if (option_values[i] == selectValue)
            {
                option.selected = "selected";
            }
            reTagObject.appendChild(option);
        }

    }//end if \u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027
    else {//\u5982\u679c\u6ca1\u6709\u6269\u5c55\u5c5e\u6027
        var sel_children = reTagObject.getElementsByTagName("option");
     //\u5c06\u63a7\u4ef6\u7684prop\u5c5e\u6027\u503c\u548c\u4e0b\u62c9\u9009\u9879\u7684value\u503c\u5bf9\u6bd4\uff0c\u5982\u679c\u76f8\u7b49\u5219\u9009\u4e2d
     	/*
        for (var j = 0; j < sel_children.length; j ++)
        {
        	//\u6655\uff0c\u5b83\u90fd\u4e0d\u5224\u65ad\u662f\u5426\u6709prop\u503c 2008-09-26
        	if(sel_children[j].getAttribute('prop') && sel_children[j].getAttribute('prop') != "")
	       	{
	       		if (new String(value) == sel_children[j].value)//\u5982\u679cprop\u5c5e\u6027\u503c\u548coption.value\u76f8\u7b49
	            {
	                sel_children[j].selected = "selected"; break;
	            }
	       	}
	       	else if (new String(value) == sel_children[j].value)//\u5f53option\u6ca1\u6709prop\u5c5e\u6027\u65f6, \u4f20\u9012\u8fc7\u6765\u7684value\u8fdb\u884c\u6bd4\u8f83
            {
                sel_children[j].selected = "selected";break;
            }
        }
        */
        for (var j = 0; j < sel_children.length; j ++)
        {
            if (new String(value) == sel_children[j].value)//\u5982\u679cprop\u5c5e\u6027\u503c\u548coption.value\u76f8\u7b49
            {
                sel_children[j].selected = "selected";
            }
        }
    }
    return reTagObject;
};

var A_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, data)
{
    var href = String(reTagObject.getAttribute('href'));
    //\u5bf9\u6709about\u7684\u60c5\u51b5\u8fdb\u884c\u5904\u7406
    if(href.indexOf("about:") == 0)
	{
		if(href.indexOf("about:blank") == 0)
			href = href.substr(11);
		else
			href = href.substr(6);
	}
	
    //1, \u5982\u679c\u7528\u6237\u6ca1\u6709\u8bbe\u7f6e\u5177\u4f53href\u8def\u5f84\uff0c\u5219\u9ed8\u8ba4\u4e3a\u4e0d\u8df3\u8f6c
    if(href == null || href == "" || href == "#")
    {
	    href = "#";
    }
    //2, \u5982\u679c\u4ee5'/'\u5f00\u5934\uff0c\u5219\u8ba4\u4e3a\u662f\u91c7\u7528\u4ee5\u524d\u7684\u65b9\u5f0f\uff0c\u5176\u8def\u5f84\u76f8\u5bf9\u4e8ewebcontent\u76ee\u5f55\uff0c\u4ece\u800c\u7ec4\u88c5\u8def\u5f84
    else if(href.charAt(0) == "/")
    {
    	var projectname = document.location.pathname.split('/')[1];
	    href = "http://" + document.location.host + "/" + projectname + href;
    }
    //3, \u9ed8\u8ba4\u7528\u6237\u91c7\u7528\u76f8\u5bf9\u4e8e\u5f53\u524d\u76ee\u5f55\u7684\u65b9\u5f0f\uff0c \u4e5f\u5c31\u662f../../
    reTagObject.setAttribute('href', href);

    return     reTagObject;
};

input_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, useExp)
{
    if ((reTagObject.getAttribute('type') != "button") && (reTagObject.getAttribute('type') != "reset"))
    {
    	//\u5fc5\u987b\u8981value\u6709\u503c\u624d\u8fdb\u884c\u66ff\u6362\uff0c\u5426\u5219\u4f7f\u7528\u539f\u6765\u7684\u503c
    	//\u6ce8\u610f\uff1a\u5982\u679c\u5bf9value\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f\uff0c\u5b83\u7684\u503c\u4f1a\u518d\u6b21\u88ab\u6539\u53d8; \u5982\u679c\u4f7f\u7528\u4e86calculate, \u5219\u503c\u4e0d\u5e94\u8be5\u88ab\u6539\u53d8
    	//val usermethod = reTagObject.getAttribute('calculate');
        if (value == null) value = "";
        //\u5982\u679cvalue\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u5219\u503c\u5e94\u5f53\u4e0d\u53d8
       	if(useExp == -1 && reTagObject.getAttribute('calculate') == null)   
       		reTagObject.value = value;
        addEventForCheckBox(reTagObject, MtableId);
    }
    return reTagObject;
};

function changeColumnTitle(tid, cid, newtitle)
{
	//\u68c0\u67e5\u8be5\u5217\u662f\u5426\u5b58\u5728
	var canBeShown = false;
	for(var t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	if(!canBeShown)
	{
		alert('The column is not existed!');
		return;
	}
	//\u53d6\u5f97index
	for(t=0; t<tableModel_arr[tid].cells.length; t++)
	{
		if (cid == tableModel_arr[tid].cells[t].id)
		{
			break;			
		}
	}
	
    var col = tableModel_arr[tid].cells;    
	var displayTB = tableModel_arr[tid].table;	
	var captions = tableModel_arr[tid].caption;
	var exceltitle = tableModel_arr[tid].excel_title;
	
	//FF\u517c\u5bb9\u5904\u7406
	if(!isIE())
	{						
		FF_innerText();
		FF_children();
	}
	
	var inner = displayTB.rows[0].cells[t].children[0].innerHTML;
	var tmpobj = document.createElement('td');
	
	tmpobj.innerHTML = captions[t];
	
	inner  = eval("inner.replace(/"+tmpobj.innerText+"/g, '"+newtitle+"')");
	//\u9700\u8bbe\u7f6e\u540e\u53f0\u5bf9\u5e94\u5217\u7684caption\u5c5e\u6027
	col[t].setAttribute("caption", inner);
	//\u9700\u8bbe\u7f6e\u6807\u7b7e\u961f\u5217\u4e2d\u5bf9\u5e94\u7684caption\u5c5e\u6027
	captions[t] = inner;	
	//\u4fee\u6539Excel\u5bfc\u51fa\u9875\u9762\u663e\u793a\u7684\u8868\u5934
	exceltitle[t] = newtitle;
	//\u6539\u53d8\u663e\u793a\u7684\u7ed3\u679c
	displayTB.rows[0].cells[t].children[0].innerHTML  = inner;	
	
	col = displayTB = captions = exceltitle = inner = tmpobj = null;
}

function createCaption(tid, cid, prop_caption_index)
{
    var columnIndex = prop_caption_index['index'];
    var col = tableModel_arr[tid].cells;
    var clums = tableModel_arr[tid].clums;
	var displayTB = tableModel_arr[tid].table;	
	//\u8868\u5934\u6837\u5f0f\u548c\u63d0\u793a
	var captiontips = tableModel_arr[tid].captiontips;
	var capstyles = tableModel_arr[tid].capstyles;
	
    var html = '';
    if (clums[columnIndex].getAttribute("checkall") == 'true' && clums[columnIndex].getAttribute("iskey") == 'true')
    {
        html += '<input style="vertical-align:middle;height: 18px;" type="checkbox" onclick="checkedAll(this, ' + tid + ')" name="allChecked" id="' + tid + 'allCheck"/>';
    }
    if (clums[columnIndex].getAttribute("caption") != null)
    {
        var title = clums[columnIndex].getAttribute("caption");
        html += title;
    }

    var objCell = displayTB.rows[0].insertCell(prop_caption_index['preDisplayIndex']);
    var cellSort = clums[columnIndex].getAttribute("allowsort");
    var sortHtml = '';
    var sortClass = null;
    if (isIE())
    {
        sortClass = "IEsortASC";
    } else {
        sortClass = "FFsortASC";
    }
    if (cellSort == 'true')
    {
        sortHtml += '<IMG id="ssbSortImg'+ i +'" class="' + sortClass + '" onclick="sortTable(' + tableModel_arr[tid].tableId + ', ' + columnIndex + ', this);">';
    }

    objCell.setAttribute("width", clums[columnIndex].getAttribute("width"));
    //objCell.setAttribute("style", "TEXT-ALIGN: left");

    html += sortHtml;

    if (columnIndex == parseInt(col.length - 1))//
    {
        objCell.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+"'>" + html + '</SPAN><span></span>';
    } else {
        objCell.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+"'>"  + html + '</SPAN><span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,' + tableModel_arr[tid].tableId + ');"></span>';
        objCell.lastChild.className = "resizeBar";
    }

    objCell.firstChild.className = "resizeTitle";
    
    columnIndex = col = clums = displayTB = objCell = cellSort = sortHtml = sortClass = null;
}

function createDataTds(tid, cid, prop_caption_index)
{
	var index = prop_caption_index['preDisplayIndex'];	
    var clums = tableModel_arr[tid].clums;
    var col = tableModel_arr[tid].cells;
    var props = tableModel_arr[tid].props;
	var caption = tableModel_arr[tid].caption;
	
	var data = getTableById(tid).data;
	var displayTB = getTableById(tid).table;	
	var length = displayTB.rows.length;
	
	var tObject = tableModel_arr[tid].table;
    var tModle = tableModel_arr[tid];
    var autoCtrId_arr = new Array();
    var row = tObject.rows;
    
	for(var i=1; i<length; i++)
	{
		var naturalline = getKey()-length+1;
    	var objCell = displayTB.rows[i].insertCell(prop_caption_index['preDisplayIndex']);
    	var usermethod = col[index].getAttribute('calculate');

    	//\u5728\u8fd9\u91cc\u5e94\u5f53\u5148\u5224\u65ad\u662f\u5426\u6709\u5b50\u8282\u70b9\uff0c\u5982\u679c\u6709\u7684\u8bdd\u4e0e\u8868\u683c\u5904\u7406\u5668\u76f8\u5173\uff0c\u5426\u5219\u4e0eprop\u76f8\u5173
    	//\u9996\u5148\u5904\u7406\u65e0\u8282\u70b9\u573a\u666f
    	if (col[index].td_Children == null || col[index].td_Children.length == 0)
    	{	
    		try
    		{  			
    			var datatype = col[index].getAttribute("datatype");
    			var statusDesc = col[index].getAttribute("ctrlex");
    			var innertxt = col[index].innerHTML;
                var prop = props[index];
                
				var text;//, exps, levels;
				var isParams = false;
				/*
				\u7b2c\u4e00\u7c7b\uff1a\u6709prop\uff0c \u4f46\u662f\u540c\u65f6\u6709calcualte\u5c5e\u6027\uff0c \u6b64\u65f6\u4ee5calculate\u5c5e\u6027\u4e3a\u4e3b\uff0c \u5ffd\u7565prop\u5c5e\u6027
				*/
				if(prop != null && prop !="")
				{
					if(usermethod !=null && usermethod !="")
					{
						text = calculateUserMethod(data, i, usermethod, innertxt, prop, eval("data[" + parseInt(i - 1) + "]." + prop));
						isParams = true;
					}
					else
						text = eval("data[" + parseInt(i - 1) + "]." + prop);
				}
				/*
				\u7b2c\u4e8c\u7c7b\u60c5\u51b5\uff1a\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4f46\u6709calculate\u5c5e\u6027
				*/
				else if(usermethod !=null && usermethod !="")
				{
					text = calculateUserMethod(data, i, usermethod, innertxt);
					isParams = true;
				}
				/*
				\u7b2c\u4e09\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027, \u4f46\u7528\u6237\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f${}
				*/
				else if(innertxt != null && innertxt != "")
			   // else if(false)
				{
					text = calculateUserMethod(data, i, usermethod, innertxt, prop);
				}
				/*
				\u7b2c\u56db\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f${}
				*/
				else
				{
					text = innertxt;	
				}
				  		
	    			
				//text = innertxt;
     	  //\u884c\u5e8f\u53f7
                if (prop == '$ROWNUM')
                {
                    text = i;
                }
                    //\u5982\u679c\u662f\u65e5\u671f\u578b\u7684
                else if (datatype != null && datatype.toLowerCase().indexOf("date") != -1)
                {
                    var formate = null;
                    var f = datatype.toLowerCase();
                    var s = f.indexOf("(");
                    if (s != -1)
                    {
                        formate = f.substring(s + 1, f.indexOf(")"));
                    }
                    text = getTimefromUTC(formate, text);
                }
                
                if(!isParams)
                	text = caculator_processor(usermethod, prop, text);
				
     	  		//firfox \u4e2d\u4e0d\u652f\u6301innerText;
                column_ctrlex_Processor(objCell, statusDesc, text);
                
    		}	
    		catch(err)
    		{
    			//alert("\u521b\u5efa\u6570\u636e\u5355\u5143\u683c\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5\u5217caculate\u548cprop\u8bbe\u7f6e\uff01");
    			alert('Fail to create dynamic datacell, please check caculate and prop properties!');
    		}
    	}
    	//\u5904\u7406\u6709\u8282\u70b9\u573a\u666f:\u5f00\u59cb
    	else
    	{
  		    //\u5b58\u5728column\u7684prop\u5c5e\u6027
            var prop = '';
            if (col[index].getAttribute("prop") != null)
            {
                prop = col[index].getAttribute("prop");
            }
	
	  		//\u521b\u5efa\u5b50\u8282\u70b9\u65f6,\u5220\u9664\u65e7\u7684\u8282\u70b9
            for (var k = 0; k < row[i].cells[index].childNodes.length; k++)
            {
                row[i].cells[index].removeChild(row[i].cells[index].childNodes[k]);
            }
 	    	//\u5faa\u73af\u5217\u91cc\u9762\u7684\u63a7\u4ef6
            for (var m = 0; m < col[index].td_Children.length; m++)
            {
		  	//\u53d6\u5f97\u6a21\u677ftd\u91cc\u9762\u7684\u63a7\u4ef6
                contrlnode = col[index].td_Children[m];
                //\u5224\u65adtd\u91cc\u9762\u7684\u63a7\u4ef6\u662f\u5426\u5b58\u5728prop\u5c5e\u6027,\u5982\u679c\u5b58\u5728,\u4ee5\u63a7\u4ef6\u7684prop\u5c5e\u6027\u4e3a\u51c6
                if (contrlnode.getAttribute("prop") != null)
                {
                    prop = contrlnode.getAttribute("prop");
                }
                //\u53d6\u5f97\u63a7\u4ef6\u7c7b\u578b
                var value = "";
                if (prop != null && prop != "")
                {
                    value = eval("data[i-1]." + prop);
                }
                //\u5f97\u5230\u63a7\u4ef6\u5904\u7406\u5668\u7c7b\u578b
                var ctrTag = table_Ctr_Processor(contrlnode, value, i, tid, data[parseInt(i - 1)], usermethod, naturalline);
                
                //\u5982\u679c\u8fd4\u56de\u7684\u662fString
                if (typeof ctrTag == "string")
                {
                    objCell.innerHTML = ctrTag;
                }
                //\u5982\u679c\u662f\u5bf9\u8c61 
                else 
                {
                    objCell.appendChild(ctrTag);
                    var Ctr_tagName = ctrTag.tagName.toUpperCase();
                    //\u5982\u679c\u662fLOV\u6807\u7b7e
                    if (Ctr_tagName == 'Z:SSB_RIA_LOV' || Ctr_tagName == 'SSB_RIA_LOV')
                    {
                        readRiaLovTag(ctrTag);
                        var tt = document.getElementById(ctrTag.id);
                        tt.value = value;
                    }
                    //\u5982\u679c\u662f\u81ea\u52a8\u5b8c\u6210\u6807\u7b7e
                    if (Ctr_tagName == 'Z:AUTOCOMPLETE' || Ctr_tagName == 'AUTOCOMPLETE')   autoCtrId_arr.push(ctrTag.getAttribute('id'));
                    //\u662fradioGroup\u63a7\u4ef6
                    if (Ctr_tagName == 'Z:RADIOGROUP' || Ctr_tagName == 'RADIOGROUP')
                    {
                        radios = new RadioGroupTag(ctrTag.getAttribute('id'));
                        radios.init();
                        radios.setValue(value);
                    }
                    if (Ctr_tagName == 'Z:UPLOAD' || Ctr_tagName == 'UPLOAD')
                    {
                        var upload = UploadTag(ctrTag.getAttribute('id'));
                        upload.init();
                        var oInput = upload.getFileCtrl();
                        oInput.value = value;
                    }
                    //\u5982\u679c\u5b58\u5728type\u5c5e\u6027
                    var tagtype = ctrTag.getAttribute("type");
                    if (tagtype != null)
                    {
                        tagtype = tagtype.toUpperCase();
                        //\u5982\u679c\u662f\u5355\u9009\u6846\u6216\u590d\u9009\u6846\u65f6\u8bbe\u7f6e\u9009\u4e2d\u9879
                        if ((tagtype == "CHECKBOX" || tagtype == "RADIO") && ctrTag.getAttribute("ctrlex") != null)
                        {
                            var ctlex = ctrTag.getAttribute('ctrlex');
                            var select = evalParse(tableData[parseInt(i - 1)], ctlex);

                            if (select != 0 && select != null)
                            {
                                ctrTag.setAttribute('checked', 'true');
                            }
                        }
                    }
                }
              }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
    		}//\u5904\u7406\u6709\u8282\u70b9\u573a\u666f:\u7ed3\u675f
    	
    	assignDataCSS(col[index], objCell); 
    }	
    index = clums = col = props = caption = data = displayTB = tObject = tModle = autoCtrId_arr = row = null;
}

function calculateUserMethod(tableData, index, usermethod, contxt, prop, preText)
{
	var isParams = false;
	var text, exps, levels;
   //\u60c5\u51b5\u4e00\uff1a\u81ea\u5b9a\u4e49\u65b9\u6cd5\u662f\u5426\u6709\u53c2\u6570
	if(usermethod != null && usermethod.indexOf("(") != - 1)
	{
	   if(usermethod.indexOf(")") != - 1)
	   {
	      var paramConvert = function(value)
	      {
	         var exps = value.match(/\${.+?}/g);
	         for(var i = 0; i < exps.length; i ++ )
	         {
	            var loc = value.indexOf(exps[i]) ;
	            if( value.charAt(loc - 1) != "'")
	            {
	               if(value.charAt(loc - 1) != "\"")
	               {
	                  value = value.replace(exps[i], "'"+exps[i]+"'");
	               }
	            }
	         }
	         return value;
	      }
         contxt = paramConvert(usermethod);
         isParams = true;
      }
      else
      usermethod = null;
   }
   //\u60c5\u51b5\u4e8c\uff1a\u81ea\u5b9a\u4e49\u65b9\u6cd5\u65e0\u53c2\u6570\u7684\u5199\u6cd5
   else if(usermethod != null)
   {
   		text = caculator_processor(usermethod, prop, preText);
   		contxt = "";
   }
   //\u60c5\u51b5\u4e09\uff1a\u8868\u8fbe\u5f0f\u5904\u7406
   if(contxt != null && contxt != "")
   {
      exps = contxt.match(/\${.+?}/g);
      for(var nn = 0; nn < exps.length; nn ++ )
      {
         levels = exps[nn].substring(2, exps[nn].length - 1).split(".");
         var datastring = "tableData[" + parseInt(index - 1) + "]";
         for(var mm = 0; mm < levels.length; mm ++ )
         {
            datastring += "." + levels[mm];
         }
         if(isParams)
         datastring = eval(datastring);
         else
         datastring = caculator_processor(usermethod, exps[nn].substring(2, exps[nn].length - 1), eval(datastring));

         contxt = contxt.replace(exps[nn], datastring);
      }

      if(isParams)
      {
         usermethod = contxt;
         contxt = caculator_processor(usermethod);
      }
      text = contxt;
   }
   
   return text;
};

function caculator_processor(usermethod, key, value)
{
	var keyvalue = new Array();	
	if(usermethod != null && usermethod != "")
	{	
		try
		{
			if(usermethod.indexOf("(") != -1)
            {                 		         
                 value = eval(usermethod);	
            }
            else
            {
                keyvalue[key] = value;	
		        value = eval(usermethod +"(keyvalue);")[key];
            }
		}
		catch(err)
		{
			var msg = "\u4f7f\u7528\u81ea\u5b9a\u4e49\u8f6c\u6362\u65b9\u6cd5\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5caculate\u5c5e\u6027\uff01";
			alert($res_entry('ui.control.table.caculator.error',msg));
		}	
		finally
		{
			keyvalue = null;
		}
	}
	
	return value;
}

function assignDataCSS(col, objCell)
{
	var align = col.getAttribute("align");
	var style = col.getAttribute("style");
	
	if(align != null && align != "")
	{	
		objCell.setAttribute("style", "TEXT-ALIGN:"+align);
        if(isIE())            
        	objCell.style.cssText = "TEXT-ALIGN:"+align;				
	}
	else if(style != null && style != "")
	{
       objCell.setAttribute("style", style);
       if(isIE())            
       		objCell.style.cssText = style.cssText;	
	}
	
	align = style = null;
}

function displayColumn(tid, cid)
{
	//\u5224\u65ad\u9875\u9762\u4e2d\u662f\u5426\u6709\u8fd9\u6837\u4e00\u5217\uff0c\u4ee5\u5217id\u4e3a\u51c6\uff1b
	var canBeShown = false;
	for(t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	//\u53ea\u6709\u8be5\u5217\u5b58\u5728\u624d\u53ef\u4ee5\u7ee7\u7eed
	if(!canBeShown)
	{
		//alert("\u8be5\u5217\u4e0d\u5b58\u5728\uff01");
		//alert('The column is not existed!');
		return;
	}
	
	//\u5224\u65ad\u8be5\u5217\u662f\u5426\u5df2\u7ecf\u663e\u793a
	for(var t=0; t<tableModel_arr[tid].cells.length; t++)
	{
		if (cid == tableModel_arr[tid].cells[t].id)
		{
			//alert("\u8be5\u5217\u5df2\u7ecf\u663e\u793a\uff01");
			//alert('The column is already existed!');
			return;
		}
	}
	
	//\u8868\u6a21\u578b\u4e2d\u6240\u6709\u7684\u5217
	var clumns = tableModel_arr[tid].clums;
	//\u8be5\u5217\u5728\u6240\u6709\u5217\u6570\u7ec4\u4e2d\u7684index
	var columnIndex;
	//\u952e\u503c\u5bf9\u8c61
	var prop_caption_index = [];
	//\u8be5\u5217\u5728cell\u6570\u7ec4\u4e2d\u7684index
	var preDisplayIndex = 0;
	//\u67d0\u5217\u662f\u5426\u53ef\u89c1
	var visible;
	
	for(var i=0; i< clumns.length; i++)
	{
		visible = clumns[i].getAttribute("visible");
		if(visible != false && visible != "false")
		{
			preDisplayIndex++;
		}
		if(clumns[i].id == cid)
		{
			prop_caption_index['preDisplayIndex'] = preDisplayIndex;
			prop_caption_index['index'] = i;
			prop_caption_index['prop'] = clumns[i].getAttribute('prop');
			prop_caption_index['caption'] = clumns[i].getAttribute('caption');
			prop_caption_index['column'] = clumns[i];
			
			//\u8be5\u5217\u4f1a\u88ab\u63d2\u5165\uff0c\u8bbe\u7f6e\u8be5\u5217\u7684visible\u4e3atrue
			clumns[i].setAttribute("visible","true");
			break;
		}
	}
	
    //\u8bbe\u7f6e\u8be5\u5217Cell\u7684\u6821\u9a8c\u51fd\u6570
    prop_caption_index['column'].validate = prop_caption_index['column'].getAttribute("validate");
    
	//\u8bbe\u7f6e\u8be5\u5217Cell\u7684\u6570\u636e\u7c7b\u578b
    prop_caption_index['column'].datatype = prop_caption_index['column'].getAttribute("datatype");
    
	//\u8bbe\u7f6e\u8be5\u5217Cell\u7684td_Children\u5c5e\u6027
    var td_Node_Children = prop_caption_index['column'].childNodes;
    var td_Children = new Array();
    for (var m = 0; m < td_Node_Children.length; m ++)
    {
        if (td_Node_Children[m].tagName != null && td_Node_Children[m].tagName != "Z:COLUMN")
        {
            td_Children.push(td_Node_Children[m]);
        }
    }
    prop_caption_index['column'].td_Children = td_Children;
    //\u5728\u8868\u683c\u6a21\u578b\u4e2d\u63d2\u5165\u8be5\u5217\u76f8\u5173\u4fe1\u606f
    tableModel_arr[tid].props = insertAt(tableModel_arr[tid].props, prop_caption_index['preDisplayIndex'], prop_caption_index['prop']);
    tableModel_arr[tid].caption = insertAt(tableModel_arr[tid].caption, prop_caption_index['preDisplayIndex'], prop_caption_index['caption']);	
    tableModel_arr[tid].cells = insertAt(tableModel_arr[tid].cells, prop_caption_index['preDisplayIndex'], prop_caption_index['column']);	
    
    //\u521b\u5efa\u5217\u8868\u5934
	createCaption(tid, cid, prop_caption_index);
	//\u521b\u5efa\u5217\u6570\u636e
	createDataTds(tid, cid, prop_caption_index);
	//\u6e05\u9664\u6570\u636e\u7a7a\u95f4
	clumns = columnIndex = prop_caption_index = visible = td_Node_Children = td_Children = null;
}

function insertAt (arr, index, value ) 
{
	var part1 = arr.slice( 0, index );
	var part2 = arr.slice( index );
	part1.push( value );
	return( part1.concat( part2 ) );
}

function removeAt(arr, index )
{
	var part1 = arr.slice( 0, index+1 );
	var part2 = arr.slice( index+1 );
	part1.pop();
	return( part1.concat( part2 ) );
}

function hideColumn(tid, cid)
{
	//\u5224\u65ad\u9875\u9762\u4e2d\u662f\u5426\u6709\u8fd9\u6837\u4e00\u5217\uff0c\u4ee5\u5217id\u4e3a\u51c6\uff1b
	var canBeShown = false;
	for(t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	//\u53ea\u6709\u8be5\u5217\u5b58\u5728\u624d\u53ef\u4ee5\u7ee7\u7eed
	if(!canBeShown)
	{
		//alert("\u8be5\u5217\u4e0d\u5b58\u5728\uff01");
		//alert('The column is not existed!');
		return;
	}
	
	//\u5224\u65ad\u8be5\u5217\u662f\u5426\u5df2\u7ecf\u9690\u85cf
	for(var valid=0; valid<tableModel_arr[tid].cells.length; valid++)
	{
		if (cid != tableModel_arr[tid].cells[valid].id) continue;
		break;
	}
	if( valid == tableModel_arr[tid].cells.length)
	{
		//alert("\u8be5\u5217\u5df2\u7ecf\u9690\u85cf\uff01");
		//alert('The column is already hidden!');
		return;
	}
	
	//\u53d6\u5f97\u663e\u793a\u7684\u6240\u6709\u5217
	var cells = tableModel_arr[tid].cells;
	//\u53d6\u5f97\u5185\u5b58\u4e2d\u7684\u6240\u6709\u5217
	var clumns = tableModel_arr[tid].clums;
	//\u8981\u5220\u9664\u7684\u663e\u793a\u5217\u7684\u5e8f\u53f7
	var deleteColumnIndex;
	var displayTB = tableModel_arr[tid].table;	
	
	//\u901a\u8fc7\u5faa\u73af\u5f97\u5230\u5c06\u5220\u9664\u5217\u5728cell\u4e2d\u7684index
	for(var i=0; i< cells.length; i++)
	{
		if(cells[i].id == cid)
		{
			deleteColumnIndex = i;
			break;
		}
	}
	
	//\u8be5\u5217\u4f1a\u88ab\u5220\u9664\uff0c\u901a\u8fc7\u5faa\u73af\u5339\u914d\uff0c\u8bbe\u7f6e\u8be5\u5217\u7684visible\u4e3afalse
	for(var i=0; i< clumns.length; i++)
	{
		if(clumns[i].id == cid)
		{
			clumns[i].setAttribute("visible","false");
			break;
		}
	}
	
    //\u5728\u8868\u683c\u6a21\u578b\u4e2d\u5220\u9664\u8be5\u5217\u76f8\u5173\u4fe1\u606f
	tableModel_arr[tid].props = removeAt(tableModel_arr[tid].props,deleteColumnIndex);
    tableModel_arr[tid].caption = removeAt(tableModel_arr[tid].caption,deleteColumnIndex);	
    tableModel_arr[tid].cells = removeAt(tableModel_arr[tid].cells,deleteColumnIndex);	
    tableModel_arr[tid].captiontips = removeAt(tableModel_arr[tid].captiontips,deleteColumnIndex);	
    
    if(!isIE())
    {
    	//\u8ba9FireFox\u652f\u6301children\u5c5e\u6027
    	FF_children();
    }
    
    //\u53d6\u5f97\u663e\u793a\u8868\u683c\uff0c\u5faa\u73af\u6bcf\u4e00\u884c\uff0c\u5220\u9664\u76f8\u5e94\u8282\u70b9
	for(var i=0; i<displayTB.rows.length; i++)
	{
    	displayTB.rows[i].removeChild(displayTB.rows[i].children[deleteColumnIndex]);
    }
    
    //\u6e05\u9664\u76f8\u5173\u53d8\u91cf
    valid = cells = clumns = deleteColumnIndex = displayTB = null;
}

function addEventForCheckBox(TagObject, MtableId)
{
    if (TagObject.getAttribute('type') && (TagObject.getAttribute('type')).toUpperCase() == "CHECKBOX")
    {
        var onclick = "";
        if (TagObject.getAttribute('onclick') != null)
        {
            onclick = TagObject.getAttribute('onclick').toString();
        }
        var even = function() {
            if (TagObject.checked == false && document.getElementById(MtableId + 'allCheck'))
            {
                document.getElementById(MtableId + 'allCheck').checked = TagObject.checked;
            }
            eval(onclick);
        };
        if (isIE())
        {
            //TagObject.attachEvent("onclick",even);
			//change by Daniel for attachevent memory leak ;
			EventManager.Add(TagObject,'click',even);
        } else {
            TagObject.addEventListener("click", even, false);
        }
    }
}
;

table_Ctr_Processor = function(tagObject, value, rowindex, MtableId, data, usermethod, naturalline)
{
    var ctrName = tagObject.tagName;
    var tagName = ctrName.toUpperCase();//\u6807\u7b7e\u540d\u79f0
    //var reTagObject = tagObject.cloneNode(true); //\u8fd4\u56de\u7684\u6807\u7b7e\u5bf9\u8c61
    
    var reTagObject = tagObject.cloneNode(true); 
     //\u4f18\u53161 \u4e0b\u62c9\u6846\u9009\u9879
     if(reTagObject.tagName.toUpperCase() == "SELECT")
 	 {
		reTagObject.selectedIndex = tagObject.selectedIndex;
 	 }
     
    if (reTagObject.id != null)  
    {
    	//\u52a0\u5f3a\u5224\u65ad\uff0c\u8ba9\u884c\u63a7\u4ef6ID\u90fd\u52a0\u4e0a\u4e00\u4e2a\u81ea\u7136\u5e8f\u53f7\uff0c\u786e\u4fdd\u65e0\u91cd\u590d
    	if(naturalline != null)
    		reTagObject.id = reTagObject.id + naturalline + rowindex;
    	else
    		reTagObject.id = reTagObject.id + rowindex;
    }
    		
    reTagObject = getOuterHtml(reTagObject, data, usermethod);
    switch (tagName)
            {
        case "INPUT" : //\u8f93\u5165\u6846
        	var useExp = tagObject.getAttribute("value");
        	//\u7528\u4e8e\u5224\u65ad\u662f\u5426\u4e3avalue\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u5982\u679c\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u503c\u4e0d\u80fd\u88ab\u6539\u53d8
        	if(useExp != null)
        		useExp = useExp.indexOf("${");
        	else
        		useExp = -1;
            return input_Ctr_Processor(reTagObject, value, rowindex, MtableId, useExp);
            break;

        case "A" : //\u8d85\u8fde\u63a5\u6807\u7b7e
            return A_Ctr_Processor(reTagObject, value, rowindex, MtableId, data);
            break;

        case "SELECT" ://\u4e0b\u62c9\u5217\u8868
            return select_Ctr_Processor(reTagObject, value, rowindex, MtableId, data);
            break;

        case "CALENDAR": //\u65e5\u671f\u63a7\u4ef6
            return Calendar_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:CALENDAR": //\u65e5\u671f\u63a7\u4ef6
            return Calendar_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "IMG": //\u56fe\u7247\u6807\u7b7e
            return reTagObject;
            break;

        case "Z:SSB_RIA_LOV"://lov\u63a7\u4ef6FF
            return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "SSB_RIA_LOV"://lov\u63a7\u4ef6IE
            return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;
        default :
            return reTagObject;
            break;
    }

};

bindData2TableCtrl = function(MtableId, tableData)
{
    var tObject = tableModel_arr[MtableId].table;
    var tModle = tableModel_arr[MtableId];
    var autoCtrId_arr = new Array();
    var row = tObject.rows;
    var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9
    for (var i = 1; i < row.length; i ++)//\u5faa\u73af\u5185\u5b58\u6bcf\u4e00\u884c
    {
        var rowcells = row[i].cells;//\u53d6\u884c\u5bf9\u8c61
		var dyn = getKey();//\u6bcf\u884c\u7684\u81ea\u7136\u5e8f\u53f7
        for (var j = 0; j < tdnodes.length; j++)//\u5faa\u73af\u6a21\u578b\u8868\u683c
        {            
			//2008-09-01 wxr \u4e3a\u6570\u636e\u6dfb\u52a0\u9ed8\u8ba4\u7684\u5bf9\u9f50\u65b9\u5f0f\uff0c\u8bfb\u53d6\u5e76\u8bbe\u7f6e\u5728<z:column/>\u4e2d\u7684style\u5c5e\u6027 begin		
			 assignDataCSS(tdnodes[j], rowcells[j]);
			//2008-09-01 wxr \u8bfb\u53d6\u5e76\u8bbe\u7f6e\u5728<z:column/>\u4e2d\u7684style\u5c5e\u6027 end			
			
			var datatype = tdnodes[j].getAttribute("datatype");
            var contrlnode = '';
            //\u7528\u6237\u81ea\u5b9a\u4e49\u8f6c\u6362\u65b9\u6cd5
			var usermethod = tdnodes[j].getAttribute('calculate');

     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6 
            if (tableModel_arr[MtableId].cells[j].td_Children.length == 0)
            {
                var statusDesc = tdnodes[j].getAttribute("ctrlex");
                var contxt = tdnodes[j].innerHTML;
                var prop = tableModel_arr[MtableId].props[j];//\u5f97\u5230column\u7684prop\u5c5e\u6027
                
				var text;//, exps, levels;
				
				var isParams = false;
				/*
				\u7b2c\u4e00\u7c7b\uff1a\u6709prop\uff0c \u4f46\u662f\u540c\u65f6\u6709calcualte\u5c5e\u6027\uff0c \u6b64\u65f6\u4ee5calculate\u5c5e\u6027\u4e3a\u4e3b\uff0c \u5ffd\u7565prop\u5c5e\u6027
				*/				
				if(prop != null && prop !="")
				{
					if(usermethod !=null && usermethod !="")
					{
						text = calculateUserMethod(tableData, i, usermethod, contxt, prop, eval("tableData[" + parseInt(i - 1) + "]." + prop));
						isParams = true;
					}
					else
						text = eval("tableData[" + parseInt(i - 1) + "]." + prop);
				}
				/*
				\u7b2c\u4e8c\u7c7b\u60c5\u51b5\uff1a\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4f46\u6709calculate\u5c5e\u6027
				*/
				else if(usermethod !=null && usermethod !="")
				{
					text = calculateUserMethod(tableData, i, usermethod, contxt);
					isParams = true;
				}
				/*
				\u7b2c\u4e09\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027, \u4f46\u7528\u6237\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f${}
				*/
				else if(contxt != null && contxt != "")
			   // else if(false)
				{
					text = calculateUserMethod(tableData, i, usermethod, contxt, prop);
				}
				/*
				\u7b2c\u56db\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f${}
				*/
				else
				{
					text = contxt;	
				}
				
     	  		//\u884c\u5e8f\u53f7
                if (prop == '$ROWNUM')
                {
                    text = i;
                }
                //\u5982\u679c\u662f\u65e5\u671f\u578b\u7684
                else if (datatype != null && datatype.toLowerCase().indexOf("date") != -1)
                {
                    var formate = null;
                    var f = datatype.toLowerCase();
                    var s = f.indexOf("(");
                    if (s != -1)
                    {
                        formate = f.substring(s + 1, f.indexOf(")"));
                    }
                    text = getTimefromUTC(formate, text);
                }
                
                if(!isParams)
                	text = caculator_processor(usermethod, prop, text);
                
     	  		//firfox \u4e2d\u4e0d\u652f\u6301innerText;
                column_ctrlex_Processor(rowcells[j], statusDesc, text);
            }
            else {//\u5305\u542b\u63a7\u4ef6
                var prop = '';
     	   //\u5b58\u5728column\u7684prop\u5c5e\u6027
                if (tdnodes[j].getAttribute("prop") != null)
                {
                    prop = tdnodes[j].getAttribute("prop");
                }
     	   //\u5faa\u73af\u5217\u91cc\u9762\u7684\u63a7\u4ef6

                for (var k = 0; k < row[i].cells[j].childNodes.length; k++)
                {
                    //\u521b\u5efa\u5b50\u8282\u70b9\u65f6,\u5220\u9664\u65e7\u7684\u8282\u70b9
                    row[i].cells[j].removeChild(row[i].cells[j].childNodes[k]);
                }
     	    
     	    //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
                for (var m = 0; m < tableModel_arr[MtableId].cells[j].td_Children.length; m++)
                {
                    //\u53d6\u5f97\u6a21\u677ftd\u91cc\u9762\u7684\u63a7\u4ef6
                    contrlnode = tableModel_arr[MtableId].cells[j].td_Children[m];
					if (contrlnode.nodeType == 3) 
					{
						row[i].cells[j].appendChild(contrlnode.cloneNode(true));
						continue;
					}
     	     //\u5224\u65adtd\u91cc\u9762\u7684\u63a7\u4ef6\u662f\u5426\u5b58\u5728prop\u5c5e\u6027,\u5982\u679c\u5b58\u5728,\u4ee5\u63a7\u4ef6\u7684prop\u5c5e\u6027\u4e3a\u51c6
                    if (contrlnode.getAttribute("prop") != null)
                    {
                        prop = contrlnode.getAttribute("prop");
                    }
     	      //\u53d6\u5f97\u63a7\u4ef6\u7c7b\u578b
                    var value = "";
                    if (prop != null && prop != "")
                    {
                        //value = evalParse(tableData[parseInt(i-1)],prop);
                        value = eval("tableData[" + parseInt(i - 1) + "]." + prop);
                    }
                    //\u5f97\u5230\u63a7\u4ef6\u5904\u7406\u5668\u7c7b\u578b;
                    var ctrTag = table_Ctr_Processor(contrlnode, value, i, MtableId, tableData[parseInt(i - 1)], usermethod, dyn);
                    //\u5982\u679c\u8fd4\u56de\u7684\u662fString
                    if (typeof ctrTag == "string")
                    {
                        row[i].cells[j].innerHTML = ctrTag;
                    }
                    else { //\u5982\u679c\u662f\u5bf9\u8c61
                        row[i].cells[j].appendChild(ctrTag);
                        var Ctr_tagName = ctrTag.tagName.toUpperCase();
				  //\u5982\u679c\u662fLOV\u6807\u7b7e
                        if (Ctr_tagName == 'Z:SSB_RIA_LOV' || Ctr_tagName == 'SSB_RIA_LOV')
                        {
                            readRiaLovTag(ctrTag);
                            var tt = document.getElementById(ctrTag.id);
                            tt.value = value;
                        }
				  //\u5982\u679c\u662f\u81ea\u52a8\u5b8c\u6210\u6807\u7b7e
                        if (Ctr_tagName == 'Z:AUTOCOMPLETE' || Ctr_tagName == 'AUTOCOMPLETE')   autoCtrId_arr.push(ctrTag.getAttribute('id'));
			      //\u662fradioGroup\u63a7\u4ef6
                        if (Ctr_tagName == 'Z:RADIOGROUP' || Ctr_tagName == 'RADIOGROUP')
                        {
                            radios = new RadioGroupTag(ctrTag.getAttribute('id'));
                            radios.init();
                            radios.setValue(value);
                        }
                        if (Ctr_tagName == 'Z:UPLOAD' || Ctr_tagName == 'UPLOAD')
                        {
                            var upload = UploadTag(ctrTag.getAttribute('id'));
                            upload.init();
                            var oInput = upload.getFileCtrl();
                            oInput.value = value;
                        }
			      //\u5982\u679c\u5b58\u5728type\u5c5e\u6027
                        var tagtype = ctrTag.getAttribute("type");
                        if (tagtype != null)
                        {
                            tagtype = tagtype.toUpperCase();
				  	//\u5982\u679c\u662f\u5355\u9009\u6846\u6216\u590d\u9009\u6846\u65f6\u8bbe\u7f6e\u9009\u4e2d\u9879
                            if ((tagtype == "CHECKBOX" || tagtype == "RADIO") && ctrTag.getAttribute("ctrlex") != null)
                            {
                                //\u9009\u4e2d\u9879
                                var ctlex = ctrTag.getAttribute('ctrlex');
                                var select = evalParse(tableData[parseInt(i - 1)], ctlex);

                                if (select != 0 && select != null)//\u5982\u679c\u4e0d\u7b49\u4e8e0\u8868\u793a\u9009\u4e2d
                                {
                                    ctrTag.setAttribute('checked', 'true');
                                }
                            }
                        }
                    }//end else

                }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
                var prop = tableModel_arr[MtableId].props[j];//\u5f97\u5230column\u7684prop\u5c5e\u6027
                fillCellWithProp(row[i].cells[j], prop, tableData[parseInt(i - 1)]);
            }

        }//\u5217\u5faa\u73af

    }//\u884c\u5faa\u73af
    for (var i = 0; i < autoCtrId_arr.length; i ++)
    {
        autos = new AutoCompleteTag(autoCtrId_arr[i]);
        autos.init();
    }
    //\u5fc5\u987b\u8981vstext\u4e0d\u4e3a\u7a7a\u65f6\u624d\u81ea\u52a8\u521b\u5efa\u7edf\u8ba1\u884c, \u4ee5\u652f\u6301\u7528\u6237\u81ea\u5b9a\u4e49\u7edf\u8ba1\u884c
    if (tableModel_arr[MtableId].vstatcaption)
    	if (tableModel_arr[MtableId].vstext != "")
    	    Vstatcaption_Processor(MtableId, tableData);
  //onFinished \u4e8b\u4ef6\u5904\u7406
    if (tableModel_arr[MtableId].onFinished) {
        parseFunction(tableModel_arr[MtableId].onFinished, tableModel_arr[MtableId].table);
    }
};

function fillCellWithProp(cell, prop, data)
{
	if(prop != null && prop != "null" && prop != "")
    {
    	var inputCtrs = cell.getElementsByTagName("input");
    	var selCtrs = cell.getElementsByTagName("select");
    	if(inputCtrs != null && inputCtrs.length > 0 && selCtrs.length == 0)
    	{
    		for(var nn=0; nn<inputCtrs.length; nn++)
    		{
    			if(inputCtrs[nn].type != "hidden") return;
    		}
			var novalue = "";
	    	if(isIE())
				novalue = cell.innerText;
			else
				novalue = cell.textContent;
			if($.trim(novalue) == "")
			{
				var ef = Express_Factory("", data);
				cell.appendChild(document.createTextNode(ef.getExpValue(prop)));
				/*
				if(isIE())
					cell.innerText = ef.getExpValue(prop);
				else
					cell.textContent = ef.getExpValue(prop);
				*/
			}
    	}
    }
};

function Vstatcaption_Processor(mid, tableData)
{
    var tr = addOneRow(mid);
    tr.className = "vstatcaption";
    var cells = tableModel_arr[mid].cells;
    var texttitle = tableModel_arr[mid].vstext;
    innerText_function(tr.cells[0], texttitle);
    tr.cells[0].className = "title";
    for (var i = 1; i < cells.length; i ++)
    {
        tr.cells[i].className = "data";
        var dd = cells[i].getAttribute(VSTATFORMULA);
        var tv = enumerate_value(cells[i], dd);
        if (cells[i].getAttribute("datatype") == "date")
        {
            var formate = tableModel_arr[mid].date_formate;
            tv = getTimefromUTC(formate, tv);
        }
        innerText_function(tr.cells[i], tv);
    }

    function enumerate_value(cell, vstat)
    {
    	if(vstat == null || vstat == "")
        	return "";
        var rtn = "";
        var arr = new Array();
        for (var i = 0; i < tableData.length; i++)
        {
            var prop = cell.getAttribute("prop");//\u5f97\u5230column\u7684prop\u5c5e\u6027
            var value = eval("tableData[" + parseInt(i) + "]." + prop);
            arr.push(value);
        }
        switch (vstat.toUpperCase()) {
            case "MAX":
                arr.sort();
                rtn = arr[arr.length - 1];
                break;
            case "MIN":
                arr.sort();
                rtn = arr[0];
                break;
            case "AVG":
                var sum = 0;
                for (var i = 0; i < arr.length; i++)
                {
                    sum += arr[i];
                }
                rtn = sum / arr.length;
                break;
            case "SUM":
                var sum = 0;
                for (var i = 0; i < arr.length; i++)
                {
                    sum += arr[i];
                }
                rtn = sum;
                break;
            default:
                break;
        }
        return rtn;
    }
    ;

}
;

function column_ctrlex_Processor(rowcells, statusDesc, text)
{
    if (text == null) rowcells.innerHTML = "";
    else if (statusDesc != null && statusDesc != '')
    {
        var _key_value_arrs = statusDesc.split(';');

        for (var i = 0; i < _key_value_arrs.length; i ++)
        {
            var _key_value_maps = _key_value_arrs[i].split(':');
            if (_key_value_maps[0] != "")
            {
                if (new String(text) == _key_value_maps[0])
                {
                    if (_key_value_maps[2] != "")  innerText_function(rowcells, _key_value_maps[2]);
                    else innerText_function(rowcells, text);
                    if (_key_value_maps[1] != "")
                        rowcells.style.cssText = "background:" + _key_value_maps[1];
                }
            }
            else
            {
                //alert("ctrlex \u5c5e\u6027\u914d\u7f6e\u9519\u8bef!");
                alert('The configure of ctrlex is Wrong!');
            }
        }
    }
    else
    {
        innerText_function(rowcells, text);
    }
}
;

function innerText_function(DOM_ctrl, text)
{
    if (isIE()) DOM_ctrl.innerText = text;
    else DOM_ctrl.textContent = text;
}
;

var _primary_key = 0;
getKey = function()
{
    return _primary_key = _primary_key + 1;
};

create_new_EditTableRow = function(tableId, tb)//\u6ce8\uff1atableId\u5e76\u6ca1\u6709\u7528\u5230
{
    var objTable = tableModel_arr[tb].table;
    var Mtable = tableModel_arr[tb];
    var tdnodes = tableModel_arr[tb].cells;//\u6a21\u578b\u8868\u5217
    var objRow = objTable.insertRow(-1);
    var autoCtrId_arr = new Array();
    this.primaryKey = getKey() + "" + objRow.rowIndex;
    if (objRow.rowIndex % 2 == 0)
    {
        objRow.className = "even";
    }
	 //\u5faa\u73af\u6a21\u578b\u8868\u683c
    for (var j = 0; j < tdnodes.length; j++)
    {
        var contrlnode = '';
        var cell = objRow.insertCell(-1);
        
        //\u8bbe\u7f6eTD\u6837\u5f0f
        assignDataCSS(tdnodes[j], cell);

		//\u5f97\u5230column\u7684prop\u5c5e\u6027
        var prop = tableModel_arr[tb].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
        if (tableModel_arr[tb].cells[j].td_Children.length == 0)
        {
            if (prop == "$ROWNUM")
            {
                cell.innerHTML = objRow.rowIndex;
            }
        }
        else {//\u5305\u542b\u63a7\u4ef6
            var tmpTag, opt;
            //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
            for (var m = 0; m < tableModel_arr[tb].cells[j].td_Children.length; m++)
            {
                var ctrTag = tableModel_arr[tb].cells[j].td_Children[m].cloneNode(true);
                //2008-09-21 \u7f16\u8f91\u8868\u683c\u7684\u4e0b\u62c9\u6846\u652f\u6301\u7528\u6237\u9ed8\u8ba4\u9009\u9879 begin
                if(ctrTag.tagName.toUpperCase() == "SELECT")
            	{
            		tmpTag = tableModel_arr[tb].cells[j].td_Children[m];
            		if(!isIE()) FF_children();
            		for(var sltindex = 0; sltindex < tmpTag.children.length; sltindex++)
            		{
            			opt = tmpTag.children[sltindex];
            			
            			if(opt.getAttribute('selected') || opt.getAttribute('selected')=="true")
            			{
            				ctrTag.selectedIndex = sltindex;
            				break;
            			}
            		}
            		tmpTag = opt = null;
            	}
                //2008-09-21 \u7f16\u8f91\u8868\u683c\u7684\u4e0b\u62c9\u6846\u652f\u6301\u7528\u6237\u9ed8\u8ba4\u9009\u9879 end
                var tagName = ctrTag.tagName.toUpperCase();
     	   	 //\u5982\u679c\u5b50\u7ed3\u70b9\u4e2d\u6709Select\u6807\u7b7e
                if (tagName == 'SELECT')
                {
                    //\u521b\u5efa\u4e0b\u62c9\u5217\u8868\u6846\u9009\u9879
                    var option = document.createElement('option');
                    option.value = '';
                    option.innerHTML = '';
     	        //\u52a0\u5165\u6807\u7b7e
                    cell.appendChild(ctrTag);
                }
                else if (tagName == "CALENDAR" || tagName == "Z:CALENDAR")//\u5982\u679c\u662f\u65e5\u5386\u6807\u7b7e
                {
                    if (ctrTag.getAttribute("default") && ctrTag.getAttribute("default") != '')
                    {
                        var defaultProp = ctrTag.getAttribute("default");
                        value = eval(defaultProp);
                    }
                    var html = Calendar_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.innerHTML = html;
                }
                else if (tagName == "Z:SSB_RIA_LOV" || tagName == "SSB_RIA_LOV")
                {
                    var value = null;
                    var new_ctrTag = table_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_ctrTag);
                    readRiaLovTag(new_ctrTag);
                }
                else if (tagName == 'Z:AUTOCOMPLETE' || tagName == 'AUTOCOMPLETE')
                {
                    var value = null;
                    var new_autoCtr = autoComplete_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_autoCtr);
                    autoCtrId_arr.push(new_autoCtr.getAttribute('id'));
                }
                else if (tagName == 'Z:RADIOGROUP' || tagName == 'RADIOGROUP')
                {
                    var value = null;
                    var new_radioCtr = radioGroup_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    var nid = new_radioCtr.getAttribute('id') + this.primaryKey;
                    new_radioCtr.setAttribute('id', nid);
                    cell.appendChild(ctrTag);
                    radios = new RadioGroupTag(new_radioCtr.getAttribute('id'));
                    radios.init();
                }
                else if (tagName == 'Z:UPLOAD' || tagName == 'UPLOAD')
                {
                    var newCtrTag = upload_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    var nid = newCtrTag.getAttribute('id') + this.primaryKey;
                    newCtrTag.setAttribute('id', nid);
                    cell.appendChild(newCtrTag);
                    var upload = UploadTag(newCtrTag.getAttribute('id'));
                    upload.init();
                }
                else if (tagName == "A")
                {
                	var value = null;
                    var new_ctrTag = table_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_ctrTag);
                }
                else//\u5176\u4ed6\u6807\u7b7e\u76f4\u63a5\u63d2\u5165
                {
                    if (ctrTag.id != null) ctrTag.id = ctrTag.id + this.primaryKey;
                    
                    //if (ctrTag.value != null) ctrTag.value = ''; //2008-09-09 wxr: \u8be5\u884c\u4f1a\u628a\u9ed8\u8ba4\u7684value\u503c\u6e05\u7a7a\u6389
                    cell.appendChild(ctrTag);
                    addEventForCheckBox(ctrTag, tb);
                    var checkctrl = document.getElementById(tb + "allCheck");
                    if (ctrTag.type != null && ctrTag.type.toLowerCase() == 'checkbox' && checkctrl && checkctrl.checked == true)
                    {
                        ctrTag.checked = true;
                    }
                }

            }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
        }
    }//\u5217\u5faa\u73af
    for (var i = 0; i < autoCtrId_arr.length; i ++)
    {
        autos = new AutoCompleteTag(autoCtrId_arr[i]);
        autos.init();
    }
    return objRow;
};

showPages = function(name)
{ //\u521d\u59cb\u5316\u5c5e\u6027
    this.name = name;      //\u5bf9\u8c61\u540d\u79f0
    this.page = 1;         //\u5f53\u524d\u9875\u6570
    this.pageCount = 0;    //\u603b\u9875\u6570
    this.argName = 'page'; //\u53c2\u6570\u540d
    this.showTimes = 1;    //\u6253\u5370\u6b21\u6570
    this.perPageCount = 10;	//\u6bcf\u9875\u663e\u793a\u6761\u6570
    this.perCount = 10;	//\u663e\u793a\u51fa\u7684\u8df3\u8f6c\u9875\u7801\u6570,\u5982[1][2][3][4][5][6][7][8][9][10]
    this.totalrow = 0;
    this.turnTo;
    this.tid;

    this.text1 = '';
    this.text2 = '&nbsp;page';
    this.text3 = '';
    this.text4 = '&nbsp;page(s)';
    this.text5 = 'total records:';
    this.text6 = '';
    this.text7 = 'perpage:';
    this.text8 = '';
        //this.pageSizeList = new Array();
};

showPages.prototype.getPage = function()
{ //\u4e1burl\u83b7\u5f97\u5f53\u524d\u9875\u6570,\u5982\u679c\u53d8\u91cf\u91cd\u590d\u53ea\u83b7\u53d6\u6700\u540e\u4e00\u4e2a
    this.page = this.turnTo;
};

showPages.prototype.checkPages = function()
{ //\u8fdb\u884c\u5f53\u524d\u9875\u6570\u548c\u603b\u9875\u6570\u7684\u9a8c\u8bc1
    if (isNaN(parseInt(this.page))) this.page = 1;
    if (isNaN(parseInt(this.pageCount))) this.pageCount = 1;
    if (this.page < 1) this.page = 1;
    if (this.pageCount < 1) this.pageCount = 0;
    if (this.page > this.pageCount) this.page = this.pageCount;
    this.page = parseInt(this.page);
    this.pageCount = parseInt(this.pageCount);

};

showPages.prototype.createHtml = function(mode)
{ //\u751f\u6210html\u4ee3\u7801

    var strHtml = '';
    var prevPage = this.page - 1;//\u524d\u4e00\u9875
    var nextPage = this.page + 1;// \u4e0b\u4e00\u9875
    var isdisabled = "";
    if (this.totalrow == 0)
    {
        this.page = 0;
        isdisabled = "disabled"
    }

    var ml = this.getMultilanguage_resource();

    strHtml += ml[0] + '<span>' + this.page + '</span>' + ml[1] + '<span>/</span> ' + ml[2] + '<span id="ssb_ria_table_pageCount">' + this.pageCount + '</span>' + ml[3];
    strHtml += '&nbsp;&nbsp;&nbsp;' + ml[4] + '<span id="ssb_ria_table_totalrow">' + this.totalrow + '</span>' + ml[5] + '&#160; ';
    strHtml += ml[6];
    strHtml += '<select name="perPageCount" onchange="tableModel_arr.' + this.tid + '.' + this.name + '.initPre(this);"' + isdisabled + '>';
    for (var i = 0; i < tableModel_arr[this.tid].pageSizeList.length; i++)
    {
        //\u5224\u65ad\u5f53\u524d\u6bcf\u9875\u591a\u5c11\u884c
        var chkSelect;
        if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pageSizeList[i])
        {
            chkSelect = ' selected="selected"';
        }
            //\u5224\u65ad\u662f\u5426\u662f\u663e\u793a\u5168\u90e8
        else if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pg.totalrow && isNaN(tableModel_arr[this.tid].pageSizeList[i]))
        {
            chkSelect = ' selected="selected"';
        }
        else {
            chkSelect = '';
        }
        strHtml += '<option value="' + tableModel_arr[this.tid].pageSizeList[i] + '"' + chkSelect + '>' + tableModel_arr[this.tid].pageSizeList[i] + '</option>';
    }
    strHtml += '</select> ' + ml[7] + '&#160;&nbsp;&nbsp;';
    if (prevPage < 1) {
        strHtml += '<span ><input class="pageNav firstPageD" disabled type=button></span>';
        strHtml += '<span ><input class="pageNav prevPageD" disabled type=button></span>';

    } else {
        strHtml += '<span ><input class="pageNav firstPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(1);"></span>';
        strHtml += '<span ><input class="pageNav prevPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + prevPage + ');"></span>';
    }
          
          //\u5224\u65ad\u662f\u5426\u663e\u793a\u6587\u672c\u6846\u8df3\u8f6c
    if (!tableModel_arr[this.tid].isList)
    {
        if (this.page != 0)
        {
            strHtml += '<input id="ssbInputToPage" class="toPage" value="' + this.page + '"/>&nbsp;';
            strHtml += '<input type="button" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
        else
        {
            strHtml += '<input id="ssbInputToPage" class="toPage" disabled="disabled" value="' + this.page + '"/>&nbsp;';
            strHtml += '<input type="button"  disabled="disabled" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
    }
        //\u5426\u5219\u4f7f\u7528\u4e0b\u62c9\u6846
    else
    {
        if (this.pageCount < 1) {
            strHtml += '<select name="toPage" disabled="disabled">';
            strHtml += '<option value="0">0</option>';
        } else {
            var chkSelect;
            strHtml += '<select name="toPage" onchange="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this);">';
            for (var i = 1; i <= this.pageCount; i++) {
                if (this.page == i) chkSelect = ' selected="selected"';
                else chkSelect = '';
                strHtml += '<option value="' + i + '"' + chkSelect + '>' + i + '</option>';
            }
        }
        strHtml += '</select>';
    }

    if (nextPage > this.pageCount) {
        strHtml += '<span ><input class="pageNav nextPageD" disabled type=button></span>';
        strHtml += '<span ><input class="pageNav lastPageD" disabled type=button></span>';
    } else {
        strHtml += '<span ><input class="pageNav nextPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + nextPage + ');"></span>';
        strHtml += '<span ><input class="pageNav lastPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + this.pageCount + ');"></span>';
    }

    if (tableModel_arr[this.tid].isExcel == 'true')
    {
        if (tableModel_arr[this.tid].isExcelConfig == 'true')
        {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:configExcel(' + this.tid + ')"></span>';
        } else {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:ExportExcel(' + this.tid + ')"></span>';
        }
    }
    strHtml += '';
    return strHtml;
};

showPages.prototype.createUrl = function (page)
{
    //\u751f\u6210\u9875\u9762\u8df3\u8f6curl
    if (isNaN(parseInt(page))) page = 1;
    if (page < 1) page = 1;
    if (page > this.pageCount) page = this.pageCount;
    var url = location.protocol + '//' + location.host + location.pathname;
    var args = location.search;
    var reg = new RegExp('([\?&]?)' + this.argName + '=[^&]*[&$]?', 'gi');
    args = args.replace(reg, '$1');
    if (args == '' || args == null) {
        args += '?' + this.argName + '=' + page;
    } else if (args.substr(args.length - 1, 1) == '?' || args.substr(args.length - 1, 1) == '&') {
        args += this.argName + '=' + page;
    } else {
        args += '&' + this.argName + '=' + page;
    }
    return url + args;
};

showPages.prototype.toPage = function(page)
{
    //\u9875\u9762\u8df3\u8f6c
    this.turnTo = 1;

    if (typeof(page) == 'object')
    {
        this.turnTo = page.options[page.selectedIndex].value;
    }
    else if (this.pageCount < page)
        this.turnTo = this.pageCount;
    else if (page <= 0)
        this.turnTo = 1;
    else
        this.turnTo = page;
	//\u7ffb\u9875
	if (tableModel_arr[this.tid].isserversort == true)
		getSortTurnPageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount);
	else
		getTurnPageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount);
};

showPages.prototype.printHtml = function(mode)
{
    //\u663e\u793ahtml\u4ee3\u7801
    this.getPageCount();//\u8ba1\u7b97\u603b\u9875\u6570
    this.getPage();//\u83b7\u53d6\u5f53\u524d\u9875\u7801
    this.checkPages();//\u68c0\u6d4b\u9875\u7801\u5408\u6cd5\u6027
    this.showTimes += 1;
       //\u9875\u7801\u9009\u62e9\u6846      
    //\u83b7\u53d6\u5206\u9875\u680f\u7684table\u5bf9\u8c61
    var dir_tr_td = document.getElementById(this.tid + '_pages');
     //\u521b\u5efa\u5206\u9875
    dir_tr_td.innerHTML = this.createHtml();
	
	var inputEvent = function(e){
		var evt = e || window.event;  
    	
		if(evt && evt.preventDefault)
			if((evt.which<48 || evt.which>57)) evt.preventDefault(); 
		else
			if((evt.keyCode<48 || evt.keyCode>57))   evt.returnValue=false;
		
		if((evt.target || evt.srcElement).value.charAt(0) == "0")
			(evt.target || evt.srcElement).value = "";
	};
	$("#ssbInputToPage").css("ime-mode","disabled").bind("keyup", inputEvent).bind("keypress", inputEvent).bind("paste", function(){return false;});
};

showPages.prototype.formatInputPage = function(e)
{ //\u9650\u5b9a\u8f93\u5165\u9875\u6570\u683c\u5f0f
    var ie = navigator.appName == "Microsoft Internet Explorer" ? true : false;
    if (!ie) var key = e.which;
    else var key = event.keyCode;
    if (key == 8 || key == 46 || (key >= 48 && key <= 57)) return true;
    return false;
};

showPages.prototype.initPre = function(e)
{ //\u8bbe\u5b9a\u6bcf\u9875\u663e\u793a\u884c\u6570     
    if (e != null)
    {
        if (e.value == "\u5168\u90e8" || e.value.toLowerCase() == "all")
            this.perPageCount = this.totalrow;
        else
            this.perPageCount = e.value;
    }
    else {
        this.perPageCount = 10; //\u6ca1\u9875\u591a\u5c11\u884c
    }


    tableModel_arr[this.tid].pg.perPageCount = this.perPageCount;

    this.getPageCount();
     //\u5982\u679c\u662f\u521d\u59cb\u8868\u683c, \u4e0d\u663e\u793a\u6570\u636e
    if (this.totalrow != 0)
    {
        //\u91cd\u7f6e\u6bcf\u9875\u663e\u793a\u6570\u636e\u6570\u540e,\u91cd\u65b0\u67e5\u8be2\u6570\u636e
        getTurnPageData(this.tid, tableModel_arr[this.tid].condition, '1', this.perPageCount);
	     // \u586b\u6570\u636e
        // setTableValue(tableModel_arr[this.tid].MtableId, pageinfo);
    }

};
showPages.prototype.getMultilanguage_resource = function()
{
    var res_entity = new Array();
    var text1 = this.text1.indexOf("$") == -1 ? this.text1 : "\u7b2c";
    var text2 = this.text2.indexOf("$") == -1 ? this.text2 : "\u9875";
    var text3 = this.text3.indexOf("$") == -1 ? this.text3 : "\u5171";
    var text4 = this.text4.indexOf("$") == -1 ? this.text4 : "\u9875";
    var text5 = this.text5.indexOf("$") == -1 ? this.text5 : "\u5171";
    var text6 = this.text6.indexOf("$") == -1 ? this.text6 : "\u6761\u8bb0\u5f55";
    var text7 = this.text7.indexOf("$") == -1 ? this.text7 : "\u6bcf\u9875";
    var text8 = this.text8.indexOf("$") == -1 ? this.text8 : "\u884c";
    res_entity.push(text1);
    res_entity.push(text2);
    res_entity.push(text3);
    res_entity.push(text4);
    res_entity.push(text5);
    res_entity.push(text6);
    res_entity.push(text7);
    res_entity.push(text8);

    return res_entity;
};

showPages.prototype.getPageCount = function()//\u8ba1\u7b97\u9875\u7801
{
    this.pageCount = 0;
     //\u5982\u679c\u65e0\u8bb0\u5f55,\u76f4\u63a5\u8fd4\u56de0
    if (this.totalrow == 0)
    {
        return this.pageCount;
    }
    else {
        if (this.totalrow % this.perPageCount == 0)
        {
            this.pageCount = parseInt(this.totalrow / this.perPageCount);
        }
        else {
            this.pageCount = parseInt(this.totalrow / this.perPageCount + 1);
        }
    }
};

LTrim = function(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1)
    {
        var j = 0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
        {
            j++;
        }
        s = s.substring(j, i);
    }
    return s;
};
RTrim = function(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length - 1)) != -1)
    {
        var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
        {
            i--;
        }
        s = s.substring(0, i + 1);
    }
    return s;
};
Trim = function(str)
{
    return RTrim(LTrim(str));
};

isIE = function()
{
    browser_name = navigator.appName;
    if (browser_name == "Microsoft Internet Explorer")
    {
        return true;
    }
    return false;
};

var table_Header_Resize = new Object();

var TableContants = {
    EC_ID : "zteTable_ssb",
    MIN_COL_WIDTH : 10,
    MIN_COLWIDTH : "30",
    DRAG_BUTTON_COLOR : "#3366ff",
    IE_WIDTH_FIX_A : 1,
    IE_WIDTH_FIX_B : 2,
    FF_WIDTH_FIX_A : -3,
    FF_WIDTH_FIX_B : -6

};

table_Header_Resize.onDragobj = false;
table_Header_Resize.Dragobj = null;
table_Header_Resize.DragobjSibling = null;
table_Header_Resize.MinColWidth = TableContants.MIN_COLWIDTH;

table_Header_Resize.DragobjBodyCell = null;
table_Header_Resize.DragobjBodyCellSibling = null;
table_Header_Resize.DragFormid = null;

table_Header_Resize.StartResize = function(event, obj, formid)
{
    var table = document.getElementById(formid);
    var e = event || window.event;
    if (!formid)
    {
        formid = TableContants.EC_ID;
    }

    obj.focus();
    document.body.style.cursor = E_RESIZE;
    var sibling = table_Header_Resize.getNextElement(obj.parentNode);

    var dx = e.screenX;

    obj.parentTdW = obj.parentNode.clientWidth;
    if (sibling == null)
    {
        return true;
        document.body.style.cursor = "";
    }
    obj.siblingW = sibling.clientWidth;
    obj.mouseDownX = dx;
    obj.totalWidth = obj.siblingW + obj.parentTdW;

    obj.oldSiblingRight = table_Header_Resize.getPosRight(sibling);

    table_Header_Resize.Dragobj = obj;
    table_Header_Resize.DragobjSibling = sibling;
    table_Header_Resize.onDragobj = true;

    table_Header_Resize.MinColWidth = TableContants.MIN_COL_WIDTH;

    if (!table_Header_Resize.MinColWidth || table_Header_Resize.MinColWidth == '' || table_Header_Resize.MinColWidth < 1)
    {
        table_Header_Resize.MinColWidth = TableContants.MIN_COLWIDTH;
    }
    table_Header_Resize.Dragobj.style.backgroundColor = TableContants.DRAG_BUTTON_COLOR;

    table_Header_Resize.Dragobj.parentTdW -= table_Header_Resize.Dragobj.mouseDownX;

    var cellIndex = table_Header_Resize.Dragobj.parentNode.cellIndex;

    try {
        table_Header_Resize.DragobjBodyCell = table.rows[0].cells[cellIndex];
        table_Header_Resize.DragobjBodyCellSibling = table_Header_Resize.getNextElement(table_Header_Resize.DragobjBodyCell);
    }
    catch(e) {
        table_Header_Resize.DragobjBodyCell = null;
    }

};

table_Header_Resize.DoResize = function(event)
{
    var e = event || window.event;

    if (table_Header_Resize.Dragobj == null)
    {
        return true;
    }
    if (!table_Header_Resize.Dragobj.mouseDownX)
    {
        return false;
    }
    document.body.style.cursor = E_RESIZE;

    var dx = e.screenX;

    var newWidth = table_Header_Resize.Dragobj.parentTdW + dx;

    var newSiblingWidth = 0;

    /* fix different from ie to ff . but I don't know why  */
    if (isIE())
    {
        newWidth = newWidth + TableContants.IE_WIDTH_FIX_A;
        newSiblingWidth = table_Header_Resize.Dragobj.totalWidth - newWidth + TableContants.IE_WIDTH_FIX_B;
    }
    else {
        newWidth = newWidth + TableContants.FF_WIDTH_FIX_A;
        newSiblingWidth = table_Header_Resize.Dragobj.totalWidth - newWidth + TableContants.FF_WIDTH_FIX_B;
    }
    if (newWidth > table_Header_Resize.MinColWidth && newSiblingWidth > table_Header_Resize.MinColWidth)
    {
        table_Header_Resize.Dragobj.parentNode.style.width = newWidth + "px";
        table_Header_Resize.DragobjSibling.style.width = newSiblingWidth + "px";
        try {
            table_Header_Resize.DragobjBodyCell.style.width = newWidth + "px";
            table_Header_Resize.DragobjBodyCellSibling.style.width = newSiblingWidth + "px";
            table_Header_Resize.DragobjBodyCell.width = newWidth + "px";
            table_Header_Resize.DragobjBodyCellSibling.width = newSiblingWidth + "px";
        } catch(e) {
        }
    }
    table_Header_Resize.onDragobj = true;

};

table_Header_Resize.EndResize = function(event)
{
    if (table_Header_Resize.Dragobj == null)
    {
        return false;
    }
    table_Header_Resize.Dragobj.mouseDownX = 0;
    document.body.style.cursor = "";
    table_Header_Resize.Dragobj.style.backgroundColor = "";
    table_Header_Resize.Dragobj = null;
    table_Header_Resize.DragobjSibling = null;

};

table_Header_Resize.resizeInit = function()
{
    document.onmousemove = table_Header_Resize.DoResize;
    document.onmouseup = table_Header_Resize.EndResize;
    document.body.ondrag = function()
    {
        return false;
    };
    document.body.onselectstart = function()
    {
        return table_Header_Resize.Dragobj == null;
    };

};

table_Header_Resize.getNextElement = function(node)
{
    if (!node)
    {
        return null;
    }
    var tnode = node.nextSibling;
    while (tnode != null)
    {
        if (tnode.nodeType == 1)
        {
            return tnode;
        }
        tnode = tnode.nextSibling;
    }
    return null;
};

table_Header_Resize.getPosLeft = function(elm)
{
    var left = elm.offsetLeft;
    while ((elm = elm.offsetParent) != null)
    {
        left += elm.offsetLeft;
    }
    return left;
};

table_Header_Resize.getPosRight = function(elm)
{
    return table_Header_Resize.getPosLeft(elm) + elm.offsetWidth;
};


convert = function(sValue, datatype)
{
    var re = /[^0-9.+-]/g;
    value = (sValue.nodeName == "#text") ? sValue.nodeValue : (sValue.value == null ? sValue.firstChild.data : sValue.value);
    if (value)
    {
        datatype = (datatype == null) ? "" : datatype;
        switch (datatype.toLowerCase())
                {
            case "int":
                rvalue = value.replace(re, '');
                return parseInt(rvalue);
            case "float":
                return parseFloat(value);
            default:
                return value.toString();
        }
    }

};


generateCompare = function(iCol, datatype)
{
    return function compareTRs(oTR1, oTR2)
    {
        var value1 = "";
        var value2 = "";
        if (oTR1.cells[iCol].firstChild != null)
        {
            value1 = convert(oTR1.cells[iCol].firstChild, datatype);
        }
        if (oTR2.cells[iCol].firstChild != null)
        {
            value2 = convert(oTR2.cells[iCol].firstChild, datatype);
        }
        if (value1 < value2)
        {
            return -1;
        }
        else if (value1 > value2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    };
};

var sortColumn_table = "";
var isAsc_table = {};
sortTable = function(sTableId, iCol, imgEl)
{
   if (typeof sTableId == 'object')    {
        var oTable = sTableId;
        var tableId = sTableId.id;
    }
    else {
        var oTable = document.getElementById(sTableId);
        var tableId = sTableId;
    }
    
    var tableId = tableId.substring(0, tableId.indexOf("_ssb"));
    var oTBody = oTable.tBodies[0];
    var oRows = oTBody.rows;
    if (oRows.length == 1) return;
    var aTRs = new Array();
	
	var sortASC = null;
    var sortDESC = null;
    if (isIE())
    {
        sortASC = "IEsortASC";
        sortDESC = "IEsortDESC";
    } else {
        sortASC = "FFsortASC";
        sortDESC = "FFsortDESC";
    }
    var sortflag = "";
   	$(tableModel_arr[tableId].table.rows[0]).find("img[id^='ssbSortImg']").each(function(){
   		if(this.id == imgEl.id)
   		{
   			if(this.className.toLowerCase().indexOf("asc") != -1) 
   				sortflag = "up";
   			else
   				sortflag = "down";
   		}
   		else
   			this.className = sortASC;
    });
	//\u4e0d\u7ba1\u524d\u53f0\u8fd8\u662f\u540e\u53f0\u6392\u5e8f, \u4e0a\u9762\u7684\u4ee3\u7801\u90fd\u9700\u8981\u6267\u884c
	
	//\u5224\u65ad\u662f\u5426\u540e\u53f0\u6392\u5e8f
	if(tableModel_arr[tableId].isserversort == true)
	{
		sortColumn_table = tableModel_arr[tableId].excel_props[iCol];
	
		if(sortColumn_table != "")
			isAsc_table[sortColumn_table] = true;
		
		if (sortflag == 'down')
	    {
	        isAsc_table[sortColumn_table] = false;
            imgEl.className = sortASC;
	    }
	    else 
		{
			isAsc_table[sortColumn_table] = true;
            imgEl.className = sortDESC;
	    }
		//\u91cd\u65b0\u4ece\u540e\u53f0\u67e5\u8be2\u6570\u636e, \u5982\u679c\u7528\u6237\u8bbe\u7f6e\u4e86\u7edf\u8ba1\u884c\u7684\u8bdd, \u5b83\u4e5f\u4f1a\u81ea\u52a8\u66f4\u65b0
		getSortTurnPageData(tableId, tableModel_arr[tableId].condition, tableModel_arr[tableId].pg.page, tableModel_arr[tableId].pg.perPageCount);
		
		return;
	}
	
	//\u524d\u53f0\u6392\u5e8f
    var util = tableModel_arr[tableId].vstatcaption;
    for (var i = 1; i < oRows.length; i++)
    {
        aTRs.push(oRows[i]);
    }
	
	//2008-09-02 wxr\uff1a\u5982\u679c\u4f7f\u7528\u4e86\u5408\u8ba1\uff0c\u5219\u5e94\u5148\u5c06\u6700\u540e\u4e00\u884c\u53d6\u51fa\u6765
	if(util)	aTRs.pop();	

	//\u4e25\u683c\u68c0\u67e5sortCol\uff0c\u53ea\u6709\u5f53\u5b83\u662f\u6570\u636e\u65f6\u624d\u6709\u6548
    if (oTable.sortCol.constructor == Number && oTable.sortCol == iCol)
    {
        aTRs.reverse();
        if (sortflag == 'down')
        {
            imgEl.className = sortASC;
        }
        else {
            imgEl.className = sortDESC;
        }

    }
    else {
        aTRs.sort(generateCompare(iCol, tableModel_arr[tableId].cells[iCol].datatype));
        oTable.sortflag = 'down';
        imgEl.className = sortDESC;
    }

    var oFragment = document.createDocumentFragment();
    oFragment.appendChild(oRows[0]);
    for (var i = 0; i < aTRs.length; i++)
    {
        setRowCss(aTRs[i], ROW_CLASS_EVEN, i);
        oFragment.appendChild(aTRs[i]);
    }

	//2008-09-02 wxr\uff1a\u5982\u679c\u4f7f\u7528\u4e86\u5408\u8ba1\uff0c\u5219\u5e94\u5728\u5c06\u628a\u5408\u8ba1\u884c\u653e\u5230\u6700\u540e
	if(util)	oFragment.appendChild(oRows[oRows.length-1]);	
	
    oTBody.appendChild(oFragment);
    oTable.sortCol = iCol;
};

SSB_Table_Init = function()
{
    //\u83b7\u53d6\u9875\u9762\u6240\u6709\u7684table\u6807\u7b7e
    if (isIE())
    {
        var IEtables = document.getElementsByTagName("table");
        for (var i = 0; i < IEtables.length; i ++)
        {
            //\u5982\u679ctable\u7684\u547d\u540d\u7a7a\u95f4\u662fz
            if (IEtables[i].scopeName.toUpperCase() == RIA_SCOPENAME)
            {
                var IEid = IEtables[i].id;   	  			
                if (IEid != null)
                {
                    init_ssb_table(IEid);
                }
            }
        }
    }
    else {//\u5728FF\u4e0b
        var FFtables = document.getElementsByTagName("z:table");
        for (var j = 0; j < FFtables.length; j ++)
        {
            var FFid = FFtables[j].getAttribute("id");
            if (FFid != null)
            {
                init_ssb_table(FFid);
            }
        }
    }

};
var evalParse = function ()
{
    var str = eval("arguments[0][arguments[1]]");
    if (str == undefined) str = "";
    return str;
};
   //\u8c03\u7528\u6846\u67b6\u521d\u59cb\u65b9\u6cd5
addOnloadEvent(SSB_Table_Init);

var cap = new Array();//excel\u6587\u4ef6\u5217\u6807\u9898
var pop = new Array();//\u5b9e\u4f53\u5c5e\u6027
var setCaptions = function(captions)
{
    cap = captions;
};
var setProps = function(props)
{
    pop = props;
};
var getCaptions = function(tid)
{
    var captions = tableModel_arr[tid].excel_title;
    return captions;
};
var getProps = function(tid)
{
    //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
    var props = tableModel_arr[tid].excel_props;
    return props;
};
var configExcel = function(tid)
{
    //\u5224\u65ad\u5f53\u524d\u8def\u5f84\u6709\u6ca1\u6709\u914d\u7f6e\u6587\u4ef6\uff0c\u5426\u5219\u4f7f\u7528/form/common/excel/\u4e0b\u7684\u914d\u7f6e\u6587\u4ef6  	
    var currpath = getHtmlString("./ExcelConfig.html");
    if (currpath)
    {
        if (typeof tid == "object")
        {
            tid = tid.id;
        }
        document.open('./ExcelConfig.html?tid=' + tid, '_blank', 'toolbar=0,location=0,direct=0,menubar=0,scrollbars=1, width=500,height=500');
    }
    else
    {
        var docPath = document.location.pathname.toString();
        //var reg = /\/[\w.]+/;
        var reg = /\/.+?\//;
        var projPath = docPath.match(reg)[0];
        if (typeof tid == "object")
        {
            tid = tid.id;
        }
        document.open(projPath + 'common/excel/ExcelConfig.html?tid=' + tid, '_blank', 'toolbar=0,location=0,direct=0,menubar=0,scrollbars=1, width=500,height=500');
    }
};
  
  //\u5bfc\u51fa\u662f\u521b\u5efa\u8868\u5355\u548c\u63d0\u4ea4\u8868\u5355
var ExportExcel = function(tid)
{
    if (typeof tid == "object")
    {
        tid = tid.id;
    }
    var table = getTableById(tid);
    if(tableModel_arr[tid].IS_EXCEL_STYLE == true)
    {
    	var excel_file_title = tableModel_arr[tid].excel_file_title;
    	var excel_file = tableModel_arr[tid].excel_file;
    }
     //\u5982\u679c\u8868\u683c\u67e5\u51fa\u7684\u6570\u636e\u5927\u5c0f\u4e0d\u4e3a\u7a7a\uff0c\u5219\u53ef\u4ee5\u5bfc\u51fa\u6587\u4ef6
    if (table.allcount != null && table.allcount != 0)
    {
        var condition = tableModel_arr[tid].condition;//\u67e5\u8be2\u6570\u636e\u7684\u6761\u4ef6
        //var cc = getBindObject(condition);//\u83b7\u53d6\u67e5\u8be2\u6761\u4ef6
        var sid = tableModel_arr[tid].sid;//table\u7684sid
        var url = document.getElementById(sid).getAttribute('url');//\u83b7\u53d6z:sevice \u6807\u7b7e\u7684URL
        var arr = url.split(".");
        var action = arr[0] + ".ssbxls";
        var form = document.getElementById('excel');
        form = null;
        if (form == null)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);
            form.style.display = "none";
            form.action = action;
            form.id = 'excel';
            form.method = 'post';
            if (tableModel_arr[tid].isExcelConfig == 'true')
            {
                //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
                var props = pop;
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
                var captions = cap;
            } else {
                //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
                var props = tableModel_arr[tid].props;
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
                var captions = tableModel_arr[tid].caption;
            }
            var con = JSON.stringify(condition);
            var json = con.substring(0, con.lastIndexOf("]")) + ",0" + "," + new String(table.allcount) + "]";
            var input = document.createElement("input");
            form.appendChild(input);
            input.name = "condition";
            input.value = json;

            var input = document.createElement("input");
            form.appendChild(input);
            input.name = "tableID";
            input.value = tid;

			if(tableModel_arr[tid].IS_EXCEL_STYLE == true)
			{
				for (var i = 0; i < props.length; i ++)
	            {
	                var prop = document.createElement("input");
	                form.appendChild(prop);
	                prop.type = "text";
	                prop.name = "prop";
	                prop.value = props[i];
	
	                var caption = document.createElement("input");
	                form.appendChild(caption);
	                caption.type = "text";
	                caption.name = "caption";
	                caption.value = captions[i];
	                
	                var excelfilestyle = document.createElement("input");
	                form.appendChild(excelfilestyle);
	                excelfilestyle.type = "text";
	                excelfilestyle.name = "excelfilestyle";
	                excelfilestyle.value = eval('excel_file.'+props[i]);
	                
	                var exceltitlestyle = document.createElement("input");
	                form.appendChild(exceltitlestyle);
	                exceltitlestyle.type = "text";
	                exceltitlestyle.name = "exceltitlestyle";
	                exceltitlestyle.value = eval('excel_file_title.'+props[i]);
	            }
			}
			else
			{
				for (var i = 0; i < props.length; i ++)
	            {
	                var prop = document.createElement("input");
	                form.appendChild(prop);
	                prop.type = "text";
	                prop.name = "prop";
	                prop.value = props[i];
	
	                var caption = document.createElement("input");
	                form.appendChild(caption);
	                caption.type = "text";
	                caption.name = "caption";
	                caption.value = captions[i];
	            }
			}
        }
        form.submit();
    }
};
 //\u521d\u59cb\u5316\u914d\u7f6e\u9875\u9762   
var initExcel = function()
{
    window.onblur = function() {
        window.focus();
    };
    var table = document.getElementById('config');
    var url = document.location.toString();
    var cookieID = window.opener.location.pathname.toString();
    var arr = url.split('=');
    var tid = arr[1];
    var tid_input = document.createElement("input");
    tid_input.type = "hidden";
    tid_input.id = "tid";
    tid_input.value = tid;
    document.body.appendChild(tid_input);
    var captions = window.opener.getCaptions(tid);
    var props = window.opener.getProps(tid);
    var prop = getCookie(cookieID + "prop");
    for (var i = 0; i < captions.length; i ++)
    {
        var row = table.insertRow(-1);
        var input = document.createElement('input');
        input.type = "checkbox";
        input.name = "name";
        input.value = props[i];
        row.insertCell(-1).appendChild(input);
        setRowCss(row, ROW_CLASS_EVEN);
        
        // Modified on 2008-08-28
        var ocell=row.insertCell(-1);
        ocell.innerHTML=captions[i];
        // innerText_function(row.insertCell(-1), captions[i]);
        // \u5224\u65ad\u662f\u5426\u4e3aIE\u6d4f\u89c8\u5668,\u5426\u5219\u9700\u7ed9\u975eIE\u751f\u6210\u4e00\u4e2ainnerText.
        if(!isIE()){
            HTMLElement.prototype.__defineGetter__("innerText",
                function(){
                   var anyString = "";
                   var childS = this.childNodes;
                   for(var i=0; i<childS.length; i++) {
                       if(childS[i].nodeType==1)
                       anyString += childS[i].innerText;
                       else if(childS[i].nodeType==3)
                       anyString += childS[i].nodeValue;
                   }
                   return anyString;
             });
             HTMLElement.prototype.__defineSetter__("innerText",
                 function(sText){
                 this.textContent=sText;
             });
        } 
        innerText_function(ocell, ocell.innerText);
        innerText_function(row.insertCell(-1), props[i]);

        if (prop != null)
        {
            var offset = prop.indexOf(props[i]);
            if (offset != -1)input.checked = "true";
        }
    }
};
var allcheck = function(o)
{
    var table = document.getElementById('config');
    var rows = table.rows;
    for (var i = 1; i < rows.length; i++)
    {
        var input = rows[i].getElementsByTagName('input');
        input[0].checked = o.checked;
    }
};
var next = function()
{
    var props = new Array();
    var captions = new Array();
    var caption = '';
    var prop = '';
    var table = document.getElementById('config');
    var rows = table.rows;
    for (var i = 1; i < rows.length; i++)
    {
        var input = rows[i].getElementsByTagName('input');
        if (input[0].checked == true)
        {
            caption += ";";
            prop += ";";
            var cells = rows[i].cells;
            if (isIE())
            {
                caption += cells[1].innerText;
                prop += cells[2].innerText;
                captions.push(cells[1].innerText);
                props.push(cells[2].innerText);
            } else {
                caption += cells[1].textContent;
                prop += cells[2].textContent;
                captions.push(cells[1].textContent);
                props.push(cells[2].textContent);
            }
        }
    }
    var cookieID = window.opener.location.pathname.toString();
    var utc = Date.UTC(2010, 12, 30);
    var date = new Date(utc);
    document.cookie = cookieID + "prop=" + escape(prop) + ";expires=" + date.toGMTString();
    var tid = document.getElementById("tid").value;
    window.opener.setCaptions(captions);
    window.opener.setProps(props);
    window.close();
    if (tid != '' && tid != null)
    {
        window.opener.ExportExcel(tid);
    }
};
var getCookie = function(cookie_name)
{
    var allcookies = document.cookie;
    var cookie_pos = allcookies.indexOf(cookie_name);
    if (cookie_pos != -1)
    {
        cookie_pos += cookie_name.length + 1;
        var cookie_end = allcookies.indexOf(";", cookie_pos - 1);
        if (cookie_end == -1)
        {
            cookie_end = allcookies.length;
        }
        var value = unescape(allcookies.substring(cookie_pos, cookie_end));
    }
    return value;
};
function uploadFile(o, sid, field, anotherField)
{
    var tr = o;
	//wanghao
    while (tr.tagName.toUpperCase() != 'TR')
    {
        tr = tr.parentNode;
        if (tr.tagName.toUpperCase() == 'BODY')
        {
            break;
        }
    }
    var obj = {};
    var list = new Array();
    obj.field = "";
    obj.anotherField = "";
    if (field != null)  obj.field = field;
    if (anotherField != null)  obj.anotherField = anotherField;
    var filepath = "";
    var desc = "uploadfile";
    var arr_upload = null;

    if (isIE())
    {
        arr_upload = tr.getElementsByTagName("upload");
    }
    else {
        arr_upload = tr.getElementsByTagName("z:upload");
    }
	
	//\u4fee\u6539\u8bb0\u5f552: wanghao
    if (arr_upload.length <= 0)
    {
        arr_upload = [];
        arr_upload.push(tr);
    }
	
	//wanghao
    __file_List = [];

    for (var i = 0; i < arr_upload.length; i ++)
    {
        var input_upload = arr_upload[i].getElementsByTagName("input");
        for (var i = 0; i < input_upload.length; i++)
        {
            if (input_upload[i].type.toUpperCase() == 'FILE')
            {
                filepath = input_upload[i].value;
			//wanghao
                __file_List.push(input_upload[i]);
            }
        }
        list.push(filepath);
        list.push('upload');
        list.push(filepath);
        obj.uploadtest = list;
	    
		//wanghao
        callSid(sid, obj);
    }
}
;
function SSB_RIA_initReturnValueObject(rtnObject, props)
{
    for (var i = 0; i < props.length; i ++)
    {
        if (props[i] != null)
        {
            var paras = props[i].split(".");
            if (paras.length > 1)
            {
                for (var j = 0; j < paras.length - 1; j++)
                {
                    var temp = paras[0];
                    for (var k = 1; k <= j; k ++)
                        temp = temp + "." + paras[k];
                    if (eval("rtnObject." + paras[j]) == undefined || eval("rtnObject." + paras[j]) == null)
                    {
                        eval("rtnObject." + temp + "= {};");
                    }
                }
            }
        }
    }

    return rtnObject;
}
;

//wanghao add
function uploadSingleFile(o, sid)
{
    var tr = o;

    while (tr.tagName.toUpperCase() != 'TR')
    {
        tr = tr.parentNode;
        if (tr.tagName.toUpperCase() == 'BODY')
        {
            break;
        }
    }

    var desc = "uploadfile";
    var arr_upload = null;

    if (isIE())
    {
        arr_upload = tr.getElementsByTagName("upload");
    }
    else {
        arr_upload = tr.getElementsByTagName("z:upload");
    }

    if (arr_upload.length <= 0)
    {
        arr_upload = [];
        arr_upload.push(tr);
    }

    __file_List = [];

    for (var i = 0; i < arr_upload.length; i ++)
    {
        var input_upload = arr_upload[i].getElementsByTagName("input");
        for (var i = 0; i < input_upload.length; i++)
        {
            if (input_upload[i].type.toUpperCase() == 'FILE')
            {
                __file_List.push(input_upload[i]);
            }
        }

        callSid(sid);
    }
}
			//
			//
			//\u6811\u7684\u7236\u7c7b
			//
var Tree_init=function(){};

//addOnloadEvent(Tree_init);

var SSB_Tree=function (treeData,div_id){
	var obj={};
	this.nodeListeners={};
	this.model={};
	this.setData(treeData);
	
	this.setObjLabel(div_id);
	
	var rootId="0";
	
	var field_id="id";

	var field_name="name";

	var field_children="children";
	
	var field_parentId="parentId";
	
	var field_enabled="enabled";
	
	var target="_self";
	
	var href="";
	
	var delay=false;
	
	var service="";
	
	//\u83dc\u5355\u662f\u5426\u53ef\u7528\u7684\u6807\u5fd7\u3002 Y\u53ef\u7528\uff0c N\u4e0d\u53ef\u7528 ,\u5f53\u83dc\u5355\u4e0d\u88ab\u7981\u7528\u65f6\uff0c\u83dc\u5355\u53d8\u7070
	var enabledFlag ="0";
	//\u6570\u636e\u662f\u5426\u5df2\u7ecf\u662f\u683c\u5f0f\u5316\u7684\u6811\u5f62\u6570\u636e
	this.isFormated=true;
	
	//\u6811\u7684onselectchange\u4e8b\u4ef6\u7684\u5904\u7406\u51fd\u6570
	this.onselectchange=function(pre,last){};

	//properties
	
	this.setRootId=function(root_id){
	   rootId=root_id;
	};
	
	this.getRootId=function(){
	   return rootId;
	};
	
	this.setField_id=function(f_id){
		field_id=f_id;
	};
	this.setField_name=function(f_name){
		field_name=f_name;
	};
	this.setField_children=function(f_children){
		field_children=f_children;
	};
	this.setField_parentId=function(f_parentId){
		//\u8bbe\u7f6e\u8fd9\u4e2a\u5b57\u6bb5\u8bf4\u660e\u6ca1\u6709\u88ab\u683c\u5f0f\u5316
		this.isFormated=false;
		field_parentId=f_parentId;
	};
	this.setField_enabled=function(f_enabled)
	{
	     field_enabled=f_enabled;
	};
	this.setEnabledFlag=function(flg)
	{
	     enabledFlag=flg;
	};
	this.setHref=function(h){
		href=h;
	};
	this.setTarget=function(t){
		target=t;
	};
	this.setDelay=function(d){
		delay=d;
	};
	this.setService=function(s){
		service=s;
	};
	
	this.getField_id=function(){
		return field_id;
	};
	this.getField_name=function(){
		return field_name;
	};
	this.getField_children=function(){
		return field_children;
	};
	this.getField_parentId=function(){
		return field_parentId;
	};
	this.getField_enabled=function()
	{
	   return field_enabled;
	};
    this.getEnabledFlag=function()
    {
        return enabledFlag;
    };
	this.getHref=function(){
		return href;
	};
	this.getTarget=function(){
		return target;
	};
	this.getDelay=function(){
		return delay;
	};
	this.getService=function(){
		return service;
	};
	
	this.setOnselectchange=function(fun){
		this.onselectchange=fun;
	};

	
	this.getDefaultIcon=function(element,level,index,parent){
		//\u5bf9\u8c61ID
		var _icon=document.createElement("input");
		_icon.type='button';
		var src="";
		var length=this.getChildren(parent).length;
		var div_id="div_"+SSB_Tree.Path;
		var isLast=(1+index)==length;
		switch(true){
			case this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.leafIcon;break;
			case this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lleafIcon;break;
			case !this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.closeIcon;
				_icon["onclick"]=SSB_Tree.defaultIconEvent;break;
			case !this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lcloseIcon;
				_icon["onclick"]=SSB_Tree.defaultLIconEvent;break;
		}
		_icon.setAttribute("tree_id",this.tree_id);
		_icon.setAttribute("div_id",div_id);		
		_icon.className=src;
		//\u8c03\u7528public\u65b9\u6cd5(\u7f3a\u7701\u4e3a\u7a7a)
		this.setIcon(element,level,index,_icon);
		return _icon;
	};
 
	 this.getDefaultNode=function(element,level,index,parent){
		var _node=document.createElement("DIV");
		_node.className='nodeDiv';
		_node.setAttribute("id",'id_'+SSB_Tree.Path);
		_node.setAttribute("level",level);
		_node.setAttribute("istreeleaf",this.isLeaf(element));
		_node.setAttribute("tree_id",this.tree_id);
		_node["model"]=element;
		//\u8bbe\u7f6e\u5f53\u524d\u8282\u70b9\u7684isLast\u5c5e\u6027\uff0c\u56e0\u4e3a\u662f\u5426\u6700\u540e\u4e00\u4e2a\u5143\u7d20\u5bf9\u9875\u9762\u663e\u793a\u6709\u5f71\u54cd
		_node.setAttribute("isLast",1+index==this.getChildren(parent).length);
		return _node;
	};
 
	this.getLine=function(element,level,index,nodeDiv){
		var lineIcon={};
		var src='';
		var iconBlank=this.blankIcon;
		var iconLine=this.lineIcon;
		//SSB_Tree.treeNodeStack\u662f\u8282\u70b9\u961f\u5217\uff0c\u56e0\u4e3a\u9700\u8981\u77e5\u9053\u5f53\u524d\u8282\u70b9\u8def\u5f84\u4e2d\u7684\u7236\u8282\u70b9\u662f\u5426\u662f\u5f53\u524d\u5c42\u7684\u6700\u6709\u4e00\u4e2a
		for(var i=0;i<SSB_Tree.treeNodeStack.length;i++){
			//\u5982\u679c\u8def\u5f84\u4e2d\u7684\u67d0\u4e00\u5c42\u662f\u6700\u540e\u4e00\u4e2a\u90a3\u4e48\u4e0d\u5e94\u8be5\u663e\u793a\u8fde\u7ebf
			var isLast=SSB_Tree.treeNodeStack[i];
			if(isLast){
				src=iconBlank;
			//\u5426\u5219\u663e\u793a\u8fde\u7ebf
			}else{
				src=iconLine;
			}
			lineIcon=document.createElement("input");
			lineIcon.type='button';
			lineIcon.className=src;
			nodeDiv.appendChild(lineIcon);
		}
	};

	this.nextLevel=function(element,level,index,parent){
		var level_id="div_"+SSB_Tree.Path;
		var div=document.createElement("DIV");
		var node_id="node_"+SSB_Tree.Path;
		div.setAttribute("id",level_id);
		//\u8bbe\u7f6e\u4e0b\u4e00\u5c42\u6240\u6709\u7236\u8282\u70b9\u7684ID
		div.setAttribute("node_id",node_id);
		div.style.display="none";
		div.setAttribute("level",level);
		div.setAttribute("newLevel","newLevel");
		return div;
	};
	this.getNodeText=function(element,level,index,node){
		var span=document.createElement('span');
		span.innerHTML=element[this.getField_name()];
		var enabledFlg=element[this.getField_enabled()];
		span.setAttribute("tree_id",this.tree_id);
		span["model"]=element;
		//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u53d8\u7070
		if(this.isGray(enabledFlg))
		{
		    span.style.color="gray";
		}
		//sujingui \u7528\u6237\u81ea\u5b9a\u4e49\u5b57\u4f53\u7684\u989c\u8272
		var textColor=this.changeTreeTextColor(element,level,index,node);
        if(textColor)
        {
            span.style.color=textColor;
        }
		span['onclick']=SSB_Tree.eventRouter;	
		span.className='nodeText';
		return span;
	};
	this.getNode=function(element,level,index,parent){
		var nodeDiv=this.getDefaultNode(element,level,index,parent);
		this.getLine(element,level,index,nodeDiv);
		nodeDiv.appendChild(this.getDefaultIcon(element,level,index,parent));
		var userElement=this.appendBeforeText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		//\u4fee\u6539 sujingui \u5728\u8282\u70b9\u524d\u9762\u52a0\u56fe\u7247
		var imgElement=this.addImageBeforeText(element,level,index,parent);
		if(imgElement){
		    nodeDiv.appendChild(imgElement);
		}
		nodeDiv.appendChild(this.getNodeText(element,level,index,parent));
		userElement=this.appendAfterText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		return nodeDiv;
	};

	this.plantTree=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);

			levelDiv.appendChild(nodeDiv);

			//\u5982\u679c\u5f53\u524d\u8282\u70b9\u4e0d\u662f\u53f6\u7ed3\u70b9\u5c31\u5904\u7406\u4ed6\u7684\u5b50\u8282\u70b9
			if(this.isBranch(current)){
				var div=this.nextLevel(current,level,i,parent);
				levelDiv.appendChild(div);
				this.plantTree(this.getChildren(current),1+level,current,div);
			}
			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};

	this.insertBeforeCurrentTopNode=function(tree,level,parent,levelDiv,menu_id){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			var index = -1;
			var allNodes = this.model;
			for(var j=0;j<allNodes.length;j++)
			{
				if(allNodes[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			var positionNode ;
			
			if(index != -1)
			{				
				positionNode = document.getElementById("id_"+allNodes[index][this.getField_id()]);
			}
			
			if(positionNode != "undefined" && positionNode != null)
			{
				levelDiv.insertBefore(nodeDiv,positionNode);
			}
			else
			{
				levelDiv.insertBefore(nodeDiv,levelDiv.firstChild);
			}		

			SSB_Tree.pathStack.pop();
			SSB_Tree.treeNodeStack.pop();
		}
	};

	this.insertAfterCurrentTopNode=function(tree,level,parent,levelDiv,menu_id){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			var index = -1;
			var allNodes = this.model;
			for(var j=0;j<allNodes.length;j++)
			{
				if(allNodes[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			var positionNode ;
			
			if(index != -1)
			{				
				positionNode = document.getElementById("id_"+allNodes[index][this.getField_id()]);
			}
			
			if(positionNode != "undefined" && positionNode != null)
			{
				levelDiv.insertBefore(nodeDiv,positionNode.nextSibling);
			}
			else
			{
				levelDiv.appendChild(nodeDiv);
			}		

			SSB_Tree.pathStack.pop();
			SSB_Tree.treeNodeStack.pop();
		}
	};
	
	//\u628a\u8282\u70b9\u52a0\u4e3a\u7b2c\u4e00\u4e2a\u8282\u70b9
	this.insertFirstOfTreeNode=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			levelDiv.insertBefore(nodeDiv,levelDiv.firstChild);

			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};
	
	//\u628a\u8282\u70b9\u52a0\u4e3a\u6700\u540e\u4e00\u4e2a\u8282\u70b9
	this.insertLastOfTreeNode=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			levelDiv.appendChild(nodeDiv);

			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};
};

SSB_Tree.prototype={
	
	//\u7a7a\u767d\u56fe\u7247
	blankIcon:"treeIcon tree_white",
	//\u8fde\u7ebf
	lineIcon:"treeIcon tree_line",
	//\u5173\u95ed\u7684\u975e\u53f6\u8282\u70b9
	closeIcon:"treeIcon tree_plus",
	//\u6253\u5f00\u7684\u975e\u53f6\u8282\u70b9
	openIcon:"treeIcon tree_minus",
	//\u53f6\u7ed3\u70b9\u56fe\u7247
	leafIcon:"treeIcon tree_blank",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u5173\u95ed\u65f6\u7684\u56fe\u7247
	lcloseIcon:"treeIcon tree_plusl",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u6253\u5f00\u65f6\u7684\u56fe\u7247
	lopenIcon:"treeIcon tree_minusl",
	//\u6700\u540e\u4e00\u4e2a\u53f6\u7ed3\u70b9
	lleafIcon:"treeIcon tree_blankl",
	
	setObjLabel:function(div_id){
		this.tree_id=div_id;
		this.objLabel=document.getElementById(div_id);
	},

	setData:function(treeData){
		if(treeData instanceof String||"string"==typeof treeData){
			var obj=eval(treeData);
		}else{
			var obj=treeData;
		}
		if(obj instanceof Array)
			this.model=obj;
		else
			this.model=[obj];
	},

	formatData:function(){
		var id=this.getField_id();
		var children=this.getField_children();
		var parentId=this.getField_parentId();
		var model=this.model;
		var hashMap={};
		
		var current={};
		var root={};
		root[id]=this.getRootId();
		model.splice(0,0,root);		
		var length=this.model.length;
		for(var i=0;i<length;i++){
			current=model[i];
			current[children]=[];
			hashMap[current[id]]=current;
		}
		var parent={};
		for(var i=0;i<length;i++){
			current=model[i];
			parent=hashMap[current[parentId]];
			if(parent)
				parent[children].push(current);
		}
		return root[children];
	},

	setNode:function(node){

	},
	
	setIcon:function(element,level,index,icon){
		return icon;
	},
	appendBeforeText:function(element,level,index,parent){
		return null;
	},
	addImageBeforeText:function(element,level,index,parent){
	    return null;
	},
	changeTreeTextColor:function(element,level,index,parent)
	{
	    return null;
	},
	appendAfterText:function(element,level,index,parent){
		return null;
	},
	isLeaf:function(element){
		if(this.getDelay()){
			var children=this.getChildren(element);
			if(!children)return true;
			return parseInt(children)==0;
		}
		return !this.isBranch(element);
	},
	isBranch:function(element){
		var children= this.getChildren(element);
		return children instanceof Array && children!=null && children.length!=0;
	},
	getChildren:function(element){
		return element[this.getField_children()];
	},

    isGray:function(enabledFlag)
    {
         var flg=this.getEnabledFlag();
         if(enabledFlag == flg)
         {
            return true;
         }
         else
         {
            return false;
         }
    },

	initTree:function(){
		if(!this.objLabel) return;
		this.destroy();
		if(!this.getDelay()&&!this.isFormated){
			this.model=this.formatData();
		}
		var id=this.getField_id();
		var children=this.getField_children();
		this.root={};
		this.root[id]=this.getRootId();
		this.root[children]=this.model;
		SSB_Tree.pathStack=[];
		SSB_Tree.treeNodeStack=[];
		SSB_Tree.Path='';
		if(!this.model)
		{
			return;
		}
		this.plantTree(this.model,1,this.root,this.objLabel);
		SSB_Tree.trees[this.tree_id]=this;
		//\u8bbe\u7f6e\u7f3a\u7701\u7684\u5355\u51fb\u4e8b\u4ef6
		this.addNodeListener("onclick",this.defaultNodeOnClick);
		
	},

	refresh:function(treeData){
		if(treeData)
			this.setData(treeData);
		if(this.objLabel){
			this.objLabel.innerHTML="";
			this.initTree();
		}
	},

	destroy:function(){
		//\u628a\u6807\u7b7e\u5185\u7684HTML\u7f6e\u7a7a
		if(this.objLabel)
			this.objLabel.innerHTML="";
		if(SSB_Tree.trees[this.tree_id]){
			//\u5220\u6389SSB_Tree.trees\u4e2d\u7684\u8fd9\u4e2a\u6811
			delete SSB_Tree.trees[this.tree_id];
			//\u628a\u5f53\u524d\u4e66\u7684\u76d1\u542c\u5668\u5bf9\u8c61\u7f6e\u7a7a
			this.nodeListeners={};
		}
	},

	gotoHref:function(node){
		 var evalHref=function(el,level,href){
		 	eval("var element=arguments[0];");
			var el_re=/\$\{([^\}]+)\}/;
			var tmp=href;
			var result=[];
			var a=[];
			if(!(a=el_re.exec(tmp))){
				return href;
			}
			while(a){
				result.push(RegExp.leftContext);
				result.push(eval(a[1]));
				a=el_re.exec(RegExp.rightContext);
			}
			result.push(RegExp.rightContext);
			return result.join("");
		};
		var href=this.getHref();
		if(href){
			href=evalHref(node["model"],node.getAttribute("level"),href);
			window.open(href,this.getTarget());
		}
	},
	defaultNodeOnClick:function(oEvent){
		var current_tree =SSB_Tree.getTreeByNode(this);
		if(current_tree.lastclick)
			current_tree.lastclick.style.fontWeight='normal';
		this.style.fontWeight='bolder';
		current_tree.lastclick=this;
		oEvent=oEvent||window.event;
		var node=oEvent.target||oEvent.srcElement;
		current_tree.gotoHref(node);
	},

	addNodeListener:function(event_type,handler){
		if(!this.nodeListeners[event_type]){
			this.nodeListeners[event_type]=[];
		}
		//\u5f53\u8be5\u4e8b\u4ef6\u7c7b\u578b \u5df2\u7ecf\u5b58\u5728\u8be5\u4e8b\u4ef6\u5904\u7406\u51fd\u6570\u65f6\uff0c\u4e0d\u80fd\u91cd\u590d\u6dfb\u52a0\u6b64\u51fd\u6570 \uff0c\u907f\u514d\u8be5\u51fd\u6570\u88ab\u6267\u884c\u591a\u6b21
		var listener=this.nodeListeners[event_type];		
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				return;
			}
		}
		
		this.nodeListeners[event_type].push(handler);		
		
		var spans = document.getElementById(this.tree_id).getElementsByTagName("span");
		//\u5c06\u6240\u6709\u83dc\u5355\u8282\u70b9\u52a0\u4e0a\u4e8b\u4ef6 2008-5-5
		for(var i=0;i<spans.length;i++)
		{
			spans[i][event_type] = SSB_Tree.eventRouter;
		}
	
	},
	
	removeNodeListener:function(event_type,handler){
		var listener=this.nodeListeners[event_type];
		if(!listener){
			return;
		}
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				listener.splice(i,1);
				return;
			}
		}
	},
	doselectchange:function(last){
		if(this.lastclick&&this.lastclick!=last)
			this.onselectchange(this.lastclick,last);
	},
	getParams:function(node){
		var evalParam=function(el,str){
			eval("var element=arguments[0];");
			return eval(str);
		};
		var params=[];
		if(!this.params)return;
		for(var i=0;i<this.params.length;i++){
			if(typeof this.params[i]=='object'){
				params.push(this.params[i].value);
			}else
				params.push(evalParam(node['model'],this.params[i]));
		}
		return params;
	},
	selectNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}

		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				if(spans[i].fireEvent){
					spans[i].fireEvent('onclick');
				}else if(spans[i].dispatchEvent){
					spans[i].dispatchEvent('click');
				}
				return true;
			}
		}
		return false;			
	},
	openNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}
		var isTreeLeaf='';
		var divNode={};
		var inputs={};
		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				divNode=spans[i].parentNode;
				isTreeLeaf=divNode.getAttribute('istreeleaf');
				//\u975e\u53f6\u8282\u70b9
				if(isTreeLeaf=='false'||!isTreeLeaf){
					inputs=divNode.getElementsByTagName('input');
					for(var j=0;j<inputs.length;j++){
						if(inputs[j].getAttribute('tree_id')){
							if(inputs[j].fireEvent){
								inputs[j].fireEvent('onclick');
							}else if(inputs[j].dispatchEvent){
								inputs[j].dispatchEvent('click');
							}
							break;
						}
					}
				}
				return true;
			}
		}
		return false;
	},
	addNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			this.model.push(element);
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertLastOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	addFirstNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}
			
			var elem = new Array();
			elem.push(element);
			sibling = elem.concat(sibling);
			this.model = sibling;
				
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertFirstOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	addLastNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			this.model.push(element);
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertLastOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	insertBeforeNode:function(element,menu_id){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			//wanghao add
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}

			var index = -1;
			for(var j=0;j<sibling.length;j++)
			{
				if(sibling[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			
			if(index == 0)
			{
				var elem = new Array();
				elem.push(element);
				sibling = elem.concat(sibling);
			}
			else
			{
				var beforeArray = sibling.slice(0,index);
				var afterArray = sibling.slice(index);
				sibling = beforeArray.concat(element).concat(afterArray);
			}
			this.model = sibling;
			
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertBeforeCurrentTopNode(insertElem,1,this.root,this.objLabel,menu_id);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u63d2\u5165\u5230\u76ee\u6807\u8282\u70b9\u7684\u524d\u9762
					//wanghao add
					var index = -1;
					for(var j=0;j<sibling.length;j++)
					{
						if(sibling[j].menuId == menu_id)
						{
							index = j;
							break;
						}
					}
					
					if(index == 0)
					{
						var elem = new Array();
						elem.push(element);
						sibling = elem.concat(sibling);
					}
					else
					{
						var beforeArray = sibling.slice(0,index);
						var afterArray = sibling.slice(index);
						sibling = beforeArray.concat(element).concat(afterArray);
					}
					
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	insertAfterNode:function(element,menu_id){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			//wanghao add
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}

			var index = -1;
			for(var j=0;j<sibling.length;j++)
			{
				if(sibling[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			if (index == sibling.length-1)
			{
				sibling.push(element);
			}
			else
			{
				var beforeArray = sibling.slice(0,index+1);
				var afterArray = sibling.slice(index+1);
				sibling = beforeArray.concat(element).concat(afterArray);
			}
			this.model = sibling;
					
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertAfterCurrentTopNode(insertElem,1,this.root,this.objLabel,menu_id);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u63d2\u5165\u5230\u76ee\u6807\u8282\u70b9\u7684\u540e\u9762
					//wanghao add
					var index = -1;
					for(var j=0;j<sibling.length;j++)
					{
						if(sibling[j].menuId == menu_id)
						{
							index = j;
							break;
						}
					}
					
					if (index == sibling.length-1)
					{
						sibling.push(element);
					}
					else
					{
						var beforeArray = sibling.slice(0,index+1);
						var afterArray = sibling.slice(index+1);
						sibling = beforeArray.concat(element).concat(afterArray);
					}
					
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	refreshNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var field_Id=this.getField_id();
		var name=this.getField_name();
		var children=this.getField_children();
		var id;
		if(typeof element=='object')
			id=element[field_Id];
		else if(typeof element=='string'){
			id=element;
		}
		for(var i=0;i<length;i++){
			if(id==spans[i]['model'][field_Id]){
				spans[i].innerHTML=element[name];
				var old=spans[i]['model'];
				spans[i]['model']=element;
				var divNode=spans[i].parentNode;
				divNode['model']=element;
				var model=[];
				if(divNode.parentNode.tagName!='DIV')
					model=this.model;
				else{
					var parentNode=divNode.parentNode.previousSibling;
					model=parentNode['model'][children];
				}
				for(var j=0;j<model.length;j++){
					if(model[j]==old){
						model.splice(i,1,element);break;
					}
				}
				break;
			}
		}
	},
	deleteNode:function(deleted){
		//check argument
		if(deleted == null)
		{
			return;
		}
		//if is not array ,add arg into array
		if(! (deleted instanceof Array))
		{
			deleted = [deleted];
		}
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var length2=deleted.length;
		var id=this.getField_id();
		var children = this.getField_children();
		for(var i=length -1 ;i>= 0;i--){
			for(var j=0;j<length2;j++){
			    //sujingui modify
			    var _span = spans[i];
			    if(_span == undefined || _span == null)
			    {
			       continue;
			    }
				var element=spans[i]['model'];
				if(deleted[j]==element[id]){
					var nodeDiv=spans[i].parentNode;
					var levelDiv=nodeDiv.parentNode;
					var tmp=nodeDiv.previousSibling;
					var previousNode=tmp!=null&&tmp.getAttribute('newLevel')=='newLevel'?tmp.previousSibling:tmp;
					var childrenDiv = nodeDiv.nextSibling;
					var parentNode = levelDiv.previousSibling;

					//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u90a3\u8981\u4fee\u6539\u5012\u6570\u7b2c\u4e8c\u4e2a\u8282\u70b9
					if(nodeDiv.getAttribute('isLast')+''=='true'){
						if(previousNode){//\u662f\u6700\u540e\u4e00\u4e2a\u4f46\u4e0d\u552f\u4e00\u3002\u6709\u524d\u7eed\u8282\u70b9\u3002
							var previousIcon = SSB_Tree.getIconByNode(previousNode);
							previousIcon.className=previousIcon.className+'l';
							//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u5012\u6570\u7b2c\u4e8c\u4e2a\u53c8\u4e0d\u662f\u53f6\u7ed3\u70b9
							if(previousNode.getAttribute('istreeleaf')+''=='false'){
								//\u4fee\u6539onclick\u4e8b\u4ef6,\u56e0\u4e3a\u66f4\u6539\u7684\u56fe\u6807\u4f1a\u4e0d\u4e00\u6837
								previousIcon.className=this.lcloseIcon;
								previousIcon['onclick']=SSB_Tree.defaultLIconEvent;
								//\u5982\u679c\u4e4b\u524d\u7684\u8282\u70b9\u7684\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d\u5c31\u628a\u4ed6\u4eec\u5220\u9664\uff0c\u8ba9\u4ed6\u91cd\u65b0\u52a0\u8f7d\uff0c\u4e0d\u7136\u4f1a\u51fa\u95ee\u9898
								if(!SSB_Tree.noUpdate(previousNode)){
									levelDiv.removeChild(tmp);
								}
							}
							levelDiv.removeChild(nodeDiv);
							//\u5982\u679c\u6709\u5b50\u8282\u70b9\u4e5f\u4e00\u8d77\u5220\u9664
							if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
								levelDiv.removeChild(childrenDiv);
							}
							previousNode.setAttribute('isLast','true');
						}else{//\u552f\u4e00\u7684
								//\u5982\u679c\u8fd9\u4e2a\u8282\u70b9\u662f\u7b2c\u4e00\u5c42\u7684\u5e76\u4e14\u552f\u4e00
							if(levelDiv.tagName!='DIV'){
								this.objLabel.innerHTML='';
							}else{
								//\u4e0d\u662f\u7b2c\u4e00\u5c42\u7684\u8282\u70b9
								//\u88ab\u5220\u9664\u7684\u662f\u552f\u4e00\u8282\u70b9,\u8981\u628a\u4ed6\u7684\u7236\u8282\u70b9\u8bbe\u7f6e\u6210\u53f6\u8282\u70b9
								var parentLevel = levelDiv.parentNode;
								var parentIcon = SSB_Tree.getIconByNode(parentNode);
								var className = parentIcon.className;
								if(className.charAt(-1+className.length)=='l')
									parentIcon.className=this.lleafIcon;
								else
									parentIcon.className=this.leafIcon;
									//\u8fd9\u4e2a\u8282\u70b9\u6240\u5728\u7684\u5c42\u5c06\u88ab\u4e00\u8d77\u5220\u9664
								parentLevel.removeChild(levelDiv);
								parentNode.setAttribute('istreeleaf','true');
								
								parentNode['model'][children]=[];
							}
						}
					}
					//\u5982\u679c\u4e0d\u662f\u6700\u540e\u4e00\u4e2a\u53ea\u7ba1\u5220\u9664\u5b83\u548c\u5b83\u7684\u5b57\u8282\u70b9
					else {
						levelDiv.removeChild(nodeDiv);
						//\u5982\u679c\u6709\u5b50\u8282\u70b9
						if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
							levelDiv.removeChild(childrenDiv);
						}
					}
					//\u66f4\u65b0model
					var childElement=[];
					if(element[this.getField_parentId()]==this.getRootId()){
						childElement=this.model;
					}else{
						childElement=parentNode['model'][children];
					}
					for(var k=0;k<childElement.length;k++){
						if(childElement[k]==element){
							childElement.splice(k,1);break;
						}
					}
				}
			}
		}
	}
};
	//static properties and function
	//\u4e0b\u6807\u5806\u6808
SSB_Tree.pathStack = [];
SSB_Tree.Path='';
	//\u8282\u70b9\u961f\u5217
SSB_Tree.treeNodeStack=[];

SSB_Tree.trees={};

SSB_Tree.getTreeByNode=function(obj){
	return SSB_Tree.trees[obj.getAttribute("tree_id")];
};

SSB_Tree.eventRouter=function(oEvent){
	var oEvent = oEvent || window.event;
	var event_type="on"+oEvent.type;
		//\u83b7\u53d6\u53d1\u751f\u4e8b\u4ef6\u7684\u6811
	var tree = SSB_Tree.getTreeByNode(this);
	tree.doselectchange(this);
		//\u83b7\u53d6\u5bf9\u5e94\u4e8b\u4ef6\u76d1\u542c\u5668\u5217\u8868
	var listeners=tree.nodeListeners[event_type];
	if(!listeners)return;
	for(var i=0;i<listeners.length;i++){
		listeners[i].call(this,oEvent);
	}
};
SSB_Tree.getIconByNode = function(node){
	var divs=node.getElementsByTagName('input');
	var length=divs.length;
	for(var i=0;i<length;i++){
		if(divs[i].getAttribute('div_id')!=null){
			return divs[i];
		}
	}
};
SSB_Tree.updateChildren=function(s,children){
	var tree=SSB_Tree.getTreeByNode(this);
	var divNode=this;
	var img=SSB_Tree.getIconByNode(divNode);
	//\u5982\u679c\u6ca1\u6709\u53ef\u66f4\u65b0\u7684\u5b57\u8282\u70b9
	if(children.length==0){
		if(divNode.getAttribute('isLast')+''=='true') img.className=tree.lleafIcon;
		else img.className=tree.leafIcon;
		img.onclick=null;
		return;
	}
	//\u8bbe\u7f6e\u8282\u70b9\u8def\u5f84\u4fe1\u606f
	SSB_Tree.setNodePath(divNode);
	var level=parseInt(divNode.getAttribute("level"));
	//\u65b0\u5c42\u6b21\u7684DIV
	var div=tree.nextLevel({},level,0,{});
	div.style.display='block';
	divNode['model'][tree.getField_children()]=children;
	//\u5f80\u65b0\u5c42\u6b21\u7684DIV\u4e2d\u6dfb\u52a0\u65b0\u7684\u8282\u70b9
	tree.plantTree(children,1+level,divNode['model'],div);
	
	var obj=divNode.nextSibling;
	var parent=divNode.parentNode;
	//\u628a\u65b0\u7684\u5c42\u6b21\u52a0\u5165\u5230\u6811\u4e2d
	if(obj)
		parent.insertBefore(div,obj);
	else
		parent.appendChild(div);
	var isLast=divNode.getAttribute('isLast')+'';
	//\u6539\u53d8\u88ab\u66f4\u65b0\u7684\u8282\u70b9\u7684\u56fe\u6807
	if(isLast=='true') {
		img.className=tree.lopenIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultLIconEvent;
		}
	}
	else {
		img.className=tree.openIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultIconEvent;
		}
	}
	divNode.setAttribute('istreeleaf','false');
};

SSB_Tree.setNodePath=function(node){
	var div={};
	SSB_Tree.pathStack=[];
	SSB_Tree.treeNodeStack=[];
	SSB_Tree.Path='';
	var tree=SSB_Tree.getTreeByNode(node);
	do{
		SSB_Tree.treeNodeStack.push(node.getAttribute('isLast')+''=='true');
		SSB_Tree.pathStack.push(node['model'][tree.getField_id()]);
		div=node.parentNode;
		node=div.previousSibling;
	}while(div.tagName=='DIV');
	SSB_Tree.pathStack.reverse();
	SSB_Tree.treeNodeStack.reverse();
	SSB_Tree.Path=SSB_Tree.pathStack.join('_');
};

SSB_Tree.noUpdate=function(node){
	var isLast=node.getAttribute('isLast')+'';
	if(isLast=='true')
		return node.nextSibling==null;
	return node.nextSibling.getAttribute('newLevel')!='newLevel';
};



SSB_Tree.handleUpdate=function(img,tree){
	if(tree.getDelay()){
		var divNode=img.parentNode;
		var params=tree.getParams(divNode);
		callMethod(tree.getService(),params,divNode,SSB_Tree.updateChildren);
	}
};
SSB_Tree.defaultIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree = SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.openIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.closeIcon;
	}
};

SSB_Tree.defaultLIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree =SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.lopenIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.lcloseIcon;
	}
};


			//
			//\u591a\u9009\u6811
			//
function SSB_MultiTree(treeData,div_id,values,autoselect){
	SSB_Tree.call(this, treeData,div_id);
	this.setValues(values);
	this.autoselect=autoselect||false;
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_MultiTree.prototype=new SSB_Tree();
//\u91cd\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_MultiTree.prototype.constructor =SSB_MultiTree;

SSB_MultiTree.prototype.appendBeforeText=function(element,level,index,parent){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	var box=[];
	box.push("<input type='checkbox' style='margin:0px' value='");
	box.push(value);
	box.push("' name='checkbox_");
	box.push(this.tree_id);
	box.push("' onclick='SSB_MultiTree.checkTheBoxs(this)' ");
	for(var i=0;i<this.selectedValues.length;i++){
		if(value==this.selectedValues[i]){
			box.push("checked=true");
		}
	}
	box.push("/>");
	div.innerHTML=box.join("");
	div.style.display="inline";
	
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u591a\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

//\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
SSB_MultiTree.prototype.selectedValues=[];

SSB_MultiTree.prototype.setValues=function(values){
	if(values&&values instanceof Array){
		this.selectedValues=values;
	}
	else{
		this.selectedValues=[];
	}
};

SSB_MultiTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName('input');
	var values=[];
	for(var i=0;i<checkboxs.length;i++){		
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].value);
		}
	}
	return values;
};
SSB_MultiTree.prototype.getObjValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName("input");
	var values=[];
	for(var i=0;i<checkboxs.length;i++){
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].parentNode.parentNode['model']);
		}
	}
	return values;
};
SSB_MultiTree.prototype.refresh=function(treeData,values){
	if(treeData)
		this.setData(treeData);
	if(values)
		this.setValues(values);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};
SSB_MultiTree.checkTheBoxs=function(box){
	var name=box.getAttribute("name");
	var treeIdRE=/^checkbox_(\S+)$/;
	var treeId=treeIdRE.exec(name)[1];
	var tree=SSB_Tree.trees[treeId];
	if(tree&&tree.autoselect){
		var span=box.parentNode.nextSibling;
		if(span.getAttribute('istreeleaf')=='false'||!span.getAttribute('istreeleaf')){
			var levelDiv=span.parentNode.nextSibling;
			if(!levelDiv||!levelDiv.getAttribute("newLevel"))return;
			var checkboxs=levelDiv.getElementsByTagName('input');
			for(var i=0;i<checkboxs.length;i++){
				checkboxs[i].checked=box.checked;
			}
		}
	}
};



			//
			//\u5355\u9009\u6811
			//
var SSB_SingleTree=function(treeData,div_id,value){
	SSB_Tree.call(this, treeData,div_id);
	//\u9ed8\u8ba4\u7684\u9009\u5b9a
	this.selectedValue="";
	this.setValue(value);
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_SingleTree.prototype=new SSB_Tree();
//\u4ece\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_SingleTree.prototype.constructor =SSB_SingleTree;

SSB_SingleTree.prototype.appendBeforeText=function(element,level,index,node){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	
	//\u5728IE\u4e2d\u7528document.createElement\u52a8\u6001\u521b\u5efa\u7684HTML\u5bf9\u8c61\uff0c\u4e0d\u80fd\u52a8\u6001\u5730\u8bbe\u7f6e\u5b83\u7684name\u5c5e\u6027\uff0c
	//\u800c\u8fd9\u91cc\u5fc5\u987b\u7528radio button\u7684name\u5c5e\u6027\uff0c\u6240\u4ee5\u8fd9\u91cc\u7528\u62fc\u5b57\u7b26\u4e32\u7684\u65b9\u5f0f\u3002
	var radio=[];
	radio.push("<input");
	radio.push("type='radio'");
	radio.push("value='"+value+"'");
	radio.push("name='radio_"+this.tree_id+"'");
	if(value==this.selectedValue){
		radio.push("checked=true");
	}
	radio.push("/>");

	div.innerHTML=radio.join(" ");

	div.style.display="inline";
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u5355\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

SSB_SingleTree.prototype.setValue=function(value){
	if(value&&""!=value){
		this.selectedValue=value;
	}
	else{
		this.selectedValue="";
	}
};

SSB_SingleTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var radios=treeDiv.getElementsByTagName("input");

	for(var i=0;i<radios.length;i++){
		if(radios[i].checked&&radios[i].type.toLowerCase()=='radio'){
			return radios[i].value;
		}
	}
};

SSB_SingleTree.prototype.refresh=function(treeData,value){
	if(treeData)
		this.setData(treeData);
	if(value)
		this.setValue(value);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};



			//
			//\u6811\u5de5\u5382\uff0c\u6839\u636e\u6807\u7b7eID\u8fd4\u56de\u6811\u5bf9\u8c61
			//
var Tree_Factory=function (data,label_id){
	var labelObj=document.getElementById(label_id);
	var rootId=labelObj.getAttribute("rootId");
	var treeType=labelObj.getAttribute("treeType");
	var field_id=labelObj.getAttribute("field_id");
	var field_name=labelObj.getAttribute("field_name");
	var field_children=labelObj.getAttribute("field_children");
	var field_parentId=labelObj.getAttribute("field_parentId");
	var field_enabled=labelObj.getAttribute("field_enabled");
	var enabledFlag=labelObj.getAttribute("enabledFlag");
	var href=labelObj.getAttribute('href');
	var target=labelObj.getAttribute('target');
	var autoselect=labelObj.getAttribute('autoselect')=="true";
	var params=labelObj.getAttribute('params');
	var service=labelObj.getAttribute("service");
	var delay=labelObj.getAttribute("delay")=='true'; 
	if(data.treeType){
		treeType=data.treeType;
	}
	var initValue=data.initValue;
	var selectedValue=data.selectedValue;
	initValue=!initValue?data:initValue;
	var tree={};
	switch(treeType){
		case "0":tree=new SSB_Tree(initValue,label_id);break;
		case "1":tree=new SSB_SingleTree(initValue,label_id,selectedValue);break;
		case "2":tree=new SSB_MultiTree(initValue,label_id,selectedValue,autoselect);break;
		default:tree=new SSB_Tree(initValue,label_id);break;
	}
	if(rootId)
	   tree.setRootId(rootId);
	if(field_id)
		tree.setField_id(field_id);
	if(field_name)
		tree.setField_name(field_name);
	if(field_children)
		tree.setField_children(field_children);
	if(field_parentId)
		tree.setField_parentId(field_parentId);
    if(field_enabled)
        tree.setField_enabled(field_enabled);
    if(enabledFlag)
        tree.setEnabledFlag(enabledFlag);
	if(href)
		tree.setHref(href);
	if(target)
		tree.setTarget(target);
	if(service){
		tree.setService(service);
	}
	tree.setDelay(delay);
	if(params){
		var re=/(value|ctrl):([^,]+)/;
		var el=/^\$\{([^\}]+)\}$/;
		tree.params=[];
		var a=[];
		while((a=re.exec(params))){
			params=RegExp.rightContext;
			if(a[1]=='value'){
				var value=el.exec(a[2]);
				if(value)
					tree.params.push(value[1]);
				else 
					tree.params.push(a[2]);
			}else if(a[1]=='ctrl'){
				tree.params.push(document.getElementById(a[2]));
			}
		}
	}
	return tree;
};

// \u5c55\u5f00\u6240\u6709\u8282\u70b9
SSB_Tree.openAllNode = function(treeID)
{
	var spanList = document.getElementById(treeID).getElementsByTagName("span");
	if(!spanList)
	{
		return;
	}
	for(var i = 0;i<spanList.length;i++)
	{
		var node = spanList[i];
		var pNode = node.parentNode;
		if(!pNode.istreeleaf)
		{
			var childNode = pNode.childNodes;
			for(var j = 0;j<childNode.length;j++)
			{
				if(childNode[j].tagName)
				{
					var tagNames = childNode[j].tagName.toUpperCase();
					if(tagNames == "input")
					{
						var div=document.getElementById(childNode[j].getAttribute("div_id"));
						if(div == "undefined" || div == null || div.style.display=="none")
						{
							childNode[j].fireEvent('onclick');
						}
					}
				}
			}
		}
	}
};

//\u6536\u7f29\u6240\u6709\u8282\u70b9
SSB_Tree.closeAllNode = function(treeID)
{
	var spanList = document.getElementById(treeID).getElementsByTagName("span");
	if(!spanList)
	{
		return;
	}
	for(var i = 0;i<spanList.length;i++)
	{
		var node = spanList[i];
		var pNode = node.parentNode;
		if(!pNode.istreeleaf)
		{
			var childNode = pNode.childNodes;
			for(var j = 0;j<childNode.length;j++)
			{
				if(childNode[j].tagName)
				{
					var tagNames = childNode[j].tagName.toUpperCase();
					if(tagNames == "input")
					{
						var div = null;
						if(childNode[j].getAttribute("div_id"))
							div=document.getElementById(childNode[j].getAttribute("div_id"));
						
						if(div != null && 	div.style.display!="none")				
							childNode[j].fireEvent('onclick');
					}
				}
			}
		}
	}
};
Function.prototype.bindAsEventListener = function(object) {
	function $A(arr){
		var a=[];
		for(var i=0;i<arr.length;i++){
			a.push(arr[i]);
		}
		return a;
	};
  	var __method = this;
	var args = $A(arguments), 
	object = args.shift();
  	return function(event) {
    	return __method.apply(object, [event || window.event].concat(args));
  	};
};

function AUTO_init(){
	var atags=document.getElementsByTagName(_ZAUTO_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_AUTO_TAG);	
	}
	var autos={};
	for(var i=0;i<atags.length;i++){
		autos=new AutoCompleteTag(atags[i].getAttribute("id"));
		autos.init();
	}
};

addOnloadEvent(AUTO_init);

function AutoCompleteTag(id){
	var options={};
	var autoLabel=document.getElementById(id);
	options.matchAnywhere=autoLabel.getAttribute("matchAnywhere")||'true';
	options.ignoreCase=autoLabel.getAttribute("ignoreCase")||'false';
	options.queryId=autoLabel.getAttribute("queryId");
	options.count=autoLabel.getAttribute("count")||10;
	var params=autoLabel.getAttribute("params");
	var url=autoLabel.getAttribute("url");
	var ctlId=autoLabel.getAttribute("ctrlId");
	return new SSB_AutoComplete(id,ctlId,url,options,params);
};

function SSB_AutoComplete(labelId,anId,url,options,params){
	this.labelId=labelId;
    this.id          = anId;
    var browser	   = navigator.userAgent.toLowerCase();
    this.isIE        = browser.indexOf("msie") != -1;
    this.isOpera     = browser.indexOf("opera")!= -1;
    this.textInput   = document.getElementById(this.id);
    this.suggestions = [];
    this.setOptions(options);
    this.initAjax(url);
	this.setParams(params);
	this.nonReturn='';
};

SSB_AutoComplete.prototype = {

	initAjax: function(url) {
		this.url=url;
   },

	setParams:function(params){
		if(!params){//\u5982\u679c\u6ca1\u6709\u8bbe\u7f6e\u53c2\u6570\u5c31\u628a\u5f53\u524dinput\u7684\u503c\u52a0\u8fdb\u6765
			params='ctrl:'+this.id;
		}
		this.params=[];
		var param_re=new RegExp("(ctrl|value):([^,]+)", "i");
		var a=[];
		var tmp=params;
		while((a=param_re.exec(tmp))!=null){
			if(a[1].toUpperCase()=='CTRL'){
				this.params.push(document.getElementById(a[2]));
			}else if(a[1].toUpperCase()=='VALUE'){
				this.params.push(a[2]);
			}
			tmp=RegExp.rightContext;
		}
	},

	setOptions: function(options) {
		function extend(obj1,obj2){
			for(var property in obj1){
				if(obj2[property]){
					obj1[property]=obj2[property];
				}
			}
		};
		this.options = {
	     	//\u81ea\u52a8\u5b8c\u6210\u4e0b\u62c9\u83dc\u5355DIV\u5143\u7d20\u7684css class
			suggestDivClassName	: 'auto_list',
		 	//\u5f53\u9f20\u6807\u79bb\u5f00\u9009\u9879\u65f6\u7684css class
			mouseoutClassName	: 'auto_item_mouseout',
		 	//\u5f53\u9f20\u6807\u79fb\u52a8\u5230\u9009\u9879\u65f6\u7684css class
			mouseoverClassName	: 'auto_item_mouseover',
		 	//\u88ab\u5339\u914d\u7684\u6587\u672c\u7684css class
			matchClassName     	: 'auto_item_match',
		 	//\u662f\u5426\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
			matchAnywhere      	: 'true',
		 	//\u662f\u5426\u533a\u5206\u5927\u5c0f\u5199
			ignoreCase			: 'false',
		 	//\u4e0b\u62c9\u83dc\u5355\u4e2d\u6761\u76ee\u4e2a\u6570
			count              	: 10,
		 	//\u67e5\u8be2\u8bed\u53e5ID
			queryId				: ""
		};

	  extend(this.options,options);
   },

   	init: function() {
      	this.textInput.setAttribute("autocomplete","off");
		
      	var keyEventHandler = new TextSuggestKeyHandler(this);

      	this.createSuggestionsDiv();

	  	SSB_AutoComplete.autoObjs[this.labelId]=this;
   	},

	getValue:function(){
		return document.getElementById(this.id).value;
	},

   	handleTextInput: function() {
     	var previousRequest    = this.lastRequestString;
     	this.lastRequestString = this.textInput.value;
     	if ( this.lastRequestString == "" )
        	this.hideSuggestions();
     	else if ( this.lastRequestString != previousRequest ) {
     		if(this.nonReturn!=''&&this.lastRequestString.indexOf(this.nonReturn)!=-1)
     			return;
     		else
        		this.sendRequestForSuggestions();
     	}else if(this.suggestions.length!=0){
     		this.showSuggestions();
     	}
   	},

	moveSelectionUp: function() {
		if ( this.selectedIndex > 0 ) {
			this.updateSelection(this.selectedIndex - 1);
		}
	},

	moveSelectionDown: function() {
		if ( this.selectedIndex < (this.suggestions.length - 1)  ) {
			this.updateSelection(this.selectedIndex + 1);
		}
	},

	updateSelection: function(n) {
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoutClassName;
		}
		this.selectedIndex = n;
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoverClassName;
		}
   	},

	sendRequestForSuggestions: function() {
		if ( this.handlingRequest ) {
			this.pendingRequest = true;
			return;
		}
		this.handlingRequest = true;
		this.callRIAService();
	},

	getParams:function(callParms){
		for(var i=0;i<this.params.length;i++){
			if('string'==typeof this.params[i]){
				callParms.push(this.params[i]);
			}else if('object'==typeof this.params[i]){
				//\u5982\u679c\u8fd9\u4e2aHTML\u5bf9\u8c61\u662f\u5f53\u524d\u81ea\u52a8\u5b8c\u6210input\u5bf9\u8c61
				if(this.params[i]==this.textInput){
					var value=this.lastRequestString;
					//\u5982\u679c\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
					if(this.options.matchAnywhere=='true'){
						value='%'+value+'%';
					}//\u5982\u679c\u5339\u914d\u5f00\u59cb\u4f4d\u7f6e
					else{
						value=value+'%';	
					}
					if(this.options.ignoreCase=='true'){
						value=value.toUpperCase();
					}
					callParms.push(value);	
				//\u5982\u679c\u662f\u5176\u4ed6HTML\u5bf9\u8c61
				}else{//\u8fd9\u91cc\u5e94\u8be5\u7528RIA\u6846\u67b6\u7684processor\u5f97\u5230\u503c
					callParms.push(this.params[i].value);	
				}
			}
		}
	},

   	callRIAService: function() {
    	var callParms = [];
		this.getParams(callParms);
		var param={};//\u53c2\u6570\u5bf9\u8c61
		param.count=10;
		param.ignoreCase=this.options.ignoreCase;
		param.paras=callParms;
		param.queryId=this.options.queryId;

		callMethod(this.url||SSB_AutoComplete.URL,[param],this,this.setValue);
	},
	
	setValue:function(s,data){
		this.suggestions=data;
		this.nonReturn=data.length==0?this.lastRequestString:'';
		this.processSuggestions();
	},
	
	processSuggestions:function(){
		if ( this.suggestions.length==0 ) {
			this.suggestionsDiv.innerHTML='';
         	this.hideSuggestions();
      	}
      	else {
        	this.updateSuggestionsDiv();
         	this.showSuggestions();
         	this.updateSelection(0);
      	}

      	this.handlingRequest = false;

      	if ( this.pendingRequest ) {
         	this.pendingRequest    = false;
         	this.lastRequestString = this.textInput.value;
         	this.sendRequestForSuggestions();
      	}
	},
	
   	setInputFromSelection: function() {
     	var suggestion  = this.suggestions[ this.selectedIndex ];
     	if(!suggestion||this.suggestionsDiv.style.display=='none')return;
	 	this.textInput.value = suggestion;
		this.lastRequestString = suggestion;
     	this.hideSuggestions();
   	},

   	showSuggestions: function() {
      	var divStyle = this.suggestionsDiv.style;
      	if ( divStyle.display == 'block' )
         	return;
      	this.positionSuggestionsDiv();
      	divStyle.display = 'block';
		if(window.navigator.userAgent.indexOf("MSIE") == -1)
			divStyle.overflow='auto';
   	},

	positionSuggestionsDiv: function() {
		var selectedPosX = 0;
        var selectedPosY = 0;
		var divStyle = this.suggestionsDiv.style;
        var theElement = this.textInput;
        if (!theElement) return;
        var theElemHeight = theElement.offsetHeight;
        var theElemWidth = theElement.offsetWidth;
        while(theElement != null){
          selectedPosX += theElement.offsetLeft;
          selectedPosY += theElement.offsetTop;
          theElement = theElement.offsetParent;
        }
		divStyle.width = theElemWidth;
        divStyle.left = selectedPosX;
       	divStyle.top = selectedPosY + theElemHeight;
   },

   	hideSuggestions: function() {
      	this.suggestionsDiv.style.display = 'none';
      	var iframe = document.getElementById("iframe_id_autocomplete");
      	iframe.style.display = "none";
   	},

   	createSuggestionsDiv: function() {
		//sujingui
		//\u5728FF\u4e2d\u4f1a\u6321\u4f4f\u540e\u9762\u63a7\u4ef6\uff1a\u6d4b\u8bd5\u9875\u9762\u4e3aRiaDemo\u7684\u7b80\u5355\u8868\u683c
		
		var iframe = document.createElement("iframe");
		iframe.style.cssText = "position:absolute;z-index:7;width:expression(this.nextSibling.offsetWidth);height:expression(this.nextSibling.offsetHeight);top:expression(this.nextSibling.offsetTop);left:expression(this.nextSibling.offsetLeft);";
		iframe.frameborder = 0;
		iframe.setAttribute('id',"iframe_id_autocomplete");
		iframe.setAttribute('frameBorder',0);
		
		//
      	this.suggestionsDiv = document.createElement("div");
      	this.suggestionsDiv.className = this.options.suggestDivClassName;

      	var divStyle = this.suggestionsDiv.style;
      	divStyle.zIndex   = 8;
      	divStyle.display  = "none";

		    //this.textInput.parentNode.appendChild(iframe);
      	//this.textInput.parentNode.appendChild(this.suggestionsDiv);
      	
      	document.body.appendChild(iframe);
      	document.body.appendChild(this.suggestionsDiv);
   	},

   	updateSuggestionsDiv: function() {
      	this.suggestionsDiv.innerHTML = "";
      	var suggestLines = this.createSuggestionSpans();
      	for ( var i = 0 ; i < suggestLines.length ; i++ )
         	this.suggestionsDiv.appendChild(suggestLines[i]);
   	},

   	createSuggestionSpans: function() {
      	var regExpFlags = "";
      	if ( this.options.ignoreCase == 'true')
         	regExpFlags = 'i';
     	var startRegExp = "^";
      	if ( this.options.matchAnywhere == 'true')
         	startRegExp = '';

      	var regExp  = new RegExp( startRegExp + this.lastRequestString, regExpFlags );

      	var suggestionSpans = [];
      	for ( var i = 0 ; i < this.suggestions.length ; i++ )
         	suggestionSpans.push( this.createSuggestionSpan( i, regExp ) );

      	return suggestionSpans;
	},

   	createSuggestionSpan: function( n, regExp ) {
      	var suggestion = this.suggestions[n];

      	var suggestionSpan = document.createElement("span");
      	suggestionSpan.className 	 = this.options.mouseoutClassName;
      	suggestionSpan.style.display = 'block';
      	suggestionSpan.id            = this.id + "_" + n;
      	suggestionSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	suggestionSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);

	  	var suggestionText={};

	  	suggestionText=suggestion;

      	var textValues=this.splitTextValues(suggestionText ,
                                             this.lastRequestString.length,
                                             regExp );

      	var textMatchSpan = document.createElement("span");
      	textMatchSpan.id            = this.id + "_match_" + n;
      	textMatchSpan.className     = this.options.matchClassName;
      	textMatchSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	textMatchSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);
      	
      	textMatchSpan.appendChild( document.createTextNode(textValues.mid) );
      	
      	suggestionSpan.appendChild( document.createTextNode( textValues.start ) );
      	suggestionSpan.appendChild( textMatchSpan );
      	suggestionSpan.appendChild( document.createTextNode( textValues.end ) );
      	
      	return suggestionSpan;
   	},
   	
   	mouseoverHandler: function(e) {
		e=e||window.event;
   	   	var src = e.srcElement||e.target;
   	   	var index = parseInt(src.id.substring(src.id.lastIndexOf('_')+1));
   	   	this.updateSelection(index);
   	},
   	
   	itemClickHandler: function(e) {
   	   	this.mouseoverHandler(e);
   	   	if ( this.suggestionsDiv.style.display == 'block' )
   	   	   	this.setInputFromSelection();
   	   	this.hideSuggestions();
   	   	this.textInput.focus();
   	},
   	
   	splitTextValues: function( text, len, regExp ) {
   	   	var startPos  = text.search(regExp);
   	   	var matchText = text.substring( startPos, startPos + len );
   	   	var startText = startPos == 0 ? "" : text.substring(0, startPos);
   	   	var endText   = text.substring( startPos + len );
   	   	return { start: startText, mid: matchText, end: endText };
   	},
   	
   	getElementContent: function(element) {
   	   	return element.firstChild.data;
   	}
};

SSB_AutoComplete.autoObjs={};

function TextSuggestKeyHandler(autoObj){
   	this.autoObj = autoObj;
   	this.input   = this.autoObj.textInput;
   	this.addKeyHandling();
};
SSB_AutoComplete.getKeyCode=function(e){
	e=e||window.event;
	return e.keyCode||e.which;
};



//change by Daniel for attachevent memory leak -------------begin.

function getEvent(){     //\u036c\u02b1\ufffd\ufffd\ufffd\ufffdie\ufffd\ufffdff\ufffd\ufffd\u0434\ufffd\ufffd
         if(document.all)    return window.event;        
         func=getEvent.caller;            
         while(func!=null){    
             var arg0=func.arguments[0];
             if(arg0){
                 if((arg0.constructor==Event || arg0.constructor ==MouseEvent)
                     || (typeof(arg0)=="object" && arg0.preventDefault && arg0.stopPropagation)){    
                    return arg0;
               }
            }
             func=func.caller;
         }
        return null;
        }
         
function keyupHandler(obj) {
   	   	var upArrow   = 38;
   	   	var downArrow = 40;
   	   	var enterKey  = 13;
   	   	var escKey	  = 27;
   	   	if ( obj.input.value.length == 0 && !obj.isOpera ){
   	      	obj.autoObj.hideSuggestions();
   	      	return;
   	   	}
   	   	switch(getEvent().keyCode){
	   	   	case upArrow:	obj.autoObj.moveSelectionUp();
	   	   	   			 	setTimeout( function(){}, 1 );
	   	   				 	break;
	   	   	case downArrow:	obj.autoObj.moveSelectionDown();
	   	   					break;
	   	   	case enterKey:	if(obj.autoObj.suggestionsDiv.style.display=='none')
	   	   						obj.autoObj.handleTextInput();
	   	   					else
	   	   						obj.autoObj.setInputFromSelection();
	   	   					break;
	   	   	case escKey:	obj.autoObj.hideSuggestions();
	   	   					break;
	   	   	default:obj.autoObj.handleTextInput();
	   	}
   	};
   	
   function onblurHandler(obj) {
   	   	if(obj.autoObj.suggestionsDiv.style.display == 'none')
   	   	   	return;
   	   	var event=getEvent();
		var toElement=event.explicitOriginalTarget||event.toElement;
		if(!toElement) return;

		var autolist=obj.autoObj.suggestionsDiv;
		do{
			if(toElement==autolist)
				return;
			toElement=toElement.parentNode;
		}while(toElement);
		autolist.style.display="none";
   	} ;
   	
   		
   	var leakKeyUp = function( obj ){ 
			return function(){
				keyupHandler(obj);
			}
	};
	
	var leakOnblur = function( obj ){ 
			return function(){
				onblurHandler(obj);
			}
	};
	
	
//change by Daniel for attachevent memory leak -------------end.
TextSuggestKeyHandler.prototype = {

   	addKeyHandling: function() {
   	   	//this.input.onkeydown = this.preventDefault.bindAsEventListener(this);
   	   	//this.input.onkeyup   = this.keyupHandler.bindAsEventListener(this);
   	   	//this.input.onblur    = this.onblurHandler.bindAsEventListener(this);
   	   	//this.input.onfocusout = this.onblurHandler.bindAsEventListener(this);
   	   	EventManager.Add(this.input,'keyup',leakKeyUp(this));
   	    EventManager.Add(this.input,'keydown',this.preventDefault);
        EventManager.Add(this.input,'blur',leakOnblur(this));        
        EventManager.Add(this.input,'focusout',leakOnblur(this));
        
   	   	if ( this.isOpera )
   	   	    EventManager.Add(this.input,'keyup',leakKeyUp(this));
   	   	   	//this.input.onkeypress = this.keyupHandler.bindAsEventListener(this);
   	},
	preventDefault:function(e){
		var enterKey=13;
		var escKey=27;
		var key=getEvent().keyCode;
		if(key==enterKey||key==escKey){
			if(e.preventDefault){
				e.preventDefault();
			}else{
				e.returnValue=false;
			}
		}
	}
	/*
   	keyupHandler: function(e) {
   	   	var upArrow   = 38;
   	   	var downArrow = 40;
   	   	var enterKey  = 13;
   	   	var escKey	  = 27;
   	   	if ( this.input.value.length == 0 && !this.isOpera ){
   	      	this.autoObj.hideSuggestions();
   	      	return;
   	   	}
   	   	switch(SSB_AutoComplete.getKeyCode(e)){
	   	   	case upArrow:	this.autoObj.moveSelectionUp();
	   	   	   			 	setTimeout( function(){}, 1 );
	   	   				 	break;
	   	   	case downArrow:	this.autoObj.moveSelectionDown();
	   	   					break;
	   	   	case enterKey:	if(this.autoObj.suggestionsDiv.style.display=='none')
	   	   						this.autoObj.handleTextInput();
	   	   					else
	   	   						this.autoObj.setInputFromSelection();
	   	   					break;
	   	   	case escKey:	this.autoObj.hideSuggestions();
	   	   					break;
	   	   	default:this.autoObj.handleTextInput();
	   	}
   	},

   	onblurHandler: function(oEvent) {
   	   	if(this.autoObj.suggestionsDiv.style.display == 'none')
   	   	   	return;
   	   	var event=window.event||oEvent;
		var toElement=event.explicitOriginalTarget||event.toElement;
		if(!toElement) return;

		var autolist=this.autoObj.suggestionsDiv;
		do{
			if(toElement==autolist)
				return;
			toElement=toElement.parentNode;
		}while(toElement);
		autolist.style.display="none";
   	}
   	*/
};

SSB_AutoComplete.URL="autocomplete/getInitValueArr.ssm";

var _ZAUTO_TAG='z:autocomplete';
var _AUTO_TAG='autocomplete';
	function RIADownload(url,array)
	{
		var oForm=document.createElement('FORM');
		oForm.id='form_riadownload';
		oForm.action=url;
		oForm.method='POST';		
    	debugger;
		for(var i =0 ;i<array.length;i++)
		{
			var jsonInput=document.createElement('INPUT');
	    	jsonInput.value=array[i];
	    	jsonInput.name='downloadfileProp';
	    	jsonInput.type='hidden';
	    	oForm.appendChild(jsonInput);	    	
		}
		oForm.style.display = "none";
    	document.body.appendChild(oForm);
		
		var createIFrame=function(){
			var oIFrame={};
			try{
				oIFrame = document.createElement("<IFRAME name=frame_riadownload />");
			}catch(e){
				oIFrame=document.createElement("IFRAME");
				oIFrame.name='frame_riadownload';
			}
			oIFrame.name ='frame_riadownload';
			oIFrame.width=0;
			oIFrame.height=0;
			oIFrame.frameBorder=0;
			oIFrame.id='frame_riadownload';
			document.body.appendChild(oIFrame);
			return oIFrame;
		};
		var oFrame=createIFrame();
		//var oForm=document.getElementById('form_riadownload');
		var downloadCallback=function(){
			var thisDocument=oFrame.contentDocument||oFrame.contentWindow.document;
			var html=thisDocument.body.innerHTML;
			if(html=="undefined"||html==null||html.trim()=="")
			{}
			else
				alert(html);
			
			setTimeout(function(){try{
									oForm.parentNode.removeChild(oForm);
									oFrame.parentNode.removeChild(oFrame);
									} catch(e){
										throw e;
									}
								  }, 100);
		};
			if(window.attachEvent){
		    	//oFrame.attachEvent('onload', uploadCallback);
		    	EventManager.Add(oFrame,'load',downloadCallback);
		    }
		    else{
		        oFrame.addEventListener('load', downloadCallback, false);
		    }
		    oForm.setAttribute("target",oFrame.name);
    		oForm.submit();
	}
var _EXTEXTAREA="EXTEXTAREA";
var _ZEXTEXTAREA="Z:EXTEXTAREA";

//\u6846\u67b6\u521d\u59cb\u5316\u5165\u53e3\u51fd\u6570
function exTextArea_Init(){

	var exTaTags=document.getElementsByTagName(_EXTEXTAREA); //ie
	if(!exTaTags||exTaTags.length==0)	{
		exTaTags=document.getElementsByTagName(_ZEXTEXTAREA);	//ff
	}
	
	if(objIsExisted(exTaTags)) {
		for(var i=0;i<exTaTags.length;i++){
			var exta = new SSB_EXTEXTAREA(exTaTags[i]);
			exta.init();
		}
	}
}
addOnloadEvent(exTextArea_Init);

var EXTEXTAREA_DEFAULT_ID = "msgArea";
var EXTEXTAREA_DEFAULT_MAXLENGTH = 200;
var EXTEXTAREA_DEFAULT_MSGSTATION = "bottom";
var EXTEXTAREA_DEFAULT_COLS = 80;
var EXTEXTAREA_DEFAULT_ROWS = 3;
var EXTEXTAREA_DEFAULT_MSG = 'You can input another {#maxlength} character(s)';//\u4f1a\u505a\u591a\u8bed\u5316\u5904\u7406
function SSB_EXTEXTAREA(exTaTags)
{
	this.id = EXTEXTAREA_DEFAULT_ID;
	this.maxlength = EXTEXTAREA_DEFAULT_MAXLENGTH;
	this.msgstation = EXTEXTAREA_DEFAULT_MSGSTATION;
	this.cols = EXTEXTAREA_DEFAULT_COLS;
	this.rows = EXTEXTAREA_DEFAULT_ROWS;
	this.msg = EXTEXTAREA_DEFAULT_MSG;
	
	(function(){
		
		//\u8bfb\u53d6\u7528\u6237\u8bbe\u7f6e\u76845\u4e2a\u57fa\u672c\u5c5e\u6027
		if(valueIsNotNull($(exTaTags).attr("id"))) this.id = $(exTaTags).attr("id");
		if(valueIsNotNull($(exTaTags).attr("msgstation"))) this.msgstation = $(exTaTags).attr("msgstation");
		if(valueIsNotNull($(exTaTags).attr("cols"))) this.cols = $(exTaTags).attr("cols");
		if(valueIsNotNull($(exTaTags).attr("rows"))) this.rows = $(exTaTags).attr("rows");
		if(valueIsNotNull($(exTaTags).attr("msg"))) this.msg = $(exTaTags).attr("msg");
		
		//\u6839\u636e\u9700\u6c42\uff0cmaxlength\u53ef\u4ee5\u6765\u81ea\u6570\u503c\uff0c \u4e5f\u53ef\u4ee5\u6765\u81ea\u7528\u6237\u51fd\u6570
		var maxchar = $(exTaTags).attr("maxlength");
		try{
			if(valueIsNotNull(maxchar))
			{
				var methods = maxchar.match(/(.*)/gi);
				if(methods){
					this.maxlength = parseInt(eval(maxchar));
				}
				else if(isNaN(maxchar))
					this.maxlength = maxchar;
			}
		}
		catch(err){	}//\u8fd9\u91cc\u4e0d\u9700\u8981\u505a\u4efb\u4f55\u5904\u7406\uff0c\u5df2\u7ecf\u9ed8\u8ba4\u8d4b\u503c
		
		this.otherprops = ssbUserPropFromHTMLTag(exTaTags,"id, maxlength, msgstation, cols, rows, msg");
	}).apply(this);
	
	this.parent = exTaTags.parentNode;
}

SSB_EXTEXTAREA.prototype =
{
	init:function(){
			//textarea
			var ssb_TextArea = document.createElement("TEXTAREA");
			$(ssb_TextArea).attr("maxlength", this.maxlength).attr("cols", this.cols).attr("rows", this.rows).attr("id", "ssb_"+this.id).appendTo(this.parent);
			for(var i=0; i<this.otherprops.length; i++){
				ssb_TextArea.setAttribute(this.otherprops[i].name, this.otherprops[i].value);
			}
			$(ssb_TextArea).bind("keyup",this.charLeft).bind("keydown",this.charLeft).bind("paste",this.charLeft).bind("mouseover",this.charLeft);
			//tipdiv
			var tipHtml = ("<div id='tip"+ this.id.replace("ssb_","") +"' class='remain'>" + this.msg + "</div>").replace(/{#maxlength}/,"<span>"+ this.maxlength +"</span>");
			
			var csswidth = function() { return isIE()? 6:4};
			if(this.msgstation == "bottom"){
				$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
			else{
				if(this.msgstation == "both"){
					$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
				}
				$(ssb_TextArea).before($(tipHtml).width($(ssb_TextArea).width()+csswidth()).css("margin-top","2px"));
				$(ssb_TextArea).css("margin-top","-2px");
			}
			
			ssb_TextArea.taGetLength = this.taGetLength;
			ssb_TextArea.setCaret = this.setCaret;
			ssb_TextArea.insertAtCaret = this.insertAtCaret;
			ssb_TextArea.updateTipsInfo = this.updateTipsInfo;
			
			if(isIE()){
				ssb_TextArea.attachEvent("onclick",this.setCaret);//\u9488\u5bf9\u7c98\u8d34
			}
			
	},
	//\u83b7\u53d6\u8f93\u5165\u957f\u5ea6
	taGetLength:function(txt){
			var multiple = 2;
			var chineseLength = 0;
			if(typeof(customChineseLength) !='undefined' && customChineseLength.constructor == Function)
				multiple = customChineseLength();
			var cArr = txt.match(/[^\x00-\xff]/ig);
			if(cArr != null)
				chineseLength = multiple * cArr.length - cArr.length;
			
			return txt.length + chineseLength;
	},
	//\u8f93\u5165\u4e8b\u4ef6\u54cd\u5e94
	charLeft: function(event){
		
		var event = event || window.event;
		var target = event.target || event.srcElement;  //  \u83b7\u5f97\u4e8b\u4ef6\u6e90
		if(target.tagName != "TEXTAREA") return;
		var maxlength = $(target).attr("maxlength");
		var pasteText = "";
		if (event.type == "paste" && window.clipboardData)	pasteText = window.clipboardData.getData("text");
		
		var content = $(target).val();
		var text = content + pasteText;
		
		var remain= maxlength - target.taGetLength(text);
		
		if(remain < 0){
				//\u7c98\u8d34\u5904\u7406
				if (event.type == "paste") {
					if(isIE()){
						var curpos = target.curpos;
	
						var len = maxlength - target.taGetLength(content);
						pasteText = pasteText.substr(0,len);
						for(var i=len-1; i>=0; i--){
							if(target.taGetLength(pasteText) <= len) break;
							pasteText = pasteText.substr(0,i);
						}
						
						if(pasteText.length == 0) return false;
						target.insertAtCaret(target,pasteText);
						remain = maxlength - target.taGetLength(target.value);
						return false;
					}
					else{
						
					}
				}
				else{
					//\u8f93\u5165\u5904\u7406
					while(remain < 0){
						text = text.substr(0,text.length-1);
						remain= maxlength - target.taGetLength(text);
					}
					$(target).val(text);
				}
			target.updateTipsInfo(target.id, remain);
		}
		target.setCaret(target);
		target.updateTipsInfo(target.id, remain);
	},
	
	updateTipsInfo: function(id, remain){
		//\u91c7\u7528id\u7684\u65b9\u5f0f\uff0c\u662f\u4e3a\u4e86\u907f\u514d\u591a\u4e2a\u6587\u672c\u57df\u662f\u5144\u5f1f\u8282\u70b9\u65f6\uff0c\u51fa\u73b0\u66f4\u65b0\u4fe1\u606f\u6709\u8bef\u7684\u60c5\u51b5
		$(this).siblings("div[id='tip"+ id.replace("ssb_","") +"']").each(function(){
			$(this).children("span").text(remain);
		});
	},
	
	setCaret: function(textObj){
		if (textObj.createTextRange) {
            textObj.caretPos = document.selection.createRange().duplicate();
        }
	},
	
	insertAtCaret: function(textObj, textFeildValue){
        if (document.all) {
            if (textObj.createTextRange && textObj.caretPos) {
                var caretPos = textObj.caretPos;
                caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == '' ? textFeildValue + '' : textFeildValue;
            }
            else {
                textObj.value = textFeildValue;
            }
        }
        else {
            if (textObj.setSelectionRange) {
                var rangeStart = textObj.selectionStart;
                var rangeEnd = textObj.selectionEnd;
                var tempStr1 = textObj.value.substring(0, rangeStart);
                var tempStr2 = textObj.value.substring(rangeEnd);
                textObj.value = tempStr1 + textFeildValue + tempStr2;
            }
            else {
            }
        }
    }
}

SSB_EXTEXTAREA.setTextAreaValue = function(taid, value)
{
	$("#ssb_"+taid).attr("value",value);
}
SSB_EXTEXTAREA.getTextAreaValue = function(taid)
{
	return $("#ssb_"+taid).attr("value");
}
/*
 * @param(object) domobj		: \u4e00\u4e2aDOM\u5bf9\u8c61
 * @param(string) exception		: \u6392\u9664\u7684\u4e0d\u60f3\u8981\u7684\u5c5e\u6027
 * @return(array) rtnArray		: \u8fd4\u56de\u6570\u7ec4\uff0c\u5176\u5143\u7d20\u4e3a(name:propertyname, value:propertyvalue)\u5f62\u5f0f
 */
function ssbUserPropFromHTMLTag(domobj, exception)
{
	var rtnArray = new Array();
	var nameValue = function(name, value) {
		this.name = name;
		this.value = value;
	}
	
	if(!isIE()) {
	    if (window.HTMLElement) {
	        HTMLElement.prototype.__defineSetter__("outerHTML", function(sHTML) {
	            var r = this.ownerDocument.createRange();
	            r.setStartBefore(this);
	            var df = r.createContextualFragment(sHTML);
	            this.parentNode.replaceChild(df, this);
	            return sHTML;
	        });
	
	        HTMLElement.prototype.__defineGetter__("outerHTML", function() {
	            var attr;
	            var attrs = this.attributes;
	            var str = "<" + this.tagName.toLowerCase();
	            for (var i = 0; i < attrs.length; i++) {
	                attr = attrs[i];
	                if (attr.specified)
	                    str += " " + attr.name + '="' + attr.value + '"';
	            }
	            if (!this.canHaveChildren)
	                return str + ">";
	            return str + ">" + this.innerHTML + "</" + this.tagName.toLowerCase() + ">";
	        });
	
	        HTMLElement.prototype.__defineGetter__("canHaveChildren", function() {
	            switch (this.tagName.toLowerCase()) {
	                case "area":
	                case "base":
	                case "basefont":
	                case "col":
	                case "frame":
	                case "hr":
	                case "img":
	                case "br":
	                case "input":
	                case "isindex":
	                case "link":
	                case "meta":
	                case "param":
                    return false;
	            }
	            return true;
	        });
	    }
	}
	
	//\u5c5e\u6027\uff0c\u5305\u62ec\u4e8b\u4ef6\u7684\u8bfb\u53d6
	var attrs = domobj.outerHTML.match(/\s+\w+=/g); 
	var hasException = valueIsNotNull(exception);
	for(var i=0;i<attrs.length;i++)	{
		attrs[i] = $.trim(attrs[i]);
		attrs[i] = attrs[i].substring(0,attrs[i].length-1);
		if(hasException)
			if(exception.indexOf(attrs[i]) != -1) continue;
		
		rtnArray.push(new nameValue(attrs[i], domobj.getAttribute(attrs[i])));
	}
	//\u5224\u65ad\u662f\u5426\u6709disabled\u5c5e\u6027
	for(var i=0; i<domobj.attributes.length; i++){
		if(domobj.attributes[i].nodeName == "disabled" && domobj.attributes[i].nodeValue)
			rtnArray.push(new nameValue("disabled", "disabled"));
	}
	//\u8003\u8651\u5230\u6709\u901a\u7528\u7684\u53ef\u80fd\uff0c\u591a\u5224\u65ad\u4e00\u4e2aselected\u5c5e\u6027
	for(var i=0; i<domobj.attributes.length; i++){
		if(domobj.attributes[i].nodeName == "selected" && domobj.attributes[i].nodeValue)
			rtnArray.push(new nameValue("selected", "selected"));
	}
	
	return rtnArray;
}
//\u5224\u65ad\u5bf9\u8c61\u662f\u5426\u5b58\u5728\u5e76\u4e0d\u4e3a\u7a7a
function objIsExisted(obj)
{
	var rtn = true;
	if(typeof(obj) == "undefined" || obj == null || obj == "null") rtn = false;
	
	return rtn;
}

//\u5224\u65ad\u5c5e\u6027\u503c\u4e0d\u4e3a\u7a7a
function valueIsNotNull(value)
{
	var rtn = true;
	if(typeof(value) == "undefined" || value == null || value == "null" || $.trim(value).length == 0) rtn = false;
	
	return rtn;
}

function getSSBTextAreaById(id)
{
	var taid = "ssb_" + id;
	var taobj = document.getElementById(taid);
	
	//\u8bfb\u53d6\u6700\u5927\u957f\u5ea6
	taobj.getMaxLength = function(){
		return $(this).attr("maxlength");
	};
	//\u8bbe\u7f6e\u6700\u5927\u957f\u5ea6\u5e76\u66f4\u65b0\u63d0\u793a
	taobj.setMaxLength = function(maxlength){
		$(this).attr("maxlength", maxlength);
		var taid = this.id.replace("ssb_","");
		this.updateTipsInfo(taid, maxlength);
	};
	//\u8bfb\u53d6\u663e\u793a\u4f4d\u7f6e
	taobj.getMsgStation = function(){
		return $(this).attr("msgstation");
	};
	//\u66f4\u65b0\u663e\u793a\u4f4d\u7f6e
	taobj.setMsgStation = function(msgstation){
		var oldstation = $(this).attr("msgstation");
		if(oldstation == msgstation) return;
		
		var tipHtml = ("<div id='tip"+ this.id.replace("ssb_","") +"' class='remain'>" + this.msg + "</div>").replace(/{#maxlength}/,"<span>"+ this.maxlength +"</span>");
		var csswidth = function() { return isIE()? 6:4};
		
		if(msgstation == "bottom"){
			if(oldstation == "both"){
				var removeNode = $(this).siblings("div[id='tip"+ id +"']").get(0);
				removeNode.parentElement.removeChild(removeNode);
			}
			else{
				$(this).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
		}
		else{
			if(this.msgstation == "both"){
				$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
			$(ssb_TextArea).before($(tipHtml).width($(ssb_TextArea).width()+csswidth()).css("margin-top","2px"));
			$(ssb_TextArea).css("margin-top","-2px");
		}
	}
	
	return taobj;
}

//\u7528\u6237\u5bf9\u4e8e\u6c49\u5b57\u957f\u5ea6\u7684\u5904\u7406\u65b9\u5f0f
/*
function customChineseLength(){ return 3;}
*/
var _IP = "IP";
var _ZIP = "Z:IP";
SSB_IP.ipObjs={};

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
function Ips_init() {
	/* \u53d6\u5230\u6240\u6709\u7684ip\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_IP);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZIP);
	}
	if(tags && tags.length > 0) {
		var ip = {};
		/* \u904d\u5386\u4e0a\u8ff0\u6570\u7ec4\uff0c\u5bf9\u6bcf\u4e00\u4e2a\u6570\u7ec4\u5143\u7d20\u751f\u6210\u4e00\u4e2aip\u63a7\u4ef6\u5bf9\u8c61\uff0c\u8c03\u7528\u5176\u521d\u59cb\u5316\u65b9\u6cd5 */
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			var value = tags[i].getAttribute("value");
			
			ipObj = new SSB_IP(id);
			ipObj.init();
			ipObj.setValue(value);
		}
	}
}
addOnloadEvent(Ips_init);

/*
 * IP\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_IP(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
}

SSB_IP.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	  var str = "<table class=\"ip\" ><tbody><tr>";
	    str += "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_1\"" +
	           " onkeydown=\"OnIPFldKeyDown('" + this.id + "', 1, event)\" onchange=\"OnIPFldChange('" + this.id + "', 1, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_2\"" +
	           " onkeydown=\"OnIPFldKeyDown('" + this.id + "', 2, event)\" onchange=\"OnIPFldChange('" + this.id + "', 2, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_3\"" +
	           " onkeydown=\"OnIPFldKeyDown('" + this.id + "', 3, event)\" onchange=\"OnIPFldChange('" + this.id + "', 3, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_4\"" +
	           " onkeydown=\"OnIPFldKeyDown('" + this.id + "', 4, event)\" onchange=\"OnIPFldChange('" + this.id + "', 4, event)\" ></td>";
	    str += "</tr></tbody></table>";
	    this.elem.innerHTML = str;
	    //document.getElementById("ttt").value = str + "\n";
	    
	    SSB_IP.ipObjs[this.id] = this;
	},
	
	/* \u83b7\u53d6 IP \u503c\uff0cIP \u503c\u65e0\u6548\uff0c\u5219\u8fd4\u56de\u96f6\u957f\u5ea6\u5b57\u7b26\u4e32 */
	getValue:function() {
		var fld_1 = document.getElementById(this.id + "_ipFld_1").value;
	    var fld_2 = document.getElementById(this.id + "_ipFld_2").value;
	    var fld_3 = document.getElementById(this.id + "_ipFld_3").value;
	    var fld_4 = document.getElementById(this.id + "_ipFld_4").value;
	    
	    if (IsByte(fld_1) && IsByte(fld_2) & IsByte(fld_3) & IsByte(fld_4))
	    {
	        return fld_1 + "." + fld_2 + "." + fld_3 + "." + fld_4;
	    }
	    else
	    {
	        return "";
	    }
	},
	
	/* \u8bbe\u7f6e IP \u503c  \u6210\u529f\u8fd4\u56de true  \u5931\u8d25\u8fd4\u56de false\uff0c\u4e0d\u6539\u53d8\u539f\u8bbe\u7f6e\u503c */
	setValue:function(initValue) {
	
		if (initValue==null || typeof(initValue)=="undefined")
			return false;
	
		var ipFldArr = initValue.split(".");
	    if (ipFldArr.length != 4)
	    {
	        return false;
	    }
	    else if (!IsByte(ipFldArr[0]) || !IsByte(ipFldArr[1]) || !IsByte(ipFldArr[2]) || !IsByte(ipFldArr[3]))
	    {
	        return false;
	    }
	    
	    document.getElementById(this.id + "_ipFld_1").value = ipFldArr[0];
	    document.getElementById(this.id + "_ipFld_2").value = ipFldArr[1];
	    document.getElementById(this.id + "_ipFld_3").value = ipFldArr[2];
	    document.getElementById(this.id + "_ipFld_4").value = ipFldArr[3];
	   
	    return true;
	}

};


/* 
 * \u786e\u4fdd\u8f93\u5165\u7684\u6570\u503c\u8303\u56f4
 */
function OnIPFldChange(id, curFldIndex, oEvent)
{
    var ipFldElem = document.getElementById(id + "_ipFld_" + curFldIndex);
    var ipFldValue = ipFldElem.value;
    if(ipFldValue == "") {
    	//
    }
    else if (!IsByte(ipFldValue))
    {
      ipFldElem.value = 255;
    }
}

/*
 * \u53ea\u5141\u8bb8\u8f93\u5165\u6570\u5b57\uff0c\u5220\u9664\u952e\uff0c\u5de6\u53f3\u952e\uff0c\u70b9\u53f7\u952e
 */
function OnIPFldKeyDown(id, curFldIndex, oEvent)
{
	var ipFldElem = document.getElementById(id + "_ipFld_" + curFldIndex);
    var ipFldValue = ipFldElem.value;
	
	if(window.event) {
		
	   if (event.keyCode == 37)
	    {
	        //\u6309\u4e0b\u5411\u5de6\u952e
	        this.PrevIPFld(id, curFldIndex);
	        event.returnValue = false;
	    }
	    else if (event.keyCode==39 || (event.keyCode==190 && !event.shiftKey) || event.keyCode==9)
	    {
	        //\u6309\u4e0b\u5411\u53f3\u952e\u6216\u53e5\u53f7\u952e
	        this.NextIPFld(id, curFldIndex);
	        event.returnValue = false;
	    }
	    else if(event.keyCode == 8 || event.keyCode == 46) {
	    	// \u9000\u683c,\u5220\u9664
	    }
	    else if((event.keyCode >= 48 && event.keyCode <= 57 && !event.shiftKey)
	    	|| (event.keyCode >= 96 && event.keyCode <= 105 && !event.shiftKey)) 
	    {
	    	// \u6570\u5b57
	    	if(parseInt(ipFldValue) == 25 
	    		&& ((event.keyCode >= 54 && event.keyCode <= 57)
	    			|| (event.keyCode >= 102 && event.keyCode <= 105))) 
	    	{
	    		event.returnValue = false;
	    	}
	    	else if(parseInt(ipFldValue) > 25)
	    	{
	    		event.returnValue = false;
	    	}
			else if(parseInt(ipFldValue) == 0) {
				ipFldElem.value = "";
			}
	    }
	    else 
	    {
	    	//\u975e\u6570\u5b57\uff0c\u7981\u6b62\u8f93\u5165
	    	event.returnValue = false;	    	
	    }	    

	}
	else { 
		
		if (oEvent.keyCode == 37)
	    {	
	        //\u6309\u4e0b\u5411\u5de6\u952e
	        this.PrevIPFld(id, curFldIndex);
	        oEvent.preventDefault();
	    }
	    else if (oEvent.keyCode==39 || (oEvent.keyCode==190 && !oEvent.shiftKey) || oEvent.keyCode==9)
	    {
	        //\u6309\u4e0b\u5411\u53f3\u952e\u6216\u53e5\u53f7\u952e
	        this.NextIPFld(id, curFldIndex);
	        oEvent.preventDefault();
	    }
	    else if(oEvent.keyCode == 8 || oEvent.keyCode == 46) {
	    	// \u9000\u683c,\u5220\u9664
	    }
	    else if((oEvent.keyCode >= 48 && oEvent.keyCode <= 57 && !oEvent.shiftKey) 
	    	|| (oEvent.keyCode >= 96 && oEvent.keyCode <= 105 && !oEvent.shiftKey))
	    {
	    	
	    	// \u6570\u5b57
	    	if(parseInt(ipFldValue) == 25 
	    		&& ((oEvent.keyCode >= 54 && oEvent.keyCode <= 57)
	    			|| (oEvent.keyCode >= 102 && oEvent.keyCode <= 105))) 
	    	{
	    		oEvent.preventDefault();
	    	}
	    	else if(parseInt(ipFldValue) > 25)
	    	{
	    		oEvent.preventDefault();
	    	}
	    	else if(parseInt(ipFldValue) == 0) {
				ipFldElem.value = "";
			}
	    }
	    else 
	    {
	    	oEvent.preventDefault();
	    }
	    
	}
}



/*
 * \u4e0b\u4e00\u4e2a\u5b57\u6bb5\u683c
 */
function PrevIPFld(id, curFldIndex)
{
    if (curFldIndex > 1)
    {
        document.getElementById(id + "_ipFld_" + (--curFldIndex)).select();
    }
}

/*
 * \u4e0a\u4e00\u4e2a\u5b57\u6bb5\u683c
 */
function NextIPFld(id, curFldIndex)
{
    if (curFldIndex < 4)
    {
        document.getElementById(id + "_ipFld_" + (++curFldIndex)).select();
    }
}

/*
 * \u68c0\u67e5\u8303\u56f4\u662f\u5426\u57280-255\u4e4b\u95f4
 */
function IsByte(str)
{
    if (str.replace(/[0-9]/gi, "") == "")
    {
        if (parseInt(str)>=0 && parseInt(str)<=255)
        {
            return true;
        }
        return false;
    }
    return false;
}
function NAVI_init(){
	var atags=document.getElementsByTagName(_ZNAVI_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_NAVI_TAG);	
	}
	var navis={};
	for(var i=0;i<atags.length;i++){
		navis=new NaviTag(atags[i].getAttribute("id"));
		navis.init();
	}
};

addOnloadEvent(NAVI_init);


function NaviTag(id){
	var labelObj=document.getElementById(id);
	var menuId=labelObj.getAttribute("menuId");
	var text=labelObj.getAttribute("text");
	var url=labelObj.getAttribute("url");
	var _ria_location = labelObj.getAttribute("caption");
	if(_ria_location==null||_ria_location=='null'||_ria_location==undefined||_ria_location=='undefined'){_ria_location='The current location:';}
	var re = /caption_text/g; 
	SSB_Navi.STARTHTML=SSB_Navi.STARTHTML.replace(re,_ria_location);
	return new SSB_Navi(menuId,text,url,id);
};

function SSB_Navi(menuId,text,url,labelId){
	this.menuId=menuId;
	this.setText(text);
	this.url=url;
	this.setObject(labelId);
	this.menuPath=[];
};
SSB_Navi.prototype={
	setText:function(text){
		if(text instanceof Array){
			this.text=text;
		}else if(text instanceof String ||'string'==typeof text){
			this.text=text.split(',');
		}else 
			this.text=[];
	},
	setObject:function(id){
		this.labelObj=document.getElementById(id);
		this.labelId=id;
	},
	init:function(){
		SSB_Navi.naviObjs[this.labelId]=this;
		this.sendAjaxRequest();
	},
	sendAjaxRequest:function(){
		callMethod(this.url||SSB_Navi.URL,[this.menuId],this,this.setValue);
	},
	setValue:function(s,pathStr){
		this.handleResponse(pathStr);
		this.concatPath();
		this.getMenuPath();
	},
	handleResponse:function(pathStr){
		if(null!=pathStr&&""!=pathStr){
			this.menuPath=pathStr.split(',');
		}
	},
	concatPath:function(){
		this.menuPath=this.menuPath.concat(this.text);
	},
	getMenuPath:function(){
		var html=[];
		html.push(SSB_Navi.STARTHTML);
		var length=this.menuPath.length-1;
		if(length>=0){
			var i=0;
			for(i=0;i<length;i++){
				html.push(this.menuPath[i]);
				html.push(SSB_Navi.SPANHTML);
			}
			html.push(this.menuPath[i]);
		}
		html.push(SSB_Navi.ENDHTML);
		this.displayPath(html);
	},
	displayPath:function(html){
		this.labelObj.innerHTML=html.join("");
	}
};

SSB_Navi.naviObjs={};

SSB_Navi.URL="naviService/getPathString.ssm";

SSB_Navi.STARTHTML="<TABLE class='tableNavi'><TBODY><TR><TD noWrap class='tdNavi'><DIV class=div_subtitle>caption_text";
SSB_Navi.SPANHTML="<SPAN class=arrow_subtitle>&gt;</SPAN>";
SSB_Navi.ENDHTML="</DIV></TD><TD noWrap class='tdEndNavi'>&nbsp;</TD></TR></TBODY></TABLE>";

var _NAVI_TAG="NAVI";
var _ZNAVI_TAG="Z:NAVI";
function Radios_init(){
	var rtags=document.getElementsByTagName(_RADIOGROUP);
	if(!rtags||rtags.length==0){
		rtags=document.getElementsByTagName(_ZRADIOGROUP);	
	}
	var radios={};
	for(var i=0;i<rtags.length;i++){
		radios=new RadioGroupTag(rtags[i].getAttribute("id"));
		radios.init();
	}
};
addOnloadEvent(Radios_init);
				//
				//radiogroup\u7684\u6807\u7b7e\u7c7b
				//
function RadioGroupTag(id){
	var label=document.getElementById(id);
	var value=label.getAttribute("value");
	var text=label.getAttribute("text");
	var checkedvalue=label.getAttribute("checkedvalue");
	var onclick=label.getAttribute("onclickfun");
	return new SSB_RadioGroup(value,text,checkedvalue,id,onclick);
};

				//
				//\u5355\u9009\u6309\u94ae\u7ec4
				//
function SSB_RadioGroup(value,text,checkedValue,id,onclick){
	function setValueArr(value){
		if(!value)return [];
		if(value instanceof Array){
			this.valueArr=value;
		}else if(value instanceof String||"string"==typeof value){
			this.valueArr=value.split(",");
		}
	};
	function setTextArr(text){
		if(!text)return [];
		if(text instanceof Array){
			this.textArr=text;
		}else if(text instanceof String||"string"==typeof text){
			this.textArr=text.split(",");
		}
	};
	setValueArr.call(this,value);
	setTextArr.call(this,text);
	this.onclick=onclick;
	this.checkedvalue=checkedValue||"";
	this.groupObj=document.getElementById(id);
	this.group_id=id;
};
SSB_RadioGroup.prototype={
	init:function(){
		var html=[];
		var checkedvalue=this.checkedvalue;
		var value={};
		for(var i=0;i<this.valueArr.length||i<this.textArr.length;i++){
			var radio=[];
			value=this.valueArr[i]||"";
			radio.push("<INPUT");
			radio.push("name=radio_"+this.group_id);
			radio.push("type=radio");
			radio.push("value="+value);
			radio.push("onclick="+this.onclick);
			if(checkedvalue==value){
				radio.push("checked=true");
			}
			radio.push(">");
			radio.push(this.textArr[i]||"");
			html.push(radio.join(" "));
		}
		this.groupObj.innerHTML=html.join(" ");
		SSB_RadioGroup.radioGroupObjs[this.group_id]=this;
	},
	getValue:function(){
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].checked){
				return radios[i].getAttribute("value");
			}
		}
	},
	setValue:function(checkedValue){
		if(checkedValue)
			this.checkedvalue=checkedValue;
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].value==checkedValue+""){
				radios[i].checked=true;
				return;
			}
		}
	}
};

SSB_RadioGroup.radioGroupObjs={};

var _RADIOGROUP="RADIOGROUP";
var _ZRADIOGROUP="Z:RADIOGROUP";
function SSB_SingleUpload(ctrlId){
	this.ctrlId=ctrlId;
	this.labelObj=document.getElementById(ctrlId)||{};
	var that=this;
	this.labelObj.doUpload=function(paraMap,url,contrlObj){
		that.doUpload(paraMap,url,contrlObj);
	};
	this.text='Browser...';
	this.desc="";
	this.maxfiles=-1;
	this.count=0;
	this.onaddfile=null;
	
	//wanghao5
	this.groupId = "";
};

//wanghao5
var file_group = {};
file_group["noId"]=[];

SSB_SingleUpload.prototype={
	initCtrl:function( ){
		var html=[];
		var ctrlId=this.ctrlId;
		//wanghao4
		html.push('<span class="fileinputs"><div  class="fakefile"  id="div_'
				+ctrlId+'"><input id="fake_'+this.ctrlId+'"  class="singleText"  title="'+this.desc+
				'"/><input  type=button class="singleButton" value="'+
				this.text+'"/></div></span>');
		this.labelObj.innerHTML=html.join(' ');
	},
	addCtrl:function( ){
		var oInput=this.getFileCtrl();
		//wanghao
		__file_List = [];
		
		//wanghao5
		if(this.groupId == "")
			file_group["noId"].push(oInput);
		else
		{
			if(!file_group[this.groupId])
				file_group[this.groupId] = [];
			
			file_group[this.groupId].push(oInput);

		}	
		
		//\u4fee\u6539\u8bb0\u5f55 \u5f20\u5b8f\u4f1f
		__file_List.push(oInput);
		//wanghao4
		oInput.className='single_input';
		var tr = document.getElementById('div_'+this.ctrlId);
	    tr.parentNode.insertBefore(oInput,tr);
	},
	getFileCtrl:function( ){
		var oInput=document.createElement('input');
		var that=this;
		oInput.id = "upload_"+this.ctrlId;
		oInput.name = "upload_"+this.ctrlId;
		oInput.type = "file";
		//wanghao4
		//oInput.hideFocus = true;
		//oInput.size = 1;
		oInput['onchange']=function(event){
			that.addUpload(event||window.event);
		};
		return oInput;
	},
	init:function( ){
		this.initCtrl( );
		this.addCtrl( );
		SSB_Upload.uploads[this.ctrlId]=this;
	},
	addUpload:function(event){
		var ctrlId=this.ctrlId;
		function setValue(value){
			document.getElementById('fake_'+ctrlId).value=value;
		};
		var onaddfile=this.onaddfile;
		var ok=false;
		var obj=event.srcElement||event.target;
		if(onaddfile==''||!onaddfile){
			setValue(obj.value);
			return;
		}
		if(onaddfile.indexOf('this') != -1){
			var o = document.getElementById(ctrlId);
			ok = parseFunction(onaddfile,o);			
		}		
		else{
		ok=eval(onaddfile);
		}
		if(ok){
			setValue(obj.value);
			return;
		}else{
			obj.parentNode.removeChild(obj);
			this.addCtrl();
		}
	},
	delUpload:function(index){

	},
	setText:function(text){
		this.text=text;
	},
	setDesc:function(desc){
		this.desc=desc||this.desc;
	},
	setMaxFiles:function(){

	},
	setOnaddfile:function(onaddfile){
		this.onaddfile=onaddfile;
	},
	
	//wanghao5
	setGroupId:function(groupId){
		this.groupId=groupId || this.groupId;
	},
	clean:function(){
		this.init();
	}
};

var _imgMap={};
	_imgMap["css"] = _imgMap["txt"] = _imgMap["js"] = "text";
	_imgMap["html"] = _imgMap["htm"] = _imgMap["shtml"] = _imgMap["hta"] = "ie";
	_imgMap["xml"] = _imgMap["xsl"] = "xml";
	_imgMap["pdf"] = "pdf";
	_imgMap["mdb"] = "msaccess";
	_imgMap["doc"] = "msword";
	_imgMap["xls"] = "msexcel";
	_imgMap["ppt"] = "mspowerpoint";
	_imgMap["exe"] = _imgMap["dll"] = _imgMap["sys"] = _imgMap["bat"] = "system";
	_imgMap["zip"] = _imgMap["rar"] = _imgMap["gzip"] = _imgMap["jar"] = _imgMap["lzh"] = "archive";
	_imgMap["ini"] = _imgMap["inf"] = "conf";
	_imgMap["csv"] = "csv";
	_imgMap["psd"] = _imgMap["jpg"] = _imgMap["gif"] = "image";
	_imgMap["ra"] = _imgMap["rm"] = _imgMap["rmvb"] = _imgMap["mp3"] = _imgMap["wma"] = _imgMap["avi"] = "media";
	
function SSB_MultiUpload(ctrlId){
	SSB_SingleUpload.call(this,ctrlId);
	this.url='';
	this.uploading=false;
	this.count=0;  //\u4e0a\u4f20\u6587\u4ef6\u5217\u8868\u957f\u5ea6
	this.currentInput={};
};
SSB_MultiUpload.prototype=new SSB_SingleUpload();
SSB_MultiUpload.prototype.constructor=SSB_MultiUpload;
SSB_MultiUpload.prototype.iconMap=_imgMap;

SSB_MultiUpload.prototype.initCtrl=function(){
	var ctrlId=this.ctrlId;
	var a= '<a hidefocus="" id="btn_'+ctrlId+'"><input class="button" type="button" value="'+this.text+'"/></a>&nbsp;'+
				'<div class="font_notice addfile_notice" style="display:inline" id="notice_'+ctrlId+'">'+this.desc+'</div>' +
		   '<table id="tab_'+ctrlId+'" class="filelist" >' +
				'<thead><tr class="filelist_title"><th class="path">'+'File Path'+'</th><th class="operate">'+'Operate'+'</th></tr>' +
				'</thead>' +
				'<tbody id="upload_list_'+ctrlId+'">' +
					'<tr id="no_upload_'+ctrlId+'" class="nofile_notice">' +
						'<td colSpan=2>'+'No add File...'+'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>';
	this.labelObj.innerHTML=a;
};

//wanghao4
SSB_MultiUpload.prototype.getFileCtrl=function(){
		var oInput=document.createElement('input');
		var that=this;
		oInput.id = "upload_"+this.ctrlId;
		oInput.name = "upload_"+this.ctrlId;
		oInput.type = "file";
		oInput.hideFocus = true;
		oInput.size = 1;
		oInput['onchange']=function(event){
			that.addUpload(event||window.event);
		};
		return oInput;
}

SSB_MultiUpload.prototype.addCtrl=function(){
	var oInput=this.getFileCtrl();
	oInput.className='multi_input';
	this.currentInput=oInput;
	document.getElementById('btn_'+this.ctrlId).appendChild(oInput);
};
SSB_MultiUpload.prototype.addUpload=function(event){
	var oInput=event.srcElement||event.target;
	var filePath=oInput.value;
	var that=this;
	if(this.url.indexOf(filePath)>0){
		alert('The current directory had the same file, does not need to be redundant uploads');
		this.delRepeatInput(oInput);
		this.addCtrl();
		return;
	}
	if(this.maxfiles!=-1&&this.count>=this.maxfiles){
		alert('Most uploads'+this.maxfiles+'files.');
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	var ok=true;
	var onaddfile=this.onaddfile;
	if(onaddfile){
		try{
			ok=eval(onaddfile);
		}catch(e){
			this.delUpload(oInput);
			this.addCtrl();
			throw e;
		}
	}
	if(!ok){
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	this.url += "|"+filePath;
	var sIcon='unknown';
	var ext=filePath.substring(1+filePath.lastIndexOf("."),filePath.length);
	sIcon=this.iconMap[ext]?this.iconMap[ext]:sIcon;
	var oFileList = document.getElementById('upload_list_'+this.ctrlId);
	var oTr = document.createElement('tr');
	oFileList.appendChild(oTr);
	var oTd = document.createElement('td');
	oTr.appendChild(oTd);
	var oIcon = document.createElement('div');
	oIcon.className='file_icon icon_'+sIcon;
	var oStr=document.createTextNode(filePath);
	oTd.appendChild(oIcon);
	oTd.appendChild(oStr);
	oTd=document.createElement('td');
	oTr.appendChild(oTd);
	var oA=document.createElement('a');
	oA.className="option_del";
	oA.onclick=function(event){
		event=event||window.event;
		var a=event.srcElement||event.target;
		that.delUpload(a['oInput']);
	};
	oA.href="javascript:void(0);";
	oTd.appendChild(oA);
	oIcon=document.createElement('div');
	oIcon.className='file_icon icon_del';

	oStr=document.createTextNode('Delete');
	oA.appendChild(oIcon);
	oA.appendChild(oStr);

	document.getElementById('no_upload_'+this.ctrlId).style.display='none';
	//\u4e92\u76f8\u4fdd\u5b58\u5f15\u7528
	oIcon['oInput']=oInput;
	oA['oInput']=oInput;
	oInput['oTr']=oTr;
	oInput.style.display='none';
	this.count++;			//\u6587\u4ef6\u589e\u52a01\u4e2a
	this.addCtrl();
};
SSB_MultiUpload.prototype.delUpload=function(oInput){
	if(this.uploading){
		alert('The file is uploading, please wait!');
		return;
	}
	var oTr=oInput['oTr'];
	this.url = this.url.replace(oInput.value,"");
	this.url = this.url.replace("||","");
	oInput.parentNode.removeChild(oInput);
	if(oTr){
		oTr.parentNode.removeChild(oTr);
		this.count--;		//\u6587\u4ef6\u51cf\u5c11\u4e00\u4e2a
	}
	if(this.count==0){
		document.getElementById("no_upload_"+this.ctrlId).style.display='';
	}
};
//\u5220\u9664\u91cd\u590d\u7684 input \u6807\u7b7e\u3002
SSB_MultiUpload.prototype.delRepeatInput=function(oInput){
	oInput.parentNode.removeChild(oInput);
};
SSB_MultiUpload.prototype.setMaxFiles=function(maxfiles){
	this.maxfiles=isNaN(maxfiles)||maxfiles<1?-1:maxfiles;
};
SSB_MultiUpload.prototype.clean=function(){
	this.init();
	this.count=0;
	this.url='';
	this.currentInput={};
};

function UploadTag(id){
	var tagObj=document.getElementById(id);
	var type=tagObj.getAttribute('type');
	var upload={};
	switch(type){
		case 'single':upload= new SSB_SingleUpload(id);break;
		case 'multi':upload= new SSB_MultiUpload(id);break;
		default:upload= new SSB_SingleUpload(id);
	}
	upload.setText(tagObj.getAttribute("text"));
	upload.setDesc(tagObj.getAttribute("desc"));
	upload.setMaxFiles(parseInt(tagObj.getAttribute("maxfiles")));
	upload.setOnaddfile(tagObj.getAttribute("onaddfile"));
	
	//wanghao5
	upload.setGroupId(tagObj.getAttribute("groupId"));
	return upload;
};

var _ZUPLOAD_TAG="z:upload";
var _UPLOAD_TAG="upload";
var _ZTABLE_CLUMN ="z:column";
var _TABLE_CLUMN ="column";

//wanghao8
var input_file = [];

function Upload_init(){
	//wanghao8
	var inputs = document.getElementsByTagName("input");
	for(var ii = 0;ii<inputs.length;ii++)
	{
		if(inputs[ii].type.toUpperCase() == "FILE")
		{
			input_file.push(inputs[ii]);
			__file_List.push(inputs[ii]);
			if(typeof(inputs[ii].groupId)=="undefined" || inputs[ii].groupId==null || trim(inputs[ii].groupId)=="")
			{
				file_group["noId"].push(inputs[ii]);
			}
			else
			{
				if(!file_group[inputs[ii].groupId])
					file_group[inputs[ii].groupId] = [];
				
				file_group[inputs[ii].groupId].push(inputs[ii]);
	
			}
		}
		
	}
	
	var atags=document.getElementsByTagName(_ZUPLOAD_TAG);
	if(!atags||atags.length==0)
	{
		atags=document.getElementsByTagName(_UPLOAD_TAG);	
	}
	var upload={};
	for(var i=0;i<atags.length;i++)
	{
		var tagType = atags[i].parentNode;
		var ptn = tagType.tagName.toUpperCase();
		//\u5982\u679c\u63a7\u4ef6\u4e0d\u5728\u8868\u683c\u63a7\u4ef6\u4e2d\u65f6
		if(ptn != _ZTABLE_CLUMN && ptn != _TABLE_CLUMN)
		{
			upload=new UploadTag(atags[i].getAttribute("id"));
		    upload.init();
		}		
	}
};
SSB_Upload={};
SSB_Upload.uploads={};
addOnloadEvent(Upload_init);
var _PIEBASIC_TAG="piechart";
var _ZPIEBASIC_TAG="z:piechart";
SSB_PIE.pieObjs={};


/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
 var pieObj = {};
function Pies_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_PIEBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZPIEBASIC_TAG);
	}
	if(tags && tags.length > 0) {	
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			pieObj = new SSB_PIE(id);
			pieObj.init();					
		}
	}
}
addOnloadEvent(Pies_init);

/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_PIE(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mattribute= document.getElementById(this.id).getAttribute("attribute");
}

SSB_PIE.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
          if(this.mattribute==null||this.mattribute=="")
        {
			this.mattribute="divshow:true; divper:false;divvalue:false;divtitle:true;pieshow:true; pieper:true;pievalue:false;pietitle:false";
        }
        
		 if(this.mdivid != null&&this.mdivid != "")
		 {
		  if(this.id==this.mdivid)
		 {
			this.mdivid="pie"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		}
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    pieObj.setValue(this);
	    }
	    
	    SSB_PIE.pieObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	
	
	setValue:function(init) {
	
	var options, placeholder,option,datas;
   
    findData(eval(init.mdatasrc));
   
    placeholder = $("#"+init.mdivid);
    
    var fina="{ pies:{show: true},legend:{"+ legendgroup(init.mlegend)+"},attribute:{"+attributegroup(init.mattribute)+"}}";
    
    option=eval("["+fina+"]");

	options = option[0];

	datas=eval("["+totaldata+"]");
	
    $.plot(placeholder,datas,options); 

	
  }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

attributegroup = function (attribute){

		var group=attribute.split(";");
		var attributestring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					attributestring +=group[i];
				}
				else
				{
					attributestring +=group[i]+',';
				}
			}
		}

		return attributestring;
};


var totaldata="";

findData =function(initvalue)
{
		totaldata="";

		
		for(var i=0;i<initvalue.length;i++)
		{
			var info="";
			
			if(i == initvalue.length-1)
			
			info="{label:'"+initvalue[i].label+"',value:["+i+","+initvalue[i].value+"],color:'"+initvalue[i].color+"'}";
			
			else
			
			info="{label:'"+initvalue[i].label+"',value:["+i+","+initvalue[i].value+"],color:'"+initvalue[i].color+"'},";
			
			totaldata+=info;
		}
}
	
	//\u8c03\u670d\u52a1
setpiesValue = function(ids,initvalue) {
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="pie"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
	
	var mattribute= document.getElementById(ids).getAttribute("attribute");

	var options, placeholder,option,datas;
   
    findData(eval(initvalue));
   
    placeholder = $("#"+mdivid);
    
     var fina="{ pies:{show: true},legend:{"+ legendgroup(mlegend)+"},attribute:{"+attributegroup(mattribute)+"}}";
    option=eval("["+fina+"]");

	options = option[0];

	datas=eval("["+totaldata+"]");
	
    $.plot(placeholder,datas,options);
   
	};
	
	

var _LINEBASIC_TAG="lineschart";
var _ZLINEBASIC_TAG="z:lineschart";
SSB_LINE.lineObjs={};

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
 var lineObj = {};
function Lines_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_LINEBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZLINEBASIC_TAG);
	}
	if(tags && tags.length > 0) {	
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			lineObj = new SSB_LINE(id);
			lineObj.init();					
		}
	}
}
addOnloadEvent(Lines_init);


var placeholder,datas,options;
/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_LINE(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mxaxis= document.getElementById(this.id).getAttribute("xaxis");
    this.myaxis= document.getElementById(this.id).getAttribute("yaxis");
    this.mgrid= document.getElementById(this.id).getAttribute("grid");
}

SSB_LINE.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
		 if(this.mxaxis==null||this.mxaxis=="")
        {
			this.mxaxis="tickDecimals: 0";
        }
   
		 if(this.myaxis==null||this.myaxis=="")
        {
			this.myaxis="min: 0";
        }
        
		 if(this.mgrid==null||this.mgrid=="")
        {
			this.mgrid="color:'#000000'";
        }
        
		 if(this.mdivid != null&&this.mdivid != "")
		 {
		  if(this.id==this.mdivid)
		 {
			this.mdivid="line"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    lineObj.setValue(this);
	    }
	    
	    SSB_LINE.lineObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	setValue:function(init) {	
	var options, placeholder,datas,option;
	
	 	//\u6570\u636e\u6e90
		datas =eval(init.mdatasrc);
		//\u663e\u793a\u6837\u5f0f
		var fina="{lines:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroup(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
 }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

xaxisgroup = function (xaxis){

		var group=xaxis.split(";");
		var xaxisstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					xaxisstring +=group[i];
				}
				else
				{
					xaxisstring +=group[i]+',';
				}
			}
		}

		return xaxisstring;
};

gridgroup = function (grid){

		var group=grid.split(";");
		var gridstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					gridstring +=group[i];
				}
				else
				{
					gridstring +=group[i]+',';
				}
			}
		}

		return gridstring;
};

yaxisgroup = function (yaxis){

		var group=yaxis.split(";");
		var yaxisstring="";
		for(var i=0;i<group.length;i++)
		{
			if(i==group.length-1)
			{
				yaxisstring +=group[i];
			}
			else
			{
				yaxisstring +=group[i]+',';
			}
		}
		return yaxisstring;
};

	
	//\u8c03\u670d\u52a1
setlinesValue = function(ids,initvalue) {
	var options, placeholder,datas,option;
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="line"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
    var mxaxis= document.getElementById(ids).getAttribute("xaxis");
    var myaxis= document.getElementById(ids).getAttribute("yaxis");
    var mgrid= document.getElementById(ids).getAttribute("grid");

	 	//\u6570\u636e\u6e90
		datas =eval(initvalue);
		//\u663e\u793a\u6837\u5f0f
		var fina="{lines:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroup(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
   
	};
	
	

var _BARBASIC_TAG="barchart";
var _ZBARBASIC_TAG="z:barchart";
SSB_BAR.barObjs={};


//\u51b3\u5b9a\u521d\u59cb\u5316\u88c5\u8f7dria \uff0cinit \u521d\u59cb\u5316 \uff0c\u7528\u6237\u521d\u59cb\u5316\uff0c

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */

function Bars_init() {
	/* \u53d6\u5230\u6240\u6709\u7684barchart\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_BARBASIC_TAG);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZBARBASIC_TAG);
	}
	if(tags && tags.length > 0) {
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			barObj = new SSB_BAR(id);
			barObj.init();		
			
		}
	}
}
addOnloadEvent(Bars_init);

/*
 * BAR\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_BAR(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.mdivid = document.getElementById(this.id).getAttribute("divid");
    this.mheight= document.getElementById(this.id).getAttribute("height");
    this.mwidth= document.getElementById(this.id).getAttribute("width");
    this.mdatasrc= document.getElementById(this.id).getAttribute("datassrc");
    this.mlegend= document.getElementById(this.id).getAttribute("legend");
    this.mxaxis= document.getElementById(this.id).getAttribute("xaxis");
    this.myaxis= document.getElementById(this.id).getAttribute("yaxis");
    this.mgrid= document.getElementById(this.id).getAttribute("grid");
    this.misgroup= document.getElementById(this.id).getAttribute("isgroup");

}

SSB_BAR.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	
	 if(this.mheight==null||this.mheight=="")
        {
			this.mheight="300px";
        }
        
	 	 if(this.mwidth==null||this.mwidth=="")
        {
			this.mwidth="600px";
        }

		 if(this.misgroup==null||this.misgroup=="")
        {
			this.misgroup="false";
        }
        
        if(this.mlegend==null||this.mlegend=="")
        {
			this.mlegend="show: true";
        }
        
		 if(this.mxaxis==null||this.mxaxis=="")
        {
			this.mxaxis="tickDecimals: 0";
        }
   
		 if(this.myaxis==null||this.myaxis=="")
        {
			this.myaxis="min: 0";
        }
        
		 if(this.mgrid==null||this.mgrid=="")
        {
			this.mgrid="color:'#000000'";
        }

		 if(this.mdivid != null&&this.mdivid != "")
		 {
		 if(this.id==this.mdivid)
		 {
			this.mdivid="bar"+this.mdivid;
		 }
		 	var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div><div id="'+this.mdivid+'2'+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
		 else
		 {
			this.mdivid="div"+this.id;
			var html='<div id="'+this.mdivid+'" style="width:'+this.mwidth+';height:'+this.mheight+';"></div>';
		 }
	    this.elem.innerHTML = html;
	    
	    if(this.mdatasrc!=null&&this.mdatasrc!="")
	    {
	    barObj.setValue(this);
	    }
	    
	   // SSB_BAR.barObjs[this.id] = this;
	},
	
	getValue:function() {
	        return ""; 
	},
	
	setValue:function(init) {
	
	var options, placeholder,datas,option;
	
	if(init.misgroup=="true"){
		ticks="";
		isGroup(eval(init.mdatasrc));
	 	//\u6570\u636e\u6e90
		datas =eval("["+totals+"]");
		
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroupbar(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
	}
	else
	{
		ticks="";
	 	//\u6570\u636e\u6e90
		datas =eval(init.mdatasrc);
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(init.mlegend)+"},xaxis:{"+xaxisgroupbar(init.mxaxis)+"},yaxis:{"+yaxisgroup(init.myaxis)+"},grid:{"+gridgroup(init.mgrid)+"}}";
    
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+init.mdivid);
		
		$.plot(placeholder,datas,options);
   }
 }	
};

legendgroup = function (legend){

		var group=legend.split(";");
		var legendstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					legendstring +=group[i];
				}
				else
				{
					legendstring +=group[i]+',';
				}
			}
		}

		return legendstring;
};

xaxisgroupbar = function (xaxis){
		
		var group=xaxis.split(";");
		var xaxisstring="";
		if(group.length!=0)
		{
			if(xaxis.search("ticks")!=-1)
			{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
				
					if(ticks!=null&&ticks!="")
					{
						if(group[i].search("ticks") != -1)
						{
							group[i]="ticks:["+ticks+"]";
						}
					}
					xaxisstring +=group[i];
				}
				else
				{
					if(ticks!=null&&ticks!="")
					{
						if(group[i].search("ticks") != -1)
						{
							group[i]="ticks:["+ticks+"]";
						}
					}
					xaxisstring +=group[i]+',';
				}
			}
			}
			else
			{
				for(var i=0;i<group.length;i++)
				{	
					xaxisstring +=group[i]+',';	
				}
				xaxisstring += "ticks:["+ticks+"]";
			}
		}

		return xaxisstring;
};

gridgroup = function (grid){

		var group=grid.split(";");
		var gridstring="";
		if(group.length!=0)
		{
			for(var i=0;i<group.length;i++)
			{
				if(i==group.length-1)
				{
					gridstring +=group[i];
				}
				else
				{
					gridstring +=group[i]+',';
				}
			}
		}

		return gridstring;
};

yaxisgroup = function (yaxis){

		var group=yaxis.split(";");
		var yaxisstring="";
		for(var i=0;i<group.length;i++)
		{
			if(i==group.length-1)
			{
				yaxisstring +=group[i];
			}
			else
			{
				yaxisstring +=group[i]+',';
			}
		}
		return yaxisstring;
};

	//\u6574\u5408\u7684\u6570\u636e
	var totals="";
	//X\u8f74\u7684\u663e\u793a\u6587\u5b57
	var ticks="";
	
	//\u5206\u7ec4\u65b9\u6cd5
isGroup = function(inputs){
		totals="";
		ticks="";
		var count=1;
		
		var retrn=inputs[0].datas;
		
		var xtick=inputs[0].xyticks;
		
		var leng=retrn.length;
		
		for(var i=0;i<leng;i++)
		{
			var info="";
			
			info="{label:'"+retrn[i].label+"',data:[";
			
			var resul="";
			
			for(var j=0;j<leng;j++)
			{		
			var ints=count+(leng+1)*j;
			
			//\u5904\u7406X\u8f74\u7684\u663e\u793a
			if(i==j)
			{
			
			var tickint=(leng+1)*j+(leng/2+1);
			
				if(i == leng-1)
				ticks +="["+tickint+",'"+xtick[i]+"']";
				else
				ticks +="["+tickint+",'"+xtick[i]+"'],";
			}
			
			//\u5904\u7406\u6570\u636e\u5bfc\u51fa\u7ed3\u679c
			if(j == leng-1)
			resul += "["+ints+","+retrn[i].data[j]+"]";
			else
			resul += "["+ints+","+retrn[i].data[j]+"],";
			}
			
			if(i==leng-1)
			info=info+resul+"]}";
			else
			info=info+resul+"]},";

			totals+=info;
			count++;
		}
	};
	
	//\u8c03\u670d\u52a1
setbarsValue = function(ids,initvalue) {
	var options, placeholder,datas,option;
	var mdivid = document.getElementById(ids).getAttribute("divid");
	if(mdivid==null||mdivid=="")
	mdivid="div"+ids;
	
	if(ids==mdivid)
	mdivid="bar"+ids;
	var mlegend= document.getElementById(ids).getAttribute("legend");
    var mxaxis= document.getElementById(ids).getAttribute("xaxis");
    var myaxis= document.getElementById(ids).getAttribute("yaxis");
    var mgrid= document.getElementById(ids).getAttribute("grid");
    var misgroup= document.getElementById(ids).getAttribute("isgroup");
   if(misgroup=="true"){
		ticks="";
		isGroup(eval(initvalue));
	 	//\u6570\u636e\u6e90
		datas =eval("["+totals+"]");
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroupbar(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
    
	    option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
	}
	else
	{
		 ticks="";
	 	//\u6570\u636e\u6e90
		datas =eval(initvalue);
		//\u663e\u793a\u6837\u5f0f
		var fina="{bars:{show: true},legend:{"+ legendgroup(mlegend)+"},xaxis:{"+xaxisgroupbar(mxaxis)+"},yaxis:{"+yaxisgroup(myaxis)+"},grid:{"+gridgroup(mgrid)+"}}";
		option=eval("["+fina+"]");

		options = option[0];
		//\u7ed8\u56fe
		placeholder = $("#"+mdivid);
		
		$.plot(placeholder,datas,options);
	 }
   
	};


var SSB_RIA_LOV = "ssb_ria_lov";
var Z_SSB_RIA_LOV = "Z:SSB_RIA_LOV";
var lov_rtn_obj = {};
var clearEvent_str="";
var fRiaLovObj = {};
var Lov_EnterEventObj = {};

var charString = "|";
var comma = ",";

//\u4fee\u6539\u8bb0\u5f552 : \u738b\u6d69
//\u65b0\u589e\u7684\u7528\u4e8eLOV\u56de\u663e\u529f\u80fd
function setHiddenValues(targetCompId,lovId)
{
	//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
	if(targetCompId == "" || typeof(targetCompId) =="undefined" || lovId == "" || typeof(lovId) =="undefined" )
		return;
	//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69	
	
	var targetCompIds = targetCompId.split(",");
	var targetCompIdValues = new Array();
	for(var i = 0;i<targetCompIds.length;i++)
	{
		var value = document.getElementById(targetCompIds[i]).value;
		
		//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
		var helpArray = new Array();
		value = trim(value);
		
		if(value.indexOf(",") == -1)
		{
			helpArray.push(value);
		}
		else
		{			
			while(value.indexOf(",") != -1)
			{
				if(value.indexOf(",") == 0)
				{
					helpArray.push("");
					value = trim(value.substr(1));
				}
				else
				{
					var helpStr = value.substring(0,value.indexOf(","));
					helpArray.push(helpStr);
					value = trim(value.substr(value.indexOf(",")+1));
				}
			}
			
			if(value.indexOf(",") == -1)
			{
				helpArray.push(value);
			}
		}
		
		targetCompIdValues[i] = helpArray;
		//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
	}
		
    var hiddenValue = "";
	
	if(targetCompIdValues.length>0)
    {
				
		for(var l = 0;l<targetCompIdValues[0].length;l++)
		{
			var j = 0;
			while(j<targetCompIdValues.length)
			{
				var targetCompIdValueList = targetCompIdValues[j];
		    	hiddenValue += targetCompIdValueList[l];
				
				if(j != targetCompIdValues.length - 1)
					hiddenValue += "|" ;
				j++;		    	
			}	
			if(l != targetCompIdValues[0].length - 1)
		    		hiddenValue += ",";	
		}
		
	}
    
    document.getElementById(lovId).value = hiddenValue;
}

//\u521d\u59cb\u5316
var SSB_riaLov_init = function()
{
	var nodes= document.getElementsByTagName(SSB_RIA_LOV);
	
	if(navigator.appName == "Microsoft Internet Explorer")
	{
		nodes= document.getElementsByTagName(SSB_RIA_LOV);
	}else
	{
		nodes= document.getElementsByTagName(Z_SSB_RIA_LOV);
	}
	
	var nodeArr = new Array();
	for(var n = 0;n<nodes.length;n++)
	{
		nodeArr[n] = nodes[n];
	}
	var nodeLength = nodeArr.length;

	for(var i=0;i<nodeLength;i++)
	{
		var tagname = nodeArr[i].parentNode.tagName.toUpperCase();
		if(tagname != 'Z:COLUMN' && tagname != 'COLUMN')
		{
		  readRiaLovTag(nodeArr[i]);
		}
	}

};

var readRiaLovTag = function (node)
{
	var riaLovObj = {};
	riaLovObj.id = node.getAttribute("id");
	riaLovObj.lovKey = node.getAttribute("lovKey");
	riaLovObj.imageUrl = node.getAttribute("imageUrl");
	riaLovObj.targetId = node.getAttribute("targetId");
	riaLovObj.title = node.getAttribute("title");
	riaLovObj.defaultParm = node.getAttribute("defaultParm");
	riaLovObj.targetCompId = node.getAttribute("targetCompId");
	riaLovObj.jsEventId = node.getAttribute("jsEventId");
	riaLovObj.innerSqlParamValue = node.getAttribute("innerSqlParamValue");
	riaLovObj.clearEvent_id = node.getAttribute("clearEvent_id");
	riaLovObj.width = node.getAttribute("width");
	riaLovObj.height = node.getAttribute("height");
	riaLovObj.emptyresultmsg = node.getAttribute("emptyresultmsg");
	//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	//\u589e\u52a0pageURL\u5c5e\u6027\u8ba9\u7528\u6237\u81ea\u5df1\u5b9a\u5236\u5f39\u51fa\u7684LOV\u9875\u9762
	riaLovObj.pageURL = node.getAttribute("pageURL");

	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	riaLovObj.isRequired = node.getAttribute("isRequired");
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	var riaLovObj_clone = {};
	riaLovObj_clone.id = node.getAttribute("id");
	riaLovObj_clone.lovKey = node.getAttribute("lovKey");
	riaLovObj_clone.imageUrl = node.getAttribute("imageUrl");
	riaLovObj_clone.targetId = node.getAttribute("targetId");
	riaLovObj_clone.title = node.getAttribute("title");
	riaLovObj_clone.defaultParm = node.getAttribute("defaultParm");
	riaLovObj_clone.targetCompId = node.getAttribute("targetCompId");
	riaLovObj_clone.jsEventId = node.getAttribute("jsEventId");
	riaLovObj_clone.innerSqlParamValue = node.getAttribute("innerSqlParamValue");
	riaLovObj_clone.clearEvent_id = node.getAttribute("clearEvent_id");
	riaLovObj_clone.width = node.getAttribute("width");
	riaLovObj_clone.height = node.getAttribute("height");
	riaLovObj_clone.emptyresultmsg = node.getAttribute("emptyresultmsg");
	//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	//\u589e\u52a0pageURL\u5c5e\u6027\u8ba9\u7528\u6237\u81ea\u5df1\u5b9a\u5236\u5f39\u51fa\u7684LOV\u9875\u9762
	riaLovObj_clone.pageURL = node.getAttribute("pageURL");

	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	riaLovObj_clone.isRequired = node.getAttribute("isRequired");
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	var obj = new SSB_riaLov();
	obj.writeRiaLovTag(riaLovObj,riaLovObj_clone);
};

var SSB_riaLov = function(){};

SSB_riaLov.prototype = {
	
	writeRiaLovTag : function (riaLovObj,riaLovObj_clone)
	{	
		var theLovElement = document.getElementById(riaLovObj.id);
		var parentElement = theLovElement.parentNode;
	
		var image=document.createElement("img");
		image.setAttribute("src",riaLovObj.imageUrl);
		image.style.cursor = "pointer";
		image.setAttribute("border","0");
		image.setAttribute("id","lov_img_"+riaLovObj.id);

		image.onclick = function (){
			openRiaLovWindow (riaLovObj,riaLovObj_clone,image);
		};

		var hiddenId = document.createElement("input");
		hiddenId.setAttribute("id",riaLovObj.id);
		hiddenId.setAttribute("type","hidden");
		
		parentElement.appendChild(image);
		parentElement.appendChild(hiddenId);
		
		parentElement.replaceChild(image,theLovElement);
	}
	
};
var	openRiaLovWindow = function (riaLovObj,riaLovObj_clone,image)
{	
	//\u4fee\u6539\u8bb0\u5f552 : \u738b\u6d69
	setHiddenValues(riaLovObj.targetCompId,riaLovObj.id);
	
	lov_rtn_obj = {};
	var valueName=document.getElementById(riaLovObj.id).value;
	
	var valueNames = valueName.split(",");
	for(var m=0;m<valueNames.length;m++)
	{
	  lov_rtn_obj[valueNames[m]]=true;
	}
	var wWidth = 640;
    var wHeight = 510;
	if(riaLovObj.width)
	{
		wWidth = riaLovObj.width;
	}
	
	if(riaLovObj.height)
	{
		wHeight = riaLovObj.height;
	}
	
	var wTop = (window.screen.height - wHeight)/2;
    var wLeft = (window.screen.width - wWidth)/2;

	fRiaLovObj = {};
	fRiaLovObj = riaLovObj_clone;
	fRiaLovObj.defaultParm = parseThings(riaLovObj.defaultParm);
	fRiaLovObj.innerSqlParamValue = parseThings(riaLovObj.innerSqlParamValue,image);
	fRiaLovObj.valueName = valueName;
	fRiaLovObj.width = wWidth;
	fRiaLovObj.height = wHeight;
	
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	Lov_EnterEventObj.isRequired = riaLovObj.isRequired;
	delete fRiaLovObj.isRequired;
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
	delete fRiaLovObj.pageURL;
	
	if(lovEnterEventObj && lovEnterEventObj.textValue && document.getElementById(lovEnterEventObj.textValue))
	{
		fRiaLovObj.textValue = document.getElementById(lovEnterEventObj.textValue).value.trim();
	}else
	{
		fRiaLovObj.textValue = null;
	}
	
	
	if(lovEnterEventObj && lovEnterEventObj.enterDown)
	{
		lov_getEnterEvent(fRiaLovObj);
	}else{
		lovEnterEventObj.textValue = null;
		var rialovurl = riaLovObj.imageUrl.substring(0,riaLovObj.imageUrl.indexOf("image"));
		
		//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
		if(!riaLovObj.pageURL || riaLovObj.pageURL.trim() == "")
		{
			//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
			//delete fRiaLovObj.pageURL;
			//\u4fee\u6539\u8bb0\u5f555 : \u738b\u6d69 ---resizable=yes
			window.open(rialovurl+"common/lov/rialovpage.html","LOV","top="+wTop+",left="+wLeft+",height="+wHeight+",width="+wWidth+",status=no,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes");
		}
		else if(riaLovObj.pageURL)
		{
			//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
			//delete fRiaLovObj.pageURL;
			//\u4fee\u6539\u8bb0\u5f555 : \u738b\u6d69 ---resizable=yes
			window.open(rialovurl+riaLovObj.pageURL.trim(),"LOV","top="+wTop+",left="+wLeft+",height="+wHeight+",width="+wWidth+",status=no,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes");		
		}
	}

};

var lov_getEnterEvent = function (fRiaLovObj)
{	
	lovEnterEventObj.enterDown = false;
	callUrl("LovEnterEventAction/getRiaLovEnterEvent.ssm",fRiaLovObj,"Lov_EnterEventObj","true");
	lov_parseResults(Lov_EnterEventObj);
};

var parseThings = function (defaultParm,image)
{
	var def_parms = defaultParm;
    var def_parm = unescape(defaultParm).substring(0,1);
    if(def_parm == "~"){	
        var str_val = unescape(defaultParm).substring(1);
        var ssbfunction =  str_val;     
         if(str_val.indexOf('this') != -1)
          {            	
           	eval("var image = image");           
           	var ssbfunction = str_val.replace("this","image");
          }
        def_parms = eval(ssbfunction);   
		defaultParm = escape(def_parms);
    }   
	return unescape(defaultParm);
}

var setTreeInfo = function (treeObj,str)
{
	var compMap = riaLovStartObj.rtnValue.compMap;
	var tagArr = riaLovObj.targetId.split(comma);
	
	for(var i = 0;i<tagArr.length;i++)
	{
		opener.document.getElementById(compMap[tagArr[i]]).value="";
		if(str != "clear")
		{
			opener.document.getElementById(compMap[tagArr[i]]).value=treeObj[tagArr[i]];
		}
	}
	
	if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
	{
		opener.document.getElementById(riaLovObj.id).value = treeObj.ria_tree_menuId;
	}
	
	if(riaLovObj.jsEventId && str == "confirm"  && opener.document.getElementById(riaLovObj.jsEventId))
	{
		opener.document.getElementById(riaLovObj.jsEventId).onchange();
	}
	
	if(str == "clear")
	{
		if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
		{
			opener.document.getElementById(riaLovObj.id).value = "";
		}
		if(riaLovObj.clearEvent_id  && opener.document.getElementById(riaLovObj.clearEvent_id))
		{
			opener.document.getElementById(riaLovObj.clearEvent_id).onchange();
		}
	}
	
	
			
};

var getTreeName = function (treeObj)
{
	
	if(riaLovStartObj && riaLovObj)
	{
		setTreeInfo(treeObj,"confirm");
	}	
    self.close();   
};

var getTreeNames = function (treeValues)
{
	if(riaLovStartObj && riaLovObj && treeValues)
	{
		var treeObj = {};
		var tagArr = riaLovObj.targetId.split(comma);
		var tree_arr = new Array(tagArr.length);
		
		for(var i=0;i<tagArr.length;i++)
		{
			tree_arr[i] = "";
		}
		for(var i=0;i<tree_arr.length;i++)
		{
			treeObj.ria_tree_menuId = "";
			var tree_char = "";
			for(var j=0;j<treeValues.length;j++)
			{
				tree_arr[i] = tree_arr[i] + tree_char + treeValues[j][tagArr[i]];
				treeObj.ria_tree_menuId = treeObj.ria_tree_menuId + tree_char + treeValues[j].ria_tree_menuId;
				tree_char = comma;
			}
			treeObj[tagArr[i]] = tree_arr[i];			
		}
				
		setTreeInfo(treeObj,"confirm");	
		
	}
		if(treeValues.length<=0)
		{
			var confirmValue = confirm('You don\'t select anything. Are you sure to close the window?');
			if(confirmValue == true)
				self.close();
		}
		else
		{
			self.close();
		}

};

function closeWindow(){
	self.close();
};

var clearTree = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		var treeObj = {};
		treeObj.menuName = "";
		treeObj.menuId = "";
		treeObj.menuFaId = "";
		
		setTreeInfo(treeObj,"clear");
	}	
	self.close();
};

var setTableInfo = function (tableStr,str,checkedStr)
{
	var compMap = riaLovStartObj.rtnValue.compMap;
	var tagArr = riaLovObj.targetId.split(comma);
	var tableArr = tableStr.split(charString);
	
	for(var i = 0;i<tagArr.length;i++)
	{
		opener.document.getElementById(compMap[tagArr[i]]).value="";
		if(str != "clear")
		{
			opener.document.getElementById(compMap[tagArr[i]]).value=tableArr[i];
		}
	}
	
	if(checkedStr && riaLovObj.id && opener.document.getElementById(riaLovObj.id))
	{
		opener.document.getElementById(riaLovObj.id).value = checkedStr;
	}		
	
	if(riaLovObj.jsEventId && riaLovObj.jsEventId!="" && str == "confirm"  && opener.document.getElementById(riaLovObj.jsEventId))
	{
		opener.document.getElementById(riaLovObj.jsEventId).onchange();
	}
	
	if(str == "clear")
	{
		if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
		{
			opener.document.getElementById(riaLovObj.id).value = "";
		}	
		if(riaLovObj.clearEvent_id && opener.document.getElementById(riaLovObj.clearEvent_id))
		{	
			opener.document.getElementById(riaLovObj.clearEvent_id).onchange();
		}
	}
		
	self.close(); 
};

var getTableName = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		var rows = getCheckedRowsValue('ssbTable');
		if(rows[0])
		{
			//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
			setTableInfo(rows[0].ria_lov_table_allValue,"confirm",rows[0].ria_lov_table_allValue);		
		}
		else
		{
			//wanghao
			var confirmValue = confirm('You don\'t select anything. Are you sure to close the window?');
			if(confirmValue == true)
				self.close(); 
		}		
	}
};

var getTableNames = function ()
{
	
	if(riaLovStartObj && riaLovObj)
	{
		var rows = getCheckedRowsValue('ssbTable');
		if(!rows.length>0)
		{
			//wanghao			
			var confirmValue = confirm('You don\'t select anything. Are you sure to close the window?');
			if(confirmValue == true)
				self.close();
			return;
		}
		var tagArr = riaLovObj.targetId.split(comma);
		var valueArr = new Array(tagArr.length);
		for(var j=0;j<tagArr.length;j++)
		{
			valueArr[j] = "";
		}
		var names = "";
		var chars = "";
		var checkedStr = "";
		for(var i=0;i<rows.length;i++)
		{
			checkedStr = checkedStr + chars + rows[i].ria_lov_table_allValue;
			for(var j=0;j<tagArr.length;j++)
			{
				valueArr[j] = valueArr[j] + chars + rows[i].ria_lov_table_allValue.split(charString)[j];
			}
			chars = comma;
		}
		chars="";
		for(var j =0;j<tagArr.length;j++){		
			names=names+ chars + valueArr[j] ;
			chars=charString;
		}
		setTableInfo(names,"confirm",checkedStr);	
	}
	self.close(); 
};

var clearTable = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		setTableInfo("","clear");
	}	
	self.close();
};

//\u9875\u9762js

function initRiaLovPage(){

	callUrl('riaLovAction/getRiaLovTags.ssm',riaLovObj,'riaLovStartObj','true');
	
	document.title = riaLovObj.title;	
	var riaLovs = riaLovStartObj.rtnValue;
	
	if(riaLovs && (riaLovs.queryType == "singletree" || riaLovs.queryType == "multitree"))
	{	
		document.getElementById("ria_tree_id").style.visibility = "visible";
		document.getElementById("treeConfirmButtonId").value = riaLovs.buttonMap.confirm;
		document.getElementById("treeClearButtonId").value = riaLovs.buttonMap.clear;
		document.getElementById("treeCancelButtonId").value = riaLovs.buttonMap.cancel;
		callSid('rialovtest',riaLovObj,'menuTree');
	}
	else if(riaLovs)
	{
		document.getElementById("floaters").style.top = 0;
		document.getElementById("ria_table_id").style.visibility = "visible";
		document.getElementById("ria_lov_lable").innerHTML = riaLovs.buttonMap.write;
		createQueryTable(riaLovs);
		
		document.getElementById("tableConfirmButtonId").value = riaLovs.buttonMap.confirm;
		document.getElementById("tableClearButtonId").value = riaLovs.buttonMap.clear;
		document.getElementById("tableCancelButtonId").value = riaLovs.buttonMap.cancel;
		document.getElementById("tableQueryButtonId").value = riaLovs.buttonMap.query;
	
		createTableColumn(riaLovs);
		riaLovObj.queryConditions = "";
		riaLovObj.isQuery = "false";
		searchTableData('getTableInfo','riaLovObj','ssbTable');
	}
};
addOnloadEvent(initRiaLovPage); 
	
function addListener()
{

	var queryType = riaLovStartObj.rtnValue.queryType;
	if(queryType == "singletree")
	{
		document.getElementById("floaters").style.top = parseInt(riaLovObj.height)-30;
		
		document.getElementById("treeConfirmButtonId").style.visibility = "hidden";	
		document.getElementById("treeClearButtonId").style.visibility = "visible";	
		document.getElementById("treeCancelButtonId").style.visibility = "visible";	
			
		SSB_Tree.trees['menuTree'].addNodeListener('onclick',getMenuTree);
		
	}else if(queryType == "multitree")
	{
		document.getElementById("floaters").style.top = parseInt(riaLovObj.height)-30;
	
		document.getElementById("treeConfirmButtonId").style.visibility = "visible";	
		document.getElementById("treeClearButtonId").style.visibility = "visible";	
		document.getElementById("treeCancelButtonId").style.visibility = "visible";	
		
	}
	
};

function confirmTree()
{
	getTreeNames(SSB_Tree.trees['menuTree'].getObjValue());
};

function getMenuTree(){
	//\u4e3a\u4e86\u8ba9\u70b9\u7236\u8282\u70b9\u4e5f\u80fd\u9009\u70b9\u503c
	/*var childNodeOnly = riaLovStartObj.rtnValue.childNodeOnly;
	if(childNodeOnly && childNodeOnly=="true")
	{
		if(this.parentNode.getAttribute('istreeleaf') == 'false' || !this.parentNode.getAttribute('istreeleaf'))
		{
			return;
		}
	}	*/
	getTreeName(this['model']);
};

 SSB_MultiTree.prototype.setNode=function(node){
	var span=node.getElementsByTagName('SPAN')[0];
	span.className='';
 };
 
 function createTableColumn(riaLovs)
 {
 	var cName = riaLovs.tableMaps.tableName;
 	var kName = riaLovs.tableMaps.disStr;
 	var queryType = riaLovs.queryType;
	var column_str = "<z:table id='ssbTable' cellspacing='1' border='0' turnpage='true'  pagesizelist='10,20,30,50' >";
	if(queryType=="singletxt")
	{
		//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	    column_str = column_str + "<z:column id=col0 width='10%' iskey='true' caption='select' prop='ria_lov_table_allValue'><input type='radio'  name='check'  ctrlex='ria_lov_table_checked' /></z:column>";			
	}
	else
	{
	    column_str =column_str + "<z:column id=col0 width='10%' iskey='true' checkall='true' caption='Select All' prop='ria_lov_table_allValue' ><input type='checkbox'  name='check' ctrlex='ria_lov_table_checked' /></z:column>";					
	}

 	for(var j = 0;j<kName.length;j++)
 	{
  		column_str = column_str + '<z:column width="'+90/kName.length+'%" caption="'+cName[kName[j]]+'"  prop="'+kName[j]+'"></z:column>';		 		
 	}
 
	var columns = document.getElementById('ssb_ria_lov_tableID');	
	var multiColumns = '<div>';
	if(queryType=="multitxt")
	{
		columns.innerHTML = multiColumns + column_str +'</z:table></div>';
	}else{
		columns.innerHTML = column_str+'</z:table>';
	}	
	SSB_Table_Init();
	if(queryType=="multitxt")
	{
		var pages = document.getElementById('ssbTable_pages');
	    pages.style.display='none';
	}
 };
 
 function createQueryTable(riaLovs)
 {
 	var queryStr ='<table cellspacing=1 class="tb_lov_searchbar" width="100%">'+ riaLovs.queryStr;
    queryStr = queryStr + '<td colspan=4 align="right"><input id="tableQueryButtonId" type="button" onClick="queryTables()"  class="button" ></td></tr>';
 	var columns = document.getElementById('queryID');	
 	columns.innerHTML = queryStr +'</table>';
 };
 
 function confirmTable()
 {
 	var queryType = riaLovStartObj.rtnValue.queryType;
	if(queryType=="singletxt")
	{
		getTableName();
	}
	else if(queryType=="multitxt")
	{
		getTableNames();
	}
 };
 
 function queryTables()
 {
 	var queryStr = "";
 	var queryStrs = riaLovStartObj.rtnValue.queryCondition;
 	var queryArr = queryStrs.split(comma);
 	var chars = "";
 	for(var i=0;i<queryArr.length;i++)
 	{
 		queryStr = queryStr  + chars + queryArr[i] +":"+ document.getElementById("ria_lov_"+queryArr[i]).value;
 		chars = comma;
 	}
 	riaLovObj.queryConditions = queryStr.replace(/\'/g,"&apos;");;
	riaLovObj.isQuery = "true";  	
 	searchTableData('getTableInfo','riaLovObj','ssbTable');
 };
 
 
//lov\u56de\u8f66\u4e8b\u4ef6 
var lovEnterEventObj = {};
var lov_enterEventOrOnblur = true;

var lovEnterEvent = function (id,tid)
{
	if(event.keyCode==13)
	{
		lov_enterEventOrOnblur = false;
		this.focus();
		lovEnterEventObj.enterDown = true;
		if(document.getElementById(tid))
		{
			lovEnterEventObj.textValue = tid;
		}
		if(document.getElementById("lov_img_"+id))
		{
			document.getElementById("lov_img_"+id).onclick();
		}		
		lov_enterEventOrOnblur = true;
	}
	
};
// \u5931\u53bb\u7126\u70b9\u4e8b\u4ef6
var lovOnblurEnterEvent = function (id,tid)
{
	if(lov_enterEventOrOnblur)
	{		
		if(!document.getElementById(tid) || document.getElementById(tid).value == "")
		{
			return;
		}
		lovEnterEventObj.enterDown = true;
		if(document.getElementById(tid))
		{
			lovEnterEventObj.textValue = tid;
		}
		if(document.getElementById("lov_img_"+id))
		{
			document.getElementById("lov_img_"+id).onclick();
		}
	}
};

var lov_parseResults = function (Lov_EnterEventObj){
	
	if(!Lov_EnterEventObj || !Lov_EnterEventObj.rtnValue)
	{
		return ;
	}
	var responseValue = Lov_EnterEventObj.rtnValue;
	var count = responseValue.number;
	switch (count)
	{
		case 0 :
		
			//\u4fee\u6539\u8bb0\u5f557 : \u738b\u6d69 -------start----------
			alert('Unable to find a match with the input of the recording, after amending enquiries');
			//\u4fee\u6539\u8bb0\u5f557 : \u738b\u6d69 -------end----------
			
			//\u4fee\u6539\u8bb0\u5f559 : \u738b\u6d69 -------start----------
			if(!Lov_EnterEventObj.isRequired || Lov_EnterEventObj.isRequired.trim() == "" ||Lov_EnterEventObj.isRequired.trim() =="true")
			{
				//\u4fee\u6539\u8bb0\u5f558 : \u738b\u6d69 -------start----------
				document.getElementById(lovEnterEventObj.textValue).focus();
				//\u4fee\u6539\u8bb0\u5f558 : \u738b\u6d69 -------start----------
			}
			//\u4fee\u6539\u8bb0\u5f559 : \u738b\u6d69 -------end----------
			
			lovEnterEventObj.textValue = null;
			break;
		case 1 :
			lov_rtnEnterValues(responseValue);
			break;
		case 2 :
			lov_confirmEvent(responseValue);
			break;
		case 3 :
			lov_openLov(responseValue);
			break;
		default:
			break;
	}

};

var lov_rtnEnterValues = function(responseValue)
{
	
	var mapValue = responseValue.mapValue;
	var listValue = responseValue.listValue;
	var jsEventId = responseValue.jsEventId;
	if(listValue && mapValue)
	{
		for(var i=0;i<listValue.length;i++)
		{
			if(document.getElementById(listValue[i]))
			{
				document.getElementById(listValue[i]).value = mapValue[listValue[i]];
			}
		}
	}
	if(jsEventId && jsEventId != "" && document.getElementById(jsEventId))
	{
		document.getElementById(jsEventId).onchange();
	}
	lovEnterEventObj.textValue = null;
};

var lov_openLov = function (responseValue)
{
	var id = responseValue.id;
	lovEnterEventObj.enterDown = false;
	if(document.getElementById("lov_img_"+id))
	{
		document.getElementById("lov_img_"+id).onclick();
	}
};

var lov_confirmEvent = function (responseValue)
{
	//if(confirm("\u4f60\u6240\u67e5\u8be2\u7684\u6570\u636e\u591a\u4e8e"+responseValue.countMax+"\u6761\uff0c\u662f\u5426\u7ee7\u7eed\u67e5\u8be2"))
	if(confirm('The records to match too much , user LOV pattern or not?'))
	{
		lov_openLov(responseValue);
	}else
	{
		lovEnterEventObj.textValue = null;
	}
};
var _SLIDER = "slider";
var _ZSLIDER = "z:slider";
var SliderArray = [];
var SliderTagArrary = [];

//Slider\u521d\u59cb\u5316\u65b9\u6cd5
function SliderInit(){
	var slidertags=document.getElementsByTagName(_SLIDER);
	if(!slidertags||slidertags.length==0){
		slidertags=document.getElementsByTagName(_ZSLIDER);	
	}
	
	if(!slidertags||slidertags.length==0)
		return;
	var slider={};
	var sliderArray = [];
	for(var i=0;i<slidertags.length;i++){
		slider=new SliderTag(slidertags[i].getAttribute("id"));
		sliderArray[slidertags[i].getAttribute("id")] = slider ;
		slider.init();
	}
	SliderArray = sliderArray;
};

//slider\u6807\u7b7e\u5bf9\u8c61
function SliderTag(id){
	var tagObj=document.getElementById(id);
	var slider={};
	slider = new SliderCtrObj(id);
	if(tagObj.getAttribute("sliderType"))	
		slider.setSliderType(tagObj.getAttribute("sliderType"));
	if(tagObj.getAttribute("min"))
		slider.setMin(parseFloat(tagObj.getAttribute("min")));
	if(tagObj.getAttribute("max"))	
		slider.setMax(parseFloat(tagObj.getAttribute("max")));		
	if(tagObj.getAttribute("steps"))	
		slider.steps = parseInt(tagObj.getAttribute("steps"));
	if(tagObj.getAttribute("decimal"))	
		slider.decimal = parseInt(tagObj.getAttribute("decimal"));
	if(tagObj.getAttribute("length"))	
		slider.setLength(parseFloat(tagObj.getAttribute("length")));
	if(tagObj.getAttribute("bindCtrId"))	
		slider.setBindCtrId(tagObj.getAttribute("bindCtrId"));
	if(tagObj.getAttribute("onchange"))	
		slider.onchange = tagObj.getAttribute("onchange");
	if(tagObj.getAttribute("name"))	
		slider.name = tagObj.getAttribute("name");
	if(tagObj.getAttribute("showlabel"))	
		slider.showlabel = tagObj.getAttribute("showlabel");
	if(tagObj.getAttribute("value"))	
		slider.value = tagObj.getAttribute("value");
	if(tagObj.getAttribute("disabled"))	
		slider.disabled = tagObj.getAttribute("disabled");
	return slider;
};

//slider\u5f53\u524d\u6ed1\u4e2d\u7684\u503c\u5bf9\u8c61
function SliderDisplayObj(id){
	this.id = id;
	this.dec = "";
	this.value = [];
	
};
//Slider\u5bf9\u8c61
function SliderCtrObj(id){
	this.id = id;
	this.sliderType = "single";
	this.min = "";
	this.max = "";
	this.length = 130;
	this.decimal = 0;
	this.steps = 0;
	this.bindCtrId = "";
	this.onchange = null;
	this.showlabel =null;
	this.name=id;
	this.value="";
	this.disabled = "false";
	
	this.sliderObj=null;
	this.displayTitleObj=null;
	this.displayObj = new SliderDisplayObj(id);
	this.mouseover = true;
	this.mousemove = false;
	//add
	this.pxLeft = "";
	this.pxTop = "";
	this.xCoord = "";
	this.yCoord = "";
	this.v = "";
	this.yMax = "";
	this.xMax = "";
	this.xMin = "";
	this.yMin = "";
	this.position = 0;
	
	var sliderPairs={
		slider1:0,
		slider2:1439,
		old1:0,
		old2:1439,
		sliderprice:20,
		sliderpriceold:40,
		changed:function(){
			return !((sliderPairs.slider1==sliderPairs.old1&&sliderPairs.slider2==sliderPairs.old2)||(sliderPairs.slider2==sliderPairs.old1&&sliderPairs.slider1==sliderPairs.old2));
		},
		pricechanged:function(){
			return sliderPairs.sliderpriceOld!=sliderPairs.sliderpriceold;
		},
		Sync:function(){

		},
		PriceSync:function(){
			sliderPairs.sliderpriceold=sliderPairs.sliderprice;
		}
	};
	
	var me=this;
	
	//document\u7684onmousemove\u4e8b\u4ef6
	this.moveSlider = function moveSlider(evnt)
	{//debugger;
		var evnt = (!evnt) ? window.event : evnt; 
		if (me.mouseover) { 
			var x = me.pxLeft + evnt.screenX - me.xCoord; 
			var y = me.pxTop + evnt.screenY - me.yCoord ;
			if (x > me.xMax) x = me.xMax ;
			if (x < me.xMin) x = me.xMin ;
			if (y > me.yMax) y = me.yMax ;
			if (y < me.yMin) y = me.yMin ;
			SliderCtrObj.carpeLeft(me.sliderObj.id, x) ; 
			SliderCtrObj.carpeTop(me.sliderObj.id, y) ;
			var sliderVal = x + y ;
			var sliderPos = (me.sliderObj.pxLen / me.sliderObj.valCount) * Math.round(me.sliderObj.valCount * sliderVal / me.sliderObj.pxLen);
			me.v = Math.round((sliderPos * me.sliderObj.scale + me.sliderObj.fromVal) * 
				Math.pow(10, me.displayObj.dec)) / Math.pow(10, me.displayObj.dec);
				
			me.displayObj.value[me.position] = me.v ;
			
			//add
			//me.displayValue();
			
			var SliderStyle = SliderCtrObj.GetObjsPos(me.sliderObj.LeftObj,me.sliderObj.RightObj);	
			var filterObj = SliderCtrObj.carpeGetElementById(me.sliderObj.filterId);
			
			
			filterObj.style.left = SliderStyle.left;
			filterObj.style.width = SliderStyle.width;
			
			me.mousemove=true;
			
			//\u589e\u52a0\u4e8b\u4ef6
			eval(me.onchange);
			return true;
		}
		me.mousemove = false;
		
		//\u589e\u52a0\u4e8b\u4ef6
		eval(me.onchange);
		return ;
	};
	
	//slider\u7684onmousedown\u4e8b\u4ef6
	this.slide = function slide(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position)
	{
		if (!evnt) evnt = window.event;
		
		//\u8bbe\u7f6e\u6240\u6709\u7684\u5c5e\u6027
		me.setAllAttributes(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position);
				
		//\u8bbe\u7f6e\u6240\u9700\u7684\u9f20\u6807\u4e8b\u4ef6
		if(me.disabled == "false")
			me.setMouseEvent();
		
	};
	
	//document\u7684onmouseup\u4e8b\u4ef6
	this.sliderMouseUp = function sliderMouseUp()
	{//debugger;
		me.mouseover = false; 
		if (!me.mousemove) return;
	
		me.v = (me.displayObj.value[me.position]) ? me.displayObj.value[me.position] : 0; // Find last display value.
		if ( me.sliderObj.scale==0 ) return;
		pos = (me.v - me.sliderObj.fromVal)/(me.sliderObj.scale); 
		if (me.yMax == 0) SliderCtrObj.carpeLeft(me.sliderObj.id, pos);
		if (me.xMax == 0) SliderCtrObj.carpeTop(me.sliderObj.id, pos); 
		
		me.removeMouseEvent();
	
		// firsttrip secondtrip
		if (me.sliderObj.LeftObj==null || me.sliderObj.id==me.sliderObj.LeftObj.id)
			sliderPairs.slider1 = me.v;
		else
			sliderPairs.slider2= me.v;
	
		if ( sliderPairs.changed() )
		{
			//alert(sliderPairs.slider1 + ";" + sliderPairs.slider2);
		//	RepeatedFilter(slidertype, sliderPairs.slider1, sliderPairs.slider2);
		//	sliderPairs.Sync();
		}
	};
	
	
	//\u5728\u7ed9\u5b9a\u7684\u63a7\u4ef6\u4e2d\u663e\u793a\u503c
	this.displayValue = function displayValue(){
		if (me.displayTitleObj!=null){
				
				if ( me.sliderType == "time")
				{
					var title=me.displayTitleObj.innerHTML.split(' - ');
					var changedIndex = me.position-1;
					var time = parseInt(me.displayObj.value[me.position],10);	
					title[changedIndex]=SliderCtrObj.leftpadstring(parseInt(time/60,10),"0",2) + ":" + SliderCtrObj.leftpadstring(parseInt(time%60,10),"0",2);
					me.displayTitleObj.innerHTML=title.join(' - ');
				}
				else
				{
					var title=me.displayTitleObj.innerHTML.split('- ');
					title[title.length-1]="" + me.displayObj.value[me.position];
					me.displayTitleObj.innerHTML=title.join('- ');
				}
			}
	};

	//\u8bbe\u7f6e\u6240\u6709\u5c5e\u6027
	this.setAllAttributes = function setAllAttributes(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position)
	{
		me.position = position;
		me.sliderObj = (evnt.target) ? evnt.target : evnt.srcElement; 
		me.sliderObj.pxLen = length ;
		me.sliderObj.valCount = count ? count : (to - from); 
		me.sliderObj.scale = (to - from) / length; 
		me.sliderObj.filterId = filter;
		me.sliderObj.LeftObj = SliderCtrObj.carpeGetElementById(LeftObjId); 
		me.sliderObj.RightObj = SliderCtrObj.carpeGetElementById(RightObjId); 
		
		me.displayObj.dec = decimal;		
		if(TitleId != "")
			me.displayTitleObj = SliderCtrObj.carpeGetElementById(TitleId);
		if ( true || orientation == 'horizontal') { 
			me.sliderObj.fromVal = from;
			me.xMax = length;
			me.yMax = 0;
			me.xMin = me.yMin = 0;
			
			var SliderStyle = SliderCtrObj.GetObjsPos(me.sliderObj.LeftObj,me.sliderObj.RightObj);
			
			if ( me.sliderObj.LeftObj!=null && me.sliderObj.LeftObj.id==me.sliderObj.id )
				me.xMax = SliderStyle.left + SliderStyle.width - parseInt(me.sliderObj.style.width);
			else
				me.xMin = SliderStyle.left;		
		}
		
		me.pxLeft = SliderCtrObj.carpeLeft(me.sliderObj.id); 
		me.pxTop  = SliderCtrObj.carpeTop(me.sliderObj.id); 
		me.xCoord = evnt.screenX;
		me.yCoord = evnt.screenY; 
		me.mouseover = true;
		me.mousemove = false;
	};
		
	//\u8bbe\u7f6e\u6240\u9700\u7684\u9f20\u6807\u4e8b\u4ef6
	this.setMouseEvent = function setMouseEvent()
	{		
		document.onmousemove = me.moveSlider; 
		document.onmouseup = me.sliderMouseUp; 
	};
	
	//\u79fb\u9664\u9f20\u6807\u4e8b\u4ef6
	this.removeMouseEvent = function removeMouseEvent()
	{
		if (document.removeEventListener) { 		
			document.removeEventListener('mousemove', me.moveSlider, true);		
			document.removeEventListener('mouseup', me.sliderMouseUp, true);
		}
		else if (document.detachEvent) { 	
			document.detachEvent('onmousemove', me.moveSlider);
			document.detachEvent('onmouseup', me.sliderMouseUp);
		}	
	};
	
		
	//\u5f97\u5230slider\u7684\u5f53\u524d\u503c
	this.getDataFromCtrl = function getDataFromCtrl(pos){
		if(pos == "left")
			return me.displayObj.value[1];
		else if(pos == "right")
			return me.displayObj.value[2];
		else
		 	return me.displayObj.value[0];
	};
	
	//\u7ed9slider\u8bbe\u503c
	this.bindDataToCtrl = function bindDataToCtrl(value,pos){
		if(pos == "left")
			me.displayObj.value[1] = value;
		else if(pos == "right")
			me.displayObj.value[2] = value;
		else
		 	me.displayObj.value[0] = value;
	
	};
	
	//\u5f97\u5230\u6574\u4e2aSlider\u5bf9\u8c61
	this.getSliderObj = function getSliderObj(){
		return me.sliderObj;
	};
};

SliderCtrObj.prototype={
	createDisplayElem:function(){
		var label = document.createElement("span");
		label.innerHTML = "";
		document.getElementById(this.id).appendChild(label);
		this.displayTitleObj = label;
	},
	setSingleSliderValue:function(value){
			if(parseFloat(value)<this.min )
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u8bbe\u7f6e\u7684value\u5c0f\u4e8e\u6700\u5c0f\u503c\uff0c\u8bf7\u4fee\u6539!");
				return;
			}
			else if( parseFloat(value)>this.max)
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u8bbe\u7f6e\u7684value\u5927\u4e8e\u6700\u5927\u503c\uff0c\u8bf7\u4fee\u6539!");
				return;
			}
				
				var val = parseFloat(value);
				this.displayObj.value[0] = val;
				
				var horizontal_slider_id = document.getElementById("horizontal_slider_single_"+this.id);
				var colorBar_id = document.getElementById("colorBar_"+this.id);
				var horizontal_slider_id_left = horizontal_slider_id.style.left;
				var colorBar_id_width = colorBar_id.style.width;
				
				horizontal_slider_id.style.left = ((val-this.min)/(this.max-this.min))*parseFloat(horizontal_slider_id_left)+"px";
							
				colorBar_id.style.width = (((val-this.min)/(this.max-this.min))*parseFloat(horizontal_slider_id_left+5))+"px";
	},
	setDoubleSliderValue:function(value){
		var val = [];
		
		if( value instanceof Array)
		{
			val[0] = parseFloat(value[0]);
			val[1] = parseFloat(value[1]);
			if(parseInt(val[0])>parseInt(val[1]))
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684value\u503c\u6709\u8bef\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
				return;
			}			
		}
		else
		{			
			if(typeof value =="string" && value.indexOf(",") != -1)
			{			
				var v = value.split(",");
				val[0] = parseFloat(v[0]);
				val[1] = parseFloat(v[1]);			
				if(v.length>2 || parseFloat(v[0])>parseFloat(v[1]))
				{
					alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684value\u503c\u6709\u8bef\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
					return;
				}
			}
			else
				return;			
		}
				this.displayObj.value[1] = val[0];
				this.displayObj.value[2] = val[1];
				var horizontal_slider_id_1 = document.getElementById("horizontal_slider_double_"+this.id+"_1");
				var horizontal_slider_id_2 = document.getElementById("horizontal_slider_double_"+this.id+"_2");
				var colorBar_id = document.getElementById("colorBar_"+this.id);
				var horizontal_slider_id_1_left = horizontal_slider_id_1.style.left;
				var horizontal_slider_id_2_left = horizontal_slider_id_2.style.left;
							
				//\u8bbe\u7f6eslider\u7684\u5de6\u53f3\u4e24\u8fb9\u7684\u4f4d\u7f6e
				horizontal_slider_id_1.style.left = ((v[0]-this.min)/(this.max-this.min))*parseInt(this.length)+"px";						
				horizontal_slider_id_2.style.left = ((v[1]-this.min)/(this.max-this.min))*parseInt(this.length)+"px";
							
				colorBar_id.style.left = horizontal_slider_id_1.style.left;
				colorBar_id.style.width = parseInt(horizontal_slider_id_2.style.left)-parseInt(horizontal_slider_id_1.style.left)+"px";
		
				
	},
	setSliderValue:function(value){
		if(!value)
			return;
			
		this.value = value;
		
		if(this.sliderType =="single")
		{
			this.setSingleSliderValue(value);
		}
		else if(this.sliderType =="double")
		{
			this.setDoubleSliderValue(value);
		}
						
	},
	setSingleSliderPosition:function(){
			var horizontal_track_id = document.getElementById("horizontal_track_"+this.id);
			var horizontal_slit_id = document.getElementById("horizontal_slit_"+this.id);
			var horizontal_slider_id = document.getElementById("horizontal_slider_single_"+this.id);
			var colorBar_id = document.getElementById("colorBar_"+this.id);
			
			horizontal_track_id.style.width = (parseFloat(this.length)+10)+"px";
			
			horizontal_slit_id.style.width = (parseFloat(this.length)+10)+"px";
			horizontal_slit_id.style.clip = 'rect(0px '+(parseFloat(this.length)+10)+'px 26px 0px)';
			
			//\u8bbe\u7f6eslider\u7684\u4f4d\u7f6e
			horizontal_slider_id.style.left = this.length+"px";
						
			colorBar_id.style.width = (parseFloat(this.length)+5)+"px";
			
			this.displayObj.value[0] = this.max;
	},
	createSingleSlider:function(){
			//\u663e\u793alabel
			eval(this.showlabel);
			
			/*if(this.bindCtrId =="")
				this.createDisplayElem();*/
				
			var sliderDiv = document.createElement("div");
			sliderDiv.id = "pad20_"+this.id;
			sliderDiv.className = "pad20";
			var horizontal_track_id = "horizontal_track_"+this.id;
			var horizontal_slit_id = "horizontal_slit_"+this.id;
			var horizontal_slider_id = "horizontal_slider_single_"+this.id;
			var colorBar_id = "colorBar_"+this.id;
			var sliderHTML = '<div class="horizontal_track" id="'+horizontal_track_id+'">'+
							'<div class="horizontal_slit"  id="'+horizontal_slit_id+'">&nbsp;</div>'+
	      					'<div class="horizontal_slider_single" id="'+horizontal_slider_id +'" style="Z-Index:906;left:130px;" '+ 
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\'\',\''+horizontal_slider_id+'\',\''+this.bindCtrId+'\',0);"></div>'+
	      					'<div class="colorBar" id="'+colorBar_id +'" ></div></div>';
			sliderDiv.innerHTML = sliderHTML;
			document.getElementById(this.id).appendChild(sliderDiv);
			
			//add
			this.setSingleSliderPosition();
			//add
			this.setSliderValue(this.value);
	},
	setDoubleSliderPosition:function(){
			var horizontal_track_id = document.getElementById("horizontal_track_"+this.id);
			var horizontal_slit_id = document.getElementById("horizontal_slit_"+this.id);
			var horizontal_slider_id_1 = document.getElementById("horizontal_slider_double_"+this.id+"_1");
			var horizontal_slider_id_2 = document.getElementById("horizontal_slider_double_"+this.id+"_2");
			var colorBar_id = document.getElementById("colorBar_"+this.id);
			
			horizontal_track_id.style.width = (parseFloat(this.length)+10)+"px";
			
			horizontal_slit_id.style.width = (parseFloat(this.length)+10)+"px";
			horizontal_slit_id.style.clip = 'rect(0px '+(parseFloat(this.length)+10)+'px 26px 0px)';
			
			//\u8bbe\u7f6eslider\u7684\u5de6\u53f3\u4e24\u8fb9\u7684\u4f4d\u7f6e
			horizontal_slider_id_1.style.left = 0+"px";						
			horizontal_slider_id_2.style.left = this.length+"px";
						
			colorBar_id.style.width = (parseFloat(this.length)+5)+"px";
						
			this.displayObj.value[1] = this.min;
			this.displayObj.value[2] = this.max;
	},
	createDoubleSlider:function(){
			//\u663e\u793alabel
			eval(this.showlabel);
			
			/*if(this.bindCtrId =="")
				this.createDisplayElem();*/
				
			var sliderDiv = document.createElement("div");
			sliderDiv.id = "pad20_"+this.id;
			sliderDiv.className = "pad20";
			var horizontal_track_id = "horizontal_track_"+this.id;
			var horizontal_slit_id = "horizontal_slit_"+this.id;
			var horizontal_slider_id_1 = "horizontal_slider_double_"+this.id+"_1";
			var horizontal_slider_id_2 = "horizontal_slider_double_"+this.id+"_2";
			var colorBar_id = "colorBar_"+this.id;
			var sliderHTML = '<div class="horizontal_track" id="'+horizontal_track_id+'">'+
							'<div class="horizontal_slit" id="'+horizontal_slit_id+'" >&nbsp;</div>'+
	      					'<div class="horizontal_slider_double" id="'+horizontal_slider_id_1 +'" style="Z-Index:908;Top:0px;Left:0px;WIDTH:10px" '+ 
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\''+horizontal_slider_id_1+'\',\''+horizontal_slider_id_2+'\',\''+this.bindCtrId+'\',1);"></div>'+
	      					'<div class="colorBar" id="'+colorBar_id +'" ></div>'+
	      					'<div class="horizontal_slider_double" id="'+horizontal_slider_id_2 +'" style="Z-Index:908;Top:0px;Left:130px;WIDTH:10px" '+ 	
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\''+horizontal_slider_id_1+'\',\''+horizontal_slider_id_2+'\',\''+this.bindCtrId+'\',2);"></div></div>';
			sliderDiv.innerHTML = sliderHTML;
			document.getElementById(this.id).appendChild(sliderDiv);
			
			//add
			this.setDoubleSliderPosition();
			//add
			this.setSliderValue(this.value);
	},
	init:function( ){	
		if(this.min >= this.max)
		{
			alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684\u6700\u5c0f\u503c\u5927\u4e8e\u6216\u7b49\u4e8e\u6700\u5927\u503c\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
			return;
		}
		
		if(this.sliderType =="single"){

			this.createSingleSlider();
		}
		else if(this.sliderType =="double"){	
			this.createDoubleSlider();		
		}
	},
	setSliderType:function(sliderType){
		this.sliderType=sliderType||this.sliderType;
	},
	setMin:function(min){
		this.min = min;
	},
	setMax:function(max){
		this.max=max;
	},
	setLength:function(length){
		this.length=length;
	},
	setBindCtrId:function(bindCtrId){
		this.bindCtrId=bindCtrId;
	}
};

//\u4eceslider\u4e2d\u5f97\u5230\u503c
SliderCtrObj.getValueFromSlider = function getValueFromSlider(id,pos){
	
	return SliderArray[id].getDataFromCtrl(pos);

};

//\u8fd4\u56deslider\u5f53\u524d\u7684\u503c\u5bf9\u8c61
SliderCtrObj.getValueObjFromSlider = function getValueObjFromSlider(id){
	if(SliderArray[id].sliderType ==  "single")
		return SliderArray[id].displayObj.value[0];
	else if(SliderArray[id].sliderType ==  "double")
	{
		var val =[SliderArray[id].displayObj.value[1],SliderArray[id].displayObj.value[2]];
		return val;
	}

};

SliderCtrObj.setValueObjFromSlider = function setValueObjFromSlider(id,val){
	if( val instanceof Array)
	{
		SliderArray[id].displayObj.value[1] = val[0];
		SliderArray[id].displayObj.value[2] = val[1];	
		SliderArray[id].setDoubleSliderPosition();
	}
	else
	{
		SliderArray[id].displayObj.value[0] = val;
		SliderArray[id].setSingleSliderPosition();
	}
	
	SliderArray[id].setSliderValue(val);
};

//\u5411slider\u8bbe\u503c
SliderCtrObj.setValueToSlider = function setValueToSlider(id,value,pos){

	SliderArray[id].bindDataToCtrl(value,pos);
};

//\u6839\u636e\u6307\u5b9aid\u5f97\u5230\u5143\u7d20\u6216\u5143\u7d20\u7ec4
SliderCtrObj.carpeGetElementById = function carpeGetElementById(element)
{
	if (document.getElementById) element = document.getElementById(element);
	else if (document.all) element = document.all[element];
	else element = null;
	return element;
};

//
SliderCtrObj.leftpadstring = function leftpadstring(sourcestring,padchar,length)
{
	var returnstring = sourcestring;
	for (var index=(""+sourcestring).length; index<length; index++ )
	{
		returnstring = padchar + returnstring;
	}
	return returnstring;
};

//\u5f97\u5230\u5143\u7d20\u7684\u4f4d\u7f6e
SliderCtrObj.GetObjsPos = function GetObjsPos(obj1, obj2)
{
	var left,width;
	
	left = 0;

	left = obj1==null?0:parseInt(obj1.style.left,10) + parseInt(obj1.style.width==""?0:obj1.style.width,10) ;
	
	width =obj2==null?0:parseInt(obj2.style.left,10)-left;
	
	if ( left-5>0) left = left-5;
	width = width + 5;
	
	return {left:left,width:width};
};

//\u5f97\u5230\u5143\u7d20\u7684left
SliderCtrObj.carpeLeft = function carpeLeft(elmnt, pos)
{
	if (!(elmnt = SliderCtrObj.carpeGetElementById(elmnt))) return 0;
	if (elmnt.style && (typeof(elmnt.style.left) == 'string')) {
		if (typeof(pos) == 'number') elmnt.style.left = pos + 'px';
		else {
			pos = parseInt(elmnt.style.left);
			if (isNaN(pos)) pos = 0;
		}
	}
	else if (elmnt.style && elmnt.style.pixelLeft) {
		if (typeof(pos) == 'number') elmnt.style.pixelLeft = pos;
		else pos = elmnt.style.pixelLeft;
	}
	return pos;
};

//\u5f97\u5230\u5143\u7d20\u7684top
SliderCtrObj.carpeTop = function carpeTop(elmnt, pos)
{
	if (!(elmnt = SliderCtrObj.carpeGetElementById(elmnt))) return 0;
	if (elmnt.style && (typeof(elmnt.style.top) == 'string')) {
		if (typeof(pos) == 'number') elmnt.style.top = pos + 'px';
		else {
			pos = parseInt(elmnt.style.top);
			if (isNaN(pos)) pos = 0;
		}
	}
	else if (elmnt.style && elmnt.style.pixelTop) {
		if (typeof(pos) == 'number') elmnt.style.pixelTop = pos;
		else pos = elmnt.style.pixelTop;
	}
	return pos;
};

addOnloadEvent(SliderInit);
/*
 * Tabs 3 - New Wave Tabs
 *
 * Copyright (c) 2007 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://docs.jquery.com/UI/Tabs
 */

(function($) {

    // if the UI scope is not availalable, add it
    $.ui = $.ui || {};

    // tabs API methods
    $.fn.tabs = function() {
        var method = typeof arguments[0] == 'string' && arguments[0];
        var args = method && Array.prototype.slice.call(arguments, 1) || arguments;

        return method == 'length' ?
            $.data(this[0], 'tabs').$tabs.length :
            this.each(function() {
                if (method) {
                    var tabs = $.data(this, 'tabs');
                    if (tabs) tabs[method].apply(tabs, args);
                } else
                    new $.ui.tabs(this, args[0] || {});
            });
    };

    // tabs class
    $.ui.tabs = function(el, options) {
        var self = this;
        
        this.options = $.extend({}, $.ui.tabs.defaults, options);
        this.element = el;

        // doesn't extend with null
        if (options.selected === null)
            this.options.selected = null;

        this.options.event += '.tabs'; // namespace event

        $(el).bind('setData.tabs', function(event, key, value) {
            if ((/^selected/).test(key))
                self.select(value);
            else {
                self.options[key] = value;
                self.tabify();
            }
        }).bind('getData.tabs', function(event, key) {
            return self.options[key];
        });

        // save instance for later
        $.data(el, 'tabs', this);

        // create tabs
        this.tabify(true);
    };
    
    $.ui.tabs.defaults = {
        // basic setup
        selected: 0,
        unselect: false,
        event: 'click',
        disabled: [],
        cookie: null, // e.g. { expires: 7, path: '/', domain: 'jquery.com', secure: true }
        // TODO history: false,

        // Ajax
        spinner: 'Loading&#8230;',
        cache: false,
        idPrefix: 'ui-tabs-',
        ajaxOptions: {},

        // animations
        fx: null, // e.g. { height: 'toggle', opacity: 'toggle', duration: 200 }

        // templates
        tabTemplate: '<li><a href="#{href}"><span>#{label}</span></a></li>',
        panelTemplate: '<div></div>',

        // CSS classes
        navClass: 'ui-tabs-nav',
        selectedClass: 'ui-tabs-selected',
        unselectClass: 'ui-tabs-unselect',
        disabledClass: 'ui-tabs-disabled',
        panelClass: 'ui-tabs-panel',
        hideClass: 'ui-tabs-hide',
        loadingClass: 'ui-tabs-loading'
    };

    // instance methods
    $.extend($.ui.tabs.prototype, {
        tabId: function(a) {
            return a.title && a.title.replace(/\s/g, '_').replace(/[^A-Za-z0-9\-_:\.]/g, '')
                || this.options.idPrefix + $.data(a);
        },
        ui: function(tab, panel) {
            return {
                instance: this,
                options: this.options,
                tab: tab,
                panel: panel
            };
        },
        tabify: function(init) {

            this.$lis = $('li:has(a[href])', this.element);
            this.$tabs = this.$lis.map(function() { return $('a', this)[0]; });
            this.$panels = $([]);

            var self = this, o = this.options;

            this.$tabs.each(function(i, a) {
                // inline tab
                if (a.hash && a.hash.replace('#', '')) // Safari 2 reports '#' for an empty hash
                    self.$panels = self.$panels.add(a.hash);
                // remote tab
                else if ($(a).attr('href') != '#') { // prevent loading the page itself if href is just "#"
                    $.data(a, 'href.tabs', a.href); // required for restore on destroy
                    $.data(a, 'load.tabs', a.href); // mutable
                    var id = self.tabId(a);
                    a.href = '#' + id;
                    var $panel = $('#' + id);
                    if (!$panel.length) {
                        $panel = $(o.panelTemplate).attr('id', id).addClass(o.panelClass)
                            .insertAfter( self.$panels[i - 1] || self.element );
                        $panel.data('destroy.tabs', true);
                    }
                    self.$panels = self.$panels.add( $panel );
                }
                // invalid tab href
                else
                    o.disabled.push(i + 1);
            });

            if (init) {

                // attach necessary classes for styling if not present
                $(this.element).hasClass(o.navClass) || $(this.element).addClass(o.navClass);
                this.$panels.each(function() {
                    var $this = $(this);
                    $this.hasClass(o.panelClass) || $this.addClass(o.panelClass);
                });

                // Try to retrieve selected tab:
                // 1. from fragment identifier in url if present
                // 2. from cookie
                // 3. from selected class attribute on <li>
                // 4. otherwise use given "selected" option
                // 5. check if tab is disabled
                this.$tabs.each(function(i, a) {
                    if (location.hash) {
                        if (a.hash == location.hash) {
                            o.selected = i;
                            // prevent page scroll to fragment
                            //if (($.browser.msie || $.browser.opera) && !o.remote) {
                            if ($.browser.msie || $.browser.opera) {
                                var $toShow = $(location.hash), toShowId = $toShow.attr('id');
                                $toShow.attr('id', '');
                                setTimeout(function() {
                                    $toShow.attr('id', toShowId); // restore id
                                }, 500);
                            }
                            scrollTo(0, 0);
                            return false; // break
                        }
                    } else if (o.cookie) {
                        var index = parseInt($.cookie('ui-tabs' + $.data(self.element)),10);
                        if (index && self.$tabs[index]) {
                            o.selected = index;
                            return false; // break
                        }
                    } else if ( self.$lis.eq(i).hasClass(o.selectedClass) ) {
                        o.selected = i;
                        return false; // break
                    }
                });

                // highlight selected tab
                this.$panels.addClass(o.hideClass);
                this.$lis.removeClass(o.selectedClass);
                if (o.selected !== null) {
                    this.$panels.eq(o.selected).show().removeClass(o.hideClass); // use show and remove class to show in any case no matter how it has been hidden before
                    this.$lis.eq(o.selected).addClass(o.selectedClass);
                }

                // load if remote tab
                var href = o.selected !== null && $.data(this.$tabs[o.selected], 'load.tabs');
                if (href)
                    this.load(o.selected);

                // Take disabling tabs via class attribute from HTML
                // into account and update option properly...
                o.disabled = $.unique(o.disabled.concat(
                    $.map(this.$lis.filter('.' + o.disabledClass),
                        function(n, i) { return self.$lis.index(n); } )
                )).sort();
                
                // clean up to avoid memory leaks in certain versions of IE 6
                $(window).bind('unload', function() {
                    self.$tabs.unbind('.tabs');
                    self.$lis = self.$tabs = self.$panels = null;
                });

            }

            // disable tabs
            for (var i = 0, li; li = this.$lis[i]; i++)
			{
				$(li)[$.inArray(i, o.disabled) != -1 && !$(li).hasClass(o.selectedClass) ? 'addClass' : 'removeClass'](o.disabledClass);				
			}

            // reset cache if switching from cached to not cached
            if (o.cache === false)
                this.$tabs.removeData('cache.tabs');
            
            // set up animations
            var hideFx, showFx, baseFx = { 'min-width': 0, duration: 1 }, baseDuration = 'normal';
            if (o.fx && o.fx.constructor == Array)
                hideFx = o.fx[0] || baseFx, showFx = o.fx[1] || baseFx;
            else
                hideFx = showFx = o.fx || baseFx;

            // reset some styles to maintain print style sheets etc.
            var resetCSS = { display: '', overflow: '', height: '' };
            if (!$.browser.msie) // not in IE to prevent ClearType font issue
                resetCSS.opacity = '';

            // Hide a tab, animation prevents browser scrolling to fragment,
            // $show is optional.
            function hideTab(clicked, $hide, $show) {
                $hide.animate(hideFx, hideFx.duration || baseDuration, function() { //
                    $hide.addClass(o.hideClass).css(resetCSS); // maintain flexible height and accessibility in print etc.
                    if ($.browser.msie && hideFx.opacity)
                        $hide[0].style.filter = '';
                    if ($show)
                        showTab(clicked, $show, $hide);
                });
            }

            // Show a tab, animation prevents browser scrolling to fragment,
            // $hide is optional.
            function showTab(clicked, $show, $hide) {
                if (showFx === baseFx)
                    $show.css('display', 'block'); // prevent occasionally occuring flicker in Firefox cause by gap between showing and hiding the tab panels
                $show.animate(showFx, showFx.duration || baseDuration, function() {
                    $show.removeClass(o.hideClass).css(resetCSS); // maintain flexible height and accessibility in print etc.
                    if ($.browser.msie && showFx.opacity)
                        $show[0].style.filter = '';

                    // callback
                    $(self.element).triggerHandler('tabsshow', [self.ui(clicked, $show[0])], o.show);

                });
            }

            // switch a tab
            function switchTab(clicked, $li, $hide, $show) {
                /*if (o.bookmarkable && trueClick) { // add to history only if true click occured, not a triggered click
                    $.ajaxHistory.update(clicked.hash);
                }*/
                $li.addClass(o.selectedClass)
                    .siblings().removeClass(o.selectedClass);
                hideTab(clicked, $hide, $show);
            }

            // attach tab event handler, unbind to avoid duplicates from former tabifying...
            this.$tabs.unbind('.tabs').bind(o.event, function() {

                //var trueClick = e.clientX; // add to history only if true click occured, not a triggered click
                var $li = $(this).parents('li:eq(0)'),
                    $hide = self.$panels.filter(':visible'),
                    $show = $(this.hash);

                // If tab is already selected and not unselectable or tab disabled or 
                // or is already loading or click callback returns false stop here.
                // Check if click handler returns false last so that it is not executed
                // for a disabled or loading tab!
                if (($li.hasClass(o.selectedClass) && !o.unselect)
                    || $li.hasClass(o.disabledClass) 
                    || $(this).hasClass(o.loadingClass)
                    || $(self.element).triggerHandler('tabsselect', [self.ui(this, $show[0])], o.select) === false
                    ) {
                    this.blur();
                    return false;
                }

                self.options.selected = self.$tabs.index(this);

                // if tab may be closed
                if (o.unselect) {
                    if ($li.hasClass(o.selectedClass)) {
                        self.options.selected = null;
                        $li.removeClass(o.selectedClass);
                        self.$panels.stop();
                        hideTab(this, $hide);
                        this.blur();
                        return false;
                    } else if (!$hide.length) {
                        self.$panels.stop();
                        var a = this;
                        self.load(self.$tabs.index(this), function() {
                            $li.addClass(o.selectedClass).addClass(o.unselectClass);
                            showTab(a, $show);
                        });
                        this.blur();
                        return false;
                    }
                }

                if (o.cookie)
                    $.cookie('ui-tabs' + $.data(self.element), self.options.selected, o.cookie);

                // stop possibly running animations
                self.$panels.stop();

                // show new tab
                if ($show.length) {

                    // prevent scrollbar scrolling to 0 and than back in IE7, happens only if bookmarking/history is enabled
                    /*if ($.browser.msie && o.bookmarkable) {
                        var showId = this.hash.replace('#', '');
                        $show.attr('id', '');
                        setTimeout(function() {
                            $show.attr('id', showId); // restore id
                        }, 0);
                    }*/

                    var a = this;
                    self.load(self.$tabs.index(this), $hide.length ? 
                        function() {
                            switchTab(a, $li, $hide, $show);
                        } :
                        function() {
                            $li.addClass(o.selectedClass);
                            showTab(a, $show);
                        }
                    );

                    // Set scrollbar to saved position - need to use timeout with 0 to prevent browser scroll to target of hash
                    /*var scrollX = window.pageXOffset || document.documentElement && document.documentElement.scrollLeft || document.body.scrollLeft || 0;
                    var scrollY = window.pageYOffset || document.documentElement && document.documentElement.scrollTop || document.body.scrollTop || 0;
                    setTimeout(function() {
                        scrollTo(scrollX, scrollY);
                    }, 0);*/

                } else
                    throw 'jQuery UI Tabs: Mismatching fragment identifier.';

                // Prevent IE from keeping other link focussed when using the back button
                // and remove dotted border from clicked link. This is controlled in modern
                // browsers via CSS, also blur removes focus from address bar in Firefox
                // which can become a usability and annoying problem with tabsRotate.
                if ($.browser.msie)
                    this.blur();

                //return o.bookmarkable && !!trueClick; // convert trueClick == undefined to Boolean required in IE
                return false;

            });

            // disable click if event is configured to something else
            if (!(/^click/).test(o.event))
                this.$tabs.bind('click.tabs', function() { return false; });

        },
        add: function(url, label, index) {
            if (index == undefined) 
                index = this.$tabs.length; // append by default

            var o = this.options;
            var $li = $(o.tabTemplate.replace(/#\{href\}/, url).replace(/#\{label\}/, label));
            $li.data('destroy.tabs', true);
			$li.addClass(o.unselectClass);

            var id = url.indexOf('#') == 0 ? url.replace('#', '') : this.tabId( $('a:first-child', $li)[0] );

            // try to find an existing element before creating a new one
            var $panel = $('#' + id);
            if (!$panel.length) {
                $panel = $(o.panelTemplate).attr('id', id)
                    .addClass(o.panelClass).addClass(o.hideClass);
                $panel.data('destroy.tabs', true);
            }
            if (index >= this.$lis.length) {
                $li.appendTo(this.element);
                $panel.appendTo(this.element.parentNode);
            } else {
                $li.insertBefore(this.$lis[index]);
                $panel.insertBefore(this.$panels[index]);
            }
            
            o.disabled = $.map(o.disabled,
                function(n, i) { return n >= index ? ++n : n });
                
            this.tabify();

            if (this.$tabs.length == 1) {
                 $li.addClass(o.selectedClass);
                 $panel.removeClass(o.hideClass);
                 var href = $.data(this.$tabs[0], 'load.tabs');
                 if (href)
                     this.load(index, href);
            }

            // callback
            $(this.element).triggerHandler('tabsadd',
                [this.ui(this.$tabs[index], this.$panels[index])], o.add
            );
        },
        remove: function(index) {
            var o = this.options,$li = this.$lis.eq(index).remove(),
            $panel = this.$panels.eq(index).remove();

            // If selected tab was removed focus tab to the right or
            // in case the last tab was removed the tab to the left.
            if ($li.hasClass(o.selectedClass) && this.$tabs.length > 1)
            {
            //wanghao
                //this.select(index + (index + 1 < this.$tabs.length ? 1 : -1));
                //alert(this.$tabs.length);
            }

            o.disabled = $.map($.grep(o.disabled, function(n, i) { return n != index; }),
                function(n, i) { return n >= index ? --n : n });

            this.tabify();

            // callback
            $(this.element).triggerHandler('tabsremove',
                [this.ui($li.find('a')[0], $panel[0])], o.remove
            );
             //wanghao
            //alert("index:"+index+"-----this.$tabs.length:"+this.$tabs.length);
            this.select(index - 1 + (index< this.$tabs.length? 1 : 0));
        },
        enable: function(index) {
            var o = this.options;
            if ($.inArray(index, o.disabled) == -1)
                return;
                
            var $li = this.$lis.eq(index).removeClass(o.disabledClass);
            if ($.browser.safari) { // fix disappearing tab (that used opacity indicating disabling) after enabling in Safari 2...
                $li.css('display', 'inline-block');
                setTimeout(function() {
                    $li.css('display', 'block');
                }, 0);
            }

            o.disabled = $.grep(o.disabled, function(n, i) { return n != index; });

            // callback
            $(this.element).triggerHandler('tabsenable',
                [this.ui(this.$tabs[index], this.$panels[index])], o.enable
            );

        },
        disable: function(index) {
            var self = this, o = this.options;
            if (index != o.selected) { // cannot disable already selected tab
                this.$lis.eq(index).addClass(o.disabledClass);

                o.disabled.push(index);
                o.disabled.sort();

                // callback
                $(this.element).triggerHandler('tabsdisable',
                    [this.ui(this.$tabs[index], this.$panels[index])], o.disable
                );
            }
        },
        select: function(index) {
            if (typeof index == 'string')
                index = this.$tabs.index( this.$tabs.filter('[href$=' + index + ']')[0] );
            this.$tabs.eq(index).trigger(this.options.event);
        },
        load: function(index, callback) { // callback is for internal usage only
            
            var self = this, o = this.options, $a = this.$tabs.eq(index), a = $a[0],
                    bypassCache = callback == undefined || callback === false, url = $a.data('load.tabs');

            callback = callback || function() {};
            
            // no remote or from cache - just finish with callback
            if (!url || ($.data(a, 'cache.tabs') && !bypassCache)) {
                callback();
                return;
            }

            // load remote from here on
            if (o.spinner) {
                var $span = $('span', a);
                $span.data('label.tabs', $span.html()).html('<em>' + o.spinner + '</em>');
            }
            var finish = function() {
                self.$tabs.filter('.' + o.loadingClass).each(function() {
                    $(this).removeClass(o.loadingClass);
                    if (o.spinner) {
                        var $span = $('span', this);
                        $span.html($span.data('label.tabs')).removeData('label.tabs');
                    }
                });
                self.xhr = null;
            };
            var ajaxOptions = $.extend({}, o.ajaxOptions, {
                url: url,
                success: function(r, s) {
                    $(a.hash).html(r);
                    finish();
                    
                    // This callback is required because the switch has to take
                    // place after loading has completed.
                    callback();

                    if (o.cache)
                        $.data(a, 'cache.tabs', true); // if loaded once do not load them again

                    // callback
                    $(self.element).triggerHandler('tabsload',
                        [self.ui(self.$tabs[index], self.$panels[index])], o.load
                    );

                    o.ajaxOptions.success && o.ajaxOptions.success(r, s);
                }
            });
            if (this.xhr) {
                // terminate pending requests from other tabs and restore tab label
                this.xhr.abort();
                finish();
            }
            $a.addClass(o.loadingClass);
            setTimeout(function() { // timeout is again required in IE, "wait" for id being restored
                self.xhr = $.ajax(ajaxOptions);
            }, 0);

        },
        url: function(index, url) {
            this.$tabs.eq(index).removeData('cache.tabs').data('load.tabs', url);
        },
        destroy: function() {
            var o = this.options;
            $(this.element).unbind('.tabs')
                .removeClass(o.navClass).removeData('tabs');
            this.$tabs.each(function() {
                var href = $.data(this, 'href.tabs');
                if (href)
                    this.href = href;
                var $this = $(this).unbind('.tabs');
                $.each(['href', 'load', 'cache'], function(i, prefix) {
                      $this.removeData(prefix + '.tabs');
                });
            });
            this.$lis.add(this.$panels).each(function() {
                if ($.data(this, 'destroy.tabs'))
                    $(this).remove();
                else
                    $(this).removeClass([o.selectedClass, o.unselectClass,
                        o.disabledClass, o.panelClass, o.hideClass].join(' '));
            });
        }
    });

})(jQuery);


var SSB_RIA_TAB = "tab";
var SSB_RIA_PANEL = "panel";



var riatabs_init = function()
{
	var riatabs = new SSB_RIATAB();
	//debugger;
	var _tabs = riatabs.getTabsNodes(document,SSB_RIA_TAB);
	var index = _tabs.length;
	if(_tabs)
	{
		for(var i=0;i<index;i++)
		{
			riatabs.createRiaTabs(_tabs[0]);
		}
		
	}
	return riatabs;
};

var SSB_RIATAB = function(){
	this.panelArr = [];

	this.getTabsNodes = function (obj,tag)
	{
		var nodes= obj.getElementsByTagName(tag);
		
		if(navigator.appName == "Microsoft Internet Explorer")
		{
			nodes= obj.getElementsByTagName(tag);
		}else
		{
			nodes= obj.getElementsByTagName("z:"+tag);
		}

		return nodes;
	};

	 this.createRiaTabs = function (tab)
	{
		var panels = this.getTabsNodes(tab,SSB_RIA_PANEL);
		var tabObj = this.getTabsTag(tab);
		this.createRiaDivs(panels,tabObj);
		
	};

	 this.createRiaDivs = function(panels,tabObj)
	{
		var theTabElement = document.getElementById(tabObj.id);
		var parentElement = theTabElement.parentNode;
		var div_node=document.createElement("div");
		div_node.className=tabObj.className;
		div_node.setAttribute("id",tabObj.id);
		//div_node.setAttribute("height",tabObj.height);
		//div_node.setAttribute("width",tabObj.width);
		parentElement.appendChild(div_node);
		//theTabElement.setAttribute("id",tabObj.id+"_1");
		this.createRiaPanels(div_node,panels,tabObj);
		parentElement.replaceChild(div_node,theTabElement);
		disposeEvents(this.panelArr,tabObj);
	};

	this.createRiaPanels = function(div_node,panels,tabObj)
	{
		if(panels)
		{
			var ul=document.createElement("ul");
			ul.setAttribute("id",tabObj.id+"_ul");
			for(var i=0;i<panels.length;i++)
			{
				var panel = this.getPanelsTag(panels[i]);
				this.panelArr.push(panel);
				this.createRiaLis(ul,panel);
			}
			
			div_node.appendChild(ul);

			var type = tabObj.type;
			
			for(var i=0;i<panels.length;i++)
			{
				var panel = this.getPanelsTag(panels[i]);
				var _div = this.createRiaPanelsDiv(div_node,panel.id);
				if(panel.href && type && $.trim(type) == "multi")
				{
					var iframe = document.createElement("iframe");
					iframe.setAttribute("src",panel.href);
					iframe.setAttribute("height",'100%');
					iframe.setAttribute("width",'100%');
					iframe.setAttribute("frameborder",'0');
					iframe.setAttribute("frameBorder",'0');
					iframe.style.marginRight = "-52px";
					_div.appendChild(iframe);
				}
				else if(panel.href && document.getElementById(panel.href))
				{
					_div.appendChild(document.getElementById(panel.href));
				}

			}
			
		}
		
	};

	this.createRiaPanelsDiv = function (div_node,id)
	{
		var _div=document.createElement("div");
		_div.setAttribute("id",id);
		_div.setAttribute("height",'100%');
		_div.setAttribute("width",'100%');
		div_node.appendChild(_div);
		return _div;
	};


	this.createRiaLis = function(ul,panel)
	{
		
		//_node.className=className;
		var lis = document.createElement("li");
		lis.className = "ui-tabs-unselect";
		var as  = document.createElement("a");
		as.setAttribute("href","#"+panel.id);
		as.innerHTML = "<span title='"+panel.tip+"'>"+panel.lable+"</span>";
		lis.appendChild(as);
		ul.appendChild(lis);
		return ul;
	};

	this.getTabsTag = function(tab)
	{
		var tabObj = {};
		tabObj.id = tab.getAttribute("id");
		tabObj.className = tab.getAttribute("className");
		tabObj.width = tab.getAttribute("width");
		tabObj.height = tab.getAttribute("height");
		tabObj.type = tab.getAttribute("type");
		return tabObj;
	};


	this.getPanelsTag = function(panel)
	{
		var panelObj = {};
		panelObj.id = panel.getAttribute("id");
		//panelObj.className = panel.getAttribute("class");
		panelObj.href = panel.getAttribute("href");
		panelObj.tip = panel.getAttribute("tip");
		panelObj.lable = panel.getAttribute("lable");
		panelObj.onblur = panel.getAttribute("onblur");
		panelObj.onfocus = panel.getAttribute("onfocus");
		return panelObj;
	};

};



SSB_RIATAB.addTabDiv = function(tabId,panelId,divName,divId,title,index)
{
	var tabObj = document.getElementById(tabId);
	if(!tabObj)
	{
		return;
	}

	var divIds = document.getElementById(panelId);

	if(index<0)
	{
		$('#'+tabId+' > ul').tabs('add', '#'+panelId, divName);
	}
	else
	{
		$('#'+tabId+' > ul').tabs('add', '#'+panelId, divName, index);
	}

	var divId = document.getElementById(panelId);
	var cdivId = document.getElementById(divId);

	if(divId && cdivId)
	{
		if(divIds)
		{
			$("#"+panelId).css({width: "100%", height:"100%"});
		}
		divId.appendChild(cdivId);
	}
	var _spans = tabObj.getElementsByTagName("span");
	if(!_spans || !title)
	{
		return;
	}
	for(var i=0;i<_spans.length;i++)
	{
		if(_spans[i] && _spans[i].innerHTML == divName)
		{
			_spans[i].title=title;
		}
	}
};

SSB_RIATAB.addTabPage = function(tabId,panelId,pageName,pageUrl,title,index)
{
	var tabObj = document.getElementById(tabId);
	if(!tabObj)
	{
		return;
	}

	if(index<0)
	{
		$('#'+tabId+' > ul').tabs('add', '#'+panelId, pageName);
	}
	else
	{
		$('#'+tabId+' > ul').tabs('add', '#'+panelId, pageName, index);
	}
	
	var divId = document.getElementById(panelId);
	if(divId)
	{
		var iframe = document.createElement("iframe");
		iframe.setAttribute("src",pageUrl);
		iframe.setAttribute("height",'100%');
		iframe.setAttribute("width",'100%');
		iframe.setAttribute("frameborder",'0');
		iframe.setAttribute("frameBorder",'0');
		iframe.style.marginRight = "-52px";
		divId.appendChild(iframe);
	}
	
	var _spans = tabObj.getElementsByTagName("span");
	if(!_spans || !title)
	{
		return;
	}
	for(var i=0;i<_spans.length;i++)
	{
		if(_spans[i] && _spans[i].innerHTML == pageName)
		{
			_spans[i].title=title;
		}
	}
	
	//wanghao
	SSB_RIATAB.selectTab(tabId,SSB_RIATAB.selectIndex(tabId)+1);
};

// \u589e\u52a0css\u6837\u5f0f
SSB_RIATAB.addCss = function (id,key,value)
{
	if(value)
	{
		$("#"+id+"").css(key,value);
	}
	else{
		$("#"+id+"").css(key);
	}
};

// \u9009\u62e9tab\u53f7
SSB_RIATAB.selectIndex = function(tabId)
{
	return $('#'+tabId+' > ul').data('selected.tabs');
};

// \u589e\u52a0tab\u6837\u5f0f
SSB_RIATAB.addTabClass = function(id,className)
{
	$("#"+id+"").addClass(className);
};

// \u5220\u9664tab\u6837\u5f0f
SSB_RIATAB.removeTabClass = function(id,className)
{
	$("#"+id+"").removeClass(className);
};

SSB_RIATAB.removeAllTabs = function(tabId)
{
	var size = SSB_RIATAB.size(tabId);
	for(var i =size-1;i>=0;i--)
	{
		$('#'+tabId+' > ul').tabs('remove',i);
	}
}

// \u5220\u9664\u9009\u4e2d\u7684tab
SSB_RIATAB.removeTabs = function(tabId)
{
	var index = SSB_RIATAB.selectIndex(tabId);
	var size = SSB_RIATAB.size(tabId);
	$('#'+tabId+' > ul').tabs('remove',index);
	//SSB_RIATAB.selectTab(tabId,0);
	//alert(SSB_RIATAB.size(tabId));
	/*if( index >0 && index < size-1 )
	{
		SSB_RIATAB.selectTab(tabId,index);
	}
	else if(index = 0)
	{
		SSB_RIATAB.selectTab(tabId,0);
	}	
	else if(index = size-1)
	{
		SSB_RIATAB.selectTab(tabId,index-1);
	}*/
					
};

// \u7b2c\u51e0\u4e2atab\u88ab\u9009\u4e2d
SSB_RIATAB.selectTab = function(tabId,index)
{
	$('#'+tabId+' > ul').tabs('select',index);
};

// tab\u4e0d\u53ef\u7528
SSB_RIATAB.disableTab = function(tabId,index)
{
	$('#'+tabId+' > ul').tabs('disable',index);
};

// tab\u53ef\u7528
SSB_RIATAB.enableTab = function(tabId,index)
{
	$('#'+tabId+' > ul').tabs('enable',index);
};

//tab\u7684\u603b\u6570
SSB_RIATAB.size = function(tabId)
{
	return $('#'+tabId+' > ul').tabs("length");
}



//\u5411\u5de6\u79fb\u52a8\u4e00\u4e2atab
SSB_RIATAB.tabScroll = function tabScroll(tabId,LorR)
{
	var index = SSB_RIATAB.selectIndex(tabId);
	var size = SSB_RIATAB.size(tabId);
	//alert(index);
	if(LorR == "left")
	{
		if(index>0)
		{
			SSB_RIATAB.selectTab(tabId,index-1);			
		}
		else
		{
			return;
		}
		
	}
	else
	{
		if(index<size-1)
		{
			SSB_RIATAB.selectTab(tabId,index+1);
		}
		else
		{
			return;
		}
	}
}

SSB_RIATAB.tabEventObj ={};

// \u589e\u52a0\u5f97\u5230\u7126\u70b9\u4e8b\u4ef6
SSB_RIATAB.addOnfocusEvent = function (tabId,id,onfocus)
{
	SSB_RIATAB.tabEventObj[id+"_tabDiv"] = onfocus;
	$(function() {
		$('#'+tabId+' > ul').tabs({
			show: function(ui) {
				if(SSB_RIATAB.tabEventObj[ui.panel.id])
				{
					eval(SSB_RIATAB.tabEventObj[ui.panel.id]);
				}
			}
		});
	});
};


var disposeEvents = function (panels,tabObj)
{
	$(function() {
		$('#'+tabObj.id+' > ul').tabs({
			show: function(ui) {
				for(var j=0;j<panels.length;j++)
				{
					if(panels[j] && panels[j].onfocus && $.trim(panels[j].onfocus) != "" && ui.panel.id == panels[j].id)
					{
						//alert(eval(panels[j].onfocus)+":"+panels[j].onfocus);
						//ui.tab.attachEvent("onfocus",eval(panels[j].onfocus));
						SSB_RIATAB.tabEventObj[ui.panel.id] = panels[j].onfocus;
						eval(panels[j].onfocus);
					}
				}
			}
		});
	});
	$("#"+tabObj.id+"").css({width: tabObj.width , height:tabObj.height});
};


SSB_RIATAB.addTabDivTest = function(tabId,panelId,pageName,index,title,divId)
{
	var tabObj = document.getElementById(tabId);
	if(!tabObj)
	{
		return;
	}
		if(index<0)
		{
			$('#'+tabId+' > ul').tabs('add', '#'+divId, pageName);
		}
		else
		{
			$('#'+tabId+' > ul').tabs('add', '#'+divId, pageName, index);
		}
	
	var divObj = document.getElementById(divId);
	var cdivId = document.getElementById(panelId);
	if(divId && cdivId)
	{
		//$("#"+divId).css({width: "100%", height:"100%"});
		divObj.appendChild(cdivId);
	}
	var _spans = tabObj.getElementsByTagName("span");
	if(!_spans || !title)
	{
		return;
	}
	for(var i=0;i<_spans.length;i++)
	{
		if(_spans[i] && _spans[i].innerHTML == pageName)
		{
			_spans[i].title=title;
		}
	}
};
function getLocal(){return 'en'};
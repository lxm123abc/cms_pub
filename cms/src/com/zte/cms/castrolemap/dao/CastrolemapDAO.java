package com.zte.cms.castrolemap.dao;

import java.util.List;

import com.zte.cms.castrolemap.dao.ICastrolemapDAO;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CastrolemapDAO extends DynamicObjectBaseDao implements ICastrolemapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCastrolemap(Castrolemap castrolemap) throws DAOException
    {
        log.debug("insert castrolemap starting...");
        super.insert("insertCastrolemap", castrolemap);
        log.debug("insert castrolemap end");
    }

    public void updateCastrolemap(Castrolemap castrolemap) throws DAOException
    {
        log.debug("update castrolemap by pk starting...");
        super.update("updateCastrolemap", castrolemap);
        log.debug("update castrolemap by pk end");
    }

    public void updateCastrolemapByCond(Castrolemap castrolemap) throws DAOException
    {
        log.debug("update castrolemap by conditions starting...");
        super.update("updateCastrolemapByCond", castrolemap);
        log.debug("update castrolemap by conditions end");
    }

    public void deleteCastrolemap(Castrolemap castrolemap) throws DAOException
    {
        log.debug("delete castrolemap by pk starting...");
        super.delete("deleteCastrolemap", castrolemap);
        log.debug("delete castrolemap by pk end");
    }

    public void deleteCastrolemapByCond(Castrolemap castrolemap) throws DAOException
    {
        log.debug("delete castrolemap by conditions starting...");
        super.delete("deleteCastrolemapByCond", castrolemap);
        log.debug("update castrolemap by conditions end");
    }

    public Castrolemap getCastrolemap(Castrolemap castrolemap) throws DAOException
    {
        log.debug("query castrolemap starting...");
        Castrolemap resultObj = (Castrolemap) super.queryForObject("getCastrolemap", castrolemap);
        log.debug("query castrolemap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Castrolemap> getCastrolemapByCond(Castrolemap castrolemap) throws DAOException
    {
        log.debug("query castrolemap by condition starting...");
        List<Castrolemap> rList = (List<Castrolemap>) super.queryForList("queryCastrolemapListByCond", castrolemap);
        log.debug("query castrolemap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DAOException
    {
        log.debug("page query castrolemap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastrolemapListCntByCond", castrolemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Castrolemap> rsList = (List<Castrolemap>) super.pageQuery("queryCastrolemapListByCond", castrolemap,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castrolemap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCastrolemapListByCond", "queryCastrolemapListCntByCond", castrolemap, start,
                pageSize, puEntity);
    }

}
package com.zte.cms.channel.apply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.IChannelWkfwhisDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelApplyCallback extends CommonCallback
{
    // 日志
    protected ICmsChannelDS channelds = null;
    protected IChannelWkfwhisDS channelWkfwhisDS;
    protected CmsChannel channel;
    protected ChannelWkfwhis channelWkfwhis;
    private Log log = SSBBus.getLog(getClass());
    private IPhysicalchannelDS physicalchannelDS = null;

    /**
     * 
     * @return ICmsChannelDS
     */
    public ICmsChannelDS getChannelds()
    {
        return channelds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return IChannelWkfwhisDS
     */
    public IChannelWkfwhisDS getChannelWkfwhisDS()
    {
        return channelWkfwhisDS;
    }

    /**
     * 
     * @param channelWkfwhisDS IChannelWkfwhisDS
     */
    public void setChannelWkfwhisDS(IChannelWkfwhisDS channelWkfwhisDS)
    {
        this.channelWkfwhisDS = channelWkfwhisDS;
    }

    /**
     * 
     * @return CmsChannel
     */
    public CmsChannel getChannel()
    {
        return channel;
    }

    /**
     * 
     * @param channel CmsChannel
     */
    public void setChannel(CmsChannel channel)
    {
        this.channel = channel;
    }

    /**
     * 
     * @return ChannelWkfwhis
     */
    public ChannelWkfwhis getChannelWkfwhis()
    {
        return channelWkfwhis;
    }

    /**
     * 
     * @param channelWkfwhis ChannelWkfwhis
     */
    public void setChannelWkfwhis(ChannelWkfwhis channelWkfwhis)
    {
        this.channelWkfwhis = channelWkfwhis;
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        String optObjecttype = ChannelConstants.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(wkflItem.getPboIndex().toString());

        String info;
        String logKey = "";
        if (wkflItem.getNodeId().longValue() == 1)
        {
            logKey = "ucdn.channel.log.wkfw.cntdistribute.add.apply";            		  
        }
        else
        {
            logKey += "ucdn.channel.log.wkfw.cntdistribute.add.audit";
        }
        info = ResourceMgt.findDefaultText(logKey);
        if (null != info)
        {
            info = info.replace("[opername]", wkflItem.getExcutorName());
            info = info.replace("[channelname]", channel.getChannelname());
            info = info.replace("[nodename]", wkflItem.getNodeName());
            info = info.replace("[handleevent]", wkflItem.getHandleEvent());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long channelindex = wkflItem.getPboIndex();
        channel = new CmsChannel();
        channel.setChannelindex(channelindex);

        // 查询channel对象
        try
        {
            channel = channelds.getCmsChannel(channel);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        // strategy对象处理，修改正式表状态
        channel.setWorkflowlife(wkflItem.getPboState());

        // 工作流对象处理，设置生效时间
        wkflItem.setEffectiveTime("0");

        // 创建工作流历史对象
        channelWkfwhis = channel.newStrategyWkfwhis();
        channelWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
        channelWkfwhis.setTaskindex(Long.valueOf(wkflItem.getTaskId()));
        channelWkfwhis.setNodeid(wkflItem.getNodeId().longValue());
        channelWkfwhis.setNodename(wkflItem.getNodeName());
        channelWkfwhis.setOpername(wkflItem.getExcutorId());
        channelWkfwhis.setOpopinion(wkflItem.getComments());
        channelWkfwhis.setTemplateindex(wkflItem.getTemplateSeqNo().longValue());
        channelWkfwhis.setHandleevent(wkflItem.getHandleEvent());
       

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        if (wkflItem.getStartTime() != null)
        {
            String workflowstarttime = dateformat.format(wkflItem.getStartTime());
            channelWkfwhis.setWorkflowstarttime(workflowstarttime);
        }
        if (wkflItem.getFinishTime() != null)
        {
            String workflowendtime = dateformat.format(wkflItem.getFinishTime());
            channelWkfwhis.setWorkflowendtime(workflowendtime);
        }
    }

    @Override
    public void operate()
    {
        log.info("domain exception:");
        try
        {
            if (wkflItem.getPboState().intValue() == -1)
            {
                channel.setStatus(130); // 状态改成审核失败
                channel.setWorkflow(0); // 未启动
                channel.setWorkflowlife(0); // 未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                channel.setEffecttime(format.format(new Date())); // 将生效时间字段设为当前时间
                channelds.updateCmsChannel(channel);
                Physicalchannel physicalchannel = new Physicalchannel();
                physicalchannel.setChannelid(channel.getChannelid());
                List list = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannel);
                if (list != null && list.size() > 0)
                {
                    for (int i = 0; i < list.size(); i++)
                    {
                        Physicalchannel physical = (Physicalchannel) list.get(i);
                        physical.setStatus(130); // 状态改成审核失败
                        physical.setWorkflow(0); // 未启动
                        physical.setWorkflowlife(0); // 未启动
                        physical.setEffecttime(format.format(new Date())); // 将生效时间字段设为当前时间
                        physicalchannelDS.updatePhysicalchannel(physical);

                    }
                }
            }
            else
            {
                channelds.updateCmsChannel(channel);
            }

            channelWkfwhisDS.insertChannelWkfwhis(channelWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

}

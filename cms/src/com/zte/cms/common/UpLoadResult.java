package com.zte.cms.common;

public class UpLoadResult
{
    private boolean result;
    private String fileName;
    private Long fileSize;
    private String md5;
    private String posterFilePath;
    private Integer flag;

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public Long getFileSize()
    {
        return fileSize;
    }

    public void setFileSize(Long fileSize)
    {
        this.fileSize = fileSize;
    }

    public String getMd5()
    {
        return md5;
    }

    public void setMd5(String md5)
    {
        this.md5 = md5;
    }

    public String getPosterFilePath()
    {
        return posterFilePath;
    }

    public void setPosterFilePath(String posterFilePath)
    {
        this.posterFilePath = posterFilePath;
    }

    public Integer getFlag()
    {
        return flag;
    }

    public void setFlag(Integer flag)
    {
        this.flag = flag;
    }

}

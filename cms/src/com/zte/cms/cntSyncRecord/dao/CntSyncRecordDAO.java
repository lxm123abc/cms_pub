package com.zte.cms.cntSyncRecord.dao;

import java.util.List;

import com.zte.cms.cntSyncRecord.dao.ICntSyncRecordDAO;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CntSyncRecordDAO extends DynamicObjectBaseDao implements ICntSyncRecordDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException
    {
        log.debug("insert cntSyncRecord starting...");
        super.insert("insertCntSyncRecord", cntSyncRecord);
        log.debug("insert cntSyncRecord end");
    }

    public void updateCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException
    {
        log.debug("update cntSyncRecord by pk starting...");
        super.update("updateCntSyncRecord", cntSyncRecord);
        log.debug("update cntSyncRecord by pk end");
    }

    public void deleteCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException
    {
        log.debug("delete cntSyncRecord by pk starting...");
        super.delete("deleteCntSyncRecord", cntSyncRecord);
        log.debug("delete cntSyncRecord by pk end");
    }

    public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord) throws DAOException
    {
        log.debug("query cntSyncRecord starting...");
        CntSyncRecord resultObj = (CntSyncRecord) super.queryForObject("getCntSyncRecord", cntSyncRecord);
        log.debug("query cntSyncRecord end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CntSyncRecord> getCntSyncRecordByCond(CntSyncRecord cntSyncRecord) throws DAOException
    {
        log.debug("query cntSyncRecord by condition starting...");
        List<CntSyncRecord> rList = (List<CntSyncRecord>) super.queryForList("queryCntSyncRecordListByCond",
                cntSyncRecord);
        log.debug("query cntSyncRecord by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize) throws DAOException
    {
        log.debug("page query cntSyncRecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCntSyncRecordListCntByCond", cntSyncRecord)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CntSyncRecord> rsList = (List<CntSyncRecord>) super.pageQuery("queryCntSyncRecordListByCond",
                    cntSyncRecord, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cntSyncRecord by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCntSyncRecordListByCond", "queryCntSyncRecordListCntByCond", cntSyncRecord,
                start, pageSize, puEntity);
    }

}
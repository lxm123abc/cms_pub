package com.zte.ucm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Resultlog
{
  private static StringBuilder sb = new StringBuilder();

  private static int i = 0;

  public void appendLog(String paramString) {
    sb.append(paramString);
    i += 1;
  }

  public static boolean isSucc()
  {
    return i <= 0;
  }

  public void resultLog()
  {
    FileWriter localFileWriter = null;
    File localFile = null;
    String str = Constants.ERRORFILENAME;
    localFile = new File(str);
    if (!localFile.exists())
      try {
        localFile.createNewFile();
      } catch (IOException localIOException1) {
        localIOException1.printStackTrace();
      }
    try
    {
      localFileWriter = new FileWriter(localFile);
      localFileWriter.write(i + "\n");
      localFileWriter.flush();
      localFileWriter.close();
    }
    catch (IOException localIOException2) {
      localIOException2.printStackTrace();
    }
  }
}

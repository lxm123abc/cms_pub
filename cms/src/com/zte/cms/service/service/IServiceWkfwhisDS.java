package com.zte.cms.service.service;

import java.util.List;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceWkfwhisDS
{
    /**
     * 新增ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException;

    /**
     * 更新ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException;

    /**
     * 批量更新ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceWkfwhisList(List<ServiceWkfwhis> serviceWkfwhisList) throws DomainServiceException;

    /**
     * 删除ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException;

    /**
     * 批量删除ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceWkfwhisList(List<ServiceWkfwhis> serviceWkfwhisList) throws DomainServiceException;

    /**
     * 查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @return ServiceWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public ServiceWkfwhis getServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException;

    /**
     * 根据条件查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象
     * @return 满足条件的ServiceWkfwhis对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceWkfwhis> getServiceWkfwhisByCond(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceWkfwhis对象
     * 
     * @param serviceWkfwhis ServiceWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.service.model.ServiceBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceBufDAO
{
    /**
     * 新增ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DAOException dao异常
     */
    public void insertServiceBuf(ServiceBuf serviceBuf) throws DAOException;

    /**
     * 根据主键更新ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DAOException dao异常
     */
    public void updateServiceBuf(ServiceBuf serviceBuf) throws DAOException;

    /**
     * 根据主键删除ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @throws DAOException dao异常
     */
    public void deleteServiceBuf(ServiceBuf serviceBuf) throws DAOException;

    /**
     * 根据主键查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @return 满足条件的ServiceBuf对象
     * @throws DAOException dao异常
     */
    public ServiceBuf getServiceBuf(ServiceBuf serviceBuf) throws DAOException;

    /**
     * 根据条件查询ServiceBuf对象
     * 
     * @param serviceBuf ServiceBuf对象
     * @return 满足条件的ServiceBuf对象集
     * @throws DAOException dao异常
     */
    public List<ServiceBuf> getServiceBufByCond(ServiceBuf serviceBuf) throws DAOException;

    /**
     * 根据条件分页查询ServiceBuf对象，作为查询条件的参数
     * 
     * @param serviceBuf ServiceBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceBuf对象，作为查询条件的参数
     * 
     * @param serviceBuf ServiceBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
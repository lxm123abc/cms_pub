package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.purview.access.model.UcpBasic;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsChannelDS
{
    public UcpBasic getUcpbasicBy(UcpBasic ucpBasic);

    public void insertCmsChannel(CmsChannel cmsChannel) throws DomainServiceException;

    public void updateCmsChannel(CmsChannel cmsChannel) throws DomainServiceException;

    public void removeCmsChannel(CmsChannel cmsChannel) throws DomainServiceException;

    public CmsChannel getCmsChannel(CmsChannel cmsChannel) throws DomainServiceException;
    
    public CmsChannel getCmsChannelById(CmsChannel cmsChannel) throws DomainServiceException;


    public TableDataInfo pageInfoQuery(CmsChannel cmsChannel, int start, int pageSize) throws DomainServiceException;

    public List getChannelByCond(CmsChannel channel);

    public void deleteChannelContent(CmsChannel channel);
}
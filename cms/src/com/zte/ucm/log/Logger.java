package com.zte.ucm.log;

public abstract interface Logger
{
  public static final int NONE = 0;
  public static final int DEBUG = 1;
  public static final int INFO = 2;
  public static final int WARNING = 4;
  public static final int ERROR = 8;
  public static final int ALL = 15;

  public abstract void debug(int paramInt);

  public abstract void debug(String paramString);

  public abstract void debug(Exception paramException);

  public abstract void debug(Object paramObject);

  public abstract void info(int paramInt);

  public abstract void info(String paramString);

  public abstract void info(Exception paramException);

  public abstract void info(Object paramObject);

  public abstract void warning(int paramInt);

  public abstract void warning(String paramString);

  public abstract void warning(Exception paramException);

  public abstract void warning(Object paramObject);

  public abstract void error(int paramInt);

  public abstract void error(String paramString);

  public abstract void error(Exception paramException);

  public abstract void error(Object paramObject);
}
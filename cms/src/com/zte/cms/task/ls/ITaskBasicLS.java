package com.zte.cms.task.ls;

import java.util.List;

import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskCmsauditRecord;
import com.zte.cms.task.model.TaskFunction;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ITaskBasicLS
{
	/**
     * 查询CP信息
     * 
     * @param taskBasic 查询条件对象
     * @param arg1 开始页数
     * @param arg2 条数
     * @return CP信息
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int arg1, int arg2);
    
    public String insertTaskmap(String uploadAccessorialContral, TaskCmsaudit taskBasic) throws DomainServiceException;
    public TaskCmsaudit getTaskmap(Integer taskid);
    public List<TaskCmsaudit> getattchList(Integer taskid);
    public void deleteAttach(String attachid);
    public String updateTaskmap(String uploadAccessorialContral,TaskCmsaudit taskmap) throws DomainServiceException;
    public String deleteTaskBasic(TaskCmsaudit taskmap) throws DomainServiceException;
    public String toAuditTask(TaskCmsaudit taskmap);
    public String toSubTask(TaskCmsaudit taskmap);

    /**
     * 获取3位的角色
     */
    public List<TaskFunction> getTaskFunctList();

    /**
     *  条件查询审批历史记录
     */
    public TableDataInfo pageAuditRecordInfoQuery(TaskCmsauditRecord record, int arg1, int arg2);

    /**
     *  获取3,6位funcid的角色
     */
    public List<TaskFunction> getTaskFunctAllList() throws DomainServiceException;

    /**
     *  导出审批历史记录
     */
    public TaskCmsauditRecord exportAuditRecordByTaskId(TaskCmsauditRecord record);
}

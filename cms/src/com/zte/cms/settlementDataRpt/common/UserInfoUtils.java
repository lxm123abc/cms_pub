package com.zte.cms.settlementDataRpt.common;

import com.zte.cms.common.DbUtil;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;
import com.zte.ucm.util.KwCheck;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: Liangxiaomin
 * @Date Created in 17:54 2019/6/26
 * @Description:
 */
public class UserInfoUtils {

    public static String getOperAllNameByOperId(Integer operid){
        if (operid == null || operid == 0) {
            return "";
        }
        String sql = "select " +
                "operallname " +
                "from zxinsys.oper_information " +
                "where " +
                "operid=" + operid;
        DbUtil db = new DbUtil();
        String operallname = "";
        List rtnlist = null;
        try {
            rtnlist = db.getQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
            rtnlist = new ArrayList();
        }
        for (int i = 0; i < rtnlist.size(); i++) {
            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(i);
            operallname = KwCheck.checkNull(tmpMap.get("operallname"));
            break;
        }
        return operallname;
    }

    /**
     * 通过cpcode 获取Email
     *
     * @param cpCode
     * @return
     * @throws Exception
     */
    public static String getEmailByCpCode(String cpCode){
        if (StringUtils.isEmpty(cpCode)) {
            return "";
        }
        String sql = "select " +
                "email " +
                "from zxinsys.oper_information " +
                "where " +
                "operid=(" +
                "select operatorid from zxdbm_umap.cms_operator_power where cpid='" + cpCode + "'" +
                ")";
        DbUtil db = new DbUtil();
        String emails = "";
        List rtnlist = null;
        try {
            rtnlist = db.getQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
            rtnlist = new ArrayList();
        }
        for (int i = 0; i < rtnlist.size(); i++) {
            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(i);
            emails = KwCheck.checkNull(tmpMap.get("email"));
            break;
        }
        return emails;
    }


    public static String getEmailByCpId(String cpId){
        if (StringUtils.isEmpty(cpId)) {
            return "";
        }
        String sql = "select " +
                "email " +
                "from zxinsys.oper_information " +
                "where " +
                "opername='" + cpId + "'";
        DbUtil db = new DbUtil();
        String emails = "";
        List rtnlist = null;
        try {
            rtnlist = db.getQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
            rtnlist = new ArrayList();
        }
        for (int i = 0; i < rtnlist.size(); i++) {
            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(i);
            emails = KwCheck.checkNull(tmpMap.get("email"));
            break;
        }
        return emails;
    }


    public static boolean checkLoginRole(String userId) {
        //查询该id用户的对象判断是否为pc
        StringBuffer sb = new StringBuffer();
        sb.append("select * from zxinsys.OPER_RIGHTS where OPERID='");
        sb.append(userId);
        sb.append("' and OPERGRPID='1000' and SERVICEKEY='CMS'");
        DbUtil dbutil = new DbUtil();
        List reslist = null;
        try {
            reslist = dbutil.getQuery(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (reslist == null) {
            throw new RuntimeException(EmunBusinessError.NUKNOWN_ERROR.getErrorMsg());
        }
        if (reslist.size() == 0) {
            //CP
            return false;
        } else {
            //管理员
            return true;
        }
    }


}

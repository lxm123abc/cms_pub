package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.IPhysicalchannelDAO;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class PhysicalchannelDAO extends DynamicObjectBaseDao implements IPhysicalchannelDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertPhysicalchannel(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("insert physicalchannel starting...");
        super.insert("insertPhysicalchannel", physicalchannel);
        log.debug("insert physicalchannel end");
    }

    public void updatePhysicalchannel(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("update physicalchannel by pk starting...");
        super.update("updatePhysicalchannel", physicalchannel);
        log.debug("update physicalchannel by pk end");
    }

    public void updatePhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("update physicalchannel by conditions starting...");
        super.update("updatePhysicalchannelByCond", physicalchannel);
        log.debug("update physicalchannel by conditions end");
    }

    public void deletePhysicalchannel(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("delete physicalchannel by pk starting...");
        super.delete("deletePhysicalchannel", physicalchannel);
        log.debug("delete physicalchannel by pk end");
    }

    public void deletePhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("delete physicalchannel by conditions starting...");
        super.delete("deletePhysicalchannelByCond", physicalchannel);
        log.debug("update physicalchannel by conditions end");
    }

    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("query physicalchannel starting...");
        Physicalchannel resultObj = (Physicalchannel) super.queryForObject("getPhysicalchannel", physicalchannel);
        log.debug("query physicalchannel end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Physicalchannel> getPhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException
    {
        log.debug("query physicalchannel by condition starting...");
        List<Physicalchannel> rList = (List<Physicalchannel>) super.queryForList("getPhysicalchannelByCond",
                physicalchannel);
        log.debug("query physicalchannel by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize) throws DAOException
    {
        log.debug("page query physicalchannel by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("pageCntPhysicalchannel", physicalchannel)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Physicalchannel> rsList = (List<Physicalchannel>) super.pageQuery("pageinfoPhysicalchannel",
                    physicalchannel, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query physicalchannel by condition end");
        return pageInfo;
    }

    public List listPhysicalchannelByChannelid(Physicalchannel physicalchannel)
    {
        List list = (List) super.queryForList("pageinfoPhysicalchannel", physicalchannel);
        return list;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryPhysicalchannelListByCond", "queryPhysicalchannelListCntByCond",
                physicalchannel, start, pageSize, puEntity);
    }
    
    //查询新增节目单时选择的物理频道
    public PageInfo pageInfoQueryschedule(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException
    {
        log.debug("page query physicalchannel by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("getPhysicalchannelBySchedulecount", cmsSchedule)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Physicalchannel> rsList = (List<Physicalchannel>) super.pageQuery("getPhysicalchannelBySchedule",
                    cmsSchedule, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query physicalchannel by condition end");
        return pageInfo;
    }
}
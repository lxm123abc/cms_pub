package com.zte.cms.cntSyncTask.service;

import java.util.List;

import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICntSyncTaskDS
{
    /**
     * 新增CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 批量更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncTaskList(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException;

    /**
     * 根据条件更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 根据条件批量更新CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCntSyncTaskListByCond(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException;

    /**
     * 删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 批量删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncTaskList(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException;

    /**
     * 根据条件删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 根据条件批量删除CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCntSyncTaskListByCond(List<CntSyncTask> cntSyncTaskList) throws DomainServiceException;

    /**
     * 查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public CntSyncTask getCntSyncTask(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws DomainServiceException;
    /**
     * 查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return CntSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public List<CntSyncTask> getCntSynhisListBybatchid(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 根据条件查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象
     * @return 满足条件的CntSyncTask对象集
     * @throws DomainServiceException ds异常
     */
    public List<CntSyncTask> getCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DomainServiceException;

    /**
     * 根据条件分页查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    
    /**
     * 根据条件分页查询CntSyncTask对象
     * 
     * @param cntSyncTask CntSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
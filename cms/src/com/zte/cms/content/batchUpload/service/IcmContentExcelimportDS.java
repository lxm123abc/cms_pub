package com.zte.cms.content.batchUpload.service;

import java.util.List;
import com.zte.cms.content.batchUpload.model.IcmContentExcelimport;
import com.zte.cms.content.batchUpload.dao.IIcmContentExcelimportDAO;
import com.zte.cms.content.batchUpload.service.IIcmContentExcelimportDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmContentExcelimportDS implements IIcmContentExcelimportDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmContentExcelimportDAO dao = null;

    public void setDao(IIcmContentExcelimportDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmContentExcelimport(IcmContentExcelimport icmContentExcelimport) throws DomainServiceException
    {
        log.debug("insert icmContentExcelimport starting...");
        try
        {
            dao.insertIcmContentExcelimport(icmContentExcelimport);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmContentExcelimport end");
    }

    public List<IcmContentExcelimport> getIcmContentExcelimportByCond(IcmContentExcelimport icmContentExcelimport)
            throws DomainServiceException
    {
        log.debug("get icmContentExcelimport by condition starting...");
        List<IcmContentExcelimport> rsList = null;
        try
        {
            rsList = dao.getIcmContentExcelimportByCond(icmContentExcelimport);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmContentExcelimport by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get icmContentExcelimport page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmContentExcelimport, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmContentExcelimport>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmContentExcelimport page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get icmContentExcelimport page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmContentExcelimport, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmContentExcelimport>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmContentExcelimport page info by condition end");
        return tableInfo;
    }
}

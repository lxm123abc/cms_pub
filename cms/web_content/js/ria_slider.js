// @version 3.00.04

var _SLIDER = "slider";
var _ZSLIDER = "z:slider";
var SliderArray = [];
var SliderTagArrary = [];

//Slider\u521d\u59cb\u5316\u65b9\u6cd5
function SliderInit(){
	var slidertags=document.getElementsByTagName(_SLIDER);
	if(!slidertags||slidertags.length==0){
		slidertags=document.getElementsByTagName(_ZSLIDER);	
	}
	
	if(!slidertags||slidertags.length==0)
		return;
	var slider={};
	var sliderArray = [];
	for(var i=0;i<slidertags.length;i++){
		slider=new SliderTag(slidertags[i].getAttribute("id"));
		sliderArray[slidertags[i].getAttribute("id")] = slider ;
		slider.init();
	}
	SliderArray = sliderArray;
};

//slider\u6807\u7b7e\u5bf9\u8c61
function SliderTag(id){
	var tagObj=document.getElementById(id);
	var slider={};
	slider = new SliderCtrObj(id);
	if(tagObj.getAttribute("sliderType"))	
		slider.setSliderType(tagObj.getAttribute("sliderType"));
	if(tagObj.getAttribute("min"))
		slider.setMin(parseFloat(tagObj.getAttribute("min")));
	if(tagObj.getAttribute("max"))	
		slider.setMax(parseFloat(tagObj.getAttribute("max")));		
	if(tagObj.getAttribute("steps"))	
		slider.steps = parseInt(tagObj.getAttribute("steps"));
	if(tagObj.getAttribute("decimal"))	
		slider.decimal = parseInt(tagObj.getAttribute("decimal"));
	if(tagObj.getAttribute("length"))	
		slider.setLength(parseFloat(tagObj.getAttribute("length")));
	if(tagObj.getAttribute("bindCtrId"))	
		slider.setBindCtrId(tagObj.getAttribute("bindCtrId"));
	if(tagObj.getAttribute("onchange"))	
		slider.onchange = tagObj.getAttribute("onchange");
	if(tagObj.getAttribute("name"))	
		slider.name = tagObj.getAttribute("name");
	if(tagObj.getAttribute("showlabel"))	
		slider.showlabel = tagObj.getAttribute("showlabel");
	if(tagObj.getAttribute("value"))	
		slider.value = tagObj.getAttribute("value");
	if(tagObj.getAttribute("disabled"))	
		slider.disabled = tagObj.getAttribute("disabled");
	return slider;
};

//slider\u5f53\u524d\u6ed1\u4e2d\u7684\u503c\u5bf9\u8c61
function SliderDisplayObj(id){
	this.id = id;
	this.dec = "";
	this.value = [];
	
};
//Slider\u5bf9\u8c61
function SliderCtrObj(id){
	this.id = id;
	this.sliderType = "single";
	this.min = "";
	this.max = "";
	this.length = 130;
	this.decimal = 0;
	this.steps = 0;
	this.bindCtrId = "";
	this.onchange = null;
	this.showlabel =null;
	this.name=id;
	this.value="";
	this.disabled = "false";
	
	this.sliderObj=null;
	this.displayTitleObj=null;
	this.displayObj = new SliderDisplayObj(id);
	this.mouseover = true;
	this.mousemove = false;
	//add
	this.pxLeft = "";
	this.pxTop = "";
	this.xCoord = "";
	this.yCoord = "";
	this.v = "";
	this.yMax = "";
	this.xMax = "";
	this.xMin = "";
	this.yMin = "";
	this.position = 0;
	
	var sliderPairs={
		slider1:0,
		slider2:1439,
		old1:0,
		old2:1439,
		sliderprice:20,
		sliderpriceold:40,
		changed:function(){
			return !((sliderPairs.slider1==sliderPairs.old1&&sliderPairs.slider2==sliderPairs.old2)||(sliderPairs.slider2==sliderPairs.old1&&sliderPairs.slider1==sliderPairs.old2));
		},
		pricechanged:function(){
			return sliderPairs.sliderpriceOld!=sliderPairs.sliderpriceold;
		},
		Sync:function(){

		},
		PriceSync:function(){
			sliderPairs.sliderpriceold=sliderPairs.sliderprice;
		}
	};
	
	var me=this;
	
	//document\u7684onmousemove\u4e8b\u4ef6
	this.moveSlider = function moveSlider(evnt)
	{//debugger;
		var evnt = (!evnt) ? window.event : evnt; 
		if (me.mouseover) { 
			var x = me.pxLeft + evnt.screenX - me.xCoord; 
			var y = me.pxTop + evnt.screenY - me.yCoord ;
			if (x > me.xMax) x = me.xMax ;
			if (x < me.xMin) x = me.xMin ;
			if (y > me.yMax) y = me.yMax ;
			if (y < me.yMin) y = me.yMin ;
			SliderCtrObj.carpeLeft(me.sliderObj.id, x) ; 
			SliderCtrObj.carpeTop(me.sliderObj.id, y) ;
			var sliderVal = x + y ;
			var sliderPos = (me.sliderObj.pxLen / me.sliderObj.valCount) * Math.round(me.sliderObj.valCount * sliderVal / me.sliderObj.pxLen);
			me.v = Math.round((sliderPos * me.sliderObj.scale + me.sliderObj.fromVal) * 
				Math.pow(10, me.displayObj.dec)) / Math.pow(10, me.displayObj.dec);
				
			me.displayObj.value[me.position] = me.v ;
			
			//add
			//me.displayValue();
			
			var SliderStyle = SliderCtrObj.GetObjsPos(me.sliderObj.LeftObj,me.sliderObj.RightObj);	
			var filterObj = SliderCtrObj.carpeGetElementById(me.sliderObj.filterId);
			
			
			filterObj.style.left = SliderStyle.left;
			filterObj.style.width = SliderStyle.width;
			
			me.mousemove=true;
			
			//\u589e\u52a0\u4e8b\u4ef6
			eval(me.onchange);
			return true;
		}
		me.mousemove = false;
		
		//\u589e\u52a0\u4e8b\u4ef6
		eval(me.onchange);
		return ;
	};
	
	//slider\u7684onmousedown\u4e8b\u4ef6
	this.slide = function slide(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position)
	{
		if (!evnt) evnt = window.event;
		
		//\u8bbe\u7f6e\u6240\u6709\u7684\u5c5e\u6027
		me.setAllAttributes(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position);
				
		//\u8bbe\u7f6e\u6240\u9700\u7684\u9f20\u6807\u4e8b\u4ef6
		if(me.disabled == "false")
			me.setMouseEvent();
		
	};
	
	//document\u7684onmouseup\u4e8b\u4ef6
	this.sliderMouseUp = function sliderMouseUp()
	{//debugger;
		me.mouseover = false; 
		if (!me.mousemove) return;
	
		me.v = (me.displayObj.value[me.position]) ? me.displayObj.value[me.position] : 0; // Find last display value.
		if ( me.sliderObj.scale==0 ) return;
		pos = (me.v - me.sliderObj.fromVal)/(me.sliderObj.scale); 
		if (me.yMax == 0) SliderCtrObj.carpeLeft(me.sliderObj.id, pos);
		if (me.xMax == 0) SliderCtrObj.carpeTop(me.sliderObj.id, pos); 
		
		me.removeMouseEvent();
	
		// firsttrip secondtrip
		if (me.sliderObj.LeftObj==null || me.sliderObj.id==me.sliderObj.LeftObj.id)
			sliderPairs.slider1 = me.v;
		else
			sliderPairs.slider2= me.v;
	
		if ( sliderPairs.changed() )
		{
			//alert(sliderPairs.slider1 + ";" + sliderPairs.slider2);
		//	RepeatedFilter(slidertype, sliderPairs.slider1, sliderPairs.slider2);
		//	sliderPairs.Sync();
		}
	};
	
	
	//\u5728\u7ed9\u5b9a\u7684\u63a7\u4ef6\u4e2d\u663e\u793a\u503c
	this.displayValue = function displayValue(){
		if (me.displayTitleObj!=null){
				
				if ( me.sliderType == "time")
				{
					var title=me.displayTitleObj.innerHTML.split(' - ');
					var changedIndex = me.position-1;
					var time = parseInt(me.displayObj.value[me.position],10);	
					title[changedIndex]=SliderCtrObj.leftpadstring(parseInt(time/60,10),"0",2) + ":" + SliderCtrObj.leftpadstring(parseInt(time%60,10),"0",2);
					me.displayTitleObj.innerHTML=title.join(' - ');
				}
				else
				{
					var title=me.displayTitleObj.innerHTML.split('- ');
					title[title.length-1]="" + me.displayObj.value[me.position];
					me.displayTitleObj.innerHTML=title.join('- ');
				}
			}
	};

	//\u8bbe\u7f6e\u6240\u6709\u5c5e\u6027
	this.setAllAttributes = function setAllAttributes(evnt, orientation, length, from, to, count, decimal,  filter,LeftObjId,RightObjId, TitleId,position)
	{
		me.position = position;
		me.sliderObj = (evnt.target) ? evnt.target : evnt.srcElement; 
		me.sliderObj.pxLen = length ;
		me.sliderObj.valCount = count ? count : (to - from); 
		me.sliderObj.scale = (to - from) / length; 
		me.sliderObj.filterId = filter;
		me.sliderObj.LeftObj = SliderCtrObj.carpeGetElementById(LeftObjId); 
		me.sliderObj.RightObj = SliderCtrObj.carpeGetElementById(RightObjId); 
		
		me.displayObj.dec = decimal;		
		if(TitleId != "")
			me.displayTitleObj = SliderCtrObj.carpeGetElementById(TitleId);
		if ( true || orientation == 'horizontal') { 
			me.sliderObj.fromVal = from;
			me.xMax = length;
			me.yMax = 0;
			me.xMin = me.yMin = 0;
			
			var SliderStyle = SliderCtrObj.GetObjsPos(me.sliderObj.LeftObj,me.sliderObj.RightObj);
			
			if ( me.sliderObj.LeftObj!=null && me.sliderObj.LeftObj.id==me.sliderObj.id )
				me.xMax = SliderStyle.left + SliderStyle.width - parseInt(me.sliderObj.style.width);
			else
				me.xMin = SliderStyle.left;		
		}
		
		me.pxLeft = SliderCtrObj.carpeLeft(me.sliderObj.id); 
		me.pxTop  = SliderCtrObj.carpeTop(me.sliderObj.id); 
		me.xCoord = evnt.screenX;
		me.yCoord = evnt.screenY; 
		me.mouseover = true;
		me.mousemove = false;
	};
		
	//\u8bbe\u7f6e\u6240\u9700\u7684\u9f20\u6807\u4e8b\u4ef6
	this.setMouseEvent = function setMouseEvent()
	{		
		document.onmousemove = me.moveSlider; 
		document.onmouseup = me.sliderMouseUp; 
	};
	
	//\u79fb\u9664\u9f20\u6807\u4e8b\u4ef6
	this.removeMouseEvent = function removeMouseEvent()
	{
		if (document.removeEventListener) { 		
			document.removeEventListener('mousemove', me.moveSlider, true);		
			document.removeEventListener('mouseup', me.sliderMouseUp, true);
		}
		else if (document.detachEvent) { 	
			document.detachEvent('onmousemove', me.moveSlider);
			document.detachEvent('onmouseup', me.sliderMouseUp);
		}	
	};
	
		
	//\u5f97\u5230slider\u7684\u5f53\u524d\u503c
	this.getDataFromCtrl = function getDataFromCtrl(pos){
		if(pos == "left")
			return me.displayObj.value[1];
		else if(pos == "right")
			return me.displayObj.value[2];
		else
		 	return me.displayObj.value[0];
	};
	
	//\u7ed9slider\u8bbe\u503c
	this.bindDataToCtrl = function bindDataToCtrl(value,pos){
		if(pos == "left")
			me.displayObj.value[1] = value;
		else if(pos == "right")
			me.displayObj.value[2] = value;
		else
		 	me.displayObj.value[0] = value;
	
	};
	
	//\u5f97\u5230\u6574\u4e2aSlider\u5bf9\u8c61
	this.getSliderObj = function getSliderObj(){
		return me.sliderObj;
	};
};

SliderCtrObj.prototype={
	createDisplayElem:function(){
		var label = document.createElement("span");
		label.innerHTML = "";
		document.getElementById(this.id).appendChild(label);
		this.displayTitleObj = label;
	},
	setSingleSliderValue:function(value){
			if(parseFloat(value)<this.min )
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u8bbe\u7f6e\u7684value\u5c0f\u4e8e\u6700\u5c0f\u503c\uff0c\u8bf7\u4fee\u6539!");
				return;
			}
			else if( parseFloat(value)>this.max)
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u8bbe\u7f6e\u7684value\u5927\u4e8e\u6700\u5927\u503c\uff0c\u8bf7\u4fee\u6539!");
				return;
			}
				
				var val = parseFloat(value);
				this.displayObj.value[0] = val;
				
				var horizontal_slider_id = document.getElementById("horizontal_slider_single_"+this.id);
				var colorBar_id = document.getElementById("colorBar_"+this.id);
				var horizontal_slider_id_left = horizontal_slider_id.style.left;
				var colorBar_id_width = colorBar_id.style.width;
				
				horizontal_slider_id.style.left = ((val-this.min)/(this.max-this.min))*parseFloat(horizontal_slider_id_left)+"px";
							
				colorBar_id.style.width = (((val-this.min)/(this.max-this.min))*parseFloat(horizontal_slider_id_left+5))+"px";
	},
	setDoubleSliderValue:function(value){
		var val = [];
		
		if( value instanceof Array)
		{
			val[0] = parseFloat(value[0]);
			val[1] = parseFloat(value[1]);
			if(parseInt(val[0])>parseInt(val[1]))
			{
				alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684value\u503c\u6709\u8bef\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
				return;
			}			
		}
		else
		{			
			if(typeof value =="string" && value.indexOf(",") != -1)
			{			
				var v = value.split(",");
				val[0] = parseFloat(v[0]);
				val[1] = parseFloat(v[1]);			
				if(v.length>2 || parseFloat(v[0])>parseFloat(v[1]))
				{
					alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684value\u503c\u6709\u8bef\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
					return;
				}
			}
			else
				return;			
		}
				this.displayObj.value[1] = val[0];
				this.displayObj.value[2] = val[1];
				var horizontal_slider_id_1 = document.getElementById("horizontal_slider_double_"+this.id+"_1");
				var horizontal_slider_id_2 = document.getElementById("horizontal_slider_double_"+this.id+"_2");
				var colorBar_id = document.getElementById("colorBar_"+this.id);
				var horizontal_slider_id_1_left = horizontal_slider_id_1.style.left;
				var horizontal_slider_id_2_left = horizontal_slider_id_2.style.left;
							
				//\u8bbe\u7f6eslider\u7684\u5de6\u53f3\u4e24\u8fb9\u7684\u4f4d\u7f6e
				horizontal_slider_id_1.style.left = ((v[0]-this.min)/(this.max-this.min))*parseInt(this.length)+"px";						
				horizontal_slider_id_2.style.left = ((v[1]-this.min)/(this.max-this.min))*parseInt(this.length)+"px";
							
				colorBar_id.style.left = horizontal_slider_id_1.style.left;
				colorBar_id.style.width = parseInt(horizontal_slider_id_2.style.left)-parseInt(horizontal_slider_id_1.style.left)+"px";
		
				
	},
	setSliderValue:function(value){
		if(!value)
			return;
			
		this.value = value;
		
		if(this.sliderType =="single")
		{
			this.setSingleSliderValue(value);
		}
		else if(this.sliderType =="double")
		{
			this.setDoubleSliderValue(value);
		}
						
	},
	setSingleSliderPosition:function(){
			var horizontal_track_id = document.getElementById("horizontal_track_"+this.id);
			var horizontal_slit_id = document.getElementById("horizontal_slit_"+this.id);
			var horizontal_slider_id = document.getElementById("horizontal_slider_single_"+this.id);
			var colorBar_id = document.getElementById("colorBar_"+this.id);
			
			horizontal_track_id.style.width = (parseFloat(this.length)+10)+"px";
			
			horizontal_slit_id.style.width = (parseFloat(this.length)+10)+"px";
			horizontal_slit_id.style.clip = 'rect(0px '+(parseFloat(this.length)+10)+'px 26px 0px)';
			
			//\u8bbe\u7f6eslider\u7684\u4f4d\u7f6e
			horizontal_slider_id.style.left = this.length+"px";
						
			colorBar_id.style.width = (parseFloat(this.length)+5)+"px";
			
			this.displayObj.value[0] = this.max;
	},
	createSingleSlider:function(){
			//\u663e\u793alabel
			eval(this.showlabel);
			
			/*if(this.bindCtrId =="")
				this.createDisplayElem();*/
				
			var sliderDiv = document.createElement("div");
			sliderDiv.id = "pad20_"+this.id;
			sliderDiv.className = "pad20";
			var horizontal_track_id = "horizontal_track_"+this.id;
			var horizontal_slit_id = "horizontal_slit_"+this.id;
			var horizontal_slider_id = "horizontal_slider_single_"+this.id;
			var colorBar_id = "colorBar_"+this.id;
			var sliderHTML = '<div class="horizontal_track" id="'+horizontal_track_id+'">'+
							'<div class="horizontal_slit"  id="'+horizontal_slit_id+'">&nbsp;</div>'+
	      					'<div class="horizontal_slider_single" id="'+horizontal_slider_id +'" style="Z-Index:906;left:130px;" '+ 
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\'\',\''+horizontal_slider_id+'\',\''+this.bindCtrId+'\',0);"></div>'+
	      					'<div class="colorBar" id="'+colorBar_id +'" ></div></div>';
			sliderDiv.innerHTML = sliderHTML;
			document.getElementById(this.id).appendChild(sliderDiv);
			
			//add
			this.setSingleSliderPosition();
			//add
			this.setSliderValue(this.value);
	},
	setDoubleSliderPosition:function(){
			var horizontal_track_id = document.getElementById("horizontal_track_"+this.id);
			var horizontal_slit_id = document.getElementById("horizontal_slit_"+this.id);
			var horizontal_slider_id_1 = document.getElementById("horizontal_slider_double_"+this.id+"_1");
			var horizontal_slider_id_2 = document.getElementById("horizontal_slider_double_"+this.id+"_2");
			var colorBar_id = document.getElementById("colorBar_"+this.id);
			
			horizontal_track_id.style.width = (parseFloat(this.length)+10)+"px";
			
			horizontal_slit_id.style.width = (parseFloat(this.length)+10)+"px";
			horizontal_slit_id.style.clip = 'rect(0px '+(parseFloat(this.length)+10)+'px 26px 0px)';
			
			//\u8bbe\u7f6eslider\u7684\u5de6\u53f3\u4e24\u8fb9\u7684\u4f4d\u7f6e
			horizontal_slider_id_1.style.left = 0+"px";						
			horizontal_slider_id_2.style.left = this.length+"px";
						
			colorBar_id.style.width = (parseFloat(this.length)+5)+"px";
						
			this.displayObj.value[1] = this.min;
			this.displayObj.value[2] = this.max;
	},
	createDoubleSlider:function(){
			//\u663e\u793alabel
			eval(this.showlabel);
			
			/*if(this.bindCtrId =="")
				this.createDisplayElem();*/
				
			var sliderDiv = document.createElement("div");
			sliderDiv.id = "pad20_"+this.id;
			sliderDiv.className = "pad20";
			var horizontal_track_id = "horizontal_track_"+this.id;
			var horizontal_slit_id = "horizontal_slit_"+this.id;
			var horizontal_slider_id_1 = "horizontal_slider_double_"+this.id+"_1";
			var horizontal_slider_id_2 = "horizontal_slider_double_"+this.id+"_2";
			var colorBar_id = "colorBar_"+this.id;
			var sliderHTML = '<div class="horizontal_track" id="'+horizontal_track_id+'">'+
							'<div class="horizontal_slit" id="'+horizontal_slit_id+'" >&nbsp;</div>'+
	      					'<div class="horizontal_slider_double" id="'+horizontal_slider_id_1 +'" style="Z-Index:908;Top:0px;Left:0px;WIDTH:10px" '+ 
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\''+horizontal_slider_id_1+'\',\''+horizontal_slider_id_2+'\',\''+this.bindCtrId+'\',1);"></div>'+
	      					'<div class="colorBar" id="'+colorBar_id +'" ></div>'+
	      					'<div class="horizontal_slider_double" id="'+horizontal_slider_id_2 +'" style="Z-Index:908;Top:0px;Left:130px;WIDTH:10px" '+ 	
		  					'onmousedown="SliderArray[\''+this.id+'\'].slide(event, \'horizontal\', '+this.length+', '+this.min+', '+this.max+', '+this.steps+', '+this.decimal+',\''+colorBar_id+'\',\''+horizontal_slider_id_1+'\',\''+horizontal_slider_id_2+'\',\''+this.bindCtrId+'\',2);"></div></div>';
			sliderDiv.innerHTML = sliderHTML;
			document.getElementById(this.id).appendChild(sliderDiv);
			
			//add
			this.setDoubleSliderPosition();
			//add
			this.setSliderValue(this.value);
	},
	init:function( ){	
		if(this.min >= this.max)
		{
			alert("id\u4e3a\""+this.id+"\"\u7684Slider\u6807\u7b7e\u7684\u6700\u5c0f\u503c\u5927\u4e8e\u6216\u7b49\u4e8e\u6700\u5927\u503c\uff0c\u8bf7\u91cd\u65b0\u8bbe\u7f6e!");
			return;
		}
		
		if(this.sliderType =="single"){

			this.createSingleSlider();
		}
		else if(this.sliderType =="double"){	
			this.createDoubleSlider();		
		}
	},
	setSliderType:function(sliderType){
		this.sliderType=sliderType||this.sliderType;
	},
	setMin:function(min){
		this.min = min;
	},
	setMax:function(max){
		this.max=max;
	},
	setLength:function(length){
		this.length=length;
	},
	setBindCtrId:function(bindCtrId){
		this.bindCtrId=bindCtrId;
	}
};

//\u4eceslider\u4e2d\u5f97\u5230\u503c
SliderCtrObj.getValueFromSlider = function getValueFromSlider(id,pos){
	
	return SliderArray[id].getDataFromCtrl(pos);

};

//\u8fd4\u56deslider\u5f53\u524d\u7684\u503c\u5bf9\u8c61
SliderCtrObj.getValueObjFromSlider = function getValueObjFromSlider(id){
	if(SliderArray[id].sliderType ==  "single")
		return SliderArray[id].displayObj.value[0];
	else if(SliderArray[id].sliderType ==  "double")
	{
		var val =[SliderArray[id].displayObj.value[1],SliderArray[id].displayObj.value[2]];
		return val;
	}

};

SliderCtrObj.setValueObjFromSlider = function setValueObjFromSlider(id,val){
	if( val instanceof Array)
	{
		SliderArray[id].displayObj.value[1] = val[0];
		SliderArray[id].displayObj.value[2] = val[1];	
		SliderArray[id].setDoubleSliderPosition();
	}
	else
	{
		SliderArray[id].displayObj.value[0] = val;
		SliderArray[id].setSingleSliderPosition();
	}
	
	SliderArray[id].setSliderValue(val);
};

//\u5411slider\u8bbe\u503c
SliderCtrObj.setValueToSlider = function setValueToSlider(id,value,pos){

	SliderArray[id].bindDataToCtrl(value,pos);
};

//\u6839\u636e\u6307\u5b9aid\u5f97\u5230\u5143\u7d20\u6216\u5143\u7d20\u7ec4
SliderCtrObj.carpeGetElementById = function carpeGetElementById(element)
{
	if (document.getElementById) element = document.getElementById(element);
	else if (document.all) element = document.all[element];
	else element = null;
	return element;
};

//
SliderCtrObj.leftpadstring = function leftpadstring(sourcestring,padchar,length)
{
	var returnstring = sourcestring;
	for (var index=(""+sourcestring).length; index<length; index++ )
	{
		returnstring = padchar + returnstring;
	}
	return returnstring;
};

//\u5f97\u5230\u5143\u7d20\u7684\u4f4d\u7f6e
SliderCtrObj.GetObjsPos = function GetObjsPos(obj1, obj2)
{
	var left,width;
	
	left = 0;

	left = obj1==null?0:parseInt(obj1.style.left,10) + parseInt(obj1.style.width==""?0:obj1.style.width,10) ;
	
	width =obj2==null?0:parseInt(obj2.style.left,10)-left;
	
	if ( left-5>0) left = left-5;
	width = width + 5;
	
	return {left:left,width:width};
};

//\u5f97\u5230\u5143\u7d20\u7684left
SliderCtrObj.carpeLeft = function carpeLeft(elmnt, pos)
{
	if (!(elmnt = SliderCtrObj.carpeGetElementById(elmnt))) return 0;
	if (elmnt.style && (typeof(elmnt.style.left) == 'string')) {
		if (typeof(pos) == 'number') elmnt.style.left = pos + 'px';
		else {
			pos = parseInt(elmnt.style.left);
			if (isNaN(pos)) pos = 0;
		}
	}
	else if (elmnt.style && elmnt.style.pixelLeft) {
		if (typeof(pos) == 'number') elmnt.style.pixelLeft = pos;
		else pos = elmnt.style.pixelLeft;
	}
	return pos;
};

//\u5f97\u5230\u5143\u7d20\u7684top
SliderCtrObj.carpeTop = function carpeTop(elmnt, pos)
{
	if (!(elmnt = SliderCtrObj.carpeGetElementById(elmnt))) return 0;
	if (elmnt.style && (typeof(elmnt.style.top) == 'string')) {
		if (typeof(pos) == 'number') elmnt.style.top = pos + 'px';
		else {
			pos = parseInt(elmnt.style.top);
			if (isNaN(pos)) pos = 0;
		}
	}
	else if (elmnt.style && elmnt.style.pixelTop) {
		if (typeof(pos) == 'number') elmnt.style.pixelTop = pos;
		else pos = elmnt.style.pixelTop;
	}
	return pos;
};

addOnloadEvent(SliderInit);

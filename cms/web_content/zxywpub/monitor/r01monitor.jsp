<%@page language="java" contentType="text/html"%>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="com.zte.jos.monitor.build.*" %>
<%@ page import="com.zte.jos.monitor.util.*" %>

<%!
  private String timedsp(long ltime){
    Calendar cal=Calendar.getInstance();
    String sTime = "Free";
    cal.setTimeInMillis(ltime);
    if(ltime>0L){
      sTime = "" + cal.get(Calendar.YEAR) +"."+ (cal.get(Calendar.MONTH)+1);
      sTime = sTime +"."+cal.get(Calendar.DAY_OF_MONTH);
      sTime = sTime +" "+cal.get(Calendar.HOUR_OF_DAY) +":"+cal.get(Calendar.MINUTE);
      sTime = sTime +":"+cal.get(Calendar.SECOND);
    }
    return sTime;
  }
%>

<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
String Strmsg="";
int cmdint=0;
if(request.getParameter("cmd")!=null) {
	cmdint=java.lang.Integer.parseInt(request.getParameter("cmd"));
}
String Remoteoper=(String)session.getAttribute("OPERNAME");
String servletPath = request.getServletPath();
String rootPath = (servletPath.split("zxywpub"))[0];
if(Remoteoper==null){
    if(cmdint>0){
%>
		<script language="javascript1.2">
		alert("Please login first.");
		top.location.href = ' <%=rootPath%>' + "zxywpub";
		</script>
<%
        cmdint=0;
    }
}
%><!-- JSP programs end -->

<HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<TITLE>R01 monitor</TITLE>
<link REL="STYLESHEET" HREF="../inc/style.css" TYPE="text/css">
<style type="text/css">
TD.entryname
{
	background-color:#cceeff;
}
</style>
</HEAD>
<BODY topmargin="0" leftmargin="0" style="background-color: rgb(249,250,244)">
<!-- Navigator bar -->

<!-- Navigator bar end -->
<table border="0" cellPadding="5" cellSpacing="1" width="100%" height="95%" bgcolor="#C0C0C0">
   <tr height="95%">
      <td vAlign="top" bgcolor="#F9FAF4" background="../images/bg01.jpg">
<!-- Main display info -->
<%
	if (cmdint == 99) {
	String Refreshtime = "5";
	if (request.getParameter("refreshtime") != null) {
	    Refreshtime = request.getParameter("refreshtime");
	}
%>
<form method="post" name="inForm" action="r01monitor.jsp">
<input type="hidden" name="cmd" value="<%=cmdint%>">
<div align="center"><center>
  <table border="0" width="400" cellspacing="0" cellpadding="2" style="font-size: 9pt">
    <tr><td><select name="refreshtime"="1">
		<OPTION value='3'>3s</OPTION>
		<OPTION value='5'>5s</OPTION>
		<OPTION value='10'>10s</OPTION>
		<OPTION value='15'>15s</OPTION>
		<OPTION value='30'>30s</OPTION>
		<OPTION value='60'>60s</OPTION>
		</select></td>
      <td align="right"><input type="button" onclick="renew()" value="Refresh" name="B11" tabindex="91"></td>
    </tr>
  </table>
  <hr size="1" width="500">

	<table border='1' width='540' cellspacing='0' cellpadding='2' style='font-size:9pt'>
<%
        R01Build _build = new R01Build();
		boolean _isModuleStart = _build.isR01Start();
		if(_isModuleStart == false) {
%>
		<tr> 
			<td class="entryname">WARN</td>
			<td>R01 module not start.</td>
		</tr>
<%
		}
		else {
		    DynamicBean _bean = _build.collectMonitorEntrys();
%>
		<tr> 
			<td class="entryname">MaxSocketNum</td>
			<td><%=_bean.getProperty("MaxSocketNum")%></td>
		</tr>
		<tr>
			<td class="entryname">SocketNum</td>
			<td><%=_bean.getProperty("MaxSocketNum")%></td>
		</tr>
		<tr>	
			<td class="entryname">SocketStartTimes</td>
			<td><%=_bean.getProperty("SocketStartTimes")%></td>
		</tr>
		<tr>
			<td class="entryname">TotalSendSucessMsgNum</td>
			<td><%=_bean.getProperty("TotalSendSucessMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleSendSucessMsgNum</td>
			<td><%=_bean.getProperty("CycleSendSucessMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">TotalSendFailedMsgNum</td>
			<td><%=_bean.getProperty("TotalSendFailedMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleSendFailedMsgNum</td>
			<td><%=_bean.getProperty("CycleSendFailedMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleSendFailedMsgs</td>
			<td><%=_bean.getProperty("CycleSendFailedMsgs")%></td>
		</tr>
		<tr>
			<td class="entryname">TotalReceiveSucessMsgNum</td>
			<td><%=_bean.getProperty("TotalReceiveSucessMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleReceiveSucessMsgNum</td>
			<td><%=_bean.getProperty("CycleReceiveSucessMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">TotalReceiveFailedMsgNum</td>
			<td><%=_bean.getProperty("TotalReceiveFailedMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleReceiveFailedMsgNum</td>
			<td><%=_bean.getProperty("CycleReceiveFailedMsgNum")%></td>
		</tr>
		<tr>
			<td class="entryname">TotalSocketBrokeNum</td>
			<td><%=_bean.getProperty("TotalSocketBrokeNum")%></td>
		</tr>
		<tr>
			<td class="entryname">CycleSocketBrokeNum</td>
			<td><%=_bean.getProperty("CycleSocketBrokeNum")%></td>
		</tr>            
<%
		}
%>
	</table>
	</center>
</div>
</form>

<script language="JavaScript1.2">
	function renew(){
		document.inForm.submit();
	}

	document.inForm.refreshtime.value="<%=Refreshtime%>";
	setTimeout('renew()',<%=Refreshtime%>*1000);
</script>
<%
}
%>
<!-- Main display info end -->
      </td>
   </tr>
<!-- Messages -->
   <tr height="5%"><td bgcolor="#f4f9fa" align="right"><%=Strmsg%></td></tr>
<!-- Messages end -->
</table>
</BODY>
</HTML>

package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.Picture;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IPictureDS
{
    /**
     * 新增Picture对象
     * 
     * @param picture Picture对象
     * @throws DomainServiceException ds异常
     */
    public void insertPicture(Picture picture) throws DomainServiceException;

    /**
     * 更新Picture对象
     * 
     * @param picture Picture对象
     * @throws DomainServiceException ds异常
     */
    public void updatePicture(Picture picture) throws DomainServiceException;

    /**
     * 批量更新Picture对象
     * 
     * @param picture Picture对象
     * @throws DomainServiceException ds异常
     */
    public void updatePictureList(List<Picture> pictureList) throws DomainServiceException;

    /**
     * 删除Picture对象
     * 
     * @param picture Picture对象
     * @throws DomainServiceException ds异常
     */
    public void removePicture(Picture picture) throws DomainServiceException;

    /**
     * 批量删除Picture对象
     * 
     * @param picture Picture对象
     * @throws DomainServiceException ds异常
     */
    public void removePictureList(List<Picture> pictureList) throws DomainServiceException;

    /**
     * 查询Picture对象
     * 
     * @param picture Picture对象
     * @return Picture对象
     * @throws DomainServiceException ds异常
     */
    public Picture getPicture(Picture picture) throws DomainServiceException;

    /**
     * 根据条件查询Picture对象
     * 
     * @param picture Picture对象
     * @return 满足条件的Picture对象集
     * @throws DomainServiceException ds异常
     */
    public List<Picture> getPictureByCond(Picture picture) throws DomainServiceException;

    /**
     * 根据条件分页查询Picture对象
     * 
     * @param picture Picture对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Picture picture, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Picture对象
     * 
     * @param picture Picture对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Picture picture, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件分页查询Picture对象
     * 
     * @param picture Picture对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询Picture对象
     * 
     * @param picture Picture对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public List<Picture> getPictureByServiceIndex(Long index) throws DomainServiceException;

    public List<Picture> getPictureByCatagoryIndex(Long index) throws DomainServiceException;

    public List<Picture> getPictureByProgramIndex(Long index) throws DomainServiceException;

    public List<Picture> getPictureBySeriesIndex(Long index) throws DomainServiceException;

    public List<Picture> getPictureByChannelIndex(Long index) throws DomainServiceException;

    public List<Picture> getPictureByCastIndex(Long index) throws DomainServiceException;

    public List<Picture> queryChannelPictureListByChnanelindex(Long index) throws DomainServiceException;
}

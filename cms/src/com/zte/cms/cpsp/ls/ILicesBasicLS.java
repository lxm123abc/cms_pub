package com.zte.cms.cpsp.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.model.UcpContact;

public interface ILicesBasicLS
{
    /**
     * 新增UcpBasic对象
     * 
     * @param ucpBasic UcpBasic对象
     * @throws DomainServiceException ds异常
     */
    public String insertUcpBasic(UcpBasic ucpBasic, List<UcpContact> ucpContactList) throws DomainServiceException;

    /**
     * 查询UcpBasic对象
     * 
     * @param ucpBasic UcpBasic对象
     * @return UcpBasic对象
     * @throws DomainServiceException ds异常
     */
    public UcpBasic getUcpBasic(UcpBasic ucpBasic) throws DomainServiceException;

    /**
     * 根据条件查询UcpBasic对象
     * 
     * @param ucpBasic UcpBasic对象
     * @return 满足条件的UcpBasic对象集
     * @throws DomainServiceException ds异常
     */
    public List<UcpBasic> getUcpBasicByCond(UcpBasic ucpBasic) throws DomainServiceException;

    /**
     * 根据牌照方ID查询牌照方信心
     * 
     * @param 牌照方ID
     * @return 牌照方对象信息
     */
    public UcpBasic getUcpBasicByIndex(Long cpIndex) throws DomainServiceException;

    /**
     * 根据牌照方查询联系人
     * 
     * @param ucpBasic UcpBasic对象信息
     * @return 相似的UcpBasic
     */
    public List<UcpContact> getUcpContactList(UcpBasic ucpBasic) throws DomainServiceException;

    /**
     * 根据UcpBasic对象的信息删除相似的Ucp对象，同时把相关联的联系人信息删除掉
     * 
     * @param ucpBasic UcpBasic对象
     * @return 删除返回码
     */
    public String removeLicesBasicByCond(UcpBasic ucpBasic) throws DomainServiceException;

    /**
     * 根据前台传入的Mode关键字判断是添加动作还是更新动作
     * 
     * @param ucpBaice 牌照方对象信息
     * @param ucpContactList 联系人列表
     * @param mode 动作标识（add|modify）
     * @return 操作结果返回值
     */
    public String applyDataByMode(UcpBasic ucpBasic, List<UcpContact> ucpContactList, String mode)
            throws DomainServiceException;

    /**
     * 
     * 更新牌照方信息
     * 
     * @param ucpBasic 牌照方对象信息
     * @param ucpContactList 联系人列表
     * @return 操作返回值
     */
    public String updateUcpBasice(UcpBasic ucpBasic, List<UcpContact> ucpContactList) throws DomainServiceException;

    /**
     * 返回区域属性的值对象列表
     * 
     * @return 返回值对象列表
     */
    public HashMap getUsysProvinceMap();

    /**
     * 根据UcpBasic对象查询Ucp列表。
     * 
     * @param ucpBasic UcpBasic对象
     * @return 返回Ucp列表
     */
    public TableDataInfo pageInfoQueryForTable(UcpBasic ucpBasic, int start, int pageSize)
            throws DomainServiceException;

    public String pauseAndRework(String cpindex, String opertype);

    /**
     * 验证CP下是否有操作员
     */
    public String validOper(String cpindex);
}

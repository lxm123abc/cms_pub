
  package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.ICmsScheduleBufDAO;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsScheduleBufDAO extends DynamicObjectBaseDao implements ICmsScheduleBufDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException
    {
    	log.debug("insert cmsScheduleBuf starting...");
    	super.insert( "insertCmsScheduleBuf", cmsScheduleBuf );
    	log.debug("insert cmsScheduleBuf end");
    }
     
    public void updateCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException
    {
    	log.debug("update cmsScheduleBuf by pk starting...");
       	super.update( "updateCmsScheduleBuf", cmsScheduleBuf );
       	log.debug("update cmsScheduleBuf by pk end");
    }


    public void deleteCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException
    {
    	log.debug("delete cmsScheduleBuf by pk starting...");
       	super.delete( "deleteCmsScheduleBuf", cmsScheduleBuf );
       	log.debug("delete cmsScheduleBuf by pk end");
    }


    public CmsScheduleBuf getCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DAOException
    {
    	log.debug("query cmsScheduleBuf starting...");
       	CmsScheduleBuf resultObj = (CmsScheduleBuf)super.queryForObject( "getCmsScheduleBuf",cmsScheduleBuf);
       	log.debug("query cmsScheduleBuf end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<CmsScheduleBuf> getCmsScheduleBufByCond( CmsScheduleBuf cmsScheduleBuf )throws DAOException
    {
    	log.debug("query cmsScheduleBuf by condition starting...");
    	List<CmsScheduleBuf> rList = (List<CmsScheduleBuf>)super.queryForList( "queryCmsScheduleBufListByCond",cmsScheduleBuf);
    	log.debug("query cmsScheduleBuf by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cmsScheduleBuf by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryCmsScheduleBufListCntByCond",  cmsScheduleBuf)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CmsScheduleBuf> rsList = (List<CmsScheduleBuf>)super.pageQuery( "queryCmsScheduleBufListByCond" ,  cmsScheduleBuf , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cmsScheduleBuf by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryCmsScheduleBufListByCond", "queryCmsScheduleBufListCntByCond", cmsScheduleBuf, start, pageSize, puEntity);
    }
    
} 
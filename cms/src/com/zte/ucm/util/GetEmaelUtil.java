package com.zte.ucm.util;


import com.zte.cms.common.DbUtil;
import com.zte.ismp.common.ConfigUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.zxywpub.BaseDataSource;
import com.zte.zxywpub.DBConnectionManager;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class GetEmaelUtil {

    private static Log logger = SSBBus.getLog(EmailServiceUtil.class);

    private static String emailInfo = StringUtils.trimToEmpty(ConfigUtil.get("email_smtp_info"));

    public static StringBuilder sendEmailForCpCode(StringBuilder cpCode) throws Exception {
        StringBuilder operatorids = new StringBuilder();
        String cpCodes = cpCode.toString();
        String[] cpCodesList = cpCodes.split(",");
        int length = cpCodesList.length;
        if (StringUtils.isNotBlank(cpCodes)){
            for(int i = 0 ;i < length;i++){
                String sql = "select operatorid from zxdbm_umap.cms_operator_power where cpid=" + cpCodesList[i];
                DbUtil db = new DbUtil();
                List rtnlist = db.getQuery(sql);
                for (int j = 0; j < rtnlist.size(); j++)
                {
                    Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                    String operatorid=tmpMap.get("operatorid").toString();
                    operatorids.append(operatorid).append(",");//把所有的id都放在一个StringBuilder里，用于后面进行查询用户的邮箱
                }
            }

        }
            return operatorids;
    }



    /**
     * 发送邮件
     * @param title 邮件标题
     * @param content   邮件内容
     * @param operatorid    收件人
     *
     */
    public static void  sendEmailForAll(String title,String content,StringBuilder operatorid)
    {

        try
        {
            //判断cp最后一位是否有“，”，没有就用传进来的id，否则就截取掉最后一位的符号“，”
            if(",".equals(operatorid.substring(operatorid.length()-1))){
                operatorid = operatorid.deleteCharAt(operatorid.length()-1);
            }

            String sql = "select email from zxinsys.oper_information where operid in( " + operatorid + ")";
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            StringBuilder emails = new StringBuilder();

            for(int i=0;i<rtnlist.size();i++){
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(i);
                emails.append(KwCheck.checkNull(tmpMap.get("email")).toString()).append(",");
            }
            if(emails != null &&  !"".equals(emails)){
                //发送邮件，在线程中发送邮件，解决一次性发送多封邮件导致的数据库连接中断
                ToEmailUtil toEmailUtil = new ToEmailUtil(emails.toString(),title,content,emailInfo);
                Thread thread = new Thread(toEmailUtil);
                thread.start();
                //EmailServiceUtil.send(title, content, emails.toString(), emailInfo);
            }else{
                System.out.println("操作员："+operatorid+"没有配置邮箱！");
            }
        }
        catch (Exception exp)
        {
            logger.error(exp);
        }
    }


    public static void main(String[] args) {
        try {
            GetEmaelUtil.sendEmailForCpCode(new StringBuilder("00000200,00000199"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

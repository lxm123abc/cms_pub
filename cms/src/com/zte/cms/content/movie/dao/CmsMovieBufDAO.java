package com.zte.cms.content.movie.dao;

import java.util.List;

import com.zte.cms.content.movie.dao.ICmsMovieBufDAO;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsMovieBufDAO extends DynamicObjectBaseDao implements ICmsMovieBufDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("insert cmsMovieBuf starting...");
        super.insert("insertCmsMovieBuf", cmsMovieBuf);
        log.debug("insert cmsMovieBuf end");
    }

    public void insertCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException
    {
        log.debug("insert cmsMovieBufList starting...");
        if (null != cmsMovieBufList)
        {
            super.insertBatch("insertCmsMovieBuf", cmsMovieBufList);
        }
        log.debug("insert cmsMovieBufList end");
    }

    public void updateCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("update cmsMovieBuf by pk starting...");
        super.update("updateCmsMovieBuf", cmsMovieBuf);
        log.debug("update cmsMovieBuf by pk end");
    }

    public void updateCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException
    {
        log.debug("update cmsMovieBufList by pk starting...");
        super.updateBatch("updateCmsMovieBuf", cmsMovieBufList);
        log.debug("update cmsMovieBufList by pk end");
    }

    public void updateCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("update cmsMovieBuf by conditions starting...");
        super.update("updateCmsMovieBufByCond", cmsMovieBuf);
        log.debug("update cmsMovieBuf by conditions end");
    }

    public void updateCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DAOException
    {
        log.debug("update cmsMovieBufList by conditions starting...");
        super.updateBatch("updateCmsMovieBufByCond", cmsMovieBufList);
        log.debug("update cmsMovieBufList by conditions end");
    }

    public void deleteCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("delete cmsMovieBuf by pk starting...");
        super.delete("deleteCmsMovieBuf", cmsMovieBuf);
        log.debug("delete cmsMovieBuf by pk end");
    }

    public void deleteCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException
    {
        log.debug("delete cmsMovieBufList by pk starting...");
        super.deleteBatch("deleteCmsMovieBuf", cmsMovieBufList);
        log.debug("delete cmsMovieBufList by pk end");
    }

    public void deleteCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("delete cmsMovieBuf by conditions starting...");
        super.delete("deleteCmsMovieBufByCond", cmsMovieBuf);
        log.debug("delete cmsMovieBuf by conditions end");
    }

    public void deleteCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DAOException
    {
        log.debug("delete cmsMovieBufList by conditions starting...");
        super.deleteBatch("deleteCmsMovieBufByCond", cmsMovieBufList);
        log.debug("delete cmsMovieBufList by conditions end");
    }

    public CmsMovieBuf getCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("query cmsMovieBuf starting...");
        CmsMovieBuf resultObj = (CmsMovieBuf) super.queryForObject("getCmsMovieBuf", cmsMovieBuf);
        log.debug("query cmsMovieBuf end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException
    {
        log.debug("query cmsMovieBuf by condition starting...");
        List<CmsMovieBuf> rList = (List<CmsMovieBuf>) super.queryForList("queryCmsMovieBufListByCond", cmsMovieBuf);
        log.debug("query cmsMovieBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf, PageUtilEntity puEntity) throws DAOException
    {
        log.debug("query cmsMovieBuf by condition starting...");
        List<CmsMovieBuf> rList = (List<CmsMovieBuf>) super.queryForList("queryCmsMovieBufListByCond", cmsMovieBuf,
                puEntity);
        log.debug("query cmsMovieBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsMovieBuf by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsMovieBufListCntByCond", cmsMovieBuf)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsMovieBuf> rsList = (List<CmsMovieBuf>) super.pageQuery("queryCmsMovieBufListByCond", cmsMovieBuf,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsMovieBuf by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsMovieBufListByCond", "queryCmsMovieBufListCntByCond", cmsMovieBuf, start,
                pageSize, puEntity);
    }

}
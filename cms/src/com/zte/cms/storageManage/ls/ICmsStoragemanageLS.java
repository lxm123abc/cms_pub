package com.zte.cms.storageManage.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStoragemanageLS
{
    /**
     * 新增CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public Map insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 更新CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public String updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 批量更新CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public void updateCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException;

    /**
     * 删除设备对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    // public String deviceRemove( CmsStoragemanage cmsStoragemanage )throws DomainServiceException;
    /**
     * 删除空间对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    // public String spaceRemove( CmsStoragemanage cmsStoragemanage )throws DomainServiceException;

    /**
     * 删除CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public Map removeCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 批量删除CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public void removeCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException;

    /**
     * 查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return CmsStoragemanage对象
     * @throws DomainServiceException ls异常
     */
    public CmsStoragemanage getCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 根据条件查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return 满足条件的CmsStoragemanage对象集
     * @throws DomainServiceException ls异常
     */
    public List<CmsStoragemanage> getCmsStoragemanageByCond(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象 得到设备的优先级
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ls异常
     */
    public TableDataInfo pageInfoQueryPriority(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ls异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ls异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public Map addDevice(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /*******************************************************************************************************************
     * 查询storageManageme中设备下面各存储区的信息
     ******************************************************************************************************************/
    public List<CmsStoragemanage> getCmsStoragemanageList(CmsStoragemanage cmsStoragmanage)
            throws DomainServiceException;
}
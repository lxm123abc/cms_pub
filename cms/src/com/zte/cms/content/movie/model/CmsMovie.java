package com.zte.cms.content.movie.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsMovie extends DynamicBaseObject
{
    private java.lang.Long movieindex;
    private java.lang.String movieid;
    private java.lang.Long programindex;
    private java.lang.String programid;
    private java.lang.Integer movietype;
    private java.lang.String fileurl;
    private java.lang.String cpcontentid;
    private java.lang.Integer sourcedrmtype;
    private java.lang.Integer destdrmtype;
    private java.lang.Integer audiotrack;
    private java.lang.Integer screenformat;
    private java.lang.Integer closedcaptioning;
    private java.lang.String duration;
    private java.lang.Long filesize;
    private java.lang.Integer bitratetype;
    private java.lang.Integer videotype;
    private java.lang.Integer audiotype;
    private java.lang.Integer resolution;
    private java.lang.Integer videoprofile;
    private java.lang.Integer systemlayer;
    private java.lang.Long domain;
    private java.lang.Integer hotdegree;
    private java.lang.String namecn;
    private java.lang.String nameen;
    private java.lang.String desccn;
    private java.lang.String descen;
    private java.lang.Long reservecode1;
    private java.lang.Long reservecode2;
    private java.lang.Integer reservesel1;
    private java.lang.Integer reservesel2;
    private java.lang.String reservetime1;
    private java.lang.String reservetime2;
    private java.lang.Long numberofframes;
    private java.lang.Integer frameheight;
    private java.lang.Integer framewidth;
    private java.lang.String framerate;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String creatoropername;
    private java.lang.String submittername;
    private java.lang.Integer templatetype;
    private java.lang.String wkfwroute;
    private java.lang.String wkfwpriority;
    private java.lang.String effecttime;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;
    private java.lang.String filename;
    private java.lang.String filepath;
    private java.lang.Integer encryptionstatus;
    private java.lang.Integer encryptiontype;
    private java.lang.String encryptionlogpath;
    private java.lang.String encryptionfilepath;
    private java.lang.String contentsourcename;
    private java.lang.String mappingid;
    
    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }

    public java.lang.Long getMovieindex()
    {
        return movieindex;
    }

    public java.lang.Integer getEncryptionstatus() {
		return encryptionstatus;
	}

	public void setEncryptionstatus(java.lang.Integer encryptionstatus) {
		this.encryptionstatus = encryptionstatus;
	}

	public java.lang.Integer getEncryptiontype() {
		return encryptiontype;
	}

	public void setEncryptiontype(java.lang.Integer encryptiontype) {
		this.encryptiontype = encryptiontype;
	}

	public void setMovieindex(java.lang.Long movieindex)
    {
        this.movieindex = movieindex;
    }

    public java.lang.String getMovieid()
    {
        return movieid;
    }

    public void setMovieid(java.lang.String movieid)
    {
        this.movieid = movieid;
    }

    public java.lang.Long getProgramindex()
    {
        return programindex;
    }

    public void setProgramindex(java.lang.Long programindex)
    {
        this.programindex = programindex;
    }

    public java.lang.String getProgramid()
    {
        return programid;
    }

    public void setProgramid(java.lang.String programid)
    {
        this.programid = programid;
    }

    public java.lang.Integer getMovietype()
    {
        return movietype;
    }

    public void setMovietype(java.lang.Integer movietype)
    {
        this.movietype = movietype;
    }

    public java.lang.String getFileurl()
    {
        return fileurl;
    }

    public void setFileurl(java.lang.String fileurl)
    {
        this.fileurl = fileurl;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.Integer getSourcedrmtype()
    {
        return sourcedrmtype;
    }

    public void setSourcedrmtype(java.lang.Integer sourcedrmtype)
    {
        this.sourcedrmtype = sourcedrmtype;
    }

    public java.lang.Integer getDestdrmtype()
    {
        return destdrmtype;
    }

    public void setDestdrmtype(java.lang.Integer destdrmtype)
    {
        this.destdrmtype = destdrmtype;
    }

    public java.lang.Integer getAudiotrack()
    {
        return audiotrack;
    }

    public void setAudiotrack(java.lang.Integer audiotrack)
    {
        this.audiotrack = audiotrack;
    }

    public java.lang.Integer getScreenformat()
    {
        return screenformat;
    }

    public void setScreenformat(java.lang.Integer screenformat)
    {
        this.screenformat = screenformat;
    }

    public java.lang.Integer getClosedcaptioning()
    {
        return closedcaptioning;
    }

    public void setClosedcaptioning(java.lang.Integer closedcaptioning)
    {
        this.closedcaptioning = closedcaptioning;
    }

    public java.lang.String getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.String duration)
    {
        this.duration = duration;
    }

    public java.lang.Long getFilesize()
    {
        return filesize;
    }

    public void setFilesize(java.lang.Long filesize)
    {
        this.filesize = filesize;
    }

    public java.lang.Integer getBitratetype()
    {
        return bitratetype;
    }

    public void setBitratetype(java.lang.Integer bitratetype)
    {
        this.bitratetype = bitratetype;
    }

    public java.lang.Integer getVideotype()
    {
        return videotype;
    }

    public void setVideotype(java.lang.Integer videotype)
    {
        this.videotype = videotype;
    }

    public java.lang.Integer getAudiotype()
    {
        return audiotype;
    }

    public void setAudiotype(java.lang.Integer audiotype)
    {
        this.audiotype = audiotype;
    }

    public java.lang.Integer getResolution()
    {
        return resolution;
    }

    public void setResolution(java.lang.Integer resolution)
    {
        this.resolution = resolution;
    }

    public java.lang.Integer getVideoprofile()
    {
        return videoprofile;
    }

    public void setVideoprofile(java.lang.Integer videoprofile)
    {
        this.videoprofile = videoprofile;
    }

    public java.lang.Integer getSystemlayer()
    {
        return systemlayer;
    }

    public void setSystemlayer(java.lang.Integer systemlayer)
    {
        this.systemlayer = systemlayer;
    }

    public java.lang.Long getDomain()
    {
        return domain;
    }

    public void setDomain(java.lang.Long domain)
    {
        this.domain = domain;
    }

    public java.lang.Integer getHotdegree()
    {
        return hotdegree;
    }

    public void setHotdegree(java.lang.Integer hotdegree)
    {
        this.hotdegree = hotdegree;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getDesccn()
    {
        return desccn;
    }

    public void setDesccn(java.lang.String desccn)
    {
        this.desccn = desccn;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.Long getReservecode1()
    {
        return reservecode1;
    }

    public void setReservecode1(java.lang.Long reservecode1)
    {
        this.reservecode1 = reservecode1;
    }

    public java.lang.Long getReservecode2()
    {
        return reservecode2;
    }

    public void setReservecode2(java.lang.Long reservecode2)
    {
        this.reservecode2 = reservecode2;
    }

    public java.lang.Integer getReservesel1()
    {
        return reservesel1;
    }

    public void setReservesel1(java.lang.Integer reservesel1)
    {
        this.reservesel1 = reservesel1;
    }

    public java.lang.Integer getReservesel2()
    {
        return reservesel2;
    }

    public void setReservesel2(java.lang.Integer reservesel2)
    {
        this.reservesel2 = reservesel2;
    }

    public java.lang.String getReservetime1()
    {
        return reservetime1;
    }

    public void setReservetime1(java.lang.String reservetime1)
    {
        this.reservetime1 = reservetime1;
    }

    public java.lang.String getReservetime2()
    {
        return reservetime2;
    }

    public void setReservetime2(java.lang.String reservetime2)
    {
        this.reservetime2 = reservetime2;
    }

    public java.lang.Long getNumberofframes()
    {
        return numberofframes;
    }

    public void setNumberofframes(java.lang.Long numberofframes)
    {
        this.numberofframes = numberofframes;
    }

    public java.lang.Integer getFrameheight()
    {
        return frameheight;
    }

    public void setFrameheight(java.lang.Integer frameheight)
    {
        this.frameheight = frameheight;
    }

    public java.lang.Integer getFramewidth()
    {
        return framewidth;
    }

    public void setFramewidth(java.lang.Integer framewidth)
    {
        this.framewidth = framewidth;
    }

    public java.lang.String getFramerate()
    {
        return framerate;
    }

    public void setFramerate(java.lang.String framerate)
    {
        this.framerate = framerate;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public java.lang.String getFilename()
    {
        return filename;
    }

    public void setFilename(java.lang.String filename)
    {
        this.filename = filename;
    }

    public void initRelation()
    {
        this.addRelation("movieindex", "MOVIEINDEX");
        this.addRelation("movieid", "MOVIEID");
        this.addRelation("programindex", "PROGRAMINDEX");
        this.addRelation("programid", "PROGRAMID");
        this.addRelation("movietype", "MOVIETYPE");
        this.addRelation("fileurl", "FILEURL");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("sourcedrmtype", "SOURCEDRMTYPE");
        this.addRelation("destdrmtype", "DESTDRMTYPE");
        this.addRelation("audiotrack", "AUDIOTRACK");
        this.addRelation("screenformat", "SCREENFORMAT");
        this.addRelation("closedcaptioning", "CLOSEDCAPTIONING");
        this.addRelation("duration", "DURATION");
        this.addRelation("filesize", "FILESIZE");
        this.addRelation("bitratetype", "BITRATETYPE");
        this.addRelation("videotype", "VIDEOTYPE");
        this.addRelation("audiotype", "AUDIOTYPE");
        this.addRelation("resolution", "RESOLUTION");
        this.addRelation("videoprofile", "VIDEOPROFILE");
        this.addRelation("systemlayer", "SYSTEMLAYER");
        this.addRelation("domain", "DOMAIN");
        this.addRelation("hotdegree", "HOTDEGREE");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("desccn", "DESCCN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("reservecode1", "RESERVECODE1");
        this.addRelation("reservecode2", "RESERVECODE2");
        this.addRelation("reservesel1", "RESERVESEL1");
        this.addRelation("reservesel2", "RESERVESEL2");
        this.addRelation("reservetime1", "RESERVETIME1");
        this.addRelation("reservetime2", "RESERVETIME2");
        this.addRelation("numberofframes", "NUMBEROFFRAMES");
        this.addRelation("frameheight", "FRAMEHEIGHT");
        this.addRelation("framewidth", "FRAMEWIDTH");
        this.addRelation("framerate", "FRAMERATE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
        this.addRelation("encryptionlogpath", "ENCRYPTIONLOGPATH");
        this.addRelation("contentsourcename", "CONTENTSOURCENAME");
    }

	public java.lang.String getEncryptionlogpath() {
		return encryptionlogpath;
	}

	public void setEncryptionlogpath(java.lang.String encryptionlogpath) {
		this.encryptionlogpath = encryptionlogpath;
	}

	public java.lang.String getFilepath() {
		return filepath;
	}

	public void setFilepath(java.lang.String filepath) {
		this.filepath = filepath;
	}

	public java.lang.String getEncryptionfilepath() {
		return encryptionfilepath;
	}

	public void setEncryptionfilepath(java.lang.String encryptionfilepath) {
		this.encryptionfilepath = encryptionfilepath;
	}

	public java.lang.String getContentsourcename()
	{
		return contentsourcename;
	}

	public void setContentsourcename(java.lang.String contentsourcename)
	{
		this.contentsourcename = contentsourcename;
	}
}

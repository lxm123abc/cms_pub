<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%

   String levelindex = request.getParameter("levelindex");
   HashMap smap = pwdrule.getSafeRule(levelindex);
  
%>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css">
<script language = "JavaScript">
function isnumber(value){
		  var allValid = true;
		  for (i = 0;i<value.length;i++)
		  {

		    ch = value.charAt(i);
		    if((ch<='9' && ch >= '0')){ 
		    }
		    else{
		     allValid = false;
		    }
		  }
		  if (!allValid) return (false);
		  else return (true);
}
function doAdd(){

 if(!isnumber(document.theForm.levelindex.value)){
   document.theForm.levelindex.focus();
   return;
 }
 if(!isnumber(document.theForm.minlen.value) ||document.theForm.minlen.value< 1){
   document.theForm.minlen.focus();
   return;
 }
 if(!isnumber(document.theForm.maxlen.value)||document.theForm.maxlen.value> 18){
   document.theForm.maxlen.focus();
   return;
 }
 if(!isnumber(document.theForm.maxwrglog.value)){
   document.theForm.maxwrglog.focus();
   return;
 } 
 if(!isnumber(document.theForm.pwdmaxday.value)){
   document.theForm.pwdmaxday.focus();
   return;
 }  
 if(!isnumber(document.theForm.usednum.value)){
   document.theForm.usednum.focus();
   return;
 }
 document.theForm.flag.value="2";
 document.theForm.action = "saferuleaddend.jsp";
 document.theForm.submit();
 
}
function doEdit(levelindex){

 if(!isnumber(document.theForm.levelindex.value)){
   document.theForm.levelindex.focus();
   return;
 }
 if(!isnumber(document.theForm.minlen.value)){
   document.theForm.minlen.focus();
   return;
 }
 if(!isnumber(document.theForm.maxlen.value)){
   document.theForm.maxlen.focus();
   return;
 }
 if(!isnumber(document.theForm.maxwrglog.value)){
   document.theForm.maxwrglog.focus();
   return;
 } 
 if(!isnumber(document.theForm.pwdmaxday.value)){
   document.theForm.pwdmaxday.focus();
   return;
 }  
 if(!isnumber(document.theForm.usednum.value)){
   document.theForm.usednum.focus();
   return;
 } 
 document.theForm.flag.value="3";
 document.theForm.action = "saferuleaddend.jsp";
 document.theForm.submit();

}
function sethintflag(hintflag){

 document.theForm.hintflag.value=hintflag;
 
}
function setprerequisite(prerequisite){
 
 document.theForm.prerequisite.value=prerequisite;
 
}
function setupdateflag(updateflag){

 document.theForm.updateflag.value=updateflag;
  
}
function setcheckflag(checkflag){
 
 document.theForm.checkflag.value=checkflag;
 
}


</script>
    </head>

    <body>
        <table width = "100%" height = "98%" border = "0" align = "center" cellpadding = "0" cellspacing = "0" >

            <tr>
              <td valign = "top">
					<form name = "theForm" action = "" method = "post">
                    <input type="hidden" name="flag" value=""/>
                    <input type="hidden" name="updateflag" value=""/>
                    <input type="hidden" name="hintflag" value=""/>
                    <input type="hidden" name="checkflag" value=""/>
                    <input type="hidden" name="prerequisite" value=""/>
                    
                        <table width = "95%" border = "0" bgcolor = "#CCCCCC" cellpadding = "1" cellspacing = "1" align = "center" class="table-style2">
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="saferuleset.securitylevel"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <input name="levelindex" type="text" value="<%= smap.get("levelindex")%>"  ></td>
							              </tr>
  						              <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="saferuleadd.minpwd"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <input name = "minlen" type= "text" value="<%= smap.get("minlen")%>"><font color="#ff0000">*</font></td>
                            </tr>
							              <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="saferuleadd.maxpwd"/> </td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <input name = "maxlen" type = "text" value="<%= smap.get("maxlen")%>"><font color="#ff0000">*</font></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="saferuleadd.scope"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <select name = "chrtype">
                                      <option value="0" <%= "0".equals((String)smap.get("chrtype"))? "selected":""  %>><fmt:message key="saferuleadd.random"/></option>
                                      <option value="1" <%= "1".equals((String)smap.get("chrtype"))? "selected":""  %>><fmt:message key="saferuleadd.number"/></option>
                                      <option value="2" <%= "2".equals((String)smap.get("chrtype"))? "selected":""  %>><fmt:message key="saferuleadd.englishalphabet"/></option>
                                      <option value="3" <%= "3".equals((String)smap.get("chrtype"))? "selected":""  %>><fmt:message key="saferuleadd.numberandalphabet"/></option>
                                    </select></td>
							              </tr> 
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="saferuleadd.creatingmode"/></td>

                                <td height = "28" bgcolor= "#FFFFFF">
                                    <select name = "createtype">
                                      <option value="0" <%= "0".equals((String)smap.get("createtype"))? "selected":""  %>><fmt:message key="saferuleadd.customer"/></option>
                                      <option value="1" <%= "1".equals((String)smap.get("createtype"))? "selected":""  %>><fmt:message key="saferuleadd.differfromname"/></option>
                                      <option value="2" <%= "2".equals((String)smap.get("createtype"))? "selected":""  %>><fmt:message key="saferuleadd.systemrandom"/></option>
                                    </select>
                                </td>
							              </tr> 							                                         
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleadd.passworddefault"/> </td>
                                <td bgcolor= "#FFFFFF"><input type= "text" name="defaultpwd" value="<%= smap.get("defaultpwd")%>"></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleadd.checkflag"/> </td>
                                <td bgcolor= "#FFFFFF">
                                <input type="radio" <%= "0".equals((String)smap.get("checkflag"))? "checked":""  %> name="_checkflag" onclick="javascript:setcheckflag('0')"><fmt:message key="saferuleadd.nocheck"/>
                                <input type="radio" <%= "1".equals((String)smap.get("checkflag"))? "checked":""  %> name="_checkflag" onclick="javascript:setcheckflag('1')"><fmt:message key="saferuleadd.check"/>
                                </td>
                            </tr>                            
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleadd.modifyflag"/> </td>
                                <td bgcolor= "#FFFFFF">
                                <input type="radio" <%= "0".equals((String)smap.get("updateflag"))? "checked":""  %> name="_updateflag" onclick="javascript:setupdateflag('0')"><fmt:message key="saferuleadd.optional"/>
                                <input type="radio" <%= "1".equals((String)smap.get("updateflag"))? "checked":""  %> name="_updateflag" onclick="javascript:setupdateflag('1')"><fmt:message key="saferuleadd.required"/>
                                </td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td><fmt:message key="saferuleadd.maxwrglogin"/></td>
                                <td bgcolor= "#FFFFFF"><input type= "text" name="maxwrglog" value="<%= smap.get("maxwrglog")%>"></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleadd.pwdchangeperiod"/> </td>
                                <td bgcolor= "#FFFFFF"><input type= "text" name="pwdmaxday" value="<%= smap.get("pwdmaxday")%>"></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td><fmt:message key="saferuleadd.hintflag"/></td>
                                <td bgcolor= "#FFFFFF">
                                <input type="radio" <%= "0".equals((String)smap.get("hintflag"))? "checked":""  %> name="_hintflag" onclick="javascript:sethintflag('0')"><fmt:message key="saferuleadd.nohint"/>
                                <input type="radio" <%= "1".equals((String)smap.get("hintflag"))? "checked":""  %> name="_hintflag" onclick="javascript:sethintflag('1')"><fmt:message key="saferuleadd.hint"/>
                                </td>                                
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleadd.historicpwd"/></td>
                                <td bgcolor= "#FFFFFF"><input type= "text" name="usednum" value="<%= smap.get("usednum")%>"></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="purview.prerequisite"/></td>
                                <td bgcolor= "#FFFFFF">
                                <input type="radio" <%= "0".equals((String)smap.get("prerequisite"))? "checked":""  %> name="_prerequisite" onclick="javascript:setprerequisite('0')"><fmt:message key="saferuleadd.customerized"/>
                                <input type="radio" <%= "1".equals((String)smap.get("prerequisite"))? "checked":""  %> name="_prerequisite" onclick="javascript:setprerequisite('1')"><fmt:message key="saferuleadd.forcedefault"/>
                                </td>
                            </tr>                            
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleset.description"/> </td>
                                <td bgcolor= "#FFFFFF">
                                    <textarea name = "description" cols = "80" rows = "4"><%= smap.get("description")%></textarea></td>
                            </tr>
                            
                            <tr bgcolor = "#F5F5F5">
                                <td colspan = "2" align = "center">
									<% if (levelindex == null ||levelindex.trim().length()==0) { %>
                                   <input type = "button" class="button" value = "<fmt:message key="saferuleset.add"/>" onclick = "return doAdd();">
									<% } else { %>
								                   <input type="hidden" name="targetindex" value="<%= levelindex%>">
										               <input type = "button" class="button" value = "<fmt:message key="saferuleset.edit"/>" onclick = "return doEdit('<%= levelindex %>');">
									<% } %>
                                   <input type = "button" class="button" value = "<fmt:message key="saferuleset.cancel"/>" onclick="javascript: history.back();">
                                </td>
                            </tr>
                        </table>
					</form>

					<hr>

          </td>

            </tr>

        </table>
    </body>
</html>
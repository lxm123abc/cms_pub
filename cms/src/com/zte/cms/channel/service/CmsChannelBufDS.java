package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.dao.ICmsChannelBufDAO;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.common.DateUtil2;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsChannelBufDS implements ICmsChannelBufDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsChannelBufDAO dao = null;

    public void setDao(ICmsChannelBufDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException
    {
        log.debug("insert cmsChannelBuf starting...");
        try
        {
            cmsChannelBuf.setLlicensofflinetime(DateUtil2.get14Time(cmsChannelBuf.getLlicensofflinetime()));
            cmsChannelBuf.setDeletetime(DateUtil2.get14Time(cmsChannelBuf.getDeletetime()));
            cmsChannelBuf.setOnlinetime(DateUtil2.get14Time(cmsChannelBuf.getOnlinetime()));
            cmsChannelBuf.setOfflinetime(DateUtil2.get14Time(cmsChannelBuf.getOfflinetime()));
            cmsChannelBuf.setLicensingstart(DateUtil2.get14Time(cmsChannelBuf.getLicensingstart()));
            cmsChannelBuf.setLicensingend(DateUtil2.get14Time(cmsChannelBuf.getLicensingend()));
            dao.insertCmsChannelBuf(cmsChannelBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsChannelBuf end");
    }

    public void updateCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException
    {
        log.debug("update cmsChannelBuf by pk starting...");
        try
        {
            cmsChannelBuf.setLlicensofflinetime(DateUtil2.get14Time(cmsChannelBuf.getLlicensofflinetime()));
            cmsChannelBuf.setDeletetime(DateUtil2.get14Time(cmsChannelBuf.getDeletetime()));
            cmsChannelBuf.setOnlinetime(DateUtil2.get14Time(cmsChannelBuf.getOnlinetime()));
            cmsChannelBuf.setOfflinetime(DateUtil2.get14Time(cmsChannelBuf.getOfflinetime()));
            cmsChannelBuf.setLicensingstart(DateUtil2.get14Time(cmsChannelBuf.getLicensingstart()));
            cmsChannelBuf.setLicensingend(DateUtil2.get14Time(cmsChannelBuf.getLicensingend()));
            dao.updateCmsChannelBuf(cmsChannelBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsChannelBuf by pk end");
    }

    public void updateCmsChannelBufList(List<CmsChannelBuf> cmsChannelBufList) throws DomainServiceException
    {
        log.debug("update cmsChannelBufList by pk starting...");
        if (null == cmsChannelBufList || cmsChannelBufList.size() == 0)
        {
            log.debug("there is no datas in cmsChannelBufList");
            return;
        }
        try
        {
            for (CmsChannelBuf cmsChannelBuf : cmsChannelBufList)
            {
                dao.updateCmsChannelBuf(cmsChannelBuf);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsChannelBufList by pk end");
    }

    public void removeCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException
    {
        log.debug("remove cmsChannelBuf by pk starting...");
        try
        {
            dao.deleteCmsChannelBuf(cmsChannelBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsChannelBuf by pk end");
    }

    public void removeCmsChannelBufList(List<CmsChannelBuf> cmsChannelBufList) throws DomainServiceException
    {
        log.debug("remove cmsChannelBufList by pk starting...");
        if (null == cmsChannelBufList || cmsChannelBufList.size() == 0)
        {
            log.debug("there is no datas in cmsChannelBufList");
            return;
        }
        try
        {
            for (CmsChannelBuf cmsChannelBuf : cmsChannelBufList)
            {
                dao.deleteCmsChannelBuf(cmsChannelBuf);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsChannelBufList by pk end");
    }

    public CmsChannelBuf getCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException
    {
        log.debug("get cmsChannelBuf by pk starting...");
        CmsChannelBuf rsObj = null;
        try
        {
            rsObj = dao.getCmsChannelBuf(cmsChannelBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsChannelBufList by pk end");
        return rsObj;
    }

    public List<CmsChannelBuf> getCmsChannelBufByCond(CmsChannelBuf cmsChannelBuf) throws DomainServiceException
    {
        log.debug("get cmsChannelBuf by condition starting...");
        List<CmsChannelBuf> rsList = null;
        try
        {
            rsList = dao.getCmsChannelBufByCond(cmsChannelBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsChannelBuf by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsChannelBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsChannelBuf, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsChannelBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsChannelBuf page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsChannelBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsChannelBuf, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsChannelBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsChannelBuf page info by condition end");
        return tableInfo;
    }
}

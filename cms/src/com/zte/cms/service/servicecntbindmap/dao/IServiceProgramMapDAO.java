package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceProgramMapDAO
{
    /**
     * 新增ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DAOException dao异常
     */
    public void insertServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException;

    /**
     * 根据主键更新ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DAOException dao异常
     */
    public void updateServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException;

    /**
     * 根据主键删除ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @throws DAOException dao异常
     */
    public void deleteServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException;

    /**
     * 根据主键查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @return 满足条件的ServiceProgramMap对象
     * @throws DAOException dao异常
     */
    public ServiceProgramMap getServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException;

    /**
     * 根据条件查询ServiceProgramMap对象
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @return 满足条件的ServiceProgramMap对象集
     * @throws DAOException dao异常
     */
    public List<ServiceProgramMap> getServiceProgramMapByCond(ServiceProgramMap serviceProgramMap) throws DAOException;

    /**
     * 根据条件分页查询ServiceProgramMap对象，作为查询条件的参数
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceProgramMap对象，作为查询条件的参数
     * 
     * @param serviceProgramMap ServiceProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
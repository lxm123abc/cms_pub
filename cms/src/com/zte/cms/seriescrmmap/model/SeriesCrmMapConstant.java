package com.zte.cms.seriescrmmap.model;

public class SeriesCrmMapConstant
{
    // mgttype 管理类型 连续剧内容与角色管理

    public static final String MGTTYPE_SERIESCRMMAP = "log.seriescrmmap.mgt";

    public final static int SERIESCRMMAP_STATUS_WAITPUBLISH = 0;// 待发布
    public final static int SERIESCRMMAP_STATUS_ADDSYNCHRONIZING = 10;// 新增同步中
    public final static int SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS = 20;// 新增同步成功
    public final static int SERIESCRMMAP_STATUS_ADDSYNCHRONIZED_FAIL = 30;// 新增同步失败
    public final static int SERIESCRMMAP_STATUS_UPDATESYNCHRONIZING = 40;// 修改同步中
    public final static int SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS = 50;// 修改同步成功
    public final static int SERIESCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL = 60;// 修改同步失败
    public final static int SERIESCRMMAP_STATUS_CANCELSYNCHRONIZING = 70;// 取消同步中
    public final static int SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_SUCCESS = 80;// 取消同步成功
    public final static int SERIESCRMMAP_STATUS_CANCELSYNCHRONIZED_FAIL = 90;// 取消同步失败

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_SERIESCRMMAP_ADD = "log.seriescrmmap.add";// 连续剧内容与角色
    public static final String OPERTYPE_SERIESCRMMAP_DEL = "log.seriescrmmap.delete";
    public static final String OPERTYPE_SERIESCRMMAP_PUBLISH = "log.seriescrmmap.publish";
    public static final String OPERTYPE_SERIESCRMMAP_CANCELPUBLISH = "log.seriescrmmap.cancelpublish";
    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_SERIESCRMMAP_ADD_INFO = "log.seriescrmmap.add.info";
    public static final String OPERTYPE_SERIESCRMMAP_DEL_INFO = "log.seriescrmmap.delete.info";
    public static final String OPERTYPE_SERIESCRMMAP_PUBLISH_INFO = "log.seriescrmmap.publish.info";
    public static final String OPERTYPE_SERIESCRMMAP_CANCELPUBLISH__INFO = "log.seriescrmmap.cancelpublish.info";

}

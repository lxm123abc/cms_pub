package com.zte.cms.cppr.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsCppr extends DynamicBaseObject
{
    private java.lang.Long operatorid;
    private java.lang.Long operatortype;
    private java.lang.String cpid;
    private java.lang.String cpcnshortname;
    private java.lang.String servicekey;
    private java.lang.Long cptype;
    private java.lang.Long cpindex;

    public void initRelation()
    {
        this.addRelation("operatorid", "operatorid");
        this.addRelation("operatortype", "operatortype");
        this.addRelation("cpid", "cpid");
        this.addRelation("cpcnshortname", "cpcnshortname");
        this.addRelation("servicekey", "servicekey");
        this.addRelation("cptype", "cptype");
        this.addRelation("cpindex", "cpindex");
     
    }
    
    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.Long getCptype()
    {
        return cptype;
    }

    public void setCptype(java.lang.Long cptype)
    {
        this.cptype = cptype;
    }

    public java.lang.Long getOperatorid()
    {
        return operatorid;
    }
    public void setOperatorid(java.lang.Long operatorid)
    {
        this.operatorid = operatorid;
    }
    public java.lang.Long getOperatortype()
    {
        return operatortype;
    }
    public void setOperatortype(java.lang.Long operatortype)
    {
        this.operatortype = operatortype;
    }
    public java.lang.String getCpid()
    {
        return cpid;
    }
    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }
    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }
    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }
    public java.lang.String getServicekey()
    {
        return servicekey;
    }
    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }
}

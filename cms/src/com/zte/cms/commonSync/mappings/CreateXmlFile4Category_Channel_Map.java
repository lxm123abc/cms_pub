package com.zte.cms.commonSync.mappings;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.framework.base.ISysCode;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CreateXmlFile4Category_Channel_Map extends
		AbstractCreateXmlFile4Object {
	private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL,
			getClass());

	public String createXmlFile4WG18(Map<String, List> objectMap,
			int targetType, int syncType) {
		// TODO Auto-generated method stub
		String action = null;
		if ((0 != targetType)&&(10!=targetType)) {
			return null;
		}
		if (1 == syncType) {
			action = "REGIST";
		} else if (2 == syncType) {
			action = "UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}
		Map<String, String> mappingMap = null;
		List<Map> mapList = null;
		Date date = new Date();
		String fileName = "category_channel_20wg" +"_"+ action.toLowerCase().replace(" ", "") + "_" + date.getTime() + ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
				+ File.separator + getCurrentTime() + File.separator
				+ "channel" + File.separator;
		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String fileFullName = absolutePath + fileName;
		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		Element mappingElement = adiElement.addElement("Mappings");
		List<CategoryChannelMap> categoryChannelMapList = (List<CategoryChannelMap>) objectMap
				.get("categorychannelmaplist");
		Element mapObjectElement = null;
		Element mapPertyElement = null;
		mapObjectElement = mappingElement.addElement("Mapping");
		mapObjectElement.addAttribute("ParentType", "Category");
		mapObjectElement.addAttribute("ID", categoryChannelMapList.get(0).getMappingid());
		mapObjectElement.addAttribute("ParentID", categoryChannelMapList.get(0)
				.getCategorycode().toString());
		mapObjectElement.addAttribute("ParentCode", categoryChannelMapList.get(
				0).getCategorycode().toString());
		mapObjectElement.addAttribute("ElementType", "Channel");
		mapObjectElement.addAttribute("ElementID", categoryChannelMapList
				.get(0).getCpcontentid().toString());
		mapObjectElement.addAttribute("ElementCode", categoryChannelMapList
				.get(0).getCpcontentid().toString());
		mapObjectElement.addAttribute("Action", action);
		writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
		writeElement(mapObjectElement, mapPertyElement, "Sequence",
				categoryChannelMapList.get(0).getSequence().toString(), false);
		writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
		writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);
		writeDom(dom, fileFullName);
		return retPath;
	}

	public String createXmlFile4IPTV30(Map<String, List> objectMap,
			int targetType, int syncType) {

		String action = null;
		if (1 == syncType || 2 == syncType) {
			action = "REGIST or UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}
		// TODO Auto-generated method stub
		Map<String, String> mappingMap = null;
		List<Map> mapList = null;
		String targetTypeName = "";
		if (targetType == 1) {
			targetTypeName = "category_channel_bms";
		} else if (targetType == 2) {
			targetTypeName = "category_channel_epg";
		}
		Date date = new Date();
		String fileName = targetTypeName +"_"+action.toLowerCase().replace(" ", "")+ "_" + date.getTime() + ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
				+ File.separator + getCurrentTime() + File.separator
				+ "channel" + File.separator;
		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String fileFullName = absolutePath + fileName;

		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		adiElement.addAttribute("xmlns:xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		Element mappingsElement = adiElement.addElement("Mappings");
		Element mappingElement = null;
		Element propertyElement = null;

		List<CategoryChannelMap> categoryChannelMapList = (List<CategoryChannelMap>) objectMap
				.get("categorychannelmaplist");

		// 元素节点
		mappingElement = mappingsElement.addElement("Mapping");
		String objectId = String.format("%02d", 28)
				+ String.format("%030d", categoryChannelMapList.get(0)
						.getMapindex());
		mappingElement.addAttribute("ObjectID", objectId);
		mappingElement.addAttribute("ParentType", "Category");
		mappingElement.addAttribute("ParentID", categoryChannelMapList.get(0)
				.getCategoryid());
		mappingElement.addAttribute("ElementType", "Channel");
		mappingElement.addAttribute("ElementID", categoryChannelMapList.get(0)
				.getChannelid());
		mappingElement.addAttribute("Action", action);

		// 属性节点
		propertyElement = mappingElement.addElement("Property");
		propertyElement.addAttribute("Name", "Sequence");
		if (categoryChannelMapList.get(0).getSequence() != null) {
			propertyElement.addText(categoryChannelMapList.get(0).getSequence()
					.toString());
		} else {
			propertyElement.addText("");
		}
		propertyElement = mappingElement.addElement("Property");
        propertyElement.addAttribute("Name", "Type");      
        propertyElement.addText("");
        
        propertyElement = mappingElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");      
        propertyElement.addText("");
        
        propertyElement = mappingElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");      
        propertyElement.addText("");

		XMLWriter writer = null;
		try {
			writer = new XMLWriter(new FileOutputStream(fileFullName));
			writer.write(dom);
		} catch (Exception e) {
			log.error("createScheduleXml exception:" + e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e) {
					log.error("createScheduleXml exception:" + e);
				}
			}
		}

		return retPath;
	}

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {
     // TODO Auto-generated method stub
        String action = null;
        if ((0 != targetType)&&(10!=targetType)) {
            return null;
        }
        if (1 == syncType) {
            action = "REGIST";
        } else if (2 == syncType) {
            action = "UPDATE";
        } else if (3 == syncType) {
            action = "DELETE";
        } else {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;
        Date date = new Date();
        String fileName = "category_channel_20ys" +"_"+action.toLowerCase().replace(" ", "")+ "_" + date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "channel" + File.separator;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants
                .filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element mappingElement = adiElement.addElement("Mappings");
        List<CategoryChannelMap> categoryChannelMapList = (List<CategoryChannelMap>) objectMap
                .get("categorychannelmaplist");
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");
        mapObjectElement.addAttribute("ParentType", "Category");
        mapObjectElement.addAttribute("ID", categoryChannelMapList.get(0).getMappingid());
        mapObjectElement.addAttribute("ParentID", categoryChannelMapList.get(0)
                .getCategorycode().toString());
        mapObjectElement.addAttribute("ParentCode", categoryChannelMapList.get(
                0).getCategorycode().toString());
        mapObjectElement.addAttribute("ElementType", "Channel");
        mapObjectElement.addAttribute("ElementID", categoryChannelMapList
                .get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("ElementCode", categoryChannelMapList
                .get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("Action", action);
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence",
                categoryChannelMapList.get(0).getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);
        writeDom(dom, fileFullName);
        return retPath;
    }

}

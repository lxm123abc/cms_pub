package com.zte.cms.seriespromap.service;

import java.util.List;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ISeriesProgramMapDS
{
    /**
     * 新增SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException;

    /**
     * 更新SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException;

    /**
     * 批量更新SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesProgramMapList(List<SeriesProgramMap> seriesProgramMapList) throws DomainServiceException;

    /**
     * 删除SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException;

    /**
     * 批量删除SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesProgramMapList(List<SeriesProgramMap> seriesProgramMapList) throws DomainServiceException;

    /**
     * 查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return SeriesProgramMap对象
     * @throws DomainServiceException ds异常
     */
    public SeriesProgramMap getSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException;

    /**
     * 根据条件查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return 满足条件的SeriesProgramMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<SeriesProgramMap> getSeriesProgramMapByCond(SeriesProgramMap seriesProgramMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据连续主键返回其下面最大集序号值
     * 
     * @param seriesProgramMap 关联对象
     * @return 集序号最大值
     * @throws DomainServiceException ds 异常
     */
    public int getMaxSequenceBySeriesindex(SeriesProgramMap seriesProgramMap) throws DomainServiceException;
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryseriesproMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException;
    
    
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryseriesproTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据条件查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return 满足条件的SeriesProgramMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<SeriesProgramMap> getnormalSeriesProTargetMapByCond(SeriesProgramMap seriesProgramMap)
            throws DomainServiceException;
}
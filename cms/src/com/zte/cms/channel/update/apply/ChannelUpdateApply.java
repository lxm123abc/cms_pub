package com.zte.cms.channel.update.apply;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.ls.ICmsChannelSyncLS;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelBufDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.update.wkfw.ls.IChannelUpdateWkfwCheck;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.jwf.workflowEngine.common.util.BpsException;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.framework.base.Sequence;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;

import net.sf.cglib.beans.BeanCopier;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelUpdateApply extends WorkflowBase implements
		IChannelUpdateApply {
	protected IChannelUpdateWkfwCheck channelUpdateWkfwCheckLS = null;
	private Log log = SSBBus.getLog(getClass());
	private ICmsChannelDS channelds = null;
	private ICmsChannelBufDS channelBufds = null;
	private IBatchObjectRecordDS batchObjectRecordDS;// 批处理
	private IPhysicalchannelDS physicalchannelDS;
	private IPhysicalchannelBufDS physicalchannelbufDS;
	private ITargetsystemDS targetSystemDS;
	private static final String CORRELATEID = "ucdn_task_correlate_id";
	private static final String TASKINDEX = "cnt_sync_task";

	private ICntSyncTaskDS cntsyncTaskDS;
	private static final String TEMPAREA_ID = "1"; // 临时区ID
	private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
	private IUsysConfigDS usysConfigDS;
	private static final String SUCCESS = "cmsprogramtype.success";
	private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
	private ICntPlatformSyncDS cntPlatformSyncDS;
	private ICntTargetSyncDS cntTargetSyncDS;
	private IObjectSyncRecordDS objectSyncRecordDS;
    private ICmsChannelSyncLS cmsChannelSyncLS;
    private String callbackProcess;
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    public ICmsChannelSyncLS getCmsChannelSyncLS()
    {
        return cmsChannelSyncLS;
    }

    public void setCmsChannelSyncLS(ICmsChannelSyncLS cmsChannelSyncLS)
    {
        this.cmsChannelSyncLS = cmsChannelSyncLS;
    }

	public ICntPlatformSyncDS getCntPlatformSyncDS() {
		return cntPlatformSyncDS;
	}

	public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS) {
		this.cntPlatformSyncDS = cntPlatformSyncDS;
	}

	public ICntTargetSyncDS getCntTargetSyncDS() {
		return cntTargetSyncDS;
	}

	public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS) {
		this.cntTargetSyncDS = cntTargetSyncDS;
	}

	public IObjectSyncRecordDS getObjectSyncRecordDS() {
		return objectSyncRecordDS;
	}

	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS) {
		this.objectSyncRecordDS = objectSyncRecordDS;
	}

	public IPhysicalchannelBufDS getPhysicalchannelbufDS() {
		return physicalchannelbufDS;
	}

	public void setPhysicalchannelbufDS(
			IPhysicalchannelBufDS physicalchannelbufDS) {
		this.physicalchannelbufDS = physicalchannelbufDS;
	}

	public IUsysConfigDS getUsysConfigDS() {
		return usysConfigDS;
	}

	public void setUsysConfigDS(IUsysConfigDS usysConfigDS) {
		this.usysConfigDS = usysConfigDS;
	}

	public ICntSyncTaskDS getCntsyncTaskDS() {
		return cntsyncTaskDS;
	}

	public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS) {
		this.cntsyncTaskDS = cntsyncTaskDS;
	}

	public ITargetsystemDS getTargetSystemDS() {
		return targetSystemDS;
	}

	public void setTargetSystemDS(ITargetsystemDS targetSystemDS) {
		this.targetSystemDS = targetSystemDS;
	}

	public IBatchObjectRecordDS getBatchObjectRecordDS() {
		return batchObjectRecordDS;
	}

	public void setBatchObjectRecordDS(IBatchObjectRecordDS batchObjectRecordDS) {
		this.batchObjectRecordDS = batchObjectRecordDS;
	}

	// 构造方法
	ChannelUpdateApply() {
		ResourceMgt
				.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
	}

	/**
	 * 
	 * @return IPhysicalchannelDS
	 */
	public IPhysicalchannelDS getPhysicalchannelDS() {
		return physicalchannelDS;
	}

	/**
	 * 
	 * @param physicalchannelDS
	 *            IPhysicalchannelDS
	 */
	public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS) {
		this.physicalchannelDS = physicalchannelDS;
	}

	/**
	 * 
	 * @return ICmsChannelDS
	 */
	public ICmsChannelDS getChannelds() {
		return channelds;
	}

	/**
	 * 
	 * @param channelds
	 *            ICmsChannelDS
	 */
	public void setChannelds(ICmsChannelDS channelds) {
		this.channelds = channelds;
	}

	/**
	 * 
	 * @return ICmsChannelBufDS
	 */
	public ICmsChannelBufDS getChannelBufds() {
		return channelBufds;
	}

	/**
	 * 
	 * @param channelBufds
	 *            ICmsChannelBufDS
	 */
	public void setChannelBufds(ICmsChannelBufDS channelBufds) {
		this.channelBufds = channelBufds;
	}

	/**
	 * 
	 * @return IChannelUpdateWkfwCheck
	 */
	public IChannelUpdateWkfwCheck getChannelUpdateWkfwCheckLS() {
		return channelUpdateWkfwCheckLS;
	}

	/**
	 * 
	 * @param channelUpdateWkfwCheckLS
	 *            IChannelUpdateWkfwCheck
	 */
	public void setChannelUpdateWkfwCheckLS(
			IChannelUpdateWkfwCheck channelUpdateWkfwCheckLS) {
		this.channelUpdateWkfwCheckLS = channelUpdateWkfwCheckLS;
	}

	/**
	 * @param channel
	 *            CmsChannel
	 * @return String
	 * @throws Exception
	 */
	public String apply(CmsChannel channel) throws Exception {
		String checkResult = "";
		try {
			checkResult = channelUpdateWkfwCheckLS
					.checkChannelUpdateWkfw(channel.getChannelindex());
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (checkResult.charAt(0) != '0') {
			return checkResult;
		}
		return apply(channel, null);
	}

	/**
	 * @param channel
	 *            CmsChannel
	 * @param priority
	 *            String
	 * @return String
	 * @throws Exception
	 */
	public String apply(CmsChannel channel, String priority) throws Exception {
		log.debug("workflow for service update starting...");
		CmsChannel channelBasic = new CmsChannel();
		channelBasic.setChannelindex(channel.getChannelindex());
		try {
			channelBasic = channelds.getCmsChannel(channelBasic);
			if (channelBasic == null) {
				return ResourceMgt
						.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS);// "1:频道已不存在";
			}
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int basicStatus = channelBasic.getStatus(); // 在修改状态之前得到基本表的状态

		channelBasic.setStatus(120); // 审核中
		channelBasic.setWorkflow(2); // 1：新增，2：修改
		channelBasic.setWorkflowlife(0);
		try {
			channelds.updateCmsChannel(channelBasic);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List list = new ArrayList();
		Physicalchannel ph = new Physicalchannel();
		ph.setChannelid(channelBasic.getChannelid());
		list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Physicalchannel ch = (Physicalchannel) list.get(i);
				ch.setStatus(channelBasic.getStatus());
				ch.setWorkflow(channelBasic.getWorkflow()); // 1：新增，2：修改
				ch.setWorkflowlife(channelBasic.getWorkflowlife());
				try {
					physicalchannelDS.updatePhysicalchannel(ch);
				} catch (DomainServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// 修改子内容状态
			}
		}

		// 把修改后的数据写入buf表
		CmsChannelBuf channelDBBuf = new CmsChannelBuf();
		BeanCopier basicObjDBCopy = BeanCopier.create(CmsChannel.class,
				CmsChannelBuf.class, true);
		basicObjDBCopy.copy(channelBasic, channelDBBuf,
				new BeanPropertyConvert());

		try {
			channelBufds.insertCmsChannelBuf(channelDBBuf);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 不需要index，直接写进去

		CmsChannelBuf channelbuf = new CmsChannelBuf();
		BeanCopier basicCopy = BeanCopier.create(CmsChannel.class,
				CmsChannelBuf.class, false);
		basicCopy.copy(channel, channelbuf, null);
		channelbuf.setWorkflow(Integer.valueOf(2));// 在这里将修改的数据存入buf表
		channelbuf.setStatus(basicStatus); // 把基本表的状态存在buf表中
		try {
			channelBufds.updateCmsChannelBuf(channelbuf);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// 发起工作流参数
		String pboName = channelbuf.getChannelname() + "["
				+ channelbuf.getChannelid() + "]";
		String pboId = String.valueOf(channel.getChannelindex());
		String pboType = ChannelConstants.getWorkflowType(channelBasic
				.getWorkflow());
		String processName = pboType + ":" + pboName;

		String callbackBean = "";
		if (null != this.getCallbackProcess()) {
			callbackBean = "callbackProcess," + this.getCallbackProcess();

		}

		WorkflowService wkflService = new WorkflowService();
		int flag = wkflService.startProcessInstance(this.getTemplateCode(),
				processName, pboId, pboName, pboType, "", callbackBean,
				priority);
		if (flag <= 0) {
			throw new Exception("1:工作流执行失败");
		}

		log.debug("workflow for service update ends...");
		return "0:"
				+ ResourceMgt
						.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_UPDATE_APPLY_SUCCESS);
	}

	/**
	 * @param channelBuf
	 *            CmsChannelBuf
	 * @param taskId
	 *            String
	 * @param handleMsg
	 *            String
	 * @param handleEvent
	 *            String
	 * @param workflowStatusCode
	 *            String
	 * @return String
	 * @throws Exception
	 */
	public String audit(CmsChannelBuf channelBuf, String taskId,
			String handleMsg, String handleEvent, String workflowStatusCode)
			throws Exception {

		String rtn = "";
		try {
			String checkResult = channelUpdateWkfwCheckLS
					.checkChannelUpdateWkfwAudit(channelBuf.getChannelindex());
			if (checkResult.charAt(0) != '0') {
				return checkResult;
			}
			// 审核时，有可能在页面做了修改，因此需要更新
			channelBufds.updateCmsChannelBuf(channelBuf);

			// 工作流处理
			WorkflowService wkflService = new WorkflowService();
			CmsChannel channel = new CmsChannel();
			wkflService.completeTask(taskId, handleMsg, handleEvent);

				// throw new DomainServiceException();
				// 工作流执行失败
			
				if ("通过".equals(handleEvent)) {

					channel.setChannelindex(channelBuf.getChannelindex());

					channelBuf = channelBufds.getCmsChannelBuf(channelBuf);
					channel = channelds.getCmsChannel(channel);
					// int status = channelBuf.getStatus();
					// if (status == 20 || status == 50 || status == 60) {//
					// 发布成功过的
					// rtn = updatePublishChannel(channel);
					// }
					CntTargetSync cntTargetSync = new CntTargetSync();
					cntTargetSync.setObjectindex(channel.getChannelindex());
					cntTargetSync.setObjecttype(5);

					List tarlist = new ArrayList();
					try {
						tarlist = cntTargetSyncDS
								.getCntTargetSyncByCond(cntTargetSync);
					} catch (DomainServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					List<CntTargetSync> publist = new ArrayList();
					if (tarlist != null && tarlist.size() > 0) {
						for (int i = 0; i < tarlist.size(); i++) {
							cntTargetSync = (CntTargetSync) tarlist.get(i);
							int status = cntTargetSync.getStatus();
							if (status == 300 || status == 500) {
								publist.add(cntTargetSync);
							}

						}
                        rtn = cmsChannelSyncLS.publishChannelcontentTar(publist);// 可以发布到的网元

					}

				}
				if (!"".equals(rtn)) {
					if ("success".equals(rtn)) {
						rtn = "0:"
								+ ResourceMgt
										.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_UPDATE_AUDIT_SUCCESS);
						// channel.setWorkflow(Integer.valueOf(0));
						// channelds.updateCmsChannel(channel);
					} else {
						throw new Exception(rtn);
					}
				} else {
					rtn = "0:"
							+ ResourceMgt
									.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_UPDATE_AUDIT_SUCCESS);
				}
				channelBufds.removeCmsChannelBuf(channelBuf);
				PhysicalchannelBuf buf = new PhysicalchannelBuf();
				buf.setChannelid(channelBuf.getChannelid());
				List list = physicalchannelbufDS
						.getPhysicalchannelBufByCond(buf);
				if (list != null && list.size() > 0) {
					buf = (PhysicalchannelBuf) list.get(0);
				}
				if (buf != null) {
					physicalchannelbufDS.removePhysicalchannelBuf(buf);
				}

			
		} catch (Exception e) {
			log.error("Error occurred while auditing ", e);

			String errorMessage = e.getMessage();

			// throw new Exception(ResourceManager.getResourceText(e));
			throw new Exception(errorMessage);

		}
		return rtn;
	}

	/**
	 * 
	 * @param channelBufds
	 *            ICmsChannelBufDS
	 */
	public void setCmsChannelBufds(ICmsChannelBufDS channelBufds) {
		this.channelBufds = channelBufds;
	}

	/**
	 * 审核页面展示的信息为修改后的信息
	 * 
	 * @param channelbuf
	 *            CmsChannelBuf
	 * @return CmsChannelBuf
	 * @throws Exception
	 *             Exception
	 */
	public CmsChannelBuf getChannelBuf(CmsChannelBuf channelbuf)
			throws Exception {
		CmsChannelBuf rsObj = null;
		try {
			rsObj = channelBufds.getCmsChannelBuf(channelbuf);
		} catch (Exception dsEx) {
			throw new Exception(dsEx.getMessage());
		}
		log.debug("get serviceList by pk end");
		return rsObj;
    }

}
package com.zte.cms.operlimit.model;

public class OperRole {
	int funcid;
	int parentid;
	int funcgrpid;
	String funcdescription;
	String servicekey;
	int opergrpid;
	int stepnum;

	public int getFuncid() {
		return funcid;
	}

	public void setFuncid(int funcid) {
		this.funcid = funcid;
	}

	public int getParentid() {
		return parentid;
	}

	public void setParentid(int parentid) {
		this.parentid = parentid;
	}

	public int getFuncgrpid() {
		return funcgrpid;
	}

	public void setFuncgrpid(int funcgrpid) {
		this.funcgrpid = funcgrpid;
	}

	

	public String getFuncdescription() {
		return funcdescription;
	}

	public void setFuncdescription(String funcdescription) {
		this.funcdescription = funcdescription;
	}

	public String getServicekey() {
		return servicekey;
	}

	public void setServicekey(String servicekey) {
		this.servicekey = servicekey;
	}

	public int getOpergrpid() {
		return opergrpid;
	}

	public void setOpergrpid(int opergrpid) {
		this.opergrpid = opergrpid;
	}

	public int getStepnum() {
		return stepnum;
	}

	public void setStepnum(int stepnum) {
		this.stepnum = stepnum;
	}

}

package com.zte.cms.storageArea.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStorageareaLS
{
    /**
     * 新增CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public String insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 更新CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public String updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 批量更新CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException;

    /**
     * 删除CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 批量删除CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException;

    /**
     * 查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 根据条件查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return 满足条件的CmsStoragearea对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 查询已经绑定到该库区的空间 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryofBindedSpace(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 空间绑定后，修改空间表内的库区绑定属性 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public Map updatebindedSpaceArea(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    // 去绑定时，首先判断去绑定的空间下是否有文件，如果没有，则取绑定，如果有，则操作失败，返回
    public String releasebindedSpace(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    // 给定一个库区的类型，通过库区下的空间的优先级以及可用容量，返回一个可用地址
    public String getAddress(String storageAreatype) throws DomainServiceException;

    // 检查绑定的库区是否为全国文件交换区或者CP上传交换区
    public String bindCheckForTwoAreas(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    // 为了配合收录和拆条获取在线区地址，重新写一个只返回空间标识的方法
    public String getSpaceID(String storageAreatype) throws DomainServiceException;

    // 给定库区的类型，获取设备的ID
    public String getDeviceID(String storageAreatype) throws DomainServiceException;
}
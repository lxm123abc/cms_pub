package com.zte.ucm.log;

public abstract interface LoggerConfig
{
  public abstract int getLoggerLevel();

  public abstract void setLoggerLevel(int paramInt);

  public abstract LoggerOutter getLoggerOutter();

  public abstract void setLoggerOutter(LoggerOutter paramLoggerOutter);

  public abstract String getClassName();

  public abstract void setClassName(String paramString);
}
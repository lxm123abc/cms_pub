package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.dao.IScheduleWkfwhisDAO;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ScheduleWkfwhisDS extends DynamicObjectBaseDS implements IScheduleWkfwhisDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IScheduleWkfwhisDAO dao = null;

    public void setDao(IScheduleWkfwhisDAO dao)
    {
        this.dao = dao;
    }

    public void insertScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException
    {
        log.debug("insert channelWkfwhis starting...");
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_schedule_wkfwhis");
        scheduleWkfwhis.setHistoryindex(index);
        String historydate = ServiceConstants.getCurrentTimeString();
        scheduleWkfwhis.setHistorydate(historydate);
        try
        {

            dao.insertScheduleWkfwhis(scheduleWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert channelWkfwhis end");
    }

    public void updateScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException
    {
        log.debug("update channelWkfwhis by pk starting...");
        try
        {
            dao.updateScheduleWkfwhis(scheduleWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelWkfwhis by pk end");
    }

    public void updateScheduleWkfwhisList(List<ScheduleWkfwhis> scheduleWkfwhisList) throws DomainServiceException
    {
        log.debug("update channelWkfwhisList by pk starting...");
        if (null == scheduleWkfwhisList || scheduleWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in channelWkfwhisList");
            return;
        }
        try
        {
            for (ScheduleWkfwhis scheduleWkfwhis : scheduleWkfwhisList)
            {
                dao.updateScheduleWkfwhis(scheduleWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update channelWkfwhisList by pk end");
    }

    public void removeScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException
    {
        log.debug("remove channelWkfwhis by pk starting...");
        try
        {
            dao.deleteScheduleWkfwhis(scheduleWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelWkfwhis by pk end");
    }

    public void removeScheduleWkfwhisList(List<ScheduleWkfwhis> scheduleWkfwhisList) throws DomainServiceException
    {
        log.debug("remove channelWkfwhisList by pk starting...");
        if (null == scheduleWkfwhisList || scheduleWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in channelWkfwhisList");
            return;
        }
        try
        {
            for (ScheduleWkfwhis scheduleWkfwhis : scheduleWkfwhisList)
            {
                dao.deleteScheduleWkfwhis(scheduleWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove channelWkfwhisList by pk end");
    }

    public ScheduleWkfwhis getScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException
    {
        log.debug("get channelWkfwhis by pk starting...");
        ScheduleWkfwhis rsObj = null;
        try
        {
            rsObj = dao.getScheduleWkfwhis(scheduleWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelWkfwhisList by pk end");
        return rsObj;
    }

    public List<ScheduleWkfwhis> getScheduleWkfwhisByCond(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException
    {
        log.debug("get channelWkfwhis by condition starting...");
        List<ScheduleWkfwhis> rsList = null;
        try
        {
            rsList = dao.getScheduleWkfwhisByCond(scheduleWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get channelWkfwhis by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get channelWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(scheduleWkfwhis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelWkfwhis page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get channelWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(scheduleWkfwhis, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get channelWkfwhis page info by condition end");
        return tableInfo;
    }
}

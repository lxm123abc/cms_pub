package com.zte.cms.content.relation.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsContentRelation extends DynamicBaseObject
{
    private java.lang.Long cgindex;
    private java.lang.String cgid;
    private java.lang.String cgname;
    private java.lang.String contentid;
    private java.lang.String description;
    private java.lang.String cpid;
    private java.lang.String operid;

    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Long getCgindex()
    {
        return cgindex;
    }

    public void setCgindex(java.lang.Long cgindex)
    {
        this.cgindex = cgindex;
    }

    public java.lang.String getCgid()
    {
        return cgid;
    }

    public void setCgid(java.lang.String cgid)
    {
        this.cgid = cgid;
    }

    public java.lang.String getCgname()
    {
        return cgname;
    }

    public void setCgname(java.lang.String cgname)
    {
        this.cgname = cgname;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public void initRelation()
    {
        this.addRelation("cgindex", "CGINDEX");
        this.addRelation("cgid", "CGID");
        this.addRelation("cgname", "CGNAME");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("description", "DESCRIPTION");
    }
}

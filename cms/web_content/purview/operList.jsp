<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    try {
        String operID = (String)session.getAttribute("OPERID");
        ArrayList list = new ArrayList();
        HashMap map = new HashMap();
        String operflag = request.getParameter("op") == null ? "" : (String)request.getParameter("op");
        String tempname = request.getParameter("opername") == null ? "" : (String)request.getParameter("opername");
        String filter = request.getParameter("filter");
        if(filter == null){
           filter = new String("0");
        }
        String name = tempname.trim();
        String serflag = request.getParameter("serflag") == null ? (session.getAttribute("OPER_TYPE_ATTRIBUTE")==null? "0":(String)session.getAttribute("OPER_TYPE_ATTRIBUTE")) : (String)request.getParameter("serflag");
        int thepage = request.getParameter("page") == null ? 0 : Integer.parseInt((String)request.getParameter("page"));
        Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
        
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        purview.setServiceKey(serviceKey);
        String operType=null;
        String spCode=null; 
        if ( operID != null ){
           if(session.getAttribute("OPER_TYPE_ATTRIBUTE")!= null && session.getAttribute("YWYY_CODE_ATTRIBUTE")!= null){
             operType = (String)session.getAttribute("OPER_TYPE_ATTRIBUTE");
             spCode = (String)session.getAttribute("YWYY_CODE_ATTRIBUTE");
             list = purview.sortChildOperators(operID,operType,spCode,filter);
           }
           else if("search".equals(operflag)){
             list = purview.sortChildOperators2(operID,name,serflag,filter);
             
        %>
						<script language="javascript">
						//parent.buttonFrame.mm.style.display = "block";
						//add.style.display = "block";
						//parent.buttonFrame.back.style.display = "none";
						//parent.buttonFrame.document.view.pageflag.value = "";
						</script>
        <%             
           }
           else{
             list = purview.sortChildOperators(operID,filter);
           }
           if (list == null)
              list = new ArrayList();

           int records = 30;
           int pages = list.size()/records;
           if(list.size()%records>0)
              pages = pages + 1;

          String idstr = "";
%>
<html>
<head>
<title><fmt:message key="operCurrentGroup.opermanage"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="JsFun.jsp" flush="" />
<script language="javascript">
   // ?��D?��???��??�쨢D����?��D??��
   function refresh () {
      parent.buttonFrame.document.view.operName.value = '';
      parent.buttonFrame.document.view.serviceKey.value = '';
      parent.buttonFrame.document.view.serviceName.value = '';
      parent.buttonFrame.document.view.unlock.disabled = true;
      parent.buttonFrame.document.view.del.disabled = true;
      parent.buttonFrame.document.view.attr.disabled = true;
      parent.buttonFrame.document.view.signOut.disabled = true;
      parent.groupFrame.document.view.operID.value = '';
      parent.currentFrame.document.view.operID.value = '';
      document.view.submit();
   }

   function refresh2 (name,serflag) {
      parent.buttonFrame.document.view.operName.value = '';
      parent.buttonFrame.document.view.serviceKey.value = '';
      parent.buttonFrame.document.view.serviceName.value = '';
      parent.buttonFrame.document.view.unlock.disabled = true;
      parent.buttonFrame.document.view.del.disabled = true;
      parent.buttonFrame.document.view.attr.disabled = true;
      parent.buttonFrame.document.view.signOut.disabled = true;

      document.view.opername.value = name;
//      document.view.serflag.value = serflag;
//      document.view.op.value = 'search';
      parent.groupFrame.refresh();
      parent.currentFrame.refresh();
      document.view.submit();
   }

   // ???��???����??���?����o�����騦����??������
   function viewOper (operID,operName,operStat,idForm) {
      var tempID = parent.buttonFrame.document.view.idid.value;
      document.getElementById(idForm).bgColor = '#FF66CC';
      if((tempID!=null) && (tempID!='') && (tempID!=idForm)) {
        document.getElementById(tempID).bgColor = '';
      }
      document.view.operID.value = operID;
      if (parent.buttonFrame.checkWin() == 'no') {
         parent.buttonFrame.document.view.operID.value = operID;
         parent.buttonFrame.document.view.operName.value = operName;
         parent.buttonFrame.document.view.serviceName.value = '';
         parent.buttonFrame.document.view.signOut.disabled = true;
         parent.buttonFrame.document.view.del.disabled = false;
         parent.buttonFrame.document.view.attr.disabled = false;
         parent.buttonFrame.document.view.signOut.disabled = true;
         try{
         if(parseInt(operStat)==0){
             parent.buttonFrame.document.view.unlock.disabled = true;
         
         }
         else{
             parent.buttonFrame.document.view.unlock.disabled = false;
         
         }
         }catch(err){}
         parent.groupFrame.document.view.operID.value = operID;
         parent.groupFrame.refresh();
         parent.currentFrame.document.view.operID.value = operID;
         parent.buttonFrame.document.view.idid.value = idForm;
         parent.currentFrame.refresh();
      }
      if (operID == '<%= operID %>') {
	      try{
         parent.buttonFrame.document.view.unlock.disabled = true;
	      }catch(err){}   
         parent.buttonFrame.document.view.del.disabled = true;
         parent.buttonFrame.document.view.attr.disabled = false;
         parent.buttonFrame.document.view.signOut.disabled = true;
         parent.buttonFrame.document.view.add.disabled = true;
      }
   }

   function toPage (page) {
      document.view.page.value = page;

      parent.buttonFrame.document.view.unlock.disabled = true;
      parent.buttonFrame.document.view.del.disabled = true;
      parent.buttonFrame.document.view.attr.disabled = true;
      parent.buttonFrame.document.view.signOut.disabled = true;
      parent.buttonFrame.document.view.operName.value = "";
      parent.buttonFrame.document.view.serviceName.value = "";


      parent.groupFrame.refresh();
      parent.currentFrame.refresh();
      parent.buttonFrame.document.view.idid.value = '';
      document.view.submit();
   }

   function goPage(){
      var fm = document.view;
      var pages = parseInt(fm.pages.value);
      var thepage =parseInt(trim(fm.gopage.value));
      if(thepage==''){
         alert("<fmt:message key="operListRights.pagerequried"/>")
         fm.gopage.focus();
         return;
      }
      if(!checkstring('0123456789',thepage)){
         alert("<fmt:message key="operListRights.pageformatillegal"/>")
         fm.gopage.focus();
         return;
      }
      if(thepage<=0 || thepage>pages ){
         alert("<fmt:message key="operListRights.pagerangeillegal"/>")
         fm.gopage.focus();
         return;
      }
      thepage = thepage -1;
      toPage(thepage);
   }

   function searchRing() {
      var fm = document.view;
      fm.op.value = 'search';
      fm.page.value = 0;
      
      parent.buttonFrame.document.view.unlock.disabled = true;
      parent.buttonFrame.document.view.del.disabled = true;
      parent.buttonFrame.document.view.attr.disabled = true;
      parent.buttonFrame.document.view.signOut.disabled = true;
      parent.buttonFrame.document.view.operName.value = "";
      parent.buttonFrame.document.view.serviceName.value = "";

      parent.groupFrame.refresh();
      parent.currentFrame.refresh();
      parent.buttonFrame.document.view.idid.value = '';
     document.view.submit();
   }

</script>
</head>
<body class="body-style1">
<form name="view" method="post" action="operList.jsp">
<input type="hidden" name="operID" value="">
<input type="hidden" name="expirepassword" value="">
<input type="hidden" name="page" value="<%= thepage %>">
<input type="hidden" name="pages" value="<%= pages %>">
<input type="hidden" name="op" value="<%= operflag %>">

<table border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style2" height=hei >
  <tr align="center">
	   <fmt:message key="operDel.opername"/>
	      <input type="text" name="opername" value="<%= name %>" maxlength="30" size="15" class="input-style7" >
	   <fmt:message key="operInfo.operatortype"/>
	      <select name="serflag">
	        <option value="0" ><fmt:message key="operListRights.systemmanager"/></option>
	        <%if(purview.showSpCategory()){%>
	        <option value="1" <%= serflag.equals("1")? "selected":""%>><fmt:message key="operListRights.cpspmanager"/></option>
	        <%} if(purview.showGrpCategory()){%>
	        <option value="3" <%= serflag.equals("2")? "selected":""%>><fmt:message key="operInfo.grpmanager"/></option>
	        <%}%>
	      </select>
	      <%
	         if(purview.showExpirePassword()){
	      %>
	           <fmt:message key="operList.operatorstatus"/>
	           <select  name="filter" />
	            <option value="0" <%= filter.equals("0")? "selected":""%>><fmt:message key="operList.normalstate"/></option>
	            <option value="1" <%= filter.equals("1")? "selected":""%>><fmt:message key="operList.blacklist"/></option>
	            <option value="2" <%= filter.equals("2")? "selected":""%>><fmt:message key="operList.locked"/></option>	            
	            <option value="3" <%= filter.equals("3")? "selected":""%>><fmt:message key="operList.inactive"/></option>
	           </select>
	      <%
	         }
	      %>
  </tr>
  <img src="button/search.gif" alt="<fmt:message key="operListRights.queryoperator"/>" onmouseover="this.style.cursor='hand'" onclick="javascript:searchRing()">
  <br>
<script language="javascript">
var serflagValue = '<%=serflag%>';
document.view.serflag.value = serflagValue;
</script>
  <tr class="table-title1" >
    <td width="150" height="24"><fmt:message key="operInfo.loginname"/></td>
    <td width="150" height="24"><fmt:message key="operDel.operfullname"/></td>
    <td width="200" height="24"><fmt:message key="operDel.description"/></td>
    <td width="150" height="24"><fmt:message key="operList.creator"/></td>
    <td width="150" height="24"><fmt:message key="operList.operatorstatus"/></td>
  </tr>
<script language="JavaScript">
	if(parent.parent.frames.length>0){
		var hei;

<%
        int temp = records-((thepage+1) * records-list.size());
	if(list!=null && (list.size()<=12||temp<12)){
%>
	hei = 400;
<%
	}else if (list!=null && temp>12 && temp<records) {
%>
	hei=400+(<%=temp%>*25);
<%
	}else{
%>
         hei=20*50;
<%
}
%>
		//parent.parent.document.all.main.style.height=hei+150;

		}
</script>
<%
        for (int i = thepage * records; i < thepage * records +records && i < list.size(); i++) {
            map = (HashMap)list.get(i);
            idstr = "table_color"+i;
%>
  <tr  id="<%=idstr%>"  bgcolor="" ondblclick="javascript:viewOper('<%= (String)map.get("OPERID") %>','<%= (String)map.get("OPERNAME")%>','<%= (String)map.get("OPERSTATUS")%>','<%=idstr%>')">
    <td height="22"><%= (String)map.get("OPERNAME") %>&nbsp;</td>
    <td height="22"><%= (String)map.get("OPERALLNAME") %>&nbsp;</td>
    <td height="22"><%= (String)map.get("OPERDESCRIPTION") %>&nbsp;</td>
    <td height="22"><%= (String)map.get("CREATORNAME") %>&nbsp;</td>
<%
     int stat = 0;
     stat = Integer.parseInt((String)map.get("OPERSTATUS"));
     String strTemp = i18n.getMessage("operList.cl.normal");
     if (stat ==1)
       strTemp = i18n.getMessage("operList.cl.locked");
     else if(stat == 2)
       strTemp = i18n.getMessage("operList.cl.blacklist");
     else  if(stat == 3)
       strTemp = i18n.getMessage("operList.cl.forbidden");
     else if(stat>9)
       strTemp = i18n.getMessage("operList.cl.blacklist");
     out.println("<td height=22>"+strTemp + "&nbsp;</td>");
     out.println("</tr>");
     }
%>
<%
        if (list.size() > records) {
%>
  <tr>
      <table border="0" cellspacing="1" cellpadding="1" align="center" class="table-style2">
        <tr>
          <td><fmt:message key="operLog.total"/>&nbsp;<%= list.size() %>&nbsp;<fmt:message key="operLog.record"/>&nbsp;<%= list.size()%records==0?list.size()/records:list.size()/records+1 %>&nbsp;<fmt:message key="operLog.page"/>&nbsp;&nbsp;<fmt:message key="operLog.nowpage"/>&nbsp;<%= thepage + 1 %>&nbsp;<fmt:message key="operLog.page"/></td>
          <td><img src="button/firstpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:toPage(0)"></td>
          <td><img src="button/prepage.gif"<%= thepage == 0 ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(" + (thepage - 1) + ")\"" %>></td>
          <td><img src="button/nextpage.gif"<%= thepage * records + records >= list.size() ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(" + (thepage + 1) + ")\"" %>></td>
          <td><img src="button/endpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:toPage(<%= (list.size() - 1) / records %>)"></td>
        </tr>
        <tr>
          <td colspan="5" align="right" >
            <table border="0" cellspacing="1" cellpadding="1" align="right" class="table-style2">
            <tr>
             <td ><fmt:message key="operListRights.pagenumber"/>&nbsp;</td>
             <td> <input style=text value="<%= String.valueOf(thepage+1) %>" name="gopage" maxlength=5 class="input-style4" > </td>
             <td ><img src="button/go.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:goPage()"  ></td>
            </tr>
            </table>
          </td>
       </tr>
      </table>
  </tr>

<%
        }
%>
</table>
</form>
<%

        }
        else {

          if(operID== null){
              %>
              <script language="javascript">
                    alert( "<fmt:message key="allotrights.loginfirst"/>");
                    parent.document.URL = '../enter.jsp';
              </script>
              <%
                      }
                      else{
               %>
              <script language="javascript">
                   alert( "<fmt:message key="allotrights.forbidden"/>");
              </script>
              <%

              }
         }
    }
    catch(Exception e) {
%>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5" height=400>
  <tr>
    <td><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
<%
    }
%>
<script language="javascript">
   if (document.view.operID.value != '') {
      parent.groupFrame.refresh();
      parent.currentFrame.document.view.operID.value = document.view.operID.value;
      parent.currentFrame.refresh();
   }
</script>
</body>
<html>

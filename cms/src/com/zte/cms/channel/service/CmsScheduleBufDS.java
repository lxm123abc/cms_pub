package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.cms.channel.dao.ICmsScheduleBufDAO;
import com.zte.cms.channel.service.ICmsScheduleBufDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsScheduleBufDS implements ICmsScheduleBufDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private ICmsScheduleBufDAO dao = null;
	
	public void setDao(ICmsScheduleBufDAO dao)
	{
	    this.dao = dao;
	}

	public void insertCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException
	{
		log.debug("insert cmsScheduleBuf starting...");
		try
		{
			dao.insertCmsScheduleBuf( cmsScheduleBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert cmsScheduleBuf end");
	}
	
	public void updateCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException
	{
		log.debug("update cmsScheduleBuf by pk starting...");
	    try
		{
			dao.updateCmsScheduleBuf( cmsScheduleBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cmsScheduleBuf by pk end");
	}

	public void updateCmsScheduleBufList( List<CmsScheduleBuf> cmsScheduleBufList )throws DomainServiceException
	{
		log.debug("update cmsScheduleBufList by pk starting...");
		if(null == cmsScheduleBufList || cmsScheduleBufList.size() == 0 )
		{
			log.debug("there is no datas in cmsScheduleBufList");
			return;
		}
	    try
		{
			for( CmsScheduleBuf cmsScheduleBuf : cmsScheduleBufList )
			{
		    	dao.updateCmsScheduleBuf( cmsScheduleBuf );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update cmsScheduleBufList by pk end");
	}



	public void removeCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException
	{
		log.debug( "remove cmsScheduleBuf by pk starting..." );
		try
		{
			dao.deleteCmsScheduleBuf( cmsScheduleBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cmsScheduleBuf by pk end" );
	}

	public void removeCmsScheduleBufList ( List<CmsScheduleBuf> cmsScheduleBufList )throws DomainServiceException
	{
		log.debug( "remove cmsScheduleBufList by pk starting..." );
		if(null == cmsScheduleBufList || cmsScheduleBufList.size() == 0 )
		{
			log.debug("there is no datas in cmsScheduleBufList");
			return;
		}
		try
		{
			for( CmsScheduleBuf cmsScheduleBuf : cmsScheduleBufList )
			{
				dao.deleteCmsScheduleBuf( cmsScheduleBuf );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove cmsScheduleBufList by pk end" );
	}



	public CmsScheduleBuf getCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException
	{
		log.debug( "get cmsScheduleBuf by pk starting..." );
		CmsScheduleBuf rsObj = null;
		try
		{
			rsObj = dao.getCmsScheduleBuf( cmsScheduleBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cmsScheduleBufList by pk end" );
		return rsObj;
	}

	public List<CmsScheduleBuf> getCmsScheduleBufByCond( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException
	{
		log.debug( "get cmsScheduleBuf by condition starting..." );
		List<CmsScheduleBuf> rsList = null;
		try
		{
			rsList = dao.getCmsScheduleBufByCond( cmsScheduleBuf );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cmsScheduleBuf by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get cmsScheduleBuf page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cmsScheduleBuf, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CmsScheduleBuf>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cmsScheduleBuf page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get cmsScheduleBuf page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(cmsScheduleBuf, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CmsScheduleBuf>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get cmsScheduleBuf page info by condition end" );
		return tableInfo;
	}
}

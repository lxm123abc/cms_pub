package com.zte.cms.content.batchUpload.ls;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IBatchUploadLS
{

    @SuppressWarnings("unchecked")
    public abstract String addBatchContents(Long cpindex, String cpid, String ip, String port, String user,
            String pass, String file, String licenseid, Integer contentType, Integer uploadType, String programType)
            throws DomainServiceException;

}
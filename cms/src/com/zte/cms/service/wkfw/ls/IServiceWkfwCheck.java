package com.zte.cms.service.wkfw.ls;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceWkfwCheck
{
    /**
     * 发起工作流之前对服务进行检查
     * 
     * @param serviceindex
     * @return
     * @throws DomainServiceException
     */
    public String checkServiceWkfw(Long serviceindex) throws DomainServiceException;

    /**
     * 审核操作之前对服务进行检查
     * 
     * @param serviceIndex
     * @return
     * @throws DomainServiceException
     */
    public String checkServiceWkfwAudit(Long serviceIndex) throws DomainServiceException;

}

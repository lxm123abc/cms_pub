package com.zte.cms.channel.service;

import java.util.ArrayList;
import java.util.List;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.dao.ICmsScheduleDAO;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.common.Generator;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.cms.common.Generator;

public class CmsScheduleDS implements ICmsScheduleDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private IPrimaryKeyGenerator primaryKeyGenerator = null;
    private ICmsScheduleDAO dao = null;
    private String scheduleId = "";

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public void setDao(ICmsScheduleDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("insert cmsSchedule starting...");
        try
        {
            Long scheduleindex = (Long) this.primaryKeyGenerator.getPrimarykey("cms_schedule");
            cmsSchedule.setScheduleindex(scheduleindex);
            scheduleId = Generator.getContentId(Long.parseLong(cmsSchedule.getCpid()), "SCHE");
            cmsSchedule.setScheduleid(scheduleId);
            cmsSchedule.setCpcontentid(scheduleId);
            dao.insertCmsSchedule(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsSchedule end");
    }

    public void updateCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("update cmsSchedule by pk starting...");
        try
        {
            dao.updateCmsSchedule(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedule by pk end");
    }

    public void updateCmsScheduleList(List<CmsSchedule> cmsScheduleList) throws DomainServiceException
    {
        log.debug("update cmsScheduleList by pk starting...");
        if (null == cmsScheduleList || cmsScheduleList.size() == 0)
        {
            log.debug("there is no datas in cmsScheduleList");
            return;
        }
        try
        {
            for (CmsSchedule cmsSchedule : cmsScheduleList)
            {
                dao.updateCmsSchedule(cmsSchedule);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsScheduleList by pk end");
    }

    public void updateCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("update cmsSchedule by condition starting...");
        try
        {
            dao.updateCmsScheduleByCond(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedule by condition end");
    }

    public void updateCmsScheduleListByCond(List<CmsSchedule> cmsScheduleList) throws DomainServiceException
    {
        log.debug("update cmsScheduleList by condition starting...");
        if (null == cmsScheduleList || cmsScheduleList.size() == 0)
        {
            log.debug("there is no datas in cmsScheduleList");
            return;
        }
        try
        {
            for (CmsSchedule cmsSchedule : cmsScheduleList)
            {
                dao.updateCmsScheduleByCond(cmsSchedule);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsScheduleList by condition end");
    }

    public void removeCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("remove cmsSchedule by pk starting...");
        try
        {
            dao.deleteCmsSchedule(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedule by pk end");
    }

    public void removeCmsScheduleList(List<CmsSchedule> cmsScheduleList) throws DomainServiceException
    {
        log.debug("remove cmsScheduleList by pk starting...");
        if (null == cmsScheduleList || cmsScheduleList.size() == 0)
        {
            log.debug("there is no datas in cmsScheduleList");
            return;
        }
        try
        {
            for (CmsSchedule cmsSchedule : cmsScheduleList)
            {
                dao.deleteCmsSchedule(cmsSchedule);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsScheduleList by pk end");
    }

    public void removeCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("remove cmsSchedule by condition starting...");
        try
        {
            dao.deleteCmsScheduleByCond(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedule by condition end");
    }

    public void removeCmsScheduleListByCond(List<CmsSchedule> cmsScheduleList) throws DomainServiceException
    {
        log.debug("remove cmsScheduleList by condition starting...");
        if (null == cmsScheduleList || cmsScheduleList.size() == 0)
        {
            log.debug("there is no datas in cmsScheduleList");
            return;
        }
        try
        {
            for (CmsSchedule cmsSchedule : cmsScheduleList)
            {
                dao.deleteCmsScheduleByCond(cmsSchedule);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsScheduleList by condition end");
    }

    public CmsSchedule getCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("get cmsSchedule by pk starting...");
        CmsSchedule rsObj = null;
        try
        {
            rsObj = dao.getCmsSchedule(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsScheduleList by pk end");
        return rsObj;
    }

    public List<CmsSchedule> getCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("get cmsSchedule by condition starting...");
        List<CmsSchedule> rsList = null;
        try
        {
            rsList = dao.getCmsScheduleByCond(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSchedule by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsSchedule page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSchedule, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedule>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedule page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryForRecrod(CmsSchedule cmsSchedule, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsSchedule page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryForRecord(cmsSchedule, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedule>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedule page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsSchedule page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSchedule, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedule>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedule page info by condition end");
        return tableInfo;
    }

    /**
     * 查询同一个频道下时间是否有重复
     * 
     * @param icmChannelSchedule
     * @return
     */
    public List<CmsSchedule> isTimeRepeated(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        List<CmsSchedule> list = new ArrayList<CmsSchedule>();
        try
        {
            list = dao.isTimeRepeated(cmsSchedule);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;

    }

    @Override
    public List<CmsSchedule> getCmsScheduleByID(CmsSchedule cmsSchedule) throws DomainServiceException
    {
        log.debug("get cmsSchedule by schedulid starting...");
        List<CmsSchedule> rsList = null;
        try
        {
            rsList = dao.getCmsScheduleByID(cmsSchedule);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSchedule by schedulid end");
        return rsList;
    }
}

package com.zte.cms.task.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import com.zte.cms.notice.util.FtpAttachment;
import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.cms.settlementDataRpt.common.ExcelUtils;
import com.zte.cms.settlementDataRpt.common.SettlementDataRptConstants;
import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;
import com.zte.cms.task.dao.ICmsTaskAuditRecordDAO;
import com.zte.cms.task.model.TaskCmsauditRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import com.zte.cms.common.DbUtil;
import com.zte.cms.notice.dao.INoticemapDAO;
import com.zte.cms.notice.model.Noticemap;
import com.zte.cms.task.dao.ICmsTaskBasicDAO;
import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskFunction;
import com.zte.ismp.common.ConfigUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import com.zte.ucm.util.EmailServiceUtil;
import com.zte.ucm.util.KwCheck;

import javax.servlet.http.HttpServletResponse;

public class CmsTaskBasicDS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ITaskBasicDS {
    private Log log = SSBBus.getLog(getClass());
    private static String emailInfo = StringUtils.trimToEmpty(ConfigUtil.get("email_smtp_info"));

    private ICmsTaskBasicDAO taskDao = null;
    private ICmsTaskAuditRecordDAO auditRecordDAO;

    public void setAuditRecordDAO(ICmsTaskAuditRecordDAO auditRecordDAO) {
        this.auditRecordDAO = auditRecordDAO;
    }

    public void setTaskDao(ICmsTaskBasicDAO taskDao) {
        this.taskDao = taskDao;
    }

    private INoticemapDAO dao = null;

    public void setDao(INoticemapDAO dao) {
        this.dao = dao;
    }

    /**
     * 根据条件分页查询taskBasic对象
     *
     * @param taskBasic taskBasic对象，作为查询条件的参数
     * @param start     起始行
     * @param pageSize  页面大小
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize) throws DomainServiceException {
        log.debug("get taskBasic page info by condition starting...");
        PageInfo pageInfo = null;
        try {
            pageInfo = taskDao.pageInfoQuery(taskBasic, start, pageSize);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<TaskCmsaudit>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get taskBasic page info by condition end");
        return tableInfo;
    }

    /**
     * 根据条件分页查询taskBasic对象
     *
     * @param taskBasic taskBasic对象，作为查询条件的参数
     * @param start     起始行
     * @param pageSize  页面大小
     * @param puEntity  排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException {
        log.debug("get taskBasic page info by condition starting...");
        PageInfo pageInfo = null;
        try {
            pageInfo = taskDao.pageInfoQuery(taskBasic, start, pageSize, puEntity);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();

        tableInfo.setData((List<TaskCmsaudit>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get taskBasic page info by condition end");
        return tableInfo;
    }

    public String insertTask(TaskCmsaudit taskBasic) throws DomainServiceException {
        log.info("insert taskBasic starting...");
        Long taskid = null;
        Long attachid = null;
        try {
            taskid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("taskid_index");
            taskBasic.setTaskid(Integer.parseInt(taskid + ""));
            taskDao.insertTask(taskBasic);

            insertAttach(Integer.parseInt(taskid + ""), taskBasic.getStep2());
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("insert taskBasic end");
        return taskid + "";
    }

    public void insertAttach(Integer taskid, Integer step2) throws DAOException {
        Long attachid;
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        Iterator<FileItem> iter = fileList.iterator();
        while (iter.hasNext()) {
            FileItem tmp = (FileItem) iter.next();
            String attachname = tmp.getName();
            if (StringUtils.isNotEmpty(attachname)) {
                Noticemap noticemap = new Noticemap();
                noticemap.setMessageid(taskid);
                attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
                noticemap.setAttachname(attachname);
                noticemap.setAttachid(Integer.parseInt(attachid + ""));
                noticemap.setAttachurl(attachid + attachname.substring(attachname.lastIndexOf(".")));
                //noticemap.setStep2(step2);
                dao.insertAttach(tmp, noticemap);
            }
        }
    }

    public TaskCmsaudit getTaskmap(TaskCmsaudit taskmap) throws DomainServiceException {
        log.info("get getTaskmap by pk starting...");
        TaskCmsaudit rsObj = null;
        try {
            rsObj = taskDao.getTaskmap(taskmap);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("get getTaskmap by pk end");
        return rsObj;
    }

    public void updateTaskmap(TaskCmsaudit noticemap) throws DomainServiceException {
        log.info("update noticemap by pk starting...");
        try {
            taskDao.updateTaskmap(noticemap);
            insertAttach(noticemap.getTaskid(), noticemap.getStep2());
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("update noticemap by pk end");
    }

    public List<TaskCmsaudit> getattchList(TaskCmsaudit taskmap) throws DomainServiceException {
        log.debug("get getattchList page info by condition starting...");
        List<TaskCmsaudit> rsList = null;
        try {
            rsList = taskDao.getattchList(taskmap);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getattchList by condition end");
        return rsList;
    }

    public void deleteAttach(String attachid) throws DomainServiceException {
        Noticemap noticemap = new Noticemap();
        log.info("remove attachment starting..., attachid=" + noticemap.getAttachid());
        try {
            TaskCmsaudit taskmap = taskDao.getAttachByid(attachid);
            noticemap.setAttachid(Integer.parseInt(attachid));
            noticemap.setAttachurl(taskmap.getAttachurl());
            dao.deleteAttach(noticemap);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove Attachment end");
    }

    public void deleteTaskBasic(TaskCmsaudit taskmap) throws DomainServiceException {
        log.info("remove deleteTaskBasic starting..., taskid=" + taskmap.getTaskid());
        try {
            taskDao.deleteTaskBasic(taskmap);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove deleteTaskBasic end");
    }

    public void toAuditTask(TaskCmsaudit taskmap) throws DomainServiceException {
        log.info("remove toAuditTask starting..., taskid=" + taskmap.getTaskid());
        try {
            TaskCmsauditRecord record = new TaskCmsauditRecord();
            Long id = null;
            //登记审批记录
            if (taskmap.getAuditdesc() != null) {
                id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("taskAuditRecordid_index");
                record.setId(id.intValue());
                record.setTaskid(taskmap.getTaskid());
                record.setAuditdesc(taskmap.getAuditdesc());
                record.setStep1(taskmap.getStep1());
                record.setStep2(taskmap.getStep2());
                record.setAuditstatus(taskmap.getFlag());
                boolean insert = auditRecordDAO.insert(record);
            }
            //查询审核员权限信息
            int step1 = 100;       //初始化 大流程100开始
            int step2 = 100001;    //小流程funcid, 小流程100001开始
            int updaterid = 0; //当前角色opergrpid
            int lastid = 0;    //上一审批角色opergrpid
            int stepnum = 1;   //流程总步数
            int status = taskmap.getStatus();//获取当前状态 0 待审批 1 审批中 2 待CP提交 3 已完成 4驳回
            String step1name = "";
            String step2name = "";


            TaskCmsaudit taskmap2 = new TaskCmsaudit();
            taskmap2.setTaskid(taskmap.getTaskid());
            // 提交审核
            if (0 == status) {
                //获取操作员流程权限类型表，根据小流程funcid
                TaskFunction taskf = new TaskFunction();
                taskf.setFuncid(step2);
                taskf = taskDao.getTaskFunction(taskf);
                updaterid = taskf.getOpergrpid();
                taskmap2.setStatus(1);
                taskmap2.setStep1(step1);
                taskmap2.setStep2(step2);
                taskmap2.setLastid(0);
                taskmap2.setUpdaterid(updaterid);
                taskmap2.setAuditdesc("");
                step2name = taskf.getFuncdescription();
            } else if (1 == status || 4 == status) {  //审核或者驳回状态下，门户继续点击审核
                int flag = taskmap.getFlag();
                step1 = taskmap.getStep1();
                step2 = taskmap.getStep2();
                //驳回
                if (1 == flag) {
                    //获取操作员流程权限类型表，根据小流程funcid
                    TaskFunction taskf = new TaskFunction();
                    taskf.setFuncid(step2);
                    taskf = taskDao.getTaskFunction(taskf);
                    lastid = taskf.getOpergrpid();
                    taskmap2.setStep1(step1);
                    int temp = Integer.parseInt(step1 + "001");
                    if (temp == step2) {  //如果第一个子流程与当前流程一致，状态改为2
                        taskmap2.setStatus(2);
                        taskmap2.setStep2(step2);
                        taskmap2.setUpdaterid(0);
                    } else {       //如果第一个子流程与当前流程不一致，状态改为4，并退一级，到上一子流程
                        taskmap2.setLastid(lastid);
                        taskmap2.setStatus(4);
                        taskmap2.setStep2(step2 - 1);
                        //获取操作员流程权限类型表，根据小流程funcid
                        taskf = new TaskFunction();
                        taskf.setFuncid(step2 - 1);
                        taskf = taskDao.getTaskFunction(taskf);
                        updaterid = taskf.getOpergrpid();
                        taskmap2.setUpdaterid(updaterid);
                    }
                    taskmap2.setAuditdesc(taskmap.getAuditdesc());
                    step2name = taskf.getFuncdescription();
                } else {
                    //获取操作员流程权限类型表，根据小流程funcid，//获取上一审批人的角色
                    TaskFunction taskf = new TaskFunction();
                    taskf.setFuncid(step2);
                    taskf = taskDao.getTaskFunction(taskf);

                    //获取当前大流程总步数
                    stepnum = taskf.getStepnum();
                    int temp = Integer.parseInt(step1 + "000") + stepnum;
                    //判断当前子流程小于总流程，则继续继续到下一子流程
                    if (step2 < temp) {
                        step2 += 1;
                        taskmap2.setAuditdesc(taskmap.getAuditdesc());
                    } else if (step2 == temp) {
                        status = 2;  //状态为待CP重新提交，标示大流程结束，页面不能审批
                        step1 += 1;
                        step2 = Integer.parseInt(step1 + "001");
                        taskmap2.setAuditdesc("");
                    }
                    lastid = taskf.getOpergrpid();

                    //获取当前审批人的角色
                    taskf = new TaskFunction();
                    taskf.setFuncid(step2);
                    taskf = taskDao.getTaskFunction(taskf);
                    //获取不到子流程，表明所有流程结束
                    if (null == taskf) {
                        taskmap2.setStatus(3);
                        taskmap2.setLastid(0);
                        taskmap2.setUpdaterid(0);
                    } else {
                        updaterid = taskf.getOpergrpid();
                        taskmap2.setStatus(status);
                        taskmap2.setStep1(step1);
                        if (2 == status) {
                            taskmap2.setStep2(step2);
                            taskmap2.setLastid(lastid);
                            taskmap2.setUpdaterid(0);
                        } else {
                            taskmap2.setStep2(step2);
                            taskmap2.setLastid(lastid);
                            taskmap2.setUpdaterid(updaterid);
                        }
                        step2name = taskf.getFuncdescription();
                    }
                }

            } else if (2 == status) { //大流程结束，待cp继续提交状态
                TaskFunction taskf = new TaskFunction();
                taskf.setFuncid(Integer.parseInt(taskmap.getStep1() + "001"));
                taskf = taskDao.getTaskFunction(taskf);
                updaterid = taskf.getOpergrpid();
                taskmap2.setStatus(1);  //下面通过进入下一流程，继续审批操作
                taskmap2.setStep2(Integer.parseInt(taskmap.getStep1() + "001"));
                taskmap2.setAuditdesc("");
                taskmap2.setUpdaterid(updaterid);
                taskmap2.setLastid(0);

            }
            taskDao.toAuditTask(taskmap2);

            if (3 != taskmap2.getStatus()) {
                TaskFunction taskf = new TaskFunction();
                taskf.setFuncid(step1);
                taskf = taskDao.getTaskFunction(taskf);
                step1name = taskf.getFuncdescription();
                String title = "上线任务：" + taskmap.getTaskname();
                String content = "您创建的上线任务：" + taskmap.getTaskname() + "，有新的进展，当前阶段是" + step1name + "--" + step2name;

                //sendEmail(title, content, taskmap.getCreatorid()+"", emailInfo);

                if (1 == taskmap2.getStatus()) {
                    sendOperidByFuncid(title, content, step2 + "");
                }
            }
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.info("remove deleteTaskBasic end");
    }

    public void sendOperidByFuncid(String title, String content, String funcid) {
        try {
            String sql = "select t1.operid from zxdbm_cms.operator_func t1,zxdbm_cms.cms_task_function t2 where t1.funcid=t2.funcid and t1.funcid=" + funcid;
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            Map<String, String> tmpMap = null;
            for (int i = 0; i < rtnlist.size(); i++) {
                tmpMap = (Map<String, String>) rtnlist.get(i);
                String operid = KwCheck.checkNull(tmpMap.get("operid")).toString();
                //发送邮件
                sendEmail(title, content, operid, emailInfo);
            }
        } catch (Exception exp) {
            log.error(exp);
        }
    }

    /**
     * 发送邮件
     *
     * @param title      邮件标题
     * @param content    邮件内容
     * @param operatorid 收件人
     * @param emailInfo  邮箱服务器信息
     */
    public void sendEmail(String title, String content, String operatorid, String emailInfo) {
        try {
            String sql = "select email from zxinsys.oper_information where operid = " + operatorid;
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(0);
            String email = KwCheck.checkNull(tmpMap.get("email")).toString();
            if (email != null && email != "") {
                //发送邮件
                EmailServiceUtil.send(title, content, email, emailInfo);
            } else {
                System.out.println("操作员：" + operatorid + "没有配置邮箱！");
            }
        } catch (Exception exp) {
            log.error(exp);
        }
    }

    public List<TaskFunction> getTaskFunctList(TaskFunction taskfunction) throws DomainServiceException {
        log.debug("get getTaskFunctList page info by condition starting...");
        List<TaskFunction> rsList = null;
        try {
            rsList = taskDao.getTaskFunctList(taskfunction);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getTaskFunctList by condition end");
        return rsList;
    }

    @Override
    public TableDataInfo pageAuditRecordInfoQuery(TaskCmsauditRecord record, int start, int pageSize) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        try {
            PageInfo pageInfo = auditRecordDAO.pageInfoQuery(record, start, pageSize);
            tableDataInfo.setTotalCount((int) pageInfo.getTotalCount());
            tableDataInfo.setData((List<TaskCmsauditRecord>) pageInfo.getResult());
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return tableDataInfo;
    }

    @Override
    public TaskCmsauditRecord exportAuditRecordByTaskId(TaskCmsauditRecord record, String userId, HttpServletResponse response) {
        try {
            PageInfo pageInfo = auditRecordDAO.pageInfoQuery(record, 0, 100);
            TaskCmsaudit taskCmsaudit = new TaskCmsaudit();
            taskCmsaudit.setTaskid(record.getTaskid());
            taskCmsaudit = taskDao.getTaskmap(taskCmsaudit);
            List<TaskCmsauditRecord> list = (List<TaskCmsauditRecord>) pageInfo.getResult();
            List<List<Map<String, Object>>> listMesh = new ArrayList<List<Map<String, Object>>>();
            List<Map<String, Object>> rowMesh = new ArrayList<Map<String, Object>>();
            String fileName = UserInfoUtils.getOperAllNameByOperId(taskCmsaudit.getCreatorid()) + SettlementDataRptConstants.Excel_AUDIT_RECORD;
            //第一行合并A1-J1
            Map<String, Object> cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, fileName);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 4);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_CENTER);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第二行第一个合并A2-C2
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出人员：" + userId);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 2);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);

            //第二行第二个合并H2-J2
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出日期：" + DateUtils.convertDataToString_fit(new Date(), DateUtils.fit_a));
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 3);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 4);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第三行合并A3-M3
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出记录：" + list.size() + "条");
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 4);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 2);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 2);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第四行合并A4-M4
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "任务名称:" + taskCmsaudit.getTaskname());
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 4);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 3);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 3);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            String[] lineName = {SettlementDataRptConstants.Excel_STEP1_CN, SettlementDataRptConstants.Excel_STEP2_CN, SettlementDataRptConstants.Excel_AUDIT_STATUS_CN,
                    SettlementDataRptConstants.Excel_AUDIT_DESC_CN, SettlementDataRptConstants.Excel_AUDIT_CREDT_CN};
            String[] lineEnName = {SettlementDataRptConstants.Excel_STEP1, SettlementDataRptConstants.Excel_STEP2, SettlementDataRptConstants.Excel_AUDIT_STATUS,
                    SettlementDataRptConstants.Excel_AUDIT_DESC, SettlementDataRptConstants.Excel_AUDIT_CREDT};

            List<TaskFunction> taskFunctList = this.getTaskFunctList(new TaskFunction());
            Map<String,String> funcMap = new HashMap<String, String>();
            for (TaskFunction taskFunction : taskFunctList) {
                funcMap.put(taskFunction.getFuncid()+"",taskFunction.getFuncdescription());
            }
            File file = ExcelUtils.getHSSFWorkbook2(list, listMesh, lineName, lineEnName, fileName,funcMap);
            if (file == null) {
                return null;
            }
            String path = file.getAbsolutePath();
            Long attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
            FileInputStream stream = new FileInputStream(path);
            String suffix = path.substring(path.lastIndexOf("."));
            String newName = ("excel_"+attachid + "") + suffix;
            FtpAttachment.upload2FTP(newName, stream);
            file.delete();
            record.setExportNewName(newName);
            record.setExportFileName(fileName);
            return record;
        } catch (DAOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DomainServiceException e) {
            e.printStackTrace();
        }
        return null;
    }

}

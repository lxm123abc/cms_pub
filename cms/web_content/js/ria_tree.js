// @version 3.00.04

			//
			//
			//\u6811\u7684\u7236\u7c7b
			//
var Tree_init=function(){};

//addOnloadEvent(Tree_init);

var SSB_Tree=function (treeData,div_id){
	var obj={};
	this.nodeListeners={};
	this.model={};
	this.setData(treeData);
	
	this.setObjLabel(div_id);
	
	var rootId="0";
	
	var field_id="id";

	var field_name="name";

	var field_children="children";
	
	var field_parentId="parentId";
	
	var field_enabled="enabled";
	
	var target="_self";
	
	var href="";
	
	var delay=false;
	
	var service="";
	
	//\u83dc\u5355\u662f\u5426\u53ef\u7528\u7684\u6807\u5fd7\u3002 Y\u53ef\u7528\uff0c N\u4e0d\u53ef\u7528 ,\u5f53\u83dc\u5355\u4e0d\u88ab\u7981\u7528\u65f6\uff0c\u83dc\u5355\u53d8\u7070
	var enabledFlag ="0";
	//\u6570\u636e\u662f\u5426\u5df2\u7ecf\u662f\u683c\u5f0f\u5316\u7684\u6811\u5f62\u6570\u636e
	this.isFormated=true;
	
	//\u6811\u7684onselectchange\u4e8b\u4ef6\u7684\u5904\u7406\u51fd\u6570
	this.onselectchange=function(pre,last){};

	//properties
	
	this.setRootId=function(root_id){
	   rootId=root_id;
	};
	
	this.getRootId=function(){
	   return rootId;
	};
	
	this.setField_id=function(f_id){
		field_id=f_id;
	};
	this.setField_name=function(f_name){
		field_name=f_name;
	};
	this.setField_children=function(f_children){
		field_children=f_children;
	};
	this.setField_parentId=function(f_parentId){
		//\u8bbe\u7f6e\u8fd9\u4e2a\u5b57\u6bb5\u8bf4\u660e\u6ca1\u6709\u88ab\u683c\u5f0f\u5316
		this.isFormated=false;
		field_parentId=f_parentId;
	};
	this.setField_enabled=function(f_enabled)
	{
	     field_enabled=f_enabled;
	};
	this.setEnabledFlag=function(flg)
	{
	     enabledFlag=flg;
	};
	this.setHref=function(h){
		href=h;
	};
	this.setTarget=function(t){
		target=t;
	};
	this.setDelay=function(d){
		delay=d;
	};
	this.setService=function(s){
		service=s;
	};
	
	this.getField_id=function(){
		return field_id;
	};
	this.getField_name=function(){
		return field_name;
	};
	this.getField_children=function(){
		return field_children;
	};
	this.getField_parentId=function(){
		return field_parentId;
	};
	this.getField_enabled=function()
	{
	   return field_enabled;
	};
    this.getEnabledFlag=function()
    {
        return enabledFlag;
    };
	this.getHref=function(){
		return href;
	};
	this.getTarget=function(){
		return target;
	};
	this.getDelay=function(){
		return delay;
	};
	this.getService=function(){
		return service;
	};
	
	this.setOnselectchange=function(fun){
		this.onselectchange=fun;
	};

	
	this.getDefaultIcon=function(element,level,index,parent){
		//\u5bf9\u8c61ID
		var _icon=document.createElement("input");
		_icon.type='button';
		var src="";
		var length=this.getChildren(parent).length;
		var div_id="div_"+SSB_Tree.Path;
		var isLast=(1+index)==length;
		switch(true){
			case this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.leafIcon;break;
			case this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lleafIcon;break;
			case !this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.closeIcon;
				_icon["onclick"]=SSB_Tree.defaultIconEvent;break;
			case !this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lcloseIcon;
				_icon["onclick"]=SSB_Tree.defaultLIconEvent;break;
		}
		_icon.setAttribute("tree_id",this.tree_id);
		_icon.setAttribute("div_id",div_id);		
		_icon.className=src;
		//\u8c03\u7528public\u65b9\u6cd5(\u7f3a\u7701\u4e3a\u7a7a)
		this.setIcon(element,level,index,_icon);
		return _icon;
	};
 
	 this.getDefaultNode=function(element,level,index,parent){
		var _node=document.createElement("DIV");
		_node.className='nodeDiv';
		_node.setAttribute("id",'id_'+SSB_Tree.Path);
		_node.setAttribute("level",level);
		_node.setAttribute("istreeleaf",this.isLeaf(element));
		_node.setAttribute("tree_id",this.tree_id);
		_node["model"]=element;
		//\u8bbe\u7f6e\u5f53\u524d\u8282\u70b9\u7684isLast\u5c5e\u6027\uff0c\u56e0\u4e3a\u662f\u5426\u6700\u540e\u4e00\u4e2a\u5143\u7d20\u5bf9\u9875\u9762\u663e\u793a\u6709\u5f71\u54cd
		_node.setAttribute("isLast",1+index==this.getChildren(parent).length);
		return _node;
	};
 
	this.getLine=function(element,level,index,nodeDiv){
		var lineIcon={};
		var src='';
		var iconBlank=this.blankIcon;
		var iconLine=this.lineIcon;
		//SSB_Tree.treeNodeStack\u662f\u8282\u70b9\u961f\u5217\uff0c\u56e0\u4e3a\u9700\u8981\u77e5\u9053\u5f53\u524d\u8282\u70b9\u8def\u5f84\u4e2d\u7684\u7236\u8282\u70b9\u662f\u5426\u662f\u5f53\u524d\u5c42\u7684\u6700\u6709\u4e00\u4e2a
		for(var i=0;i<SSB_Tree.treeNodeStack.length;i++){
			//\u5982\u679c\u8def\u5f84\u4e2d\u7684\u67d0\u4e00\u5c42\u662f\u6700\u540e\u4e00\u4e2a\u90a3\u4e48\u4e0d\u5e94\u8be5\u663e\u793a\u8fde\u7ebf
			var isLast=SSB_Tree.treeNodeStack[i];
			if(isLast){
				src=iconBlank;
			//\u5426\u5219\u663e\u793a\u8fde\u7ebf
			}else{
				src=iconLine;
			}
			lineIcon=document.createElement("input");
			lineIcon.type='button';
			lineIcon.className=src;
			nodeDiv.appendChild(lineIcon);
		}
	};

	this.nextLevel=function(element,level,index,parent){
		var level_id="div_"+SSB_Tree.Path;
		var div=document.createElement("DIV");
		var node_id="node_"+SSB_Tree.Path;
		div.setAttribute("id",level_id);
		//\u8bbe\u7f6e\u4e0b\u4e00\u5c42\u6240\u6709\u7236\u8282\u70b9\u7684ID
		div.setAttribute("node_id",node_id);
		div.style.display="none";
		div.setAttribute("level",level);
		div.setAttribute("newLevel","newLevel");
		return div;
	};
	this.getNodeText=function(element,level,index,node){
		var span=document.createElement('span');
		//2009-06-03 wxr \u4e0d\u80fd\u4f7f\u7528innerHTML!!!
		//\u6ce8\uff1ainnerHTML\u8d4b\u503c\u5185\u5bb9\u5982\u679c\u5305\u542b"&"\u5b57\u7b26\uff0c\u6709\u65f6\u4f1a\u628a"&"\u53ca\u540e\u9762\u5185\u5bb9\u4e22\u5931
		/*
		if(typeof span.innerText != "undefined")
			span.innerText=element[this.getField_name()];
		else
			span.innerHTML=element[this.getField_name()];
		*/
		if(isIEorFF()) {
			span.innerText=element[this.getField_name()];
		}
		else {
			span.textContent=element[this.getField_name()];
		}
		//childNodes[0].nodeValue\u4e0d\u4f1a\u628a"&"\u5b57\u7b26\u53ca\u540e\u9762\u5185\u5bb9\u4e22\u5931\uff0c\u4f46IE6\u8981\u6c42childNodes\u4e0d\u4e3a\u7a7a
		//span.childNodes[0].nodeValue=element[this.getField_name()];
		var enabledFlg=element[this.getField_enabled()];
		span.setAttribute("tree_id",this.tree_id);
		span["model"]=element;
		//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u53d8\u7070
		if(this.isGray(enabledFlg))
		{
		    span.style.color="gray";
		}
		//sujingui \u7528\u6237\u81ea\u5b9a\u4e49\u5b57\u4f53\u7684\u989c\u8272
		var textColor=this.changeTreeTextColor(element,level,index,node);
        if(textColor)
        {
            span.style.color=textColor;
        }
		span['onclick']=SSB_Tree.eventRouter;	
		span.className='nodeText';
		return span;
	};
	this.getNode=function(element,level,index,parent){
		var nodeDiv=this.getDefaultNode(element,level,index,parent);
		this.getLine(element,level,index,nodeDiv);
		nodeDiv.appendChild(this.getDefaultIcon(element,level,index,parent));
		var userElement=this.appendBeforeText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		//\u4fee\u6539 sujingui \u5728\u8282\u70b9\u524d\u9762\u52a0\u56fe\u7247
		var imgElement=this.addImageBeforeText(element,level,index,parent);
		if(imgElement){
		    nodeDiv.appendChild(imgElement);
		}
		nodeDiv.appendChild(this.getNodeText(element,level,index,parent));
		userElement=this.appendAfterText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		return nodeDiv;
	};

	this.plantTree=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);

			levelDiv.appendChild(nodeDiv);

			//\u5982\u679c\u5f53\u524d\u8282\u70b9\u4e0d\u662f\u53f6\u7ed3\u70b9\u5c31\u5904\u7406\u4ed6\u7684\u5b50\u8282\u70b9
			if(this.isBranch(current)){
				var div=this.nextLevel(current,level,i,parent);
				levelDiv.appendChild(div);
				this.plantTree(this.getChildren(current),1+level,current,div);
			}
			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};

	this.insertBeforeCurrentTopNode=function(tree,level,parent,levelDiv,menu_id){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			var index = -1;
			var allNodes = this.model;
			for(var j=0;j<allNodes.length;j++)
			{
				if(allNodes[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			var positionNode ;
			
			if(index != -1)
			{				
				positionNode = document.getElementById("id_"+allNodes[index][this.getField_id()]);
			}
			
			if(positionNode != "undefined" && positionNode != null)
			{
				levelDiv.insertBefore(nodeDiv,positionNode);
			}
			else
			{
				levelDiv.insertBefore(nodeDiv,levelDiv.firstChild);
			}		

			SSB_Tree.pathStack.pop();
			SSB_Tree.treeNodeStack.pop();
		}
	};

	this.insertAfterCurrentTopNode=function(tree,level,parent,levelDiv,menu_id){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			var index = -1;
			var allNodes = this.model;
			for(var j=0;j<allNodes.length;j++)
			{
				if(allNodes[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			var positionNode ;
			
			if(index != -1)
			{				
				positionNode = document.getElementById("id_"+allNodes[index][this.getField_id()]);
			}
			
			if(positionNode != "undefined" && positionNode != null)
			{
				levelDiv.insertBefore(nodeDiv,positionNode.nextSibling);
			}
			else
			{
				levelDiv.appendChild(nodeDiv);
			}		

			SSB_Tree.pathStack.pop();
			SSB_Tree.treeNodeStack.pop();
		}
	};
	
	//\u628a\u8282\u70b9\u52a0\u4e3a\u7b2c\u4e00\u4e2a\u8282\u70b9
	this.insertFirstOfTreeNode=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			levelDiv.insertBefore(nodeDiv,levelDiv.firstChild);

			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};
	
	//\u628a\u8282\u70b9\u52a0\u4e3a\u6700\u540e\u4e00\u4e2a\u8282\u70b9
	this.insertLastOfTreeNode=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);
			
			levelDiv.appendChild(nodeDiv);

			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
			
			$(nodeDiv).find(".treeIcon").removeClass("tree_blank").addClass("tree_blankl");
			$(nodeDiv).attr("isLast","true");
			
			if(nodeDiv.previousSibling == null) {
				return;
			}
			else {
				if($(nodeDiv.previousSibling).attr("istreeleaf") != undefined){
					//\u5224\u65ad\u662f\u5426\u6709istreeleaf\u5c5e\u6027\uff0c\u6709\u8bf4\u660e\u5b83\u662f\u7236\u8282\u70b9\u6240\u5728div\uff0c\u5219\u53ea\u9700\u4fee\u6539\u5176isLast\u5c5e\u6027\u548c\u663e\u793a\u6309\u94ae\u7684\u6837\u5f0f
					$(nodeDiv.previousSibling).attr("isLast","false");	
					if($(nodeDiv.previousSibling).attr("istreeleaf") == "true" || $(nodeDiv.previousSibling).attr("istreeleaf") == true){
						//\u5982\u679c\u662f\u53f6\u5b50\u8282\u70b9\uff0c\u5219\u53ea\u9700\u4fee\u6539\u6837\u5f0f\u5373\u53ef
						$(nodeDiv.previousSibling).find(".tree_blankl").removeClass("tree_blankl").addClass("tree_blank");
						return;
					} else {							
						$(nodeDiv.previousSibling).find(":button")[0]["onclick"]=SSB_Tree.defaultIconEvent;
					}						
					
					$(nodeDiv.previousSibling).find(".tree_minusl").removeClass("tree_minusl").addClass("tree_minus");
					$(nodeDiv.previousSibling).find(".tree_plusl").removeClass("tree_plusl").addClass("tree_plus");	
					$(nodeDiv.previousSibling).find("div.nodeDiv").find("input:eq(0)").removeClass("tree_white").addClass("tree_line");																
				} else if($(nodeDiv.previousSibling).attr("newLevel") == "newLevel") {
					//\u5224\u65ad\u662f\u5426\u662f\u5b50\u8282\u70b9\u6240\u5728div\uff0c\u5219\u53ea\u9700\u4fee\u6539\u5176isLast\u5c5e\u6027\u548c\u663e\u793a\u6309\u94ae\u7684\u6837\u5f0f
					$(nodeDiv.previousSibling.previousSibling).attr("isLast","false");
					if($(nodeDiv.previousSibling.previousSibling).attr("istreeleaf") == "true" || $(nodeDiv.previousSibling.previousSibling).attr("istreeleaf") == true){
						$(nodeDiv.previousSibling.previousSibling).find(".tree_blankl").removeClass("tree_blankl").addClass("tree_blank");
						return;
					} else {							
						$(nodeDiv.previousSibling.previousSibling).find(":button")[0]["onclick"]=SSB_Tree.defaultIconEvent;	
					}					
					
					$(nodeDiv.previousSibling.previousSibling).find(".tree_minusl").removeClass("tree_minusl").addClass("tree_minus");
					$(nodeDiv.previousSibling.previousSibling).find(".tree_plusl").removeClass("tree_plusl").addClass("tree_plus");	
					$(nodeDiv.previousSibling).find("div.nodeDiv").find("input:eq(0)").removeClass("tree_white").addClass("tree_line");						
				}
			}
		}
	};
};

SSB_Tree.prototype={
	
	//\u7a7a\u767d\u56fe\u7247
	blankIcon:"treeIcon tree_white",
	//\u8fde\u7ebf
	lineIcon:"treeIcon tree_line",
	//\u5173\u95ed\u7684\u975e\u53f6\u8282\u70b9
	closeIcon:"treeIcon tree_plus",
	//\u6253\u5f00\u7684\u975e\u53f6\u8282\u70b9
	openIcon:"treeIcon tree_minus",
	//\u53f6\u7ed3\u70b9\u56fe\u7247
	leafIcon:"treeIcon tree_blank",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u5173\u95ed\u65f6\u7684\u56fe\u7247
	lcloseIcon:"treeIcon tree_plusl",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u6253\u5f00\u65f6\u7684\u56fe\u7247
	lopenIcon:"treeIcon tree_minusl",
	//\u6700\u540e\u4e00\u4e2a\u53f6\u7ed3\u70b9
	lleafIcon:"treeIcon tree_blankl",
	
	setObjLabel:function(div_id){
		this.tree_id=div_id;
		this.objLabel=document.getElementById(div_id);
	},

	setData:function(treeData){
		if(treeData instanceof String||"string"==typeof treeData){
			var obj=eval(treeData);
		}else{
			var obj=treeData;
		}
		if(obj instanceof Array)
			this.model=obj;
		else
			this.model=[obj];
	},

	formatData:function(){
		var id=this.getField_id();
		var children=this.getField_children();
		var parentId=this.getField_parentId();
		var model=this.model;
		var hashMap={};
		
		var current={};
		var root={};
		root[id]=this.getRootId();
		model.splice(0,0,root);		
		var length=this.model.length;
		for(var i=0;i<length;i++){
			current=model[i];
			current[children]=[];
			hashMap[current[id]]=current;
		}
		var parent={};
		for(var i=0;i<length;i++){
			current=model[i];
			parent=hashMap[current[parentId]];
			if(parent)
				parent[children].push(current);
		}
		return root[children];
	},

	setNode:function(node){

	},
	
	setIcon:function(element,level,index,icon){
		return icon;
	},
	appendBeforeText:function(element,level,index,parent){
		return null;
	},
	addImageBeforeText:function(element,level,index,parent){
	    return null;
	},
	changeTreeTextColor:function(element,level,index,parent)
	{
	    return null;
	},
	appendAfterText:function(element,level,index,parent){
		return null;
	},
	isLeaf:function(element){
		if(this.getDelay()){
			var children=this.getChildren(element);
			if(!children)return true;
			return parseInt(children)==0;
		}
		return !this.isBranch(element);
	},
	isBranch:function(element){
		var children= this.getChildren(element);
		return children instanceof Array && children!=null && children.length!=0;
	},
	getChildren:function(element){
		return element[this.getField_children()];
	},

    isGray:function(enabledFlag)
    {
         var flg=this.getEnabledFlag();
         if(enabledFlag == flg)
         {
            return true;
         }
         else
         {
            return false;
         }
    },

	initTree:function(){
		if(!this.objLabel) return;
		this.destroy();
		if(!this.getDelay()&&!this.isFormated){
			this.model=this.formatData();
		}
		var id=this.getField_id();
		var children=this.getField_children();
		this.root={};
		this.root[id]=this.getRootId();
		this.root[children]=this.model;
		SSB_Tree.pathStack=[];
		SSB_Tree.treeNodeStack=[];
		SSB_Tree.Path='';
		if(!this.model)
		{
			return;
		}
		this.plantTree(this.model,1,this.root,this.objLabel);
		SSB_Tree.trees[this.tree_id]=this;
		//\u8bbe\u7f6e\u7f3a\u7701\u7684\u5355\u51fb\u4e8b\u4ef6
		this.addNodeListener("onclick",this.defaultNodeOnClick);
		
	},

	refresh:function(treeData){
		if(treeData)
			this.setData(treeData);
		if(this.objLabel){
			this.objLabel.innerHTML="";
			this.initTree();
		}
	},

	destroy:function(){
		//\u628a\u6807\u7b7e\u5185\u7684HTML\u7f6e\u7a7a
		if(this.objLabel)
			this.objLabel.innerHTML="";
		if(SSB_Tree.trees[this.tree_id]){
			//\u5220\u6389SSB_Tree.trees\u4e2d\u7684\u8fd9\u4e2a\u6811
			delete SSB_Tree.trees[this.tree_id];
			//\u628a\u5f53\u524d\u4e66\u7684\u76d1\u542c\u5668\u5bf9\u8c61\u7f6e\u7a7a
			this.nodeListeners={};
		}
	},

	gotoHref:function(node){
		 var evalHref=function(el,level,href){
		 	eval("var element=arguments[0];");
			var el_re=/\$\{([^\}]+)\}/;
			var tmp=href;
			var result=[];
			var a=[];
			if(!(a=el_re.exec(tmp))){
				return href;
			}
			while(a){
				result.push(RegExp.leftContext);
				result.push(eval(a[1]));
				a=el_re.exec(RegExp.rightContext);
			}
			result.push(RegExp.rightContext);
			return result.join("");
		};
		var href=this.getHref();
		if(href){
			href=evalHref(node["model"],node.getAttribute("level"),href);
			window.open(href,this.getTarget());
		}
	},
	defaultNodeOnClick:function(oEvent){
		var current_tree =SSB_Tree.getTreeByNode(this);
		if(current_tree.lastclick)
			current_tree.lastclick.style.fontWeight='normal';
		this.style.fontWeight='bolder';
		current_tree.lastclick=this;
		oEvent=oEvent||window.event;
		var node=oEvent.target||oEvent.srcElement;
		current_tree.gotoHref(node);
	},

	addNodeListener:function(event_type,handler){
		if(!this.nodeListeners[event_type]){
			this.nodeListeners[event_type]=[];
		}
		//\u5f53\u8be5\u4e8b\u4ef6\u7c7b\u578b \u5df2\u7ecf\u5b58\u5728\u8be5\u4e8b\u4ef6\u5904\u7406\u51fd\u6570\u65f6\uff0c\u4e0d\u80fd\u91cd\u590d\u6dfb\u52a0\u6b64\u51fd\u6570 \uff0c\u907f\u514d\u8be5\u51fd\u6570\u88ab\u6267\u884c\u591a\u6b21
		var listener=this.nodeListeners[event_type];		
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				return;
			}
		}
		
		this.nodeListeners[event_type].push(handler);		
		
		var spans = document.getElementById(this.tree_id).getElementsByTagName("span");
		//\u5c06\u6240\u6709\u83dc\u5355\u8282\u70b9\u52a0\u4e0a\u4e8b\u4ef6 2008-5-5
		for(var i=0;i<spans.length;i++)
		{
			spans[i][event_type] = SSB_Tree.eventRouter;
		}
	
	},
	
	removeNodeListener:function(event_type,handler){
		var listener=this.nodeListeners[event_type];
		if(!listener){
			return;
		}
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				listener.splice(i,1);
				return;
			}
		}
	},
	doselectchange:function(last){
		if(this.lastclick&&this.lastclick!=last)
			this.onselectchange(this.lastclick,last);
	},
	getParams:function(node){
		var evalParam=function(el,str){
			eval("var element=arguments[0];");
			return eval(str);
		};
		var params=[];
		if(!this.params)return;
		for(var i=0;i<this.params.length;i++){
			if(typeof this.params[i]=='object'){
				params.push(this.params[i].value);
			}else
				params.push(evalParam(node['model'],this.params[i]));
		}
		return params;
	},
	selectNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}

		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				if(spans[i].fireEvent){
					spans[i].fireEvent('onclick');
				}else if(spans[i].dispatchEvent){
					spans[i].dispatchEvent('click');
				}
				return true;
			}
		}
		return false;			
	},
	openNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}
		var isTreeLeaf='';
		var divNode={};
		var inputs={};
		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				divNode=spans[i].parentNode;
				isTreeLeaf=divNode.getAttribute('istreeleaf');
				//\u975e\u53f6\u8282\u70b9
				if(isTreeLeaf=='false'||!isTreeLeaf){
					inputs=divNode.getElementsByTagName('input');
					for(var j=0;j<inputs.length;j++){
						if(inputs[j].getAttribute('tree_id')){
							if(inputs[j].fireEvent){
								inputs[j].fireEvent('onclick');
							}else if(inputs[j].dispatchEvent){
								inputs[j].dispatchEvent('click');
							}
							break;
						}
					}
				}
				return true;
			}
		}
		return false;
	},
	addNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			this.model.push(element);
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertLastOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined' || sibling == 0 || sibling == "")
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	addFirstNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}
			
			var elem = new Array();
			elem.push(element);
			sibling = elem.concat(sibling);
			this.model = sibling;
				
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertFirstOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	addLastNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			this.model.push(element);
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertLastOfTreeNode(insertElem,1,this.root,this.objLabel);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	insertBeforeNode:function(element,menu_id){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			//wanghao add
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}

			var index = -1;
			for(var j=0;j<sibling.length;j++)
			{
				if(sibling[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			
			if(index == 0)
			{
				var elem = new Array();
				elem.push(element);
				sibling = elem.concat(sibling);
			}
			else
			{
				var beforeArray = sibling.slice(0,index);
				var afterArray = sibling.slice(index);
				sibling = beforeArray.concat(element).concat(afterArray);
			}
			this.model = sibling;
			
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertBeforeCurrentTopNode(insertElem,1,this.root,this.objLabel,menu_id);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u63d2\u5165\u5230\u76ee\u6807\u8282\u70b9\u7684\u524d\u9762
					//wanghao add
					var index = -1;
					for(var j=0;j<sibling.length;j++)
					{
						if(sibling[j].menuId == menu_id)
						{
							index = j;
							break;
						}
					}
					
					if(index == 0)
					{
						var elem = new Array();
						elem.push(element);
						sibling = elem.concat(sibling);
					}
					else
					{
						var beforeArray = sibling.slice(0,index);
						var afterArray = sibling.slice(index);
						sibling = beforeArray.concat(element).concat(afterArray);
					}
					
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	insertAfterNode:function(element,menu_id){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]==this.getRootId()){
			//wanghao add
			var sibling=this.model;
			if(sibling == null || typeof(sibling) == 'undefined')
			{
				sibling = new Array();
			}

			var index = -1;
			for(var j=0;j<sibling.length;j++)
			{
				if(sibling[j].menuId == menu_id)
				{
					index = j;
					break;
				}
			}
			if (index == sibling.length-1)
			{
				sibling.push(element);
			}
			else
			{
				var beforeArray = sibling.slice(0,index+1);
				var afterArray = sibling.slice(index+1);
				sibling = beforeArray.concat(element).concat(afterArray);
			}
			this.model = sibling;
					
			var insertElem = [];
			insertElem.push(element);
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.insertAfterCurrentTopNode(insertElem,1,this.root,this.objLabel,menu_id);
			return;
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null && levelDiv.getAttribute('newLevel')=='newLevel') parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u63d2\u5165\u5230\u76ee\u6807\u8282\u70b9\u7684\u540e\u9762
					//wanghao add
					var index = -1;
					for(var j=0;j<sibling.length;j++)
					{
						if(sibling[j].menuId == menu_id)
						{
							index = j;
							break;
						}
					}
					
					if (index == sibling.length-1)
					{
						sibling.push(element);
					}
					else
					{
						var beforeArray = sibling.slice(0,index+1);
						var afterArray = sibling.slice(index+1);
						sibling = beforeArray.concat(element).concat(afterArray);
					}
					
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	refreshNode:function(element){
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var field_Id=this.getField_id();
		var name=this.getField_name();
		var children=this.getField_children();
		var id;
		if(typeof element=='object')
			id=element[field_Id];
		else if(typeof element=='string'){
			id=element;
		}
		for(var i=0;i<length;i++){
			if(id==spans[i]['model'][field_Id]){
				//2009-06-03 wxr \u4e0d\u80fd\u4f7f\u7528innerHTML!!!
				//\u91c7\u7528innerHTML\u7684\u65b9\u5f0f\uff0c\u5982\u679c\u663e\u793a\u540d\u79f0\u4e2d\u5e26\u6709</>\u7b49\u7b26\u53f7\uff0c\u5219\u4f1a\u663e\u793a\u5f02\u5e38\uff0c\u5e94\u5f53\u91c7\u7528innerText/contentText
				//spans[i].innerHTML=element[name];
				if(isIEorFF()) {
					spans[i].innerText=element[name];
				}
				else {
					spans[i].textContent=element[name];
				}
				
				var old=spans[i]['model'];
				
				/*
				if(!this.getDelay()) {
					//wxr 20091221\u4e0b\u9762\u8fd9\u79cd\u5199\u6cd5\u4e0d\u5bf9\uff0c\u56e0\u4e3a\u8282\u70b9\u5b9e\u4f53\u7684\u5b50\u8282\u70b9\u5b57\u6bb5\u4e0d\u4e00\u5b9a\u5c31\u662fchildren\uff0c\u5e94\u5f53\u91c7\u7528\u540e\u9762\u7684\u5f62\u5f0f
					element.children = old.children;
				}
				*/
				//\u5982\u679c\u5f85\u5237\u65b0\u7684\u8282\u70b9\u6ca1\u6709\u5305\u62ec\u5176\u5b50\u8282\u70b9\u7684List\u4fe1\u606f\uff0c\u5219\u5c06\u539f\u8282\u70b9\u7684\u5b50\u8282\u70b9\u4fe1\u606f\u8d4b\u503c\u7ed9\u5f85\u5237\u65b0\u8282\u70b9
				var cName = this.getField_children();
				if(!(element[cName] instanceof Array) || (element[cName] instanceof Array) && element[cName].length==0) {
					element[cName] = old[cName];
				}
				
				spans[i]['model']=element;
				var divNode=spans[i].parentNode;
				divNode['model']=element;
				var model=[];
				if(divNode.parentNode.tagName!='DIV')
					model=this.model;
				else{
					var parentNode=divNode.parentNode.previousSibling;
					model=parentNode['model'][children];
				}
				for(var j=0;j<model.length;j++){
					if(model[j]==old){
						//model.splice(i,1,element);break;	//2009-06-04wxr \u600e\u4e48\u7528i\uff1f
						model.splice(j,1,element);break;
					}
				}
				break;
			}
		}
	},
	deleteNode:function(deleted){
		//check argument
		if(deleted == null)
		{
			return;
		}
		//if is not array ,add arg into array
		if(! (deleted instanceof Array))
		{
			deleted = [deleted];
		}
		var spans=this.objLabel.getElementsByTagName('span');
		var length=spans.length;
		var length2=deleted.length;
		var id=this.getField_id();
		var children = this.getField_children();
		for(var i=length -1 ;i>= 0;i--){
			for(var j=0;j<length2;j++){
			    //sujingui modify
			    var _span = spans[i];
			    if(_span == undefined || _span == null)
			    {
			       continue;
			    }
				var element=spans[i]['model'];
				
				var isequal = false;
				if(typeof(compare2object) != "undefined" && compare2object.constructor == Function) {
					isequal = compare2object(deleted[j], element);
				}
				else {
					isequal = (deleted[j]==element[id]);
				}
				if(isequal){
//				if(deleted[j]==element[id]){	//\u539f\u6765\u7684\u6bd4\u8f83\u5224\u65ad
					var nodeDiv=spans[i].parentNode;
					var levelDiv=nodeDiv.parentNode;
					var tmp=nodeDiv.previousSibling;
					var previousNode=tmp!=null&&tmp.getAttribute('newLevel')=='newLevel'?tmp.previousSibling:tmp;
					var childrenDiv = nodeDiv.nextSibling;
					var parentNode = levelDiv.previousSibling;

					//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u90a3\u8981\u4fee\u6539\u5012\u6570\u7b2c\u4e8c\u4e2a\u8282\u70b9
					if(nodeDiv.getAttribute('isLast')+''=='true'){
						if(previousNode){//\u662f\u6700\u540e\u4e00\u4e2a\u4f46\u4e0d\u552f\u4e00\u3002\u6709\u524d\u7eed\u8282\u70b9\u3002
							var previousIcon = SSB_Tree.getIconByNode(previousNode);
							previousIcon.className=previousIcon.className+'l';
							//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u5012\u6570\u7b2c\u4e8c\u4e2a\u53c8\u4e0d\u662f\u53f6\u7ed3\u70b9
							if(previousNode.getAttribute('istreeleaf')+''=='false'){
								//\u4fee\u6539onclick\u4e8b\u4ef6,\u56e0\u4e3a\u66f4\u6539\u7684\u56fe\u6807\u4f1a\u4e0d\u4e00\u6837
								//previousIcon.className=this.lcloseIcon;
								previousIcon['onclick']=SSB_Tree.defaultLIconEvent;
								//\u5982\u679c\u4e4b\u524d\u7684\u8282\u70b9\u7684\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d\u5c31\u628a\u4ed6\u4eec\u5220\u9664\uff0c\u8ba9\u4ed6\u91cd\u65b0\u52a0\u8f7d\uff0c\u4e0d\u7136\u4f1a\u51fa\u95ee\u9898
								if(!SSB_Tree.noUpdate(previousNode)){
									//levelDiv.removeChild(tmp);
								}
								
								//\u589e\u52a0tree_line\u7684\u5220\u9664\u5904\u7406
								if ($.browser.mozilla ) {
									var tmpl = parseInt("" + $(previousNode).attr("level"));
								   $(previousNode.nextSibling).find("div.nodeDiv").find("input:eq("+ (tmpl-1)+ ")").removeClass("tree_line").addClass("tree_white");
								} else {
									$(previousNode.nextSibling).find("div.nodeDiv").find("input:eq("+ (previousNode.level-1)+ ")").removeClass("tree_line").addClass("tree_white");
								} 
							
							}
							levelDiv.removeChild(nodeDiv);
							//\u5982\u679c\u6709\u5b50\u8282\u70b9\u4e5f\u4e00\u8d77\u5220\u9664
							if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
								levelDiv.removeChild(childrenDiv);
							}
							previousNode.setAttribute('isLast','true');
						}else{//\u552f\u4e00\u7684
								//\u5982\u679c\u8fd9\u4e2a\u8282\u70b9\u662f\u7b2c\u4e00\u5c42\u7684\u5e76\u4e14\u552f\u4e00
							if(levelDiv.tagName!='DIV'){
								this.objLabel.innerHTML='';
							}else{
								//\u4e0d\u662f\u7b2c\u4e00\u5c42\u7684\u8282\u70b9
								//\u88ab\u5220\u9664\u7684\u662f\u552f\u4e00\u8282\u70b9,\u8981\u628a\u4ed6\u7684\u7236\u8282\u70b9\u8bbe\u7f6e\u6210\u53f6\u8282\u70b9
								var parentLevel = levelDiv.parentNode;
								var parentIcon = SSB_Tree.getIconByNode(parentNode);
								var className = parentIcon.className;
								if(className.charAt(-1+className.length)=='l')
									parentIcon.className=this.lleafIcon;
								else
									parentIcon.className=this.leafIcon;
									//\u8fd9\u4e2a\u8282\u70b9\u6240\u5728\u7684\u5c42\u5c06\u88ab\u4e00\u8d77\u5220\u9664
								parentLevel.removeChild(levelDiv);
								parentNode.setAttribute('istreeleaf','true');
								
								parentNode['model'][children]=[];
							}
						}
					}
					//\u5982\u679c\u4e0d\u662f\u6700\u540e\u4e00\u4e2a\u53ea\u7ba1\u5220\u9664\u5b83\u548c\u5b83\u7684\u5b57\u8282\u70b9
					else {
						levelDiv.removeChild(nodeDiv);
						//\u5982\u679c\u6709\u5b50\u8282\u70b9
						if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
							levelDiv.removeChild(childrenDiv);
						}
					}
					//\u66f4\u65b0model
					var childElement=[];
					if(element[this.getField_parentId()]==this.getRootId()){
						childElement=this.model;
					}else{
						childElement=parentNode['model'][children];
					}
					for(var k=0;k<childElement.length;k++){
						if(childElement[k]==element){
							childElement.splice(k,1);break;
						}
					}
				}
			}
		}
	}
};
	//static properties and function
	//\u4e0b\u6807\u5806\u6808
SSB_Tree.pathStack = [];
SSB_Tree.Path='';
	//\u8282\u70b9\u961f\u5217
SSB_Tree.treeNodeStack=[];

SSB_Tree.trees={};

SSB_Tree.getTreeByNode=function(obj){
	return SSB_Tree.trees[obj.getAttribute("tree_id")];
};

SSB_Tree.eventRouter=function(oEvent){
	var oEvent = oEvent || window.event;
	var event_type="on"+oEvent.type;
		//\u83b7\u53d6\u53d1\u751f\u4e8b\u4ef6\u7684\u6811
	var tree = SSB_Tree.getTreeByNode(this);
	tree.doselectchange(this);
		//\u83b7\u53d6\u5bf9\u5e94\u4e8b\u4ef6\u76d1\u542c\u5668\u5217\u8868
	var listeners=tree.nodeListeners[event_type];
	if(!listeners)return;
	for(var i=0;i<listeners.length;i++){
		listeners[i].call(this,oEvent);
	}
};
SSB_Tree.getIconByNode = function(node){
	var divs=node.getElementsByTagName('input');
	var length=divs.length;
	for(var i=0;i<length;i++){
		if(divs[i].getAttribute('div_id')!=null){
			return divs[i];
		}
	}
};
SSB_Tree.updateChildren=function(s,children){
	var tree=SSB_Tree.getTreeByNode(this);
	var divNode=this;
	var img=SSB_Tree.getIconByNode(divNode);
	//\u5982\u679c\u6ca1\u6709\u53ef\u66f4\u65b0\u7684\u5b57\u8282\u70b9
	if(children.length==0){
		if(divNode.getAttribute('isLast')+''=='true') img.className=tree.lleafIcon;
		else img.className=tree.leafIcon;
		img.onclick=null;
		return;
	}
	//\u8bbe\u7f6e\u8282\u70b9\u8def\u5f84\u4fe1\u606f
	SSB_Tree.setNodePath(divNode);
	var level=parseInt(divNode.getAttribute("level"));
	//\u65b0\u5c42\u6b21\u7684DIV
	var div=tree.nextLevel({},level,0,{});
	div.style.display='block';
	divNode['model'][tree.getField_children()]=children;
	//\u5f80\u65b0\u5c42\u6b21\u7684DIV\u4e2d\u6dfb\u52a0\u65b0\u7684\u8282\u70b9
	tree.plantTree(children,1+level,divNode['model'],div);
	
	var obj=divNode.nextSibling;
	var parent=divNode.parentNode;
	//\u628a\u65b0\u7684\u5c42\u6b21\u52a0\u5165\u5230\u6811\u4e2d
	if(obj)
		parent.insertBefore(div,obj);
	else
		parent.appendChild(div);
	var isLast=divNode.getAttribute('isLast')+'';
	//\u6539\u53d8\u88ab\u66f4\u65b0\u7684\u8282\u70b9\u7684\u56fe\u6807
	if(isLast=='true') {
		img.className=tree.lopenIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultLIconEvent;
		}
	}
	else {
		img.className=tree.openIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultIconEvent;
		}
	}
	divNode.setAttribute('istreeleaf','false');	
};

SSB_Tree.setNodePath=function(node){
	var div={};
	SSB_Tree.pathStack=[];
	SSB_Tree.treeNodeStack=[];
	SSB_Tree.Path='';
	var tree=SSB_Tree.getTreeByNode(node);
	do{
		SSB_Tree.treeNodeStack.push(node.getAttribute('isLast')+''=='true');
		SSB_Tree.pathStack.push(node['model'][tree.getField_id()]);
		div=node.parentNode;
		node=div.previousSibling;
	}while(div.tagName=='DIV');
	SSB_Tree.pathStack.reverse();
	SSB_Tree.treeNodeStack.reverse();
	SSB_Tree.Path=SSB_Tree.pathStack.join('_');
};

SSB_Tree.noUpdate=function(node){
	var isLast=node.getAttribute('isLast')+'';
	if(isLast=='true')
		return node.nextSibling==null;
	return node.nextSibling.getAttribute('newLevel')!='newLevel';
};



SSB_Tree.handleUpdate=function(img,tree){
	if(tree.getDelay()){
		var divNode=img.parentNode;
		var params=tree.getParams(divNode);
		callMethod(tree.getService(),params,divNode,SSB_Tree.updateChildren);
	}
};
SSB_Tree.defaultIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree = SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.openIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.closeIcon;
	}
};

SSB_Tree.defaultLIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree =SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.lopenIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.lcloseIcon;
	}
};


			//
			//\u591a\u9009\u6811
			//
function SSB_MultiTree(treeData,div_id,values,autoselect){
	SSB_Tree.call(this, treeData,div_id);
	this.setValues(values);
	this.autoselect=autoselect||false;
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_MultiTree.prototype=new SSB_Tree();
//\u91cd\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_MultiTree.prototype.constructor =SSB_MultiTree;

SSB_MultiTree.prototype.appendBeforeText=function(element,level,index,parent){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	var box=[];
	box.push("<input type='checkbox' style='margin:0px' value='");
	box.push(value);
	box.push("' name='checkbox_");
	box.push(this.tree_id);
	box.push("' onclick='SSB_MultiTree.checkTheBoxs(this)' ");
	for(var i=0;i<this.selectedValues.length;i++){
		if(value==this.selectedValues[i]){
			box.push("checked=true");
		}
	}
	box.push("/>");
	div.innerHTML=box.join("");
	div.style.display="inline";
	
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u591a\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

//\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
SSB_MultiTree.prototype.selectedValues=[];

SSB_MultiTree.prototype.setValues=function(values){
	if(values&&values instanceof Array){
		this.selectedValues=values;
	}
	else{
		this.selectedValues=[];
	}
};

SSB_MultiTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName('input');
	var values=[];
	for(var i=0;i<checkboxs.length;i++){		
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].value);
		}
	}
	return values;
};
SSB_MultiTree.prototype.getObjValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName("input");
	var values=[];
	for(var i=0;i<checkboxs.length;i++){
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].parentNode.parentNode['model']);
		}
	}
	return values;
};
SSB_MultiTree.prototype.refresh=function(treeData,values){
	if(treeData)
		this.setData(treeData);
	if(values)
		this.setValues(values);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};
SSB_MultiTree.checkTheBoxs=function(box){
	var name=box.getAttribute("name");
	var treeIdRE=/^checkbox_(\S+)$/;
	var treeId=treeIdRE.exec(name)[1];
	var tree=SSB_Tree.trees[treeId];
	if(tree&&tree.autoselect){
	//wanghao add
		var div=box.parentNode.parentNode.parentNode;
		if(div||div.getAttribute("newLevel")){
			var divboxes = div.childNodes;
			var flag = true;
			for(var j=0;j<divboxes.length;j++){
				if(!divboxes[j].getAttribute("newLevel")) {
					var checkboxs=$(divboxes[j]).find(":checkbox");					
					for(var i=0;i<checkboxs.length;i++){
						if(checkboxs[i].checked != true) {
							flag = false;
							break;
						}
					}
				}
			}
			
			if(div.previousSibling) {
				var tmpdiv = div;				
				var allowCycle = true;
				if(flag)
				{					
					//\u5982\u679c\u6240\u6709\u5b50\u8282\u70b9\u90fd\u88ab\u9009\u4e2d\uff0c\u5219\u7236\u8282\u70b9\u4e5f\u8981\u88ab\u9009\u4e2d\uff0c\u5e76\u5e94\u5411\u4e0a\u904d\u5386
					while(allowCycle){
						tmpdiv = tmpdiv.previousSibling;
						if(!tmpdiv || tmpdiv.tagName != "DIV") break;
						
						var tocheck = true;
						$(tmpdiv.nextSibling).find(":checkbox").each(function(){
							if(!this.checked) tocheck = false;	
							
							return false;
						});
						
						if(tocheck){
							$(tmpdiv).find(":checkbox").each(function(){
								this.checked = true;	
							});
							
							var nextSibling = tmpdiv.nextSibling.nextSibling;
							var previousSibling = tmpdiv;
							tmpdiv = tmpdiv.parentNode;									
							
							//\u5224\u65ad\u540e\u9762\u7684\u5144\u5f1f\u8282\u70b9\u662f\u5426\u90fd\u5df2\u7ecf\u9009\u4e2d
							while(nextSibling){
								$(nextSibling).find(":checkbox").each(function(){
									if(!this.checked) allowCycle = false;	
									
									return false;
								});
								
								if(!allowCycle) break;
								nextSibling = nextSibling.nextSibling;
							}
							//\u5224\u65ad\u524d\u9762\u7684\u5144\u5f1f\u8282\u70b9\u662f\u5426\u90fd\u5df2\u7ecf\u9009\u4e2d
							previousSibling = previousSibling.previousSibling; 
							while(previousSibling){
								//if(!previousSibling.previousSibling)  break;
								$(previousSibling).find(":checkbox").each(function(){
									if(!this.checked) allowCycle = false;	
									
									return false;
								});
								
								if(!allowCycle) break;
								previousSibling = previousSibling.previousSibling;
							}
						}
						else {
							allowCycle = false;
						}											
					}
				}
				else {
					//\u5982\u679c\u53d6\u6d88\u9009\u4e2d\uff0c\u5219\u5176\u7956\u5148\u8282\u70b9\u90fd\u5e94\u5f53\u53d6\u6d88\u9009\u4e2d
					while(allowCycle){						
						tmpdiv = tmpdiv.previousSibling;
						$(tmpdiv).find(":checkbox").each(function(){
							this.checked = false;	
						});
						
						tmpdiv = tmpdiv.parentNode;				
						if(!tmpdiv.previousSibling || tmpdiv.previousSibling.tagName != "DIV") allowCycle = false;		
					}
				}	
			}		
		}
	
		var span=box.parentNode.nextSibling;
		if(span.getAttribute('istreeleaf')=='false'||!span.getAttribute('istreeleaf')){
			var levelDiv=span.parentNode.nextSibling;
			if(!levelDiv||!levelDiv.getAttribute("newLevel"))return;
			
			$(levelDiv).find(":checkbox").each(function(){
				this.checked = box.checked;
			});
		}
	}
};



			//
			//\u5355\u9009\u6811
			//
var SSB_SingleTree=function(treeData,div_id,value){
	SSB_Tree.call(this, treeData,div_id);
	//\u9ed8\u8ba4\u7684\u9009\u5b9a
	this.selectedValue="";
	this.setValue(value);
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_SingleTree.prototype=new SSB_Tree();
//\u4ece\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_SingleTree.prototype.constructor =SSB_SingleTree;

SSB_SingleTree.prototype.appendBeforeText=function(element,level,index,node){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	
	//\u5728IE\u4e2d\u7528document.createElement\u52a8\u6001\u521b\u5efa\u7684HTML\u5bf9\u8c61\uff0c\u4e0d\u80fd\u52a8\u6001\u5730\u8bbe\u7f6e\u5b83\u7684name\u5c5e\u6027\uff0c
	//\u800c\u8fd9\u91cc\u5fc5\u987b\u7528radio button\u7684name\u5c5e\u6027\uff0c\u6240\u4ee5\u8fd9\u91cc\u7528\u62fc\u5b57\u7b26\u4e32\u7684\u65b9\u5f0f\u3002
	var radio=[];
	radio.push("<input");
	radio.push("type='radio'");
	radio.push("value='"+value+"'");
	radio.push("name='radio_"+this.tree_id+"'");
	if(value==this.selectedValue){
		radio.push("checked=true");
	}
	radio.push("/>");

	div.innerHTML=radio.join(" ");

	div.style.display="inline";
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u5355\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

SSB_SingleTree.prototype.setValue=function(value){
	if(value&&""!=value){
		this.selectedValue=value;
	}
	else{
		this.selectedValue="";
	}
};

SSB_SingleTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var radios=treeDiv.getElementsByTagName("input");

	for(var i=0;i<radios.length;i++){
		if(radios[i].checked&&radios[i].type.toLowerCase()=='radio'){
			return radios[i].value;
		}
	}
};

SSB_SingleTree.prototype.refresh=function(treeData,value){
	if(treeData)
		this.setData(treeData);
	if(value)
		this.setValue(value);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};



			//
			//\u6811\u5de5\u5382\uff0c\u6839\u636e\u6807\u7b7eID\u8fd4\u56de\u6811\u5bf9\u8c61
			//
var Tree_Factory=function (data,label_id){
	var labelObj=document.getElementById(label_id);
	var rootId=labelObj.getAttribute("rootId");
	var treeType=labelObj.getAttribute("treeType");
	var field_id=labelObj.getAttribute("field_id");
	var field_name=labelObj.getAttribute("field_name");
	var field_children=labelObj.getAttribute("field_children");
	var field_parentId=labelObj.getAttribute("field_parentId");
	var field_enabled=labelObj.getAttribute("field_enabled");
	var enabledFlag=labelObj.getAttribute("enabledFlag");
	var href=labelObj.getAttribute('href');
	var target=labelObj.getAttribute('target');
	var autoselect=labelObj.getAttribute('autoselect')=="true";
	var params=labelObj.getAttribute('params');
	var service=labelObj.getAttribute("service");
	var delay=labelObj.getAttribute("delay")=='true'; 
	if(data.treeType){
		treeType=data.treeType;
	}
	var initValue=data.initValue;
	var selectedValue=data.selectedValue;
	initValue=!initValue?data:initValue;
	var tree={};
	switch(treeType){
		case "0":tree=new SSB_Tree(initValue,label_id);break;
		case "1":tree=new SSB_SingleTree(initValue,label_id,selectedValue);break;
		case "2":tree=new SSB_MultiTree(initValue,label_id,selectedValue,autoselect);break;
		default:tree=new SSB_Tree(initValue,label_id);break;
	}
	if(rootId)
	   tree.setRootId(rootId);
	if(field_id)
		tree.setField_id(field_id);
	if(field_name)
		tree.setField_name(field_name);
	if(field_children)
		tree.setField_children(field_children);
	if(field_parentId)
		tree.setField_parentId(field_parentId);
    if(field_enabled)
        tree.setField_enabled(field_enabled);
    if(enabledFlag)
        tree.setEnabledFlag(enabledFlag);
	if(href)
		tree.setHref(href);
	if(target)
		tree.setTarget(target);
	if(service){
		tree.setService(service);
	}
	tree.setDelay(delay);
	if(params){
		var re=/(value|ctrl):([^,]+)/;
		var el=/^\$\{([^\}]+)\}$/;
		tree.params=[];
		var a=[];
		while((a=re.exec(params))){
			params=RegExp.rightContext;
			if(a[1]=='value'){
				var value=el.exec(a[2]);
				if(value)
					tree.params.push(value[1]);
				else 
					tree.params.push(a[2]);
			}else if(a[1]=='ctrl'){
				tree.params.push(document.getElementById(a[2]));
			}
		}
	}
	return tree;
};

// \u5c55\u5f00\u6240\u6709\u8282\u70b9
SSB_Tree.openAllNode = function(treeID)
{
	var spanList = document.getElementById(treeID).getElementsByTagName("span");
	if(!spanList)
	{
		return;
	}
	for(var i = 0;i<spanList.length;i++)
	{
		var node = spanList[i];
		var pNode = node.parentNode;
		if(!pNode.istreeleaf)
		{
			var childNode = pNode.childNodes;
			for(var j = 0;j<childNode.length;j++)
			{
				if(childNode[j].tagName)
				{
					var tagNames = childNode[j].tagName.toUpperCase();
					if(tagNames == "INPUT")
					{
						var div=document.getElementById(childNode[j].getAttribute("div_id"));
						if(div == "undefined" || div == null || div.style.display=="none")
						{
							childNode[j].fireEvent('onclick');
						}
					}
				}
			}
		}
	}
};

//\u6536\u7f29\u6240\u6709\u8282\u70b9
SSB_Tree.closeAllNode = function(treeID)
{
	var spanList = document.getElementById(treeID).getElementsByTagName("span");
	if(!spanList)
	{
		return;
	}
	for(var i = 0;i<spanList.length;i++)
	{
		var node = spanList[i];
		var pNode = node.parentNode;
		if(!pNode.istreeleaf)
		{
			var childNode = pNode.childNodes;
			for(var j = 0;j<childNode.length;j++)
			{
				if(childNode[j].tagName)
				{
					var tagNames = childNode[j].tagName.toUpperCase();
					if(tagNames == "INPUT")
					{
						var div = null;
						if(childNode[j].getAttribute("div_id"))
							div=document.getElementById(childNode[j].getAttribute("div_id"));
						
						if(div != null && 	div.style.display!="none")				
							childNode[j].fireEvent('onclick');
					}
				}
			}
		}
	}
};

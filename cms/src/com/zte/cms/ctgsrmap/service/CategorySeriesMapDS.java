package com.zte.cms.ctgsrmap.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.ctgsrmap.dao.ICategorySeriesMapDAO;
import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CategorySeriesMapDS extends DynamicObjectBaseDS implements ICategorySeriesMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICategorySeriesMapDAO dao = null;

    public void setDao(ICategorySeriesMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException
    {
        log.debug("insert categorySeriesMap starting...");
        try
        {
            Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("category_series_map");
            categorySeriesMap.setMapindex(mapindex);
            String mappingid = String.format("%02d", ObjectType.CATEGORYSERIESMAP_TYPE)+ String.format("%030d", mapindex);
            categorySeriesMap.setMappingid(mappingid);
            dao.insertCategorySeriesMap(categorySeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert categorySeriesMap end");
    }

    public void updateCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException
    {
        log.debug("update categorySeriesMap by pk starting...");
        try
        {
            dao.updateCategorySeriesMap(categorySeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categorySeriesMap by pk end");
    }

    public void updateCategorySeriesMapList(List<CategorySeriesMap> categorySeriesMapList)
            throws DomainServiceException
    {
        log.debug("update categorySeriesMapList by pk starting...");
        if (null == categorySeriesMapList || categorySeriesMapList.size() == 0)
        {
            log.debug("there is no datas in categorySeriesMapList");
            return;
        }
        try
        {
            for (CategorySeriesMap categorySeriesMap : categorySeriesMapList)
            {
                dao.updateCategorySeriesMap(categorySeriesMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categorySeriesMapList by pk end");
    }

    public void removeCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException
    {
        log.debug("remove categorySeriesMap by pk starting...");
        try
        {
            dao.deleteCategorySeriesMap(categorySeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categorySeriesMap by pk end");
    }

    public void removeCategorySeriesMapList(List<CategorySeriesMap> categorySeriesMapList)
            throws DomainServiceException
    {
        log.debug("remove categorySeriesMapList by pk starting...");
        if (null == categorySeriesMapList || categorySeriesMapList.size() == 0)
        {
            log.debug("there is no datas in categorySeriesMapList");
            return;
        }
        try
        {
            for (CategorySeriesMap categorySeriesMap : categorySeriesMapList)
            {
                dao.deleteCategorySeriesMap(categorySeriesMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categorySeriesMapList by pk end");
    }

    public CategorySeriesMap getCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DomainServiceException
    {
        log.debug("get categorySeriesMap by pk starting...");
        CategorySeriesMap rsObj = null;
        try
        {
            rsObj = dao.getCategorySeriesMap(categorySeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categorySeriesMapList by pk end");
        return rsObj;
    }

    public List<CategorySeriesMap> getCategorySeriesMapByCond(CategorySeriesMap categorySeriesMap)
            throws DomainServiceException
    {
        log.debug("get categorySeriesMap by condition starting...");
        List<CategorySeriesMap> rsList = null;
        try
        {
            rsList = dao.getCategorySeriesMapByCond(categorySeriesMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categorySeriesMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get categorySeriesMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categorySeriesMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategorySeriesMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categorySeriesMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get categorySeriesMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categorySeriesMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategorySeriesMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categorySeriesMap page info by condition end");
        return tableInfo;
    }
}

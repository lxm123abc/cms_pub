package com.zte.cms.service.update.apply;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.service.IServiceWkfwhisDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class ServiceUpdateApplyCallback extends CommonCallback
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected IServiceDS serviceds = null;
    protected IServiceBufDS serviceBufds = null;
    protected IServiceWkfwhisDS serviceWkfwhisDS;

    protected Service serviceBasic;
    protected ServiceBuf serviceBuf;
    protected ServiceWkfwhis serviceWkfwhis;

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    public void setServiceWkfwhisDS(IServiceWkfwhisDS serviceWkfwhisDS)
    {
        this.serviceWkfwhisDS = serviceWkfwhisDS;
    }

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        String optObjecttype = ServiceConstants.getWorkflowTypeName(ServiceConstants.WKFWTYPECODE_SERVICE_UPDATE);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(wkflItem.getPboIndex().toString());

        String info;
        String logKey = "";
        if (wkflItem.getNodeId().longValue() == 1)
        {
            logKey = "ucdn.serv.wkfw.update.apply";
        }
        else
        {
            logKey = "ucdn.serv.wkfw.update.audit";
        }
        info = ResourceMgt.findDefaultText(logKey);
        if (null != info)
        {
            info = info.replace("[opername]", wkflItem.getExcutorName());
            info = info.replace("[servicename]", serviceBasic.getServicename());
            info = info.replace("[nodename]", wkflItem.getNodeName());
            info = info.replace("[handleevent]", wkflItem.getHandleEvent());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long serviceindex = wkflItem.getPboIndex();
        serviceBuf = new ServiceBuf();
        serviceBuf.setServiceindex(serviceindex);

        // 查询service对象
        try
        {
            serviceBuf = serviceBufds.getServiceBuf(serviceBuf);
            serviceBuf.setWorkflowlife(wkflItem.getPboState());

            serviceBasic = new Service();
            serviceBasic.setServiceindex(serviceindex);
            serviceBasic = serviceds.getService(serviceBasic);
            serviceBasic.setWorkflowlife(wkflItem.getPboState());

            if (serviceBuf.getEffecttime() == null || "".equals(serviceBuf.getEffecttime()))
            {
                wkflItem.setEffectiveTime("0");
            }
            else
            {
                wkflItem.setEffectiveTime("0");//修改为立即生效
            }
            wkflItem.setPboId(serviceBuf.getServiceid());

            // 创建工作流历史表
            serviceWkfwhis = new ServiceWkfwhis();
            BeanCopier copier = BeanCopier.create(ServiceBuf.class, ServiceWkfwhis.class, false);
            copier.copy(serviceBuf, serviceWkfwhis, null);

            serviceWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
            serviceWkfwhis.setTaskindex(new Long(wkflItem.getTaskId()));
            serviceWkfwhis.setNodeid(Long.valueOf(wkflItem.getNodeId().longValue()));
            serviceWkfwhis.setNodename(wkflItem.getNodeName());
            serviceWkfwhis.setOpername(wkflItem.getExcutorId());
            serviceWkfwhis.setTemplateindex(Long.valueOf(wkflItem.getTemplateSeqNo().longValue()));
            serviceWkfwhis.setOpopinion(wkflItem.getComments());
            serviceWkfwhis.setHandleevent(wkflItem.getHandleEvent());

            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            if (wkflItem.getStartTime() != null)
            {
                String workflowstarttime = dateformat.format(wkflItem.getStartTime());
                serviceWkfwhis.setWorkflowstarttime(workflowstarttime);
            }
            if (wkflItem.getFinishTime() != null)
            {
                String workflowendtime = dateformat.format(wkflItem.getFinishTime());
                serviceWkfwhis.setWorkflowendtime(workflowendtime);
            }

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

    }

    @Override
    public void operate()
    {
        try
        {
            if (wkflItem.getPboState().intValue() == -1) // 审核不通过，根据原来的状态修改审核不通过的状态
            {
                int oldStatus = serviceBuf.getStatus();
                if (oldStatus == 50)
                {
                    serviceBasic.setStatus(20);
                }
                else
                {
                    serviceBasic.setStatus(oldStatus);
                }

                serviceBufds.removeServiceBuf(serviceBuf);

                serviceBasic.setWorkflowlife(0); // 未启动
                serviceBasic.setWorkflow(0); // 未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                serviceBasic.setEffecttime(format.format(new Date()));// 将生效时间字段设为当前时间

            }
            else
            {
                serviceBufds.updateServiceBuf(serviceBuf);
            }
            serviceds.updateService(serviceBasic);
            serviceWkfwhisDS.insertServiceWkfwhis(serviceWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

    public void setServiceBufds(IServiceBufDS serviceBufds)
    {
        this.serviceBufds = serviceBufds;
    }

}

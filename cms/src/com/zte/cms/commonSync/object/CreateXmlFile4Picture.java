package com.zte.cms.commonSync.object;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.framework.base.ISysCode;

public class CreateXmlFile4Picture extends AbstractCreateXmlFile4Object
{

	private static final String CONTENT_SINGERLANG_CODEKEY="cms_content_singerlang";
    private static final String SERVICE_KEY="CMS";
    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;

        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        List<Integer> typeList = (List<Integer>)objectMap.get("type");
        String ElementID = "";
        String ElementCode = "";
        String ElementType = "";
        String ParentID = "";
        String ParentType = "";
        String ParentCode = "";
        String ID = "";
        String CODE = "";
        String FileURL = "";
        String fileName = "";
        String relativePath = "";
        String absolutePath = "";
        String retPath = "";
        String fileFullName = "";
        String Description = "";
        String Type = "";
        if (ObjectType.PROGRAMPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_program_20wg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	ID = pictureList.get(0).getPicturecode();
            	CODE = pictureList.get(0).getPicturecode();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<ProgramPictureMap> programPictureMapList = (List<ProgramPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<programPictureMapList.size(); i++)
            {
            	ParentID = programPictureMapList.get(0).getPicturecode();
            	ParentCode = programPictureMapList.get(0).getPicturecode();
            	ElementType = "Program";
            	ElementID = programPictureMapList.get(0).getCpcontentid();
            	ElementCode = programPictureMapList.get(0).getCpcontentid();
            	Type = programPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if(ObjectType.CASTPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_cast_20wg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("pictureList");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<Castcdn> castList = (List<Castcdn>)objectMap.get("castList");
            List<CastPictureMap> castPictureMapList = (List<CastPictureMap>)objectMap.get("castPictureMapList");
            for (int i =0; i<castList.size(); i++)
            {
                ParentID = pictureList.get(0).getPicturecode();
                ParentCode = pictureList.get(0).getPicturecode();
                ElementType = "Cast";
                ElementID = castList.get(0).getCastcode();
                ElementCode = castList.get(0).getCastcode();
                Type = castPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if(ObjectType.CHANNELPICTUREMAP_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_channel_20wg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	ID = pictureList.get(0).getPicturecode();
            	CODE = pictureList.get(0).getPicturecode();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<ChannelPictureMap> channelPictureMapList = (List<ChannelPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<channelPictureMapList.size(); i++)
            {
            	ParentID = channelPictureMapList.get(0).getPicturecode();
            	ParentCode = channelPictureMapList.get(0).getPicturecode();
            	ElementType = "Channel";
            	ElementID = channelPictureMapList.get(0).getCpcontentid();
            	ElementCode = channelPictureMapList.get(0).getCpcontentid();
            	Type = channelPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.SERIESPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_series_20wg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	ID = pictureList.get(0).getPicturecode();
            	CODE = pictureList.get(0).getPicturecode();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<SeriesPictureMap> seriesPictureMapList = (List<SeriesPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<seriesPictureMapList.size(); i++)
            {
            	ParentID = seriesPictureMapList.get(0).getPicturecode();
            	ParentCode = seriesPictureMapList.get(0).getPicturecode();
            	ElementType = "Series";
            	ElementID = seriesPictureMapList.get(0).getCpcontentid();
            	ElementCode = seriesPictureMapList.get(0).getCpcontentid();
            	Type = seriesPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.CATEGORYPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_category_20wg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	ID = pictureList.get(0).getPicturecode();
            	CODE = pictureList.get(0).getPicturecode();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<CategoryPictureMap> categoryPictureMapList = (List<CategoryPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<categoryPictureMapList.size(); i++)
            {
            	ParentID = categoryPictureMapList.get(0).getPicturecode();
            	ParentCode = categoryPictureMapList.get(0).getPicturecode();
            	ElementType = "Category";
            	ElementID = categoryPictureMapList.get(0).getCatagorycode();
            	ElementCode = categoryPictureMapList.get(0).getCatagorycode();
            	Type = categoryPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.SERVICEPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
        }
        
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");   
        Element picobjectElement = null;                
        Element picpertyElement = null;
        picobjectElement = objectsElement.addElement("Object");
        picobjectElement.addAttribute("ElementType", "Picture");
        picobjectElement.addAttribute("ID", ID);
        picobjectElement.addAttribute("Code", CODE);
        picobjectElement.addAttribute("Action", action);
        String ftpPrefix = com.zte.ismp.common.util.CommonUtils.getConfigValue("cms.pictureSyn.for20ftpaddress");
        writeElement(picobjectElement, picpertyElement, "FileURL",ftpPrefix+FileURL, true);
        writeElement(picobjectElement, picpertyElement, "Description",Description, true);
        Element mappingElement = adiElement.addElement("Mappings");
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");
        mapObjectElement.addAttribute("ParentType", "Picture");
        mapObjectElement.addAttribute("ID", "");
        mapObjectElement.addAttribute("ParentID", ParentID);
        mapObjectElement.addAttribute("ParentCode", ParentCode);
        mapObjectElement.addAttribute("ElementType", ElementType);
        mapObjectElement.addAttribute("ElementID", ElementID);
        mapObjectElement.addAttribute("ElementCode", ElementCode);
        mapObjectElement.addAttribute("Action", action);
        writeElement(mapObjectElement, mapPertyElement, "Type", Type, false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false); 
        writeDom(dom,fileFullName);      
        return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;
        if (1 == syncType || 2 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
        return null;
    }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;      
        Date date = new Date();
        String ElementID = "";
        String ElementCode = "";
        String ElementType ="";
        String ParentID = "";
        String ParentType = "";
        String ParentCode = "";
        String CODE = "";
        String FileURL = "";
        String fileName = "";
        String relativePath = "";
        String absolutePath = "";
        String retPath = "";
        String fileFullName = "";
        String Description = "";
        String Type = "";
        String targetTypeName ="";
        String pictureId = "";
        String ObjectID = "";
        List<Integer> typeList = (List<Integer>)objectMap.get("type");
        if (ObjectType.PROGRAMPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_program_epg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	pictureId = pictureList.get(0).getPictureid();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<ProgramPictureMap> programPictureMapList = (List<ProgramPictureMap>)objectMap.get("pictureMap");

            for (int i =0; i<programPictureMapList.size(); i++)
            {
            	ParentID = programPictureMapList.get(0).getPictureid();
            	ParentCode = programPictureMapList.get(0).getPictureid();
            	ElementType = "Program";
            	ParentType = "Picture";
            	ElementID = programPictureMapList.get(0).getProgramid();
            	ElementCode = programPictureMapList.get(0).getProgramid();
            	Type = programPictureMapList.get(0).getMaptype().toString();
            	ObjectID =programPictureMapList.get(0).getMappingid();
            }
        }
        else if(ObjectType.CASTPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_cast_epg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("pictureList");
            for (int i =0; i<pictureList.size(); i++)
            {
                pictureId = pictureList.get(0).getPictureid();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<CastPictureMap> castPictureMapList = (List<CastPictureMap>)objectMap.get("castPictureMapList");
            for (int i =0; i<castPictureMapList.size(); i++)
            {
                ParentID = castPictureMapList.get(0).getPictureid();
                ParentCode = castPictureMapList.get(0).getPictureid();
                ElementType = "Cast";
                ParentType = "Picture";
                ElementID = castPictureMapList.get(0).getCastid();
                ElementCode = castPictureMapList.get(0).getCastid();
                Type = castPictureMapList.get(0).getMaptype().toString();
                ObjectID =castPictureMapList.get(0).getMappingid();
            }
        }
        else if(ObjectType.CHANNELPICTUREMAP_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_channel_epg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	pictureId = pictureList.get(0).getPictureid();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<ChannelPictureMap> channelPictureMapList = (List<ChannelPictureMap>)objectMap.get("pictureMap");

            for (int i =0; i<channelPictureMapList.size(); i++)
            {
            	ParentID = channelPictureMapList.get(0).getPictureid();
            	ParentCode = channelPictureMapList.get(0).getPictureid();
            	ElementType = "Channel";
            	ParentType = "Picture";
            	ElementID = channelPictureMapList.get(0).getChannelid();
            	ElementCode = channelPictureMapList.get(0).getChannelid();
            	Type = channelPictureMapList.get(0).getMaptype().toString();
            	ObjectID =channelPictureMapList.get(0).getMappingid();
            }
        }
        else if (ObjectType.SERIESPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_series_epg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	pictureId = pictureList.get(0).getPictureid();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<SeriesPictureMap> seriesPictureMapList = (List<SeriesPictureMap>)objectMap.get("pictureMap");

            for (int i =0; i<seriesPictureMapList.size(); i++)
            {
            	ParentID = seriesPictureMapList.get(0).getPictureid();
            	ParentCode = seriesPictureMapList.get(0).getPictureid();
            	ElementType = "Series";
            	ParentType = "Picture";
            	ElementID = seriesPictureMapList.get(0).getSeriesid();
            	ElementCode = seriesPictureMapList.get(0).getSeriesid();
            	Type = seriesPictureMapList.get(0).getMaptype().toString();
            	ObjectID =seriesPictureMapList.get(0).getMappingid();
            }        	
        }
        else if (ObjectType.CATEGORYPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_category_epg"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
            	pictureId = pictureList.get(0).getPictureid();
            	FileURL = pictureList.get(0).getPictureurl();
            	Description = pictureList.get(0).getDescription();
            }
            List<CategoryPictureMap> categoryPictureMapList = (List<CategoryPictureMap>)objectMap.get("pictureMap");

            for (int i =0; i<categoryPictureMapList.size(); i++)
            {
            	ParentID = categoryPictureMapList.get(0).getPictureid();
            	ParentCode = categoryPictureMapList.get(0).getPictureid();
            	ElementType = "Category";
            	ParentType = "Picture";
            	ElementID = categoryPictureMapList.get(0).getCategoryid();
            	ElementCode = categoryPictureMapList.get(0).getCategoryid();
            	Type = categoryPictureMapList.get(0).getMaptype().toString();
            	ObjectID =categoryPictureMapList.get(0).getMappingid();
            }
        }
        else if (ObjectType.SERVICEPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
        }
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");   
        Element picobjectElement = null;                
        Element picpertyElement = null;
        picobjectElement = objectsElement.addElement("Object");
        picobjectElement.addAttribute("pictureId", pictureId);
        picobjectElement.addAttribute("ElementType", "Picture");
        picobjectElement.addAttribute("Action", action);
        String ftpPrefix = com.zte.ismp.common.util.CommonUtils.getConfigValue("cms.pictureSyn.for30ftpaddress");
        writeElement(picobjectElement, picpertyElement, "FileURL",ftpPrefix+FileURL, true);
        writeElement(picobjectElement, picpertyElement, "Description",Description, true);
        if ((2==targetType && 2!=syncType)||(1==targetType &&2!=syncType))//mapping关系只发到epg，且更新时不带mapping
        {
            Element mappingElement = adiElement.addElement("Mappings");
            Element mapObjectElement = null;
            Element mapPertyElement = null;
            mapObjectElement = mappingElement.addElement("Mapping");
            mapObjectElement.addAttribute("ParentType", ParentType);
            mapObjectElement.addAttribute("ParentID",ParentID);
            mapObjectElement.addAttribute("ElementType", ElementType);
            mapObjectElement.addAttribute("ElementID", ElementID);
            mapObjectElement.addAttribute("ObjectID", ObjectID);
            mapObjectElement.addAttribute("Action", action);
            writeElement(mapObjectElement, mapPertyElement, "Type", Type, false);
            writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false); 
      }
       writeDom(dom,fileFullName);
       return retPath;
    }
    
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        List<Integer> typeList = (List<Integer>)objectMap.get("type");
        String ElementID = "";
        String ElementCode = "";
        String ElementType = "";
        String ParentID = "";
        String ParentType = "";
        String ParentCode = "";
        String ID = "";
        String CODE = "";
        String FileURL = "";
        String fileName = "";
        String relativePath = "";
        String absolutePath = "";
        String retPath = "";
        String fileFullName = "";
        String Description = "";
        String Type = "";
        if (ObjectType.PROGRAMPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_program_20ys"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<ProgramPictureMap> programPictureMapList = (List<ProgramPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<programPictureMapList.size(); i++)
            {
                ParentID = programPictureMapList.get(0).getPicturecode();
                ParentCode = programPictureMapList.get(0).getPicturecode();
                ElementType = "Program";
                ElementID = programPictureMapList.get(0).getCpcontentid();
                ElementCode = programPictureMapList.get(0).getCpcontentid();
                Type = programPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if(ObjectType.CASTPICTURE_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_cast_20ys"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("pictureList");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<Castcdn> castList = (List<Castcdn>)objectMap.get("castList");
            List<CastPictureMap> castPictureMapList = (List<CastPictureMap>)objectMap.get("castPictureMapList");
            for (int i =0; i<castList.size(); i++)
            {
                ParentID = pictureList.get(0).getPicturecode();
                ParentCode = pictureList.get(0).getPicturecode();
                ElementType = "Cast";
                ElementID = castList.get(0).getCastcode();
                ElementCode = castList.get(0).getCastcode();
                Type = castPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if(ObjectType.CHANNELPICTUREMAP_TYPE == typeList.get(0).intValue())
        {
            fileName = "picture_channel_20ys"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<ChannelPictureMap> channelPictureMapList = (List<ChannelPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<channelPictureMapList.size(); i++)
            {
                ParentID = channelPictureMapList.get(0).getPicturecode();
                ParentCode = channelPictureMapList.get(0).getPicturecode();
                ElementType = "Channel";
                ElementID = channelPictureMapList.get(0).getCpcontentid();
                ElementCode = channelPictureMapList.get(0).getCpcontentid();
                Type = channelPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.SERIESPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_series_20ys"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<SeriesPictureMap> seriesPictureMapList = (List<SeriesPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<seriesPictureMapList.size(); i++)
            {
                ParentID = seriesPictureMapList.get(0).getPicturecode();
                ParentCode = seriesPictureMapList.get(0).getPicturecode();
                ElementType = "Series";
                ElementID = seriesPictureMapList.get(0).getCpcontentid();
                ElementCode = seriesPictureMapList.get(0).getCpcontentid();
                Type = seriesPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.CATEGORYPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
            fileName = "picture_category_20ys"+"_"+action.toLowerCase().trim().replace(" ", "")+"_"+date.getTime() + ".xml";
            relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"picture"+File.separator ;
            absolutePath = mountPoint + relativePath;
            retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
            File file = new File(absolutePath);
            if (!file.exists())
            {
                file.mkdirs();
            }
            fileFullName = absolutePath + fileName;
            List<Picture> pictureList = (List<Picture>)objectMap.get("picture");
            for (int i =0; i<pictureList.size(); i++)
            {
                ID = pictureList.get(0).getPicturecode();
                CODE = pictureList.get(0).getPicturecode();
                FileURL = pictureList.get(0).getPictureurl();
                Description = pictureList.get(0).getDescription();
            }
            List<CategoryPictureMap> categoryPictureMapList = (List<CategoryPictureMap>)objectMap.get("pictureMap");
            for (int i =0; i<categoryPictureMapList.size(); i++)
            {
                ParentID = categoryPictureMapList.get(0).getPicturecode();
                ParentCode = categoryPictureMapList.get(0).getPicturecode();
                ElementType = "Category";
                ElementID = categoryPictureMapList.get(0).getCatagorycode();
                ElementCode = categoryPictureMapList.get(0).getCatagorycode();
                Type = categoryPictureMapList.get(0).getMaptype().toString();
            }
        }
        else if (ObjectType.SERVICEPICTUREMAP_TYPE== typeList.get(0).intValue())
        {
        }
        
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");   
        Element picobjectElement = null;                
        Element picpertyElement = null;
        picobjectElement = objectsElement.addElement("Object");
        picobjectElement.addAttribute("ElementType", "Picture");
        picobjectElement.addAttribute("ID", ID);
        picobjectElement.addAttribute("Code", CODE);
        picobjectElement.addAttribute("Action", action);
        String ftpPrefix = com.zte.ismp.common.util.CommonUtils.getConfigValue("cms.pictureSyn.for20ftpaddress");
        writeElement(picobjectElement, picpertyElement, "FileURL",ftpPrefix+FileURL, true);
        writeElement(picobjectElement, picpertyElement, "Description",Description, true);
        Element mappingElement = adiElement.addElement("Mappings");
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");
        mapObjectElement.addAttribute("ParentType", "Picture");
        mapObjectElement.addAttribute("ID", "");
        mapObjectElement.addAttribute("ParentID", ParentID);
        mapObjectElement.addAttribute("ParentCode", ParentCode);
        mapObjectElement.addAttribute("ElementType", ElementType);
        mapObjectElement.addAttribute("ElementID", ElementID);
        mapObjectElement.addAttribute("ElementCode", ElementCode);
        mapObjectElement.addAttribute("Action", action);
        writeElement(mapObjectElement, mapPertyElement, "Type", Type, false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false); 
        writeDom(dom,fileFullName);      
        return retPath;
    }
}

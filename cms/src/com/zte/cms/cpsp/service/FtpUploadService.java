package com.zte.cms.cpsp.service;

import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import java.io.IOException;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:35 2019/6/26
 * @Description:
 */
public interface FtpUploadService {
    String upload(Long l) throws DAOException;

    public void delete(int attachid,String attachurl) throws DAOException;
}

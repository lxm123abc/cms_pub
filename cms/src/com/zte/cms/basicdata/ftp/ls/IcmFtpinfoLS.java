package com.zte.cms.basicdata.ftp.ls;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;

import javax.servlet.http.HttpSession;
import com.zte.cms.basicdata.ftp.ds.IIcmFtpinfoDS;
import com.zte.cms.basicdata.ftp.model.FTPConstant;
import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceCodeParser;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.cms.storageManage.service.ICmsStoragemanageDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;

/**
 * 内容摘要: 主要完成ftp的增、删、改、查功能
 */
public class IcmFtpinfoLS implements IIcmFtpinfoLS
{

    private IIcmFtpinfoDS icmftpinfods = null;

    private ICmsStoragemanageDS cmsStoragemanageDS = null;

    private static final String TEMPAREA_ID = "1"; // 临时区ID
    private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ResourceCodeParser parser = new ResourceCodeParser(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

    public void setCmsStoragemanageDS(ICmsStoragemanageDS cmsStoragemanageDS)
    {
        this.cmsStoragemanageDS = cmsStoragemanageDS;
    }

    public void setIcmftpinfods(IIcmFtpinfoDS icmftpinfods)
    {
        this.icmftpinfods = icmftpinfods;
    }

    /**
     * 新增ftp信息 实现流程： 1.判断临时区绑定空间是否还有剩余空间可用 2.在服务器上创建一临时区目录 3.插入一条ftp记录 4.修改临时区绑定空间的可用空间大小
     * 
     * @throws Exception
     */
    public String insertIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws Exception
    {

        String objectId = "";
        File destDir = null;
        String operResult = CommonLogConstant.OPTRESUT_SUCCESS + ":"
                + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);// ftp增加成功
        log.debug("insertIcmFtpinfo  start");
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            // 获取设备ID
            String deviceID = storageareaLs.getDeviceID(TEMPAREA_ID);
            if (null == deviceID || (TEMPAREA_ERRORSiGN).equals(deviceID) || "".equals(deviceID))
            {
                //return "1:获取设备ID失败";
            	return ResourceMgt.findDefaultText("ftp.get.deviceid.failed");
            }
            // 获取挂载点
            String rootPath = GlobalConstants.getMountPoint();
            if (rootPath == null || rootPath.trim().equals(""))
            {
                //return "1:获取挂载点失败";
                return ResourceMgt.findDefaultText("ftp.get.mount.failed");
            }
            rootPath = rootPath + deviceID + File.separator;
            CmsStoragemanage cmsStoragemanage = this.getStroagmanage(TEMPAREA_ID);// 获取临时区绑定的空间
            if (cmsStoragemanage == null)
            {
                //return "1:临时区未绑定任何空间";
                return ResourceMgt.findDefaultText("ftp.temporary.unbind");
            }
            else
            {
                if (cmsStoragemanage.getCurrentcapacity() != null && cmsStoragemanage.getCurrentcapacity() != ""
                        && Long.parseLong(cmsStoragemanage.getCurrentcapacity()) <= 0L)
                {
                    //return "1:临时区可用空间不足" + icmFtpinfo.getSpacesize() + "M";
                    return ResourceMgt.findDefaultText("ftp.temporary.spacenotenough")+ icmFtpinfo.getSpacesize() + "M";
                }
            }

            try
            {
                /*
                 * 获取临时区绑定空间的已使用空间大小 并和待新增的FTP空间大小作比较
                 */
                String tempUsedTotalCount = icmftpinfods.checkFTPInfo(cmsStoragemanage, rootPath);
                if (tempUsedTotalCount == null || tempUsedTotalCount.trim().equals(""))
                {
                   // return "1:获取临时区已使用空间大小失败";
                    return ResourceMgt.findDefaultText("ftp.get.temporaryusedspace.failed");
                }
                long tempUnusedTotalCount = Long.parseLong(cmsStoragemanage.getStoragecapacity()) * 1024
                        - Long.parseLong(tempUsedTotalCount) - icmFtpinfo.getSpacesize().longValue();
                log.info("#########################" + ResourceMgt.findDefaultText("ftp.icmftpinfols.insert.compare.result")+": = :"
                        + (tempUnusedTotalCount < 0L)); //"IcmFtpinfoLS中新增方法insertIcmFtpinfo比较结果的值"
                if (tempUnusedTotalCount < 0L)
                {
                    //return "1:临时区可用空间不足" + icmFtpinfo.getSpacesize() + "M";
                    return ResourceMgt.findDefaultText("ftp.temporary.spacenotenough")+ icmFtpinfo.getSpacesize() + "M";
                }
            }
            catch (Exception e)
            {
                log.error("Error occurred in insertIcmFtpinfo method", e);
            }

            // 在服务器上创建一临时区目录
            String tempArea = storageareaLs.getAddress(TEMPAREA_ID);
            if (null == tempArea || (TEMPAREA_ERRORSiGN).equals(tempArea))
            {// 获取临时区地址失败
               // return "1:获取临时区地址失败";
                return ResourceMgt.findDefaultText("ftp.get.temporaryadress.failed");
            }
            else
            {
                File tempDir = new File(GlobalConstants.getMountPoint() + tempArea + File.separator);

                if (!tempDir.exists())
                {
                    //return "1:临时区目录不存在，请检查临时区绑定";
                    return ResourceMgt.findDefaultText("ftp.temporary.nocatalog");
                }
                else
                // 判断目标文件夹是否存在，若不存在则创建
                {
                    StringBuffer destfile = new StringBuffer("");
                    destfile.append(GlobalConstants.getMountPoint() + icmFtpinfo.getFtppath());
                    destDir = new File(destfile.toString());
                    if (!destDir.exists())
                    {
                        destDir.mkdirs();
                    }
                    else
                    {
                        //return "1:该文件目录已存在";
                        return ResourceMgt.findDefaultText("ftp.filecatalog.exist");
                    }
                }
            }

            icmFtpinfo.setCpid("0");
            icmFtpinfo.setSpaceindex(cmsStoragemanage.getStorageindex());
            objectId = icmftpinfods.insertIcmFtpinfo(icmFtpinfo);

            // 更新临时区绑定的空间的可用空间大小
            String currentcapacity = String.valueOf((long) (Math.floor((Long.parseLong(cmsStoragemanage
                    .getCurrentcapacity()) * 1024 - icmFtpinfo.getSpacesize()) / 1024F)));
            cmsStoragemanage.setCurrentcapacity(currentcapacity);
            cmsStoragemanageDS.updateCmsStoragemanage(cmsStoragemanage);

            // 操作成功增加插入ftp的日志
            CommonLogUtil.insertOperatorLog(objectId, FTPConstant.MGTTYPE_FTP, FTPConstant.OPERTYPE_FTP_ADD,
                    FTPConstant.OPERTYPE_FTP_ADD_INFO, CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (DomainServiceException e1)
        {

            operResult = CommonLogConstant.OPTRESUT_FALSE + ":"
                    + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_FAIL); // ftp增加失败
            log.error("Error occurred in insertIcmFtpinfo method", e1);
            if (destDir != null && destDir.exists())
            {
                destDir.delete();
            }
            throw new DomainServiceException(e1);
        }
        catch (Exception e)
        {
            operResult = CommonLogConstant.OPTRESUT_FALSE + ":"
                    + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_FAIL); // ftp增加失败
            log.error("Error occurred in insertIcmFtpinfo method", e);
            if (destDir != null && destDir.exists())
            {
                destDir.delete();
            }
            throw new Exception(e);
        }
        log.debug(" insertIcmFtpinfo end");

        return operResult;
    }

    /**
     * 更新ftp信息 实现流程： 1.判断临时区绑定空间是否还有剩余空间可用 2.在服务器上修改临时区目录 3.修改ftp记录 4.修改临时区绑定空间的可用空间大小
     * 
     * @throws Exception
     */
    public String updateIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {

        String objectId = icmFtpinfo.getInfoindex() + "";
        String operResult = CommonLogConstant.OPTRESUT_SUCCESS + ":"
                + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        IcmFtpinfo ifinfo = new IcmFtpinfo();
        IcmFtpinfo ifinfo2 = null;
        log.debug("updateIcmFtpinfo start");
        try
        {

            IcmFtpinfo oldIcmFtpinfo = new IcmFtpinfo();
            oldIcmFtpinfo = (IcmFtpinfo) this.icmftpinfods.getIcmFtpinfo(icmFtpinfo.getInfoindex());
            if (oldIcmFtpinfo == null)
            {
                return CommonLogConstant.OPTRESUT_FALSE + ":"
                        + parser.getResourceItem(FTPConstant.OPERTYPE_FTP_NOTEXIST);
            }

            // 根据文件路径查询该条FTP记录是否存在
            ifinfo.setFtppath(icmFtpinfo.getFtppath());
            List<IcmFtpinfo> listifinfo = this.icmftpinfods.getIcmFtpinfoByCond(ifinfo);
            if (!listifinfo.isEmpty())
            {
                ifinfo2 = listifinfo.get(0);
            }

            // 获取设备ID
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            String deviceID = storageareaLs.getDeviceID(TEMPAREA_ID);
            if (null == deviceID || (TEMPAREA_ERRORSiGN).equals(deviceID) || "".equals(deviceID))
            {
                //return "1:获取设备ID失败";
                return ResourceMgt.findDefaultText("ftp.get.deviceid.failed");
            }
            // 获取挂载点
            String rootPath = GlobalConstants.getMountPoint();
            String rootpath = null;

            if (rootPath == null || rootPath.trim().equals(""))
            {
                //return "1:获取挂载点失败";
                return ResourceMgt.findDefaultText("ftp.get.mount.failed");
            }
            rootpath = rootPath;
            rootPath = rootPath + deviceID + File.separator;
            CmsStoragemanage cmsStoragemanage = this.getStroagmanage(TEMPAREA_ID);// 获取临时区绑定的空间
            if (cmsStoragemanage == null)
            {
                //return "1:临时区未绑定任何空间";
                return ResourceMgt.findDefaultText("ftp.temporary.unbind");
            }
            else
            {

                if (cmsStoragemanage.getCurrentcapacity() != null && cmsStoragemanage.getCurrentcapacity() != ""
                        && Long.parseLong(cmsStoragemanage.getCurrentcapacity()) <= 0L)
                {
                    //return "1:临时区可用空间不足" + icmFtpinfo.getSpacesize() + "M";
                    return ResourceMgt.findDefaultText("ftp.temporary.spacenotenough")+ icmFtpinfo.getSpacesize() + "M";
                }

                // 临时区绑定空间已发生变化,防止一个页面新增FTP空间大小过大，临时区绑定另一个空间      "比较结果:"
                log.info("#########################"+ResourceMgt.findDefaultText("ftp.compare.result")
                        + (cmsStoragemanage.getStorageindex().longValue() != oldIcmFtpinfo.getSpaceindex().longValue())
                        + "######################");
                if (cmsStoragemanage.getStorageindex().longValue() != oldIcmFtpinfo.getSpaceindex().longValue())
                {
                    //return "1:临时区空间已发生改变,FTP目录不允许修改";
                    return ResourceMgt.findDefaultText("ftp.temporarychanged.ftpcannotrework");
                }
                else
                {
                    /*
                     * 获取临时区绑定空间的已使用空间大小 并和待修改的FTP空间大小作比较
                     */
                    String tempUsedTotalCount = icmftpinfods.checkFTPInfo(cmsStoragemanage, rootPath);
                    if (tempUsedTotalCount == null || tempUsedTotalCount.trim().equals(""))
                    {
                        //return "1:获取临时区已使用空间大小失败";
                        return ResourceMgt.findDefaultText("ftp.get.temporaryusedspace.failed");
                    }
                    long tempUnusedTotalCount = Long.parseLong(cmsStoragemanage.getStoragecapacity()) * 1024
                            - Long.parseLong(tempUsedTotalCount) - icmFtpinfo.getSpacesize().longValue();
                    log.info("#########################" +  ResourceMgt.findDefaultText("ftp.icmftpinfols.update.compare.result")+": = :"
                            + (tempUnusedTotalCount < 0L));  //"IcmFtpinfoLS中修改方法updateIcmFtpinfo比较结果的值"
                    if (tempUnusedTotalCount < 0L)
                    {
                        //return "1:临时区可用空间不足" + icmFtpinfo.getSpacesize() + "M";
                        return ResourceMgt.findDefaultText("ftp.temporary.spacenotenough")+ icmFtpinfo.getSpacesize() + "M";
                    }
                    else
                    {

                        StringBuffer newfile = new StringBuffer("");// 新目录
                        newfile.append(GlobalConstants.getMountPoint() + icmFtpinfo.getFtppath());

                        StringBuffer oldfile = new StringBuffer("");// 旧目录
                        oldfile.append(GlobalConstants.getMountPoint() + oldIcmFtpinfo.getFtppath());

                        File newDir = new File(newfile.toString());
                        File oldDir = new File(oldfile.toString());
                        if (oldDir.exists())
                        {
                            if (oldDir.list().length == 0)
                            {
                                if (!newDir.exists())// 判断新目录是否存在且数据库中没有该条记录
                                {
                                    oldDir.renameTo(newDir);
                                }
                                else
                                {
                                    if (ifinfo2 != null && ifinfo2.getInfoindex() != null)
                                    {
                                        if (ifinfo2.getInfoindex().intValue() == icmFtpinfo.getInfoindex().intValue())// 新目录存在且由自己从前台创建的
                                        {
                                            oldDir.renameTo(newDir);
                                        }
                                        else
                                        // 该文件目录是由别人从前台创建的
                                        {
                                            //return "1:该文件目录已存在";
                                            return ResourceMgt.findDefaultText("ftp.filecatalog.exist");
                                        }
                                    }
                                    else
                                    // 该文件目录是直接从后台手工创建的
                                    {
                                        //return "1:该文件目录已存在";
                                        return ResourceMgt.findDefaultText("ftp.filecatalog.exist");
                                    }
                                }
                            }
                        }
                        else
                        {
                            //return "1:该文件目录不存在或已被删除";
                            return ResourceMgt.findDefaultText("ftp.filecatalog.notexist");
                        }

                        String ftppath = filterSlashStr(rootpath + icmFtpinfo.getFtppath());
                        log.info("#########################" + "ftppath = :" + ftppath);
                        String useredSize = exeCmd("du -ms " + ftppath);
                        if (useredSize == null || useredSize == "")
                        {
                            //return "1:获取该FTP已使用空间大小失败";
                            return ResourceMgt.findDefaultText("ftp.getusedspace.failed");
                        }
                        log.info("#########################" + "useredSize = :" + useredSize);
                        // 如果待更新的FTP目录的空间大小比实际该目录已使用的空间大小还大
                        String details = useredSize.substring(0, findfirstspace(useredSize));
                        long spaceresult = icmFtpinfo.getSpacesize().longValue() - Long.parseLong(details);
                        log.info("#########################" + "spaceresult = :" + spaceresult);
                        if (spaceresult < 0)
                        {
                            //return "1:该目录已用空间超过分配的空间大小，不能修改";
                            return ResourceMgt.findDefaultText("ftp.catalogspace.failed");
                        }

                        icmFtpinfo.setCurrentcapacity(String.valueOf(spaceresult));
                        this.icmftpinfods.updateIcmFtpinfo(icmFtpinfo);

                        long compareSpacesize = icmFtpinfo.getSpacesize().longValue()
                                - oldIcmFtpinfo.getSpacesize().longValue();
                        long moreSpacesize = Math.abs(compareSpacesize);
                        String currentcapacity = "";
                        // 如果待修改的FTP记录的空间大小比先前的该条记录的空间大小大
                        if (compareSpacesize > 0)
                        {
                            currentcapacity = String.valueOf((long) (Math.floor((Long.parseLong(cmsStoragemanage
                                    .getCurrentcapacity()) * 1024 - moreSpacesize) / 1024F)));
                            cmsStoragemanage.setCurrentcapacity(currentcapacity);
                            cmsStoragemanageDS.updateCmsStoragemanage(cmsStoragemanage);
                        }
                        // 如果待修改的FTP记录的空间大小比先前的该条记录的空间大小小
                        else if (compareSpacesize < 0)
                        {
                            currentcapacity = String.valueOf((long) (Math.floor((Long.parseLong(cmsStoragemanage
                                    .getCurrentcapacity()) * 1024 + moreSpacesize) / 1024F)));
                            cmsStoragemanage.setCurrentcapacity(currentcapacity);
                            cmsStoragemanageDS.updateCmsStoragemanage(cmsStoragemanage);
                        }

                        // 操作成功增加更新ftp的日志
                        CommonLogUtil.insertOperatorLog(objectId, FTPConstant.MGTTYPE_FTP,
                                FTPConstant.OPERTYPE_FTP_MOD, FTPConstant.OPERTYPE_FTP_MOD_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in updateIcmFtpinfo method", e);
            operResult = CommonLogConstant.OPTRESUT_FALSE + ":"
                    + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_FAIL);
            throw new DomainServiceException(e);
        }

        log.debug("updateIcmFtpinfo end");
        return operResult;
    }

    /**
     * 删除一条ftp记录 实现流程： 1、获取临时区绑定的空间 2.还原临时区绑定的空间的可用空间大小 3.根据条件在服务器上删除临时区目录
     */
    public String deleteIcmFtpinfo(Long infoindex) throws DomainServiceException
    {

        String objectId = infoindex + "";
        String operResult = CommonLogConstant.OPTRESUT_SUCCESS + ":"
                + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        log.debug("deleteIcmFtpinfo start");
        try
        {
            IcmFtpinfo icmftpinfo = this.icmftpinfods.getIcmFtpinfo(infoindex);
            if (icmftpinfo == null)
            {
                return CommonLogConstant.OPTRESUT_FALSE + ":"
                        + parser.getResourceItem(FTPConstant.OPERTYPE_FTP_NOTEXIST);
            }

            // 获取临时区绑定的空间
            CmsStoragemanage cmsStormange = new CmsStoragemanage();
            cmsStormange.setStorageindex(icmftpinfo.getSpaceindex());
            CmsStoragemanage cmsStoragemanage = cmsStoragemanageDS.getCmsStoragemanage(cmsStormange);
            if (cmsStoragemanage == null)
            {
                //return "1:获取该FTP的临时区空间失败";
                return ResourceMgt.findDefaultText("ftp.get.temporaryspace.failed");
            }

            StringBuffer destfile = new StringBuffer("");
            destfile.append(GlobalConstants.getMountPoint() + icmftpinfo.getFtppath());
            File destDir = new File(destfile.toString());
            if (destDir.exists())
            {
                if (destDir.list().length == 0)// 目录不存在子文件或子目录,则删除
                {
                    destDir.delete();
                }
                else
                {
                    //return "1:该文件目录下存在子文件夹或子文件,不能删除";
                    return ResourceMgt.findDefaultText("ftp.delete.cant.forsubfile");
                }
            }
            else
            {
                //return "1:该文件目录不存在或已被删除";
                return ResourceMgt.findDefaultText("ftp.filecatalog.notexist");

            }

            this.icmftpinfods.deleteIcmFtpinfo(icmftpinfo);

            // 还原临时区绑定的空间的可用空间大小
            String currentcapacity = String.valueOf((long) (Math.floor((Long.parseLong(cmsStoragemanage
                    .getCurrentcapacity()) * 1024 + icmftpinfo.getSpacesize()) / 1024F)));
            cmsStoragemanage.setCurrentcapacity(currentcapacity);
            cmsStoragemanageDS.updateCmsStoragemanage(cmsStoragemanage);

            // 操作成功增加删除ftp的日志
            CommonLogUtil.insertOperatorLog(objectId, FTPConstant.MGTTYPE_FTP, FTPConstant.OPERTYPE_FTP_DEL,
                    FTPConstant.OPERTYPE_FTP_DEL_INFO, CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (DomainServiceException e)
        {
            log.debug("Error occurred in deleteIcmFtpinfo method", e);
            operResult = CommonLogConstant.OPTRESUT_FALSE + ":"
                    + parser.getResourceItem(CommonLogConstant.RESOURCE_OPERATION_FAIL);
            throw new DomainServiceException(e);
        }

        log.debug("deleteIcmFtpinfo end");
        return operResult;
    }

    /**
     * 分页查询
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize) throws DomainServiceException
    {

        log.debug("pageInfoQuery start");
        TableDataInfo dataInfo = null;
        try
        {
            icmFtpinfo.setFtppath(EspecialCharMgt.conversion(icmFtpinfo.getFtppath()));
            icmFtpinfo.setFtpport(EspecialCharMgt.conversion(icmFtpinfo.getFtpport()));            
            icmFtpinfo.setServerip(EspecialCharMgt.conversion(icmFtpinfo.getServerip()));
            icmFtpinfo.setFtpaccount(EspecialCharMgt.conversion(icmFtpinfo.getFtpaccount()));

            dataInfo = icmftpinfods.pageInfoQuery(icmFtpinfo, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQuery error");
        }
        log.debug("pageInfoQuery end");
        return dataInfo;
    }

    /**
     * 分页查询
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {

        log.debug("pageInfoQuery start");
        TableDataInfo dataInfo = null;
        try
        {
            icmFtpinfo.setServerip(EspecialCharMgt.conversion(icmFtpinfo.getServerip()));
            icmFtpinfo.setFtpaccount(EspecialCharMgt.conversion(icmFtpinfo.getFtpaccount()));

            dataInfo = icmftpinfods.pageInfoQuery(icmFtpinfo, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQuery error");
        }
        log.debug("pageInfoQuery end");
        return dataInfo;

    }

    /**
     * 根据主键获得一条ftp记录
     */
    public IcmFtpinfo getIcmFtpinfo(Long infoindex) throws DomainServiceException
    {

        log.debug(" getIcmFtpinfo  start");
        IcmFtpinfo icmFtpinfo = null;
        try
        {
            icmFtpinfo = (IcmFtpinfo) this.icmftpinfods.getIcmFtpinfo(infoindex);
        }
        catch (Exception e)
        {
            log.error("Error occurred in getIcmFtpinfo method", e);
            throw new DomainServiceException(e);
        }
        log.debug("getIcmFtpinfo  end");
        return icmFtpinfo;
    }

    /*******************************************************************************************************************
     * 公告栏上显示FTP信息
     ******************************************************************************************************************/
    public IcmFtpinfo getIcmFtpinfoForBulletin() throws DomainServiceException
    {
        RIAContext contexts = RIAContext.getCurrentInstance();
        ISession session = contexts.getSession();
        HttpSession httpSession = session.getHttpSession();// 冲session中获得当前CPID
        String cpid = (String) httpSession.getAttribute("CPID");
        IcmFtpinfo ftpinfo = new IcmFtpinfo();
        if (null != cpid && (!"".equals(cpid)))
        {// 操作员是CP权限组但是没有绑定CP
            ftpinfo.setCpid(cpid);
            List<IcmFtpinfo> ftpinfolist = this.icmftpinfods.getIcmFtpinfoByCond(ftpinfo);
            if (ftpinfolist.size() > 0 && ftpinfolist != null)
            {
                ftpinfo = ftpinfolist.get(0);
                return ftpinfo;
            }
        }
        return null;
    }

    // 修改之前查询服务器上该目录下是否存在子目录或子文件
    public String isExistFiles(Long infoindex) throws DomainServiceException
    {

        log.debug(" isExistFiles  start");
        IcmFtpinfo icmFtpinfo = null;
        String returnResult = "0";
        try
        {
            icmFtpinfo = (IcmFtpinfo) this.icmftpinfods.getIcmFtpinfo(infoindex);
            if (icmFtpinfo == null)
            {

                //return "1:ftp信息不存在或已被删除";
                return ResourceMgt.findDefaultText("ftp.message.not.exist");
            }
            else
            {
                StringBuffer destfile = new StringBuffer("");
                destfile.append(GlobalConstants.getMountPoint() + icmFtpinfo.getFtppath());
                File destDir = new File(destfile.toString());
                if (destDir.exists())
                {
                    if (destDir.list().length != 0)
                    {
                        return "1";
                    }
                }
                else
                {
                    //return "1:该文件目录不存在或已被删除";
                    return ResourceMgt.findDefaultText("ftp.filecatalog.notexist");
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in getIcmFtpinfo method", e);
            throw new DomainServiceException(e);
        }
        log.debug(" isExistFiles  end");
        return returnResult;
    }

    // 获取临时区目录
    public String getStroageareaPath() throws DomainServiceException
    {

        log.debug("getStroageareaPath  start");
        String stroageareaPath = "";
        // 获取临时区路径
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            stroageareaPath = storageareaLs.getAddress(TEMPAREA_ID);// 临时区
            if (null == stroageareaPath || (TEMPAREA_ERRORSiGN).equals(stroageareaPath))
            {// 获取临时区地址失败
                return "1";
            }
        }
        catch (Exception e)
        {

            log.error("Error occured in getStroageareaPath method", e);
            throw new DomainServiceException(e);
        }

        log.debug("getStroageareaPath  end");
        return stroageareaPath;
    }

    // 获取临时区绑定的空间
    public CmsStoragemanage getStroagmanage(String storageAreatype) throws DomainServiceException
    {

        log.debug("getStroagmanage  start");
        CmsStoragemanage areaTypeStorage = new CmsStoragemanage();
        areaTypeStorage.setStoragearea(storageAreatype);
        List<CmsStoragemanage> bindedSpaceList = cmsStoragemanageDS.getCmsStoragemanageofbindedSpace(areaTypeStorage);
        // 取出符合条件的第一个空间
        CmsStoragemanage cmsStoragemanage = new CmsStoragemanage();
        if (bindedSpaceList.size() > 0)
        {
            cmsStoragemanage = bindedSpaceList.get(0);
        }
        else
        {
            return null;
        }
        log.debug("getStroagmanage  end");
        return cmsStoragemanage;
    }

    private String getFtpFolderName(String ftpPath)
    {
        int lastSlashIndex = ftpPath.lastIndexOf('/');
        String folderName = ftpPath.substring(lastSlashIndex + 1);
        return folderName;
    }

    private int findfirstspace(String str)
    {
        int firstspace = -1;
        int index = 0;
        int totallength = 0;
        if (!str.trim().equals(""))
        {
            totallength = str.length();
            while (totallength > 0)
            {

                if (" ".equals(str.substring(index, index + 1)) || "\t".equals(str.substring(index, index + 1)))
                {
                    firstspace = index;
                    break;
                }
                index++;
                totallength--;
            }
        }
        return firstspace;
    }

    private String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 执行shell
     * 
     * @param command String
     * @return String
     * @throws Exception Exception
     */
    private String exeCmd(String command) throws Exception
    {
        String retCmdInfo = null;
        Process process = null;
        try
        {

            Runtime runtime = Runtime.getRuntime();

            process = runtime.exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            String test = in.toString();
            int index = 0;

            while ((line = in.readLine()) != null)
            {

                if (retCmdInfo == null)
                {
                    retCmdInfo = line;
                }
                else
                {
                    retCmdInfo += ("\r\n" + line);
                }

            }
            process.destroy();

        }
        catch (Exception e)
        {
            if (process != null)
            {
                process.destroy();
            }
            throw e;
        }
        return retCmdInfo;
    }

}

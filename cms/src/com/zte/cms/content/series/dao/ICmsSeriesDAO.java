package com.zte.cms.content.series.dao;

import java.util.List;

import com.zte.cms.content.series.model.CmsSeries;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsSeriesDAO
{
    /**
     * 新增CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DAOException dao异常
     */
    public void insertCmsSeries(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据主键更新CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DAOException dao异常
     */
    public void updateCmsSeries(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据主键删除CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @throws DAOException dao异常
     */
    public void deleteCmsSeries(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据主键查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return 满足条件的CmsSeries对象
     * @throws DAOException dao异常
     */
    public CmsSeries getCmsSeries(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据ID查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return 满足条件的CmsSeries对象
     * @throws DAOException dao异常
     */
    public CmsSeries getCmsSeriesById(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据条件查询CmsSeries对象
     * 
     * @param cmsSeries CmsSeries对象
     * @return 满足条件的CmsSeries对象集
     * @throws DAOException dao异常
     */
    public List<CmsSeries> getCmsSeriesByCond(CmsSeries cmsSeries) throws DAOException;

    /**
     * 根据条件分页查询CmsSeries对象，作为查询条件的参数
     * 
     * @param cmsSeries CmsSeries对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSeries对象，作为查询条件的参数
     * 
     * @param cmsSeries CmsSeries对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据模糊条件查询并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    
    /**
     * 根据模糊条件查询连续剧平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    
    /**
     * 根据模糊条件查询栏目连续剧关联关系平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    /**
     * 根据模糊条件查询栏目连续剧关联关系网元发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    /**
     * 根据模糊条件查询连续剧角色关联关系平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    /**
     * 根据模糊条件查询连续剧角色关联关系发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    
    /**
     * 根据seriesid和platform查询连续剧关联平台数据
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize) throws DAOException;
    
    /**
     * 根据seriesid查询关联角色表中的角色名和角色类型
     * 
     */
    public List<CmsSeries> getSeriesCastTypeName(CmsSeries cmsSeries) throws DAOException;
    
    

}
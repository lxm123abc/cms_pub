package com.zte.cms.task.service;

import java.util.List;

import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskCmsauditRecord;
import com.zte.cms.task.model.TaskFunction;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import javax.servlet.http.HttpServletResponse;

public interface ITaskBasicDS
{
    /**
     * 根据条件分页查询CmsCpbind对象
     * 
     * @param taskBasic CmsCpbind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsCpbind对象
     * 
     * @param taskBasic CmsCpbind对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    
    public String insertTask(TaskCmsaudit taskBasic) throws DomainServiceException;

	public TaskCmsaudit getTaskmap(TaskCmsaudit taskmap) throws DomainServiceException;
	
	public List<TaskCmsaudit> getattchList(TaskCmsaudit taskmap) throws DomainServiceException;

	public void updateTaskmap(TaskCmsaudit noticemap) throws DomainServiceException;

	public void deleteAttach(String attachid) throws DomainServiceException;
	
	public void deleteTaskBasic(TaskCmsaudit taskmap) throws DomainServiceException;

	public void toAuditTask(TaskCmsaudit taskmap2) throws DomainServiceException;
	
	public List<TaskFunction> getTaskFunctList(TaskFunction taskfunction) throws DomainServiceException;

	public TableDataInfo pageAuditRecordInfoQuery(TaskCmsauditRecord record, int start, int pageSize);

	public TaskCmsauditRecord exportAuditRecordByTaskId(TaskCmsauditRecord record, String userId, HttpServletResponse response);
}
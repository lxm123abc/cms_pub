package com.zte.cms.content.program.service;

import java.util.List;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramBufDS
{
    /**
     * 新增CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 新增CmsProgramBuf对象，不重新获取index
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgramBufNoPK(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 新增CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException;

    /**
     * 更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 批量更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException;

    /**
     * 根据条件更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException;

    /**
     * 删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 批量删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException;

    /**
     * 根据条件删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException;

    /**
     * 查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @return CmsProgramBuf对象
     * @throws DomainServiceException ds异常
     */
    public CmsProgramBuf getCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 根据条件来查询单个的 CmsProgramBuf 对象
     * 
     * @param cmsProgramBuf
     * @return
     * @throws Exception
     */
    public CmsProgramBuf getcmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws Exception;

    /**
     * 根据条件查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @return 满足条件的CmsProgramBuf对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
package com.zte.cms.EMBtask.dao;

import java.util.List;

import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IMovieTechauditResultDAO
{
    /**
     * 新增MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @throws DAOException dao异常
     */
    public void insertMovieTechauditResult(MovieTechauditResult movieTechauditResult) throws DAOException;

    /**
     * 根据条件更新MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult更新条件
     * @throws DAOException dao异常
     */
    public void updateMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult) throws DAOException;

    /**
     * 根据条件删除MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult删除条件
     * @throws DAOException dao异常
     */
    public void deleteMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult) throws DAOException;

    /**
     * 根据条件查询MovieTechauditResult对象
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @return 满足条件的MovieTechauditResult对象集
     * @throws DAOException dao异常
     */
    public List<MovieTechauditResult> getMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DAOException;

    /**
     * 根据条件分页查询MovieTechauditResult对象，作为查询条件的参数
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询MovieTechauditResult对象，作为查询条件的参数
     * 
     * @param movieTechauditResult MovieTechauditResult对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
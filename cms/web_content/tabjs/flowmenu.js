function DisplayMenu() {
	var menuVector = getMenuVector();
	var menustr = '<ul id="cmsmenu" class="sf-menu">';
	// 一级菜单开始
	for (var k = 0; k < menuVector.length; k++) {
		var menu = menuVector[k];
		var description = menu.description;
		var menuName = menu.name;
		menustr = menustr + '<li>';
		if (menu.target && menu.enabled) {
			menustr = menustr + ' <a href="#" ';
			if (menu.display == false) {
				menustr = menustr + ' style="display:none" ';
			}
			menustr = menustr + ' >' + menuName + '</A>';
		} else if (menu.display == true && menu.enabled == false) {
			menustr = menustr + ' <A href="#" id="linkmenu' + menu.orderId + '" >' + menuName + '</A>';
		} else if (menu.gray == true) {
			menustr = menustr + ' <A href="#" id="linkmenu' + menu.orderId + '"  ><font color="#808080">' + menuName + '</A>';
		} else {
			menustr = menustr + ' <A class="sf-with-ul first-one" href="#" id="linkmenu' + menu.orderId + '"';
			if (menu.display == false) {
				menustr = menustr + ' style="display:none" ';
			}
			menustr = menustr + ' >' + menuName + '<span class="sf-sub-indicator"> &#187;</span></A>';
		}
		// 二级菜单开始
		if (menu.menuList.length > 0) {
			menustr = menustr + '<ul>';
			for (var j = 0; j < menu.menuList.length; j++) {
				var childMenu = menu.menuList[j];
				var childMenudescription = childMenu.description;
				var childMenuName = childMenu.name;
				var onclickEvent = "";
				if (childMenu.uri.indexOf("?") != -1) {
					var uri = ".." + childMenu.uri;
					onclickEvent = "window.top.myTab.Cts(" + "'" + childMenuName + "','" + uri + "');";
				}
				menustr = menustr + '<li>';
				if (childMenu.target && childMenu.enabled) {
					menustr = menustr + ' <a href="#" ';
					if (childMenu.display == false) {
						menustr = menustr + ' style="display:none" ';
					}
					menustr = menustr + ' >' + childMenuName + '</A>';
				} else if (childMenu.display == true && childMenu.enabled == false) {
					menustr = menustr + ' <A href="#" id="linkmenu' + childMenu.orderId + '" >' + childMenuName + '</A>';
				} else if (childMenu.gray == true) {
					menustr = menustr + ' <A href="#" id="linkmenu' + childMenu.orderId + '" ><font color="#808080">' + childMenuName + '</A>';
				} else {					
					menustr = menustr + ' <A href="#" id="linkmenu' + childMenu.orderId + '" onclick="' + onclickEvent + '"';					
					if (childMenu.menuList.length > 0) {
						menustr = menustr + ' class="sf-with-ul"';
					}
					if (childMenu.display == false) {
						menustr = menustr + ' style="display:none" ';
					}
					menustr = menustr + ' >' + childMenuName;
					if (childMenu.menuList.length > 0) {
						menustr = menustr + '<span class="sf-sub-indicator"> &#187;</span>';
					}
					menustr = menustr + '</A>';
				}

				// 三级菜单开始
				if (childMenu.menuList.length > 0) {

					menustr = menustr + '<ul>';
					for (var i = 0; i < childMenu.menuList.length; i++) {
						var subMenu = childMenu.menuList[i];
						var subMenudescription = subMenu.description;
						var bg2ClassSubMenu = "bg3";

						// wanghao
						if (subMenu.menuList.length > 0) {
							bg2ClassSubMenu = "bg2";
						}
						var subMenuName = subMenu.name;
						var subMenuOnclickEvent = "";
						if (subMenu.uri.indexOf("?") != -1) {
							var uriSubMenu = ".." + subMenu.uri;
							subMenuOnclickEvent = "window.top.myTab.Cts(" + "'" + subMenuName + "','" + uriSubMenu + "');";
						}
						menustr = menustr + '<li>';

						if (subMenu.target && subMenu.enabled) {
							menustr = menustr + ' <a href="#" ';
							if (subMenu.display == false) {
								menustr = menustr + ' style="display:none" ';
							}
							menustr = menustr + ' onclick="' + subMenuOnclickEvent + '" >' + subMenuName + '</A>';
						} else if (subMenu.display == true && subMenu.enabled == false) {
							menustr = menustr + ' <A onclick="' + subMenuOnclickEvent + '"   href="#" id="linkmenu_' + subMenu.orderId + '">' + subMenuName + '</A>';
						} else if (subMenu.gray == true) {
							menustr = menustr + ' <A onclick="' + subMenuOnclickEvent + '"   href="#" id="linkmenu_' + subMenu.orderId + '"><font color="#808080">' + subMenuName + '</A>';
						} else {
							menustr = menustr + ' <A onclick="' + subMenuOnclickEvent + '" href="#" id="linkmenu_' + subMenu.orderId + '"';
							// menustr = menustr +' <A onclick="window.top.fraMain.addPage('+"'"+subMenuName+"'"+');return false;" href="#" id="linkmenu_'+ subMenu.orderId+'"' ;
							if (subMenu.display == false) {
								menustr = menustr + ' style="display:none" ';
							}
							menustr = menustr + '>' + subMenuName + '</A>';

						}

						// 4级菜单
						if (subMenu.menuList.length > 0) {
							menustr = menustr + '<ul style="display:none;" >';
							for (var i = 0; i < subMenu.menuList.length; i++) {
								var fourthMenu = subMenu.menuList[i];
								var fourthMenudescription = fourthMenu.description;
								var fourthMenuName = fourthMenu.name;
								// wanghao
								var bg2ClassFourthMenu = "bg3";

								if (fourthMenu.menuList.length > 0) {
									bg2ClassFourthMenu = "bg2";
								}
								var fourthMenuName = fourthMenu.name;
								var fourthMenuOnclickEvent = "";
								if (fourthMenu.uri.indexOf("?") != -1) {
									// var uriFourthMenu = "../.."+subMenu.uri;
									var uriFourthMenu = ".." + subMenu.uri;
									// fourthMenuOnclickEvent = "window.top.fraMain.addPage("+"'"+fourthMenuName+"','"+uriFourthMenu+"');return false;";
									fourthMenuOnclickEvent = "window.top.myTab.Cts(" + "'" + fourthMenuName + "','" + uriFourthMenu + "');";
								}
								menustr = menustr + '<li class="' + bg2ClassFourthMenu + '" ';

								if (fourthMenu.alt) {
									menustr = menustr + 'title=' + fourthMenudescription;
								}
								menustr = menustr + ' >';
								if (fourthMenu.target && fourthMenu.enabled) {
									menustr = menustr + ' <a href="#" ';
									if (fourthMenu.display == false) {
										menustr = menustr + ' style="display:none" ';
									}
									menustr = menustr + ' onclick="' + fourthMenuOnclickEvent + '"  >' + fourthMenuName + '</A>';
								} else if (fourthMenu.display == true
										&& fourthMenu.enabled == false) {
									menustr = menustr + ' <A onclick="'
											+ fourthMenuOnclickEvent
											+ '"    href="#" id="linkmenu_'
											+ fourthMenu.orderId + '">'
											+ fourthMenuName + '</A>';
								} else if (fourthMenu.gray == true) {
									menustr = menustr + ' <A onclick="'
											+ fourthMenuOnclickEvent
											+ '"    href="#" id="linkmenu_'
											+ fourthMenu.orderId
											+ '"><font color="#808080">'
											+ fourthMenuName + '</A>';
								} else {
									menustr = menustr + ' <A onclick="'
											+ fourthMenuOnclickEvent
											+ '"    href="#" id="linkmenu_'
											+ fourthMenu.orderId + '"';
									// menustr = menustr +' <A onclick="window.top.fraMain.addPage('+"'"+fourthMenuName+"'"+');return false;" href="#" id="linkmenu_'+ fourthMenu.orderId+'"' ;
									if (fourthMenu.display == false) {
										menustr = menustr + ' style="display:none" ';
									}
									menustr = menustr + '>' + fourthMenuName + '</A>';
								}
								menustr = menustr + '</li>';
							}
							menustr = menustr + '</ul>';
						}
						// end-----
						menustr = menustr + '</li>';
					}
					menustr = menustr + '</ul>';
				}
				menustr = menustr + '</li>';
			}
			menustr = menustr + '</ul></div>';
		}
		menustr = menustr + '</li>';
	}
	menustr = menustr + '</ul>';
	document.getElementById('showMenu').innerHTML = menustr;
}

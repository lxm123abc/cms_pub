package com.zte.cms.batchobjectrecord.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class BatchObjectRecord extends DynamicBaseObject
{
    private java.lang.Long recordindex;
    private java.lang.String batchid;
    private java.lang.Long objindex;
    private java.lang.Integer objtype;
    private java.lang.Integer desttype;
    private java.lang.Long destindex;

    public java.lang.Long getRecordindex()
    {
        return recordindex;
    }

    public void setRecordindex(java.lang.Long recordindex)
    {
        this.recordindex = recordindex;
    }

    public java.lang.String getBatchid()
    {
        return batchid;
    }

    public void setBatchid(java.lang.String batchid)
    {
        this.batchid = batchid;
    }

    public java.lang.Long getObjindex()
    {
        return objindex;
    }

    public void setObjindex(java.lang.Long objindex)
    {
        this.objindex = objindex;
    }

    public java.lang.Integer getObjtype()
    {
        return objtype;
    }

    public void setObjtype(java.lang.Integer objtype)
    {
        this.objtype = objtype;
    }

    public java.lang.Integer getDesttype()
    {
        return desttype;
    }

    public void setDesttype(java.lang.Integer desttype)
    {
        this.desttype = desttype;
    }

    public java.lang.Long getDestindex()
    {
        return destindex;
    }

    public void setDestindex(java.lang.Long destindex)
    {
        this.destindex = destindex;
    }

    public void initRelation()
    {
        this.addRelation("recordindex", "RECORDINDEX");
        this.addRelation("batchid", "BATCHID");
        this.addRelation("objindex", "OBJINDEX");
        this.addRelation("objtype", "OBJTYPE");
        this.addRelation("desttype", "DESTTYPE");
        this.addRelation("destindex", "DESTINDEX");
    }
}

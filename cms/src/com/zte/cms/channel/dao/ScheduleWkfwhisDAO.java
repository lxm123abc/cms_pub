package com.zte.cms.channel.dao;

import java.util.List;


import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ScheduleWkfwhisDAO extends DynamicObjectBaseDao implements IScheduleWkfwhisDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException
    {
        log.debug("insert channelWkfwhis starting...");
        super.insert("insertScheduleWkfwhis", scheduleWkfwhis);
        log.debug("insert channelWkfwhis end");
    }

    public void updateScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException
    {
        log.debug("update channelWkfwhis by pk starting...");
        super.update("updateScheduleWkfwhis", scheduleWkfwhis);
        log.debug("update channelWkfwhis by pk end");
    }

    public void deleteScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException
    {
        log.debug("delete channelWkfwhis by pk starting...");
        super.delete("deleteScheduleWkfwhis", scheduleWkfwhis);
        log.debug("delete channelWkfwhis by pk end");
    }

    public ScheduleWkfwhis getScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DAOException
    {
        log.debug("query channelWkfwhis starting...");
        ScheduleWkfwhis resultObj = (ScheduleWkfwhis) super.queryForObject("getScheduleWkfwhis", scheduleWkfwhis);
        log.debug("query channelWkfwhis end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ScheduleWkfwhis> getScheduleWkfwhisByCond(ScheduleWkfwhis scheduleWkfwhis) throws DAOException
    {
        log.debug("query channelWkfwhis by condition starting...");
        List<ScheduleWkfwhis> rList = (List<ScheduleWkfwhis>) super.queryForList("queryScheduleWkfwhisListByCond",
                scheduleWkfwhis);
        log.debug("query channelWkfwhis by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize) throws DAOException
    {
        log.debug("page query channelWkfwhis by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryScheduleWkfwhisListCntByCond", scheduleWkfwhis)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ScheduleWkfwhis> rsList = (List<ScheduleWkfwhis>) super.pageQuery("queryScheduleWkfwhisListByCond",
                    scheduleWkfwhis, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query channelWkfwhis by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryScheduleWkfwhisListByCond", "queryScheduleWkfwhisListCntByCond",
                scheduleWkfwhis, start, pageSize, puEntity);
    }

}
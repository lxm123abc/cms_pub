/**
 * obj:     后台返回的数据对象。例如:$M.zteTable
 * tableId: 表格ID
 */
var __purviewMap = {};
var __contextPath = {};

function noDataTrips(obj, tableId)
{
    
    //分页数据返回的是PageDataInfo类型，其data属性为数据数组
    if((obj.data == undefined && obj.length==0)||obj.data.length == 0)
    {
        //设置所关联的表格ID
        var ztableid = tableId;
        //显示出来的表格
        var displayTB = getTableById(ztableid).table;
        //显示出来的表格的列数
        var colNum = displayTB.rows[0].cells.length;
        //因为没有数据，新增一空白提示行
        var tipRow = displayTB.insertRow(-1);
        //新建提示单元，设置其属性以及要显示的文本
        var tipTd = document.createElement("td");
        tipTd.style.cssText = "color:red; text-align:center; font-size:12px";
        tipTd.setAttribute("colSpan",colNum);
        tipTd.insertBefore(document.createTextNode($res_entry('msg.note.notListforCondition')),null);
        //将提示单元加入行
        tipRow.insertBefore(tipTd,null);
    }
    
}

function getError(errordes){
    var reg = /(\w+):?(.*)/;
    var rtn = errordes.match(reg);
    var error = {};
    error.code = rtn[1];
    error.des = rtn[2];
    return error;
}

/**
 * @errordes 错误描述 格式为 code:des
 * @url 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 */
function openMsg(errordes, url, errorback){
    var _url = getContextPath() + '/CMS/common/msg.html';
    var p_closeFlag = true;
    var error = getError(errordes);
    if (url != undefined && url != null && (errorback || errorback == undefined || (!errorback && error.code == "0"))) {
        _url = _url + '?url=' + url;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrorcode").value = error.code;
    document.getElementById("txtErrordes").value = error.des;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    
    popdialog.openWithIframe($res_entry('msg.note.inf'), _url, 310, 200, p_closeFlag);
}
/**
 * @info 显示信息，任意字符串
 * @url 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 */
function openMsgBox(errordes, url, errorback){
    var _url = getContextPath() + '/umap/common/msg1.html';
    var p_closeFlag = true;

    if (url != undefined && url != null && errorback) {
        _url = _url + '?url=' + url;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrordes").value = errordes;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    
    var srceenwidth= window.screen.width;
    var msgwidth = srceenwidth / 3.2 ;
    if(msgwidth<320)
    	msgwidth = 320;
    popdialog.openWithIframe($res_entry('msg.note.inf'), _url, msgwidth, 200, p_closeFlag);
}


/**
 * @info 显示信息，任意字符串
 * @myurl 返回url
 * @errorback 出错是否返回url中的页面，默认为true
 * @width  提示框宽
 * @heigth 提示框高
 */
function openMsgBoxSize(errordes, windowwidth,windowheigth,myurl, errorback){
    var _url = getContextPath() + '/CMS/common/msg1.html';
    var p_closeFlag = true; 

    if (myurl != undefined && myurl != null && errorback) {
        _url = _url + '?url=' + myurl;
        p_closeFlag = false;
    }
    
    document.getElementById("txtErrordes").value = errordes;
    document.getElementById("imgClose").src = getContextPath() + '/skins/common/close.gif';
    

    popdialog.openWithIframe($res_entry('msg.note.inf'), _url, windowwidth, windowheigth, p_closeFlag);
}


function getContextPath() {
    // 在index中已做了处理
    var txtContextpath = parent.document.getElementById("txtContextpath");
    if (txtContextpath != null) {
        return txtContextpath.value;
    }
    if (__contextPath.rtnValue == undefined) {
        callUrl('systemMgt/getContextPath.ssm', null, '__contextPath', true);
    }
    if (__contextPath.rtnValue != undefined) {
        return __contextPath.rtnValue;
    }
    return "";
}

/**
 * @value 权限值，funcname
 * @return 如果有权限返回值为1，否则为null
 */
function getPurview(value) {
    // 在index中已做了处理
    if (parent._purviewMap != undefined) {
        return parent._purviewMap[value];
    }
    if (__purviewMap.rtnValue == undefined) {
        callUrl('umapLoginMgt/getPurview.ssm', null, '__purviewMap', true);
    }
    if (__purviewMap.rtnValue != undefined) {
        return __purviewMap.rtnValue[value];
    }
    return null;
}

function canOpen(value) {
    if (getPurview(value) != "1") {
        var l = parent.location;
        if (l == undefined){
            l = location;
        }
        //l.href = getContextPath() + '/uiloader/login.html';
        return false;
    }
    return true;
}
//用于框架页面，弹出不返回到登陆页面
function canOpen2(value) {
    if (getPurview(value) != "1") 
	{
        var l = parent.location;
        if (l == undefined)
		{
            l = location;
        }   
        return false;
    }
    return true;
}


//操作弹出菜单js
function menuFix() {
    var DivRef = document.all('PopupDiv');
    var IfrRef = document.all('DivShim');
    var sfEls = [];
    var k = 1;
    var count=0;
    var container = [];
    
    while(k <= _primary_key) {
		for(var m = 1; m <= _primary_key; m++) {
		    var sfEl = document.getElementById("nav" + m +k);
		    var sfId="nav" + m +k;
		    if(sfEl == "undefined" || sfEl == null)
		    {
			    continue;
			}
		    else
		    {
		    	var flag = false;
		    	for(var c=0;c<container.length;c++)
		    	{
		    		if(sfId == container[c])		    		
		    		{
		    			flag = true;
		    			break;
		    		}
		    	}
		    	
		    	if(	flag == false )	
		    	{			    	
			    	count++;
			    	container[count-1] = sfId;
			    	sfEls[count-1] = sfEl;
				    break;
				}
			}
	    }
	    	    
	    k ++;
    }

    for (var i = 0; i < sfEls.length; i ++) {
        var ifr = document.getElementById(IfrRef[i].id);
        var divR = document.getElementById(DivRef[i].id);

        document.getElementById(sfEls[i].id).onmouseover = function() {
            var ii=0;
		    var len=sfEls.length;
		    for(var j=0; j<len; j++){
			    if(document.getElementById(sfEls[j].id) == this){
				    ii=j;
				    break;
			    }
		    }

	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	        divR = document.getElementById(DivRef[ii].id);
	        ifr = document.getElementById(IfrRef[ii].id);
	        ifr.style.display = "block";
	        ifr.style.width = divR.offsetWidth;
	        ifr.style.height = divR.offsetHeight;
	        ifr.style.top = divR.style.top;
	        ifr.style.left = divR.style.left;
	        ifr.style.zIndex = divR.style.zIndex + 1;
	        
	    }
	
	    document.getElementById(sfEls[i].id).onMouseDown = function() {
	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	    }
	
	    document.getElementById(sfEls[i].id).onMouseUp = function() {
	        this.className += (this.className.length > 0 ? " " : "") + "sfhover";
	    }
	
	    document.getElementById(sfEls[i].id).onmouseout=function() {
	        this.className = this.className.replace(new RegExp("( ?|^)sfhover\\b"), "");
	        ifr.style.display = "none";
	    }
    }
}

	//查询历史状态   ref 要绑定数据的控件如: "$M.obj" , 传空的时候不进行绑定， statusSaveObj 在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
	function searchHisData(ref, statusSaveObj)
	{
		var parentObj = getWindowParent();
		if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
		{
			statusSaveObj = "QueryConObj";
		}
		var tableHisStatus = parentObj[statusSaveObj];
		if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
		{
			var condition = tableHisStatus.condition;
			//将查询条件绑定到控件上
			if(typeof(ref) != "undefined" && ref != null)
			{
				setBindObject(ref, condition[0]);
			}	
			getTurnPageData(tableHisStatus.tid, condition,tableHisStatus.pageIndex, tableHisStatus.pagesize);
			
		}	
	}

	//得到最顶层页面的对象
	function getWindowParent()
	{
		var parentObj = window.parent;
		var indexUrl = parentObj.location.href;
		var count = 0;
		while(indexUrl != undefined && indexUrl !=null && indexUrl.indexOf('/uiloader/index.html') == -1)
		{
			count++;
			parentObj = parentObj.parent;
			indexUrl = parentObj.location.href;
			if(count>20)
			{
				break;
			}
		}
		count = 0;
		return parentObj;
	}

	//保存table历史状态的类 
	function TableHisStatus(tid, condition, pageIndex, pagesize)
	{
		this.tid = tid;
		this.pageIndex = pageIndex;
		this.pagesize = pagesize;
		this.condition  = condition;
	}

	//进去其他链接时 如进入修改或者查询等页面前 调用此方法保存状态  tableID 为<z:table> 的id  ， statusSaveObj 为在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
	function savePageStatus(tableID, statusSaveObj)
	{
		var ztable = getTableById(tableID);
		var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

		var parentObj = getWindowParent();
		if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
		{
			parentObj['QueryConObj']=hisStatus;
		}
		else
		{
			parentObj[statusSaveObj]=hisStatus;
		}
	}
	//用于一个页面有两个服务时
   //进去其他链接时 如进入修改或者查询等页面前 调用此方法保存状态  tableID 为<z:table> 的id  ， statusSaveObj 为在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
	function savePageStatus2(tableID, statusSaveObj)
	{
		var ztable = getTableById(tableID);
		var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

		var parentObj = getWindowParent();
		if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
		{
			parentObj['QueryConObj']=hisStatus;
		}
		else
		{
			parentObj['serviceSaveObj']=hisStatus;
		}
	}
	//查询历史状态   ref 要绑定数据的控件如: "$M.obj" , 传空的时候不进行绑定， statusSaveObj 在uiloader/index.html页面保存状态的对象，不传该参数时 默认为 QueryConObj 对象
	function searchHisDataForFrame(ref, sid,statusSaveObj)
	{
		var parentObj = getWindowParent();
		if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
		{
			statusSaveObj = "QueryConObj";
		}
		var tableHisStatus = parentObj[statusSaveObj];
		if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
		{
			var condition = tableHisStatus.condition;
			//将查询条件绑定到控件上
			if(typeof(ref) != "undefined" && ref != null)
			{
				setBindObject(ref, condition[0]);
			}	
             tableModel_arr[tableHisStatus.tid].sid = sid;
			getTurnPageData(tableHisStatus.tid, condition,tableHisStatus.pageIndex, tableHisStatus.pagesize);
			//parentObj[statusSaveObj]={};
		}	
	}
	
	//使用历史查询时，得到历史查询的查询条件
	function getHisQueryCondition(statusSaveObj)
	{
		var parentObj = getWindowParent();
		if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
		{
			statusSaveObj = "QueryConObj";
		}
		var tableHisStatus = parentObj[statusSaveObj];
		if(typeof(tableHisStatus) != "undefined" && tableHisStatus != null)
		{
			var condition = tableHisStatus.condition;
			return condition[0];
		}
		else
		{
			return null;
		}
	}
//计算UTF8字符串占用字节数
function utf8_strlen2(str) 
{ 
    var cnt = 0; 
    for( i=0; i<str.length; i++) 
    { 
        var value = str.charCodeAt(i); 
        if( value < 0x080) 
        { 
            cnt += 1; 
        } 
        else if( value < 0x0800) 
        { 
            cnt += 2; 
        } 
        else  if(value < 0x010000)
        { 
            cnt += 3; 
        }
		else
		{
			cnt += 4;
		}
    } 
    return cnt; 
}
//检验电话号码  数字、字母、-
function isMobile(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
//检验邮编  数字、字母、-
function isZipCode(str)
{
	if(str.length > 20)
	{
		return false;
	}
	else
	{
		var regex=/^[0-9a-zA-Z\-]*$/;
		return regex.test(str);
	}
}
//在做提交前清空所有的提示信息
function clearAllMsgLabel()
{
	$("label[@id$='_msg']").html("");
}

//动态改变select 框的长度
function dynChgSelectWidth(id)
{
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width=7*utf8_strlen2(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}
//解决上传控件漂移问题,需要重新初始化控件
addOnloadEvent(function() {
	Upload_init();
})

/**
	 * 动态改变select 框的长度
	 * @param id   下拉框id
	 * @return
	 */
	function dynChgWidth(id)
	{
		var selectObj = document.getElementById(id);
		if(selectObj != undefined && selectObj != null)
		{
			var v  = selectObj.options[selectObj.selectedIndex].text;
			var width = 8*gbk_strlen2(v);
			width = width + getexlens(v);
			width = (width<130 ? 130 : width);
			selectObj.style.width=width;
		}
	}
	
	/**
	 * 计算GBK字符串占用字节数
	 * @param str  要计算长度的字符串
	 * @return    字符串的字节长度
	 */
	function gbk_strlen2(str) 
	{ 
		var cnt = 0; 
		for( i=0; i<str.length; i++) 
		{ 
		    var value = str.charCodeAt(i); 
		    if( value < 0x080) 
		    { 
		        cnt += 1; 
		    } 
		    else if( value < 0x0800) 
		    { 
		        cnt += 2; 
		    } 
		    else  if(value < 0x010000)
		    { 
		        cnt += 2; 
		    }
			else
			{
				cnt += 4;
			}
		} 
		return cnt; 
	}	 
	
	function getexlens(obj)
	{
	    var mask1 = "w_";
	    var mask2 = "m";
	    var mask3 = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
	    var mask4 = "W";
	    var mask5 = "0123456789";
	    
	    var len = 0;
	    var countW = 0;
	    var count = 0;
	    var countnum = 0;
	    
	    for(var i = 0; i < obj.length; i++)
	    {
	        if(mask1.indexOf(obj.charAt(i)) != -1)
	        {
	            count++;
	            len = len + 2;
	        } else if(mask2.indexOf(obj.charAt(i)) != -1)
	        {
	            count++;
	            len = len + 2;
	        }else if(mask3.indexOf(obj.charAt(i)) != -1)
	        {
	            count++;
	            len = len + 2;
	        }
	        else if(mask4.indexOf(obj.charAt(i)) != -1)
	        {
	            count++;
	            countW++;
	            len = len + 4;
	        }
	        else if(mask5.indexOf(obj.charAt(i)) != -1)
	        {
	            count++;
	            countnum++;
	        }    
	    }
	
	    if(count > 0 && count < obj.length)
	    {
	       len = len + 6;
	    }
	    
	    if(countW>0 && countW<15)
	    {
	        len = len + 25;
	    }else if(countW>0 && countW<23){
	        len = len + 10;
	    }
	    
	    if(count == 0)
	    {
	        len = len + 4;
	    }
	    if(countnum > 0)
	    {
	        len = len + 4;
	    }
	    return len;
	}
	
	
	//进行页面校验
function checkPropertyByRex(propertyname,isneed,type,length)
{   
	var tempStr=trim(document.getElementById(propertyname).value);
    if ( isneed && (tempStr=="")) 
    {
        document.getElementById(propertyname + "_msg").style.display="";
        document.getElementById(propertyname + "_msg").innerText= $res_entry("msg.check.noempty","");
        document.getElementById(propertyname).focus();
        return false;
    }
    if ( typeof(type) != 'undefined' && type!=null && type!="") 
    {
        var rex;
        var text;
        var flag = true;
        if(type == "special")
        {
        	var pattern = /[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\<\>\/?]/;
        	if(pattern.test(tempStr))
        	{	
        		flag = false;
            	text = $res_entry("msg.check.special","")+"` ~ ! @ # $ % ^ & * ( ) = + \\ | [ { ] } ; : ' \" , < > /";
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        else if(type == "allstring")
        {
        	var len=utf8_strlen2(tempStr);
        	if(len > length)
        	{
            	flag = false;
            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
            }
        } 
        else if(type == "string")
        {
        	//var pattern = /\{|\}|\||(\/>)|(\]\]>)/;
        	var pattern = /[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\.\<\>\/?]/
        	var showpattern = "`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\.\<\>\/? ";
        	if(pattern.test(tempStr))
        	{	
        		flag = false;
            	//text = $res_entry("msg.check.special","")+"|  {  }  ]]>  \/>";
            	text = $res_entry("msg.check.special","")+showpattern;
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        else if(type == "number")
        {
            rex=new RegExp("^((0?)|[1-9][0-9]{0,"+(length-1)+"})$");
            flag = rex.test(tempStr);
            text=$res_entry("msg.check.number.prefix","")+length+$res_entry("msg.check.number.suffix","");
        }
        else if(type == "code")
        {
            rex=new RegExp("^[0-9]{0,"+length+"}$");
            flag = rex.test(tempStr);
            text=$res_entry("msg.check.number.prefix","")+length+$res_entry("msg.check.code.suffix","");
        }
        else if(type == "rtsp")
        {
            var pattern = /^(rtsp|RTSP):\/\/([\w-]+\.)+[\w-]+\/([^\\\/:\*\?\"<>\|]+\/)*[\w-\?#&]+(\.[\w-\?#&]+)?$/;
        	if(!pattern.test(tempStr))
        	{	
        		flag = false;
            	text = $res_entry("msg.check.format","")
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        
        if (!flag && tempStr != "") 
        {
            document.getElementById(propertyname + "_msg").style.display="";
            document.getElementById(propertyname + "_msg").innerText = text;
            document.getElementById(propertyname).focus();
            return false;
        }
    }
    document.getElementById(propertyname + "_msg").innerText="";
    document.getElementById(propertyname + "_msg").style.display="none";
    return true;
}

function beginTimeLEndTime(beginTime,endTime,input_msg){
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.12"); //"开始时间必须小于等于结束时间";
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;               
    }
  }
}


  package com.zte.cms.content.program.programsync.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ProgramPlatformSynDAO extends DynamicObjectBaseDao implements IProgramPlatformSynDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCntTargetsysSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("insert cntTargetsysSyn starting...");
    	super.insert( "insertCntTargetsysSyn", cntTargetsysSyn );
    	log.debug("insert cntTargetsysSyn end");
    }
     
    public void updateCntTargetsysSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("update cntTargetsysSyn by pk starting...");
       	super.update( "updateCntTargetsysSyn", cntTargetsysSyn );
       	log.debug("update cntTargetsysSyn by pk end");
    }

    public void updateCntTargetsysSynByCond( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("update cntTargetsysSyn by conditions starting...");
    	super.update( "updateCntTargetsysSynByCond", cntTargetsysSyn );
    	log.debug("update cntTargetsysSyn by conditions end");
    }   

    public void deleteCntBindTgsysSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("delete cntTargetsysSyn by pk starting...");
       	super.delete( "deleteCntBindTgsysSyn", cntTargetsysSyn );
       	log.debug("delete cntTargetsysSyn by pk end");
    }
    
    public void deleteCntTargetsysSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("delete cntTargetsysSyn by pk starting...");
       	super.delete( "deleteCntTargetsysSyn", cntTargetsysSyn );
       	log.debug("delete cntTargetsysSyn by pk end");
    }

    public void deleteCntTargetsysSynByCond( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("delete cntTargetsysSyn by conditions starting...");
    	super.delete( "deleteCntTargetsysSynByCond", cntTargetsysSyn );
    	log.debug("update cntTargetsysSyn by conditions end");
    } 
    
    public void deleteCntTargetsysSynByProgramindex( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("delete cntTargetsysSyn by conditions starting...");
    	super.delete( "deleteCntTargetsysSynByProgramindex", cntTargetsysSyn );
    	log.debug("update cntTargetsysSyn by conditions end");
    }   

    public ProgramPlatformSyn getCntTargetsysSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query cntTargetsysSyn starting...");
       	ProgramPlatformSyn resultObj = (ProgramPlatformSyn)super.queryForObject( "getCntTargetsysSyn",cntTargetsysSyn);
       	log.debug("query cntTargetsysSyn end");
       	return resultObj;
    }

    public ProgramPlatformSyn getCntTargetsysBindSyn( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query cntTargetsysSyn starting...");
       	ProgramPlatformSyn resultObj = (ProgramPlatformSyn)super.queryForObject( "getCntTargetsysBindSyn",cntTargetsysSyn);
       	log.debug("query cntTargetsysSyn end");
       	return resultObj;
    }   
    //按条件查询
    public int getCntTargetsysBindSynPub( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query cntTargetsysSyn starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynPub",cntTargetsysSyn);
       	log.debug("query cntTargetsysSyn end");
       	return result;
    } 
    
    public int getCntTargetsysBindSynCnt( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query cntTargetsysSyn starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynCnt",cntTargetsysSyn);
       	log.debug("query cntTargetsysSyn end");
       	return result;
    } 
    
    //完全发布成功
    public int getCntTargetsysBindSynCntPubSuccess( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynCntPubSuccess starting...");
       	int result = (Integer)super.queryForObject( "getCntTargetsysBindSynCntPubSuccess",cntTargetsysSyn);
       	log.debug("query getCntTargetsysBindSynCntPubSuccess end");
       	return result;
    }
    
	@SuppressWarnings("unchecked")
    public List<ProgramPlatformSyn> getCntTargetsysSynByCond( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query cntTargetsysSyn by condition starting...");
    	List<ProgramPlatformSyn> rList = (List<ProgramPlatformSyn>)super.queryForList( "queryCntTargetsysSynListByCond",cntTargetsysSyn);
    	log.debug("query cntTargetsysSyn by condition end");
    	return rList;
    }
	
    public List<ProgramPlatformSyn> getCntTargetsysBindSynPubSuccess( ProgramPlatformSyn cntTargetsysSyn )throws DAOException
    {
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition starting...");
    	List<ProgramPlatformSyn> rList = (List<ProgramPlatformSyn>)super.queryForList( "getCntTargetsysBindSynPubSuccess",cntTargetsysSyn);
    	log.debug("query getCntTargetsysBindSynPubSuccess by condition end");
    	return rList;
    }
    
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize)throws DAOException
    {
    	log.debug("page query programPlatformSyn by condition starting...");
     	PageInfo pageInfo = null;
     	if (programPlatformSyn.getTypePlatform().equals("iptv3") )
     	{
        	int totalCnt = ((Integer) super.queryForObject("queryProgramPlatfSynListCntByCond",  programPlatformSyn)).intValue();
        	if( totalCnt > 0 )
        	{
        		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
        		List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryProgramPlatfSynListByCond" ,  programPlatformSyn , start , fetchSize );
        		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        	}
        	else
        	{
        		pageInfo = new PageInfo();
        	}
     		
     	}
     	if (programPlatformSyn.getTypePlatform().equals("iptv2") )
     	{

        	int totalCnt = ((Integer) super.queryForObject("queryProgramPlatfSyn2ListCntByCond",  programPlatformSyn)).intValue();
        	if( totalCnt > 0 )
        	{
        		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
        		List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryProgramPlatfSyn2ListByCond" ,  programPlatformSyn , start , fetchSize );
        		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        	}
        	else
        	{
        		pageInfo = new PageInfo();
        	}
     		
     	}

    	log.debug("page query programPlatformSyn by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramPlatformSyn cntTargetsysSyn, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryCntTargetsysSynListByCond", "queryCntTargetsysSynListCntByCond", cntTargetsysSyn, start, pageSize, puEntity);
    }

    public ProgramPlatformSyn getProgramPlatformSyn3( ProgramPlatformSyn programPlatformSyn )throws DAOException
    {
    	log.debug("query programPlatformSyn starting...");
    	ProgramPlatformSyn resultObj = (ProgramPlatformSyn)super.queryForObject( "getProgramPlatf3Syn",programPlatformSyn);
       	log.debug("query programPlatformSyn end");
       	return resultObj;
    }
    
    public ProgramPlatformSyn getProgramPlatformSyn2( ProgramPlatformSyn programPlatformSyn )throws DAOException
    {
    	log.debug("query programPlatformSyn starting...");
    	ProgramPlatformSyn resultObj = (ProgramPlatformSyn)super.queryForObject( "getProgramPlatf2Syn",programPlatformSyn);
       	log.debug("query programPlatformSyn end");
       	return resultObj;
    }    
} 
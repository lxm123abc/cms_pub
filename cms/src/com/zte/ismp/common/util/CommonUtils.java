package com.zte.ismp.common.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtils;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.util.Util;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.zxywpub.encrypt;

public class CommonUtils
{
    private static Log log = SSBBus.getLog(CommonUtils.class);
    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    // 过滤数据库脚本中需要转义的字符
    private static String[][] FilterDBScriptChars = { { "\\", "\\\\" }, { "\'", "\'\'" }, { "'", "''" } };

    /**
     * 取得zxdbm_umap.usys_config配置表中的信息
     * 
     * @param cfgkey
     * @return
     */
    public static String getConfigValue(String cfgkey)
    {
        String cfgvalue = "";
        IUsysConfigDS usysCfg = (IUsysConfigDS) SSBBus.findDomainService("usysConfigDS");
        UsysConfig usysConfig = new UsysConfig();
        usysConfig.setCfgkey(cfgkey);
        try
        {
            UsysConfig usysConfigEnd = usysCfg.getUsysConfig(usysConfig);
            cfgvalue = usysConfigEnd.getCfgvalue();
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch bloc
            e.printStackTrace();
            log.error("system param cfgkey=" + cfgkey + " is null", e);
            return null;
        }
        return cfgvalue;
    }

    /**
     * 将日期如(2008-11-26 15:00:23) 转换为14位的字符串 (20081126150023)
     * 
     * @param time19
     * @return
     */
    public static String get14Time(String time19)
    {
        if (Util.isStrEmpty(time19))
        {
            return "";
        }
        else if (time19.length() == 14)
        {
            return time19;
        }
        else if (time19.length() != 19)
        {
            return time19;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            Date date = sdf.parse(time19);
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            log.info("get14Time" + e);
            return "";
        }
    }

    /**
     * 将日期如 (20081126150023)转换为19位的字符串 (2008-11-26 15:00:23)
     * 
     * @param time19
     * @return
     */
    public static String get19Time(String time14)
    {
        if (Util.isStrEmpty(time14))
        {
            return "";
        }
        else if (time14.length() == 19)
        {
            return time14;
        }
        else if (time14.length() != 14)
        {
            return time14;
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            Date date = sdf.parse(time14);
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            log.info("get19Time" + e);
            return "";
        }
    }

    /**
     * 根据毫秒得到14 位格式的日期
     * 
     * @param date
     * @return
     */
    public static String get14TimeByLong(long date)
    {
        Date d = new Date(date);
        return sDateFormat.format(d);
    }

    /**
     * 将当前时间转换为14位格式的日期
     * 
     * @return
     */
    public static String getCurrentTimeString()
    {
        Date date = new Date();
        return sDateFormat.format(date);
    }

    /**
     * 根据yyyyMMddHHmmss时间得到Date
     * 
     * @param time
     * @return
     */
    public static Date getDateByTimeString(String source) throws Exception
    {
        return sDateFormat.parse(source);
    }

    /**
     * 根据指定的时间格式得到Date
     * 
     * @param time
     * @return
     */
    public static Date getDateByFormat(String time, String format) throws Exception
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.parse(time);
        }
        catch (Exception e)
        {
            log.error("getDateByFormat exception:" + e);
            throw e;
        }
    }

    /**
     * 根据指定的时间格式得到时间的字符串表示形式
     * 
     * @param time
     * @return
     */
    public static String getTimeStringByFormat(Date date, String format) throws Exception
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(date);
        }
        catch (Exception e)
        {
            log.error("getTimeStringByFormat exception:" + e);
            throw e;
        }
    }

    /**
     * 变长解密 ，去掉后面的 000，得到原始密码
     * 
     * @param ciphertext
     * @return
     */
    public static String varLenDecrypt(String ciphertext)
    {
        String pwd = encrypt.VarLenEncrypt(ciphertext, -1);
        if (!Util.isStrEmpty(pwd) && pwd.length() >= 3)
        {
            pwd = pwd.substring(0, pwd.length() - 3);
        }
        else
        {
            log.info("varLenDecrypt : ciphertext length less than three");
        }
        return pwd;
    }

    /**
     * 变长加密 在密码后面 000，避免解密出现错误，默认加密成35位长度
     * 
     * @param pwd
     * @return 返回null， 加密失败
     */
    public static String varLenEncrypt(String pwd)
    {
        if (Util.isStrEmpty(pwd))
        {
            pwd = "";
        }
        if (pwd.length() > 32)
        {
            return null;
        }
        pwd += "000";
        return encrypt.VarLenEncrypt(pwd, 35);
    }

    /**
     * 变长加密， pwd的长度不能大于 len
     * 
     * @param pwd 明文
     * @param len 加密后的长度
     * @return
     */
    public static String varLenEncrypt(String pwd, int len)
    {
        if (Util.isStrEmpty(pwd))
        {
            pwd = "";
        }
        if (pwd.length() > (len - 3))
        {
            return null;
        }
        pwd += "000";
        return encrypt.VarLenEncrypt(pwd, len);
    }

    /**
     * 判断一个字符串是不是yyyyMMddHHmmss 格式
     * 
     * @param s
     */
    public static boolean isValidDate(String s)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            sdf.parse(s);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    /**
     * 日期相加
     * 
     * @param dateStr日期字符串 ，格式和dtfmt指定的格式相同 (如:yyyyMMddHHmmss)
     * @param m 时间增量 (为正数时日期相加，为负时日期相减)
     * @param model 要增加的日期字段 1. 年 2. 月 3. 日 4. 时 5. 分 6. 秒
     * @param dtfmt 日期格式
     * @return 月份相加的结果
     */
    public static String dateOperation(String dateStr, int m, int model, String dtfmt)
    {
        try
        {
            SimpleDateFormat myFormatter = new SimpleDateFormat(dtfmt);
            Date mydate = myFormatter.parse(dateStr);
            Calendar cal = Calendar.getInstance();
            cal.setTime(mydate);
            switch (model)
            {
                case 1: // year
                    cal.add(Calendar.YEAR, m);
                    break;
                case 2: // month
                    cal.add(Calendar.MONTH, m);
                    break;
                case 3: // day
                    cal.add(Calendar.DATE, m);
                    break;
                case 4: // hour
                    cal.add(Calendar.HOUR, m);
                    break;
                case 5: // minute
                    cal.add(Calendar.MINUTE, m);
                    break;
                case 6: // second
                    cal.add(Calendar.SECOND, m);
                    break;
                default:
                    break;
            }
            return myFormatter.format(cal.getTime());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 填充IP地址，对不足3位的前面补0 如：10.129.62.30 填充后 010.129.062.030
     * 
     * @param ipaddr
     * @return
     */
    public static String fillIpAddr(String ipaddr)
    {
        String str[] = ipaddr.split("\\.");
        if (str != null && str.length == 4)
        {
            for (int i = 0; i < 4; i++)
            {
                if (str[i].length() == 1)
                {
                    str[i] = "00" + str[i];
                }
                else if (str[i].length() == 2)
                {
                    str[i] = "0" + str[i];
                }
            }
            String str1 = str[0] + "." + str[1] + "." + str[2] + "." + str[3];
            return str1;
        }
        return ipaddr;
    }

    /**
     * 去掉IP地址前面的0. 如010.129.062.030 去掉0后 10.129.62.30
     * 
     * @param ipaddr
     * @return
     */
    public static String trimIpAddr(String ipaddr)
    {
        String str[] = ipaddr.split("\\.");
        if (str != null && str.length == 4)
        {
            for (int i = 0; i < 4; i++)
            {
                if ("000".equals(str[i]))
                {
                    str[i] = "0";
                }
                else
                {
                    for (int j = 0; j < str[i].length(); j++)
                    {
                        if (!(str[i].charAt(j) == '0'))
                        {
                            str[i] = str[i].substring(j);
                            break;
                        }
                    }
                }
            }
            String str1 = str[0] + "." + str[1] + "." + str[2] + "." + str[3];
            return str1;
        }
        return ipaddr;
    }

    /**
     * IP地址格式校验
     * 
     * @param s
     * @return true 符合IP地址格式 false 不符合
     */
    public static boolean ipValid(String s)
    {
        String regex0 = "(2[0-4]\\d)" + "|(25[0-5])";
        String regex1 = "1\\d{2}";
        String regex2 = "[1-9]\\d";
        String regex3 = "\\d";
        String regex = "(" + regex0 + ")|(" + regex1 + ")|(" + regex2 + ")|(" + regex3 + ")";
        regex = "(" + regex + ")\\.(" + regex + ")\\.(" + regex + ")\\.(" + regex + ")";

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(s);
        return m.matches();
    }

    /**
     * 生成32位唯一值
     * 
     * @return
     */
    public static String getKey()
    {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }

    /**
     * 拷贝对象属性值
     * 
     * @param dest 目标对象
     * @param orig 源对象
     * @throws Exception
     * @since ZXMCISMP-UCMPV2.01.01
     */
    public static void copyProperties(Object dest, Object orig) throws Exception
    {
        try
        {
            Map map = BeanUtils.describe(orig);
            Set set = map.keySet();
            Iterator it = set.iterator();
            while (it.hasNext())
            {
                Object key = it.next();
                Object p = map.get(key);
                if (p != null && !key.equals("class"))
                {
                    BeanUtils.setProperty(dest, key.toString(), p);
                }
            }
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
            throw new Exception(e);
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace();
            throw new Exception(e);
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    /**
     * 将字符串转换为数字
     * 
     * @param str 要转换的字符串
     * @param nInit 转换发生异常是的返回值
     * @return
     */
    public static int str2Int(String str, int nInit)
    {
        int nret = nInit;
        try
        {
            nret = Integer.parseInt(str);
        }
        catch (Exception e)
        {
            nret = nInit;
            e.printStackTrace();
            log.error(e);
        }
        return nret;
    }

    /**
     * 将字符串转换为数字,转换发生异常是返回0
     * 
     * @param str 要转换的字符串
     * @return
     */
    public static int str2Int(String str)
    {
        return str2Int(str, 0);
    }

    /**
     * 判断字符串是否为空
     * 
     * @param str
     * @return true 为空 ， false 不为空
     */
    public static boolean isStrEmpty(String str)
    {
        return ((str == null) || (str.trim().length() == 0));
    }

    /**
     * 判断列表是否为空
     * 
     * @param collection
     * @return true 为空 ，false 不为空
     */
    public static boolean isEmpty(Collection collection)
    {
        return (collection == null || collection.isEmpty());
    }

    /**
     * 判断Map是否为空
     * 
     * @param map
     * @return true 为空 ，false 不为空
     */
    public static boolean isEmpty(Map map)
    {
        return (map == null || map.isEmpty());
    }

    /**
     * 文件路径相连
     * 
     * @param path
     * @param subPath
     * @return
     */
    public static String appendPath(String path, String subPath)
    {
        boolean parentEmp = isStrEmpty(path);
        boolean childEmp = isStrEmpty(subPath);
        if ((parentEmp) && (childEmp))
        {
            return "";
        }
        if (parentEmp)
        {
            return subPath;
        }
        if (childEmp)
        {
            return path;
        }

        if ((path.endsWith("/")) || (path.endsWith("\\")))
        {
            return path + subPath;
        }

        return path + File.separator + subPath;
    }

    /**
     * 分割字符串
     * 
     * @param str 要分割的字符串
     * @param spilit_sign 字符串的分割标志
     * @return 分割后得到的字符串数组
     */
    public static String[] strSpilit(String str, String spilit_sign)
    {

        String[] spilit_string = str.split(spilit_sign);
        if (spilit_string[0].equals(""))
        {
            String[] new_string = new String[spilit_string.length - 1];
            for (int i = 1; i < spilit_string.length; i++)
            {
                new_string[i - 1] = spilit_string[i];
            }
            return new_string;
        }
        else
        {
            return spilit_string;
        }
    }

    /**
     * 用特殊的字符连接字符串
     * 
     * @param strings 要连接的字符串数组
     * @param spilit_sign 连接字符
     * @return 连接字符串
     */
    public static String strConnect(String[] strings, String spilit_sign)
    {

        StringBuffer strbuff = new StringBuffer();
        for (int i = 0; i < strings.length; i++)
        {
            strbuff.append(strings[i]);
            strbuff.append(spilit_sign);
        }
        return strbuff.toString();
    }

    /**
     * 过滤数据库脚本中的特殊字符（包括回车符(\n)和换行符(\r)）
     * 
     * @param str 要进行过滤的字符串
     * @return 过滤后的字符串
     */
    public static String strFilterDBScriptChar(String str)
    {

        if (str == null)
        {
            return "";
        }
        String[] str_arr = strSpilit(str, "");
        for (int i = 0; i < str_arr.length; i++)
        {
            for (int j = 0; j < FilterDBScriptChars.length; j++)
            {
                if (FilterDBScriptChars[j][0].equals(str_arr[i]))
                {
                    str_arr[i] = FilterDBScriptChars[j][1];
                }
            }
        }
        return (strConnect(str_arr, "")).trim();
    }

    /**
     * 对页面输入的字符串进行校验
     * 
     * @param a String
     * @return String
     */
    public static String trans(String a)
    {

        if (a == null)
        {
            return "";
        }
        else
        {
            String b = a.trim();
            b = strFilterDBScriptChar(b);
            return b;
        }
    }

    public static void main(String[] args)
    {
        try
        {
            //System.out.println(getDateByTimeString("20100613101730"));
            
            System.out.println(varLenDecrypt("uYXvzXs6qKT7ndOAzSis_pBZUydr9T"));


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

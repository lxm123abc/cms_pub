package com.zte.cms.task.dao;

import java.util.List;

import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskFunction;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsTaskBasicDAO
{
    /**
     * 根据条件分页查询UcpBasic对象，作为查询条件的参数
     * 
     * @param UcpBasic ucpBasic对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询UcpBasic对象，作为查询条件的参数
     * 
     * @paramUcpBasic ucpBasic对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

	public void insertTask(TaskCmsaudit taskBasic) throws DAOException;

	public TaskCmsaudit getTaskmap(TaskCmsaudit taskmap) throws DAOException;
	public TaskFunction getTaskFunction(TaskFunction taskfunction) throws DAOException;

	public void updateTaskmap(TaskCmsaudit noticemap) throws DAOException;

	public List<TaskCmsaudit> getattchList(TaskCmsaudit taskmap) throws DAOException;

	public TaskCmsaudit getAttachByid(String attachid) throws DAOException;
	
	 public void deleteTaskBasic(TaskCmsaudit taskmap) throws DAOException;

	public void toAuditTask(TaskCmsaudit taskmap) throws DAOException;
	
	public List<TaskFunction> getTaskFunctList(TaskFunction taskfunction)  throws DAOException;

}
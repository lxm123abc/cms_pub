package com.zte.cms.basicdata.common;

public class CmsProgramtypeConstant
{

    public static final String MGTTYPE_CMSPROGRAMTYPE = "log.cmsprogramtype.operator";

    public static final String OPERTYPE_CMSPROGRAMTYPE_ADD = "log.cmsprogramtype.add";
    public static final String OPERTYPE_CMSPROGRAMTYPE_UPD = "log.cmsprogramtype.update";
    public static final String OPERTYPE_CMSPROGRAMTYPE_DEL = "log.cmsprogramtype.delete";

    public static final String OPERTYPE_CMSPROGRAMTYPE_ADD_INFO = "log.cmsprogramtype.add.info";
    public static final String OPERTYPE_CMSPROGRAMTYPE_UPD_INFO = "log.cmsprogramtype.update.info";
    public static final String OPERTYPE_CMSPROGRAMTYPE_DEL_INFO = "log.cmsprogramtype.delete.info";

    public static final String OPERTYPE_PUBLISH_CMSPROGRAMTYPE_PUBLISH = "log.cmsprogramtype.publish";
    public static final String OPERTYPE_CANCLE_PUBLISH_CMSPROGRAMTYPE_PUBLISH = "log.cmsprogramtype.canclepublish";

    public static final String OPERTYPE_PUBLISH_CMSPROGRAMTYPE_PUBLISH_INFO = "log.cmsprogramtype.publish.info";
    public static final String OPERTYPE_CANCLE_PUBLISH_CMSPROGRAMTYPE_PUBLISH_INFO = "log.cmsprogramtype.canclepublish.info";

    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

    public static final String FAIL = "cmsprogramtype.fail";
    public static final String SUCCESS = "cmsprogramtype.success";

}

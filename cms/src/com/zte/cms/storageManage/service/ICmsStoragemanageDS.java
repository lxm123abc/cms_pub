package com.zte.cms.storageManage.service;

import java.util.List;

import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStoragemanageDS
{

    // public String getRootAddress() ;
    /**
     * 新增CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 更新CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 批量更新CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException;

    /**
     * 删除CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 批量删除CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStoragemanageList(List<CmsStoragemanage> cmsStoragemanageList) throws DomainServiceException;

    /**
     * 查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return CmsStoragemanage对象
     * @throws DomainServiceException ds异常
     */
    public CmsStoragemanage getCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DomainServiceException;

    /**
     * 根据条件查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return 满足条件的CmsStoragemanage对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsStoragemanage> getCmsStoragemanageByCond(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象 从设备那里得到空间的优先级 查出来 返回
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */

    public TableDataInfo pageInfoQueryPriority(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    // 通过连表查询，使空间得到设备的优先级
    public List<CmsStoragemanage> getCmsStoragemanageByCondPriority(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException;

    // 查找绑定的空间中优先级最高，容量最大，且可用的空间
    public List<CmsStoragemanage> getCmsStoragemanageofbindedSpace(CmsStoragemanage cmsStoragemanage)
            throws DomainServiceException;

    /*******************************************************************************************************************
     * 查询storageManageme中设备下面各存储区的信息
     ******************************************************************************************************************/
    public List<CmsStoragemanage> getCmsStoragemanageList() throws DomainServiceException;
}
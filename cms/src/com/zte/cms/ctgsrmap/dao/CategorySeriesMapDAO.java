package com.zte.cms.ctgsrmap.dao;

import java.util.List;

import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CategorySeriesMapDAO extends DynamicObjectBaseDao implements ICategorySeriesMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException
    {
        log.debug("insert categorySeriesMap starting...");
        super.insert("insertCategorySeriesMap", categorySeriesMap);
        log.debug("insert categorySeriesMap end");
    }

    public void updateCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException
    {
        log.debug("update categorySeriesMap by pk starting...");
        super.update("updateCategorySeriesMap", categorySeriesMap);
        log.debug("update categorySeriesMap by pk end");
    }

    public void deleteCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException
    {
        log.debug("delete categorySeriesMap by pk starting...");
        super.delete("deleteCategorySeriesMap", categorySeriesMap);
        log.debug("delete categorySeriesMap by pk end");
    }

    public CategorySeriesMap getCategorySeriesMap(CategorySeriesMap categorySeriesMap) throws DAOException
    {
        log.debug("query categorySeriesMap starting...");
        CategorySeriesMap resultObj = (CategorySeriesMap) super.queryForObject("getCategorySeriesMap",
                categorySeriesMap);
        log.debug("query categorySeriesMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CategorySeriesMap> getCategorySeriesMapByCond(CategorySeriesMap categorySeriesMap) throws DAOException
    {
        log.debug("query categorySeriesMap by condition starting...");
        List<CategorySeriesMap> rList = (List<CategorySeriesMap>) super.queryForList(
                "queryCategorySeriesMapListByCond", categorySeriesMap);
        log.debug("query categorySeriesMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query categorySeriesMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCategorySeriesMapListCntByCond", categorySeriesMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CategorySeriesMap> rsList = (List<CategorySeriesMap>) super.pageQuery(
                    "queryCategorySeriesMapListByCond", categorySeriesMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query categorySeriesMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategorySeriesMap categorySeriesMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCategorySeriesMapListByCond", "queryCategorySeriesMapListCntByCond",
                categorySeriesMap, start, pageSize, puEntity);
    }

}
package com.zte.cms.channel.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zte.cms.channel.dao.ICmsChannelDAO;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.Generator;
import com.zte.purview.access.model.UcpBasic;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsChannelDS extends DynamicObjectBaseDS implements ICmsChannelDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    public String channelId = "";
    private ICmsChannelDAO dao = null;

    public void setDao(ICmsChannelDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsChannel(CmsChannel cmsChannel) throws DomainServiceException
    {
        log.debug("insert cmsChannel starting...");
        try
        {
            Long channelIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_ucdn_channel");
            cmsChannel.setChannelindex(channelIndex);
            channelId = Generator.getContentId(Long.parseLong(cmsChannel.getCpid()), "CHAN");
            cmsChannel.setChannelid(channelId);
            //String cpcontentid = Generator.getWGCodeByContentId(channelId);
            cmsChannel.setCpcontentid(channelId);
            dao.insertCmsChannel(cmsChannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsChannel end");
    }

    public void updateCmsChannel(CmsChannel cmsChannel) throws DomainServiceException
    {
        log.debug("update cmsChannel by pk starting...");
        try
        {
            dao.updateCmsChannel(cmsChannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsChannel by pk end");
    }

    public void updateCmsChannelList(List<CmsChannel> cmsChannelList) throws DomainServiceException
    {
        log.debug("update cmsChannelList by pk starting...");
        if (null == cmsChannelList || cmsChannelList.size() == 0)
        {
            log.debug("there is no datas in cmsChannelList");
            return;
        }
        try
        {
            for (CmsChannel cmsChannel : cmsChannelList)
            {
                dao.updateCmsChannel(cmsChannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsChannelList by pk end");
    }

    public void removeCmsChannel(CmsChannel cmsChannel) throws DomainServiceException
    {
        log.debug("remove cmsChannel by pk starting...");
        try
        {
            dao.deleteCmsChannel(cmsChannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsChannel by pk end");
    }

    public void removeCmsChannelList(List<CmsChannel> cmsChannelList) throws DomainServiceException
    {
        log.debug("remove cmsChannelList by pk starting...");
        if (null == cmsChannelList || cmsChannelList.size() == 0)
        {
            log.debug("there is no datas in cmsChannelList");
            return;
        }
        try
        {
            for (CmsChannel cmsChannel : cmsChannelList)
            {
                dao.deleteCmsChannel(cmsChannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsChannelList by pk end");
    }

    public CmsChannel getCmsChannel(CmsChannel cmsChannel) throws DomainServiceException
    {
        log.debug("get cmsChannel by pk starting...");
        CmsChannel rsObj = null;
        try
        {
            rsObj = dao.getCmsChannel(cmsChannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsChannelList by pk end");
        return rsObj;
    }

    // 按照cpid得到内容提供商
    public UcpBasic getUcpbasicBy(UcpBasic ucpBasic)
    {
        UcpBasic basic = null;
        try
        {
            String sql = "select * from ucp_basic where cptype=" + ucpBasic.getCptype() + " and cpid='"
                    + ucpBasic.getCpid() + "'";
            List list = new DbUtil().getQuery(sql);
            basic = new UcpBasic();
            for (Iterator iterator = list.iterator(); iterator.hasNext();)
            {
                Map map = (Map) iterator.next();
                basic.setCpid((String) map.get("cpid"));
                basic.setCpindex(Long.parseLong((String) map.get("cpindex")));
                basic.setCpcnshortname((String) map.get("cpcnshortname"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return basic;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsChannel cmsChannel, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsChannel page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsChannel, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsChannel>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsChannel page info by condition end");
        return tableInfo;
    }

    public List getChannelByCond(CmsChannel channel)
    {
        return dao.getChannelByCond(channel);
    }

    public void deleteChannelContent(CmsChannel channel)
    {
        try
        {
            dao.deleteCmsChannel(channel);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
    }

    public CmsChannel getCmsChannelById(CmsChannel cmsChannel) throws DomainServiceException
    {
        log.debug("get getCmsChannelById ds starting...");
        CmsChannel rsObj = null;
        try
        {
            rsObj = dao.getCmsChannelById(cmsChannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getCmsChannelById ds end");
        return rsObj;
    }
}

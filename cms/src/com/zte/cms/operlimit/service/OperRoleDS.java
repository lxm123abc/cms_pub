package com.zte.cms.operlimit.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zte.cms.common.Generator;
import com.zte.cms.operlimit.dao.IOperRoleDAO;
import com.zte.cms.operlimit.model.OperRole;
import com.zte.cms.operlimit.model.OperatorLimit;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;

public class OperRoleDS extends DynamicObjectBaseDS implements IOperRoleDS {

	private IOperRoleDAO dao = null;
	OperatorLimit operRole=new OperatorLimit();
	
	private Log log = SSBBus.getLog(getClass());
	public void setDao(IOperRoleDAO dao) {
		this.dao = dao;
	}

	@Override
	public List getOperRole() throws DomainServiceException {
		List<OperRole> list=dao.getOperRole(new OperRole());
		return list;
	}
	
	public List getUserOperFunById(Integer operGrpId) throws DomainServiceException {
		operRole=new OperatorLimit();
		operRole.setOperid(operGrpId);
		List<OperatorLimit> list=dao.getFuncById(operRole);
		return list;
	}

	
	@Override
	public boolean insertOperRole(Map operRoleValues) throws DomainServiceException {
		 log.debug("insert cmsChannel starting...");
		 Iterator it = operRoleValues.entrySet().iterator();
		 Map aas = new HashMap();
		 while(it.hasNext()){
			 Map.Entry entry =(Map.Entry) it.next();
			 aas.put(entry.getKey(), entry.getValue());
		 }
		 
		 String str = aas.get("FUNCID").toString();
		 
		 String operid = aas.get("OPERGRPID").toString();
		 int newoperid=Integer.parseInt(operid);
		 operRole.setOperid(newoperid);
		 try {
			dao.deleteOperRole(operRole);
		
		 //根据operid删除信息
		   String[] split = str.split(",");
		    for (int i = 0; i < split.length; i++) {
			 String funcid = split[i];
			 int newfuncid =Integer.parseInt(funcid);
		     operRole.setFuncid(newfuncid);
			 operRole.setOperid(newoperid);
			 dao.insertOperRole(operRole);
			} 
		   return true;
		 }catch (DAOException daoEx) {
				log.error("dao exception:", daoEx);
	            throw new DomainServiceException(daoEx);
	            
			}
			
		}
		 
	
		  
	      
	}



package com.zte.cms.channel.update.apply;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsScheduleBuf;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IScheduleUpdateApply
{

    /**
     * 修改工作流申请
     * 
     * @param channel CmsChannel
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsSchedule schedule) throws DomainServiceException;

    /**
     * 
     * @param channel CmsChannel
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsSchedule schedule, String priority) throws DomainServiceException;

    /**
     * 修改工作流审核
     * 
     * @param channelbuf CmsChannelBuf
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String audit(CmsScheduleBuf schedulebuf, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException;

    /**
     * 
     * @param channelbuf CmsChannelBuf
     * @return CmsChannelBuf
     * @throws Exception Exception
     */
    public CmsScheduleBuf getScheduleBuf(CmsScheduleBuf schedulebuf) throws Exception;

}
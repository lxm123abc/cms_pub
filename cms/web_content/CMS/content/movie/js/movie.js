//新增页面获取节目时长
function getDuration()
{
	$("#duration").val($("#hh").val() + "" + $("#mi").val() + "" + $("#ss").val() + "" + $("#ff").val());
}

function showDuration(duration)
{
	$("#hh").val(duration.substring(0, 2));  //HH
	$("#mi").val(duration.substring(2, 4));  //MI
	$("#ss").val(duration.substring(4, 6));  //SS
	$("#ff").val(duration.substring(6, 8));  //FF
}


function doValidPage()
{
    	$("label.validFont").text("");
    	var isPass=false;
    	var namecn=document.getElementById("namecn");
    	isPass=isNull(namecn,"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}	
    	isPass=isMaxLength(namecn,"namecn_msg",namecn.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr($.trim(namecn.value),"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	//内容名称校验结束
    	
    	var movietype=$("#movietype").val();
    	if(movietype=="")
    	{
    		$("#movietype_msg").text($res_entry("msg.info.basicdata.032"));
    		return false;
    	}
    	//媒体类型校验结束
    	
    	/**
    	var bitratetype=$("#bitratetype").val();
    	if(bitratetype=="")
    	{
    		$("#bitratetype_msg").text($res_entry("msg.info.basicdata.032"));
    		return false;
    	}
    	*/
    	//码流速率校验结束
    		
    	var numberofframes=document.getElementById("numberofframes");
		isPass=isPlusInteger(numberofframes,"numberofframes_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧数结束
    	
    	var frameheight=document.getElementById("frameheight");
		isPass=isPlusInteger(frameheight,"frameheight_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧高结束
		
		var framewidth=document.getElementById("framewidth");
		isPass=isPlusInteger(framewidth,"framewidth_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证宽结束
		
		var framerate=document.getElementById("framerate");		
		isPass=isMaxLength(framerate,"framerate_msg",framerate.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(framerate.value),"framerate_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧率(fps)结束
		
		
		var validDuration =document.getElementById("duration");
    	isPass=isNull(validDuration,"duration_msg");
    	if(isPass==false)
    	{
    		return false;
    	}	
    	
    	var duration = checkDuration(validDuration, 'duration_msg');
    	if(duration == false)
    	{
    		return false;
    	}
    	//验证播放时长结束
		/*
		var filenameArr=$("#fake_httppath").val().split("\\");
		if(hasChineseChars(filenameArr[filenameArr.length-1])){
			document.getElementById("httppath_msg").innerHTML="文件名不能包含中文字符";
		    document.getElementById("fake_httppath").focus();
		    return false;
		}
		
 		if(utf8_strlen2(filenameArr[filenameArr.length-1]) > 60){
     		 document.getElementById("httppath_msg").innerText = "文件名不能超过60个字符";
     		 document.getElementById("fake_httppath").focus();
    		 return false;
        }
        
        var first =httppath.indexOf(".");
	   if(first==0||first==-1){
	   	 $("#httppath_msg").text("文件与封装格式不一致");
	   	 return false;
	   }
	   var last= httppath.lastIndexOf(".");
	   var filename=httppath.substring(last+1).toUpperCase() ;

	   var type=document.getElementById("systemlayer").value;
	   if(type==1 && filename=="TS"){
	   	return true;
	   }else if(type ==2 && filename=="3GP"){
	   	 return true;
	   }else{
	   	 $("#httppath_msg").text("文件与封装格式不一致");
	   	 return false;
	   }
    	*/

}












function strTrim(str) 
{
	str = str + " ";
    var first = 0;
    var last = str.length - 1;
    while (str.charAt(first) == " ") { first++; }
    while (str.charAt(last) == " ") { last--; }
	if (first > last) return "";
    return str.substring(first, last + 1);
}


//校验播放时长
 function checkDuration(input, input_msg)
 {    	

    	var flag = true;
    	var duration = strTrim(input.value);
    	
    	if (null != duration && "" != duration)
    	{
    		if (duration == "00000000")
    		{
    			flag = false;
    		}
    		else
    		{
    			var reg = /^(\d{2})[0-5][0-9][0-5](\d{3})$/;
    			if (reg.test(duration) == false)
    			{
    				flag = false;
    			}
    		}
    		
    		if (flag == false)
    		{
    			document.getElementById(input_msg).innerText = $res_entry("msg.info.content.191");   //"播放时长格式不正确";  
    			document.getElementById("hh").focus();
    		}
    		else
    		{
                document.getElementById(input_msg).innerText = "";
    		}
    		return flag;
    	}else
    	{
    		return false;
    	}
}

function movietypeCalculate(keyvalue)
{
	var movietype = getRiaCtrlValue("movietype");
	if(movietype==1)
	{
		return $res_entry("label.cms.srcfiletype1");
	}else if(movietype==2)
	{
		return $res_entry("label.cms.srcfiletype2");
	}else
	{
		return "";
	}
}

function systemlayerCalculate(keyvalue)
{
	var systemlayer = getRiaCtrlValue("systemlayer");
	if(systemlayer==1)
	{
		return "TS";
	}else if(systemlayer==2)
	{
		return "3GP";
	}else
	{
		return "";
	}
}


function videotypeCalculate(keyvalue)
{
	 videotypeMap=videotypeMap.rtnValue;
	 return videotypeMap[getRiaCtrlValue("videotype")];
}

function audiotypeCalculate(keyvalue)
{
	 audiotypeMap=audiotypeMap.rtnValue;
	 return audiotypeMap[getRiaCtrlValue("audiotrack")];
}

function bitratetypeCalculate(keyvalue)
{
	 bitratetypeMap=bitratetypeMap.rtnValue;
	 return bitratetypeMap[getRiaCtrlValue("bitratetype")];
}

function resolutionCalculate(keyvalue)
{
	 resolutionMap=resolutionMap.rtnValue;
	 return resolutionMap[getRiaCtrlValue("resolution")];
}   

function videoprofileCalculate(keyvalue)
{
	 videoprofileMap=videoprofileMap.rtnValue;
	 return videoprofileMap[getRiaCtrlValue("videoprofile")];
}      

function audiotrackCalculate(keyvalue)
{
	 audiotrackMap=audiotrackMap.rtnValue;
	 return audiotrackMap[getRiaCtrlValue("audiotype")];
}    

function subEncryptionstatusCalculate(keyvalue)
{
	 subEncryptionstatusMap=subEncryptionstatusMap.rtnValue;
	 return subEncryptionstatusMap[getRiaCtrlValue("encryptionstatus")];
}  

function SubencryptiontypeCalculate(keyvalue)
{
	 subEncryptiontypeMap=subEncryptiontypeMap.rtnValue;
	 return subEncryptiontypeMap[getRiaCtrlValue("encryptiontype")];
}  

function sourcedrmtypeCalculate(keyvalue)
{
	var sourcedrmtype = getRiaCtrlValue("sourcedrmtype");
	if(sourcedrmtype==0)
	{
		return $res_entry("msg.info.basicdata.025");
	}else if(sourcedrmtype==1)
	{
		return $res_entry("msg.info.basicdata.024");
	}else
	{
		return "";
	}
}

function destdrmtypeCalculate(keyvalue)
{
	var destdrmtype = getRiaCtrlValue("destdrmtype");
	if(destdrmtype==0)
	{
		return $res_entry("msg.info.basicdata.025");
	}else if(destdrmtype==1)
	{
		return $res_entry("msg.info.basicdata.024");
	}else
	{
		return "";
	}
}

function screenformatCalculate(keyvalue)
{
	var screenformat = getRiaCtrlValue("screenformat");
	if(screenformat==0)
	{
		return "4x3";
	}else if(screenformat==1)
	{
		return "16x9(Wide)";
	}else
	{
		return "";
	}
}

function closedcaptioningCalculate(keyvalue)
{
	var closedcaptioning = getRiaCtrlValue("closedcaptioning");
	if(closedcaptioning==0)
	{
		return $res_entry("msg.info.content.192");
	}else if(closedcaptioning==1)
	{
		return $res_entry("msg.info.content.193");
	}else
	{
		return "";
	}
}

function domainCalculate(keyvalue)
{
	var domain = getRiaCtrlValue("domain");
	switch(domain)
	{
	 case  257:  
		 return "IPTV网络(IPTV TS RTSP)";
	 case  258:
		 return "IPTV网络(HPD)";
	 case  260:
		 return "IPTV网络(ISMA)";
	 case  264:	 
		 return "IPTV网络(HLS)";
	 case  513:
		 return "互联网网络(IPTV TS RTSP)";
	 case  514:
		 return "互联网网络(HPD)";
	 case  516:
		 return "互联网网络(ISMA)";
	 case  520:
		 return "互联网网络(HLS)";
	 case  1025:
		 return "移动网络(IPTV TS RTSP)";
	 case  1026:
		 return "移动网络(HPD)";
	 case  1028:
		 return "移动网络(ISMA)";
	 case  1032:	
		 return "移动网络(HLS)";
	 default:
		 return "";
	}		
	
}

function hotdegreeCalculate(keyvalue)
{
	var hotdegree = getRiaCtrlValue("hotdegree");
	if(hotdegree==0)
	{
		return $res_entry("label.cms.channel.info.hotdegree0");
	}else if(hotdegree==1)
	{
		return $res_entry("label.cms.channel.info.hotdegree1");
	}else
	{
		return "";
	}
}



function durationCalculate(keyvalue)
{
	var duration = getRiaCtrlValue("duration");

	var rtnStr = "";
	if(duration!=null && duration!=""){
		rtnStr = duration.substring(0,2) + ":" + duration.substring(2,4) + ":" + duration.substring(4,6) + ":" + duration.substring(6,8);
	}
	return rtnStr;
}





//根据权限和状态显示查看按钮
function query_view(i)
{
	if(i>0)
	{
		if(viewRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}


//根据权限和状态显示修改按钮
function query_modify(i)
{
	if(i>0 && modRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case   0:	//待发布
			 case  20:	//新增同步成功
			 case  50:	//修改同步成功
			 case  60:	//修改同步失败
			 case  80:	//取消同步成功					 			 
			 case 110:  //待审核
			 case 130:	//一审失败
			 case 150:  //二审失败
			 case 180:	//三审失败	
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}




//根据权限和状态显示删除按钮
function query_del(i)
{
	if(i>0 && delRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case 110:  //待审核
			 case 130:	//一审失败
			 case 150:  //二审失败
			 case 160:  //回收站
			 case 180:	//三审失败		
				 this.style.display = "";
				 break;
			 default:
				 this.style.display = "none";
			}	
	}
}

//根据权限和状态显示预览按钮
function query_preview(i)
{
	if(i>0 && preRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  100:  //初始
				 this.style.display = "none";
				 break;
			 default:
				 this.style.display = "";
			}	
	}
}

function query_videoaudio(i)
{
	if(i>0 && videoaudioRight)
	{
		var status=data[i-1].status;
			switch(status)
			{
			 case  100:  //初始
				 this.style.display = "none";
				 break;
			 default:
				 this.style.display = "";
			}	
	}
}

function validftp()
{
		var isPass=false;
		//IP
	    var ip = document.getElementById("ip");
    	isPass=isNull(ip,"ip_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	
    	if(checkIP(ip.value) == false)
    	{
    		document.getElementById("ip_msg").innerText=$res_entry("msg.info.ipformat.incorrect");
    		return false;
    	}
	    
	    //端口号
	    var port = document.getElementById("port");
	    isPass=isNull(port,"port_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	
    	isPass=isAboveZero(port,"port_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
    	
    	if(port.value < 0 || port.value > 65535)
    	{
    		document.getElementById("port_msg").innerText=$res_entry("msg.info.basicdata.064");
    		return false;
    	}
	    
	    //用户名
	    var username = document.getElementById("username");
	    isPass=isNull(username,"username_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
	    
	    //密码
	    var password = document.getElementById("password");
	    isPass=isNull(password,"password_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	
    	
	    //文件路径
	    var path = document.getElementById("path");
	    isPass=isNull(path,"path_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
	    
	    //文件名
	    var filename = document.getElementById("filename2");
	    isPass=isNull(filename,"filename_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
}

function checkIP(sIPAddress) {

    var exp=/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
 
    var reg = sIPAddress.match(exp);

    if(reg==null){

        return false;

    }

    return true;
}

package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.service.dao.IServiceDAO;
import com.zte.cms.service.model.Service;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceContentBindDAO extends DynamicObjectBaseDao implements IServiceContentBindDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    /**
     * 查询服务和内容绑定关系的列表，根据传入的内容的类型不同，查询不同的sql语句，其中不同的sql语句对应不同的服务内容绑定表
     * 
     * @param service，
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        int cntType = service.getFeetype();
        if (cntType == 1) // 连续剧
        {
            return super.indexPageQuery("queryServiceBindSeriesListByCond", "queryServiceBindSeriesListCntByCond",
                    service, start, pageSize, puEntity);
        }
        else if (cntType == 2)// VOD
        {
            return super.indexPageQuery("queryServiceBindProgramListByCond", "queryServiceBindProgramListCntByCond",
                    service, start, pageSize, puEntity);
        }
        else if (cntType == 3) // 直播内容
        {
            return super.indexPageQuery("queryServiceBindChannelListByCond", "queryServiceBindChannelListCntByCond",
                    service, start, pageSize, puEntity);
        }
        else if (cntType == 4) // 节目内容
        {
            return super.indexPageQuery("queryServiceBindSchedualListByCond", "queryServiceBindSchedualListCntByCond",
                    service, start, pageSize, puEntity);
        }
        else
        {
            return null;
        }

    }

    /**
     * 查询可以绑定服务的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        // 根据内容的不同，查询不同的表
        int cntType = service.getFeetype();
        if (cntType == 1) // 连续剧
        {
            return super.indexPageQuery("queryServiceForBindSeriesListByCond",
                    "queryServiceForBindSeriesListCntByCond", service, start, pageSize, puEntity);
        }
        else if (cntType == 2)// VOD
        {
            return super.indexPageQuery("queryServiceForBindProgramListByCond",
                    "queryServiceForBindProgramListCntByCond", service, start, pageSize, puEntity);
        }
        else if (cntType == 3) // 直播内容
        {
            return super.indexPageQuery("queryServiceForBindChannelListByCond",
                    "queryServiceForBindChannelListCntByCond", service, start, pageSize, puEntity);
        }
        else
        {
            return null;
        }
    }
    /**
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return PageInfo
     * @throws DAOException DAOException
     */
    public PageInfo serviceCntMapQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
    throws DAOException
    {   
        //根据内容的不同，查询不同的表
        int cntType = service.getStatus();
        if(cntType ==1) //连续剧
        {
            return super.indexPageQuery("queryServiceFromSeriesInfoListByCond", "queryServiceFromSeriesInfoListCntByCond", service,
                    start, pageSize, puEntity);
        }
        else if(cntType == 2)//VOD
        {
            return super.indexPageQuery("queryServiceFromProgInfoListByCond", "queryServiceFromProgInfoListCntByCond", service,
                    start, pageSize, puEntity);
        }
        else if(cntType == 3)  //直播内容
        {
            return super.indexPageQuery("queryServiceFromChannelInfoListByCond", "queryServiceFromChannelInfoListCntByCond", service,
                    start, pageSize, puEntity);
        }
        else if(cntType == 4)  //节目内容
        {
            return super.indexPageQuery("queryServiceFromSchedInfoListByCond", "queryServiceFromSchedInfoListCntByCond", service,
                    start, pageSize, puEntity);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * 查询服务和栏目的关联关系
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DAOException DAOException
     */
    public PageInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws DAOException
    {
        return super.indexPageQuery("queryServiceCategoryMapListByCond",
                "queryServiceCategoryMapListCntByCond", service, start, pageSize, puEntity);
    }
    
    //查询服务是否绑定内容
    public boolean isBingContent(Service service) throws DAOException
    {
        int seriesCnt = ((Integer) super.queryForObject("queryServiceBingSeriesCntByCond", service)).intValue();   
        int programCnt = ((Integer) super.queryForObject("queryProgramBingSeriesCntByCond", service)).intValue();    
        int channelCnt = ((Integer) super.queryForObject("queryChannelBingSeriesCntByCond", service)).intValue();    
        int scheduleCnt = ((Integer) super.queryForObject("queryScheduleBingSeriesCntByCond", service)).intValue();    
        if( seriesCnt == 0 && programCnt == 0 && channelCnt == 0 && scheduleCnt == 0 ){
            return true;   //不存在绑定关系   
        } else{
            return false;           
        }

    }

}
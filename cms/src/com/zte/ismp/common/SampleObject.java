package com.zte.ismp.common;

public class SampleObject extends Object
{

    private String samid = null;
    private String samname = null;
    private String samdesc = null;
    private String createtime = null;
    private Integer samno = 0;

    public String getSamid()
    {
        return samid;
    }

    public void setSamid(String samid)
    {
        this.samid = samid;
    }

    public String getSamname()
    {
        return samname;
    }

    public void setSamname(String samname)
    {
        this.samname = samname;
    }

    public String getSamdesc()
    {
        return samdesc;
    }

    public void setSamdesc(String samdesc)
    {
        this.samdesc = samdesc;
    }

    public String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(String createtime)
    {
        this.createtime = createtime;
    }

    public Integer getSamno()
    {
        return samno;
    }

    public void setSamno(Integer samno)
    {
        this.samno = samno;
    }

}

package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.dao.IServiceChannelMapDAO;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceChannelMapDAO extends DynamicObjectBaseDao implements IServiceChannelMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException
    {
        log.debug("insert serviceChannelMap starting...");
        super.insert("insertServiceChannelMap", serviceChannelMap);
        log.debug("insert serviceChannelMap end");
    }

    public void updateServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException
    {
        log.debug("update serviceChannelMap by pk starting...");
        super.update("updateServiceChannelMap", serviceChannelMap);
        log.debug("update serviceChannelMap by pk end");
    }

    public void deleteServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException
    {
        log.debug("delete serviceChannelMap by pk starting...");
        super.delete("deleteServiceChannelMap", serviceChannelMap);
        log.debug("delete serviceChannelMap by pk end");
    }

    public ServiceChannelMap getServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DAOException
    {
        log.debug("query serviceChannelMap starting...");
        ServiceChannelMap resultObj = (ServiceChannelMap) super.queryForObject("getServiceChannelMap",
                serviceChannelMap);
        log.debug("query serviceChannelMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceChannelMap> getServiceChannelMapByCond(ServiceChannelMap serviceChannelMap) throws DAOException
    {
        log.debug("query serviceChannelMap by condition starting...");
        List<ServiceChannelMap> rList = (List<ServiceChannelMap>) super.queryForList(
                "queryServiceChannelMapListByCond", serviceChannelMap);
        log.debug("query serviceChannelMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceChannelMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceChannelMapListCntByCond", serviceChannelMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceChannelMap> rsList = (List<ServiceChannelMap>) super.pageQuery(
                    "queryServiceChannelMapListByCond", serviceChannelMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceChannelMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceChannelMapListByCond", "queryServiceChannelMapListCntByCond",
                serviceChannelMap, start, pageSize, puEntity);
    }

}
package com.zte.cms.channel.wkfw.ls;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IScheduleWkfwCheck
{
    /**
     * 发起工作流之前对服务进行检查
     * 
     * @param channelindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkScheduleWkfw(Long scheduleindex) throws DomainServiceException;

    /**
     * 审核操作之前对服务进行检查
     * 
     * @param channelIndex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkScheduleWkfwAudit(Long scheduleIndex) throws DomainServiceException;

}

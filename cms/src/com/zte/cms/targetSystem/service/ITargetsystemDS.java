package com.zte.cms.targetSystem.service;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ITargetsystemDS
{
    /**
     * 新增Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public void insertTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public void updateTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 批量更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public void updateTargetsystemList(List<Targetsystem> targetsystemList) throws DomainServiceException;

    /**
     * 根据条件更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateTargetsystemByCond(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 根据条件批量更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateTargetsystemListByCond(List<Targetsystem> targetsystemList) throws DomainServiceException;

    /**
     * 删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public void removeTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 批量删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public void removeTargetsystemList(List<Targetsystem> targetsystemList) throws DomainServiceException;

    /**
     * 根据条件删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeTargetsystemByCond(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 根据条件批量删除Targetsystem对象
     * 
     * @param targetsystem Targetsystem删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeTargetsystemListByCond(List<Targetsystem> targetsystemList) throws DomainServiceException;

    /**
     * 查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public Targetsystem getTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 根据条件查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return 满足条件的Targetsystem对象集
     * @throws DomainServiceException ds异常
     */
    public List<Targetsystem> getTargetsystemByCond(Targetsystem targetsystem) throws DomainServiceException;
    
    
    /**
     * 检查目标系统的id是否重复
     * @param targetsystemID
     * @return boolean
     * @throws DomainServiceException
     */
    public boolean checkTargetsystemID(String targetsystemID)throws DomainServiceException;
    

    /**
     * 根据条件分页查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    
    public TableDataInfo pageInfoQueryPop(Targetsystem targetsystem, int start,
			int pageSize) throws DomainServiceException;
    
    /**
     * 查询服务关联目标系统
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws DAOException
     */
    public TableDataInfo queryServiceBindTargetListByCond(Service service, int start, int pageSize)throws DomainServiceException;
}
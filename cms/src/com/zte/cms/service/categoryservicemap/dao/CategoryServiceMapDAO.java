
  package com.zte.cms.service.categoryservicemap.dao;

import java.util.List;

import com.zte.cms.service.categoryservicemap.dao.ICategoryServiceMapDAO;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CategoryServiceMapDAO extends DynamicObjectBaseDao implements ICategoryServiceMapDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException
    {
    	log.debug("insert categoryServiceMap starting...");
    	super.insert( "insertCategoryServiceMap", categoryServiceMap );
    	log.debug("insert categoryServiceMap end");
    }
     
    public void updateCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException
    {
    	log.debug("update categoryServiceMap by pk starting...");
       	super.update( "updateCategoryServiceMap", categoryServiceMap );
       	log.debug("update categoryServiceMap by pk end");
    }


    public void deleteCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException
    {
    	log.debug("delete categoryServiceMap by pk starting...");
       	super.delete( "deleteCategoryServiceMap", categoryServiceMap );
       	log.debug("delete categoryServiceMap by pk end");
    }


    public CategoryServiceMap getCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DAOException
    {
    	log.debug("query categoryServiceMap starting...");
       	CategoryServiceMap resultObj = (CategoryServiceMap)super.queryForObject( "getCategoryServiceMap",categoryServiceMap);
       	log.debug("query categoryServiceMap end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<CategoryServiceMap> getCategoryServiceMapByCond( CategoryServiceMap categoryServiceMap )throws DAOException
    {
    	log.debug("query categoryServiceMap by condition starting...");
    	List<CategoryServiceMap> rList = (List<CategoryServiceMap>)super.queryForList( "queryCategoryServiceMapListByCond",categoryServiceMap);
    	log.debug("query categoryServiceMap by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize)throws DAOException
    {
    	log.debug("page query categoryServiceMap by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryCategoryServiceMapListCntByCond",  categoryServiceMap)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CategoryServiceMap> rsList = (List<CategoryServiceMap>)super.pageQuery( "queryCategoryServiceMapListByCond" ,  categoryServiceMap , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query categoryServiceMap by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryCategoryServiceMapListByCond", "queryCategoryServiceMapListCntByCond", categoryServiceMap, start, pageSize, puEntity);
    }
    
} 
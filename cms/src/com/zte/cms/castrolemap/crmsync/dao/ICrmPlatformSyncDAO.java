package com.zte.cms.castrolemap.crmsync.dao;

import com.zte.cms.castrolemap.crmsync.model.CrmPlatformSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICrmPlatformSyncDAO
{

    /**
     * 根据条件分页查询CrmPlatformSync对象，作为查询条件的参数
     *
     * @param crmPlatformSync CrmPlatformSync对象 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CrmPlatformSync crmPlatformSync, int start, int pageSize)throws DAOException;
    
    /**
     * 按条件查询
     * @param crmPlatformSync
     * @return
     * @throws DAOException
     */
    public CrmPlatformSync getCrmPlatformSync( CrmPlatformSync crmPlatformSync )throws DAOException;

}

package com.zte.cms.batchobjectrecord.dao;

import java.util.List;

import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IBatchObjectRecordDAO
{
    /**
     * 新增BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DAOException dao异常
     */
    public void insertBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据主键更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DAOException dao异常
     */
    public void updateBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据条件更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord更新条件
     * @throws DAOException dao异常
     */
    public void updateBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据主键删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DAOException dao异常
     */
    public void deleteBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据条件删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord删除条件
     * @throws DAOException dao异常
     */
    public void deleteBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据主键查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @return 满足条件的BatchObjectRecord对象
     * @throws DAOException dao异常
     */
    public BatchObjectRecord getBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据条件查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @return 满足条件的BatchObjectRecord对象集
     * @throws DAOException dao异常
     */
    public List<BatchObjectRecord> getBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DAOException;

    /**
     * 根据条件分页查询BatchObjectRecord对象，作为查询条件的参数
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询BatchObjectRecord对象，作为查询条件的参数
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
function valid(type){
		var error=true;
		clearAllMsgLabel();
		var cpid=getHtmlCtrlTrimValue("cpid");
		if(htmlElemValueIsEmpty(cpid))
		{
			setMsgCtrlTextAndFocus('cpid','此项不能为空')	;
			return false;
		}
		
		var namecn=getHtmlCtrlTrimValue("namecn");
		document.getElementById("namecn").value=namecn;
		if(htmlElemValueIsEmpty(namecn))
		{
			setMsgCtrlTextAndFocus('namecn','此项不能为空')	;
			return false;
		}
		
	    if (!isSafe(namecn,new RegExp('[\'\"!@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]','g')))
	    {
	        setMsgCtrlText("namecn",$res_entry("msg.cms.content.42053","")+'!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
	        return false;
	    }
	    
	     error =  IsMaxLength(document.getElementById('namecn'),'namecn_msg',60);
	    if(error == false){
			        return false;
		 }
	    
	    if(document.getElementById("filetype").selectedIndex==0){
	    	setMsgCtrlText("filetype","请选择");
	        return false;
	    }
	    
	    
	    if(document.getElementById("formatindex").selectedIndex==0){
	    	setMsgCtrlText("formatindex","请选择");
	        return false;
	    }
	    
	    if(document.getElementById("definition").selectedIndex==0){
	    	setMsgCtrlText("definition","请选择");
	        return false;
	    }
	    if(document.getElementById("encodingformat").selectedIndex==0){
	    	setMsgCtrlText("encodingformat","请选择");
	        return false;
	    }
	    
	    var bitrate=getHtmlCtrlTrimValue("bitrate");
		document.getElementById("bitrate").value=bitrate;
		if(htmlElemValueIsEmpty(bitrate))
		{
			setMsgCtrlTextAndFocus('bitrate','此项不能为空')	;
			return false;
		}
		
		if(isNaN(bitrate)||bitrate.indexOf(".")!=-1){
			setMsgCtrlTextAndFocus('bitrate','请输入整数')	;
			return false;
		}
		
		if(bitrate.indexOf("+")!=-1)
		{
			setMsgCtrlTextAndFocus('bitrate','此项不能输入"+"符号')	;
			return false;
		}
		
		if(bitrate<50000){
			setMsgCtrlTextAndFocus('bitrate','码流不能低于50000')	;
			return false;
		}
		 error =  IsMaxLength(document.getElementById('bitrate'),'bitrate_msg',10);
	     if(error == false){
			        return false;
		 }
		 
		var duration=getHtmlCtrlTrimValue("duration");
		document.getElementById("duration").value=duration;
		if(htmlElemValueIsEmpty(duration))
		{
			setMsgCtrlTextAndFocus('duration','此项不能为空')	;
			return false;
		}
		 
		if(isNaN(duration)||duration.indexOf(".")!=-1){
			setMsgCtrlTextAndFocus('duration','请输入大于0的整数')	;
			return false;
		}
		if(duration.substr(0,1)==0){
			setMsgCtrlTextAndFocus('duration','请输入大于0的整数')	;
			return false;
		}
		
		if(duration.indexOf("+")!=-1)
		{
			setMsgCtrlTextAndFocus('duration','此项不能输入"+"符号')	;
			return false;
		}
		
		if(duration<=0){
			setMsgCtrlTextAndFocus('duration','播放时长不能小于0')	;
			return false;
		} 
		 

//            error = checkDuration(document.getElementById("duration"), "duration_msg");        
//            if (error == false)
//            {
//                  return false;
//            }          

	if(type==0){
		var httppath=$("#fake_httppath").val();
		if( httppath==""){
			setMsgCtrlTextAndFocus('httppath','此项不能为空')	;
			return false;
		}
		var filenameArr=$("#fake_httppath").val().split("\\");
		if(hasChineseChars(filenameArr[filenameArr.length-1])){
			document.getElementById("httppath_msg").innerHTML="文件名不能包含中文字符";
		    document.getElementById("fake_httppath").focus();
		    return false;
		}
		
 		if(utf8_strlen2(filenameArr[filenameArr.length-1]) > 60){
     		 document.getElementById("httppath_msg").innerText = "文件名不能超过60个字符";
     		 document.getElementById("fake_httppath").focus();
    		 return false;
        }


		
	   var first =httppath.indexOf(".");
	   if(first==0||first==-1){
	   	 setMsgCtrlText("httppath","媒体类型不一致");
	   	 return false;
	   }
	   var last= httppath.lastIndexOf(".");
	   var filename=httppath.substring(last+1).toUpperCase() ;

	   var type=document.getElementById("formatindex").value;
	   if(type==1 && filename=="TS"){
	   	return true;
	   }else if(type ==2 && filename=="3GP"){
	   	 return true;
	   }else{
	   
	     setMsgCtrlText("httppath","媒体类型不一致");
	   	 return false;
	   }
	   	
	}else{
		

		var contentDir=getHtmlCtrlTrimValue("contentDir");
		document.getElementById("contentDir").value=contentDir;
		if(htmlElemValueIsEmpty(contentDir))
		{
			setMsgCtrlTextAndFocus('contentDir','此项不能为空')	;
			return false;
		}
		
		error =  IsMaxLength(document.getElementById('contentDir'),'contentDir_msg',128);
	    if(error == false){
			        return false;
		 }
		
		var content=document.getElementById("contentDir");
		if(!CheckSpecial(content,'contentDir_msg')){
			return false;
		}
		if(content.value.substr(content.value.length-1,content.value.length)!="/"){
			setMsgCtrlTextAndFocus('contentDir','此项不能为空必须以 / 结尾')	;
			return false;
		}
		  
	    var uploadedfilename=getHtmlCtrlTrimValue("uploadedfilename");
		document.getElementById("uploadedfilename").value=uploadedfilename;
		if(htmlElemValueIsEmpty(uploadedfilename))
		{
			setMsgCtrlTextAndFocus('uploadedfilename','此项不能为空')	;
			return false;
		}
		
		if(hasChineseChars(uploadedfilename)){
			document.getElementById("uploadedfilename_msg").innerHTML="文件名不能包含中文字符";
		    document.getElementById("uploadedfilename").focus();
		    return false;
		}
		
 		if(utf8_strlen2(uploadedfilename) > 60){
     		 document.getElementById("uploadedfilename_msg").innerText = "文件名不能超过60个字符";
     		 document.getElementById("uploadedfilename").focus();
    		 return false;
        }
		
		
	   var uploadfilename=document.getElementById("uploadedfilename");
		if(!CheckSpecial(uploadfilename,'uploadedfilename_msg')){
			return false;
		}	
		
	   var first =uploadedfilename.indexOf(".");
	   if(first==0||first==-1){
	   	 setMsgCtrlText("uploadedfilename","媒体类型不一致");
	   	 return false;
	   }
	
	   var last= uploadedfilename.lastIndexOf(".");
	   var filename=uploadedfilename.substring(last+1).toUpperCase() ;
	   var type=document.getElementById("formatindex").value;
	   if(type==1 && filename=="TS"){
	     return true;
	   }else if(type ==2 && filename=="3GP"){
	   	 return true;
	   }else{
	     setMsgCtrlText("uploadedfilename","媒体类型不一致");
	   	 return false;
	   }
	  
	
	    

	   	
	}
}

function clearAllMsgLabel()
{
    $("label[@id$='_msg']").html("");
}



function CheckSpecial(input,input_msg)
{
   var str="!@#$^%'\\[]{}:*&?\"<>|,;";
   if(input.id=="uploadedfilename"){
   	   str="!@#$^%'/\\[]{}:*&?\"<>|,;";
   }
   if(checkstring(str,input.value))
   {
      document.getElementById(input_msg).innerText =$res_entry("msg.cms.content.42053","")+str;
      input.focus();
      return false;

   }
   else
   {
     return true;
   }
}


function checkstring(checkstr,userinput)
{
	var allValid = false;
	for (i = 0;i<userinput.length;i++)
	{
		ch = userinput.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = true;
			break;
	 	}
  	}

	return allValid;
}

function getTime(str)
{
	if (str.length > 8)
	{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	}else{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	}
}



function showSourceType(type){
	if(type==1){
		document.getElementById("sourcetype").innerText="母片";
	}else if(type==2){
		document.getElementById("sourcetype").innerText="子文件";
	}else{
		document.getElementById("sourcetype").innerText="";
	}
}

function showUploadtype(uploadtype)
{
	if(uploadtype==0){
		document.getElementById("uploadtype").innerText="HTTP";
	}else if(uploadtype==1){
		document.getElementById("uploadtype").innerText="已上载";
	}else{
		document.getElementById("uploadtype").innerText="";
	}
}

function showFormatindex(formatindex)
{
	if(formatindex!=null && formatindex!=""){
		document.getElementById("formatindex").innerText=videoformatMap[formatindex]; //显示媒体类型
	}
}

function showFiletype(filetype)
{	
	if(filetype!=null && filetype!=""){
		document.getElementById("filetype").innerText=filetypeMap[filetype]; 
	}
}


function showDefinition(definition)
{
	if(definition!=null && definition!=""){
		document.getElementById("definition").innerText=vediogroupMap[definition]; //显示视频规格
	}
}

function showEncodingformat(encodingformat)
{
	if(encodingformat!=null && encodingformat!=""){
		document.getElementById("encodingformat").innerText=encodingformatMap[encodingformat]; //显示编码格式 
	}
}

function showResolution(resolution)
{
	if(resolution!=null && resolution!=""){
		document.getElementById("resolution").innerText=resolutionMap[resolution]; //显示视频分辨率
	}
}

function showCountry(country)
{
	if(country!=null && country!=""){
		document.getElementById("country").innerText=countryMap[country]; //显示视频分辨率
	}
}

function showSubtitlelanguage(subtitlelanguage)
{
	if(subtitlelanguage!=null && subtitlelanguage!=""){
		document.getElementById("subtitlelanguage").innerText=subtitlelanguageMap[subtitlelanguage]; //显示视频分辨率
	}
}

function showDubbinglanguage(dubbinglanguage)
{
	if(dubbinglanguage!=null && dubbinglanguage!=""){
		document.getElementById("dubbinglanguage").innerText=subtitlelanguageMap[dubbinglanguage]; //显示视频分辨率
	}
}

function showCmsid(cmsid)
{
	if(cmsid!=null && cmsid!=""){
		document.getElementById("cmsid").innerText=cmsidMap[cmsid]; //显示视频分辨率
	}
}

    //function getViewDuration()
  //  {
    	
 //  	var duration = getRiaCtrlValue('duration_v');
 //   	rtnStr = duration.substring(0,2) + ":" + duration.substring(2,4) + ":" + duration.substring(4,6) + ":" + duration.substring(6,8);
 //   	return rtnStr;
 //   }


//function showDuration(duration)
//{
//	var rtnStr = "";
//	if(duration!=null && duration!=""){
//		rtnStr = duration.substring(0,2) + ":" + duration.substring(2,4) + ":" + duration.substring(4,6) + ":" + duration.substring(6,8);
//		document.getElementById("duration").innerText=rtnStr;
//	}else
//	{
//		document.getElementById("duration").innerText="";
//	}
//}


function dynChgWidth(id)
{
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width = getCharWidth(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}	

function IsMaxLength(input,input_msg,len){

  if(utf8_strlen2(input.value) > len){
      document.getElementById(input_msg).innerText = "此项长度不能超过"+len+"个字符(一个汉字占3个字符)";//"不能超过"+len+ "个字符";
      document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}  


 function checkDuration(input, input_msg)
    {    	
    	var flag = true;
    	var duration = trim(input.value);
    	
    	if (null != duration && "" != duration)
    	{
    		if (duration == "00000000")
    		{
    			flag = false;
    		}
    		else
    		{
    			var reg = /^(\d{2})[0-5][0-9][0-5](\d{3})$/;
    			if (reg.test(duration) == false)
    			{
    				flag = false;
    			}
    		}
    		
    		if (flag == false)
    		{
    			document.getElementById(input_msg).innerText = $res_entry("msg.info.duration.incorrect");   //"播放时长格式不正确";  
    			document.getElementById("hh").focus();
    		}
    		else
    		{
                document.getElementById(input_msg).innerText = "";
    		}
    		return flag;
    	}
    }
    
    
function hasChineseChars(sCheck){
  for (nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charCodeAt(nIndex);
        if (cCheck > 255)
           return true;
  }
  return false;
}


function isPlusInteger(input,input_msg)
  {
  	var inputValue=$.trim(input.value);
  	if(inputValue=="")
  	{
  		$("#"+input_msg).text("");
  		return true;
  	}
  	else
  	{
	var integerRex=/^((0)|([1-9]+[0-9]*))$/;
	var rtnValue=integerRex.test(inputValue);
	if(rtnValue==false)
	{
			$("#"+input_msg).text("请输入大于等于0的数字");
			return false;
	}
	else
	{
		$("#"+input_msg).text("");
		return true;
	}
  	}
  }
  
  function utf8_strlen2(str) 
{ 
	var cnt = 0; 
	for( i=0; i<str.length; i++) 
	{ 
	    var value = str.charCodeAt(i); 
	    if( value < 0x080) 
	    { 
	        cnt += 1; 
	    } 
	    else if( value < 0x0800) 
	    { 
	        cnt += 2; 
	    } 
	    else  if(value < 0x010000)
	    { 
	        cnt += 3; 
	    }
		else
		{
			cnt += 4;
		}
	} 
	return cnt; 
}
     //验证输入框中字数是否超过最大长度
function isMaxLength(input,input_msg,len)
{
 		if(utf8_strlen2(trim(input.value)) > len){
     		document.getElementById(input_msg).innerText = "此项长度不能超过"+len+"个字符（一个汉字占3个字符）";//"超过最大长度限制:";
    		// document.getElementById(input.id).focus();
     		return false;
 		}else{
      		document.getElementById(input_msg).innerText = "";   
      		return true;   
		}
}
var specialStr="!@#$%^&*;|?()[]{}<>\'\"";
function isSpecialStr(checkstr,msgBox_id,specialStrParam)
{
var valSpecialStr=specialStr;
if(specialStrParam!="undefined"&&specialStrParam!= null)
{
	valSpecialStr=specialStrParam;
}
var allValid = true;
for (i = 0;i<valSpecialStr.length;i++)
{
	ch = valSpecialStr.charAt(i);
 	if(checkstr.indexOf(ch) > -1)
 	{
		allValid = false;
		break;
 	}
}
if(!allValid)
{
	document.getElementById(msgBox_id).innerText="此项不能包含以下特殊符号 "+valSpecialStr;
}
return allValid;
}
function editMediainfo(){
    var isPass=false;
    if(document.getElementById("edit").value=="确认修改"){
        var bitratetype=$("#bitratetype").val();
    	if(bitratetype=="")
    	{
    		$("#bitratetype_msg").text("请选择");
    		return false;
    	}
    	//码流速率校验结束 		
    	
    	var frameheight=document.getElementById("frameheight");
		isPass=isPlusInteger(frameheight,"frameheight_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧高结束
		
		var framewidth=document.getElementById("framewidth");
		isPass=isPlusInteger(framewidth,"framewidth_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证宽结束
		
		var framerate=document.getElementById("framerate");		
		isPass=isMaxLength(framerate,"framerate_msg",framerate.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(framerate.value),"framerate_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧率(fps)结束
		
		var numberofframes=document.getElementById("numberofframes");
		isPass=isPlusInteger(numberofframes,"numberofframes_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证帧数结束
    	
       callSid('updateCmsMovie','[$M.obj1]');
    }
    document.getElementById("edit").value="确认修改";
    for(var i=1;i<9;i++){
      document.getElementById("div"+i).style.display="block";
    }   
    callSid('getCmsMovie'); 
}

function showEditMediainfoResult(rtn)
{
    document.getElementById("edit").value="修改内容元数据";
    for(var i=1;i<9;i++){
      document.getElementById("div"+i).style.display="none";
    } 
	if(rtn.substring(0,1) == 0)
	{
    	openMsg(rtn);    
    }else
	{
		openMsg(rtn);    
	}
	
}

function formatData(vainfo){
      //视音频文件格式类型和视音频文件格式子类型
        if(vainfo.streammediatype==0){
           document.getElementById('streammediatype').innerText = "无效格式";
           document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==1){
            document.getElementById('streammediatype').innerText = "OPENDML Video 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==2){
            document.getElementById('streammediatype').innerText = "MSVFW Video 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==4){
            document.getElementById('streammediatype').innerText = "MSVFW Audio 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==16){
            document.getElementById('streammediatype').innerText = " Element Stream file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==32){
            document.getElementById('streammediatype').innerText = "Program Stream file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==64){
            document.getElementById('streammediatype').innerText = "Transport Stream file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==128){
            document.getElementById('streammediatype').innerText = "ISO MP4 Stream file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==256){
            document.getElementById('streammediatype').innerText = "LEITCH Video Server File，LXF";
             if(vainfo.streammediasubtype==0){
                document.getElementById('streammediasubtype').innerText = " LXF格式的Version0文件";
            }else if(vainfo.streammediasubtype==1){
                 document.getElementById('streammediasubtype').innerText = "LXF格式的Version1文件";
            }else{
                 document.getElementById('streammediasubtype').innerText = 0;
            }
        }else if(vainfo.streammediatype==512){
            document.getElementById('streammediatype').innerText = "HDV file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==1024){
            document.getElementById('streammediatype').innerText = "3GPP file";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==4096){
            document.getElementById('streammediatype').innerText = "SONY MAV70 Video Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==8192){
            document.getElementById('streammediatype').innerText = "SONY MAV70 Video Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==16384){
            document.getElementById('streammediatype').innerText = "PINNACLE MediaStream Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==32768){
            document.getElementById('streammediatype').innerText = "RPOFILE Video Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==65536){
            document.getElementById('streammediatype').innerText = "MXF File";
            if(vainfo.streammediasubtype==0){
                document.getElementById('streammediasubtype').innerText = "默认的MXF文件STREAMTYPE_MXF_STD";
            }else if(vainfo.streammediasubtype==1){
                 document.getElementById('streammediasubtype').innerText = "XDCAM的MXF文件STREAMTYPE_MXF_XDCAM";
            }else if(vainfo.streammediasubtype==2){
                 document.getElementById('streammediasubtype').innerText = "K2的MXF文件STREAMTYPE_MXF_K2";
            }else{
                 document.getElementById('streammediasubtype').innerText = 0;
            }
        }else if(vainfo.streammediatype==131072){
            document.getElementById('streammediatype').innerText = "GXF File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==262144){
            document.getElementById('streammediatype').innerText = "SEACHANGE Video Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==524288){
            document.getElementById('streammediatype').innerText = "OMNEON Video Server File";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==1048576){
            document.getElementById('streammediatype').innerText = "VCD 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==2097152){
            document.getElementById('streammediatype').innerText = "SVCD 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==4194304){
            document.getElementById('streammediatype').innerText = "DVD 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==8388608){
            document.getElementById('streammediatype').innerText = "TS SubTitle文件";
            if(vainfo.streammediasubtype==0){
                document.getElementById('streammediasubtype').innerText = "默认标准TS流文件STREAMTYPE_TS_STD";
            }else if(vainfo.streammediasubtype==1){
                 document.getElementById('streammediasubtype').innerText = "符合Seachange要求的TS流文件STREAMTYPE_TS_SEACHANGE";
            }else{
                 document.getElementById('streammediasubtype').innerText = 0;
            }
        }else if(vainfo.streammediatype==16777216){
            document.getElementById('streammediatype').innerText = "REAL MEDIA 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==33554432){
            document.getElementById('streammediatype').innerText = "WINDOWS MEDIA PLAY 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==67108864){
            document.getElementById('streammediatype').innerText = "QUICKTIME 文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==134217728){
            document.getElementById('streammediatype').innerText = " Zaxel文件";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==268435456){
            document.getElementById('streammediatype').innerText = "扩展定义1";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==536870912){
            document.getElementById('streammediatype').innerText = "扩展定义2";
            document.getElementById('streammediasubtype').innerText = 0;
        }else if(vainfo.streammediatype==1073741824){
            document.getElementById('streammediatype').innerText = "扩展定义3";  
            document.getElementById('streammediasubtype').innerText = 0;
        }else
        {
            document.getElementById('streammediatype').innerText = "扩展定义4";
            document.getElementById('streammediasubtype').innerText = 0;
        }
        
        if(vainfo.isincludevideo==1){
           document.getElementById('isincludevideo').innerText = "是";
        }else{
          document.getElementById('isincludevideo').innerText = "否";
        }
        if(vainfo.isincludeaudio==1){
          document.getElementById('isincludeaudio').innerText = "是";
        }else{
          document.getElementById('isincludeaudio').innerText = "否";
        }

        //视频编码类型和视频编解码的子类型
        if(vainfo.videocodingformat==0){
            document.getElementById('videocodingformat').innerText = "无效类型";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==1){
            document.getElementById('videocodingformat').innerText = "MJPEG 场压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==2){
            document.getElementById('videocodingformat').innerText = "MJPEG 帧压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==4){
            document.getElementById('videocodingformat').innerText = "MPEG2I 帧压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==8){
            document.getElementById('videocodingformat').innerText = "MPEG2IBP帧压缩方式";
            if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "高品质";
            }else if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "标准";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==16){
            document.getElementById('videocodingformat').innerText = "Mpeg2D10 编码格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_D10_50M";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_D10_40M";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_D10_30M";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==32){
            document.getElementById('videocodingformat').innerText = "DV25编码格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==64){
            document.getElementById('videocodingformat').innerText = "DV50编码格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==128){
            document.getElementById('videocodingformat').innerText = "DVSD编码格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==256){
            document.getElementById('videocodingformat').innerText = "MPEG1 编码";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==512){
            document.getElementById('videocodingformat').innerText = "MPEG4 编码";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_MPEG4_ISOMPEG4";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_MPEG4_ISOMPEG4_ASP";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_MPEG4_MSV3";
            }else if(vainfo.videosubtype==3){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_MPEG4_MSV2";
            }else if(vainfo.videosubtype==4){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_MPEG4_MSV1";
            }else{
                document.getElementById('videosubtype').innerText = 0;
            }
        }else if(vainfo.videocodingformat==1024){
            document.getElementById('videocodingformat').innerText = "MJPEG 无损场压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==2048){
            document.getElementById('videocodingformat').innerText = "MJPEG 无损帧压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==4096){
            document.getElementById('videocodingformat').innerText = "YUV非压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==8192){
            document.getElementById('videocodingformat').innerText = "RGB非压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==16384){
            document.getElementById('videocodingformat').innerText = "real media(rm)格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_RM_V8";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = " VIDEOTYPE_RM_V9";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==32768){
            document.getElementById('streammediatype').innerText = "window media(wm)格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_WM_V8";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_WM_V9";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_WM_V7";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==65536){
            document.getElementById('videocodingformat').innerText = "quicktime 格式";
            if(vainfo.videosubtype==1685480224){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCNTSC";
            }else if(vainfo.videosubtype==1685480304){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCPAL";
            }else if(vainfo.videosubtype==1685483632){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCProPAL";
            }else if(vainfo.videosubtype==1685468528){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCPro50NTSC";
            }else if(vainfo.videosubtype==1685480302){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCPro50NTSC";
            }else if(vainfo.videosubtype==1685481526){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCProHD1080i60";
            }else if(vainfo.videosubtype==1685481525){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_QT_DVCProHD1080i50";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==131072){
            document.getElementById('videocodingformat').innerText = "video compression manager (VCM)压缩格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==262144){
            document.getElementById('videocodingformat').innerText = "DVHD 压缩方式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DVHD_1080_50I";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DVHD_1080_60I";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==1048576){
            document.getElementById('videocodingformat').innerText = "Zaxtar 无损编码压缩方式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==2097152){
            document.getElementById('videocodingformat').innerText = "AVS压缩格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==4194304){
            document.getElementById('videocodingformat').innerText = "H263压缩格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H263_SUBQCIF";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H263_QCIF";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H263_CIF";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==8388608){
            document.getElementById('videocodingformat').innerText = "H264压缩格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H264_BASELINE";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H264_MAIN";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_H264_HIGH";
            }else if(vainfo.videosubtype==3){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_X264_BASELINE";
            }else if(vainfo.videosubtype==4){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_X264_MAIN";
            }else if(vainfo.videosubtype==5){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_X264_HIGH";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }            
        }else if(vainfo.videocodingformat==16777216){
            document.getElementById('videocodingformat').innerText = "MJPEG2000压缩格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==33554432){
            document.getElementById('videocodingformat').innerText = "HDV机的压缩格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_HDV_HD2_1080i50";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            } 
        }else if(vainfo.videocodingformat==67108864){
            document.getElementById('videocodingformat').innerText = " P2AVCIntra的压缩格式";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==134217728){
            document.getElementById('videocodingformat').innerText = "AVIDDNXHD的压缩格式";
            if(vainfo.videosubtype==0){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_50I_185_8bit";
            }else if(vainfo.videosubtype==1){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_50I_120_8bit";
            }else if(vainfo.videosubtype==2){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_50I_185_10bit";
            }else if(vainfo.videosubtype==3){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_TR_1080_50I_120_8bit";
            }else if(vainfo.videosubtype==4){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_5994I_220_8bit";
            }else if(vainfo.videosubtype==5){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_5994I_145_8bit";
            }else if(vainfo.videosubtype==6){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_1080_5994I_220_10bit";
            }else if(vainfo.videosubtype==7){
                document.getElementById('videosubtype').innerText = "VIDEOTYPE_DNxHD_TR_1080_5994I_145_8bit";
            }else{
                document.getElementById('videosubtype').innerText = "0";
            }
        }else if(vainfo.videocodingformat==268435456){
            document.getElementById('videocodingformat').innerText = "自定义1";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==536870912){
            document.getElementById('videocodingformat').innerText = "自定义2";
            document.getElementById('videosubtype').innerText =0;
        }else if(vainfo.videocodingformat==1073741824){
            document.getElementById('videocodingformat').innerText = "自定义3";  
            document.getElementById('videosubtype').innerText =0;
        }else{
            document.getElementById('videocodingformat').innerText = "自定义4";
            document.getElementById('videosubtype').innerText =0;
        }
      
        
        document.getElementById('cxsize').innerText = vainfo.cxsize;
        document.getElementById('cysize').innerText = vainfo.cysize;
        document.getElementById('standardrate').innerText = vainfo.standardrate;
        document.getElementById('standardscale').innerText = vainfo.standardscale;
        //扫描模式
        if(vainfo.scanmode==-1){
           document.getElementById('scanmode').innerText = "Invalid";
        }else if(vainfo.scanmode==0){
           document.getElementById('scanmode').innerText = "FirstFieldTop";
        }else if(vainfo.scanmode==1){
           document.getElementById('scanmode').innerText = "SecondFieldTop";
        }else if(vainfo.scanmode==2){
           document.getElementById('scanmode').innerText = "Progressive";
        }else if(vainfo.scanmode==3){
           document.getElementById('scanmode').innerText = "ProgressiveSegmented";
        }else if(vainfo.scanmode==4){
           document.getElementById('scanmode').innerText ="FirstFieldTopBackToBack";
        }else if(vainfo.scanmode==5){
          document.getElementById('scanmode').innerText = "SecondFieldTopBackToBack";
        }
        //视频颜色格式
        if(vainfo.colorformat==0){
           document.getElementById('colorformat').innerText = "invalid color format";
        }else if(vainfo.colorformat==1){
            document.getElementById('colorformat').innerText = "DWORD,0xAARRGGBB";
        }else if(vainfo.colorformat==2){
            document.getElementById('colorformat').innerText = "BYTE,RRGGBBRRGGBBRRGGBB";
        }else if(vainfo.colorformat==4){
            document.getElementById('colorformat').innerText = "BYTE,BBGGRRBBGGRRBBGGRR";
        }else if(vainfo.colorformat==16){
            document.getElementById('colorformat').innerText = "DWORD,0xTTYYUUVV";
        }else if(vainfo.colorformat==32){
            document.getElementById('colorformat').innerText = "DWORD,0xTTVVYYUU";
        }else if(vainfo.colorformat==64){
            document.getElementById('colorformat').innerText = "DWORD,0xYUYVYUYV";
        }else if(vainfo.colorformat==128){
            document.getElementById('colorformat').innerText = "DWORD,0xUYVYUYVY";
        }else if(vainfo.colorformat==256){
            document.getElementById('colorformat').innerText = "BYTE,YYYYYYYYYY...UVUVUVUVUV";
        }else if(vainfo.colorformat==512){
            document.getElementById('colorformat').innerText = "BYTE,YYYYYYYY...UUUU...VVVV,422 buffer style";
        }else if(vainfo.colorformat==1024){
            document.getElementById('colorformat').innerText = "BYTE,YYYYYYYY...UU...VV, 420 buffer style";
        }else if(vainfo.colorformat==2048){
            document.getElementById('colorformat').innerText = "BYTE,YYYYYYYY...UU...VV, 411 buffer style";
        }else if(vainfo.colorformat==65536){
            document.getElementById('colorformat').innerText = "BYTE,0xYYUUYYVV, 10bit...BlueFish define";
        }else if(vainfo.colorformat==131072){
            document.getElementById('colorformat').innerText = "BYTE,0xYYUUYYVV, 10bit...Apple define";
        }else if(vainfo.colorformat==262144){
            document.getElementById('colorformat').innerText = "BYTE,0xYYUUYYVV, 10bit...Apple define";
        }else if(vainfo.colorformat==524288){
            document.getElementById('colorformat').innerText = "BYTE,0xYYUUYYVV, 8bit...X1000 define";
        }else if(vainfo.colorformat==1048576){
            document.getElementById('colorformat').innerText = "BYTE,0xYYUUYYVV, 10bit...X1000 define";
        }else if(vainfo.colorformat==2097152){
            document.getElementById('colorformat').innerText = "BYTE,0xYYYYUUVV, 8bit...DPS define";
        }
        //视频码率
        document.getElementById('videobitrate').innerText = vainfo.videobitrate;
        if(vainfo.isconstantrate==1){
          document.getElementById('isconstantrate').innerText = "是";
        }else{
          document.getElementById('isconstantrate').innerText = "否";
        }
        
        document.getElementById('gopsize').innerText = vainfo.gopsize;
        document.getElementById('referenceperiod').innerText = vainfo.referenceperiod;
        document.getElementById('isy16235').innerText = vainfo.isy16235;
        //音频编码类型和音频编码子类型
        if(vainfo.audiocodingformat==0){
           document.getElementById('audiocodingformat').innerText = "unknow type";
           document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==1){
            document.getElementById('audiocodingformat').innerText = "WAVE PCM 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==2){
            document.getElementById('audiocodingformat').innerText = "audio compression MPEG4 (ACM) 音频压缩WAVE";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==4){
            document.getElementById('audiocodingformat').innerText = "MPEG1 音频 (Layer 1) 压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==8){
            document.getElementById('audiocodingformat').innerText = "MPEG1 音频 (Layer 2) 压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==16){
            document.getElementById('audiocodingformat').innerText = "MPEG1 音频 (Layer 3) 压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==32){
            document.getElementById('audiocodingformat').innerText = " AAC 压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==64){
            document.getElementById('audiocodingformat').innerText = "AC3 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==128){
            document.getElementById('audiocodingformat').innerText = " DT3 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==256){
            document.getElementById('audiocodingformat').innerText = "real media type";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.colorformat==512){
            document.getElementById('audiocodingformat').innerText = "windows media play 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==1024){
            document.getElementById('audiocodingformat').innerText = "quicktime 音频压缩方式";
            if(vainfo.audiosubtype==1936684916){
               document.getElementById('audiosubtype').innerText ="AUDIOTYPE_QT_16BitLittleEndian";
            }else{
               document.getElementById('audiosubtype').innerText =0;
            }
        }else if(vainfo.audiocodingformat==4096){
            document.getElementById('audiocodingformat').innerText = "AVS音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==8192){
            document.getElementById('audiocodingformat').innerText = "Dolly-E 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==16384){
            document.getElementById('audiocodingformat').innerText = " Dolly-S 音频压缩方式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==65536){
            document.getElementById('audiocodingformat').innerText = "ITUT的语音压缩标准,分ALaw和ULaw两种模式";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==131072){
            document.getElementById('audiocodingformat').innerText = "AMR-NB for 3GPP or GSM";
            if(vainfo.audiosubtype==0){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_475";
            }else if(vainfo.audiosubtype==1){
               document.getElementById('audiosubtype').innerText =" AUDIOMODE_AMRNB_515";
            }else if(vainfo.audiosubtype==2){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_590";
            }else if(vainfo.audiosubtype==3){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_670";
            }else if(vainfo.audiosubtype==4){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_740";
            }else if(vainfo.audiosubtype==5){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_795";
            }else if(vainfo.audiosubtype==6){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_102";
            }else if(vainfo.audiosubtype==7){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_122";
            }else if(vainfo.audiosubtype==8){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRNB_SID";
            }else{
               document.getElementById('audiosubtype').innerText =0;
            }
        }else if(vainfo.audiocodingformat==16777216){
            document.getElementById('audiocodingformat').innerText = "AMR-WB for 3GPP only";
            if(vainfo.audiosubtype==0){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_660";
            }else if(vainfo.audiosubtype==1){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_885";
            }else if(vainfo.audiosubtype==2){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_1265";
            }else if(vainfo.audiosubtype==3){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_1425";
            }else if(vainfo.audiosubtype==4){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_1585";
            }else if(vainfo.audiosubtype==5){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_1825";
            }else if(vainfo.audiosubtype==6){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_1985";
            }else if(vainfo.audiosubtype==7){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_2305";
            }else if(vainfo.audiosubtype==8){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_2385";
            }else if(vainfo.audiosubtype==9){
               document.getElementById('audiosubtype').innerText ="AUDIOMODE_AMRWB_SID";
            }else{
               document.getElementById('audiosubtype').innerText =0;
            }
        }else if(vainfo.audiocodingformat==268435456){
            document.getElementById('audiocodingformat').innerText = "自定义1";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==536870912){
            document.getElementById('audiocodingformat').innerText = "自定义2";
            document.getElementById('audiosubtype').innerText =0;
        }else if(vainfo.audiocodingformat==1073741824){
            document.getElementById('audiocodingformat').innerText = "自定义3";  
            document.getElementById('audiosubtype').innerText =0;
        }else
        {
            document.getElementById('audiocodingformat').innerText = "自定义4";
            document.getElementById('audiosubtype').innerText =0;
        }
         
        document.getElementById('channels').innerText = vainfo.channels;
        document.getElementById('audiodatarate').innerText = vainfo.audiodatarate;
        document.getElementById('audiosamplingfreq').innerText = vainfo.audiosamplingfreq;
        document.getElementById('audiobitdepth').innerText = vainfo.audiobitdepth;
        document.getElementById('blockalign').innerText = vainfo.blockalign;
        document.getElementById('filesize').innerText = vainfo.filesize;
        document.getElementById('audiosamples').innerText = vainfo.audiosamples;
        document.getElementById('videoframes').innerText = vainfo.videoframes;
        document.getElementById('exattribute1').innerText = vainfo.exattribute1;
        document.getElementById('exattribute2').innerText = vainfo.exattribute2;
        document.getElementById('exattribute3').innerText = vainfo.exattribute3;
        document.getElementById('exattribute4').innerText = vainfo.exattribute4;
        document.getElementById('exattribute5').innerText = vainfo.exattribute5;
   }

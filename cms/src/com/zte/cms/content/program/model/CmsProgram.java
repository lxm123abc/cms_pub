package com.zte.cms.content.program.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsProgram extends DynamicBaseObject
{
    private java.lang.Long programindex;
    private java.lang.String programid;
    private java.lang.String programindexs;
    private java.lang.Long cpindex;
    private java.lang.String cpid;
    private java.lang.String cpcontentid;
    private java.lang.Long ordernumber;
    private java.lang.String namecn;
    private java.lang.String originalnamecn;
    private java.lang.String firstletter;
    private java.lang.String nameen;
    private java.lang.String riginalnameen;
    private java.lang.String sortname;
    private java.lang.String searchname;
    private java.lang.String genre;
    private java.lang.String desccn;
    private java.lang.String descen;
    private java.lang.String contentversion;
    private java.lang.Integer seriesflag;
    private java.lang.Integer contenttype;
    private java.lang.String contenttypeid;
    private java.lang.Integer platform;
    private java.lang.Integer isvalid;
    private java.lang.Integer status;
    private java.lang.Integer oldstatus;
    private java.lang.String keywords;
    private java.lang.String copyrightcn;
    private java.lang.String copyrighten;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String effectivedate;
    private java.lang.String expirydate;
    private java.lang.Long exclusivevalidity;
    private java.lang.Integer copyrightexpiration;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String llicensofflinetime;
    private java.lang.String deletetime;
    private java.lang.Integer countrytype;
    private java.lang.String country;
    private java.lang.String copyproperty;
    private java.lang.Integer authorizationway;
    private java.lang.String importlisence;
    private java.lang.String releaselisence;
    private java.lang.String provid;
    private java.lang.String cityid;
    private java.lang.Long licenseindex;
    private java.lang.String licenseid;
    private java.lang.Integer tradetype;
    private java.lang.String contenttags;
    private java.lang.String pricetaxin;
    private java.lang.Integer newcomedays;
    private java.lang.Integer remainingdays;
    private java.lang.Long clicknumber;
    private java.lang.Integer recommendstart;
    private java.lang.Integer rating;
    private java.lang.String actors;
    private java.lang.String writers;
    private java.lang.String directors;
    private java.lang.String language;
    private java.lang.String releaseyear;
    private java.lang.String orgairdate;
    private java.lang.String licensingstart;
    private java.lang.String licensingend;
    private java.lang.Integer macrovision;
    private java.lang.Integer sourcetype;
    private java.lang.String viewpoint;
    private java.lang.String awards;
    private java.lang.Integer duration;
    private java.lang.String programtype;
    private java.lang.String productcorp;
    private java.lang.String producer;
    private java.lang.String publisher;
    private java.lang.String subtitlelanguage;
    private java.lang.String dubbinglanguage;
    private java.lang.String title;
    private java.lang.String reserve1;
    private java.lang.String reserve2;
    private java.lang.String reserve3;
    private java.lang.String reserve4;
    private java.lang.String reserve5;
    private java.lang.String unicontentid;
    private java.lang.String submittername;
    private java.lang.String creatoropername;
    private java.lang.Integer templatetype;
    private java.lang.String effecttime;
    private java.lang.String wkfwpriority;
    private java.lang.String wkfwroute;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;
    private java.lang.String onlinetimeStartDate;
    private java.lang.String onlinetimeEndDate;
    private java.lang.String cntofflinestarttime;
    private java.lang.String cntofflineendtime;
    private java.lang.String createStarttime;
    private java.lang.String createEndtime;
    private java.lang.String statuses;
    private java.lang.Integer encryptionstatus;
    private java.lang.Integer encryptiontype;
    private java.lang.String operid;
    private java.lang.String cpcnshortname;
    private java.lang.String scriptwriter;
    private java.lang.String compere;
    private java.lang.String guest;
    private java.lang.String reporter;
    private java.lang.String opincharge;
    private java.lang.String kpeople;
    

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.Long getProgramindex()
    {
        return programindex;
    }

    public void setProgramindex(java.lang.Long programindex)
    {
        this.programindex = programindex;
    }

    public java.lang.String getProgramid()
    {
        return programid;
    }

    public void setProgramid(java.lang.String programid)
    {
        this.programid = programid;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.Long getOrdernumber()
    {
        return ordernumber;
    }

    public void setOrdernumber(java.lang.Long ordernumber)
    {
        this.ordernumber = ordernumber;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getOriginalnamecn()
    {
        return originalnamecn;
    }

    public void setOriginalnamecn(java.lang.String originalnamecn)
    {
        this.originalnamecn = originalnamecn;
    }

    public java.lang.String getFirstletter()
    {
        return firstletter;
    }

    public void setFirstletter(java.lang.String firstletter)
    {
        this.firstletter = firstletter;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getRiginalnameen()
    {
        return riginalnameen;
    }

    public void setRiginalnameen(java.lang.String riginalnameen)
    {
        this.riginalnameen = riginalnameen;
    }

    public java.lang.String getSortname()
    {
        return sortname;
    }

    public void setSortname(java.lang.String sortname)
    {
        this.sortname = sortname;
    }

    public java.lang.String getSearchname()
    {
        return searchname;
    }

    public void setSearchname(java.lang.String searchname)
    {
        this.searchname = searchname;
    }

    public java.lang.String getGenre()
    {
        return genre;
    }

    public void setGenre(java.lang.String genre)
    {
        this.genre = genre;
    }

    public java.lang.String getDesccn()
    {
        return desccn;
    }

    public void setDesccn(java.lang.String desccn)
    {
        this.desccn = desccn;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.String getContentversion()
    {
        return contentversion;
    }

    public void setContentversion(java.lang.String contentversion)
    {
        this.contentversion = contentversion;
    }

    public java.lang.Integer getSeriesflag()
    {
        return seriesflag;
    }

    public void setSeriesflag(java.lang.Integer seriesflag)
    {
        this.seriesflag = seriesflag;
    }

    public java.lang.Integer getContenttype()
    {
        return contenttype;
    }

    public void setContenttype(java.lang.Integer contenttype)
    {
        this.contenttype = contenttype;
    }

    public java.lang.String getContenttypeid()
    {
        return contenttypeid;
    }

    public void setContenttypeid(java.lang.String contenttypeid)
    {
        this.contenttypeid = contenttypeid;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.Integer getIsvalid()
    {
        return isvalid;
    }

    public void setIsvalid(java.lang.Integer isvalid)
    {
        this.isvalid = isvalid;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getOldstatus()
    {
        return oldstatus;
    }

    public void setOldstatus(java.lang.Integer oldstatus)
    {
        this.oldstatus = oldstatus;
    }

    public java.lang.String getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.String keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.String getCopyrightcn()
    {
        return copyrightcn;
    }

    public void setCopyrightcn(java.lang.String copyrightcn)
    {
        this.copyrightcn = copyrightcn;
    }

    public java.lang.String getCopyrighten()
    {
        return copyrighten;
    }

    public void setCopyrighten(java.lang.String copyrighten)
    {
        this.copyrighten = copyrighten;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getEffectivedate()
    {
        return effectivedate;
    }

    public void setEffectivedate(java.lang.String effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public java.lang.String getExpirydate()
    {
        return expirydate;
    }

    public void setExpirydate(java.lang.String expirydate)
    {
        this.expirydate = expirydate;
    }

    public java.lang.Long getExclusivevalidity()
    {
        return exclusivevalidity;
    }

    public void setExclusivevalidity(java.lang.Long exclusivevalidity)
    {
        this.exclusivevalidity = exclusivevalidity;
    }

    public java.lang.Integer getCopyrightexpiration()
    {
        return copyrightexpiration;
    }

    public void setCopyrightexpiration(java.lang.Integer copyrightexpiration)
    {
        this.copyrightexpiration = copyrightexpiration;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getLlicensofflinetime()
    {
        return llicensofflinetime;
    }

    public void setLlicensofflinetime(java.lang.String llicensofflinetime)
    {
        this.llicensofflinetime = llicensofflinetime;
    }

    public java.lang.String getDeletetime()
    {
        return deletetime;
    }

    public void setDeletetime(java.lang.String deletetime)
    {
        this.deletetime = deletetime;
    }

    public java.lang.Integer getCountrytype()
    {
        return countrytype;
    }

    public void setCountrytype(java.lang.Integer countrytype)
    {
        this.countrytype = countrytype;
    }

    public java.lang.String getCountry()
    {
        return country;
    }

    public void setCountry(java.lang.String country)
    {
        this.country = country;
    }

    public java.lang.String getCopyproperty()
    {
        return copyproperty;
    }

    public void setCopyproperty(java.lang.String copyproperty)
    {
        this.copyproperty = copyproperty;
    }

    public java.lang.Integer getAuthorizationway()
    {
        return authorizationway;
    }

    public void setAuthorizationway(java.lang.Integer authorizationway)
    {
        this.authorizationway = authorizationway;
    }

    public java.lang.String getImportlisence()
    {
        return importlisence;
    }

    public void setImportlisence(java.lang.String importlisence)
    {
        this.importlisence = importlisence;
    }

    public java.lang.String getReleaselisence()
    {
        return releaselisence;
    }

    public void setReleaselisence(java.lang.String releaselisence)
    {
        this.releaselisence = releaselisence;
    }

    public java.lang.String getProvid()
    {
        return provid;
    }

    public void setProvid(java.lang.String provid)
    {
        this.provid = provid;
    }

    public java.lang.String getCityid()
    {
        return cityid;
    }

    public void setCityid(java.lang.String cityid)
    {
        this.cityid = cityid;
    }

    public java.lang.Long getLicenseindex()
    {
        return licenseindex;
    }

    public void setLicenseindex(java.lang.Long licenseindex)
    {
        this.licenseindex = licenseindex;
    }

    public java.lang.String getLicenseid()
    {
        return licenseid;
    }

    public void setLicenseid(java.lang.String licenseid)
    {
        this.licenseid = licenseid;
    }

    public java.lang.Integer getTradetype()
    {
        return tradetype;
    }

    public void setTradetype(java.lang.Integer tradetype)
    {
        this.tradetype = tradetype;
    }

    public java.lang.String getContenttags()
    {
        return contenttags;
    }

    public void setContenttags(java.lang.String contenttags)
    {
        this.contenttags = contenttags;
    }

    public java.lang.String getPricetaxin()
    {
        return pricetaxin;
    }

    public void setPricetaxin(java.lang.String pricetaxin)
    {
        this.pricetaxin = pricetaxin;
    }

    public java.lang.Integer getNewcomedays()
    {
        return newcomedays;
    }

    public void setNewcomedays(java.lang.Integer newcomedays)
    {
        this.newcomedays = newcomedays;
    }

    public java.lang.Integer getRemainingdays()
    {
        return remainingdays;
    }

    public void setRemainingdays(java.lang.Integer remainingdays)
    {
        this.remainingdays = remainingdays;
    }

    public java.lang.Long getClicknumber()
    {
        return clicknumber;
    }

    public void setClicknumber(java.lang.Long clicknumber)
    {
        this.clicknumber = clicknumber;
    }

    public java.lang.Integer getRecommendstart()
    {
        return recommendstart;
    }

    public void setRecommendstart(java.lang.Integer recommendstart)
    {
        this.recommendstart = recommendstart;
    }

    public java.lang.Integer getRating()
    {
        return rating;
    }

    public void setRating(java.lang.Integer rating)
    {
        this.rating = rating;
    }

    public java.lang.String getActors()
    {
        return actors;
    }

    public void setActors(java.lang.String actors)
    {
        this.actors = actors;
    }

    public java.lang.String getWriters()
    {
        return writers;
    }

    public void setWriters(java.lang.String writers)
    {
        this.writers = writers;
    }

    public java.lang.String getDirectors()
    {
        return directors;
    }

    public void setDirectors(java.lang.String directors)
    {
        this.directors = directors;
    }

    public java.lang.String getLanguage()
    {
        return language;
    }

    public void setLanguage(java.lang.String language)
    {
        this.language = language;
    }

    public java.lang.String getReleaseyear()
    {
        return releaseyear;
    }

    public void setReleaseyear(java.lang.String releaseyear)
    {
        this.releaseyear = releaseyear;
    }

    public java.lang.String getOrgairdate()
    {
        return orgairdate;
    }

    public void setOrgairdate(java.lang.String orgairdate)
    {
        this.orgairdate = orgairdate;
    }

    public java.lang.String getLicensingstart()
    {
        return licensingstart;
    }

    public void setLicensingstart(java.lang.String licensingstart)
    {
        this.licensingstart = licensingstart;
    }

    public java.lang.String getLicensingend()
    {
        return licensingend;
    }

    public void setLicensingend(java.lang.String licensingend)
    {
        this.licensingend = licensingend;
    }

    public java.lang.Integer getMacrovision()
    {
        return macrovision;
    }

    public void setMacrovision(java.lang.Integer macrovision)
    {
        this.macrovision = macrovision;
    }

    public java.lang.Integer getSourcetype()
    {
        return sourcetype;
    }

    public void setSourcetype(java.lang.Integer sourcetype)
    {
        this.sourcetype = sourcetype;
    }

    public java.lang.String getViewpoint()
    {
        return viewpoint;
    }

    public void setViewpoint(java.lang.String viewpoint)
    {
        this.viewpoint = viewpoint;
    }

    public java.lang.String getAwards()
    {
        return awards;
    }

    public void setAwards(java.lang.String awards)
    {
        this.awards = awards;
    }

    public java.lang.Integer getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.Integer duration)
    {
        this.duration = duration;
    }

    public java.lang.String getProgramtype()
    {
        return programtype;
    }

    public void setProgramtype(java.lang.String programtype)
    {
        this.programtype = programtype;
    }

    public java.lang.String getProductcorp()
    {
        return productcorp;
    }

    public void setProductcorp(java.lang.String productcorp)
    {
        this.productcorp = productcorp;
    }

    public java.lang.String getProducer()
    {
        return producer;
    }

    public void setProducer(java.lang.String producer)
    {
        this.producer = producer;
    }

    public java.lang.String getPublisher()
    {
        return publisher;
    }

    public void setPublisher(java.lang.String publisher)
    {
        this.publisher = publisher;
    }

    public java.lang.String getSubtitlelanguage()
    {
        return subtitlelanguage;
    }

    public void setSubtitlelanguage(java.lang.String subtitlelanguage)
    {
        this.subtitlelanguage = subtitlelanguage;
    }

    public java.lang.String getDubbinglanguage()
    {
        return dubbinglanguage;
    }

    public void setDubbinglanguage(java.lang.String dubbinglanguage)
    {
        this.dubbinglanguage = dubbinglanguage;
    }

    public java.lang.String getTitle()
    {
        return title;
    }

    public void setTitle(java.lang.String title)
    {
        this.title = title;
    }

    public java.lang.String getReserve1()
    {
        return reserve1;
    }

    public void setReserve1(java.lang.String reserve1)
    {
        this.reserve1 = reserve1;
    }

    public java.lang.String getReserve2()
    {
        return reserve2;
    }

    public void setReserve2(java.lang.String reserve2)
    {
        this.reserve2 = reserve2;
    }

    public java.lang.String getReserve3()
    {
        return reserve3;
    }

    public void setReserve3(java.lang.String reserve3)
    {
        this.reserve3 = reserve3;
    }

    public java.lang.String getReserve4()
    {
        return reserve4;
    }

    public void setReserve4(java.lang.String reserve4)
    {
        this.reserve4 = reserve4;
    }

    public java.lang.String getReserve5()
    {
        return reserve5;
    }

    public void setReserve5(java.lang.String reserve5)
    {
        this.reserve5 = reserve5;
    }

    public java.lang.String getUnicontentid()
    {
        return unicontentid;
    }

    public void setUnicontentid(java.lang.String unicontentid)
    {
        this.unicontentid = unicontentid;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public void initRelation()
    {
        this.addRelation("programindex", "PROGRAMINDEX");
        this.addRelation("programid", "PROGRAMID");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("ordernumber", "ORDERNUMBER");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("originalnamecn", "ORIGINALNAMECN");
        this.addRelation("firstletter", "FIRSTLETTER");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("riginalnameen", "RIGINALNAMEEN");
        this.addRelation("sortname", "SORTNAME");
        this.addRelation("searchname", "SEARCHNAME");
        this.addRelation("genre", "GENRE");
        this.addRelation("desccn", "DESCCN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("contentversion", "CONTENTVERSION");
        this.addRelation("seriesflag", "SERIESFLAG");
        this.addRelation("contenttype", "CONTENTTYPE");
        this.addRelation("contenttypeid", "CONTENTTYPEID");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("isvalid", "ISVALID");
        this.addRelation("status", "STATUS");
        this.addRelation("oldstatus", "OLDSTATUS");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("copyrightcn", "COPYRIGHTCN");
        this.addRelation("copyrighten", "COPYRIGHTEN");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("effectivedate", "EFFECTIVEDATE");
        this.addRelation("expirydate", "EXPIRYDATE");
        this.addRelation("exclusivevalidity", "EXCLUSIVEVALIDITY");
        this.addRelation("copyrightexpiration", "COPYRIGHTEXPIRATION");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("llicensofflinetime", "LLICENSOFFLINETIME");
        this.addRelation("deletetime", "DELETETIME");
        this.addRelation("countrytype", "COUNTRYTYPE");
        this.addRelation("country", "COUNTRY");
        this.addRelation("copyproperty", "COPYPROPERTY");
        this.addRelation("authorizationway", "AUTHORIZATIONWAY");
        this.addRelation("importlisence", "IMPORTLISENCE");
        this.addRelation("releaselisence", "RELEASELISENCE");
        this.addRelation("provid", "PROVID");
        this.addRelation("cityid", "CITYID");
        this.addRelation("licenseindex", "LICENSEINDEX");
        this.addRelation("licenseid", "LICENSEID");
        this.addRelation("tradetype", "TRADETYPE");
        this.addRelation("contenttags", "CONTENTTAGS");
        this.addRelation("pricetaxin", "PRICETAXIN");
        this.addRelation("newcomedays", "NEWCOMEDAYS");
        this.addRelation("remainingdays", "REMAININGDAYS");
        this.addRelation("clicknumber", "CLICKNUMBER");
        this.addRelation("recommendstart", "RECOMMENDSTART");
        this.addRelation("rating", "RATING");
        this.addRelation("actors", "ACTORS");
        this.addRelation("writers", "WRITERS");
        this.addRelation("directors", "DIRECTORS");
        this.addRelation("language", "LANGUAGE");
        this.addRelation("releaseyear", "RELEASEYEAR");
        this.addRelation("orgairdate", "ORGAIRDATE");
        this.addRelation("licensingstart", "LICENSINGSTART");
        this.addRelation("licensingend", "LICENSINGEND");
        this.addRelation("macrovision", "MACROVISION");
        this.addRelation("sourcetype", "SOURCETYPE");
        this.addRelation("viewpoint", "VIEWPOINT");
        this.addRelation("awards", "AWARDS");
        this.addRelation("duration", "DURATION");
        this.addRelation("programtype", "PROGRAMTYPE");
        this.addRelation("productcorp", "PRODUCTCORP");
        this.addRelation("producer", "PRODUCER");
        this.addRelation("publisher", "PUBLISHER");
        this.addRelation("subtitlelanguage", "SUBTITLELANGUAGE");
        this.addRelation("dubbinglanguage", "DUBBINGLANGUAGE");
        this.addRelation("title", "TITLE");
        this.addRelation("reserve1", "RESERVE1");
        this.addRelation("reserve2", "RESERVE2");
        this.addRelation("reserve3", "RESERVE3");
        this.addRelation("reserve4", "RESERVE4");
        this.addRelation("reserve5", "RESERVE5");
        this.addRelation("unicontentid", "UNICONTENTID");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
    }

    public java.lang.String getOnlinetimeStartDate()
    {
        return onlinetimeStartDate;
    }

    public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
    {
        this.onlinetimeStartDate = onlinetimeStartDate;
    }

    public java.lang.String getOnlinetimeEndDate()
    {
        return onlinetimeEndDate;
    }

    public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
    {
        this.onlinetimeEndDate = onlinetimeEndDate;
    }

    public java.lang.String getCntofflinestarttime()
    {
        return cntofflinestarttime;
    }

    public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
    {
        this.cntofflinestarttime = cntofflinestarttime;
    }

    public java.lang.String getCntofflineendtime()
    {
        return cntofflineendtime;
    }

    public void setCntofflineendtime(java.lang.String cntofflineendtime)
    {
        this.cntofflineendtime = cntofflineendtime;
    }

    public java.lang.String getCreateStarttime()
    {
        return createStarttime;
    }

    public void setCreateStarttime(java.lang.String createStarttime)
    {
        this.createStarttime = createStarttime;
    }

    public java.lang.String getCreateEndtime()
    {
        return createEndtime;
    }

    public void setCreateEndtime(java.lang.String createEndtime)
    {
        this.createEndtime = createEndtime;
    }

    public java.lang.String getProgramindexs()
    {
        return programindexs;
    }

    public void setProgramindexs(java.lang.String programindexs)
    {
        this.programindexs = programindexs;
    }

    public java.lang.String getStatuses()
    {
        return statuses;
    }
    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }

	public java.lang.Integer getEncryptionstatus() {
		return encryptionstatus;
	}

	public void setEncryptionstatus(java.lang.Integer encryptionstatus) {
		this.encryptionstatus = encryptionstatus;
	}

	public java.lang.Integer getEncryptiontype() {
		return encryptiontype;
	}

	public void setEncryptiontype(java.lang.Integer encryptiontype) {
		this.encryptiontype = encryptiontype;
	}

    public java.lang.String getScriptwriter()
    {
        return scriptwriter;
    }

    public void setScriptwriter(java.lang.String scriptwriter)
    {
        this.scriptwriter = scriptwriter;
    }

    public java.lang.String getCompere()
    {
        return compere;
    }

    public void setCompere(java.lang.String compere)
    {
        this.compere = compere;
    }

    public java.lang.String getGuest()
    {
        return guest;
    }

    public void setGuest(java.lang.String guest)
    {
        this.guest = guest;
    }

    public java.lang.String getReporter()
    {
        return reporter;
    }

    public void setReporter(java.lang.String reporter)
    {
        this.reporter = reporter;
    }

    public java.lang.String getOpincharge()
    {
        return opincharge;
    }

    public void setOpincharge(java.lang.String opincharge)
    {
        this.opincharge = opincharge;
    }

    public java.lang.String getKpeople()
    {
        return kpeople;
    }

    public void setKpeople(java.lang.String kpeople)
    {
        this.kpeople = kpeople;
    }
	
}

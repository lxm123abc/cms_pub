package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IScheduleWkfwhisDS
{
    /**
     * 新增ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void insertScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException;

    /**
     * 更新ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException;

    /**
     * 批量更新ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void updateScheduleWkfwhisList(List<ScheduleWkfwhis> scheduleWkfwhisList) throws DomainServiceException;

    /**
     * 删除ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException;

    /**
     * 批量删除ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public void removeScheduleWkfwhisList(List<ScheduleWkfwhis> scheduleWkfwhisList) throws DomainServiceException;

    /**
     * 查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @return ChannelWkfwhis对象
     * @throws DomainServiceException ds异常
     */
    public ScheduleWkfwhis getScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException;

    /**
     * 根据条件查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象
     * @return 满足条件的ChannelWkfwhis对象集
     * @throws DomainServiceException ds异常
     */
    public List<ScheduleWkfwhis> getScheduleWkfwhisByCond(ScheduleWkfwhis scheduleWkfwhis) throws DomainServiceException;

    /**
     * 根据条件分页查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ChannelWkfwhis对象
     * 
     * @param channelWkfwhis ChannelWkfwhis对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ScheduleWkfwhis scheduleWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

package com.zte.cms.commonSync.common;

import java.util.List;
import java.util.Map;

import com.zte.cms.commonSync.mappings.CreateXmlFile4Category_Channel_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Category_Program_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Category_Schedule_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Category_Series_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Category_Service_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Program_Castrolemap_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Program_Schedulerecord_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Series_Castrolemap_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Series_Program_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Service_Channel_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Service_Program_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Service_Schedule_Map;
import com.zte.cms.commonSync.mappings.CreateXmlFile4Service_Series_Map;
import com.zte.cms.commonSync.object.CreateXmlFile4Cast;
import com.zte.cms.commonSync.object.CreateXmlFile4Castrolemap;
import com.zte.cms.commonSync.object.CreateXmlFile4Category;
import com.zte.cms.commonSync.object.CreateXmlFile4Channel;
import com.zte.cms.commonSync.object.CreateXmlFile4Picture;
import com.zte.cms.commonSync.object.CreateXmlFile4Program;
import com.zte.cms.commonSync.object.CreateXmlFile4Schedule;
import com.zte.cms.commonSync.object.CreateXmlFile4Series;
import com.zte.cms.commonSync.object.CreateXmlFile4Service;
import com.zte.cms.commonSync.object.CreateXmlFile4EPG;

public class Object2XmlFile4IPTV20Imp implements IObject2XmlFile
{

    public String createXmlFile(Map<String, List> objectMap, int targetType, int objectType, int syncType) throws Exception
    {
        AbstractCreateXmlFile4Object xmlCreator = null;

        // 根据对象类型和规范往xml中写对象（object）或者mapping
        switch (objectType)
        {
            case 1:
                xmlCreator = new CreateXmlFile4Service();
                break;
            case 2:
                xmlCreator = new CreateXmlFile4Category();
                break;
            case 3:
                xmlCreator = new CreateXmlFile4Program();
                break;
            case 4:
                xmlCreator = new CreateXmlFile4Series();
                break;
            case 5:
                xmlCreator = new CreateXmlFile4Channel();
                break;
            case 6:
                xmlCreator = new CreateXmlFile4Schedule();
                break;
            case 7: // movie 不单发，发布program时带上一起发布
                break;
            case 8:// physicalchannel 发布channel时带上一起发布
                break;
            case 9:// schedulerecord 发布schedule时带上一起发布
                break;
            case 10:
                xmlCreator = new CreateXmlFile4Picture();
                break;
            case 11:
                xmlCreator = new CreateXmlFile4Cast();
                break;
            case 12:
                xmlCreator = new CreateXmlFile4Castrolemap();
                break;
            case 13:
                break;
            case 14:
                break;
            case 15:
                break;
            case 16:
            	xmlCreator = new CreateXmlFile4EPG();
                break;
            case 17:
                break;
            case 18:
                break;
            case 19:
                break;
            case 20:
                break;
            case 21:
                xmlCreator = new CreateXmlFile4Service_Program_Map();
                break;
            case 22:
                xmlCreator = new CreateXmlFile4Service_Series_Map();
                break;
            case 23:
                xmlCreator = new CreateXmlFile4Service_Channel_Map();
                break;
            case 24:// 业务侧不支持
                xmlCreator = new CreateXmlFile4Service_Schedule_Map();
                break;
            case 25:// service-picture 不单发 ，发布海报时一起发布 业务侧不支持
                break;
            case 26:
                xmlCreator = new CreateXmlFile4Category_Program_Map();
                break;
            case 27:
                xmlCreator = new CreateXmlFile4Category_Series_Map();
                break;
            case 28:
                xmlCreator = new CreateXmlFile4Category_Channel_Map();
                break;
            case 29:// 业务侧不支持
                xmlCreator = new CreateXmlFile4Category_Schedule_Map();
                break;
            case 30:
                xmlCreator = new CreateXmlFile4Category_Service_Map();
                break;
            case 31:// Category-picture 不单发 ，发布海报时一起发布
                break;
            case 32:// Program-movie 不单发 ，发布program时一起发布
                break;
            case 33:// 业务侧不支持
                xmlCreator = new CreateXmlFile4Program_Schedulerecord_Map();
                break;
            case 34:
                xmlCreator = new CreateXmlFile4Program_Castrolemap_Map();
                break;
            case 35:// program-picture 不单发 ，发布海报时一起发布
                break;
            case 36:
                xmlCreator = new CreateXmlFile4Series_Program_Map();
                break;
            case 37:
                xmlCreator = new CreateXmlFile4Series_Castrolemap_Map();
                break;
            case 38:// series-picture 不单发 ，发布海报时一起发布
                break;
            case 39:// channel-picture 不单发 ，发布海报时一起发布
                break;
            case 40:// Schedual-schedualrecord 不单发 ，发布时和schedule一起发布
                break;
            case 41:// cast-picture 不单发 ，发布海报时一起发布
                break;
            default:
                xmlCreator = null;
        }
        // 返回xml文件路径
        if (null == xmlCreator)
        {
            return null;
        }else
        {
            xmlCreator.init();
            switch(targetType)
            {
                case 0:
                    return xmlCreator.createXmlFile4WG18(objectMap, targetType, syncType);
                case 10:
                    return xmlCreator.createXmlFile4YS26(objectMap, targetType, syncType);
                default:
                    return null;
            }
            
        }
    }

}

package com.zte.cms.categorycdn.service;

import java.util.List;

import com.zte.cms.categorycdn.dao.ICategorycdnDAO;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.cms.common.Generator;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.EspecialCharMgt;

public class CategorycdnDS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICategorycdnDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICategorycdnDAO dao = null;

    public void setDao(ICategorycdnDAO dao)
    {
        this.dao = dao;
    }

    public void insertCategorycdn(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("insert categorycdn starting...");
        try
        {
            dao.insertCategorycdn(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert categorycdn end");
    }

    public void updateCategorycdn(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("update categorycdn by pk starting...");
        try
        {
            dao.updateCategorycdn(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categorycdn by pk end");
    }

    public void updateCategorycdnList(List<Categorycdn> categorycdnList) throws DomainServiceException
    {
        log.debug("update categorycdnList by pk starting...");
        if (null == categorycdnList || categorycdnList.size() == 0)
        {
            log.debug("there is no datas in categorycdnList");
            return;
        }
        try
        {
            for (Categorycdn categorycdn : categorycdnList)
            {
                dao.updateCategorycdn(categorycdn);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categorycdnList by pk end");
    }

    public void removeCategorycdn(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("remove categorycdn by pk starting...");
        try
        {
            dao.deleteCategorycdn(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categorycdn by pk end");
    }

    public void removeCategorycdnList(List<Categorycdn> categorycdnList) throws DomainServiceException
    {
        log.debug("remove categorycdnList by pk starting...");
        if (null == categorycdnList || categorycdnList.size() == 0)
        {
            log.debug("there is no datas in categorycdnList");
            return;
        }
        try
        {
            for (Categorycdn categorycdn : categorycdnList)
            {
                dao.deleteCategorycdn(categorycdn);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categorycdnList by pk end");
    }

    public Categorycdn getCategorycdn(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("get categorycdn by pk starting...");
        Categorycdn rsObj = null;
        try
        {
            rsObj = dao.getCategorycdn(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categorycdnList by pk end");
        return rsObj;
    }

    public List<Categorycdn> getCategorycdnByCond(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("get categorycdn by condition starting...");
        List<Categorycdn> rsList = null;
        try
        {
            rsList = dao.getCategorycdnByCond(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categorycdn by condition end");
        return rsList;
    }
    public List<Categorycdn> selectByCategoryindex(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug("get categorycdn by condition starting...");
        List<Categorycdn> rsList = null;
        try
        {
            rsList = dao.selectByCategoryindex(categorycdn);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categorycdn by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get categorycdn page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categorycdn, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Categorycdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categorycdn page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get categorycdn page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categorycdn, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Categorycdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categorycdn page info by condition end");
        return tableInfo;
    }

    public List<Categorycdn> listMenuTreeByParentid(String typeid)
    {
        return dao.listMenuTreeByParentid(typeid);
    }
    
    public List<Categorycdn> listMenuTreeByParentidcategoryAll(String typeid)
    {
        return dao.listMenuTreeByParentidcategoryAll(typeid);
    }

    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex)
    {
        return dao.listMenuTreeByParentidPub(parentindex);
    }
    
    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex, String type, String obindex)
    {
        return dao.listMenuTreeByParentidPub(parentindex,type,obindex);
    }

    public TableDataInfo queryCmsCategory(Categorycdn categorycdn, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("queryCmsProgramtype info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
        	categorycdn.setCategoryname(EspecialCharMgt.conversion(categorycdn.getCategoryname()));
        	categorycdn.setCategoryid(EspecialCharMgt.conversion(categorycdn.getCategoryid()));

        	pageInfo = dao.pageInfoQuery(categorycdn, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Categorycdn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("queryCmsProgramtype info by condition end");
        return tableInfo;
    }

    // 按照id查询
    public Categorycdn getCmsCategoryById(Long id)
    {
        return dao.getCmsProgramtypeById(id);
    }

    public Integer queryByCategoryid(Categorycdn categorycdn)
    {
        return dao.queryByCategoryid(categorycdn);
    }

    public Integer queryByCategoryname(Categorycdn categorycdn)
    {
        return dao.queryByCategoryname(categorycdn);
    }

    public Categorycdn getCmsCategory(String categoryid)
    {
        return dao.getCmsCategory(categoryid);
    }

    public Categorycdn categoryInsert(Categorycdn categorycdn)
    {
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_category");
        categorycdn.setCategoryindex(index);
        String contentid = Generator.getContentId(00000000L,"CATE");//获取objectid
        //String categorycode = Generator.getWGCodeByContentId(contentid);//文广code
        categorycdn.setCategoryid(contentid);
        categorycdn.setCatagorycode(contentid);
        try
        {
            dao.insertCmsCategory(categorycdn);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
        return categorycdn;
    }

    public List<Categorycdn> queryChildCmsCategory(Long id)
    {
        return dao.queryChildCmsCategory(id);
    }

    public Categorycdn queryByParentid(String typeid)
    {
        return dao.queryByParentid(typeid);
    }

    public Long queryCategoryindexByCategoryname(Categorycdn categorycdn)
    {
        return dao.queryCategoryindexByCategoryname(categorycdn);
    }

    public Categorycdn getCategoryByCategoryId(Categorycdn categorycdn) throws DomainServiceException
    {
        log.debug( "get getCategoryByCategoryIdstarting..." );
        Categorycdn rsObj = new Categorycdn();
        try
        {     
            rsObj = dao.getCategoryByCategoryId(categorycdn);
                        
        }
        catch( Exception daoEx )
        {
            log.error( "dao exception:", daoEx );           
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get getCategoryByCategoryId end" );
        return rsObj;
 
    }

    /**
     * 
     */
    public TableDataInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("query category and content map from content information given starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.getCategoryCntMapQuery(categorycdnMap, start, pageSize, puEntity);
        } catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategorycdnMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("query category and content map from content information given ends...");
        return tableInfo;
    }
}

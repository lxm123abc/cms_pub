   function dealErrorResult(errorCode, rtn_expDesc)
    {
    	var flag = _para.flag;
    	var rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag);
    	openMsg("1:" + rtn_expDesc, rtnUrl, true)
    }

  function movietypeCalculate(keyvalue)
{
	var movietype = keyvalue;
	if(movietype==1)
	{
		return $res_entry("label.cms.srcfiletype1");
	}else if(movietype==2)
	{
		return $res_entry("label.cms.srcfiletype2");
	}else
	{
		return "";
	}
}

function systemlayerCalculate(keyvalue)
{
	var systemlayer = keyvalue;
	if(systemlayer==1)
	{
		return "TS";
	}else if(systemlayer==2)
	{
		return "3GP";
	}else
	{
		return "";
	}
}

function durationCalculate(keyvalue)
{
	var duration = keyvalue;

	var rtnStr = "";
	if(duration!=null && duration!=""){
		rtnStr = duration.substring(0,2) + ":" + duration.substring(2,4) + ":" + duration.substring(4,6) + ":" + duration.substring(6,8);
	}
	return rtnStr;
}


function sourcedrmtypeCalculate(keyvalue)
{
	var sourcedrmtype = keyvalue;
	if(sourcedrmtype==0)
	{
		return $res_entry("msg.info.basicdata.025");
	}else if(sourcedrmtype==1)
	{
		return $res_entry("msg.info.basicdata.024");
	}else
	{
		return "";
	}
}

function destdrmtypeCalculate(keyvalue)
{
	var destdrmtype = keyvalue;
	if(destdrmtype==0)
	{
		return $res_entry("msg.info.basicdata.025");
	}else if(destdrmtype==1)
	{
		return $res_entry("msg.info.basicdata.024");
	}else
	{
		return "";
	}
}

function screenformatCalculate(keyvalue)
{
	var screenformat = keyvalue;
	if(screenformat==0)
	{
		return "4x3";
	}else if(screenformat==1)
	{
		return "16x9(Wide)";
	}else
	{
		return "";
	}
}

function closedcaptioningCalculate(keyvalue)
{
	var closedcaptioning = keyvalue;
	if(closedcaptioning==0)
	{
		return $res_entry("msg.info.content.192");
	}else if(closedcaptioning==1)
	{
		return $res_entry("msg.info.content.193");
	}else
	{
		return "";
	}
}

function domainCalculate(keyvalue)
{
	var domain = keyvalue;
	switch(domain)
	{
	 case  257:  
		 return "IPTV网络(IPTV TS RTSP)";
	 case  258:
		 return "IPTV网络(HPD)";
	 case  260:
		 return "IPTV网络(ISMA RTSP)";
	 case  264:	 
		 return "IPTV网络(HLS)";
	 case  513:
		 return "互联网网络(IPTV TS RTSP)";
	 case  514:
		 return "互联网网络(HPD)";
	 case  516:
		 return "互联网网络(ISMA RTSP)";
	 case  520:
		 return "互联网网络(HLS)";
	 case  1025:
		 return "移动网络(IPTV TS RTSP)";
	 case  1026:
		 return "移动网络(HPD)";
	 case  1028:
		 return "移动网络(ISMA RTSP)";
	 case  1032:	
		 return "移动网络(HLS)";
	 default:
		 return "";
	}		
}
function hotdegreeCalculate(keyvalue)
{
	var hotdegree = keyvalue;
	if(hotdegree==0)
	{
		return $res_entry("label.cms.channel.info.hotdegree0");
	}else if(hotdegree==1)
	{
		return $res_entry("label.cms.channel.info.hotdegree1");
	}else
	{
		return "";
	}
}


  var bitratetypeMap ={};
  var videotypeMap={};
  var audiotypeMap={};
  var resolutionMap={};
  var videoprofileMap={};
  var audiotrackMap={};
  var fileMovieObj = {}; 
  var o_fileMovieObj = {};
  
  function showMovieInfo(){
        
         fileMovieObj['programid'] = _para.contentid;
         o_fileMovieObj['programid'] = _para.contentid;
         callUrl("cmsMovieBufFacade/getCmsMovieBufByCond.ssm",fileMovieObj,"fileMovieObj","true");   //显示子内容缓存表
         callUrl("cmsMovieLSFacade/getCmsMovieByProgramid.ssm",_para.contentid,"o_fileMovieObj","true"); //显示子内容以前的信息
         
         callUrl('sysCode/getSysCodeMapByCodekey.ssm','["cms_movie_bitratetype"]','bitratetypeMap',true); 
         callUrl("sysCode/getSysCodeMapByCodekey.ssm",'[cms_movie_videotype]','videotypeMap',true);  
         callUrl('sysCode/getSysCodeMapByCodekey.ssm','["cms_movie_audiotype"]','audiotypeMap',true); 
         callUrl('sysCode/getSysCodeMapByCodekey.ssm','[cms_movie_resolution]','resolutionMap',true); 
         callUrl("sysCode/getSysCodeMapByCodekey.ssm",'[cms_movie_videoprofile]','videoprofileMap',true); 
         callUrl("sysCode/getSysCodeMapByCodekey.ssm",'[cms_movie_audiotrack]','audiotrackMap',true);     
         bitratetypeMap = bitratetypeMap.rtnValue;
         videotypeMap = videotypeMap.rtnValue;
         audiotypeMap = audiotypeMap.rtnValue;
         resolutionMap = resolutionMap.rtnValue;
         videoprofileMap = videoprofileMap.rtnValue;
         audiotrackMap = audiotrackMap.rtnValue;
         
         fileMovieObj = fileMovieObj.rtnValue;
         o_fileMovieObj = o_fileMovieObj.rtnValue;
        
         if(fileMovieObj!= null &&  typeof(fileMovieObj) != "undefined" && fileMovieObj.length > 0 ){
            for(var k = 0; k < fileMovieObj.length; k++){
                      makeRows();  //显示视频信息
                      if(fileMovieObj[k]['namecn']!= null){ //子内容名称
                          document.getElementById("sonnamecn"+cc).innerHTML = fileMovieObj[k]['namecn'];
                      }
                      if(fileMovieObj[k]['movieindex']!= null){ //子内容索引
                          document.getElementById("movieindex"+cc).innerHTML = fileMovieObj[k]['movieindex'];
                      }
                     if(fileMovieObj[k]['movietype']!= null){ //媒体类型
                          document.getElementById("movietype"+cc).innerHTML = movietypeCalculate(fileMovieObj[k]['movietype']);
                     }
		              if(fileMovieObj[k]['systemlayer']!= null){ //封装格式
		                document.getElementById("systemlayer"+cc).innerHTML = systemlayerCalculate(fileMovieObj[k]['systemlayer']);
		              }
		              if(fileMovieObj[k]['videotype']!= null){ //视频编码格式
		                document.getElementById("videotype"+cc).innerHTML = videotypeMap[fileMovieObj[k]['videotype']];
		              }
		              if(fileMovieObj[k]['audiotrack']!=null){ //音频编码格式
		               document.getElementById("audiotrack"+cc).innerHTML = audiotypeMap[fileMovieObj[k]['audiotrack']];
		              }
		            
		              if(fileMovieObj[k]['resolution']!=null){ //分辨率类型
		                document.getElementById("resolution"+cc).innerHTML = resolutionMap[fileMovieObj[k]['resolution']];
		              }
		              if(fileMovieObj[k]['videoprofile']!= null){//视频规格
		               document.getElementById("videoprofile"+cc).innerHTML = videoprofileMap[fileMovieObj[k]['videoprofile']];
		              }
		             if(fileMovieObj[k]['bitratetype']!=null){ //码流速率
		               document.getElementById("bitratetype"+cc).innerHTML = bitratetypeMap[fileMovieObj[k]['bitratetype']];
		             }
		             
		               if(fileMovieObj[k]['audiotype']!= null){ // 声道类型
		               document.getElementById("audiotype"+cc).innerHTML = audiotrackMap[fileMovieObj[k]['audiotype']];
		             }
		              
		             if(fileMovieObj[k]['numberofframes']!=null){ //帧数
		               document.getElementById("numberofframes"+cc).innerHTML = fileMovieObj[k]['numberofframes'];
		              }
		              if(fileMovieObj[k]['frameheight']!= null){ //帧高
		                document.getElementById("frameheight"+cc).innerHTML = fileMovieObj[k]['frameheight'];
		              }
		              if(fileMovieObj[k]['framewidth']!=null){//帧宽
		                document.getElementById("framewidth"+cc).innerHTML = fileMovieObj[k]['framewidth'];
		              }
		              if(fileMovieObj[k]['framerate']!=null){ //帧率(fps)
		                document.getElementById("framerate"+cc).innerHTML = fileMovieObj[k]['framerate'];
		              }
		              if(fileMovieObj[k]['sourcedrmtype']!=null){ //源文件是否有版权保护
		                document.getElementById("sourcedrmtype"+cc).innerHTML = sourcedrmtypeCalculate(fileMovieObj[k]['sourcedrmtype']);
		              }
		               if(fileMovieObj[k]['destdrmtype']!=null){ //目标文件是否有版权保护
		                document.getElementById("destdrmtype"+cc).innerHTML = destdrmtypeCalculate(fileMovieObj[k]['destdrmtype']);
		               }
		              if(fileMovieObj[k]['screenformat']!=null){ //屏幕格式
		                document.getElementById("screenformat"+cc).innerHTML = screenformatCalculate(fileMovieObj[k]['screenformat']);
		              }
		              if(fileMovieObj[k]['domain']!=null){  //发布到CDN的域标
		                 document.getElementById("domain"+cc).innerHTML = domainCalculate(fileMovieObj[k]['domain']);
		              }
		              if(fileMovieObj[k]['closedcaptioning']!=null){  //屏幕格式
		                 document.getElementById("closedcaptioning"+cc).innerHTML = closedcaptioningCalculate(fileMovieObj[k]['closedcaptioning']);
		              }
		              if(fileMovieObj[k]['hotdegree']!=null){ //发布到CDN的热度
		               document.getElementById("hotdegree"+cc).innerHTML = hotdegreeCalculate(fileMovieObj[k]['hotdegree']);
		              }
		              if( fileMovieObj[k]['duration']!=null){ //播放时长
		                document.getElementById("duration"+cc).innerHTML = durationCalculate(fileMovieObj[k]['duration']);
		              }
		              if(fileMovieObj[k]['fileurl']!= null){ //文件地址
		                document.getElementById("fileurl"+cc).innerHTML = fileMovieObj[k]['fileurl'];
		              }
                      if(o_fileMovieObj.length > 0){
                          for(var m = 0; m < o_fileMovieObj.length;m++ ){ 
                             if(o_fileMovieObj[m]['movieindex'] == fileMovieObj[k]['movieindex'] ){// 循环判断缓存表里面的index 和以前表里面的index 是否相同
                                      
			                             if(o_fileMovieObj[m]['namecn'] != null){
			                                document.getElementById("o_sonnamecn"+cc).innerHTML = o_fileMovieObj[m]['namecn'];
			                             }
			                             setDisplay(o_fileMovieObj[m]['namecn'],'sonnamecn'+cc,'d_sonnamecn'+cc);
			                             document.getElementById("o_movieindex"+cc).innerHTML = o_fileMovieObj[m]['movieindex'];
			                            
			                            if(o_fileMovieObj[m]['movietype']!= null){
			                               document.getElementById("o_movietype"+cc).innerHTML = movietypeCalculate(o_fileMovieObj[m]['movietype']);
			                             }
			                             setDisplay(movietypeCalculate(o_fileMovieObj[m]['movietype']),'movietype'+cc,'d_movietype'+cc);	
			                        
			                             if(o_fileMovieObj[m]['systemlayer'] != null){
			                                document.getElementById("o_systemlayer"+cc).innerHTML = systemlayerCalculate(o_fileMovieObj[m]['systemlayer']);
			                             }
							              setDisplay(systemlayerCalculate(o_fileMovieObj[m]['systemlayer']),'systemlayer'+cc,'d_systemlayer'+cc);
							              
							              if(o_fileMovieObj[m]['videotype'] != null){
							                document.getElementById("o_videotype"+cc).innerHTML = videotypeMap[o_fileMovieObj[m]['videotype']];
							              }
							              setDisplay(document.getElementById("o_videotype"+cc).innerHTML,'videotype'+cc,'d_videotype'+cc);
							              
							              
							              if(o_fileMovieObj[m]['audiotype'] != null){
							                document.getElementById("o_audiotype"+cc).innerHTML = audiotrackMap[o_fileMovieObj[m]['audiotype']];
							              }
							               setDisplay(audiotrackMap[o_fileMovieObj[m]['audiotype']],'audiotype'+cc,'d_audiotype'+cc);		
							             
							             if(o_fileMovieObj[m]['resolution'] != null){
							                document.getElementById("o_resolution"+cc).innerHTML = resolutionMap[o_fileMovieObj[m]['resolution']];
							              }
							             setDisplay(resolutionMap[o_fileMovieObj[m]['resolution']],'resolution'+cc,'d_resolution'+cc);		
							              
							              if(o_fileMovieObj[m]['videoprofile'] != null){
							                 document.getElementById("o_videoprofile"+cc).innerHTML = videoprofileMap[o_fileMovieObj[m]['videoprofile']];
							              }
							              setDisplay(videoprofileMap[o_fileMovieObj[m]['videoprofile']],'videoprofile'+cc,'d_videoprofile'+cc);	
							              
							              if(o_fileMovieObj[m]['bitratetype']!= null){
							                 document.getElementById("o_bitratetype"+cc).innerHTML = bitratetypeMap[o_fileMovieObj[m]['bitratetype']];
							              }
							               setDisplay(bitratetypeMap[o_fileMovieObj[m]['bitratetype']],'bitratetype'+cc,'d_bitratetype'+cc);	
							               	
							              if(o_fileMovieObj[m]['audiotrack'] != null){
							                 document.getElementById("o_audiotrack"+cc).innerHTML = audiotypeMap[o_fileMovieObj[m]['audiotrack']];
							              }
							               setDisplay(audiotypeMap[o_fileMovieObj[m]['audiotrack']],'audiotrack'+cc,'d_audiotrack'+cc);	
							            
							              if(o_fileMovieObj[m]['numberofframes'] !=null){
							                 document.getElementById("o_numberofframes"+cc).innerHTML = o_fileMovieObj[m]['numberofframes'];
							              }
							               setDisplay(o_fileMovieObj[m]['numberofframes'],'numberofframes'+cc,'d_numberofframes'+cc); 
							             
							             if(o_fileMovieObj[m]['frameheight'] != null){
							                document.getElementById("o_frameheight"+cc).innerHTML = o_fileMovieObj[m]['frameheight'];
							             }
							             setDisplay(o_fileMovieObj[m]['frameheight'],'frameheight'+cc,'d_frameheight'+cc);
							             
							             if(o_fileMovieObj[m]['framewidth'] != null){
							               document.getElementById("o_framewidth"+cc).innerHTML = o_fileMovieObj[m]['framewidth'];
							             }
							               setDisplay(o_fileMovieObj[m]['framewidth'],'framewidth'+cc,'d_framewidth'+cc);
							               
							              if( o_fileMovieObj[m]['framerate'] != null){
							                document.getElementById("o_framerate"+cc).innerHTML = o_fileMovieObj[m]['framerate'];
							              }
							                setDisplay(o_fileMovieObj[m]['framerate'],'framerate'+cc,'d_framerate'+cc);		
							              
							              if(o_fileMovieObj[m]['sourcedrmtype'] != null){
							                document.getElementById("o_sourcedrmtype"+cc).innerHTML = sourcedrmtypeCalculate(o_fileMovieObj[m]['sourcedrmtype']);
							              }
							               setDisplay(sourcedrmtypeCalculate(o_fileMovieObj[m]['sourcedrmtype']),'sourcedrmtype'+cc,'d_sourcedrmtype'+cc);	
							               
							              if(o_fileMovieObj[m]['destdrmtype'] != null){
							                document.getElementById("o_destdrmtype"+cc).innerHTML = destdrmtypeCalculate(o_fileMovieObj[m]['destdrmtype']);
							              }
							               setDisplay(destdrmtypeCalculate(o_fileMovieObj[m]['destdrmtype']),'destdrmtype'+cc,'d_destdrmtype'+cc);	
							               	
							              if(o_fileMovieObj[m]['screenformat'] != null){
							                document.getElementById("o_screenformat"+cc).innerHTML = screenformatCalculate(o_fileMovieObj[m]['screenformat']);
							              }
							               setDisplay(screenformatCalculate(o_fileMovieObj[m]['screenformat']),'screenformat'+cc,'d_screenformat'+cc);	
							              
							              if(o_fileMovieObj[m]['closedcaptioning'] != null){
							                document.getElementById("o_closedcaptioning"+cc).innerHTML = closedcaptioningCalculate(o_fileMovieObj[m]['closedcaptioning']);
							              }
							               setDisplay(closedcaptioningCalculate(o_fileMovieObj[m]['closedcaptioning']),'closedcaptioning'+cc,'d_closedcaptioning'+cc);
							               
							              if(o_fileMovieObj[m]['domain'] != null){
							                document.getElementById("o_domain"+cc).innerHTML = domainCalculate(o_fileMovieObj[m]['domain']);
							              }
							                setDisplay(domainCalculate(o_fileMovieObj[m]['domain']),'domain'+cc,'d_domain'+cc);	
							              
							              if(o_fileMovieObj[m]['hotdegree'] != null){
							                document.getElementById("o_hotdegree"+cc).innerHTML = hotdegreeCalculate(o_fileMovieObj[m]['hotdegree']);
							              }
							               setDisplay(hotdegreeCalculate(o_fileMovieObj[m]['hotdegree']),'hotdegree'+cc,'d_hotdegree'+cc);	
							              
							              if(o_fileMovieObj[m]['duration'] != null){
							                  document.getElementById("o_duration"+cc).innerHTML = durationCalculate(o_fileMovieObj[m]['duration']);
							              }
							               setDisplay(durationCalculate(o_fileMovieObj[m]['duration']),'duration'+cc,'d_duration'+cc);
							              
							              if(o_fileMovieObj[m]['fileurl'] != null){
							                  document.getElementById("o_fileurl"+cc).innerHTML = o_fileMovieObj[m]['fileurl'];
							              }
							               setDisplay(o_fileMovieObj[m]['fileurl'],'fileurl'+cc,'d_fileurl'+cc);	
				                }
				            }    
                        }
                     cc++;
               }
         }
   }   
 
    //比较修改前和修改后字段区别的js文件
    function setDisplay(oldValue,newValueId, labelId)
    {
		 var o_labelId = "o" + labelId.substring(1);
		 var newValue = document.getElementById(newValueId).innerHTML;
		 if(oldValue==null){
			oldValue='';
		 }
	     if(newValue!=oldValue){
	       if (oldValue != null && oldValue == ""){
		          oldValue = oldValue + "";
		          if (oldValue == ""){
		   	          //document.getElementById(labelId).style.color = "#999";
		   	          document.getElementById(newValueId).style.color = "#339900";
		          }else {
		             document.getElementById(labelId).style.display='block';
		          }      
	        }else{
	            document.getElementById(labelId).style.display='block';
	        }
		 }
    }
   
   
   function seriesflagCalculates(keyvalue)
   {
   	   	var seriesflag = keyvalue;
   		if(seriesflag == "0")
   		{
   			return $res_entry("label.cms.content.program.seriesflag0");
   		}else if(seriesflag == "1")
   		{
   			return $res_entry("label.cms.content.program.seriesflag1");
   		}else
   		{
   			return "";
   		}
   }
    
       function sourcetypeCalculates(keyvalue)
   {
   		var sourcetype = keyvalue;
   		if(sourcetype == "1")
   		{
   			return "VOD";
   		}else if(sourcetype == "5")
   		{
   			return "Advertisement";
   		}else
   		{
   			return "";
   		}
   }
   
      //内容查看页面显示"业务平台"
   function platformCalculates(keyvalue)
   {
   		var platform = keyvalue;
   		if(platform == "1")
   		{
   			return $res_entry("label.cms.cpposition.PC");
   		}else if(platform == "2")
   		{
   			return $res_entry("label.cms.cpposition.CE");
   		}else if(platform == "3")
   		{
   			return "IPTV";
   		}else if(platform == "4")
   		{
   			return $res_entry("label.cms.cpposition.4");
   		}else
   		{
   			return "";
   		}
   }
   
   function isvalidCalculats(keyvalue)
   {
   		var isvalid = keyvalue;
   		if(isvalid == "0")
   		{
   			return $res_entry("msg.info.basicdata.022");
   		}else if(isvalid == "1")
   		{
   			return $res_entry("msg.info.basicdata.023");
   		}else
   		{
   			return "";
   		}
   }
    
    var provinceMaps ={};
    function countrytypeCalculates(keyvalue)
   {
         callUrl("sysCode/getSysCodeMapByCodekey.ssm",'[cms_content_actorcountrytype]','provinceMaps',true);    //地区类型
   		 provinceMaps=provinceMaps.rtnValue;
   		 return provinceMaps[keyvalue];
   }
   
   
   var countryMaps = {};
    //内容查看页面显示"区域"
   function providCalculates(keyvalue)
   {
        callUrl('ucpBasicLS/getUsysProvinceMap.ssm',null,'countryMaps',true);	//区域
   		countryMaps = countryMaps.rtnValue;
   		for(var i =0;i<countryMaps["valueList"].length;i++) 
		{ 
			if(keyvalue==countryMaps["valueList"][i])
			{
				return countryMaps["textList"][i];
				
			}
		}
   } 
   
   var feetypeMaps = {};
   //内容查看页面显示"交易类别"
   function tradetypeCalculates(keyvalue)
   {
         callUrl("sysCode/getSysCodeMapByCodekey.ssm",'[cms_content_feetype]','feetypeMaps',true);      //交易属性
   		 feetypeMaps=feetypeMaps.rtnValue;
   		 return feetypeMap[keyvalue];
   }
   
    //内容查看页面显示"推荐星级"
   function recommendstartCalculates(keyvalue)
   {
   		var recommendstart = keyvalue;
	   	if(recommendstart>=1 && recommendstart<3){
	        return $res_entry("msg.info.content.009");
	    }else if(recommendstart>=3 && recommendstart<5){
	        return $res_entry("msg.info.content.010");
	    }else if(recommendstart>=5 && recommendstart<7){
	        return $res_entry("msg.info.content.011");
	    }else if(recommendstart>=7 && recommendstart<9){
	        return $res_entry("msg.info.content.012");
	    }else if(recommendstart>=9 && recommendstart<=10){
	        return $res_entry("msg.info.content.013");
	    }else{
	        return "";
	    }
   }
   
    //内容查看页面显示"拷贝保护"
   function macrovisionCalculates(keyvalue)
   {
   		var macrovision = keyvalue;
   		if(macrovision == "0")
   		{
   			return $res_entry("label.cms.channel.info.macrovision0");
   		}else if(macrovision == "1")
   		{
   			return $res_entry("label.cms.channel.info.macrovision1");
   		}else
   		{
   			return "";
   		}
   }
   
   var languageMaps ={};
   //内容查看页面显示"语种"
   function languageCalculates(keyvalue)
   {
        callUrl('sysCode/getSysCodeMapByCodekey.ssm','["cms_content_singerlang"]','languageMaps',true); //语言
	    languageMaps = languageMaps.rtnValue;
   		return languageMaps[keyvalue];
   }
   
   var limitlevelMaps = {};
   //内容查看页面显示"内容级别"
   function ratingCalculates(keyvalue)
   {
        callUrl('sysCode/getSysCodeMapByCodekey.ssm','[cms_content_contentlevel]','limitlevelMaps',true); //内容级别
   		limitlevelMaps = limitlevelMaps.rtnValue;
   		return limitlevelMaps[keyvalue];
   }
   

	var cpid ="";
	var licenseid="";
	var ucpBasic={};
	var programid = "";
	function showCmsProgramBuf(program)
	{
		cpid = program["cpid"];
		programid = program["programid"];
		licenseid = program["licenseid"];
		callUrl('ucpBasicLS/getUcpBasicByCPIDAndCptype.ssm','[cpid,1]','ucpBasic',true);
		ucpBasic = ucpBasic.rtnValue;
		document.getElementById("cpid").innerText=ucpBasic["cpcnshortname"];
		callUrl('ucpBasicLS/getUcpBasicByCPIDAndCptype.ssm','[licenseid,2]','ucpBasic',true);
		ucpBasic = ucpBasic.rtnValue;
		document.getElementById("licenseid").innerText=ucpBasic["cpcnshortname"];
		
		for(var i =0;i<programtypeMap["codeValue"].length;i++) 
		{ 
			if(program["programtype"]==programtypeMap["codeValue"][i])
			{
				document.getElementById("programtype").innerText=programtypeMap["codeText"][i];
				break;
			}
		}
		
		callSid('getCmsProgram');     //查询修改前的Program对象
	}
	
	function show(program){
	    
	    cpid = program["cpid"];
		programid = program["programid"];
		licenseid = program["licenseid"];
		callUrl('ucpBasicLS/getUcpBasicByCPIDAndCptype.ssm','[cpid,1]','ucpBasic',true);
		ucpBasic = ucpBasic.rtnValue;
		document.getElementById("o_cpid").innerText = ucpBasic["cpcnshortname"];
		callUrl('ucpBasicLS/getUcpBasicByCPIDAndCptype.ssm','[licenseid,2]','ucpBasic',true);
		ucpBasic = ucpBasic.rtnValue;
		document.getElementById("o_licenseid").innerText=ucpBasic["cpcnshortname"];
		
		for(var i =0;i<programtypeMap["codeValue"].length;i++) 
		{ 
			if(program["programtype"] == programtypeMap["codeValue"][i])
			{
				document.getElementById("o_programtype").innerText=programtypeMap["codeText"][i];
				break;
			}
		}
	     
	     if(program['seriesflag'] != null){
	          document.getElementById("o_seriesflag").innerText = seriesflagCalculates(program['seriesflag']);
	     }
	     
	     if(program['sourcetype'] != null){
	         document.getElementById("o_sourcetype").innerText = sourcetypeCalculates(program['sourcetype']);
	     }
	     if(program['platform'] != null){
	       document.getElementById("o_platform").innerText = platformCalculates(program['platform']);
	     }
	     if(program['isvalid'] != null){
	       document.getElementById("o_isvalid").innerText = isvalidCalculats(program['isvalid']);
	     }
	     if(program['effectivedate'] != null){
	       document.getElementById("o_effectivedate").innerText = getTime(program['effectivedate']);
	     }
	     
	     if(program['expirydate'] != null){
	       document.getElementById("o_expirydate").innerText = getTime(program['expirydate']);
	     }
	     
	     if(program['llicensofflinetime']!=null){
	        document.getElementById("o_llicensofflinetime").innerText = getTime(program['llicensofflinetime']);
	     }
	     if(program['deletetime'] != null){
	       document.getElementById("o_deletetime").innerText = getTime(program['deletetime']);
	     }
	     
	     if(program['countrytype'] != null){
	        document.getElementById("o_countrytype").innerText = countrytypeCalculates(program['countrytype']);
	     }
	     
	     if(program['provid'] != null){
	       document.getElementById("o_provid").innerText = providCalculates(program['provid']);
	     }
	     
	     if(program['tradetype'] != null ){
	      document.getElementById("o_tradetype").innerText = tradetypeCalculates(program['tradetype']);
	     
	     }
	     if(program['recommendstart'] != null){
	       document.getElementById("o_recommendstart").innerText = recommendstartCalculates(program['recommendstart']);
	     }
	     if(program['orgairdate'] != null){
	      document.getElementById("o_orgairdate").innerText = getTime(program['orgairdate']);
	     }
	     
	     if(program['macrovision'] != null){
	       document.getElementById("o_macrovision").innerText = macrovisionCalculates(program['macrovision']);
	     }
	     if(program['licensingstart'] != null){
	       document.getElementById("o_licensingstart").innerText = getTime(program['licensingstart']);
	     }
	      if(program['licensingend'] != null){
	       document.getElementById("o_licensingend").innerText = getTime(program['licensingend']);
	     }
	     
	     if(program['language']!=null){
	       document.getElementById("o_language").innerText = languageCalculates(program['language']);
	     }
	     
	     if(program['subtitlelanguage'] != null){
	       document.getElementById("o_subtitlelanguage").innerText = languageCalculates(program['subtitlelanguage']);
	     }
	     if(program['dubbinglanguage'] != null){
	       document.getElementById("o_dubbinglanguage").innerText = languageCalculates(program['dubbinglanguage']);
	     }  
	      if(program['rating'] != null){
	       document.getElementById("o_rating").innerText = ratingCalculates(program['rating']);
	     }
	
		setDisplay(program['namecn'],'namecn','d_namecn');
		setDisplay(program['originalnamecn'],'originalnamecn','d_originalnamecn');
		setDisplay(document.getElementById("o_cpid").innerText,'cpid','d_cpid');
		setDisplay(document.getElementById("o_licenseid").innerText,'licenseid','d_licenseid');
		setDisplay(document.getElementById("o_programtype").innerText,'programtype','d_programtype');	
		setDisplay(program['ordernumber'],'ordernumber','d_ordernumber');
		setDisplay(seriesflagCalculates(program['seriesflag']),'seriesflag','d_seriesflag');
		setDisplay(document.getElementById("o_sourcetype").innerText,'sourcetype','d_sourcetype');
		setDisplay(program['genre'],'genre','d_genre');
		setDisplay(program['contenttypeid'],'contenttypeid','d_contenttypeid');
		setDisplay(program['duration'],'duration','d_duration');
		setDisplay(document.getElementById("o_isvalid").innerText,'isvalid','d_isvalid');
		setDisplay(program['sortname'],'sortname','d_sortname');
		setDisplay(program['searchname'],'searchname','d_searchname');
		setDisplay(program['contentversion'],'contentversion','d_contentversion');
		setDisplay(document.getElementById("o_platform").innerText,'platform','d_platform');
		setDisplay(program['keywords'],'keywords','d_keywords');
		setDisplay(program['copyrightcn'],'copyrightcn','d_copyrightcn');
		setDisplay(document.getElementById("o_effectivedate").innerText,'effectivedate','d_effectivedate');
		setDisplay(document.getElementById("o_expirydate").innerText,'expirydate','d_expirydate');
		setDisplay(program['exclusivevalidity'],'exclusivevalidity','d_exclusivevalidity');
		setDisplay(program['copyrightexpiration'],'copyrightexpiration','d_copyrightexpiration');
		setDisplay(document.getElementById("o_llicensofflinetime").innerText,'llicensofflinetime','d_llicensofflinetime');
		setDisplay(document.getElementById("o_deletetime").innerText,'deletetime','d_deletetime');
		setDisplay(countrytypeCalculates(program['countrytype']),'countrytype','d_countrytype');
		setDisplay(program['copyproperty'],'copyproperty','d_copyproperty');
		setDisplay(program['country'],'country','d_country');
		setDisplay(program['importlisence'],'importlisence','d_importlisence');
		setDisplay(program['releaselisence'],'releaselisence','d_releaselisence');
		setDisplay(document.getElementById("o_provid").innerText,'provid','d_provid');
		setDisplay(document.getElementById("o_tradetype").innerText,'tradetype','d_tradetype');
		setDisplay(program['contenttags'],'contenttags','d_contenttags');
		setDisplay(program['pricetaxin'],'pricetaxin','d_pricetaxin');
		setDisplay(program['newcomedays'],'newcomedays','d_newcomedays');
		setDisplay(program['remainingdays'],'remainingdays','d_remainingdays');
		setDisplay(document.getElementById("o_recommendstart").innerText,'recommendstart','d_recommendstart');
		setDisplay(document.getElementById("o_orgairdate").innerText ,'orgairdate','d_orgairdate');
		setDisplay( document.getElementById("o_macrovision").innerText,'macrovision','d_macrovision');
		setDisplay(document.getElementById("o_licensingstart").innerText,'licensingstart','d_licensingstart');
		setDisplay(document.getElementById("o_licensingend").innerText,'licensingend','d_licensingend');
		setDisplay(program['viewpoint'],'viewpoint','d_viewpoint');
		setDisplay(program['awards'],'awards','d_awards');
		setDisplay(program['actors'],'actors','d_actors');
	    setDisplay(program['writers'],'writers','d_writers');
	    setDisplay(program['directors'],'directors','d_directors');
	    setDisplay(program['productcorp'],'productcorp','d_productcorp');
	    setDisplay(program['producer'],'producer','d_producer');
	    setDisplay(program['publisher'],'publisher','d_publisher');
		setDisplay(program['title'],'title','d_title');
	    setDisplay(program['releaseyear'],'releaseyear','d_releaseyear');
		setDisplay(document.getElementById("o_language").innerText,'language','d_language');
		setDisplay(document.getElementById("o_subtitlelanguage").innerText,'subtitlelanguage','d_subtitlelanguage');
		setDisplay(document.getElementById("o_dubbinglanguage").innerText,'dubbinglanguage','d_dubbinglanguage');
		setDisplay(document.getElementById("o_rating").innerText,'rating','d_rating');
		setDisplay(program['desccn'],'desccn','d_desccn');
		showMovieInfo(); //显示视频信息
	}
	
	var rowIndex = 1;
 function makeRows(){
       
       var tbody = document.getElementById('tableObj');
       
        //子内容信息列表
        newTr11=tbody.insertRow();
		var th=document.createElement('th');
		th.setAttribute("width","25%");
        th.innerText =$res_entry("msg.info.content.207"); //子内容名称
	    newTr11.appendChild(th);
	    var td=document.createElement('td');
		td.setAttribute("colSpan","4");
		td.innerHTML='<label  >'+ rowIndex +'</label>';
	    newTr11.appendChild(td);
       
       //子内容名称
        newTr=tbody.insertRow();
		var th=document.createElement('th');
		th.setAttribute("width","25%");
        th.innerText =$res_entry("label.subcnt.programname"); //子内容名称
	    newTr.appendChild(th);
	    var td=document.createElement('td');
		td.setAttribute("colSpan","4");
		td.innerHTML='<label  id="sonnamecn'+cc+'"></label><div class="red" id="d_sonnamecn'+ cc +'" style="display: none;"><label id="o_sonnamecn'+cc+'"></label></div>';
	    newTr.appendChild(td);
        
    
        //媒体类型
        newTr0 = tbody.insertRow();
        var th0 = document.createElement('th');
        th0.setAttribute("width","25%");
        th0.innerText = $res_entry("label.cms.content.mediatype");  //媒体类型
        newTr0.appendChild(th0);
        var td0=document.createElement('td');
        td0.setAttribute("width","25%");
        var str = '<td><label  id="movietype'+cc+'"></label></td><div class="red" id="d_movietype'+ cc +'" style="display: none;"><label id="o_movietype'+cc+'"></label></div>';
        td0.innerHTML=str;
	    newTr0.appendChild(td0);
        //封装格式
        var th1=document.createElement('th');
		th1.setAttribute("width","25%");
		th1.innerText = $res_entry("label.cms.channel.info.systemlayer"); //封装格式
	    newTr0.appendChild(th1);
		var td1=document.createElement('td');
		td1.innerHTML='<td><label  id="systemlayer'+cc+'"></label><div class="red" id="d_systemlayer'+ cc +'" style="display: none;"><label id="o_systemlayer'+cc+'"></label></div></td>';
	    newTr0.appendChild(td1);
        
            
        //视频编码格式
		newTr1=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("msg.info.content.199"); //视频编码格式
	    newTr1.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="videotype'+cc+'"></label><div class="red" id="d_videotype'+ cc +'" style="display: none;"><label id="o_videotype'+cc+'"></label></div></td>';
		newTr1.appendChild(td0);
		//音频编码格式
		th1=document.createElement('th');
		th1.innerText = $res_entry("msg.info.content.205"); //音频编码格式
	    newTr1.appendChild(th1);
		td1=document.createElement('td');
	    td1.innerHTML='<td ><label  id="audiotrack'+cc+'"></label><div class="red" id="d_audiotrack'+ cc +'" style="display: none;"><label id="o_audiotrack'+cc+'"></label></div></td>';
	    newTr1.appendChild(td1);
        
        //分辨率类型
		newTr2=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("msg.info.content.200"); //分辨率类型
	    newTr2.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="resolution'+cc+'"></label><div class="red" id="d_resolution'+ cc +'" style="display: none;"><label id="o_resolution'+cc+'"></label></div></td>';
		newTr2.appendChild(td0);
		//视频规格
		th1=document.createElement('th');
		th1.innerText = $res_entry("label.cms.channel.info.videoprofile"); //视频规格
	    newTr2.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="videoprofile'+cc+'"></label><div class="red" id="d_videoprofile'+ cc +'" style="display: none;"><label id="o_videoprofile'+cc+'"></label></div></td>';
	    newTr2.appendChild(td1);
        
        //码流速率
		newTr3=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("label.cms.channel.info.bitratetype"); //码流速率
	    newTr3.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="bitratetype'+cc+'"></label><div class="red" id="d_bitratetype'+ cc +'" style="display: none;"><label id="o_bitratetype'+cc+'"></label></div></td>';
		newTr3.appendChild(td0);
		//声道类型
		th1=document.createElement('th');
		th1.innerText = $res_entry("label.cms.content.movie.audiotype"); //声道类型
	    newTr3.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="audiotype'+cc+'"></label><div class="red" id="d_audiotype'+ cc +'" style="display: none;"><label id="o_audiotype'+cc+'"></label></div></td>';
	    newTr3.appendChild(td1);
        
        
        //帧数
		newTr4=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("msg.info.content.206"); //帧数
	    newTr4.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="numberofframes'+cc+'"></label><div class="red" id="d_numberofframes'+ cc +'" style="display: none;"><label id="o_numberofframes'+cc+'"></label></div></td>';
		newTr4.appendChild(td0);
		//帧高
		th1=document.createElement('th');
		th1.innerText = $res_entry("th.app.cms.template.framehight"); //帧高
	    newTr4.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="frameheight'+cc+'"></label><div class="red" id="d_frameheight'+ cc +'" style="display: none;"><label id="o_frameheight'+cc+'"></label></div></td>';
	    newTr4.appendChild(td1);
	    
	    //帧宽
		newTr5=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("th.app.cms.template.framewidth"); //帧宽
	    newTr5.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="framewidth'+cc+'"></label><div class="red" id="d_framewidth'+ cc +'" style="display: none;"><label id="o_framewidth'+cc+'"></label></div></td>';
		newTr5.appendChild(td0);
		//帧率(fps)
		th1=document.createElement('th');
		th1.innerText = $res_entry("th.app.cms.template.framerate"); //帧率(fps)
	    newTr5.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="framerate'+cc+'"></label><div class="red" id="d_framerate'+ cc +'" style="display: none;"><label id="o_framerate'+cc+'"></label></div></td>';
	    newTr5.appendChild(td1);
	    
	    //源文件是否有版权保护
		newTr6=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("label.cms.content.movie.sourcedrmtype");  //源文件是否有版权保护
	    newTr6.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="sourcedrmtype'+cc+'"></label><div class="red" id="d_sourcedrmtype'+ cc +'" style="display: none;"><label id="o_sourcedrmtype'+cc+'"></label></div></td>';
		newTr6.appendChild(td0);
		//目标文件是否有版权保护
		th1=document.createElement('th');
		th1.innerText = $res_entry("label.cms.content.movie.destdrmtype");  //目标文件是否有版权保护
	    newTr6.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="destdrmtype'+cc+'"></label><div class="red" id="d_destdrmtype'+ cc +'" style="display: none;"><label id="o_destdrmtype'+cc+'"></label></div></td>';
	    newTr6.appendChild(td1);
	    
	    //屏幕格式
		newTr7=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = $res_entry("label.cms.content.movie.screenformat"); //屏幕格式
	    newTr7.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="screenformat'+cc+'"></label><div class="red" id="d_screenformat'+ cc +'" style="display: none;"><label id="o_screenformat'+cc+'"></label></div></td>';
		newTr7.appendChild(td0);
		//字幕标志
		th1=document.createElement('th');
		th1.innerText = $res_entry("label.cms.content.movie.closedcaptioning"); //字幕标志
	    newTr7.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="closedcaptioning'+cc+'"></label><div class="red" id="d_closedcaptioning'+ cc +'" style="display: none;"><label id="o_closedcaptioning'+cc+'"></label></div></td>';
	    newTr7.appendChild(td1);
	    
	    //发布到CDN的域标
		newTr8=tbody.insertRow();
		th0=document.createElement('th');
	    th0.innerText = "发布到CDN的域标"; //发布到CDN的域标
	    newTr8.appendChild(th0);
	    td0=document.createElement('td');
		td0.innerHTML='<td ><label  id="domain'+cc+'"></label><div class="red" id="d_domain'+ cc +'" style="display: none;"><label id="o_domain'+cc+'"></label></div></td>';
		newTr8.appendChild(td0);
		//发布到CDN的热度
		th1=document.createElement('th');
		th1.innerText = $res_entry("label.cms.content.movie.hotdegree"); //发布到CDN的热度
	    newTr8.appendChild(th1);
		td1=document.createElement('td');
		td1.innerHTML='<td ><label  id="hotdegree'+cc+'"></label><div class="red" id="d_hotdegree'+ cc +'" style="display: none;"><label id="o_hotdegree'+cc+'"></label></div></td>';
	    newTr8.appendChild(td1);
       
        //播放时长
        newTr9=tbody.insertRow();
	    th0=document.createElement('th');
		th0.setAttribute("width","25%");
        th0.innerText =$res_entry("label.cms.content.playtime"); //播放时长
	    newTr9.appendChild(th0);
	    td0=document.createElement('td');
		//td0.setAttribute("colSpan","4");
		td0.innerHTML='<td><label  id="duration'+cc+'"></label><div class="red" id="d_duration'+ cc +'" style="display: none;"><label id="o_duration'+cc+'"></label></div></td>';
	    newTr9.appendChild(td0);
	    
	    //文件地址
        newTr10=tbody.insertRow();
		th0=document.createElement('th');
		th0.setAttribute("width","25%");
        th0.innerText =$res_entry("label.fileurl"); //文件地址
	    newTr10.appendChild(th0);
	    td0=document.createElement('td');
		td0.setAttribute("colSpan","4");
	    td0.innerHTML='<td><label id="fileurl'+cc+'"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id="movieindex'+cc+'"  style="display:none"></label><a href="#" id="btn'+cc+'" onclick=showMovieBuf("movieindex"+'+cc+')>预览</a>&nbsp;&nbsp;&nbsp;&nbsp;<div class="red" id="d_fileurl'+cc+'" style="display: none;"><label id="o_fileurl'+cc+'"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id="o_movieindex'+cc+'"  style="display:none"></label><a href="#" id="btn'+cc+'" onclick=showMovie("o_movieindex"+'+cc+')>预览</a>&nbsp;&nbsp;&nbsp;</div></td>';
		//td0.innerHTML='<td><label  id="fileurl'+cc+'"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id="movieindex'+cc+'"  style="display:none"></label><a href="#" id="btn'+cc+'" onclick=showMovie("fileurl"+'+cc+')>预览</a>&nbsp;&nbsp;&nbsp;<a href="#" id="btn1'+cc+'"  onclick=showVideoAudioBuf('+ cc +')>查看视音频信息</a>&nbsp;<div class="red" id="d_fileurl'+cc+'" style="display: none;"><label id="o_fileurl'+cc+'"></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label id="o_movieindex'+cc+'"  style="display:none"></label><a href="#" id="btn'+cc+'" onclick=showMovie("o_fileurl"+'+cc+')>预览</a>&nbsp;&nbsp;&nbsp;<a href="#" id="btn1'+cc+'" onclick=showVideoAudio('+ cc +')>查看视音频信息</a></div></td>';
	    newTr10.appendChild(td0);
	    
	    rowIndex ++;
   }
   
   //预览视频
    function showMovie(id){
		 var o_movieindex = document.getElementById(id).innerHTML;
		 var sURL = "../movie/movieView.html?movieindex=" + o_movieindex +"&type=2"; 
         var sFeatures = "height=580,width=700,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(sURL, "", sFeatures);
	}
	
	 function showMovieBuf(id){
		 var movieindex = document.getElementById(id).innerHTML;
		 var sURL = "../movie/movieView.html?movieindex=" + movieindex+"&type=1" ; 
         var sFeatures = "height=580,width=700,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(sURL, "", sFeatures);
	}
	
	//查看缓存表里面视音频信息
	function showVideoAudioBuf(c)
    {
        var movieindex = document.getElementById("movieindex"+c).innerText;
		var sourcetype = 3;      //视频文件来源：子内容
		var sURL = "../basic/mediaInfoView.html?movieindex=" + movieindex+"&sourcetype="+sourcetype +"&type=2"; 
        var sFeatures = "height=550,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
        window.open(sURL, "", sFeatures);
		
    } 
    //查看以前的视音频信息
    function showVideoAudio(c)
    {
        var movieindex = document.getElementById("o_movieindex"+c).innerText;
		var sourcetype = 3;      //视频文件来源：子内容
		var sURL = "../basic/mediaInfoView.html?movieindex=" + movieindex+"&sourcetype="+sourcetype+"&type=1"; 
        var sFeatures = "height=550,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
        window.open(sURL, "", sFeatures);
    }   


  var opopinion="";
	var wkfwParam = [];  //单条审核  wkfwParam工作流参数：内容ID、工单任务号、工单流程号、工单节点名称、审核意见、审核结果
	function apply(){
		clearAllMsgWide();
		opopinion=$("#opopinion").val();
	    
	    wkfwParam = [];
	    wkfwParam[0] = $("#programindex").val();   //内容index
		wkfwParam[1] = _para.taskid;      //工单任务号
		wkfwParam[2] = _para.processId;   //工单流程号
		wkfwParam[3] = _para.nodeName;    //工单节点名称
		
		var result = SSB_RadioGroup.radioGroupObjs["txtResult"].getValue();
         
		if(result == 0){
			if(""==opopinion.trim()){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.133"));
				focusAndSelect("opopinion",true);
				return false;
			}
			if(!checkUnallowedcharWide("opopinion",true)){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.134"));
				focusAndSelect("opopinion",true);
				return false;
			}
			if (confirm($res_entry("msg.info.content.003"))){
			       wkfwParam[4] = opopinion;
			       wkfwParam[5] = $res_entry("label.WorkFlow.Fail");
                   callSid('modAudit',wkfwParam);
            }else{
                return false;
            }
		}else if(result == 1){
			if (confirm($res_entry("msg.info.content.004"))){
			       wkfwParam[4] = $res_entry("label.WorkFlow.Pass");
			       wkfwParam[5] = $res_entry("label.WorkFlow.Pass");
                   callSid('modAudit',wkfwParam);
            }else{
                return false;
            }
		}
	}
	
	function nopassReasonFunc(){
          var txtReasonValue = SSB_RadioGroup.radioGroupObjs["txtReason"].getValue();
	     if(txtReasonValue==1){
	     $("#txtopopinion_msg").text("");
	           document.getElementById("opopinion").value="";
	           document.getElementById("opopinion").disabled="";
	           document.getElementById("opopinion").value=$res_entry("msg.info.content.005");
	          document.getElementById("opopinion").disabled="disabled";
	         
	     }else if(txtReasonValue==2){
	       $("#txtopopinion_msg").text("");
	           document.getElementById("opopinion").value="";
	            document.getElementById("opopinion").disabled="";
	          document.getElementById("opopinion").value=$res_entry("msg.info.content.006");
	          document.getElementById("opopinion").disabled="disabled";
	     }else if(txtReasonValue==3){
	       $("#txtopopinion_msg").text("");
	          document.getElementById("opopinion").value="";
	          document.getElementById("opopinion").disabled="";
	     }
   }
	
	
	function txtResultFunc(){
          var txtRadioValue = SSB_RadioGroup.radioGroupObjs["txtResult"].getValue();
	     if(txtRadioValue == 0){
	         $("#txtopopinion_msg").text("");
	          document.getElementById("reasonSpan").innerHTML = $res_entry("msg.info.content.195");
	         document.getElementById("nopassReason").style.display="block";
	         document.getElementById("oppniontr").style.display="block";
	         nopassReasonFunc();
	     }else if(txtRadioValue == 1){
	           document.getElementById("oppniontr").style.display="none";
	           $("#txtopopinion_msg").text("");
	           document.getElementById("nopassReason").style.display="none";
	           document.getElementById("opopinion").disabled="";
	           document.getElementById("opopinion").value="";
	     }
   }
	
	/**
	 * 特殊字符
	 * @param {String} input_id
	 * @param (boolean) bool
	 * unallowed character, and returns false;
	 */
	function checkUnallowedcharWide(input_id,bool){
		var pattern = /[\\|\'\"\<\>]/;
		if(pattern.test($('#'+input_id).val())){
			focusAndSelect(input_id,bool);
			return false;
		}else{
			return true;	
		}
	}
	
	/**
	 * 选中当前错误数据
	 * @param {String} input_id
	 * @param (boolean) bool
	 * Select the current error data 
	 */
	function focusAndSelect(input_id,bool){
		bool?$('#'+input_id).one('click',function(){$(this).focus();$(this).select();}):0;
		$('#'+input_id).click()
	}
    
	
	/**
	 * 在做提交前清空所有的提示信息
	 */
	function clearAllMsgWide(){
		$("label[@id*='_msg']").text("");
	}
	
	 function onErrorTrips(){
      // var flag=_para.flag;
	  // if(_para.batchFlag=="yes"){
           openMsg("1:"+$res_entry("msg.info.content.007"),escape("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag),true);
      // }else if(_para.batchFlag=="no"){
      //     openMsg("1:工作流异常,请稍后重试",escape("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag),true);
      // }
   }
   
    function showResult(returnInfo) 
    {
    	var flag = _para.flag;
    	var rtnUrl = "";
    	if (_para.batchFlag=="yes")
    	{
    		rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=true&flag="+flag);
        }
    	else
    	{
    		rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=true&flag="+flag);
    	} 

	    var rtnInfo = eval(returnInfo);
        var rtnMain = rtnInfo.main;
        openMsg(rtnMain, rtnUrl, true);
    }
   
       function toBackPrePage() {
	        document.getElementById("opopinion").value="";
	    	var flag=_para.flag;
	    	if(flag==1)
	    	{
				window.location.href="program_wkfw.html?isLoad=true&flag=1";
			}
			else if(flag==2)
			{
				window.location.href="program_wkfw.html?isLoad=true&flag=2";
			}
			else if(flag==3)
			{
				window.location.href="program_wkfw.html?isLoad=true&flag=3";
			}
	}	
  	
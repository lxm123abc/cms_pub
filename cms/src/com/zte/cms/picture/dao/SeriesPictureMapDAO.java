package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.ISeriesPictureMapDAO;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class SeriesPictureMapDAO extends DynamicObjectBaseDao implements ISeriesPictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("insert seriesPictureMap starting...");
        super.insert("insertSeriesPictureMap", seriesPictureMap);
        log.debug("insert seriesPictureMap end");
    }

    public void updateSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("update seriesPictureMap by pk starting...");
        super.update("updateSeriesPictureMap", seriesPictureMap);
        log.debug("update seriesPictureMap by pk end");
    }

    public void deleteSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("delete seriesPictureMap by pk starting...");
        super.delete("deleteSeriesPictureMap", seriesPictureMap);
        log.debug("delete seriesPictureMap by pk end");
    }

    public SeriesPictureMap getSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("query seriesPictureMap starting...");
        SeriesPictureMap resultObj = (SeriesPictureMap) super.queryForObject("getSeriesPictureMap", seriesPictureMap);
        log.debug("query seriesPictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<SeriesPictureMap> getSeriesPictureMapByCond(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("query seriesPictureMap by condition starting...");
        List<SeriesPictureMap> rList = (List<SeriesPictureMap>) super.queryForList("querySeriesPictureMapListByCond",
                seriesPictureMap);
        log.debug("query seriesPictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesPictureMapListCntByCond", seriesPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesPictureMap> rsList = (List<SeriesPictureMap>) super.pageQuery("querySeriesPictureMapListByCond",
                    seriesPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesPictureMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("querySeriesPictureMapListByCond", "querySeriesPictureMapListCntByCond",
                seriesPictureMap, start, pageSize, puEntity);
    }

    /**
     * 根据连续剧Index查询其下面已发布海报的数量
     * 
     * @seriesPictureMap SeriesPictureMap对象
     * @return 该连续剧下已发布的海报的数量
     * @throws DomainServiceException ds异常
     * 
     */
    public Integer getPublishedMapBySeriesIndexCnt(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        Integer publishedCount = 0;
        log.debug("getPublishedMapBySeriesIndexCnt starting...");
        publishedCount = (Integer) super.queryForObject("getPublishedMapBySeriesIndexCnt", seriesPictureMap);
        log.debug("getPublishedMapBySeriesIndexCnt end");
        return publishedCount;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesPictureMapSynListCntByCond", seriesPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesPictureMap> rsList = (List<SeriesPictureMap>) super.pageQuery("querySeriesPictureMapSynListByCond",
                    seriesPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesPictureMap by condition end");
        return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesPictureTargetMapSynListCntByCond", seriesPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesPictureMap> rsList = (List<SeriesPictureMap>) super.pageQuery("querySeriesPictureTargetMapSynListByCond",
                    seriesPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesPictureMap by condition end");
        return pageInfo;
    }    
    
    @SuppressWarnings("unchecked")
    public List<SeriesPictureMap> getSeriesnormalPictureMapByCond(SeriesPictureMap seriesPictureMap) throws DAOException
    {
        log.debug("query seriesPictureMap by condition starting...");
        List<SeriesPictureMap> rList = (List<SeriesPictureMap>) super.queryForList("getSeriesPictureTargetMapSynListByCond",
                seriesPictureMap);
        log.debug("query seriesPictureMap by condition end");
        return rList;
    }
}
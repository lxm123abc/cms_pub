package com.zte.cms.castrolemap.crmsync.ls;

import com.zte.cms.castrolemap.crmsync.model.CrmTargetSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICrmTargetSyncLS
{
    /**
     * 根据条件分页查询CrmPlatformSync对象 
     *
     * @param crmTargetSync CrmPlatformSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws Exception ds异常
     */
    public TableDataInfo pageInfoQuery(CrmTargetSync crmTargetSync, int start, int pageSize) throws Exception;
    
    /**
     * 人物发布到网元
     *
     * @param castindex 人物index 
     * @param targetindex 目标系统index
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCrmToTarget(Long castrolemapindex,Long targetindex, int platform ,int syncType) throws Exception;

    //删除角色与网元关联关系
    public String deleteCrmBindTarget(Long castrolemapindex,Long targetindex) throws Exception;
}

package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastPictureMapDS
{
    /**
     * 新增CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException;

    /**
     * 更新CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException;

    /**
     * 批量更新CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastPictureMapList(List<CastPictureMap> castPictureMapList) throws DomainServiceException;

    /**
     * 删除CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException;

    /**
     * 批量删除CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastPictureMapList(List<CastPictureMap> castPictureMapList) throws DomainServiceException;

    /**
     * 查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @return CastPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public CastPictureMap getCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException;

    /**
     * 根据条件查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象
     * @return 满足条件的CastPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CastPictureMap> getCastPictureMapByCond(CastPictureMap castPictureMap) throws DomainServiceException;

    /**
     * 根据条件分页查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CastPictureMap对象
     * 
     * @param castPictureMap CastPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    
    public TableDataInfo pageInfoQueryCastPicTargetMap(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException;
    
    public TableDataInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException;
}


package com.zte.cms.channel.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.basicdata.area.ls.IsysProvinceLS;
import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.categorycdn.common.CategoryConstant;
import com.zte.cms.categorycdn.ls.CategoryConstants;
import com.zte.cms.channel.common.CmsChannelConstant;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.channel.service.ICategoryChannelMapDS;
import com.zte.cms.channel.service.IChannelWkfwhisDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.channel.update.apply.IChannelUpdateApply;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CSplatformSyn.service.ICSCntPlatformSyncDS;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.CStargetSyn.service.ICSCntTargetSyncDS;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.IChannelPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.framework.base.Sequence;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysCityDS;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class CmsChannelSyncLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsChannelSyncLS
{

    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private ICmsChannelDS channelDS;
    private IUcpBasicDS basicDS;
    private IPhysicalchannelDS physicalchannelDS;
    private ITargetsystemDS targetSystemDS;
    private IsysProvinceLS provinceLS;
    private IUsysCityDS cityDS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IBatchObjectRecordDS batchObjectRecordDS;// 批处理
    private IUsysConfigDS usysConfigDS;
    private IPhysicaChannellLS physicaChannellLS;
    private ICategoryChannelMapDS categoryChannelMapDS;
    private ICmsScheduleDS cmsScheduleDS;
    private IPictureDS pictureDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private IChannelUpdateApply channelupdateApply;
    private ICntSyncRecordDS cntSyncRecordDS;
    private IServiceChannelMapDS serviceChannelMapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private IChannelPictureMapDS channelPictureMapDS;
    private static final String TEMPAREA_ID = "1"; // 临时区ID
    private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    private static final String SUCCESS = "cmsprogramtype.success";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
    private ICSCntPlatformSyncDS cscntPlatformSyncDS;
    private ICSCntTargetSyncDS CScntTargetSyncDS;
    protected IChannelWkfwhisDS channelWkfwhisDS;
    protected IScheduleWkfwhisDS scheduleWkfwhisDS;

    public final static String CNTSYNCXML = "xmlsync";
    public final static String PROGRAM_DIR = "program";

    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败

    // 内容相关对象同步任务表 objectType
    public final static int PROGRAM_OBJECTTYPE = 1;
    public final static int MOVIE_OBJECTTYPE = 2;
    private Map chaneMap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和
    // 其子内容对象

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress = "";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private ICmsStorageareaLS cmsStorageareaLS;

    public IScheduleWkfwhisDS getScheduleWkfwhisDS()
    {
        return scheduleWkfwhisDS;
    }

    public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS)
    {
        this.scheduleWkfwhisDS = scheduleWkfwhisDS;
    }

    public IChannelWkfwhisDS getChannelWkfwhisDS()
    {
        return channelWkfwhisDS;
    }

    public void setChannelWkfwhisDS(IChannelWkfwhisDS channelWkfwhisDS)
    {
        this.channelWkfwhisDS = channelWkfwhisDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public ICSCntTargetSyncDS getCScntTargetSyncDS()
    {
        return CScntTargetSyncDS;
    }

    public void setCScntTargetSyncDS(ICSCntTargetSyncDS scntTargetSyncDS)
    {
        CScntTargetSyncDS = scntTargetSyncDS;
    }

    public ICSCntPlatformSyncDS getCscntPlatformSyncDS()
    {
        return cscntPlatformSyncDS;
    }

    public void setCscntPlatformSyncDS(ICSCntPlatformSyncDS cscntPlatformSyncDS)
    {
        this.cscntPlatformSyncDS = cscntPlatformSyncDS;
    }

    public IChannelPictureMapDS getChannelPictureMapDS()
    {
        return channelPictureMapDS;
    }

    public void setChannelPictureMapDS(IChannelPictureMapDS channelPictureMapDS)
    {
        this.channelPictureMapDS = channelPictureMapDS;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }


    // 不用插入map
    public String toRecordObj(Long taskindex, Long objindex, Long destindex, String objid, String elementid,
            int objtype, int actiontype, String cpcontentid) throws DomainServiceException
    {
        // 查出物理频道
        Physicalchannel physicalchannel = new Physicalchannel();
        physicalchannel.setChannelid(objid);
        List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
        if (phylist != null && phylist.size() > 0)
        {
            for (int i = 0; i < phylist.size(); i++)
            {
                physicalchannel = (Physicalchannel) phylist.get(i);
                ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
                objectSyncRecord.setTaskindex(taskindex);
                objectSyncRecord.setObjectindex(physicalchannel.getPhysicalchannelindex());
                objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
                objectSyncRecord.setObjecttype(8);// 对象类型编号：1-service，2-Category，3-Program
                objectSyncRecord.setObjectid(physicalchannel.getPhysicalchannelid());
                objectSyncRecord.setDestindex(destindex);
                objectSyncRecord.setObjectcode(physicalchannel.getCpcontentid());// 文广code
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            }
        }
        // 查找目标系统类型
        Targetsystem targetsystem = new Targetsystem();
        targetsystem.setTargetindex(destindex);
        List list = targetSystemDS.getTargetsystemByCond(targetsystem);
        if (list != null && list.size() > 0)
        {
            targetsystem = (Targetsystem) list.get(0);
        }
        int targetsystemtype = targetsystem.getTargettype();
        if (targetsystem.getPlatform() == 2 && targetsystemtype == 3)
        {// 3.0平台,cdn

        }
        else
        {

            ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objindex);
            objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
            objectSyncRecord.setObjecttype(objtype);// 对象类型编号：1-service，2-Category，3-Program
            if (!elementid.equals(""))
            {
                objectSyncRecord.setElementid(elementid);
                objectSyncRecord.setParentid(objid);// mapping的做法
                objectSyncRecord.setObjectid(objid);
            }
            else
            {
                objectSyncRecord.setObjectid(objid);
            }
            objectSyncRecord.setDestindex(destindex);
            objectSyncRecord.setObjectcode(cpcontentid);
            try
            {
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    public String insertSyncTask(Long targetindex, String contentmngxmlurl, Long objindex, String objectid,
            int objtype, int actiontype, String cpcontentid, Long batchid, int status) throws DomainServiceException
    {

        Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setBatchid(batchid);
        cntsyncTask.setStatus(status);// 任务状态：0-初始 1-待执行 2-执行中 3-成功结束 4-失败结束
        cntsyncTask.setPriority(5);// 任务优先级：1-高 2-中 3-低
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(contentmngxmlurl);
        cntsyncTask.setDestindex(targetindex);
        cntsyncTask.setSource(1);
        cntsyncTask.setTaskindex(taskindex);
        cntsyncTask.setRetrytimes(3);
        try
        {
            cntsyncTaskDS.insertCntSyncTask(cntsyncTask);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        toRecordObj(taskindex, objindex, targetindex, objectid, "", objtype, actiontype, cpcontentid);// 记录到发布日志表,2是栏目
        // 栏目

        return null;
    }

    /** 按id得到频道信息 */
    public CmsChannel getChannel(CmsChannel channel)
    {
        CmsChannel cmsChannel = null;
        try
        {
            cmsChannel = channelDS.getCmsChannel(channel);
            if (cmsChannel == null)
            {// 对象已被删除
                return null;
            }
            else
            {
                return cmsChannel;
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        return cmsChannel;
    }
    public String publishChannelcontentTar(List<CntTargetSync> tarlist)
            throws DomainServiceException {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int actiontype = 0;// 发布操作
        ResourceMgt
                .addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List channellist = new ArrayList();
        try {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals("")) {
                return "1:获取挂载点失败";
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath)) {
                return "1:获取临时区失败";
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null) {
                return "1:获取发布XML文件FTP地址失败";
            }
            
            List pubSuccessChannels = new ArrayList();

            // 判断频道内容是否存在，判断网元状态,不用判断物理频道
            
            if (tarlist != null && tarlist.size() > 0) {
                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int j = 0; j < tarlist.size(); j++) {
                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS
                            .getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0) {
                        targetSystem = (Targetsystem) targetlist.get(0);
                        if (targetSystem.getStatus() != 0)
                        {
                            ifail++;
                            operateInfo = new OperateInfo(String.valueOf(index++),
                                    tarsync.getSyncindex().toString(), "操作失败",
                                    "目标系统状态不正确");
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;// 中断进入下一个循环
                        }
                    }
                    if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400) {
                        actiontype = 1;// 新增发布
                        tarsync.setOperresult(10);// 新增同步中
                    } else if (tarsync.getStatus() == 300
                            || tarsync.getStatus() == 500) {
                        actiontype = 2;// 修改发布
                        tarsync.setOperresult(40);// 修改同步中
                    } else {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++),
                                tarsync.getSyncindex().toString(), "操作失败",
                                "当前状态不能发布");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;// 中断进入下一个循环
                    }
                    String xmladdress = "";
                    CmsChannel channel = new CmsChannel();
                    channel.setChannelid(tarsync.getObjectid());
                    List list = channelDS.getChannelByCond(channel);
                    if (list != null && list.size() > 0) {
                        objMap.put("channellist", list);
                        Physicalchannel physicalchannel = new Physicalchannel();
                        physicalchannel.setChannelid(channel.getChannelid());
                        List phylist = physicalchannelDS
                                .getPhysicalchannelByCond(physicalchannel);
                        objMap.put("physicalchannellist", phylist);
                    }
                    CntPlatformSync cntPlatformSync = new CntPlatformSync();
                    cntPlatformSync.setSyncindex(tarlist.get(j)
                            .getRelateindex());
                    cntPlatformSync = (CntPlatformSync) cntPlatformSyncDS
                            .getCntPlatformSyncByCond(cntPlatformSync).get(0);
                    if (cntPlatformSync.getPlatform() == 2) {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2)
                                .createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5,
                                        actiontype);// 文广1是regist2是栏目

                    } else {
                      //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1)
                                .createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5,
                                        actiontype);// 文广1是regist2是栏目
                    }
                    int pristatus = 1;
                    //bms和egp的情况且有别的网元的情况,status可为0或1
                    if(((targetSystem.getTargettype()==1)||(targetSystem.getTargettype()==2))&&(tarlist.size()>0)){
                        for(int count = 0;count<tarlist.size();count++){
                            CntTargetSync cs = (CntTargetSync)tarlist.get(count);
                            if(!cs.getTargetindex().equals(targetSystem.getTargetindex())){
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if(target!=null&&target.size()>0){
                                    tsnew = (Targetsystem)target.get(0);
                                }
                                if((tsnew.getTargettype()==3)){//cdn
                                    pristatus=0;
                                }else if(tsnew.getTargettype()==1){//bms
                                    pristatus=0;
                                }
                                
                            }
                        }
                    }
                    insertSyncTask(tarsync.getTargetindex(), xmladdress,
                            ((CmsChannel) list.get(0)).getChannelindex(),
                            channel.getChannelid(), 5, actiontype,
                            ((CmsChannel) list.get(0)).getCpcontentid(),batchid,pristatus);// 调用同步任务模块DS代码

                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);

                    iSuccessed++;

                    operateInfo = new OperateInfo(String.valueOf(index++),
                            tarsync.getSyncindex().toString(), ResourceMgt
                                    .findDefaultText(SUCCESS), ResourceMgt
                                    .findDefaultText(PUBLISH_SUCCESS));
                    returnInfo.appendOperateInfo(operateInfo);
                    channellist.add(channel);
                    CntTargetSync cntTargetSync = new CntTargetSync();
                    cntTargetSync
                            .setRelateindex(cntPlatformSync.getSyncindex());
                    int status = 0;
                    int finalstatus = 0;
                    List cnttarlist = cntTargetSyncDS
                            .getCntTargetSyncByCond(cntTargetSync);
                    if (cnttarlist != null && cnttarlist.size() > 0) {// 得到这个平台下的所有关联了的网元
                        for (int count = 0; count < cnttarlist.size(); count++) {
                            cntTargetSync = (CntTargetSync) cnttarlist
                                    .get(count);
                            status = cntTargetSync.getStatus();
                            if (status == 200
                                    && (finalstatus != 500 || finalstatus != 400)) {// 200发布中
                                finalstatus = 200;
                            }
                            if (status == 500 && finalstatus != 400) {
                                finalstatus = 500;
                            }
                            if (status == 400) {
                                finalstatus = 400;
                            }
                            if (status == 0) {
                                finalstatus = 0;
                                break;
                            }

                        }

                    }
                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != finalstatus) {
                        cntPlatformSync.setStatus(finalstatus);
                        cntPlatformSyncDS
                                .updateCntPlatformSync(cntPlatformSync);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (channellist.size() == 0 || ifail == channellist.size()) {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        } else if (ifail > 0 && ifail < channellist.size()) {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        } else {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        String rtn = "success";
        return rtn;
    }




    // 获取临时区目录
    private String getTempAreaPath() throws DomainServiceException
    {
        log.debug("getTempAreaPath  start");
        String stroageareaPath = "";
        // 获取临时区路径
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            stroageareaPath = storageareaLs.getAddress(TEMPAREA_ID);// 临时区
            if (null == stroageareaPath || "".equals(stroageareaPath) || (TEMPAREA_ERRORSiGN).equals(stroageareaPath))
            {// 获取临时区地址失败
                return "";
            }
        }
        catch (Exception e)
        {
            log.error("Error occured in getTempAreaPath method");
            throw new DomainServiceException(e);
        }

        log.debug("getTempAreaPath  end");
        return stroageareaPath;
    }

    // 频道关联平台map方法
    public String preparetopublishForMap(Targetsystem tar, long objindex, String objid, int objtype, String elementid,
            String parentid) throws DomainServiceException
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        cntPlatformSync.setElementid(elementid);
        cntPlatformSync.setParentid(parentid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTargetForMap(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex, elementid,
                parentid);
        return operdesc;
    }

    public String preparetopublish(Targetsystem tar, long objindex, String objid, int objtype)
            throws DomainServiceException
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTarget(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex);
        return operdesc;
    }

    public String publishToTargetForMap(Long targetindex, long objindex, String objid, int objtype, Long relateindex,
            String elementid, String parentid) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);
        cntTargetSync.setElementid(elementid);
        cntTargetSync.setParentid(parentid);

        CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
        categoryChannelMap.setChannelid(objid);
        List listmap = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
        if (listmap != null && listmap.size() > 0)
        {
            for (int i = 0; i < listmap.size(); i++)
            {
                categoryChannelMap = (CategoryChannelMap) listmap.get(i);
                CntTargetSync tarsync = new CntTargetSync();

                tarsync.setElementid(categoryChannelMap.getChannelid());
                tarsync.setParentid(categoryChannelMap.getCategoryid());
                tarsync.setTargetindex(targetindex);
                List tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {

                }
                else
                {
                    CntTargetSync tarsy = new CntTargetSync();
                    tarsy.setObjectid(categoryChannelMap.getCategoryid());
                    tarsy.setTargetindex(targetindex);
                    List tarlistctg = cntTargetSyncDS.getCntTargetSyncByCond(tarsy);
                    if (tarlistctg != null && tarlistctg.size() > 0)
                    {
                        tarsy = cntTargetSync;
                        tarsy.setStatus(0);
                        tarsy.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                        tarsy.setRelateindex(relateindex);

                        cntTargetSyncDS.insertCntTargetSync(tarsy);
                        // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
                        // "log.channelcontent.operator",
                        // "log.target.channel.bind.add",
                        // "log.target.channel.bind.add.info",
                        // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                        resultStr.append(ResourceMgt
                                .findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                                + objid
                                + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                                + "  ");
                        sucess = true;
                    }
                }
            }

        }

        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
                // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
                // "log.channelcontent.operator",
                // "log.target.channel.bind.add",
                // "log.target.channel.bind.add.info",
                // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    public String publishToTarget(Long targetindex, long objindex, String objid, int objtype, Long relateindex)
            throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);

        Targetsystem target = new Targetsystem();
        target.setTargetindex(targetindex);
        List tarlist = targetSystemDS.getTargetsystemByCond(target);
        if (tarlist != null)
        {
            target = (Targetsystem) tarlist.get(0);
        }
        // CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
        // categoryChannelMap.setChannelid(objid);
        // List listmap = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
        // if (listmap != null && listmap.size() > 0)
        // {
        // for (int i = 0; i < listmap.size(); i++)
        // {
        // categoryChannelMap = (CategoryChannelMap) listmap.get(i);
        // CntTargetSync tarsync = new CntTargetSync();
        //
        // tarsync.setElementid(categoryChannelMap.getChannelid());
        // tarsync.setParentid(categoryChannelMap.getCategoryid());
        // tarsync.setTargetindex(targetindex);
        // List tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
        // if (tarlist != null && tarlist.size() > 0)
        // {
        //
        // }
        // else
        // {
        // CntTargetSync tarsy = new CntTargetSync();
        // tarsy.setObjectid(categoryChannelMap.getCategoryid());// 判断栏目在这个网元是上否有关联数据
        // tarsy.setTargetindex(targetindex);
        // tarsy.setObjecttype(2);
        // List tarlistctg = cntTargetSyncDS.getCntTargetSyncByCond(tarsy);
        // if (tarlistctg != null && tarlistctg.size() > 0)// 如果栏目也有发布数据
        // {
        // tarsy = tarsync;
        // tarsy.setStatus(0);
        // tarsy.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
        // tarsy.setRelateindex(relateindex);
        //
        // cntTargetSyncDS.insertCntTargetSync(tarsy);
        //
        // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
        // "log.channelcontent.operator",
        // "log.channelcontent.bindtarget",
        // "log.channelcontent.bindtarget.info",
        // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
        // resultStr.append(ResourceMgt
        // .findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
        // + objid
        // + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
        // + "  ");
        // sucess = true;
        // }
        // }
        // }
        //
        // }

        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)// 先查询有没有数据，再关联
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);

                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }

                CommonLogUtil.insertOperatorLog(objid + ", " + target.getTargetid(), "log.channelcontent.operator",
                        "log.channelcontent.bindtarget", "log.channelcontent.bindtarget.info",
                        CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }


    /**
     * 获取时间 如"20111002"
     * 
     * @return
     */
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    /**
     * 将字符中的"\"转换成"/"
     * 
     * @param str
     * @return
     */
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 取出同步XML文件FTP地址
     * 
     * @return
     */
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }




    // 频道各个方法

    public ICmsChannelDS getChannelDS()
    {
        return channelDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public IUcpBasicDS getBasicDS()
    {
        return basicDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUsysCityDS getCityDS()
    {
        return cityDS;
    }

    public void setCityDS(IUsysCityDS cityDS)
    {
        this.cityDS = cityDS;
    }

    public IsysProvinceLS getProvinceLS()
    {
        return provinceLS;
    }

    public void setProvinceLS(IsysProvinceLS provinceLS)
    {
        this.provinceLS = provinceLS;
    }

    public ICntSyncTaskDS getCntsyncTaskDS()
    {
        return cntsyncTaskDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public IBatchObjectRecordDS getBatchObjectRecordDS()
    {
        return batchObjectRecordDS;
    }

    public void setBatchObjectRecordDS(IBatchObjectRecordDS batchObjectRecordDS)
    {
        this.batchObjectRecordDS = batchObjectRecordDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public IPhysicaChannellLS getPhysicaChannellLS()
    {
        return physicaChannellLS;
    }

    public void setPhysicaChannellLS(IPhysicaChannellLS physicaChannellLS)
    {
        this.physicaChannellLS = physicaChannellLS;
    }

    public ICategoryChannelMapDS getCategoryChannelMapDS()
    {
        return categoryChannelMapDS;
    }

    public void setCategoryChannelMapDS(ICategoryChannelMapDS categoryChannelMapDS)
    {
        this.categoryChannelMapDS = categoryChannelMapDS;
    }

    public ICmsScheduleDS getCmsScheduleDS()
    {
        return cmsScheduleDS;
    }

    public void setCmsScheduleDS(ICmsScheduleDS cmsScheduleDS)
    {
        this.cmsScheduleDS = cmsScheduleDS;
    }

    public IPictureDS getPictureDS()
    {
        return pictureDS;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public ICmsSchedulerecordDS getCmsSchedulerecordDS()
    {
        return cmsSchedulerecordDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public IChannelUpdateApply getChannelupdateApply()
    {
        return channelupdateApply;
    }

    public void setChannelupdateApply(IChannelUpdateApply channelupdateApply)
    {
        this.channelupdateApply = channelupdateApply;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    public IServiceChannelMapDS getServiceChannelMapDS()
    {
        return serviceChannelMapDS;
    }

    public void setServiceChannelMapDS(IServiceChannelMapDS serviceChannelMapDS)
    {
        this.serviceChannelMapDS = serviceChannelMapDS;
    }


}

    function showUpload()
	{
    	clearAllMsgLabel();   //清空信息提示框
    	
    	var uploadtype = SSB_RadioGroup.radioGroupObjs["uploadtype"].getValue();
		if (uploadtype == "0")
		{//HTTP上载
		    document.getElementById("tr_http").style.display = "block"; 
		    document.getElementById("tr_uploaded").style.display = "none";
		    document.getElementById("tr_ftp").style.display = "none";
		    document.getElementById("tr_transfile").style.display = "none";
		    
		    $("#filetype").attr("disabled", false);
		    $("#formatindex").attr("disabled", false);
		    $("#bitrate").attr("disabled", false);
			$("#duration").attr("disabled", false);
			$("#encodingformat").attr("disabled", false);
		    
		    //清空已上载信息
		    $("#uploadeddir").val("");
		    $("#uploadedfilename").val("");
		    
		    //清空FTP上载信息
		    $("#ip").val("");
		    $("#port").val("");	
		    $("#username").val("");
		    $("#password").val("");
		    $("#path").val("");
		    $("#filename").val("");
		}
		else if (uploadtype == "2")
		{//已上载
		    document.getElementById("tr_http").style.display = "none"; 
		    document.getElementById("tr_uploaded").style.display = "block";
		    document.getElementById("tr_ftp").style.display = "none";
		    document.getElementById("tr_transfile").style.display = "none";
		    
		    $("#filetype").attr("disabled", false);
		    $("#formatindex").attr("disabled", false);
		    $("#bitrate").attr("disabled", false);
			$("#duration").attr("disabled", false);
			$("#encodingformat").attr("disabled", false);		    
		    
		    //初始化CP的FTP文件目录
		    callSid("getFtpinoByCpid", _para.cpid);
		    
		    //清空HTTP信息
		    $("#fake_httppath").val("");
		    
		    //清空FTP信息
		    $("#ip").val("");
		    $("#port").val("");	
		    $("#username").val("");
		    $("#password").val("");
		    $("#path").val("");
		    $("#filename").val("");	
		}
		else if (uploadtype == "3")
		{//FTP上载
		    document.getElementById("tr_http").style.display = "none"; 
		    document.getElementById("tr_uploaded").style.display = "none";
		    document.getElementById("tr_ftp").style.display = "block";	
		    document.getElementById("tr_transfile").style.display = "none";
		    
		    $("#filetype").attr("disabled", false);
		    $("#formatindex").attr("disabled", false);
		    $("#bitrate").attr("disabled", false);
			$("#duration").attr("disabled", false);
			$("#encodingformat").attr("disabled", false);
		    
		    //清空HTTP上载信息
		    $("#fake_httppath").val("");
		    
		    //清空已上载信息
		    $("#uploadeddir").val("");
		    $("#uploadedfilename").val("");
		}
		else if (uploadtype == "10")
		{
		    document.getElementById("tr_http").style.display = "none"; 
		    document.getElementById("tr_uploaded").style.display = "none";
		    document.getElementById("tr_ftp").style.display = "none";
		    document.getElementById("tr_transfile").style.display = "block";
		    
		    $("#filetype").attr("disabled", true);
		    $("#formatindex").attr("disabled", true);
			$("#bitrate").attr("disabled", true);
			$("#duration").attr("disabled", true);
			$("#encodingformat").attr("disabled", true);	
			
			//根据fileindex获取子文件
			var fileindex = $("#transfileindex").val();
			if (fileindex != "")
			{
				callSid("searchTransfileByIndex", fileindex);
			}
			else
			{
			    //清空子内容基本信息：名称、文件类型、媒体类型
			    $("#filetype").val("");
			    dynChgFiletype();
			    $("#formatindex").val("");
			    document.getElementById("transfileindex").value = "";
			    $("#transfilename").val("");				
			}
	    	
		    //清空HTTP上载信息
		    $("#fake_httppath").val("");
		    
		    //清空已上载信息
		    $("#uploadeddir").val("");
		    $("#uploadedfilename").val("");	
		    
		    //清空FTP信息
		    $("#ip").val("");
		    $("#port").val("");	
		    $("#username").val("");
		    $("#password").val("");
		    $("#path").val("");
		    $("#filename").val("");
		}
	}

	function dynChgFiletype()
	{
		var filetype = $("#filetype").val();
		
		if (filetype == "1" || filetype == "2")
		{//1：正片   2预览片
			//清空图片类型、图片分辨率校验信息提示框
			$("tr.trpicture label[@id$='_msg']").html("");
			
			document.getElementById("tr_picture").style.display = "none";
			document.getElementById("tr_video").style.display = "block";
			document.getElementById("tb_video").style.display = "block";

			//媒体类型
			callUrl('sysCode/getSysCode.ssm','["cms_file_videoformat","",true]','$M.formatindexList',true);	
					
			//媒体信息			
		    callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_bitrate","",true]','$M.bitrateList',true);           //码流
		    callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_encodingformat","",true]','$M.encodingformatList',true); //编码格式		       
		    callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_resolution","",true]','$M.resolutionList', true);        //视频分辨率	
		} 
		else if (filetype == "3")
		{ 
			//清空媒体信息校验信息提示框
			$("tbody.tbvideo label[@id$='_msg']").html("");
			
			document.getElementById("tr_picture").style.display = "block";
			document.getElementById("tr_video").style.display = "none";
			document.getElementById("tb_video").style.display = "none";	
			
			//媒体类型
			callUrl('sysCode/getSysCode.ssm','["cms_file_pictureformat","",true]','$M.formatindexList',true);
			
			//图片信息
			callUrl('sysCode/getSysCode.ssm','["cms_file_picturetype","",true]','$M.posttypeList',true);     //图片类型
			callUrl('sysCode/getSysCode.ssm','["cms_file_pictureresolution","",true]','$M.prop02List',true); //图片分辨率
		}
		else
		{
			//清空图片类型、图片分辨率校验信息提示框
			$("tr.trpicture label[@id$='_msg']").html("");
			//清空媒体信息校验信息提示框
			$("tbody.tbvideo label[@id$='_msg']").html("");
			
			document.getElementById("tr_picture").style.display = "none";
			document.getElementById("tr_video").style.display = "none";
			document.getElementById("tb_video").style.display = "none";	
			
	        var formatindex = document.getElementById('formatindex');
	        if (formatindex)
	        {
	        	formatindex.length = 0;
	        }       
		}
	} 
	
	function selTransfile()
	{
	    var url = "../content/popSourcefile.html?cpid=" + _para.cpid + "&platform=" + _para.platform;
	    var rtnObj = window.showModalDialog(url, "", "dialogWidth:800px;dialogHeight:600px");    
	    if (typeof(rtnObj) != 'undefined')
	    {
	    	//根据fileindex获取子文件
	    	callSid("searchTransfileByIndex", rtnObj.fileindex);
	    	
	    	$("#transfileindex").val(rtnObj.fileindex);
	        $("#transfilename").val(rtnObj.filename);
	    }		
	}
	
	function showTransfileData(icmSourcefile)
	{		
    	//文件类型
        callUrl('sysCode/getSysCode.ssm','["cms_file_filetype",' + icmSourcefile.filetype + ', true]','$M.filetypeList',true);	
        dynChgFiletype();
        
        if (icmSourcefile.filetype == "1" || icmSourcefile.filetype == "2")
        {
			$("#bitrate").attr("disabled", true);
			$("#duration").attr("disabled", true);
			$("#encodingformat").attr("disabled", true);
        	
        	//媒体类型 
            callUrl('sysCode/getSysCode.ssm','["cms_file_videoformat",' + icmSourcefile.formatindex + ', true]','$M.formatindexList',true);
            
            //媒体信息
			callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_bitrate",' + icmSourcefile.bitrate + ',true]','$M.bitrateList',true); 
	
			//展示播放时长
			var duration = icmSourcefile.duration;
			$("#duration").val(duration);
			
			callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_encodingformat",' + icmSourcefile.encodingformat + ',true]','$M.encodingformatList',true);  
        }
	}
	
	function previewVideoAudio(fileindex)
	{
	    var sURL = "videoaudio_preview.html?fileindex=" + fileindex;
	    var sFeatures = "height=600,width=720,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
	    window.open(sURL, "", sFeatures);
	}

	function previewImage(fileindex)
	{
	    var sURL = "image_preview.html?fileindex=" + fileindex;
	    var sFeatures = "height=300,width=300,status=no,toolbar=no,menubar=no,location=no,scrollbars=no,top=" + (screen.height - 600)/2 + ",left=" + (screen.width - 800)/2;
	    window.open(sURL, "", sFeatures);
	}
	
	function checkFile(action)
	{//action=1:新增  action=2:修改
		var flag = true;
		var error = true;

		//子内容名称
		var programname = document.getElementById("programname");
		//子内容类型
		var filetype = document.getElementById("filetype");
		//媒体类型
		var formatindex = document.getElementById("formatindex");
		
		//非空校验
		//子内容名称
        error = inputIsNull(programname, "programname_msg");
        if (error == false)
        {
            flag = false;
        } 
        
        if (action == "1")
        {//action=1:新增 需校验子内容类型、上载信息、媒体信息；修改时子内容类型不能修改、不需要校验上载信息
            //文件来源
            var uploadtype = SSB_RadioGroup.radioGroupObjs["uploadtype"].getValue();
            if (uploadtype == "10")
            {//母片子文件
            	//子内容类型
            	error = isTransfileNull(filetype, "transfileindex_msg");
                if (error == false)
                {
                    flag = false;
                } 
                //媒体类型
                error = isTransfileNull(formatindex, "transfileindex_msg");
                if (error == false)
                {
                    flag = false;
                }
                
            }
            else
            {
                //子内容类型
            	error = inputIsNull(filetype, "filetype_msg");
                if (error == false)
                {
                    flag = false;
                } 

                //媒体类型
                error = inputIsNull(formatindex, "formatindex_msg");
                if (error == false)
                {
                    flag = false;
                }            	
            }

	        if (uploadtype == "0")
	        {//HTTP上载
	        	//文件地址ַ
	        	var httppath = document.getElementById("fake_httppath"); //document.all.fake_httppath
		        error = inputIsNull(httppath, "httppath_msg");
		        if (error == false)
		        {
		            flag = false;
		        }  	        	
	        }
	        else if (uploadtype == "2")
	        {//已上载
	        	//文件路径
	        	var uploadeddir = document.getElementById("uploadeddir");
	        	//文件名称
	        	var uploadedfilename = document.getElementById("uploadedfilename");
	        	
		        error = inputIsNull(uploadeddir, "uploadeddir_msg");
		        if (error == false)
		        {
		            flag = false;
		        }	 
		        error = inputIsNull(uploadedfilename, "uploadedfilename_msg");
		        if (error == false)
		        {
		            flag = false;
		        }	  
	        }
	        else if (uploadtype == "3")
	        {//FTP上载
	        	//IP
	        	var ip = document.getElementById("ip");
	        	//端口号
	        	var port = document.getElementById("port");
	        	//用户名
	        	var username = document.getElementById("username");
	        	//密码
	        	var password = document.getElementById("password");
	        	//文件路径
	        	var path = document.getElementById("path");
	        	//文件名
	        	var filename = document.getElementById("filename");
	        	
		        error = inputIsNull(ip, "ip_msg");
		        if (error == false)
		        {
		            flag = false;
		        }	
		        error = inputIsNull(port, "port_msg");
		        if (error == false)
		        {
		            flag = false;
		        }	
		        error = inputIsNull(username, "username_msg");
		        if (error == false)
		        {
		            flag = false;
		        }
		        error = inputIsNull(password, "password_msg");
		        if (error == false)
		        {
		            flag = false;
		        }	
		        error = inputIsNull(path, "path_msg");
		        if (error == false)
		        {
		            flag = false;
		        }
		        error = inputIsNull(filename, "filename_msg");
		        if (error == false)
		        {
		            flag = false;
		        }			        
	        } 
	        else if (uploadtype == "10")
	        {//母片子文件
	        	var transfileindex = document.getElementById("transfileindex");
	        	error = isTransfileNull(transfileindex, "transfileindex_msg");
		        if (error == false)
		        {
		            flag = false;
		        }		        	
	        }	        
        }
        
        if (filetype.value == "1" || filetype.value == "2")
        {//正片、预览片，才需要校验的媒体信息
			//码流
			var bitrate = document.getElementById("bitrate");
			//播放时长
			var duration = document.getElementById("duration");
			//编码格式
			var encodingformat = document.getElementById("encodingformat");
			//视频分辨率
			var resolution = document.getElementById("resolution");
			
	        error = inputIsNull(bitrate, "bitrate_msg");
	        if (error == false)
	        {
	            flag = false;
	        }
	        
	        error = inputIsNull(duration, "duration_msg");
	        if (error == false)
	        {
	            flag = false;
	        }
	        
	        error = inputIsNull(encodingformat, "encodingformat_msg");
	        if (error == false)
	        {
	            flag = false;
	        }	
	        error = inputIsNull(resolution, "resolution_msg");
	        if (error == false)
	        {
	            flag = false;
	        }				        	
        }
        else if (filetype.value == "3")
        {//海报，需要校验图片类型及图片分辨率
			var posttype = document.getElementById("posttype");
			//图片分辨率
			var picresolution = document.getElementById("picresolution");	

	        error = inputIsNull(posttype, "posttype_msg");
	        if (error == false)
	        {
	            flag = false;
	        }	
	        error = inputIsNull(picresolution, "picresolution_msg");
	        if (error == false)
	        {
	            flag = false;
	        }		        	
        }
        if (flag == false)
        {
            return flag;
        }//非空校验结束        
        
		//特殊字符校验
        //子内容名称
        error = checkSpecialChar(programname,'programname_msg','');
        if (error == false)
        {
            flag = false;
        }
        //文件名
        if (action == "1")
        {//新增
        	var uploadtype = SSB_RadioGroup.radioGroupObjs["uploadtype"].getValue();
        	if (uploadtype == "3")
        	{//FTP上载：需校验文件名是否含有特殊字符[*?
        		var filename = document.getElementById("filename");
                error = checkSpecialChar(filename, 'filename_msg', 'filename');
                if (error == false)
                {
                    flag = false;
                }        		
        	}
        }
        if (flag == false)
        {
            return flag;
        }        
        
        if (action == "1")
        {//新增        	
        	var uploadtype = SSB_RadioGroup.radioGroupObjs["uploadtype"].getValue();
        	if (uploadtype == "0")
        	{
        		//文件地址
        		var httppath = document.all.fake_httppath; //document.getElementById("fake_httppath");
        		if (filetype.value == "1" || filetype.value == "2")
        		{
        			//媒体类型  formatindex=1：TS formatindex=2：3GP
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(httppath, "TS", "httppath_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(httppath, "3GP", "httppath_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}
        		}
        		else if (filetype.value == "3")
        		{
        			//媒体类型  formatindex=1JPG formatindex=2GIF formatindex=3PNG
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(httppath, "JPG", "httppath_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(httppath, "GIF", "httppath_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			} 
           			else if (formatindex.value == "3")
           			{
           				error = checkSuffix(httppath, "PNG", "httppath_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}   			
        		}
                if (flag == false)
                {
                    return flag;
                }         		
        	}
        	else if (uploadtype == "2")
        	{
        		//已上载
        		//文件路径
        		var uploadeddir = document.getElementById("uploadeddir");
        		error = checkPath(uploadeddir, "uploadeddir_msg");
           		if (error == false)
        		{
        			flag = false;
        		}          		
        		//文件名
        		var uploadedfilename = document.getElementById("uploadedfilename");

        		//正片、预览片
        		if (filetype.value == "1" || filetype.value == "2")
        		{
        			//媒体类型  formatindex=1：TS formatindex=2：3GP
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(uploadedfilename, "TS", "uploadedfilename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(uploadedfilename, "3GP", "uploadedfilename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}
        		}
        		else if (filetype.value == "3")
        		{
        			//媒体类型  formatindex=1JPG formatindex=2GIF formatindex=3PNG
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(uploadedfilename, "JPG", "uploadedfilename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(uploadedfilename, "GIF", "uploadedfilename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			} 
           			else if (formatindex.value == "3")
           			{
           				error = checkSuffix(uploadedfilename, "PNG", "uploadedfilename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}   			
        		}
                if (flag == false)
                {
                    return flag;
                } 
        	}
        	else if (uploadtype == "3")
        	{//FTP
        		//IP地址
        		var ip = document.getElementById("ip");
        		error = checkIpFormat(ip, "ip_msg");
        		if (error == false)
        		{
        			flag = false;
        		}
                //端口号
                var port = document.getElementById("port");
                error = checkPort(port, "port_msg");
        		if (error == false)
        		{
        			flag = false;
        		}        		
        		//文件路径
        		var path = document.getElementById("path");
        		error = checkPath(path, "path_msg");
           		if (error == false)
        		{
        			flag = false;
        		}  
           	    //文件名
           		var filename = document.getElementById("filename");
           		
           		if (filetype.value == "1" || filetype.value == "2" )
           		{//媒体类型  formatindex=1：TS formatindex=2：3GP
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(filename, "TS", "filename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(filename, "3GP", "filename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}
           		}
           		else if (filetype.value == "3")
           		{//海报媒体类型  formatindex=1JPG formatindex=2GIF formatindex=3PNG
           			if (formatindex.value == "1")
           			{
           				error = checkSuffix(filename, "JPG", "filename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		} 
           			}
           			else if (formatindex.value == "2")
           			{
           				error = checkSuffix(filename, "GIF", "filename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			} 
           			else if (formatindex.value == "3")
           			{
           				error = checkSuffix(filename, "PNG", "filename_msg");
                   		if (error == false)
                		{
                			flag = false;
                		}            				
           			}              			
           		}
           		
                if (flag == false)
                {
                    return flag;
                }    //FTP校验结束       
        	}
        }        
        
        //播放时长  不超过4位正整数
        if (filetype.value == 1 || filetype.value == 2)
        {
            var duration = document.getElementById("duration");
            error = checkDuration(duration, "duration_msg");        
            if (error == false)
            {
                flag = false;
            }          
            if (flag == false)
            {
                return flag;
            }             	
        }    
		
		//最大长度校验
        //子内容名称
        error = isMaxLength(programname, "programname_msg", 100);        
        if (error == false)
        {
            flag = false;
        }  
        
        if (action == 1)
        {//新增
        	var uploadtype = SSB_RadioGroup.radioGroupObjs["uploadtype"].getValue();

        	if (uploadtype == "0")
        	{//HTTP上载:判断HTTP上载的文件名是否超长
         		var httppath = $("#fake_httppath").val();  //  document.all.fake_httppath; 
           		var httpfilename = httppath.substring(httppath.lastIndexOf("\\") + 1);
            	if (utf8_strlen2(trim(httpfilename)) > 60)
            	{
                	document.getElementById("httppath_msg").innerText = $res_entry("msg.info.httpfilename.exceed") + 60 + $res_entry("msg.check.string.suffix"); //此项的字符长度不能超过len（一个汉字占3个字符）
                	error = false;
            	}
            	else
            	{
                	document.getElementById("httppath_msg").innerText = "";   
                	error = true;   
            	}       
                if (error == false)
                {
                    flag = false;
                }           		
        	}
        	else if (uploadtype == "2")
        	{//已上载
        		var uploadeddir = document.getElementById("uploadeddir");
        		var uploadedfilename = document.getElementById("uploadedfilename");
        		
                error = isMaxLength(uploadeddir, "uploadeddir_msg", 100);        
                if (error == false)
                {
                    flag = false;
                }   
                error = isMaxLength(uploadedfilename, "uploadedfilename_msg", 60);        
                if (error == false)
                {
                    flag = false;
                }                 
        	}
        	else if (uploadtype == "3")
        	{//FTP
        		var port = document.getElementById("port");
        		var username = document.getElementById("username");
        		var password = document.getElementById("password");
        		var path = document.getElementById("path");
        		var filename = document.getElementById("filename");
        		
                error = isMaxLength(port, "port_msg", 100);        
                if (error == false)
                {
                    flag = false;
                } 
                error = isMaxLength(username, "username_msg", 100);        
                if (error == false)
                {
                    flag = false;
                } 
                error = isMaxLength(password, "password_msg", 100);        
                if (error == false)
                {
                    flag = false;
                }
                error = isMaxLength(path, "path_msg", 100);        
                if (error == false)
                {
                    flag = false;
                }
                error = isMaxLength(filename, "filename_msg", 60);        
                if (error == false)
                {
                    flag = false;
                }                          
        	}
        }
        if (flag == false)
        {
            return flag;
        }   
        
        return flag;
	}
	
	//验证必填字段是否已填
	function inputIsNull(input,input_msg)
	{
   	    if(trim(input.value) == '' || input.value == null)
        {
            document.getElementById(input_msg).innerText = $res_entry("msg.info.mustfill");  //"此项不能为空"
            document.getElementById(input.id).focus();
            return false;
        }
        else
        {
            document.getElementById(input_msg).innerText = "";
            return true;
        }
    }
	
	function isTransfileNull(input,input_msg)
	{
        if (trim(input.value) == '' || input.value == null)
        {
            document.getElementById(input_msg).innerText = $res_entry("msg.info.transfile.mustfill");  //"请选择子文件"
            error = false;
        }
        else
        {
            document.getElementById(input_msg).innerText = "";
            error = true;
        }		
	}
	
	//验证输入框中字数是否超过最大长度
	function isMaxLength(input,input_msg,len)
	{	
    	if (utf8_strlen2(trim(input.value)) > len)
    	{
        	document.getElementById(input_msg).innerText = $res_entry("msg.check.string.prefix") + len + $res_entry("msg.check.string.suffix"); //此项的字符长度不能超过len（一个汉字占3个字符）
        	document.getElementById(input.id).focus();
        	return false;
    	}
    	else
    	{
        	document.getElementById(input_msg).innerText = "";   
        	return true;   
    	}
	}
	
     //判断是否含有特殊字符
	function checkSpecialChar(input,input_msg, id)
	{
		var flag = false;     //校验标识，默认不含特殊字符
		var specialStr = "";   //需要限制的特殊字符
		var pattern;           //匹配模式串
		
		var value = input.value.trim();
		
		if (id == "filename")
	    {//文件名：此项不能包含这些字符：[  *  ?
	    	pattern = /[\[\*\?]/;
	    	specialStr = " [ * ?";
	    }
	    else
	    {//此项不能包含这些字符：! @ # $ % ^ & * ; | ? ()[]{}<> ' "
	    	pattern = /[!@#\$%^&\*;\|\?\(\)\[\]\{\}<>\'\"]/;
	    	specialStr = " ! @ # $ % ^ & * ; | ? () [] {} <> \' \"";
	    }		
		
		flag = pattern.test(value); 
		
	    if (flag == true)
	    {
	        document.getElementById(input_msg).innerText = $res_entry("msg.info.notinclude") + specialStr; //"此项不能包含这些字符："
	        document.getElementById(input.id).focus(); 
	        return false;
	    }
	    else
	    {
	        document.getElementById(input_msg).innerText = "";   
	        return true;     
	    }
	} 
	
	//判断文件名后缀是否存在，若存在判断与固定的后缀是否一致
	function checkSuffix(input, in_suffix, input_msg)
	{
		var in_value = trim(input.value);
		if (null != in_value && "" != in_value && in_value.indexOf(".") != -1)
		{
			var suffix = in_value.substring(in_value.lastIndexOf(".") + 1).toUpperCase();
			if (suffix != in_suffix)
			{
				document.getElementById(input_msg).innerText = $res_entry("msg.info.suffix.mustmatch"); //文件后缀名必须与媒体类型一致
				document.getElementById(input.id).focus();
				return false; 
			}
            else
            {
                document.getElementById(input_msg).innerText = ""; 
                return true;                
            }   		
		}
		else
		{
			document.getElementById(input_msg).innerText = $res_entry("msg.info.suffix.mustfill");  //"文件后缀名不能为空"
			document.getElementById(input.id).focus();
			return false; 			
		}
	}
	
	//判断IP格式是否正确
    function checkIpFormat(input,input_msg)
    {

        if(trim(input.value) != '' && input.value != null)
        {
            if(checkIPaddr(trim(input.value)) == false)
            {
                document.getElementById(input_msg).innerText = $res_entry("msg.info.ipformat.incorrect");   //"IP地址格式不正确"
                document.getElementById(input.id).focus();
                return false;         
            }
            else
            {
                document.getElementById(input_msg).innerText = ""; 
                return true;                
            }    
        }
    
    }
    
    //判断文件路径是否以/开始和结束
    function checkPath(input, input_msg)
    {
        var value = input.value;
    	var first = value.charAt(0);
        var last = value.charAt(value.length-1);
        if (first!='/' || last!='/' || (input.value).indexOf('//')!=-1)
        {
            document.getElementById(input_msg).innerText = $res_entry("msg.info.pathformat.incorrect");  //"文件路径开始和结尾必须都使用反斜杠/"
            document.getElementById(input.id).focus();        	
            return false;
        }
        else
        {
            document.getElementById(input_msg).innerText = ""; 
            return true;                
        } 
    } 

    //判断端口号是否为数字
    function checkPort(input, input_msg)
    {   
        var newPar=/^\d+$/;
        if (newPar.test(trim(input.value)) == false)
        {
            document.getElementById(input_msg).innerText = $res_entry("msg.info.mustisnumber");  //此项必须由数字组成
            document.getElementById(input.id).focus();        	
           return false;        	
        }
        else
        {
            document.getElementById(input_msg).innerText = ""; 
            return true;                
        } 
    }
    
    function checkDuration(input, input_msg)
    {    	
    	var flag = true;
    	var duration = trim(input.value);
    	
    	if (null != duration && "" != duration)
    	{
    		var reg = /^([1-9]|[1-9]\d{1,3})$/;
			if (reg.test(duration) == false)
			{
				flag = false;
			}
    		
    		if (flag == false)
    		{
    			document.getElementById(input_msg).innerText = $res_entry("msg.info.duration.incorrect");   //"播放时长格式不正确";  
    			document.getElementById(input.id).focus();
    		}
    		else
    		{
                document.getElementById(input_msg).innerText = "";
    		}
    		return flag;
    	}
    }
    
    function utf8_strlen2(str) 
    { 
    	var cnt = 0; 
    	for( i=0; i<str.length; i++) 
    	{ 
    	    var value = str.charCodeAt(i); 
    	    if( value < 0x080) 
    	    { 
    	        cnt += 1; 
    	    } 
    	    else if( value < 0x0800) 
    	    { 
    	        cnt += 2; 
    	    } 
    	    else  if(value < 0x010000)
    	    { 
    	        cnt += 3; 
    	    }
    		else
    		{
    			cnt += 4;
    		}
    	} 
    	return cnt; 
    }    
    
    //展示CP的FTP路径
    function setFtppath(ftpinfo)
    {
    	document.getElementById("uploadedpath").innerText = ftpinfo.ftppath + "/";
    }
    
    function changeFtppath()
    {
    	var ftppath = $("#ftppath").val() + "/" + $("#uploadeddir").val();
    	ftppath = ftppath.replace(/(\/+)/g, "/");
    	document.getElementById("uploadedpath").innerText = ftppath; //$("#ftppath").val() + "/" + $("#uploadeddir").val();
    }

    //展示子内容修改的基本信息
    function setModFileData(file)
    {
        var filetype = file.filetype;

        //文件类型
        callUrl('sysCode/getSysCode.ssm','["cms_file_filetype",' + filetype + ', true]','$M.filetypeList',true);
        	
        if (filetype == "1" || filetype == "2")
        {//1：正片    2：预览片
			document.getElementById("tr_picture").style.display = "none";
			document.getElementById("tr_video").style.display = "block";
			document.getElementById("tb_video").style.display = "block"; 

			//媒体类型 
            callUrl('sysCode/getSysCode.ssm','["cms_file_videoformat",' + file.formatindex + ', true]','$M.formatindexList',true);

			//获取媒体信息
			callSid("searchModFileVideoinfoByIndex", file.fileindex);	
			
			var uploadtype = file.uploadtype;
			if (uploadtype == "10")
			{//文件来源：母片子文件
				//码流、播放时长、编码格式属性不能修改
				$("#bitrate").attr("disabled", true);
				$("#duration").attr("disabled", true);
				$("#encodingformat").attr("disabled", true);
			}
			else
			{
				//码流、播放时长、编码格式属性能修改
				$("#bitrate").attr("disabled", false);
				$("#duration").attr("disabled", false);
				$("#encodingformat").attr("disabled", false);				
			}
        }
        else if (filetype == "3")
        {
			document.getElementById("tr_picture").style.display = "block";
			document.getElementById("tr_video").style.display = "none";
			document.getElementById("tb_video").style.display = "none";  

            //媒体类型
            callUrl('sysCode/getSysCode.ssm','["cms_file_pictureformat",' + file.formatindex + ', true]','$M.formatindexList',true);
			
			callUrl('sysCode/getSysCode.ssm','["cms_file_picturetype",' + file.posttype + ',true]','$M.posttypeList',true);
			callUrl('sysCode/getSysCode.ssm','["cms_file_pictureresolution",' + file.prop02 + ', true]','$M.prop02List',true);
			
			//获取媒体信息
			callSid("searchModFileVideoinfoByIndex", file.fileindex);	
        }
        else
        {
			document.getElementById("tr_picture").style.display = "none";
			document.getElementById("tr_video").style.display = "none";
			document.getElementById("tb_video").style.display = "none";	            
        }
    }
    //展示子内容修改的媒体信息
    function setModFileVideoinfoData(videoinfo) 
    {
    	$("#programname").val(videoinfo.programname);
    	
    	var filetype = document.getElementById("filetype").value;
    	
    	if (filetype == "1" || filetype == "2")
    	{//如果是正片、预览片，需要获取的媒体信息
			//媒体信息
			callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_bitrate",' + videoinfo.bitrate + ',true]','$M.bitrateList',true); 
	
			//展示播放时长
			var duration = videoinfo.duration;
			
			callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_encodingformat",' + videoinfo.encodingformat + ',true]','$M.encodingformatList',true); 
			callUrl('sysCode/getSysCode.ssm','["cms_videoinfo_resolution",' + videoinfo.resolution + ',true]','$M.resolutionList',true);			
    	}
    }  

    //展示子内容查看的基本信息
    function setFileData(file)
    {
        var filetype = file.filetype;
 
        if (filetype == "1" || filetype == "2")
        {//1：正片    2：预览片
			document.getElementById("tr_view_picture").style.display = "none";
			document.getElementById("tr_view_video").style.display = "block";
			document.getElementById("tb_view_video").style.display = "block"; 	

			//获取媒体信息
			callSid("searchFileVideoinfoByIndex", file.fileindex);	
        }
        else if (filetype == "3")
        {
			document.getElementById("tr_view_picture").style.display = "block";
			document.getElementById("tr_view_video").style.display = "none";
			document.getElementById("tb_view_video").style.display = "none"; 
			
			//获取媒体信息
			callSid("searchFileVideoinfoByIndex", file.fileindex);				
       }
        else
        {
			document.getElementById("tr_view_picture").style.display = "none";
			document.getElementById("tr_view_video").style.display = "none";
			document.getElementById("tb_view_video").style.display = "none";	            
        }
    }

    var filetypeValue = {};
    function getViewFiletype()
    {
        var filetype = getRiaCtrlValue('filetype_v');
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_file_filetype", ' + filetype + ']', 'filetypeValue', true); 
        return filetypeValue.rtnValue;       
    } 

    var formatValue = {};
    function getViewFileformat()
    {
    	var filetype = getRiaCtrlValue('filetype_v');
        var formatindex = getRiaCtrlValue('formatindex_v');

        if (filetype == "1" || filetype == "2")
        {//正片、预览片
        	callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_file_videoformat", ' + formatindex + ']', 'formatValue', true);
        }
        else if (filetype == "3")
        {
        	callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_file_pictureformat", ' + formatindex + ']', 'formatValue', true);
        } 
        return formatValue.rtnValue;       
    } 

    var posttypeValue = {};
    function getViewPosttype()
    {
        var posttype = getRiaCtrlValue('posttype_v');
        if (posttype == "0" || posttype == "1" || posttype == "2" || posttype == "3" || posttype == "4" || posttype == "5" || posttype == "6" || posttype == "7" || posttype == "9" || posttype == "10" || posttype == "11" || posttype == "12" || posttype == "99")
        {
        	callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_file_picturetype", ' + posttype + ']', 'posttypeValue', true); 
        	return posttypeValue.rtnValue;
        }
        else
        {
        	return "";
        }     
    }  

    var picresolutionValue = {};
    function getViewPicResolution()
    {
        var picresolution = getRiaCtrlValue('picresolution_v');
        if (picresolution == "1" || picresolution == "2" || picresolution == "3" || picresolution == "4" || picresolution == "5" || picresolution == "6" || picresolution == "7" || picresolution == "8" || picresolution == "9" || picresolution == "10" || picresolution == "11")
        {
    	    callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_file_pictureresolution", ' + picresolution + ']', 'picresolutionValue', true); 
    	    return picresolutionValue.rtnValue; 
        }
        else
        {
        	return "";
        }
    } 
    
    function getViewDuration()
    {
    	var rtnStr = "";
    	var duration = getRiaCtrlValue('duration_v');
    	rtnStr = duration + " ";
    	return rtnStr;
    }

    var encodingformatValue = {};
    function getViewEncodingFormat()
    {
        var encodingformat = getRiaCtrlValue('encodingformat_v');
        if (encodingformat == "1" || encodingformat == "2")
        {
            callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_videoinfo_encodingformat", ' + encodingformat + ']', 'encodingformatValue', true); 
    	    return encodingformatValue.rtnValue;         	
        }
        else
        {
        	return "";
        }
    }      

    var resolutionValue = {};
    function getViewResolution()
    {
        var resolution = getRiaCtrlValue('resolution_v');
        if (resolution == "1" || resolution == "2" || resolution == "3")
        {
    	    callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_videoinfo_resolution", ' + resolution + ']', 'resolutionValue', true); 
    	    return resolutionValue.rtnValue;          	
        }
        else
        {
        	return "";
        }
    } 

    function getViewAdownload()
    {
    	var rtnString = "";
        var adownload = getRiaCtrlValue('adownload_v');
        if (adownload == "0")
        {
        	rtnString = $res_entry("label.no");
        }
        else if (adownload == "1")
        {
        	rtnString = $res_entry("label.yes");
        }  
        return rtnString; 
    }       

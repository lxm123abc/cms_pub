package com.zte.cms.channel.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsSchedulerecord extends DynamicBaseObject
{
    private java.lang.Long schedulerecordindex;
    private java.lang.String schedulerecordid;
    private java.lang.String scheduleid;
    private java.lang.String physicalchannelid;
    private java.lang.String startdate;
    private java.lang.String starttime;
    private java.lang.String duration;
    private java.lang.String cpcontentid;
    private java.lang.String description;
    private java.lang.Integer isarchive;
    private java.lang.Long domain;
    private java.lang.Integer hotdegree;
    private java.lang.String namecn;
    private java.lang.String nameen;
    private java.lang.String descen;
    private java.lang.Long reservecode1;
    private java.lang.Long reservecode2;
    private java.lang.Integer reservesel1;
    private java.lang.Integer reservesel2;
    private java.lang.String reservetime1;
    private java.lang.String reservetime2;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String creatoropername;
    private java.lang.String submittername;
    private java.lang.Integer templatetype;
    private java.lang.String wkfwroute;
    private java.lang.String wkfwpriority;
    private java.lang.String effecttime;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;

    private java.lang.String programname;
    private java.lang.String channelname;
    private java.lang.String physicalchannelideq;

    private java.lang.String startdate1;
    private java.lang.String startdate2;
    
    private java.lang.String statuses;
    private java.lang.String channelid;
    
    

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.String getStartdate1()
    {
        return startdate1;
    }

    public void setStartdate1(java.lang.String startdate1)
    {
        this.startdate1 = startdate1;
    }

    public java.lang.String getStartdate2()
    {
        return startdate2;
    }

    public void setStartdate2(java.lang.String startdate2)
    {
        this.startdate2 = startdate2;
    }

    public java.lang.String getProgramname()
    {
        return programname;
    }

    public void setProgramname(java.lang.String programname)
    {
        this.programname = programname;
    }

    public java.lang.String getChannelname()
    {
        return channelname;
    }

    public void setChannelname(java.lang.String channelname)
    {
        this.channelname = channelname;
    }

    public java.lang.Long getSchedulerecordindex()
    {
        return schedulerecordindex;
    }

    public void setSchedulerecordindex(java.lang.Long schedulerecordindex)
    {
        this.schedulerecordindex = schedulerecordindex;
    }

    public java.lang.String getSchedulerecordid()
    {
        return schedulerecordid;
    }

    public void setSchedulerecordid(java.lang.String schedulerecordid)
    {
        this.schedulerecordid = schedulerecordid;
    }

    public java.lang.String getScheduleid()
    {
        return scheduleid;
    }

    public void setScheduleid(java.lang.String scheduleid)
    {
        this.scheduleid = scheduleid;
    }

    public java.lang.String getPhysicalchannelid()
    {
        return physicalchannelid;
    }

    public void setPhysicalchannelid(java.lang.String physicalchannelid)
    {
        this.physicalchannelid = physicalchannelid;
    }

    public java.lang.String getStartdate()
    {
        return startdate;
    }

    public void setStartdate(java.lang.String startdate)
    {
        this.startdate = startdate;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.String duration)
    {
        this.duration = duration;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getIsarchive()
    {
        return isarchive;
    }

    public void setIsarchive(java.lang.Integer isarchive)
    {
        this.isarchive = isarchive;
    }

    public java.lang.Long getDomain()
    {
        return domain;
    }

    public void setDomain(java.lang.Long domain)
    {
        this.domain = domain;
    }

    public java.lang.Integer getHotdegree()
    {
        return hotdegree;
    }

    public void setHotdegree(java.lang.Integer hotdegree)
    {
        this.hotdegree = hotdegree;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.Long getReservecode1()
    {
        return reservecode1;
    }

    public void setReservecode1(java.lang.Long reservecode1)
    {
        this.reservecode1 = reservecode1;
    }

    public java.lang.Long getReservecode2()
    {
        return reservecode2;
    }

    public void setReservecode2(java.lang.Long reservecode2)
    {
        this.reservecode2 = reservecode2;
    }

    public java.lang.Integer getReservesel1()
    {
        return reservesel1;
    }

    public void setReservesel1(java.lang.Integer reservesel1)
    {
        this.reservesel1 = reservesel1;
    }

    public java.lang.Integer getReservesel2()
    {
        return reservesel2;
    }

    public void setReservesel2(java.lang.Integer reservesel2)
    {
        this.reservesel2 = reservesel2;
    }

    public java.lang.String getReservetime1()
    {
        return reservetime1;
    }

    public void setReservetime1(java.lang.String reservetime1)
    {
        this.reservetime1 = reservetime1;
    }

    public java.lang.String getReservetime2()
    {
        return reservetime2;
    }

    public void setReservetime2(java.lang.String reservetime2)
    {
        this.reservetime2 = reservetime2;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public void initRelation()
    {
        this.addRelation("schedulerecordindex", "SCHEDULERECORDINDEX");
        this.addRelation("schedulerecordid", "SCHEDULERECORDID");
        this.addRelation("scheduleid", "SCHEDULEID");
        this.addRelation("physicalchannelid", "PHYSICALCHANNELID");
        this.addRelation("startdate", "STARTDATE");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("duration", "DURATION");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("isarchive", "ISARCHIVE");
        this.addRelation("domain", "DOMAIN");
        this.addRelation("hotdegree", "HOTDEGREE");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("reservecode1", "RESERVECODE1");
        this.addRelation("reservecode2", "RESERVECODE2");
        this.addRelation("reservesel1", "RESERVESEL1");
        this.addRelation("reservesel2", "RESERVESEL2");
        this.addRelation("reservetime1", "RESERVETIME1");
        this.addRelation("reservetime2", "RESERVETIME2");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
    }

    public java.lang.String getPhysicalchannelideq()
    {
        return physicalchannelideq;
    }

    public void setPhysicalchannelideq(java.lang.String physicalchannelideq)
    {
        this.physicalchannelideq = physicalchannelideq;
    }

    public java.lang.String getStatuses()
    {
        return statuses;
    }

    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }
}

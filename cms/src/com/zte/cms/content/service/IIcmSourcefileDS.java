package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmSourcefileDS
{
    /**
     * 新增IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public void insertIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 批量更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmSourcefileList(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException;

    /**
     * 根据条件更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 根据条件批量更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateIcmSourcefileListByCond(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException;

    /**
     * 删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 批量删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmSourcefileList(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException;

    /**
     * 根据条件删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 根据条件批量删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeIcmSourcefileListByCond(List<IcmSourcefile> icmSourcefileList) throws DomainServiceException;

    /**
     * 查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @return IcmSourcefile对象
     * @throws DomainServiceException ds异常
     */
    public IcmSourcefile getIcmSourcefile(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 根据条件查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @return 满足条件的IcmSourcefile对象集
     * @throws DomainServiceException ds异常
     */
    public List<IcmSourcefile> getIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
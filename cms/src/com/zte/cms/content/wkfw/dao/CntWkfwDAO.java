package com.zte.cms.content.wkfw.dao;

import java.util.List;
import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;

public class CntWkfwDAO extends DynamicObjectBaseDao implements ICntWkfwDAO
{
    // ��־
    private Log log = SSBBus.getLog(getClass());

    public PageInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity)
    {
        return super.indexPageQuery("queryMyNeedHandleWorkflowForCmsByCond",
                "queryMyNeedHandleWorkflowForCmsCntByCond", cntWkfwCondition, start, pageSize, puEntity);
    }

    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntWkfwCondition)
    {
        long result = super.getTotalCount("queryMyNeedHandleWorkflowForCmsCntByCond", cntWkfwCondition);
        return result;
    }

    /**
     * ���ݹ�����ѯ
     * 
     * @param cmsProgramWkfwhis
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQuery(CntWkfwhis CntWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsProgramWkfwhisUcdnListByCond", "queryCmsProgramWkfwhisUcdnListCntByCond",
                CntWkfwhis, start, pageSize, puEntity);
    }

    /**
     * ���ݹ�����̸���
     * 
     * @param cmsProgramWkfwhis
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQuery(CntWkfwhis CntWkfwhis, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramWkfwhis by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryWorkflowTrackListCntByCond", CntWkfwhis)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CntWkfwhis> rsList = (List<CntWkfwhis>) super.pageQuery("queryWorkflowTrackListByCond", CntWkfwhis,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramWkfwhis by condition end");
        return pageInfo;
    }

    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis)
    {
        long result = super.getTotalCount("queryWorkflowTrackListCntByCond", cntWkfwis);
        return result;
    }

    /**
     * �����Ϣ�鿴
     * 
     * @param cmsProgramWkfwhis
     * @return
     */
    public CntWkfwhis getCmsProgramWkfwhis(CntWkfwhis CntWkfwhis) throws DAOException
    {
        log.debug("getCmsProgramWkfwhis starting............");
        CntWkfwhis cmsobj = (CntWkfwhis) super.queryForObject("queryWorkflowTrackListByCond", CntWkfwhis);
        log.debug("getCmsProgramWkfwhis end..........");
        return cmsobj;
    }
}

package com.zte.cms.content.platformSyn.dao;

import java.util.List;

import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICntPlatformSyncDAO
{
     /**
	  * 新增CntPlatformSync对象 
	  * 
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */	
     public void insertCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
	  * 根据主键更新CntPlatformSync对象
	  * 
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public void updateCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException;


     /**
	  * 根据主键删除CntPlatformSync对象
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public void deleteCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException;
     
     
     /**
	  * 根据主键查询CntPlatformSync对象
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @return 满足条件的CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public CntPlatformSync getCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
	  * 根据条件查询CntPlatformSync对象 
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @return 满足条件的CntPlatformSync对象集
	  * @throws DAOException dao异常
	  */
     public List<CntPlatformSync> getCntPlatformSyncByCond( CntPlatformSync cntPlatformSync )throws DAOException;

     /**
	  * 根据条件分页查询CntPlatformSync对象，作为查询条件的参数
	  *
	  * @param cntPlatformSync CntPlatformSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntPlatformSync对象，作为查询条件的参数
	  *
	  * @param cntPlatformSync CntPlatformSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     
     /**
      * 根据objecttype和objectindex删除CntPlatformSync对象
      *
      * @param cntPlatformSync CntPlatformSync对象
      * @throws DAOException dao异常
      */
     public void deleteCntPlatformSyncByObjindex( CntPlatformSync cntPlatformSync )throws DAOException;
        
     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync对象
      * @param cntPlatformSync
      * @return
      * @throws DAOException
      */
     public CntPlatformSync getCntPlatformSync4Object( CntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync的对应的子网元数目
      * @param cntPlatformSync
      * @return
      * @throws DAOException
      */
     public int getCntPlatformSyncChildNumber( CntPlatformSync cntPlatformSync )throws DAOException;
}
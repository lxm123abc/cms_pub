package com.zte.cms.commonSync.common;

import java.util.List;
import java.util.Map;

public interface IObject2XmlFile
{
    public String createXmlFile(Map<String, List> objectMap, int targetType, int objectType, int syncType)throws Exception;
}

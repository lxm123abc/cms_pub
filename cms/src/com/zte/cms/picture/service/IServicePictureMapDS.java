package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.ServicePictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServicePictureMapDS
{
    /**
     * 新增ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException;

    /**
     * 更新ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException;

    /**
     * 批量更新ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServicePictureMapList(List<ServicePictureMap> servicePictureMapList)
            throws DomainServiceException;

    /**
     * 删除ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException;

    /**
     * 批量删除ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServicePictureMapList(List<ServicePictureMap> servicePictureMapList)
            throws DomainServiceException;

    /**
     * 查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @return ServicePictureMap对象
     * @throws DomainServiceException ds异常
     */
    public ServicePictureMap getServicePictureMap(ServicePictureMap servicePictureMap) throws DomainServiceException;

    /**
     * 根据条件查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @return 满足条件的ServicePictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServicePictureMap> getServicePictureMapByCond(ServicePictureMap servicePictureMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

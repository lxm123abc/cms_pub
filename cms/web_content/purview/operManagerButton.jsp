<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
  Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
    for(Iterator i=purviewList.keySet().iterator();i.hasNext();){
    System.out.print(i.next() + " ");
  } 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<title><fmt:message key="operCurrentGroup.opermanage"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   var addWin;          // ???��2������?������??
   var delWin;          // ��?3y2������?������??
   var infoWin;         // ?��??2������?����?D?����??
   var unLockWin;       // ?a??����?����?D?����??

   function unLockOper () {
      if (delWin != null && (! delWin.closed)) {
         alert('<fmt:message key="operManagerButton.delclosewindow"/>');
         delWin.focus();
         return;
      }
      if (infoWin != null && (! infoWin.closed)) {
         alert('<fmt:message key="operManagerButton.modclosewindow"/>');
         infoWin.focus();
         return;
      }
       if (addWin != null && (! addWin.closed)) {
         alert('<fmt:message key="operManagerButton.addclosewindow"/>');
         addWin.focus();
         return;
      }
      if (unLockWin != null && (! unLockWin.closed))
         if (window.confirm('<fmt:message key="operManagerButton.addwindowtoclose"/>'))
            unLockWin.close();
         else
            unLockWin.focus();
      else
         unLockWin = window.open('operUnlock.jsp?operID=' + document.view.operID.value,'del','width=300, height=80');
   }



   // ???��2������?��
   function addOper() {
       if (unLockWin != null && (! unLockWin.closed)) {
         alert('<fmt:message key="operManagerButton.unlockclosewindow"/>');
         unLockWin.focus();
         return;
      }
      if (delWin != null && (! delWin.closed)) {
         alert('<fmt:message key="operManagerButton.delclosewindow"/>');
         delWin.focus();
         return;
      }
      if (infoWin != null && (! infoWin.closed)) {
         alert('<fmt:message key="operManagerButton.modclosewindow"/>');
         infoWin.focus();
         return;
      }
      if (addWin != null && (! addWin.closed))
         if (window.confirm('<fmt:message key="operManagerButton.addwindowtoclose"/>'))
            addWin.close();
         else
            addWin.focus();
      else
         addWin = window.open('operInfo.jsp','add','width=650, height=500');
   }

   // ��?3y2������?��
   function delOper() {
       if (unLockWin != null && (! unLockWin.closed)) {
         alert('<fmt:message key="operManagerButton.unlockclosewindow"/>');
         unLockWin.focus();
         return;
      }
      if (addWin != null && (! addWin.closed)) {
         alert('<fmt:message key="operManagerButton.addclosewindow"/>');
         addWin.focus();
         return;
      }
      if (infoWin != null && (! infoWin.closed)) {
         alert('<fmt:message key="operManagerButton.modclosewindow"/>');
         infoWin.focus();
         return;
      }
      if (delWin != null && (! delWin.closed))
         if (window.confirm('<fmt:message key="operManagerButton.delwindowtoclose"/>'))
            delWin.close();
         else
            delWin.focus();
      else
         delWin = window.open('operDel.jsp?operID=' + document.view.operID.value,'del','width=300, height=80');
   }

   // DT??2������?����?D?
   function updateOper() {
      if (unLockWin != null && (! unLockWin.closed)) {
         alert('<fmt:message key="operManagerButton.unlockclosewindow"/>');
         unLockWin.focus();
         return;
      }
      if (addWin != null && (! addWin.closed)) {
         alert('<fmt:message key="operManagerButton.addclosewindow"/>');
         addWin.focus();
         return;
      }
      if (delWin != null && (! delWin.closed)) {
         alert('<fmt:message key="operManagerButton.delclosewindow"/>');
         delWin.focus();
         return;
      }
      if (infoWin != null && (! infoWin.closed))
         if (window.confirm('<fmt:message key="operManagerButton.modwindowtoclose"/>'))
            infoWin.close();
         else
            infoWin.focus();
      else
         infoWin = window.open('operInfo.jsp?operID=' + document.view.operID.value,'update','width=650, height=500');
   }

   // ?��2a????���?����??-��?�䨰?a
   function checkWin () {
      if (addWin != null && (! addWin.closed)) {
         alert('<fmt:message key="operManagerButton.addwindowopened"/>');
         addWin.focus();
         return 'add';                                  // ???�����?����?�䨰?a
      }
      else if (delWin != null && (! delWin.closed)) {
         alert('<fmt:message key="operManagerButton.delwindowopened"/>');
         delWin.focus();
         return 'del';                                  // ��?3y���?����?�䨰?a
      }
      else if (infoWin != null && (! infoWin.closed)) {
         alert('<fmt:message key="operManagerButton.modwindowopened"/>');
         infoWin.focus();
         return 'info';                                 // ��?D?���?����?�䨰?a
      }
      else
         return 'no';                                   // ?T�䨰?a���?��
   }

   // ?��D?2������?����D����
   function refresh () {
      //parent.listFrame.refresh();
      parent.document.URL = 'operManage.jsp';
   }

   // ?��D?�̡�?���???��?2������?�̨�3
   function refreshCurrent () {
      parent.currentFrame.document.view.operID.value = document.view.operID.value;
      parent.currentFrame.document.view.serviceKey.value = document.view.serviceKey.value;
      parent.currentFrame.refresh();
   }
</script>
</head>
<body class="body-style1">
<form name="view">
<input type="hidden" name="operID" value="">
<input type="hidden" name="operStat" value="">
<input type="hidden" name="serviceKey" value="">
<input type="hidden" name="idid" value="">
<input type="hidden" name="pageflag" value="">
<input type="hidden" name="pid" value="">
<table border="0" height="60%">
    <%
      if (purviewList.get("2-8") != null){
    %>
    <tr height="20%" valign="middle">

    <td><input type="button" name="unlock" value="<fmt:message key="operUnlock.ulock"/>" class="button-style1"  onclick="javascript:unLockOper()"  disabled></td>
    <%
    }
    %>  
  </tr>
  
  <tr height="20%" valign="middle">
    <td><input type="button" name="refresh" value="<fmt:message key="operManagerButton.refresh"/>" class="button-style1" onclick="parent.document.URL = 'operManage.jsp'"></td>
  </tr>
    <%
      if (purviewList.get("2-1") != null){
    %>  
  <tr height="20%" valign="middle">
    <td><input type="button" name="add" value="<fmt:message key="operGrpManage.add"/>" class="button-style1" onclick="javascript:addOper()"></td>
  </tr>
    <%}%>  
    <%
      if (purviewList.get("2-2") != null){
    %>    
  <tr height="20%" valign="middle">
    <td><input type="button" name="del" value="<fmt:message key="operGrpManage.del"/>" class="button-style1" onclick="javascript:delOper()" disabled></td>
  </tr>
    <%}%> 
    <%
      if (purviewList.get("2-3") != null){
    %>        
  <tr height="20%" valign="middle">
    <td><input type="button" name="attr" value="<fmt:message key="operManagerButton.property"/>" class="button-style1" onclick="javascript:updateOper()" disabled></td>
  </tr>
    <%}%>   
</table>
    <%
      if (purviewList.get("2-5") != null){
    %> 
<table border="0" height="12%">

  <tr valign="middle">
    <td><input type="button" name="signOut" value="<fmt:message key="operManagerButton.signout"/>" class="button-style1" onclick="javascript:refreshCurrent()" disabled></td>
  </tr>
  
</table>
  <%}%> 
<table border="1" bordercolorlight="#669933" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" style="DISPLAY: none">
  <tr>
    <td><input type="text" name="operName" value="" disabled style="width: 80">
  </tr>
  <tr>
    <td><input type="text" name="serviceName" value="" disabled style="width: 80">
  </tr>
</table>
</form>
</body>
</html>

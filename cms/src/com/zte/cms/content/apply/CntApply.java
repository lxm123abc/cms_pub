package com.zte.cms.content.apply;

import java.util.List;

import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.util.ApplyPool;
import com.zte.cms.content.util.AuditPool;
import com.zte.jwf.workflowEngine.common.util.BpsException;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;

/**
 * <p>
 * 文件名称: CntApply.java
 * </p>
 * <p>
 * 文件描述: 内容订单申请类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2011
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容提交审核、审核方法
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：2011年05月10日
 *    版 本 号：ZXIN10 CMSV2.01.02
 *    修 改 人：李梅
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntApply extends WorkflowBase implements ICntApply
{
    private Log log = SSBBus.getLog(getClass());

    private ICntApplyWkfwCheck cntApplyWkfwCheck = null;
    private ICmsProgramDS cmsProgramDS = null;
    private String callbackProcess;
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    /**
     * <p>
     * 提交审核，发起新增申请工作流
     * </p>
     * 
     * @param cntorderList 订单编号列表
     * @return 返回工作流新增申请结果，类型String，显示资源文件信息
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.cntorder
     * @see com.zte.cms.cntorder.model.Cntorder
     * @since ZXIN10 CMSV2.01.01
     */
    public String runApply(String programid) throws Exception
    {
        log.debug("CntApply: runApply begin and programid=" + programid);
        try
        {
            //if (applyList.contains(programid))
            if (ApplyPool.getInstance().contains(programid))
            {// 内容正在被其他操作员提审 250004
                return CntConstants.CNT_BEING_APPLIED;
            }

            //applyList.add(programid);

            CmsProgram cmsProgram = new CmsProgram();
            List<CmsProgram> cmsProgramList = null;
            cmsProgram.setProgramid(programid);
            cmsProgramList = cmsProgramDS.getCmsProgramByCond(cmsProgram);
            if (null != cmsProgramList && cmsProgramList.size() > 0)
            {
                cmsProgram = cmsProgramList.get(0);
            }
            else
            {
                cmsProgram = null;
            }

            // 校验当前内容是否满足提审条件
            String errorCode = cntApplyWkfwCheck.checkCntWkfwApply(cmsProgram);
            if (!errorCode.equals(CntConstants.CNT_SUCCESS))
            {// 校验未通过
                //applyList.remove(programid);
                ApplyPool.getInstance().remove(programid);
                return errorCode;
            }

            // 更新内容工作流类型为申请
            CmsProgram program = new CmsProgram();
            program.setProgramindex(cmsProgram.getProgramindex());
            program.setWorkflow(1); // 工作流类型为：申请
            cmsProgramDS.updateCmsProgram(program);

            // 工作流处理
            // 工作流对象名称
            String pboName = cmsProgram.getNamecn() + "[" + cmsProgram.getCpid() + "]";

            // 工作流对象ID
            String pboId = String.valueOf(cmsProgram.getProgramindex());
            // 工作流对象类型
            String pboType = CntConstants.CNT_PBOTYPE_APPLY;
            // 工作流进程名称
            String processName = pboType + ":" + pboName;
            // 工作流参数变量
            StringBuffer wkfwParams = new StringBuffer();
            wkfwParams.append("callbackProcess,");
            wkfwParams.append(this.getCallbackProcess());
            String route = cmsProgramDS.getRoute(cmsProgram);
            wkfwParams.append(route); // 拼接工作流路由信息
            // 启动工作流
            WorkflowService wkflService = new WorkflowService();
            int result = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType,
                    "", wkfwParams.toString(), null);
            if (result < 0)
            {
                throw new BpsException(result + "", "Error occurred in startProcessInstance method");
            }
            // else
            // {// 工作流处理成功，调用内容统计接口
            // CmsCount.countContent(icmContentBusiness.getCpid(), IcmContentWkfwConstants.cms_oneaudit_count);
            // }
        }
        catch (Exception e)
        {
            //applyList.remove(programid);
            ApplyPool.getInstance().remove(programid);

            if (e instanceof BpsException){
                BpsException bpsEx = (BpsException) e;
                log.error("BpsException occurred in runApply method, programid=" + programid + ", errorCode=" + bpsEx.getCode());
                //内容提审流程执行失败 250005
                throw new Exception(CntConstants.CNT_APPLYWKFW_EXECERROR);
        }
            else{
                log.error("Exception occurred in runApply method, programid=" + programid);

            throw new Exception(CntConstants.CNT_FAIL); // "250099"
            }
        }

        //applyList.remove(programid);
        ApplyPool.getInstance().remove(programid);

        log.debug("CntApply: runApply end and programid=" + programid);
        return CntConstants.CNT_SUCCESS;// 申请工作流成功
    }

    /**
     * <p>
     * 审核待处理的内容新增申请
     * </p>
     * 
     * @param cntorderList 订单编号列表
     * @param taskidList 任务ID列表
     * @param wkfwParam 审核信息，包括审核意见（不可为空）、审核结果（通过/不通过，不可为空）
     * @return 返回审核工单结果，类型String，显示资源文件信息
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.cntorder
     * @see com.zte.cms.cntorder.model.Cntorder
     * @since ZXIN10 CMSV2.01.01
     */
    // 审核具体处理流程 返回操作结果码 工作流参数：内容ID、工单任务号、工单流程号、工单节点名称、审核意见、审核结果
    public String runAudit(String[] wkfwParam) throws Exception
    {
        log.debug("CntApply: runAudit begin, programid=" + wkfwParam[0]
                + ", taskid=" + wkfwParam[1] + ", processid=" + wkfwParam[2]
                + ", nodeName=" + wkfwParam[3] + ", handleMsg=" + wkfwParam[4]
                + ", handleEvent=" + wkfwParam[5]);
        String programid = wkfwParam[0];
        String taskid = wkfwParam[1];
        String processid = wkfwParam[2];
        String nodeName = wkfwParam[3]; // 工单节点名称
        String handleMsg = wkfwParam[4]; // 审核意见
        String handleEvent = wkfwParam[5]; // 审核结果

        try
        {
            //if (auditList.contains(contentid))
            if (AuditPool.getInstance().contains(programid))
            {// 内容正在被其它操作员审核
                return CntConstants.CNT_BEING_AUDITED;
            }
            //auditList.add(programid);

            // 据contentid获取当前内容的相关信息
            CmsProgram cmsProgram = new CmsProgram();
            List<CmsProgram> cmsProgramList = null;
            cmsProgram.setProgramid(programid);
            cmsProgramList = cmsProgramDS.getCmsProgramByCond(cmsProgram);
            if (null != cmsProgramList && cmsProgramList.size() > 0)
            {
                cmsProgram = cmsProgramList.get(0);
            }
            else
            {
                cmsProgram = null;
                // throw new Exception(CntConstants.CNT_FAIL);
            }
            // 判断当前内容是否满足审核条件
            String rtnCode = cntApplyWkfwCheck.checkCntWkfwAudit(cmsProgram, processid, nodeName);
            if (!CntConstants.CNT_SUCCESS.equals(rtnCode))
            {// 不满足审核条件
                AuditPool.getInstance().remove(programid);                
                return rtnCode;
            }

            // 启动工作流
            // 工作流处理
            WorkflowService wkflService = new WorkflowService();
            wkflService.completeTask(taskid, handleMsg, handleEvent);
            
            // else
            // {//工作流执行成功
            // String cpid = icmContentBusiness.getCpid();
            // if(handleEvent.equals("不通过"))
            // {
            // CmsCount.countContent(cpid,IcmContentWkfwConstants.cms_auditnotpass_count);//内容审核不通过时统计其数量
            // }
            // else
            // {
            // //查询待二审数量
            // //加载日志
            // ResourceMgt.addDefaultResourceBundle("log_icmcontent_resource");
            // if(nodeName.equals("一审")&&handleEvent.equals("通过"))
            // {
            // String isNoAudit = getSteps(icmContentBusiness);
            // int step1=Integer.parseInt(isNoAudit.substring(isNoAudit.indexOf(",")+1, isNoAudit.lastIndexOf("|")));
            // //step1=1表示走二审走三审 step1=0表示免二审免三审
            // int step=Integer.parseInt(isNoAudit.substring(isNoAudit.lastIndexOf(",")+1, isNoAudit.length()));
            // //step2=1表示走二审走三审 step2=2表示走二审免三审 step2=3表示免三审走二审
            // if(step1==0)
            // {//step1=0表示免二审免三审
            // log.debug("step1=0标示免二审免三审");
            // }
            // else if (step==1)
            // {
            // CmsCount.countContent(cpid,IcmContentWkfwConstants.cms_twoaudit_count);//内容审核通过时统计二审数量
            // CmsCount.countContent(cpid,IcmContentWkfwConstants.cms_threeaudit_count);//内容审核通过时统计三审数量
            // }
            // else if(step==2)
            // {
            // CmsCount.countContent(cpid,IcmContentWkfwConstants.cms_twoaudit_count);//内容审核通过时统计二审数量
            // }
            // else if(step==3)
            // {
            // CmsCount.countContent(cpid,IcmContentWkfwConstants.cms_threeaudit_count);//内容审核通过时统计三审数量
            // }
            // }
            // }
            // }
        }

        catch (Exception e)
        {
            //auditList.remove(programid);

            AuditPool.getInstance().remove(programid);
            //
            if (e instanceof BpsException){
                BpsException bpsEx = (BpsException)e;
                log.error("BpsException occurred in audit, errorCode=" + bpsEx.getCode());
                throw new Exception(CntConstants.CNT_AUDITWKFW_EXECERROR);
        }
            else{
                log.error("Exception occurred in audit", e);


            throw new Exception(CntConstants.CNT_FAIL);
            }
        }

        AuditPool.getInstance().remove(programid);
        log.debug("CntApply: runAudit end, programid=" + wkfwParam[0]
                + ", taskid=" + wkfwParam[1] + ", processid=" + wkfwParam[2]
                + ", nodeName=" + wkfwParam[3] + ", handleMsg=" + wkfwParam[4]
                + ", handleEvent=" + wkfwParam[5]);
        return CntConstants.CNT_SUCCESS;
    }

    public void setCntApplyWkfwCheck(ICntApplyWkfwCheck cntApplyWkfwCheck)
    {
        this.cntApplyWkfwCheck = cntApplyWkfwCheck;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

}

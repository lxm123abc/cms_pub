package com.zte.cms.service.service;

import java.util.List;

import com.zte.cms.service.dao.IServiceContentBindDAO;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceContentBind;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceContentBindDS extends DynamicObjectBaseDS implements IServiceContentBindDS
{
    private Log log = SSBBus.getLog(getClass());
    private IServiceContentBindDAO servicecnttbinddao = null;

    // 查询服务和内容的绑定关系列表

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = servicecnttbinddao.pageInfoQueryCntBindService(service, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceContentBind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 查询满足绑定条件的内容，用于服务和内容的绑定
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get service page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = servicecnttbinddao.pageInfoQueryContentForServiceBind(service, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceContentBind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    public void setServicecnttbinddao(IServiceContentBindDAO servicecnttbinddao)
    {
        this.servicecnttbinddao = servicecnttbinddao;
    }

    /**
     * 查询服务和栏目的关联关系列表
     * 
     * @param service Service
     * @param start int
     * @param pageSize int 
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
     public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
     {
         log.debug("get service and category map by condition starting...");
         PageInfo pageInfo = null;
         try
         {
             pageInfo = servicecnttbinddao.pageInfoQueryServiceCategoryMap(service, start, pageSize, puEntity);
         }
         catch (DAOException daoEx)
         {
             log.error("dao exception:", daoEx);
             // TODO 根据实际应用，可以在此处添加异常国际化处理
             throw new DomainServiceException(daoEx);
         }
         TableDataInfo tableInfo = new TableDataInfo();
         tableInfo.setData((List<ServiceContentBind>) pageInfo.getResult());
         tableInfo.setTotalCount((int) pageInfo.getTotalCount());
         log.debug("get service page info by condition end");
         return tableInfo;
     }
     
     /**
      * 
      * @param service Service
      * @param start int
      * @param pageSize int
      * @param puEntity PageUtilEntity
      * @return PageInfo
      * @throws DAOException DAOException
      */
     public TableDataInfo serviceCntMapQuery(Service service, int start, int pageSize,
             PageUtilEntity puEntity) throws DomainServiceException
     {
         log.debug("query services from content information given starting...");
         PageInfo pageInfo = null;
         try
         {
             pageInfo = servicecnttbinddao.serviceCntMapQuery(service, start, pageSize, puEntity);
         } catch (DAOException daoEx)
         {
             log.error("dao exception:", daoEx);
             // TODO 根据实际应用，可以在此处添加异常国际化处理
             throw new DomainServiceException(daoEx);
         }
         TableDataInfo tableInfo = new TableDataInfo();
         tableInfo.setData((List<ServiceContentBind>) pageInfo.getResult());
         tableInfo.setTotalCount((int) pageInfo.getTotalCount());
         log.debug("query services from content information given ends...");
         return tableInfo;
     }
     
     //返回是否绑定了内容
     public boolean isBingContent(Service service ) throws DomainServiceException
     {
         log.debug("query services from content information given ends...");
         try
        {
            servicecnttbinddao.isBingContent(service);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
         log.debug("query services from content information given ends...");
         
        return false;
         
     }
}

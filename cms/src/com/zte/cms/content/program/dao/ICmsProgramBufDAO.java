package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsProgramBufDAO
{
    /**
     * 新增CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void insertCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 新增CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void insertCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException;

    /**
     * 根据主键更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void updateCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 根据主键更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void updateCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException;

    /**
     * 根据条件更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 根据条件更新CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DAOException;

    /**
     * 根据主键删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 根据主键删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException;

    /**
     * 根据条件删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 根据条件删除CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DAOException;

    /**
     * 根据主键查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @return 满足条件的CmsProgramBuf对象
     * @throws DAOException dao异常
     */
    public CmsProgramBuf getCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException;

    /**
     * 根据条件查询CmsProgramBuf对象
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @return 满足条件的CmsProgramBuf对象集
     * @throws DAOException dao异常
     */
    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException;

    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询CmsProgramBuf对象，作为查询条件的参数
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsProgramBuf对象，作为查询条件的参数
     * 
     * @param cmsProgramBuf CmsProgramBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
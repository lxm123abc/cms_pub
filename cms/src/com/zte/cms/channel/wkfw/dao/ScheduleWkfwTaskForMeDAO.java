package com.zte.cms.channel.wkfw.dao;

import java.util.List;


import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwTaskForMe;

import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ScheduleWkfwTaskForMeDAO extends DynamicObjectBaseDao implements IScheduleWkfwTaskForMeDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#queryStrategyWkfwTaskForMeListByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public List<ScheduleWkfwTaskForMe> queryScheduleWkfwTaskForMeListByCond(
            ScheduleWkfwCondition scheduleWkfwTaskForMeCondition) throws DAOException
    {
        log.debug("query ChannelWkfwTaskForMeList by condition starting...");
        List<ScheduleWkfwTaskForMe> rList = (List<ScheduleWkfwTaskForMe>) super.queryForList(
                "queryScheduleWkfwTaskForMeListByCond", scheduleWkfwTaskForMeCondition);
        log.debug("query ChannelWkfwTaskForMeList by condition end");
        return rList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#queryStrategyWkfwTaskForMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public int queryscheduleWkfwTaskForMeListCntByCond(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DAOException
    {
        log.debug("query ChannelWkfwTaskForMeList count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryScheduleWkfwProcessWithMeListCntByCond",
                scheduleWkfwTaskForMeCondition)).intValue();
        log.debug("query ChannelWkfwTaskForMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query ChannelWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryScheduleWkfwProcessWithMeListCntByCond",
                scheduleWkfwTaskForMeCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ScheduleWkfwTaskForMe> rsList = (List<ScheduleWkfwTaskForMe>) super.pageQuery(
                    "queryScheduleWkfwProcessWithMeListByCond", scheduleWkfwTaskForMeCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query strategyWkfwTaskForMeList page info by condition end");
        return pageInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryScheduleWkfwTaskForMeListByCond", "queryScheduleWkfwTaskForMeListCntByCond",
                scheduleWkfwTaskForMeCondition, start, pageSize, puEntity);
    }
}
package com.zte.cms.check;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.server.model.CmsServer;
import com.zte.cms.server.model.DbServer;
import com.zte.cms.server.service.ICmsServerDS;
import com.zte.ismp.common.util.DbUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.ResourceMgt;
import com.zte.cms.server.model.DiskInfo;

/**
 * <p>
 * 文件名称: ServerCheck.java
 * </p>
 * <p>
 * 文件描述: 调用checkserver类实现监控服务器
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 调用checkserver类实现监控服务器
 * </p>
 * <p>
 * 其他说明: 供定时任务发起时调用
 * </p>
 * <p>
 * 完成日期：2011年1月20日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 黄杰弟
 */
public class ServerCheck implements IServerCheck {

	private ICmsServerDS cmsServerDS = null;
	private Thread mythread = null;
	private Thread cleanthread = null;

	// 日志
	private Log log = SSBBus.getLog(getClass());

	public ICmsServerDS getCmsServerDS() {
		return cmsServerDS;
	}

	public void setCmsServerDS(ICmsServerDS cmsServerDS) {
		this.cmsServerDS = cmsServerDS;
	}

	public ServerCheck() {
		// mythread = new MyThreadLS();
		// mythread.start();
		//	
		// cleanthread = new CleanThreadLS();
		// cleanthread.start();
	}

	public DbServer checkall() throws Exception {
		check();
		return checkDbServer();
	}

	/**
	 * 检查各个服务器的cpu和内存使用情况
	 */
	public String check() throws Exception {

		String result = "";
		try {
			CheckServer cs = new CheckServer();

			List<CmsServer> list = cmsServerDS.getCmsServerByCond(null);
			CmsServer tempserver = null;
			CmsServer serverExist = null;
			String loginres = "";
			Long caculate = null;
			Long free = null;
			Long total = null;

			for (int i = 0; i < list.size(); i++) {
				try {
					tempserver = list.get(i);
					cs.setIp(tempserver.getInipaddress());
					cs.setUser(tempserver.getUseraccount());
					cs.setPassword(tempserver.getUserpassword());
					cs.setPort("23");
					// System.out.println("------------------ip = "+tempserver.getInipaddress());
					loginres = cs.login();

					if (!"error".equals(loginres)) {
						cs.excute();
						cs.disconnect();

						if ("4".equals(cs.getExecuteresult())) {
							tempserver.setCpuusage(cs.getPercent());

							free = Long.parseLong(cs.getFreemem());
							tempserver.setFreememary(cs.getFreemem());

							total = Long.parseLong(cs.getTotalmem());
							tempserver.setTotalmemary(cs.getTotalmem());

							caculate = total - free;
							tempserver.setUsedmemary(String.valueOf(caculate));
							tempserver.setParam1("0");
						} else {
							tempserver.setParam1("1");// 服务器异常

						}
					} else {

						tempserver.setParam1("1");// 服务器异常
					}

					try {
						serverExist = new CmsServer();
						serverExist.setServerindex(tempserver.getServerindex());

						serverExist = cmsServerDS.getCmsServer(serverExist);

						if (serverExist != null) {
							if ("1".equals(tempserver.getParam1())) {
								tempserver.setCpuusage("");
								tempserver.setTotalmemary("");
								tempserver.setUsedmemary("");
								tempserver.setFreememary("");
							}
							cmsServerDS.updateCmsServer(tempserver);
						}
					} catch (Exception e) {
						log
								.error("error while updating cmsserver in servercheck");
						log.error(e);
					}

				} catch (Exception e) {
					log.error("error occur in servercheck in the for cycle");
					log.error(e);
					tempserver.setParam1("1");
					tempserver.setCpuusage("");
					tempserver.setTotalmemary("");
					tempserver.setUsedmemary("");
					tempserver.setFreememary("");
					cmsServerDS.updateCmsServer(tempserver);
				}
			}

		} catch (Exception e) {
			log.error("error occur in servercheck");
			log.error(e);
		}
		return "";
	}

	/**
	 * 检查数据库表空间的大小和数据库连接数
	 */
	public DbServer checkDbServer() throws Exception {
		log.debug("check DbServer start................");
		DbServer dbserver = new DbServer();
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
		try {
			DbUtil dbutil = new DbUtil();
			String sql = "SELECT D.TABLESPACE_NAME \"tablespace\", D.STATUS \"status\","
					+ "(A.BYTES / 1024 / 1024 /1024) as \"totalsize\","
					+ "((A.BYTES - DECODE(F.BYTES, NULL, 0, F.BYTES)) / 1024 / 1024 /1024) as \"usedsize\","
					+ "(DECODE(F.BYTES, NULL, 0, F.BYTES) / 1024 / 1024 /1024) as \"freesize\", "
					+ "((A.BYTES - DECODE(F.BYTES, NULL, 0, F.BYTES)) / 1024 / 1024)/(A.BYTES / 1024 / 1024) as \"usage\","
					+ "DECODE(sign(((A.BYTES - DECODE(F.BYTES, NULL, 0, F.BYTES)) / 1024 / 1024)/(A.BYTES / 1024 / 1024)-0.9),1,'1','0') as \"tips\" "
					+ "FROM SYS.DBA_TABLESPACES D, SYS.SM$TS_AVAIL A, SYS.SM$TS_FREE F "
					+ "WHERE D.TABLESPACE_NAME = A.TABLESPACE_NAME AND D.tablespace_name = 'ZXIN_DATA' "
					+ "AND F.TABLESPACE_NAME (+) = D.TABLESPACE_NAME";
			List resultList = dbutil.getQuery(sql);
			Map rsMap = (HashMap) resultList.get(0);
			String usedsize = String.valueOf(rsMap.get("usedsize"));
			dbserver.setUsedsize(String.valueOf(df.format(Double
					.parseDouble(usedsize))));
			String freesize = String.valueOf(rsMap.get("freesize"));
			freesize = String.valueOf(df.format(Double.parseDouble(freesize)));
			dbserver.setFreesize(freesize);

			sql = "select value from v$parameter where name = 'processes'";

			resultList = dbutil.getQuery(sql);

			String totalconnect = String.valueOf(((HashMap) resultList.get(0))
					.get("value"));

			sql = "select count(*) from v$process";

			String freeconnect = dbutil.checkconflict(sql);

			freeconnect = String.valueOf(Long.valueOf(totalconnect)
					- Long.valueOf(freeconnect));

			dbserver.setTotalconnect(totalconnect);
			dbserver.setFreeconnect(freeconnect);

			// String ipaddr = String.valueOf(ConfigUtil.get(CheckConstant.DB_SERVER_IP));
			// CheckServer cs = new CheckServer();
			// cs.setIp(ipaddr);
			// cs.setUser("zxin10");
			// cs.setPassword("zxin10");
			// cs.setPort("23");
			//			
			// String result = "0";
			// String loginres = cs.login();
			// if(!"error".equals(loginres)){
			// result = cs.excuteHasReturn("df -mh /");
			// cs.disconnect();
			// }else{
			// throw new Exception("10002");
			// }
			//			
			// //String result = CycleJob.exeCmd("df -m /");
			//			
			// result = getfreesize(result);
			//			
			// dbserver.setExtendsize(result);

		} catch (Exception e) {
			log.error("error occur while checking DbServer");
			log.error(e);
			throw e;
		}

		log.debug("check DbServer end..................");
		return dbserver;
	}

	// 判断设备的文件夹是否存在，如果存在，检测文件夹的大小并返回
	public String getFolderSize(String devicename) throws Exception {
		String result = "0";
		try {
			// int Gcount = 0;
			// int Tcount = 0;
			// int Mcount = 0;
			// int Kcount = 0;
			// int totalcount = 0;
			String temp = "";
			String path = GlobalConstants.getMountPoint() + devicename;
			// Gcount = checkContainG(path);
			// Tcount = checkContainT(path);
			// Mcount = checkContainM(path);
			// Kcount = checkContainK(path);
			//		    
			// totalcount = Gcount + Tcount + Mcount + Kcount;
			//		    
			temp = exeCmd("df -h " + GlobalConstants.getMountPoint()
					+ devicename);

			result = getfreesize(temp, path);

			if (result == null || result == "") {
				result = "0";
			}
		} catch (Exception e) {
			log.error(e);
			result = "0";
		}
		return result;
	}

	/**
	 * 获取/根目录的可用控件大小，单位是G
	 * 
	 * @param str
	 * @return
	 */
	/*
	 * private String getfreesize(String str, int totalcount){ // System.out.println("************************"); //
	 * System.out.println(str); // System.out.println("***************************"); String result = ""; String temp =
	 * ""; StringBuffer sb = new StringBuffer(); int flag = 0; int flagT = 0; int countperc = 0; int largest =
	 * totalcount + 3;
	 * 
	 * for(int i = 0; i < str.length(); i++) { temp = str.substring(i, i+1); if("G".equals(temp)) { if(++flag >
	 * totalcount && flag <= largest) { result = getsize(sb.toString()); result = justify(result); //break; } }else
	 * if("T".equals(temp)){ if(++flag > totalcount && flag <= largest) { result = getsize(sb.toString()); result =
	 * justify(String.valueOf(Float.valueOf(result)*1024)); //break; } }else if("M".equals(temp)){ if(++flag >
	 * totalcount && flag <= largest) { result = getsize(sb.toString()); result = justify(result); result =
	 * String.valueOf(Long.valueOf(result)/1024); //break; } }else if("K".equals(temp)){ if(++flag > totalcount && flag <=
	 * largest) { result = getsize(sb.toString()); result = justify(result); result =
	 * String.valueOf(Long.valueOf(result)/1024/1024); //break; } }else{ sb.append(temp); } }
	 * 
	 * return result; }
	 */

	// 从df -h命令得到的结果中，得到可用容量的大小，作为设备的容量
	private String getfreesize(String str, String devicepath) {
		int mountedOnLength = 0; // 设备路径的长度
		String subStr = ""; // 去除设备路径后剩下的子串
		String ipaddress = "";
		StringBuffer avalible = new StringBuffer(); // 可用容量的大小
		String avastring = "";
		//取出总容量的大小
		String[] strArray = null;
		//strArray = str.trim().split(" ");		
	    String []splitpart = str.trim().split("%");
	    String middlepart = "";
	    if(splitpart!=null&&splitpart.length>0&&splitpart.length==3){
	    	middlepart = splitpart[1];
	    }
	    
	    
	    strArray = middlepart.split(" ");
		String needstr = "";//物理区域总容量		
		
		int countnull = 0;//得到物理区域总容量
		for (int count = strArray.length-1; count > 0; count--) {
			if (!strArray[count].equals("")) {
				countnull++;
				
				if (countnull == 2) {
					avastring = strArray[count];
					
				}
				if (countnull == 4) {
					needstr = strArray[count];
					
				}
				if (countnull == 5) {
					ipaddress = strArray[count];
					
				}
			}

		}
		System.out.println("         needstr                  " + needstr);
		int beginnum = -1;
		int endnum = 0;
		for(int ipcount=0;ipcount<ipaddress.length();ipcount++){
			if(checknum(ipaddress.charAt(ipcount))&&(beginnum==-1)){//是数字或.
				beginnum = ipcount;
				
			}
			if((beginnum!=-1)&&(!checknum(ipaddress.charAt(ipcount)))){
				endnum = ipcount;
				break;
			}
		}
		String ipstring = "";
		if(beginnum<endnum){
			ipstring = ipaddress.substring(beginnum,endnum);
		}
		
		// 把信息的最后一段，也就是挂载点截取出来
//		mountedOnLength = strArray[strArray.length - 1].length();
	
//		subStr = str.substring(0, str.length() - mountedOnLength - 1);
//		subStr = subStr.trim();
//		int j = 0; // 记录半分比的长度，以便于去除
//		for (int i = subStr.length() - 1; i >= 0; i--) {
//			String charaOfPercentage = subStr.substring(i, i + 1); // 百分比的每一个字符。由于下面要用equals函数，因此这里要用substring得到每个字符串子串
//			if (!((" ").equals(charaOfPercentage))) {
//				j++;
//			} else {
//				break; // 找到使用百分比之前的空格退出循环
//			}
//		}
//		subStr = str.substring(0, subStr.length() - j - 1); // 得到的结果中去除百分比后的字符串
//		subStr = subStr.trim();

		// 得到可用容量
//		for (int m = subStr.length() - 1; m >= 0; m--) {
//			String charaOfAvali = subStr.substring(m, m + 1); // 可用容量的每一个字符。由于下面要用equals函数，因此这里要用substring得到每个字符串子串
//			if (!" ".equals(charaOfAvali)) {
//				avalible.append(charaOfAvali);
//			} else {
//				break; // 找到可用容量后退出循环
//			}
//		}
		// 这时得到的charaOfAvali值为实际容量字符串的逆转，比如说容量为84G,charaOfAvali的值为G48，需要先将其反转
		// 得到去除单位后的数字字符串，然后反转
//		String strNum = avalible.substring(1, avalible.length());
//		String strNum = avastring;
//		StringBuffer avalibleNum = new StringBuffer();
//		int strNumLength = strNum.length();
//		efor (int strNumIdx = strNumLength - 1; strNumIdx >= 0; strNumIdx--) {
//			avalibleNum.append(strNum.substring(strNumIdx, strNumIdx + 1));
//		}
		String strNum = avastring.toString();
		int strlength = strNum.length();
		
		String result = null; // 计算后的结果
		switch (strNum.charAt(strlength-1)) {
		case 'G':
			result = justify(strNum.substring(0,strNum.length() -1));
			break;
		case 'T':
			result = justify(String.valueOf(Float.valueOf(strNum.substring(0,strNum.length() -1)) * 1024));
			break;
		case 'M':
			result = String.valueOf(Long.valueOf(strNum.substring(0,strNum.length() -1)) / 1024);
			result = justify(result);
			break;
		case 'K':
			result = String.valueOf(Long.valueOf(strNum.substring(0,strNum.length() -1)) / 1024 / 1024);
			result = justify(result);
			break;

		}

		char unit = needstr.charAt(needstr.length()-1);
		switch (unit) {
		case 'G':
			needstr = justify(needstr.substring(0,needstr.length()-1));
			break;
		case 'T':
			needstr = justify(String.valueOf(Float.valueOf(needstr.substring(0,needstr.length()-1)) * 1024));
			break;
		case 'M':
			needstr = String.valueOf(Long.valueOf(needstr.substring(0,needstr.length()-1)) / 1024);
			needstr = justify(needstr);
			break;
		case 'K':
			needstr = String.valueOf(Long.valueOf(needstr.substring(0,needstr.length()-1)) / 1024 / 1024);
			needstr = justify(needstr);
			break;

		}
		return result+";"+needstr+";"+ipstring;

	}

	public String justify(String source) {
		String result;
		int i = source.length();

		i = source.indexOf('.') == -1 ? source.length() : source.indexOf('.');

		result = source.substring(0, i);
		return result;
	}

	/**
	 * 获取数字
	 * 
	 * @param str
	 * @return
	 */
	public String getsize(String str) {
		String size = "";
		char temp;
		for (int i = str.length() - 1; i >= 0; i--) {
			temp = str.charAt(i);
			if (checknum(temp)) {
				size = temp + size;
			} else {
				return size;
			}
		}
		return size;
	}

	/**
	 * 检查是否是数字
	 * 
	 * @param str String类型
	 * @return boolean类型
	 */
	private boolean checknum(char chr) {

		String mask = "0123456789.";

		if (mask.indexOf(chr) != -1) {
			return true;
		}

		return false;
	}

	/**
	 * 检查字符串包含G的个数
	 * 
	 * @param str String类型
	 * @return int类型
	 */
	private int checkContainG(String str) {

		String mask = "G";
		int countG = 0;
		for (int i = 0; i < str.length(); i++) {
			if (mask.indexOf(str.charAt(i)) != -1) {
				countG++;
			}
		}

		return countG;
	}

	/**
	 * 检查字符串包含T的个数
	 * 
	 * @param str String类型
	 * @return int类型
	 */
	private int checkContainT(String str) {

		String mask = "T";
		int countT = 0;
		for (int i = 0; i < str.length(); i++) {
			if (mask.indexOf(str.charAt(i)) != -1) {
				countT++;
			}
		}

		return countT;
	}

	/**
	 * 检查字符串包含M的个数
	 * 
	 * @param str String类型
	 * @return int类型
	 */
	private int checkContainM(String str) {

		String mask = "M";
		int countM = 0;
		for (int i = 0; i < str.length(); i++) {
			if (mask.indexOf(str.charAt(i)) != -1) {
				countM++;
			}
		}

		return countM;
	}

	/**
	 * 检查字符串包含K的个数
	 * 
	 * @param str String类型
	 * @return int类型
	 */
	private int checkContainK(String str) {

		String mask = "K";
		int countK = 0;
		for (int i = 0; i < str.length(); i++) {
			if (mask.indexOf(str.charAt(i)) != -1) {
				countK++;
			}
		}

		return countK;
	}

	/**
	 * 执行shell
	 * 
	 * @param command String
	 * @return String
	 * @throws Exception Exception
	 */
	public static String exeCmd(String command) throws Exception {
		String retCmdInfo = null;
		Process process = null;
		try {

			Runtime runtime = Runtime.getRuntime();

			process = runtime.exec(command);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String line;
			String test = in.toString();
			int index = 0;

			while ((line = in.readLine()) != null) {

				if (retCmdInfo == null) {
					retCmdInfo = line;
				} else {
					retCmdInfo += ("\r\n" + line);
				}

			}
			process.destroy();

		} catch (Exception e) {
			if (process != null) {
				process.destroy();
			}
			throw e;
		}
		return retCmdInfo;
	}

	// 监控硬盘使用情况，连上服务器，用df -h命令，获取所需要的信息，返回到页面
	public List<DiskInfo> getServerInfo(CmsServer cmsserver) throws Exception {
		ResourceMgt.addDefaultResourceBundle(CheckConstant.CMS_SERVER_RESOURCE);
		String result = "";
		String loginres = "";
		char prompt;
		CheckServer cs = new CheckServer();
		CmsServer tempserver = cmsServerDS.getCmsServer(cmsserver);
		// 判断该条记录在数据库中是否存在
		if (null == tempserver) {
			result = ResourceMgt
					.findDefaultText(CheckConstant.SERVER_DOES_NOT_EXIST_IN_DATEBASE);
		}
		cs.setIp(tempserver.getInipaddress());
		cs.setUser(tempserver.getUseraccount());
		cs.setPassword(tempserver.getUserpassword());
		cs.setPort("23"); // 配置连接参数
		loginres = cs.login();

		if ("error".equals(loginres)) {
			return null;
		} else {
			prompt = loginres.charAt(loginres.length() - 1);
			result = cs.getResult("df -h"); // 执行该命令，并返回执行后得到的信息
			cs.disconnect();
		}
		if ("".equals(result.trim())) {
			return null;
		}
		String[] str = result.split("\r\n");
		String rtn = null; // 返回结果
		List<DiskInfo> ListDiskInfo = new ArrayList();
		// 得到result的第一行，将英文的转换成中文
		// 对result进行处理，使其看起来美观
		// 将第一列去掉
		// 去除其中的空格，然后将其填入列表
		for (int i = 1; i < str.length; i++) {
			String[] strColumns;
			strColumns = str[i].split(" ");
			DiskInfo diskinfo = new DiskInfo();
			int k = 0;
			for (int j = 0; j < strColumns.length; j++) {
				if (!"".equals(strColumns[j])) {
					switch (k) {
					case 0:
						diskinfo.setFilesystem(strColumns[j]);
						break;
					case 1:
						diskinfo.setSize(strColumns[j]);
						break;
					case 2:
						diskinfo.setUsed(strColumns[j]);
						break;
					case 3:
						diskinfo.setAvail(strColumns[j]);
						break;
					case 4:
						diskinfo.setUsepercentagy(strColumns[j]);
						break;
					case 5:
						diskinfo.setMountedon(strColumns[j]);
						break;
					default:
						break;
					}

					k++;

				}
			}
			ListDiskInfo.add(diskinfo);

		}
		// 由于读取的信息存在换行情况，因此生成的ListDiskInfo列表有可能因为换行的原因有误，因此需要检验并将换行的合成一行
		// 循环列表，检查每一个对象的第二个属性，如果为空，则说明该服务器的信息在读取时有换行，因此将下一个对象的数据填入该对象，并将下一个对象删除
		int lengthList = ListDiskInfo.size();
		for (int i = 0; i < lengthList; i++) {
			String totalSize = ListDiskInfo.get(i).getSize();
			if ("".equals(totalSize) || (null == totalSize)) {
				DiskInfo diskInfo = ListDiskInfo.get(i + 1);
				ListDiskInfo.get(i).setSize(diskInfo.getFilesystem());
				ListDiskInfo.get(i).setUsed(diskInfo.getSize());
				ListDiskInfo.get(i).setAvail(diskInfo.getUsed());
				ListDiskInfo.get(i).setUsepercentagy(diskInfo.getAvail());
				ListDiskInfo.get(i).setMountedon(diskInfo.getUsepercentagy());
				ListDiskInfo.remove(i + 1);
				lengthList = ListDiskInfo.size();
			}
		}
		return ListDiskInfo;

	}

}

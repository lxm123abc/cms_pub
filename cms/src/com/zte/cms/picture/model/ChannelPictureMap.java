package com.zte.cms.picture.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ChannelPictureMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.String mappingid;
    private java.lang.Long channelindex;
    private java.lang.String channelid;
    private java.lang.Long pictureindex;
    private java.lang.String pictureid;
    private java.lang.Integer maptype;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;

	private java.lang.String picturecode;
	private java.lang.String cpcontentid;
	private java.lang.String targetcode;
	private java.lang.String onlinetimeStartDate;
	private java.lang.String onlinetimeEndDate;
	private java.lang.String cntofflinestarttime;
	private java.lang.String cntofflineendtime;
	private java.lang.String createStarttime;
	private java.lang.String createEndtime;
	private java.lang.String targetname;
	private java.lang.String targetid;
    private java.lang.Integer targettype;
	private java.lang.Long syncindex;
	private java.lang.Long relateindex;
	private java.lang.Integer objecttype;
	private java.lang.Long objectindex;
	private java.lang.String objectid;
	private java.lang.String elementid;
	private java.lang.String parentid;
	private java.lang.Long targetindex;
	private java.lang.Integer operresult;
	private java.lang.Long reserve01;
	private java.lang.String reserve02;
    private java.lang.Integer typetarget;	
	private java.lang.String typePlatform;
	private java.lang.Integer platform;
	private java.lang.Integer isfiledelete;	
    private java.lang.String channelname;	
    private java.lang.Integer targetstatus;
	
	public java.lang.Integer getTargetstatus()
	{
		return targetstatus;
	}
	public void setTargetstatus(java.lang.Integer targetstatus)
	{
		this.targetstatus = targetstatus;
	}
	public java.lang.String getPicturecode()
	{
		return picturecode;
	}

	public void setPicturecode(java.lang.String picturecode)
	{
		this.picturecode = picturecode;
	}

	public java.lang.String getCpcontentid()
	{
		return cpcontentid;
	}

	public void setCpcontentid(java.lang.String cpcontentid)
	{
		this.cpcontentid = cpcontentid;
	}

	public java.lang.String getTargetcode()
	{
		return targetcode;
	}

	public void setTargetcode(java.lang.String targetcode)
	{
		this.targetcode = targetcode;
	}

	public java.lang.String getOnlinetimeStartDate()
	{
		return onlinetimeStartDate;
	}

	public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
	{
		this.onlinetimeStartDate = onlinetimeStartDate;
	}

	public java.lang.String getOnlinetimeEndDate()
	{
		return onlinetimeEndDate;
	}

	public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
	{
		this.onlinetimeEndDate = onlinetimeEndDate;
	}

	public java.lang.String getCntofflinestarttime()
	{
		return cntofflinestarttime;
	}

	public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
	{
		this.cntofflinestarttime = cntofflinestarttime;
	}

	public java.lang.String getCntofflineendtime()
	{
		return cntofflineendtime;
	}

	public void setCntofflineendtime(java.lang.String cntofflineendtime)
	{
		this.cntofflineendtime = cntofflineendtime;
	}

	public java.lang.String getCreateStarttime()
	{
		return createStarttime;
	}

	public void setCreateStarttime(java.lang.String createStarttime)
	{
		this.createStarttime = createStarttime;
	}

	public java.lang.String getCreateEndtime()
	{
		return createEndtime;
	}

	public void setCreateEndtime(java.lang.String createEndtime)
	{
		this.createEndtime = createEndtime;
	}

	public java.lang.String getTargetname()
	{
		return targetname;
	}

	public void setTargetname(java.lang.String targetname)
	{
		this.targetname = targetname;
	}

	public java.lang.String getTargetid()
	{
		return targetid;
	}

	public void setTargetid(java.lang.String targetid)
	{
		this.targetid = targetid;
	}

	public java.lang.Integer getTargettype()
	{
		return targettype;
	}

	public void setTargettype(java.lang.Integer targettype)
	{
		this.targettype = targettype;
	}

	public java.lang.Long getSyncindex()
	{
		return syncindex;
	}

	public void setSyncindex(java.lang.Long syncindex)
	{
		this.syncindex = syncindex;
	}

	public java.lang.Long getRelateindex()
	{
		return relateindex;
	}

	public void setRelateindex(java.lang.Long relateindex)
	{
		this.relateindex = relateindex;
	}

	public java.lang.Integer getObjecttype()
	{
		return objecttype;
	}

	public void setObjecttype(java.lang.Integer objecttype)
	{
		this.objecttype = objecttype;
	}

	public java.lang.Long getObjectindex()
	{
		return objectindex;
	}

	public void setObjectindex(java.lang.Long objectindex)
	{
		this.objectindex = objectindex;
	}

	public java.lang.String getObjectid()
	{
		return objectid;
	}

	public void setObjectid(java.lang.String objectid)
	{
		this.objectid = objectid;
	}

	public java.lang.String getElementid()
	{
		return elementid;
	}

	public void setElementid(java.lang.String elementid)
	{
		this.elementid = elementid;
	}

	public java.lang.String getParentid()
	{
		return parentid;
	}

	public void setParentid(java.lang.String parentid)
	{
		this.parentid = parentid;
	}

	public java.lang.Long getTargetindex()
	{
		return targetindex;
	}

	public void setTargetindex(java.lang.Long targetindex)
	{
		this.targetindex = targetindex;
	}

	public java.lang.Integer getOperresult()
	{
		return operresult;
	}

	public void setOperresult(java.lang.Integer operresult)
	{
		this.operresult = operresult;
	}

	public java.lang.Long getReserve01()
	{
		return reserve01;
	}

	public void setReserve01(java.lang.Long reserve01)
	{
		this.reserve01 = reserve01;
	}

	public java.lang.String getReserve02()
	{
		return reserve02;
	}

	public void setReserve02(java.lang.String reserve02)
	{
		this.reserve02 = reserve02;
	}

	public java.lang.Integer getTypetarget()
	{
		return typetarget;
	}

	public void setTypetarget(java.lang.Integer typetarget)
	{
		this.typetarget = typetarget;
	}

	public java.lang.String getTypePlatform()
	{
		return typePlatform;
	}

	public void setTypePlatform(java.lang.String typePlatform)
	{
		this.typePlatform = typePlatform;
	}

	public java.lang.Integer getPlatform()
	{
		return platform;
	}

	public void setPlatform(java.lang.Integer platform)
	{
		this.platform = platform;
	}

	public java.lang.Integer getIsfiledelete()
	{
		return isfiledelete;
	}

	public void setIsfiledelete(java.lang.Integer isfiledelete)
	{
		this.isfiledelete = isfiledelete;
	}

	public java.lang.String getChannelname()
	{
		return channelname;
	}

	public void setChannelname(java.lang.String channelname)
	{
		this.channelname = channelname;
	}
    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getChannelindex()
    {
        return channelindex;
    }

    public void setChannelindex(java.lang.Long channelindex)
    {
        this.channelindex = channelindex;
    }

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.Long getPictureindex()
    {
        return pictureindex;
    }

    public void setPictureindex(java.lang.Long pictureindex)
    {
        this.pictureindex = pictureindex;
    }

    public java.lang.String getPictureid()
    {
        return pictureid;
    }

    public void setPictureid(java.lang.String pictureid)
    {
        this.pictureid = pictureid;
    }

    public java.lang.Integer getMaptype()
    {
        return maptype;
    }

    public void setMaptype(java.lang.Integer maptype)
    {
        this.maptype = maptype;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }
    
    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("mappingid", "MAPPINGID");
        this.addRelation("channelindex", "CHANNELINDEX");
        this.addRelation("channelid", "CHANNELID");
        this.addRelation("pictureindex", "PICTUREINDEX");
        this.addRelation("pictureid", "PICTUREID");
        this.addRelation("maptype", "MAPTYPE");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
    }
}

package com.zte.ucm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil
{
  public static String getDate(String paramString)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat(paramString);

    String str = localSimpleDateFormat.format(new Date());
    return str;
  }
}

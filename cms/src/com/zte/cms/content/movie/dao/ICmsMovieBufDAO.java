package com.zte.cms.content.movie.dao;

import java.util.List;

import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsMovieBufDAO
{
    /**
     * 新增CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void insertCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 新增CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void insertCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException;

    /**
     * 根据主键更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void updateCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 根据主键更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void updateCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException;

    /**
     * 根据条件更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 根据条件更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DAOException;

    /**
     * 根据主键删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void deleteCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 根据主键删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public void deleteCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DAOException;

    /**
     * 根据条件删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 根据条件删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DAOException;

    /**
     * 根据主键查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @return 满足条件的CmsMovieBuf对象
     * @throws DAOException dao异常
     */
    public CmsMovieBuf getCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DAOException;

    /**
     * 根据条件查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @return 满足条件的CmsMovieBuf对象集
     * @throws DAOException dao异常
     */
    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DAOException;

    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf, PageUtilEntity puEntity) throws DAOException;

    /**
     * 根据条件分页查询CmsMovieBuf对象，作为查询条件的参数
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsMovieBuf对象，作为查询条件的参数
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
package com.zte.cms.content.apply;

import com.zte.cms.content.program.model.CmsProgram;

/**
 * <p>
 * 文件名称: ICntApplyWkfwCheck.java
 * </p>
 * <p>
 * 文件描述: 内容申请工作流流程校验接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供校验内容是否可以提交审核、是否可以审核的接口
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public interface ICntApplyWkfwCheck
{

    /**
     * <p>
     * 检查内容是否可以提交审核
     * </p>
     * 
     * @param orderindex Long类型
     * @return 返回检查结果 <br/>350000：校验通过 <br/>350001：内容不存在 <br/>350002：内容状态不正确 <br/>350003：内容不在工作流中或所处工作流不正确
     *         <br/>350004：内容工作流流程状态不正确
     * @throws Exception抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String checkCntWkfwApply(CmsProgram cmsProgram) throws Exception;

    /**
     * <p>
     * 检查内容是否可以发起申请工作流的审核操作
     * </p>
     * 
     * @param orderindex Long类型
     * @return 返回检查结果 <br/>350000：校验通过 <br/>350001：内容不存在 <br/>350002：内容状态不正确 <br/>350003：内容不在工作流中或所处工作流不正确
     *         <br/>350004：内容工作流流程状态不正确
     * @throws Exception抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String checkCntWkfwAudit(CmsProgram cmsProgram, String processid, String nodeName) throws Exception;
}

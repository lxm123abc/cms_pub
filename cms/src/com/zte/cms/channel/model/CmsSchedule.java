package com.zte.cms.channel.model;

import net.sf.cglib.beans.BeanCopier;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsSchedule extends DynamicBaseObject
{
    private java.lang.Long scheduleindex;
    private java.lang.String scheduleid;
    private java.lang.String channelid;
    private java.lang.String programname;
    private java.lang.String startdate;
    private java.lang.String starttime;
    private java.lang.String duration;
    private java.lang.Integer isvalid;
    private java.lang.String cpcontentid;
    private java.lang.String description;
    private java.lang.String unicontentid;
    private java.lang.String originalnamecn;
    private java.lang.Integer contenttype;
    private java.lang.String contenttypeid;
    private java.lang.String keywords;
    private java.lang.String licensingstart;
    private java.lang.String licensingend;
    private java.lang.Long cpindex;
    private java.lang.String cpid;
    private java.lang.String copyrightcn;
    private java.lang.String copyrighten;
    private java.lang.String provid;
    private java.lang.String cityid;
    private java.lang.Long licenseindex;
    private java.lang.String licenseid;
    private java.lang.Integer tradetype;
    private java.lang.Long exclusivevalidity;
    private java.lang.Integer copyrightexpiration;
    private java.lang.String llicensofflinetime;
    private java.lang.String deletetime;
    private java.lang.Integer platform;
    private java.lang.String nameen;
    private java.lang.String descen;
    private java.lang.String title;
    private java.lang.Long reservecode1;
    private java.lang.Long reservecode2;
    private java.lang.Integer reservesel1;
    private java.lang.Integer reservesel2;
    private java.lang.String reservetime1;
    private java.lang.String reservetime2;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String lastupdatetime;
    private java.lang.String onlinetime;
    private java.lang.String offlinetime;
    private java.lang.String creatoropername;
    private java.lang.String submittername;
    private java.lang.Integer templatetype;
    private java.lang.String wkfwroute;
    private java.lang.String wkfwpriority;
    private java.lang.String effecttime;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String servicekey;
    private java.lang.Integer entrysource;

    private java.lang.String startdate1;
    private java.lang.String startdate2;
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    private java.lang.String physicallist;
    private java.lang.String channelcpcontentid;

    
    public java.lang.String getChannelcpcontentid() {
		return channelcpcontentid;
	}

	public void setChannelcpcontentid(java.lang.String channelcpcontentid) {
		this.channelcpcontentid = channelcpcontentid;
	}

	public java.lang.String getPhysicallist() {
		return physicallist;
	}

	public void setPhysicallist(java.lang.String physicallist) {
		this.physicallist = physicallist;
	}

	public java.lang.String getStarttime1()
    {
        return starttime1;
    }

    public void setStarttime1(java.lang.String starttime1)
    {
        this.starttime1 = starttime1;
    }

    public java.lang.String getStarttime2()
    {
        return starttime2;
    }

    public void setStarttime2(java.lang.String starttime2)
    {
        this.starttime2 = starttime2;
    }

    public java.lang.Long getScheduleindex()
    {
        return scheduleindex;
    }

    public void setScheduleindex(java.lang.Long scheduleindex)
    {
        this.scheduleindex = scheduleindex;
    }

    public java.lang.String getScheduleid()
    {
        return scheduleid;
    }

    public void setScheduleid(java.lang.String scheduleid)
    {
        this.scheduleid = scheduleid;
    }

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.String getProgramname()
    {
        return programname;
    }

    public void setProgramname(java.lang.String programname)
    {
        this.programname = programname;
    }

    public java.lang.String getStartdate()
    {
        return startdate;
    }

    public void setStartdate(java.lang.String startdate)
    {
        this.startdate = startdate;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.String duration)
    {
        this.duration = duration;
    }

    public java.lang.Integer getIsvalid()
    {
        return isvalid;
    }

    public void setIsvalid(java.lang.Integer isvalid)
    {
        this.isvalid = isvalid;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getUnicontentid()
    {
        return unicontentid;
    }

    public void setUnicontentid(java.lang.String unicontentid)
    {
        this.unicontentid = unicontentid;
    }

    public java.lang.String getOriginalnamecn()
    {
        return originalnamecn;
    }

    public void setOriginalnamecn(java.lang.String originalnamecn)
    {
        this.originalnamecn = originalnamecn;
    }

    public java.lang.Integer getContenttype()
    {
        return contenttype;
    }

    public void setContenttype(java.lang.Integer contenttype)
    {
        this.contenttype = contenttype;
    }

    public java.lang.String getContenttypeid()
    {
        return contenttypeid;
    }

    public void setContenttypeid(java.lang.String contenttypeid)
    {
        this.contenttypeid = contenttypeid;
    }

    public java.lang.String getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.String keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.String getLicensingstart()
    {
        return licensingstart;
    }

    public void setLicensingstart(java.lang.String licensingstart)
    {
        this.licensingstart = licensingstart;
    }

    public java.lang.String getLicensingend()
    {
        return licensingend;
    }

    public void setLicensingend(java.lang.String licensingend)
    {
        this.licensingend = licensingend;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCopyrightcn()
    {
        return copyrightcn;
    }

    public void setCopyrightcn(java.lang.String copyrightcn)
    {
        this.copyrightcn = copyrightcn;
    }

    public java.lang.String getCopyrighten()
    {
        return copyrighten;
    }

    public void setCopyrighten(java.lang.String copyrighten)
    {
        this.copyrighten = copyrighten;
    }

    public java.lang.String getProvid()
    {
        return provid;
    }

    public void setProvid(java.lang.String provid)
    {
        this.provid = provid;
    }

    public java.lang.String getCityid()
    {
        return cityid;
    }

    public void setCityid(java.lang.String cityid)
    {
        this.cityid = cityid;
    }

    public java.lang.Long getLicenseindex()
    {
        return licenseindex;
    }

    public void setLicenseindex(java.lang.Long licenseindex)
    {
        this.licenseindex = licenseindex;
    }

    public java.lang.String getLicenseid()
    {
        return licenseid;
    }

    public void setLicenseid(java.lang.String licenseid)
    {
        this.licenseid = licenseid;
    }

    public java.lang.Integer getTradetype()
    {
        return tradetype;
    }

    public void setTradetype(java.lang.Integer tradetype)
    {
        this.tradetype = tradetype;
    }

    public java.lang.Long getExclusivevalidity()
    {
        return exclusivevalidity;
    }

    public void setExclusivevalidity(java.lang.Long exclusivevalidity)
    {
        this.exclusivevalidity = exclusivevalidity;
    }

    public java.lang.Integer getCopyrightexpiration()
    {
        return copyrightexpiration;
    }

    public void setCopyrightexpiration(java.lang.Integer copyrightexpiration)
    {
        this.copyrightexpiration = copyrightexpiration;
    }

    public java.lang.String getLlicensofflinetime()
    {
        return llicensofflinetime;
    }

    public void setLlicensofflinetime(java.lang.String llicensofflinetime)
    {
        this.llicensofflinetime = llicensofflinetime;
    }

    public java.lang.String getDeletetime()
    {
        return deletetime;
    }

    public void setDeletetime(java.lang.String deletetime)
    {
        this.deletetime = deletetime;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.String descen)
    {
        this.descen = descen;
    }

    public java.lang.String getTitle()
    {
        return title;
    }

    public void setTitle(java.lang.String title)
    {
        this.title = title;
    }

    public java.lang.Long getReservecode1()
    {
        return reservecode1;
    }

    public void setReservecode1(java.lang.Long reservecode1)
    {
        this.reservecode1 = reservecode1;
    }

    public java.lang.Long getReservecode2()
    {
        return reservecode2;
    }

    public void setReservecode2(java.lang.Long reservecode2)
    {
        this.reservecode2 = reservecode2;
    }

    public java.lang.Integer getReservesel1()
    {
        return reservesel1;
    }

    public void setReservesel1(java.lang.Integer reservesel1)
    {
        this.reservesel1 = reservesel1;
    }

    public java.lang.Integer getReservesel2()
    {
        return reservesel2;
    }

    public void setReservesel2(java.lang.Integer reservesel2)
    {
        this.reservesel2 = reservesel2;
    }

    public java.lang.String getReservetime1()
    {
        return reservetime1;
    }

    public void setReservetime1(java.lang.String reservetime1)
    {
        this.reservetime1 = reservetime1;
    }

    public java.lang.String getReservetime2()
    {
        return reservetime2;
    }

    public void setReservetime2(java.lang.String reservetime2)
    {
        this.reservetime2 = reservetime2;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getLastupdatetime()
    {
        return lastupdatetime;
    }

    public void setLastupdatetime(java.lang.String lastupdatetime)
    {
        this.lastupdatetime = lastupdatetime;
    }

    public java.lang.String getOnlinetime()
    {
        return onlinetime;
    }

    public void setOnlinetime(java.lang.String onlinetime)
    {
        this.onlinetime = onlinetime;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getCreatoropername()
    {
        return creatoropername;
    }

    public void setCreatoropername(java.lang.String creatoropername)
    {
        this.creatoropername = creatoropername;
    }

    public java.lang.String getSubmittername()
    {
        return submittername;
    }

    public void setSubmittername(java.lang.String submittername)
    {
        this.submittername = submittername;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.String getWkfwroute()
    {
        return wkfwroute;
    }

    public void setWkfwroute(java.lang.String wkfwroute)
    {
        this.wkfwroute = wkfwroute;
    }

    public java.lang.String getWkfwpriority()
    {
        return wkfwpriority;
    }

    public void setWkfwpriority(java.lang.String wkfwpriority)
    {
        this.wkfwpriority = wkfwpriority;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getServicekey()
    {
        return servicekey;
    }

    public void setServicekey(java.lang.String servicekey)
    {
        this.servicekey = servicekey;
    }

    public java.lang.Integer getEntrysource()
    {
        return entrysource;
    }

    public void setEntrysource(java.lang.Integer entrysource)
    {
        this.entrysource = entrysource;
    }

    public void initRelation()
    {
        this.addRelation("scheduleindex", "SCHEDULEINDEX");
        this.addRelation("scheduleid", "SCHEDULEID");
        this.addRelation("channelid", "CHANNELID");
        this.addRelation("programname", "PROGRAMNAME");
        this.addRelation("startdate", "STARTDATE");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("duration", "DURATION");
        this.addRelation("isvalid", "ISVALID");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("unicontentid", "UNICONTENTID");
        this.addRelation("originalnamecn", "ORIGINALNAMECN");
        this.addRelation("contenttype", "CONTENTTYPE");
        this.addRelation("contenttypeid", "CONTENTTYPEID");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("licensingstart", "LICENSINGSTART");
        this.addRelation("licensingend", "LICENSINGEND");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("copyrightcn", "COPYRIGHTCN");
        this.addRelation("copyrighten", "COPYRIGHTEN");
        this.addRelation("provid", "PROVID");
        this.addRelation("cityid", "CITYID");
        this.addRelation("licenseindex", "LICENSEINDEX");
        this.addRelation("licenseid", "LICENSEID");
        this.addRelation("tradetype", "TRADETYPE");
        this.addRelation("exclusivevalidity", "EXCLUSIVEVALIDITY");
        this.addRelation("copyrightexpiration", "COPYRIGHTEXPIRATION");
        this.addRelation("llicensofflinetime", "LLICENSOFFLINETIME");
        this.addRelation("deletetime", "DELETETIME");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("title", "TITLE");
        this.addRelation("reservecode1", "RESERVECODE1");
        this.addRelation("reservecode2", "RESERVECODE2");
        this.addRelation("reservesel1", "RESERVESEL1");
        this.addRelation("reservesel2", "RESERVESEL2");
        this.addRelation("reservetime1", "RESERVETIME1");
        this.addRelation("reservetime2", "RESERVETIME2");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("lastupdatetime", "LASTUPDATETIME");
        this.addRelation("onlinetime", "ONLINETIME");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("creatoropername", "CREATOROPERNAME");
        this.addRelation("submittername", "SUBMITTERNAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("wkfwroute", "WKFWROUTE");
        this.addRelation("wkfwpriority", "WKFWPRIORITY");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("servicekey", "SERVICEKEY");
        this.addRelation("entrysource", "ENTRYSOURCE");
    }

    public java.lang.String getStartdate1()
    {
        return startdate1;
    }

    public void setStartdate1(java.lang.String startdate1)
    {
        this.startdate1 = startdate1;
    }

    public java.lang.String getStartdate2()
    {
        return startdate2;
    }

    public void setStartdate2(java.lang.String startdate2)
    {
        this.startdate2 = startdate2;
    }
    
    public ScheduleWkfwhis newStrategyWkfwhis()
    {
        ScheduleWkfwhis scheduleWkfwhis = new ScheduleWkfwhis();
        BeanCopier Copier = BeanCopier.create(CmsSchedule.class, ScheduleWkfwhis.class, false);
        Copier.copy(this, scheduleWkfwhis, null);
        return scheduleWkfwhis;
    }
}

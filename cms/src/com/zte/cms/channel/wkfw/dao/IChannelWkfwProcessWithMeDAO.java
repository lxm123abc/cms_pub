package com.zte.cms.channel.wkfw.dao;

import java.util.List;

import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.model.ChannelWkfwProcessWithMe;

import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IChannelWkfwProcessWithMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ChannelWkfwProcessWithMe> queryChannelWkfwProcessWithMeListByCond(
            ChannelWkfwCondition channelWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int queryChannelWkfwProcessWithMeListCntByCond(
            ChannelWkfwCondition channelWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start,
            int pageSize) throws DAOException;

    public abstract PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DAOException;

}
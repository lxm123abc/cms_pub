package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.ICastPictureMapDAO;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CastPictureMapDAO extends DynamicObjectBaseDao implements ICastPictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCastPictureMap(CastPictureMap castPictureMap) throws DAOException
    {
        log.debug("insert castPictureMap starting...");
        super.insert("insertCastPictureMap", castPictureMap);
        log.debug("insert castPictureMap end");
    }

    public void updateCastPictureMap(CastPictureMap castPictureMap) throws DAOException
    {
        log.debug("update castPictureMap by pk starting...");
        super.update("updateCastPictureMap", castPictureMap);
        log.debug("update castPictureMap by pk end");
    }

    public void deleteCastPictureMap(CastPictureMap castPictureMap) throws DAOException
    {
        log.debug("delete castPictureMap by pk starting...");
        super.delete("deleteCastPictureMap", castPictureMap);
        log.debug("delete castPictureMap by pk end");
    }

    public CastPictureMap getCastPictureMap(CastPictureMap castPictureMap) throws DAOException
    {
        log.debug("query castPictureMap starting...");
        CastPictureMap resultObj = (CastPictureMap) super.queryForObject("getCastPictureMap", castPictureMap);
        log.debug("query castPictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CastPictureMap> getCastPictureMapByCond(CastPictureMap castPictureMap) throws DAOException
    {
        log.debug("query castPictureMap by condition starting...");
        List<CastPictureMap> rList = (List<CastPictureMap>) super.queryForList("queryCastPictureMapListByCond",
                castPictureMap);
        log.debug("query castPictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query castPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastPictureMapListCntByCond", castPictureMap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CastPictureMap> rsList = (List<CastPictureMap>) super.pageQuery("queryCastPictureMapListByCond",
                    castPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castPictureMap by condition end");
        return pageInfo;
    }
    
    public PageInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query castPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastPictureMapSynListCntByCond", castPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramPictureMap> rsList = (List<ProgramPictureMap>) super.pageQuery(
                    "queryCastPictureMapSynListByCond", castPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castPictureMap by condition end");
        return pageInfo;
    } 

    public PageInfo pageInfoQueryCastPicTargetMap(CastPictureMap castPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query castPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastPictureTargetMapSynListCntByCond", castPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramPictureMap> rsList = (List<ProgramPictureMap>) super.pageQuery(
                    "queryCastPictureTargetMapSynListByCond", castPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castPictureMap by condition end");
        return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCastPictureMapListByCond", "queryCastPictureMapListCntByCond",
                castPictureMap, start, pageSize, puEntity);
    }

}
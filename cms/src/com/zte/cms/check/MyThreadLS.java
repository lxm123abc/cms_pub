package com.zte.cms.check;

import java.lang.Thread;

import com.zte.cms.cpContractManagement.task.CpContractTask;
import com.zte.ismp.common.ConfigUtil;
import com.zte.ssb.device.customtimer.service.IJob;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

public class MyThreadLS extends Thread {

    private IJob cyclejob = null;

    private Log log = SSBBus.getLog(getClass());

    private static int count = 0;

    private int flag = 0;

    public void run() {

        CycleJob cjob = new CycleJob();
        CpContractTask job = new CpContractTask();
        Long sleeptime = Long.parseLong(ConfigUtil.get(CheckConstant.THREAD_SLEEP_TIME)) * 60000;
        while (true) {
            try {
                // System.out.println("------------seleeptime = " + sleeptime);
                job.execute();
                if (MyThreadLS.count >= 1) {
                    Thread.sleep(sleeptime);
                    // if(flag == 0 ){
                    // cyclejob = (IJob)SSBBus.findDomainService("cyclejob");
                    // flag = 1;
                    // }
                    // cyclejob.execute(null);
                    cjob.myExecute(MyThreadLS.count);
                } else {
                    Thread.sleep(180000);
                    // if(flag == 0 ){
                    // cyclejob = (IJob)SSBBus.findDomainService("cyclejob");
                    // flag = 1;
                    // }
                    // cyclejob.execute(null);
                    cjob.myExecute(MyThreadLS.count);
                    MyThreadLS.count++;
                }
            } catch (Exception e) {
                log.error("Error occur in MyThreadLS while running");
                log.error(e);
                continue;
            }
        }
    }
}

package com.zte.cms.content.wkfw.dao;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmContentWkfwTaskHisResultDAO
{

    /**
     * <p>
     * 根据条件分页查询我已经处理过工单数量
     * </p>
     * 
     * @param uappablWkfwCondition UappablWkfwCondition对象，查询条件
     * @return 查询结果
     * @throws DomainServiceException抛出异常
     * 
     */
    public long getMyHaveHandleWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition) throws DAOException;

    /**
     * 根据条件分页查询UappablWkfwTaskResult对象，作为查询条件的参数
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwTaskResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询UappablWkfwTaskResult对象，作为查询条件的参数
     * 
     * @param uappablWkfwCondition 类型UappablWkfwCondition对象
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @param puEntity 排序对象
     * @return 查询结果
     * @throws DAOException dao异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwTaskResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
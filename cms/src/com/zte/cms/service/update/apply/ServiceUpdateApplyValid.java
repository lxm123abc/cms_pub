package com.zte.cms.service.update.apply;


import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.servicepublishmanager.ls.IServiceSynLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class ServiceUpdateApplyValid extends CommonValid
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected IServiceDS serviceds = null;
    protected IServiceBufDS serviceBufds = null;
    private IServiceSynLS servicesynls = null;
    
    
    public IServiceSynLS getServicesynls() {
		return servicesynls;
	}

	public void setServicesynls(IServiceSynLS servicesynls) {
		this.servicesynls = servicesynls;
	}

	

	public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {

        Service service = new Service();
        service.setServiceindex(index);
        try
        {
            service = serviceds.getService(service);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        if (null != service)
        {
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
            ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

            String optObjecttype = ServiceConstants.getWorkflowTypeName(ServiceConstants.WKFWTYPECODE_SERVICE_UPDATE);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptObject(String.valueOf(index));
            String info = ResourceMgt.findDefaultText("ucdn.service.log.wkfw.update.valid");

            if (null != info)
            {
                info = info.replace("[servicename]", service.getServicename());
            }
            loginfo.setOptDetail(info);
            loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
            return loginfo;
        }
        else
        {
            return null;
        }

    }

    @Override
    public void operate(Long index)
    {
        // 修改状态
        try
        {
            Service serviceOld = new Service();
            serviceOld.setServiceindex(index);
            serviceOld = serviceds.getService(serviceOld);

            ServiceBuf servicebuf = new ServiceBuf();
            servicebuf.setServiceindex(index);
            servicebuf = serviceBufds.getServiceBuf(servicebuf);

            Service serviceNew = new Service();

            BeanCopier copier = BeanCopier.create(ServiceBuf.class, Service.class, true);
            copier.copy(servicebuf, serviceNew, new BeanPropertyConvert());
            serviceNew.setWorkflow(Integer.valueOf(0));
            serviceds.updateService(serviceNew);
            //发布方法
            try {
				servicesynls.modPublishSyn(serviceNew.getServiceid());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            
            // 根据老的状态，修改新的状态
//            int oldStatus = servicebuf.getStatus();
//            if (oldStatus == 0 || oldStatus == 80) // 待发布或者取消同步成功
//            {
//
//                serviceNew.setStatus(oldStatus);
//                serviceds.updateService(serviceNew);
//            }
//            else
//            {
//                serviceNew.setStatus(40); // 修改同步中
//                serviceds.updateService(serviceNew);
//                // 生成同步任务
//                try
//                {
//                    servicels.serviceUpdatePublish(serviceNew.getServiceindex());
//                }
//                catch (Exception e)
//                {
//                    throw new RuntimeException(e.getMessage());
//                }
//            }
           
            serviceBufds.removeServiceBuf(servicebuf);

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

    public void setServiceBufds(IServiceBufDS serviceBufds)
    {
        this.serviceBufds = serviceBufds;
    }

   

}

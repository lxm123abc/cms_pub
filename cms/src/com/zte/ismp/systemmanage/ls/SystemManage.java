package com.zte.ismp.systemmanage.ls;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;

import com.zte.ismp.common.ConfigUtil;
import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ismp.common.util.FileUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.servicecontainer.business.server.RIAContext;

public class SystemManage implements ISystemManage
{

    private Log log = SSBBus.getLog(getClass());

    public String getAdconfigxmlContent() throws Exception
    {

        String keyvalue = "";
        // String key = "";

        List<Element> elements = (List<Element>) getRoot().getChildren("info");

        if (elements != null)
        {
            for (int i = 0; i < elements.size(); i++)
            {
                Element element = (Element) elements.get(i);
                keyvalue += element.getAttributeValue("name").toLowerCase() + "'" + element.getAttributeValue("value")
                        + "\"";
            }
        }
        // System.out.println(keyvalue);
        /*
         * Hashtable<String, String> ht = CommonInfo.getHashTable(); //Enumeration<String> keyS = ht.keys();
         * 
         * ArrayList list = new ArrayList(ht.keySet()); Collections.sort(list); //Collections.reverse(list); for
         * (Iterator iterator = list.iterator(); iterator.hasNext();) { key = (String) iterator.next(); keyvalue += key +
         * "'" + ht.get(key) + "\""; }
         */
        return keyvalue;
        // return RIAContext.getCurrentInstance().getRealPath("/") + "/" + "WEB-INF" + "/classes/ad_config.xml";

    }

    public String modAdconfigxml(String[] keyarray, String[] valuearray) throws Exception
    {

        String configfilename = ConfigUtil.DEFAULT_CONFIG_FILE;
        String key;
        String value;

        HashMap<String, String> oldMap = new HashMap();
        Element oldroot = getRoot();
        List<Element> elements = (List<Element>) oldroot.getChildren("info");

        List contentlist = oldroot.getContent();
        Hashtable<String, List> htComment = null;
        if (contentlist != null && contentlist.size() > 0)
        {
            boolean addhtcommentflag = false;
            List<String> commentList = null;

            for (Iterator iterator = contentlist.iterator(); iterator.hasNext();)
            {
                Object o = (Object) iterator.next();
                if (o instanceof Comment)
                {
                    if (htComment == null)
                    {
                        htComment = new Hashtable<String, List>();
                    }
                    if (commentList == null)
                    {
                        commentList = new LinkedList<String>();
                    }
                    commentList.add(((Comment) o).getText());
                    addhtcommentflag = true;
                }
                else if (o instanceof Element)
                {
                    if (addhtcommentflag && commentList != null && commentList.size() > 0)
                    {
                        htComment.put(((Element) o).getAttributeValue("name").toLowerCase().trim(), commentList);
                        commentList = null;
                        addhtcommentflag = false;
                    }
                }
            }
        }
        if (elements != null)
        {
            Element element;
            for (int i = 0; i < elements.size(); i++)
            {
                element = (Element) elements.get(i);
                oldMap.put(element.getAttributeValue("name").toLowerCase().trim(), element.getAttributeValue("value"));
            }
        }

        ReturnInfo returnInfo = new ReturnInfo();
        String LOADTPATH = RIAContext.getCurrentInstance().getRealPath("/") + "/" + "WEB-INF" + "/classes/";
        FileUtil fileUtil = new FileUtil();
        String backupfilepath = LOADTPATH + ConfigUtil.DEFAULT_CONFIG_FILE_HEAD + getYMDHMS() + ".xml.bak";
        // String backupfilepath = LOADTPATH + "ad_config.xml.bak";
        try
        {

            HashMap<String, String> newMap = new HashMap();
            try
            {
                fileUtil.copyFile(LOADTPATH + configfilename, backupfilepath);
            }
            catch (Exception ex)
            {
                throw new Exception("2200001");// 操作失败
            }
            try
            {
                Element root = new Element("root");
                Element info;
                // org.jdom.Comment comment = new org.jdom.Comment(" this is system parameter config file ");
                // root.addContent(comment);
                for (int i = 0; i < keyarray.length; i++)
                {
                    key = keyarray[i];
                    if (key != null && key.trim().length() > 0)
                    {

                        value = valuearray[i];

                        info = new Element("info");
                        info.setAttribute("name", key);
                        info.setAttribute("value", value);
                        newMap.put(key, value);
                        if (htComment != null)
                        {

                            if (htComment.get(key.toLowerCase().trim()) != null
                                    && htComment.get(key.toLowerCase().trim()).size() > 0)
                            {
                                List commentlist = htComment.get(key.toLowerCase().trim());
                                for (Iterator iterator = commentlist.iterator(); iterator.hasNext();)
                                {
                                    root.addContent(new org.jdom.Comment((String) iterator.next()));
                                }

                            }
                        }
                        root.addContent(info);
                    }
                }

                Document docxml = new Document(root);
                Format format = Format.getPrettyFormat();
                format.setEncoding("UTF-8");
                org.jdom.output.XMLOutputter output = new org.jdom.output.XMLOutputter(format);
                FileOutputStream fo = new FileOutputStream(LOADTPATH + configfilename);
                output.output(docxml, fo);
                fo.close();
                ConfigUtil.reLoad();

                try
                {
                    // TaskUpdateProcessor tup = new TaskUpdateProcessor();
                    // tup.updateTask(oldMap, newMap);
                }
                catch (Exception ex)
                {
                    log.error("Time Task Exception", ex);
                    throw new Exception("10002");// 更新定时任务出错
                }
            }
            catch (Exception ex)
            {
                // 如果生成文件有问题或重载失败，需要把原先的文件换回去,并且重新
                fileUtil.copyFile(backupfilepath, LOADTPATH + configfilename);
                ConfigUtil.reLoad();

                if (ex.getMessage().equals("10002"))
                {
                    try
                    {
                        newMap = new HashMap();
                        elements = (List<Element>) getRoot().getChildren("info");
                        if (elements != null)
                        {
                            Element element;
                            for (int i = 0; i < elements.size(); i++)
                            {
                                element = (Element) elements.get(i);
                                newMap.put(element.getAttributeValue("name").toLowerCase().trim(), element
                                        .getAttributeValue("value"));
                            }
                        }
                        // TaskUpdateProcessor tup = new TaskUpdateProcessor();
                        // tup.updateTask(null, newMap);
                    }
                    catch (Exception e)
                    {
                        ex.printStackTrace();
                        log.error("Time Task Exception", e);
                        throw new Exception("10002");// 更新定时任务出错
                    }
                }
                throw new Exception(ex);
            }
        }
        catch (Exception ex)
        {
            fileUtil.delFile(backupfilepath);
            ex.printStackTrace();
            returnInfo.setFlag(ConfigUtil.FAILURE_FLAG);
            returnInfo.setReturnMessage(ResourceManager.getErrorcodeByException(ex));
            log.error("ADException Method modAdconfigxml", ex);
            throw new Exception(ResourceManager.getResourceText(ex));
        }
        fileUtil.delFile(backupfilepath);
        returnInfo.setFlag(ConfigUtil.SUCCESS_FLAG);
        returnInfo.setReturnMessage("mod.success");// 修改成功
        return returnInfo.toString();
    }

    public String getYMDHMS()
    {

        StringBuffer ymdhms = new StringBuffer();
        Calendar gc1 = Calendar.getInstance();
        int year = gc1.get(Calendar.YEAR);
        int month = gc1.get(Calendar.MONTH) + 1;
        int day = gc1.get(Calendar.DAY_OF_MONTH);
        int hour = gc1.get(Calendar.HOUR_OF_DAY);
        int min = gc1.get(Calendar.MINUTE);
        int sec = gc1.get(Calendar.SECOND);
        ymdhms.append(year);
        ymdhms.append("-");
        if (month < 10)
        {
            ymdhms.append("0" + String.valueOf(month));
        }
        else
        {
            ymdhms.append(String.valueOf(month));
        }
        ymdhms.append("-");
        if (day < 10)
        {
            ymdhms.append("0" + String.valueOf(day));
        }
        else
        {
            ymdhms.append(String.valueOf(day));
        }
        ymdhms.append("-");
        if (hour < 10)
        {
            ymdhms.append("0" + String.valueOf(hour));
        }
        else
        {
            ymdhms.append(String.valueOf(hour));
        }
        ymdhms.append("-");
        if (min < 10)
        {
            ymdhms.append("0" + String.valueOf(min));
        }
        else
        {
            ymdhms.append(String.valueOf(min));
        }
        ymdhms.append("-");
        if (sec < 10)
        {
            ymdhms.append("0" + String.valueOf(sec));
        }
        else
        {
            ymdhms.append(String.valueOf(sec));
        }
        return ymdhms.toString();
    }

    private Element getRoot()
    {

        Element root = null;
        try
        {
            java.net.URL confURL = ConfigUtil.class.getClassLoader().getResource(ConfigUtil.DEFAULT_CONFIG_FILE);
            String path = confURL.getPath();
            if ((null != path) && (path.trim().length() > 0))
            {
                SAXBuilder dom = new SAXBuilder();
                Document doc = dom.build(confURL.openStream());
                root = doc.getRootElement();

            }
            // ArrayList list = new ArrayList( CommonInfo.htComment.keySet());

            // Collections.reverse(list);
            /*
             * for (Iterator iterator = list.iterator(); iterator.hasNext();) { String key = (String) iterator.next();
             * keyvalue += key + "'" + CommonInfo.htComment.get(key) + "\""; }
             */

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return root;
    }

}

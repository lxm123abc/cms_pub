package com.zte.cms.server.ls;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.OperatorLog;
import com.zte.cms.common.ResourceCodeParser;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;

public class ServerLogMgt
{

    // 写操作日志
    public static void insertOperatorLog(ResourceCodeParser parser, String objId, String objName, int iObjType,
            int iOperType, int iOperRst)
    {
        String ObjTypeDesc = null;
        String OperTypeDesc = null;
        String OperRstDesc = null;
        String operAction = null;

        if (parser == null)
        {
            parser = new ResourceCodeParser(ServerConstant.CMS_SERV_RES_FILE_NAME);
        }

        switch (iObjType)
        // 操作对象
        {
            case 1:// 服务器
                ObjTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_OBJ_SEROBJ);
                break;
            default:
                ObjTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_UNKNOW);
        }

        switch (iOperType)
        // 操作类型
        {
            case 1:// 新增
                OperTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_ADD);
                operAction = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_ADDED);
                break;
            case 2:// 修改
                OperTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_UPDATE);
                operAction = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_UPDATED);
                break;
            case 3:// 删除
                OperTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_DELETE);
                operAction = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_DELETED);
                break;
            default:
                OperTypeDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_UNKNOW);
                operAction = parser.getResourceItem(ServerConstant.RES_CMS_SERV_UNKNOW);
        }
        // OperTypeDesc = OperTypeDesc + ObjTypeDesc;

        switch (iOperRst)
        // 操作结果
        {
            case 0:// 成功
                OperRstDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_SUCCESS);
                break;
            case 1:// 失败
                OperRstDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_FAIL);
                break;
            default:
                OperRstDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_UNKNOW);
        }

        OperInfo oper = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);

        OperRstDesc = parser.getResourceItem(ServerConstant.RES_CMS_SERV_LOG_OPER_PREFIX) + oper.getUserId()
                + operAction + " " + objName + " " + OperRstDesc;

        OperatorLog.insertOperatorLog(objId, ObjTypeDesc, OperTypeDesc, OperRstDesc);

    }

}

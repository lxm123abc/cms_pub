package com.zte.cms.operCnttypeMap.ls;

import java.util.List;

import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.cms.operCnttypeMap.service.IOperCnttypemapDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class OperCnttypemapLS implements IOperCnttypemapLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private IOperCnttypemapDS operCnttypemapDS;

    public String saveOperCnttypeMap(List<OperCnttypemap> list, Long operid)
    {
        log.debug("saveOperCnttypeMap starting....");
        String result = "0";
        try
        {
            OperCnttypemap delCnttypemap = new OperCnttypemap();
            delCnttypemap.setOperid(operid);
            List<OperCnttypemap> delList = operCnttypemapDS.getOperCnttypemapByCond(delCnttypemap);

            if (delList != null && delList.size() > 0)
            {
                operCnttypemapDS.removeOperCnttypemapListByCond(delList);
            }

            for (OperCnttypemap operCnttypemap : list)
            {
                operCnttypemapDS.insertOperCnttypemap(operCnttypemap);
            }

            if ((list == null || list.size() <= 0) && (delList == null || delList.size() <= 0))
            {
                result = "1";
            }
        }
        catch (Exception e)
        {
            result = "1"; // 设置操作结果失败
            log.error("OperCnttypemapLS exception:" + e.getMessage());
        }

        log.debug("saveOperCnttypeMap end");
        return result;
    }

    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap)
    {
        log.debug("getOperCnttypemapByCond starting ...");
        List<OperCnttypemap> list = null;
        try
        {
            list = operCnttypemapDS.getOperCnttypemapByCond(operCnttypemap);
        }
        catch (Exception e)
        {
            log.error("OperCnttypemapLS exception:" + e.getMessage());
        }
        log.debug("getOperCnttypemapByCond end");
        return list;
    }

    public void setOperCnttypemapDS(IOperCnttypemapDS operCnttypemapDS)
    {
        this.operCnttypemapDS = operCnttypemapDS;
    }
}

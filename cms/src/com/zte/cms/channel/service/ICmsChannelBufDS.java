package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsChannelBufDS
{
    /**
     * 新增CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException;

    /**
     * 更新CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException;

    /**
     * 批量更新CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsChannelBufList(List<CmsChannelBuf> cmsChannelBufList) throws DomainServiceException;

    /**
     * 删除CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException;

    /**
     * 批量删除CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsChannelBufList(List<CmsChannelBuf> cmsChannelBufList) throws DomainServiceException;

    /**
     * 查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @return CmsChannelBuf对象
     * @throws DomainServiceException ds异常
     */
    public CmsChannelBuf getCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DomainServiceException;

    /**
     * 根据条件查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @return 满足条件的CmsChannelBuf对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsChannelBuf> getCmsChannelBufByCond(CmsChannelBuf cmsChannelBuf) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}

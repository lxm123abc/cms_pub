

package com.zte.cms.impSyncTask.service;

import java.util.List;
import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IObjectCpcntRecordDS
{
    /**
  * 新增ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */
	public void insertObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;
    /**
  * 新增ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */
	public void insertObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException;	
	
    /**
  * 更新ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */
	public void updateObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;

    /**
  * 批量更新ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */
	public void updateObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException;

	/**
  * 根据条件更新ObjectCpcntRecord对象  
  *
  * @param objectCpcntRecord ObjectCpcntRecord更新条件
  * @throws DomainServiceException ds异常
  */ 
	public void updateObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;

	/**
  * 根据条件批量更新ObjectCpcntRecord对象  
  *
  * @param objectCpcntRecord ObjectCpcntRecord更新条件
  * @throws DomainServiceException ds异常
  */ 
	public void updateObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException;

	/**
  * 删除ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */ 
	public void removeObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;

	/**
  * 批量删除ObjectCpcntRecord对象
  *
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常
  */ 
	public void removeObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException;

    /**
  * 根据条件删除ObjectCpcntRecord对象
  * 
  * @param objectCpcntRecord ObjectCpcntRecord删除条件
  * @throws DomainServiceException ds异常
  */  
    public void removeObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;	

    /**
  * 根据条件批量删除ObjectCpcntRecord对象
  * 
  * @param objectCpcntRecord ObjectCpcntRecord删除条件
  * @throws DomainServiceException ds异常
  */ 
    public void removeObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException;	

	/**
  * 查询ObjectCpcntRecord对象
  
  * @param objectCpcntRecord ObjectCpcntRecord对象
  * @return ObjectCpcntRecord对象
  * @throws DomainServiceException ds异常 
  */
	 public ObjectCpcntRecord getObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException; 

	 /**
   * 根据条件查询ObjectCpcntRecord对象 
   * 
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @return 满足条件的ObjectCpcntRecord对象集
   * @throws DomainServiceException ds异常
   */
     public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException;

	 /**
   * 根据条件分页查询ObjectCpcntRecord对象 
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DomainServiceException ds异常
   */
     public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DomainServiceException;
     
	 /**
   * 根据条件分页查询ObjectCpcntRecord对象 
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DomainServiceException ds异常
   */
     public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     /**
      * 根据taskindex分页查询ObjectCpcntRecord对象 
      *
      * @param objectCpcntRecord ObjectCpcntRecord对象，作为查询条件的参数 
      * @param start 起始行
      * @param pageSize 页面大小
      * @param puEntity 排序空置参数@see PageUtilEntity
      * @return  查询结果
      * @throws DomainServiceException ds异常
      */
     public TableDataInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DomainServiceException;
}
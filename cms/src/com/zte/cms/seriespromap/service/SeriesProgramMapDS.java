package com.zte.cms.seriespromap.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.cms.seriespromap.dao.ISeriesProgramMapDAO;
import com.zte.cms.seriespromap.service.ISeriesProgramMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class SeriesProgramMapDS extends DynamicObjectBaseDS implements ISeriesProgramMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ISeriesProgramMapDAO dao = null;

    public void setDao(ISeriesProgramMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("insert seriesProgramMap starting...");
        try
        {
            Long seriesprogrammapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("series_program_map_index");
            seriesProgramMap.setMapindex(seriesprogrammapindex);
            String mappingid = String.format("%02d", ObjectType.SERIESPROGRAMMAP_TYPE)+ String.format("%030d", seriesprogrammapindex);
            seriesProgramMap.setMappingid(mappingid);
            dao.insertSeriesProgramMap(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert seriesProgramMap end");
    }

    public void updateSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("update seriesProgramMap by pk starting...");
        try
        {
            dao.updateSeriesProgramMap(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesProgramMap by pk end");
    }

    public void updateSeriesProgramMapList(List<SeriesProgramMap> seriesProgramMapList) throws DomainServiceException
    {
        log.debug("update seriesProgramMapList by pk starting...");
        if (null == seriesProgramMapList || seriesProgramMapList.size() == 0)
        {
            log.debug("there is no datas in seriesProgramMapList");
            return;
        }
        try
        {
            for (SeriesProgramMap seriesProgramMap : seriesProgramMapList)
            {
                dao.updateSeriesProgramMap(seriesProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesProgramMapList by pk end");
    }

    public void removeSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("remove seriesProgramMap by pk starting...");
        try
        {
            dao.deleteSeriesProgramMap(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesProgramMap by pk end");
    }

    public void removeSeriesProgramMapList(List<SeriesProgramMap> seriesProgramMapList) throws DomainServiceException
    {
        log.debug("remove seriesProgramMapList by pk starting...");
        if (null == seriesProgramMapList || seriesProgramMapList.size() == 0)
        {
            log.debug("there is no datas in seriesProgramMapList");
            return;
        }
        try
        {
            for (SeriesProgramMap seriesProgramMap : seriesProgramMapList)
            {
                dao.deleteSeriesProgramMap(seriesProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesProgramMapList by pk end");
    }

    public SeriesProgramMap getSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("get seriesProgramMap by pk starting...");
        SeriesProgramMap rsObj = null;
        try
        {
            rsObj = dao.getSeriesProgramMap(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesProgramMapList by pk end");
        return rsObj;
    }

    public List<SeriesProgramMap> getSeriesProgramMapByCond(SeriesProgramMap seriesProgramMap)
            throws DomainServiceException
    {
        log.debug("get seriesProgramMap by condition starting...");
        List<SeriesProgramMap> rsList = null;
        try
        {
            rsList = dao.getSeriesProgramMapByCond(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesProgramMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesProgramMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get seriesProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesProgramMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesProgramMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("pageInfoQueryForFrontPage starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryForFrontPage(seriesProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("pageInfoQueryForFrontPage end");
        return tableInfo;
    }

    public int getMaxSequenceBySeriesindex(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("getMaxSequenceBySeriesindex starting...");
        int maxSequence = 0;
        try
        {
            maxSequence = dao.getMaxSequenceBySeriesindex(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:" + daoEx);
            throw new DomainServiceException(daoEx);
        }
        return maxSequence;
    }
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryseriesproMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryseriesproMap(seriesProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesProgramMap page info by condition end");
        return tableInfo;
    }
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryseriesproTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryseriesproTargetMap(seriesProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesProgramMap page info by condition end");
        return tableInfo;
    }    
    
    
    
    public List<SeriesProgramMap> getnormalSeriesProTargetMapByCond(SeriesProgramMap seriesProgramMap)
            throws DomainServiceException
    {
        log.debug("get seriesProgramMap by condition starting...");
        List<SeriesProgramMap> rsList = null;
        try
        {
            rsList = dao.getnormalSeriesProTargetMapByCond(seriesProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesProgramMap by condition end");
        return rsList;
    }    
}

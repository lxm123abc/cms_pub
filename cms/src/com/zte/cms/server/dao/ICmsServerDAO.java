package com.zte.cms.server.dao;

import java.util.List;

import com.zte.cms.server.model.CmsServer;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsServerDAO
{
    /**
     * 新增CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void insertCmsServer(CmsServer cmsServer) throws DAOException;

    /**
     * 新增CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void insertCmsServerList(List<CmsServer> cmsServerList) throws DAOException;

    /**
     * 根据主键更新CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void updateCmsServer(CmsServer cmsServer) throws DAOException;

    /**
     * 根据主键更新CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void updateCmsServerList(List<CmsServer> cmsServerList) throws DAOException;

    /**
     * 根据条件更新CmsServer对象
     * 
     * @param cmsServer CmsServer更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsServerByCond(CmsServer cmsServer) throws DAOException;

    /**
     * 根据条件更新CmsServer对象
     * 
     * @param cmsServer CmsServer更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsServerListByCond(List<CmsServer> cmsServerList) throws DAOException;

    /**
     * 根据主键删除CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void deleteCmsServer(CmsServer cmsServer) throws DAOException;

    /**
     * 根据主键删除CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DAOException dao异常
     */
    public void deleteCmsServerList(List<CmsServer> cmsServerList) throws DAOException;

    /**
     * 根据条件删除CmsServer对象
     * 
     * @param cmsServer CmsServer删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsServerByCond(CmsServer cmsServer) throws DAOException;

    /**
     * 根据条件删除CmsServer对象
     * 
     * @param cmsServer CmsServer删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsServerListByCond(List<CmsServer> cmsServerList) throws DAOException;

    /**
     * 根据主键查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @return 满足条件的CmsServer对象
     * @throws DAOException dao异常
     */
    public CmsServer getCmsServer(CmsServer cmsServer) throws DAOException;

    /**
     * 根据条件查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @return 满足条件的CmsServer对象集
     * @throws DAOException dao异常
     */
    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer) throws DAOException;

    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer, PageUtilEntity puEntity) throws DAOException;

    /**
     * 根据条件分页查询CmsServer对象，作为查询条件的参数
     * 
     * @param cmsServer CmsServer对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsServer对象，作为查询条件的参数
     * 
     * @param cmsServer CmsServer对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
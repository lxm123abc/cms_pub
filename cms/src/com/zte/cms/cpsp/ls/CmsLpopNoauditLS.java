package com.zte.cms.cpsp.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.cpsp.CpspLogConstant;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.cpsp.common.ICmsLpopCheckLS;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;
import com.zte.cms.cpsp.service.ICmsUcpBasicDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;

public class CmsLpopNoauditLS implements ICmsLpopNoauditLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsLpopNoauditDS ds;
    private ICmsLpopCheckLS check;
    private IUcpBasicDS ucpBasicDS;
    private ICmsProgramDS cmsProgramDS;
    private DbUtil dbUtil = new DbUtil();
    private String result = "0:操作成功";

    private ICmsUcpBasicDS cmsUcpBasicDS;

    public ICmsUcpBasicDS getCmsUcpBasicDS()
    {
        return cmsUcpBasicDS;
    }

    public void setCmsUcpBasicDS(ICmsUcpBasicDS cmsUcpBasicDS)
    {
        this.cmsUcpBasicDS = cmsUcpBasicDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    public ICmsLpopNoauditDS getDs()
    {
        return ds;
    }

    public void setCheck(ICmsLpopCheckLS check)
    {
        this.check = check;
    }

    public void setDs(ICmsLpopNoauditDS ds)
    {
        this.ds = ds;
    }

    /**
     * 获得当前登录操作员
     * 
     * @return operid 操作员ID
     */
    public String getOperidFromSession(String param)
    {
        log.debug("getOperidFromSession begin ...");
        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        // 获取牌照放operid
        String operid = (String) httpSession.getAttribute("OPERID");
        if (null != operid)
        {
            log.debug("lpid is :[" + operid + "]");
        }
        else
        {
            log.debug("lpid is null check CERID is exist in session scope !");
        }
        log.debug("getOperidFromSession end ...");
        return operid;
    }

    /**
     * 删除免审CP
     */
    public String deleteCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException
    {

        for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
        {

            ds.removeCmsLpopNoaudit(cmsLpopNoaudit);
        }
        return CmsLpopNoauditConstants.success;
    }

    /**
     * 新增牌照放免审CP
     * 
     * @param cmsLpopNoauditList 免审对象集合
     * @return 成功返回 0, 失败返回 1
     */
    public String insertCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList)
    {
        log.debug("insertCmsLpopNoaudit LS begin ...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer iFail = 0;
        Integer iSuccessed = 0;
        Integer iRemoved = 0;
        String failID = "";
        String successID = "";
        String CPID = "";
        try
        {
            if (null != cmsLpopNoauditList && cmsLpopNoauditList.size() > 0)
            {
                for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
                {
                    if (null != cmsLpopNoaudit)
                    {
                        CPID = cmsLpopNoaudit.getCpid();
                        // 如果mapindex 为空表示当前对象是要新增的
                        if (null == cmsLpopNoaudit.getMapindex())
                        {
                            try
                            {
                                // 标识是牌照方
                                cmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.lp_type);
                                // 校验CP是否已经存在，不存在则不新增
                                if (!check.checkCmsCpExist(cmsLpopNoaudit))
                                {
                                    iFail++;
                                    failID = iFail.toString();
                                    operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.lisence.nonaudit.set"),
                                    		ResourceMgt.findDefaultText("mes.cp.not.exist"));//配置免审失败   该CP已经不存在

                                    returnInfo.appendOperateInfo(operateInfo);
                                    continue;
                                }
                                // 校验是否已经存在，存在则不新增
                                if (check.checkCmsLpExist(cmsLpopNoaudit))
                                {
                                    ds.insertCmsLpopNoaudit(cmsLpopNoaudit);
                                }
                                iSuccessed++;
                                successID = iSuccessed.toString();
                                operateInfo = new OperateInfo(successID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.nonaudit.set.success"),
                                		ResourceMgt.findDefaultText("mes.lp.nonaudit.set.success"));//配置免审成功   牌照方免审配置成功

                                returnInfo.appendOperateInfo(operateInfo);
                                // 保存操作日志
                                CommonLogUtil.insertOperatorLog(cmsLpopNoaudit.getLpid(),
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_MGT,
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_SET,
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_SET_INFO,
                                        CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            catch (Exception e)
                            {
                                log.error("insertCmsLpopNoaudit LS error.", e);
                                iFail++;
                                failID = iFail.toString();
                                operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.lisence.nonaudit.set"), ResourceMgt.findDefaultText("mes.nonaudit.set.fail"));

                                returnInfo.appendOperateInfo(operateInfo);//配置免审失败   牌照方免审配置失败
                            }
                        }
                        else
                        {
                            try
                            {
                                if (cmsLpopNoaudit.getMapindex() == 0
                                        || "0".equals(cmsLpopNoaudit.getMapindex().toString().trim()))
                                {
                                    CmsLpopNoaudit cmsLopNo = new CmsLpopNoaudit();

                                    cmsLopNo.setCpid(cmsLpopNoaudit.getCpid());
                                    cmsLopNo.setCpname(cmsLpopNoaudit.getCpname());
                                    cmsLopNo.setLpid(cmsLpopNoaudit.getLpid());
                                    // 2标示牌照方
                                    cmsLopNo.setLptype(CmsLpopNoauditConstants.lp_type);
                                    List<CmsLpopNoaudit> list = ds.getCmsLpopNoauditByCond(cmsLopNo);
                                    if (list.size() > 0)
                                    {
                                        cmsLopNo = list.get(0);
                                        if (check.checkCmsCpExist(cmsLopNo))
                                        {

                                            ds.removeCmsLpopNoaudit(cmsLopNo);
                                            continue;
                                        }
                                    }
                                    continue;
                                }
                                // 如果mapindex不为空表示当前对象是要删除的
                                ds.removeCmsLpopNoaudit(cmsLpopNoaudit);
                                iSuccessed++;
                                successID = iSuccessed.toString();
                                operateInfo = new OperateInfo(successID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.release.nonaudit.success"),
                                		ResourceMgt.findDefaultText("mes.lp.release.nonaudit.success"));//解除免审成功   牌照方解除免审成功

                                returnInfo.appendOperateInfo(operateInfo);

                                // 保存操作日志
                                CommonLogUtil.insertOperatorLog(cmsLpopNoaudit.getLpid(),
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_MGT,
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_CANCEL,
                                        CpspLogConstant.OPERTYPE_LP_NONAUDIT_CANCEL_INFO,
                                        CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            catch (Exception e)
                            {
                                log.error("removeCmsLpopNoaudit LS error.", e);
                                iFail++;
                                failID = iFail.toString();
                              //解除免审失败   牌照方解除免审失败
                                operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.release.nonaudit.fail"), ResourceMgt.findDefaultText("mes.lp.release.nonaudit.fail"));                       
                                returnInfo.appendOperateInfo(operateInfo);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            iSuccessed = 0;
            operateInfo = new OperateInfo("00000000", ResourceMgt.findDefaultText("mes.sys.wrong"), ResourceMgt.findDefaultText("mes.sys.wrong"), ResourceMgt.findDefaultText("mes.sys.wrong"));//系统故障
            returnInfo.appendOperateInfo(operateInfo);
        }
        log.debug("insertOperatorCmsLpopNoaudit LS end ...");
        if (cmsLpopNoauditList.size() == 0 || iFail == cmsLpopNoauditList.size())
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText("fail.count") + iFail + ""; // "350099"
        }
        else if (iFail > 0 && iFail < cmsLpopNoauditList.size())
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + iFail + ""; // "350098"
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        return returnInfo.toString();
    }

    /**
     * 新增运营商免审CP
     * 
     * @param cmsLpopNoauditList 免审对象集合
     * @return 成功返回 0, 失败返回 1
     */
    public String insertOperatorCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList)
    {
        log.debug("insertOperatorCmsLpopNoaudit LS begin ...");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer iFail = 0;
        Integer iSuccessed = 0;
        Integer iRemoved = 0;
        String failID = "";
        String successID = "";
        String CPID = "";
        try
        {
            if (null != cmsLpopNoauditList && cmsLpopNoauditList.size() > 0)
            {
                for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
                {
                    if (null != cmsLpopNoaudit)
                    {
                        CPID = cmsLpopNoaudit.getCpid();
                        // 校验CP是否已经存在，不存在则不新增
                        if (!check.checkCmsCpExist(cmsLpopNoaudit))
                        {
                            iFail++;
                            failID = iFail.toString();
                            operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), "配置免审失败",
                                    "该CP已经不存在");

                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }                        
                        if (null == cmsLpopNoaudit.getMapindex())
                        {// 封转对象的mapindex为空说明是要新增的
                            try
                            {
                                /**
                                 * CP免二审设置的时候判断该CP是否有内容状态是一审中（120），如果有则不能设置免二审。
                                 * **/
                                List<CmsProgram> proList=new ArrayList<CmsProgram>();
                                CmsProgram cmsProgram=new CmsProgram();
                                cmsProgram.setCpid(cmsLpopNoaudit.getCpid());
                                cmsProgram.setStatus(120);
                                proList=cmsProgramDS.getCmsProgramByCond(cmsProgram);
                                if(proList!=null&&proList.size()>0){
                                    iFail++;
                                    failID = iFail.toString();
                                    operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(),"配置免审失败",
                                        "该CP存在状态为一审中的内容,不能设置免二审");

                                    returnInfo.appendOperateInfo(operateInfo);
                                    continue;
                                }
                                
                                cmsLpopNoaudit.setLpid(CmsLpopNoauditConstants.op_type + "");
                                // 3表示运营商
                                cmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.op_type);                             
                                // 校验是否已经存在，存在则不新增
                                if (check.checkCmsOpExist(cmsLpopNoaudit))
                                {
                                    ds.insertCmsLpopNoaudit(cmsLpopNoaudit);
                                }
                                iSuccessed++;
                                successID = iSuccessed.toString();
                                operateInfo = new OperateInfo(successID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.nonaudit.set.success"),
                                		ResourceMgt.findDefaultText("mes.op.nonaudit.set.success"));//配置免审成功   运营商免审配置成功

                                returnInfo.appendOperateInfo(operateInfo);
                                // 保存操作日志
                                CommonLogUtil.insertOperatorLog(cmsLpopNoaudit.getLpid(),
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_MGT,
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_SET,
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_SET_INFO,
                                        CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            catch (Exception e)
                            {
                                log.error("insertOperatorCmsLpopNoaudit LS error.", e);
                                iFail++;
                                failID = iFail.toString();
                                //配置免审失败   运营商免审配置失败
                                operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.lisence.nonaudit.set"), 
                                		ResourceMgt.findDefaultText("mes.op.nonaudit.set.fail"));

                                returnInfo.appendOperateInfo(operateInfo);

                            }
                        }
                        else
                        {// 封装对象的mapindex不为空说明页面上面已经绑定了的是要删除的
                            try
                            {
                                if (cmsLpopNoaudit.getMapindex() == 0
                                        || "0".equals(cmsLpopNoaudit.getMapindex().toString().trim()))
                                {
                                    CmsLpopNoaudit cmsLopNo = new CmsLpopNoaudit();

                                    cmsLopNo.setCpid(cmsLpopNoaudit.getCpid());
                                    cmsLopNo.setCpname(cmsLpopNoaudit.getCpname());
                                    cmsLopNo.setLpid(CmsLpopNoauditConstants.op_type + "");
                                    // 3表示运营商
                                    cmsLopNo.setLptype(CmsLpopNoauditConstants.op_type);
                                    List<CmsLpopNoaudit> list = ds.getCmsLpopNoauditByCond(cmsLopNo);
                                    if (list.size() > 0)
                                    {
                                        cmsLopNo = list.get(0);
                                        if (check.checkCmsCpExist(cmsLopNo))
                                        {

                                            ds.removeCmsLpopNoaudit(cmsLopNo);
                                            iRemoved++;
                                            continue;
                                        }
                                    }
                                    iRemoved++;
                                    continue;
                                }
                                /**
                                 * CP免二审设置的时候判断该CP是否有内容状态是一审中（120），如果有则不能设置免二审。
                                 * **/
                                List<CmsProgram> proList=new ArrayList<CmsProgram>();
                                CmsProgram cmsProgram=new CmsProgram();
                                cmsProgram.setCpid(cmsLpopNoaudit.getCpid());
                                cmsProgram.setStatus(120);
                                proList=cmsProgramDS.getCmsProgramByCond(cmsProgram);
                                if(proList!=null&&proList.size()>0){
                                    iFail++;
                                    failID = iFail.toString();
                                    operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(), "配置免审失败",
                                            "该CP存在状态为一审中的内容,不能设置免二审");
                                    returnInfo.appendOperateInfo(operateInfo);
                                    continue;
                                }
                                cmsLpopNoaudit.setLpid(CmsLpopNoauditConstants.op_type + "");
                                ds.removeCmsLpopNoaudit(cmsLpopNoaudit);
                                iSuccessed++;
                                successID = iSuccessed.toString();
                                operateInfo = new OperateInfo(successID, cmsLpopNoaudit.getCpid(), ResourceMgt.findDefaultText("mes.release.nonaudit.success"),
                                		ResourceMgt.findDefaultText("mes.op.release.nonaudit.success"));// 解除免审成功   运营商解除免审成功

                                returnInfo.appendOperateInfo(operateInfo);
                                // 保存操作日志
                                CommonLogUtil.insertOperatorLog(cmsLpopNoaudit.getLpid(),
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_MGT,
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_CANCEL,
                                        CpspLogConstant.OPERTYPE_OP_NONAUDIT_CANCEL_INFO,
                                        CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            catch (Exception e)
                            {
                                log.error("insertOperatorCmsLpopNoaudit LS error.", e);
                                iFail++;
                                failID = iFail.toString();
                                //解除免审失败   运营商解除免审失败
                                operateInfo = new OperateInfo(failID, cmsLpopNoaudit.getCpid(),ResourceMgt.findDefaultText("mes.release.nonaudit.fail") , 
                                		ResourceMgt.findDefaultText("mes.op.release.nonaudit.fail"));

                                returnInfo.appendOperateInfo(operateInfo);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            iSuccessed = 0;
            operateInfo = new OperateInfo("00000000",ResourceMgt.findDefaultText("mes.sys.wrong") ,ResourceMgt.findDefaultText("mes.sys.wrong") , ResourceMgt.findDefaultText("mes.sys.wrong"));//系统故障
            returnInfo.appendOperateInfo(operateInfo);
        }
        log.debug("insertOperatorCmsLpopNoaudit LS end ...");
        if (cmsLpopNoauditList.size() == 0 || iFail == cmsLpopNoauditList.size())
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText("fail.count") + iFail + ""; // "350099"
        }
        else if (iFail > 0 && iFail < cmsLpopNoauditList.size())
        {
            flag = "2"; // "2"
            errorcode = ResourceMgt.findDefaultText("success.count")+ iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + iFail + ""; // "350098"
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        return returnInfo.toString();
    }
    
    
    

    /**
     * 查询免一审、免二审、免审、直通CP
     */
    public TableDataInfo pageInfoEachSetQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pagesize)
    throws DomainServiceException
    {
        TableDataInfo dataInfo = null;
    	try
    	{
            if (cmsLpopNoaudit.getCpid()!=null&&!"".equals(cmsLpopNoaudit.getCpid()))
            {
            	cmsLpopNoaudit.setCpid(EspecialCharMgt.conversion(cmsLpopNoaudit.getCpid()));
            }
            if (cmsLpopNoaudit.getCpname()!=null&&!"".equals(cmsLpopNoaudit.getCpname()))
            {
            	cmsLpopNoaudit.setCpname(EspecialCharMgt.conversion(cmsLpopNoaudit.getCpname()));
            }
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsLpopNoaudit.setOperid(operinfo.getOperid());
            dataInfo = ds.pageInfoQuery(cmsLpopNoaudit, start, pagesize);
    		
    	}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
        }
        return dataInfo;
    } 
    
    /**
     * 查询牌照放免审CP
     */
    public TableDataInfo pageInfoQuery(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
            throws DomainServiceException
    {
        // 1表示CP
        ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);
        // 2表示牌照方
        ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.lp_type);
        ucpCmsLpopNoaudit.setStatus(1);
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mapindex");
        return ds.pageInfoQuery(ucpCmsLpopNoaudit, start, pagesize, puEntity);
    }

    /**
     * 查询运营商免审的CP
     */
    public TableDataInfo pageInfoOperatorQuery(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
            throws DomainServiceException
    {
        // 从session中获取操作员牌照ＩＤ
        ucpCmsLpopNoaudit.setLpid(CmsLpopNoauditConstants.op_type + "");
        ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);
        ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.op_type);
        ucpCmsLpopNoaudit.setStatus(1);
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mapindex");
        return ds.pageInfoQuery(ucpCmsLpopNoaudit, start, pagesize, puEntity);
    }

    /**
     * 查询牌照方list
     * 
     * @return 牌照方免审对象集合
     */
    public List<UcpCmsLpopNoaudit> getCmsLpNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("getCmsLpNoauditList LS begin ...");
        List<UcpCmsLpopNoaudit> list = null;

        if (null != ucpCmsLpopNoaudit)
        {
            try
            {
                // 1表示CP
                ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);// 1标示CP
                // 2表示牌照方
                ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.lp_type);// 2标示牌照方
                list = ds.getCmsLpopNoauditList(ucpCmsLpopNoaudit);

                for (int i = 0; i < list.size(); i++)
                {// 把CP状态为已删除的从集合中remove 只显示可以绑定的
                    if (list.get(i).getStatus() != null)
                    {
                        if (list.get(i).getStatus() == 4)
                        {// (4删除)(1正常,21同步中,40已同步,41新增同步失败,42删除同步失败)
                            list.remove(i);
                        }
                    }
                }

            }
            catch (DomainServiceException e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
            catch (NumberFormatException e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
            catch (Exception e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
        }
        log.debug("getCmsLpNoauditList LS end ...");
        return list;
    }

    /*
     * 查询CP信息 在牌照方设置免审时以table形式展示
     */
    public TableDataInfo getCmsLPNoauditListTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable starting...");
        TableDataInfo lplpTable = null;

        try
        {
            // 1表示CP
            ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);// 1标示CP
            // 2表示牌照方
            ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.lp_type);// 2标示牌照方
            ucpCmsLpopNoaudit.setStatus(4);
            try
            {
                if (null != ucpCmsLpopNoaudit.getCpid() && !ucpCmsLpopNoaudit.getCpid().equals(""))
                {
                    String cpid = EspecialCharMgt.conversion(ucpCmsLpopNoaudit.getCpid());
                    ucpCmsLpopNoaudit.setCpid(cpid);
                }
                if (null != ucpCmsLpopNoaudit.getCpcnshortname() && !ucpCmsLpopNoaudit.getCpcnshortname().equals(""))
                {
                    String Cpcnshortname = EspecialCharMgt.conversion(ucpCmsLpopNoaudit.getCpcnshortname());
                    ucpCmsLpopNoaudit.setCpcnshortname(Cpcnshortname);
                }
                lplpTable = ds.pageInfoQueryForOPTable(ucpCmsLpopNoaudit, start, pageSize);
            }
            catch (DAOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable end");
        }
        catch (Exception e)
        {
            log.error("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable exception:" + e);
        }

        return lplpTable;
    }

    /**
     * 查询牌照方list
     * 
     * @return 牌照方免审对象集合(返回null) param 均为不可能的条件 使table初始化提示：对不起，没有符合条件的记录
     */
    public List<UcpCmsLpopNoaudit> noCmsLpNoauditListTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
    {
        log.debug("getCmsLpNoauditList LS begin ...");
        List<UcpCmsLpopNoaudit> list = null;

        if (null != ucpCmsLpopNoaudit)
        {
            try
            {

                ucpCmsLpopNoaudit.setCptype(-1);// 不存在的Cptype

                ucpCmsLpopNoaudit.setLptype(-2);// 不存在的Lptype

                list = ds.getCmsLpopNoauditList(ucpCmsLpopNoaudit);
            }
            catch (DomainServiceException e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
            catch (NumberFormatException e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
            catch (Exception e1)
            {
                log.error("getCmsLpNoauditList LS error.", e1);
            }
        }
        log.debug("getCmsLpNoauditList LS end ...");
        return list;

    }

    /**
     * 牌照方免审设置
     */
    public TableDataInfo noCmsLpNoauditListTripsTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
    {
        log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable starting...");
        TableDataInfo lplpTable = null;

        try
        {
            // 1表示CP
            ucpCmsLpopNoaudit.setCptype(-1);// 不存在标示CP
            // 2表示牌照方
            ucpCmsLpopNoaudit.setLptype(-2);// 不存在牌照方
            ucpCmsLpopNoaudit.setStatus(4);
            try
            {
                lplpTable = ds.pageInfoQueryForOPTable(ucpCmsLpopNoaudit, start, pagesize);
            }
            catch (DAOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable end");
        }
        catch (Exception e)
        {
            log.error("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable exception:" + e);
        }

        return lplpTable;

    }

    /**
     * 查询运营商list
     * 
     * @param ucpCmsLpopNoaudit 免审对象
     * @return 运营商免审对象集合
     */
    public List<UcpCmsLpopNoaudit> getCmsOpNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("getCmsOpNoauditList LS begin ...");
        List<UcpCmsLpopNoaudit> list = null;
        if (null != ucpCmsLpopNoaudit)
        {
            try
            {
                ucpCmsLpopNoaudit.setLpid(CmsLpopNoauditConstants.op_type + "");// 3运营商
                ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);// 1cp
                ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.op_type);// 3运营商
                list = ds.getCmsLpopNoauditList(ucpCmsLpopNoaudit);

                for (int i = 0; i < list.size(); i++)
                {// 把CP状态为已删除的从集合中remove 只显示可以绑定的
                    if (list.get(i).getStatus() != null)
                    {
                        if (list.get(i).getStatus() == 4)
                        {// (4删除)(1正常,21同步中,40已同步,41新增同步失败,42删除同步失败)
                            list.remove(i);
                        }
                    }
                }

            }
            catch (DomainServiceException e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
            catch (NumberFormatException e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
            catch (Exception e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
        }

        log.debug("getCmsOpNoauditList LS end ...");
        return list;
    }

    /*
     * 查询CP信息 在运营商设置免审时以table形式展示
     */
    public TableDataInfo getCmsOpNoauditListTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable starting...");
        TableDataInfo opopTable = null;

        try
        {
            ucpCmsLpopNoaudit.setLpid(CmsLpopNoauditConstants.op_type + "");
            ucpCmsLpopNoaudit.setCptype(CmsLpopNoauditConstants.cp_type);
            ucpCmsLpopNoaudit.setLptype(CmsLpopNoauditConstants.op_type);
            // ucpCmsLpopNoaudit.setStatus(4);
            try
            {
                if (null != ucpCmsLpopNoaudit.getCpid() && !ucpCmsLpopNoaudit.getCpid().equals(""))
                {
                    String cpid = EspecialCharMgt.conversion(ucpCmsLpopNoaudit.getCpid());
                    ucpCmsLpopNoaudit.setCpid(cpid);
                }
                if (null != ucpCmsLpopNoaudit.getCpcnshortname() && !ucpCmsLpopNoaudit.getCpcnshortname().equals(""))
                {
                    String Cpcnshortname = EspecialCharMgt.conversion(ucpCmsLpopNoaudit.getCpcnshortname());
                    ucpCmsLpopNoaudit.setCpcnshortname(Cpcnshortname);
                }

                opopTable = ds.pageInfoQueryForOPTable(ucpCmsLpopNoaudit, start, pageSize);
            }
            catch (DAOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable end");
        }
        catch (Exception e)
        {
            log.error("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable exception:" + e);
        }

        return opopTable;
    }

    /**
     * 查询运营商list
     * 
     * @param ucpCmsLpopNoaudit 免审对象
     * @return 为空给出无运营商提示
     */
    public List<UcpCmsLpopNoaudit> noOpNoauditListTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("getCmsOpNoauditList LS begin ...");
        List<UcpCmsLpopNoaudit> list = null;
        if (null != ucpCmsLpopNoaudit)
        {
            try
            {
                ucpCmsLpopNoaudit.setLpid("-0");
                ucpCmsLpopNoaudit.setCptype(-2);
                ucpCmsLpopNoaudit.setLptype(-3);
                list = ds.getCmsLpopNoauditList(ucpCmsLpopNoaudit);

            }
            catch (DomainServiceException e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
            catch (NumberFormatException e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
            catch (Exception e)
            {
                log.error("getCmsOpNoauditList LS error.", e);
            }
        }

        log.debug("getCmsOpNoauditList LS end ...");
        return list;
    }

    /**
     * 查询CP信息 在运营商设置免审时以table形式展示 无数据时给出提示
     */
    public TableDataInfo noOpNoauditListTableTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
    {
        log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable starting...");
        TableDataInfo opopTable = null;

        try
        {
            ucpCmsLpopNoaudit.setLpid("-0");
            ucpCmsLpopNoaudit.setCptype(-2);
            ucpCmsLpopNoaudit.setLptype(-3);
            // ucpCmsLpopNoaudit.setStatus(4);
            try
            {
                opopTable = ds.pageInfoQueryForOPTable(ucpCmsLpopNoaudit, start, pageSize);
            }
            catch (DAOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            log.debug("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable end");
        }
        catch (Exception e)
        {
            log.error("com.zte.cms.cpsp.ls IcmCpidmgtLS pageInfoQueryForTable exception:" + e);
        }

        return opopTable;
    }

    /**
     * 免审直通设置取消  lfh20120930
     * @throws DomainServiceException 
     */
    public String removeNoauditByCond(List<CmsLpopNoaudit> cmsLpopNoauditCpidList)throws DomainServiceException{
    	log.debug("saveLpopNoaudit starting....");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1; 
		try
		{
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";           
    		for(CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditCpidList)
    		{
                message = removeNoauditoneByCond(cmsLpopNoaudit);
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    operateInfo = new OperateInfo(String.valueOf(num), cmsLpopNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    fail++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), cmsLpopNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                num++;
    		}
            if (success == cmsLpopNoauditCpidList.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == cmsLpopNoauditCpidList.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("saveLpopNoaudit end");
        return returnInfo.toString();     	
    }
    public String removeNoauditoneByCond(CmsLpopNoaudit cmsLpopNoauditCpid)throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
		log.debug("removeNoauditByCond starting....");
		try
		{
    		UcpBasic ucpBasic = new UcpBasic();
			String cpid =cmsLpopNoauditCpid.getCpid();	
			ucpBasic.setCpid(cpid);
			ucpBasic.setCptype(1);
			
            List<UcpBasic> ucpBasiclist = ucpBasicDS.getUcpBasicByCond(ucpBasic);
            if (ucpBasiclist != null && ucpBasiclist.size() > 0 && ucpBasiclist.get(0).getStatus() == 1)
            {
				CmsLpopNoaudit cmsLpopNoauditTemp = new CmsLpopNoaudit();
				cmsLpopNoauditTemp = ds.getCmsLpopNoaudit(cmsLpopNoauditCpid);
				if(cmsLpopNoauditTemp != null&&!cmsLpopNoauditTemp.equals(""))
				{
					ds.removeCmsLpopNoaudit(cmsLpopNoauditCpid);
					switch (cmsLpopNoauditCpid.getLptype())
					{
					    case CmsLpopNoauditConstants.cp_type:
			                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
			                        CommonLogConstant.OPERTYPE_CP_NOAUDITONEDEL, CommonLogConstant.OPERTYPE_CP_NOAUDITONEDEL_INFO,
			                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
			                break;

					    case CmsLpopNoauditConstants.op_type:
			                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
			                        CommonLogConstant.OPERTYPE_CP_NOAUDITTWODEL, CommonLogConstant.OPERTYPE_CP_NOAUDITTWODEL_INFO,
			                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
			                break;

					    case CmsLpopNoauditConstants.noaudit_type:
			                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
			                        CommonLogConstant.OPERTYPE_CP_NOAUDITDEL, CommonLogConstant.OPERTYPE_CP_NOAUDITDEL_INFO,
			                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);	
			                break;

			           case CmsLpopNoauditConstants.directToSync_type:
			                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
			                        CommonLogConstant.OPERTYPE_CP_PASSDEL, CommonLogConstant.OPERTYPE_CP_PASSDEL_INFO,
			                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);	
			                break;
			                
			          default:
			                break;

					}
					
				}
				else
				{
					switch (cmsLpopNoauditCpid.getLptype())
					{
					    case CmsLpopNoauditConstants.cp_type:
				        	return "1:操作失败,该CP已经不处于免一审状态";

					    case CmsLpopNoauditConstants.op_type:
				        	return "1:操作失败,该CP已经不处于免二审状态";
					    case CmsLpopNoauditConstants.noaudit_type:
				        	return "1:操作失败,该CP已经不处于免审状态";
					    case CmsLpopNoauditConstants.directToSync_type:
				        	return "1:操作失败,该CP已经不处于直通状态";	
				        default:
				        	return "1:操作失败 ";
					}
				}
            }

			else
			{
	        	return "1:操作失败,该CP处于暂停或已删除状态";
			}

		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            throw e;
        }
		return result;
    }
    
    //供cp查询调用
    private boolean checkCpidSet(String ucpCpid,String cpidSql){
    	boolean hadSet = false;
    	
		List ucpidList;		
		try {
			ucpidList = dbUtil.getQuery(cpidSql);
			log.debug("ucpidList:::::::" + ucpidList);					
			for (int j = 0; j < ucpidList.size(); j++) {
				log.debug("ucpidList.get(i).toString()"+ucpidList.get(j).toString());
				String str = ucpidList.get(j).toString().substring(
						ucpidList.get(j).toString().length() - 9,
						ucpidList.get(j).toString().length() - 1);
				log.debug("str------"+str);
				if (str.equals(ucpCpid)) 
				{
					hadSet = true;
				    break;
				}
				else{
					hadSet=false;
				}			
			}					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hadSet;
    }
    /**
	 * ucp_baisc表cp查询，供免一审设置用  lfh20121006
	 * @throws DomainServiceException 
	 */
    public TableDataInfo pageInfoQueryUcpbasicFir(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException {
    	log.debug("ucpBasic.cptype"+ucpBasic.getCptype());
    	log.debug("ucpBasic.cpCpcnshortname"+ucpBasic.getCpcnshortname());
    	log.debug("ucpBasic.cpCpid"+ucpBasic.getCpid());
		// 1 查询ucpBasic结果返回值
		TableDataInfo tableInfoCpf = new TableDataInfo();
		PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("cpid");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        ucpBasic.setSagid(operinfo.getOperid());
        if (ucpBasic.getCpid()!=null && !ucpBasic.getCpid().equals(""))
        {
            ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
        }
        if (ucpBasic.getCpcnshortname()!=null && !ucpBasic.getCpcnshortname().equals(""))
        {
            ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
        }
        tableInfoCpf = cmsUcpBasicDS.pageInfoQuery(ucpBasic, 0, 100000, puEntity);
        
        List ubList = tableInfoCpf.getData();
		List ucpbascTempList = new ArrayList();
		//log.debug("typepara-----------"+typePara);
		// 2 首先判断要查询的CP是否存在
		if (ubList.size() < 1) {
			// tableInfoCp.setData(ubList);
			tableInfoCpf.setTotalCount(0);
			return tableInfoCpf;
		} 
		else {
			for (int i = 0; i < ubList.size(); i++) {
				boolean hadSet = false;
				UcpBasic ub = (UcpBasic) ubList.get(i);
				String ucpCpid = ub.getCpid();
				String cpidSql = "";
				cpidSql = "select cpid from zxdbm_cms.cms_lpop_noaudit where lptype in (1,4)";
				hadSet = checkCpidSet(ucpCpid,cpidSql);				
				if (!hadSet) {
					ucpbascTempList.add(ub);
				}
			}
	        PageInfo pageInfo = null;

			int fetchSize = pageSize > (ucpbascTempList.size() - start) ? (ucpbascTempList.size() - start) : pageSize;
	        List<UcpBasic> ucpBasicList = new ArrayList<UcpBasic>();
	        for(int j = start;j<start+fetchSize;j++)
	        {
	        	ucpBasicList.add((UcpBasic)ucpbascTempList.get(j));
	        }
	        pageInfo = new PageInfo(start, ucpbascTempList.size(), fetchSize, ucpBasicList);
	        tableInfoCpf.setData((List<UcpBasic>)pageInfo.getResult());
	        tableInfoCpf.setTotalCount(ucpbascTempList.size());
		}
		return tableInfoCpf;
    }
    /**
	 * ucp_baisc表cp查询，供免二审设置用  lfh20121006
	 * @throws DomainServiceException 
	 */
    public TableDataInfo pageInfoQueryUcpbasicSec(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException {
		// 1 查询ucpBasic结果返回值
		TableDataInfo tableInfoCpe = new TableDataInfo();
		PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("cpid");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        ucpBasic.setSagid(operinfo.getOperid());
        if (ucpBasic.getCpid()!=null && !ucpBasic.getCpid().equals(""))
        {
            ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
        }
        if (ucpBasic.getCpcnshortname()!=null && !ucpBasic.getCpcnshortname().equals(""))
        {
            ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
        }        
        tableInfoCpe = cmsUcpBasicDS.pageInfoQuery(ucpBasic, 0, 100000, puEntity);
                
        List ubList = tableInfoCpe.getData();
		List ucpbascTempList = new ArrayList();
		// 2 首先判断要查询的CP是否存在
		if (ubList.size() < 1) {
			// tableInfoCp.setData(ubList);
			tableInfoCpe.setTotalCount(0);
			return tableInfoCpe;
		} 
		else {
			for (int i = 0; i < ubList.size(); i++) {
				boolean hadSet = false;
				UcpBasic ub = (UcpBasic) ubList.get(i);
				String ucpCpid = ub.getCpid();
				log.debug("待验证ucpCpid："+ucpCpid);
				String cpidSql = "select cpid from zxdbm_cms.cms_lpop_noaudit where lptype in (4,3)";
				hadSet = checkCpidSet(ucpCpid,cpidSql);				
				if (!hadSet) {
					ucpbascTempList.add(ub);
				}
			}
	        PageInfo pageInfo = null;

			int fetchSize = pageSize > (ucpbascTempList.size() - start) ? (ucpbascTempList.size() - start) : pageSize;
	        List<UcpBasic> ucpBasicList = new ArrayList<UcpBasic>();
	        for(int j = start;j<start+fetchSize;j++)
	        {
	        	ucpBasicList.add((UcpBasic)ucpbascTempList.get(j));
	        }
	        pageInfo = new PageInfo(start, ucpbascTempList.size(), fetchSize, ucpBasicList);
	        tableInfoCpe.setData((List<UcpBasic>)pageInfo.getResult());
	        tableInfoCpe.setTotalCount(ucpbascTempList.size());
		}
		return tableInfoCpe;
    }
    /**
	 * ucp_baisc表cp查询，供免审设置用  lfh20121006
	 * @throws DomainServiceException 
	 */
    public TableDataInfo pageInfoQueryUcpbasicAll(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException {
		// 1 查询ucpBasic结果返回值
		TableDataInfo tableInfoCpa = new TableDataInfo();
		PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("cpid");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        ucpBasic.setSagid(operinfo.getOperid());
        if (ucpBasic.getCpid()!=null && !ucpBasic.getCpid().equals(""))
        {
            ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
        }
        if (ucpBasic.getCpcnshortname()!=null && !ucpBasic.getCpcnshortname().equals(""))
        {
            ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
        }      
        tableInfoCpa = cmsUcpBasicDS.pageInfoQuery(ucpBasic, 0, 100000, puEntity);
        
        List ubList = tableInfoCpa.getData();
		List ucpbascTempList = new ArrayList();
		//log.debug("typepara-----------"+typePara);
		// 2 首先判断要查询的CP是否存在
		if (ubList.size() < 1) {
			// tableInfoCp.setData(ubList);
			tableInfoCpa.setTotalCount(0);
			return tableInfoCpa;
		} 
		else {
			for (int i = 0; i < ubList.size(); i++) {
				boolean hadSet = false;
				UcpBasic ub = (UcpBasic) ubList.get(i);
				String ucpCpid = ub.getCpid();
				String cpidSql = "select cpid from zxdbm_cms.cms_lpop_noaudit where lptype in (1,4,3)";
				hadSet = checkCpidSet(ucpCpid,cpidSql);
				if (!hadSet) {
					ucpbascTempList.add(ub);
				}				
			}
	        PageInfo pageInfo = null;

			int fetchSize = pageSize > (ucpbascTempList.size() - start) ? (ucpbascTempList.size() - start) : pageSize;
	        List<UcpBasic> ucpBasicList = new ArrayList<UcpBasic>();
	        for(int j = start;j<start+fetchSize;j++)
	        {
	        	ucpBasicList.add((UcpBasic)ucpbascTempList.get(j));
	        }
	        pageInfo = new PageInfo(start, ucpbascTempList.size(), fetchSize, ucpBasicList);
	        tableInfoCpa.setData((List<UcpBasic>)pageInfo.getResult());
	        tableInfoCpa.setTotalCount(ucpbascTempList.size());
		}
		return tableInfoCpa;
    }
    /**
	 * ucp_baisc表cp查询，供直通设置用  lfh20121006
	 * @throws DomainServiceException 
	 */
    public TableDataInfo pageInfoQueryUcpbasicDir(UcpBasic ucpBasic, int start, int pageSize) throws DomainServiceException {
		// 1 查询ucpBasic结果返回值
		TableDataInfo tableInfoCpd = new TableDataInfo();
		PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("cpid");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        ucpBasic.setSagid(operinfo.getOperid());
        if (ucpBasic.getCpid()!=null && !ucpBasic.getCpid().equals(""))
        {
            ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
        }
        if (ucpBasic.getCpcnshortname()!=null && !ucpBasic.getCpcnshortname().equals(""))
        {
            ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
        }        
        tableInfoCpd = cmsUcpBasicDS.pageInfoQuery(ucpBasic, 0, 100000, puEntity);
        
        List ubList = tableInfoCpd.getData();
		List ucpbascTempList = new ArrayList();
		//log.debug("typepara-----------"+typePara);
		// 2 首先判断要查询的CP是否存在
		if (ubList.size() < 1) {
			// tableInfoCp.setData(ubList);
			tableInfoCpd.setTotalCount(0);
			return tableInfoCpd;
		} 
		else {
			for (int i = 0; i < ubList.size(); i++) {
				boolean hadSet = false;
				UcpBasic ub = (UcpBasic) ubList.get(i);
				String ucpCpid = ub.getCpid();
				String cpidSql = "select cpid from zxdbm_cms.cms_lpop_noaudit where lptype in (5)";
				hadSet = checkCpidSet(ucpCpid,cpidSql);
				if (!hadSet) {
					ucpbascTempList.add(ub);
				}		
			}
	        PageInfo pageInfo = null;

			int fetchSize = pageSize > (ucpbascTempList.size() - start) ? (ucpbascTempList.size() - start) : pageSize;
	        List<UcpBasic> ucpBasicList = new ArrayList<UcpBasic>();
	        for(int j = start;j<start+fetchSize;j++)
	        {
	        	ucpBasicList.add((UcpBasic)ucpbascTempList.get(j));
	        }
	        pageInfo = new PageInfo(start, ucpbascTempList.size(), fetchSize, ucpBasicList);
	        tableInfoCpd.setData((List<UcpBasic>)pageInfo.getResult());
	        tableInfoCpd.setTotalCount(ucpbascTempList.size());
		}
		return tableInfoCpd;
    }
  
    /**
	 * 免一审设置  20121006
	 */
/*
    public String saveLpopFirNoaudi(List<UcpBasic> cmsLpopNoFirstauditList)throws DomainServiceException{
    	log.debug("saveLpopNoaudit starting....");
    	log.debug("cmsLpopNoFirstauditList.size()+"+cmsLpopNoFirstauditList.size());
        String result = "070000";// 设置操作结果为真
    	if (cmsLpopNoFirstauditList != null && cmsLpopNoFirstauditList.size() > 0) // 如果存在
		{
			for(UcpBasic ucbcpFirNoaudit : cmsLpopNoFirstauditList){
				log.debug("cpFirNoaudit.cpid"+ucbcpFirNoaudit.getCpid());
				long cpindex = ucbcpFirNoaudit.getCpindex();
				log.debug("cpFirNoaudit.cpindex"+ucbcpFirNoaudit.getCpindex());
				String cpid =ucbcpFirNoaudit.getCpid();
				UcpBasic ucpBasic = new UcpBasic();
				ucpBasic.setCpindex(cpindex);				
				CmsLpopNoaudit cpFirNoaudit = new CmsLpopNoaudit();
				try {
					ucpBasic=ucpBasicDS.getUcpBasic(ucpBasic);
	                //ucpBasiclist = ucpBasicDS.getUcpBasicByCond(ucpBasic);					
				} catch (DomainServiceException e) {
					result = "070001";
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//如果CP存在				
				if(ucpBasic != null&&!ucpBasic.equals("")){	
					cpFirNoaudit.setCpid(cpid);
					cpFirNoaudit.setCpname(ucpBasic.getCpcnshortname());
					cpFirNoaudit.setLptype(1);
					// 校验是否已经存在，存在则不新增
                    if (check.checkCmsLpExist(cpFirNoaudit)) {
						try {
							ds.insertCmsLpopNoaudit(cpFirNoaudit);
						} catch (DomainServiceException e) {
							result = "070001";
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}  		
		}
    	log.debug("result::"+result);
		return result;    	
    }
*/    
    public String saveLpopFirNoaudit(List<UcpBasic> cmsLpopNoFirstauditList)throws DomainServiceException
    {
    	log.debug("saveLpopNoaudit starting....");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1; 
		try
		{
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";           
    		for(UcpBasic ucbcpSecNoaudit : cmsLpopNoFirstauditList)
    		{
                message = saveLpopFirNoauditbyone(ucbcpSecNoaudit);
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    fail++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                num++;
    		}
            if (success == cmsLpopNoFirstauditList.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == cmsLpopNoFirstauditList.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("saveLpopNoaudit end");
        return returnInfo.toString(); 
    	
    }
    public String saveLpopFirNoauditbyone(UcpBasic ucbcpFirNoaudit)throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{
    		CmsLpopNoaudit cpFirNoaudit = new CmsLpopNoaudit();
    		UcpBasic ucpBasic = new UcpBasic();
			long cpindex = ucbcpFirNoaudit.getCpindex();
			String cpid =ucbcpFirNoaudit.getCpid();	
			ucpBasic.setCpindex(cpindex);
			ucpBasic.setStatus(1);
    		List <UcpBasic> ucpBasicList = new ArrayList <UcpBasic>();
    		ucpBasicList=ucpBasicDS.getUcpBasicByCond(ucpBasic);
			if(ucpBasicList != null&&ucpBasicList.size()!=0)
			{	ucpBasic = ucpBasicList.get(0);
				cpFirNoaudit.setCpid(cpid);
				cpFirNoaudit.setCpname(ucpBasic.getCpcnshortname());
				cpFirNoaudit.setLptype(CmsLpopNoauditConstants.cp_type);
                if (check.checkCmsLpExist(cpFirNoaudit)) 
                {
                	cpFirNoaudit.setLptype(CmsLpopNoauditConstants.noaudit_type);
                    if (check.checkCmsLpExist(cpFirNoaudit)) 
                    {
                    	cpFirNoaudit.setLptype(CmsLpopNoauditConstants.cp_type);
					ds.insertCmsLpopNoaudit(cpFirNoaudit);
	                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
	                        CommonLogConstant.OPERTYPE_CP_NOAUDITONEADD, CommonLogConstant.OPERTYPE_CP_NOAUDITONEADD_INFO,
	                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
        	        	return "1:操作失败,该CP已经处于免审状态";
                    }                
                }
                else
                {
    	        	return "1:操作失败,该CP已经处于免一审状态";
                }
			}
			else
			{
	        	return "1:操作失败,该CP处于暂停或已删除状态";				
			}    		
    	}
    	catch(Exception e)
    	{
            e.printStackTrace();
            throw e;    		
    	}
    	return result; 

    }    
    /**
	 * 免二审设置  20121006
	 */
    public String saveSecondNoaudit(List<UcpBasic> cmsLpopNoSecNoauditList)throws DomainServiceException
    {
    	log.debug("saveLpopNoaudit starting....");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1; 
		try
		{
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";           
    		for(UcpBasic ucbcpSecNoaudit : cmsLpopNoSecNoauditList)
    		{
                message = saveSecondNoauditbyone(ucbcpSecNoaudit);
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    fail++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                num++;
    		}
            if (success == cmsLpopNoSecNoauditList.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == cmsLpopNoSecNoauditList.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("saveLpopNoaudit end");
        return returnInfo.toString();     	
    }
			
    public String saveSecondNoauditbyone(UcpBasic ucbcpSecNoaudit)throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{
    		CmsLpopNoaudit cpSecNoaudit = new CmsLpopNoaudit();
    		UcpBasic ucpBasic = new UcpBasic();
			long cpindex = ucbcpSecNoaudit.getCpindex();
			String cpid =ucbcpSecNoaudit.getCpid();	
			ucpBasic.setCpindex(cpindex);
			ucpBasic.setStatus(1);
    		List <UcpBasic> ucpBasicList = new ArrayList <UcpBasic>();
    		ucpBasicList=ucpBasicDS.getUcpBasicByCond(ucpBasic);
			if(ucpBasicList != null&&ucpBasicList.size()!=0)
			{	
				ucpBasic = ucpBasicList.get(0);
				cpSecNoaudit.setCpid(cpid);
				cpSecNoaudit.setCpname(ucpBasic.getCpcnshortname());
				cpSecNoaudit.setLptype(CmsLpopNoauditConstants.op_type);
                if (check.checkCmsLpExist(cpSecNoaudit)) 
                {
                	cpSecNoaudit.setLptype(CmsLpopNoauditConstants.noaudit_type);
                    if (check.checkCmsLpExist(cpSecNoaudit)) 
                    {
        				cpSecNoaudit.setLptype(CmsLpopNoauditConstants.op_type);
    					ds.insertCmsLpopNoaudit(cpSecNoaudit);
    	                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
    	                        CommonLogConstant.OPERTYPE_CP_NOAUDITTWOADD, CommonLogConstant.OPERTYPE_CP_NOAUDITTWOADD_INFO,
    	                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);             	
                    }
                    else
                    {
        	        	return "1:操作失败,该CP已经处于免审状态";

                    }
                }
                else
                {
    	        	return "1:操作失败,该CP已经处于免二审状态";
                }
			}
			else
			{
	        	return "1:操作失败,该CP处于暂停或已删除状态";				
			}    		
    	}
    	catch(Exception e)
    	{
            e.printStackTrace();
            throw e;    		
    	}
    	return result; 

    }
			   
    public String saveLpopNoauditbyone(UcpBasic ucbcpNoaudit)throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

    	try
    	{
    		CmsLpopNoaudit cpNoaudit = new CmsLpopNoaudit();
    		UcpBasic ucpBasic = new UcpBasic();
			long cpindex = ucbcpNoaudit.getCpindex();
			String cpid =ucbcpNoaudit.getCpid();	
			ucpBasic.setCpindex(cpindex);
			ucpBasic.setStatus(1);
    		List <UcpBasic> ucpBasicList = new ArrayList <UcpBasic>();
    		ucpBasicList=ucpBasicDS.getUcpBasicByCond(ucpBasic);
			if(ucpBasicList != null&&ucpBasicList.size()!=0)
			{	
				ucpBasic = ucpBasicList.get(0);
				cpNoaudit.setCpid(cpid);
				cpNoaudit.setCpname(ucpBasic.getCpcnshortname());
				cpNoaudit.setLptype(CmsLpopNoauditConstants.noaudit_type);
                if (check.checkCmsLpExist(cpNoaudit)) 
                {
                	cpNoaudit.setLptype(CmsLpopNoauditConstants.cp_type);
                    if (check.checkCmsLpExist(cpNoaudit)) 
                    {
                    	cpNoaudit.setLptype(CmsLpopNoauditConstants.op_type);
                        if (check.checkCmsLpExist(cpNoaudit)) 
                        {
            				cpNoaudit.setLptype(CmsLpopNoauditConstants.noaudit_type);
        					ds.insertCmsLpopNoaudit(cpNoaudit);
        	                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
        	                        CommonLogConstant.OPERTYPE_CP_NOAUDITADD, CommonLogConstant.OPERTYPE_CP_NOAUDITADD_INFO,
        	                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        else
                        {
            	        	return "1:操作失败,该CP已经处于免二审状态";
                        	
                        }                    	
                    }
                    else
                    {
        	        	return "1:操作失败,该CP已经处于免一审状态";

                    }
                }
                else
                {
    	        	return "1:操作失败,该CP已经处于免审状态";
                }
			}
			else
			{
	        	return "1:操作失败,该CP处于暂停或已删除状态";				
			}    		
    	}
    	catch(Exception e)
    	{
            e.printStackTrace();
            throw e;    		
    	}
    	return result; 

    }   
    
    /**
	 * 免审设置  20121012
	 */
    public String saveLpopNoaudit(List<UcpBasic> cmsLpopNoauditList)throws DomainServiceException
    {
    	log.debug("saveLpopNoaudit starting....");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1; 
		try
		{
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";           
    		for(UcpBasic ucbcpSecNoaudit : cmsLpopNoauditList)
    		{
                message = saveLpopNoauditbyone(ucbcpSecNoaudit);
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    fail++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                num++;
    		}
            if (success == cmsLpopNoauditList.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == cmsLpopNoauditList.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("saveLpopNoaudit end");
        return returnInfo.toString();
    }
    public String saveDirsyncCpByone(UcpBasic cmsLpopDirSync)throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

    	try
    	{
    		CmsLpopNoaudit cpDirSync = new CmsLpopNoaudit();
    		UcpBasic ucpBasic = new UcpBasic();
			long cpindex = cmsLpopDirSync.getCpindex();
			String cpid =cmsLpopDirSync.getCpid();	
			ucpBasic.setCpindex(cpindex);
			ucpBasic.setStatus(1);
    		List <UcpBasic> ucpBasicList = new ArrayList <UcpBasic>();
    		ucpBasicList=ucpBasicDS.getUcpBasicByCond(ucpBasic);
			if(ucpBasicList != null&&ucpBasicList.size()!=0)
			{	
				ucpBasic = ucpBasicList.get(0);
				cpDirSync.setCpid(cpid);
				cpDirSync.setCpname(ucpBasic.getCpcnshortname());
				cpDirSync.setLptype(CmsLpopNoauditConstants.directToSync_type);
                if (check.checkCmsLpExist(cpDirSync)) 
                {
					ds.insertCmsLpopNoaudit(cpDirSync);
	                CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_NOAUDIT,
	                        CommonLogConstant.OPERTYPE_CP_PASSADD, CommonLogConstant.OPERTYPE_CP_PASSADD_INFO,
	                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
    	        	return "1:操作失败,该CP已经处于直通状态";
                }
			}
			else
			{
	        	return "1:操作失败,该CP处于暂停或已删除状态";				
			}    		
    	}
    	catch(Exception e)
    	{
            e.printStackTrace();
            throw e;    		
    	}
    	return result; 

    	
    }
    /**
	 * 直通设置 lfh 20121012
	 */
    public String saveDirsyncCp(List<UcpBasic> cmsLpopDirSyncList)throws DomainServiceException
    {
    	log.debug("saveLpopNoaudit starting....");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1; 
		try
		{
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";           
    		for(UcpBasic ucbcpSecNoaudit : cmsLpopDirSyncList)
    		{
                message = saveDirsyncCpByone(ucbcpSecNoaudit);
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    fail++;
                    resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), ucbcpSecNoaudit.getCpid(), 
                    		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                    returnInfo.appendOperateInfo(operateInfo);
                }
                num++;
    		}
            if (success == cmsLpopDirSyncList.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == cmsLpopDirSyncList.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
		}
        catch (Exception e)
        {
            log.error("CmsLpopNoauditLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("saveLpopNoaudit end");
        return returnInfo.toString();
    }
    
    
    /**
	 * 免一审设置 
	 */
    public String saveLpopNoauditByCond(List<CmsLpopNoaudit> cmsLpopNoauditList)
    {
        log.debug("saveLpopNoauditByCond starting....");
        String result = "070000";// 设置操作结果为真
        try
        {
//            // 先清除所有的免一审的CP
//            CmsLpopNoaudit cmsLpopNoaudit = new CmsLpopNoaudit();
//            cmsLpopNoaudit.setLptype(1);
//            List<CmsLpopNoaudit> list = ds.getCmsLpopNoauditByCond(cmsLpopNoaudit);
//            if (list != null && list.size() > 0) // 如果存在
//            {
//                ds.removeCmsLpopNoauditList(list);
//            }

            for (CmsLpopNoaudit cmslpopNoaudit : cmsLpopNoauditList)
            {

                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cmslpopNoaudit.getCpid());
                ucpBasic.setCptype(1);
                // 查询CP是否存在,若存在且状态正确则插入一条记录
                List<UcpBasic> ucpBasiclist = ucpBasicDS.getUcpBasicByCond(ucpBasic);
                if (ucpBasiclist != null && ucpBasiclist.size() > 0 && ucpBasiclist.get(0).getStatus() == 1)
                {
                    ds.insertCmsLpopNoaudit(cmslpopNoaudit);
                }
            }
        }
        catch (DomainServiceException e)
        {
            result = "070001";
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("saveLpopNoauditByCond end");
        return result;
    }
    

}
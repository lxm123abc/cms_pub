package com.zte.cms.cast.dao;

import java.util.List;

import com.zte.cms.cast.model.Castcdn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastcdnDAO
{
    /**
     * 新增Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DAOException dao异常
     */
    public void insertCastcdn(Castcdn castcdn) throws DAOException;

    /**
     * 根据主键更新Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DAOException dao异常
     */
    public void updateCastcdn(Castcdn castcdn) throws DAOException;

    /**
     * 根据条件更新Castcdn对象
     * 
     * @param castcdn Castcdn更新条件
     * @throws DAOException dao异常
     */
    public void updateCastcdnByCond(Castcdn castcdn) throws DAOException;

    /**
     * 根据主键删除Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DAOException dao异常
     */
    public void deleteCastcdn(Castcdn castcdn) throws DAOException;

    /**
     * 根据条件删除Castcdn对象
     * 
     * @param castcdn Castcdn删除条件
     * @throws DAOException dao异常
     */
    public void deleteCastcdnByCond(Castcdn castcdn) throws DAOException;

    /**
     * 根据主键查询Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @return 满足条件的Castcdn对象
     * @throws DAOException dao异常
     */
    public Castcdn getCastcdn(Castcdn castcdn) throws DAOException;
    
    public Castcdn getCastById(Castcdn castcdn) throws DAOException;

    /**
     * 根据条件查询Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @return 满足条件的Castcdn对象集
     * @throws DAOException dao异常
     */
    public List<Castcdn> getCastcdnByCond(Castcdn castcdn) throws DAOException;

    /**
     * 根据条件分页查询Castcdn对象，作为查询条件的参数
     * 
     * @param castcdn Castcdn对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Castcdn对象，作为查询条件的参数
     * 
     * @param castcdn Castcdn对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
}
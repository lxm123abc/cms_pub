package com.zte.cms.content.relation.dao;

import java.util.ArrayList;
import java.util.List;

import com.zte.cms.content.relation.dao.ICmsContentRelationDAO;
import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsContentRelationDAO extends DynamicObjectBaseDao implements ICmsContentRelationDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException
    {
        log.debug("insert cmsContentRelation starting...");
        super.insert("insertCmsContentRelation", cmsContentRelation);
        log.debug("insert cmsContentRelation end");
    }

    public void updateCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException
    {
        log.debug("update cmsContentRelation by pk starting...");
        super.update("updateCmsContentRelation", cmsContentRelation);
        log.debug("update cmsContentRelation by pk end");
    }

    public void deleteCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException
    {
        log.debug("delete cmsContentRelation by pk starting...");
        super.delete("deleteCmsContentRelation", cmsContentRelation);
        log.debug("delete cmsContentRelation by pk end");
    }

    public CmsContentRelation getCmsContentRelation(CmsContentRelation cmsContentRelation) throws DAOException
    {
        log.debug("query cmsContentRelation starting...");
        CmsContentRelation resultObj = (CmsContentRelation) super.queryForObject("getCmsContentRelation",
                cmsContentRelation);
        log.debug("query cmsContentRelation end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsContentRelation> getCmsContentRelationByCond(CmsContentRelation cmsContentRelation)
            throws DAOException
    {
        log.debug("query cmsContentRelation by condition starting...");
        List<CmsContentRelation> rList = (List<CmsContentRelation>) super.queryForList(
                "queryCmsContentRelationListByCond", cmsContentRelation);
        log.debug("query cmsContentRelation by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsContentRelation by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsContentRelationListCntByCond", cmsContentRelation))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsContentRelation> rsList = (List<CmsContentRelation>) super.pageQuery(
                    "queryCmsContentRelationListByCond", cmsContentRelation, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsContentRelation by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCmsContentRelationListByCond", "queryCmsContentRelationListCntByCond",
                cmsContentRelation, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery2(CmsContentRelation cmsContentRelation, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsContentRelation by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCgidCntByCond", cmsContentRelation)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<String> rsList = (List<String>) super.pageQuery("queryCgidByCond", cmsContentRelation, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsContentRelation by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize) throws DAOException
    {
        log.debug("page query UnSelected Content by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryUnSelectedContentListCntByCond", info)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsContentRelationInfo> rsList = (List<CmsContentRelationInfo>) super.pageQuery(
                    "queryUnSelectedContentListByCond", info, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query UnSelected Content by condition end");
        return pageInfo;
    }

    public List<CmsContentRelation> querySelectedContentList(CmsContentRelation cmsContentRelation) throws DAOException
    {
        log.debug("query querySelectedContentList by condition starting...");
        List<CmsContentRelation> rsList = (List<CmsContentRelation>) super.queryForList(
                "querySelectedContentListByCond", cmsContentRelation);
        log.debug("query querySelectedContentList by condition starting...");
        return rsList;
    }

    public PageInfo pageInfoQuery(String sql, String sqlcount, int start, int pageSize) throws DAOException
    {
        PageInfo pageInfo = null;
        CmsContentRelationInfo info = new CmsContentRelationInfo();
        info.setSql(sql);
        info.setSqlcount(sqlcount);
        int totalCnt = ((Integer) super.queryForObject("queryTempSelectedContentListCntByCond", info)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsContentRelationInfo> rsList = (List<CmsContentRelationInfo>) super.pageQuery(
                    "queryTempSelectedContentListByCond", info, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);

        }
        else
        {
            pageInfo = new PageInfo();
        }
        // log.debug("page query ucmContent by condition end");
        return pageInfo;
    }

    public List<CmsContentRelationInfo> pageInfoQueryList(String sql, String sqlcount) throws DAOException
    {
        log.debug("query pageInfoQueryList by condition starting...");
        List<CmsContentRelationInfo> rsList = null;
        CmsContentRelationInfo info = new CmsContentRelationInfo();
        info.setSql(sql);
        info.setSqlcount(sqlcount);
        int totalCnt = ((Integer) super.queryForObject("queryTempSelectedContentListCntByCond", info)).intValue();
        if (totalCnt > 0)
        {
            rsList = (List<CmsContentRelationInfo>) super.queryForList("queryTempSelectedContentListByCond", info);
        }
        else
        {
            rsList = new ArrayList<CmsContentRelationInfo>();
        }
        log.debug("query pageInfoQueryList by condition end...");
        return rsList;
    }
}
package com.zte.cms.picture.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Picture extends DynamicBaseObject
{
    private java.lang.Long pictureindex;
    private java.lang.String pictureid;
    private java.lang.String pictureurl;
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String modtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private java.lang.String picturecode;
    private java.lang.Long objindex;
    private java.lang.Integer maptype;

    public java.lang.Integer getMaptype()
    {
        return maptype;
    }

    public void setMaptype(java.lang.Integer maptype)
    {
        this.maptype = maptype;
    }

    public java.lang.Long getObjindex()
    {
        return objindex;
    }

    public void setObjindex(java.lang.Long objindex)
    {
        this.objindex = objindex;
    }

    public java.lang.Long getPictureindex()
    {
        return pictureindex;
    }

    public void setPictureindex(java.lang.Long pictureindex)
    {
        this.pictureindex = pictureindex;
    }

    public java.lang.String getPictureid()
    {
        return pictureid;
    }

    public void setPictureid(java.lang.String pictureid)
    {
        this.pictureid = pictureid;
    }

    public java.lang.String getPictureurl()
    {
        return pictureurl;
    }

    public void setPictureurl(java.lang.String pictureurl)
    {
        this.pictureurl = pictureurl;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getModtime()
    {
        return modtime;
    }

    public void setModtime(java.lang.String modtime)
    {
        this.modtime = modtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public java.lang.String getPicturecode()
    {
        return picturecode;
    }

    public void setPicturecode(java.lang.String picturecode)
    {
        this.picturecode = picturecode;
    }

    public void initRelation()
    {
        this.addRelation("pictureindex", "PICTUREINDEX");
        this.addRelation("pictureid", "PICTUREID");
        this.addRelation("pictureurl", "PICTUREURL");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("modtime", "MODTIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
        this.addRelation("picturecode", "PICTURECODE");
        this.addRelation("objindex", "OBJINDEX");
    }
}

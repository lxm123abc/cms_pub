package com.zte.cms.egptemplt.dao;

import java.util.List;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IEpgtempltDAO
{
    /**
     * 新增Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DAOException dao异常
     */
    public void insertEpgtemplt(Epgtemplt epgtemplt) throws DAOException;

    /**
     * 根据主键更新Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DAOException dao异常
     */
    public void updateEpgtemplt(Epgtemplt epgtemplt) throws DAOException;

    /**
     * 根据主键删除Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @throws DAOException dao异常
     */
    public void deleteEpgtemplt(Epgtemplt epgtemplt) throws DAOException;

    /**
     * 根据主键查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @return 满足条件的Epgtemplt对象
     * @throws DAOException dao异常
     */
    public Epgtemplt getEpgtemplt(Epgtemplt epgtemplt) throws DAOException;

    /**
     * 根据条件查询Epgtemplt对象
     * 
     * @param epgtemplt Epgtemplt对象
     * @return 满足条件的Epgtemplt对象集
     * @throws DAOException dao异常
     */
    public List<Epgtemplt> getEpgtempltByCond(Epgtemplt epgtemplt) throws DAOException;

    /**
     * 根据条件分页查询Epgtemplt对象，作为查询条件的参数
     * 
     * @param epgtemplt Epgtemplt对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Epgtemplt对象，作为查询条件的参数
     * 
     * @param epgtemplt Epgtemplt对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
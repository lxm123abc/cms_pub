package com.zte.cms.cntSyncTask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CntSyncTask extends DynamicBaseObject implements Cloneable
{
    private java.lang.Long taskindex;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer status;
    private java.lang.Integer priority;
    private java.lang.Integer sequence;
    private java.lang.Integer retrytimes;
    private java.lang.String description;
    private java.lang.Integer resultcode;
    private java.lang.String resultdesc;
    private java.lang.String resultfileurl;
    private java.lang.String correlateid;
    private java.lang.String contentmngxmlurl;
    private java.lang.Integer source;
    private java.lang.Integer targettype;
    private java.lang.Integer platform;
    private java.lang.Long destindex;
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    private java.lang.String endtime1;
    private java.lang.String endtime2;
    
    private java.lang.String targetname;
    private java.lang.Long wg_taskindex;
    private java.lang.Long batchid;

    public Object clone(){
       try
       {
           return super.clone();
       }
       catch (CloneNotSupportedException e)
       {
           e.printStackTrace();
           return null;
       }
       
        
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.Integer priority)
    {
        this.priority = priority;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getRetrytimes()
    {
        return retrytimes;
    }

    public void setRetrytimes(java.lang.Integer retrytimes)
    {
        this.retrytimes = retrytimes;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getResultcode()
    {
        return resultcode;
    }

    public void setResultcode(java.lang.Integer resultcode)
    {
        this.resultcode = resultcode;
    }

    public java.lang.String getResultdesc()
    {
        return resultdesc;
    }

    public void setResultdesc(java.lang.String resultdesc)
    {
        this.resultdesc = resultdesc;
    }

    public java.lang.String getResultfileurl()
    {
        return resultfileurl;
    }

    public void setResultfileurl(java.lang.String resultfileurl)
    {
        this.resultfileurl = resultfileurl;
    }


    public java.lang.String getCorrelateid()
    {
        return correlateid;
    }

    public void setCorrelateid(java.lang.String correlateid)
    {
        this.correlateid = correlateid;
    }

    public java.lang.String getContentmngxmlurl()
    {
        return contentmngxmlurl;
    }

    public void setContentmngxmlurl(java.lang.String contentmngxmlurl)
    {
        this.contentmngxmlurl = contentmngxmlurl;
    }



    public java.lang.Long getDestindex()
    {
        return destindex;
    }

    public void setDestindex(java.lang.Long destindex)
    {
        this.destindex = destindex;
    }

	public java.lang.String getStarttime1()
	{
		return starttime1;
	}

	public void setStarttime1(java.lang.String starttime1)
	{
		this.starttime1 = starttime1;
	}

	public java.lang.String getStarttime2()
	{
		return starttime2;
	}

	public void setStarttime2(java.lang.String starttime2)
	{
		this.starttime2 = starttime2;
	}

	public java.lang.String getEndtime1()
	{
		return endtime1;
	}

	public void setEndtime1(java.lang.String endtime1)
	{
		this.endtime1 = endtime1;
	}

	public java.lang.String getEndtime2()
	{
		return endtime2;
	}

	public void setEndtime2(java.lang.String endtime2)
	{
		this.endtime2 = endtime2;
	}
    public void initRelation()
    {
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("status", "STATUS");
        this.addRelation("priority", "PRIORITY");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("retrytimes", "RETRYTIMES");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("resultcode", "RESULTCODE");
        this.addRelation("resultdesc", "RESULTDESC");
        this.addRelation("resultfileurl", "RESULTFILEURL");
        this.addRelation("correlateid", "CORRELATEID");
        this.addRelation("contentmngxmlurl", "CONTENTMNGXMLURL");
        this.addRelation("source", "SOURCE");
        this.addRelation("destindex", "DESTINDEX");
        this.addRelation("targettype", "TARGETTYPE");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("starttime1", "STARTTIME1");
        this.addRelation("starttime2", "STARTTIME2");
        this.addRelation("endtime1", "ENDTIME1");
        this.addRelation("endtime2", "ENDTIME2");
        
        this.addRelation("targetname", "TARGETNAME");
    }

    public java.lang.Integer getSource()
    {
        return source;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public void setSource(java.lang.Integer source)
    {
        this.source = source;
    }

    public java.lang.Integer getTargettype()
    {
        return targettype;
    }

    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }

    public java.lang.Long getWg_taskindex()
    {
        return wg_taskindex;
    }

    public void setWg_taskindex(java.lang.Long wg_taskindex)
    {
        this.wg_taskindex = wg_taskindex;
    }

    public java.lang.Long getBatchid()
    {
        return batchid;
    }

    public void setBatchid(java.lang.Long batchid)
    {
        this.batchid = batchid;
    }
    
}

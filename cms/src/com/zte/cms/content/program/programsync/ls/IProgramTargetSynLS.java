package com.zte.cms.content.program.programsync.ls;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface IProgramTargetSynLS
{
    /**
     * 根据条件分页查询CntTargetsysSyn对象
     * @param ProgramTargetSyn cntTargetsysSyn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize) throws Exception;
  
    /**
     * 根据条件分页查询CntTargetsysSyn对象
     * @param ProgramTargetSyn cntTargetsysSyn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize) throws Exception;
}

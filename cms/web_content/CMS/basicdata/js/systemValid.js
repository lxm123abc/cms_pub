//特殊字符过滤
function filtrate(object){
    object.value = object.value.replace(/[@:]/g,'');
}

function checkNum(sCheck){

    var regexp= /^\d+$/; 

    var reg = sCheck.match(regexp); 

    if(reg==null){

        return false;

    }

    return true;

}


function checkNumORNull(sCheck){

    if (Trim(sCheck) == "") {

        return true;

    }

    return checkNum(sCheck);

}

function checkNull(inputStr) {

    return ( Trim(inputStr).length==0);

}

function checkIP(sIPAddress) {

    var exp=/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
 
    var reg = sIPAddress.match(exp);

    if(reg==null){

        return false;

    }

    return true;
}

function checkPort(port)
{
  var newPar=/^\d+$/;   
 return newPar.test(port);

}


function checkUrl(inputStr) {

    var regUrl=/^(http:[/][/]|www.)([a-z]|[A-Z]|[0-9]|[/.]|[~]|[:])*$/;

    return inputStr.match(regUrl); 

}

function strTrim(str){

	str = str + " ";

    var first = 0;

    var last = str.length - 1;

    while (str.charAt(first) == " ") { first++; }

    while (str.charAt(last) == " ") { last--; }

	if (first > last) return "";

    return str.substring(first, last + 1);

}

function is_empty(str) {

	if (str == null || str == "") return true;
    if (strTrim(str) == "") return true;
	return false;

}



  function showSearches(url, val0, val1,val2,val3,val4,val5){
    if (val0 == null) val0 = '';
    if (val1 == null) val1 = '';
	if (val2 == null) var val2 = '';
	if (val3 == null) var val3 = '';
	if (val4 == null) var val4 = '';
	if (val5 == null) var val5 = '';
	var obj = new Object();
	obj.url = url;          
	obj.val0 = val0;
	obj.val1 = val1;
	obj.val2 = val2;
	obj.val3 = val3;
	obj.val4 = val4;
	obj.val5 = val5;
    var retObj;
    var sPopFeatures2 = "dialogWidth:640px;dialogHeight:480px;scrolling:yes;toolbar:no;location:no;directories:no;status:no;menubar:no;scrollbars:yes;resizable:yes";

    retObj = showModalDialog(url,obj,sPopFeatures2);
	if (retObj == null || typeof(retObj) != "object") return null;
	return retObj;

  }
  
  
     function strlength(str){
        var l=str.length;
        var n=l;
        for(var i=0;i<l;i++){
    		    if(str.charCodeAt(i)<0 || str.charCodeAt(i)>255){
    		    	n+=2;
    		    }
    	  }
        return n;
    }
    
 //验证输入框中字数是否超过最大长度
function IsMaxLength(input,input_msg,len){

  if(utf8_strlen2(trim(input.value)) > len){
      document.getElementById(input_msg).innerText = $res_entry("input.system.maxlength.suggest.13")+len+$res_entry("input.system.maxlength.suggest.14");//"不能超过"+len+ "个字符";
      document.getElementById(input.id).focus();
      return false;
  }else{
       document.getElementById(input_msg).innerText = "";   
       return true;   
  }
}   


function hasChineseChars(sCheck){
  for (var nIndex=0; nIndex<sCheck.length; nIndex++)
  {
     cCheck = sCheck.charCodeAt(nIndex);
        if (cCheck > 255)
           return true;
  }
  return false;
}

function dynChgSelectWidth(id)
{
	
	var selectObj = document.getElementById(id);
	if(selectObj != undefined && selectObj != null)
	{
		var v  = selectObj.options[selectObj.selectedIndex].text;
		var width = getCharWidth(v);
		width = (width<130 ? 130 : width);
		selectObj.style.width=width;
	}
}

function getCharWidth(str) 
{ 
	var width = 0; 
	var type2 = "\'";
	var type3 = "fijltI! |[]\\:;,./";
	var type4 = "r`()-{}\"";
	var type5 = "vxyz^*";
	var type6 = "ckJ";
	var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
	var type8 = "BEKPS&";
	var type9 = "wCDGHMNOQRU";
	var type11 = "mW%";
	var type12 = "@";
	for( i=0; i<str.length; i++) 
	{ 
		var value = str.charCodeAt(i); 
		if( value < 0x080) 
		{ 
			if(type2.indexOf(str.charAt(i)) != -1)
			{
				width += 2; 
			}
			else if(type3.indexOf(str.charAt(i)) != -1)
			{
				width += 3; 
			}
			else if(type4.indexOf(str.charAt(i)) != -1)
			{
				width += 4; 
			}
			else if(type5.indexOf(str.charAt(i)) != -1)
			{
				width += 5; 
			}
			else if(type6.indexOf(str.charAt(i)) != -1)
			{
				width += 6; 
			}
			else if(type7.indexOf(str.charAt(i)) != -1)
			{
				width += 7; 
			}
			else if(type8.indexOf(str.charAt(i)) != -1)
			{
				width += 8; 
			}
			else if(type9.indexOf(str.charAt(i)) != -1)
			{
				width += 9; 
			}
			else if(type11.indexOf(str.charAt(i)) != -1)
			{
				width += 11; 
			}
			else if(type12.indexOf(str.charAt(i)) != -1)
			{
				width += 12; 
			}
			else
			{
				width += 12; 
			}
		} 
		else 
		{ 
			width += 12; 
		}
	} 
	width += 28;
	return width; 
}	

  
 
package com.zte.cms.clean;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zte.cms.check.MyThreadLS;

public class MultiThreadServlet extends HttpServlet {

    public void init() throws ServletException {
        try {
            super.init();
            sysInit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        sysInit();
    }

    /**
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException exception
     * @throws IOException      exception
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        sysInit();
    }

    public void sysInit() throws ServletException, IOException {
        try {
            // 启动服务器监控线程和定时清除直播回放内容线程
            Thread mythread = null;
            // Thread cleanthread = null;

            mythread = new MyThreadLS();
            mythread.start();

            // cleanthread = new CleanThreadLS();
            // cleanthread.start();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}

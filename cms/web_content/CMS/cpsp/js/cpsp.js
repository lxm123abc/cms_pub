//循环显示联系人信息
function showContentType(contenTypeList)
{
	var tbody = document.getElementById('cpcontenttype');
	newTr=tbody.insertRow();
	var count=0;
	for(var i =0 ;i<contenTypeList.length ; i++)
	{   
		if(count%5==0){
			 newTr=tbody.insertRow();
		}
		createRow(newTr,contenTypeList[i]); //循环创建节目类型
		count++;
	}
}

//创建节目类型
function createRow(tr,contentType)
{
    var td=document.createElement('td');
    td.setAttribute("width","20%");
	td.innerHTML="<input type='checkbox' name='contenttype' value='"+contentType.typeid+"'  />"+contentType.typename;
    tr.appendChild(td);
}

var programTypeCPMap=new Array();
function checkedConType()
{
	var cpid=$("#cpid").val();
	var contenttypeList=document.getElementsByName("contenttype");
	for(var i=0;i<contenttypeList.length;i++){
		if(contenttypeList[i].checked==true){
			  var obj = new Object();
			  obj["cpid"]=cpid;
			  obj["typeid"]=contenttypeList[i].value;
			  programTypeCPMap.push(obj);
			 
		}
	}

	if(programTypeCPMap.length==0){
		document.getElementById("contentType_msg").innerText="请选择以上的节目类型";
		return false;
	}
}

function getTime(str){  
    if(str!=null){
    	if(str.length==4){
    		return str.substr(0,4);
    	}
    	if(str.length == 8){
	    	return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    }
	    
	    if (str.length == 10){ 
	        return  str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2)+ " " + str.substr(8,2);
	    }
	    
	    if(str.length ==14){
	       return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	    }else{
	        return "";
	    }  
    }else{
        return "";
    }
}
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<html>
<head>
<title><fmt:message key="allotrights.altitle"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   // ???��???����??���?����o�����騦����??������
   function viewOper (operID,operName,idForm) {
      var fm = document.inputForm;
      var tempID = fm.idid.value;
      //alert('idForm:'+idForm+"\n"+"tempId: "+tempID);
      document.getElementById(idForm).bgColor = '#FF66CC';
      if((tempID!=null) && (tempID!='') && (tempID!=idForm)) {
        //alert("tempid2: "+tempID);
        document.getElementById(tempID).bgColor = '';
      }

      parent.allotFrame.document.view.operID.value = operID;
      parent.bmFrame.document.view.operID.value = operID;
      parent.allotFrame.document.view.operName.value = operName;
      fm.idid.value = idForm;
      parent.bmFrame.document.view.memberRight.disabled = false;
      parent.allotFrame.document.view.submit();
   }

   function toPage (page) {
      document.inputForm.page.value = page;

      parent.allotFrame.document.view.operID.value = "";
      parent.allotFrame.document.view.operName.value = "";
      parent.allotFrame.document.view.submit();
      document.inputForm.idid.value = '';
      parent.bmFrame.document.view.memberRight.disabled = true;
      document.inputForm.submit();
   }

   function goPage(){
      var fm = document.inputForm;
      var pages = parseInt(fm.pages.value);
      var thepage =parseInt(trim(fm.gopage.value));
      if(thepage==''){
         alert("<fmt:message key="operListRights.pagerequried"/>")
         fm.gopage.focus();
         return;
      }
      if(!checkstring('0123456789',thepage)){
         alert("<fmt:message key="operListRights.pageformatillegal"/>")
         fm.gopage.focus();
         return;
      }
      if(thepage<=0 || thepage>pages ){
         alert("<fmt:message key="operListRights.pagerangeillegal"/>")
         fm.gopage.focus();
         return;
      }
      thepage = thepage -1;
      toPage(thepage);
   }

   function searchRing() {
     var fm = document.inputForm;
     fm.op.value = 'search';
     fm.page.value = 0;

      parent.allotFrame.document.view.operID.value = "";
      parent.allotFrame.document.view.operName.value = "";
      parent.allotFrame.document.view.submit();
      document.inputForm.idid.value = '';

     document.inputForm.submit();
   }
</script>
</head>

<body class="body-style1">
<%
    try {
        String operID = (String)session.getAttribute("OPERID");
        int thepage = request.getParameter("page") == null ? 0 : Integer.parseInt((String)request.getParameter("page"));
        String operflag = request.getParameter("op") == null ? "" : (String)request.getParameter("op");
        String name = request.getParameter("opername") == null ? "" : (String)request.getParameter("opername");
        String serflag = request.getParameter("serflag") == null ? "0" : (String)request.getParameter("serflag");
        ArrayList list = new ArrayList();
        HashMap map = new HashMap();
        int records = 30;//??��3??��?��?????��y
        if (operID != null)
            if("search".equals(operflag)) {
             list = purview.sortChildOperators2(operID,name,serflag,"0");
           } else {
            list = purview.sortChildOperators(operID,"0");
           }
        if (list == null)
            list = new ArrayList();

        int pages = list.size()/records;
        if(list.size()%records>0)
            pages = pages + 1;

        String idstr = "";
%>
<form name="inputForm" action="operListRights.jsp" method="POST">
<input type="hidden" name="page" value="<%= thepage %>">
<input type="hidden" name="pages" value="<%= pages %>">
<input type="hidden" name="op" value="<%= operflag %>">
<input type="hidden" name="idid" value="">

<table  border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style2" height=hei>
  <tr align="center">
   <fmt:message key="operDel.opername"/><br>
      <input type="text" name="opername" value="<%= name %>" maxlength="30" size="15" class="input-style7" ><br>
   <fmt:message key="operInfo.operatortype"/><br>
      <select name="serflag">
        <option value="0" selected="selected"><fmt:message key="operListRights.systemmanager"/></option>
         <%if(purview.showSpCategory()){%>
        <option value="1" selected="selected"><fmt:message key="operListRights.cpspmanager"/></option>
         <%} if(purview.showGrpCategory()){%>
        <option value="3" selected="selected"><fmt:message key="operInfo.grpmanager"/></option>
        <%}%>
        </select>
      <img src="button/search.gif" alt="<fmt:message key="operListRights.queryoperator"/>" onmouseover="this.style.cursor='hand'" onclick="javascript:searchRing()">
  </tr><br>
<script language="javascript">
var serflagValue = '<%=serflag%>';
document.inputForm.serflag.value = serflagValue;
</script>
  <tr class="table-title1">
    <td width="90" height="24"><fmt:message key="operInfo.loginname"/></td>
    <td width="100" height="24"><fmt:message key="operDel.operfullname"/></td>
  </tr>
<script language="JavaScript">
	if(parent.parent.frames.length>0){
		var hei;
<%
	if(list!=null && list.size()>25){
%>
	hei = 20*50;
<%
	}else{
%>
	hei=500;
<%
}
%>
		//parent.parent.document.all.main.style.height=hei;
		}
</script>
<%
        for (int i = thepage * records; i < thepage * records +records && i < list.size(); i++) {
            map = (HashMap)list.get(i);
            idstr = "table_color"+i;
%>
  <tr id="<%=idstr%>" bgcolor="" <%= operID.equals((String)map.get("OPERID")) ? "" : "ondblclick=\"javascript:viewOper('" + (String)map.get("OPERID") + "','" + (String)map.get("OPERNAME") + "','"+idstr+"')\"" %>>
    <td height="22" ><%= (String)map.get("OPERNAME") %>&nbsp;</td>
    <td height="22" ><%= (String)map.get("OPERALLNAME") %>&nbsp;</td>
  </tr>
<%
        }
%>
<%
        if (list.size() > records) {
%>
  <tr>
      <table border="0" cellspacing="1" cellpadding="1" align="center" class="table-style2">
        <tr>
          <fmt:message key="operLog.total"/>&nbsp;<%= list.size() %>&nbsp;<fmt:message key="operLog.record"/>&nbsp;<%= list.size()%records==0?list.size()/records:list.size()/records+1 %>&nbsp;<fmt:message key="operLog.page"/>&nbsp;&nbsp;<fmt:message key="operLog.nowpage"/>&nbsp;<%= thepage + 1 %>&nbsp;<fmt:message key="operLog.page"/>
        </tr>
        <tr>
          <td><img src="button/firstpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:toPage(0)"></td>
          <td><img src="button/prepage.gif"<%= thepage == 0 ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(" + (thepage - 1) + ")\"" %>></td>
          <td><img src="button/nextpage.gif"<%= thepage * records + records >= list.size() ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(" + (thepage + 1) + ")\"" %>></td>
          <td><img src="button/endpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:toPage(<%= (list.size() - 1) / records %>)"></td>
        </tr>
        <tr>
          <td colspan="5" align="right" >
            <table border="0" cellspacing="1" cellpadding="1" align="right" class="table-style2">
            <tr>
             <td ><fmt:message key="operListRights.pagenumber"/>&nbsp;</td>
             <td> <input style=text value="<%= String.valueOf(thepage+1) %>" name="gopage" maxlength=5 class="input-style4" > </td>
             <td ><img src="button/go.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:goPage()"  ></td>
            </tr>
            </table>
          </td>
       </tr>
      </table>
  </tr>

<%
        }
%>
</table>
</form>
<%
    }
    catch (Exception e) {
%>
<table border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td height="24" colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
<%
    }
%>
</table>
</form>
</body>
</html>

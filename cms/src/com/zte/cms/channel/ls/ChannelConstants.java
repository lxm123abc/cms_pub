package com.zte.cms.channel.ls;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;

public class ChannelConstants
{
    // 资源文件
    // 文件名称
    public static final String RESOURCE_UCDN_CHANNEL = "ucdn_channel_resource";
    // 要删除的不存在或者已经被删除
    public static final String RESOURCE_UCDN_CHANNEL_4DEL_DOESNOT_EXIST = "ucdn.channel.4del.doesnot.exist";
    // 要修改的不存在或者已经被删除
    public static final String RESOURCE_UCDN_CHANNEL_4UPDATE_DOESNOT_EXIST = "ucdn.channel.4update.doesnot.exist";
    // 新增服成功
    public static final String RESOURCE_UCDN_CHANNEL_ADD_SUCCESSFUL = "ucdn.channel.channel.add.successful";
    // 删除成功
    public static final String RESOURCE_UCDN_CHANNEL_DEL_SUCCESSFUL = "ucdn.channel.channel.del.successful";
    // 修改成功
    public static final String RESOURCE_UCDN_CHANNEL_UPDATE_SUCCESSFUL = "ucdn.channel.channel.update.successful";
    // 已经有相同的名称存在
    public static final String RESOURCE_UCDN_CHANNEL_NAME_EXISTS = "ucdn.channel.channel.name.exists";
    // 已经有相同的ID存在
    public static final String RESOURCE_UCDN_CHANNEL_ID_EXISTS = "ucdn.channel.channel.id.exists";
    // 不存在

    public static final String RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS = "ucdn.channel.wkfw.no.exists";
    // 状态不正确
    public static final String RESOURCE_UCDN_CHANNEL_STATUS_WRONG = "ucdn.channel.status.wrong";

    public static final String RESOURCE_UCDN_PHCHANNEL_STATUS_WRONG = "ucdn.phchannel.status.wrong";

    // 已经在审核中
    public static final String RESOURCE_UCDN_CHANNEL_WKFW_IN_ALREADY = "ucdn.channel.wkfw.in.already";
    // 工作流状态不正确
    public static final String RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG = "ucdn.channel.wkfw.status.wrong";
    // 校验成功，可以操作
    public static final String RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK = "ucdn.channel.wkfw.check.ok";
    // 不在工作流中或者所处工作流不正确
    public static final String RESOURCE_UCDN_CHANNEL_NOT_IN_WKFW_OR_WRONG_WKFW = "ucdn.channel.not.in.wkfw.or.wrong.wkfw";

    // 新增审核提交成功
    public static final String RES_CMS_CHANNEL_WKFW_ADD_APPLY_SUCCESS = "cms.channel.msg.wkfw.add.apply.success";

    // 新增审核成功
    public static final String RES_CMS_CHANNEL_WKFW_ADD_AUDIT_SUCCESS = "cms.channel.msg.wkfw.add.audit.success";
    // 修改审核提交成功
    public static final String RES_CMS_CHANNEL_WKFW_UPDATE_APPLY_SUCCESS = "cms.channel.msg.wkfw.update.apply.success";
    public static final String RES_CMS_CHANNEL_WKFW_UPDATE_AUDIT_SUCCESS = "cms.channel.msg.wkfw.update.audit.success";

    /* 工作流类型相关 */
    public static final String RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD = "cms.channel.workflowtype.name.channel.add";// 新增
    public static final String RES_CMS_STRA_WKFWTYPENAME_CHANNEL_UPDATE = "cms.channel.workflowtype.name.channel.update";// 修改
    // 操作成功
    public static final String RES_CMS_CHANNEL_OPER_SUCCESS = "res.ucdn.channel.oper.success";
    // 操作失败
    public static final String RES_CMS_CHANNEL_OPER_FAIL = "res.ucdn.channel.oper.fail";
    // 状态不正确，不能发布
    public static final String RESOURCE_UCDN_CHANNEL_PUB_WRONG_STATUS = "res.ucdn.channel.pub.wrong.status";
    // 没有被选中
    public static final String RESOURCE_UCDN_CHANNEL_PUB_NO_CHANNEL_CHOSEN = "res.ucdn.channel.pub.no.channel.chosen";
    // BMS目标系统不存在
    public static final String RESOURCE_UCDN_CHANNEL_PUB_NO_BMS_SYSTEM = "res.ucdn.channel.pub.no.bms.system";
    // EPG目标系统不存在
    public static final String RESOURCE_UCDN_CHANNEL_PUB_NO_EPG_SYSTEM = "res.ucdn.channel.pub.no.epg.system";
    // 新增发布成功
    public static final String RESOURCE_UCDN_CHANNEL_PUBLISH_SUCCESS = "res.ucdn.channel.pub.successful";
    // 取消发布成功
    public static final String RESOURCE_UCDN_CHANNEL_PUBLISH_DEL_SUCCESS = "res.ucdn.channel.pub.del.successful";
    // 获取临时区地址失败
    public static final String RES_UCDN_PUB_GET_TEMP_AREA_FAILED = "res.ucdn.pub.get.temp.are.failed";

    // 获取同步XML文件FTP地址失败
    public static final String RES_UCDN_PUB_GET_FTP_ADDR_FAILED_FOR_XML_FILE = "res.ucdn.pub.get.ftp.addr.failed.for.xml.file";
    // 失败数量
    public static final String RES_UCDN_CHANNEL_FAILED_NUM = "res.ucdn.channel.failed.num";
    // 成功数量
    public static final String RES_UCDN_CHANNEL_SUCC_NUM = "res.ucdn.channel.succ.num";
    // 请选择
    public static final String RES_CMS_CHANNEL_OPTION_PLEASESELECT = "res.ucdn.channel.option.pleaseselect";



    public static final int WKFWTYPECODE_CHANNEL_ADD = 1;// 新增
    public static final int WKFWTYPECODE_CHANNEL_UPDATE = 2;// 修改

    public static final String WKFWTYPE_CHANNEL_ALL = "CMS_CHANNEL_%";
    public static final String WKFWTYPE_CHANNEL_ADD = "CMS_CHANNEL_A";// 新增
    public static final String WKFWTYPE_CHANNEL_UPDATE = "CMS_CHANNEL_U";// 修改
    //schedule审核
    public static final String WKFWTYPE_SCHEDULE_ALL = "CMS_SCHEDULE_%";
    public static final String WKFWTYPE_SCHEDULE_ADD = "CMS_SCHEDULE_A";// 新增
    public static final String WKFWTYPE_SCHEDULE_UPDATE = "CMS_SCHEDULE_U";// 修改
    // 工作流常量
    public static final int WKFW_STATUS_AUDITING = 120;
    
    public static final String ucdn_schedule_affectothers_pubing = "ucdn.schedule.affectothers.pubing";
    public static final String ucdn_schedule_affectothers_auditing = "ucdn.schedule.affectothers.auditing";
    public static final String ucdn_schedule_affectothers_pubsuccess = "ucdn.schedule.affectothers.pubsuccess";
    public static final String ucdn_schedule_updateapplysuccess = "ucdn.schedule.updateapplysuccess";
    public static final String ucdn_schedule_cpmissing = "ucdn.schedule.cpmissing";
    public static final String ucdn_schedulerecord_updateapplydeployfail = "ucdn.schedulerecord.updateapplydeployfail";
    public static final String ucdn_schedulerecord_updateapplyprocessingfail = "ucdn.schedulerecord.updateapplyprocessingfail";
    public static final String ucdn_schedulerecord_updateapplyprocessingundeployfail = "ucdn.schedulerecord.updateapplyprocessingundeployfail";
    public static final String ucdn_schedulerecordno_updateapplydeployfail = "ucdn.schedulerecordno.updateapplydeployfail";
    public static final String ucdn_schedulerecordundepoy_updateapplydeployfail = "ucdn.schedulerecordundepoy.updateapplydeployfail";

    
    public static String getWorkflowType(int workflowTypeCode)
    {
        switch (workflowTypeCode)
        {
            case WKFWTYPECODE_CHANNEL_ADD:
                return WKFWTYPE_CHANNEL_ADD;
            case WKFWTYPECODE_CHANNEL_UPDATE:
                return WKFWTYPE_CHANNEL_UPDATE;
            default:
                return null;
        }
    }

    public static String getWorkflowTypeName(int workflowTypeCode)
    {
        ResourceMgt.addDefaultResourceBundle(RESOURCE_UCDN_CHANNEL);
        switch (workflowTypeCode)
        {
            case WKFWTYPECODE_CHANNEL_ADD:
                return ResourceMgt.findDefaultText(RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD);
            default:
                return null;
        }
    }

    public static String getWorkflowTypeName(String workflowType)
    {
        ResourceMgt.addDefaultResourceBundle(RESOURCE_UCDN_CHANNEL);
        if (WKFWTYPE_CHANNEL_ADD.equals(workflowType))
        {
            return ResourceMgt.findDefaultText(RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD);
        }else{
        	return ResourceMgt.findDefaultText(RES_CMS_STRA_WKFWTYPENAME_CHANNEL_UPDATE);
        }
        
    }

    public static String getUserid()
    {
        UserInfo ui = LoginMgt.getUserInfo();
        return ui == null ? "" : ui.getUserId();
    }

    /**
     * 得到14的 字符串日期
     */
    public static String getCurrentTimeString()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        return sdf.format(date);
    }

}

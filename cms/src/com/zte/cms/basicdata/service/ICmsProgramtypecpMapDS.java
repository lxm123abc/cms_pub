package com.zte.cms.basicdata.service;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsProgramtypecpMapDS
{
    public void insertProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap);

    public void deleteProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap);

    /**
     * 查询CmsProgramtypecpMap对象
     * 
     * @param cmsProgramtypecpMap CmsProgramtypecpMap对象
     * @return CmsProgramtypecpMap对象
     * @throws DomainServiceException ds异常
     */
    public CmsProgramtypecpMap getCmsProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsProgramtypecpMap对象
     * 
     * @param cmsProgramtypecpMap CmsProgramtypecpMap对象
     * @return 满足条件的CmsProgramtypecpMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapByCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramtypecpMap对象
     * 
     * @param cmsProgramtypecpMap CmsProgramtypecpMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsProgramtypecpMap对象
     * 
     * @param cmsProgramtypecpMap CmsProgramtypecpMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public TableDataInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException;

    public TableDataInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DomainServiceException;

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap);

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DomainServiceException;
}
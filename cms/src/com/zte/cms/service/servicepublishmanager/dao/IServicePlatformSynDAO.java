package com.zte.cms.service.servicepublishmanager.dao;

import java.util.List;

import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServicePlatformSynDAO
{

     /**
	  * 根据主键查询CntTargetsysSyn对象
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象
	  * @throws DAOException dao异常
	  */
     public ServicePlatformSyn getServicePlatformSynByIndex( ServicePlatformSyn servicePlatformSyn )throws DAOException;
          
     /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
     public List<ServicePlatformSyn> getServicePlatformSynByCond( ServicePlatformSyn servicePlatformSyn )throws DAOException;

     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param ServicePlatformSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
  
     

     public String updateServicePlatformSyn(ServicePlatformSyn servicePlatformSyn)throws DAOException;
     

     public PageInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DAOException;
     public PageInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DAOException;
     public PageInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)throws DAOException;
    //根据parentid和platform来查找相同平台下父节点相同的mapping关系
     public List<ServicePlatformSyn> getSameParentPlatformSyn ( ServicePlatformSyn servicePlatformSyn )throws DAOException;


     
    

}

  package com.zte.cms.service.categoryservicemap.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;


public class CategoryServiceMap extends DynamicBaseObject
{
	private java.lang.Long mapindex;
	private java.lang.Long categoryindex;
	private java.lang.String categoryid;
	private java.lang.Long serviceindex;
	private java.lang.String serviceid;
	private java.lang.Integer sequence;
	private java.lang.Integer status;
	private java.lang.String createtime;
	private java.lang.String publishtime;
	private java.lang.String cancelpubtime;
    
	public void initRelation()
	{
	    this.addRelation("mapindex", "MAPINDEX");
	    this.addRelation("categoryindex", "CATEGORYINDEX");
	    this.addRelation("categoryid", "CATEGORYID");
	    this.addRelation("serviceindex", "SERVICEINDEX");
	    this.addRelation("serviceid", "SERVICEID");
	    this.addRelation("sequence", "SEQUENCE");
	    this.addRelation("status", "STATUS");
	    this.addRelation("createtime", "CREATETIME");
	    this.addRelation("publishtime", "PUBLISHTIME");
	    this.addRelation("cancelpubtime", "CANCELPUBTIME");
	}
	/**
	 * 
	 * @return java.lang.Long
	 */
    public java.lang.Long getMapindex()
    {
        return mapindex;
    } 
         
    /**
     * 
     * @param mapindex java.lang.Long
     */
    public void setMapindex(java.lang.Long mapindex) 
    {
        this.mapindex = mapindex;
    }
    
    /**
     * 
     * @return java.lang.Long
     */
    public java.lang.Long getCategoryindex()
    {
        return categoryindex;
    } 
         
    /**
     * 
     * @param categoryindex java.lang.Long
     */
    public void setCategoryindex(java.lang.Long categoryindex) 
    {
        this.categoryindex = categoryindex;
    }
    
    /**
     * 
     * @return java.lang.String
     */
    public java.lang.String getCategoryid()
    {
        return categoryid;
    } 
         
    /**
     * 
     * @param categoryid java.lang.String
     */
    public void setCategoryid(java.lang.String categoryid) 
    {
        this.categoryid = categoryid;
    }
    
    /**
     * 
     * @return java.lang.Long
     */
    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    } 
         
    /**
     * 
     * @param serviceindex java.lang.Long
     */
    public void setServiceindex(java.lang.Long serviceindex) 
    {
        this.serviceindex = serviceindex;
    }
    
    /**
     * 
     * @return java.lang.String
     */
    public java.lang.String getServiceid()
    {
        return serviceid;
    } 
         
    /**
     * 
     * @param serviceid java.lang.String
     */
    public void setServiceid(java.lang.String serviceid) 
    {
        this.serviceid = serviceid;
    }
    
    /**
     * 
     * @return java.lang.Integer
     */
    public java.lang.Integer getSequence()
    {
        return sequence;
    } 
         
    /**
     * 
     * @param sequence java.lang.Integer
     */
    public void setSequence(java.lang.Integer sequence) 
    {
        this.sequence = sequence;
    }
    
    /**
     * 
     * @return java.lang.Integer
     */
    public java.lang.Integer getStatus()
    {
        return status;
    } 
         
    /**
     * 
     * @param status java.lang.Integer
     */
    public void setStatus(java.lang.Integer status) 
    {
        this.status = status;
    }
    
    /**
     * 
     * @return java.lang.String
     */
    public java.lang.String getCreatetime()
    {
        return createtime;
    } 
         
    /**
     * 
     * @param createtime java.lang.String
     */
    public void setCreatetime(java.lang.String createtime) 
    {
        this.createtime = createtime;
    }
    
    /**
     * 
     * @return java.lang.String
     */
    public java.lang.String getPublishtime()
    {
        return publishtime;
    } 
         
    /**
     * 
     * @param publishtime java.lang.String
     */
    public void setPublishtime(java.lang.String publishtime) 
    {
        this.publishtime = publishtime;
    }
    
    /**
     * 
     * @return java.lang.String
     */
    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    } 
         
    /**
     * 
     * @param cancelpubtime java.lang.String
     */
    public void setCancelpubtime(java.lang.String cancelpubtime) 
    {
        this.cancelpubtime = cancelpubtime;
    }
    

 
}

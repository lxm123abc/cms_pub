
function newQueryDeclare(showclear, showcreate, pcheck, pname, pvalue, ptype) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr,myTh,myTd;
	myTr = queryBodyObj.insertRow(queryBodyObj.rows.length - 1);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "名称";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pname\" value='" + nvl(pname, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showclear ? 
		"<a href=\"#\" onclick=\"clearQueryProp(this.parentNode.parentNode.rowIndex)\"> 删除 </a>" : "");
	myTr.appendChild(myTd);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = "值";
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pvalue\" value='" + nvl(pvalue, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = (showcreate ? 
		"<a href=\"#\" onclick=\"hrefJsonStr(this,'&')\"> 生成值 </a>" : "");
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 3);
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
}

function buildQueryHTMLByDeclare(pclass, pdeclare, pexpress) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr,myTh,myTd;
	myTr = document.createElement("tr");
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = (pclass == "variable" ? "类型名称" : "调用名称");
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pdeclare\" value='" + nvl(pdeclare, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
	myTh = document.createElement("th");
	myTh.setAttribute("width", "10%");
	myTh.innerHTML = (pclass == "variable" ? "表达式" : "返回值");
	myTr.appendChild(myTh);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "20%");
	myTd.innerHTML = "<input type=\"text\" id=\"pexpress\" value='" + nvl(pexpress, true) + 
		"' style=\"vertical-align:middle\" />";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("width", "5%");
	myTd.innerHTML = "<a href=\"#\" onclick=\"hrefJsonStr(this,'&')\"> 生成值 </a>";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 3);
	myTd.innerHTML = "";
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildDeclareButton(pclass, isJe) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 9);
	myTd.setAttribute("align", "center");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 清 空 定 义 \"" + 
		" onclick=\"clearTableHTML()\" />";
	if (pclass == "variable") {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 创 建 属 性 \"" +
				" onclick=\"newQueryDeclare(true, true, false)\" />";
	}
	if (pclass == "invoke") {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 添 加 入 参 \"" +
				" onclick=\"newQueryDeclare(true, true, false)\" />";
	}
	if (!bol(isJe)) {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 保 存 定 义 \"" +
			" onclick=\"doJInsert('" + pclass + "')\" />";
	}
	else {
		myTd.innerHTML += "<input type=\"button\" class=\"inputButton\" value=\" 修 改 定 义 \"" + 
			" onclick=\"doJEditCallback('" + pclass + "')\" />";
	}
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildQueryHTMLByJOut(jout) {
	var queryBodyObj = document.getElementById("queryBody");
	var myTr = document.createElement("tr");
	var myTd = document.createElement("td");
	myTd.setAttribute("align", "right");
	myTd.setAttribute("width", "45px");
	myTd.innerHTML = "jout : ";
	myTr.appendChild(myTd);
	myTd = document.createElement("td");
	myTd.setAttribute("colSpan", 8);
	myTd.setAttribute("align", "left");
	myTd.innerHTML = jout;
	myTr.appendChild(myTd);
	queryBodyObj.appendChild(myTr);
}

function buildTableTHHTMLByDeclare() {
	var xtabTr = "<tr>";
	xtabTr += "<th width=\"45px\"><input type=\"checkbox\" id=\"tabchk\" onclick=\"parent.doSelectAll()\" /></th>";
	xtabTr += "<th width=\"45px\"></th>";
	xtabTr += "<th width=\"30px\">BP</th>";
	xtabTr += "<th>this.declare</th>";
	xtabTr += "<th>this.class</th>";
	xtabTr += "<th>this.param</th>";
	xtabTr += "<th>this.express</th>";
	xtabTr += "</tr>";
	return xtabTr;
}

function buildTableTRHTMLByDeclare(jsObj) {
	var xtabTr = "<tr>";
	xtabTr += "<td align=\"center\"><input type=\"checkbox\" id=\"trchk\"/></td>";
	xtabTr += "<td align=\"center\">";
	xtabTr += "<a href=\"#\" onclick=\"parent.doJMoveUp(this)\"><img src=\"images/arrow_up.gif\"></img></a>&nbsp;";
	xtabTr += "<a href=\"#\" onclick=\"parent.doJMoveDown(this)\"><img src=\"images/arrow_down.gif\"></img></a>";
	xtabTr += "</td>";
	xtabTr += "<td align=\"center\" ondblclick=\"parent.doJBreakPoint(this)\"><img src=\"images/blank.gif\"></img></td>";
	xtabTr += "<td><label id=\"lbldeclare\">" + createJsonStr(jsObj.pdeclare) + "</label></td>";
	xtabTr += "<td><label id=\"lblclass\">" + createJsonStr(jsObj.pclass) + "</label></td>";
	xtabTr += "<td><label id=\"lblparam\">" + createJsonStr(jsObj.pparam) + "</label></td>";
	xtabTr += "<td><label id=\"lblexpress\">" + createJsonStr(jsObj.pexpress) + "</label></td>";
	xtabTr += "</tr>";
	return xtabTr;
}

function buildTableHTMLByDeclare(jsObj) {
	clearTableHTML();
	var xtab = "<table border = 1>";
	xtab += buildTableTHHTMLByDeclare();
	for (var i=0; i<jsObj.length; i++) {
		xtab += buildTableTRHTMLByDeclare(jsObj[i]);
	}	
	xtab += "</table>";
	$("#xtable").contents().find("#xtable").html(xtab);
	buildTableButtonByDeclare();
}

function buildTableButtonByDeclare() {
	var tableBodyObj = document.getElementById("tableBody");
	var myTr = tableBodyObj.insertRow();
	var myTd = myTr.insertCell();
	myTd.setAttribute("align", "left");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 删 除 \" onclick=\"doJGC()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 修 改 \" onclick=\"doJEdit()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 复 制 \" onclick=\"doJCopy()\" />";
	myTd = myTr.insertCell();
	myTd.setAttribute("align", "right");
	myTd.setAttribute("class", "toolbar");
	myTd.innerHTML = "<input type=\"button\" class=\"inputButton\" value=\" 编 译 \" onclick=\"doJCompile()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 运 行 \" onclick=\"doJRun()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 调 试 \" onclick=\"doJDebug()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 终 止 \" onclick=\"doJStop()\" />" +
		"<input type=\"button\" class=\"inputButton\" value=\" 保 存 文 件 \" onclick=\"parent.frames['xtree'].saveFile('bpel')\" />";
}
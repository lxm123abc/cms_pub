package com.zte.cms.basicdata.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.cms.basicdata.service.ICmsProgramtypecpMapDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class CmsProgramtypecpMapLS implements ICmsProgramtypecpMapLS
{

    private ICmsProgramtypecpMapDS cmsProgramtypecpMapDS;
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());

    public TableDataInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
    {
        log.debug("queryCmsProgramtype start...");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        TableDataInfo dataInfo = null;
        try
        {
            cmsProgramtypecpMap.setOperid(operinfo.getOperid());
            dataInfo = cmsProgramtypecpMapDS.queryCPByTypid(cmsProgramtypecpMap, start, pageSize);
        }
        catch (Exception e)
        {
            log.equals(e);
        }
        return dataInfo;
    }

    public TableDataInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
    {
        log.debug("queryCmsProgramtype start...");
        TableDataInfo dataInfo = null;
        try
        {

            if (cmsProgramtypecpMap.getTypeid() != null && !cmsProgramtypecpMap.getTypeid().equals(""))
            {
                String typeid = EspecialCharMgt.conversion(cmsProgramtypecpMap.getTypeid());
                cmsProgramtypecpMap.setTypeid(typeid);
            }
            else if (cmsProgramtypecpMap.getTypename() != null && !cmsProgramtypecpMap.getTypename().equals(""))
            {
                String typename = EspecialCharMgt.conversion(cmsProgramtypecpMap.getTypename());
                cmsProgramtypecpMap.setTypename(typename);
            }

            dataInfo = cmsProgramtypecpMapDS.queryCmsProgramtypeByCpId(cmsProgramtypecpMap, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsProgramtype error");
        }
        log.debug("queryCmsProgramtype end");
        return dataInfo;
    }

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        log.debug("queryCPByTypidCount start...");
        Integer count = 0;
        count = cmsProgramtypecpMapDS.queryCPByTypidCount(cmsProgramtypecpMap);
        log.debug("queryCPByTypidCount end...");
        return count;
    }

    public ICmsProgramtypecpMapDS getCmsProgramtypecpMapDS()
    {
        return cmsProgramtypecpMapDS;
    }

    public void setCmsProgramtypecpMapDS(ICmsProgramtypecpMapDS cmsProgramtypecpMapDS)
    {
        this.cmsProgramtypecpMapDS = cmsProgramtypecpMapDS;
    }
}

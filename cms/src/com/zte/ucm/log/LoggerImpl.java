package com.zte.ucm.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;

public class LoggerImpl
  implements Logger, LoggerConfig
{
  private String sModel = "";

  private int iLevel = 8;
  private LoggerOutter loggerOutter;
  private String sClassName = "";

  public LoggerImpl()
  {
    this.sModel = "";
    this.sClassName = "";
  }

  public LoggerImpl(String paramString1, String paramString2)
  {
    this.sModel = paramString1;
    this.sClassName = paramString2;
  }

  public void debug(int paramInt)
  {
    if ((getLoggerLevel() & 0x1) != 0)
    {
      print(getLoggerOutter(), "DEBUG:", "" + paramInt);
    }
  }

  public void debug(String paramString)
  {
    if ((getLoggerLevel() & 0x1) != 0)
    {
      print(getLoggerOutter(), "DEBUG:", message(paramString));
    }
  }

  public String getExceptionDetail(Throwable paramThrowable)
  {
    StringWriter localStringWriter = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter(localStringWriter));

    return localStringWriter.toString();
  }

  public void debug(Exception paramException)
  {
    print(getLoggerOutter(), "DEBUG:", getExceptionDetail(paramException));

    String str = "";
    if (paramException != null)
    {
      str = paramException.toString();
    }
    else {
      str = "";
    }

    if ((getLoggerLevel() & 0x1) != 0)
    {
      print(getLoggerOutter(), "DEBUG:", message(str));
    }
  }

  public void debug(Object paramObject)
  {
    String str = "";
    if ((paramObject instanceof String))
    {
      str = (String)paramObject;
    } else if ((paramObject instanceof Throwable))
    {
      str = ((Throwable)paramObject).toString();
    }
    else if (paramObject != null)
    {
      str = paramObject.toString();
    }
    else {
      str = "";
    }

    if ((getLoggerLevel() & 0x1) != 0)
    {
      print(getLoggerOutter(), "DEBUG:", paramObject);
    }
  }

  public void info(int paramInt)
  {
    if ((getLoggerLevel() & 0x2) != 0)
    {
      print(getLoggerOutter(), "INFO:", paramInt);
    }
  }

  public void info(String paramString)
  {
    if ((getLoggerLevel() & 0x2) != 0)
    {
      print(getLoggerOutter(), "INFO:", paramString);
    }
  }

  public void info(Exception paramException)
  {
    print(getLoggerOutter(), "INFO:", getExceptionDetail(paramException));

    if ((getLoggerLevel() & 0x2) != 0)
    {
      print(getLoggerOutter(), "INFO:", paramException);
    }
  }

  public void info(Object paramObject)
  {
    if ((getLoggerLevel() & 0x2) != 0)
    {
      print(getLoggerOutter(), "INFO:", paramObject);
    }
  }

  public void warning(int paramInt)
  {
    if ((getLoggerLevel() & 0x4) != 0)
    {
      print(getLoggerOutter(), "WARNING:", paramInt);
    }
  }

  public void warning(String paramString)
  {
    if ((getLoggerLevel() & 0x4) != 0)
    {
      print(getLoggerOutter(), "WARNING:", paramString);
    }
  }

  public void warning(Exception paramException)
  {
    print(getLoggerOutter(), "WARNING:", getExceptionDetail(paramException));

    if ((getLoggerLevel() & 0x4) != 0)
    {
      print(getLoggerOutter(), "WARNING:", paramException);
    }
  }

  public void warning(Object paramObject)
  {
    if ((getLoggerLevel() & 0x4) != 0)
    {
      print(getLoggerOutter(), "WARNING:", paramObject);
    }
  }

  public void error(int paramInt)
  {
    if ((getLoggerLevel() & 0x8) != 0)
    {
      print(getLoggerOutter(), "ERROR:", paramInt);
    }
  }

  public void error(String paramString)
  {
    if ((getLoggerLevel() & 0x8) != 0)
    {
      print(getLoggerOutter(), "ERROR:", paramString);
    }
  }

  public void error(Exception paramException)
  {
    print(getLoggerOutter(), "ERROR:", getExceptionDetail(paramException));

    paramException.printStackTrace();
    if ((getLoggerLevel() & 0x8) != 0)
    {
      print(getLoggerOutter(), "ERROR:", paramException);
    }
  }

  public void error(Object paramObject)
  {
    if ((getLoggerLevel() & 0x8) != 0)
    {
      print(getLoggerOutter(), "ERROR:", paramObject);
    }
  }

  public int getLoggerLevel()
  {
    return this.iLevel;
  }

  public void setLoggerLevel(int paramInt)
  {
    this.iLevel = paramInt;
  }

  public LoggerOutter getLoggerOutter()
  {
    return this.loggerOutter;
  }

  public void setLoggerOutter(LoggerOutter paramLoggerOutter)
  {
    this.loggerOutter = paramLoggerOutter;
  }

  public String getClassName()
  {
    return this.sClassName;
  }

  public void setClassName(String paramString)
  {
    this.sClassName = paramString;
  }

  private void print(LoggerOutter paramLoggerOutter, String paramString, int paramInt)
  {
    synchronized (paramLoggerOutter)
    {
      paramLoggerOutter.print(paramString);
      printPrefix(paramLoggerOutter);
      paramLoggerOutter.print(paramInt);
      printSufix(paramLoggerOutter);
    }
  }

  private void print(LoggerOutter paramLoggerOutter, String paramString1, String paramString2)
  {
    synchronized (paramLoggerOutter)
    {
      paramLoggerOutter.print(paramString1);
      printPrefix(paramLoggerOutter);
      paramLoggerOutter.print(paramString2);
      printSufix(paramLoggerOutter);
    }
  }

  private void print(LoggerOutter paramLoggerOutter, String paramString, Object paramObject)
  {
    synchronized (paramLoggerOutter)
    {
      paramLoggerOutter.print(paramString);
      printPrefix(paramLoggerOutter);
      paramLoggerOutter.print(paramObject);
      printSufix(paramLoggerOutter);
    }
  }

  protected void printPrefix(LoggerOutter paramLoggerOutter)
  {
    paramLoggerOutter.print(new Timestamp(System.currentTimeMillis()) + ":");
    paramLoggerOutter.print(this.sModel + ":");
    paramLoggerOutter.print(getClassName() + ":");
  }

  protected void printSufix(LoggerOutter paramLoggerOutter)
  {
    paramLoggerOutter.print("\n");
  }

  private StringBuffer message(String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append(' ').append(getCaller());
    if ((null != paramString) && (paramString.trim().length() > 0))
    {
      localStringBuffer.append('\n').append(paramString.trim());
    }
    return localStringBuffer;
  }

  private String getCaller()
  {
    Exception localException = new Exception();
    StackTraceElement[] arrayOfStackTraceElement = localException.getStackTrace();
    StackTraceElement localStackTraceElement = null;

    for (int i = 0; i < arrayOfStackTraceElement.length; i++)
    {
      if (arrayOfStackTraceElement[i].getClassName().equals(this.sClassName))
      {
        if (i < arrayOfStackTraceElement.length - 1)
        {
          localStackTraceElement = arrayOfStackTraceElement[i];
          break;
        }

        return "";
      }
      if (arrayOfStackTraceElement[i].getClassName().equals(LoggerConfig.class.getName())) {
        continue;
      }
      localStackTraceElement = arrayOfStackTraceElement[i];
    }

    if (null == localStackTraceElement)
    {
      return "";
    }
    StringBuffer localStringBuffer = new StringBuffer();
    if (!localStackTraceElement.getClassName().equals(this.sClassName))
    {
      localStringBuffer.append(this.sClassName).append(" -> ");
    }
    localStringBuffer.append(localStackTraceElement.getClassName()).append('.').append(localStackTraceElement.getMethodName());
    localStringBuffer.append('(').append(localStackTraceElement.getFileName()).append(": ").append(localStackTraceElement.getLineNumber()).append(')');

    return localStringBuffer.toString();
  }
}
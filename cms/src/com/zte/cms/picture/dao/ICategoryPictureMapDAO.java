package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategoryPictureMapDAO
{
    /**
     * 新增CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DAOException dao异常
     */
    public void insertCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException;

    /**
     * 根据主键更新CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DAOException dao异常
     */
    public void updateCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException;

    /**
     * 根据主键删除CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException;

    /**
     * 根据主键查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return 满足条件的CategoryPictureMap对象
     * @throws DAOException dao异常
     */
    public CategoryPictureMap getCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException;

    /**
     * 根据条件查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return 满足条件的CategoryPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<CategoryPictureMap> getCategoryPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DAOException;

    /**
     * 根据条件分页查询CategoryPictureMap对象，作为查询条件的参数
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CategoryPictureMap对象，作为查询条件的参数
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

    /**
     * 根据条件分页查询CategoryPictureMap对象，作为查询条件的参数
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException;
    
    /**
     * 根据条件分页查询CategoryPictureMap对象，作为查询条件的参数
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return 满足条件的CategoryPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<CategoryPictureMap> getCategorynormalPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DAOException;   
}
package com.zte.cms.service.model;

import net.sf.cglib.beans.BeanCopier;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Service extends DynamicBaseObject
{
    private java.lang.Long serviceindex;
    private java.lang.String serviceid;
    private java.lang.String servicename;
    private java.lang.Integer feetype;
    private java.lang.Integer feecode;
    private java.lang.Integer fixedfee;
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String effecttime;
    private java.lang.String createtime;
    private java.lang.String modtime;
    private java.lang.String publishtime;
    private java.lang.String corpmode;
    private java.lang.String cntrange;
    private java.lang.String servicecode;
    private java.lang.Long cpindex;
    private java.lang.String cancelpubtime;
    private java.lang.String cpid;
    private java.lang.String operid;

    
    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    }

    public void setServiceindex(java.lang.Long serviceindex)
    {
        this.serviceindex = serviceindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.String getServicename()
    {
        return servicename;
    }

    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }

    public java.lang.Integer getFeetype()
    {
        return feetype;
    }

    public void setFeetype(java.lang.Integer feetype)
    {
        this.feetype = feetype;
    }

    public java.lang.Integer getFeecode()
    {
        return feecode;
    }

    public void setFeecode(java.lang.Integer feecode)
    {
        this.feecode = feecode;
    }

    public java.lang.Integer getFixedfee()
    {
        return fixedfee;
    }

    public void setFixedfee(java.lang.Integer fixedfee)
    {
        this.fixedfee = fixedfee;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getModtime()
    {
        return modtime;
    }

    public void setModtime(java.lang.String modtime)
    {
        this.modtime = modtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getServicecode()
    {
        return servicecode;
    }

    public void setServicecode(java.lang.String servicecode)
    {
        this.servicecode = servicecode;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    public void initRelation()
    {
        this.addRelation("serviceindex", "SERVICEINDEX");
        this.addRelation("serviceid", "SERVICEID");
        this.addRelation("servicename", "SERVICENAME");
        this.addRelation("feetype", "FEETYPE");
        this.addRelation("feecode", "FEECODE");
        this.addRelation("fixedfee", "FIXEDFEE");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("status", "STATUS");
        this.addRelation("workflow", "WORKFLOW");
        this.addRelation("workflowlife", "WORKFLOWLIFE");
        this.addRelation("effecttime", "EFFECTTIME");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("modtime", "MODTIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("corpmode", "CORPMODE");
        this.addRelation("cntrange", "CNTRANGE");
        this.addRelation("servicecode", "SERVICECODE");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");

    }

    public java.lang.String getCorpmode()
    {
        return corpmode;
    }

    public void setCorpmode(java.lang.String corpmode)
    {
        this.corpmode = corpmode;
    }

    public java.lang.String getCntrange()
    {
        return cntrange;
    }

    public void setCntrange(java.lang.String cntrange)
    {
        this.cntrange = cntrange;
    }

    public ServiceWkfwhis newStrategyWkfwhis()
    {
        ServiceWkfwhis serviceWkfwhis = new ServiceWkfwhis();
        BeanCopier Copier = BeanCopier.create(Service.class, ServiceWkfwhis.class, false);
        Copier.copy(this, serviceWkfwhis, null);
        return serviceWkfwhis;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }
}

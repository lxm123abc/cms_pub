package com.zte.cms.channel.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.zte.cms.basicdata.area.ls.IsysProvinceLS;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.categorycdn.common.CategoryConstant;
import com.zte.cms.categorycdn.ls.CategoryConstants;
import com.zte.cms.channel.common.CmsChannelConstant;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.channel.service.ICategoryChannelMapDS;
import com.zte.cms.channel.service.IChannelWkfwhisDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.channel.update.apply.IChannelUpdateApply;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CSplatformSyn.service.ICSCntPlatformSyncDS;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.CStargetSyn.service.ICSCntTargetSyncDS;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.IChannelPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysCityDS;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsChannelLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsChannelLS
{

    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private ICmsChannelDS channelDS;
    private IUcpBasicDS basicDS;
    private static final int STATUS = 4; // CP被删除时的状态
    private IPhysicalchannelDS physicalchannelDS;
    private ITargetsystemDS targetSystemDS;
    private IsysProvinceLS provinceLS;
    private IUsysCityDS cityDS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IBatchObjectRecordDS batchObjectRecordDS;// 批处理
    private IUsysConfigDS usysConfigDS;
    private IPhysicaChannellLS physicaChannellLS;
    private ICategoryChannelMapDS categoryChannelMapDS;
    private ICmsScheduleDS cmsScheduleDS;
    private IPictureDS pictureDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private ICmsChannelSyncLS cmsChannelSyncLS;
    private IChannelUpdateApply channelupdateApply;
    private ICntSyncRecordDS cntSyncRecordDS;
    private IServiceChannelMapDS serviceChannelMapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private IChannelPictureMapDS channelPictureMapDS;
    private static final String TEMPAREA_ID = "1"; // 临时区ID
    private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    private final static String CMSCASTMTYPE_CNTSYNCXML = "xmlsync";
    private final static String CMSCHANNELMTYPE_PROGRAMTYPE = "channelContent";
    private static final String BATCHID = "ucdn_task_batch_id";
    private static final String CORRELATEID = "ucdn_task_correlate_id";
    private static final String SUCCESS = "cmsprogramtype.success";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
    private ICSCntPlatformSyncDS cscntPlatformSyncDS;
    private ICSCntTargetSyncDS CScntTargetSyncDS;
    private static final String FAIL = "cmsprogramtype.fail";
    protected IChannelWkfwhisDS channelWkfwhisDS;
    protected IScheduleWkfwhisDS scheduleWkfwhisDS;

    private String result = "0:操作成功";
    public final static String CNTSYNCXML = "xmlsync";
    public final static String PROGRAM_DIR = "program";

    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败

    // 内容相关对象同步任务表 objectType
    public final static int PROGRAM_OBJECTTYPE = 1;
    public final static int MOVIE_OBJECTTYPE = 2;
    private Map chaneMap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和
    // 其子内容对象

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress = "";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private ICmsStorageareaLS cmsStorageareaLS;

    public ICmsChannelSyncLS getCmsChannelSyncLS()
    {
        return cmsChannelSyncLS;
    }

    public void setCmsChannelSyncLS(ICmsChannelSyncLS cmsChannelSyncLS)
    {
        this.cmsChannelSyncLS = cmsChannelSyncLS;
    }

    public IScheduleWkfwhisDS getScheduleWkfwhisDS()
    {
        return scheduleWkfwhisDS;
    }

    public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS)
    {
        this.scheduleWkfwhisDS = scheduleWkfwhisDS;
    }

    public IChannelWkfwhisDS getChannelWkfwhisDS()
    {
        return channelWkfwhisDS;
    }

    public void setChannelWkfwhisDS(IChannelWkfwhisDS channelWkfwhisDS)
    {
        this.channelWkfwhisDS = channelWkfwhisDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public ICSCntTargetSyncDS getCScntTargetSyncDS()
    {
        return CScntTargetSyncDS;
    }

    public void setCScntTargetSyncDS(ICSCntTargetSyncDS scntTargetSyncDS)
    {
        CScntTargetSyncDS = scntTargetSyncDS;
    }

    public ICSCntPlatformSyncDS getCscntPlatformSyncDS()
    {
        return cscntPlatformSyncDS;
    }

    public void setCscntPlatformSyncDS(ICSCntPlatformSyncDS cscntPlatformSyncDS)
    {
        this.cscntPlatformSyncDS = cscntPlatformSyncDS;
    }

    public IChannelPictureMapDS getChannelPictureMapDS()
    {
        return channelPictureMapDS;
    }

    public void setChannelPictureMapDS(IChannelPictureMapDS channelPictureMapDS)
    {
        this.channelPictureMapDS = channelPictureMapDS;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    /** 列表展示频道信息 */
    public TableDataInfo pageInfo(CmsChannel cmsChannel, int start, int pageSize)
    {
        log.debug("pageInfo CmsChannel start...");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        cmsChannel.setOperid(operinfo.getOperid());
        TableDataInfo dataInfo = null;
        try
        {
            if (cmsChannel.getChannelid() != null && !cmsChannel.getChannelid().equals(""))
            {
                String channelid = EspecialCharMgt.conversion(cmsChannel.getChannelid());
                cmsChannel.setChannelid(channelid);
            }
            if (cmsChannel.getCpcnshortname() != null && !cmsChannel.getCpcnshortname().equals(""))
            {
                String cpcnshortname = EspecialCharMgt.conversion(cmsChannel.getCpcnshortname());
                cmsChannel.setCpcnshortname(cpcnshortname);
            }
            if (cmsChannel.getChannelname() != null && !cmsChannel.getChannelname().equals(""))
            {
                String channelname = EspecialCharMgt.conversion(cmsChannel.getChannelname());
                cmsChannel.setChannelname(channelname);
            }
            if (cmsChannel.getStartcreatetime() != null && !cmsChannel.getStartcreatetime().equals(""))
            {
                cmsChannel.setStartcreatetime(DateUtil2.get14Time(cmsChannel.getStartcreatetime()));
            }
            if (cmsChannel.getEndcreatetime() != null && !cmsChannel.getEndcreatetime().equals(""))
            {
                cmsChannel.setEndcreatetime(DateUtil2.get14Time(cmsChannel.getEndcreatetime()));
            }
            if (cmsChannel.getStartOnlinetime() != null && !cmsChannel.getStartOnlinetime().equals(""))
            {
                cmsChannel.setStartOnlinetime(DateUtil2.get14Time(cmsChannel.getStartOnlinetime()));
            }
            if (cmsChannel.getEndOnlinetime() != null && !cmsChannel.getEndOnlinetime().equals(""))
            {
                cmsChannel.setEndOnlinetime(DateUtil2.get14Time(cmsChannel.getEndOnlinetime()));
            }
            if (cmsChannel.getStartOfflinetime() != null && !cmsChannel.getStartOfflinetime().equals(""))
            {
                cmsChannel.setStartOfflinetime(DateUtil2.get14Time(cmsChannel.getStartOfflinetime()));
            }
            if (cmsChannel.getEndOfflinetime() != null && !cmsChannel.getEndOfflinetime().equals(""))
            {
                cmsChannel.setEndOfflinetime(DateUtil2.get14Time(cmsChannel.getEndOfflinetime()));
            }
            dataInfo = channelDS.pageInfoQuery(cmsChannel, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfo error");
        }
        log.debug("pageInfo CmsChannel end");
        return dataInfo;
    }

    /** 按id得到频道信息 */
    public CmsChannel getChannel(CmsChannel channel)
    {
        CmsChannel cmsChannel = null;
        try
        {
            cmsChannel = channelDS.getCmsChannel(channel);
            if (cmsChannel == null)
            {// 对象已被删除
                return null;
            }
            else
            {
                return cmsChannel;
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        return cmsChannel;
    }

    /** 频道新增 */
    public String insertChannelContent(CmsChannel channel)
    {
        log.debug("insertIcmChannel starting............");
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String cpid = channel.getCpid();
        String licenseid = channel.getLicenseid();

        try
        {

            channel.setLlicensofflinetime(DateUtil2.get14Time(channel.getLlicensofflinetime()));
            channel.setDeletetime(DateUtil2.get14Time(channel.getDeletetime()));
            channel.setOnlinetime(DateUtil2.get14Time(channel.getOnlinetime()));
            channel.setOfflinetime(DateUtil2.get14Time(channel.getOfflinetime()));
            channel.setLicensingstart(DateUtil2.get14Time(channel.getLicensingstart()));
            channel.setLicensingend(DateUtil2.get14Time(channel.getLicensingend()));

            // 判断频道名称是否重复
            /*
            if (isRepeatedChannelName(channel.getChannelname()) > 0)
            {
                return "1:频道名称重复";
            }
            */
            // 判断建议频道号是否重复
            if (isRepeatedChannelNumber(channel.getChannelnumber()) > 0)
            {
                return "1:建议频道号重复";
            }
            /*
             * //判断CP对于频道的标识是否重复 if (channel.getCpcontentid()!=null&&!channel.getCpcontentid().equals("")) { CmsChannel
             * cpcontentidChannel=new CmsChannel(); cpcontentidChannel.setCpcontentid(channel.getCpcontentid()); List
             * listCpcontentidChannel=channelDS.getChannelByCond(cpcontentidChannel); if
             * (listCpcontentidChannel!=null&&listCpcontentidChannel.size()>0) { return "1:CP对于频道的标识重复"; } }
             */

            // 判断所属全局内容唯一标识
            if (channel.getUnicontentid() != null && !channel.getUnicontentid().equals(""))
            {
                CmsChannel unicontentidChannel = new CmsChannel();
                unicontentidChannel.setUnicontentid(channel.getUnicontentid());
                List unicontentChannelList = channelDS.getChannelByCond(unicontentidChannel);
                if (unicontentChannelList != null && unicontentChannelList.size() > 0)
                {
                    return "1:所属全局唯一标识重复";
                }
            }

            channel.setStatus(110);

            // 判断CP是否存在
            if ((null != cpid) && (!"".equals(cpid)))
            { // 插入频道的时候判断cp有没有被删除，如果被删除则不能执行插入操作
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cpid);
                ucpBasic.setCptype(1);
                List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() != 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == STATUS)
                    {
                        return "1:CP不存在"; // 表示cp已被删除
                    }
                    if (status == 2)
                    {
                        return "1:CP已暂停"; // 表示cp已被删除
                    }
                }
            }
            // 判断牌照方是否存在
            if ((null != licenseid) && (!"".equals(licenseid)))
            { // 插入频道的时候判断牌照方有没有被删除，如果被删除则不能执行插入操作
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(licenseid);
                ucpBasic.setCptype(2);
                List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() != 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == STATUS)
                    {
                        return "1:牌照方不存在"; // 表示牌照方被删除
                    }
                    if (status == 2)
                    {
                        return "1:牌照方已暂停"; // 表示cp已被删除
                    }
                }
            }
            channelDS.insertCmsChannel(channel);
            CommonLogUtil.insertOperatorLog(channel.getChannelid(), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_ADD,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_ADD_INFO, CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "1:操作失败";
        }
        return "0:操作成功";
    }

    /*
     * 查询这个建议频道号是否重复 @param icmChannel @param oper 当insert时，oper为1，当修改时，oper为2 当新建频道时，重复的频道名称为0时，返回0 @return
     * 不重复返回0，重复返回1，异常返回-1
     */
    private Integer isRepeatedChannelNumber(Integer channelNumber) throws DomainServiceException
    {
        Integer flag = 0;
        Integer num = 0;
        CmsChannel channel = new CmsChannel();
        channel.setChannelnumber(channelNumber);
        num = channelDS.getChannelByCond(channel).size();
        if (num == 0)
        {
            flag = 0;
        }
        else
        {
            flag = 1;
        }

        return flag;
    }

    /**
     * 判断频道名称是否重复
     * 
     * @param channelName
     * @return
     */
    private Integer isRepeatedChannelName(String channelName)
    {
        CmsChannel channel = new CmsChannel();
        channel.setChannelname(channelName);
        List list = channelDS.getChannelByCond(channel);
        if (list != null && list.size() > 0)
        {// 有重复
            return 1;
        }
        else
            // 没有重复
            return 0;
    }

    /** 删除频道 */
    public String deleteChannelContent(CmsChannel channel)
    {
        try
        {
            CmsChannel cmsChannel = getChannel(channel);
            String channelid = "";
            List tarlist = new ArrayList();
            List platlist = new ArrayList();
            if (cmsChannel == null)
            {
                return "1:该频道不存在";
            }
            else
            {
                channelid = cmsChannel.getChannelid();

                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(cmsChannel.getChannelindex());
                cntTargetSync.setObjecttype(5);

                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setObjectid(cmsChannel.getChannelid());
                cntPlatformSync.setObjecttype(5);
                platlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                try
                {
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (tarlist != null && tarlist.size() > 0)
                {
                    for (int i = 0; i < tarlist.size(); i++)
                    {
                        cntTargetSync = (CntTargetSync) tarlist.get(i);
                        int status = cntTargetSync.getStatus();
                        // 发布中，不允许修改
                        if (status == 200 || status == 300 || status == 500)
                        {

                            return "1:频道已发布不能删除";
                        }
                    }
                }
                Integer status = cmsChannel.getStatus();
                // 初始状态110、审核失败130、待同步0、取消同步成功80
                if (status == 120)
                {
                    return "1:频道处于审核中不能删除";
                }

                // 判断是否有栏目
                CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
                categoryChannelMap.setChannelid(cmsChannel.getChannelid());
                List listCategorychannel = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
                if (listCategorychannel != null && listCategorychannel.size() > 0)
                {
                    // return "1:频道与栏目有关联关系，不能删除";
                    categoryChannelMapDS.removeCategoryChannelMapList(listCategorychannel);
                }

                // 判断是否有海报
                Picture picture = new Picture();
                List listPicture = pictureDS.getPictureByChannelIndex(cmsChannel.getChannelindex());
                if (listPicture != null && listPicture.size() > 0)
                {
                    // return "1:频道下有海报，不能删除";
                    pictureDS.removePictureList(listPicture);
                }

                // 判断是否有节目单
                CmsSchedule cmsSchedule = new CmsSchedule();
                cmsSchedule.setChannelid(cmsChannel.getChannelid());
                List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                if (listSchedule != null && listSchedule.size() > 0)
                {
                    // return "1:频道下有节目单，不能删除";
                    for (int i = 0; i < listSchedule.size(); i++)
                    {
                        cmsSchedule = (CmsSchedule) listSchedule.get(i);
                        // 删除节目单工作流历史表记录
                        ScheduleWkfwhis cmsScheduleWkfwhis = new ScheduleWkfwhis();
                        cmsScheduleWkfwhis.setScheduleid(cmsSchedule.getScheduleid());
                        List<ScheduleWkfwhis> cmsScheduleWkfwhisList = scheduleWkfwhisDS
                                .getScheduleWkfwhisByCond(cmsScheduleWkfwhis);
                        if (cmsScheduleWkfwhisList != null && cmsScheduleWkfwhisList.size() > 0)
                        {
                            scheduleWkfwhisDS.removeScheduleWkfwhisList(cmsScheduleWkfwhisList);
                        }
                    }
                    cmsScheduleDS.removeCmsScheduleList(listSchedule);
                }

                // 判断是否有物理频道
                Physicalchannel physicalchannel = new Physicalchannel();
                physicalchannel.setChannelid(cmsChannel.getChannelid());
                List listPhysicalchannel = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannel);
                if (listPhysicalchannel != null && listPhysicalchannel.size() > 0)
                {
                    // return "2:频道下有物理频道，确认把物理频道一起删除";
                    physicalchannelDS.removePhysicalchannelList(listPhysicalchannel);
                }
                // 删除相关网元及平台的发布数据,调用删除关联平台的方法，先查出所有网元,里面有删除平台方法
                if (tarlist != null && tarlist.size() > 0)
                {
                    for (int num = 0; num < tarlist.size(); num++)
                    {
                        CntTargetSync cnt = (CntTargetSync) tarlist.get(num);
                        deletecntsync(cnt.getSyncindex());// 删除对象网元，海报网元，平台，节目单网元
                    }
                }

            }
            // 删除工作流历史表记录
            ChannelWkfwhis cmsChannelWkfwhis = new ChannelWkfwhis();
            cmsChannelWkfwhis.setChannelid(cmsChannel.getChannelid());
            List<ChannelWkfwhis> cmsChannelWkfwhisList = channelWkfwhisDS.getChannelWkfwhisByCond(cmsChannelWkfwhis);
            if (cmsChannelWkfwhisList != null && cmsChannelWkfwhisList.size() > 0)
            {
                channelWkfwhisDS.removeChannelWkfwhisList(cmsChannelWkfwhisList);
            }
            channelDS.deleteChannelContent(cmsChannel);
            // if (tarlist != null && tarlist.size() > 0) {
            // cntTargetSyncDS.removeCntTargetSyncList(tarlist);
            // }
            // if (platlist != null && platlist.size() > 0) {
            // cntPlatformSyncDS.removeCntPlatformSyncList(platlist);
            // }
            CommonLogUtil.insertOperatorLog(cmsChannel.getChannelid(), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_DEL,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_DEL_INFO, CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
            return "0:操作成功";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    /** 删除频道时连并物理频道一起删除 */
    public String deleteChannelWithPhysical(CmsChannel channel)
    {
        try
        {
            CmsChannel cmsChannel = getChannel(channel);
            String channelid = "";
            if (cmsChannel == null)
            {
                return "1:该频道不存在";
            }
            else
            {
                channelid = cmsChannel.getChannelid();
                Integer status = cmsChannel.getStatus();
                // 初始状态110、审核失败130、待同步0、取消同步成功80
                if (status != 0 && status != 80 && status != 130 && status != 110)
                {
                    return "1:当前状态下不能删除频道";
                }

                // 判断是否有栏目
                CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
                categoryChannelMap.setChannelid(cmsChannel.getChannelid());
                List listCategorychannel = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
                if (listCategorychannel != null && listCategorychannel.size() > 0)
                {
                    return "1:频道与栏目有关联关系，不能删除";
                }

                // 判断是否有海报
                Picture picture = new Picture();
                List listPicture = pictureDS.getPictureByChannelIndex(cmsChannel.getChannelindex());
                if (listPicture != null && listPicture.size() > 0)
                {
                    return "1:频道下有海报，不能删除";
                }

                // 判断是否有节目单
                CmsSchedule cmsSchedule = new CmsSchedule();
                cmsSchedule.setChannelid(cmsChannel.getChannelid());
                List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                if (listSchedule != null && listSchedule.size() > 0)
                {
                    return "1:频道下有节目单，不能删除";
                }

            }

            // 判断是否有物理频道
            Physicalchannel physicalchannel = new Physicalchannel();
            physicalchannel.setChannelid(cmsChannel.getChannelid());
            List<Physicalchannel> listPhysicalchannel = physicalchannelDS
                    .listPhysicalchannelByChannelid(physicalchannel);
            if (listPhysicalchannel != null && listPhysicalchannel.size() > 0)
            {
                for (Physicalchannel delPhysicalchannel : listPhysicalchannel)
                {
                    physicalchannelDS.removePhysicalchannel(delPhysicalchannel);
                }
            }
            channelDS.deleteChannelContent(channel);

            CommonLogUtil.insertOperatorLog(channel.getChannelid(), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_DEL,
                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_DEL_INFO, CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
            return "0:操作成功";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    /** 修改频道 */
    public String updateChannelContent(CmsChannel channel)
    {
        try
        {
            // 判断修改的频道是否存在
            CmsChannel channelCheck = channelDS.getCmsChannel(channel);
            if (channelCheck == null)
            {
                return ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);
            }
            channel.setLlicensofflinetime(DateUtil2.get14Time(channel.getLlicensofflinetime()));
            channel.setDeletetime(DateUtil2.get14Time(channel.getDeletetime()));
            channel.setOnlinetime(DateUtil2.get14Time(channel.getOnlinetime()));
            channel.setOfflinetime(DateUtil2.get14Time(channel.getOfflinetime()));
            channel.setLicensingstart(DateUtil2.get14Time(channel.getLicensingstart()));
            channel.setLicensingend(DateUtil2.get14Time(channel.getLicensingend()));

            CmsChannel cmsChannel = getChannel(channel);
            String cpid = channel.getCpid();
            String licenseid = channel.getLicenseid();
            if (channel == null)
            {
                return null;
            }
            else
            {
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(cmsChannel.getChannelindex());
                cntTargetSync.setObjecttype(5);
                List tarlist = new ArrayList();
                try
                {
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (tarlist != null && tarlist.size() > 0)
                {
                    for (int i = 0; i < tarlist.size(); i++)
                    {
                        cntTargetSync = (CntTargetSync) tarlist.get(i);
                        int status = cntTargetSync.getStatus();
                        // 发布中，不允许修改
                        if (status == 200)
                        {
                            String msg = "1:发布中，不允许修改";
                            return msg;
                        }
                    }
                }
                /*
                 * //判断CP对于频道的标识是否重复 if (channel.getCpcontentid()!=null&&!channel.getCpcontentid().equals("")) {
                 * CmsChannel cpcontentidChannel=new CmsChannel();
                 * cpcontentidChannel.setCpcontentid(channel.getCpcontentid()); List
                 * listCpcontentidChannel=channelDS.getChannelByCond(cpcontentidChannel); if
                 * (listCpcontentidChannel!=null&&listCpcontentidChannel.size()>1) { return "1:CP对于频道的标识重复"; }else { if
                 * (listCpcontentidChannel.size()==1) { cpcontentidChannel=(CmsChannel) listCpcontentidChannel.get(0);
                 * if (!cpcontentidChannel.getChannelid().equals(channel.getChannelid())) { return "1:CP对于频道的标识重复"; } }
                 * } }
                 */

                // 判断频道名称是否重复
                /*
                CmsChannel channelnameChannel = new CmsChannel();
                channelnameChannel.setChannelname(channel.getChannelname());
                List<CmsChannel> channelnameChannelList = channelDS.getChannelByCond(channelnameChannel);
                if (channelnameChannelList != null && channelnameChannelList.size() > 0)
                {
                    channelnameChannel = channelnameChannelList.get(0);
                    if (!channelnameChannel.getChannelid().equals(channel.getChannelid()))
                    {
                        return "1:频道名称已存在";
                    }
                }
                */

                // 判断建议频道号是否重复
                CmsChannel channelnumberChannel = new CmsChannel();
                channelnumberChannel.setChannelnumber(channel.getChannelnumber());
                List<CmsChannel> channelnumberChannelList = channelDS.getChannelByCond(channelnumberChannel);
                if (channelnumberChannelList != null && channelnumberChannelList.size() > 0)
                {
                    channelnumberChannel = channelnumberChannelList.get(0);
                    if (!channelnumberChannel.getChannelid().equals(channel.getChannelid()))
                    {
                        return "1:建议频道号重复";
                    }
                }

                // 判断所属全局内容唯一标识
                if (channel.getUnicontentid() != null && !channel.getUnicontentid().equals(""))
                {
                    CmsChannel unicontentidChannel = new CmsChannel();
                    unicontentidChannel.setUnicontentid(channel.getUnicontentid());
                    List unicontentChannelList = channelDS.getChannelByCond(unicontentidChannel);
                    if (unicontentChannelList != null && unicontentChannelList.size() > 1)
                    {
                        return "1:所属全局唯一标识重复";
                    }
                    else
                    {
                        if (unicontentChannelList.size() == 1)
                        {
                            unicontentidChannel = (CmsChannel) unicontentChannelList.get(0);
                            if (!unicontentidChannel.getChannelid().equals(channel.getChannelid()))
                            {
                                return "1:所属全局唯一标识重复";
                            }
                        }
                    }
                }

                // CP和牌照方是否存在
                // 判断CP是否存在
                if ((null != cpid) && (!"".equals(cpid)))
                { // 插入频道的时候判断cp有没有被删除，如果被删除则不能执行插入操作
                    UcpBasic ucpBasic = new UcpBasic();
                    ucpBasic.setCpid(cpid);
                    ucpBasic.setCptype(1);
                    List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                    if (null != list && list.size() != 0)
                    {
                        ucpBasic = (UcpBasic) list.get(0);
                        if (ucpBasic.getStatus() == STATUS)
                        {
                            return "1:CP不存在"; // 表示cp已被删除
                        }
                    }
                }
                // 判断牌照方是否存在
                if ((null != licenseid) && (!"".equals(licenseid)))
                { // 插入频道的时候判断牌照方有没有被删除，如果被删除则不能执行插入操作
                    UcpBasic ucpBasic = new UcpBasic();
                    ucpBasic.setCpid(licenseid);
                    ucpBasic.setCptype(2);
                    List list = basicDS.getUcpBasicExactByCond(ucpBasic);
                    if (null != list && list.size() != 0)
                    {
                        ucpBasic = (UcpBasic) list.get(0);
                        int licenseidStatus = ucpBasic.getStatus();
                        if (licenseidStatus == STATUS)
                        {
                            return "1:牌照方不存在"; // 表示牌照方被删除
                        }
                    }
                }
            }
            if (channelCheck.getStatus() == 110 || channelCheck.getStatus() == 130)
            {

                try
                {
                    channelDS.updateCmsChannel(channel);// 还未提审核流程，则修改不需要审核

                }
                catch (Exception e)
                {
                    throw new Exception(e.getMessage());
                }

                CommonLogUtil.insertOperatorLog(channel.getChannelid(), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                        CmsChannelConstant.OPERTYPE_CHANNELCONTENT_UPD,
                        CmsChannelConstant.OPERTYPE_CHANNELCONTENT_UPD_INFO,
                        CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_UPDATE_SUCCESSFUL);// 频道修改成功
            }
            else if (channelCheck.getStatus() == 120)
            // 审核中
            {
                return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG); // 频道状态不正确
            }
            else
            // 直接发起审核流程
            {
                // 查看是否免审
                UsysConfig usysConfig = new UsysConfig();
                List<UsysConfig> usysConfigList = null;
                int flag = 0;
                usysConfig.setCfgkey("cms.channel.noaudit.switch");
                usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                if (null != usysConfigList && usysConfigList.size() > 0)
                {
                    usysConfig = usysConfigList.get(0);
                    flag = Integer.parseInt(usysConfig.getCfgvalue());//
                }
                String rtn = "";
                if (flag == 0)
                {// 免审

                    channelDS.updateCmsChannel(channel);// 更新本地表频道
                    CntTargetSync cntTargetSync = new CntTargetSync();
                    cntTargetSync.setObjectindex(channel.getChannelindex());
                    cntTargetSync.setObjecttype(5);

                    List tarlist = new ArrayList();
                    try
                    {
                        tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                    }
                    catch (DomainServiceException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    List<CntTargetSync> publist = new ArrayList();
                    if (tarlist != null && tarlist.size() > 0)
                    {
                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                .toString());
                        for (int i = 0; i < tarlist.size(); i++)
                        {
                            cntTargetSync = (CntTargetSync) tarlist.get(i);
                            int statuscha = cntTargetSync.getStatus();
                            if (statuscha == 300 || statuscha == 500)
                            {
                                publist.add(cntTargetSync);

                            }

                        }
                        rtn = cmsChannelSyncLS.publishChannelcontentTar(publist);// 可以发布到的网元
                    }
                    if (rtn.equals("") || rtn.equals("success"))
                    {
                        rtn = "0:操作成功";
                    }
                }
                else
                {
                    rtn = channelupdateApply.apply(channel);
                }
                return rtn;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }

    }

    /**
     * 预下线
     * 
     * @param channel
     * @return
     */
    public String startOffline(CmsChannel channel)
    {
        try
        {
            CmsChannel rtnChannel = getChannel(channel);
            if (rtnChannel == null)
            {
                return "1:频道不存在";
            }
            else
            {
                if (rtnChannel.getStatus() != 20 && rtnChannel.getStatus() != 50)
                {
                    return "1:当前状态不能预下线";
                }
                else
                {

                    // 物理频道在***中时频道不能取消同步
                    Physicalchannel physicalchannel = new Physicalchannel();
                    physicalchannel.setChannelid(rtnChannel.getChannelid());
                    List listPhysicalchannel = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                    if (listPhysicalchannel != null && listPhysicalchannel.size() > 0)
                    {
                        int count = 0;
                        for (Iterator iterator = listPhysicalchannel.iterator(); iterator.hasNext();)
                        {
                            Physicalchannel rtnPhysicalchannel = (Physicalchannel) iterator.next();
                            int status = rtnPhysicalchannel.getStatus();
                            if (status == 120 || status == 10 || status == 40 || status == 70)
                            {
                                count++;
                            }
                        }
                        if (count > 0)
                        {
                            return "1:频道下有不符合要求物理频道，不能预下线";
                        }
                        else
                        {// 如果已生成了录制计划不能取消同步
                            count = 0;
                            for (Iterator iterator = listPhysicalchannel.iterator(); iterator.hasNext();)
                            {
                                Physicalchannel schedulePhysicalchannel = (Physicalchannel) iterator.next();
                                CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
                                cmsSchedulerecord
                                        .setPhysicalchannelideq(schedulePhysicalchannel.getPhysicalchannelid());
                                List listSchedulerecord = cmsSchedulerecordDS
                                        .getCmsSchedulerecordByCond(cmsSchedulerecord);

                                if (listSchedulerecord != null && listSchedulerecord.size() > 0)
                                {
                                    count++;
                                }
                            }
                            if (count > 0)
                            {
                                return "1:频道已生成了录制计划，不能预下线";
                            }
                        }
                    }

                    ServiceChannelMap serviceChannelMap = new ServiceChannelMap();
                    serviceChannelMap.setChannelid(rtnChannel.getChannelid());
                    List listServiceChannel = serviceChannelMapDS.getServiceChannelMapByCond(serviceChannelMap);
                    if (listServiceChannel != null && listServiceChannel.size() > 0)
                    {
                        return "1:频道已关联了服务，不能预下线";
                    }

                    // 判断如果有已关联栏目，不能取消发布
                    CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
                    categoryChannelMap.setChannelid(rtnChannel.getChannelid());
                    List listCategoryChannel = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
                    if (listCategoryChannel != null && listCategoryChannel.size() > 0)
                    {
                        return "1:频道关联了栏目，不能预下线";
                    }

                    // 如果有已发布的节目单不能取消发布
                    CmsSchedule cmsSchedule = new CmsSchedule();
                    cmsSchedule.setChannelid(rtnChannel.getChannelid());
                    // cmsSchedule.setStatus(20);// 已发布
                    List<CmsSchedule> listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                    if (listSchedule != null && listSchedule.size() > 0)
                    {
                        for (CmsSchedule schedule : listSchedule)
                        {
                            if (schedule.getStatus() == 20 || schedule.getStatus() == 50 || schedule.getStatus() == 60)
                            {
                                return "1:频道下有节目单已发布，不能预下线";
                            }
                        }
                    }

                    // 如果有已发布的海报，就不能取消同步
                    List listPicture = pictureDS.getPictureByChannelIndex(rtnChannel.getChannelindex());
                    if (listPicture != null && listPicture.size() > 0)
                    {
                        int count = 0;
                        for (Iterator iterator = listPicture.iterator(); iterator.hasNext();)
                        {
                            Picture picture = (Picture) iterator.next();
                            if (picture.getStatus() != 0 && picture.getStatus() != 30 && picture.getStatus() != 80)
                            {// 已发布
                                count++;
                            }
                        }
                        if (count > 0)
                        {
                            return "1:频道下有海报状态不正确，不能预下线";
                        }
                        else
                        {
                            rtnChannel.setClicknumber(Long.parseLong((rtnChannel.getStatus().toString())));
                            rtnChannel.setStatus(65);
                            channelDS.updateCmsChannel(rtnChannel);
                            CommonLogUtil.insertOperatorLog(rtnChannel.getChannelid(),
                                    CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE,
                                    CmsChannelConstant.OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE_INFO,
                                    CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                            return "0:操作成功";
                        }
                    }
                    else
                    {
                        rtnChannel.setClicknumber(Long.parseLong((rtnChannel.getStatus().toString())));
                        rtnChannel.setStatus(65);
                        channelDS.updateCmsChannel(rtnChannel);
                        CommonLogUtil.insertOperatorLog(rtnChannel.getChannelid(),
                                CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                CmsChannelConstant.OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE,
                                CmsChannelConstant.OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE_INFO,
                                CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                        return "0:操作成功";
                    }
                }
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    // 不用插入map
    public String toRecordObj(Long taskindex, Long objindex, Long destindex, String objid, String elementid,
            int objtype, int actiontype, String cpcontentid) throws DomainServiceException
    {
        // 查出物理频道
        Physicalchannel physicalchannel = new Physicalchannel();
        physicalchannel.setChannelid(objid);
        List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
        if (phylist != null && phylist.size() > 0)
        {
            for (int i = 0; i < phylist.size(); i++)
            {
                physicalchannel = (Physicalchannel) phylist.get(i);
                ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
                objectSyncRecord.setTaskindex(taskindex);
                objectSyncRecord.setObjectindex(physicalchannel.getPhysicalchannelindex());
                objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
                objectSyncRecord.setObjecttype(8);// 对象类型编号：1-service，2-Category，3-Program
                objectSyncRecord.setObjectid(physicalchannel.getPhysicalchannelid());
                objectSyncRecord.setDestindex(destindex);
                objectSyncRecord.setObjectcode(physicalchannel.getCpcontentid());// 文广code
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            }
        }
        // 查找目标系统类型
        Targetsystem targetsystem = new Targetsystem();
        targetsystem.setTargetindex(destindex);
        List list = targetSystemDS.getTargetsystemByCond(targetsystem);
        if (list != null && list.size() > 0)
        {
            targetsystem = (Targetsystem) list.get(0);
        }
        int targetsystemtype = targetsystem.getTargettype();
        if (targetsystem.getPlatform() == 2 && targetsystemtype == 3)
        {// 3.0平台,cdn

        }
        else
        {

            ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objindex);
            objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
            objectSyncRecord.setObjecttype(objtype);// 对象类型编号：1-service，2-Category，3-Program
            if (!elementid.equals(""))
            {
                objectSyncRecord.setElementid(elementid);
                objectSyncRecord.setParentid(objid);// mapping的做法
                objectSyncRecord.setObjectid(objid);
            }
            else
            {
                objectSyncRecord.setObjectid(objid);
            }
            objectSyncRecord.setDestindex(destindex);
            objectSyncRecord.setObjectcode(cpcontentid);
            try
            {
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    public String insertSyncTask(Long targetindex, String contentmngxmlurl, Long objindex, String objectid,
            int objtype, int actiontype, String cpcontentid, Long batchid, int status) throws DomainServiceException
    {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setBatchid(batchid);
        cntsyncTask.setStatus(status);// 任务状态：0-初始 1-待执行 2-执行中 3-成功结束 4-失败结束
        cntsyncTask.setPriority(5);// 任务优先级：1-高 2-中 3-低
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(contentmngxmlurl);
        cntsyncTask.setDestindex(targetindex);
        cntsyncTask.setSource(1);
        cntsyncTask.setTaskindex(taskindex);
        cntsyncTask.setStarttime(dateformat.format(new Date()));
        cntsyncTask.setRetrytimes(3);
        try
        {
            cntsyncTaskDS.insertCntSyncTask(cntsyncTask);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        toRecordObj(taskindex, objindex, targetindex, objectid, "", objtype, actiontype, cpcontentid);// 记录到发布日志表,2是栏目
        // 栏目

        return null;
    }

    // 取消发布频道网元方法,1个频道，1个平台下的
    public HashMap cancelpublishChannelcontentTar(List<CntTargetSync> tarlist) throws DomainServiceException
    {
        log.debug("publishChannelcontent start ");
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";

        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setSyncindex(tarlist.get(0).getRelateindex());
        List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
        if (pltlist != null && pltlist.size() > 0)
        {
            cntPlatformSync = (CntPlatformSync) pltlist.get(0);
        }
        int index = 1;
        List canPubSuccessChannels = new ArrayList();
        Targetsystem targetSystem = new Targetsystem();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                map.put("msg", "1:获取挂载点失败");
                return map;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                map.put("msg", "1:获取临时区失败");
                return map;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                map.put("msg", "1:获取发布XML文件FTP地址失败");
                return map;
            }

            // 判断频道内容是否存在
            CntTargetSync sync = tarlist.get(0);// 通过网元得到频道信息,只传进来一个网元

            CmsChannel channel = new CmsChannel();
            channel.setChannelindex(sync.getObjectindex());// 获得频道
            channel = getChannel(channel);
            if (channel == null)
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelindex().toString(), "操作失败",
                // "频道不存在");
                // returnInfo.appendOperateInfo(operateInfo);
                map.put("msg", "1:频道不存在");
                return map;
            }
            else if (channel.getStatus() != 0)
            {
                map.put("msg", "1:频道未审核通过");
                return map;
            }
            else
            {
                if (tarlist != null && tarlist.size() > 0)
                {
                    for (int j = 0; j < tarlist.size(); j++)
                    {

                        CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {
                            tarsync = (CntTargetSync) tarsynclistinittar.get(0);
                        }
                        else
                        {
                            map.put("msg", "1:发布数据不存在");

                            return map;
                        }
                        List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                        if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                        {

                        }
                        else
                        {
                            map.put("msg", "1:发布数据不存在");

                            return map;
                        }

                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            map.put("msg", "1:目标系统状态不正确");
                            return map;

                        }
                        int statusbegin = tarsync.getStatus();
                        if (tarsync.getStatus() == 0 || tarsync.getStatus() == 200 || tarsync.getStatus() == 400)
                        {// 频道在此网元上状态不正确
                         // ifail++;
                         // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelid(), "操作失败",
                         // "频道在此网元上的状态不能取消发布");
                         // returnInfo.appendOperateInfo(operateInfo);
                            map.put("msg", "1:频道发布数据状态不正确");
                            continue;

                        }
                        else
                        {

                            String objid = tarsync.getObjectid();// 栏目id
                            boolean flagele = false;
                            CntTargetSync syncservice = new CntTargetSync();
                            syncservice.setElementid(objid);
                            syncservice.setObjecttype(23);// 服务与频道 关联
                            syncservice.setTargetindex(tarsync.getTargetindex());
                            List synclist = cntTargetSyncDS.getCntTargetSyncByCond(syncservice);
                            if (synclist != null && synclist.size() > 0)
                            {
                                for (int num = 0; num < synclist.size(); num++)
                                {
                                    sync = (CntTargetSync) synclist.get(num);
                                    int status = sync.getStatus();
                                    if (status == 200 || status == 300 || status == 500)
                                    {
                                        // ifail++;
                                        // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelid()
                                        // .toString(), ResourceMgt.findDefaultText(FAIL),
                                        // "此频道与服务的 mapping关系已发布不能取消同步");
                                        // returnInfo.appendOperateInfo(operateInfo);
                                        map.put("msg", "1:此频道与服务的关联关系已发布不能取消发布");
                                        flagele = true;
                                        continue;
                                    }

                                }
                            }

                            CntTargetSync syncparent = new CntTargetSync();
                            syncparent.setElementid(objid);
                            syncparent.setObjecttype(28);// 频道与栏目
                            syncparent.setTargetindex(tarsync.getTargetindex());
                            List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                            if (syncparentlist != null && syncparentlist.size() > 0)
                            {
                                for (int num = 0; num < syncparentlist.size(); num++)
                                {
                                    syncparent = (CntTargetSync) syncparentlist.get(num);
                                    int status = syncparent.getStatus();
                                    if (status == 200 || status == 300 || status == 500)
                                    {
                                        // ifail++;
                                        // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelid()
                                        // .toString(), ResourceMgt.findDefaultText(FAIL),
                                        // "此频道与栏目的 mapping关系已发布不能取消同步");
                                        // returnInfo.appendOperateInfo(operateInfo);
                                        map.put("msg", "1:此频道与栏目的关联关系已发布不能取消发布");
                                        flagele = true;
                                        continue;
                                    }

                                }
                            }

                            CntTargetSync syncpic = new CntTargetSync();
                            syncpic.setElementid(objid);
                            syncpic.setObjecttype(39);// 频道与海报
                            syncpic.setTargetindex(tarsync.getTargetindex());
                            List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                            if (syncpiclist != null && syncpiclist.size() > 0)
                            {
                                for (int num = 0; num < syncpiclist.size(); num++)
                                {
                                    syncpic = (CntTargetSync) syncpiclist.get(num);
                                    int status = syncpic.getStatus();
                                    if (status == 200 || status == 300 || status == 500)
                                    {
                                        // ifail++;
                                        // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelid()
                                        // .toString(), ResourceMgt.findDefaultText(FAIL),
                                        // "此频道与海报的 mapping关系已发布不能取消同步");
                                        // returnInfo.appendOperateInfo(operateInfo);
                                        map.put("msg", "1:此频道与海报的关联关系已发布不能取消发布");
                                        flagele = true;
                                        continue;
                                    }

                                }
                            }
                            if (flagele == true)
                            {
                                continue;
                            }
                            // 如果有已发布的节目单不能取消发布,这个需改到到网元下去查状态
                            CmsSchedule cmsSchedule = new CmsSchedule();
                            cmsSchedule.setChannelid(channel.getChannelid());
                            List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                            if (listSchedule != null && listSchedule.size() > 0)
                            {
                                int schenum = 0;
                                for (int k = 0; k < listSchedule.size(); k++)
                                {
                                    cmsSchedule = (CmsSchedule) listSchedule.get(k);
                                    CntTargetSync tarsyncschedule = new CntTargetSync();
                                    tarsyncschedule.setObjectid(cmsSchedule.getScheduleid());
                                    tarsyncschedule.setObjecttype(6);
                                    tarsyncschedule.setTargetindex(tarsync.getTargetindex());
                                    List list = cntTargetSyncDS.getCntTargetSyncByCond(tarsyncschedule);
                                    if (list != null && list.size() > 0)
                                    {
                                        for (int count = 0; count < list.size(); count++)
                                        {
                                            tarsyncschedule = (CntTargetSync) list.get(count);
                                            if (tarsyncschedule.getStatus() == 200
                                                    || tarsyncschedule.getStatus() == 300
                                                    || tarsyncschedule.getStatus() == 500)
                                            {
                                                schenum++;
                                            }
                                        }

                                    }
                                }
                                if (schenum > 0)
                                {
                                    ifail++;

                                    map.put("msg", "1:频道下节目单已发布");
                                    continue;
                                }
                            }

                            canPubSuccessChannels.add(tarsync);

                            map.put("msg", "0:操作成功");
                            tarsync.setStatus(200);// 200发布中
                            cntTargetSyncDS.updateCntTargetSync(tarsync);
                            continue;
                        }
                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }

            }

            if (canPubSuccessChannels.size() > 0)
            {
                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int ss = 0; ss < canPubSuccessChannels.size(); ss++)
                {
                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) canPubSuccessChannels.get(ss);
                    int status = tarsync.getStatus();
                    int actiontype = 3;// 取消发布
                    tarsync.setOperresult(70);// 取消同步中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);

                    String xmladdress = "";

                    List list = new ArrayList();
                    list.add(channel);
                    if (list != null && list.size() > 0)
                    {
                        objMap.put("channellist", list);
                        Physicalchannel physicalchannel = new Physicalchannel();
                        physicalchannel.setChannelid(channel.getChannelid());
                        List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                        objMap.put("physicalchannellist", phylist);
                    }

                    cntPlatformSync.setSyncindex(tarsync.getRelateindex());
                    List listplt = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                    if (listplt != null && listplt.size() > 0)
                    {
                        cntPlatformSync = (CntPlatformSync) listplt.get(0);
                    }
                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {

                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                    }
                    int pristatus = 1;

                    insertSyncTask(tarsync.getTargetindex(), xmladdress, channel.getChannelindex(), channel
                            .getChannelid(), 5, actiontype, channel.getCpcontentid(), batchid, pristatus);// 调用同步任务模块DS代码

                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                            "log.channel.mgt", "log.channelcontent.canclepublish",
                            "log.channelcontent.canclepublish.info", CategoryConstant.RESOURCE_OPERATION_SUCCESS);

                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        if (canPubSuccessChannels.size() == 0 || ifail == canPubSuccessChannels.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < canPubSuccessChannels.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    // 发布到网元的方法
    public HashMap publishChannelcontentTar(List<CntTargetSync> tarlist) throws DomainServiceException
    {
        log.debug("publishChannelcontent start ");
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int actiontype = 0;// 发布操作
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List channellist = new ArrayList();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                map.put("msg", "1:获取挂载点失败");
                return map;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                map.put("msg", "1:获取临时区失败");
                return map;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                map.put("msg", "1:获取发布XML文件FTP地址失败");
                return map;
            }

            List pubSuccessChannels = new ArrayList();

            // 判断频道内容是否存在，判断网元状态,不用判断物理频道

            CntTargetSync sync = tarlist.get(0);// 通过网元得到频道信息

            CmsChannel channel = new CmsChannel();
            channel.setChannelindex(sync.getObjectindex());// 获得频道
            channel = getChannel(channel);
            if (channel == null)
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelindex().toString(), "操作失败",
                // "频道不存在");
                // returnInfo.appendOperateInfo(operateInfo);
                map.put("msg", "1:频道不存在");
                return map;
            }
            else if (channel.getStatus() != 0)
            {
                map.put("msg", "1:频道未审核通过");
                return map;
            }
            if (tarlist != null && tarlist.size() > 0)
            {

                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(tarlist.get(0).getRelateindex());
                cntPlatformSync = (CntPlatformSync) cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync).get(0);
                for (int j = 0; j < tarlist.size(); j++)
                {

                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);
                    List cntinitlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (cntinitlist != null && cntinitlist.size() > 0)
                    {

                    }
                    else
                    {
                        map.put("msg", "1:发布数据不存在");
                        return map;
                    }
                    int status = tarsync.getStatus();
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        map.put("msg", "1:目标系统状态不正确");
                        return map;
                    }
                    if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                    {
                        actiontype = 1;// 新增发布
                        tarsync.setOperresult(10);// 新增同步中
                    }
                    else if (tarsync.getStatus() == 500 || tarsync.getStatus() == 300)
                    {
                        actiontype = 2;// 修改发布
                        tarsync.setOperresult(40);// 修改同步中
                    }
                    else
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // "操作失败", "当前状态不能发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        map.put("msg", "1:发布数据状态不正确");
                        // targetSyncStatus[j] = status;
                        continue;// 中断进入下一个循环
                    }
                    String xmladdress = "";

                    List list = new ArrayList();
                    list.add(channel);

                    objMap.put("channellist", list);
                    Physicalchannel physicalchannel = new Physicalchannel();
                    physicalchannel.setChannelid(channel.getChannelid());
                    List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                    objMap.put("physicalchannellist", phylist);

                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {

                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                    }
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());

                    int pristatus = 1;
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, ((CmsChannel) list.get(0)).getChannelindex(),
                            channel.getChannelid(), 5, actiontype, ((CmsChannel) list.get(0)).getCpcontentid(),
                            batchid, pristatus);// 调用同步任务模块DS代码

                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    // targetSyncStatus[j] = tarsync.getStatus();
                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                            "log.channel.mgt", "log.live.publish", "log.live.publish.info",
                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                    // iSuccessed++;
                    //
                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                    // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    // channellist.add(channel);
                    map.put("msg", "0:操作成功");

                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (channellist.size() == 0 || ifail == channellist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < channellist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    public List checkCanPublicList(List tarlist)
    {
        ArrayList array = new ArrayList();
        for (int j = 0; j < tarlist.size(); j++)
        {
            Map<String, List> objMap = new HashMap();
            CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);
            List tarsynclistinit = null;
            try
            {
                tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
            {

            }
            else
            {
                continue;
            }
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(tarsync.getTargetindex());
            List targetlist = null;
            try
            {
                targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (targetlist != null && targetlist.size() > 0)
            {
                targetSystem = (Targetsystem) targetlist.get(0);
            }
            if (targetSystem.getStatus() != 0)
            {
                continue;
            }
            int targettype = targetSystem.getTargettype();
            if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
            {

            }
            else if (tarsync.getStatus() == 500)
            {

            }
            else
            {
                // ifail++;
                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex()
                // .toString(), "操作失败", "当前状态不能发布");
                // returnInfo.appendOperateInfo(operateInfo);
                continue;// 中断进入下一个循环
            }
            array.add(tarsync);
        }
        return array;
    }
    /** 新增同步单个,为提示信息用 */
    public HashMap publishChannelcontentSingle(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int actiontype = 0;// 发布操作
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List channellist = new ArrayList();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                map.put("msg", "1:获取挂载点失败");
                return map;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                map.put("msg", "1:获取临时区失败");
                return map;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                map.put("msg", "1:获取发布XML文件FTP地址失败");
                return map;
            }
            CmsChannel channel = new CmsChannel();

            List pubSuccessChannels = new ArrayList();
            boolean flagbool = false;
            String msg = "";
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                // 判断频道内容是否存在，判断网元状态,不用判断物理频道

                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List pltinitlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltinitlist != null && pltinitlist.size() > 0)
                {
                    channel.setChannelid(((CntPlatformSync) pltinitlist.get(0)).getObjectid());
                    List chalist = channelDS.getChannelByCond(channel);
                    if (chalist != null && chalist.size() > 0)
                    {
                        channel = (CmsChannel) chalist.get(0);
                    }
                    if (channel == null)
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String.valueOf(index++), channel.getChannelindex().toString(),
                        // "操作失败",
                        // "频道不存在");
                        // returnInfo.appendOperateInfo(operateInfo);
                        map.put("msg", "1:频道不存在");
                        return map;
                    }
                    else if (channel.getStatus() != 0)
                    {
                        map.put("msg", "1:频道未审核通过");
                        return map;
                    }
                }
                else
                {
                    map.put("msg", "1:发布数据不存在");
                    return map;
                }

                if (cntPlatformSync.getStatus() == 200 || cntPlatformSync.getStatus() == 300)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex().toString(),
                            "操作失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
                else
                {
                    List tarlist = new ArrayList();
                    CntTargetSync tarsync = new CntTargetSync();
                    tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                    tarsync.setObjecttype(5);
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarlist != null && tarlist.size() > 0)
                    {
                        tarlist = checkCanPublicList(tarlist);
                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                .toString());
                        for (int j = 0; j < tarlist.size(); j++)
                        {

                            Map<String, List> objMap = new HashMap();
                            tarsync = (CntTargetSync) tarlist.get(j);
                            List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                            {

                            }
                            else
                            {
                                msg = msg + " 发布数据不存在";

                                continue;
                            }
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(tarsync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getStatus() != 0)
                            {
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                continue;
                            }
                            int targettype = targetSystem.getTargettype();
                            if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                            {
                                actiontype = 1;// 新增发布
                                tarsync.setOperresult(10);// 新增同步中
                            }
                            else if (tarsync.getStatus() == 500)
                            {
                                actiontype = 2;// 修改发布
                                tarsync.setOperresult(40);// 修改同步中
                            }
                            else
                            {
                                // ifail++;
                                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex()
                                // .toString(), "操作失败", "当前状态不能发布");
                                // returnInfo.appendOperateInfo(operateInfo);
                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                                continue;// 中断进入下一个循环
                            }
                            String xmladdress = "";

                            List list = new ArrayList();
                            list.add(channel);
                            if (list != null && list.size() > 0)
                            {
                                channel = (CmsChannel) list.get(0);
                                objMap.put("channellist", list);
                                Physicalchannel physicalchannel = new Physicalchannel();
                                physicalchannel.setChannelid(channel.getChannelid());
                                List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                                objMap.put("physicalchannellist", phylist);
                            }

                            if (cntPlatformSync.getPlatform() == 2)
                            {// 3.0平台,1个平台生成1个xml
                                xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                            }
                            else
                            {
                              //如果是2.0平台就每个网元生成一个不同的batchid
                                batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                                xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                            }
                            int status = 1;
                            // bms和egp的情况且有别的网元的情况,status可为0或1
                            if (((targettype == 1) || (targettype == 2)) && (tarlist.size() > 0))
                            {
                                for (int count = 0; count < tarlist.size(); count++)
                                {
                                    CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                    if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                    {
                                        Targetsystem tsnew = new Targetsystem();
                                        tsnew.setTargetindex(cs.getTargetindex());
                                        List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                        if (target != null && target.size() > 0)
                                        {
                                            tsnew = (Targetsystem) target.get(0);
                                        }
                                        if ((tsnew.getTargettype() == 3))
                                        {// cdn
                                            status = 0;
                                        }
                                        else if (tsnew.getTargettype() == 1)
                                        {// bms
                                            status = 0;
                                        }

                                    }
                                }
                            }
                            insertSyncTask(tarsync.getTargetindex(), xmladdress, channel.getChannelindex(), channel
                                    .getChannelid(), 5, actiontype, channel.getCpcontentid(), batchid, status);// 调用同步任务模块DS代码

                            tarsync.setStatus(200);// 200发布中
                            cntTargetSyncDS.updateCntTargetSync(tarsync);
                            flagbool = true;
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":操作成功";
                            // iSuccessed++;
                            //
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                            // returnInfo.appendOperateInfo(operateInfo);
                            channellist.add(channel);
                            CommonLogUtil.insertOperatorLog(
                                    tarsync.getObjectid() + ", " + targetSystem.getTargetid(), "log.channel.mgt",
                                    "log.live.publish", "log.live.publish.info",
                                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        CntTargetSync finaltarget = new CntTargetSync();
                        finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                        List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                        if (finaltargetlist != null && finaltargetlist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[finaltargetlist.size()];
                            for (int ii = 0; ii < finaltargetlist.size(); ii++)
                            {
                                targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                            }
                            int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                            int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                            if (originalstatus != statussync)
                            {
                                cntPlatformSync.setStatus(statussync);
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }

                }
                if (flagbool == true)
                {
                    // iSuccessed++;
                    //
                    // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                    // "成功", msg);
                    // returnInfo.appendOperateInfo(operateInfo);
                    msg = "0:" + msg;
                    map.put("msg", msg);

                }
                else
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                    // "失败", msg);
                    // returnInfo.appendOperateInfo(operateInfo);
                    msg = "1:" + msg;
                    map.put("msg", msg);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (channellist.size() == 0 || ifail == channellist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < channellist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        // returnInfo.setFlag(flag);
        // returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    /** 新增同步 */
    public String publishChannelcontent(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int actiontype = 0;// 发布操作
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List channellist = new ArrayList();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                return "1:获取挂载点失败";
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                return "1:获取临时区失败";
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1:获取发布XML文件FTP地址失败";
            }

            List pubSuccessChannels = new ArrayList();
            boolean flagbool = false;

            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                // 判断频道内容是否存在，判断网元状态,不用判断物理频道
                CmsChannel channel = new CmsChannel();
                String msg = "";
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List pltinitlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltinitlist != null && pltinitlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltinitlist.get(0);
                    channel.setChannelid(cntPlatformSync.getObjectid());
                    List chalist = channelDS.getChannelByCond(channel);
                    if (chalist != null && chalist.size() > 0)
                    {
                        channel = (CmsChannel) chalist.get(0);
                    }
                    if (channel == null)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid(), "操作失败",
                                "频道不存在");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;// 中断进入下一个循环
                    }
                    else if (channel.getStatus() != 0)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid(), "操作失败",
                                "频道未审核通过");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;// 中断进入下一个循环
                    }
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "操作失败", "发布数据不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
                if (cntPlatformSync.getStatus() == 200 || cntPlatformSync.getStatus() == 300)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "操作失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
                else
                {
                    List tarlist = new ArrayList();
                    CntTargetSync tarsync = new CntTargetSync();
                    tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                    tarsync.setObjecttype(5);
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarlist != null && tarlist.size() > 0)
                    {
                        tarlist = checkCanPublicList(tarlist);
                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                .toString());
                        for (int j = 0; j < tarlist.size(); j++)
                        {

                            Map<String, List> objMap = new HashMap();
                            tarsync = (CntTargetSync) tarlist.get(j);
                            List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                            if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                            {

                            }
                            else
                            {
                                msg = msg + " 发布数据不存在";

                                continue;
                            }
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(tarsync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getStatus() != 0)
                            {
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                continue;
                            }
                            int targettype = targetSystem.getTargettype();
                            if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                            {
                                actiontype = 1;// 新增发布
                                tarsync.setOperresult(10);// 新增同步中
                            }
                            else if (tarsync.getStatus() == 500)
                            {
                                actiontype = 2;// 修改发布
                                tarsync.setOperresult(40);// 修改同步中
                            }
                            else
                            {
                                // ifail++;
                                // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex()
                                // .toString(), "操作失败", "当前状态不能发布");
                                // returnInfo.appendOperateInfo(operateInfo);
                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                                continue;// 中断进入下一个循环
                            }
                            String xmladdress = "";

                            List list = new ArrayList();
                            list.add(channel);
                            if (list != null && list.size() > 0)
                            {
                                channel = (CmsChannel) list.get(0);
                                objMap.put("channellist", list);
                                Physicalchannel physicalchannel = new Physicalchannel();
                                physicalchannel.setChannelid(channel.getChannelid());
                                List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                                objMap.put("physicalchannellist", phylist);
                            }

                            if (cntPlatformSync.getPlatform() == 2)
                            {// 3.0平台,1个平台生成1个xml
                                xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                            }
                            else
                            {
                              //如果是2.0平台就每个网元生成一个不同的batchid
                                batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                                xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                            }
                            int status = 1;
                            // bms和egp的情况且有别的网元的情况,status可为0或1
                            if (((targettype == 1) || (targettype == 2)) && (tarlist.size() > 0))
                            {
                                for (int count = 0; count < tarlist.size(); count++)
                                {
                                    CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                    if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                    {
                                        Targetsystem tsnew = new Targetsystem();
                                        tsnew.setTargetindex(cs.getTargetindex());
                                        List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                        if (target != null && target.size() > 0)
                                        {
                                            tsnew = (Targetsystem) target.get(0);
                                        }
                                        if ((tsnew.getTargettype() == 3))
                                        {// cdn
                                            status = 0;
                                        }
                                        else if (tsnew.getTargettype() == 1)
                                        {// bms
                                            status = 0;
                                        }

                                    }
                                }
                            }
                            insertSyncTask(tarsync.getTargetindex(), xmladdress, channel.getChannelindex(), channel
                                    .getChannelid(), 5, actiontype, channel.getCpcontentid(), batchid, status);// 调用同步任务模块DS代码

                            tarsync.setStatus(200);// 200发布中
                            cntTargetSyncDS.updateCntTargetSync(tarsync);
                            flagbool = true;
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":操作成功";
                            // iSuccessed++;
                            //
                            // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                            // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                            // returnInfo.appendOperateInfo(operateInfo);
                            channellist.add(channel);
                            CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", " + targetSystem.getTargetid(),
                                    "log.channel.mgt", "log.live.publish", "log.live.publish.info",
                                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        CntTargetSync finaltarget = new CntTargetSync();
                        finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                        List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                        if (finaltargetlist != null && finaltargetlist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[finaltargetlist.size()];
                            for (int ii = 0; ii < finaltargetlist.size(); ii++)
                            {
                                targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                            }
                            int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                            int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                            if (originalstatus != statussync)
                            {
                                cntPlatformSync.setStatus(statussync);
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }

                }
                if (flagbool == true)
                {
                    iSuccessed++;

                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "成功", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    /** 删除同步 channelList[]频道集合 单个为修改批量提示信息用 */
    public HashMap canclePublishChannelSingle(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";

        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List canPubSuccessChannels = new ArrayList();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                map.put("msg", "1:获取挂载点失败");
                return map;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                map.put("msg", "1:获取临时区失败");
                return map;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                map.put("msg", "1:获取发布XML文件FTP地址失败");
                return map;
            }
            boolean flagbool = false;
            String msg = "";
            List tarlist = new ArrayList();
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                // 判断频道内容是否存在
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                CmsChannel channel = new CmsChannel();
                channel.setChannelindex(cntPlatformSync.getObjectindex());// 获得频道
                CmsChannel rtnChannel = getChannel(channel);
                if (rtnChannel == null)
                {

                    map.put("msg", "1:频道不存在");

                    return map;
                }
                else if (rtnChannel.getStatus() != 0)
                {// 未审核通过

                    map.put("msg", "1:频道未审核通过");

                    return map;
                }
                else
                {
                    int statusinit = cntPlatformSync.getStatus();
                    if (statusinit == 200 || statusinit == 0 || statusinit == 400)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++),
                                cntPlatformSync.getObjectid().toString(), "失败", "发布数据状态不正确");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    CntTargetSync tarsync = new CntTargetSync();
                    tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);

                    int pritargettype = -1;
                    if (tarlist != null && tarlist.size() > 0)
                    {

                        for (int count = 0; count < tarlist.size(); count++)
                        {
                            // 找出优先级最高的网元

                            CntTargetSync cntTargetSync = (CntTargetSync) tarlist.get(count);
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getTargettype() == 2)
                            {
                                pritargettype = 2;
                            }
                            else if ((targetSystem.getTargettype() == 1) && (pritargettype != 2))
                            {
                                pritargettype = 1;
                            }
                            else if ((targetSystem.getTargettype() != 1) && (pritargettype != 2)
                                    && (pritargettype != 1))
                            {
                                pritargettype = targetSystem.getTargettype();
                            }
                        }

                        boolean priflagbool = false;
                        for (int sed = 0; sed < tarlist.size(); sed++)
                        {

                            tarsync = (CntTargetSync) tarlist.get(sed);

                            int statusbegin = tarsync.getStatus();
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(tarsync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }

                            if (targetSystem.getTargettype() == pritargettype)
                            {

                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                    continue;

                                }
                                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 200
                                        || tarsync.getStatus() == 400)
                                {// 频道在此网元上状态不正确

                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                                    continue;

                                }
                                else
                                {

                                    String objid = cntPlatformSync.getObjectid();// 栏目id
                                    boolean flagele = false;
                                    CntTargetSync sync = new CntTargetSync();
                                    sync.setElementid(objid);
                                    sync.setObjecttype(23);// 服务与频道 关联
                                    sync.setTargetindex(tarsync.getTargetindex());
                                    List synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                                    if (synclist != null && synclist.size() > 0)
                                    {
                                        for (int num = 0; num < synclist.size(); num++)
                                        {
                                            sync = (CntTargetSync) synclist.get(num);
                                            int status = sync.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                flagele = true;
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与服务的关联关系";

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncparent = new CntTargetSync();
                                    syncparent.setElementid(objid);
                                    syncparent.setObjecttype(28);// 频道与栏目
                                    syncparent.setTargetindex(tarsync.getTargetindex());
                                    List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                    if (syncparentlist != null && syncparentlist.size() > 0)
                                    {
                                        for (int num = 0; num < syncparentlist.size(); num++)
                                        {
                                            syncparent = (CntTargetSync) syncparentlist.get(num);
                                            int status = syncparent.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与栏目的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncpic = new CntTargetSync();
                                    syncpic.setElementid(objid);
                                    syncpic.setObjecttype(39);// 频道与海报
                                    syncpic.setTargetindex(tarsync.getTargetindex());
                                    List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                    if (syncpiclist != null && syncpiclist.size() > 0)
                                    {
                                        for (int num = 0; num < syncpiclist.size(); num++)
                                        {
                                            syncpic = (CntTargetSync) syncpiclist.get(num);
                                            int status = syncpic.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与海报的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }
                                    if (flagele == true)
                                    {

                                        continue;
                                    }

                                    // 如果有已发布的节目单不能取消发布,这个需改到到网元下去查状态
                                    CmsSchedule cmsSchedule = new CmsSchedule();
                                    cmsSchedule.setChannelid(rtnChannel.getChannelid());
                                    List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                                    if (listSchedule != null && listSchedule.size() > 0)
                                    {
                                        int schenum = 0;
                                        for (int k = 0; k < listSchedule.size(); k++)
                                        {
                                            cmsSchedule = (CmsSchedule) listSchedule.get(k);
                                            CntTargetSync tarsyncschedule = new CntTargetSync();
                                            tarsyncschedule.setObjectid(cmsSchedule.getScheduleid());
                                            tarsyncschedule.setObjecttype(6);
                                            tarsyncschedule.setTargetindex(tarsync.getTargetindex());
                                            List list = cntTargetSyncDS.getCntTargetSyncByCond(tarsyncschedule);
                                            if (list != null && list.size() > 0)
                                            {
                                                for (int count = 0; count < list.size(); count++)
                                                {
                                                    tarsyncschedule = (CntTargetSync) list.get(count);
                                                    if (tarsyncschedule.getStatus() == 200
                                                            || tarsyncschedule.getStatus() == 300
                                                            || tarsyncschedule.getStatus() == 500)
                                                    {
                                                        schenum++;
                                                    }
                                                }

                                            }
                                        }
                                        if (schenum > 0)
                                        {

                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "频道下节目单已发布";
                                            continue;
                                        }
                                    }

                                    priflagbool = true;

                                }
                            }
                        }

                        if (priflagbool == true)
                        {

                            for (int j = 0; j < tarlist.size(); j++)
                            {

                                tarsync = (CntTargetSync) tarlist.get(j);
                                List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                                if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                                {

                                }
                                else
                                {
                                    msg = msg + " 发布数据不存在";

                                    continue;
                                }
                                int statusbegin = tarsync.getStatus();
                                Targetsystem targetSystem = new Targetsystem();
                                targetSystem.setTargetindex(tarsync.getTargetindex());
                                List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                                if (targetlist != null && targetlist.size() > 0)
                                {
                                    targetSystem = (Targetsystem) targetlist.get(0);
                                }

                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";

                                    continue;
                                }
                                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 200
                                        || tarsync.getStatus() == 400)
                                {// 频道在此网元上状态不正确

                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";

                                    continue;

                                }
                                else
                                {

                                    String objid = cntPlatformSync.getObjectid();// 栏目id
                                    boolean flagele = false;
                                    CntTargetSync sync = new CntTargetSync();
                                    sync.setElementid(objid);
                                    sync.setObjecttype(23);// 服务与频道 关联
                                    sync.setTargetindex(tarsync.getTargetindex());
                                    List synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                                    if (synclist != null && synclist.size() > 0)
                                    {
                                        for (int num = 0; num < synclist.size(); num++)
                                        {
                                            sync = (CntTargetSync) synclist.get(num);
                                            int status = sync.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                flagele = true;
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与服务的关联关系";

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncparent = new CntTargetSync();
                                    syncparent.setElementid(objid);
                                    syncparent.setObjecttype(28);// 频道与栏目
                                    syncparent.setTargetindex(tarsync.getTargetindex());
                                    List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                    if (syncparentlist != null && syncparentlist.size() > 0)
                                    {
                                        for (int num = 0; num < syncparentlist.size(); num++)
                                        {
                                            syncparent = (CntTargetSync) syncparentlist.get(num);
                                            int status = syncparent.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与栏目的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncpic = new CntTargetSync();
                                    syncpic.setElementid(objid);
                                    syncpic.setObjecttype(39);// 频道与海报
                                    syncpic.setTargetindex(tarsync.getTargetindex());
                                    List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                    if (syncpiclist != null && syncpiclist.size() > 0)
                                    {
                                        for (int num = 0; num < syncpiclist.size(); num++)
                                        {
                                            syncpic = (CntTargetSync) syncpiclist.get(num);
                                            int status = syncpic.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与海报的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }

                                    if (flagele == true)
                                    {

                                        continue;
                                    }
                                    // 如果有已发布的节目单不能取消发布,这个需改到到网元下去查状态
                                    CmsSchedule cmsSchedule = new CmsSchedule();
                                    cmsSchedule.setChannelid(rtnChannel.getChannelid());
                                    List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                                    if (listSchedule != null && listSchedule.size() > 0)
                                    {
                                        int schenum = 0;
                                        for (int k = 0; k < listSchedule.size(); k++)
                                        {
                                            cmsSchedule = (CmsSchedule) listSchedule.get(k);
                                            CntTargetSync tarsyncschedule = new CntTargetSync();
                                            tarsyncschedule.setObjectid(cmsSchedule.getScheduleid());
                                            tarsyncschedule.setObjecttype(6);
                                            tarsyncschedule.setTargetindex(tarsync.getTargetindex());
                                            List list = cntTargetSyncDS.getCntTargetSyncByCond(tarsyncschedule);
                                            if (list != null && list.size() > 0)
                                            {
                                                for (int count = 0; count < list.size(); count++)
                                                {
                                                    tarsyncschedule = (CntTargetSync) list.get(count);
                                                    if (tarsyncschedule.getStatus() == 200
                                                            || tarsyncschedule.getStatus() == 300
                                                            || tarsyncschedule.getStatus() == 500)
                                                    {
                                                        schenum++;
                                                    }
                                                }

                                            }
                                        }
                                        if (schenum > 0)
                                        {

                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "频道下节目单已发布";

                                            continue;
                                        }
                                    }

                                    flagbool = true;
                                    canPubSuccessChannels.add(tarsync);
                                    msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";
                                    tarsync.setStatus(200);// 200发布中
                                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", "
                                            + targetSystem.getTargetid(), "log.channel.mgt",
                                            "log.channelcontent.canclepublish",
                                            "log.channelcontent.canclepublish.info",
                                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);

                                    continue;
                                }
                            }

                            CntTargetSync finaltarget = new CntTargetSync();
                            finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                            if (finaltargetlist != null && finaltargetlist.size() > 0)
                            {
                                int[] targetSyncStatus = new int[finaltargetlist.size()];
                                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                                {
                                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                                }
                                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                                int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                                if (originalstatus != statussync)
                                {
                                    cntPlatformSync.setStatus(statussync);
                                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                                }
                            }
                        }
                    }

                }
                if (flagbool == true)
                {
                    msg = "0:" + msg;
                    map.put("msg", msg);
                }
                else
                {
                    msg = "1:" + msg;
                    map.put("msg", msg);
                }
            }

            if (canPubSuccessChannels.size() > 0)
            {

                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int ss = 0; ss < canPubSuccessChannels.size(); ss++)
                {
                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) canPubSuccessChannels.get(ss);
                    int status = tarsync.getStatus();

                    int actiontype = 3;// 取消发布
                    tarsync.setOperresult(70);// 取消同步中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    String xmladdress = "";
                    CmsChannel channel = new CmsChannel();
                    channel.setChannelid(tarsync.getObjectid());
                    List list = channelDS.getChannelByCond(channel);
                    if (list != null && list.size() > 0)
                    {
                        objMap.put("channellist", list);
                        Physicalchannel physicalchannel = new Physicalchannel();
                        physicalchannel.setChannelid(channel.getChannelid());
                        List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                        objMap.put("physicalchannellist", phylist);
                    }
                    CntPlatformSync cntPlatformSync = new CntPlatformSync();
                    cntPlatformSync.setSyncindex(tarsync.getRelateindex());
                    List listplt = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                    if (listplt != null && listplt.size() > 0)
                    {
                        cntPlatformSync = (CntPlatformSync) listplt.get(0);
                    }
                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {
                      //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                    }
                    int pristatus = 1;
                    // bms和egp的情况且有别的网元的情况,status可为0或1
                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                            && (canPubSuccessChannels.size() > 0))
                    {
                        for (int count = 0; count < canPubSuccessChannels.size(); count++)
                        {
                            CntTargetSync cs = (CntTargetSync) canPubSuccessChannels.get(count);
                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                            {
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if (target != null && target.size() > 0)
                                {
                                    tsnew = (Targetsystem) target.get(0);
                                }
                                if ((tsnew.getTargettype() == 2))
                                {// cdn
                                    pristatus = 0;
                                }
                                else if (tsnew.getTargettype() == 1)
                                {// bms
                                    pristatus = 0;
                                }

                            }
                        }
                    }
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, ((CmsChannel) list.get(0)).getChannelindex(),
                            channel.getChannelid(), 5, actiontype, ((CmsChannel) list.get(0)).getCpcontentid(),
                            batchid, pristatus);// 调用同步任务模块DS代码

                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }

        log.debug("canclePuhlishProgramtype end...");
        return map;
    }

    /** 删除同步 channelList[]频道集合 */
    public String canclePublishChannel(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";

        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;

        int index = 1;
        List canPubSuccessChannels = new ArrayList();
        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                return "1:获取挂载点失败";
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                return "1:获取临时区失败";
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1:获取发布XML文件FTP地址失败";
            }

            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                // 判断频道内容是否存在
                boolean flagbool = false;
                String msg = "";
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "发布数据不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                CmsChannel channel = new CmsChannel();
                channel.setChannelindex(cntPlatformSync.getObjectindex());// 获得频道
                CmsChannel rtnChannel = getChannel(channel);
                if (rtnChannel == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "频道不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else if (rtnChannel.getStatus() != 0)
                {// 未审核通过

                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "频道未审核通过");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    int statusinit = cntPlatformSync.getStatus();
                    if (statusinit == 200 || statusinit == 0 || statusinit == 400)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++),
                                cntPlatformSync.getObjectid().toString(), "失败", "发布数据状态不正确");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    List tarlist = new ArrayList();
                    CntTargetSync tarsync = new CntTargetSync();
                    tarsync.setRelateindex(cntPlatformSync.getSyncindex());
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    int pritargettype = -1;
                    if (tarlist != null && tarlist.size() > 0)
                    {

                        for (int count = 0; count < tarlist.size(); count++)
                        {
                            // 找出优先级最高的网元

                            CntTargetSync cntTargetSync = (CntTargetSync) tarlist.get(count);
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getTargettype() == 2)
                            {
                                pritargettype = 2;
                            }
                            else if ((targetSystem.getTargettype() == 1) && (pritargettype != 2))
                            {
                                pritargettype = 1;
                            }
                            else if ((targetSystem.getTargettype() != 1) && (pritargettype != 2)
                                    && (pritargettype != 1))
                            {
                                pritargettype = targetSystem.getTargettype();
                            }
                        }

                        boolean priflagbool = false;
                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        for (int sed = 0; sed < tarlist.size(); sed++)
                        {                          
                            tarsync = (CntTargetSync) tarlist.get(sed);

                            int statusbegin = tarsync.getStatus();
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(tarsync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }

                            if (targetSystem.getTargettype() == pritargettype)
                            {

                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                    continue;

                                }
                                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 200
                                        || tarsync.getStatus() == 400)
                                {// 频道在此网元上状态不正确

                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                                    continue;

                                }
                                else
                                {

                                    String objid = cntPlatformSync.getObjectid();// 栏目id
                                    boolean flagele = false;
                                    CntTargetSync sync = new CntTargetSync();
                                    sync.setElementid(objid);
                                    sync.setObjecttype(23);// 服务与频道 关联
                                    sync.setTargetindex(tarsync.getTargetindex());
                                    List synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                                    if (synclist != null && synclist.size() > 0)
                                    {
                                        for (int num = 0; num < synclist.size(); num++)
                                        {
                                            sync = (CntTargetSync) synclist.get(num);
                                            int status = sync.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                flagele = true;
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与服务的关联关系";

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncparent = new CntTargetSync();
                                    syncparent.setElementid(objid);
                                    syncparent.setObjecttype(28);// 频道与栏目
                                    syncparent.setTargetindex(tarsync.getTargetindex());
                                    List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                    if (syncparentlist != null && syncparentlist.size() > 0)
                                    {
                                        for (int num = 0; num < syncparentlist.size(); num++)
                                        {
                                            syncparent = (CntTargetSync) syncparentlist.get(num);
                                            int status = syncparent.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与栏目的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncpic = new CntTargetSync();
                                    syncpic.setElementid(objid);
                                    syncpic.setObjecttype(39);// 频道与海报
                                    syncpic.setTargetindex(tarsync.getTargetindex());
                                    List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                    if (syncpiclist != null && syncpiclist.size() > 0)
                                    {
                                        for (int num = 0; num < syncpiclist.size(); num++)
                                        {
                                            syncpic = (CntTargetSync) syncpiclist.get(num);
                                            int status = syncpic.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {

                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与海报的关联关系";
                                                flagele = true;

                                                break;
                                            }

                                        }
                                    }
                                    if (flagele == true)
                                    {

                                        continue;
                                    }

                                    // 如果有已发布的节目单不能取消发布,这个需改到到网元下去查状态
                                    CmsSchedule cmsSchedule = new CmsSchedule();
                                    cmsSchedule.setChannelid(rtnChannel.getChannelid());
                                    List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                                    if (listSchedule != null && listSchedule.size() > 0)
                                    {
                                        int schenum = 0;
                                        for (int k = 0; k < listSchedule.size(); k++)
                                        {
                                            cmsSchedule = (CmsSchedule) listSchedule.get(k);
                                            CntTargetSync tarsyncschedule = new CntTargetSync();
                                            tarsyncschedule.setObjectid(cmsSchedule.getScheduleid());
                                            tarsyncschedule.setObjecttype(6);
                                            tarsyncschedule.setTargetindex(tarsync.getTargetindex());
                                            List list = cntTargetSyncDS.getCntTargetSyncByCond(tarsyncschedule);
                                            if (list != null && list.size() > 0)
                                            {
                                                for (int count = 0; count < list.size(); count++)
                                                {
                                                    tarsyncschedule = (CntTargetSync) list.get(count);
                                                    if (tarsyncschedule.getStatus() == 200
                                                            || tarsyncschedule.getStatus() == 300
                                                            || tarsyncschedule.getStatus() == 500)
                                                    {
                                                        schenum++;
                                                    }
                                                }

                                            }
                                        }
                                        if (schenum > 0)
                                        {

                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "频道下节目单已发布";
                                            continue;
                                        }
                                    }

                                    priflagbool = true;

                                }
                            }
                        }

                        if (priflagbool == true)
                        {
                            for (int j = 0; j < tarlist.size(); j++)
                            {

                                tarsync = (CntTargetSync) tarlist.get(j);
                                List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                                if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                                {

                                }
                                else
                                {
                                    msg = msg + " 发布数据不存在";

                                    continue;
                                }
                                int statusbegin = tarsync.getStatus();
                                Targetsystem targetSystem = new Targetsystem();
                                targetSystem.setTargetindex(tarsync.getTargetindex());
                                List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                                if (targetlist != null && targetlist.size() > 0)
                                {
                                    targetSystem = (Targetsystem) targetlist.get(0);
                                }
                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                    continue;
                                }
                                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 200
                                        || tarsync.getStatus() == 400)
                                {// 频道在此网元上状态不正确
                                 // ifail++;
                                 // operateInfo = new OperateInfo(String.valueOf(index++), rtnChannel.getChannelid(),
                                 // "操作失败", "频道在此网元上的状态不能取消发布");
                                 // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + "频道的状态不能取消发布";
                                    continue;

                                }
                                else
                                {

                                    String objid = cntPlatformSync.getObjectid();// 栏目id
                                    boolean flagele = false;
                                    CntTargetSync sync = new CntTargetSync();
                                    sync.setElementid(objid);
                                    sync.setObjecttype(23);// 服务与频道 关联
                                    sync.setTargetindex(tarsync.getTargetindex());
                                    List synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                                    if (synclist != null && synclist.size() > 0)
                                    {
                                        for (int num = 0; num < synclist.size(); num++)
                                        {
                                            sync = (CntTargetSync) synclist.get(num);
                                            int status = sync.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {
                                                // ifail++;
                                                // operateInfo = new OperateInfo(String.valueOf(index++),
                                                // cntPlatformSync
                                                // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                                // "此频道与服务的 mapping关系已发布不能取消同步");
                                                // returnInfo.appendOperateInfo(operateInfo);
                                                flagele = true;
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与服务的关联关系";
                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncparent = new CntTargetSync();
                                    syncparent.setElementid(objid);
                                    syncparent.setObjecttype(28);// 频道与栏目
                                    syncparent.setTargetindex(tarsync.getTargetindex());
                                    List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                    if (syncparentlist != null && syncparentlist.size() > 0)
                                    {
                                        for (int num = 0; num < syncparentlist.size(); num++)
                                        {
                                            syncparent = (CntTargetSync) syncparentlist.get(num);
                                            int status = syncparent.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {
                                                // ifail++;
                                                // operateInfo = new OperateInfo(String.valueOf(index++),
                                                // cntPlatformSync
                                                // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                                // "此频道与栏目的 mapping关系已发布不能取消同步");
                                                // returnInfo.appendOperateInfo(operateInfo);
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与栏目的关联关系";
                                                flagele = true;
                                                break;
                                            }

                                        }
                                    }

                                    CntTargetSync syncpic = new CntTargetSync();
                                    syncpic.setElementid(objid);
                                    syncpic.setObjecttype(39);// 频道与海报
                                    syncpic.setTargetindex(tarsync.getTargetindex());
                                    List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                    if (syncpiclist != null && syncpiclist.size() > 0)
                                    {
                                        for (int num = 0; num < syncpiclist.size(); num++)
                                        {
                                            syncpic = (CntTargetSync) syncpiclist.get(num);
                                            int status = syncpic.getStatus();
                                            if (status == 200 || status == 300 || status == 500)
                                            {
                                                // ifail++;
                                                // operateInfo = new OperateInfo(String.valueOf(index++),
                                                // cntPlatformSync
                                                // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                                // "此频道与海报的 mapping关系已发布不能取消同步");
                                                // returnInfo.appendOperateInfo(operateInfo);
                                                msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布有频道与海报的关联关系";
                                                flagele = true;
                                                break;
                                            }

                                        }
                                    }

                                    if (flagele == true)
                                    {
                                        continue;
                                    }
                                    // 如果有已发布的节目单不能取消发布,这个需改到到网元下去查状态
                                    CmsSchedule cmsSchedule = new CmsSchedule();
                                    cmsSchedule.setChannelid(rtnChannel.getChannelid());
                                    List listSchedule = cmsScheduleDS.getCmsScheduleByCond(cmsSchedule);
                                    if (listSchedule != null && listSchedule.size() > 0)
                                    {
                                        int schenum = 0;
                                        for (int k = 0; k < listSchedule.size(); k++)
                                        {
                                            cmsSchedule = (CmsSchedule) listSchedule.get(k);
                                            CntTargetSync tarsyncschedule = new CntTargetSync();
                                            tarsyncschedule.setObjectid(cmsSchedule.getScheduleid());
                                            tarsyncschedule.setObjecttype(6);
                                            tarsyncschedule.setTargetindex(tarsync.getTargetindex());
                                            List list = cntTargetSyncDS.getCntTargetSyncByCond(tarsyncschedule);
                                            if (list != null && list.size() > 0)
                                            {
                                                for (int count = 0; count < list.size(); count++)
                                                {
                                                    tarsyncschedule = (CntTargetSync) list.get(count);
                                                    if (tarsyncschedule.getStatus() == 200
                                                            || tarsyncschedule.getStatus() == 300
                                                            || tarsyncschedule.getStatus() == 500)
                                                    {
                                                        schenum++;
                                                    }
                                                }

                                            }
                                        }
                                        if (schenum > 0)
                                        {
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), rtnChannel
                                            // .getChannelid(), "操作失败", "频道下节目单的状态不正确");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "频道下节目单已发布";
                                            continue;
                                        }
                                    }
                                    // 如果有已发布的海报，就不能取消同步

                                    flagbool = true;
                                    
                                    tarsync.setReserve01(batchid);
                                    
                                    canPubSuccessChannels.add(tarsync);
                                    tarsync.setStatus(200);// 200发布中
                                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                                    msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";
                                    // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex()
                                    // .toString(), "操作成功", "取消发布成功");
                                    // returnInfo.appendOperateInfo(operateInfo);

                                    CommonLogUtil.insertOperatorLog(tarsync.getObjectid() + ", "
                                            + targetSystem.getTargetid(), "log.channel.mgt",
                                            "log.channelcontent.canclepublish",
                                            "log.channelcontent.canclepublish.info",
                                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                                    continue;
                                    // }
                                }
                            }

                            CntTargetSync finaltarget = new CntTargetSync();
                            finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                            if (finaltargetlist != null && finaltargetlist.size() > 0)
                            {
                                int[] targetSyncStatus = new int[finaltargetlist.size()];
                                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                                {
                                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                                }
                                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                                int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                                if (originalstatus != statussync)
                                {
                                    cntPlatformSync.setStatus(statussync);
                                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                                }
                            }
                        }
                    }

                }
                if (flagbool == true)
                {
                    iSuccessed++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "成功", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
            }
            if (canPubSuccessChannels.size() > 0)
            {
                //Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                
                for (int ss = 0; ss < canPubSuccessChannels.size(); ss++)
                {
                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) canPubSuccessChannels.get(ss);                    
                    Long batchid  = tarsync.getReserve01();
                    tarsync.setReserve01(null);
                    int status = tarsync.getStatus();

                    int actiontype = 3;// 取消发布
                    tarsync.setOperresult(70);// 取消同步中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    String xmladdress = "";
                    CmsChannel channel = new CmsChannel();
                    channel.setChannelid(tarsync.getObjectid());
                    List list = channelDS.getChannelByCond(channel);
                    if (list != null && list.size() > 0)
                    {
                        objMap.put("channellist", list);
                        Physicalchannel physicalchannel = new Physicalchannel();
                        physicalchannel.setChannelid(channel.getChannelid());
                        List phylist = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                        objMap.put("physicalchannellist", phylist);
                    }
                    CntPlatformSync cntPlatformSync = new CntPlatformSync();
                    cntPlatformSync.setSyncindex(tarsync.getRelateindex());
                    List listplt = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                    if (listplt != null && listplt.size() > 0)
                    {
                        cntPlatformSync = (CntPlatformSync) listplt.get(0);
                    }
                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {
                      //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 5, actiontype);// 文广1是regist2是栏目
                    }
                    int pristatus = 1;
                    // bms和egp的情况且有别的网元的情况,status可为0或1
                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                            && (canPubSuccessChannels.size() > 0))
                    {
                        for (int count = 0; count < canPubSuccessChannels.size(); count++)
                        {
                            CntTargetSync cs = (CntTargetSync) canPubSuccessChannels.get(count);
                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                            {
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if (target != null && target.size() > 0)
                                {
                                    tsnew = (Targetsystem) target.get(0);
                                }
                                if ((tsnew.getTargettype() == 2))
                                {// cdn
                                    pristatus = 0;
                                }
                                else if (tsnew.getTargettype() == 1)
                                {// bms
                                    pristatus = 0;
                                }

                            }
                        }
                    }
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, ((CmsChannel) list.get(0)).getChannelindex(),
                            channel.getChannelid(), 5, actiontype, ((CmsChannel) list.get(0)).getCpcontentid(),
                            batchid, pristatus);// 调用同步任务模块DS代码

                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    // 获取临时区目录
    private String getTempAreaPath() throws DomainServiceException
    {
        log.debug("getTempAreaPath  start");
        String stroageareaPath = "";
        // 获取临时区路径
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            stroageareaPath = storageareaLs.getAddress(TEMPAREA_ID);// 临时区
            if (null == stroageareaPath || "".equals(stroageareaPath) || (TEMPAREA_ERRORSiGN).equals(stroageareaPath))
            {// 获取临时区地址失败
                return "";
            }
        }
        catch (Exception e)
        {
            log.error("Error occured in getTempAreaPath method");
            throw new DomainServiceException(e);
        }

        log.debug("getTempAreaPath  end");
        return stroageareaPath;
    }

    // 频道关联平台map方法
    public String preparetopublishForMap(Targetsystem tar, long objindex, String objid, int objtype, String elementid,
            String parentid) throws DomainServiceException
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        cntPlatformSync.setElementid(elementid);
        cntPlatformSync.setParentid(parentid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTargetForMap(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex, elementid,
                parentid);
        return operdesc;
    }

    public String preparetopublish(Targetsystem tar, long objindex, String objid, int objtype)
            throws DomainServiceException
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTarget(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex);
        return operdesc;
    }

    public String publishToTargetForMap(Long targetindex, long objindex, String objid, int objtype, Long relateindex,
            String elementid, String parentid) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);
        cntTargetSync.setElementid(elementid);
        cntTargetSync.setParentid(parentid);

        CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
        categoryChannelMap.setChannelid(objid);
        List listmap = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
        if (listmap != null && listmap.size() > 0)
        {
            for (int i = 0; i < listmap.size(); i++)
            {
                categoryChannelMap = (CategoryChannelMap) listmap.get(i);
                CntTargetSync tarsync = new CntTargetSync();

                tarsync.setElementid(categoryChannelMap.getChannelid());
                tarsync.setParentid(categoryChannelMap.getCategoryid());
                tarsync.setTargetindex(targetindex);
                List tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {

                }
                else
                {
                    CntTargetSync tarsy = new CntTargetSync();
                    tarsy.setObjectid(categoryChannelMap.getCategoryid());
                    tarsy.setTargetindex(targetindex);
                    List tarlistctg = cntTargetSyncDS.getCntTargetSyncByCond(tarsy);
                    if (tarlistctg != null && tarlistctg.size() > 0)
                    {
                        tarsy = cntTargetSync;
                        tarsy.setStatus(0);
                        tarsy.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                        tarsy.setRelateindex(relateindex);

                        cntTargetSyncDS.insertCntTargetSync(tarsy);
                        // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
                        // "log.channelcontent.operator",
                        // "log.target.channel.bind.add",
                        // "log.target.channel.bind.add.info",
                        // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                        resultStr.append(ResourceMgt
                                .findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                                + objid
                                + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                                + "  ");
                        sucess = true;
                    }
                }
            }

        }

        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
                // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
                // "log.channelcontent.operator",
                // "log.target.channel.bind.add",
                // "log.target.channel.bind.add.info",
                // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    public String publishToTarget(Long targetindex, long objindex, String objid, int objtype, Long relateindex)
            throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);

        Targetsystem target = new Targetsystem();
        target.setTargetindex(targetindex);
        List tarlist = targetSystemDS.getTargetsystemByCond(target);
        if (tarlist != null)
        {
            target = (Targetsystem) tarlist.get(0);
        }
        // CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
        // categoryChannelMap.setChannelid(objid);
        // List listmap = categoryChannelMapDS.getCategoryChannelMapByCond(categoryChannelMap);
        // if (listmap != null && listmap.size() > 0)
        // {
        // for (int i = 0; i < listmap.size(); i++)
        // {
        // categoryChannelMap = (CategoryChannelMap) listmap.get(i);
        // CntTargetSync tarsync = new CntTargetSync();
        //
        // tarsync.setElementid(categoryChannelMap.getChannelid());
        // tarsync.setParentid(categoryChannelMap.getCategoryid());
        // tarsync.setTargetindex(targetindex);
        // List tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
        // if (tarlist != null && tarlist.size() > 0)
        // {
        //
        // }
        // else
        // {
        // CntTargetSync tarsy = new CntTargetSync();
        // tarsy.setObjectid(categoryChannelMap.getCategoryid());// 判断栏目在这个网元是上否有关联数据
        // tarsy.setTargetindex(targetindex);
        // tarsy.setObjecttype(2);
        // List tarlistctg = cntTargetSyncDS.getCntTargetSyncByCond(tarsy);
        // if (tarlistctg != null && tarlistctg.size() > 0)// 如果栏目也有发布数据
        // {
        // tarsy = tarsync;
        // tarsy.setStatus(0);
        // tarsy.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
        // tarsy.setRelateindex(relateindex);
        //
        // cntTargetSyncDS.insertCntTargetSync(tarsy);
        //
        // CommonLogUtil.insertOperatorLog(objid + ", " + targetindex,
        // "log.channelcontent.operator",
        // "log.channelcontent.bindtarget",
        // "log.channelcontent.bindtarget.info",
        // CategoryConstants.RESOURCE_OPERATION_SUCCESS);
        // resultStr.append(ResourceMgt
        // .findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
        // + objid
        // + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
        // + "  ");
        // sucess = true;
        // }
        // }
        // }
        //
        // }

        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)// 先查询有没有数据，再关联
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);

                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }

                CommonLogUtil.insertOperatorLog(objid + ", " + target.getTargetid(), "log.channelcontent.operator",
                        "log.channelcontent.bindtarget", "log.channelcontent.bindtarget.info",
                        CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    /* 频道关联网元,生成发布数据 */
    public String bindBatchChannelTargetsys(List<Targetsystem> targetsystemList, List<CmsChannel> cmsChannelList)
            throws DomainServiceException
    {

        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalChannelindex = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsChannelList.size(); i++) // 遍历要操作的内容主键，依次操作要关联的内容
        {
            totalChannelindex++;
            String result = null;
            String operdesc = null;
            String proStatusStr = null;

            CmsChannel cmsChannel = (CmsChannel) cmsChannelList.get(i);

            if (cmsChannel == null)
            {
                proStatusStr = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED);
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsChannel.getChannelid(), result.substring(2,
                        result.length()), proStatusStr);
                returnInfo.appendOperateInfo(operateInfo);
            }// 未审核通过不允许关联平台
            else if (cmsChannel.getStatus() != 0)
            {
                proStatusStr = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED);
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsChannel.getChannelid(), result.substring(2,
                        result.length()), "未审核通过不允许关联平台");
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
            boolean flag = false;
            String msg = "";

            for (int j = 0; j < targetsystemList.size(); j++)
            {

                operdesc = preparetopublish(targetsystemList.get(j), cmsChannel.getChannelindex(), cmsChannel
                        .getChannelid(), 5);// 添加到网元的数据，栏目

                if (operdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                {
                    flag = true;
                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "关联成功";
                }
                else
                {
                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "已经被关联";
                }

                // 增加节目单关联平台
                CmsSchedule schedule = new CmsSchedule();
                schedule.setChannelid(cmsChannel.getChannelid());
                schedule.setStatus(0);// 审核通过
                List schelist = cmsScheduleDS.getCmsScheduleByCond(schedule);
                if (schelist != null && schelist.size() > 0)
                {
                    for (int sche = 0; sche < schelist.size(); sche++)
                    {
                        schedule = (CmsSchedule) schelist.get(sche);
                        preparetopublish(targetsystemList.get(j), schedule.getScheduleindex(),
                                schedule.getScheduleid(), 6);// 添加到网元的数据，节目单
                    }

                }

                // 增加频道关联栏目的mapping
                CategoryChannelMap ctgmap = new CategoryChannelMap();
                ctgmap.setChannelindex(cmsChannel.getChannelindex());
                List ctglist = categoryChannelMapDS.getCategoryChannelMapByCond2(ctgmap);// 此频道下的所有map
                if (ctglist != null && ctglist.size() > 0)
                {
                    for (int count = 0; count < ctglist.size(); count++)
                    {
                        ctgmap = (CategoryChannelMap) ctglist.get(count);
                        long ctgindex = ctgmap.getCategoryindex();
                        CntTargetSync ctgsync = new CntTargetSync();
                        ctgsync.setObjecttype(2);
                        ctgsync.setObjectindex(ctgindex);
                        List synclist = cntTargetSyncDS.getCntTargetSyncByCond(ctgsync);
                        if (synclist != null && synclist.size() > 0)
                        {
                            for (int kk = 0; kk < synclist.size(); kk++)
                            {
                                long targetindex = ((CntTargetSync) synclist.get(kk)).getTargetindex();
                                if (targetindex == targetsystemList.get(j).getTargetindex())
                                {// 栏目的关联平台同频道的一致
                                    preparetopublishForMap(targetsystemList.get(j), ctgmap.getMapindex(), ctgmap
                                            .getMappingid(), 28, ctgmap.getChannelid(), ctgmap.getCategoryid());
                                }
                            }
                        }
                    }
                }

                if (targetsystemList.get(j).getTargettype() == 2 || targetsystemList.get(j).getTargettype() == 0
                        || targetsystemList.get(j).getTargettype() == 10)
                {
                    ChannelPictureMap map = new ChannelPictureMap();
                    map.setChannelindex(cmsChannel.getChannelindex());
                    List list = channelPictureMapDS.getChannelPictureMapByCond(map);// 栏目下存在海报,将海报一起关联平台
                    if (list != null && list.size() > 0)
                    {
                        for (int k = 0; k < list.size(); k++)
                        {
                            ChannelPictureMap cpmap = (ChannelPictureMap) list.get(k);
                            Long pindex = cpmap.getPictureindex();

                            preparetopublishForMap(targetsystemList.get(j), cpmap.getMapindex(), cpmap.getMappingid(),
                                    39, cpmap.getChannelid(), cpmap.getPictureid());// 海报的发布数据

                        }
                    }
                }

            }
            if (flag == true)
            {
                successCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsChannel.getChannelid(), "成功", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), cmsChannel.getChannelid(), "失败", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }

            num++;
        }

        if ((successCount) == totalChannelindex)
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalChannelindex)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();
    }

    /**
     * 获取时间 如"20111002"
     * 
     * @return
     */
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    /**
     * 将字符中的"\"转换成"/"
     * 
     * @param str
     * @return
     */
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 取出同步XML文件FTP地址
     * 
     * @return
     */
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }

    public TableDataInfo queryCtgPltSyncCha(CSCntPlatformSync cntPlatformSync, int start, int pageSize)
    {
        log.debug("queryCtgPltSyncCha start...");
        TableDataInfo dataInfo = null;
        try
        {
            // 格式化内容带有时间的字段

            cntPlatformSync.setCreatetimeStartDate(DateUtil2.get14Time(cntPlatformSync.getCreatetimeStartDate()));
            cntPlatformSync.setCreatetimeEndDate(DateUtil2.get14Time(cntPlatformSync.getCreatetimeEndDate()));
            cntPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeStartDate()));
            cntPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeEndDate()));
            cntPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(cntPlatformSync.getCntofflinestarttime()));
            cntPlatformSync.setCntofflineendtime(DateUtil2.get14Time(cntPlatformSync.getCntofflineendtime()));
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cntPlatformSync.setOperid(operinfo.getOperid());
            dataInfo = cscntPlatformSyncDS.pageInfoQueryCha(cntPlatformSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgPltSyncCha error");
        }
        log.debug("queryCtgPltSyncCha end");
        return dataInfo;
    }

    public TableDataInfo queryCtgPltSyncCach(CSCntPlatformSync cntPlatformSync, int start, int pageSize)
    {
        log.debug("queryCtgPltSyncCha start...");
        TableDataInfo dataInfo = null;
        try
        {
            cntPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeStartDate()));
            cntPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeEndDate()));
            cntPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(cntPlatformSync.getCntofflinestarttime()));
            cntPlatformSync.setCntofflineendtime(DateUtil2.get14Time(cntPlatformSync.getCntofflineendtime()));
            dataInfo = cscntPlatformSyncDS.pageInfoQueryCach(cntPlatformSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgPltSyncCha error");
        }
        log.debug("queryCtgPltSyncCha end");
        return dataInfo;
    }

    public TableDataInfo queryCtgTarSyncCha(CSCntTargetSync CScntTargetSync, int start, int pageSize)
    {
        log.debug("queryCtgTarSync start...");
        TableDataInfo dataInfo = null;
        try
        {
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            CScntTargetSync.setOperid(operinfo.getOperid());
            dataInfo = CScntTargetSyncDS.pageInfoQueryCha(CScntTargetSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgTarSync error");
        }
        log.debug("queryCtgTarSync end");
        return dataInfo;
    }

    public TableDataInfo queryCtgTarSyncCach(CSCntTargetSync CScntTargetSync, int start, int pageSize)
    {
        log.debug("queryCtgTarSync start...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = CScntTargetSyncDS.pageInfoQueryCach(CScntTargetSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgTarSync error");
        }
        log.debug("queryCtgTarSync end");
        return dataInfo;
    }

    // 频道删除平台关联关系方法 28
    // 删除网元的发布数据，删除网元和平台信息,在网元全部删除后删除平台,删除海报相关发布数据
    public HashMap deletecntsync(long syncindex)
    {
        log.debug("deleteCategory start...");

        HashMap result = new HashMap();
        String msg = "";
        CntTargetSync sync = new CntTargetSync();
        sync.setSyncindex(syncindex);
        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(sync);
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (list != null && list.size() > 0)
        {

            for (int i = 0; i < list.size(); i++)
            {
                sync = (CntTargetSync) list.get(i);
                int status = sync.getStatus();
                // 发布中，不允许修改
                if (status == 200 || status == 300 || status == 500)
                {
                    result.put("msg", "1:在该状态下不允许删除");
                    return result;
                }
            }

            try
            {
                CntTargetSync cntsync = new CntTargetSync();
                cntsync.setRelateindex(sync.getRelateindex());// 得到相关网元

                CntPlatformSync platsync = new CntPlatformSync();
                platsync.setSyncindex(cntsync.getRelateindex());// 得到平台
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(platsync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    platsync = (CntPlatformSync) pltlist.get(0);
                }
                long targetindex = sync.getTargetindex();// 频道关联的网元index

                // 获得节目单信息

                CmsSchedule schedule = new CmsSchedule();
                schedule.setChannelid(sync.getObjectid());
                List schedulelist = cmsScheduleDS.getCmsScheduleByCond(schedule);// 可能会有多个节目单
                if (schedulelist != null && schedulelist.size() > 0)
                {
                    for (int kk = 0; kk < schedulelist.size(); kk++)
                    {
                        schedule = (CmsSchedule) schedulelist.get(kk);
                        String scheduleid = schedule.getScheduleid();
                        CntTargetSync schedulesync = new CntTargetSync();
                        schedulesync.setObjecttype(6);
                        schedulesync.setTargetindex(sync.getTargetindex());
                        schedulesync.setObjectid(scheduleid);
                        List schedulelistsync = cntTargetSyncDS.getCntTargetSyncByCond(schedulesync);
                        cntTargetSyncDS.removeCntTargetSyncList(schedulelistsync);// 删除节目单的此网元上的发布数据

                        for (int count = 0; count < schedulelistsync.size(); count++)
                        {
                            CntPlatformSync platschesync = new CntPlatformSync();

                            platschesync.setSyncindex(((CntTargetSync) schedulelistsync.get(count)).getRelateindex());// 得到节目单平台

                            List pltschelist = cntPlatformSyncDS.getCntPlatformSyncByCond(platschesync);
                            if (pltschelist != null && pltschelist.size() > 0)
                            {
                                platschesync = (CntPlatformSync) pltschelist.get(0);
                            }
                            CntTargetSync schesyncall = new CntTargetSync();
                            schesyncall.setRelateindex(((CntTargetSync) schedulelistsync.get(count)).getRelateindex());

                            List allschelist = cntTargetSyncDS.getCntTargetSyncByCond(schesyncall);
                            if (allschelist != null && allschelist.size() > 0)
                            {
                                int[] targetSyncStatus = new int[allschelist.size()];
                                for (int ss = 0; ss < allschelist.size(); ss++)
                                {
                                    targetSyncStatus[ss] = ((CntTargetSync) (allschelist.get(ss))).getStatus();
                                }
                                int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                                int originalstatus = platschesync.getStatus();// 平台的原始状态
                                if (originalstatus != statusfinal)
                                {
                                    platschesync.setStatus(statusfinal);
                                    cntPlatformSyncDS.updateCntPlatformSync(platschesync);
                                }
                            }
                            else
                            {

                                cntPlatformSyncDS.removeCntPlatformSync(platschesync);// 删除平台节目单的发布数据

                            }
                        }

                    }
                }

                // 如果栏目关联到这个频道，也关联到这个网元，删除这个栏目与频道的mapping的发布数据
                CntTargetSync ctgsync = new CntTargetSync();
                ctgsync.setTargetindex(targetindex);
                ctgsync.setObjecttype(28);// 栏目
                ctgsync.setElementid(sync.getObjectid());// 得到频道id
                List ctgmaplist = cntTargetSyncDS.getCntTargetSyncByCond(ctgsync);
                if (ctgmaplist != null && ctgmaplist.size() > 0)
                {
                    cntTargetSyncDS.removeCntTargetSyncList(ctgmaplist);// 删除栏目频道的mapping发布数据
                    // 删除平台相应发布数据
                    for (int num = 0; num < ctgmaplist.size(); num++)
                    {

                        CntPlatformSync platctgsync = new CntPlatformSync();

                        platctgsync.setSyncindex(((CntTargetSync) ctgmaplist.get(num)).getRelateindex());// 得到栏目mapping平台
                        List pltctgmaplist = cntPlatformSyncDS.getCntPlatformSyncByCond(platctgsync);
                        if (pltctgmaplist != null && pltctgmaplist.size() > 0)
                        {
                            platctgsync = (CntPlatformSync) pltctgmaplist.get(0);
                        }

                        CntTargetSync picsyncall = new CntTargetSync();
                        picsyncall.setRelateindex(((CntTargetSync) ctgmaplist.get(num)).getRelateindex());

                        List allctglist = cntTargetSyncDS.getCntTargetSyncByCond(picsyncall);
                        if (allctglist != null && allctglist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[allctglist.size()];
                            for (int count = 0; count < allctglist.size(); count++)
                            {
                                targetSyncStatus[count] = ((CntTargetSync) (allctglist.get(count))).getStatus();
                            }
                            int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                            int originalstatus = platctgsync.getStatus();// 平台的原始状态
                            if (originalstatus != statusfinal)
                            {
                                platctgsync.setStatus(statusfinal);
                                cntPlatformSyncDS.updateCntPlatformSync(platctgsync);
                            }
                        }
                        else
                        {

                            cntPlatformSyncDS.removeCntPlatformSync(platctgsync);// 删除平台栏目的发布数据

                        }
                    }
                }

                // 如果海报也关联到这个网元，删除这个海报与频道的mapping的发布数据
                CntTargetSync picsync = new CntTargetSync();
                picsync.setTargetindex(targetindex);
                picsync.setObjecttype(39);// 海报
                picsync.setElementid(sync.getObjectid());// 得到频道id
                List picmaplist = cntTargetSyncDS.getCntTargetSyncByCond(picsync);
                if (picmaplist != null && picmaplist.size() > 0)
                {
                    cntTargetSyncDS.removeCntTargetSyncList(picmaplist);// 删除海报频道的mapping发布数据
                    // 删除平台相应发布数据
                    for (int num = 0; num < picmaplist.size(); num++)
                    {

                        CntPlatformSync platpicsync = new CntPlatformSync();

                        platpicsync.setSyncindex(((CntTargetSync) picmaplist.get(num)).getRelateindex());// 得到海报mapping平台
                        List pltpicmaplist = cntPlatformSyncDS.getCntPlatformSyncByCond(platpicsync);
                        if (pltpicmaplist != null && pltpicmaplist.size() > 0)
                        {
                            platpicsync = (CntPlatformSync) pltpicmaplist.get(0);
                        }
                        CntTargetSync picsyncall = new CntTargetSync();
                        picsyncall.setRelateindex(((CntTargetSync) picmaplist.get(num)).getRelateindex());

                        List allpiclist = cntTargetSyncDS.getCntTargetSyncByCond(picsyncall);
                        if (allpiclist != null && allpiclist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[allpiclist.size()];
                            for (int count = 0; count < allpiclist.size(); count++)
                            {
                                targetSyncStatus[count] = ((CntTargetSync) (allpiclist.get(count))).getStatus();
                            }
                            int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                            int originalstatus = platpicsync.getStatus();// 平台的原始状态
                            if (originalstatus != statusfinal)
                            {
                                platpicsync.setStatus(statusfinal);
                                cntPlatformSyncDS.updateCntPlatformSync(platpicsync);
                            }
                        }
                        else
                        {

                            cntPlatformSyncDS.removeCntPlatformSync(platpicsync);// 删除平台海报的发布数据

                        }
                    }
                }

                cntTargetSyncDS.removeCntTargetSyncList(list);// 删除网元关联数据频道

                // 获得有共同平台的所有targetlist
                List alllist = cntTargetSyncDS.getCntTargetSyncByCond(cntsync);
                if (alllist != null && alllist.size() > 0)
                {
                    int[] targetSyncStatus = new int[alllist.size()];
                    for (int count = 0; count < alllist.size(); count++)
                    {
                        targetSyncStatus[count] = ((CntTargetSync) (alllist.get(count))).getStatus();
                    }
                    int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                    int originalstatus = platsync.getStatus();// 平台的原始状态
                    if (originalstatus != statusfinal)
                    {
                        platsync.setStatus(statusfinal);
                        cntPlatformSyncDS.updateCntPlatformSync(platsync);
                    }
                }
                else
                {

                    cntPlatformSyncDS.removeCntPlatformSync(platsync);// 删除平台频道
                }
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                result.put("msg", "1:删除失败");
                return result;

            }
            CommonLogUtil.insertOperatorLog(sync.getObjectid(), "log.channelcontent.operator",
                    "log.target.channel.bind.delete", "log.target.channel.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            result.put("msg", "0:删除成功");
        }
        else
        {
            CommonLogUtil.insertOperatorLog(sync.getObjectid(), "log.channelcontent.operator",
                    "log.target.channel.bind.delete", "log.target.channel.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_FAIL);
            result.put("msg", "1:此条数据已被删除");
            return result;
        }
        log.debug("deleteCategory end");

        return result;
    }

    // 频道各个方法

    public ICmsChannelDS getChannelDS()
    {
        return channelDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public IUcpBasicDS getBasicDS()
    {
        return basicDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUsysCityDS getCityDS()
    {
        return cityDS;
    }

    public void setCityDS(IUsysCityDS cityDS)
    {
        this.cityDS = cityDS;
    }

    public IsysProvinceLS getProvinceLS()
    {
        return provinceLS;
    }

    public void setProvinceLS(IsysProvinceLS provinceLS)
    {
        this.provinceLS = provinceLS;
    }

    public ICntSyncTaskDS getCntsyncTaskDS()
    {
        return cntsyncTaskDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public IBatchObjectRecordDS getBatchObjectRecordDS()
    {
        return batchObjectRecordDS;
    }

    public void setBatchObjectRecordDS(IBatchObjectRecordDS batchObjectRecordDS)
    {
        this.batchObjectRecordDS = batchObjectRecordDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public IPhysicaChannellLS getPhysicaChannellLS()
    {
        return physicaChannellLS;
    }

    public void setPhysicaChannellLS(IPhysicaChannellLS physicaChannellLS)
    {
        this.physicaChannellLS = physicaChannellLS;
    }

    public ICategoryChannelMapDS getCategoryChannelMapDS()
    {
        return categoryChannelMapDS;
    }

    public void setCategoryChannelMapDS(ICategoryChannelMapDS categoryChannelMapDS)
    {
        this.categoryChannelMapDS = categoryChannelMapDS;
    }

    public ICmsScheduleDS getCmsScheduleDS()
    {
        return cmsScheduleDS;
    }

    public void setCmsScheduleDS(ICmsScheduleDS cmsScheduleDS)
    {
        this.cmsScheduleDS = cmsScheduleDS;
    }

    public IPictureDS getPictureDS()
    {
        return pictureDS;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public ICmsSchedulerecordDS getCmsSchedulerecordDS()
    {
        return cmsSchedulerecordDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public IChannelUpdateApply getChannelupdateApply()
    {
        return channelupdateApply;
    }

    public void setChannelupdateApply(IChannelUpdateApply channelupdateApply)
    {
        this.channelupdateApply = channelupdateApply;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    public IServiceChannelMapDS getServiceChannelMapDS()
    {
        return serviceChannelMapDS;
    }

    public void setServiceChannelMapDS(IServiceChannelMapDS serviceChannelMapDS)
    {
        this.serviceChannelMapDS = serviceChannelMapDS;
    }

    /********************************* 海报发布管理 ******************************/

    public TableDataInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            channelPictureMap.setCreateStarttime(DateUtil2.get14Time(channelPictureMap.getCreateStarttime()));
            channelPictureMap.setCreateEndtime(DateUtil2.get14Time(channelPictureMap.getCreateEndtime()));
            channelPictureMap.setOnlinetimeStartDate(DateUtil2.get14Time(channelPictureMap.getOnlinetimeStartDate()));
            channelPictureMap.setOnlinetimeEndDate(DateUtil2.get14Time(channelPictureMap.getOnlinetimeEndDate()));
            channelPictureMap.setCntofflinestarttime(DateUtil2.get14Time(channelPictureMap.getCntofflinestarttime()));
            channelPictureMap.setCntofflineendtime(DateUtil2.get14Time(channelPictureMap.getCntofflineendtime()));

            if (channelPictureMap.getPictureid() != null && !"".equals(channelPictureMap.getPictureid()))
            {
                channelPictureMap.setPictureid(EspecialCharMgt.conversion(channelPictureMap.getPictureid()));
            }

            dataInfo = channelPictureMapDS.pageInfoQuerychanPicMap(channelPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }

    public TableDataInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize)
            throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            dataInfo = channelPictureMapDS.pageInfoQuerypchanPicTargetMap(channelPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }

    public String batchPublishOrDelPublishchanPic(List<ChannelPictureMap> list, String operType, int type)
            throws Exception
    {
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
            if (operType.equals("0")) // 批量发布
            {
                for (ChannelPictureMap channelPictureMap : list)
                {
                    message = publishchanPicMapPlatformSyn(channelPictureMap.getMapindex().toString(),
                            channelPictureMap.getChannelid(), channelPictureMap.getPictureid(), type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), channelPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), channelPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else
            // 批量取消发布
            {
                for (ChannelPictureMap channelPictureMap : list)
                {
                    message = delPublishchanPicMapPlatformSyn(channelPictureMap.getMapindex().toString(),
                            channelPictureMap.getChannelid(), channelPictureMap.getPictureid(), type, 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), channelPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), channelPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("cmsChannelLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();
    }

    public String publishchanPicMapPlatformSyn(String mapindex, String channelid, String pictureid, int type,
            int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null;
        boolean sucessPub = false;
        StringBuffer resultPubStr = new StringBuffer();
        try
        {
            ChannelPictureMap channelPictureMap = new ChannelPictureMap();

            channelPictureMap.setMapindex(Long.parseLong(mapindex));
            channelPictureMap.setChannelid(channelid);
            channelPictureMap.setPictureid(pictureid);
            List<ChannelPictureMap> channelPictureMapList = new ArrayList<ChannelPictureMap>();
            channelPictureMapList = channelPictureMapDS.getChannelPictureMapByCond(channelPictureMap);
            if (channelPictureMapList == null || channelPictureMapList.size() == 0)
            {
                return "1:操作失败,频道关联海报记录不存在";
            }
            String initresult = "";
            initresult = initDataPic(channelPictureMapList.get(0), "platform", type, 1);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
            cntPlatformSync.setObjecttype(39);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(channelPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            chaneMap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(39);
            chaneMap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ChannelPictureMap channelPictureMapObj = new ChannelPictureMap();
            int platformType = 1;
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setObjectindex(channelPictureMap.getMapindex());
            if (type == 1) // 3.0平台
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                chaneMap.put("pictureMap", channelPictureMapList);
                int targettype;
                for (ChannelPictureMap channelPictureMapTmp : channelPictureMapList)
                {
                    targettype = channelPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (channelPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": " + "目标系统状态不正确"
                                + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        synType = 1;
                        boolean isxml = true;
                        boolean ispub = true;
                        cntTargetSync.setObjectindex(channelPictureMapTmp.getChannelindex());
                        cntTargetSync.setTargetindex(channelPictureMapTmp.getTargetindex());
                        cntTargetSync.setObjecttype(5);
                        cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                        if (cntTargetSync == null
                                || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync
                                        .getStatus() != 500))
                        {
                            resultPubStr.append("该频道没有发布到" + TARGETSYSTEM + channelPictureMapTmp.getTargetid() + "  ");
                            ispub = false;
                        }
                        if (ispub)
                        {
                            int synTargetType = 1;
                            if (channelPictureMapTmp.getStatus() == 500) // 修改发布
                            {
                                synTargetType = 2;
                                synType = 2;
                            }
                            if (targettype == 2
                                    && (channelPictureMapTmp.getStatus() == 0
                                            || channelPictureMapTmp.getStatus() == 400 || channelPictureMapTmp
                                            .getStatus() == 500)) // EPG系统
                            {
                                String xml;
                                if (synTargetType == 1)
                                {
                                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap,
                                            targettype, 10, 1); // 新增发布到EPG
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                                + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                else
                                {
                                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap,
                                            targettype, 10, 2); // 修改发布到EPG
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                                + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                if (isxml)
                                {
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                            .toString();
                                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                    Long targetindex = channelPictureMapTmp.getTargetindex();

                                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index"); // 获取taskindex
                                    Long objectPicindex = channelPictureMapTmp.getPictureindex();
                                    String objectid = channelPictureMapTmp.getPictureid();
                                    String objectcode = channelPictureMapTmp.getPicturecode();
                                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                            synType, objectcode, 1); // 插入对象日志发布表
                                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index");
                                    String objectidMapping = channelPictureMapTmp.getMappingid();
                                    String parentid = channelPictureMapTmp.getPictureid();
                                    String elementid = channelPictureMapTmp.getChannelid();
                                    Long objectindex = channelPictureMapTmp.getMapindex();
                                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping,
                                            elementid, parentid, targetindex, synType);

                                    insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                    cntTargetSyncTemp.setSyncindex(channelPictureMapTmp.getSyncindex());
                                    if (synTargetType == 1)
                                    {
                                        cntTargetSyncTemp.setOperresult(10);
                                    }
                                    else
                                    {
                                        cntTargetSyncTemp.setOperresult(40);
                                    }
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(channelPictureMapTmp.getMappingid() + ","
                                            + channelPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH,
                                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub = true;
                                    resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                            + operSucess + "  ");
                                }
                            }
                        }
                    }
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                chaneMap.put("pictureMap", channelPictureMapList);
                for (ChannelPictureMap channelPictureMapTmp : channelPictureMapList)
                {
                    int targettype = channelPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (channelPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": " + "目标系统状态不正确"
                                + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        synType = 1;
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        boolean isxml = true;
                        boolean ispub = true;
                        cntTargetSync.setObjectindex(channelPictureMapTmp.getChannelindex());
                        cntTargetSync.setTargetindex(channelPictureMapTmp.getTargetindex());
                        cntTargetSync.setObjecttype(5);
                        cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                        if (cntTargetSync == null
                                || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync
                                        .getStatus() != 500))
                        {
                            resultPubStr.append("该频道没有发布到" + TARGETSYSTEM + channelPictureMapTmp.getTargetid() + "  ");
                            ispub = false;
                        }
                        if (ispub)
                        {
                            int synTargetType = 1;
                            if (channelPictureMapTmp.getStatus() == 500) // 修改发布
                            {
                                synTargetType = 2;
                                synType = 2;
                            }
                            if ((channelPictureMapTmp.getStatus() == 0 || channelPictureMapTmp.getStatus() == 400 || channelPictureMapTmp
                                    .getStatus() == 500)) // 2.0平台网元
                            {
                                String xml;
                                if (synTargetType == 1)
                                {
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap,
                                            targettype, 10, 1); // 新增发布
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                                + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                else
                                {
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap,
                                            targettype, 10, 2); // 修改发布
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                                + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                if (isxml)
                                {
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                            .toString();
                                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                    Long targetindex = channelPictureMapTmp.getTargetindex();
                                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index"); // 获取taskindex
                                    Long objectPicindex = channelPictureMapTmp.getPictureindex();
                                    String objectid = channelPictureMapTmp.getPictureid();
                                    String objectcode = channelPictureMapTmp.getPicturecode();
                                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                            synType, objectcode, 1); // 插入对象日志发布表
                                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index");
                                    String objectidMapping = channelPictureMapTmp.getMappingid();
                                    String parentid = channelPictureMapTmp.getPictureid();
                                    String elementid = channelPictureMapTmp.getChannelid();
                                    Long objectindex = channelPictureMapTmp.getMapindex();
                                    String elementcode = channelPictureMapTmp.getCpcontentid();
                                    String parentcode = objectcode;
                                    insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping,
                                            elementid, parentid, elementcode, parentcode, targetindex, synType);
                                    insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                    cntTargetSyncTemp.setSyncindex(channelPictureMapTmp.getSyncindex());
                                    if (synTargetType == 1)
                                    {
                                        cntTargetSyncTemp.setOperresult(10);
                                    }
                                    else
                                    {
                                        cntTargetSyncTemp.setOperresult(40);
                                    }
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(channelPictureMapTmp.getMappingid() + ","
                                            + channelPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH,
                                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub = true;
                                    resultPubStr.append(PUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + ": "
                                            + operSucess + "  ");
                                }
                            }
                        }
                    }
                }
            }
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTemp.setStatus(200);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        rtnPubStr = sucessPub == false ? "1:" + resultPubStr.toString() : "0:" + resultPubStr.toString();
        return rtnPubStr;
    }

    public String delPublishchanPicMapPlatformSyn(String mapindex, String channelid, String pictureid, int type,
            int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null;
        boolean sucessPub = false;
        StringBuffer resultPubStr = new StringBuffer();
        try
        {
            ChannelPictureMap channelPictureMap = new ChannelPictureMap();
            channelPictureMap.setMapindex(Long.parseLong(mapindex));
            channelPictureMap.setChannelid(channelid);
            channelPictureMap.setPictureid(pictureid);
            List<ChannelPictureMap> channelPictureMapList = new ArrayList<ChannelPictureMap>();
            channelPictureMapList = channelPictureMapDS.getChannelPictureMapByCond(channelPictureMap);
            if (channelPictureMapList == null || channelPictureMapList.size() == 0)
            {
                return "1:操作失败,频道关联海报记录不存在";
            }
            String initresult = "";
            initresult = initDataPic(channelPictureMapList.get(0), "platform", type, 3);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
            cntPlatformSync.setObjecttype(39);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(channelPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            chaneMap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(39);
            chaneMap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ChannelPictureMap channelPictureMapObj = new ChannelPictureMap();
            int platformType = 1;
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setObjectindex(channelPictureMap.getMapindex());
            if (type == 1) // 3.0平台
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                chaneMap.put("pictureMap", channelPictureMapList);
                for (ChannelPictureMap channelPictureMapTmp : channelPictureMapList)
                {
                    int targettype = channelPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (channelPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub + ": "
                                + "目标系统状态不正确" + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        boolean isxml = true;
                        if (targettype == 2
                                && (channelPictureMapTmp.getStatus() == 300 || channelPictureMapTmp.getStatus() == 500)) // EPG系统
                        {
                            String xml;
                            xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap, targettype, 10, 3); // 取消发布到EPG
                            if (null == xml)
                            {
                                isxml = false;
                                resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub
                                        + ": " + "生成同步指令xml文件失败" + "  ");
                            }
                            if (isxml)
                            {
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                        .toString();
                                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                Long targetindex = channelPictureMapTmp.getTargetindex();
                                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index"); // 获取taskindex
                                Long objectPicindex = channelPictureMapTmp.getPictureindex();
                                String objectid = channelPictureMapTmp.getPictureid();
                                String objectcode = channelPictureMapTmp.getPicturecode();
                                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                        synType, objectcode, 1); // 插入对象日志发布表
                                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index");
                                String objectidMapping = channelPictureMapTmp.getMappingid();
                                String parentid = channelPictureMapTmp.getPictureid();
                                String elementid = channelPictureMapTmp.getChannelid();
                                Long objectindex = channelPictureMapTmp.getMapindex();
                                insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping,
                                        elementid, parentid, targetindex, synType);
                                insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                cntTargetSyncTemp.setSyncindex(channelPictureMapTmp.getSyncindex());
                                cntTargetSyncTemp.setOperresult(70);
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                CommonLogUtil.insertOperatorLog(channelPictureMapTmp.getMappingid() + ","
                                        + channelPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL,
                                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                sucessPub = true;
                                resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub
                                        + ": " + operSucess + "  ");
                            }

                        }
                    }
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                chaneMap.put("pictureMap", channelPictureMapList);
                for (ChannelPictureMap channelPictureMapTmp : channelPictureMapList)
                {
                    int targettype = channelPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (channelPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub + ": "
                                + "目标系统状态不正确" + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        boolean isxml = true;
                        if ((channelPictureMapTmp.getStatus() == 300 || channelPictureMapTmp.getStatus() == 500)) // 2.0平台网元
                        {
                            String xml;
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap, targettype, 10, 3); // 新增发布
                            if (null == xml)
                            {
                                isxml = false;
                                resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub
                                        + ": " + "生成同步指令xml文件失败" + "  ");
                            }
                            if (isxml)
                            {
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                        .toString();
                                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                Long targetindex = channelPictureMapTmp.getTargetindex();
                                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index"); // 获取taskindex
                                Long objectPicindex = channelPictureMapTmp.getPictureindex();
                                String objectid = channelPictureMapTmp.getPictureid();
                                String objectcode = channelPictureMapTmp.getPicturecode();
                                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                        synType, objectcode, 1); // 插入对象日志发布表
                                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index");
                                String objectidMapping = channelPictureMapTmp.getMappingid();
                                String parentid = channelPictureMapTmp.getPictureid();
                                String elementid = channelPictureMapTmp.getChannelid();
                                Long objectindex = channelPictureMapTmp.getMapindex();
                                String elementcode = channelPictureMapTmp.getCpcontentid();
                                String parentcode = objectcode;
                                insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping,
                                        elementid, parentid, elementcode, parentcode, targetindex, synType);
                                insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务
                                cntTargetSyncTemp.setSyncindex(channelPictureMapTmp.getSyncindex());
                                cntTargetSyncTemp.setOperresult(70);
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                CommonLogUtil.insertOperatorLog(channelPictureMapTmp.getMappingid() + ","
                                        + channelPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL,
                                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                sucessPub = true;
                                resultPubStr.append(DELPUBTARGETSYSTEM + channelPictureMapTmp.getTargetid() + delPub
                                        + ": " + operSucess + "  ");
                            }

                        }
                    }
                }
            }
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTemp.setStatus(200);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        rtnPubStr = sucessPub == false ? "1:" + resultPubStr.toString() : "0:" + resultPubStr.toString();
        return rtnPubStr;
    }

    public String publishchanPicMapTarget(String mapindex, String targetindex, int type) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            ChannelPictureMap channelPictureMap = new ChannelPictureMap();
            channelPictureMap.setMapindex(Long.parseLong(mapindex));
            channelPictureMap.setTargetindex(Long.parseLong(targetindex));
            channelPictureMap.setObjecttype(39);
            List<ChannelPictureMap> channelPictureMapList = new ArrayList<ChannelPictureMap>();
            channelPictureMapList = channelPictureMapDS.getChannelPictureMapByCond(channelPictureMap);
            if (channelPictureMapList == null || channelPictureMapList.size() == 0)
            {
                return "1:操作失败,频道关联海报记录不存在";
            }
            int synType = 1;
            String initresult = "";
            initresult = initDataPic(channelPictureMapList.get(0), targetindex, 3, synType);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(channelPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            chaneMap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(39);
            chaneMap.put("type", typeList);
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(channelPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(39);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ChannelPictureMap channelPictureMapObj = new ChannelPictureMap();
            int platformType = 1;
            if (type == 1)
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (channelPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";

                }
                int targettype = channelPictureMapList.get(0).getTargettype();
                chaneMap.put("pictureMap", channelPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(channelPictureMapList.get(0).getChannelindex());
                cntTargetSync.setTargetindex(channelPictureMapList.get(0).getTargetindex());
                cntTargetSync.setObjecttype(5);
                cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                if (cntTargetSync == null
                        || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
                {
                    return "1:操作失败,该频道没有发布到该网元";
                }
                int synTargetType = 1;
                if (channelPictureMapList.get(0).getStatus() == 500) // 修改发布
                {
                    synTargetType = 2;
                    synType = 2;
                }
                if (targettype == 2) // EPG系统
                {
                    String xml;
                    if (synTargetType == 1)
                    {
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap, targettype, 10, 1); // 新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                    }
                    else
                    {
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap, targettype, 10, 2); // 修改发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                    }
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                    Long objectPicindex = channelPictureMapList.get(0).getPictureindex();
                    String objectid = channelPictureMapList.get(0).getPictureid();
                    String objectcode = channelPictureMapList.get(0).getPicturecode();
                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex),
                            synType, objectcode, 1); // 插入对象日志发布表
                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                            "object_sync_record_index");
                    String objectidMapping = channelPictureMapList.get(0).getMappingid();
                    String parentid = channelPictureMapList.get(0).getPictureid();
                    String elementid = channelPictureMapList.get(0).getChannelid();
                    Long objectindex = channelPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                            Long.valueOf(targetindex), synType);
                    insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                    cntTargetSyncTemp.setSyncindex(channelPictureMapList.get(0).getSyncindex());
                    if (synTargetType == 1)
                    {
                        cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);
                    }
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                    CommonLogUtil.insertOperatorLog(channelPictureMapList.get(0).getMappingid() + ","
                            + channelPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH,
                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (channelPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";

                }
                int targettype = channelPictureMapList.get(0).getTargettype();

                chaneMap.put("pictureMap", channelPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(channelPictureMapList.get(0).getChannelindex());
                cntTargetSync.setTargetindex(channelPictureMapList.get(0).getTargetindex());
                cntTargetSync.setObjecttype(5);
                cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                if (cntTargetSync == null
                        || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
                {
                    return "1:操作失败,该频道没有发布到该网元";
                }
                int synTargetType = 1;
                if (channelPictureMapList.get(0).getStatus() == 500) // 修改发布
                {
                    synTargetType = 2;
                    synType = 2;
                }

                String xml;
                if (synTargetType == 1)
                {
                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap, targettype, 10, 1); // 新增发布
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                }
                else
                {
                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap, targettype, 10, 2); // 修改发布
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                }
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                Long objectPicindex = channelPictureMapList.get(0).getPictureindex();
                String objectid = channelPictureMapList.get(0).getPictureid();
                String objectcode = channelPictureMapList.get(0).getPicturecode();
                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex), synType,
                        objectcode, 1); // 插入对象日志发布表
                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                String objectidMapping = channelPictureMapList.get(0).getMappingid();
                String parentid = channelPictureMapList.get(0).getPictureid();
                String elementid = channelPictureMapList.get(0).getChannelid();
                Long objectindex = channelPictureMapList.get(0).getMapindex();
                String elementcode = channelPictureMapList.get(0).getCpcontentid();
                String parentcode = objectcode;
                insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                        elementcode, parentcode, Long.valueOf(targetindex), synType);
                insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                cntTargetSyncTemp.setSyncindex(channelPictureMapList.get(0).getSyncindex());
                if (synTargetType == 1)
                {
                    cntTargetSyncTemp.setOperresult(10);
                }
                else
                {
                    cntTargetSyncTemp.setOperresult(40);
                }
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                CommonLogUtil.insertOperatorLog(channelPictureMapList.get(0).getMappingid() + ","
                        + channelPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH,
                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            }
            List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
            cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
            int[] targetSyncStatus = new int[cntTargetSyncList.size()];
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                targetSyncStatus[i] = cntTargetSyncList.get(i).getStatus();
            }
            int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
            if (cntPlatformSynctmp.getStatus() != status) // 平台状态与计算出的状态不一致需要更新
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(channelPictureMap.getMapindex());
                cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(status);
                cntPlatformSyncTmp.setPlatform(platformType);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return result;
    }

    public String delPublishchanPicMapTarget(String mapindex, String targetindex, int type) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            ChannelPictureMap channelPictureMap = new ChannelPictureMap();
            channelPictureMap.setMapindex(Long.parseLong(mapindex));
            channelPictureMap.setTargetindex(Long.parseLong(targetindex));
            channelPictureMap.setObjecttype(39);
            List<ChannelPictureMap> channelPictureMapList = new ArrayList<ChannelPictureMap>();
            channelPictureMapList = channelPictureMapDS.getChannelPictureMapByCond(channelPictureMap);
            if (channelPictureMapList == null || channelPictureMapList.size() == 0)
            {
                return "1:操作失败,频道关联海报记录不存在";
            }
            String initresult = "";
            int synType = 3;
            initresult = initDataPic(channelPictureMapList.get(0), targetindex, 3, synType);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(channelPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            chaneMap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(39);
            chaneMap.put("type", typeList);
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(channelPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(39);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            ChannelPictureMap channelPictureMapObj = new ChannelPictureMap();
            int platformType = 1;
            if (type == 1)
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (channelPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";

                }
                int targettype = channelPictureMapList.get(0).getTargettype();
                chaneMap.put("pictureMap", channelPictureMapList);
                if (targettype == 2) // EPG系统
                {
                    String xml;
                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(chaneMap, targettype, 10, 3); // 新增发布到EPG
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                    Long objectPicindex = channelPictureMapList.get(0).getPictureindex();
                    String objectid = channelPictureMapList.get(0).getPictureid();
                    String objectcode = channelPictureMapList.get(0).getPicturecode();
                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex),
                            synType, objectcode, 1); // 插入对象日志发布表
                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                            "object_sync_record_index");
                    String objectidMapping = channelPictureMapList.get(0).getMappingid();
                    String parentid = channelPictureMapList.get(0).getPictureid();
                    String elementid = channelPictureMapList.get(0).getChannelid();
                    Long objectindex = channelPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                            Long.valueOf(targetindex), synType);
                    insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                    cntTargetSyncTemp.setSyncindex(channelPictureMapList.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                    CommonLogUtil.insertOperatorLog(channelPictureMapList.get(0).getMappingid() + ","
                            + channelPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL,
                            CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                channelPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                channelPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                channelPictureMapList = channelPictureMapDS.getChannelPicturenormalMapByCond(channelPictureMapObj);
                if (channelPictureMapList == null || channelPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (channelPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";
                }
                int targettype = channelPictureMapList.get(0).getTargettype();

                chaneMap.put("pictureMap", channelPictureMapList);

                String xml;
                xml = StandardImpFactory.getInstance().create(1).createXmlFile(chaneMap, targettype, 10, 3); // 取消发布
                if (null == xml)
                {
                    return "1:操作失败,生成同步指令xml文件失败";
                }
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                Long objectPicindex = channelPictureMapList.get(0).getPictureindex();
                String objectid = channelPictureMapList.get(0).getPictureid();
                String objectcode = channelPictureMapList.get(0).getPicturecode();
                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex), synType,
                        objectcode, 1); // 插入对象日志发布表
                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                String objectidMapping = channelPictureMapList.get(0).getMappingid();
                String parentid = channelPictureMapList.get(0).getPictureid();
                String elementid = channelPictureMapList.get(0).getChannelid();
                Long objectindex = channelPictureMapList.get(0).getMapindex();
                String elementcode = channelPictureMapList.get(0).getCpcontentid();
                String parentcode = objectcode;
                insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                        elementcode, parentcode, Long.valueOf(targetindex), synType);
                insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务

                cntTargetSyncTemp.setSyncindex(channelPictureMapList.get(0).getSyncindex());
                cntTargetSyncTemp.setOperresult(70);
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                CommonLogUtil.insertOperatorLog(channelPictureMapList.get(0).getMappingid() + ","
                        + channelPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CHANNELSYN,
                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL,
                        CommonLogConstant.OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            }
            List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
            cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
            int[] targetSyncStatus = new int[cntTargetSyncList.size()];
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                targetSyncStatus[i] = cntTargetSyncList.get(i).getStatus();
            }
            int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
            if (cntPlatformSynctmp.getStatus() != status) // 平台状态与计算出的状态不一致需要更新
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(channelPictureMap.getMapindex());
                cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(status);
                cntPlatformSyncTmp.setPlatform(platformType);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return result;
    }

    public String initDataPic(ChannelPictureMap channelPictureMap, String targetindex, int platfType, int synType)
            throws Exception
    {
        try
        {
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(channelPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(39);
            CmsChannel cmsChannel = new CmsChannel();
            cmsChannel.setChannelindex(channelPictureMap.getChannelindex());
            cmsChannel = channelDS.getCmsChannel(cmsChannel);
            if (cmsChannel == null)
            {
                return "1:操作失败,频道不存在";
            }
            Picture picture = new Picture();
            picture.setPictureindex(channelPictureMap.getPictureindex());
            picture = pictureDS.getPicture(picture);
            if (picture == null)
            {
                return "1:操作失败,海报不存在";
            }
            if (platfType == 1 || platfType == 2) // 发布到3.0平台 或者2.0平台
            {
                CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
                if (platfType == 1)
                {
                    cntPlatformSync.setPlatform(2);
                    cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                    if (cntPlatformSynctmp == null)
                    {
                        return "1:操作失败,发布记录不存在";
                    }
                }
                if (platfType == 2)
                {
                    cntPlatformSync.setPlatform(1);
                    cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                    if (cntPlatformSynctmp == null)
                    {
                        return "1:操作失败,发布记录不存在";
                    }
                }
                if (synType == 1) // 发布
                {
                    if (cntPlatformSynctmp.getStatus() != 0 && cntPlatformSynctmp.getStatus() != 400
                            && cntPlatformSynctmp.getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                else
                // 取消发布
                {
                    if (cntPlatformSynctmp.getStatus() != 300 && cntPlatformSynctmp.getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                if (platfType == 1)
                {
                    cntTargetSyncTemp.setTargettype(2); // 海报只发布到EPG
                }
                else
                {
                    cntTargetSyncTemp.setPlatform(1); // 海报只发布到2.0网元
                }
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null || cntTargetSyncList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType == 1)
                {
                    boolean targetstatus = false;
                    for (CntTargetSync cntTargetSync : cntTargetSyncList)
                    {
                        if (cntTargetSync.getStatus() == 0 || cntTargetSync.getStatus() == 400
                                || cntTargetSync.getStatus() == 500)
                        {
                            targetstatus = true;
                        }
                    }
                    if (!targetstatus)
                    {
                        return "1:操作失败,目标网元发布状态不正确";
                    }
                }
                else
                {
                    boolean targetstatus = false;
                    for (CntTargetSync cntTargetSync : cntTargetSyncList)
                    {
                        if (cntTargetSync.getStatus() == 300 || cntTargetSync.getStatus() == 500)
                        {
                            targetstatus = true;
                        }
                    }
                    if (!targetstatus)
                    {
                        return "1:操作失败,目标网元发布状态不正确";
                    }
                }
            }
            if (platfType == 3) // 发布到网元
            {
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(channelPictureMap.getMapindex());
                cntTargetSync.setObjecttype(39);
                cntTargetSync.setTargetindex(Long.valueOf(targetindex));
                cntTargetSyncListTemp = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
                if (cntTargetSyncListTemp == null || cntTargetSyncListTemp.size() == 0)
                {
                    return "1:操作失败,发布记录不存在";
                }
                if (synType == 1) // 发布
                {
                    if (cntTargetSyncListTemp.get(0).getStatus() != 0
                            && cntTargetSyncListTemp.get(0).getStatus() != 400
                            && cntTargetSyncListTemp.get(0).getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                else
                // 取消发布
                {
                    if (cntTargetSyncListTemp.get(0).getStatus() != 300
                            && cntTargetSyncListTemp.get(0).getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
            }

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return "success";
    }

    private void insertPicMappingRecord(Long syncindexMapping, Long index, Long objectindexMovie,
            String objectidMapping, String elementid, String parentid, Long targetindex, int synType)
    {
        ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncMappingRecord.setSyncindex(syncindexMapping);
            objectSyncMappingRecord.setTaskindex(index);
            objectSyncMappingRecord.setObjectindex(objectindexMovie);
            objectSyncMappingRecord.setObjectid(objectidMapping);
            objectSyncMappingRecord.setDestindex(targetindex);
            objectSyncMappingRecord.setObjecttype(39);
            objectSyncMappingRecord.setElementid(elementid);
            objectSyncMappingRecord.setParentid(parentid);
            objectSyncMappingRecord.setActiontype(synType);
            objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    private void insertObjectRecord(Long syncindex, Long taskindex, Long objectindex, String objectid, int objecttype,
            Long targetindex, int synType, String objectcode, int platType)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setObjectcode(objectcode);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecord.setObjectcode(objectcode);
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    private void insertSyncPicTask(Long taskindex, String xmlAddress, String correlateid, Long targetindex)
    {
        Long batchid = (Long) (this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setBatchid(batchid);
            cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(targetindex);
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsChannelLS exception:" + e.getMessage());
        }
    }

    private void insertPicMappingIPTV2Record(Long syncindexMapping, Long index, Long objectindexMovie,
            String objectidMapping, String elementid, String parentid, String elementcode, String parentcode,
            Long targetindex, int synType)
    {
        ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncMappingRecord.setSyncindex(syncindexMapping);
            objectSyncMappingRecord.setTaskindex(index);
            objectSyncMappingRecord.setObjectindex(objectindexMovie);
            objectSyncMappingRecord.setObjectid(objectidMapping);
            objectSyncMappingRecord.setDestindex(targetindex);
            objectSyncMappingRecord.setObjecttype(39);
            objectSyncMappingRecord.setElementid(elementid);
            objectSyncMappingRecord.setParentid(parentid);
            objectSyncMappingRecord.setElementcode(elementcode);
            objectSyncMappingRecord.setParentcode(parentcode);
            objectSyncMappingRecord.setActiontype(synType);
            objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    /********************************* 海报发布管理 ******************************/
}

package com.zte.cms.content.program.programsync.ls;

public class ProgramSynConstants {
    public static final String MGTTYPE_VODTARGETSYN = "log.vodtargetsyn.mgt";
    public static final String MGTTYPE_SERIESTARGETSYN = "log.seriestargetsyn.mgt";
    public static final String MGTTYPE_SERPROTARGETSYN = "log.serprotargetsyn.mgt";
	
	public static final String OPERTYPE_VOD_PUBLISH = "log.vod.mgt.publish";// 点播内容发布
	public static final String OPERTYPE_VOD_PUBLISH_INFO = "log.vod.mgt.publish.info";// 点播内容发布
	
	public static final String OPERTYPE_VOD_UNPUBLISH = "log.vod.mgt.unpublish";// 点播内容取消发布
	public static final String OPERTYPE_VOD_UNPUBLISH_INFO = "log.vod.mgt.unpublish.info";// 点播内容取消发布
	
	public static final String OPERTYPE_SERIES_PUBLISH = "log.series.mgt.publish";// 连续剧发布
    public static final String OPERTYPE_SERIES_PUBLISH_INFO = "log.series.mgt.publish.info";// 连续剧发布
    
    public static final String OPERTYPE_SERIES_UNPUBLISH = "log.series.mgt.unpublish";// 连续剧取消发布
    public static final String OPERTYPE_SERIES_UNPUBLISH_INFO = "log.series.mgt.unpublish.info";// 连续剧取消发布
    
    public static final String OPERTYPE_SERPRO_PUBLISH = "log.serpro.mgt.publish";// 连续剧单集关联发布
    public static final String OPERTYPE_SERPRO_PUBLISH_INFO = "log.serpro.mgt.publish.info";// 连续剧单集关联发布
    
    public static final String OPERTYPE_SERPRO_UNPUBLISH = "log.serpro.mgt.unpublish";// 连续剧单集关联取消发布
    public static final String OPERTYPE_SERPRO_UNPUBLISH_INFO = "log.serpro.mgt.unpublish.info";//连续剧单集关联取消发布

	public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
	public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
	 
    public static final String SUCCESS = "success";                  //操作成功 
	
    public static final int programAuditSucess = 0; //内容状态为审核通过
    public static final int programTobePub = 0; //内容状态为待发布
    public static final int programPubing = 200; //内容状态为发布中
    public static final int programPubSUCESS = 300; //内容状态为发布成功
    public static final int programPubFail= 400; //内容状态为发布失败
    public static final int programModifyPubFail = 500; //内容状态为修改发布失败
    public static final int programTobeOffline = 600; //内容状态为预下线

    public static final String CMS_PROGRAM_TARGEGESYSTEM= "cms.program_targetsystem";

}

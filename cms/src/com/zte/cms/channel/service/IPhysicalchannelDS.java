package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IPhysicalchannelDS
{
    /**
     * 新增Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public void insertPhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public void updatePhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 批量更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public void updatePhysicalchannelList(List<Physicalchannel> physicalchannelList) throws DomainServiceException;

    /**
     * 根据条件更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel更新条件
     * @throws DomainServiceException ds异常
     */
    public void updatePhysicalchannelByCond(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 根据条件批量更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel更新条件
     * @throws DomainServiceException ds异常
     */
    public void updatePhysicalchannelListByCond(List<Physicalchannel> physicalchannelList)
            throws DomainServiceException;

    /**
     * 删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public void removePhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 批量删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public void removePhysicalchannelList(List<Physicalchannel> physicalchannelList) throws DomainServiceException;

    /**
     * 根据条件删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel删除条件
     * @throws DomainServiceException ds异常
     */
    public void removePhysicalchannelByCond(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 根据条件批量删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel删除条件
     * @throws DomainServiceException ds异常
     */
    public void removePhysicalchannelListByCond(List<Physicalchannel> physicalchannelList)
            throws DomainServiceException;

    /**
     * 查询Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @return Physicalchannel对象
     * @throws DomainServiceException ds异常
     */
    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException;

    /**
     * 根据条件查询Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @return 满足条件的Physicalchannel对象集
     * @throws DomainServiceException ds异常
     */
    public List<Physicalchannel> getPhysicalchannelByCond(Physicalchannel physicalchannel)
            throws DomainServiceException;

    /**
     * 根据条件分页查询Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize)
            throws DomainServiceException;

    public List listPhysicalchannelByChannelid(Physicalchannel physicalchannel);
    
  //查询新增节目单时选择的物理频道
    public TableDataInfo pageInfoQueryschedule(CmsSchedule cmsSchedule, int start, int pageSize)
            throws DomainServiceException;
}
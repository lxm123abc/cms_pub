package com.zte.cms.avtemplate.dao;

import java.util.List;

import com.zte.cms.avtemplate.dao.ICmsAvtemplateinfoDAO;
import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

/**
 * 模版
 * 
 * @author Administrator
 * 
 */
public class CmsAvtemplateinfoDAO extends DynamicObjectBaseDao implements ICmsAvtemplateinfoDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("insert cmsAvtemplateinfo starting...");
        super.insert("insertCmsAvtemplateinfo", cmsAvtemplateinfo);
        log.debug("insert cmsAvtemplateinfo end");
    }

    public void updateCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("update cmsAvtemplateinfo by pk starting...");
        super.update("updateCmsAvtemplateinfo", cmsAvtemplateinfo);
        log.debug("update cmsAvtemplateinfo by pk end");
    }

    public void deleteCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("delete cmsAvtemplateinfo by pk starting...");
        super.delete("deleteCmsAvtemplateinfo", cmsAvtemplateinfo);
        log.debug("delete cmsAvtemplateinfo by pk end");
    }

    public CmsAvtemplateinfo getCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("query cmsAvtemplateinfo starting...");
        CmsAvtemplateinfo resultObj = (CmsAvtemplateinfo) super.queryForObject("getCmsAvtemplateinfo",
                cmsAvtemplateinfo);
        log.debug("query cmsAvtemplateinfo end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsAvtemplateinfo> getCmsAvtemplateinfoByCond(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("query cmsAvtemplateinfo by condition starting...");
        List<CmsAvtemplateinfo> rList = (List<CmsAvtemplateinfo>) super.queryForList(
                "queryCmsAvtemplateinfoListByCond", cmsAvtemplateinfo);
        log.debug("query cmsAvtemplateinfo by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsAvtemplateinfo by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsAvtemplateinfoListCntByCond", cmsAvtemplateinfo))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsAvtemplateinfo> rsList = (List<CmsAvtemplateinfo>) super.pageQuery(
                    "queryCmsAvtemplateinfoListByCond", cmsAvtemplateinfo, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsAvtemplateinfo by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsAvtemplateinfoListByCond", "queryCmsAvtemplateinfoListCntByCond",
                cmsAvtemplateinfo, start, pageSize, puEntity);
    }

    public List<CmsAvtemplateinfo> getTemplateByName(CmsAvtemplateinfo cmsAvtemplateinfo) throws DAOException
    {
        log.debug("query cmsAvtemplateinfo by condition starting...");
        List<CmsAvtemplateinfo> rList = (List<CmsAvtemplateinfo>) super.queryForList("queryTemplateListCntByCode",
                cmsAvtemplateinfo);
        log.debug("query cmsAvtemplateinfo by condition end");
        return rList;
    }
}

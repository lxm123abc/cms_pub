package com.zte.cms.EMBtask.ls;

import com.zte.cms.EMBtask.dao.IMovieTechauditResultDAO;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.cms.EMBtask.service.IMovieTechauditResultDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class MovieTechauditResultLS implements IMovieTechauditResultLS
{
    private Log log = SSBBus.getLog(getClass());
    private IMovieTechauditResultDS movieTechauditResultDS = null;

    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = movieTechauditResultDS.pageInfoQuery(movieTechauditResult, start, pageSize);
        }
        catch (DomainServiceException Ex)
        {
            log.error("ls exception:", Ex);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(Ex);
        }
        log.debug("get cmsEmbtask page info by condition end");
        return tableInfo;
    }

    public void setMovieTechauditResultDS(IMovieTechauditResultDS movieTechauditResultDS)
    {
        this.movieTechauditResultDS = movieTechauditResultDS;
    }
}

package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsChannelBufDAO
{
    /**
     * 新增CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DAOException dao异常
     */
    public void insertCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException;

    /**
     * 根据主键更新CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DAOException dao异常
     */
    public void updateCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException;

    /**
     * 根据主键删除CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @throws DAOException dao异常
     */
    public void deleteCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException;

    /**
     * 根据主键查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @return 满足条件的CmsChannelBuf对象
     * @throws DAOException dao异常
     */
    public CmsChannelBuf getCmsChannelBuf(CmsChannelBuf cmsChannelBuf) throws DAOException;

    /**
     * 根据条件查询CmsChannelBuf对象
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @return 满足条件的CmsChannelBuf对象集
     * @throws DAOException dao异常
     */
    public List<CmsChannelBuf> getCmsChannelBufByCond(CmsChannelBuf cmsChannelBuf) throws DAOException;

    /**
     * 根据条件分页查询CmsChannelBuf对象，作为查询条件的参数
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsChannelBuf对象，作为查询条件的参数
     * 
     * @param cmsChannelBuf CmsChannelBuf对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsChannelBuf cmsChannelBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
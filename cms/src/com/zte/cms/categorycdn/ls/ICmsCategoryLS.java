package com.zte.cms.categorycdn.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface ICmsCategoryLS
{

    /**
     * 
     * @param categorycdn Categorycdn
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     */
    public TableDataInfo queryCmsCategory(Categorycdn categorycdn, int start, int pageSize);

    /**
     * 
     * @param typeid String
     * @return Categorycdn
     */
    public Categorycdn queryByParentidCategory(String typeid);

    /**
     * 
     * @param typeid String
     * @return List<Categorycdn>
     */
    public List<Categorycdn> listMenuTreeByParentid(String typeid);

    /**
     * 
     * @param id Long
     * @return Categorycdn
     */
    public Categorycdn getCmsCategoryById(Long id);

    /**
     * 
     * @param categorycdn Categorycdn
     * @return Integer
     */
    public Integer queryByCategoryid(Categorycdn categorycdn);

    /**
     * 
     * @param categorycdn Categorycdn
     * @return Integer
     */
    public Integer queryByCategoryname(Categorycdn categorycdn);

    /**
     * 
     * @param categorycdn Categorycdn
     * @return HashMap
     */
    public HashMap insertCategory(Categorycdn categorycdn);

    /**
     * 
     * @param id Long
     * @return List<Categorycdn>
     */
    public List<Categorycdn> queryChildCmsCategory(Long id);

    /**
     * 
     * @param id Long
     * @return HashMap
     */
    public HashMap deleteCategory(Long id);

    /**
     * 
     * @param categorycdn Categorycdn
     * @return Long
     */
    public Long queryCategoryindexByCategoryname(Categorycdn categorycdn);

    /**
     * 
     * @param categorycdn Categorycdn
     * @return Categorycdn
     */
    public Categorycdn updateCategory(Categorycdn categorycdn);

 



    /**
     * 
     * @param index String
     * @return String
     */
    public String getIsChildexists(String index);

    /**
     * 
     * @param categoryid String
     * @return List<Categorycdn
     */
    public List<Categorycdn> listMenuTreeByParentidPub(String categoryid);
    public List<Categorycdn> listMenuTreeByParentidPubCk(String parentindex, String type, String obindex);
    public String bindBatchProgramTargetsys(
			List<Targetsystem> targetsystemList, List<Categorycdn> cmsProgramList)
			throws DomainServiceException;
    public TableDataInfo queryCtgPltSync(CSCntPlatformSync cntPlatformSync, int start,
			int pageSize);
  
    public HashMap publishCategoryPlt(List<CntPlatformSync> cntPlatformSynclist)
	throws Exception ;
    public HashMap publishCategoryTar(List<CntTargetSync> cntTargetSynclist)
	throws Exception;
    public HashMap cancelpublishCategoryTar(List<CntTargetSync> cntTargetSynclist)
	throws Exception;
    public HashMap cancelPublishCategoryPlt(
			List<CntPlatformSync> cntPlatformSynclist) throws Exception;
    public TableDataInfo queryCtgTarSync(CSCntTargetSync CScntTargetSync,
			int start, int pageSize);
	public HashMap deletecntsync(long syncindex);
    public List<Categorycdn> listMenuTreeByParentidcategoryAll(String categoryid);

    /**
     * 通过给定的内容，查询出改内容绑定的服务
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize) throws Exception;

    /**
     * 
     * @param service Service
     * @param start int 
     * @param pageSize int 
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception;
    
    
/*******************************海报发布管理***********************************/    
    
    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws Exception;
    
    
    
    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws Exception;
        
    
    
    public String batchPublishOrDelPublishcatPic(List<CategoryPictureMap> list,String operType, int type) throws Exception;
    
    public String publishcatPicMapPlatformSyn(String mapindex, String categoryid,String pictureid,int type,int synType) throws Exception;

    public String delPublishcatPicMapPlatformSyn(String mapindex, String categoryid,String pictureid,int type,int synType) throws Exception;
    
    public String publishcatPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;
    
    public String delPublishcatPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;

    public TableDataInfo getCategoryCntMapList(CategorycdnMap categorycdnMap, int start, int pageSize) throws Exception;
    
    /*******************************海报发布管理***********************************/    
}
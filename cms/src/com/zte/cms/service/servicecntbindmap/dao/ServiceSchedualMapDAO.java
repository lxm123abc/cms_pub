package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.dao.IServiceSchedualMapDAO;
import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceSchedualMapDAO extends DynamicObjectBaseDao implements IServiceSchedualMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException
    {
        log.debug("insert serviceSchedualMap starting...");
        super.insert("insertServiceSchedualMap", serviceSchedualMap);
        log.debug("insert serviceSchedualMap end");
    }

    public void updateServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException
    {
        log.debug("update serviceSchedualMap by pk starting...");
        super.update("updateServiceSchedualMap", serviceSchedualMap);
        log.debug("update serviceSchedualMap by pk end");
    }

    public void deleteServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException
    {
        log.debug("delete serviceSchedualMap by pk starting...");
        super.delete("deleteServiceSchedualMap", serviceSchedualMap);
        log.debug("delete serviceSchedualMap by pk end");
    }

    public ServiceSchedualMap getServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException
    {
        log.debug("query serviceSchedualMap starting...");
        ServiceSchedualMap resultObj = (ServiceSchedualMap) super.queryForObject("getServiceSchedualMap",
                serviceSchedualMap);
        log.debug("query serviceSchedualMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceSchedualMap> getServiceSchedualMapByCond(ServiceSchedualMap serviceSchedualMap)
            throws DAOException
    {
        log.debug("query serviceSchedualMap by condition starting...");
        List<ServiceSchedualMap> rList = (List<ServiceSchedualMap>) super.queryForList(
                "queryServiceSchedualMapListByCond", serviceSchedualMap);
        log.debug("query serviceSchedualMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceSchedualMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceSchedualMapListCntByCond", serviceSchedualMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceSchedualMap> rsList = (List<ServiceSchedualMap>) super.pageQuery(
                    "queryServiceSchedualMapListByCond", serviceSchedualMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceSchedualMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryServiceSchedualMapListByCond", "queryServiceSchedualMapListCntByCond",
                serviceSchedualMap, start, pageSize, puEntity);
    }

}
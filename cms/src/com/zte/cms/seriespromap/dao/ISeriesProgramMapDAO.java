package com.zte.cms.seriespromap.dao;

import java.util.List;

import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ISeriesProgramMapDAO
{
    /**
     * 新增SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DAOException dao异常
     */
    public void insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据主键更新SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DAOException dao异常
     */
    public void updateSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据主键删除SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @throws DAOException dao异常
     */
    public void deleteSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据主键查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return 满足条件的SeriesProgramMap对象
     * @throws DAOException dao异常
     */
    public SeriesProgramMap getSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据条件查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return 满足条件的SeriesProgramMap对象集
     * @throws DAOException dao异常
     */
    public List<SeriesProgramMap> getSeriesProgramMapByCond(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据条件分页查询SeriesProgramMap对象，作为查询条件的参数
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询SeriesProgramMap对象，作为查询条件的参数
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询SeriesProgramMap对象，作为查询条件的参数
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DAOException;

    /**
     * 根据连续主键返回其下面最大集序号值
     * 
     * @param seriesProgramMap 关联对象
     * @return 集序号最大值
     * @throws DAOException dao 异常
     */
    public int getMaxSequenceBySeriesindex(SeriesProgramMap seriesProgramMap) throws DAOException;

    /**
     * 根据条件分页查询SeriesProgramMap对象，作为查询条件的参数
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryseriesproTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DAOException;
    
    /**
     * 根据条件分页查询SeriesProgramMap对象，作为查询条件的参数
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryseriesproMap(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DAOException;    
    
    /**
     * 根据条件查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象
     * @return 满足条件的SeriesProgramMap对象集
     * @throws DAOException dao异常
     */
    public List<SeriesProgramMap> getnormalSeriesProTargetMapByCond(SeriesProgramMap seriesProgramMap) throws DAOException;
}
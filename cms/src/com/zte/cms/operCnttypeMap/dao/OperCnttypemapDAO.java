package com.zte.cms.operCnttypeMap.dao;

import java.util.List;

import com.zte.cms.operCnttypeMap.dao.IOperCnttypemapDAO;
import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class OperCnttypemapDAO extends DynamicObjectBaseDao implements IOperCnttypemapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertOperCnttypemap(OperCnttypemap operCnttypemap) throws DAOException
    {
        log.debug("insert operCnttypemap starting...");
        super.insert("insertOperCnttypemap", operCnttypemap);
        log.debug("insert operCnttypemap end");
    }

    public void updateOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException
    {
        log.debug("update operCnttypemap by conditions starting...");
        super.update("updateOperCnttypemapByCond", operCnttypemap);
        log.debug("update operCnttypemap by conditions end");
    }

    public void deleteOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException
    {
        log.debug("delete operCnttypemap by conditions starting...");
        super.delete("deleteOperCnttypemapByCond", operCnttypemap);
        log.debug("update operCnttypemap by conditions end");
    }

    @SuppressWarnings("unchecked")
    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException
    {
        log.debug("query operCnttypemap by condition starting...");
        List<OperCnttypemap> rList = (List<OperCnttypemap>) super.queryForList("queryOperCnttypemapListByCond",
                operCnttypemap);
        log.debug("query operCnttypemap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize) throws DAOException
    {
        log.debug("page query operCnttypemap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryOperCnttypemapListCntByCond", operCnttypemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<OperCnttypemap> rsList = (List<OperCnttypemap>) super.pageQuery("queryOperCnttypemapListByCond",
                    operCnttypemap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query operCnttypemap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryOperCnttypemapListByCond", "queryOperCnttypemapListCntByCond",
                operCnttypemap, start, pageSize, puEntity);
    }

}
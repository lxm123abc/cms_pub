package com.zte.cms.server.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class DiskInfo extends DynamicBaseObject
{
    private java.lang.String filesystem;
    private java.lang.String size;
    private java.lang.String used;
    private java.lang.String avail;
    private java.lang.String usepercentagy;
    private java.lang.String mountedon;

    public java.lang.String getFilesystem()
    {
        return filesystem;
    }

    public void setFilesystem(java.lang.String filesystem)
    {
        this.filesystem = filesystem;
    }

    public java.lang.String getSize()
    {
        return size;
    }

    public void setSize(java.lang.String size)
    {
        this.size = size;
    }

    public java.lang.String getUsed()
    {
        return used;
    }

    public void setUsed(java.lang.String used)
    {
        this.used = used;
    }

    public java.lang.String getAvail()
    {
        return avail;
    }

    public void setAvail(java.lang.String avail)
    {
        this.avail = avail;
    }

    public java.lang.String getUsepercentagy()
    {
        return usepercentagy;
    }

    public void setUsepercentagy(java.lang.String usepercentagy)
    {
        this.usepercentagy = usepercentagy;
    }

    public java.lang.String getMountedon()
    {
        return mountedon;
    }

    public void setMountedon(java.lang.String mountedon)
    {
        this.mountedon = mountedon;
    }

    public void initRelation()
    {
        this.addRelation("filesystem", "FILESYSTEM");
        this.addRelation("size", "SIZE");
        this.addRelation("used", "USED");
        this.addRelation("avail", "AVAIL");
        this.addRelation("usepercentagy", "USEPERCENTAGE");
        this.addRelation("mountedon", "MOUNTEDON");

    }

}

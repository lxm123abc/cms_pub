package com.zte.cms.categorycdn.ls;

import com.zte.umap.common.ResourceMgt;

public class CategoryConstants
{

    public static final String CMS_TARGETSYSTEM_RES_FILE_NAME = "cms_Targetsystem_resource";// 资源文件名
    public static final String CMS_TARGETSYSTEM_MSG_HAS_DELETED = "cms.targetsystem.msg.targetsystem.has.deleted";
    public static final String CMS_TARGETSYSTEM_MSG_HAS_BINDED = "cms.targetsystem.msg.targetsystem.has.binded";
    public static final String CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL = "cms.targetsystem.msg.oper.successful";
    public static final String CMS_TARGETSYSTEM_MSG_OPER_FAIL = "cms.targetsystem.msg.oper.fail";
    public static final String CMS_TARGETSYSTEM_MSG_PROGRAM_STATUS_INCORRECT = "cms.targetsystem.msg.program.status.incorrect";
    public static final String CMS_TARGETSYSTEM_MSG_TARGETSYSTEM = "cms.targetsystem.msg.targetsystem";
    public static final String CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED = "cms.targetsystem.msg.program.has.deleted";
    public static final String CMS_TARGETSYSTEM_MSG_SERIES_HAS_DELETED = "cms.targetsystem.msg.series.has.deleted";
    public static final String CMS_TARGETSYSTEM_MSG_MOVIE_HAS_DELETED = "cms.targetsystem.msg.movie.has.deleted";
    public static final String CMS_TARGETSYSTEM_MSG_MOVIE__STATUS_INCORRECT = "cms.targetsystem.msg.movie.status.incorrect";
	public static final String MGTTYPE_TARGETSYSTEM = "log.target.mgt";

	    public static final String MGTTYPE_TARGETSYSTEM_BIND = "log.targetbind.mgt";

	    public static final String TARGET_ID = "log.target.id";
	    
	    /**
	     * opertype 操作类型（String)
	     */
	    public static final String OPERTYPE_TARGET_ADD = "log.target.add";// 运营平台新增
	    public static final String OPERTYPE_TARGET_MOD = "log.target.modify";// 运营平台修改
	    public static final String OPERTYPE_TARGET_DEL = "log.target.delete";// 运营平台删除
	    public static final String OPERTYPE_TARGET_PROGRAM_BIND_DEL = "log.target.program.bind.delete";// 内容和运营平台绑定关系删除
	    public static final String OPERTYPE_TARGET_PROGRAM_BIND_ADD = "log.target.program.bind.add";// 内容和运营平台绑定关系新增

	    public static final String OPERTYPE_TARGET_SERIES_BIND_DEL = "log.target.series.bind.delete";// 连续剧剧头和运营平台绑定关系删除
	    public static final String OPERTYPE_TARGET_SERIES_BIND_ADD = "log.target.series.bind.add";// 连续剧剧头和运营平台绑定关系新增
	    public static final String OPERTYPE_TARGET_SERIESPROGRAM_BIND_DEL = "log.target.seriesprogram.bind.delete";// 连续剧单集和运营平台绑定关系删除
	    public static final String OPERTYPE_TARGET_SERIESPROGRAM_BIND_ADD = "log.target.seriesprogram.bind.add";// 连续剧单集和运营平台绑定关系新增
	    public static final String OPERTYPE_TARGET_SERIESMAPPING_BIND_DEL = "log.target.seriesmapping.bind.delete";// 连续剧单集mapping和运营平台关联关系删除
	    public static final String OPERTYPE_TARGET_SERIESMAPPING_BIND_ADD = "log.target.seriesmapping.bind.add";// 连续剧单集mapping和运营平台关联关系新增
	    /**
	     * opertypeinfo 操作类型（String)
	     */
	    public static final String OPERTYPE_TARGET_ADD_INFO = "log.target.add.info";
	    public static final String OPERTYPE_TARGET_MOD_INFO = "log.target.modify.info";
	    public static final String OPERTYPE_TARGET_DEL_INFO = "log.target.delete.info";
	    public static final String OPERTYPE_TARGET_PROGRAM_BIND_DEL_INFO = "log.target.program.bind.delete.info";
	    public static final String OPERTYPE_TARGET_PROGRAM_BIND_ADD_INFO = "log.target.program.bind.add.info";

	    public static final String OPERTYPE_TARGET_SERIES_BIND_DEL_INFO = "log.target.series.bind.delete.info";
	    public static final String OPERTYPE_TARGET_SERIES_BIND_ADD_INFO = "log.target.series.bind.add.info";
	    public static final String OPERTYPE_TARGET_SERIESPROGRAM_BIND_DEL_INFO = "log.target.seriesprogram.bind.delete.info";
	    public static final String OPERTYPE_TARGET_SERIESPROGRAM_BIND_ADD_INFO = "log.target.seriesprogram.bind.add.info";
	    public static final String OPERTYPE_TARGET_SERIESMAPPING_BIND_DEL_INFO = "log.target.seriesmapping.bind.delete.info";
	    public static final String OPERTYPE_TARGET_SERIESMAPPING_BIND_ADD_INFO = "log.target.seriesmapping.bind.add.info";
	   // 目标系统管理操作结果资源标识
	    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success"; // 操作成功
	    public static final String RESOURCE_OPERATION_FAIL = "operation.fail"; // 操作失败

    public static String setSourceFile()
    {
        ResourceMgt.addDefaultResourceBundle(CMS_TARGETSYSTEM_RES_FILE_NAME);
        return null;
    }
}

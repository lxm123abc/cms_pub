package com.zte.cms.channel.wkfw.dao;

import java.util.List;

import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.model.ChannelWkfwTaskForMe;

import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IChannelWkfwTaskForMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ChannelWkfwTaskForMe> queryChannelWkfwTaskForMeListByCond(
            ChannelWkfwCondition channelWkfwTaskForMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int querychannelWkfwTaskForMeListCntByCond(ChannelWkfwCondition channelWkfwTaskForMeCondition)
            throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start, int pageSize)
            throws DAOException;

    public abstract PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
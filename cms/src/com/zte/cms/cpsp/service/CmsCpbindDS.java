package com.zte.cms.cpsp.service;

import java.util.List;

import com.zte.cms.cpsp.dao.ICmsCpbindDAO;
import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CmsCpbindDS implements ICmsCpbindDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IPrimaryKeyGenerator primaryKeyGenerator = null;

    private ICmsCpbindDAO dao = null;

    public void setDao(ICmsCpbindDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("insert cmsCpbind starting...");
        try
        {
            String bindIndex = primaryKeyGenerator.getPrimarykey("cms_cpbind").toString();
            cmsCpbind.setBindindex(Long.parseLong(bindIndex));
            dao.insertCmsCpbind(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsCpbind end");
    }

    public void updateCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("update cmsCpbind by pk starting...");
        try
        {
            dao.updateCmsCpbind(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsCpbind by pk end");
    }

    public void updateCmsCpbindList(List<CmsCpbind> cmsCpbindList) throws DomainServiceException
    {
        log.debug("update cmsCpbindList by pk starting...");
        if (null == cmsCpbindList || cmsCpbindList.size() == 0)
        {
            log.debug("there is no datas in cmsCpbindList");
            return;
        }
        try
        {
            for (CmsCpbind cmsCpbind : cmsCpbindList)
            {
                dao.updateCmsCpbind(cmsCpbind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsCpbindList by pk end");
    }

    public void updateCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("update cmsCpbind by condition starting...");
        try
        {
            dao.updateCmsCpbindByCond(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsCpbind by condition end");
    }

    public void updateCmsCpbindListByCond(List<CmsCpbind> cmsCpbindList) throws DomainServiceException
    {
        log.debug("update cmsCpbindList by condition starting...");
        if (null == cmsCpbindList || cmsCpbindList.size() == 0)
        {
            log.debug("there is no datas in cmsCpbindList");
            return;
        }
        try
        {
            for (CmsCpbind cmsCpbind : cmsCpbindList)
            {
                dao.updateCmsCpbindByCond(cmsCpbind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsCpbindList by condition end");
    }

    public void removeCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("remove cmsCpbind by pk starting...");
        try
        {
            dao.deleteCmsCpbind(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsCpbind by pk end");
    }

    public void removeCmsCpbindList(List<CmsCpbind> cmsCpbindList) throws DomainServiceException
    {
        log.debug("remove cmsCpbindList by pk starting...");
        if (null == cmsCpbindList || cmsCpbindList.size() == 0)
        {
            log.debug("there is no datas in cmsCpbindList");
            return;
        }
        try
        {
            for (CmsCpbind cmsCpbind : cmsCpbindList)
            {
                dao.deleteCmsCpbind(cmsCpbind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsCpbindList by pk end");
    }

    public void removeCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("remove cmsCpbind by condition starting...");
        try
        {
            dao.deleteCmsCpbindByCond(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsCpbind by condition end");
    }

    public void removeCmsCpbindListByCond(List<CmsCpbind> cmsCpbindList) throws DomainServiceException
    {
        log.debug("remove cmsCpbindList by condition starting...");
        if (null == cmsCpbindList || cmsCpbindList.size() == 0)
        {
            log.debug("there is no datas in cmsCpbindList");
            return;
        }
        try
        {
            for (CmsCpbind cmsCpbind : cmsCpbindList)
            {
                dao.deleteCmsCpbindByCond(cmsCpbind);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsCpbindList by condition end");
    }

    public CmsCpbind getCmsCpbind(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("get cmsCpbind by pk starting...");
        CmsCpbind rsObj = null;
        try
        {
            rsObj = dao.getCmsCpbind(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsCpbindList by pk end");
        return rsObj;
    }

    public List<CmsCpbind> getCmsCpbindByCond(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("get cmsCpbind by condition starting...");
        List<CmsCpbind> rsList = null;
        try
        {
            rsList = dao.getCmsCpbindByCond(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsCpbind by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsCpbind page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsCpbind, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsCpbind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsCpbind page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsCpbind cmsCpbind, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsCpbind page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsCpbind, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsCpbind>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsCpbind page info by condition end");
        return tableInfo;
    }

    public void removeCpbindBySetCond(CmsCpbind cmsCpbind) throws DomainServiceException
    {
        log.debug("remove removeCpbindByCond by condition starting...");
        try
        {
            dao.removeCpbindBySetCond(cmsCpbind);
        }
        catch (DAOException daoEx)
        {
            daoEx.printStackTrace();
            log.error("dao exception:", daoEx);
        }
        log.debug("remove removeCpbindByCond by condition end");

    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }
}

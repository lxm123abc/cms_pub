<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    try {
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        purview.setServiceKey(serviceKey); 
        String operID = request.getParameter("operID") == null ? "" : (String)request.getParameter("operID");
        ArrayList list = new ArrayList();
        HashMap map = new HashMap();
        if (operID.length() > 0)
            list = purview.getRights(operID);
%>
<html>
<head>
<title><fmt:message key="operCurrentGroup.opermanage"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   function refresh () {
      document.view.submit();
   }
</script>
</head>
<body class="body-style1">
<form name="view" method="post" action="operGroupList.jsp">
<input type="hidden" name="operID" value="">
<table border="0" width="100%" class="text-default">
  <tr>
    <td width="100%"><fmt:message key="operGroupList.existedGrp"/></td>
  </tr>
  <tr>
    <td>
      <table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
        <tr class="table-title1">
          <td width="50%" height="24"><fmt:message key="operGroupList.grpname"/></td>
          <td width="50%" height="24"><fmt:message key="operGroupList.managesys"/></td>
        </tr>
<%
        for (int i = 0; i < list.size(); i++) {
            map = (HashMap)list.get(i);
%>
        <tr bgcolor="#FFFFFF">
          <td height="22"><%= (String)map.get("GRPSCRIPT") %>&nbsp;</td>
          <td height="22"><%= (String)map.get("DESCRIPTION") %>&nbsp;</td>
        </tr>
<%
        }
%>
      </table>
    </td>
  </tr>
</table>
</form>
<%
        if (operID.length() > 0) {
%>
<script language="javascript">
   //parent.currentFrame.document.view.operID.value = '<%= operID %>';
   //parent.currentFrame.refresh();
</script>
<%
        }
    }
    catch (Exception e) {
%>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
<%
    }
%>
</body>
<html>

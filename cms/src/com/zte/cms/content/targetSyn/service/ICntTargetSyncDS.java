package com.zte.cms.content.targetSyn.service;

import java.util.List;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICntTargetSyncDS
{
    /**
	 * 新增CntTargetSync对象
	 *
	 * @param cntTargetSync CntTargetSync对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException;
	  
    /**
	 * 更新CntTargetSync对象
	 *
	 * @param cntTargetSync CntTargetSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException;

    /**
	 * 批量更新CntTargetSync对象
	 *
	 * @param cntTargetSync CntTargetSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCntTargetSyncList( List<CntTargetSync> cntTargetSyncList )throws DomainServiceException;



	/**
	 * 删除CntTargetSync对象
	 *
	 * @param cntTargetSync CntTargetSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException;

	/**
	 * 批量删除CntTargetSync对象
	 *
	 * @param cntTargetSync CntTargetSync对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCntTargetSyncList( List<CntTargetSync> cntTargetSyncList )throws DomainServiceException;




	/**
	 * 查询CntTargetSync对象
	 
	 * @param cntTargetSync CntTargetSync对象
	 * @return CntTargetSync对象
	 * @throws DomainServiceException ds异常 
	 */
	 public CntTargetSync getCntTargetSync( CntTargetSync cntTargetSync )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询CntTargetSync对象 
	  * 
	  * @param cntTargetSync CntTargetSync对象
	  * @return 满足条件的CntTargetSync对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<CntTargetSync> getCntTargetSyncByCond( CntTargetSync cntTargetSync )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CntTargetSync cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
     
     /**
      * 根据objecttype、objectindex和targetindex查询对应的关联信息数目
      * @param cntTargetSync
      * @return
      * @throws DomainServiceException
      */
     public int countCntTargetSync4Object(CntTargetSync cntTargetSync)throws DomainServiceException;
     
     /**
      * 根据条件批量删除CntTargetSync对象
      * 
      * @param cntTargetSync CntTargetSync删除条件
      * @throws DomainServiceException ds异常
      */ 
     public void removeCntTargetSynListByObjindex( List<CntTargetSync> cntTargetSynList )throws DomainServiceException;  
     
     /**
      * 根据objectindex和elementid查询已知服务索引和VOD内容id的可以绑定的VOD内容CntTargetSync对象
      * 
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象集
      * @throws DomainServiceException ds异常
      */
     public List<CntTargetSync> getCommonTarget4ServiceAndProgram( CntTargetSync cntTargetSync )throws DomainServiceException;
     /**
      * 根据objectindex和elementid查询已知服务索引和连续剧内容id的可以绑定的VOD内容CntTargetSync对象
      * 
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象集
      * @throws DomainServiceException ds异常
      */
     public List<CntTargetSync> getCommonTarget4ServiceAndSeries( CntTargetSync cntTargetSync )throws DomainServiceException;
     /**
      * 根据objectindex和elementid查询已知服务索引和直播频道内容id的可以绑定的VOD内容CntTargetSync对象
      * 
      * @param cntTargetSync CntTargetSync对象
      * @return 满足条件的CntTargetSync对象集
      * @throws DomainServiceException ds异常
      */
     public List<CntTargetSync> getCommonTarget4ServiceAndChannel( CntTargetSync cntTargetSync )throws DomainServiceException;
	  /**
	  * 根据条件查询CntTargetSync对象 
	  * 
	  * @param cntTargetSync CntTargetSync对象
	  * @return 满足条件的CntTargetSync对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<CntTargetSync> getcntSynnormaltargetListBycond( CntTargetSync cntTargetSync )throws DomainServiceException;
}

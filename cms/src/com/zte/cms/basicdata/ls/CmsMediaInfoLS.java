package com.zte.cms.basicdata.ls;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoRequest;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponse;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormat;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatAudioInfo;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormatVideoInfo;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfo;
import com.zte.cms.basicdata.mediainfo.QueryMediaInfoServiceLocator;
import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;
import com.zte.cms.basicdata.service.ICmsExtractedvafileInfoDS;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.cms.content.movie.service.ICmsMovieBufDS;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.ssb.dao.DataIntegrityViolationException;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsMediaInfoLS implements ICmsMediaInfoLS
{

    private Log log = SSBBus.getLog(getClass());

    /**
     * ICmsExtractedvafileInfoDS组件
     */
    private ICmsExtractedvafileInfoDS cmsExtractedvafileInfoDS = null;
    /**
     * IIcmFileDS组件
     */
    private ICmsMovieDS cmsMovieDS = null;

    private ICmsMovieBufDS cmsMovieBufDS = null;
    /**
     * IIcmSourcefileDS组件
     */
    private IIcmSourcefileDS icmSourcefileDS = null;

    private IUsysConfigDS usysConfigDS = null;

    public void setCmsExtractedvafileInfoDS(ICmsExtractedvafileInfoDS cmsExtractedvafileInfoDS)
    {
        this.cmsExtractedvafileInfoDS = cmsExtractedvafileInfoDS;
    }

    public void setIcmSourcefileDS(IIcmSourcefileDS icmSourcefileDS)
    {
        this.icmSourcefileDS = icmSourcefileDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsMovieBufDS(ICmsMovieBufDS cmsMovieBufDS)
    {
        this.cmsMovieBufDS = cmsMovieBufDS;
    }
    public String extractMediaInfo(String fileindex, String sourcetype)
    {

        if (fileindex != null && !fileindex.equalsIgnoreCase("undefined") && sourcetype != null
                && !sourcetype.equalsIgnoreCase("undefined"))
        {
            try
            {
                UsysConfig usysConfig = new UsysConfig();
                String vaWebserviceAddr = null;
                String windowShareddir = null;

                // 获取视音频信息提取webservice地址
                log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
                usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.vainfoextract.webserviceaddr");
                log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
                if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
                {
                    //return "1:获取视音频信息提取webservice地址失败";
                	return ResourceMgt.findDefaultText("basicls.getwebserviceadr.failed");
                }
                else
                {
                    vaWebserviceAddr = usysConfig.getCfgvalue();
                }

                // 获取WINDOWS共享访问点
                log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
                usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.windows.share");
                log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
                if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
                {
                    //return "1:获取WINDOWS共享访问点失败";
                	return ResourceMgt.findDefaultText("basicls.getwindowspoint.failed");
                }
                else
                {
                    windowShareddir = usysConfig.getCfgvalue();
                    // windowShareddir = "\\\\10.129.36.35\\root\\";
                }

                if (Integer.parseInt(sourcetype) == 1 || Integer.parseInt(sourcetype) == 2)
                {
                    IcmSourcefile srcfile = new IcmSourcefile();
                    srcfile.setFileindex(Long.parseLong(fileindex));

                    srcfile = icmSourcefileDS.getIcmSourcefile(srcfile);
                    if (srcfile.getAddresspath() != null && !srcfile.getAddresspath().equalsIgnoreCase(""))
                    {
                        QueryMediaInfoRequest req = new QueryMediaInfoRequest();
                        req.setFilePath(windowShareddir + (srcfile.getAddresspath()).replaceAll("/+", "\\\\"));
                        CmsExtractedvafileInfo rtnfileinfo = sendGetMediaInfoRequest(req, vaWebserviceAddr);

                        if (rtnfileinfo != null)
                        {
                            CmsExtractedvafileInfo fileinfo = new CmsExtractedvafileInfo();
                            fileinfo.setFileindex(Long.parseLong(fileindex));
                            cmsExtractedvafileInfoDS.removeCmsExtractedvafileInfo(fileinfo);
                            rtnfileinfo.setFileindex(Long.parseLong(fileindex));
                            rtnfileinfo.setSourcetype(Integer.parseInt(sourcetype));
                            cmsExtractedvafileInfoDS.insertCmsExtractedvafileInfo(rtnfileinfo);
                        }
                        else
                        {
                            //return "1:获取媒体信息失败";
                            return ResourceMgt.findDefaultText("basicls.getmessage.failed");
                        }

                    }
                    else
                    {
                        //return "1:文件路径为空";
                    	return ResourceMgt.findDefaultText("basicls.filepath.void");
                    }
                }
                else if (Integer.parseInt(sourcetype) == 3)
                {
                    CmsMovie cmsMovie = new CmsMovie();
                    cmsMovie.setMovieindex(Long.parseLong(fileindex));
                    cmsMovie = cmsMovieDS.getCmsMovie(cmsMovie);
                    if (cmsMovie.getFileurl() != null && !cmsMovie.getFileurl().equalsIgnoreCase(""))
                    {
                        QueryMediaInfoRequest req = new QueryMediaInfoRequest();
                        req.setFilePath((windowShareddir + cmsMovie.getFileurl()).replaceAll("/+", "\\\\"));
                        CmsExtractedvafileInfo rtnfileinfo = sendGetMediaInfoRequest(req, vaWebserviceAddr);
                        if (rtnfileinfo != null)
                        {
                            CmsExtractedvafileInfo fileinfo = new CmsExtractedvafileInfo();
                            fileinfo.setFileindex(Long.parseLong(fileindex));
                            cmsExtractedvafileInfoDS.removeCmsExtractedvafileInfo(fileinfo);
                            rtnfileinfo.setFileindex(Long.parseLong(fileindex));
                            rtnfileinfo.setSourcetype(Integer.parseInt(sourcetype));
                            cmsExtractedvafileInfoDS.insertCmsExtractedvafileInfo(rtnfileinfo);
                        }
                        else
                        {
                            //return "1:获取媒体信息失败";
                            return ResourceMgt.findDefaultText("basicls.getmessage.failed");
                        }
                    }
                    else
                    {
                        //return "1:文件路径为空";
                    	return ResourceMgt.findDefaultText("basicls.filepath.void");
                    }
                }
                else
                {
                    //return "1:提取视音频信息的文件类型不正确";
                	return ResourceMgt.findDefaultText("basicls.fileforaudio.wrong");
                }
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block

                //return "1:提取的视音频信息入库失败";
            	return ResourceMgt.findDefaultText("basicls.pushvideomessage.failed");
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                if (e instanceof DataIntegrityViolationException)
                {
                    //return "1:提取的视音频信息入库失败";
                	return ResourceMgt.findDefaultText("basicls.pushvideomessage.failed");
                }
                else
                {
                    //return "1:提取视音频信息失败";
                	return ResourceMgt.findDefaultText("basicls.getaudioandvideo.failed");
                }
            }

        }

        //return "0:获取媒体信息成功";
        return ResourceMgt.findDefaultText("basicls.getmedia.secceed");
    }

    private CmsExtractedvafileInfo sendGetMediaInfoRequest(QueryMediaInfoRequest req, String url)
    {
        QueryMediaInfoResponse rps = new QueryMediaInfoResponse();
        CmsExtractedvafileInfo fileinfo = new CmsExtractedvafileInfo();

        try
        {
            URL portAddress = new URL(url);
            QueryMediaInfoServiceLocator locator = new QueryMediaInfoServiceLocator();
            QueryMediaInfoPort port = locator.getQueryMediaInfoServiceHttpPort(portAddress);
            log.debug("#########################QueryMediaInfoRequest start ##############################");
            log.debug("QueryMediaInfoRequest:FilePath：" + req.getFilePath());
            log.debug("#########################QueryMediaInfoRequest end   ##############################");
            rps = port.queryMediaInfo(req);
            log.debug("#########################QueryMediaInfoResponse start ##############################");

            if (rps != null && rps.getStatus() == 0)
            {

                QueryMediaInfoResponseAVFormat vaFormat = rps.getAVFormat();
                if (null != vaFormat)
                {
                    fileinfo.setStreammediatype(vaFormat.getStreamMediaType());
                    fileinfo.setStreammediasubtype(vaFormat.getStreamMediaSubType());
                    fileinfo.setIsincludevideo(vaFormat.getIsIncludeVideo());
                    fileinfo.setIsincludeaudio(vaFormat.getIsIncludeAudio());
                    log.debug("QueryMediaInfoResponseAVFormat:StreamMediaType:" + vaFormat.getStreamMediaType());
                    log.debug("QueryMediaInfoResponseAVFormat:StreamMediaSubType:" + vaFormat.getStreamMediaSubType());
                    log.debug("QueryMediaInfoResponseAVFormat:IsIncludeVideo:" + vaFormat.getIsIncludeVideo());
                    log.debug("QueryMediaInfoResponseAVFormat:getIsIncludeAudio:" + vaFormat.getIsIncludeAudio());
                }

                QueryMediaInfoResponseAVFormatVideoInfo videoInfo = vaFormat.getVideoInfo();
                if (null != videoInfo)
                {
                    fileinfo.setVideocodingformat(videoInfo.getVideoCodingFormat());
                    fileinfo.setVideosubtype(videoInfo.getVideoSubType());
                    fileinfo.setCxsize(videoInfo.getCXSize());
                    fileinfo.setCysize(videoInfo.getCYSize());
                    fileinfo.setStandardrate(videoInfo.getStandardRate());
                    fileinfo.setStandardscale(videoInfo.getStandardScale());
                    fileinfo.setScanmode(videoInfo.getScanMode());
                    fileinfo.setColorformat(videoInfo.getColorFormat());
                    fileinfo.setVideobitrate(videoInfo.getVideoBitrate());
                    fileinfo.setIsconstantrate(videoInfo.getIsConstantRate());
                    fileinfo.setGopsize(videoInfo.getGOPSize());
                    fileinfo.setReferenceperiod(videoInfo.getReferencePeriod());
                    fileinfo.setIsy16235(videoInfo.getIsY16_235());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:VideoCodingFormat:"
                            + videoInfo.getVideoCodingFormat());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:VideoSubType:" + videoInfo.getVideoSubType());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:CXSize:" + videoInfo.getCXSize());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:CYSize:" + videoInfo.getCYSize());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:StandardRate:" + videoInfo.getStandardRate());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:StandardScale" + videoInfo.getStandardScale());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:ScanMode:" + videoInfo.getScanMode());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:ColorFormat:" + videoInfo.getColorFormat());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:VideoBitrate:" + videoInfo.getVideoBitrate());
                    log
                            .debug("QueryMediaInfoResponseAVFormatVideoInfo:IsConstantRate:"
                                    + videoInfo.getIsConstantRate());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:GOPSize:" + videoInfo.getGOPSize());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:ReferencePeriod:"
                            + videoInfo.getReferencePeriod());
                    log.debug("QueryMediaInfoResponseAVFormatVideoInfo:IsY16_235:" + videoInfo.getIsY16_235());
                }

                QueryMediaInfoResponseAVFormatAudioInfo audioInfo = vaFormat.getAudioInfo();
                if (null != audioInfo)
                {
                    fileinfo.setAudiocodingformat(audioInfo.getAudioCodingFormat());
                    fileinfo.setAudiosubtype(audioInfo.getAudioSubType());
                    fileinfo.setChannels(audioInfo.getChannels());
                    fileinfo.setAudiodatarate(audioInfo.getAudioDataRate());
                    fileinfo.setAudiosamplingfreq(audioInfo.getAudioSamplingFreq());
                    fileinfo.setAudiobitdepth(audioInfo.getAudioBitDepth());
                    fileinfo.setBlockalign(audioInfo.getBlockAlign());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:AudioCodingFormat:"
                            + audioInfo.getAudioCodingFormat());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:AudioSubType:" + audioInfo.getAudioSubType());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:Channels:" + audioInfo.getChannels());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:AudioDataRate:" + audioInfo.getAudioDataRate());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:AudioSamplingFreq:"
                            + audioInfo.getAudioSamplingFreq());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:BitDepth:" + audioInfo.getAudioBitDepth());
                    log.debug("QueryMediaInfoResponseAVFormatAudioInfo:BlockAlign:" + audioInfo.getBlockAlign());
                }

                QueryMediaInfoResponseFileInfo info = rps.getFileInfo();
                if (null != info)
                {
                    fileinfo.setFilesize(info.getFileSize());
                    fileinfo.setAudiosamples(info.getAudioSamples());
                    fileinfo.setVideoframes(info.getVideoFrames());
                    log.debug("QueryMediaInfoResponseFileInfo:FileSize:" + info.getFileSize());
                    log.debug("QueryMediaInfoResponseFileInfo:AudioSamples:" + info.getAudioSamples());
                    log.debug("QueryMediaInfoResponseFileInfo:VideoFrames:" + info.getVideoFrames());
                    // fileinfo.setExattribute1("");
                    // fileinfo.setExattribute2("");
                    // fileinfo.setExattribute3("");
                    // fileinfo.setExattribute4("");
                    // fileinfo.setExattribute5("");
                }
                log.debug("#########################QueryMediaInfoResponse end  ##############################");
            }
            else
            {
                log.debug("#########################QueryMediaInfoResponse end  ##############################");
                return null;
            }

        }
        catch (RemoteException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        catch (ServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return null;
        }
        catch (NumberFormatException e1)
        {

            e1.printStackTrace();
            return null;
        }

        return fileinfo;
    }

    public CmsExtractedvafileInfo getVAInfo(String fileindex)
    {
        CmsExtractedvafileInfo vafileinfo = null;
        CmsExtractedvafileInfo cond = new CmsExtractedvafileInfo();
        cond.setFileindex(Long.parseLong(fileindex));
        try
        {
            vafileinfo = cmsExtractedvafileInfoDS.getCmsExtractedvafileInfo(cond);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return vafileinfo;
    }

    /**
     * 内容审核的时候点击查看视音频信息时提取视音频信息
     */
    public Object getMovieInfo(String fileindex, String sourcetype)
    {
        // TODO Auto-generated method stub
        CmsExtractedvafileInfo vafileinfo = this.getVAInfo(fileindex);
        if (vafileinfo != null)
        {
            return vafileinfo;
        }
        else
        {
            String str = this.extractMediaInfo(fileindex, sourcetype);
            //if (str.equals("0:获取媒体信息成功"))
            if (str.equals(ResourceMgt.findDefaultText("basicls.getmedia.secceed")))
            {
                return this.getVAInfo(fileindex);
            }
            else
            {
                return str;
            }
        }
    }

    /**
     * 内容审核的时候点击查看视音频信息时提取视音频信息
     */
    public Object getMovieBufInfo(String fileindex, String sourcetype)
    {
        // TODO Auto-generated method stub
        CmsExtractedvafileInfo vafileinfo = this.getVAInfo(fileindex);
        if (vafileinfo != null)
        {
            return vafileinfo;
        }
        else
        {
            String str = this.extractMediaBufInfo(fileindex, sourcetype);
            //if (str.equals("0:获取媒体信息成功"))
            if (str.equals(ResourceMgt.findDefaultText("basicls.getmedia.secceed")))
            {
                return this.getVAInfo(fileindex);
            }
            else
            {
                return str;
            }
        }
    }

    public String extractMediaBufInfo(String fileindex, String sourcetype)
    {

        if (fileindex != null && !fileindex.equalsIgnoreCase("undefined") && sourcetype != null
                && !sourcetype.equalsIgnoreCase("undefined"))
        {
            try
            {
                UsysConfig usysConfig = new UsysConfig();
                String vaWebserviceAddr = null;
                String windowShareddir = null;

                // 获取视音频信息提取webservice地址
                log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
                usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.vainfoextract.webserviceaddr");
                log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
                if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
                {
                    //return "1:获取视音频信息提取webservice地址失败";
                	return ResourceMgt.findDefaultText("basicls.getwebserviceadr.failed");

                }
                else
                {
                    vaWebserviceAddr = usysConfig.getCfgvalue();
                }

                // 获取WINDOWS共享访问点
                log.debug("UsysConfigDS: getUsysConfigByCfgkey starting...");
                usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.windows.share");
                log.debug("UsysConfigDS: getUsysConfigByCfgkey end");
                if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
                {
                    //return "1:获取WINDOWS共享访问点失败";
                	return ResourceMgt.findDefaultText("basicls.getwindowspoint.failed");
                }
                else
                {
                    windowShareddir = usysConfig.getCfgvalue();
                }
                if (Integer.parseInt(sourcetype) == 3)
                {
                    CmsMovieBuf cmsMovieBuf = new CmsMovieBuf();
                    cmsMovieBuf.setMovieindex(Long.parseLong(fileindex));
                    cmsMovieBuf = cmsMovieBufDS.getCmsMovieBuf(cmsMovieBuf);
                    if (cmsMovieBuf.getFileurl() != null && !cmsMovieBuf.getFileurl().equalsIgnoreCase(""))
                    {
                        QueryMediaInfoRequest req = new QueryMediaInfoRequest();
                        req.setFilePath((windowShareddir + cmsMovieBuf.getFileurl()).replaceAll("/+", "\\\\"));
                        CmsExtractedvafileInfo rtnfileinfo = sendGetMediaInfoRequest(req, vaWebserviceAddr);
                        if (rtnfileinfo != null)
                        {
                            CmsExtractedvafileInfo fileinfo = new CmsExtractedvafileInfo();
                            fileinfo.setFileindex(Long.parseLong(fileindex));
                            cmsExtractedvafileInfoDS.removeCmsExtractedvafileInfo(fileinfo);
                            rtnfileinfo.setFileindex(Long.parseLong(fileindex));
                            rtnfileinfo.setSourcetype(Integer.parseInt(sourcetype));
                            cmsExtractedvafileInfoDS.insertCmsExtractedvafileInfo(rtnfileinfo);
                        }
                        else
                        {
                            //return "1:获取媒体信息失败";
                        	  return ResourceMgt.findDefaultText("basicls.getmessage.failed");
                        }
                    }
                    else
                    {
                        //return "1:文件路径为空";
                    	return ResourceMgt.findDefaultText("basicls.filepath.void");
                    }
                }
                else
                {
                    //return "1:提取视音频信息的文件类型不正确";
                	return ResourceMgt.findDefaultText("basicls.fileforaudio.wrong");
                }
            }
            catch (DomainServiceException e)
            {

                //return "1:提取的视音频信息入库失败";
            	return ResourceMgt.findDefaultText("basicls.pushvideomessage.failed");
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                if (e instanceof DataIntegrityViolationException)
                {
                   // return "1:提取的视音频信息入库失败";
                	return ResourceMgt.findDefaultText("basicls.pushvideomessage.failed");
                }
                else
                {
                   // return "1:提取视音频信息失败";
                	return ResourceMgt.findDefaultText("basicls.getaudioandvideo.failed");
                }
            }

        }
        //return "0:获取媒体信息成功";
        return ResourceMgt.findDefaultText("basicls.getmedia.secceed");
    }
}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.Picture;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IPictureDAO
{
    /**
     * 新增Picture对象
     * 
     * @param picture Picture对象
     * @throws DAOException dao异常
     */
    public void insertPicture(Picture picture) throws DAOException;

    /**
     * 根据主键更新Picture对象
     * 
     * @param picture Picture对象
     * @throws DAOException dao异常
     */
    public void updatePicture(Picture picture) throws DAOException;

    /**
     * 根据主键删除Picture对象
     * 
     * @param picture Picture对象
     * @throws DAOException dao异常
     */
    public void deletePicture(Picture picture) throws DAOException;

    /**
     * 根据主键查询Picture对象
     * 
     * @param picture Picture对象
     * @return 满足条件的Picture对象
     * @throws DAOException dao异常
     */
    public Picture getPicture(Picture picture) throws DAOException;

    /**
     * 根据条件查询Picture对象
     * 
     * @param picture Picture对象
     * @return 满足条件的Picture对象集
     * @throws DAOException dao异常
     */
    public List<Picture> getPictureByCond(Picture picture) throws DAOException;

    /**
     * 根据条件分页查询Picture对象，作为查询条件的参数
     * 
     * @param picture Picture对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Picture picture, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryService(Long relativeIndex, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryCatagory(Long relativeIndex, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryProgram(Long relativeIndex, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQuerySeries(Long relativeIndex, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryChannel(Long relativeIndex, int start, int pageSize) throws DAOException;

    public PageInfo pageInfoQueryCast(Long relativeIndex, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Picture对象，作为查询条件的参数
     * 
     * @param picture Picture对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序控制参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Picture picture, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public PageInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

    public List<Picture> getPictureByServiceIndex(Long index) throws DAOException;

    public List<Picture> getPictureByCatagoryIndex(Long index) throws DAOException;

    public List<Picture> getPictureByProgramIndex(Long index) throws DAOException;

    public List<Picture> getPictureBySeriesIndex(Long index) throws DAOException;

    public List<Picture> getPictureByChannelIndex(Long index) throws DAOException;

    public List<Picture> getPictureByCastIndex(Long index) throws DAOException;

    public List<Picture> queryChannelPictureListByChnanelindex(Long index) throws DAOException;

}
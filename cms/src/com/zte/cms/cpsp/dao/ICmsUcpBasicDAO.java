package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cppr.model.CmsCppr;
import com.zte.cms.cppr.model.CmsUcpBasic;
import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.cpsp.common.model.UcpBasic;

public interface ICmsUcpBasicDAO {
    /**
     * 根据条件分页查询UcpBasic对象，作为查询条件的参数
     *
     * @param ucpBasic ucpBasic对象
     * @param start    起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询UcpBasic对象，作为查询条件的参数
     *
     * @param start    起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     * @paramUcpBasic ucpBasic对象
     */
    public PageInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public List<CmsUcpBasic> getCmsUcpBasicListByServiceKey(CmsUcpBasic cmsUcpBasic) throws DAOException;

    public void deleteCmsOperPower(CmsCppr cmsCppr) throws DAOException;

    void insertUcpBasic(UcpBasicModel model) throws DAOException;

    UcpBasicModel getUcpBasicByIndex(UcpBasicModel model);

    public void deleteUcpBasicFile(UcpBasicModel model);

    void updateUcpBasic(UcpBasicModel model) throws DAOException;

    List<UcpBasicModel> checkQualificationDoc(String operid);

}
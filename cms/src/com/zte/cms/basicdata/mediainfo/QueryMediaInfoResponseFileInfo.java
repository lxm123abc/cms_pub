/**
 * QueryMediaInfoResponseFileInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoResponseFileInfo implements java.io.Serializable
{
    /* 文件大小 */
    private long fileSize;

    /* 音频采样总数 */
    private java.lang.Long audioSamples;

    /* 视频帧数 */
    private java.lang.Long videoFrames;

    /* 扩展属性 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute[] extendAttribute;

    public QueryMediaInfoResponseFileInfo()
    {
    }

    public QueryMediaInfoResponseFileInfo(long fileSize, java.lang.Long audioSamples, java.lang.Long videoFrames,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute[] extendAttribute)
    {
        this.fileSize = fileSize;
        this.audioSamples = audioSamples;
        this.videoFrames = videoFrames;
        this.extendAttribute = extendAttribute;
    }

    /**
     * Gets the fileSize value for this QueryMediaInfoResponseFileInfo.
     * 
     * @return fileSize * 文件大小
     */
    public long getFileSize()
    {
        return fileSize;
    }

    /**
     * Sets the fileSize value for this QueryMediaInfoResponseFileInfo.
     * 
     * @param fileSize * 文件大小
     */
    public void setFileSize(long fileSize)
    {
        this.fileSize = fileSize;
    }

    /**
     * Gets the audioSamples value for this QueryMediaInfoResponseFileInfo.
     * 
     * @return audioSamples * 音频采样总数
     */
    public java.lang.Long getAudioSamples()
    {
        return audioSamples;
    }

    /**
     * Sets the audioSamples value for this QueryMediaInfoResponseFileInfo.
     * 
     * @param audioSamples * 音频采样总数
     */
    public void setAudioSamples(java.lang.Long audioSamples)
    {
        this.audioSamples = audioSamples;
    }

    /**
     * Gets the videoFrames value for this QueryMediaInfoResponseFileInfo.
     * 
     * @return videoFrames * 视频帧数
     */
    public java.lang.Long getVideoFrames()
    {
        return videoFrames;
    }

    /**
     * Sets the videoFrames value for this QueryMediaInfoResponseFileInfo.
     * 
     * @param videoFrames * 视频帧数
     */
    public void setVideoFrames(java.lang.Long videoFrames)
    {
        this.videoFrames = videoFrames;
    }

    /**
     * Gets the extendAttribute value for this QueryMediaInfoResponseFileInfo.
     * 
     * @return extendAttribute * 扩展属性
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute[] getExtendAttribute()
    {
        return extendAttribute;
    }

    /**
     * Sets the extendAttribute value for this QueryMediaInfoResponseFileInfo.
     * 
     * @param extendAttribute * 扩展属性
     */
    public void setExtendAttribute(
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute[] extendAttribute)
    {
        this.extendAttribute = extendAttribute;
    }

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute getExtendAttribute(int i)
    {
        return this.extendAttribute[i];
    }

    public void setExtendAttribute(int i,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfoExtendAttribute _value)
    {
        this.extendAttribute[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj)
    {
        if (!(obj instanceof QueryMediaInfoResponseFileInfo))
            return false;
        QueryMediaInfoResponseFileInfo other = (QueryMediaInfoResponseFileInfo) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null)
        {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && this.fileSize == other.getFileSize()
                && ((this.audioSamples == null && other.getAudioSamples() == null) || (this.audioSamples != null && this.audioSamples
                        .equals(other.getAudioSamples())))
                && ((this.videoFrames == null && other.getVideoFrames() == null) || (this.videoFrames != null && this.videoFrames
                        .equals(other.getVideoFrames())))
                && ((this.extendAttribute == null && other.getExtendAttribute() == null) || (this.extendAttribute != null && java.util.Arrays
                        .equals(this.extendAttribute, other.getExtendAttribute())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode()
    {
        if (__hashCodeCalc)
        {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getFileSize()).hashCode();
        if (getAudioSamples() != null)
        {
            _hashCode += getAudioSamples().hashCode();
        }
        if (getVideoFrames() != null)
        {
            _hashCode += getVideoFrames().hashCode();
        }
        if (getExtendAttribute() != null)
        {
            for (int i = 0; i < java.lang.reflect.Array.getLength(getExtendAttribute()); i++)
            {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtendAttribute(), i);
                if (obj != null && !obj.getClass().isArray())
                {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            QueryMediaInfoResponseFileInfo.class, true);

    static
    {
        typeDesc.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>QueryMediaInfoResponse>FileInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileSize");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "FileSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioSamples");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioSamples"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videoFrames");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "VideoFrames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendAttribute");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "ExtendAttribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS",
                ">>>QueryMediaInfoResponse>FileInfo>ExtendAttribute"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}

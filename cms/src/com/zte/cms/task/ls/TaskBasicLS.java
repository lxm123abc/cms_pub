package com.zte.cms.task.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.cms.task.model.TaskCmsauditRecord;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import org.apache.commons.lang.StringUtils;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskFunction;
import com.zte.cms.task.service.ITaskBasicDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;

import javax.servlet.http.HttpServletResponse;

public class TaskBasicLS extends DynamicObjectBaseDS implements ITaskBasicLS
{
	private Log log = SSBBus.getLog(getClass());
    private ITaskBasicDS cmsTaskBasicDS = null;

    public void setCmsTaskBasicDS(ITaskBasicDS cmsTaskBasicDS)
    {
        this.cmsTaskBasicDS = cmsTaskBasicDS;
    }

    /**
     * 查询CP信息
     *
     * @param taskBasic 查询条件对象
     * @param arg1 开始页数
     * @param arg2 条数
     * @return CP信息
     */
    public TableDataInfo pageInfoQuery(TaskCmsaudit taskBasic, int arg1, int arg2)
    {
        log.debug("pageInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try
        {
    		PageUtilEntity puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("cpid");
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");

            String startCreateTime = taskBasic.getCreatetime();
            if (StringUtils.isNotEmpty(startCreateTime))
            {
                startCreateTime = startCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                taskBasic.setCreatetime(startCreateTime);
            }

            String endCreateTime = taskBasic.getCreatetimeend();
            if (StringUtils.isNotEmpty(endCreateTime))
            {
                endCreateTime = endCreateTime.trim().replace(" ", "").replace(":", "").replace("-", "");
                taskBasic.setCreatetimeend(endCreateTime);
            }

            //判断操作员是否拥有系统管理员权限，如果有给sagid=1，否则sadid=operid。
            //sagid=1的时候，查询所有的cp信息，否者只查询操作员所属的cp
            String sql = "select opergrpid from  zxinsys.oper_rights where operid = " + operinfo.getOperid();
            DbUtil db = new DbUtil();
            List rtnlist = db.getQuery(sql);
            for (int j = 0; j < rtnlist.size(); j++)
            {
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                //判断操作员下是否有系统管理组权限
                if (tmpMap.get("opergrpid").toString().equals("1000"))
                {
                    break;
                }
            }
            if (taskBasic.getTaskname()!=null && !taskBasic.getTaskname().equals(""))
            {
                taskBasic.setTaskname(EspecialCharMgt.conversion(taskBasic.getTaskname()));
            }
            taskBasic.setOperid(operinfo.getOperid());
            dataInfo = cmsTaskBasicDS.pageInfoQuery(taskBasic, arg1, arg2,puEntity);

        }
        catch (DomainServiceException e)
        {
            log.error("dao exception:" + e.getMessage());
        }
        catch (NumberFormatException e)
        {
            log.error("dao exception:" + e.getMessage());
        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return dataInfo;

    }

    /**
     * 增加
     */
    public String insertTaskmap(String uploadAccessorialContral, TaskCmsaudit taskBasic) throws DomainServiceException
    {

        log.info("insertTaskmap start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        String taskid = "";
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        int cpid = loginUser.getCpindex();
        TaskCmsaudit taskBasicobj = new TaskCmsaudit();
        //taskmapobj.setOperatorid(operId);
        taskBasicobj.setCpid(cpid+"");
        taskBasicobj.setCreatorid(Integer.parseInt(operId));
        taskBasicobj.setTaskname(taskBasic.getTaskname());
        taskBasicobj.setTaskdesc(taskBasic.getTaskdesc());

        try
        {
            //if (taskmap.getMessagetype() == 1)
                log.info("insertTaskmap for announcement ");

                taskid = cmsTaskBasicDS.insertTask(taskBasicobj);
                CommonLogUtil.insertOperatorLog(taskid,
                                                "log.task.infomgr",
                                                "log.task.infomgr.add",
                                                "log.task.infomgr.add.info",
                                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                rtnInfo.setFlag("0");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error("Error occurred in insertTaskmap method", e);
            throw new DomainServiceException();
        }
        log.info("insertTaskmap end ");
        return rtnInfo.toString();
    }

    public String updateTaskmap(String uploadAccessorialContral, TaskCmsaudit taskmap) throws DomainServiceException
    {
    	ReturnInfo rtnInfo = new ReturnInfo();
        log.info("updateNoticemap start....................");
        TaskCmsaudit tempnotice = new TaskCmsaudit();
        tempnotice.setTaskid(taskmap.getTaskid());
        tempnotice = cmsTaskBasicDS.getTaskmap(taskmap);
        if (tempnotice == null)
        {
        	rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("notice.modify.notexist"));//"操作成功"); 
            return rtnInfo.toString();
        }

        cmsTaskBasicDS.updateTaskmap(taskmap);
        //noticeDS.updateAttach(taskmap);

        // 写操作日志//
        CommonLogUtil.insertOperatorLog(taskmap.getTaskid() + "",
                                        "log.task.infomgr",
                                        "log.task.infomgr.modify",
                                        "log.task.infomgr.modify.info",
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        log.info("updateTaskmap end......................");
        rtnInfo.setFlag("0");
        rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功"); 
        return rtnInfo.toString();
    }


    public TaskCmsaudit getTaskmap(Integer taskid)
    {
    	TaskCmsaudit taskmap = new TaskCmsaudit();
        taskmap.setTaskid(taskid);
        try {
            taskmap = cmsTaskBasicDS.getTaskmap(taskmap);
            taskmap.setCreatorname(UserInfoUtils.getOperAllNameByOperId(taskmap.getCreatorid()));
        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("Error occurred in getTaskmap method:", e);
            return null;
        }
        return taskmap;
    }

    public void deleteAttach(String attachid){
    	 try
         {
    		 cmsTaskBasicDS.deleteAttach(attachid);
         }
         catch (DomainServiceException e)
         {
             e.printStackTrace();
             log.error("Error occurred in getTaskmap method:", e);
         }
    }
    public List<TaskCmsaudit> getattchList(Integer taskid)
    {
    	TaskCmsaudit taskmap = new TaskCmsaudit();
        taskmap.setTaskid(taskid);
        List<TaskCmsaudit> uploadList = null;
        try
        {
        	uploadList = cmsTaskBasicDS.getattchList(taskmap);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getTaskmap method:", e);
            return null;
        }
        return uploadList;
    }

    /**
     * 删除消息
     */
    public String deleteTaskBasic(TaskCmsaudit taskmap) throws DomainServiceException
    {
    	log.info(" deleteTaskBasic start ");
        ReturnInfo rtnInfo;
    	 try
         {
    		 TaskCmsaudit taskmap2 = cmsTaskBasicDS.getTaskmap(taskmap);
             rtnInfo = new ReturnInfo();
             if (taskmap2 == null)
             {
                 rtnInfo.setFlag("1");
                 rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("msg.info.records.null"));
                 return rtnInfo.toString();
             }
             List<TaskCmsaudit> uploadList = cmsTaskBasicDS.getattchList(taskmap);
             if (uploadList.size() > 0)
             {
            	 for (int i = 0; i < uploadList.size(); i++) {
            		 TaskCmsaudit taskattach = uploadList.get(i);
            		 cmsTaskBasicDS.deleteAttach(taskattach.getAttachid());
				}
             }
             cmsTaskBasicDS.deleteTaskBasic(taskmap2);
             CommonLogUtil.insertOperatorLog(taskmap.getTaskid() + "",
                                             "log.task.infomgr",
                                             "log.task.infomgr.del",
                                             "log.task.infomgr.del.info",
                                             CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

             rtnInfo.setFlag("0");
             rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");

         }
         catch (Exception e)
         {
             e.printStackTrace();
             log.error("Error occurred in deleteTaskBasic method:", e);
             throw new DomainServiceException();
         }

         log.info(" deleteTaskBasic end ");
         return rtnInfo.toString();
    }

	public String toSubTask(TaskCmsaudit taskmap){
		log.info(" toAuditTask start ");
		ReturnInfo rtnInfo = new ReturnInfo();
		try
		{
			TaskCmsaudit taskmap2 = cmsTaskBasicDS.getTaskmap(taskmap);
			cmsTaskBasicDS.toAuditTask(taskmap2);
			CommonLogUtil.insertOperatorLog(taskmap.getTaskid() + "",
                    "log.task.infomgr",
                    "log.task.infomgr.subaudit",
                    "log.task.infomgr.subaudit.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
		}
		catch (DomainServiceException e)
		{
			e.printStackTrace();
			log.error("Error occurred in toAuditTask method:", e);
		}
		rtnInfo.setFlag("0");
		rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
		log.info(" toAuditTask end ");
		return rtnInfo.toString();
	}

	public String toAuditTask(TaskCmsaudit taskmap){
		log.info(" toAuditTask start ");
		ReturnInfo rtnInfo = new ReturnInfo();
		try
		{
			TaskCmsaudit taskmap2 = cmsTaskBasicDS.getTaskmap(taskmap);
			taskmap2.setFlag(taskmap.getFlag());
			taskmap2.setAuditdesc(taskmap.getAuditdesc());

			cmsTaskBasicDS.toAuditTask(taskmap2);
			CommonLogUtil.insertOperatorLog(taskmap.getTaskid() + "",
                    "log.task.infomgr",
                    "log.task.infomgr.audit",
                    "log.task.infomgr.audit.info",
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
		}
		catch (DomainServiceException e)
		{
			e.printStackTrace();
			log.error("Error occurred in toAuditTask method:", e);
		}
		rtnInfo.setFlag("0");
		rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功");
		log.info(" toAuditTask end ");
		return rtnInfo.toString();
	}

	public List<TaskFunction> getTaskFunctList()
    {
		TaskFunction taskmap = new TaskFunction();
        taskmap.setParentid(1);  //cms_task_function.parentid 
        List<TaskFunction> funcList = null;
        try
        {
        	funcList = cmsTaskBasicDS.getTaskFunctList(taskmap);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            log.error("Error occurred in getTaskFunctList method:", e);
            return null;
        }
        return funcList;
    }

    @Override
    public TableDataInfo pageAuditRecordInfoQuery(TaskCmsauditRecord record, int arg1, int arg2) {
        return cmsTaskBasicDS.pageAuditRecordInfoQuery(record,arg1,arg2);
    }

    @Override
    public List<TaskFunction> getTaskFunctAllList() throws DomainServiceException {
        return cmsTaskBasicDS.getTaskFunctList(new TaskFunction());
    }

    @Override
    public TaskCmsauditRecord exportAuditRecordByTaskId(TaskCmsauditRecord record) {
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String userId = loginUser.getUserId();

        RIAContext context = RIAContext.getCurrentInstance();
        HttpServletResponse response = context.getResponse();
        return cmsTaskBasicDS.exportAuditRecordByTaskId(record, userId, response);
    }
}

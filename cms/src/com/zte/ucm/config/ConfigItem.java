package com.zte.ucm.config;

public class ConfigItem
{
  private String item_name;
  private String item_code;
  private String item_desc;
  private String item_default_value;
  private String item_current_value;
  private String if_device_related;
  private String if_visible;
  private String restart_flag;
  private String section_no;
  private String related_file;
  private String related_file_type;
  private String operate;
  private String version_no;

  public String getItem_name()
  {
    return this.item_name;
  }

  public void setItem_name(String paramString)
  {
    this.item_name = paramString;
  }

  public String getItem_code()
  {
    return this.item_code;
  }

  public void setItem_code(String paramString)
  {
    this.item_code = paramString;
  }

  public String getItem_desc()
  {
    return this.item_desc;
  }

  public void setItem_desc(String paramString)
  {
    this.item_desc = paramString;
  }

  public String getItem_default_value()
  {
    return this.item_default_value;
  }

  public void setItem_default_value(String paramString)
  {
    this.item_default_value = paramString;
  }

  public String getItem_current_value()
  {
    return this.item_current_value;
  }

  public void setItem_current_value(String paramString)
  {
    this.item_current_value = paramString;
  }

  public String getIf_device_related()
  {
    return this.if_device_related;
  }

  public void setIf_device_related(String paramString)
  {
    this.if_device_related = paramString;
  }

  public String getIf_visible()
  {
    return this.if_visible;
  }

  public void setIf_visible(String paramString)
  {
    this.if_visible = paramString;
  }

  public String getRestart_flag()
  {
    return this.restart_flag;
  }

  public void setRestart_flag(String paramString)
  {
    this.restart_flag = paramString;
  }

  public String getSection_no()
  {
    return this.section_no;
  }

  public void setSection_no(String paramString)
  {
    this.section_no = paramString;
  }

  public String getRelated_file()
  {
    return this.related_file;
  }

  public void setRelated_file(String paramString)
  {
    this.related_file = paramString;
  }

  public String getRelated_file_type()
  {
    return this.related_file_type;
  }

  public void setRelated_file_type(String paramString)
  {
    this.related_file_type = paramString;
  }

  public String getOperate()
  {
    return this.operate;
  }

  public void setOperate(String paramString)
  {
    this.operate = paramString;
  }

  public String getVersion_no()
  {
    return this.version_no;
  }

  public void setVersion_no(String paramString)
  {
    this.version_no = paramString;
  }
}
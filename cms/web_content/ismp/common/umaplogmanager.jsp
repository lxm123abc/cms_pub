<%@ page contentType="text/html; charset=GBK" isELIgnored="false" %>
<%@ taglib uri="../../WEB-INF/tlds/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="../../WEB-INF/tlds/struts-html.tld" prefix="html" %>
<%@ taglib uri="../../WEB-INF/tlds/websearcher.tld" prefix="zte" %>

<html>
<head>
<title>UMAP debug control page</title>
</head>
<body bgcolor="#c0c0c0">
<h1 align="center">UMAP 日志控制项目</h1>
<form name="logmanager" action="logmanager.jsp" method="POST">
<div align="center">
  <input type="submit" name="change" value="submit">
  <input type="reset" name="cancel" value="reset">
</div>
<div align="center">
  <zte:debug/>
</div>
<div align="center">
  <input type="submit" name="change" value="submit">
  <input type="reset" name="cancel" value="reset">
</div>
</form>
</body>
<script language="JavaScript" type="">
  function models_click(){
    var models = document.all("models");
    var allmodels = document.all("allmodels");
    for(i = 0; i < models.length; i++){
      models[i].checked = allmodels.checked;
    }
  }

  function debugs_click(){
    var debugs = document.all("debug");
    var alldebugs = document.all("alldebugs");
    for(i = 0; i < debugs.length; i++){
      debugs[i].checked = alldebugs.checked;
    }
  }

  function infos_click(){
    var infos = document.all("info");
    var allinfos = document.all("allinfos");
    for(i = 0; i < infos.length; i++){
      infos[i].checked = allinfos.checked;
    }
  }

  function warnings_click(){
    var warnings = document.all("warning");
    var allwarnings = document.all("allwarnings");
    for(i = 0; i < warnings.length; i++){
      warnings[i].checked = allwarnings.checked;
    }
  }

  function errors_click(){
    var errors = document.all("error");
    var allerrors = document.all("allerrors");
    for(i = 0; i < errors.length; i++){
      errors[i].checked = allerrors.checked;
    }
  }

  function fatals_click(){
    var fatals = document.all("fatal");
    var allfatals = document.all("allfatals");
    for(i = 0; i < fatals.length; i++){
      fatals[i].checked = allfatals.checked;
    }
  }
</script>
</html>

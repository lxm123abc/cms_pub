package com.zte.ucm.log;

public abstract class LoggerOutter
{
  public abstract String getId();

  public abstract void print(int paramInt);

  public abstract void print(String paramString);

  public abstract void print(Object paramObject);
}
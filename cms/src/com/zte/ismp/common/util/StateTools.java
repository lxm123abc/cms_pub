package com.zte.ismp.common.util;

import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>
 * 文件名称: StateTools.java
 * </p>
 * <p>
 * 文件描述: 关于程序当前状态的保存和取用类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 目前主要是关于session的一些操作
 * </p>
 * <p>
 * 其他说明: 静态方法
 * </p>
 * <p>
 * 建立日期：2007-8-16
 * </p>
 * <p>
 * 修改日期：
 * </p>
 * 
 * @version 1.0
 * @author 张东涛
 */
public class StateTools
{
    private static final String STATE_MAP = "__$statedate$__";

    /**
     * 获取request，session中变量并保存 如果获取不到，则取默认值defaultValue
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名称
     * @param attributeName String 属性名称
     * @param defaultValue Object 默认值，可为null
     * @return Object 返回依次在request，session，defaultValue中的属性值；并且如果此值不为null（包括“null”），则将此值保存在session中
     */
    public static Object getValueFromAllAndSave(HttpServletRequest request, String modelName, String attributeName,
            Object defaultValue)
    {
        Object obj = getValueFromAll(request, modelName, attributeName, defaultValue);
        if (obj != null)
        {
            setValueToSession(request, modelName, attributeName, obj);
        }
        return obj;
    }

    /**
     * 取出session里的指定属性值
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名称
     * @param attributeName String 属性名称
     * @return Object 返回session里的指定属性值
     */
    public static Object getValueFromSession(HttpServletRequest request, String modelName, String attributeName)
    {
        return getModelMapFromSession(request, modelName).get(attributeName);
    }

    /**
     * 从request中取出attributeName属性值
     * 
     * @param request
     * @param attributeName
     * @return
     */
    public static Object getValueFromRequest(HttpServletRequest request, String attributeName)
    {
        Object objReturn = null;
        objReturn = request.getParameter(attributeName);
        if (objReturn == null)
        {
            objReturn = request.getAttribute(attributeName);
        }
        return objReturn;
    }

    /**
     * 取出指定的属性值,查找范围是request和session,都没有则返回null
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名称
     * @param attributeName String 属性名称
     * @param isRequestFirst boolean 是否request里的值优先取用，否则先从session里取
     * @return Object 返回指定的属性值,查找范围是request和session,都没有则返回null
     */
    public static Object getValue(HttpServletRequest request, String modelName, String attributeName,
            boolean isRequestFirst)
    {
        Object objReturn = null;
        if (isRequestFirst)
        {
            objReturn = getValueFromRequest(request, attributeName);
            if (objReturn == null || ((String) objReturn).equals("null"))
            {
                objReturn = getValueFromSession(request, modelName, attributeName);
            }
        }
        else
        {
            objReturn = getValueFromSession(request, modelName, attributeName);
            if (objReturn == null)
            {
                objReturn = getValueFromRequest(request, attributeName);
            }
        }
        return objReturn;
    }

    /**
     * 取出指定属性值,查找范围是先request后session，都没有则返回默认值
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     * @param attributeName String 属性名
     * @param defaultValue Object 默认值
     * @return 返回指定属性值,查找范围是先request后session，都没有则返回默认值
     */
    public static Object getValueFromAll(HttpServletRequest request, String modelName, String attributeName,
            Object defaultValue)
    {
        Object obj = getValue(request, modelName, attributeName, true);
        if (obj == null)
        {
            return defaultValue;
        }
        else
        {
            return obj;
        }
    }

    /**
     * 取出session中模块名对应的HashMap对象
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     * @return HashMap 返回session中模块名对应的HashMap对象
     */
    public static HashMap getModelMapFromSession(HttpServletRequest request, String modelName)
    {
        HashMap stateMap = getStateMapFromSession(request);
        if (!stateMap.containsKey(modelName))
        {
            stateMap.put(modelName, new HashMap());
        }
        return (HashMap) stateMap.get(modelName);
    }

    /**
     * 取出session“stateData”对应的HashMap，如无，则新建一个；这是关于状态保存的根对象
     * 
     * @param req HttpServletRequest
     * @return
     */
    public static HashMap getStateMapFromSession(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        if (session.getAttribute(STATE_MAP) == null)
        {
            session.setAttribute(STATE_MAP, new HashMap());
        }
        return (HashMap) session.getAttribute(STATE_MAP);
    }

    /**
     * 在session中设置指定的属性值
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     * @param attributeName String 属性名
     * @param attributeVlaue Object 属性值
     */
    public static void setValueToSession(HttpServletRequest request, String modelName, String attributeName,
            Object attributeVlaue)
    {
        getModelMapFromSession(request, modelName).put(attributeName, attributeVlaue);
    }

    /**
     * 直接把request中attributeName的属性值放入attributeName的session属性中
     * 
     * @param req
     * @param modelName
     * @param attributeName
     */
    public static void setRequestToSession(HttpServletRequest request, String modelName, String attributeName)
    {
        Object obj = getValueFromRequest(request, attributeName);
        if (obj != null)
        {
            setValueToSession(request, modelName, attributeName, obj);
        }
    }

    /**
     * 删除stateSession数据，建议不要使用，连需要保留状态的session（"_reserve_"为模块名开头）也会被删除
     * 
     * @param req
     */
    public static void removeRootSession(HttpServletRequest request)
    {
        request.getSession().removeAttribute(STATE_MAP);
    }

    /**
     * 删除除了以"_reserve_"为模块名开头的所有模块session数据，建议使用这个方法来重置session
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     */
    public static void removeSession(HttpServletRequest request)
    {
        HashMap stateMap = getStateMapFromSession(request);
        Iterator keys = getStateMapFromSession(request).keySet().iterator();
        while (keys.hasNext())
        {
            String key = (String) keys.next();
            if (!key.startsWith("_reserve_"))
            {
                stateMap.remove(key);
            }
        }
    }

    /**
     * 删除指定模块的session数据
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     */
    public static void removeModelMapFromSession(HttpServletRequest request, String modelName)
    {
        getStateMapFromSession(request).remove(modelName);
    }

    /**
     * 删除除了指定模块外的所有模块session数据
     * 
     * @param req HttpServletRequest
     * @param modelName String 模块名
     */

    public static void removeModelMapExcludeSpecialFromSession(HttpServletRequest request, String modelName)
    {
        HashMap stateMap = getStateMapFromSession(request);
        Iterator keys = getStateMapFromSession(request).keySet().iterator();
        while (keys.hasNext())
        {
            String key = (String) keys.next();
            if (!key.equals(modelName) && !key.startsWith("_reserve_"))
            {
                stateMap.remove(key);
            }
        }
    }

    public static void main(String[] args)
    {
        String a = "aaaahref=httpaahref=httfa".replaceAll("href=(?!http)", "href=www");
        System.out.println(a);
    }

}

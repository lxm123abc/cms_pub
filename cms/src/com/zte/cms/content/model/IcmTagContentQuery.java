package com.zte.cms.content.model;

public class IcmTagContentQuery
{
    private java.lang.Long tagcontentindex;
    private java.lang.Long tagindex;
    private java.lang.String contentid;
    private java.lang.String cpid;

    public java.lang.Long getTagcontentindex()
    {
        return tagcontentindex;
    }

    public void setTagcontentindex(java.lang.Long tagcontentindex)
    {
        this.tagcontentindex = tagcontentindex;
    }

    public java.lang.Long getTagindex()
    {
        return tagindex;
    }

    public void setTagindex(java.lang.Long tagindex)
    {
        this.tagindex = tagindex;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

}

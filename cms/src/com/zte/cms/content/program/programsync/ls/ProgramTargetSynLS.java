package com.zte.cms.content.program.programsync.ls;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.cms.content.program.programsync.service.IProgramTargetSynDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;

public class ProgramTargetSynLS extends DynamicObjectBaseDS implements IProgramTargetSynLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private IProgramTargetSynDS cntTargetsysSynDS;

    public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            programTargetSyn.setOperid(operinfo.getOperid());
            dataInfo = cntTargetsysSynDS.pageInfoQuery(programTargetSyn, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }        
        return dataInfo;
    }

    public TableDataInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            dataInfo = cntTargetsysSynDS.pageInfoQueryProgramTargetSynByCond(programTargetSyn, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }        
        return dataInfo;
    }

    public void setCntTargetsysSynDS(IProgramTargetSynDS cntTargetsysSynDS)
    {
        this.cntTargetsysSynDS = cntTargetsysSynDS;
    }    
}

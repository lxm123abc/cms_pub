
var timername = "";
var LoginObject = {}; // 登录对象
var classObject = {}; // 实体对象
var tableObject = {}; // 表对象
var methodObject = {}; //方法列表
var orderObject = {}; // 排序条件对象，方法参数
var jsObject = {}; // JS对象，方法参数
var jsObject0 = {};
var jsObject1 = {};
var jsObject2 = {};
var jsObject3 = {};
var jsObject4 = {};
var jsObject5 = {};
var jsObject6 = {};
var jsObject7 = {};
var jsObject8 = {};
var jsObject9 = {};
var resultObject = {}; // 查询结果

function getTableNameFormClassName(fullname) {
	var clsname = fullname.substr(fullname.lastIndexOf(".") + 1);
	var tname = "";
	for (var i=0; i<clsname.length; i++) {
		var tchr = clsname.charAt(i);
		if (is_english(tchr) && tchr == tchr.toUpperCase()) {
			tname += "_" + clsname.charAt(i).toLowerCase();
		}
		else {
			tname += tchr;
		}
	}
	return tname.substr(1);
}

function changeComponentList() {
	var valcomponent = $("#component").find("select[id='sltComponent']").val();
	$("#component").find("select[id='sltBean']").attr("length", 0); 
	$("#component").find("select[id='sltParam']").attr("length", 0); 
	if (!isEmpty(valcomponent)) {initBeanList(valcomponent); changeBeanList();}
}

function changeBeanList() {
	var valbean = $("#component").find("select[id='sltBean']").val();
	$("#component").find("select[id='sltParam']").attr("length", 0); 
	if (!isEmpty(valbean)) {initParamList(valbean);}
}

function doBeanUserDefined() {
	var chk = $("#component").find("input[id='chkBeanUserDefined']").attr("checked");
	var tdc = $("#component").find("td[id='tdComponent']"); if (chk) {tdc.hide();} else {tdc.show();}
	var tdcud = $("#component").find("td[id='tdComponentUd']"); if (chk) {tdcud.show();} else {tdcud.hide();}
	if (chk) $("#component").find("input[id='sltComponentUd']").val("此项忽略，请点击加载服务按钮");
	var tdb = $("#component").find("td[id='tdBean']"); if (chk) {tdb.hide();} else {tdb.show();}
	var tdbud = $("#component").find("td[id='tdBeanUd']"); if (chk) {tdbud.show();} else {tdbud.hide();}
	var btn = $("#component").find("input[id='btnQueryMethod']"); if (chk) {btn.show();} else {btn.hide();}
	if (chk) $("#component").find("select[id='sltParam']").attr("length", 0); else changeBeanList();
	var tdp = $("#component").find("td[id='tdParam']").prev(); if (chk) {tdp.html("服务");} else {tdp.html("实体");}
	var tdqm = $("#component").find("td[id='tdQueryModel']"); if (chk) {tdqm.hide();} else {tdqm.show();}
	var tdqp = $("#component").find("td[id='tdQueryParam']"); if (chk) {tdqp.show();} else {tdqp.hide();}
}

function getUserInfo() {
	LoginObject = {};
	callUrl('$/ssb/uiloader/loginMgt/getUserInfo.ssm', null, 'LoginObject', true);
	if (!isEmpty(LoginObject) && !isEmpty(LoginObject.rtnValue) && !isEmpty(LoginObject.rtnValue.userId)) return;
	clearInterval(timername); timername = "";
	$("#component").find("input[id='btnLogin']").attr("value", "未登录");
}

function doLogin() {
	if (!isEmpty(timername)) {clearInterval(timername); timername = "";}
	var UserInfo = {};
	UserInfo.userId = $("#component").find("input[id='txtUserid']").val();
	UserInfo.password = $("#component").find("input[id='txtPassword']").val();
	LoginObject = {};
	callUrl('$/ssb/uiloader/loginMgt/login.ssm', UserInfo, 'LoginObject', true);
	var rtnCode = LoginObject.rtnValue;
	rtnCode = (rtnCode == "0" || rtnCode == "0000" || rtnCode == "9999");
	$("#component").find("input[id='btnLogin']").attr("value", rtnCode ? "已登录" : "未登录");
	if (rtnCode) timername = setInterval("getUserInfo();", 5 * 60 * 1000);
}

function queryClassModel(obj) {
	classObject = {};
	callUrl('classQueryDS/queryClassObject.ssm', '[' + obj + ']', 'classObject', true);
	clearQueryHTML();
	buildQueryHTMLByClass(classObject);
	buildQueryButton();
}

function queryTableModel(obj) {
	tableObject = {};
	var tableInfo = {};
	tableInfo.dtype = $("#component").find("select[id='sltDatabaseType']").val();
	tableInfo.tname = getTableNameFormClassName(obj);
	callUrl('ujdbcTableColumnDS/queryUjdbcTableColumnListByCond.ssm', tableInfo, 'tableObject', true);
	clearQueryHTML();
	buildQueryHTMLByTable(tableObject);
	buildQueryButton();
}

function doQueryModel() {
	var valparam = $("#component").find("select[id='sltParam']").val();
	var chk = $("#component").find("input[id='chkQueryTable']").attr("checked");
	if (chk) {queryTableModel(valparam);} else {queryClassModel(valparam);}
}

function doQueryMethod() {
	methodObject = {};
	var valbean = $("#component").find("input[id='sltBeanUd']").val();
	if (isEmpty(valbean)) {
		alert("请输入功能名称"); return;
	}
	if (valbean.indexOf("/") != -1) valbean = valbean.substring(0, valbean.indexOf("/"));
	callUrl('classQueryDS/queryClassMethod.ssm', '[' + valbean + ']', 'methodObject', true);
	appendMethodHTMLByClass(methodObject);
}

function doQueryParam() {
	var valmethod = $("#component").find("select[id='sltParam']").val();
	if (isEmpty(valmethod)) {
		alert("请先选择功能"); return;
	}
	clearQueryHTML();
	buildUserDefinedButton();
	buildQueryHTMLByParam(valmethod);
}

function doTypeExpress(pvalue, ptype, chk) {
	clearQueryHTML();
	if (!chk) {
		buildReturnButton();
		newQueryProp(false, false, false, ptype, pvalue);
		return;
	}
	if (ptype.charAt(0) == '[' || ptype == "java.util.List") {
		pvalue = getJsonVal(pvalue);
		ptype = (ptype.charAt(0) == '[') ? ptype.substring(2, ptype.indexOf(";")) : "object";
		buildReturnButton(ptype);
		for (var i=0; i<pvalue.length; i++)
			newQueryProp(true, true, false, ptype, createJsonStr(pvalue[i]), ptype);
		return;
	}
	classObject = {};
	callUrl('classQueryDS/queryClassObject.ssm', '[' + ptype + ']', 'classObject', true);
	buildQueryHTMLByClass(classObject, getJsonVal(pvalue));
	buildReturnButton();
}

function getURL(actor, ext, ignoreext) {
	var chk = $("#component").find("input[id='chkBeanUserDefined']").attr("checked");
	var valbean = chk ? $("#component").find("input[id='sltBeanUd']").val() : $("#component").find("select[id='sltBean']").val();
	//var valbean = chk ? $("#component").find("input[id='fc']").val() : $("#component").find("select[id='sltBean']").val();
	
	if (valbean.indexOf("/") != -1) valbean = valbean.substr(valbean.indexOf("/") + 1);
	var valparam = ignoreext ? "" : document.getElementById("sltParam").value;
	if (!ignoreext) valparam = chk ? valparam.substring(0, valparam.indexOf("(")) : valparam.substr(valparam.lastIndexOf(".") + 1);
	return valbean + "/" + actor + valparam + ext + ".ssm";
}

function getQueryCondByPK() {
	var obj = {};
	$("#queryBody").find("input[name='qrychk'][checked]").each(function(){
		var tid = $(this).prev().attr("id");
		var val = $(this).prev().val(); 
		if (tid == "pname") {
			var pval = $(this).parent().parent().find("input[id='pvalue']").val();
			var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
			var pobj = getJsonVal(pval);
			if (!pchk) {
				if (isEmpty(val) || is_numcode(val)) return false;
				obj.temptemp = pobj; 
				eval("obj." + val + "=obj.temptemp");
			}
			else {
				obj = pobj;
			}
		}
		else {
			tid = tid.substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			eval("obj." + tid + "=\"" + val + "\"");
		}
	});
	if (obj.temptemp != undefined) obj.temptemp = undefined;
	return obj;
}

function getQueryCond() {
	var objs = [];
	objs[0] = {};
	var i = 1;
	var hasShift = false;
	$("#queryBody").find("input[id^='txt']").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		if (!isEmpty(val)) {
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		}
		hasShift = true;
	});
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
		var pobj = getJsonVal(pval);
		if (!pchk || (i>9)) {
			if (!isEmpty(val) && !is_numcode(val)) {
				objs[0].temptemp = pobj; 
				eval("objs[0]." + val + "=objs[0].temptemp");
			}
			hasShift = true;
		}
		else {
			objs[i++] = pobj;
		}
	});
	if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
	if (!hasShift) objs.shift();
	return objs;
}

function getParamsFromQueryCond(objs) {
	var params = "";
	for (var i=0; i<objs.length; i++) {
		jsObject = objs[i];
		if (typeof(jsObject) == "string" && jsObject == "[]") jsObject = [];
		eval("jsObject" + i + "=jsObject");
		params += ",jsObject" + i;
	}
	return params.substr(1);
}

function getReturnObject(isrecursion) {
	var objs = [];
	objs[0] = {};
	var i = 1;
	var hasShift = false;
	$("#queryBody").find("input[id^='txt']").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		if (!isEmpty(val)) {
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		}
		hasShift = true;
	});
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		var pobj = getJsonVal(pval);
		objs[i++] = pobj;
	});
	if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
	if (hasShift) {return objs[0];} else {objs.shift(); return isrecursion ? objs : objs[0];}
}

function doClear() {
	$("#queryBody").find("input[id^='txt']").each(function(){
		$(this).val("");
	});
}

function doGetByUserDefined() {
	resultObject = {};
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("", ""), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doGetByPk() {
	resultObject = {};
	jsObject = getQueryCondByPK();
	if (isEmpty(jsObject)) {
		alert("请选择主键查询,自定义对象必须输入对象名称"); return;
	}
	callUrl(getURL("get", ""), '[jsObject]', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doGetByCond() {
	resultObject = {};
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("get", "ByCond"), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	buildTableHTML(resultObject,0,0,null,true);
}

function doPage(page) {
	resultObject = {};
	var pageSize = $("#tableBody").find("select[id='pageSize']").val();
	if (pageSize == undefined) pageSize = 10;
	var start = (page - 1) * pageSize;
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("pageInfoQuery", "", true) , 
		'[' + (isEmpty(params) ? '' : (params + ',')) + start + ',' + pageSize + ']', 'resultObject', true);
	buildTableHTML(resultObject,start,pageSize,null,true);
}

function doPageOrder(page,orderByColumn,isAsc) {
	resultObject = {};
	var pageSize = $("#tableBody").find("select[id='pageSize']").val();
	if (pageSize == undefined) pageSize = 10;
	var start = (page - 1) * pageSize;
	var params = getParamsFromQueryCond(getQueryCond());
	orderObject = {};
	orderObject.orderByColumn = orderByColumn;
	orderObject.isAsc = isAsc;
	callUrl(getURL("pageInfoQuery", "", true) , 
		'[' + (isEmpty(params) ? '' : (params + ',')) + start + ',' + pageSize + ',orderObject]', 'resultObject', true);
	buildTableHTML(resultObject,start,pageSize,orderByColumn,isAsc);
}

function doInsert() {
	var err = 0;
	$("#queryBody").find("input[id^='txt'][datatype]").each(function(){
		var tid = $(this).attr("id").substring(3);
		tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		var val = $(this).val();
		var type = $(this).attr("datatype");
		var maxchar = $(this).attr("maxlength");
		if ((type == "numeric" || type == "number") && !is_numcode(val)) {
			alert(tid + "必须为数字类型"); err = 1; return false;
		}
		if ((type == "varchar" || type == "varchar2") && (getLens(val) > parseInt(maxchar))) {
			alert(tid + "不能输入超过" + maxchar + "个字符或者" + getZchLens(maxchar) + "个汉字"); err = 1; return false;
		}
	});
	if (err) return;
	var params = getParamsFromQueryCond(getQueryCond());
	callUrl(getURL("insert", ""), isEmpty(params) ? null : '[' + params + ']', 'resultObject', true);
	alert("调用增加操作成功");
}

function doSelectAll() {
	var tabchk = $("#itab").contents().find("input[id='tabchk']").attr("checked");
	$("#itab").contents().find("input[id='trchk']").each(function(){
		$(this).attr("checked", tabchk);
	});
}

function getTrObjs(isbatch) {
	var trobjs = [];
	var i = 0;
	$("#itab").contents().find("input[id='trchk'][checked]").each(function(){
		var trobj = {};
		$(this).parent().parent().find("input[id^='txt']").each(function(){
			var tid = $(this).attr("id").substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			var val = $(this).val();
			
			var obj = getJsonVal(val);
			
			if (!isEmpty(obj))	{
				trobj.temptemp = obj;
				eval("trobj." + tid + "=trobj.temptemp");
			}
		
		});
		
		
		
		if (trobj.temptemp != undefined) trobj.temptemp = undefined;
		trobjs[i] = trobj;
		if (!isbatch) return false;
		i++;
	});
	return trobjs;
}

function doCopyToClipBoard() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要复制的记录"); return;
	}
	jsObject = jsObject[0];
	var clipBoardContent = '';
	clipBoardContent += createJsonStr(jsObject);
	window.clipboardData.setData("Text", clipBoardContent); 
}

function doCopyListToClipBoard() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要复制的记录"); return;
	}
	var clipBoardContent = '';
	clipBoardContent += createJsonStr(jsObject);
	window.clipboardData.setData("Text", clipBoardContent); 
}

function doUpdate() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要更新的记录"); return;
	}
	jsObject = jsObject[0];
	callUrl(getURL("update", ""), '[jsObject]', 'resultObject', true);
	alert("调用修改操作成功");
}

function doUpdateList() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要更新的记录"); return;
	}
	callUrl(getURL("update", "List"), '[jsObject]', 'resultObject', true);
	alert("调用批量更新操作成功");
}

function doRemove() {
	jsObject = getTrObjs(false);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要删除的记录"); return;
	}
	jsObject = jsObject[0];
	callUrl(getURL("remove", ""), '[jsObject]', 'resultObject', true);
	alert("调用删除操作成功");
}

function doRemoveList() {
	jsObject = getTrObjs(true);
	if (isEmpty(jsObject)) {
		alert("请在列表中选择要删除的记录"); return;
	}
	callUrl(getURL("remove", "List"), '[jsObject]', 'resultObject', true);
	alert("调用批量删除操作成功");
}

function readPageObject() {
	var obj = {};
	var val = $("#component").find("select[id='sltDatabaseType']").val(); obj.sltDatabaseType = val;
	val = $("#component").find("input[id='chkBeanUserDefined']").attr("checked"); obj.chkBeanUserDefined = val;
	val = $("#component").find("input[id='chkQueryTable']").attr("checked"); obj.chkQueryTable = val;
	val = $("#component").find("select[id='sltComponent']").val(); obj.sltComponent = val;
	val = $("#component").find("select[id='sltBean']").val(); obj.sltBean = val;
	val = $("#component").find("input[id='sltBeanUd']").val(); obj.sltBeanUd = val;
	val = $("#component").find("select[id='sltParam']").val(); obj.sltParam = val;
	$("#queryBody").find("input[name='qrychk'][checked]").each(function(){
		var tid = $(this).prev().attr("id");
		var val = $(this).prev().val(); 
		if (tid == "pname") {
			tid = val;
		}
		else {
			tid = tid.substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
		}
		obj.qrychk = tid;
	});
	if (document.getElementById("queryBody").firstChild) {
		var objs = [];
		objs[0] = {};
		objs[1] = {};
		var i = 2;
		$("#queryBody").find("input[id^='txt']").each(function(){
			var tid = $(this).attr("id").substring(3);
			tid = tid.charAt(0).toLowerCase() + tid.substr(1);
			var val = $(this).val();
			objs[0].temptemp = getJsonVal(val); 
			eval("objs[0]." + tid + "=objs[0].temptemp");
		});
		$("#queryBody").find("input[id='pname']").each(function(){
			var val = $(this).val();
			var pval = $(this).parent().parent().find("input[id='pvalue']").val();
			var pchk = $(this).parent().parent().find("input[id='pcheck']").attr("checked");
			var pobj = getJsonVal(pval);
			if (!pchk) {
				if (!isEmpty(val) && !is_numcode(val)) {
					objs[1].temptemp = pobj; 
					eval("objs[1]." + val + "=objs[1].temptemp");
				}
			}
			else {
				objs[i++] = pobj;
			}
		});
		if (objs[0].temptemp != undefined) objs[0].temptemp = undefined;
		if (objs[1].temptemp != undefined) objs[1].temptemp = undefined;
		obj.querycond = objs;
	}
	$("#itemName", parent.frames["itree"].document).find("input[id='txtItemJsonStr']").val(createJsonStr(obj));
}

function writePageObject() {
	var obj = getJsonVal($("#itemName", parent.frames["itree"].document).find("input[id='txtItemJsonStr']").val());
	if (obj.sltDatabaseType == undefined) return;
	$("#component").find("select[id='sltDatabaseType']").val(obj.sltDatabaseType);
	$("#component").find("input[id='chkBeanUserDefined']").attr("checked", obj.chkBeanUserDefined);
	$("#component").find("input[id='chkQueryTable']").attr("checked", obj.chkQueryTable);
	if (isEmpty(obj.sltComponent)) {
		$("#component").find("select[id='sltComponent']").attr("selectedIndex", 0);
	}
	else {
		$("#component").find("select[id='sltComponent']").val(obj.sltComponent);
	}
	changeComponentList();
	$("#component").find("select[id='sltBean']").val(obj.sltBean);
	$("#component").find("input[id='sltBeanUd']").val(obj.sltBeanUd); doBeanUserDefined();
	if (isEmpty(obj.sltParam)) return;
	if (obj.chkBeanUserDefined) doQueryMethod(); else changeBeanList();
	$("#component").find("select[id='sltParam']").val(obj.sltParam);
	if (obj.querycond == undefined) return;
	var objs = obj.querycond;
	if (obj.chkBeanUserDefined) {
		doQueryParam();
		var i = 2;
		$("#queryBody").find("input[id='pname']").each(function(){
			$(this).parent().parent().find("input[id='pvalue']").val(createJsonStr(objs[i++]));
		});
		return;
	}
	doQueryModel();
	for (var tid in objs[0]) {
		$("#queryBody").find("input[id='txt" + tid.charAt(0).toUpperCase() + tid.substr(1) + "']").val(objs[0][tid]);
	}
	for (var tid in objs[1]) {
		newQueryProp(true, true, false, tid, objs[1][tid]);
	}
	for (var i=2; i<objs.length; i++) {
		newQueryProp(true, true, false, "arg" + (i-2), createJsonStr(objs[i]));
	}
}
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<html>
<head>
<title><fmt:message key="allotrights.altitle"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   function changeServiceKey () {
      document.view.param1.value = -1;
      document.view.param2.value = -1;
      document.view.param3.value = -1;
      document.view.param4.value = -1;
      document.view.submit();
   }

   function paramChanged (paramOrder) {
      if (paramOrder == 1) {
         document.view.param1.value = document.view.param1List.value;
         document.view.param2.value = -1;
         document.view.param3.value = -1;
         document.view.param4.value = -1;
      }
      else if (paramOrder == 2) {
         document.view.param2.value = document.view.param2List.value;
         document.view.param3.value = -1;
         document.view.param4.value = -1;
      }
      else if (paramOrder == 3) {
         document.view.param4.value = -1;
         document.view.param3.value = document.view.param3List.value;
      }
      else if (paramOrder == 4)
         document.view.param4.value = document.view.param4List.value;
      else {
         alert('<fmt:message key="allotrights.paramerr"/>');
         return;
      }
      document.view.submit();
   }
   
   function operGrpIDExists(operGrpID)
   {
  	var i = 0;
  	for(i = 0; i<document.view.allowOperGroup.options.length ; i++)
  	{
  	  var opt=document.view.allowOperGroup.options[i].value;
  	  var idx = opt.indexOf("|");
  	  var grpId = opt.substr(0,idx);
  	  if(grpId == operGrpID)
  	    return true;
  	}
	  return false;
   }
   
   function authorizeGrp () {
      if (document.view.allOperGroup.selectedIndex >= 0) {
         document.view.operGrpID.value = document.view.allOperGroup.value;
         if(operGrpIDExists(document.view.allOperGroup.value))
         {
             alert('<fmt:message key="allotrights.rightalloted"/>');
         }
         else
         {
           document.view.operate.value = 'authorize';
           document.view.submit();
         }
      }
      else
         alert('<fmt:message key="allotrights.chooseright"/>');
   }

   function dismissGrp () {
      if (document.view.allowOperGroup.selectedIndex >= 0) {
         document.view.operate.value = 'dismiss';
         if (confirm('<fmt:message key="allotrights.surereclaim"/>'))
         {
  			   <!--document.view.operGrpID.value = document.view.allowOperGroup.value; -->
  			  	var idx=document.view.allowOperGroup.value.indexOf("|");
         	document.view.operGrpID.value =  document.view.allowOperGroup.value.substr(0,idx);
         	var str=document.view.allowOperGroup.value.substr(idx+1);
         	idx=str.indexOf("_");
         	document.view.param1.value=str.substr(0,idx);
         	str=str.substr(idx+1);
         	idx=str.indexOf("_");
         	document.view.param2.value=str.substr(0,idx);
         	str=str.substr(idx+1);
         	idx=str.indexOf("_");
         	document.view.param3.value=str.substr(0,idx);
         	str=str.substr(idx+1);
         	idx=str.indexOf("_");
         	document.view.param4.value=str.substr(0,idx);
            document.view.submit();
      }
 }
      else
         alert('<fmt:message key="allotrights.choosereclaim"/>');
   }
</script>
</head>
<body class="body-style1">
<%
    try {
      String serviceKey = (String)session.getAttribute("SERVICEKEY");
      purview.setServiceKey(serviceKey);
      String creatorID = (String)session.getAttribute("OPERID");
      String operID1 = (String)session.getAttribute("OPERID");
      String operName1 = (String)session.getAttribute("OPERNAME");
      if(operID1!=null){

        ArrayList serviceList = purview.getServiceList(serviceKey);
        String selectedServiceKey = (String)request.getParameter("serviceKey");
        String operID = request.getParameter("operID") == null ? "" : (String)request.getParameter("operID");
        String operName = request.getParameter("operName") == null ? "" : (String)request.getParameter("operName");
        String operGrpID = request.getParameter("operGrpID") == null ? "" : (String)request.getParameter("operGrpID");
        String operate = request.getParameter("operate") == null ? "" : (String)request.getParameter("operate");
        int param1 = request.getParameter("param1") == null ? -1 : Integer.parseInt((String)request.getParameter("param1"));
        int param2 = request.getParameter("param2") == null ? -1 : Integer.parseInt((String)request.getParameter("param2"));
        int param3 = request.getParameter("param3") == null ? -1 : Integer.parseInt((String)request.getParameter("param3"));
        int param4 = request.getParameter("param4") == null ? -1 : Integer.parseInt((String)request.getParameter("param4"));
        HashMap map = new HashMap();
        String flagString = "";
        // ?D?????����?����???����?��?1???��D��??33??��
        if (selectedServiceKey == null)
            if (serviceList.size() > 0)
                selectedServiceKey = (String)((HashMap)serviceList.get(0)).get("SERVICEKEY");
            else
                selectedServiceKey = serviceKey;
        if (selectedServiceKey.equalsIgnoreCase("all"))
            selectedServiceKey = (String)((HashMap)serviceList.get(0)).get("SERVICEKEY");
        // ������������?T
        if (operate.equalsIgnoreCase("authorize")) {
            map.put("SESSIONOPERID",operID1);
            map.put("SESSIONOPERNAME",operName1);
            map.put("OPERID",operID);
            map.put("SERVICEKEY",selectedServiceKey);
            map.put("OPERGRPID",operGrpID);
            map.put("PARAM1",param1 + "");
            map.put("PARAM2",param2 + "");
            map.put("PARAM3",param3 + "");
            map.put("PARAM4",param4 + "");
            try{
            if (purview.authorizeOperGrp(map))
                flagString = i18n.getMessage("allotrights.cl.authsuccess");
            else
                flagString = i18n.getMessage("allotrights.cl.authfailed");
             }catch(Exception ex){}
             
        }
        // ��???����?T
        else if (operate.equalsIgnoreCase("dismiss")) {
        		int gparam1 = request.getParameter("param1") == null ? -1 : Integer.parseInt((String)request.getParameter("param1"));
        		int gparam2 = request.getParameter("param2") == null ? -1 : Integer.parseInt((String)request.getParameter("param2"));
        		int gparam3 = request.getParameter("param3") == null ? -1 : Integer.parseInt((String)request.getParameter("param3"));
        		int gparam4 = request.getParameter("param4") == null ? -1 : Integer.parseInt((String)request.getParameter("param4"));
            purview.dismissOperGrp(Integer.parseInt(operID),selectedServiceKey,Integer.parseInt(operGrpID),
                                   gparam1,gparam2,gparam3,gparam4,operID1,operName1);
            flagString = i18n.getMessage("allotrights.cl.reclaim") + "  " + operName + "  " +  i18n.getMessage("allotrights.cl.rights");
        }
        ArrayList paramList = purview.getServiceParamList(selectedServiceKey);   // ����?T��??�쨢D����
        ArrayList paramInfoList = new ArrayList();
        ArrayList allOperGroup = purview.getGroupByService(selectedServiceKey,Integer.parseInt(creatorID));  // ?����D��?2������?������
        ArrayList allowOperGroup = new ArrayList();                         // ��??-��???��?2������?������
        if (! operID.equals(""))
            allowOperGroup = purview.getGroupByService(operID,selectedServiceKey);
        // ��?��?��???��?2������?�������䨮?����D?����???��?2������?������?D��?3y
       if (allowOperGroup.size() > 0 && purview.getOperSerType(operID)!=0) {
            int allowOperGrpID = 0;
            for (int i = 0; i < allowOperGroup.size(); i++) {
                allowOperGrpID = Integer.parseInt((String)((HashMap)allowOperGroup.get(i)).get("OPERGRPID"));
                for (int j = allOperGroup.size() - 1; j >= 0; j--)
                    if (allowOperGrpID == Integer.parseInt((String)((HashMap)allOperGroup.get(j)).get("OPERGRPID")))
                        allOperGroup.remove(j);
            }
        }
        String paramName = "";
        int value = 0;
%>
<form name="view" method="post" action="allotRights.jsp">
<table border="0" width="100%">
  <tr>
    <td width="100%">
      <table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
        <tr>
          <td width="30%" align="center"><fmt:message key="allotrights.managesystem"/></td>
          <td width="70%" class="tr-ring">
            <select name="serviceKey" <%= serviceList.size() < 2 ? "disabled" : "" %> onChange="javascript:changeServiceKey()" class="input-style1">
<%
        for(int i = 0; i < serviceList.size(); i++) {
            map = (HashMap)serviceList.get(i);
%>
              <option value="<%= (String)map.get("SERVICEKEY") %>" <%= selectedServiceKey.equals((String)map.get("SERVICEKEY")) ? "selected" : "" %>><%= (String)map.get("DESCRIPTION") %></option>
<%
        }
%>
            </select></td>
        </tr>
        <tr height="80" align="center">
          <td><fmt:message key="allotrights.authscope"/><%= operID.equals("") ? "" : "<br><br><font color=\"red\">"+i18n.getMessage("allotrights.cl.operator")+"<br>" + operName + "</font>" %></td>
<%
        if (paramList.size() == 0 || ((operID!=null) && (!operID.equals("")) && (purview.getOperSerType(operID)!=0))) {
%>
          <td class="tr-ring"><fmt:message key="allotrights.wholemanagesys"/></td>
<%
        }
        else {
            if(param1 == -1)
                param1 = 0;
            map = (HashMap)paramList.get(0);
            paramInfoList = purview.getServiceParamInfo(map);
            paramName = (String)map.get("PCFIELDNAME");
%>
          <td>
            <table width="100%" border="0" class="table-style5">
              <tr height="20">
                <td width="30%" align="right"><%= paramName %></td>
                <td width="70%">
                  <select name="param1List" onChange="javascript:paramChanged(1)" class="input-style1">
<%
            for (int i = 0; i < paramInfoList.size(); i++) {
                map = (HashMap)paramInfoList.get(i);
                value = Integer.parseInt((String)map.get("VALUE"));
%>
                    <option value="<%= value %>" <%= param1 == value ? "selected" : "" %>><%= (String)map.get("NAME") %></option>
<%
            }
%>
                  </select>
                </td>
              </tr>
              <tr height="20">
<%
            if (paramList.size() < 2) {
%>
                <td colspan="2"></td>
<%
            }
            else {
                map = (HashMap)paramList.get(1);
                paramName = (String)map.get("PCFIELDNAME");
                map.put("PARAM1",param1 + "");
                paramInfoList = purview.getServiceParamInfo(map);
                if (param1 != 0 && param2 == -1)
                    param2 = 0;
%>
                <td align="right"><%= paramName %></td>
                <td>
                  <select name="param2List" <%= param1 == 0 ? "disabled" : "" %> onChange="javascript:paramChanged(2)" class="input-style1">
<%
                for (int i = 0; i < paramInfoList.size(); i++) {
                    map = (HashMap)paramInfoList.get(i);
                    value = Integer.parseInt((String)map.get("VALUE"));
%>
                    <option value="<%= value %>" <%= param2 == value ? "selected" : "" %>><%= (String)map.get("NAME") %></option>
<%
            }
%>
                  </select>
                </td>
<%
            }
%>
              </tr>
              <tr height="20">
<%
            if (paramList.size() < 3) {
%>
                <td colspan="2"></td>
<%
            }
            else {
                map = (HashMap)paramList.get(1);
                paramName = (String)map.get("PCFIELDNAME");
                map.put("PARAM1",param1 + "");
                map.put("PARAM2",param2 + "");
                paramInfoList = purview.getServiceParamInfo(map);
                if (param2 != 0 && param3 == -1)
                    param3 = 0;
%>
                <td align="right"><%= paramName %></td>
                <td>
                  <select name="param3List" <%= param1 * param2 == 0 ? "disabled" : "" %> onChange="javascript:paramChanged(3)" class="input-style1">
<%
                for (int i = 0; i < paramInfoList.size(); i++) {
                    map = (HashMap)paramInfoList.get(i);
                    value = Integer.parseInt((String)map.get("VALUE"));
%>
                    <option value="<%= value %>" <%= param3 == value ? "selected" : "" %>><%= (String)map.get("NAME") %></option>
<%
            }
%>
                  </select>
                </td>
<%
            }
%>
              </tr>
              <tr height="20">
<%
            if (paramList.size() < 3) {
%>
                <td colspan="2"></td>
<%
            }
            else {
                map = (HashMap)paramList.get(1);
                paramName = (String)map.get("PCFIELDNAME");
                map.put("PARAM1",param1 + "");
                map.put("PARAM2",param2 + "");
                map.put("PARAM3",param3 + "");
                paramInfoList = purview.getServiceParamInfo(map);
                if (param3 != 0 && param4 == -1)
                    param4 = 0;
%>
                <td align="right"><%= paramName %></td>
                <td>
                  <select name="param4List" <%= param1 * param2 * param3 == 0 ? "disabled" : "" %> onChange="javascript:paramChanged(4)" class="input-style1">
<%
                for (int i = 0; i < paramInfoList.size(); i++) {
                    map = (HashMap)paramInfoList.get(i);
                    value = Integer.parseInt((String)map.get("VALUE"));
%>
                    <option value="<%= value %>" <%= param4 == value ? "selected" : "" %>><%= (String)map.get("NAME") %></option>
<%
            }
%>
                  </select>
                </td>
<%
            }
%>
              </tr>
            </table>
          </td>
<%
        }
%>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" class="body-style1">
        <tr>
          <td height="24" align="left" class="tr-ring"><fmt:message key="allotrights.grpauthenticated"/></td>
          </tr>
        <tr>
          <td align="left" class="tr-ring">
            <select name="allOperGroup" size="6" <%= operID.equals("") ? "disabled" : "" %> class="input-style3">
  <%
        for (int i = 0; i < allOperGroup.size(); i++) {
            map = (HashMap)allOperGroup.get(i);
            boolean filter = false;
            String _operid = map.get("OPERGRPID").toString();
            for (int k = 0; k < allowOperGroup.size(); k++) {
               HashMap _map = (HashMap)allowOperGroup.get(k);
               if(_operid.equals(_map.get("OPERGRPID").toString())){
                  filter = true;
                  break;
               }
            
            }
            if(filter){
              continue;
            }
%>
                <option value="<%= (String)map.get("OPERGRPID") %>"><%= (String)map.get("GRPSCRIPT") %></option>
  <%
        }
%>
            </select></td>
          </tr>
        <tr>
          <td>
            <table width="100%" border="0" class="body-style1">
              <tr align="center">
                <td width="50%"><input type="button" name="authorize" value="<fmt:message key="allotrights.authenticate"/>" onClick="javascript:authorizeGrp()" <%= operID.equals("") ? "disabled" : "" %> class="button-style4"></td>
                <td width="50%"><input type="button" name="dismiss" value="<fmt:message key="allotrights.reclaim"/>" onClick="javascript:dismissGrp()" <%= operID.equals("") ? "disabled" : "" %> class="button-style4"></td>
              </tr>
            </table>
          </td>
        <tr>
          <td height="24" align="right" class="tr-ring"><div align="left"><fmt:message key="allotrights.authenticatedgrp"/></div></td>
          </tr>
        <tr>
          <td align="right" class="tr-ring">
            <div align="left">
              <select name="allowOperGroup" size="11" <%= (operID.equals("")||operID.equals(operID1)) ? "disabled" : "" %> class="input-style3" >
      <%
        for (int i = 0; i < allowOperGroup.size(); i++) {
            map = (HashMap)allowOperGroup.get(i);
   			String[] pnames = new String[4];
    		   pnames[0]=(String)map.get("PARAM1NAME");
            pnames[1]=(String)map.get("PARAM2NAME");
            pnames[2]=(String)map.get("PARAM3NAME");
            pnames[3]=(String)map.get("PARAM4NAME");
    		   String[] grpparams = new String[4];
            grpparams[0]=(String)map.get("PARAM1");
            grpparams[1]=(String)map.get("PARAM2");
            grpparams[2]=(String)map.get("PARAM3");
            grpparams[3]=(String)map.get("PARAM4");



            String strTmp="|"+grpparams[0]+"_"+grpparams[1]+"_"+grpparams[2]+"_"+grpparams[3]+"_";
%>
						<option value="<%
						out.println((String)map.get("OPERGRPID")+strTmp);%>">
                  	<%
                  	   String s=(String)map.get("GRPSCRIPT");

                  	   if( ((operID!=null) && (!operID.equals("")) && (purview.getOperSerType(operID)==0))){
                  	   	int k;
                  	   	//s+="     :";
                  	   	//for(k=0;k<4;k++)
                  	   		//s+=pnames[k]+" &nbsp; ";
                  	   }
                  		out.println(s);
                  	%>
                  </option>
                  <%
        }
%>
              </select>
            </div></td>
          </tr>
      </table>
    </td>
  </tr>
</table>
<input type="hidden" name="param1" value="<%= param1 %>">
<input type="hidden" name="param2" value="<%= param2 %>">
<input type="hidden" name="param3" value="<%= param3 %>">
<input type="hidden" name="param4" value="<%= param4 %>">
<input type="hidden" name="operID" value="<%= operID %>">
<input type="hidden" name="operName" value="<%= operName %>">
<input type="hidden" name="operGrpID" value="">
<input type="hidden" name="operate" value="">
</form>
<%
        if (flagString.length() > 0) {
%>
<script>
   alert('<%= flagString %>');
</script>
<%
        }

        }
        else {

          if(operID1 == null){
              %>
              <script language="javascript">
                    alert( "<fmt:message key="allotrights.loginfirst"/>");
                    parent.document.URL = '../enter.jsp';
              </script>
              <%
                      }
                      else{
               %>
              <script language="javascript">
                   alert( "<fmt:message key="allotrights.forbidden"/>");
              </script>
              <%

              }
         }
    }
    catch(Exception e) {
    e.printStackTrace();
%>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
<%
    }
%>
</body>
</html>

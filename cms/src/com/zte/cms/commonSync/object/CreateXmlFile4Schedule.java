package com.zte.cms.commonSync.object;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CreateXmlFile4Schedule extends AbstractCreateXmlFile4Object {
	private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_USER,
			getClass());

	private final static String SCHEDULE_FOLDER = "schedule";
	private final static String CMSCASTMTYPE_CNTSYNCXML = "xmlsync";

	public String createXmlFile4WG18(Map<String, List> objectMap,
			int targetType, int syncType) {
		// TODO Auto-generated method stub
		String action = null;
		// if (0 != targetType) {
		// return null;
		// }
		// 获取发布Action类型
		if (1 == syncType) {
			action = "REGIST";
		} else if (2 == syncType) {
			action = "UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}

		Date date = new Date();
		String fileName = "schedule" + "_" + "20wg" + "_"
		        +action.toLowerCase().replace(" ", "")+"_"+ date.getTime() + ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
				+ File.separator + getCurrentTime() + File.separator
				+ "schedule" + File.separator;

		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		String fileFullName = absolutePath + fileName;

		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		List<CmsSchedule> schedulelist = (List<CmsSchedule>) objectMap
				.get("schedulelist");

		Element progobjectElement = null;
		Element progpertyElement = null;
		progobjectElement = objectsElement.addElement("Object");
		progobjectElement.addAttribute("ElementType", "Schedule");
		progobjectElement.addAttribute("ID", schedulelist.get(0)
				.getCpcontentid());
		progobjectElement.addAttribute("Code", schedulelist.get(0)
				.getCpcontentid());

		progobjectElement.addAttribute("Action", action);

		writeElement(progobjectElement, progpertyElement, "ChannelID",
				schedulelist.get(0).getChannelcpcontentid(), true);
		writeElement(progobjectElement, progpertyElement, "ChannelCode",
				schedulelist.get(0).getChannelcpcontentid(), true);
		writeElement(progobjectElement, progpertyElement, "ProgramName",
				schedulelist.get(0).getProgramname(), true);
		writeElement(progobjectElement, progpertyElement, "StartDate",
				schedulelist.get(0).getStartdate(), true);
		writeElement(progobjectElement, progpertyElement, "StartTime",
				schedulelist.get(0).getStarttime(), true);
		writeElement(progobjectElement, progpertyElement, "Duration",
				schedulelist.get(0).getDuration(), true);
		writeElement(progobjectElement, progpertyElement, "Status",
				schedulelist.get(0).getIsvalid(), true);
		writeElement(progobjectElement, progpertyElement, "Description",
				schedulelist.get(0).getDescription(), true);
		writeElement(progobjectElement, progpertyElement, "ObjectType",
				"", false);
		writeElement(progobjectElement, progpertyElement, "ObjectCode",
				"", false);
		
		writeDom(dom, fileFullName);

		return retPath;
	}

	public String createXmlFile4IPTV30(Map<String, List> objectMap,
			int targetType, int syncType) {
		// TODO Auto-generated method stub
		CmsSchedule schedule = new CmsSchedule();
		List list = objectMap.get("schedulelist");
		if (list != null && list.size() > 0) {
			schedule = (CmsSchedule) list.get(0);
		}
		String targetTypestr = "";

		if (targetType == 1) {
			targetTypestr = "bms";
		}
		if (targetType == 2) {
			targetTypestr = "epg";
		}
		if (targetType == 3) {
			targetTypestr = "cdn";
		}
		String synctype = "";
		if (1 == syncType || 2 == syncType) {
            synctype = "REGIST or UPDATE";
        } else if (3 == syncType) {
            synctype = "DELETE";
        } else {
            return null;
        }
		Date date = new Date();
		String dir = getCurrentTime();
		String fileName = "";
		if (targetType == 2) {
			fileName = "schedule_schedulerecord" + "_" + targetTypestr + "_"
			        +synctype.toLowerCase().replace(" ", "")+"_"+ date.getTime() + ".xml";
		} else if(targetType==3){
			fileName = "schedulerecord" + "_" + targetTypestr + "_"
			        +synctype.toLowerCase().replace(" ", "")+"_"+ date.getTime() + ".xml";
		}else if(targetType==1){
		    fileName = "schedule" + "_" + targetTypestr + "_"
		            +synctype.toLowerCase().replace(" ", "")+"_"+ date.getTime() + ".xml";
		}
		
		String tempFilePath = tempAreaPath + File.separator
				+ CMSCASTMTYPE_CNTSYNCXML + File.separator + dir
				+ File.separator + "schedule" + File.separator;
		String mountAndTempfilePath = mountPoint + tempFilePath;
		String retPath = GlobalConstants
				.filterSlashStr(tempFilePath + fileName);
		File file = new File(mountAndTempfilePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String fileFullName = mountAndTempfilePath + fileName;
		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		Element objectElement = null;
		Element propertyElement = null;

		String startDate = schedule.getStartdate();// 截取日期
		String startTime = schedule.getStarttime();// 截取时间;

		if (targetType != 3) {
			objectElement = objectsElement.addElement("Object");
			objectElement.addAttribute("Action", synctype);
			objectElement.addAttribute("ElementType", "Schedule");// 填写Object
			objectElement.addAttribute("ContentID", schedule.getScheduleid());

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "Status");
			propertyElement.addCDATA(String.valueOf(schedule.getIsvalid()));

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "StartDate");
			propertyElement.addText(startDate);
			
			propertyElement = objectElement.addElement("Property");
            propertyElement.addAttribute("Name", "ChannelID");
            propertyElement.addText(schedule.getChannelid());

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "ProgramName");
			propertyElement.addCDATA(schedule.getProgramname());

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "Duration");
			propertyElement.addText(String.valueOf(schedule.getDuration()));

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "StartTime");
			propertyElement.addText(startTime);// 第九个property name="StartTime"

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "CPContentID");
			propertyElement.addCDATA(schedule.getCpcontentid());

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "UniContentId");
			if (schedule.getUnicontentid() == null) {
				propertyElement.addCDATA("");
			} else {
				propertyElement.addCDATA(schedule.getUnicontentid());
			}

			propertyElement = objectElement.addElement("Property");
			propertyElement.addAttribute("Name", "Description");
			if (schedule.getDescription() == null) {
				propertyElement.addCDATA("");
			} else {
				propertyElement.addCDATA(schedule.getDescription());
			}
		}

		if (targetType == 2 || targetType == 3) {  //2 epg,3 cdn
			List scherelist = objectMap.get("schedulerecordlist");
			if (scherelist != null && scherelist.size() > 0) {
				for (int i = 0; i < scherelist.size(); i++) {
					CmsSchedulerecord scheduleRecord = (CmsSchedulerecord) scherelist
							.get(i);
//					String startDatere = scheduleRecord.getStartdate();// 截取日期
//					String startTimere = scheduleRecord.getStarttime();// 截取时间;

					objectElement = objectsElement.addElement("Object");
					objectElement.addAttribute("Action", synctype);
					objectElement.addAttribute("ElementType", "ScheduleRecord");// 填写Object
					objectElement.addAttribute("ScheduleId", scheduleRecord
							.getScheduleid());
					objectElement.addAttribute("PhysicalContentID",
							scheduleRecord.getSchedulerecordid());
					objectElement.addAttribute("PhysicalChannelID",
							scheduleRecord.getPhysicalchannelid());

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "Domain");// 第三个property
					// name="ObjectType"
					if (scheduleRecord.getDomain() == null) {
						propertyElement.addCDATA("");
					} else {
						propertyElement.addCDATA(String.valueOf(scheduleRecord
								.getDomain()));
					}

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "HotDegree");// 第四个property
					// name="StartDate"
					if (scheduleRecord.getHotdegree() == null) {
						propertyElement.addText("");
					} else {
						propertyElement.addText(String.valueOf(scheduleRecord
								.getHotdegree()));
					}

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "StartDate");// 第五个property
					// name="ProgramName"
					propertyElement.addCDATA(startDate);
					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "StartTime");// 第五个property
					// name="ProgramName"
					propertyElement.addCDATA(startTime);

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "Duration");// 第七个property
					// name="Duration"
					propertyElement.addText(String.valueOf(schedule
							.getDuration()));

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "CPContentID");
					propertyElement.addText(scheduleRecord.getCpcontentid());// 第九个property
					// name="StartTime"

					propertyElement = objectElement.addElement("Property");
					propertyElement.addAttribute("Name", "Description");
					if (scheduleRecord.getDescription() == null) {
						propertyElement.addCDATA("");
					} else {
						propertyElement.addCDATA(scheduleRecord
								.getDescription());
					}
				}
			}
		}
		XMLWriter writer = null;
		try {
			writer = new XMLWriter(new FileOutputStream(fileFullName));
			writer.write(dom);
		} catch (Exception e) {
			log
					.error("createScheduleXml exception: in the class CmsScheduleLS............."
							+ e);

		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e) {
					log
							.error("createScheduleXml exception: in the class CmsScheduleLS............."
									+ e);

				}
			}
		}

		log
				.debug("createScheduleXml end  in the class CmsScheduleLS.............");

		return retPath;
	}

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {
        String action = null;
        // if (0 != targetType) {
        // return null;
        // }
        // 获取发布Action类型
        if (1 == syncType) {
            action = "REGIST";
        } else if (2 == syncType) {
            action = "UPDATE";
        } else if (3 == syncType) {
            action = "DELETE";
        } else {
            return null;
        }

        Date date = new Date();
        String fileName = "schedule" + "_" + "20ys" + "_"
                +action.toLowerCase().replace(" ", "")+"_"+ date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "schedule" + File.separator;

        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants
                .filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }

        String fileFullName = absolutePath + fileName;

        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        List<CmsSchedule> schedulelist = (List<CmsSchedule>) objectMap
                .get("schedulelist");

        Element progobjectElement = null;
        Element progpertyElement = null;
        progobjectElement = objectsElement.addElement("Object");
        progobjectElement.addAttribute("ElementType", "Schedule");
        progobjectElement.addAttribute("ID", schedulelist.get(0)
                .getCpcontentid());
        progobjectElement.addAttribute("Code", schedulelist.get(0)
                .getCpcontentid());

        progobjectElement.addAttribute("Action", action);

        writeElement(progobjectElement, progpertyElement, "ChannelID",
                schedulelist.get(0).getChannelcpcontentid(), true);
        writeElement(progobjectElement, progpertyElement, "ChannelCode",
                schedulelist.get(0).getChannelcpcontentid(), true);
        writeElement(progobjectElement, progpertyElement, "ProgramName",
                schedulelist.get(0).getProgramname(), true);
        writeElement(progobjectElement, progpertyElement, "StartDate",
                schedulelist.get(0).getStartdate(), true);
        writeElement(progobjectElement, progpertyElement, "StartTime",
                schedulelist.get(0).getStarttime(), true);
        writeElement(progobjectElement, progpertyElement, "Duration",
                schedulelist.get(0).getDuration(), true);
        //新增加storageduration
        writeElement(progobjectElement, progpertyElement, "StorageDuration",
                schedulelist.get(0).getReservecode1(), true);
        writeElement(progobjectElement, progpertyElement, "Status",
                schedulelist.get(0).getIsvalid(), true);
        writeElement(progobjectElement, progpertyElement, "Description",
                schedulelist.get(0).getDescription(), true);
       
        
        writeDom(dom, fileFullName);

        return retPath;
    }

}

package com.zte.cms.settlementDataRpt.common;

/**
 * ProjectName:
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 15:30 2019/5/6
 * @Description:
 */
public class CommonReturnType {
    //表明对应请求的返回状态  "succsess" / "fail"
    private String status;

    //如果成功返回JSON数据,   如果为失败data使用通用错误码格式
    private Object data;

    //定义一个通用的创建方法
    public static CommonReturnType create(Object result){
        return create(result,"success");
    }
    public static CommonReturnType create(Object result, String status){
        CommonReturnType commonReturnType = new CommonReturnType();
        commonReturnType.setData(result);
        commonReturnType.setStatus(status);
        return commonReturnType;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

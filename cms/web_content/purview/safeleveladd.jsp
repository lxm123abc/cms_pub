<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%

   String servicekey = request.getParameter("servicekey");
   String param = request.getParameter("param");
   HashMap smap = pwdrule.getSafeLevel(servicekey,param);
%>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script language = "JavaScript">
function doAdd(){

 if(document.theForm.servicekey.value==""){
   document.theForm.servicekey.focus();
   return;
 }
 if(document.theForm.param.value==""){
   document.param.focus();
   return;
 }
 if(document.theForm.paramname.value==""){
   document.paramname.focus();
   return;
 }
 if(document.theForm.value.value==""){
   document.value.focus();
   return;
 }
 if(document.theForm.defaultvalue.value==""){
   document.defaultvalue.focus();
   return;
 } 
 document.theForm.flag.value="2";
 document.theForm.action = "safeleveladdend.jsp";
 document.theForm.submit();
 
}
function doEdit(param,servicekey){
 
 if(document.theForm.servicekey.value==""){
   document.theForm.servicekey.focus();
   return;
 }
 if(document.theForm.param.value==""){
   document.param.focus();
   return;
 }
 if(document.theForm.paramname.value==""){
   document.paramname.focus();
   return;
 }
 if(document.theForm.value.value==""){
   document.value.focus();
   return;
 }
 if(document.theForm.defaultvalue.value==""){
   document.defaultvalue.focus();
   return;
 } 
 document.theForm.flag.value="3";
 document.theForm.action = "safeleveladdend.jsp";
 document.theForm.submit();

}
        </script>
    </head>

    <body>
        <table width = "100%" height = "98%" border = "0" align = "center" cellpadding = "0" cellspacing = "0" >

            <tr>
              <td valign = "top">
					<%

					%>
					<form name = "theForm" action = "" method = "post">
                    <input type="hidden" name="flag" value="">
                        <table width = "95%" border = "0" bgcolor = "#CCCCCC" cellpadding = "1" cellspacing = "1" align = "center" class="table-style2">
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="safeleveladd.servicekey"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <select name = "servicekey" disabled >
                                    <option value="0" <%= servicekey.equals("0")? "selected":"" %>><fmt:message key="allotrights.wholemanagesys"/></option>
                                    <option value="sms" <%= servicekey.equals("sms")? "selected":"" %>><fmt:message key="allotrights.smsmanagesys"/></option>
                                    </select>
                                </td>
							              </tr>
  						              <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="safelevelset.param"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <input name = "param" type = "text" value="<%= smap.get("param")%>"><font color="#ff0000">*</font></td>
                            </tr>
							              <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="safelevelset.paramname"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <input name = "paramname" type = "text" value="<%= smap.get("paramname")%>"><font color="#ff0000">*</font></td>
                            </tr>
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="safeleveladd.value"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <select name = "value">
                                    <%
                                      ArrayList ruleList = pwdrule.getAllRule();
                                      for(int i=0;i<ruleList.size();i++){
                                      
                                         HashMap p = (HashMap)ruleList.get(i);
                                         if(!((String)smap.get("value")).equals((String)p.get("purview.levelindex"))){
                                           out.println("<option value=\'"+p.get("purview.levelindex")+"\'>"+p.get("purview.levelindex")+"</option>");
                                         }
                                         else{
                                           out.println("<option value=\'"+p.get("purview.levelindex")+"\' selected >"+p.get("purview.levelindex")+"</option>");
                                         }
                                         
                                      }
                                    %>                                      
                                    </select></td>
							              </tr> 
                            <tr bgcolor = "#F5F5F5">
                                <td height = "28" width="12%"><fmt:message key="safeleveladd.defaultvalue"/></td>

                                <td height = "28" bgcolor = "#FFFFFF">
                                    <select name = "defaultvalue">
                                    
                                    <%
                                    
                                      for(int i=0;i<ruleList.size();i++){
                                      
                                         HashMap p = (HashMap)ruleList.get(i);
                                         if(!((String)smap.get("defaultvalue")).equals((String)p.get("purview.levelindex"))){
                                           out.println("<option value=\'"+p.get("purview.levelindex")+"\'>"+p.get("purview.levelindex")+"</option>");
                                         }
                                         else{
                                           out.println("<option value=\'"+p.get("purview.levelindex")+"\' selected >"+p.get("purview.levelindex")+"</option>");
                                         }
                                         
                                      }                                    
                                    
                                    %>
                                    </select></td>
							              </tr> 							                                         
                            <tr bgcolor = "#F5F5F5">
                                <td> <fmt:message key="saferuleset.description"/> </td>

                                <td>
                                    <textarea name = "description" cols = "80" rows = "4"><%= smap.get("description")%></textarea></td>
									                  
                            </tr>

                            <tr bgcolor = "#F5F5F5">
                                <td colspan = "2" align = "center">
									<% if (servicekey == null ||servicekey.trim().length()==0) { %>
                                   <input type = "button" class="button" value = "<fmt:message key="saferuleset.add"/>" onclick = "return doAdd();" disabled />
									<% } else { %>
								                   <input type="hidden" name="oldparam" value="<%= param%>">
								                   <input type="hidden" name="oldservicekey" value="<%= servicekey%>">									
										               <input type = "button" class="button" value = "<fmt:message key="saferuleset.edit"/>" onclick = "return doEdit('<%= param %>','<%= servicekey %>');" />
									<% } %>
                                   <input type = "button" class="button" value = "<fmt:message key="saferuleset.cancel"/>" onclick="javascript: history.back();" />
                                </td>
                            </tr>
                        </table>
					</form>

					<hr>

          </td>

            </tr>

        </table>
    </body>
</html>
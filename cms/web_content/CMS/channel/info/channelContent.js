

function setChanneltype(keyvalue) {
	var haschild = keyvalue['channeltype'];
	if (haschild == "1")
		keyvalue['channeltype'] = '直播频道';
	else if (haschild == "2")
		keyvalue['channeltype'] = 'Web频道';
	return keyvalue;
}

// 验证（为空，数字，长度，特殊字符）type为类型，length长度，boolean是否允许为空,false不能为空
function checkRegProperty(id, length, boolean) {

	var value = $('#' + id).val();
	value = $.trim(value);
	var pattern = /[!@#$%\^\&\*;\|\?\(\)\[\]\{\}'\\"\<\>\/]/;
	var patternStr = "!@#$%^&*;|?\\()[]{}<>'\"";
	// 频道名称不能有“-”，其他字段可以
	//if (id == "channelname") {
	//	pattern = /[!@#$,%\^\&\*;\|\?\(\)\[\]\{\}'\\"\<\>\/\-]/;
	//	patternStr = "!@#$%^&*;|?\\()[]{}<>'\"-";
	//}

	if (boolean == false) {
		if (value == "") {
			$('#' + id + '_msg').text("此项不能为空");
			return false;
		}
	}
	if (!isMaxLengthWide(id, length, true)) {
		$('#' + id + '_msg').text("此项长度不能超过" + length + "个字符（一个汉字占3个字符）");
		$('#' + id).focus();
		return false;
	}
	if (pattern.test(value)) {
		$('#' + id + '_msg').text("此项不能包含以下特殊字符:" + patternStr);
		$('#' + id).focus();
		return false;
	}
	return true;
}

// 时间,所选日期必须大于系统时间
function validatedate(dateid, datetarget, date_msg) {
	var date = formatDBDate(document.getElementById(datetarget).value, 14);
	var nowdate = getTimefromUTC("yyyy-mm-dd hh:mm:ss", new Date());
	var date1 = formatDBDate(nowdate, 14);
	if ((date - date1) < 0) {
		$("#" + date_msg).html("此项应大于系统时间");
		dateid.onfocus();
		return;
	} else {
		$("#" + date_msg).html("");
	}
}

// 开始时间必须小于结束时间
function betweenTimeValidate(dateid, startTimeTarget, endTimeTarget) {
	startTimeTarget = document.getElementById(startTimeTarget).value;
	endTimeTarget = document.getElementById(endTimeTarget).value;
	if (startTimeTarget != "" && endTimeTarget != "") {
		var startTime = formatDBDate(startTimeTarget, 14);
		var endTime = formatDBDate(endTimeTarget, 14);
		if ((startTime - endTime) >= 0) {
			return false;
		}
	}
	return true;
}

// weburl是否必填
function showWeburl(obj) {
	if (obj.value == "1") {
		$("#weburl_show").text("");
		document.getElementById("subtypeTitle").style.display = "block";
		document.getElementById("subtypeContent").style.display = "block";
	} else {
		$("#weburl_show").text("*");
		document.getElementById("subtypeTitle").style.display = "none";
		document.getElementById("subtypeContent").style.display = "none";
	}
}

// 默认时移时长和存储时长是否必填
function setTimeshiftDiv(obj) {
	if (obj.value == "1") {
		$("#storageduration_show").text("*");
		$("#timeshiftduration_show").text("*");
	} else {
		$("#storageduration_show").text("");
		$("#timeshiftduration_show").text("");
	}
}

//物理频道新增、修改时选择不头疼呢“接收频道方式”设置哪些字段为必填
function setStyleDis(obj) {
	$("#multicastportNeed").text("");
	$("#multicastipNeed").text("");
	$("#unicasturlNeed").text("");
	if (obj.value == "multicast") {
		$("#multicastportNeed").text("*");
		$("#multicastipNeed").text("*");
	} else {
		$("#unicasturlNeed").text("*");
	}
}

// 添加验证数据
function addchannelValid(type) {

	var storagedurationVal = $("#storageduration").val();// 存储时长9，数字
	var timeshiftdurationVal = $("#timeshiftduration").val();// 时移时长
	var starttimeVal = $("#starttime").val();// 时移时长
	var endtimeVal = $("#endtime").val();// 时移时长
	var exclusivevalidityVal = $("#exclusivevalidity").val();// 时移时长
	var copyrightexpirationVal = $("#copyrightexpiration").val();// 时移时长

	// 设置得到单选按钮值

	var isvalidTxt = $("input[type='radio'][name='isvalid'][checked]").val();
	$("#isvalidTxt").val(isvalidTxt);

	var channeltypeTxt = $("input[type='radio'][name='channeltype'][checked]")
			.val();
	$("#channeltypeTxt").val(channeltypeTxt);

	var subtypeTxt = $("input[type='radio'][name='subtype'][checked]").val();
	$("#subtypeTxt").val(subtypeTxt);

	var macrovisionTxt = $("input[type='radio'][name='macrovision'][checked]")
			.val();
	$("#macrovisionTxt").val(macrovisionTxt);

	var bilingualTxt = $("input[type='radio'][name='bilingual'][checked]")
			.val();
	$("#bilingualTxt").val(bilingualTxt);

	var platform = $("input[type='radio'][name='platform'][checked]").val();
	$("#platformTxt").val(platform);

	// 正则
	var numPattern = /^((0)|([1-9]+[0-9]*))$/;
	var urlPattern = /^(http)(s?)\:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/;
	// 频道名称
	if (!checkRegProperty("channelname", 64, false)) {
		return false;
	}
	// 台标名称
	if (!checkRegProperty("callsign", 32, false)) {
		return false;
	}
	// 建议频道号
	if (!checkRegProperty("channelnumber", 3, false)) {
		return false;
	}
	var channelnumberVal = $("#channelnumber").val();
	channelnumberVal = $.trim(channelnumberVal);

	if (!numPattern.test(channelnumberVal)) {
		$("#channelnumber_msg").text("此项必须为小于等于3位的数字");
		return false;
	}
	/*
	if (type == "insert") {
		var timeshiftTxt = $("input[type='radio'][name='timeshift'][checked]")
				.val();
		$("#timeshiftTxt").val(timeshiftTxt);

	} else {
		timeshiftTxt = $("#timeshift").val();
	}
	*/
	
	var timeshiftTxt = $("input[type='radio'][name='timeshift'][checked]")
	.val();
	$("#timeshiftTxt").val(timeshiftTxt);
	
	// 邮编
	if (!checkRegProperty("zipcode", 10, true)) {
		return false;
	}
	/*
	 * //内容类型编码 if(!checkRegProperty("contenttypeid",8,true)){ return false; }
	 */
	// 内容提供商(CP)
	if (type == "insert") {
		var cpspnameVal = $("#cpspname").val();
		if (cpspnameVal.length == 0) {
			$("#cpspname_msg").text("此项不能为空");
			return false;
		}
		// 牌照方
		var licenseidVal = $("#licenseid").val();
		if (licenseidVal == "") {
			$("#licensename_msg").text("请选择");
			return false;
		}
	}

	// 国家
	if (!checkRegProperty("country", 10, true)) {
		return false;
	}

	// 州/省
	if (!checkRegProperty("state", 10, true)) {
		return false;
	}

	// 市
	if (!checkRegProperty("city", 10, true)) {
		return false;
	}

	// 存储时长
	storagedurationVal = $.trim(storagedurationVal);
	
	if (timeshiftTxt == 1) {
		if (storagedurationVal.length == 0) {
			$('#storageduration_msg').text("此项不能为空");
			return false;
		}
	}

	if (storagedurationVal.length > 0) {
		if (!isMaxLengthWide("storageduration", 9, true)) {
			$('#storageduration_msg').text("此项不能大于9位");
			return false;
		}
		if (!numPattern.test(storagedurationVal)) {
			$('#storageduration_msg').text("此项只能是数字");
			return false;
		}
	}

	// 默认时移时长timeshiftduration
	if (timeshiftTxt == 1) {
		if (timeshiftdurationVal.length == 0) {
			$('#timeshiftduration_msg').text("此项不能为空");
			return false;
		}
	}
	if (timeshiftdurationVal.length > 0) {
		if (!isMaxLengthWide("timeshiftduration", 9, true)) {
			$('#timeshiftduration_msg').text("此项不能大于9位");
			return false;
		}
		if (!numPattern.test(timeshiftdurationVal)) {
			$('#timeshiftduration_msg').text("此项只能是数字");
			return false;
		}
	}

	// 音频
	if (!checkRegProperty("videotype", 10, true)) {
		return false;
	}
	// 视频
	if (!checkRegProperty("audiotype", 10, true)) {
		return false;
	}

	// weburl
	var weburl = $("#weburl").val();
	weburl = $.trim(weburl);
	var contenttype = $("input[name='channeltype'][checked]").val();
	if (contenttype == "2") {
		if (weburl.length == 0) {
			$("#weburl_msg").text("此项不能为空");
			return false;
		}
	}
	if (!isMaxLengthWide("weburl", 128, true)) {
		$("#weburl_msg").text("此项长度不能超过128个字符（一个汉字占3个字符）");
		return false;
	}

	if (weburl.length > 0) {
		if (!urlPattern.test(weburl)) {
			$('#weburl_msg').text("url格式不正确");
			return false;
		}
	}
	// 所属全局内容一标识
	if (!checkRegProperty("unicontentid", 128, true)) {
		return false;
	}
	// sourceurl
	if (!isMaxLengthWide("sourceurl", 128, true)) {
		$("#sourceurl_msg").text("此项长度不能超过128个字符（一个汉字占3个字符）");
		return false;
	}
	var sourceurl = $("#sourceurl").val();
	sourceurl = $.trim(sourceurl);
	if (sourceurl.length > 0) {
		if (!urlPattern.test(sourceurl)) {
			$('#sourceurl_msg').text("url格式不正确");
			return false;
		}
	}
	// 原名
	if (!checkRegProperty("originalnamecn", 128, true)) {
		return false;
	}
	// 关键字
	/*
	 * if(!checkRegProperty("keywords",128,true)){ return false; }
	 */

	/*
	 * // 中文版权信息 if (!checkRegProperty("copyrightcn", 100, true)) { return
	 * false; }
	 */

	// 内容类型编码
	if (!checkRegProperty("contenttypeid", 8, true)) {
		return false;
	}

	// 内容独家有效期
	if (!checkRegProperty("exclusivevalidity", 10, true)) {
		return false;
	}
	exclusivevalidityVal = $.trim(exclusivevalidityVal);
	if (exclusivevalidityVal.length > 0) {
		if (!numPattern.test(exclusivevalidityVal)) {
			$('#exclusivevalidity_msg').text("此项只能是数字");
			return false;
		}
	}
	// 版权失效期
	if (type == "insert") {
		if (!checkRegProperty("copyrightexpiration", 3, true)) {
			return false;
		}
		copyrightexpirationVal = $.trim(copyrightexpirationVal);
		if (copyrightexpirationVal.length > 0) {
			if (!numPattern.test(copyrightexpirationVal)) {
				$('#copyrightexpiration_msg').text("此项只能是数字");
				return false;
			}
		}
	}

	// 开始时间
	// var timePattern=/^([0-1]?[0-9]|2[0-3])([0-5][0-9])$/;
	var hourPattern = /^([0-1]?[0-9]|2[0-3])$/;
	var minutePattern = /^([0-5][0-9])$/;
	var startHour = $("#startHour").val();
	var startMinute = $("#startMinute").val();
	var endHour = $("#endHour").val();
	var endMinute = $("#endMinute").val();
	startHour = $.trim(startHour);
	startMinute = $.trim(startMinute);
	endHour = $.trim(endHour);
	endMinute = $.trim(endMinute);

	var starttime = 0;
	var endtime = 0;

	if ((startHour.length == 0) && (startMinute.length == 0)) {
		$("#starttime_msg").text("此项不能为空");
		return false;
	}

	if ((startHour.length != 2) || (startMinute.length != 2)) {
		$("#starttime_msg").text("此项的时长格式是HH24MI");
		return false;
	} else if ((!hourPattern.test(startHour))
			|| (!minutePattern.test(startMinute))) {
		$("#starttime_msg").text("此项的时长格式是HH24MI");
		return false;
	} else {
		starttime = startHour + startMinute;
		$("#starttime").val(starttime);
	}

	if (endHour.length == 0 && endMinute.length == 0) {
		$("#endtime_msg").text("此项不能为空");
		return false;
	}

	if ((endHour.length != 2) || (endMinute.length != 2)) {
		$("#endtime_msg").text("此项的时长格式是HH24MI");
		return false;
	} else if ((!hourPattern.test(endHour)) || (!minutePattern.test(endMinute))) {
		$("#endtime_msg").text("此项的时长格式是HH24MI");
		return false;
	} else {
		endtime = endHour + endMinute;
		$("#endtime").val(endtime);
	}

	if (starttime - endtime >= 0) {
		$('#endtime_msg').text("结束时间必须大于开始时间");
		return false;
	}

	// 时间验证
	// 所选的时间不能小于当前时间
	var licensingstart = document.getElementById("licensingstart").value;
	var licensingend = document.getElementById("licensingend").value;

	// 验证日期格式
	if (type == "insert") {
		var llicensofflinetime = document.getElementById("llicensofflinetime").value;
		var deletetime = document.getElementById("deletetime").value;
		// 牌照方内容自定义下线时间
		if (llicensofflinetime.length > 0) {
			validatedate("calendar1", "llicensofflinetime",
					"llicensofflinetime_msg");
		}
		// 内容删除时间
		if (deletetime.length > 0) {
			validatedate("calendar2", "deletetime", "deletetime_msg");
		}
	}

	// 有效开始时间
	if (licensingstart.length > 0) {
		validatedate("calendar5", "licensingstart", "licensingstart_msg");
	}
	// 有效结束时间
	if (licensingend.length > 0) {
		validatedate("calendar6", "licensingend", "licensingend_msg");
	}

	// 有效开始时间不能大于有效结束时间
	var checkstarttime = betweenTimeValidate("calendar5", "licensingstart",
			"licensingend");
	if (!checkstarttime) {
		$("#licensingstart_msg").text("有效开始时间不能大于有效结束时间");
		return false;
	}

	// 描述信息
	if (!checkRegProperty("description", 1024, true)) {
		return false;
	}

	return true;
}

var ucpbasic = {};
var province = {};
var rtncity = {};
var programtype = {};
function setViewData(data) {
	if (data == null) {
		var url = "CMS/channel/channelContentList.html?isLoad=true";
		openMsg("1:频道不存在", encodeURIComponent(url), true);
	} else {
		// 设置时间
		$("#channelid").val(data['channelid']);
		document.getElementById("llicensofflinetime").innerText = getTime2(data['llicensofflinetime']);
		document.getElementById("deletetime").innerText = getTime2(data['deletetime']);
		document.getElementById("onlinetime").innerText = getTime2(data['onlinetime']);
		document.getElementById("offlinetime").innerText = getTime2(data['offlinetime']);
		document.getElementById("licensingstart").innerText = getTime2(data['licensingstart']);
		document.getElementById("licensingend").innerText = getTime2(data['licensingend']);

		var starttime = data["starttime"];
		var endtime = data["endtime"];
		var starthour = starttime.substring(0, 2);
		var startminute = starttime.substring(2, 4);
		var endhour = endtime.substring(0, 2);
		var endminute = endtime.substring(2, 4);

		var starttime = starthour + "时 " + startminute + "分";
		$("#starttime").text(starttime);

		var endtime = endhour + "时 " + endminute + "分";
		$("#endtime").text(endtime);

		// 设置是否显示“信号来源”(当频道类型为web频道时，不显示“信号来源”信息)
		var channeltype = data["channeltype"];
		if (channeltype == "1") {// 频道类型为“直播频道”则显示，否则就新仓
			document.getElementById("subtypeTitle").style.display = "block";
			document.getElementById("subtypeContent").style.display = "block";
		} else {
			document.getElementById("subtypeTitle").style.display = "none";
			document.getElementById("subtypeContent").style.display = "none";
		}

		// 单选值
		// 时移标志
		var timeshift = data["timeshift"];
		if (timeshift == "1") {
			$("#timeshift").text("生效");
		} else {
			$("#timeshift").text("不生效");
		}

		// 状态标志
		var isvalid = data["isvalid"];
		if (isvalid == "1") {
			$("#isvalid").text("生效");
		} else {
			$("#isvalid").text("失效");
		}

		var channeltype = data["channeltype"];
		if (channeltype == "1") {
			$("#channeltype").text("直播频道");
		} else {
			$("#channeltype").text("web频道");
		}

		var subtype = data["subtype"];
		if (subtype == "1") {
			$("#subtype").text("信号源来自live");
		} else {
			$("#subtype").text("信号源来自virtual");
		}

		var macrovision = data["macrovision"];
		if (macrovision == "1") {
			$("#macrovision").text("有拷贝保护");
		} else {
			$("#macrovision").text("无拷贝保护");
		}

		var bilingual = data["bilingual"];
		if (bilingual == "1") {
			$("#bilingual").text("是");
		} else {
			$("#bilingual").text("否");
		}

		// 下拉列表
		var tradetype = data["tradetype"];
		if (tradetype == "1") {
			$("#tradetype").text("内容买断方式");
		} else if (tradetype == "2") {
			$("#tradetype").text("保底+分成方式");
		} else if (tradetype == "3") {
			$("#tradetype").text("产品分成方式");
		} else if (tradetype == "4") {
			$("#tradetype").text("流量分成方式");
		}

		var platform = data["platform"];
		if (platform == "1")
			$("#platformLab").text("PC：互联网视讯");
		else if (platform == "2")
			$("#platformLab").text("CE：天翼视讯");
		else if (platform = "3")
			$("#platformLab").text("TV：IPTV");
		else
			$("#platformLab").text("MP：魔屏");

		// 设置内容提供商
		var cpid = data["cpid"];
		var basic = new Object();
		ucpbasic = new Object();
		basic["cpid"] = cpid;
		basic["cptype"] = "1";
		callUrl("channelDSFacade/getUcpbasicBy.ssm", basic, "ucpbasic", true);
		ucpbasic = ucpbasic.rtnValue;
		$("#cpspname").text(ucpbasic["cpcnshortname"]);

		// 牌照方
		var licenseid=data["licenseid"];
		basic = new Object();
		basic["cpid"] = licenseid;
		basic["cptype"] = "2";
		ucpbasic = new Object();
		callUrl("channelDSFacade/getUcpbasicBy.ssm", basic, "ucpbasic", true);
		ucpbasic = ucpbasic.rtnValue;
		$("#licenseid").text(ucpbasic["cpcnshortname"]);

		// 设置省份
		var prov = new Object();
		prov["provid"] = data["provid"];

		callUrl("cmsSysProvinceLSFacade/getUsysProvinceById.ssm", prov,
				"province", true);
		province = province.rtnValue;
		$("#provname").text(province["provname"]);

		// 查询得到图片信息
		callSid("queryChannelPictureListByChnanelindex", data["channelindex"]);

		// callUrl("cmsProgramtypeFacade/getCmsProgramtype.ssm",data.contenttypeid,"programtype",true);
		// programtype=programtype.rtnValue;
		// $("#contenttypeid").text(programtype["typename"]);

	}
}

var dd = 0;
function showimagelist(imagelist) {
	dd = 0;
	if (imagelist != "") {
		for (var index = 0; index < imagelist.length; index++) {
			makeimageitem("imagebase");
			if (imagelist[index]["pictureurl"] != null) {
				$("#lblprop02" + index).text(imagelist[index]["pictureurl"]);
				document.getElementById("lblprop02" + index).setAttribute(
						"align", "center");
				$("#tdaddresspath" + index).attr("src",
						imagelist[index]["pictureurl"]);
			}
		}
		// 图片自适应
		setPicAdjust("basicinfoID");
	}
}

function makeimageitem(table) {
	// prop02 图片分辨率
	// addresspath 地址
	// playnums 1:JPG 2:GIF 3:PNG
	var tbody = document.getElementById(table);
	newTr = tbody.insertRow();
	var td = document.createElement('td');
	td.setAttribute("height", "25px");
	td.setAttribute("align", "left");
	td.innerHTML = '<td align="left" id="thimage' + dd + '">'
			+ '<label align="left" id="lblprop02' + dd + '"></label>'
			+ '<label align="left" id="lblformat' + dd + '"></label>' + '</td>';
	newTr.appendChild(td);

	var tr1 = tbody.insertRow();
	var td1 = document.createElement('td');
	td1.innerHTML = '<td id="tdimage>' + dd + '"></td>'
			+ '<div class="imgOverClasses"><image id="tdaddresspath' + dd
			+ '"/></div>';

	td1.setAttribute("align", "left");
	tr1.appendChild(td1);
	dd++;
}

function setPicAdjust(tableId) {
	// 由于要与上面的表格对其，故先获取上面表格的宽度
	var $tableWidth = $("#" + tableId).width();
	var $picBoxWidth = $tableWidth - ($tableWidth * 0.2);// 减去20%的右边单元的说明
	$picBoxWidth = ($picBoxWidth - 10);// 计算图片最大宽度要求，减去10个像素(div中有4个像素是空白像素，减去10是为了页面各栏保证对齐)
	// $("div.imgOverClass").css("width",$picBoxWidth);
	// 遍历每个图片
	$("div.imgOverClass image").each(function() {
				var $picWidth = $(this).width();// 计算图片的宽度
				var $picHeigth = $(this).height();// 计算图片的高度
				// 如果图片的宽度大于最大宽度要求，则进行If语句
				if (($picWidth) >= ($picBoxWidth)) {
					// 计算压缩的比例
					var $zipPara = ($picBoxWidth) / ($picWidth);
					// 把最大宽度要求赋值给图片的width属性
					$picWidth = $picBoxWidth;
					// 高度进行同比例压缩
					$picHeigth = ($picHeigth) * ($zipPara);
					// 取整数部分
					$picHeigth = Math.round($picHeigth);
					// 把属性赋值给图片
					$(this).attr("width", $picWidth);
					$(this).attr("height", $picHeigth);
				}
			});

}

// 物理频道验证信息
function validPhysicchannel(type) {
	// 得到单选按钮值
	var srccasttypeTxt = $("input[name='srccasttype'][checked]").val();
	var destcasttypeTxt = $("input[name='destcasttype'][checked]").val();
	var hotdegreeTxt = $("input[name='hotdegree'][checked]").val();

	$("#srccasttypeTxt").val(srccasttypeTxt);
	$("#destcasttypeTxt").val(destcasttypeTxt);
	$("#hotdegreeTxt").val(hotdegreeTxt);

	if (type == "insert") {
		var doaminTxt = $("input[name='domain'][checked]").val();		
		$("#domainTxt").val(doaminTxt);
	}

	var storagedurationVal = $("#storageduration").val();// 存储时长
	storagedurationVal = $.trim(storagedurationVal);
	var timeshiftdurationVal = $("#timeshiftduration").val();// 默认时移时长
	timeshiftdurationVal = $.trim(timeshiftdurationVal);

	var numPattern = /^((0)|([1-9]+[0-9]*))$/;
	var urlPattern = /^rtsp:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/;
	var ipPattern = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
	var timeshift = "";

	if (type == "insert") {
		var timeshift = $("input[name='timeshift'][checked]").val();
		$("#timeshiftTxt").val(timeshift);
		// 频道名称
		if (!checkRegProperty("namecn", 64, false)) {
			return false;
		}
	} else {
		timeshift = $("#timeshift").val();
		$("#timeshiftTxt").val(timeshift);
	}

	// 存储时长
	if (timeshift == "1") {
		if (storagedurationVal.length == 0) {
			$("#storageduration_msg").text("此项不能为空");
			return false;
		}
	}
	if(storagedurationVal.length > 0) {
		if (!numPattern.test(storagedurationVal)) {
			$("#storageduration_msg").text("此项只能是数字");
			return false;
		}
	}
	
	if (storagedurationVal.length > 9) {
		$("#storageduration_msg").text("此项不能大于9位");
		return false;
	}

	// 默认时移时长
	if (timeshift == "1") {
		if (timeshiftdurationVal.length == 0) {
			$("#timeshiftduration_msg").text("此项不能为空");
			return false;
		}
	}
	if(timeshiftdurationVal.length > 0){
		if (!numPattern.test(timeshiftdurationVal)) {
			$("#timeshiftduration_msg").text("此项只能是数字");
			return false;
		}
	}
	if (timeshiftdurationVal.length > 9) {
		$("#timeshiftduration_msg").text("此项不能大于9位");
		return false;
	}

	// 组播端口(如果是组播必填)
	var multicastportVal = $("#multicastport").val();
	multicastportVal = $.trim(multicastportVal);
	if (srccasttypeTxt == "multicast") {
		if (multicastportVal.length == 0) {
			$("#multicastport_msg").text("此项不能为空");
			return false;
		}
	}
	if (multicastportVal.length > 0) {
		// 验证端口
		if (!numPattern.test(multicastportVal)) {
			$("#multicastport_msg").text("此项只能是1-99999的整数");
			return false;
		} else {
			if (multicastportVal < 1 || multicastportVal > 99999) {
				$("#multicastport_msg").text("此项只能是1-99999的整数");
				return false;
			}
		}
	}

	// 组播IP
	var multicastipVal = $("#multicastip").val();
	multicastipVal = $.trim(multicastipVal);
	if (srccasttypeTxt == "multicast") {
		if (multicastipVal.length == 0) {
			$("#multicastip_msg").text("此项不能为空");
			return false;
		}
	}
	if (multicastipVal.length > 0) {
		if (!ipPattern.test(multicastipVal)) {
			$("#multicastip_msg").text("IP格式不正确");
			return false;
		}
	}

	// 单播URL
	var unicasturlVal = $("#unicasturl").val();
	unicasturlVal = $.trim(unicasturlVal);
	if (srccasttypeTxt == "unicast") {
		if (unicasturlVal.length == 0) {
			$("#unicasturl_msg").text("此项不能为空");
			return false;
		}
	}

	if (unicasturlVal.length > 0) {
		if (unicasturlVal.length > 256) {
			$("#unicasturl_msg").text("此项不能大于256个字节");
			return false;
		}

		if (!urlPattern.test(unicasturlVal)) {
			$("#unicasturl_msg").text("单播URL以'rtsp://IP地址'为前缀");
			return false;
		}

	}

	// 码流
	var bitratetype = document.getElementById("bitratetype").value;
	if (bitratetype == "") {
		$("#bitratetype_msg").text("请选择");
		return false;
	}
	return true;
}

function setViewPhysicalchannelData(data) {
	
	if (data == null || typeof data == "undefined") {

		var channelid = $("#channelid").val();
		var url = "CMS/channel/listPhysicalManager.html?isLoad=true&&channelid="
				+ channelid;
		openMsg("1:物理频道不存在", encodeURIComponent(url), true);
	} else {
		$("#channelid").val(data["channelid"]);
		// 设置单元按钮
		var srccasttype = data["srccasttype"];
		if (srccasttype == "unicast") {
			$("#srccasttypeTxt").text("单播");
		} else {
			$("#srccasttypeTxt").text("组播");
		}

		var destcasttype = data["destcasttype"];
		if (destcasttype == "unicast") {
			$("#destcasttypeTxt").text("单播");
		} else {
			$("#destcasttypeTxt").text("组播");
		}

		var hotdegree = data["hotdegree"];
		if (hotdegree == "1") {
			$("#hotdegreeTxt").text("热");
		} else {
			$("#hotdegreeTxt").text("不热");
		}

		var timeshift = data["timeshift"];
		if (timeshift == "1") {
			$("#timeshiftTxt").text("生效");
		} else {
			$("#timeshiftTxt").text("不生效");
		}
		
		var cpcontentid = data["cpcontentid"];
		
		$("#cpcontentid").val(cpcontentid);
		
		// 取得复选框值
		
		var videotype = data["videotype"];
		if (videotype == 1)
			$("#videotypeTxt").text("H.264");
		if (videotype == 2)
			$("#videotypeTxt").text("MPEG4");
		else if (videotype == 3)
			$("#videotypeTxt").text("AVS");
		else if (videotype == 4)
			$("#videotypeTxt").text("MPEG2");
		else if (videotype == 5)
			$("#videotypeTxt").text("MP3");
		else if (videotype == 6)
			$("#videotypeTxt").text("WMV");

		var audiotype = data["audiotype"];
		if (audiotype == 1)
			$("#audiotypeTxt").text("MP2");
		else if (audiotype == 2)
			$("#audiotypeTxt").text("AAC");
		else if (audiotype == 3)
			$("#audiotypeTxt").text("AMR");
		
		var resolution = data["resolution"];
		if (resolution == 1)
			$("#resolutionTxt").text("QCIF");
		if (resolution == 2)
			$("#resolutionTxt").text("QVGA");
		else if (resolution == 3)
			$("#resolutionTxt").text("2/3 D1");
		else if (resolution == 4)
			$("#resolutionTxt").text("3/4 D1");
		else if (resolution == 5)
			$("#resolutionTxt").text("D1");
		else if (resolution == 6)
			$("#resolutionTxt").text("720P");
		else if (resolution == 7)
			$("#resolutionTxt").text("1080i");
		else if (resolution == 8)
			$("#resolutionTxt").text("1080P");

		var videoprofile = data["videoprofile"];
		if (videoprofile == 1)
			$("#videoprofileTxt").text("Simple");
		if (videoprofile == 2)
			$("#videoprofileTxt").text("Advanced Simple");
		if (videoprofile == 3)
			$("#videoprofileTxt").text("Baseline");
		if (videoprofile == 4)
			$("#videoprofileTxt").text("Main");
		else if (videoprofile == 5)
			$("#videoprofileTxt").text("High");
		

		var systemlayer = data["systemlayer"];
		if (systemlayer == 1)
			$("#systemlayerTxt").text("TS");
		if (systemlayer == 2)
			$("#systemlayerTxt").text("3GP");
		if (systemlayer == 3)
			$("#systemlayerTxt").text("mp4");
		if (systemlayer == 4)
			$("#systemlayerTxt").text("flv");
		else if (systemlayer == 5)
			$("#systemlayerTxt").text("rtp");

		var bitratetype = data["bitratetype"];
		if (bitratetype == 1)
			$("#bitratetypeTxt").text("400K");
		if (bitratetype == 2)
			$("#bitratetypeTxt").text("700K");
		if (bitratetype == 3)
			$("#bitratetypeTxt").text("1.3M");
		if (bitratetype == 4)
			$("#bitratetypeTxt").text("2M");
		if (bitratetype == 5)
			$("#bitratetypeTxt").text("2.5M");
		if (bitratetype == 6)
			$("#bitratetypeTxt").text("8M");
		else if (bitratetype == 7)
			$("#bitratetypeTxt").text("10M");
		
		var domain = data["domain"];
		
		if (domain == 257) {
			$("#domain").text("IPTV网络(IPTV TS RTSP)");
		}else if(domain == 258){
			$("#domain").text("IPTV网络(HPD)");
		}else if(domain == 260){
			$("#domain").text("IPTV网络(ISMA RTSP)");
		}else if(domain == 264){
			$("#domain").text("IPTV网络(HLS)");
				
		} else if (domain == 513) {
			$("#domain").text("互联网网络(IPTV TS RTSP)");
		}else if (domain == 514) {
			$("#domain").text("互联网网络(HPD)");
		}else if (domain == 516) {
			$("#domain").text("互联网网络(ISMA RTSP)");
		}else if (domain == 520) {
			$("#domain").text("互联网网络(HLS)");
		
		} else if (domain == 1025) {
			$("#domain").text("移动网络(IPTV TS RTSP)");
		}else if (domain == 1026) {
			$("#domain").text("移动网络(HPD)");
		}else if (domain == 1028) {
			$("#domain").text("移动网络(ISMA RTSP)");
		}else if (domain == 1032) {
			$("#domain").text("移动网络(HLS)");
		
		} else {
			$("#domain").text("IPTV网络(IPTV TS RTSP)");
		}
	}
}

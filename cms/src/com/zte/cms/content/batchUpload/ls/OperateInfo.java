package com.zte.cms.content.batchUpload.ls;

public class OperateInfo
{

    /**
     * 业务对象的ID或编码
     */
    private String id;

    /**
     * 业务对象的名称
     */
    private String name;

    /**
     * 操作成功失败标志
     */
    private String flag;

    /**
     * 操作信息
     */
    private String message;

    public OperateInfo()
    {

    }

    public OperateInfo(String id, String name, String flag, String message)
    {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.message = message;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFlag()
    {
        return flag;
    }

    public void setFlag(String flag)
    {
        this.flag = flag;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}

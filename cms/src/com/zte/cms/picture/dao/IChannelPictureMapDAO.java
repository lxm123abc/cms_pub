package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IChannelPictureMapDAO
{
    /**
     * 新增ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DAOException dao异常
     */
    public void insertChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException;

    /**
     * 根据主键更新ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DAOException dao异常
     */
    public void updateChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException;

    /**
     * 根据主键删除ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException;

    /**
     * 根据主键查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return 满足条件的ChannelPictureMap对象
     * @throws DAOException dao异常
     */
    public ChannelPictureMap getChannelPictureMap(ChannelPictureMap channelPictureMap) throws DAOException;

    /**
     * 根据条件查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return 满足条件的ChannelPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<ChannelPictureMap> getChannelPictureMapByCond(ChannelPictureMap channelPictureMap) throws DAOException;

    /**
     * 根据条件分页查询ChannelPictureMap对象，作为查询条件的参数
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ChannelPictureMap对象，作为查询条件的参数
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ChannelPictureMap channelPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询ChannelPictureMap对象，作为查询条件的参数
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerychanPicMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ChannelPictureMap对象，作为查询条件的参数
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerypchanPicTargetMap(ChannelPictureMap channelPictureMap, int start, int pageSize) throws DAOException;
    
    
    /**
     * 根据条件查询ChannelPictureMap对象
     * 
     * @param channelPictureMap ChannelPictureMap对象
     * @return 满足条件的ChannelPictureMap对象集
     * @throws DAOException dao异常
     */
    public List<ChannelPictureMap> getChannelPicturenormalMapByCond(ChannelPictureMap channelPictureMap) throws DAOException;
}
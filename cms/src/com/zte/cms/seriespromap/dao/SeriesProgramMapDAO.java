package com.zte.cms.seriespromap.dao;

import java.util.List;

import com.zte.cms.seriespromap.dao.ISeriesProgramMapDAO;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class SeriesProgramMapDAO extends DynamicObjectBaseDao implements ISeriesProgramMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("insert seriesProgramMap starting...");
        super.insert("insertSeriesProgramMap", seriesProgramMap);
        log.debug("insert seriesProgramMap end");
    }

    public void updateSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("update seriesProgramMap by pk starting...");
        super.update("updateSeriesProgramMap", seriesProgramMap);
        log.debug("update seriesProgramMap by pk end");
    }

    public void deleteSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("delete seriesProgramMap by pk starting...");
        super.delete("deleteSeriesProgramMap", seriesProgramMap);
        log.debug("delete seriesProgramMap by pk end");
    }

    public SeriesProgramMap getSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("query seriesProgramMap starting...");
        SeriesProgramMap resultObj = (SeriesProgramMap) super.queryForObject("getSeriesProgramMap", seriesProgramMap);
        log.debug("query seriesProgramMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<SeriesProgramMap> getSeriesProgramMapByCond(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("query seriesProgramMap by condition starting...");
        List<SeriesProgramMap> rList = (List<SeriesProgramMap>) super.queryForList("querySeriesProgramMapListByCond",
                seriesProgramMap);
        log.debug("query seriesProgramMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesProgramMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesProgramMapListCntByCond", seriesProgramMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesProgramMap> rsList = (List<SeriesProgramMap>) super.pageQuery("querySeriesProgramMapListByCond",
                    seriesProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesProgramMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesProgramMap seriesProgramMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("querySeriesProgramMapListByCond", "querySeriesProgramMapListCntByCond",
                seriesProgramMap, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DAOException
    {
        log.debug("pageInfoQueryForFrontPage starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesProMapForPageCnt", seriesProgramMap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesProgramMap> rsList = (List<SeriesProgramMap>) super.pageQuery("querySeriesProMapForPage",
                    seriesProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryForFrontPage end");
        return pageInfo;
    }

    public int getMaxSequenceBySeriesindex(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("getMaxSequenceBySeriesindex starting...");
        int rtnMaxSequence = 0;
        Integer maxSequence = (Integer) super.queryForObject("getMaxSequenceBySeriesindex", seriesProgramMap);
        if (maxSequence != null)
        {
            rtnMaxSequence = maxSequence;
        }
        log.debug("getMaxSequenceBySeriesindex end");
        return rtnMaxSequence;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryseriesproMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesProgramMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesProMapSynForPageCnt", seriesProgramMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesProgramMap> rsList = (List<SeriesProgramMap>) super.pageQuery("querySeriesProMapSynForPage",
                    seriesProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesProgramMap by condition end");
        return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryseriesproTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesProgramMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesProTargetMapSynListCntByCond", seriesProgramMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesProgramMap> rsList = (List<SeriesProgramMap>) super.pageQuery("querySeriesProTargetMapSynListByCond",
                    seriesProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesProgramMap by condition end");
        return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public List<SeriesProgramMap> getnormalSeriesProTargetMapByCond(SeriesProgramMap seriesProgramMap) throws DAOException
    {
        log.debug("query seriesProgramMap by condition starting...");
        List<SeriesProgramMap> rList = (List<SeriesProgramMap>) super.queryForList("getSeriesProTargetMapSynListByCond",
                seriesProgramMap);
        log.debug("query seriesProgramMap by condition end");
        return rList;
    }    
    
}
package com.zte.ismp.systemconfig.ls;

import java.util.List;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 提供了系统参数配置的一套接口
 * 
 * @author 钱月清 qian.yueqing@zte.com.cn
 * @version ZXMCISMP-UMAPV2.01.01
 */
public interface IIsysConfigLS
{

    /**
     * <p>
     * 更新UsysConfig对象
     * </p>
     * 
     * @param usysConfig UsysConfig对象
     * @return String类型，返回操作结果：0：操作成功；1：操作失败
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表USYS_CONFIG
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public String updateUsysConfig(UsysConfig usysConfig) throws DomainServiceException;

    /**
     * <p>
     * 查询UsysConfig对象
     * </p>
     * 
     * @param usysConfig UsysConfig对象
     * @return UsysConfig对象
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表USYS_CONFIG
     * @see com.zte.umap.sys.model.UsysConfig
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public UsysConfig getUsysConfig(UsysConfig usysConfig) throws DomainServiceException;

    /**
     * <p>
     * 根据条件查询UsysConfig对象
     * </p>
     * 
     * @param usysConfig UsysConfig对象
     * @return 满足条件的UsysConfig对象集
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表USYS_CONFIG
     * @see com.zte.umap.sys.model.UsysConfig
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public List<UsysConfig> getUsysConfigByCond(UsysConfig usysConfig) throws DomainServiceException;

    /**
     * <p>
     * 根据条件分页查询UsysConfig对象
     * </p>
     * 
     * @param usysConfig UsysConfig对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表USYS_CONFIG
     * @see com.zte.umap.sys.model.UsysConfig
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(UsysConfig usysConfig, int start, int pageSize) throws DomainServiceException;

    /**
     * <p>
     * 根据条件分页查询UsysConfig对象
     * </p>
     * 
     * @param usysConfig UsysConfig对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表USYS_CONFIG
     * @see com.zte.umap.sys.model.UsysConfig
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(UsysConfig usysConfig, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 获取系统参数配置项
     * 
     * @param cfgkey
     * @return
     * @throws DomainServiceException
     */
    public String getUsysConfigByCfgkey(String cfgkey) throws DomainServiceException;
}

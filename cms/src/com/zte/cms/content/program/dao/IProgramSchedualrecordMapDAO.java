package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IProgramSchedualrecordMapDAO
{
     /**
	  * 新增ProgramSchedualrecordMap对象 
	  * 
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @throws DAOException dao异常
	  */	
     public void insertProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;
     
     /**
	  * 根据主键更新ProgramSchedualrecordMap对象
	  * 
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @throws DAOException dao异常
	  */
     public void updateProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;

     /**
	  * 根据条件更新ProgramSchedualrecordMap对象  
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap更新条件
	  * @throws DAOException dao异常
	  */
     public void updateProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;

     /**
	  * 根据主键删除ProgramSchedualrecordMap对象
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @throws DAOException dao异常
	  */
     public void deleteProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;
     
     /**
	  * 根据条件删除ProgramSchedualrecordMap对象
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap删除条件
	  * @throws DAOException dao异常
	  */ 
     public void deleteProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;
     
     /**
	  * 根据主键查询ProgramSchedualrecordMap对象
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @return 满足条件的ProgramSchedualrecordMap对象
	  * @throws DAOException dao异常
	  */
     public ProgramSchedualrecordMap getProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;
     
     /**
	  * 根据条件查询ProgramSchedualrecordMap对象 
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象
	  * @return 满足条件的ProgramSchedualrecordMap对象集
	  * @throws DAOException dao异常
	  */
     public List<ProgramSchedualrecordMap> getProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException;

     /**
	  * 根据条件分页查询ProgramSchedualrecordMap对象，作为查询条件的参数
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询ProgramSchedualrecordMap对象，作为查询条件的参数
	  *
	  * @param programSchedualrecordMap ProgramSchedualrecordMap对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
        
}
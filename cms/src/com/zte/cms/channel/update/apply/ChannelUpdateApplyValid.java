package com.zte.cms.channel.update.apply;

import java.util.ArrayList;
import java.util.List;

import net.sf.cglib.beans.BeanCopier;
import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.ls.ICmsChannelLS;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;

import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelBufDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;


/**
 * 
 * @author Administrator
 * 
 */
public class ChannelUpdateApplyValid extends CommonValid {
    // 日志
    protected ICmsChannelDS channelds = null;
    protected ICmsChannelBufDS channelBufds = null;
    private Log log = SSBBus.getLog(getClass());
    private IPhysicalchannelDS physicalchannelDS;
	private IPhysicalchannelBufDS physicalchannelbufDS;
    private ICmsChannelLS channells = null;

	public IPhysicalchannelBufDS getPhysicalchannelbufDS() {
		return physicalchannelbufDS;
	}

	public void setPhysicalchannelbufDS(
			IPhysicalchannelBufDS physicalchannelbufDS) {
		this.physicalchannelbufDS = physicalchannelbufDS;
	}

    /**
     * 
     * @return ICmsChannelDS
     */
	public ICmsChannelDS getChannelds() {
        return channelds;
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
	public IPhysicalchannelDS getPhysicalchannelDS() {
        return physicalchannelDS;
    }

	/**
	 * 
	 * @param physicalchannelDS
	 *            IPhysicalchannelDS
	 */
	public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS) {
        this.physicalchannelDS = physicalchannelDS;
    }

	/**
	 * 
	 * @param channelds
	 *            ICmsChannelDS
	 */
	public void setChannelds(ICmsChannelDS channelds) {
        this.channelds = channelds;
    }

    /**
     * 
     * @return ICmsChannelBufDS
     */
	public ICmsChannelBufDS getChannelBufds() {
        return channelBufds;
    }

	/**
	 * 
	 * @param channelBufds
	 *            ICmsChannelBufDS
	 */
	public void setChannelBufds(ICmsChannelBufDS channelBufds) {
        this.channelBufds = channelBufds;
    }

    /**
     * 
     * @return ICmsChannelLS
     */
	public ICmsChannelLS getChannells() {
        return channells;
    }

	/**
	 * 
	 * @param channells
	 *            ICmsChannelLS
	 */
	public void setChannells(ICmsChannelLS channells) {
        this.channells = channells;
    }

    @Override
	public AppLogInfoEntity getLogInfo(Long index) {

        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(index);
		try {
            channel = channelds.getCmsChannel(channel);
		} catch (DomainServiceException dsEx) {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

		if (null != channel) {
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
			ResourceMgt
					.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

			String optObjecttype = ChannelConstants
					.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_UPDATE);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptObject(String.valueOf(index));
			String info = ResourceMgt
					.findDefaultText("ucdn.channel.log.wkfw.cntdistribute.update.valid");

			if (null != info) {
                info = info.replace("[channelname]", channel.getChannelname());
            }
            loginfo.setOptDetail(info);
            loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
            return loginfo;
		} else {
            return null;
        }

    }

    @Override
	public void operate(Long index) {
		// 修改状态
		try {
            CmsChannel channelOld = new CmsChannel();
            channelOld.setChannelindex(index);
            channelOld = channelds.getCmsChannel(channelOld);

            CmsChannelBuf channelbuf = new CmsChannelBuf();
            channelbuf.setChannelindex(index);
            channelbuf = channelBufds.getCmsChannelBuf(channelbuf);

            CmsChannel channelNew = new CmsChannel();

			BeanCopier copier = BeanCopier.create(CmsChannelBuf.class,
					CmsChannel.class, true);
            copier.copy(channelbuf, channelNew, new BeanPropertyConvert());
			//channelNew.setStatus(channelOld.getStatus());
            channelNew.setWorkflow(Integer.valueOf(0));
			channelds.updateCmsChannel(channelNew);

			PhysicalchannelBuf buf = new PhysicalchannelBuf();
			buf.setChannelid(channelOld.getChannelid());
			List listbuf = physicalchannelbufDS.getPhysicalchannelBufByCond(buf);
			if(listbuf!=null&&listbuf.size()>0){
				buf = (PhysicalchannelBuf)listbuf.get(0);
			}

            // 根据老的状态，修改新的状态
			
            int oldStatus = channelbuf.getStatus();
            if (oldStatus == 0 || oldStatus == 80) // 待发布或者取消同步成功
            {

                channelNew.setStatus(oldStatus);
				
                channelds.updateCmsChannel(channelNew);
				List list = new ArrayList();
				Physicalchannel ph = new Physicalchannel();
				ph.setChannelid(channelNew.getChannelid());
				list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
				if (list != null && list.size() > 0) 
				{
					for (int i = 0; i < list.size(); i++) 
					{
						Physicalchannel ch = (Physicalchannel) list.get(i);
						if (ch.getPhysicalchannelindex().equals( buf.getPhysicalchannelindex())) 
						{
							BeanCopier copierph = BeanCopier.create(PhysicalchannelBuf.class, Physicalchannel.class,true);
							copierph.copy(buf, ch, new BeanPropertyConvert());
						}
						ch.setStatus(channelNew.getStatus());
						ch.setWorkflow(channelNew.getWorkflow());
						physicalchannelDS.updatePhysicalchannel(ch);

						// 修改子内容状态
            }
				}
			} else {
                // channelNew.setStatus(40); //修改同步中
				
				// 生成同步任务
				try {
                    CmsChannel[] channel = new CmsChannel[1];
                    channel[0] = channelNew;
					
            List list = new ArrayList();
            Physicalchannel ph = new Physicalchannel();
            ph.setChannelid(channelNew.getChannelid());
            list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
					if (list != null && list.size() > 0) {
						for (int i = 0; i < list.size(); i++) {
                    Physicalchannel ch = (Physicalchannel) list.get(i);

							if (ch.getPhysicalchannelindex().equals(buf
									.getPhysicalchannelindex())) {
								BeanCopier copierph = BeanCopier.create(
										PhysicalchannelBuf.class, Physicalchannel.class,
										true);
								copierph.copy(buf, ch, new BeanPropertyConvert());
							}
                    ch.setWorkflow(channelNew.getWorkflow());
							ch.setStatus(channelNew.getStatus());
                    physicalchannelDS.updatePhysicalchannel(ch);
                    // 修改子内容状态
                }
            }
					//改为修改物理频道完毕后再同步频道和物理频道，避免同步的是旧数据
					//channells.updatePublishChannel(channel);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}

			

		} catch (DomainServiceException dsEx) {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }
}

package com.zte.cms.content.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmTagContent extends DynamicBaseObject
{
    private java.lang.Long tagcontentindex;
    private java.lang.Long tagindex;
    private java.lang.String contentid;

    public java.lang.Long getTagcontentindex()
    {
        return tagcontentindex;
    }

    public void setTagcontentindex(java.lang.Long tagcontentindex)
    {
        this.tagcontentindex = tagcontentindex;
    }

    public java.lang.Long getTagindex()
    {
        return tagindex;
    }

    public void setTagindex(java.lang.Long tagindex)
    {
        this.tagindex = tagindex;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public void initRelation()
    {
        this.addRelation("tagcontentindex", "TAGCONTENTINDEX");
        this.addRelation("tagindex", "TAGINDEX");
        this.addRelation("contentid", "CONTENTID");
    }
}

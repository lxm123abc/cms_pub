package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.dao.IIcmSourcefileDAO;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmSourcefileDAO extends DynamicObjectBaseDao implements IIcmSourcefileDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("insert icmSourcefile starting...");
        super.insert("insertIcmSourcefile", icmSourcefile);
        log.debug("insert icmSourcefile end");
    }

    public void updateIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("update icmSourcefile by pk starting...");
        super.update("updateIcmSourcefile", icmSourcefile);
        log.debug("update icmSourcefile by pk end");
    }

    public void updateIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("update icmSourcefile by conditions starting...");
        super.update("updateIcmSourcefileByCond", icmSourcefile);
        log.debug("update icmSourcefile by conditions end");
    }

    public void deleteIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("delete icmSourcefile by pk starting...");
        super.delete("deleteIcmSourcefile", icmSourcefile);
        log.debug("delete icmSourcefile by pk end");
    }

    public void deleteIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("delete icmSourcefile by conditions starting...");
        super.delete("deleteIcmSourcefileByCond", icmSourcefile);
        log.debug("update icmSourcefile by conditions end");
    }

    public IcmSourcefile getIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("query icmSourcefile starting...");
        IcmSourcefile resultObj = (IcmSourcefile) super.queryForObject("getIcmSourcefile", icmSourcefile);
        log.debug("query icmSourcefile end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<IcmSourcefile> getIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException
    {
        log.debug("query icmSourcefile by condition starting...");
        List<IcmSourcefile> rList = (List<IcmSourcefile>) super.queryForList("queryIcmSourcefileListByCond",
                icmSourcefile);
        log.debug("query icmSourcefile by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmSourcefile by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmSourcefileListCntByCond", icmSourcefile)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmSourcefile> rsList = (List<IcmSourcefile>) super.pageQuery("queryIcmSourcefileListByCond",
                    icmSourcefile, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmSourcefile by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryIcmSourcefileListByCond", "queryIcmSourcefileListCntByCond", icmSourcefile,
                start, pageSize, puEntity);
    }

}
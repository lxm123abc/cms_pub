package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceSchedualMapDAO
{
    /**
     * 新增ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DAOException dao异常
     */
    public void insertServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException;

    /**
     * 根据主键更新ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DAOException dao异常
     */
    public void updateServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException;

    /**
     * 根据主键删除ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @throws DAOException dao异常
     */
    public void deleteServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException;

    /**
     * 根据主键查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @return 满足条件的ServiceSchedualMap对象
     * @throws DAOException dao异常
     */
    public ServiceSchedualMap getServiceSchedualMap(ServiceSchedualMap serviceSchedualMap) throws DAOException;

    /**
     * 根据条件查询ServiceSchedualMap对象
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @return 满足条件的ServiceSchedualMap对象集
     * @throws DAOException dao异常
     */
    public List<ServiceSchedualMap> getServiceSchedualMapByCond(ServiceSchedualMap serviceSchedualMap)
            throws DAOException;

    /**
     * 根据条件分页查询ServiceSchedualMap对象，作为查询条件的参数
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServiceSchedualMap对象，作为查询条件的参数
     * 
     * @param serviceSchedualMap ServiceSchedualMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServiceSchedualMap serviceSchedualMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
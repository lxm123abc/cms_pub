package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.dao.ICmsProgramBufDAO;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramBufDAO extends DynamicObjectBaseDao implements ICmsProgramBufDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("insert cmsProgramBuf starting...");
        super.insert("insertCmsProgramBuf", cmsProgramBuf);
        log.debug("insert cmsProgramBuf end");
    }

    public void insertCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException
    {
        log.debug("insert cmsProgramBufList starting...");
        if (null != cmsProgramBufList)
        {
            super.insertBatch("insertCmsProgramBuf", cmsProgramBufList);
        }
        log.debug("insert cmsProgramBufList end");
    }

    public void updateCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("update cmsProgramBuf by pk starting...");
        super.update("updateCmsProgramBuf", cmsProgramBuf);
        log.debug("update cmsProgramBuf by pk end");
    }

    public void updateCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException
    {
        log.debug("update cmsProgramBufList by pk starting...");
        super.updateBatch("updateCmsProgramBuf", cmsProgramBufList);
        log.debug("update cmsProgramBufList by pk end");
    }

    public void updateCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("update cmsProgramBuf by conditions starting...");
        super.update("updateCmsProgramBufByCond", cmsProgramBuf);
        log.debug("update cmsProgramBuf by conditions end");
    }

    public void updateCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DAOException
    {
        log.debug("update cmsProgramBufList by conditions starting...");
        super.updateBatch("updateCmsProgramBufByCond", cmsProgramBufList);
        log.debug("update cmsProgramBufList by conditions end");
    }

    public void deleteCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("delete cmsProgramBuf by pk starting...");
        super.delete("deleteCmsProgramBuf", cmsProgramBuf);
        log.debug("delete cmsProgramBuf by pk end");
    }

    public void deleteCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DAOException
    {
        log.debug("delete cmsProgramBufList by pk starting...");
        super.deleteBatch("deleteCmsProgramBuf", cmsProgramBufList);
        log.debug("delete cmsProgramBufList by pk end");
    }

    public void deleteCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("delete cmsProgramBuf by conditions starting...");
        super.delete("deleteCmsProgramBufByCond", cmsProgramBuf);
        log.debug("delete cmsProgramBuf by conditions end");
    }

    public void deleteCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DAOException
    {
        log.debug("delete cmsProgramBufList by conditions starting...");
        super.deleteBatch("deleteCmsProgramBufByCond", cmsProgramBufList);
        log.debug("delete cmsProgramBufList by conditions end");
    }

    public CmsProgramBuf getCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("query cmsProgramBuf starting...");
        CmsProgramBuf resultObj = (CmsProgramBuf) super.queryForObject("getCmsProgramBuf", cmsProgramBuf);
        log.debug("query cmsProgramBuf end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DAOException
    {
        log.debug("query cmsProgramBuf by condition starting...");
        List<CmsProgramBuf> rList = (List<CmsProgramBuf>) super.queryForList("queryCmsProgramBufListByCond",
                cmsProgramBuf);
        log.debug("query cmsProgramBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf, PageUtilEntity puEntity)
            throws DAOException
    {
        log.debug("query cmsProgramBuf by condition starting...");
        List<CmsProgramBuf> rList = (List<CmsProgramBuf>) super.queryForList("queryCmsProgramBufListByCond",
                cmsProgramBuf, puEntity);
        log.debug("query cmsProgramBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramBuf by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramBufListCntByCond", cmsProgramBuf)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramBuf> rsList = (List<CmsProgramBuf>) super.pageQuery("queryCmsProgramBufListByCond",
                    cmsProgramBuf, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramBuf by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsProgramBufListByCond", "queryCmsProgramBufListCntByCond", cmsProgramBuf,
                start, pageSize, puEntity);
    }

}
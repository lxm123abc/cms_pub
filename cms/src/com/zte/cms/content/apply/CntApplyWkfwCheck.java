package com.zte.cms.content.apply;

import java.util.List;

import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.ResourceMgt;

/**
 * <p>
 * 文件名称: CntApplyWkfwCheck.java
 * </p>
 * <p>
 * 文件描述: 内容申请工作流流程校验类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供校验内容订单是否可以提交审核、是否可以审核的方法
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntApplyWkfwCheck implements ICntApplyWkfwCheck
{
    private Log log = SSBBus.getLog(getClass());
    private ICmsMovieDS cmsMovieDS = null;
    private ICmsProgramWkfwhisDS cmsProgramWkfwhisDS = null;

    /**
     * <p>
     * 检查内容订单是否可以提交审核
     * </p>
     * 
     * @param orderindex Long类型
     * @return 返回检查结果 <br/>350000：校验通过 <br/>350001：内容不存在 <br/>350002：内容状态不正确 <br/>350003：内容不在工作流中或所处工作流不正确
     *         <br/>350004：内容工作流流程状态不正确
     * @throws Exception抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String checkCntWkfwApply(CmsProgram cmsProgram) throws Exception
    {
        // 判断内容是否存在
        if (null == cmsProgram)
        {// 内容不存在
            log.error("errorCode=" + CntConstants.CNT_NOT_EXIST
                    + " : content does not exist, cannot submit it for audit");
            return CntConstants.CNT_NOT_EXIST;
        }

        // 判断内容状态是否正确 110：待审核 130：一审失败 150：二审失败 160：回收站 180：三审失败
        if ((110 != cmsProgram.getStatus().intValue()) && (130 != cmsProgram.getStatus().intValue())
                && (150 != cmsProgram.getStatus().intValue()) && (160 != cmsProgram.getStatus().intValue())
                && (180 != cmsProgram.getStatus().intValue()))
        {// 内容状态不正确
            log.error("errorCode=" + CntConstants.CNT_STATUS_ERROR
                    + " : content status error, cannot submit it for audit");
            return CntConstants.CNT_STATUS_ERROR;
        }

        // 获取内容的所有子内容
        List<CmsMovie> cmsMovieList = null;
        CmsMovie cmsMovie = new CmsMovie();
        cmsMovie.setProgramid(cmsProgram.getProgramid());
        cmsMovieList = cmsMovieDS.getCmsMovieByCond(cmsMovie);
        // 判断内容下是否存在子内容
        if (null == cmsMovieList || 0 == cmsMovieList.size())
        {// 内容没有子内容,不能提审 250006
            log.error("errorCode=" + CntConstants.CNT_HASNO_SUBCNT
                    + " : content has no subcontents, cannot submit it for audit");
            return CntConstants.CNT_HASNO_SUBCNT;
        }
         // 判断内容的子内容状态是否是可以提审的状态：待审核（110），一审失败（130），二审失败（150），回收站（160），三审失败（180）。
         boolean flag = true; 
         for (int j = 0; j < cmsMovieList.size(); j++)
         {
             if ((cmsMovieList.get(j).getStatus()!=110)&&(cmsMovieList.get(j).getStatus()!=130)
                     &&(cmsMovieList.get(j).getStatus()!=150)&&(cmsMovieList.get(j).getStatus()!=160)
                     &&(cmsMovieList.get(j).getStatus()!=180))
             {
                 flag = false;
                 break;
             }
         }
         if (flag == false)
         {//内容下的子内容状态不正确 "250007"
             log.error("errorCode=" + CntConstants.SUBCNT_STATUS_ERROR + " : subcontent status error, cannot submit content for audit");
             return CntConstants.SUBCNT_STATUS_ERROR;
         }		
         
         //判断内容的加密状态是否能提审
         if (cmsProgram.getEncryptionstatus() != 1) //内容加密状态不为已加密
         {
     	    int encryptCount = 0;
            for(CmsMovie movie:cmsMovieList)
            {
                if(movie.getEncryptionstatus() != 0) //子内容加密状态不为未加密
                {
         	       encryptCount++;
                }
            }
            if (encryptCount > 0)  //内容下所有子内容不全是未加密状态
            {
                return CntConstants.CNT_ENCRYPTIONSTATUS_ERROR;
            }

         }         
         
        // 判断内容已被提审 所处工作流是否正确
        if ((0 != cmsProgram.getWorkflow()))
        {// 判断内容已被提审 "250003"
            log.error("errorCode=" + CntConstants.CNT_WORKFLOW_ERROR
                    + " : content is not in the right workflow, cannot submit it for audit");
            return CntConstants.CNT_WORKFLOW_ERROR;
        }

        log.debug("errorCode=" + CntConstants.CNT_SUCCESS + ": content apply wkfw check success");
        return CntConstants.CNT_SUCCESS; // 内容可以提交审核
    }

    /**
     * <p>
     * 检查内容是否可以发起申请工作流的审核操作
     * </p>
     * 
     * @param orderindex Long类型
     * @return 返回检查结果 <br/>350000：校验通过 <br/>350001：内容不存在 <br/>350002：内容状态不正确 <br/>350003：内容不在工作流中或所处工作流不正确
     *         <br/>350004：内容工作流流程状态不正确
     * @throws Exception抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    public String checkCntWkfwAudit(CmsProgram cmsProgram, String processid, String nodeName) throws Exception
    {

        if (null == cmsProgram)
        {// 内容不存在
            log.error("errorCode=" + CntConstants.CNT_NOT_EXIST + " : content does not exist, cannot audit");
            return CntConstants.CNT_NOT_EXIST;
        }

        if (!((CntConstants.CNT_STATUS_ONEAUDITING == cmsProgram.getStatus() && (ResourceMgt.findDefaultText("cnt.first.audit")).equals(nodeName))
                || (CntConstants.CNT_STATUS_TWOAUDITING == cmsProgram.getStatus() && (ResourceMgt.findDefaultText("cnt.second.audit")).equals(nodeName)) || (CntConstants.CNT_STATUS_THREEAUDITING == cmsProgram
                .getStatus() && (ResourceMgt.findDefaultText("cnt.third.audit")).equals(nodeName))))
        {
            log.error("errorCode=" + CntConstants.CNT_STATUS_ERROR + " : content status error, cannot audit");
            return CntConstants.CNT_STATUS_ERROR;
        }

        // 判断内容是否处于工作流中
        if (1 != cmsProgram.getWorkflow())
        {// 内容尚未被提审或审核流程已被处理完成 250008
            log.error("errorCode=" + CntConstants.CNT_AUDITWKFW_ERROR + " : content audit worflow error, cannot audit");
            return CntConstants.CNT_AUDITWKFW_ERROR;
        }

        // 判断当前审核流程是否被其他操作员操作过
        CmsProgramWkfwhis cmsProgramWkfwhis = new CmsProgramWkfwhis();
        List<CmsProgramWkfwhis> cmsProgramWkfwhisList = null;
        cmsProgramWkfwhis.setNodename(nodeName);
        cmsProgramWkfwhis.setProgramid(cmsProgram.getProgramid());
        cmsProgramWkfwhis.setWorkflowindex(Long.parseLong(processid));
        cmsProgramWkfwhisList = cmsProgramWkfwhisDS.getCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
        if (null != cmsProgramWkfwhisList && cmsProgramWkfwhisList.size() > 0)
        {
            log.error("errorCode=" + CntConstants.CNT_HASBEEN_AUDITED + " : content has been audited, cannot audit");
            return CntConstants.CNT_HASBEEN_AUDITED;
        }

        log.debug("errorCode=" + CntConstants.CNT_SUCCESS + ": content aduit wkfw check success");
        return CntConstants.CNT_SUCCESS; // 内容可以审核
    }

    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setCmsProgramWkfwhisDS(ICmsProgramWkfwhisDS cmsProgramWkfwhisDS)
    {
        this.cmsProgramWkfwhisDS = cmsProgramWkfwhisDS;
    }

}

package com.zte.ucm;

import com.zte.ucm.chkconf.ChkAssign;
import com.zte.ucm.updateconf.UpdateAssign;

import java.io.PrintStream;

public class SmartConfig
{
  public static void main(String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length == 5))
    {
      Constants.CONFIGFILEPATH = paramArrayOfString[1];
      Constants.CONFIGAPPLYITEMSXML = paramArrayOfString[2];
      Constants.LOGPATH = paramArrayOfString[3];
      Constants.ERRORFILENAME = paramArrayOfString[4];
    }
    else {
      System.out.println("The wrong input parameters!!!");
      return;
    }
    String str = paramArrayOfString[0];
    Object localObject;
    if ("0".equals(str)) {
      localObject = new UpdateAssign();
      ((UpdateAssign)localObject).exe();
    }
    else if ("1".equals(str)) {
      localObject = new ChkAssign();
      ((ChkAssign)localObject).exe();
    }
  }
}
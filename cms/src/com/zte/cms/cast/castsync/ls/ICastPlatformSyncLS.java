package com.zte.cms.cast.castsync.ls;

import java.util.List;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICastPlatformSyncLS
{
    /**
     * 根据条件分页查询castPlatformSync对象
     * @param CastPlatformSync castPlatformSync对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(CastPlatformSync castPlatformSync, int start, int pageSize) throws Exception;

    
    /**
     * 人物发布到平台
     *
     * @param castindex 人物index 
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType  同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCastToPlatform(Long castindex, int platform ,int syncType) throws Exception;
    
    public String batchPublishCastToPlatform(List<Castcdn> castList, int platform ,int syncType) throws Exception;
    
    public TableDataInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize) throws Exception;
    
    public String publishCastPicMapToPlatform(Long mapindex,int platform,int syncType) throws Exception;
    
    public String batchPublishCastPicMapToPlatform(List<CastPictureMap> castPictureMapList, int platform ,int syncType) throws Exception;
}

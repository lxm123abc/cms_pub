package com.zte.cms.batchobjectrecord.service;

import java.util.List;
import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IBatchObjectRecordDS
{
    /**
     * 新增BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public void insertBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 批量更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateBatchObjectRecordList(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException;

    /**
     * 根据条件更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 根据条件批量更新BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateBatchObjectRecordListByCond(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException;

    /**
     * 删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 批量删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeBatchObjectRecordList(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException;

    /**
     * 根据条件删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 根据条件批量删除BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeBatchObjectRecordListByCond(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException;

    /**
     * 查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @return BatchObjectRecord对象
     * @throws DomainServiceException ds异常
     */
    public BatchObjectRecord getBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException;

    /**
     * 根据条件查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象
     * @return 满足条件的BatchObjectRecord对象集
     * @throws DomainServiceException ds异常
     */
    public List<BatchObjectRecord> getBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord)
            throws DomainServiceException;

    /**
     * 根据条件分页查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询BatchObjectRecord对象
     * 
     * @param batchObjectRecord BatchObjectRecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
package com.zte.cms.commonSync.object;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.MD5;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CreateXmlFile4EPG extends AbstractCreateXmlFile4Object{
	
    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        String md5 = null;
        String needUnTar = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST-UPDATE";
            needUnTar = "1";
        }
        else if(2 == syncType)
        {
            action = "REGIST-UPDATE";
            needUnTar = "1";
        }else  if (3 == syncType)
        {
            action = "DELETE";
            needUnTar = "0";
        }
        else
        {
            return null;
        }
              
        Date date = new Date();
        long milldate = date.getTime();
        String fileName = "epgfile_" +  "20_" + milldate+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "epgfile" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        
        List<Epgtemplt> list = (List<Epgtemplt>) objectMap.get("epgtemplt");
        Epgtemplt epgtemplt = list.get(0);
        
        String  sourcefile = epgtemplt.getAddresspath().substring(0,epgtemplt.getAddresspath().lastIndexOf("."))+".tar";
        String  sourceurl = "";
        sourceurl = com.zte.ismp.common.util.CommonUtils.getConfigValue("cms.epgfilesyn.for20ftpaddress") + sourcefile;
        
        File tarfile = new File( GlobalConstants.getMountPoint() + sourcefile);
        if (tarfile.exists()){
          try
          {
              md5 = MD5.getFileMD5String(tarfile);
          }catch(Exception e)
          {
              e.printStackTrace();
              md5 = "";
          }
        }
        else
        {
        	md5 = "";
        }
        
        Element epgObjectElement = null;
        Element epgPertyElement = null;

        // 元素节点
        epgObjectElement = objectsElement.addElement("Object");
        epgObjectElement.addAttribute("ElementType", "EPGFileSet");
        epgObjectElement.addAttribute("ID",Long.toString(milldate)); //ready

        // 属性节点                
        writeElement(epgObjectElement, epgPertyElement, "EPGGroup",epgtemplt.getEpggroup(), false);
        writeElement(epgObjectElement, epgPertyElement, "SystemFile", "", false); //ready
        writeElement(epgObjectElement, epgPertyElement, "NeedUnTar", needUnTar, false);  //ready
        writeElement(epgObjectElement, epgPertyElement, "BeginTime", "", false);  //ready

         // 元素节点
        epgObjectElement = objectsElement.addElement("Object");
        epgObjectElement.addAttribute("ElementType", "EPGFile");
        epgObjectElement.addAttribute("ID",Long.toString(milldate) ); //ready
        epgObjectElement.addAttribute("Action",action ); 

        // 属性节点                
        writeElement(epgObjectElement, epgPertyElement, "SourceUrl",sourceurl, false);
        writeElement(epgObjectElement, epgPertyElement, "DestPath", "", false); 
        writeElement(epgObjectElement, epgPertyElement, "DestFile", "", false);  
        writeElement(epgObjectElement, epgPertyElement, "MD5", md5, false);  
        
        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
    	String action = null;
        String md5 = null;
        String needUnTar = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST-UPDATE";
            needUnTar = "1";
        }
        else if(2 == syncType)
        {
            action = "REGIST-UPDATE";
            needUnTar = "1";
        }else  if (3 == syncType)
        {
            action = "DELETE";
            needUnTar = "0";
        }
        else
        {
            return null;
        }
              
        Date date = new Date();
        long milldate = date.getTime();
        String fileName = "epgfile_" +  "30_" + milldate+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "epgfile" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        
        List<Epgtemplt> list = (List<Epgtemplt>) objectMap.get("epgtemplt");
        Epgtemplt epgtemplt = list.get(0);
        
        String  sourcefile = epgtemplt.getAddresspath().substring(0,epgtemplt.getAddresspath().lastIndexOf("."))+".tar";        
        String  sourceurl = "";
        sourceurl = com.zte.ismp.common.util.CommonUtils.getConfigValue("cms.epgfilesyn.ftpaddress") + sourcefile;
        File tarfile = new File(GlobalConstants.getMountPoint() + sourcefile);
        if (tarfile.exists()){
          try
          {
              md5 = MD5.getFileMD5String(tarfile);
          }catch(Exception e)
          {
              e.printStackTrace();
              md5 = "";
          }
        }
        else
        {
        	md5 = "";
        }
        
        Element epgObjectElement = null;
        Element epgPertyElement = null;

        // 元素节点
        epgObjectElement = objectsElement.addElement("Object");
        epgObjectElement.addAttribute("ElementType", "EPGFileSet");
        epgObjectElement.addAttribute("ID",Long.toString(milldate)); //ready

        // 属性节点                
        writeElement(epgObjectElement, epgPertyElement, "EPGGroup",epgtemplt.getEpggroup(), false);
        writeElement(epgObjectElement, epgPertyElement, "SystemFile", "", false); //ready
        writeElement(epgObjectElement, epgPertyElement, "NeedUnTar", needUnTar, false);  //ready
        writeElement(epgObjectElement, epgPertyElement, "BeginTime", "", false);  //ready

         // 元素节点
        epgObjectElement = objectsElement.addElement("Object");
        epgObjectElement.addAttribute("ElementType", "EPGFile");
        epgObjectElement.addAttribute("ID",Long.toString(milldate) ); //ready
        epgObjectElement.addAttribute("Action",action ); 

        // 属性节点                
        writeElement(epgObjectElement, epgPertyElement, "SourceUrl",sourceurl, false);
        writeElement(epgObjectElement, epgPertyElement, "DestPath", "", false); 
        writeElement(epgObjectElement, epgPertyElement, "DestFile", "", false);  
        writeElement(epgObjectElement, epgPertyElement, "MD5", md5, false);  
        
        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return retPath;
    }

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        return null;
    }
    
	//public static void main(String args[]){
	//	File tarfile = new File("C:\\Documents and Settings\\Administrator\\My Documents\\My Videos\\test.tar");		
	//	Date date = new Date();
    //    System.out.println(date.getTime());           
    //    try{
    //    	String md5 = MD5.getFileMD5String(tarfile);
    //    	System.out.println(md5);
    //    }catch(Exception e)
    //    {
    //    	e.printStackTrace();
    //    }     
    //    Date date2 = new Date();
    //    System.out.println(date2.getTime());
	//}
}

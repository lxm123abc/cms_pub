package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramPictureMapDS
{
    /**
     * 新增ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException;

    /**
     * 更新ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException;

    /**
     * 批量更新ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateProgramPictureMapList(List<ProgramPictureMap> programPictureMapList)
            throws DomainServiceException;

    /**
     * 删除ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException;

    /**
     * 批量删除ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeProgramPictureMapList(List<ProgramPictureMap> programPictureMapList)
            throws DomainServiceException;

    /**
     * 查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return ProgramPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public ProgramPictureMap getProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException;

    /**
     * 根据条件查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return 满足条件的ProgramPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ProgramPictureMap> getProgramPictureMapByCond(ProgramPictureMap programPictureMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    /**
     * 根据条件分页查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据条件分页查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException;    
    
    /**
     * 根据条件查询ProgramPictureMap对象
     * 
     * @param programPictureMap ProgramPictureMap对象
     * @return 满足条件的ProgramPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ProgramPictureMap> getProgramnormalPictureMapByCond(ProgramPictureMap programPictureMap)
            throws DomainServiceException;    
}

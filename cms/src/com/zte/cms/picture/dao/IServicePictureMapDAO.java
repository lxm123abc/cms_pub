package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.model.ServicePictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServicePictureMapDAO
{
    /**
     * 新增ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DAOException dao异常
     */
    public void insertServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException;

    /**
     * 根据主键更新ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DAOException dao异常
     */
    public void updateServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException;

    /**
     * 根据主键删除ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @throws DAOException dao异常
     */
    public void deleteServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException;

    /**
     * 根据主键查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @return 满足条件的ServicePictureMap对象
     * @throws DAOException dao异常
     */
    public ServicePictureMap getServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException;

    /**
     * 根据条件查询ServicePictureMap对象
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @return 满足条件的ServicePictureMap对象集
     * @throws DAOException dao异常
     */
    public List<ServicePictureMap> getServicePictureMapByCond(ServicePictureMap servicePictureMap) throws DAOException;

    /**
     * 根据条件分页查询ServicePictureMap对象，作为查询条件的参数
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ServicePictureMap对象，作为查询条件的参数
     * 
     * @param servicePictureMap ServicePictureMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
package com.zte.cms.basicdata.area.ls;

import java.util.HashMap;
import java.util.List;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.sys.model.Tree;
import com.zte.umap.sys.model.UsysCity;

/**
 * 提供了区域管理中城市管理的一套接口
 * 
 * @author 钱月清 qian.yueqing@zte.com.cn
 * @version ZXMCISMP-UMAPV2.01.01
 */
public interface IIsysCityLS
{
    public Tree insertUsysCityInIptvTree(String provid, UsysCity usysCity) throws DomainServiceException;

    public Tree updateUsysCityInIptvTree(UsysCity usysCity) throws DomainServiceException;

    public Tree removeUsysCityFromIptvTree(UsysCity usysCity) throws DomainServiceException;

    public UsysCity getUsysCity(UsysCity usysCity) throws DomainServiceException;

    public List<UsysCity> getUsysCityByCond(UsysCity usysCity) throws DomainServiceException;

    public List<Tree> getUsysCityInIptvTree(Tree tree) throws DomainServiceException;

    public HashMap<String, Object> getSysCity(String selectedValue, Boolean needSelect) throws DomainServiceException;

    public HashMap<String, Object> getSysCityByProvid(String provid, String selectedValue, Boolean needSelect)
            throws DomainServiceException;

    public UsysCity getUsysCityById(UsysCity usysCity) throws DomainServiceException;

    public String getUsysCityByName(UsysCity usyscity) throws DomainServiceException;

}

    
  //检查密码范围，只能包含数字或字母或特殊字符
  function checkpwd0(pwd){
      //change chenwenbin 09.11.18
      //修改正则表达式，使密码只匹配 ~!@#$%^&*()-=_+ 这几个特殊字符
      var pattern = /^[0-9a-zA-Z~!@#\$%\^&\*\(\)\-=_\+]*$/;

      if (pattern.test(pwd)) 
          return (true);
      else 
          return (false);
  }
      
  //检查密码范围，包含且包含包含数字
  function checkpwd1(pwd){
	  var allValid = true;
	  for (i = 0;i<pwd.length;i++)
	  {
	
	    ch = pwd.charAt(i);
	    if((ch<='9' && ch >= '0')){ 
	    }
	    else{
	     allValid = false;
	    }
	  }
	  if (!allValid) return (false);
	  else return (true);
  }
  
  //检查密码范围，包含且包含字母
  function checkpwd2(pwd){
	  var allValid = true;
	  for (i = 0;i<pwd.length;i++)
	  {
	    ch = pwd.charAt(i);
	    if((ch <='z' && ch >= 'a') ||(ch <='Z' && ch >='A')){ 
	    }
	    else{
	     allValid = false;
	    }
	  }
	  if (!allValid) return (false);
	  else return (true);
  }

  //检查密码范围，包含且包含数字和字母	
  function checkpwd3(pwd){
	  var allValid = true;
	  var num = 0;
	  var num1 = 0;
      //修改正则表达式，使密码只匹配 ~!@#$%^&*()-=_+ 这几个特殊字符		  
	  var pattern = /^[0-9a-zA-Z]*$/;
	  
	  for (i = 0;i<pwd.length;i++)
	  {
	    ch = pwd.charAt(i);
	    if(ch<='9' && ch >= '0'){
	    	num = num + 1;
	    	continue;
	    }
	    
	    if(ch <='z' && ch >= 'a' || ch <='Z' && ch >='A'){
	    	num1 = num1 + 1;
	    	continue;
	    }
	  }

	  if((num > 0 && num1 > 0) && pattern.test(pwd)){
	  	allValid = true;
	  }else{
	  	allValid = false;
	  }
	  
	  if (!allValid) return (false);
	  else return (true);
  }	

  //检查密码范围，包含且包含数字、字母和特殊字符
  function checkpwd4(pwd){
	  var allValid = true;
	  var num = 0;
	  var num1 = 0;
	  var num2 = 0;
      //修改正则表达式，使密码只匹配 ~!@#$%^&*()-=_+ 这几个特殊字符		  
	  var pattern = /^[~!@#\$%\^&\*\(\)\-=_\+]*$/;
	  
	  for (i = 0;i<pwd.length;i++)
	  {
	    ch = pwd.charAt(i);
	    if(ch<='9' && ch >= '0'){
	    	num = num + 1;
	    	continue;
	    }
	    
	    if(ch <='z' && ch >= 'a' || ch <='Z' && ch >='A'){
	    	num1 = num1 + 1;
	    	continue;
	    }
	   
	    if(pattern.test(ch)){
	    	num2 = num2 + 1;
	    	continue;
	    }
	  }

	  if((num > 0 && num1 > 0 && num2 > 0) && checkpwd0(pwd)){
	  	allValid = true;
	  }else{
	  	allValid = false;
	  }
	  
	  if (!allValid) return (false);
	  else return (true);
  } 
  
  //判断是否为数字
  function isNumber(value){
     if(value== "0")
        return true;
     var re = /^[1-9]+[0-9]*]*$/;    //判断正整数 /^[1-9]+[0-9]*]*$/   
     if (!re.test(value))
     {
         return false;
     }
     return true;
  }
  
  
  // 得到字符串的长度（汉字按3个字符计）
  function strlength(str){
      var l=str.length;
      var n=l;
      for(var i=0;i<l;i++)
          if(str.charCodeAt(i)<0||str.charCodeAt(i)>255)
              n+=2;
      return n;
  }  

   // 检查输入是否合法，包括汉字
  function CheckInputStr(Sender,strName)
  {
  	var i = 0;
	var sValue = Sender.value;
	for( i = 0; i < sValue.length;  i++)
	{
		var sChar = sValue.charAt(i);
		if (((sChar < 'A') || (sChar > 'Z')) && ((sChar < 'a') || (sChar > 'z')) &&
			((sChar < '0') || (sChar > '9')) && (sChar != '_') && (sChar != '@') && (sChar != '-')&& (sChar != '(')&& (sChar != ')')&& (sChar != '[')&& (sChar != ']') && (sChar != '*')&& (sChar != '$')&& (sChar != '{')&& (sChar != '}')&& (sChar != '!')&& (sChar != '#') && (sChar != '/')
			 && (sChar != '.') &&(sChar!=' ') && (sValue.charCodeAt(i)>0) && (sValue.charCodeAt(i)<255) )
		{
			alert(strName + ' ' + $res_entry('purview.operInfo.two','不可含有非法字符，例如') + " " +sChar+" ");
			Sender.select();
			Sender.focus();
			return false;
		}
	}
	return true;
  } 
  
  function trim (str) {
      if (typeof(str) != 'string')
         return '';
      var tmp = leftTrim(str);
      return rightTrim(tmp);
   }  
   
 // 删除字符串的左边空格
   function leftTrim (str) {
      if (typeof(str) != 'string')
         return '';
      var tmp = str;
      var i = 0;
      for (i = 0; i < str.length; i++) {
         if (tmp.substring(0,1) == ' ')
            tmp = tmp.substring(1,tmp.length);
         else
            return tmp;
      }
   }

   // 删除字符串的右边空格
   function rightTrim (str) {
      if (typeof(str) != 'string')
         return '';
      var tmp = str;
      var i = 0;
      for (i = str.length - 1; i >= 0; i--) {
         if (tmp.substring(tmp.length - 1,tmp.length) == ' ')
            tmp = tmp.substring(0,tmp.length - 1);
         else
            return tmp;
      }
   }   
  
  //测试某个字符是属于哪一类.  
   function CharMode(iN){  
     if (iN>=48 && iN <=57) //数字  
         return 1;  
     if ((iN>=65 && iN <=90) || (iN>=97 && iN <=122)) //大小写字母  
         return 2;  
     else  
         return 4; //特殊字符  
   }  
   
   //计算出当前密码当中一共有多少种模式  
  function bitTotal(num){  
      modes=0;  
      for (i=0;i<4;i++){  
         if (num & 1) modes++;  
             num>>>=1;  
      }  
      return modes;  
  }  
   
  //返回密码的强度级别  
  function checkStrong(sPW, pwdMinLength){  
      if (sPW.length<pwdMinLength)  
          return 0; //密码太短  
      Modes=0;  
      for (i=0;i<sPW.length;i++){  
         //测试每一个字符的类别并统计一共有多少种模式.  
         Modes|=CharMode(sPW.charCodeAt(i));  
      }  
      return bitTotal(Modes);  
  }  

  //当用户放开键盘或密码输入框失去焦点时,根据不同的级别显示不同的颜色  
  function pwStrength(pwd, pwdMinLength){  
      O_color="#eeeeee";  
      L_color="#FF0000";  
      M_color="#FF9900";  
      H_color="#33CC00";  
      if (pwd==null||pwd==''){  
         Lcolor=Mcolor=Hcolor=O_color;  
      }  
      else{  
         S_level=checkStrong(pwd, pwdMinLength);
         switch(S_level) {  
            case 0:  
               Lcolor=Mcolor=Hcolor=O_color;  
            case 1:  
               Lcolor=L_color;  
               Mcolor=Hcolor=O_color;  
               break;  
            case 2:  
               Lcolor=Mcolor=M_color;  
               Hcolor=O_color;  
               break;  
            default:  
               Lcolor=Mcolor=Hcolor=H_color;  
          }  
      }  
      document.getElementById("strength_L").style.background=Lcolor;  
      document.getElementById("strength_M").style.background=Mcolor;  
      document.getElementById("strength_H").style.background=Hcolor;  
      return;  
  }
  
  // 生成n位随机数字
  function randomGenPwd(n){
      var randPwd = "";
      var pwdScope = new Array("0","1","2","3","4","5","6","7","8","9");
      for(i=1;i<=n;i++){
         randPwd = randPwd + pwdScope[Math.floor(Math.random()*10)];
      } 
      return randPwd;
  }
  //add end
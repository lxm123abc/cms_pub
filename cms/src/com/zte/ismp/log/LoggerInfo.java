package com.zte.ismp.log;

public class LoggerInfo
{
    private String loggerName;
    private String loggerLevel;

    public String getLoggerName()
    {
        return loggerName;
    }

    public void setLoggerName(String loggerName)
    {
        this.loggerName = loggerName;
    }

    public String getLoggerLevel()
    {
        return loggerLevel;
    }

    public void setLoggerLevel(String loggerLevel)
    {
        this.loggerLevel = loggerLevel;
    }

}

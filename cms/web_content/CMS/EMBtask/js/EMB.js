function beginTimeLEndTime(beginTime,endTime,input_msg){
  document.getElementById(input_msg).innerText = "";   
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.12"); 
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        return true;               
    }
  }
}

function startSmallestEndBiggest(beginTime,endTime,input_msg){
  document.getElementById(input_msg).innerText = "";   
  if(beginTime.value != '' && beginTime.value != null && endTime.value != '' && endTime.value != null){
    if(beginTime.value > endTime.value){
        document.getElementById(input_msg).innerText = $res_entry("embtask.mgs.starttime.smallest.smaller.than.endtime.biggest"); 
        document.getElementById(beginTime.id).focus();
        return false;
    }else{
        return true;               
    }
  }
}


function getTime(str)
{
	if (str.length > 8)
	{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	}else{
		return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	}
}
	
function savePageStatus(tableID, statusSaveObj)
{
	var ztable = getTableById(tableID);
	var hisStatus = new TableHisStatus(tableID,ztable.condition,ztable.pageIndex, ztable.pagesize);

	var parentObj = getWindowParent();
	if(typeof(statusSaveObj) == "undefined" || statusSaveObj == null)
	{
		parentObj['QueryConObj']=hisStatus;
	}
	else
	{
		parentObj[statusSaveObj]=hisStatus;
	}
}	


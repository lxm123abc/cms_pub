package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.IcmTransfile;
import com.zte.cms.content.dao.IIcmTransfileDAO;
import com.zte.cms.content.service.IIcmTransfileDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmTransfileDS implements IIcmTransfileDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmTransfileDAO dao = null;

    public void setDao(IIcmTransfileDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("insert icmTransfile starting...");
        try
        {
            dao.insertIcmTransfile(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmTransfile end");
    }

    public void updateIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("update icmTransfile by pk starting...");
        try
        {
            dao.updateIcmTransfile(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTransfile by pk end");
    }

    public void updateIcmTransfileList(List<IcmTransfile> icmTransfileList) throws DomainServiceException
    {
        log.debug("update icmTransfileList by pk starting...");
        if (null == icmTransfileList || icmTransfileList.size() == 0)
        {
            log.debug("there is no datas in icmTransfileList");
            return;
        }
        try
        {
            for (IcmTransfile icmTransfile : icmTransfileList)
            {
                dao.updateIcmTransfile(icmTransfile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTransfileList by pk end");
    }

    public void updateIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("update icmTransfile by condition starting...");
        try
        {
            dao.updateIcmTransfileByCond(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTransfile by condition end");
    }

    public void updateIcmTransfileListByCond(List<IcmTransfile> icmTransfileList) throws DomainServiceException
    {
        log.debug("update icmTransfileList by condition starting...");
        if (null == icmTransfileList || icmTransfileList.size() == 0)
        {
            log.debug("there is no datas in icmTransfileList");
            return;
        }
        try
        {
            for (IcmTransfile icmTransfile : icmTransfileList)
            {
                dao.updateIcmTransfileByCond(icmTransfile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTransfileList by condition end");
    }

    public void removeIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("remove icmTransfile by pk starting...");
        try
        {
            dao.deleteIcmTransfile(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTransfile by pk end");
    }

    public void removeIcmTransfileList(List<IcmTransfile> icmTransfileList) throws DomainServiceException
    {
        log.debug("remove icmTransfileList by pk starting...");
        if (null == icmTransfileList || icmTransfileList.size() == 0)
        {
            log.debug("there is no datas in icmTransfileList");
            return;
        }
        try
        {
            for (IcmTransfile icmTransfile : icmTransfileList)
            {
                dao.deleteIcmTransfile(icmTransfile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTransfileList by pk end");
    }

    public void removeIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("remove icmTransfile by condition starting...");
        try
        {
            dao.deleteIcmTransfileByCond(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTransfile by condition end");
    }

    public void removeIcmTransfileListByCond(List<IcmTransfile> icmTransfileList) throws DomainServiceException
    {
        log.debug("remove icmTransfileList by condition starting...");
        if (null == icmTransfileList || icmTransfileList.size() == 0)
        {
            log.debug("there is no datas in icmTransfileList");
            return;
        }
        try
        {
            for (IcmTransfile icmTransfile : icmTransfileList)
            {
                dao.deleteIcmTransfileByCond(icmTransfile);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTransfileList by condition end");
    }

    public IcmTransfile getIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("get icmTransfile by pk starting...");
        IcmTransfile rsObj = null;
        try
        {
            rsObj = dao.getIcmTransfile(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTransfileList by pk end");
        return rsObj;
    }

    public List<IcmTransfile> getIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException
    {
        log.debug("get icmTransfile by condition starting...");
        List<IcmTransfile> rsList = null;
        try
        {
            rsList = dao.getIcmTransfileByCond(icmTransfile);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTransfile by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get icmTransfile page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmTransfile, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTransfile>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmTransfile page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get icmTransfile page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmTransfile, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTransfile>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmTransfile page info by condition end");
        return tableInfo;
    }
}

package com.zte.cms.settlementDataRpt.common;

/**
 * ProjectName: cms-com.zte.cms.settlementDataRpt.common
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 14:58 2019/5/8
 * @Description:
 */
public class SettlementDataRptConstants {

    public static final String SPID_CN = "SP账号";
    public static final String SPID = "spId";

    public static final String SPNAME_CN = "SP公司名称";
    public static final String SPNAME = "spName";

    public static final String SETTLEMENTAMOUNT_CN = "结算金额";
    public static final String SETTLEMENTAMOUNT = "settlementAmount";

    public static final String TAXRATE_CN = "税率";
    public static final String TAXRATE = "taxRate";

    public static final String TAXNOTINCLUDED_CN = "不含税额";
    public static final String TAXNOTINCLUDED = "taxNotIncluded";

    public static final String TAXAMOUNT_CN = "税额";
    public static final String TAXAMOUNT = "taxAmount";

    public static final String PROJECTTYPE_CN = "项目";
    public static final String PROJECTTYPE = "projectType";

    public static final String RPTDT_CN = "账单日期";
    public static final String RPTDT = "rptDt";


    /**
     *  Excel
     */
    public static final String SUFFIX_XLS = ".xls";
    public static final String SUFFIX_XLSX = ".xlsx";

    public static final String EXCEL_PARAM = "param";
    public static final String EXCEL_STARTCOL = "startCol";
    public static final String EXCEL_ENDCOL = "endCol";
    public static final String EXCEL_STARTROW = "startRow";
    public static final String EXCEL_ENDROW = "endRow";
    public static final String EXCEL_ALIGN = "align";
    public static final String EXCEL_FONT_NAME = "宋体";
    public static final String Excel_RPT_FILE_NAME = "(四川移动)结算数据报表";


    public static final String Excel_AUDIT_RECORD = "_审批记录导出.xlsx";
    public static final String Excel_STEP1_CN = "流程环节";
    public static final String Excel_STEP1 = "step1";
    public static final String Excel_STEP2_CN = "审批机构";
    public static final String Excel_STEP2 = "step2";
    public static final String Excel_AUDIT_STATUS_CN = "审批结果";
    public static final String Excel_AUDIT_STATUS = "auditstatus";
    public static final String Excel_AUDIT_DESC_CN = "审批意见";
    public static final String Excel_AUDIT_DESC = "auditdesc";
    public static final String Excel_AUDIT_CREDT_CN = "审批时间";
    public static final String Excel_AUDIT_CREDT = "auditcredt";
}

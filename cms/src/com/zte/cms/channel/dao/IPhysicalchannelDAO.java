package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IPhysicalchannelDAO
{
    /**
     * 新增Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DAOException dao异常
     */
    public void insertPhysicalchannel(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据主键更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DAOException dao异常
     */
    public void updatePhysicalchannel(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据条件更新Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel更新条件
     * @throws DAOException dao异常
     */
    public void updatePhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据主键删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @throws DAOException dao异常
     */
    public void deletePhysicalchannel(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据条件删除Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel删除条件
     * @throws DAOException dao异常
     */
    public void deletePhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据主键查询Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @return 满足条件的Physicalchannel对象
     * @throws DAOException dao异常
     */
    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据条件查询Physicalchannel对象
     * 
     * @param physicalchannel Physicalchannel对象
     * @return 满足条件的Physicalchannel对象集
     * @throws DAOException dao异常
     */
    public List<Physicalchannel> getPhysicalchannelByCond(Physicalchannel physicalchannel) throws DAOException;

    /**
     * 根据条件分页查询Physicalchannel对象，作为查询条件的参数
     * 
     * @param physicalchannel Physicalchannel对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Physicalchannel对象，作为查询条件的参数
     * 
     * @param physicalchannel Physicalchannel对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public List listPhysicalchannelByChannelid(Physicalchannel physicalchannel);
    //查询新增节目单时选择的物理频道
    public PageInfo pageInfoQueryschedule(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException;
}
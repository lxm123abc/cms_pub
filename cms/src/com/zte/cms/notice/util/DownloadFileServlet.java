package com.zte.cms.notice.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;

import com.zte.ismp.systemconfig.ls.IIsysConfigLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class DownloadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Log log = SSBBus.getLog(getClass());
	private static String path = "/home/zxin10/CmsNoticeAttachment/";

	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {
		log.info("Getting information of attachment FTP server...");
		request.setCharacterEncoding("UTF-8");
		//从系统配置文件中获取FTP信息
		String ftpaddress;
		IIsysConfigLS configLs = (IIsysConfigLS)SSBBus.findDomainService("ismpSysConfigLS");
		try {
			ftpaddress = configLs.getUsysConfigByCfgkey("cms.noticeAttachment.ftpaddress");
		} catch (DomainServiceException e1) {
			log.error("Got FTP information failed.");
			e1.printStackTrace();
			return ;
		}
		log.info("Start tp parse the FTP connection information...");
		ftpaddress = ftpaddress.substring(ftpaddress.lastIndexOf("/")+1);
		String[] parm = ftpaddress.split("@");
		String[] userInfo = parm[0].split(":");
		String[] address = parm[1].split(":");
		String user = userInfo[0];
		String password = userInfo[1];
		String host = address[0];
		int port = Integer.parseInt(address[1].trim());
		log.info("Paring FTP connection information failed!");

		//获取目标文件名
		String targetFilename = request.getParameter("newname");
		String filename = request.getParameter("filename");
        String encode = request.getParameter("encode");
        if (StringUtils.isEmpty(encode)){
            filename = new String(filename.getBytes("iso8859-1"),"utf-8");
        }else {
            filename = new String(filename.getBytes("iso8859-1"),"GBK");
        }

		if(targetFilename==null) {
			log.error("Didn't get the of the target file!");
			return ;
		}

		FTPClient ftpConn = new FTPClient();
		InputStream in = null;
		OutputStream out = null;
		try {
			log.info("Connecting to FTP server...");
			//连接FTP并判断是否连接成功
			ftpConn.connect(host, port);
			ftpConn.login(user, password);
			int reply = ftpConn.getReplyCode();
			if(!FTPReply.isPositiveCompletion(reply)) {
				ftpConn.logout();
				ftpConn.disconnect();
				throw new Exception("FTP connect refused");
			}
			//下面三行代码是为了保证正确下载中文文件
			ftpConn.setControlEncoding("GBK");
			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
			conf.setServerLanguageCode("zh");
			//转移到目标文件目录下
			ftpConn.changeWorkingDirectory(path);
			ftpConn.setFileType(FTPClient.BINARY_FILE_TYPE);
			log.info("Connected success!Downloading the file");
			//读取文件
			response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(filename, "UTF-8"));
			in = ftpConn.retrieveFileStream(targetFilename);
			out = response.getOutputStream();
			int i = 0;// 得到文件大小
			byte data[] = new byte[100000];
			while ((i = in.read(data)) != -1)
			{
				out.write(data, 0, i);
			}
			out.flush();
		} catch (SocketException e) {
			log.error("Attachment FTP connect failed");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Attachment FTP's host/ip error");
			e.printStackTrace();
		} catch (Exception e) {
			log.error("Connected to FTP server failed.");
			e.printStackTrace();
		} finally {
			if(in != null) {
				in.close();
			}
			if(out != null) {
				out.close();
			}
			if(ftpConn.isConnected()) {
				ftpConn.logout();
				ftpConn.disconnect();
			}
		}
	}
}
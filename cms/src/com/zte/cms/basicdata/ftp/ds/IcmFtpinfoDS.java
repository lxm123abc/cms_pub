package com.zte.cms.basicdata.ftp.ds;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;

import com.zte.cms.basicdata.ftp.dao.IIcmFtpinfoDAO;
import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

/**
 * 内容摘要: 主要完成ftp的增、删、改、查功能
 */
public class IcmFtpinfoDS extends DynamicObjectBaseDS implements IIcmFtpinfoDS
{
    private IIcmFtpinfoDAO dao = null;

    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void setDao(IIcmFtpinfoDAO dao)
    {
        this.dao = dao;
    }

    public void deleteIcmFtpinfo(IcmFtpinfo icmftpino) throws DomainServiceException
    {
        log.debug("remove icmContentBasicinfo by pk starting...");
        try
        {
            this.dao.deleteIcmFtpinfo(icmftpino);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmContentBasicinfo by pk end");

    }

    public IcmFtpinfo getIcmFtpinfo(Long infoindex) throws DomainServiceException
    {
        log.debug(" getIcmFtpinfo  start");
        IcmFtpinfo ucmServer = null;
        try
        {

            ucmServer = (IcmFtpinfo) this.dao.getIcmFtpinfoByinfoindex(infoindex);
        }
        catch (Exception e)
        {
            log.error("Error occurred in getIcmFtpinfo method", e);
            throw new DomainServiceException(e);
        }
        log.debug("getIcmFtpinfo  end");
        return ucmServer;
    }

    public String insertIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {

        log.debug("insert icmFtpinfo starting...");
        Long infoindex = null;
        try
        {
            infoindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_ftpinfo");
            icmFtpinfo.setInfoindex(infoindex);
            dao.insertIcmFtpinfo(icmFtpinfo);
            log.debug("insert icmFtpinfo end");

        }
        catch (DAOException daoEx)
        {

            log.error("dao exception:", daoEx);
            throw new DomainServiceException(daoEx);
        }
        return infoindex.toString();

    }

    public void updateIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {
        log.debug("update icmFtpinfo by pk starting...");
        try
        {
            dao.updateIcmFtpinfo(icmFtpinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmFtpinfo by pk end");
    }

    public void updateIcmFtpinfoList(List<IcmFtpinfo> icmFtpinfoList) throws DomainServiceException
    {
        log.debug("update icmFtpinfoList by pk starting...");
        if (null == icmFtpinfoList || icmFtpinfoList.size() == 0)
        {
            log.debug("there is no datas in icmFtpinfoList");
            return;
        }
        try
        {
            for (IcmFtpinfo icmFtpinfo : icmFtpinfoList)
            {
                dao.updateIcmFtpinfo(icmFtpinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmFtpinfoList by pk end");
    }

    public void updateIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {
        log.debug("update icmFtpinfo by condition starting...");
        try
        {
            dao.updateIcmFtpinfoByCond(icmFtpinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmFtpinfo by condition end");
    }

    public void updateIcmFtpinfoListByCond(List<IcmFtpinfo> icmFtpinfoList) throws DomainServiceException
    {
        log.debug("update icmFtpinfoList by condition starting...");
        if (null == icmFtpinfoList || icmFtpinfoList.size() == 0)
        {
            log.debug("there is no datas in icmFtpinfoList");
            return;
        }
        try
        {
            for (IcmFtpinfo icmFtpinfo : icmFtpinfoList)
            {
                dao.updateIcmFtpinfoByCond(icmFtpinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmFtpinfoList by condition end");
    }

    public void removeIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {
        log.debug("remove icmFtpinfo by pk starting...");
        try
        {
            dao.deleteIcmFtpinfo(icmFtpinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmFtpinfo by pk end");
    }

    public void removeIcmFtpinfoList(List<IcmFtpinfo> icmFtpinfoList) throws DomainServiceException
    {
        log.debug("remove icmFtpinfoList by pk starting...");
        if (null == icmFtpinfoList || icmFtpinfoList.size() == 0)
        {
            log.debug("there is no datas in icmFtpinfoList");
            return;
        }
        try
        {
            for (IcmFtpinfo icmFtpinfo : icmFtpinfoList)
            {
                dao.deleteIcmFtpinfo(icmFtpinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmFtpinfoList by pk end");
    }

    public void removeIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {
        log.debug("remove icmFtpinfo by condition starting...");
        try
        {
            dao.deleteIcmFtpinfoByCond(icmFtpinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmFtpinfo by condition end");
    }

    public void removeIcmFtpinfoListByCond(List<IcmFtpinfo> icmFtpinfoList) throws DomainServiceException
    {
        log.debug("remove icmFtpinfoList by condition starting...");
        if (null == icmFtpinfoList || icmFtpinfoList.size() == 0)
        {
            log.debug("there is no datas in icmFtpinfoList");
            return;
        }
        try
        {
            for (IcmFtpinfo icmFtpinfo : icmFtpinfoList)
            {
                dao.deleteIcmFtpinfoByCond(icmFtpinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmFtpinfoList by condition end");
    }

    public List<IcmFtpinfo> getIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DomainServiceException
    {
        log.debug("get icmFtpinfo by condition starting...");
        List<IcmFtpinfo> rsList = null;
        try
        {
            rsList = dao.getIcmFtpinfoByCond(icmFtpinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmFtpinfo by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get icmFtpinfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmFtpinfo, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmFtpinfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmFtpinfo page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get icmFtpinfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmFtpinfo, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmFtpinfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmFtpinfo page info by condition end");
        return tableInfo;
    }

    public String checkFTPInfo(CmsStoragemanage cmsstrmgt, String fatherPath) throws Exception
    {
        log.debug("checkFTPInfo starting...");
        long ftpUnusedTotalCount = 0;
        long allHasUsedSize = 0;
        try
        {
            String realPath = "";
            String details = "";
            // 获取需要查询大小的目录路径
            realPath = fatherPath + cmsstrmgt.getStorageid();
            realPath = filterSlashStr(realPath);
            // 调用shell查询目录大小
            details = exeCmd("du -ms " + realPath);
            log.debug("#########################" + "details = cmsFtpinfoDS.checkFTPInfo(cmsstrmgt, fatherPath):"
                    + details + "######################");
            if (details == null)
            {
                return null;
            }
            // 获取目录大小的数值，单位kb
            details = details.substring(0, findfirstspace(details));
            allHasUsedSize = Long.parseLong(details);
            log.debug("#########################" + "allHasUsedSize:" + allHasUsedSize + "######################");
            IcmFtpinfo icmFtpinfo = new IcmFtpinfo();
            icmFtpinfo.setSpaceindex(cmsstrmgt.getStorageindex());
            List<IcmFtpinfo> ftpInfoList = this.getIcmFtpinfoByCond(icmFtpinfo);
            String currentCapacity = "";
            String ftpFullPathName = "";
            File tempFile = null;
            if (ftpInfoList != null && ftpInfoList.size() > 0)
            {
                for (IcmFtpinfo ftpInfoInList : ftpInfoList)
                {
                    ftpFullPathName = realPath + "/" + getFtpFolderName(ftpInfoInList.getFtppath());
                    log
                            .debug("######################### ftpFullPathName:" + ftpFullPathName
                                    + "######################");
                    tempFile = new File(ftpFullPathName);
                    if (!tempFile.exists())
                    {
                        log
                                .debug("######################### ftp path does not exist and then make dir ######################");
                        tempFile.mkdirs();
                    }
                    details = exeCmd("du -ms " + ftpFullPathName);
                    if (details == null)
                    {
                        return null;
                    }
                    details = details.substring(0, findfirstspace(details));
                    log.debug("#########################  details: " + details + " ######################");
                    currentCapacity = String.valueOf(ftpInfoInList.getSpacesize() - Long.parseLong(details));
                    log.debug("######################### currentCapacity: " + currentCapacity
                            + " ######################");
                    // log.debug("currentCapacity:"+currentCapacity);
                    // 如果使用空间大于等于FTP分配空间，FTP状态更改为不可用
                    ftpInfoInList.setCurrentcapacity(currentCapacity);
                    if (Long.parseLong(details) >= ftpInfoInList.getSpacesize())
                    {
                        // ftpInfoInList.setCurrentcapacity("0");
                        ftpInfoInList.setStatus(0);
                    }
                    // 如果使用空间小于FTP分配空间，则FTP状态更改为可用
                    else
                    {
                        ftpInfoInList.setStatus(1);
                    }

                    this.updateIcmFtpinfo(ftpInfoInList);
                    // 如果已使用空间大于FTP可分配空间，则表示当前FTP空间存储已经溢出，否则需要把FTP剩余的未使用的空间到临时区已使用空
                    if ((ftpInfoInList.getSpacesize() - Long.parseLong(details)) > 0)
                    {
                        ftpUnusedTotalCount = ftpUnusedTotalCount
                                + (ftpInfoInList.getSpacesize() - Long.parseLong(details));
                    }
                }
            }

        }
        catch (Exception e)
        {
            log.error("checkFTPInfo exception:" + e);
            return null;
        }
        log.debug("#########################  ftpUnusedTotalCount " + ftpUnusedTotalCount + " ######################");
        long totalHasAllocateSize = allHasUsedSize + ftpUnusedTotalCount;
        log
                .debug("#########################  totalHasAllocateSize " + totalHasAllocateSize
                        + " ######################");
        log.debug("checkFTPInfo end");
        return String.valueOf(totalHasAllocateSize);
    }

    private String getFtpFolderName(String ftpPath)
    {
        int lastSlashIndex = ftpPath.lastIndexOf('/');
        String folderName = ftpPath.substring(lastSlashIndex + 1);
        return folderName;
    }

    private int findfirstspace(String str)
    {
        int firstspace = -1;
        int index = 0;
        int totallength = 0;
        if (!str.trim().equals(""))
        {
            totallength = str.length();
            while (totallength > 0)
            {

                if (" ".equals(str.substring(index, index + 1)) || "\t".equals(str.substring(index, index + 1)))
                {
                    firstspace = index;
                    break;
                }
                index++;
                totallength--;
            }
        }
        return firstspace;
    }

    private String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 执行shell
     * 
     * @param command String
     * @return String
     * @throws Exception Exception
     */
    private String exeCmd(String command) throws Exception
    {
        String retCmdInfo = null;
        Process process = null;
        try
        {

            Runtime runtime = Runtime.getRuntime();

            process = runtime.exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            String test = in.toString();
            int index = 0;

            while ((line = in.readLine()) != null)
            {

                if (retCmdInfo == null)
                {
                    retCmdInfo = line;
                }
                else
                {
                    retCmdInfo += ("\r\n" + line);
                }

            }
            process.destroy();

        }
        catch (Exception e)
        {
            if (process != null)
            {
                process.destroy();
            }
            throw e;
        }
        return retCmdInfo;
    }

}

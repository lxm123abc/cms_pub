
var loginTimer = "";
var loginObject = {}; // 登录对象

function doLogin() {
	if (!isEmpty(loginTimer)) clearInterval(loginTimer); loginTimer = "";
	var userinfo = {};
	userinfo.userId = $("#loginBody").find("input[id='txtUserid']").val();
	userinfo.password = $("#loginBody").find("input[id='txtPassword']").val();
	loginObject = {};
	callUrl('$/ssb/uiloader/loginMgt/login.ssm', userinfo, 'loginObject', true);
	if (isEmpty(loginObject) || isEmpty(loginObject.rtnValue)) {
		$("#divlogin").find("label[id='lblUserid']").text("未登录");
		return;
	}
	var rtnCode = loginObject.rtnValue;
	if (rtnCode != "0" && rtnCode != "0000" && rtnCode != "9999") {
		$("#divlogin").find("label[id='lblUserid']").text("登录失败:" + rtnCode);
		return;
	}
	$("#divlogin").find("label[id='lblUserid']").text(userinfo.userId);
	loginTimer = setInterval("getUserInfo()", 5 * 60 * 1000);
}

function getUserInfo() {
	loginObject = {};
	callUrl('$/ssb/uiloader/loginMgt/getUserInfo.ssm', null, 'loginObject', true);
	if (!isEmpty(loginObject) && !isEmpty(loginObject.rtnValue) && !isEmpty(loginObject.rtnValue.userId)) {
		$("#divlogin").find("label[id='lblUserid']").text(loginObject.rtnValue.userId);
		if (isEmpty(loginTimer)) loginTimer = setInterval("getUserInfo()", 5 * 60 * 1000);
		return;
	}
	if (!isEmpty(loginTimer)) clearInterval(loginTimer); loginTimer = "";
	$("#divlogin").find("label[id='lblUserid']").text("未登录");
}
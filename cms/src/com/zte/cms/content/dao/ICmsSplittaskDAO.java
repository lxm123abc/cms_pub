package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.CmsSplittask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsSplittaskDAO
{
    /**
     * 新增CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DAOException dao异常
     */
    public void insertCmsSplittask(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据主键更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DAOException dao异常
     */
    public void updateCmsSplittask(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据条件更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据主键删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DAOException dao异常
     */
    public void deleteCmsSplittask(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据条件删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据主键查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @return 满足条件的CmsSplittask对象
     * @throws DAOException dao异常
     */
    public CmsSplittask getCmsSplittask(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据条件查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @return 满足条件的CmsSplittask对象集
     * @throws DAOException dao异常
     */
    public List<CmsSplittask> getCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DAOException;

    /**
     * 根据条件分页查询CmsSplittask对象，作为查询条件的参数
     * 
     * @param cmsSplittask CmsSplittask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSplittask对象，作为查询条件的参数
     * 
     * @param cmsSplittask CmsSplittask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询CmsSplittask对象，作为查询条件的参数
     * 
     * @param cmsSplittask CmsSplittask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSplittask对象，作为查询条件的参数
     * 
     * @param cmsSplittask CmsSplittask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.IProgramPictureMapDAO;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ProgramPictureMapDAO extends DynamicObjectBaseDao implements IProgramPictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("insert programPictureMap starting...");
        super.insert("insertProgramPictureMap", programPictureMap);
        log.debug("insert programPictureMap end");
    }

    public void updateProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("update programPictureMap by pk starting...");
        super.update("updateProgramPictureMap", programPictureMap);
        log.debug("update programPictureMap by pk end");
    }

    public void deleteProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("delete programPictureMap by pk starting...");
        super.delete("deleteProgramPictureMap", programPictureMap);
        log.debug("delete programPictureMap by pk end");
    }

    public ProgramPictureMap getProgramPictureMap(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("query programPictureMap starting...");
        ProgramPictureMap resultObj = (ProgramPictureMap) super.queryForObject("getProgramPictureMap",
                programPictureMap);
        log.debug("query programPictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ProgramPictureMap> getProgramPictureMapByCond(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("query programPictureMap by condition starting...");
        List<ProgramPictureMap> rList = (List<ProgramPictureMap>) super.queryForList(
                "queryProgramPictureMapListByCond", programPictureMap);
        log.debug("query programPictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryProgramPictureMapListCntByCond", programPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramPictureMap> rsList = (List<ProgramPictureMap>) super.pageQuery(
                    "queryProgramPictureMapListByCond", programPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programPictureMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryProgramPictureMapListByCond", "queryProgramPictureMapListCntByCond",
                programPictureMap, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryProPictureTargetMapSynListCntByCond", programPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramPictureMap> rsList = (List<ProgramPictureMap>) super.pageQuery(
                    "queryProPictureTargetMapSynListByCond", programPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programPictureMap by condition end");
        return pageInfo;
    }
    
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query programPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryProgramPictureMapSynListCntByCond", programPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ProgramPictureMap> rsList = (List<ProgramPictureMap>) super.pageQuery(
                    "queryProgramPictureMapSynListByCond", programPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query programPictureMap by condition end");
        return pageInfo;
    }    
  
    
    @SuppressWarnings("unchecked")
    public List<ProgramPictureMap> getProgramnormalPictureMapByCond(ProgramPictureMap programPictureMap) throws DAOException
    {
        log.debug("query programPictureMap by condition starting...");
        List<ProgramPictureMap> rList = (List<ProgramPictureMap>) super.queryForList(
                "getProPictureTargetMapSynListByCond", programPictureMap);
        log.debug("query programPictureMap by condition end");
        return rList;
    }
}
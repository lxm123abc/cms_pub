package com.zte.ismp.common.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>
 * 文件名称: StrUtil.java
 * </p>
 * <p>
 * 文件描述: 字符串操作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 字符串操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class StrUtil
{

    // ****** 代码段: 常量定义 *******************************************************************************/
    // 过滤Html脚本中需要转义的字符
    private static String[][] FilterHtmlChars = { { "<", "&lt;" }, { ">", "&gt;" }, { " ", "&nbsp;" },
            { "\'", "&#39;" }, { "\"", "&quot;" }, { "&", "&amp;" }, { "/", "&#47;" }, { "\\", "&#92;" },
            { "\n", "<br>" } };

    // 过滤JS脚本中需要转义的字符
    private static String[][] FilterJavaScriptChars = { { "\'", "\\\'" }, { "\\", "\\\\" } };

    // 过滤数据库脚本中需要转义的字符
    private static String[][] FilterDBScriptChars = { { "\\", "\\\\" }, { "\'", "\'\'" }, { "_", "\\_" },
            { "%", "\\%" }, { "'", "''" } };

    // ****** 代码段: 变量定义 *******************************************************************************/

    // ****** 代码段: 公共方法 *******************************************************************************/

    /**
     * 将不同长度的数字字符串转换成2位长度的数字字符串
     * 
     * @param str String
     * @param len int
     * @return String
     */
    public static String toStringFormat(String str, int len)
    {

        if (str == null)
        {
            return "";
        }
        if (str.length() > len)
        {
            return str;
        }

        String tmpStr = str;
        while (tmpStr.length() < len)
        {
            tmpStr = '0' + tmpStr;
        }
        return tmpStr;
    }

    /**
     * 根据实际情况增加
     * 
     * @param str String 原来的字符串
     * @param len int 增加后的长度
     * @param direction int 方向 0:左侧增加 其他：右侧增加
     * @param c char 用来扩充的字符
     * @return String
     */
    public static String toStringFormat(String str, int len, int direction, char c)
    {

        if (str == null)
        {
            return "";
        }
        if (str.length() > len)
        {
            return str;
        }

        String tmpStr = str;
        if (direction == 0)
        {
            while (tmpStr.length() < len)
            {
                tmpStr = c + tmpStr;
            }
        }
        else
        {
            while (tmpStr.length() < len)
            {
                tmpStr = tmpStr + c;
            }
        }
        return tmpStr;
    }

    /**
     * 将对象转换成制定pattern的金额格式 如pattern为"0.00",则123会转换成123.00
     * 
     * @param o Object
     * @param pattern String
     * @return String
     */
    public static String toMoneyStringFormat(Object o, String pattern)
    {

        DecimalFormat formatter = new DecimalFormat(pattern);
        return formatter.format(o);
    }

    /**
     * 将对象转换成制定的pattern的时间格式 如pattern为"yyyy-MM-dd HH:mm:ss"
     * 
     * @param o Object
     * @param pattern String
     * @return String
     */
    public static String toTimeStringFormat(Object o, String pattern)
    {

        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(o);
    }

    /**
     * 从数据库中读出数据时，去掉null将其转换成""
     * 
     * @param oldStr String
     * @return String
     */
    public static String outStr(String oldStr)
    {

        String newStr = null;
        if (oldStr == null)
        {
            newStr = "";
        }
        else
        {
            newStr = oldStr;
        }
        return newStr;
    }

    /**
     * 删除数组中的空
     * 
     * @param oldArray String[]
     * @return String[]
     */
    public static String[] delArrayBlack(String[] oldArray)
    {

        Vector v = new Vector();
        if (oldArray == null || oldArray.length == 0)
        {
            return oldArray;
        }
        int len = oldArray.length;
        for (int i = 0; i < len; i++)
        {
            if (!oldArray[i].equals(""))
            {
                v.add(oldArray[i]);
            }
        }

        String[] newArray = new String[v.size()];
        for (int i = 0; i < v.size(); i++)
        {
            newArray[i] = (String) v.elementAt(i);
        }
        return newArray;
    }

    /**
     * 比较（check方式）
     * 
     * @param a String
     * @param b String
     * @return String
     */
    public static String checked(String a, String b)
    {

        if (a.equals(b))
        {
            return "checked";
        }
        else
        {
            return "";
        }
    }

    /**
     * 比较（check方式）
     * 
     * @param a boolean
     * @return String
     */
    public static String checked(boolean a)
    {

        if (a)
        {
            return "checked";
        }
        else
        {
            return "";
        }
    }

    /**
     * 比较（select方式）
     * 
     * @param a String
     * @param b String
     * @return String
     */
    public static String selected(String a, String b)
    {

        if (a.equals(b))
        {
            return "selected";
        }
        else
        {
            return "";
        }
    }

    /**
     * 对页面输入的字符串进行校验
     * 
     * @param a String
     * @return String
     */
    public static String trans(String a)
    {

        if (a == null)
        {
            return "";
        }
        else
        {
            String b = a.trim();
            b = StrUtil.strFilterDBScriptChar(b);
            return b;
        }
    }

    /**
     * 对数据库中取出的数据进行编码处理，以便于在WEB页面显示
     * 
     * @param intext String
     * @return String
     */
    public static String encode(String intext)
    {

        if (intext == null)
        {
            return "";
        }
        String newStr = intext.trim();
        // newStr = newStr.replaceAll("&", "&amp;");
        // newStr = newStr.replaceAll("\"", "&quot;");
        // newStr = newStr.replaceAll("<", "&lt;");
        // newStr = newStr.replaceAll(">", "&gt;");
        // newStr = newStr.replaceAll(" ", "&nbsp;"); //会使数据库产生乱码
        return newStr;
    }

    // ****** 代码段: 处理非法字符代码段 **************************************************************/
    /**
     * 过滤JS脚本中的特殊字符
     * 
     * @param str 要过滤的字符串
     * @return 过滤后的字符串
     */
    public static String strFilterJavaScriptChar(String str)
    {

        if (str == null)
        {
            return "";
        }

        // 先替换转移字符
        String newStr = str.trim();
        // newStr = newStr.replaceAll("&amp;", "&");
        // newStr = newStr.replaceAll("&quot;", "\"");
        // newStr = newStr.replaceAll("&lt;", "<");
        // newStr = newStr.replaceAll("&gt;", ">");
        // newStr = newStr.replaceAll("<br>", "\n");
        // newStr = newStr.replaceAll("&nbsp;", " ");

        // 再对'和/作处理,对于/，如果用replaceAll，则要使用newStr = newStr.replaceAll("\\\\", "\\\\\\\\");
        String[] str_arr = strSpilit(newStr, "");
        for (int i = 0; i < str_arr.length; i++)
        {
            for (int j = 0; j < FilterJavaScriptChars.length; j++)
            {
                if (FilterJavaScriptChars[j][0].equals(str_arr[i]))
                {
                    str_arr[i] = FilterJavaScriptChars[j][1];
                }
            }
        }
        return (strConnect(str_arr, "")).trim();
    }

    /**
     * 过滤数据库脚本中的特殊字符（包括回车符(\n)和换行符(\r)）
     * 
     * @param str 要进行过滤的字符串
     * @return 过滤后的字符串
     */
    public static String strFilterDBScriptChar(String str)
    {

        if (str == null)
        {
            return "";
        }
        String[] str_arr = strSpilit(str, "");
        for (int i = 0; i < str_arr.length; i++)
        {
            for (int j = 0; j < FilterDBScriptChars.length; j++)
            {
                if (FilterDBScriptChars[j][0].equals(str_arr[i]))
                {
                    str_arr[i] = FilterDBScriptChars[j][1];
                }
            }
        }
        return (strConnect(str_arr, "")).trim();
    }

    /**
     * 转义数据中的字符成html实体字符
     * 
     * @param str 要进行过滤的字符串
     * @return 过滤后的字符串
     */
    public static String strFilterHtmlChar(String str)
    {

        if (str == null)
        {
            return "";
        }
        String[] str_arr = strSpilit(str, "");
        for (int i = 0; i < str_arr.length; i++)
        {
            for (int j = 0; j < FilterHtmlChars.length; j++)
            {
                if (FilterHtmlChars[j][0].equals(str_arr[i]))
                {
                    str_arr[i] = FilterHtmlChars[j][1];
                }
            }
        }
        return (strConnect(str_arr, "")).trim();
    }

    /**
     * 分割字符串
     * 
     * @param str 要分割的字符串
     * @param spilit_sign 字符串的分割标志
     * @return 分割后得到的字符串数组
     */
    public static String[] strSpilit(String str, String spilit_sign)
    {

        String[] spilit_string = str.split(spilit_sign);
        if (spilit_string[0].equals(""))
        {
            String[] new_string = new String[spilit_string.length - 1];
            for (int i = 1; i < spilit_string.length; i++)
            {
                new_string[i - 1] = spilit_string[i];
            }
            return new_string;
        }
        else
        {
            return spilit_string;
        }
    }

    /**
     * 用特殊的字符连接字符串
     * 
     * @param strings 要连接的字符串数组
     * @param spilit_sign 连接字符
     * @return 连接字符串
     */
    public static String strConnect(String[] strings, String spilit_sign)
    {

        StringBuffer strbuff = new StringBuffer();
        for (int i = 0; i < strings.length; i++)
        {
            strbuff.append(strings[i]);
            strbuff.append(spilit_sign);
        }
        return strbuff.toString();
    }

    /**
     * 替换字符串
     * 
     * @param s String
     * @param strb String
     * @param strh String
     * @return String
     */
    public static final String replaceString(String s, String strb, String strh)
    {

        if (s == null || s.length() == 0)
        {
            return s;
        }
        StringBuffer tmp = new StringBuffer();
        int k;
        while ((k = s.indexOf(strb)) >= 0)
        {
            tmp.append(s.substring(0, k));
            tmp.append(strh);
            s = s.substring(k + strb.length());
        }
        if (s.length() > 0)
        {
            tmp.append(s);
        }
        return tmp.toString();
    }

    public static final String getString(String strName)
    {

        return getString(strName, ((String) (null)));
    }

    public static final String getString(String strName, String def)
    {

        if (strName == null)
        {
            return def;
        }
        else
        {
            return strName;
        }
    }

    public static final int getInt(String strName)
    {

        return getInt(strName, 0);
    }

    public static final int getInt(String strName, int defaultvalue)
    {

        if (strName == null)
        {
            return defaultvalue;
        }
        try
        {
            return Integer.parseInt(strName.trim());
        }
        catch (Exception e)
        {
            return defaultvalue;
        }
    }

    public static final long getLong(String strName) throws Exception, NumberFormatException
    {

        if (strName == null)
        {
            throw new Exception("getLong(String strName):Input value is NULL!");
        }
        try
        {
            return Long.parseLong(strName.trim());
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException("getLong NumberFormatException for input string:" + strName);
        }
    }

    public static final float getFloat(String strName) throws Exception, NumberFormatException
    {

        if (strName == null)
        {
            throw new Exception("Input value is NULL!");
        }
        try
        {
            return Float.parseFloat(strName.trim());
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException("getFloat(String) NumberFormatException for input string:" + strName);
        }
    }

    public static final float getFloat(HttpServletRequest request, String strName) throws Exception,
            NumberFormatException
    {

        String val = request.getParameter(strName);
        if (val == null)
        {
            return 0.0F;
        }
        try
        {
            return Float.parseFloat(val.trim());
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException("getFloat(request) NumberFormatException for input string:" + val);
        }
    }

    public static final float getFloat(HttpServletRequest request, String strName, float def) throws Exception,
            NumberFormatException
    {

        String val = request.getParameter(strName);
        if (val == null)
        {
            System.out.println("val is null,so return default!");
            return def;
        }
        try
        {
            return Float.parseFloat(val.trim());
        }
        catch (NumberFormatException e)
        {
            System.out
                    .println("getFloat(HttpServletRequest request,String strName,float def) NumberFormatException for input string:"
                            + val + ",so return default!");
        }
        return def;
    }

    public static final double getDouble(String strName) throws Exception, NumberFormatException
    {

        if (strName == null)
        {
            throw new Exception("Input value is NULL!");
        }
        try
        {
            return Double.parseDouble(strName.trim());
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException("getFloat(String) NumberFormatException for input string:" + strName);
        }
    }

    public static final long getLong(String strName, long defaultvalue)
    {

        if (strName == null)
        {
            return defaultvalue;
        }
        try
        {
            return Long.parseLong(strName);
        }
        catch (Exception e)
        {
            return defaultvalue;
        }
    }

    public static final String getString(String s, int length)
    {

        if (s == null)
        {
            return null;
        }
        if (getNumofByte(s) <= length)
        {
            return s;
        }
        byte bb[] = s.getBytes();
        byte temp[] = new byte[length];
        for (int i = 0; i < length; i++)
        {
            temp[i] = bb[i];
        }

        return getString(temp);
    }

    private static int getNumofByte(String s)
    {

        if (s == null)
        {
            return 0;
        }
        else
        {
            return s.getBytes().length;
        }
    }

    private static final String getString(byte bb[])
    {

        String s = new String(bb);
        if (s.length() == 0)
        {
            int length = bb.length;
            if (length > 1)
            {
                byte temp[] = new byte[length - 1];
                for (int i = 0; i < length - 1; i++)
                {
                    temp[i] = bb[i];
                }

                return getString(temp);
            }
            else
            {
                return "";
            }
        }
        else
        {
            return s;
        }
    }

    public static final String getMD5(String s)
    {

        if (s == null)
        {
            return null;
        }
        if (s.trim().length() == 0)
        {
            return s;
        }
        else
        {
            MD5 md5 = new MD5();
            return md5.getMD5ofStr(s);
        }
    }

    public static final int getInt(HttpServletRequest request, String ParamName)
    {

        return getInt(request, ParamName, 0);
    }

    public static final int getInt(HttpServletRequest request, String ParamName, int defV)
    {

        if (request.getParameter(ParamName) != null)
        {
            try
            {
                int t = Integer.parseInt(request.getParameter(ParamName));
                return t;
            }
            catch (NumberFormatException e)
            {
                return 0;
            }
        }
        else
        {
            return defV;
        }
    }

    public static final long getLong(HttpServletRequest request, String ParamName)
    {

        if (request.getParameter(ParamName) != null)
        {
            try
            {
                long t = Long.parseLong(request.getParameter(ParamName).trim());
                return t;
            }
            catch (NumberFormatException e)
            {
                return 0L;
            }
        }
        else
        {
            return 0L;
        }
    }

    public static final long getLong(HttpServletRequest request, String ParamName, long def)
    {

        if (request.getParameter(ParamName) != null)
        {
            try
            {
                long t = Long.parseLong(request.getParameter(ParamName));
                return t;
            }
            catch (NumberFormatException e)
            {
                return 0L;
            }
        }
        else
        {
            return def;
        }
    }

    public static final String getString(HttpServletRequest request, String paramName)
    {

        if (paramName == null)
        {
            return null;
        }
        String t = request.getParameter(paramName);
        if (t != null && !t.equals(""))
        {
            return t.trim();
        }
        else
        {
            return "";
        }
    }

    public static final String getString(HttpServletRequest request, String paramName, String defaultVal)
    {

        String t = request.getParameter(paramName);
        if (t != null && !t.equals(""))
        {
            return t.trim();
        }
        else
        {
            return defaultVal;
        }
    }

    public static final HttpSession getSession(HttpServletRequest request)
    {

        return request.getSession(true);
    }

    public static final String cutString(String str, int len, String tail)
    {

        int slen = str.length();
        if (slen > len)
        {
            str = str.substring(0, len) + tail;
        }
        return str;
    }

    public static String formatNumber(String formatstr, double number)
    {

        DecimalFormat df = new DecimalFormat(formatstr);
        return df.format(number);
    }

    public static String formatNumber(String formatstr, long number)
    {

        DecimalFormat df = new DecimalFormat(formatstr);
        return df.format(number);
    }

    public static boolean isValidTime(String strTime)
    {

        SimpleDateFormat localTime = new SimpleDateFormat("HH:mm:ss");
        try
        {
            Date date1 = localTime.parse(strTime);
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
    }

    public static String getFileFormat(String filename)
    {

        // 验证文件名格式合法性
        int ext = filename.lastIndexOf(".") == -1 ? filename.length() : filename.lastIndexOf(".");
        // 默认文件名(内容编号.扩展名)
        return filename.substring(ext);
    }

    /**
     * 将null转换为“”
     * 
     * @param o Object
     * @return
     */
    public static String trans(Object o)
    {
        if (o == null)
        {
            return "";
        }
        else
        {
            return o.toString().trim();
        }
    }

    /*
     * 对字符串中出现的$进行转义，在使用String replaceAll(regex, replacement)等方法时需要进行$字符转义 @param str 要过滤的字符串 @return 过滤后的字符串
     */
    public static String filterDollarStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf('$', 0) > -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf('$', 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf('$', 0));
                        rtnStr += "\\$";
                        str = str.substring(str.indexOf('$', 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }
    
    /**
     * 计算含汉字的字符串的长度
     * @param str 待计算字符串
     * @param language 判断依据语言标准 "en"-一个汉字占3个字符   其他-一个汉字占2个字符
     * @return 字符串的长度
     */
    public static int exlen(String str, String language)
    {
        int temp = 0;
        if ("en".equals(language))
        {
            for (int i = 0; i < str.length(); i++)
            {
                int value = str.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 3;
                }
                else
                {
                    temp += 4;
                }
            }
        }
        else
        {
            for (int i = 0; i < str.length(); i++)
            {
                int value = str.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 2;
                }
                else
                {
                    temp += 4;
                }
            }
        }

        return temp;
    }
    
    public static void main(String[] args)
    {
        String test = "中国hao 声音";
        System.out.println(test + "  " + exlen(test,"en"));
    }
}

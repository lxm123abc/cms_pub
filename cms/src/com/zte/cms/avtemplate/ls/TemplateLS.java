package com.zte.cms.avtemplate.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.EMBtask.service.ICmsEmbtaskDS;
import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.cms.avtemplate.service.ICmsAvtemplateinfoDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.model.IcmTransfile;
import com.zte.cms.content.service.IIcmTransfileDS;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;
import com.zte.umap.ucms.model.UcmFile;
import com.zte.umap.ucms.service.IUcmFileDS;

/**
 * @author Administrator 模板管理,查询
 */
public class TemplateLS implements ITemplateLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public ICmsEmbtaskDS cmsEmbtaskDS;
    public IUcmFileDS ucmFileDS;

    private ICmsAvtemplateinfoDS templateDS = null;
    private IIcmTransfileDS icmTransfileDS = null;
    private DbUtil dbUtil = new DbUtil();

    public void setIcmTransfileDS(IIcmTransfileDS icmTransfileDS)
    {
        this.icmTransfileDS = icmTransfileDS;
    }

    public ICmsAvtemplateinfoDS getTemplateDS()
    {
        return templateDS;
    }

    public void setTemplateDS(ICmsAvtemplateinfoDS templateDS)
    {
        this.templateDS = templateDS;
    }

    public CmsAvtemplateinfo getTemplate(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        cmsAvtemplateinfo = templateDS.getCmsAvtemplateinfo(cmsAvtemplateinfo);
        return cmsAvtemplateinfo;
    }

    /**
     * 通过帧宽帧高计算grouptype,即标清，高清，低码流
     * 
     * @param width
     * @param hight
     * @return
     */
    public int caculateGrouptype(int width, int hight)
    {
        int type = -1;
        if (width >= 1280 || hight >= 720)
        {
            type = 3;
        }
        else if (width < 480 && hight < 360)
        {
            type = 2;
        }
        else
        {
            type = 4;
        }
        return type;
    }

    /**
     * @新增template
     */
    public String insertTemplate(CmsAvtemplateinfo templateinfo) throws DomainServiceException
    {
        // 计算grouptype
        int framewidth = templateinfo.getFramewidth();
        int framehight = templateinfo.getFramehight();
        int type = caculateGrouptype(framewidth, framehight);
        templateinfo.setGrouptype(type);
        templateinfo.setTemplatetype(1);
        templateDS.insertCmsAvtemplateinfo(templateinfo);
        log.debug("insertCmsAvtemplateinfo()");

        // 记录业务日志
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.template.title"); // 粗编模板管理
        String optObject = String.valueOf(templateinfo.getTemplateindex());
        String optObjecttype = ResourceManager.getResourceText("log.template.add"); // 粗编模板新增

        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.templatedetail.add"); // 操作员[opername]新增了模板[templatename]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[templatename\\]", String.valueOf(templateinfo.getTemplateindex()));

        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        LogInfoMgt.doLog(loginfo);
        String rtn = ResourceManager.getResourceText("rtn.templateadd.success");
        return "0:" + rtn;
    }

    /**
     * 修改粗编模板的方法
     */
    public String updateTemplate(CmsAvtemplateinfo templateinfo) throws DomainServiceException
    {

        String rtn = "";
        CmsAvtemplateinfo tmp = new CmsAvtemplateinfo();
        tmp.setTemplateindex(templateinfo.getTemplateindex());
        tmp = templateDS.getCmsAvtemplateinfo(tmp);
        if(tmp==null){
        	rtn = ResourceManager.getResourceText("msg.info.templatenotexiste");
        	return "1:" + rtn;//
        }
        // 修改校验

        // 订单
        // 粗编任务
        CmsEmbtask ceb = new CmsEmbtask();
        ceb.setTemplateindex(templateinfo.getTemplateindex());
        List<CmsEmbtask> ceblist = cmsEmbtaskDS.getCmsEmbtaskByCond(ceb);
        rtn = ResourceManager.getResourceText("rtn.templateocupied.cntorder");
        if (ceblist != null && ceblist.size() > 0)
        {
            return "1:" + rtn;
        }

        //
        IcmTransfile it = new IcmTransfile();
        it.setTempletid(templateinfo.getTemplateindex());
        List<IcmTransfile> itlist = icmTransfileDS.getIcmTransfileByCond(it);
        rtn = ResourceManager.getResourceText("rtn.templateocupied.cntorder");
        if (itlist != null && itlist.size() > 0)
        {
            return "1:" + rtn;
        }
        // 子内容
        UcmFile uf = new UcmFile();
        uf.setFormatindex(templateinfo.getTemplateindex());
        List<UcmFile> ufist = ucmFileDS.getUcmFileByCond(uf);
        rtn = ResourceManager.getResourceText("rtn.templateocupied.cntorder");
        if (ufist != null && ufist.size() > 0)
        {
            return "1:" + rtn;
        }
        String sql = "select count(*) num from cms_avtemplateinfo where templateindex!="
                + templateinfo.getTemplateindex() + "" + " and templatename='" + templateinfo.getTemplatename() + "' ";
        List listcode;
        try
        {
            listcode = dbUtil.getQuery(sql);
            if (listcode != null && listcode.size() > 0)
            {
                HashMap mapCode = (HashMap) listcode.get(0);
                if (!mapCode.get("num").equals("0"))
                {
                    rtn = ResourceManager.getResourceText("rtn.templatename.exist");
                    return "1:" + rtn;
                }
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //
        int framewidth = templateinfo.getFramewidth();
        int framehight = templateinfo.getFramehight();
        int type = caculateGrouptype(framewidth, framehight);
        templateinfo.setGrouptype(type);
        templateinfo.setTemplatetype(1);
        templateDS.updateCmsAvtemplateinfo(templateinfo);

        log.debug("updatetemplate()");
        // 记录业务日志
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.template.title"); // 目标系统管理
        String optObject = String.valueOf(templateinfo.getTemplateindex());
        String optObjecttype = ResourceManager.getResourceText("log.template.mod"); // 目标系统修改

        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.templatedetail.mod"); // 操作员[opername]修改了目标系统[templatename]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[templatename\\]", String.valueOf(templateinfo.getTemplateindex()));

        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        LogInfoMgt.doLog(loginfo);
        rtn = ResourceManager.getResourceText("rtn.templateupdate.success");

        return "0:" + rtn;
    }

    public String removeTemplate(Long templateindex) throws DomainServiceException
    {
        // 避免删除掉的模板被订单使用，删除之前先做查询,判断此index是否已被订单使用
        String rtn = "";

        // 订单
        // 粗编任务
        CmsEmbtask ceb = new CmsEmbtask();
        ceb.setTemplateindex(templateindex);
        List<CmsEmbtask> ceblist = cmsEmbtaskDS.getCmsEmbtaskByCond(ceb);
        rtn = ResourceManager.getResourceText("rtn.templateocupied.cntorder");
        if (ceblist != null && ceblist.size() > 0)
        {
            return "1:" + rtn;
        }

        // 粗编策略

        // 子内容
        UcmFile uf = new UcmFile();
        uf.setFormatindex(templateindex);
        List<UcmFile> ufist = ucmFileDS.getUcmFileByCond(uf);
        rtn = ResourceManager.getResourceText("rtn.templateocupied.cntorder");
        if (ufist != null && ufist.size() > 0)
        {
            return "1:" + rtn;
        }
        //
        CmsAvtemplateinfo templateinfo = new CmsAvtemplateinfo();
        templateinfo.setTemplateindex(templateindex);
        templateinfo = templateDS.getCmsAvtemplateinfo(templateinfo);
        templateDS.removeCmsAvtemplateinfo(templateinfo);

        // 记录业务日志
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.template.title"); // 粗编模板管理
        String optObject = String.valueOf(templateinfo.getTemplateindex());
        String optObjecttype = ResourceManager.getResourceText("log.template.del"); // 粗编模板删除

        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.templatedetail.del"); // 操作员[opername]删除了粗编模板[templatename]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[templatename\\]", String.valueOf(templateinfo.getTemplateindex()));

        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        LogInfoMgt.doLog(loginfo);
        String rtndelete = ResourceManager.getResourceText("rtn.templatedelete.success");

        return "0:" + rtndelete;
    }

    public String checkExist(Long templateindex) throws DomainServiceException
    {
        String result = "0";
        CmsAvtemplateinfo templateinfo = new CmsAvtemplateinfo();
        templateinfo.setTemplateindex(templateindex);
        List<CmsAvtemplateinfo> list = templateDS.getCmsAvtemplateinfoByCond(templateinfo);
        if (list.size() < 1)
        {
            result = "1";
        }
        return result;
    }

    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize)
            throws DomainServiceException
    {
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("templateindex");

        if (null != cmsAvtemplateinfo.getTemplatename() && !cmsAvtemplateinfo.getTemplatename().equals(""))
        {
            String templatename = EspecialCharMgt.conversion(cmsAvtemplateinfo.getTemplatename());
            cmsAvtemplateinfo.setTemplatename(templatename);
        }

        // if (cmsAvtemplateinfo.getTemplatename() != null
        // && !cmsAvtemplateinfo.getTemplatename().equals("")) {
        // String templatename = EspecialCharMgt.conversion(cmsAvtemplateinfo
        // .getTemplatename());
        // cmsAvtemplateinfo.setTemplatename(templatename);
        // }
        if (cmsAvtemplateinfo.getTemplatetype() != null && !cmsAvtemplateinfo.getTemplatetype().equals(""))
        {

            cmsAvtemplateinfo.setTemplatetype(cmsAvtemplateinfo.getTemplatetype());
        }

        return pageInfoQuery(cmsAvtemplateinfo, start, pageSize, puEntity);
    }

    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        return templateDS.pageInfoQuery(cmsAvtemplateinfo, start, pageSize, puEntity);
    }

    public ICmsEmbtaskDS getCmsEmbtaskDS()
    {
        return cmsEmbtaskDS;
    }

    public void setCmsEmbtaskDS(ICmsEmbtaskDS cmsEmbtaskDS)
    {
        this.cmsEmbtaskDS = cmsEmbtaskDS;
    }

    public IUcmFileDS getUcmFileDS()
    {
        return ucmFileDS;
    }

    public void setUcmFileDS(IUcmFileDS ucmFileDS)
    {
        this.ucmFileDS = ucmFileDS;
    }

    /**
     * 将所有templateindex,templatename放进Map中，供查询table时将templateindex转换成templatename时使用
     * 
     * @return
     */
    public Map<String, String> getTemplateMap(int servicetype, int templatetype)
    {
        log.debug(" getAllTemplateMap start ...");
        Map<String, String> rsMap = new HashMap<String, String>();

        try
        {

            String sql = "";

            sql = "select b.templateindex,b.templatename " + "from cms_avtemplateinfo b  where b.templatetype="
                    + templatetype;

            if (servicetype != -1)
            {
                sql = sql + " and b.servicetype=" + servicetype;
            }
            List rtnlist = dbUtil.getQuery(sql);
            Map tmpMap = new HashMap();
            for (int i = 0; i < rtnlist.size(); i++)
            {
                tmpMap = (Map) rtnlist.get(i);
                rsMap.put((String) tmpMap.get("templateindex"), (String) tmpMap.get("templatename"));
            }
            log.debug(sql);
        }
        catch (Exception e)
        {
            log.error("getAlltemplateMap Exception:", e);
        }
        log.debug(" getAlltemplateMap end ...");
        return rsMap;
    }

    /**
     * 模板和servicetype是多对1的关系。 根据servicetype和targettype获取模板index和name servicetype传-1时代表不传值
     * 
     * @return
     * @throws Exception
     */
    public Map getTemplate(int servicetype, int templatetype, boolean needSelect, String defaultvalue) throws Exception
    {
        List list = new ArrayList();
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        Map<String, Object> map = new HashMap<String, Object>();

        if (needSelect)
        {
            // --请选择--
            textList.add(ResourceManager.getResourceText("label.selectdefault"));
            valueList.add("");
        }

        String sql = "select ca.* from CMS_AVTEMPLATEINFO ca where " + "ca.templatetype=" + templatetype;

        if (servicetype != -1)
        {
            sql = sql + " and ca.servicetype=" + servicetype;
        }
        list = dbUtil.getQuery(sql);

        Map rsMap = null;
        for (int i = 0; i < list.size(); i++)
        {
            rsMap = (Map) list.get(i);
            textList.add((String) rsMap.get("templatename"));
            valueList.add((String) rsMap.get("templateindex"));
        }
        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", defaultvalue);
        return map;
    }

    public String checkExistByName(String templatename) throws DomainServiceException
    {
        String result = "0";
        CmsAvtemplateinfo ci = new CmsAvtemplateinfo();

        ci.setTemplatename(templatename);

        List list = templateDS.getTemplateByName(ci);
        int str = (Integer) list.get(0);
        if (str == 0)
        {// 不存在
            result = "1";
        }
        return result;
    }
}

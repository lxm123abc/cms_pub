package com.zte.cms.castrolemap.dao;

import java.util.List;

import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICastrolemapDAO
{
    /**
     * 新增Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DAOException dao异常
     */
    public void insertCastrolemap(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据主键更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DAOException dao异常
     */
    public void updateCastrolemap(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据条件更新Castrolemap对象
     * 
     * @param castrolemap Castrolemap更新条件
     * @throws DAOException dao异常
     */
    public void updateCastrolemapByCond(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据主键删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @throws DAOException dao异常
     */
    public void deleteCastrolemap(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据条件删除Castrolemap对象
     * 
     * @param castrolemap Castrolemap删除条件
     * @throws DAOException dao异常
     */
    public void deleteCastrolemapByCond(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据主键查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @return 满足条件的Castrolemap对象
     * @throws DAOException dao异常
     */
    public Castrolemap getCastrolemap(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据条件查询Castrolemap对象
     * 
     * @param castrolemap Castrolemap对象
     * @return 满足条件的Castrolemap对象集
     * @throws DAOException dao异常
     */
    public List<Castrolemap> getCastrolemapByCond(Castrolemap castrolemap) throws DAOException;

    /**
     * 根据条件分页查询Castrolemap对象，作为查询条件的参数
     * 
     * @param castrolemap Castrolemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Castrolemap对象，作为查询条件的参数
     * 
     * @param castrolemap Castrolemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
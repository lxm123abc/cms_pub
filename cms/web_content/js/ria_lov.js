// @version 3.00.04


var SSB_RIA_LOV = "ssb_ria_lov";
var Z_SSB_RIA_LOV = "Z:SSB_RIA_LOV";
var lov_rtn_obj = {};
var clearEvent_str="";
var fRiaLovObj = {};
var Lov_EnterEventObj = {};

var charString = "|";
var comma = ",";

//\u4fee\u6539\u8bb0\u5f552 : \u738b\u6d69
//\u65b0\u589e\u7684\u7528\u4e8eLOV\u56de\u663e\u529f\u80fd
function setHiddenValues(targetCompId,lovId)
{
	//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
	if(targetCompId == "" || typeof(targetCompId) =="undefined" || lovId == "" || typeof(lovId) =="undefined" )
		return;
	//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69	
	
	var targetCompIds = targetCompId.split(",");
	var targetCompIdValues = new Array();
	for(var i = 0;i<targetCompIds.length;i++)
	{
		var value = document.getElementById(targetCompIds[i]).value;
		
		//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
		var helpArray = new Array();
		value = trim(value);
		
		if(value.indexOf(",") == -1)
		{
			helpArray.push(value);
		}
		else
		{			
			while(value.indexOf(",") != -1)
			{
				if(value.indexOf(",") == 0)
				{
					helpArray.push("");
					value = trim(value.substr(1));
				}
				else
				{
					var helpStr = value.substring(0,value.indexOf(","));
					helpArray.push(helpStr);
					value = trim(value.substr(value.indexOf(",")+1));
				}
			}
			
			if(value.indexOf(",") == -1)
			{
				helpArray.push(value);
			}
		}
		
		targetCompIdValues[i] = helpArray;
		//\u4fee\u6539\u8bb0\u5f556 : \u738b\u6d69
	}
		
    var hiddenValue = "";
	
	if(targetCompIdValues.length>0)
    {
				
		for(var l = 0;l<targetCompIdValues[0].length;l++)
		{
			var j = 0;
			while(j<targetCompIdValues.length)
			{
				var targetCompIdValueList = targetCompIdValues[j];
		    	hiddenValue += targetCompIdValueList[l];
				
				if(j != targetCompIdValues.length - 1)
					hiddenValue += "|" ;
				j++;		    	
			}	
			if(l != targetCompIdValues[0].length - 1)
		    		hiddenValue += ",";	
		}
		
	}
    
    document.getElementById(lovId).value = hiddenValue;
}

//\u521d\u59cb\u5316
var SSB_riaLov_init = function()
{
	var nodes= document.getElementsByTagName(SSB_RIA_LOV);
	
	if(navigator.appName == "Microsoft Internet Explorer")
	{
		nodes= document.getElementsByTagName(SSB_RIA_LOV);
	}else
	{
		nodes= document.getElementsByTagName(Z_SSB_RIA_LOV);
	}
	
	var nodeArr = new Array();
	for(var n = 0;n<nodes.length;n++)
	{
		nodeArr[n] = nodes[n];
	}
	var nodeLength = nodeArr.length;

	for(var i=0;i<nodeLength;i++)
	{
		var tagname = nodeArr[i].parentNode.tagName.toUpperCase();
		if(tagname != 'Z:COLUMN' && tagname != 'COLUMN')
		{
		  readRiaLovTag(nodeArr[i]);
		}
	}

};

var readRiaLovTag = function (node)
{
	var riaLovObj = {};
	riaLovObj.id = node.getAttribute("id");
	riaLovObj.lovKey = node.getAttribute("lovKey");
	riaLovObj.imageUrl = node.getAttribute("imageUrl");
	riaLovObj.targetId = node.getAttribute("targetId");
	riaLovObj.title = node.getAttribute("title");
	riaLovObj.defaultParm = node.getAttribute("defaultParm");
	riaLovObj.targetCompId = node.getAttribute("targetCompId");
	riaLovObj.jsEventId = node.getAttribute("jsEventId");
	riaLovObj.innerSqlParamValue = node.getAttribute("innerSqlParamValue");
	riaLovObj.clearEvent_id = node.getAttribute("clearEvent_id");
	riaLovObj.width = node.getAttribute("width");
	riaLovObj.height = node.getAttribute("height");
	riaLovObj.emptyresultmsg = node.getAttribute("emptyresultmsg");
	//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	//\u589e\u52a0pageURL\u5c5e\u6027\u8ba9\u7528\u6237\u81ea\u5df1\u5b9a\u5236\u5f39\u51fa\u7684LOV\u9875\u9762
	riaLovObj.pageURL = node.getAttribute("pageURL");

	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	riaLovObj.isRequired = node.getAttribute("isRequired");
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	var riaLovObj_clone = {};
	riaLovObj_clone.id = node.getAttribute("id");
	riaLovObj_clone.lovKey = node.getAttribute("lovKey");
	riaLovObj_clone.imageUrl = node.getAttribute("imageUrl");
	riaLovObj_clone.targetId = node.getAttribute("targetId");
	riaLovObj_clone.title = node.getAttribute("title");
	riaLovObj_clone.defaultParm = node.getAttribute("defaultParm");
	riaLovObj_clone.targetCompId = node.getAttribute("targetCompId");
	riaLovObj_clone.jsEventId = node.getAttribute("jsEventId");
	riaLovObj_clone.innerSqlParamValue = node.getAttribute("innerSqlParamValue");
	riaLovObj_clone.clearEvent_id = node.getAttribute("clearEvent_id");
	riaLovObj_clone.width = node.getAttribute("width");
	riaLovObj_clone.height = node.getAttribute("height");
	riaLovObj_clone.emptyresultmsg = node.getAttribute("emptyresultmsg");
	//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	//\u589e\u52a0pageURL\u5c5e\u6027\u8ba9\u7528\u6237\u81ea\u5df1\u5b9a\u5236\u5f39\u51fa\u7684LOV\u9875\u9762
	riaLovObj_clone.pageURL = node.getAttribute("pageURL");

	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	riaLovObj_clone.isRequired = node.getAttribute("isRequired");
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	var obj = new SSB_riaLov();
	obj.writeRiaLovTag(riaLovObj,riaLovObj_clone);
};

var SSB_riaLov = function(){};

SSB_riaLov.prototype = {
	
	writeRiaLovTag : function (riaLovObj,riaLovObj_clone)
	{	
		var theLovElement = document.getElementById(riaLovObj.id);
		var parentElement = theLovElement.parentNode;
	
		var image=document.createElement("img");
		image.setAttribute("src",riaLovObj.imageUrl);
		image.style.cursor = "pointer";
		image.setAttribute("border","0");
		image.setAttribute("id","lov_img_"+riaLovObj.id);

		image.onclick = function (){
			openRiaLovWindow (riaLovObj,riaLovObj_clone,image);
		};

		var hiddenId = document.createElement("input");
		hiddenId.setAttribute("id",riaLovObj.id);
		hiddenId.setAttribute("type","hidden");
		
		parentElement.appendChild(image);
		parentElement.appendChild(hiddenId);
		
		parentElement.replaceChild(image,theLovElement);
	}
	
};
var	openRiaLovWindow = function (riaLovObj,riaLovObj_clone,image)
{	
	//\u4fee\u6539\u8bb0\u5f552 : \u738b\u6d69
	setHiddenValues(riaLovObj.targetCompId,riaLovObj.id);
	
	lov_rtn_obj = {};
	var valueName=document.getElementById(riaLovObj.id).value;
	
	var valueNames = valueName.split(",");
	for(var m=0;m<valueNames.length;m++)
	{
	  lov_rtn_obj[valueNames[m]]=true;
	}
	var wWidth = 640;
    var wHeight = 510;
	if(riaLovObj.width)
	{
		wWidth = riaLovObj.width;
	}
	
	if(riaLovObj.height)
	{
		wHeight = riaLovObj.height;
	}
	
	var wTop = (window.screen.height - wHeight)/2;
    var wLeft = (window.screen.width - wWidth)/2;

	fRiaLovObj = {};
	fRiaLovObj = riaLovObj_clone;
	fRiaLovObj.defaultParm = parseThings(riaLovObj.defaultParm);
	fRiaLovObj.innerSqlParamValue = parseThings(riaLovObj.innerSqlParamValue,image);
	fRiaLovObj.valueName = valueName;
	fRiaLovObj.width = wWidth;
	fRiaLovObj.height = wHeight;
	
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------start----------
	Lov_EnterEventObj.isRequired = riaLovObj.isRequired;
	delete fRiaLovObj.isRequired;
	//\u4fee\u6539\u8bb0\u5f5510 : \u738b\u6d69 -------end----------
	
	//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
	delete fRiaLovObj.pageURL;
	
	if(lovEnterEventObj && lovEnterEventObj.textValue && document.getElementById(lovEnterEventObj.textValue))
	{
		fRiaLovObj.textValue = document.getElementById(lovEnterEventObj.textValue).value.trim();
	}else
	{
		fRiaLovObj.textValue = null;
	}
	
	
	if(lovEnterEventObj && lovEnterEventObj.enterDown)
	{
		lov_getEnterEvent(fRiaLovObj);
	}else{
		lovEnterEventObj.textValue = null;
		var rialovurl = riaLovObj.imageUrl.substring(0,riaLovObj.imageUrl.indexOf("image"));
		
		//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
		if(!riaLovObj.pageURL || riaLovObj.pageURL.trim() == "")
		{
			//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
			//delete fRiaLovObj.pageURL;
			//\u4fee\u6539\u8bb0\u5f555 : \u738b\u6d69 ---resizable=yes
			window.open(rialovurl+"common/lov/rialovpage.html","LOV","top="+wTop+",left="+wLeft+",height="+wHeight+",width="+wWidth+",status=no,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes");
		}
		else if(riaLovObj.pageURL)
		{
			//\u4fee\u6539\u8bb0\u5f554 : \u738b\u6d69
			//delete fRiaLovObj.pageURL;
			//\u4fee\u6539\u8bb0\u5f555 : \u738b\u6d69 ---resizable=yes
			window.open(rialovurl+riaLovObj.pageURL.trim(),"LOV","top="+wTop+",left="+wLeft+",height="+wHeight+",width="+wWidth+",status=no,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes");		
		}
	}

};

var lov_getEnterEvent = function (fRiaLovObj)
{	
	lovEnterEventObj.enterDown = false;
	callUrl("LovEnterEventAction/getRiaLovEnterEvent.ssm",fRiaLovObj,"Lov_EnterEventObj","true");
	lov_parseResults(Lov_EnterEventObj);
};

var parseThings = function (defaultParm,image)
{
	var def_parms = defaultParm;
    var def_parm = unescape(defaultParm).substring(0,1);
    if(def_parm == "~"){	
        var str_val = unescape(defaultParm).substring(1);
        var ssbfunction =  str_val;     
         if(str_val.indexOf('this') != -1)
          {            	
           	eval("var image = image");           
           	var ssbfunction = str_val.replace("this","image");
          }
        def_parms = eval(ssbfunction);   
		defaultParm = escape(def_parms);
    }   
	return unescape(defaultParm);
}

var setTreeInfo = function (treeObj,str)
{
	var compMap = riaLovStartObj.rtnValue.compMap;
	var tagArr = riaLovObj.targetId.split(comma);
	
	for(var i = 0;i<tagArr.length;i++)
	{
		opener.document.getElementById(compMap[tagArr[i]]).value="";
		if(str != "clear")
		{
			opener.document.getElementById(compMap[tagArr[i]]).value=treeObj[tagArr[i]];
		}
	}
	
	if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
	{
		opener.document.getElementById(riaLovObj.id).value = treeObj.ria_tree_menuId;
	}
	
	if(riaLovObj.jsEventId && str == "confirm"  && opener.document.getElementById(riaLovObj.jsEventId))
	{
		opener.document.getElementById(riaLovObj.jsEventId).onchange();
	}
	
	if(str == "clear")
	{
		if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
		{
			opener.document.getElementById(riaLovObj.id).value = "";
		}
		if(riaLovObj.clearEvent_id  && opener.document.getElementById(riaLovObj.clearEvent_id))
		{
			opener.document.getElementById(riaLovObj.clearEvent_id).onchange();
		}
	}
	
	
			
};

var getTreeName = function (treeObj)
{
	
	if(riaLovStartObj && riaLovObj)
	{
		setTreeInfo(treeObj,"confirm");
	}	
    self.close();   
};

var getTreeNames = function (treeValues)
{
	if(riaLovStartObj && riaLovObj && treeValues)
	{
		var treeObj = {};
		var tagArr = riaLovObj.targetId.split(comma);
		var tree_arr = new Array(tagArr.length);
		
		for(var i=0;i<tagArr.length;i++)
		{
			tree_arr[i] = "";
		}
		for(var i=0;i<tree_arr.length;i++)
		{
			treeObj.ria_tree_menuId = "";
			var tree_char = "";
			for(var j=0;j<treeValues.length;j++)
			{
				tree_arr[i] = tree_arr[i] + tree_char + treeValues[j][tagArr[i]];
				treeObj.ria_tree_menuId = treeObj.ria_tree_menuId + tree_char + treeValues[j].ria_tree_menuId;
				tree_char = comma;
			}
			treeObj[tagArr[i]] = tree_arr[i];			
		}
				
		setTreeInfo(treeObj,"confirm");	
		
	}
		if(treeValues.length<=0)
		{
			var confirmValue = confirm($res_entry("lov.confirm","\u60a8\u8fd8\u6ca1\u6709\u9009\u62e9\u4efb\u4f55\u4e00\u9879\uff0c\u786e\u5b9a\u8981\u5173\u95ed\u7a97\u53e3\u5417?"));
			if(confirmValue == true)
				self.close();
		}
		else
		{
			self.close();
		}

};

function closeWindow(){
	self.close();
};

var clearTree = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		var treeObj = {};
		treeObj.menuName = "";
		treeObj.menuId = "";
		treeObj.menuFaId = "";
		
		setTreeInfo(treeObj,"clear");
	}	
	self.close();
};

var setTableInfo = function (tableStr,str,checkedStr)
{
	var compMap = riaLovStartObj.rtnValue.compMap;
	var tagArr = riaLovObj.targetId.split(comma);
	var tableArr = tableStr.split(charString);
	
	for(var i = 0;i<tagArr.length;i++)
	{
		opener.document.getElementById(compMap[tagArr[i]]).value="";
		if(str != "clear")
		{
			opener.document.getElementById(compMap[tagArr[i]]).value=tableArr[i];
		}
	}
	
	if(checkedStr && riaLovObj.id && opener.document.getElementById(riaLovObj.id))
	{
		opener.document.getElementById(riaLovObj.id).value = checkedStr;
	}		
	
	if(riaLovObj.jsEventId && riaLovObj.jsEventId!="" && str == "confirm"  && opener.document.getElementById(riaLovObj.jsEventId))
	{
		opener.document.getElementById(riaLovObj.jsEventId).onchange();
	}
	
	if(str == "clear")
	{
		if(riaLovObj.id && opener.document.getElementById(riaLovObj.id))
		{
			opener.document.getElementById(riaLovObj.id).value = "";
		}	
		if(riaLovObj.clearEvent_id && opener.document.getElementById(riaLovObj.clearEvent_id))
		{	
			opener.document.getElementById(riaLovObj.clearEvent_id).onchange();
		}
	}
		
	self.close(); 
};

var getTableName = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		var rows = getCheckedRowsValue('ssbTable');
		if(rows[0])
		{
			//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
			setTableInfo(rows[0].ria_lov_table_allValue,"confirm",rows[0].ria_lov_table_allValue);		
		}
		else
		{
			//wanghao
			var confirmValue = confirm($res_entry("lov.confirm","\u60a8\u8fd8\u6ca1\u6709\u9009\u62e9\u4efb\u4f55\u4e00\u9879\uff0c\u786e\u5b9a\u8981\u5173\u95ed\u7a97\u53e3\u5417?"));
			if(confirmValue == true)
				self.close(); 
		}		
	}
};

var getTableNames = function ()
{
	
	if(riaLovStartObj && riaLovObj)
	{
		var rows = getCheckedRowsValue('ssbTable');
		if(!rows.length>0)
		{
			//wanghao			
			var confirmValue = confirm($res_entry("lov.confirm","\u60a8\u8fd8\u6ca1\u6709\u9009\u62e9\u4efb\u4f55\u4e00\u9879\uff0c\u786e\u5b9a\u8981\u5173\u95ed\u7a97\u53e3\u5417?"));
			if(confirmValue == true)
				self.close();
			return;
		}
		var tagArr = riaLovObj.targetId.split(comma);
		var valueArr = new Array(tagArr.length);
		for(var j=0;j<tagArr.length;j++)
		{
			valueArr[j] = "";
		}
		var names = "";
		var chars = "";
		var checkedStr = "";
		for(var i=0;i<rows.length;i++)
		{
			checkedStr = checkedStr + chars + rows[i].ria_lov_table_allValue;
			for(var j=0;j<tagArr.length;j++)
			{
				valueArr[j] = valueArr[j] + chars + rows[i].ria_lov_table_allValue.split(charString)[j];
			}
			chars = comma;
		}
		chars="";
		for(var j =0;j<tagArr.length;j++){		
			names=names+ chars + valueArr[j] ;
			chars=charString;
		}
		setTableInfo(names,"confirm",checkedStr);	
	}
	self.close(); 
};

var clearTable = function ()
{
	if(riaLovStartObj && riaLovObj)
	{
		setTableInfo("","clear");
	}	
	self.close();
};

//\u9875\u9762js

function initRiaLovPage(){

	callUrl('riaLovAction/getRiaLovTags.ssm',riaLovObj,'riaLovStartObj','true');
	
	document.title = riaLovObj.title;	
	var riaLovs = riaLovStartObj.rtnValue;
	
	if(riaLovs && (riaLovs.queryType == "singletree" || riaLovs.queryType == "multitree"))
	{	
		document.getElementById("ria_tree_id").style.visibility = "visible";
		document.getElementById("treeConfirmButtonId").value = riaLovs.buttonMap.confirm;
		document.getElementById("treeClearButtonId").value = riaLovs.buttonMap.clear;
		document.getElementById("treeCancelButtonId").value = riaLovs.buttonMap.cancel;
		callSid('rialovtest',riaLovObj,'menuTree');
	}
	else if(riaLovs)
	{
		document.getElementById("floaters").style.height = "1px";
		document.getElementById("floaters").style.width = "1px";
		document.getElementById("floaters").style.top = 0;
		document.getElementById("ria_table_id").style.visibility = "visible";
		document.getElementById("ria_lov_lable").innerHTML = riaLovs.buttonMap.write;
		createQueryTable(riaLovs);
		
		document.getElementById("tableConfirmButtonId").value = riaLovs.buttonMap.confirm;
		document.getElementById("tableClearButtonId").value = riaLovs.buttonMap.clear;
		document.getElementById("tableCancelButtonId").value = riaLovs.buttonMap.cancel;
		document.getElementById("tableQueryButtonId").value = riaLovs.buttonMap.query;
	
		createTableColumn(riaLovs);
		riaLovObj.queryConditions = "";
		riaLovObj.isQuery = "false";
		searchTableData('getTableInfo','riaLovObj','ssbTable');
	}
};
addOnloadEvent(initRiaLovPage); 
	
function addListener()
{

	var queryType = riaLovStartObj.rtnValue.queryType;
	if(queryType == "singletree")
	{
		document.getElementById("floaters").style.top = parseInt(riaLovObj.height)-30;
		
		document.getElementById("treeConfirmButtonId").style.visibility = "hidden";	
		document.getElementById("treeClearButtonId").style.visibility = "visible";	
		document.getElementById("treeCancelButtonId").style.visibility = "visible";	
			
		SSB_Tree.trees['menuTree'].addNodeListener('onclick',getMenuTree);
		
	}else if(queryType == "multitree")
	{
		document.getElementById("floaters").style.top = parseInt(riaLovObj.height)-30;
	
		document.getElementById("treeConfirmButtonId").style.visibility = "visible";	
		document.getElementById("treeClearButtonId").style.visibility = "visible";	
		document.getElementById("treeCancelButtonId").style.visibility = "visible";	
		
	}
	
};

function confirmTree()
{
	getTreeNames(SSB_Tree.trees['menuTree'].getObjValue());
};

function getMenuTree(){
	//\u4e3a\u4e86\u8ba9\u70b9\u7236\u8282\u70b9\u4e5f\u80fd\u9009\u70b9\u503c
	/*var childNodeOnly = riaLovStartObj.rtnValue.childNodeOnly;
	if(childNodeOnly && childNodeOnly=="true")
	{
		if(this.parentNode.getAttribute('istreeleaf') == 'false' || !this.parentNode.getAttribute('istreeleaf'))
		{
			return;
		}
	}	*/
	getTreeName(this['model']);
};

 SSB_MultiTree.prototype.setNode=function(node){
	var span=node.getElementsByTagName('SPAN')[0];
	span.className='';
 };
 
 function createTableColumn(riaLovs)
 {
 	var cName = riaLovs.tableMaps.tableName;
 	var kName = riaLovs.tableMaps.disStr;
 	var queryType = riaLovs.queryType;
	var column_str = "<z:table id='ssbTable' cellspacing='1' border='0' turnpage='true'  pagesizelist='10,20,30,50' >";
	if(queryType=="singletxt")
	{
		//\u4fee\u6539\u8bb0\u5f553 : \u738b\u6d69
	    column_str = column_str + "<z:column id=col0 width='10%' iskey='true' caption=$res_entry('lov.table.select','\u9009\u62e9') prop='ria_lov_table_allValue'><input type='radio'  name='check'  ctrlex='ria_lov_table_checked' /></z:column>";			
	}
	else
	{
	    column_str =column_str + "<z:column id=col0 width='10%' iskey='true' checkall='true' caption=$res_entry('lov.table.select.all','\u5168\u9009') prop='ria_lov_table_allValue' ><input type='checkbox'  name='check' ctrlex='ria_lov_table_checked' /></z:column>";					
	}

 	for(var j = 0;j<kName.length;j++)
 	{
  		column_str = column_str + '<z:column width="'+90/kName.length+'%" caption="'+cName[kName[j]]+'"  prop="'+kName[j]+'"></z:column>';		 		
 	}
 
	var columns = document.getElementById('ssb_ria_lov_tableID');	
	var multiColumns = '<div>';
	if(queryType=="multitxt")
	{
		columns.innerHTML = multiColumns + column_str +'</z:table></div>';
	}else{
		columns.innerHTML = column_str+'</z:table>';
	}	
	SSB_Table_Init();
	if(queryType=="multitxt")
	{
		var pages = document.getElementById('ssbTable_pages');
	    pages.style.display='none';
	}
 };
 
 function createQueryTable(riaLovs)
 {
 	var queryStr ='<table cellspacing=1 class="tb_lov_searchbar" width="100%">'+ riaLovs.queryStr;
    queryStr = queryStr + '<td colspan=4 align="right"><input id="tableQueryButtonId" type="button" onClick="queryTables()"  class="button" ></td></tr>';
 	var columns = document.getElementById('queryID');	
 	columns.innerHTML = queryStr +'</table>';
 };
 
 function confirmTable()
 {
 	var queryType = riaLovStartObj.rtnValue.queryType;
	if(queryType=="singletxt")
	{
		getTableName();
	}
	else if(queryType=="multitxt")
	{
		getTableNames();
	}
 };
 
 function queryTables()
 {
 	var queryStr = "";
 	var queryStrs = riaLovStartObj.rtnValue.queryCondition;
 	var queryArr = queryStrs.split(comma);
 	var chars = "";
 	for(var i=0;i<queryArr.length;i++)
 	{
 		queryStr = queryStr  + chars + queryArr[i] +":"+ document.getElementById("ria_lov_"+queryArr[i]).value;
 		chars = comma;
 	}
 	riaLovObj.queryConditions = queryStr.replace(/\'/g,"&apos;");;
	riaLovObj.isQuery = "true";  	
 	searchTableData('getTableInfo','riaLovObj','ssbTable');
 };
 
 
//lov\u56de\u8f66\u4e8b\u4ef6 
var lovEnterEventObj = {};
var lov_enterEventOrOnblur = true;

var lovEnterEvent = function (id,tid)
{
	if(event.keyCode==13)
	{
		lov_enterEventOrOnblur = false;
		this.focus();
		lovEnterEventObj.enterDown = true;
		if(document.getElementById(tid))
		{
			lovEnterEventObj.textValue = tid;
		}
		if(document.getElementById("lov_img_"+id))
		{
			document.getElementById("lov_img_"+id).onclick();
		}		
		lov_enterEventOrOnblur = true;
	}
	
};
// \u5931\u53bb\u7126\u70b9\u4e8b\u4ef6
var lovOnblurEnterEvent = function (id,tid)
{
	if(lov_enterEventOrOnblur)
	{		
		if(!document.getElementById(tid) || document.getElementById(tid).value == "")
		{
			return;
		}
		lovEnterEventObj.enterDown = true;
		if(document.getElementById(tid))
		{
			lovEnterEventObj.textValue = tid;
		}
		if(document.getElementById("lov_img_"+id))
		{
			document.getElementById("lov_img_"+id).onclick();
		}
	}
};

var lov_parseResults = function (Lov_EnterEventObj){
	
	if(!Lov_EnterEventObj || !Lov_EnterEventObj.rtnValue)
	{
		return ;
	}
	var responseValue = Lov_EnterEventObj.rtnValue;
	var count = responseValue.number;
	switch (count)
	{
		case 0 :
		
			//\u4fee\u6539\u8bb0\u5f557 : \u738b\u6d69 -------start----------
			alert($res_entry('ria.tip.res',"\u65e0\u6cd5\u627e\u5230\u4e0e\u8f93\u5165\u5185\u5bb9\u5339\u914d\u7684\u8bb0\u5f55\uff0c\u8bf7\u4fee\u6539\u540e\u518d\u67e5\u8be2"));
			//\u4fee\u6539\u8bb0\u5f557 : \u738b\u6d69 -------end----------
			
			//\u4fee\u6539\u8bb0\u5f559 : \u738b\u6d69 -------start----------
			if(!Lov_EnterEventObj.isRequired || Lov_EnterEventObj.isRequired.trim() == "" ||Lov_EnterEventObj.isRequired.trim() =="true")
			{
				//\u4fee\u6539\u8bb0\u5f558 : \u738b\u6d69 -------start----------
				document.getElementById(lovEnterEventObj.textValue).focus();
				//\u4fee\u6539\u8bb0\u5f558 : \u738b\u6d69 -------start----------
			}
			//\u4fee\u6539\u8bb0\u5f559 : \u738b\u6d69 -------end----------
			
			lovEnterEventObj.textValue = null;
			break;
		case 1 :
			lov_rtnEnterValues(responseValue);
			break;
		case 2 :
			lov_confirmEvent(responseValue);
			break;
		case 3 :
			lov_openLov(responseValue);
			break;
		default:
			break;
	}

};

var lov_rtnEnterValues = function(responseValue)
{
	
	var mapValue = responseValue.mapValue;
	var listValue = responseValue.listValue;
	var jsEventId = responseValue.jsEventId;
	if(listValue && mapValue)
	{
		for(var i=0;i<listValue.length;i++)
		{
			if(document.getElementById(listValue[i]))
			{
				document.getElementById(listValue[i]).value = mapValue[listValue[i]];
			}
		}
	}
	if(jsEventId && jsEventId != "" && document.getElementById(jsEventId))
	{
		document.getElementById(jsEventId).onchange();
	}
	lovEnterEventObj.textValue = null;
};

var lov_openLov = function (responseValue)
{
	var id = responseValue.id;
	lovEnterEventObj.enterDown = false;
	if(document.getElementById("lov_img_"+id))
	{
		document.getElementById("lov_img_"+id).onclick();
	}
};

var lov_confirmEvent = function (responseValue)
{
	//if(confirm("\u4f60\u6240\u67e5\u8be2\u7684\u6570\u636e\u591a\u4e8e"+responseValue.countMax+"\u6761\uff0c\u662f\u5426\u7ee7\u7eed\u67e5\u8be2"))
	if(confirm($res_entry("lov.data.tooMush","\u5339\u914d\u8bb0\u5f55\u8fc7\u591a\uff0c\u662f\u5426\u901a\u8fc7LOV\u9009\u62e9")))
	{
		lov_openLov(responseValue);
	}else
	{
		lovEnterEventObj.textValue = null;
	}
};

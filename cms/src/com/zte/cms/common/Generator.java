package com.zte.cms.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;


public class Generator
{
    /**
     * 得到Contentid
     * 
     * @param cpid CP的编号
     * @param type object类型：在imp_sh_cms_config表中配置 'PROG'-program,'SERI'-series,'CHAN'-channel,'SCHE'-schedule
     *                         'PACK'-service,'CATE'-category,'PICT'-picture,'CAST'-cast,'CARM'-castrolemap
     */
    public static String getContentId(Long cpid, String type)
    {
        Connection con = null;
        String contentId = "";
        DbUtil dbutil = new DbUtil();
        CallableStatement proc = null;
        try
        {
            con = dbutil.getConn();
            proc = con.prepareCall("{ call sp_get_contentid(?, ?, ?) }");
            proc.setLong(1, cpid);
            proc.setString(2, type);
            proc.registerOutParameter(3, java.sql.Types.VARCHAR);
            proc.execute();
            contentId = proc.getString(3);

        }
        catch (SQLException e)
        {
            con.close();
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (proc != null)
                    proc.close();
                if (con != null)
                    con.close();
            }
            catch (SQLException e)
            {

            }
            return contentId;
        }
    }

    /**
     * 得到physicalcontentid
     * 
     * @param type object类型：在imp_sh_cms_config表中配置 'MOVI'-movie,'PHCH'-物理频道,'SCHERD'-录制计划
     */
    public static String getPhysicalContentId(String type)
    {
        Connection con = null;
        String physicalContentId = "";
        DbUtil dbutil = new DbUtil();
        CallableStatement proc = null;
        try
        {
            con = dbutil.getConn();
            proc = con.prepareCall("{ call sp_get_physicalcontentid(?, ?) }");
            proc.setString(1, type);
            proc.registerOutParameter(2, java.sql.Types.VARCHAR);
            proc.execute();
            physicalContentId = proc.getString(2);

        }
        catch (SQLException e)
        {
            con.close();
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (proc != null)
                    proc.close();
                if (con != null)
                    con.close();
            }
            catch (SQLException e)
            {

            }
            return physicalContentId;
        }
    }
    
    /**
     * 解析contentId，根据规则生成wgcode
     * 适用于program、series、channel、schedule、service、category、picture、cast、castrolemap
     * @param contentId 
     */
    public static String getWGCodeByContentId(String contentId)
    {
        Connection con = null;
        String wgcode = "";
        DbUtil dbutil = new DbUtil();
        CallableStatement proc = null;
        try
        {
            con = dbutil.getConn();
            proc = con.prepareCall("{ call sp_get_wgcodebycntid(?, ?) }");
            proc.setString(1, contentId);
            proc.registerOutParameter(2, java.sql.Types.VARCHAR);
            proc.execute();
            wgcode = proc.getString(2);

        }
        catch (SQLException e)
        {
            con.close();
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (proc != null)
                    proc.close();
                if (con != null)
                    con.close();
            }
            catch (SQLException e)
            {

            }
            return wgcode;
        }
    }
    
    /**
     * 解析physicalContentId，根据规则生成wgcode
     * 适用于：movie、physicalchannel、schedulerecord
     * @param physicalContentId 
     */
    public static String getWGCodeByPhysicalContentId(String physicalContentId)
    {
        Connection con = null;
        String wgcode = "";
        DbUtil dbutil = new DbUtil();
        CallableStatement proc = null;
        try
        {
            con = dbutil.getConn();
            proc = con.prepareCall("{ call sp_get_wgcodebyphysicalcntid(?, ?) }");
            proc.setString(1, physicalContentId);
            proc.registerOutParameter(2, java.sql.Types.VARCHAR);
            proc.execute();
            wgcode = proc.getString(2);

        }
        catch (SQLException e)
        {
            con.close();
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (proc != null)
                    proc.close();
                if (con != null)
                    con.close();
            }
            catch (SQLException e)
            {

            }
            return wgcode;
        }
    }

    public static void main(String[] args)
    {
        try
        {
            System.out.println(getContentId(new Long("12345678"), "PROG"));
            System.out.println(getPhysicalContentId("MOVI"));
            System.out.println(getWGCodeByContentId("0000000100000001000000000000123"));
            System.out.println(getWGCodeByPhysicalContentId("01000000000000000000000000000123"));
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

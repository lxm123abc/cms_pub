package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsLpopNoauditDAO extends DynamicObjectBaseDao implements ICmsLpopNoauditDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("insert cmsLpopNoaudit starting...");
        super.insert("insertCmsLpopNoaudit", cmsLpopNoaudit);
        log.debug("insert cmsLpopNoaudit end");
    }

    public void updateCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("update cmsLpopNoaudit by pk starting...");
        super.update("updateCmsLpopNoaudit", cmsLpopNoaudit);
        log.debug("update cmsLpopNoaudit by pk end");
    }

    public void updateCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("update cmsLpopNoaudit by conditions starting...");
        super.update("updateCmsLpopNoauditByCond", cmsLpopNoaudit);
        log.debug("update cmsLpopNoaudit by conditions end");
    }

    public void deleteCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("delete cmsLpopNoaudit by pk starting...");
        super.delete("deleteCmsLpopNoaudit", cmsLpopNoaudit);
        log.debug("delete cmsLpopNoaudit by pk end");
    }

    public void deleteCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("delete cmsLpopNoaudit by conditions starting...");
        super.delete("deleteCmsLpopNoauditByCond", cmsLpopNoaudit);
        log.debug("update cmsLpopNoaudit by conditions end");
    }

    public CmsLpopNoaudit getCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("query cmsLpopNoaudit starting...");
        CmsLpopNoaudit resultObj = (CmsLpopNoaudit) super.queryForObject("getCmsLpopNoaudit", cmsLpopNoaudit);
        log.debug("query cmsLpopNoaudit end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsLpopNoaudit> getCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException
    {
        log.debug("query cmsLpopNoaudit by condition starting...");
        List<CmsLpopNoaudit> rList = (List<CmsLpopNoaudit>) super.queryForList("getCmsLpopNoauditListByCond",
                cmsLpopNoaudit);
        log.debug("query cmsLpopNoaudit by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsLpopNoaudit by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsLpopNoauditListCntByCond", cmsLpopNoaudit)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsLpopNoaudit> rsList = (List<CmsLpopNoaudit>) super.pageQuery("queryCmsLpopNoauditListByCond",
                    cmsLpopNoaudit, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsLpopNoaudit by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsLpopNoauditListByCond", "queryCmsLpopNoauditListCntByCond",
                cmsLpopNoaudit, start, pageSize, puEntity);
    }

    public PageInfo pageInfoQuery(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        // TODO Auto-generated method stub
        return super.indexPageQuery("queryUcpCmsLpopNoauditListByCond", "queryUcpCmsLpopNoauditListCntByCond",
                ucpcmsLpopNoaudit, start, pageSize, puEntity);

    }

    @SuppressWarnings("unchecked")
    public List<UcpCmsLpopNoaudit> getCmsLpopNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit) throws DAOException
    {
        log.debug("query cmsLpopNoaudit by condition starting...");
        List<UcpCmsLpopNoaudit> rList = (List<UcpCmsLpopNoaudit>) super.queryForList("queryUcpCmsLpopNoauditList",
                ucpCmsLpopNoaudit);
        log.debug("query cmsLpopNoaudit by condition end");
        return rList;
    }

    /*
     * 查询CP信息 以table展示
     */
    public PageInfo pageInfoQueryForOPTable(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsLpopNoaudit by condition starting...");
        PageInfo pageInfo = null;
        if (ucpcmsLpopNoaudit.getLptype() == 2)
        {
            // int totalCnt = ((Integer) super.queryForObject("queryUcpCmsLpopNoauditListCntByCond",
            // ucpcmsLpopNoaudit)).intValue();
            int totalCnt = ((List<UcpCmsLpopNoaudit>) super.queryForList("queryUcpCmsLpopNoauditList",
                    ucpcmsLpopNoaudit)).size();
            if (totalCnt > 0)
            {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
                List<UcpCmsLpopNoaudit> rsList = (List<UcpCmsLpopNoaudit>) super.pageQuery(
                        "queryUcpCmsLpopNoauditList", ucpcmsLpopNoaudit, start, fetchSize);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
            }
            else
            {
                pageInfo = new PageInfo();
            }
        }
        else
        {
            // int totalCnt = ((Integer) super.queryForObject("queryUcpCmsLpopNoauditListCntByCond",
            // ucpcmsLpopNoaudit)).intValue();
            int totalCnt = ((List<UcpCmsLpopNoaudit>) super.queryForList("queryUcpCmsLpopNoauditListForYun",
                    ucpcmsLpopNoaudit)).size();
            if (totalCnt > 0)
            {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
                List<UcpCmsLpopNoaudit> rsList = (List<UcpCmsLpopNoaudit>) super.pageQuery(
                        "queryUcpCmsLpopNoauditListForYun", ucpcmsLpopNoaudit, start, fetchSize);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
            }
            else
            {
                pageInfo = new PageInfo();
            }
        }
        log.debug("page query cmsLpopNoaudit by condition end");
        return pageInfo;

    }

}
package com.zte.cms.ctgpgmap.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.program.programsync.service.IProgramTargetSynDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.ctgpgmap.model.CategoryProgramMapConstants;
import com.zte.cms.ctgpgmap.service.ICategoryProgramMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 
 * @author Administrator
 * 
 */
public class CntPgMapLS extends DynamicObjectBaseDS implements ICntPgMapLS {
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL,getClass());
    private ICategoryProgramMapDS ctgpgmapDS;
	private ICategorycdnDS catDS;
    private ICmsProgramDS cmsProgramDS;
    private IUsysConfigDS usysConfigDS = null;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ITargetsystemDS targetSystemDS;
	private ICntSyncTaskDS cntsyncTaskDS;
	private IObjectSyncRecordDS objectSyncRecordDS;
    private IProgramTargetSynDS programTargetSynDS;
    private ICmsStorageareaLS cmsStorageareaLS;
	private DbUtil dbUtil = new DbUtil();
    public Long syncindex = null;  
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";

    public static final String pubFail = "发布失败";
    private Map cntgrmap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
	public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    private String result = "0:操作成功";
    public IProgramTargetSynDS getProgramTargetSynDS()
	{
		return programTargetSynDS;
	}
	public void setProgramTargetSynDS(IProgramTargetSynDS programTargetSynDS)
	{
		this.programTargetSynDS = programTargetSynDS;
	}
	public IObjectSyncRecordDS getObjectSyncRecordDS()
	{
		return objectSyncRecordDS;
	}
	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
	{
		this.objectSyncRecordDS = objectSyncRecordDS;
	}
	public ICntSyncTaskDS getCntsyncTaskDS()
	{
		return cntsyncTaskDS;
	}
	public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
	{
		this.cntsyncTaskDS = cntsyncTaskDS;
	}
    
    public ICmsStorageareaLS getCmsStorageareaLS()
	{
		return cmsStorageareaLS;
	}
	public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
	{
		this.cmsStorageareaLS = cmsStorageareaLS;
	}

	public ICategorycdnDS getCatDS() {
		return catDS;
	}
	public void setCatDS(ICategorycdnDS catDS) {
		this.catDS = catDS;
	}

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }
    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }
    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    
	public ICmsProgramDS getCmsProgramDS() {
        return cmsProgramDS;
    }

	public void setCmsProgramDS(ICmsProgramDS cmsProgramDS) {
        this.cmsProgramDS = cmsProgramDS;
    }

	public IUsysConfigDS getUsysConfigDS() {
        return usysConfigDS;
    }

	public void setUsysConfigDS(IUsysConfigDS usysConfigDS) {
        this.usysConfigDS = usysConfigDS;
    }

	public ICategoryProgramMapDS getCtgpgmapDS() {
        return ctgpgmapDS;
    }

	public void setCtgpgmapDS(ICategoryProgramMapDS ctgpgmapDS) {
        this.ctgpgmapDS = ctgpgmapDS;
    }

    
    /**
     * @param categoryProgramMap CategoryProgramMap
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     */
	public TableDataInfo queryCntPgMapList(
			CategoryProgramMap categoryProgramMap, int start, int pageSize) {
        log.debug("queryCntPgMapList start...");
        TableDataInfo dataInfo = null;
		try {
			dataInfo = ctgpgmapDS.pageInfoQuery(categoryProgramMap, start,
					pageSize);

		} catch (DomainServiceException e) {
            log.error("queryCntPgMapList error");
        }
        log.debug("queryCntPgMapList end");
        return dataInfo;
    }

    /**
     * 删除
     */
    /**
     * @param id Long
     * @return CategoryProgramMap
     */
	public String deletectgpgmap(Long mapindex) {
        log.debug("deleteProgramtype start...");
        String result = ResourceMgt.findDefaultText("category.delete.success.figure");
        CategoryProgramMap cpmap = new CategoryProgramMap();
		try {
		    cpmap.setMapindex(mapindex);
            cpmap = ctgpgmapDS.getCategoryProgramMap(cpmap);
            if (cpmap == null){
                return "1"+ResourceMgt.findDefaultText("bind.relation.notexist");
            }
            
            CntPlatformSync platform = new CntPlatformSync();
            platform.setObjecttype(26);
            platform.setObjectid(cpmap.getMappingid());
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(26);
            target.setObjectid(cpmap.getMappingid());
            List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
            if(targetList!=null && targetList.size()>0){
                for(CntTargetSync ctgpTarget:targetList){
                    int ctgpSyncStatus = ctgpTarget.getStatus();
                    if(ctgpSyncStatus==200||ctgpSyncStatus==300){
                        return "1"+ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete");
                    }
                }
                cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
                cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
            }
            
            ctgpgmapDS.removeCategoryProgramMap(cpmap);
            CommonLogUtil.insertOperatorLog(
                    cpmap.getMapindex().toString(),
                    CategoryProgramMapConstants.MGTTYPE_CTGPGMAP,
                    CategoryProgramMapConstants.OPERTYPE_CTGPG_DELETE,
                    CategoryProgramMapConstants.OPERTYPE_CTGPG_DELETE_INFO,
                    CategoryProgramMapConstants.RESOURCE_OPERATION_SUCCESS);
		} catch (DomainServiceException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        log.debug("deleteProgramtype end");
        return result;
    }

    /**
     * 添加
     */
    /**
     * @param categoryProgramMap CategoryProgramMap
     * @return String
     */
	public String insertCtgpgmap(CategoryProgramMap categoryProgramMap) {
        log.debug("insertCtgpgmap starting...");
        String msg = "";
		try {
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(categoryProgramMap.getProgramindex());
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if (cmsProgram == null)
            {
                return ResourceMgt.findDefaultText("this.cnt.sync.fail");
            }else if(cmsProgram.getStatus() != 0)
            {
                return "点播内容状态不正确";
            }
            
			Categorycdn categroy = new Categorycdn();
			categroy.setCategoryindex(categoryProgramMap.getCategoryindex());
			categroy = catDS.getCategorycdn(categroy);
			if(categroy == null){
			    return ResourceMgt.findDefaultText("category.not.exsit");
			}
			 
			Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("category_program_map_index");
            String mappingid = String.format("%02d", 26)+ String.format("%030d", mapindex);
            categoryProgramMap.setMapindex(mapindex);
            categoryProgramMap.setMappingid(mappingid);
			categoryProgramMap.setProgramid(cmsProgram.getProgramid());
            categoryProgramMap.setStatus(0); // 待发布
            ctgpgmapDS.insertCategoryProgramMap(categoryProgramMap);
            msg = ResourceMgt.findDefaultText("category.add.success");//"0:添加成功"
            CommonLogUtil.insertOperatorLog(categoryProgramMap.getMapindex().toString(),
                    CategoryProgramMapConstants.MGTTYPE_CTGPGMAP,
                    CategoryProgramMapConstants.OPERTYPE_CTGPG_ADD,
                    CategoryProgramMapConstants.OPERTYPE_CTGPG_ADD_INFO,
                    CategoryProgramMapConstants.RESOURCE_OPERATION_SUCCESS);
			
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(3);
            target.setObjectid(cmsProgram.getProgramid());
            //运营平台存在
            List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(targetSyncList !=null && targetSyncList.size()>0){
                Targetsystem targetSystem = new Targetsystem();
                for(CntTargetSync targetSync:targetSyncList){
                    targetSystem.setTargetindex(targetSync.getTargetindex());
                    targetSystem = targetSystemDS.getTargetsystem(targetSystem);
                    
                    target.setObjecttype(2);
                    target.setObjectid(categoryProgramMap.getCategoryid());
                    target.setTargetindex(targetSystem.getTargetindex());
                    List<CntTargetSync> ctgtargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    //点播内容关联栏目关联平台，前提是栏目已关联该平台
                    if(ctgtargetSyncList !=null && ctgtargetSyncList.size()>0)
                    {
                        CntPlatformSync platform = new CntPlatformSync();
                        platform.setObjecttype(26);
                        platform.setObjectid(mappingid);
                        platform.setPlatform(targetSystem.getPlatform());
                        List<CntPlatformSync> ctgprogmapPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        if(ctgprogmapPlatSyncList !=null && ctgprogmapPlatSyncList.size()>0)  
                        {
                            syncindex = ctgprogmapPlatSyncList.get(0).getSyncindex();
                            platform = ctgprogmapPlatSyncList.get(0);
                            platform.setStatus(0);
                            cntPlatformSyncDS.updateCntPlatformSync(platform);   
                        }else{
                            insertPlatformSyncTask(26,mapindex,mappingid,cmsProgram.getProgramid(),
                                    categroy.getCategoryid(),targetSystem.getPlatform(),0);
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(mappingid + ", " + targetSystem.getTargetid(),CategoryProgramMapConstants.MGTTYPE_CTGPGMAP,
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD), ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_CTGPROGMAP_BIND_ADD_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        
                        insertTargetSyncTask(syncindex,26,mapindex,mappingid,cmsProgram.getProgramid(),
                                categroy.getCategoryid(),targetSystem.getTargetindex(),0,0);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(mappingid + ", " + targetSystem.getTargetid(),CategoryProgramMapConstants.MGTTYPE_CTGPGMAP,
                                ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD), ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD_INFO),
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                }
            }

		} catch (Exception e) {
            log.error("cmsProgramLS exception:" + e.getMessage());
            return ResourceMgt.findDefaultText("category.add.fail");//"1:添加失败"
        }

        log.debug("insertCtgpgmap end");
        return msg;
    }

    /**
     * 按照typeid查询，得到cmsprogramtype
     * 
     * @param mapid String
     * @return CategoryProgramMap
     */
	public CategoryProgramMap getCmsCategory(String mapid) {
        log.debug("getCmsProgramtype start...");
        CategoryProgramMap map = new CategoryProgramMap();
        map.setMapindex(new Long(mapid));
		try {
            map = ctgpgmapDS.getCategoryProgramMap(map);
		} catch (DomainServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.debug("getCmsProgramtype end");
        return map;
    }

    /**
     * @param pgindex String
     * @return CmsProgram
     */
	public CmsProgram getCmsProgram(String pgindex) {
        log.debug("getCmsProgramtype start...");
        CmsProgram pg = new CmsProgram();
        pg.setProgramindex(new Long(pgindex));
		try {
            pg = cmsProgramDS.getCmsProgram(pg);
		} catch (DomainServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.debug("getCmsProgramtype end");
        return pg;
    }

    /**
     * 
     * @return Logger
     */
	public Logger getLog() {
        return log;
    }

    /**
     * 
     * @param log Logger
     */
	public void setLog(Logger log) {
        this.log = log;
    }

    // 收录内容的收录源的获取
    /**
     * @param programindex String
     * @return Map
     */
	public Map<String, String> getpgname(String programindex) {
        Map<String, String> rsMap = new HashMap<String, String>();

		try {

            String sql = "";

            sql = "select programindex ,namecn from cms_program ";

			if (!programindex.equals("")) {
                sql = sql + " where programindex=" + programindex;
            }
            List rtnlist = dbUtil.getQuery(sql);
            Map tmpMap = new HashMap();
			for (int i = 0; i < rtnlist.size(); i++) {
                tmpMap = (Map) rtnlist.get(i);
				rsMap.put((String) tmpMap.get("programindex"), (String) tmpMap
						.get("namecn"));
        }
		} catch (Exception e) {
            e.printStackTrace();
        }
        return rsMap;
    }

    /**
     * @param ctgindex String
     * @return Map
     */
	public Map<String, String> getctgname(String ctgindex) {
        Map<String, String> rsMap = new HashMap<String, String>();

		try {

            String sql = "";

            sql = "select ctg.categoryindex ,ctg.categoryname from categorycdn ctg, category_program_map m ";

			if (!ctgindex.equals("")) {
				sql = sql
						+ "  where ctg.categoryindex=m.categoryindex and m.programindex="
						+ ctgindex;
            }
            List rtnlist = dbUtil.getQuery(sql);
            Map tmpMap = new HashMap();
			for (int i = 0; i < rtnlist.size(); i++) {
                tmpMap = (Map) rtnlist.get(i);
				rsMap.put((String) tmpMap.get("categoryindex"), (String) tmpMap
						.get("categoryname"));
        }
		} catch (Exception e) {
            e.printStackTrace();
        }
        return rsMap;
    }
	
	/**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    /*******publish management*****/
    

    
    
    public TableDataInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            // 格式化内容带有时间的字段
            categoryProgramMap.setCreateStarttime(DateUtil2.get14Time(categoryProgramMap.getCreateStarttime()));
            categoryProgramMap.setCreateEndtime(DateUtil2.get14Time(categoryProgramMap.getCreateEndtime()));
            categoryProgramMap.setOnlinetimeStartDate(DateUtil2.get14Time(categoryProgramMap.getOnlinetimeStartDate()));
            categoryProgramMap.setOnlinetimeEndDate(DateUtil2.get14Time(categoryProgramMap.getOnlinetimeEndDate()));
            categoryProgramMap.setCntofflinestarttime(DateUtil2.get14Time(categoryProgramMap.getCntofflinestarttime()));
            categoryProgramMap.setCntofflineendtime(DateUtil2.get14Time(categoryProgramMap.getCntofflineendtime()));
            

            if(categoryProgramMap.getCategoryid()!=null&&!"".equals(categoryProgramMap.getCategoryid())){
            	categoryProgramMap.setCategoryid(EspecialCharMgt.conversion(categoryProgramMap.getCategoryid()));
            }
            if(categoryProgramMap.getCategoryname()!=null&&!"".equals(categoryProgramMap.getCategoryname())){
            	categoryProgramMap.setCategoryname(EspecialCharMgt.conversion(categoryProgramMap.getCategoryname()));
            }

            dataInfo = ctgpgmapDS.pageInfoQueryctprMap(categoryProgramMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
   
    public TableDataInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");            
            dataInfo = ctgpgmapDS.pageInfoQueryctprTargetMap(categoryProgramMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
    
    public String batchPublishOrDelPublishcntpg(List<CategoryProgramMap> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;

            String message = "";
            String resultStr = "";
    
            if (operType.equals("0"))  //批量发布
            {
                for (CategoryProgramMap categoryProgramMap : list)
                {
                    message = publishCtprMapPlatformSyn(categoryProgramMap.getMapindex().toString(), categoryProgramMap.getProgramid(),categoryProgramMap.getCategoryid(),type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                for (CategoryProgramMap categoryProgramMap : list)
                {
                    message = delPublishCtprMapPlatformSyn(categoryProgramMap.getMapindex().toString(), categoryProgramMap.getProgramid(),categoryProgramMap.getCategoryid(),type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
         
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("CntPgMapLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }

        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
  
    /**
     * 判断获取发布时应填的状态
     * @param categoryProgramMapList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getTaskstatus(List<CategoryProgramMap> categoryProgramMapList) throws Exception
    {
        int tgType = 1 ;
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元
        
        for(CategoryProgramMap targetsysSyn:categoryProgramMapList)
        {
            if (targetsysSyn.getTargettype()==1&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400))
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400))
            {
                hasepg = true;
            }
        }
        
        if (hasbms) //存在bms的情况
        {
            tgType = 1;
        }
        if (!hasbms&&hasepg) //不存在bms,只存在epg的情况
        {
            tgType =2;
        }  
        return tgType;
    }    

    /**
     * 判断获取取消发布时应填的状态
     * @param categoryProgramMapList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getdelTaskstatus(List<CategoryProgramMap> categoryProgramMapList) throws Exception
    {
        int tgType = 2 ;
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(CategoryProgramMap targetsysSyn:categoryProgramMapList)
        {

            if (targetsysSyn.getTargettype()==1&&targetsysSyn.getStatus()==300)
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&targetsysSyn.getStatus()==300)
            {
                hasepg = true;
            }
        }
        if (hasepg) //存在epg的情况
        {
            tgType =2;
        }
        if (!hasepg&&hasbms) //不存在epg,只存在bms的情况
        {
            tgType =1;
        }  
        return tgType;
    }       
    
    //点播内容关联栏目针对平台发布
    public String publishCtprMapPlatformSyn(String mapindex, String programid,String categoryid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
		
    	try
    	{       
    		CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
    		categoryProgramMap.setMapindex(Long.parseLong(mapindex));

    		categoryProgramMap.setProgramid(programid);
    		categoryProgramMap.setCategoryid(categoryid);
			List<CategoryProgramMap> categoryProgramMapList = new ArrayList<CategoryProgramMap>();

			categoryProgramMapList =ctgpgmapDS.getCategoryProgramMapByCond(categoryProgramMap);
			if (categoryProgramMapList == null ||categoryProgramMapList.size()==0)
			{
				return "1:操作失败,点播内容关联栏目记录不存在";
			}
			
    		String initresult="";
    		initresult =initData(categoryProgramMapList.get(0),"platform",type,1);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(26);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            CategoryProgramMap categoryProgramMapObj=new CategoryProgramMap();
            int  platformType = 1;
            
    		Categorycdn categorycdn = new Categorycdn();
    		categorycdn.setCategoryindex(categoryProgramMapList.get(0).getCategoryindex());
    		categorycdn=catDS.getCategorycdn(categorycdn);
    		
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(categoryProgramMap.getMapindex());

    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(categoryProgramMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
            int stat = 1; //待发布
            int tgType = 1;
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
    	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
    	    	int targettype = 0;
                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                tgType= getTaskstatus(categoryProgramMapList);
                if (tgType == 1)
                {
                    for (CategoryProgramMap catgrProMapTmp:categoryProgramMapList )
                    {
                         int targType = catgrProMapTmp.getTargettype();
                         if (targType == 1)
                         {
                             CntTargetSync cntTargetSyncpro = new CntTargetSync();          
                             cntTargetSyncpro.setObjectindex(catgrProMapTmp.getProgramindex());
                             cntTargetSyncpro.setTargetindex(catgrProMapTmp.getTargetindex());
                             cntTargetSyncpro.setObjecttype(3);
                             cntTargetSyncpro = cntTargetSyncDS.getCntTargetSync(cntTargetSyncpro);
                             if (cntTargetSyncpro == null||(cntTargetSyncpro != null && cntTargetSyncpro.getStatus() != 300 && cntTargetSyncpro.getStatus() != 500))
                             {
                                 return "1:操作失败,该点播内容没有发布到"+TARGETSYSTEM
                                         +catgrProMapTmp.getTargetid()+",不能发布";     
                             }
                             
                             CntTargetSync cntTargetSyncCat = new CntTargetSync();          
                             cntTargetSyncCat.setObjectindex(catgrProMapTmp.getCategoryindex());
                             cntTargetSyncCat.setTargetindex(catgrProMapTmp.getTargetindex());
                             cntTargetSyncCat.setObjecttype(2);
                             cntTargetSyncCat = cntTargetSyncDS.getCntTargetSync(cntTargetSyncCat);
                             if (cntTargetSyncCat == null||(cntTargetSyncCat != null && cntTargetSyncCat.getStatus() != 300 && cntTargetSyncCat.getStatus() != 500))
                             {
                                 return "1:操作失败,该栏目没有发布到"+TARGETSYSTEM
                                         +catgrProMapTmp.getTargetid()+",不能发布";  
                             }

                         }

                    }

                }
                
    			for (CategoryProgramMap categoryProgramMapTmp:categoryProgramMapList )
    			{
    			    targettype = categoryProgramMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (categoryProgramMapTmp.getTargetstatus() != 0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                	    		 +categoryProgramMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                +"  ");
                   	    isstatus = false;
    				}
    				if (isstatus)
    				{
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    	            synType = 1;
    	    		boolean isxml =true ;
    				boolean ispub = true;
    				cntTargetSync.setObjectindex(categoryProgramMapTmp.getProgramindex());
    				cntTargetSync.setTargetindex(categoryProgramMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				//点播内容在该平台已发布
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
               	    		 +categoryProgramMapTmp.getTargetid()+"  "
                               +"  ");    	
                	    ispub = false;
                	}
    				if (ispub)
    				{
        	            CntTargetSync cntTargetSynctemp = new CntTargetSync();    		

        	            cntTargetSynctemp.setObjectindex(categoryProgramMapTmp.getCategoryindex());
        	            cntTargetSynctemp.setTargetindex(categoryProgramMapTmp.getTargetindex());
        	            cntTargetSynctemp.setObjecttype(2);
        	            cntTargetSynctemp = cntTargetSyncDS.getCntTargetSync(cntTargetSynctemp);
        				//栏目在该平台已发布
        				if (cntTargetSynctemp == null||(cntTargetSynctemp != null && cntTargetSynctemp.getStatus() != 300 && cntTargetSynctemp.getStatus() != 500))
        				{
                    	    resultPubStr.append("该栏目没有发布到"+TARGETSYSTEM
                   	    		 +categoryProgramMapTmp.getTargetid()
                                   +"  ");    	
                    	    ispub = false;
                    	}    					
    				}
    				if (ispub)
    				{
                        int synTargetType =1;
                        if (categoryProgramMapTmp.getStatus()== 500) //修改发布    
                        {
                    		synTargetType =2;
            	            synType = 2;

                        }
                       	if ((targettype ==1|| targettype ==2)
                       			&&(categoryProgramMapTmp.getStatus()==0||categoryProgramMapTmp.getStatus()==400
                       			||categoryProgramMapTmp.getStatus()== 500))  //BMS或EPG系统
                       	{
                       		String xml;
                       		if (synTargetType==1)
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 1); //新增发布到BMS
                                if (null == xml)
                                {
                                    isxml = false;
                                    resultPubStr.append(PUBTARGETSYSTEM
                                             +categoryProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                } 
                       		}
                       		else
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 2); //修改发布到BMS
                                if (null == xml)
                                {
                                    isxml = false;
                                    resultPubStr.append(PUBTARGETSYSTEM
                                             +categoryProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		if (isxml)
                       		{
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                Long targetindex =categoryProgramMapTmp.getTargetindex();
     
                                //Mapping 对象日志记录
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = categoryProgramMapTmp.getMappingid();
                                String elementid = categoryProgramMapTmp.getProgramid();
                                String  parentid = categoryProgramMapTmp.getCategoryid();
                                Long objectindex = categoryProgramMapTmp.getMapindex();
                                insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                                if (targettype == tgType)
                                {
                                    stat = 1;
                                }
                                else
                                {
                                    stat = 0;
                                }
                                insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务

                                cntTargetSyncTemp.setSyncindex(categoryProgramMapTmp.getSyncindex());
                                if (synTargetType ==1)
                                {
                                	cntTargetSyncTemp.setOperresult(10);
                                }
                                else
                                {
                                	cntTargetSyncTemp.setOperresult(40);
                                }                            
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                CommonLogUtil.insertOperatorLog(categoryProgramMapTmp.getMappingid()+","+
                                		categoryProgramMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                        CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                sucessPub=true;
                                resultPubStr.append(PUBTARGETSYSTEM
                        	    		+categoryProgramMapTmp.getTargetid()+": "
                                        +operSucess+"  ");
                       		}
                         }
                      }                       		
    			   } 
    		   }
    	    }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
       	    	   categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
       	    	   if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
       	    	int targettype = 0;
       			for (CategoryProgramMap categoryProgramMapTmp:categoryProgramMapList )
       			{
                    targettype = categoryProgramMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (categoryProgramMapTmp.getTargetstatus() != 0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                	    		 +categoryProgramMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                +"  ");
                   	    isstatus = false;
    				}
    				if (isstatus)
    				{
    	            synType = 1;

       	            CntTargetSync cntTargetSync = new CntTargetSync();    		
       				boolean ispub = true;
       				boolean isxml =true ;
       				cntTargetSync.setObjectindex(categoryProgramMapTmp.getProgramindex());
       				cntTargetSync.setTargetindex(categoryProgramMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(3);

       				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
       				//点播内容在该平台已发布
       				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
       				{
                   	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
                  	    		 +categoryProgramMapTmp.getTargetid()
                                  +"  ");    	
                   	    ispub = false;

                   	}
       				if (ispub)
       				{
        	            CntTargetSync cntTargetSynctemp = new CntTargetSync();    		

        	            cntTargetSynctemp.setObjectindex(categoryProgramMapTmp.getCategoryindex());
        	            cntTargetSynctemp.setTargetindex(categoryProgramMapTmp.getTargetindex());
        	            cntTargetSynctemp.setObjecttype(2);

        	            cntTargetSynctemp = cntTargetSyncDS.getCntTargetSync(cntTargetSynctemp);
           				//栏目在该平台已发布
           				if (cntTargetSynctemp == null||(cntTargetSynctemp != null && cntTargetSynctemp.getStatus() != 300 && cntTargetSynctemp.getStatus() != 500))
           				{
                       	    resultPubStr.append("该栏目没有发布到"+TARGETSYSTEM
                      	    		 +categoryProgramMapTmp.getTargetid()
                                      +"  ");    	
                       	    ispub = false;
                       	}
       					
       				}
       				if (ispub)
       				{
                           int synTargetType =1;
                            if (categoryProgramMapTmp.getStatus()== 500) //修改发布    
                            {
                        		synTargetType =2;
             	                synType = 2;

                            }
                         	if ((categoryProgramMapTmp.getStatus()==0||
                         			categoryProgramMapTmp.getStatus()==400||categoryProgramMapTmp.getStatus()== 500))  //2.0平台网元
                         	{
                         		String xml;
                         		if (synTargetType==1)
                         		{
                                   xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 1); //新增发布
                                   if (null == xml)
                                   {
                                   	isxml = false;
                                  	    resultPubStr.append(PUBTARGETSYSTEM
                                	    		 +categoryProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                +"  ");
                                   }
                          		}
                          		else
                          		{
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 2); //修改发布
                                    if (null == xml)
                                    {
                                    	isxml = false;
                                   	    resultPubStr.append(PUBTARGETSYSTEM
                                 	    		 +categoryProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                 +"  ");
                                    }
                         		}
                         		if (isxml)
                         		{
                                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =categoryProgramMapTmp.getTargetindex();
       
                                    //Mapping 对象日志记录
                                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                    String objectidMapping = categoryProgramMapTmp.getMappingid();
                                    String elementid = categoryProgramMapTmp.getProgramid();
                                    String  parentid = categoryProgramMapTmp.getCategoryid();
                                    Long objectindex = categoryProgramMapTmp.getMapindex();
                                    String elementcode = cmsProgram.getCpcontentid();
                                    String parentcode = categorycdn.getCatagorycode();
                                    insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                    insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务

                                    cntTargetSyncTemp.setSyncindex(categoryProgramMapTmp.getSyncindex());
                                    if (synTargetType ==1)
                                    {
                                    	cntTargetSyncTemp.setOperresult(10);

                                   }
                                   else
                                   {
                                   	cntTargetSyncTemp.setOperresult(40);
                                   }                            
                                   cntTargetSyncTemp.setStatus(200);
                                   cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                   CommonLogUtil.insertOperatorLog(categoryProgramMapTmp.getMappingid()+","+
                                   		categoryProgramMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                           CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_INFO,
                                           CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                   sucessPub=true;
                                   resultPubStr.append(PUBTARGETSYSTEM
                           	    		+categoryProgramMapTmp.getTargetid()+": "
                                           +operSucess+"  ");
                         		}
                             }
                         }                          		
       				} 
    	       }
    	       }
               // 更新平台发布状态       
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }

    	}
        catch (Exception e)
        {
            throw e;
        }
    	
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();

    	return rtnPubStr;   
    	}   
   
    //点播内容关联栏目针对平台取消发布
    public String delPublishCtprMapPlatformSyn(String mapindex, String programid,String categoryid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
    		categoryProgramMap.setMapindex(Long.parseLong(mapindex));

    		categoryProgramMap.setProgramid(programid);
    		categoryProgramMap.setCategoryid(categoryid);
			List<CategoryProgramMap> categoryProgramMapList = new ArrayList<CategoryProgramMap>();

			categoryProgramMapList =ctgpgmapDS.getCategoryProgramMapByCond(categoryProgramMap);
			if (categoryProgramMapList == null ||categoryProgramMapList.size()==0)
			{
				return "1:操作失败,点播内容关联栏目记录不存在";
			}
			
    		String initresult="";
    		initresult =initData(categoryProgramMapList.get(0),"platform",type,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(26);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            CategoryProgramMap categoryProgramMapObj=new CategoryProgramMap();
            int  platformType = 1;
    		Categorycdn categorycdn = new Categorycdn();
    		categorycdn.setCategoryindex(categoryProgramMapList.get(0).getCategoryindex());
    		categorycdn=catDS.getCategorycdn(categorycdn);
    		
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(categoryProgramMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(categoryProgramMap.getMapindex());
            int stat = 1; //待发布
            int tgType = 2;
    	    if (type == 1)
    	    {

    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
    	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
    	    	
    	    	int targettype = 0; 
                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));

    			for (CategoryProgramMap categoryProgramMapTmp:categoryProgramMapList )
    			{
    			    targettype = categoryProgramMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (categoryProgramMapTmp.getTargetstatus() != 0)
    				{
                	    resultPubStr.append(DELPUBTARGETSYSTEM
              	    	+categoryProgramMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                        +"  "); 
                   	    isstatus = false;
    				}
    				if (isstatus)
    				{
    				boolean isxml = true;
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
                        //发布成功和修改发布失败
                       if (categoryProgramMapTmp.getStatus()==300||categoryProgramMapTmp.getStatus()==500)
                       {
                          	if (targettype ==1|| targettype ==2)  //BMS或EPG系统
                           	{
                           		String xml;
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 3); //取消发布
                                if (null == xml)
                                {
                                    isxml = false;
                                    resultPubStr.append(DELPUBTARGETSYSTEM
                                    +categoryProgramMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                    +"  ");                               
                                }   
                       			if (isxml)
                       			{
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =categoryProgramMapTmp.getTargetindex();
         
                                    //Mapping 对象日志记录
                                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                    String objectidMapping = categoryProgramMapTmp.getMappingid();
                                    String elementid = categoryProgramMapTmp.getProgramid();
                                    String  parentid = categoryProgramMapTmp.getCategoryid();
                                    Long objectindex = categoryProgramMapTmp.getMapindex();
                                    insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                                    if (targettype == tgType)
                                    {
                                        stat = 1;
                                    }
                                    else
                                    {
                                        stat = 0;
                                    }
                                    insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务

                                    cntTargetSyncTemp.setSyncindex(categoryProgramMapTmp.getSyncindex());

                                    cntTargetSyncTemp.setOperresult(70);
                                  
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(categoryProgramMapTmp.getMappingid()+","+
                                       		categoryProgramMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                            CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                    sucessPub=true;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                                  	+categoryProgramMapTmp.getTargetid()+delPub+": "
                                    +operSucess+"  ");   
                       			}
                            }
                       }
                    }
    			 }
    			}
    	       
    	       if (type == 2)
    	       {
       			cntPlatformSync.setPlatform(1);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
    	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
    	    	
    	    	int targettype;
    			for (CategoryProgramMap categoryProgramMapTmp:categoryProgramMapList )
    			{
                    targettype = categoryProgramMapTmp.getTargettype();

    				boolean isstatus = true;
    				if (categoryProgramMapTmp.getTargetstatus() != 0&&(categoryProgramMapTmp.getTargettype() ==0)
    						&&(categoryProgramMapTmp.getStatus()==300||categoryProgramMapTmp.getStatus()==500))
    				{
                	    resultPubStr.append(DELPUBTARGETSYSTEM
              	    	+categoryProgramMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                        +"  "); 
                   	    isstatus = false;
    				}
    				if (isstatus)
    			   {
    				boolean isxml = true;
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
                        //发布成功和修改发布失败
                       if (categoryProgramMapTmp.getStatus()==300||categoryProgramMapTmp.getStatus()==500)
                       {

                           		String xml;

                                xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 3); //新增发布
                                if (null == xml)
                                {
                                    isxml = false;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                          	    	+categoryProgramMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                    +"  ");                               
                            	}  
                                if (isxml)
                                {
                                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                 
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                    Long targetindex =categoryProgramMapTmp.getTargetindex();
         
                                    //Mapping 对象日志记录
                                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                    String objectidMapping = categoryProgramMapTmp.getMappingid();
                                    String elementid = categoryProgramMapTmp.getProgramid();
                                    String  parentid = categoryProgramMapTmp.getCategoryid();
                                    Long objectindex = categoryProgramMapTmp.getMapindex();
                                    String elementcode = cmsProgram.getCpcontentid();
                                    String parentcode = categorycdn.getCatagorycode();
                                    insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                    insertSyncTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                                    cntTargetSyncTemp.setSyncindex(categoryProgramMapTmp.getSyncindex());
                                    cntTargetSyncTemp.setOperresult(70);
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(categoryProgramMapTmp.getMappingid()+","+
                                       		categoryProgramMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                            CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                    sucessPub=true;
                            	    resultPubStr.append(DELPUBTARGETSYSTEM
                                  	+categoryProgramMapTmp.getTargetid()+delPub+": "
                                    +operSucess+"  "); 
                                }
                            }
                       }
                    }  
                       		 
    	       }
               // 更新平台发布状态     
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp); 
    	       }

    	}
        catch (Exception e)
        {
            throw e;
        }
    	
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();

    	return rtnPubStr;   
    	}   

    //针对网元发布
    public String publishcatgrProTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
    		categoryProgramMap.setMapindex(Long.parseLong(mapindex));

    		categoryProgramMap.setTargetindex(Long.parseLong(targetindex));
    		categoryProgramMap.setObjecttype(26);
			List<CategoryProgramMap> categoryProgramMapList = new ArrayList<CategoryProgramMap>();

			categoryProgramMapList =ctgpgmapDS.getCategoryProgramMapByCond(categoryProgramMap);
			if (categoryProgramMapList == null ||categoryProgramMapList.size()==0)
			{
				return "1:操作失败,点播内容关联栏目记录不存在";
			}
    		int synType = 1;
            String initresult="";

    		initresult =initData(categoryProgramMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(categoryProgramMap.getMapindex());
			cntPlatformSync.setObjecttype(26);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            CategoryProgramMap categoryProgramMapObj=new CategoryProgramMap();
            int  platformType = 1;

    		Categorycdn categorycdn = new Categorycdn();
    		categorycdn.setCategoryindex(categoryProgramMapList.get(0).getCategoryindex());
    		categorycdn=catDS.getCategorycdn(categorycdn);
    		
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(categoryProgramMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
            int stat = 1; //待发布
    		
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			categoryProgramMapObj.setTargetindex(Long.parseLong(targetindex));
    	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
    	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (categoryProgramMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	int targettype = categoryProgramMapList.get(0).getTargettype();
    	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();

				cntTargetSync.setObjectindex(categoryProgramMapList.get(0).getProgramindex());
				cntTargetSync.setTargetindex(categoryProgramMapList.get(0).getTargetindex());
				cntTargetSync.setObjecttype(3);
				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
				//点播内容在该平台已发布
				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
				{
                    return "1:操作失败,该点播内容没有发布到该网元";

            	}
	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();

	            cntTargetSyncTmp.setObjectindex(categoryProgramMapList.get(0).getCategoryindex());
	            cntTargetSyncTmp.setTargetindex(categoryProgramMapList.get(0).getTargetindex());
	            cntTargetSyncTmp.setObjecttype(2);
	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
				//栏目在该平台已发布
				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
				{
                    return "1:操作失败,该栏目没有发布到该网元";

            	}
				
                int synTargetType =1;
                if (categoryProgramMapList.get(0).getStatus()== 500) //修改发布    
                {
            		synTargetType =2;
            		synType = 2;

                }
               	if (targettype ==1|| targettype ==2)  //BMS或EPG系统
               	{
               		String xml;
               		if (synTargetType==1)
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 1); //新增发布到BMS
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }

               		}
               		else
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 2); //修改发布到BMS
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}    					

                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = categoryProgramMapList.get(0).getMappingid();
                    String elementid = categoryProgramMapList.get(0).getProgramid();
                    String  parentid = categoryProgramMapList.get(0).getCategoryid();
                    Long objectindex = categoryProgramMapList.get(0).getMapindex();
                    insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                                     
                    insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex),batchid,stat); //插入同步任务
                    cntTargetSyncTemp.setSyncindex(categoryProgramMapList.get(0).getSyncindex());
                    if (synTargetType ==1)
                    {
                    	cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                    	cntTargetSyncTemp.setOperresult(40);
                    }                            
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(categoryProgramMapList.get(0).getMappingid()+","+
                    categoryProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     


                     }
               	}
    	       
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			categoryProgramMapObj.setTargetindex(Long.parseLong(targetindex));

        	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
        	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	if (categoryProgramMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
                    int targettype = categoryProgramMapList.get(0).getTargettype();

        	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
                    CntTargetSync cntTargetSync = new CntTargetSync();

    				cntTargetSync.setObjectindex(categoryProgramMapList.get(0).getProgramindex());
    				cntTargetSync.setTargetindex(categoryProgramMapList.get(0).getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				//点播内容在该平台已发布
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                        return "1:操作失败,该点播内容没有发布到该网元";

                	}
    	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();

    	            cntTargetSyncTmp.setObjectindex(categoryProgramMapList.get(0).getCategoryindex());
    	            cntTargetSyncTmp.setTargetindex(categoryProgramMapList.get(0).getTargetindex());
    	            cntTargetSyncTmp.setObjecttype(2);
    	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
    				//栏目在该平台已发布
    				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
    				{
                        return "1:操作失败,该栏目没有发布到该网元";
                	}
    				
                    int synTargetType =1;
                    if (categoryProgramMapList.get(0).getStatus()== 500) //修改发布    
                    {
                		synTargetType =2;
                    }

                   		String xml;
               			if (synTargetType ==1)
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 1); //新增发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }
               			}
               			else
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 2); //修改发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }
               			}

                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = categoryProgramMapList.get(0).getMappingid();
                        String elementid = categoryProgramMapList.get(0).getProgramid();
                        String  parentid = categoryProgramMapList.get(0).getCategoryid();
                        Long objectindex = categoryProgramMapList.get(0).getMapindex();
                        String elementcode = cmsProgram.getCpcontentid();
                        String parentcode = categorycdn.getCatagorycode();
                        
                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, 
                        		elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                                                            
                        insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex),batchid,stat); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(categoryProgramMapList.get(0).getSyncindex());
                        if (synTargetType ==1)
                        {
                        	cntTargetSyncTemp.setOperresult(10);
                        }
                        else
                        {
                        	cntTargetSyncTemp.setOperresult(40);
                        }                            
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(categoryProgramMapList.get(0).getMappingid()+","+
                        categoryProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     

                }

               // 更新平台发布状态         
   			List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();

    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(categoryProgramMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	

    	return result; 
    }
    
    //针对网元取消发布
    public String delpublishcatgrProTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
    		categoryProgramMap.setMapindex(Long.parseLong(mapindex));

    		categoryProgramMap.setTargetindex(Long.parseLong(targetindex));
    		categoryProgramMap.setObjecttype(26);
			List<CategoryProgramMap> categoryProgramMapList = new ArrayList<CategoryProgramMap>();

			categoryProgramMapList =ctgpgmapDS.getCategoryProgramMapByCond(categoryProgramMap);
			if (categoryProgramMapList == null ||categoryProgramMapList.size()==0)
			{
				return "1:操作失败,点播内容关联栏目记录不存在";
			}
    		int synType = 3;
            String initresult="";

    		initresult =initData(categoryProgramMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(categoryProgramMap.getMapindex());
			cntPlatformSync.setObjecttype(26);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            CategoryProgramMap categoryProgramMapObj=new CategoryProgramMap();
            int  platformType = 1;
    		Categorycdn categorycdn = new Categorycdn();
    		categorycdn.setCategoryindex(categoryProgramMapList.get(0).getCategoryindex());
    		categorycdn=catDS.getCategorycdn(categorycdn);
    		
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(categoryProgramMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
            int stat = 1; //待发布
    		
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			categoryProgramMapObj.setTargetindex(Long.parseLong(targetindex));
    	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
    	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (categoryProgramMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	int targettype = categoryProgramMapList.get(0).getTargettype();
    	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);
				
               	if (targettype ==1|| targettype ==2)  //BMS或EPG系统
               	{
               		String xml;
                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(cntgrmap, targettype, 26, 3); //取消发布
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }	
				
                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = categoryProgramMapList.get(0).getMappingid();
                    String elementid = categoryProgramMapList.get(0).getProgramid();
                    String  parentid = categoryProgramMapList.get(0).getCategoryid();
                    Long objectindex = categoryProgramMapList.get(0).getMapindex();
                    insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex),batchid,stat); //插入同步任务

                    cntTargetSyncTemp.setSyncindex(categoryProgramMapList.get(0).getSyncindex());
                	cntTargetSyncTemp.setOperresult(70);               
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(categoryProgramMapList.get(0).getMappingid()+","+
                    categoryProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                    CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);  
                   
                    }
               }    	       
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			categoryProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			categoryProgramMapObj.setTargetindex(Long.parseLong(targetindex));
        	    	categoryProgramMapList = ctgpgmapDS.getnormaltargetSynListByCond(categoryProgramMapObj);
        	    	if (categoryProgramMapList == null || categoryProgramMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	if (categoryProgramMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
                    int targettype = categoryProgramMapList.get(0).getTargettype();

        	    	cntgrmap.put("categoryprogramMap", categoryProgramMapList);

                   		String  xml = StandardImpFactory.getInstance().create(1).createXmlFile(cntgrmap, targettype, 26, 3); //取消发布
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = categoryProgramMapList.get(0).getMappingid();
                        String elementid = categoryProgramMapList.get(0).getProgramid();
                        String  parentid = categoryProgramMapList.get(0).getCategoryid();
                        Long objectindex = categoryProgramMapList.get(0).getMapindex();
                        String elementcode = cmsProgram.getCpcontentid();
                        String parentcode = categorycdn.getCatagorycode();
                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                 
                        insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex),batchid,stat); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(categoryProgramMapList.get(0).getSyncindex());

                        cntTargetSyncTemp.setOperresult(70);
                        
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(categoryProgramMapList.get(0).getMappingid()+","+categoryProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                }

               // 更新平台发布状态         
      			List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();

     	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
     	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
     	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(categoryProgramMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }   	
    	return result; 
    }
          
    public String initData(CategoryProgramMap categoryProgramMap,String targetindex,int platfType,int synType)throws Exception
    {
    	try
    	{
    		CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(categoryProgramMap.getMapindex());
			cntPlatformSync.setObjecttype(26);
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(categoryProgramMap.getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		if (cmsProgram == null)
    		{
                return "1:操作失败,内容不存在";
    		}
    		
    		Categorycdn categorycdn = new Categorycdn();
    		categorycdn.setCategoryindex(categoryProgramMap.getCategoryindex());
    		categorycdn=catDS.getCategorycdn(categorycdn);
    		if (categorycdn == null)
    		{
                return "1:操作失败,栏目不存在";
    		}
    		
    		if(platfType == 1 ||platfType == 2) //发布到3.0平台 或者2.0平台
    		{
        		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();

        		if (platfType == 1)
        		{
        			cntPlatformSync.setPlatform(2);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        			
        		}

        		if (platfType == 2)
        		{
        			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
        		
    			if (synType == 1) //发布
    			{
    				//状态为待发布、发布失败、修改发布失败才能发布
    				if(cntPlatformSynctmp.getStatus()!=0&&cntPlatformSynctmp.getStatus()!=400&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    				
    				
    			}
    			else  //取消发布
    			{   
    				//状态为发布成功、修改发布失败才能取消发布
    				if(cntPlatformSynctmp.getStatus()!=300&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    			
                /**
                 * 获取关联目标网元及状态
                 * */
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null||cntTargetSyncList.size()==0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType ==1)
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==0||cntTargetSync.getStatus()==400||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";

                	}
                }
                else
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==300||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";

                	}
                }                
    		}

    		if (platfType == 3) //发布到网元
    		{
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
    			CntTargetSync cntTargetSync = new CntTargetSync();
    			cntTargetSync.setObjectindex(categoryProgramMap.getMapindex());
    			cntTargetSync.setObjecttype(26);
    			cntTargetSync.setTargetindex(Long.valueOf(targetindex));
    			cntTargetSyncListTemp =cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
    			if (cntTargetSyncListTemp==null ||cntTargetSyncListTemp.size()==0)
    			{
                    return "1:操作失败,发布记录不存";
    			}
    			
    			if (synType == 1) //发布
    			{
    				//状态为待发布、发布失败、修改发布失败才能发布
    				if(cntTargetSyncListTemp.get(0).getStatus()!=0&&cntTargetSyncListTemp.get(0).getStatus()!=400&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				//状态为发布成功、修改发布失败才能取消发布
    				if(cntTargetSyncListTemp.get(0).getStatus()!=300&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    		}
            List<UsysConfig> usysConfigList = null;
            UsysConfig usysConfig = new UsysConfig();
            usysConfig = usysConfigDS.getUsysConfigByCfgkey(FTPADDRESS);
            if (null == usysConfig || null == usysConfig.getCfgvalue()
                || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return "1:操作失败,获取同步内容实体文件FTP地址失败";
            } 
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	
    	return "success";
    }

    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid,int status)
    {
    	CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(status);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    
    private void insertObjectMappingRecord(Long syncindexMapping, Long index,Long objectindex, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindex);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(26);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    } 
 
    private void insertObjectMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindex, String objectidMapping, 
    		String elementid, String parentid,String elementcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindex);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(26);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elementcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
    
    /*******publish management end*****/   
}

package com.zte.cms.cpsp.ls;

import java.util.List;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;

public interface ICmsLpopNoauditLS
{

    /**
     * 查询牌照方
     * 
     * @param ucpCmsLpopNoaudit
     * @param start
     * @param pagesize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoQuery(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
            throws DomainServiceException;

    /**
     * 查询运营商
     * 
     * @param ucpCmsLpopNoaudit
     * @param start
     * @param pagesize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoOperatorQuery(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize)
            throws DomainServiceException;

    /**
     * 新增牌照方免审CP
     * 
     * @param cmsLpopNoauditList
     * @return
     * @throws DomainServiceException
     */
    public String insertCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 新增运营商免审ＣＰ
     * 
     * @param cmsLpopNoauditList
     * @return
     * @throws DomainServiceException
     */
    public String insertOperatorCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 删除免审CP
     * 
     * @param cmsLpopNoauditList
     * @return
     * @throws DomainServiceException
     */
    public String deleteCmsLpopNoaudit(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;
    
    
    /**
     * 查询各种免审直通设置
     * 
     * @param CmsLpopNoaudit
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo pageInfoEachSetQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pagesize)throws DomainServiceException;
    
    
    /**
     * 查询牌照方
     * 
     * @param ucpCmsLpopNoaudit
     * @return
     * @throws DomainServiceException
     */
    public List<UcpCmsLpopNoaudit> getCmsLpNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 查询CP信息 在牌照方设置免审时以List形式展示 无数据时给出提示
     */
    public List<UcpCmsLpopNoaudit> noCmsLpNoauditListTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize);

    /**
     * 查询CP信息 在牌照方设置免审时以table形式展示 无数据时给出提示
     */
    public TableDataInfo getCmsLPNoauditListTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize);

    /**
     * 查询CP信息 在牌照方设置免审时以table形式展示 无数据时给出提示
     */
    public TableDataInfo noCmsLpNoauditListTripsTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pagesize);

    /**
     * 查询运营商
     * 
     * @param ucpCmsLpopNoaudit
     * @return
     * @throws DomainServiceException
     */
    public List<UcpCmsLpopNoaudit> getCmsOpNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 查询CP信息 在运营商设置免审时以List形式展示 无数据时给出提示
     */
    public List<UcpCmsLpopNoaudit> noOpNoauditListTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize);

    /**
     * 查询CP信息 在运营商设置免审时以table形式展示
     */
    public TableDataInfo getCmsOpNoauditListTable(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize);

    /**
     * 查询CP信息 在运营商设置免审时以table形式展示 无数据时给出提示
     */
    public TableDataInfo noOpNoauditListTableTrips(UcpCmsLpopNoaudit ucpCmsLpopNoaudit, int start, int pageSize);
    /**
     * 免一审设置
     */
    
    public String saveLpopNoauditByCond(List<CmsLpopNoaudit> cmsLpopNoauditList);
    
    /**
     * 获得当前登录操作员
     * 
     * @return
     */
    public String getOperidFromSession(String param);
    
    /**
	 * 免一审设置 lfh 20211006
	 */
    public String saveLpopFirNoaudit(List<UcpBasic> cmsLpopNoFirstauditList)throws DomainServiceException;
    //public String saveLpopNoaudit(List<String> cmsLpopNoFirstauditList)throws DomainServiceException;
    /**
	 * 免二审设置 lfh 20211006
	 */
    public String saveSecondNoaudit(List<UcpBasic> cmsLpopNoSecNoauditList)throws DomainServiceException;
    /**
	 * 免审设置 lfh 20211012
	 */
    public String saveLpopNoaudit(List<UcpBasic> cmsLpopNoauditList)throws DomainServiceException;
    /**
	 * 直通设置 lfh 20211012
	 */
    public String saveDirsyncCp(List<UcpBasic> cmsLpopNoauditList)throws DomainServiceException;

    /**
     * 免审直通设置取消  lfh20120930
     * @throws DomainServiceException 
     */
    public String removeNoauditByCond(List<CmsLpopNoaudit> cmsLpopNoauditCpidList)throws DomainServiceException;
   /**
    * ucp_baisc表cp查询，供免审直通设置用 lfh
    */
   public TableDataInfo pageInfoQueryUcpbasicFir(UcpBasic ucpBasic, int arg1, int arg2) throws DomainServiceException;
   public TableDataInfo pageInfoQueryUcpbasicSec(UcpBasic ucpBasic, int arg1, int arg2) throws DomainServiceException;
   public TableDataInfo pageInfoQueryUcpbasicAll(UcpBasic ucpBasic, int arg1, int arg2) throws DomainServiceException;
   public TableDataInfo pageInfoQueryUcpbasicDir(UcpBasic ucpBasic, int arg1, int arg2) throws DomainServiceException;
  // public TableDataInfo pageInfoQueryUcpbasicCp(UcpBasic ucpBasic,String type,int arg1, int arg2)throws DomainServiceException;
  // public TableDataInfo pageInfoQueryUcpbasicCp(UcpBasic ucpBasic, int arg1, int arg2) throws DomainServiceException ;

}

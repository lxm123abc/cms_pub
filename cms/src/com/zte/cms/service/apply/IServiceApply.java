package com.zte.cms.service.apply;

import com.zte.cms.service.model.Service;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceApply
{

    /**
     * 工作流申请
     * 
     * @param serviceindex
     * @return
     * @throws DomainServiceException
     */
    public abstract String apply(Long serviceindex) throws DomainServiceException;

    public abstract String apply(Service service, String templateRoute, String priority) throws DomainServiceException;

    /**
     * 工作流审核
     * 
     * @param service
     * @param taskId
     * @param handleMsg
     * @param handleEvent
     * @param workflowStatusCode
     * @return
     * @throws DomainServiceException
     */
    public abstract String audit(Service service, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException;

}
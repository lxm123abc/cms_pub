package com.zte.cms.service.categoryservicemap.service;

import java.util.List;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategoryServiceMapDS
{
    /**
	 * 新增CategoryServiceMap对象
	 *
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException;
	  
    /**
	 * 更新CategoryServiceMap对象
	 *
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException;

    /**
	 * 批量更新CategoryServiceMap对象
	 *
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCategoryServiceMapList( List<CategoryServiceMap> categoryServiceMapList )throws DomainServiceException;



	/**
	 * 删除CategoryServiceMap对象
	 *
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException;

	/**
	 * 批量删除CategoryServiceMap对象
	 *
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCategoryServiceMapList( List<CategoryServiceMap> categoryServiceMapList )throws DomainServiceException;




	/**
	 * 查询CategoryServiceMap对象
	 
	 * @param categoryServiceMap CategoryServiceMap对象
	 * @return CategoryServiceMap对象
	 * @throws DomainServiceException ds异常 
	 */
	 public CategoryServiceMap getCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询CategoryServiceMap对象 
	  * 
	  * @param categoryServiceMap CategoryServiceMap对象
	  * @return 满足条件的CategoryServiceMap对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<CategoryServiceMap> getCategoryServiceMapByCond( CategoryServiceMap categoryServiceMap )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CategoryServiceMap对象 
	  *
	  * @param categoryServiceMap CategoryServiceMap对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CategoryServiceMap对象 
	  *
	  * @param categoryServiceMap CategoryServiceMap对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
}

package com.zte.cms.seriespromap.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.series.common.SeriesConfig;
import com.zte.cms.content.series.ls.ICmsSeriesLS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.seriespromap.common.SeriesProgramMapConfig;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.cms.seriespromap.service.ISeriesProgramMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class SeriesProgramMapLS extends DynamicObjectBaseDS implements ISeriesProgramMapLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private ISeriesProgramMapDS seriesProgramMapDS;
    private ICmsProgramDS cmsProgramDS;
    private ICmsSeriesDS cmsSeriesDS;
    private IUsysConfigDS usysConfigDS;
    private ITargetsystemDS targetSystemDS;
    private ICntSyncTaskDS ntsyncTaskDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ICntSyncRecordDS cntSyncRecordDS;
	private ICntPlatformSyncDS cntPlatformSyncDS;
	private ICntTargetSyncDS cntTargetSyncDS;
	private IObjectSyncRecordDS objectSyncRecordDS;
	private ICntSyncTaskDS cntSyncTaskDS;
    public final static String CNTSYNCXML = "xmlsync";
    public final static String SERIES_PROM_DIR = "seriesprogram";
    public final static int OBJECTTYPE = 36;
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    public static final String BATCHID = "ucdn_task_batch_id";// 任务批次号
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
	private Map sereispromap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress="";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private String result = "0:操作成功";
    // Action 类型
    public static final int TASKTYPE_REGIST = 1;
    public static final int TASKTYPE_UPDATE = 2;
    public static final int TASKTYPE_DELETE = 3;

    // 目标系统
    public static final int DESTTYPE_BMS = 1;
    public static final int DESTTYPE_EPG = 2;
    public static final int DESTTYPE_CDN = 3;

    public String removeSeriesProgramMapByIndex(long mapindex) throws Exception
    {
        log.debug("removeSeriesProgramMapByIndex starting...");
        String rtn = "0";
        try
        {
            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setMapindex(mapindex);
            seriesProgramMap = seriesProgramMapDS.getSeriesProgramMap(seriesProgramMap);
            if (seriesProgramMap == null || seriesProgramMap.getMapindex() == null)
            {
                rtn = "100"; // 该关联关系已经被删除
            }else
            {
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectid(seriesProgramMap.getMappingid());
                cntTargetSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                boolean isPublished = false;
                if(null != cntTargetSyncList && cntTargetSyncList.size() > 0)
                {
                    
                    for (CntTargetSync targetSync:cntTargetSyncList)
                    {
                        if (StatusTranslator.STATUS_PUBLISHING == targetSync.getStatus() || StatusTranslator.STATUS_PUBLISHED == targetSync.getStatus()||StatusTranslator.STATUS_MODIFYPUBFAILED == targetSync.getStatus())
                        {
                            isPublished = true;
                            break;
                        }
                    }
                    
                    if (!isPublished)
                    {
                        for (int i = 0; i < cntTargetSyncList.size(); i++)
                        {
                            cntTargetSync = cntTargetSyncList.get(i);
                            cntTargetSyncDS.removeCntTargetSync(cntTargetSync);

                            // 获取平台发布信息
                            CntPlatformSync cntPlatformSync = new CntPlatformSync();
                            cntPlatformSync.setObjectid(cntTargetSync.getObjectid());
                            cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                            cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                            cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);

                            // 查询子网元发布信息
                            CntTargetSync childTargetSync = new CntTargetSync();
                            childTargetSync.setObjectid(cntTargetSync.getObjectid());
                            childTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                            childTargetSync.setObjecttype(cntTargetSync.getObjecttype());
                            List<CntTargetSync> childTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(childTargetSync);

                            if (childTargetSyncList.size() < 1)
                            {
                                // 删除主平台关联发布信息
                                cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                                // 更新电视剧与其他对象的mapping是否有关联平台发布，没有就删除它们之间的mapping关系（暂不实现）
                                
                            }
                            else
                            {
                                // 更改主平台关联发布状态
                                int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                                for (int j = 0; j < childTargetSyncList.size(); j++)
                                {
                                    childTargetSyncStatus[j] = childTargetSyncList.get(j).getStatus();
                                }
                                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }
                }
                
                if (!isPublished)
                {
                    seriesProgramMapDS.removeSeriesProgramMap(seriesProgramMap);
                    CommonLogUtil.insertOperatorLog(String.valueOf(seriesProgramMap.getMapindex()),
                            SeriesProgramMapConfig.SERIES_PROGRAM_MGT, SeriesProgramMapConfig.SERIES_PROGRAM_MAP_DELETE,
                            SeriesProgramMapConfig.SERIES_PROGRAM_MAP_DELETE_INFO,
                            SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);
                    rtn = "0";
                }else
                {
                    rtn = "101";//mapping已发布
                }
               
            }
        }
        catch (Exception e)
        {
            log.debug("removeSeriesProgramMapByIndex exception:" + e);
            throw e;
        }
        return rtn;
    }

    public String insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException
    {
        log.debug("insertSeriesProgramMap starting...");
        try
        {
            long seriesindex = seriesProgramMap.getSeriesindex();
            CmsSeries cmsSeries = new CmsSeries();
            cmsSeries.setSeriesindex(seriesindex);
            cmsSeries = cmsSeriesDS.getCmsSeries(cmsSeries);
            if (cmsSeries == null || cmsSeries.getSeriesindex() == null)
            {
                return "200";// 连续剧内容已经被删除；
            }
            
            Long volumnCount = cmsSeries.getVolumncount();
            if (volumnCount == null || volumnCount == 0)
            {
                return "201";// 总集数为空不能添加单集
            }

            SeriesProgramMap seriesProgramDemo = new SeriesProgramMap();
            seriesProgramDemo.setSeriesindex(seriesindex);
            List<SeriesProgramMap> seriesProMapList = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramDemo);
            if (seriesProMapList.size() >= volumnCount)
            {
                return "202"; // 绑定单集的数量不能大于总集数
            }
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(seriesProgramMap.getProgramindex());
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if (cmsProgram == null || cmsProgram.getProgramindex() == null)
            {
                return "203";// 该连续剧单集已经被删除
            }
            int statuts = cmsProgram.getStatus();
            if (statuts != 0)//审核通过
            {
                return "204";// 该连续剧单集状态不正确
            }
            
            seriesProgramDemo = new SeriesProgramMap();
            seriesProgramDemo.setProgramindex(seriesProgramMap.getProgramindex());
            seriesProMapList = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramDemo);
            if (seriesProMapList != null && seriesProMapList.size() > 0)
            {
                if (seriesProMapList.get(0).getSeriesindex().longValue() == seriesindex)
                {
                    return "101";// 该单集已经绑定过了
                }else
                {
                    return "205";//该单集已经打包到其他连续剧,不能再被打包
                }
            }
            
            seriesProgramDemo = new SeriesProgramMap();
            seriesProgramDemo.setSequence(seriesProgramMap.getSequence());
            seriesProgramDemo.setSeriesindex(seriesProgramMap.getSeriesindex());
            seriesProMapList = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramDemo);

            if (seriesProMapList != null && seriesProMapList.size() > 0)
            {
                return "100";// 当前集序号已经存在
            }
            if (seriesProgramMap.getSequence() > volumnCount)
            {
                return "102";// 集序号不能大于连续剧总集数
            }

            seriesProgramMap.setStatus(0);
            seriesProgramMapDS.insertSeriesProgramMap(seriesProgramMap);
            
            CntTargetSync seriesTargetSync = new CntTargetSync();
            seriesTargetSync.setObjectid(cmsSeries.getSeriesid());
            seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
            List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
            
            if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
            {
                CntTargetSync progTargetSync = new CntTargetSync();
                progTargetSync.setObjectid(cmsProgram.getProgramid());
                progTargetSync.setObjecttype(ObjectType.PROGRAM_TYPE);
                List<CntTargetSync> progTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(progTargetSync);
                if (null != progTargetSyncList && progTargetSyncList.size() > 0)
                {
                    for (CntTargetSync srTargetSync:seriesTargetSyncList)
                    {
                        for (CntTargetSync progTSync:progTargetSyncList)
                        {
                            if (srTargetSync.getTargetindex().intValue() == progTSync.getTargetindex().intValue())
                            {
                                ICmsSeriesLS cmsSeriesLS = (ICmsSeriesLS) SSBBus.findDomainService("cmsSeriesLS");
                                Targetsystem target = new Targetsystem();
                                target.setTargetindex(srTargetSync.getTargetindex());
                                target.setStatus(0);//正常状态
                                List<Targetsystem> targetList = targetSystemDS.getTargetsystemByCond(target);
                                if (null != targetList && targetList.size() > 0)
                                {
                                    
                                    CntPlatformSync cntPlatformSync = new CntPlatformSync();
                                    cntPlatformSync.setObjectindex(seriesProgramMap.getMapindex());
                                    cntPlatformSync.setObjectid(seriesProgramMap.getMappingid());
                                    cntPlatformSync.setElementid(cmsProgram.getProgramid());
                                    cntPlatformSync.setParentid(cmsSeries.getSeriesid());
                                    cntPlatformSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                                    String seriesProgramMapOperdesc = cmsSeriesLS.bind2Platform(targetList.get(0),cntPlatformSync);
                                    if (seriesProgramMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                                    {
                                        CommonLogUtil
                                        .insertOperatorLog(
                                                seriesProgramMap.getMappingid() + ", " + targetList.get(0).getTargetid(),
                                                SeriesConfig.SERIES_MGT,
                                                SeriesConfig.SERIESPROG_BINDTARGET,
                                                SeriesConfig.SERIESPROG_BINDTARGET_INFO,
                                                SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
            CommonLogUtil.insertOperatorLog(String.valueOf(seriesProgramMap.getMapindex()),
                    SeriesProgramMapConfig.SERIES_PROGRAM_MGT, SeriesProgramMapConfig.SERIES_PROGRAM_MAP_ADD,
                    SeriesProgramMapConfig.SERIES_PROGRAM_MAP_ADD_INFO,
                    SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);

        }
        catch (DomainServiceException e)
        {
            log.error("insertSeriesProgramMap exception:" + e);
            throw e;
        }
        return "0";
    }

    public TableDataInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("SeriesProgramMapLS starting...");
        TableDataInfo tableinfo = null;
        try
        {
            String namecn = seriesProgramMap.getNamecn();
            EspecialCharMgt.conversion(namecn);
            seriesProgramMap.setNamecn(namecn);
            String programid = seriesProgramMap.getProgramid();
            EspecialCharMgt.conversion(programid);
            seriesProgramMap.setProgramid(programid);
            String createtime1 = seriesProgramMap.getCreatetime1();
            createtime1 = convertDateTimeFromPage(createtime1);
            seriesProgramMap.setCreatetime1(createtime1);
            String createtime2 = seriesProgramMap.getCreatetime2();
            createtime2 = convertDateTimeFromPage(createtime2);
            seriesProgramMap.setCreatetime2(createtime2);
            tableinfo = seriesProgramMapDS.pageInfoQueryForFrontPage(seriesProgramMap, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQueryForFrontPage exception:" + e);
            throw e;
        }
        log.debug("SeriesProgramMapLS end");
        return tableinfo;
    }
/*

ResourceMgt.findDefaultText("cancel.pub.series.single.relation.success")
ResourceMgt.findDefaultText("success.count")
ResourceMgt.findDefaultText("fail.count")
ResourceMgt.findDefaultText("blank.fail.count")
success.count=成功数量：
fail.count=失败数量：
blank.fail.count= 失败数量：
 */
    public String batchPublishMap(List<String> mapIndexs) throws Exception
    {

        log.debug("batchPublishMap starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int success = 0;
        int fail = 0;
        int index = 1;
        int exception = 0;
        try
        {
            String mountPoint = getMountPoint();
            if ((mountPoint == null) || (mountPoint.trim().equals("")))
            {
                return "1056030";// 获取挂载点失败
            }
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSiGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1056020";// 获取临时区失败代码
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1056040";
            }

            String bmsDestIndex = getTargetSystem(1);
            if (bmsDestIndex == null)
                return "1056050";// 获取BMS地址失败

            String epgDestIndex = getTargetSystem(2);
            if (epgDestIndex == null)
                return "1056060";// 获取EPG地址失败
            SeriesProgramMap seriesProMap = null;
            for (String mapindex : mapIndexs)
            {
                try
                {
                    seriesProMap = new SeriesProgramMap();
                    seriesProMap.setMapindex(Long.parseLong(mapindex));
                    seriesProMap = seriesProgramMapDS.getSeriesProgramMap(seriesProMap);
                    if (seriesProMap == null || seriesProMap.getMapindex() == null)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex), ResourceMgt.findDefaultText("pub.opr.fail"),
                        		ResourceMgt.findDefaultText("this.series.single.relation.deleted"));//"发布操作失败","该连续剧单集关联已经被删除"
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    CmsSeries cmsSeries = new CmsSeries();
                    cmsSeries.setSeriesindex(seriesProMap.getSeriesindex());
                    cmsSeries = cmsSeriesDS.getCmsSeries(cmsSeries);
                    if (cmsSeries == null | cmsSeries.getSeriesindex() == null
                            || cmsSeries.getSeriesindex().longValue() < 1)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                        		ResourceMgt.findDefaultText("this.series.deleted"));//"发布操作失败","该连续剧已经被删除"
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }

                    int seriesStatus = cmsSeries.getStatus();
                    if (seriesStatus != 20 && seriesStatus != 50 && seriesStatus != 60)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                        		ResourceMgt.findDefaultText("this.series.status.wrong"));//"发布操作失败","该连续剧状态不正确"
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }

                    int status = seriesProMap.getStatus();
                    if (status == 0 || status == 30 || status == 80 || status == 90)
                    {
                        if (status == 90)
                        {
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            cntSyncRecord.setObjtype(OBJECTTYPE);
                            cntSyncRecord.setObjindex(seriesProMap.getMapindex());
                            cntSyncRecord.setDestindex(Long.parseLong(bmsDestIndex));

                            List<CntSyncRecord> bmsSynRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);

                            cntSyncRecord = new CntSyncRecord();
                            cntSyncRecord.setObjtype(OBJECTTYPE);
                            cntSyncRecord.setObjindex(seriesProMap.getMapindex());
                            cntSyncRecord.setDestindex(Long.parseLong(epgDestIndex));
                            List<CntSyncRecord> epgSynRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);

                            if (bmsSynRecordList.size() > 0 && epgSynRecordList.size() > 0)
                            {
                                seriesProMap.setStatus(20);
                                seriesProgramMapDS.updateSeriesProgramMap(seriesProMap);
                                operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),
                                		ResourceMgt.findDefaultText("pub.opr.success") , ResourceMgt.findDefaultText("pub.series.single.relation.success"));//"发布操作成功", "发布连续剧单集关联成功"
                                returnInfo.appendOperateInfo(operateInfo);
                                success++;
                                CommonLogUtil.insertOperatorLog(String.valueOf(seriesProMap.getMapindex()),
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MGT,
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MAP_PUBLISH,
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MAP_PUBLISH_INFO,
                                        SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);
                                continue;
                            }
                        }
                        List<SeriesProgramMap> mapList = new ArrayList<SeriesProgramMap>();
                        mapList.add(seriesProMap);
                        String bmsXmlAddr = createXml(mapList, 1, mountPoint, desPath, synXMLFTPAdd);
                        String epgXmlAddr = createXml(mapList, 1, mountPoint, desPath, synXMLFTPAdd);

                        String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();// 获取任务批次号
                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        insertCntSyncTask(batchid, correlateidBMS, seriesProMap.getMapindex(), bmsXmlAddr, OBJECTTYPE,
                                TASKTYPE_REGIST, DESTTYPE_BMS, bmsDestIndex, 1, 1);
                        insertCntSyncTask(batchid, correlateidEPG, seriesProMap.getMapindex(), epgXmlAddr, OBJECTTYPE,
                                TASKTYPE_REGIST, DESTTYPE_EPG, epgDestIndex, 1, 1);
                        seriesProMap.setStatus(10);
                        seriesProgramMapDS.updateSeriesProgramMap(seriesProMap);
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.success") ,
                        		ResourceMgt.findDefaultText("pub.series.single.relation.success"));//"发布操作成功",发布连续剧单集关联成功
                        returnInfo.appendOperateInfo(operateInfo);
                        success++;
                        CommonLogUtil.insertOperatorLog(String.valueOf(seriesProMap.getMapindex()),
                                SeriesProgramMapConfig.SERIES_PROGRAM_MGT,
                                SeriesProgramMapConfig.SERIES_PROGRAM_MAP_PUBLISH,
                                SeriesProgramMapConfig.SERIES_PROGRAM_MAP_PUBLISH_INFO,
                                SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex), ResourceMgt.findDefaultText("pub.opr.fail"),
                        		ResourceMgt.findDefaultText("this.series.single.relation.status,wrong"));//"发布操作失败",该连续剧单集关联状态不正确
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }

                }
                catch (Exception e)
                {
                    log.error("manipuate database exception:" + e);
                    operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                    		ResourceMgt.findDefaultText("database.ope.abnormal"));//发布操作失败  "操作数据库异常"
                    returnInfo.appendOperateInfo(operateInfo);
                    exception++;
                }
            }
        }
        catch (Exception e)
        {
            log.error("batchPublishMap exception:" + e);
            throw e;
        }

        if (success + exception == mapIndexs.size())
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
        }
        else if (fail + exception == mapIndexs.size())
        {
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count")+ fail);
        }
        else
        {
            returnInfo.setFlag("2");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("blank.fail.count") + fail);
        }
        log.debug("batchPublishMap end");
        return returnInfo.toString();
    }

    public String batchCancelPublishMap(List<String> mapIndexs) throws Exception
    {

        log.debug("batchPublishMap starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int success = 0;
        int fail = 0;
        int index = 1;
        int exception = 0;
        try
        {
            String mountPoint = getMountPoint();
            if ((mountPoint == null) || (mountPoint.trim().equals("")))
            {
                return "1056030";// 获取挂载点失败
            }
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSiGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1056020";// 获取临时区失败代码
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1056040";
            }

            String bmsDestIndex = getTargetSystem(1);
            if (bmsDestIndex == null)
                return "1056050";// 获取BMS地址失败

            String epgDestIndex = getTargetSystem(2);
            if (epgDestIndex == null)
                return "1056060";// 获取EPG地址失败
            SeriesProgramMap seriesProMap = null;
            for (String mapindex : mapIndexs)
            {
                try
                {
                    seriesProMap = new SeriesProgramMap();
                    seriesProMap.setMapindex(Long.parseLong(mapindex));
                    seriesProMap = seriesProgramMapDS.getSeriesProgramMap(seriesProMap);
                    if (seriesProMap == null || seriesProMap.getMapindex() == null)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                        		ResourceMgt.findDefaultText("this.series.single.relation.deleted"));// "发布操作失败","该连续剧单集关联已经被删除"
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    int status = seriesProMap.getStatus();
                    if (status == 20 || status == 30 || status == 90)
                    {
                        if (status == 30)
                        {
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            cntSyncRecord.setObjtype(OBJECTTYPE);
                            cntSyncRecord.setObjindex(seriesProMap.getMapindex());
                            cntSyncRecord.setDestindex(Long.parseLong(epgDestIndex));
                            List<CntSyncRecord> bmsSynRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);

                            cntSyncRecord = new CntSyncRecord();
                            cntSyncRecord.setObjtype(OBJECTTYPE);
                            cntSyncRecord.setObjindex(seriesProMap.getMapindex());
                            cntSyncRecord.setDestindex(Long.parseLong(epgDestIndex));
                            List<CntSyncRecord> epgSynRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);

                            if ((epgSynRecordList == null && bmsSynRecordList == null)
                                    || (epgSynRecordList.size() < 1 && bmsSynRecordList.size() < 1))
                            {
                                seriesProMap.setStatus(80);
                                seriesProgramMapDS.updateSeriesProgramMap(seriesProMap);
                                operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),
                                		ResourceMgt.findDefaultText("pub.opr.success")  , ResourceMgt.findDefaultText("cancel.pub.series.single.relation.success"));//"发布操作成功", "取消发布连续剧单集关联成功"
                                returnInfo.appendOperateInfo(operateInfo);
                                success++;
                                CommonLogUtil.insertOperatorLog(String.valueOf(seriesProMap.getMapindex()),
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MGT,
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MAP_CANCEL_PUBLISH,
                                        SeriesProgramMapConfig.SERIES_PROGRAM_MAP_CANCEL_PUBLISH_INFO,
                                        SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);
                                continue;
                            }
                        }
                        List<SeriesProgramMap> mapList = new ArrayList<SeriesProgramMap>();
                        mapList.add(seriesProMap);
                        String bmsXmlAddr = createXml(mapList, 3, mountPoint, desPath, synXMLFTPAdd);
                        String epgXmlAddr = createXml(mapList, 3, mountPoint, desPath, synXMLFTPAdd);

                        String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();// 获取任务批次号
                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        insertCntSyncTask(batchid, correlateidBMS, seriesProMap.getMapindex(), bmsXmlAddr, OBJECTTYPE,
                                TASKTYPE_DELETE, DESTTYPE_BMS, bmsDestIndex, 1, 1);
                        insertCntSyncTask(batchid, correlateidEPG, seriesProMap.getMapindex(), epgXmlAddr, OBJECTTYPE,
                                TASKTYPE_DELETE, DESTTYPE_EPG, epgDestIndex, 1, 1);
                        seriesProMap.setStatus(70);
                        seriesProgramMapDS.updateSeriesProgramMap(seriesProMap);
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.success") ,
                        		ResourceMgt.findDefaultText("cancel.pub.series.single.relation.success"));// "发布操作成功","取消发布连续剧单集关联成功"
                        returnInfo.appendOperateInfo(operateInfo);
                        success++;
                        CommonLogUtil.insertOperatorLog(String.valueOf(seriesProMap.getMapindex()),
                                SeriesProgramMapConfig.SERIES_PROGRAM_MGT,
                                SeriesProgramMapConfig.SERIES_PROGRAM_MAP_CANCEL_PUBLISH,
                                SeriesProgramMapConfig.SERIES_PROGRAM_MAP_CANCEL_PUBLISH_INFO,
                                SeriesProgramMapConfig.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                        		ResourceMgt.findDefaultText("this.series.single.relation.status,wrong"));//"发布操作失败","该连续剧单集关联状态不正确"
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }

                }
                catch (Exception e)
                {
                    log.error("manipuate database exception:" + e);
                    operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(mapindex),ResourceMgt.findDefaultText("pub.opr.fail") ,
                    		ResourceMgt.findDefaultText("database.ope.abnormal"));//"发布操作失败" , "操作数据库异常"
                    returnInfo.appendOperateInfo(operateInfo);
                    exception++;
                }
            }
        }
        catch (Exception e)
        {
            log.error("batchPublishMap exception:" + e);
            throw e;
        }

        if (success + exception == mapIndexs.size())
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count")+ success);
        }
        else if (fail + exception == mapIndexs.size())
        {
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + fail);
        }
        else
        {
            returnInfo.setFlag("2");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("blank.fail.count") + fail);
        }
        log.debug("batchPublishMap end");
        return returnInfo.toString();
    }

    private String convertDateTimeFromPage(String pageDateTime)
    {
        String convertedValue = pageDateTime;
        if (!convertedValue.equals(""))
        {
            String year = convertedValue.substring(0, 4);
            String month = convertedValue.substring(5, 7);
            String day = convertedValue.substring(8, 10);

            String hour = convertedValue.substring(11, 13);
            String minute = convertedValue.substring(14, 16);
            String second = convertedValue.substring(17, 19);
            convertedValue = year + month + day + hour + minute + second;

        }
        return convertedValue;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 取出同步XML文件FTP地址
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig || null == usysConfig.getCfgvalue() || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }

    // 获取挂载点
    private String getMountPoint()
    {
        String moutPoint = GlobalConstants.getMountPoint();
        return moutPoint;
    }

    /**
     * 根据要发布的系统类型或者对应的系统index
     * 
     * @param type 系统类型
     * @return 发布系统index
     */
    private String getTargetSystem(int type)
    {
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(type);
        List<Targetsystem> listtsystem;
        String systemIndex = null;
        try
        {
            listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem != null && listtsystem.size() > 0)
            {
                targetSystem = listtsystem.get(0);
                systemIndex = targetSystem.getTargetindex().toString();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        return systemIndex;
    }

    /**
     * 
     * @param batchid 任务批次号
     * @param correlateid 任务流水号
     * @param programIndex 同步对象index
     * @param xmlAddress 内容相关对象的XML文件URL
     * @param objecttype 同步对象类型：若干object，若干map关系
     * @param taskType 同步类型：1 REGIST，2 UPDATE，3 DELETE
     * @param desttype 目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex 目标系统index
     * @param batchsingleflag 本批任务是否针对单个对象：0否，1是
     * @param tasksingleflag 当前任务是否针对单个对象：0否，1是
     */
    private void insertCntSyncTask(String batchid, String correlateid, Long seriesIndex, String xmlAddress,
            int objecttype, int taskType, int desttype, String destindex, int batchsingleflag, int tasksingleflag)
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        try
        {
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(destindex));
            ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }

    private String createXml(List<SeriesProgramMap> seriesProgramList, int syncType, String mountPoint,
            String tempAreaPath, String ftpAddress) throws Exception
    {
        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = "";
        fileName = date.getTime() + "_seriesprogram.xml";
        // 创建XML文件
        String synctype = "";
        String tempFilePath = mountPoint + tempAreaPath + File.separator + CNTSYNCXML + File.separator + dir
                + File.separator + SERIES_PROM_DIR + File.separator;
        String retPath = filterSlashStr(tempAreaPath + File.separator + CNTSYNCXML + File.separator + dir
                + File.separator + SERIES_PROM_DIR + File.separator + fileName);
        File file = new File(tempFilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = tempFilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        // 获取发布Action类型
        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }
        for (SeriesProgramMap mapDemo : seriesProgramList)
        {
            Element objectElement = null;
            Element seriesElement = null;
            // 元素节点
            objectElement = objectsElement.addElement("Mapping");
            objectElement.addAttribute("ParentType", "Series");
            objectElement.addAttribute("ElementType", "Program");
            String objectid = String.format("%02d",36) + String.format("%030d",mapDemo.getMapindex());
            objectElement.addAttribute("ObjectID",objectid);
            objectElement.addAttribute("ParentID", mapDemo.getSeriesid());
            objectElement.addAttribute("ElementID", mapDemo.getProgramid());
            objectElement.addAttribute("Action", synctype);

            // writeElement(objectElement, seriesElement, "Type", "99");// 插入连续剧名字
            writeElement(objectElement, seriesElement, "Sequence", mapDemo.getSequence());// 插入连续剧名字

            writeElement(objectElement, seriesElement, "ValidStart", "");// 插入连续剧名字
            writeElement(objectElement, seriesElement, "ValidEnd", "");// 插入连续剧名字

            XMLWriter writer = null;
            try
            {
                writer = new XMLWriter(new FileOutputStream(fileFullName));
                writer.write(dom);
            }
            catch (Exception e)
            {
                log.error("exception:" + e);
            }
            finally
            {
                if (writer != null)
                {
                    try
                    {
                        writer.close();
                    }
                    catch (IOException e)
                    {
                        log.error("IO exception:" + e);
                    }
                }
            }
        }
        return retPath;
    }

    private void writeElement(Element objectElement, Element propertyElement, String key, Object value)
    {
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", key);
        if (value == null)
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(String.valueOf(value));
        }
    }

    // 获取时间 如"20111006"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    public ICntSyncTaskDS getNtsyncTaskDS()
    {
        return ntsyncTaskDS;
    }

    public void setNtsyncTaskDS(ICntSyncTaskDS ntsyncTaskDS)
    {
        this.ntsyncTaskDS = ntsyncTaskDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public ICmsSeriesDS getCmsSeriesDS()
    {
        return cmsSeriesDS;
    }

    public void setCmsSeriesDS(ICmsSeriesDS cmsSeriesDS)
    {
        this.cmsSeriesDS = cmsSeriesDS;
    }

    public ICmsProgramDS getCmsProgramDS()
    {
        return cmsProgramDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public ISeriesProgramMapDS getSeriesProgramMapDS()
    {
        return seriesProgramMapDS;
    }

    public void setSeriesProgramMapDS(ISeriesProgramMapDS seriesProgramMapDS)
    {
        this.seriesProgramMapDS = seriesProgramMapDS;
    }

    public ICmsStorageareaLS getCmsStorageareaLS()
    {
        return cmsStorageareaLS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }
    
	public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
	{
		this.cntPlatformSyncDS = cntPlatformSyncDS;
	}
    

	public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
	{
		this.cntTargetSyncDS = cntTargetSyncDS;
	}    
    

	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
	{
		this.objectSyncRecordDS = objectSyncRecordDS;
	}	
	
	public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
	{
		this.cntSyncTaskDS = cntSyncTaskDS;
	}

	
	
	/*************************连续剧关联单集发布 begin************************/
    public TableDataInfo pageInfoQueryseriesProMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {

            // 格式化内容带有时间的字段
            seriesProgramMap.setCreateStarttime(DateUtil2.get14Time(seriesProgramMap.getCreateStarttime()));
            seriesProgramMap.setCreateEndtime(DateUtil2.get14Time(seriesProgramMap.getCreateEndtime()));
            seriesProgramMap.setOnlinetimeStartDate(DateUtil2.get14Time(seriesProgramMap.getOnlinetimeStartDate()));
            seriesProgramMap.setOnlinetimeEndDate(DateUtil2.get14Time(seriesProgramMap.getOnlinetimeEndDate()));
            seriesProgramMap.setCntofflinestarttime(DateUtil2.get14Time(seriesProgramMap.getCntofflinestarttime()));
            seriesProgramMap.setCntofflineendtime(DateUtil2.get14Time(seriesProgramMap.getCntofflineendtime()));
            

            if(seriesProgramMap.getProgramid()!=null&&!"".equals(seriesProgramMap.getProgramid())){
            	seriesProgramMap.setProgramid(EspecialCharMgt.conversion(seriesProgramMap.getProgramid()));
            }
            if(seriesProgramMap.getPronamecn()!=null&&!"".equals(seriesProgramMap.getPronamecn())){
            	seriesProgramMap.setPronamecn(EspecialCharMgt.conversion(seriesProgramMap.getPronamecn()));
            }
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            seriesProgramMap.setOperid(operinfo.getOperid());

            dataInfo = seriesProgramMapDS.pageInfoQueryseriesproMap(seriesProgramMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
   
    public TableDataInfo pageInfoQueryseriesProTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            // 格式化内容带有时间的字段
            
            dataInfo = seriesProgramMapDS.pageInfoQueryseriesproTargetMap(seriesProgramMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
    
		
    public String batchPublishOrDelPublishseriesPro(List<SeriesProgramMap> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishSeriesProgramMap starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
            if (operType.equals("0"))  //批量发布
            {
                for (SeriesProgramMap seriesProgramMap : list)
                {
                    message = publishseriesProMapPlatformSyn(seriesProgramMap.getMapindex().toString(), seriesProgramMap.getSeriesid(),seriesProgramMap.getProgramid(),type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                for (SeriesProgramMap seriesProgramMap : list)
                {
                    message = delPublishseriesProMapPlatformSyn(seriesProgramMap.getMapindex().toString(), seriesProgramMap.getSeriesid(),seriesProgramMap.getProgramid(),type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesProgramMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("SeriesProgramMapLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
    
    /**
     * 判断获取发布时应填的状态
     * @param seriesProgramMapList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getTaskstatus(List<SeriesProgramMap> seriesProgramMapList) throws Exception
    {
        int tgType = 1;
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(SeriesProgramMap targetsysSyn:seriesProgramMapList)
        {
            if (targetsysSyn.getTargettype()==1&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400))
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&(targetsysSyn.getStatus()==0||targetsysSyn.getStatus()==400))
            {
                hasepg = true;
            }
        }
        
        if (hasbms) //存在bms的情况
        {
             tgType = 1;
        }
        if (!hasbms&&hasepg) //不存在bms,只存在epg的情况
        {
            tgType = 2;
        } 
        return tgType;
    }    

    /**
     * 判断获取取消发布时应填的状态
     * @param seriesProgramMapList ,targettype,publishType
     * @return int status
     * @throws Exception ds异常 
     */
    public int getdelTaskstatus(List<SeriesProgramMap> seriesProgramMapList) throws Exception
    {
        int tgType = 2;
        boolean hasbms = false; //判断有无关联bms网元
        boolean hasepg = false; //判断有无关联epg网元

        for(SeriesProgramMap targetsysSyn:seriesProgramMapList)
        {

            if (targetsysSyn.getTargettype()==1&&targetsysSyn.getStatus()==300)
            {
                hasbms = true;
            }
            if (targetsysSyn.getTargettype()==2&&targetsysSyn.getStatus()==300)
            {
                hasepg = true;
            }
        }
        if (hasepg) //存在epg的情况
        {
            tgType = 2;
        }
        if (!hasepg&&hasbms) //不存在epg,只存在bms的情况
        {
            tgType = 1;
        } 
        return tgType;
    }  
    
    public String publishseriesProMapPlatformSyn(String mapindex, String seriesid,String programid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
    		seriesProgramMap.setMapindex(Long.parseLong(mapindex));
    		seriesProgramMap.setSeriesid(seriesid);
    		seriesProgramMap.setProgramid(programid);
			List<SeriesProgramMap> seriesProgramMapList = new ArrayList<SeriesProgramMap>();
			
			seriesProgramMapList =seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
			if (seriesProgramMapList == null ||seriesProgramMapList.size()==0)
			{
				return "1:操作失败,连续剧关联单集记录不存在";
			}
    		String initresult="";
    		initresult =initData(seriesProgramMapList.get(0),"platform",type,1);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(36);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
 
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesProgramMap seriesProgramMapObj=new SeriesProgramMap();
            int  platformType = 1;
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(seriesProgramMap.getMapindex());
            int stat = 1; //待发布
            int tgType = 1;
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

    	    	if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	sereispromap.put("seriesProgramMap", seriesProgramMapList);
    	    	int targettype = 0;
                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")); 
                tgType = getTaskstatus(seriesProgramMapList);
                if (tgType == 1)
                {
                    for (SeriesProgramMap seriesProgramMapTemp:seriesProgramMapList )
                    {
                        int targType = seriesProgramMapTemp.getTargettype();
                        if (targType == 1)
                        {
                            CntTargetSync cntTargetSynSeri = new CntTargetSync();          
                            cntTargetSynSeri.setObjectindex(seriesProgramMapTemp.getSeriesindex());
                            cntTargetSynSeri.setTargetindex(seriesProgramMapTemp.getTargetindex());
                            cntTargetSynSeri.setObjecttype(4);
                            cntTargetSynSeri = cntTargetSyncDS.getCntTargetSync(cntTargetSynSeri);
                            if (cntTargetSynSeri == null||(cntTargetSynSeri != null && cntTargetSynSeri.getStatus() != 300 && cntTargetSynSeri.getStatus() != 500))
                            {
                                return "1:操作失败,该连续剧没有发布到"+TARGETSYSTEM
                                        +seriesProgramMapTemp.getTargetid()+",不能发布";  
                            }
                            CntTargetSync cntTargetSynPro = new CntTargetSync();          
                            cntTargetSynPro.setObjectindex(seriesProgramMapTemp.getProgramindex());
                            cntTargetSynPro.setTargetindex(seriesProgramMapTemp.getTargetindex());
                            cntTargetSynPro.setObjecttype(3);
                            cntTargetSynPro = cntTargetSyncDS.getCntTargetSync(cntTargetSynPro);
                            //点播内容在该平台已发布
                            if (cntTargetSynPro == null||(cntTargetSynPro != null && cntTargetSynPro.getStatus() != 300 && cntTargetSynPro.getStatus() != 500))
                            {
                                return "1:操作失败,该单集没有发布到"+TARGETSYSTEM
                                        +seriesProgramMapTemp.getTargetid()+",不能发布"; 
                            }
                        }

                    }

                }
    			for (SeriesProgramMap seriesProgramMapTmp:seriesProgramMapList )
    			{
    			    targettype = seriesProgramMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (seriesProgramMapTmp.getTargetstatus()!=0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                	    		 +seriesProgramMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                +"  ");
                   	    isstatus = false;
    				}    			
    				if (isstatus)
    				{
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    	            synType = 1;
    				boolean ispub = true;
    				cntTargetSync.setObjectindex(seriesProgramMapTmp.getSeriesindex());
    				cntTargetSync.setTargetindex(seriesProgramMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(4);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                	    resultPubStr.append("该连续剧没有发布到"+TARGETSYSTEM
               	    		 +seriesProgramMapTmp.getTargetid()
                               +"  ");    	
                	    ispub = false;
                	}
    				if (ispub)
    				{
        	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();    		

        	            cntTargetSyncTmp.setObjectindex(seriesProgramMapTmp.getProgramindex());
        	            cntTargetSyncTmp.setTargetindex(seriesProgramMapTmp.getTargetindex());
        	            cntTargetSyncTmp.setObjecttype(3);
        	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
        				//点播内容在该平台已发布
        				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
        				{
                    	    resultPubStr.append("该单集没有发布到"+TARGETSYSTEM
                   	    		 +seriesProgramMapTmp.getTargetid()
                                   +"  ");    	
                    	    ispub = false;
                    	}
    				}
    				if (ispub)
    				{
                        int synTargetType =1;
                        if (seriesProgramMapTmp.getStatus()== 500) //修改发布    
                        {
                    		synTargetType =2;
            	            synType = 2;
                        }
        				boolean isxml = true;

                       	if ((targettype ==1||targettype ==2)
                       			&&(seriesProgramMapTmp.getStatus()== 0||
                       			seriesProgramMapTmp.getStatus()== 400||seriesProgramMapTmp.getStatus()== 500))  //EPG或BMS系统
                       	{
                       		String xml;
                       		if (synTargetType==1)
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 1); //新增发布到EPG
                                if (null == xml)
                                {
                                    isxml = false;
                                    resultPubStr.append(PUBTARGETSYSTEM
                                             +seriesProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		else
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 2); //新增发布到EPG
                                if (null == xml)
                                {
                                    isxml = false;
                                    resultPubStr.append(PUBTARGETSYSTEM
                                             +seriesProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		
                       		if (isxml)
                       		{
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                Long targetindex =seriesProgramMapTmp.getTargetindex();
                                
                                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = seriesProgramMapTmp.getMappingid();
                                String parentid = seriesProgramMapTmp.getSeriesid();
                                String  elementid = seriesProgramMapTmp.getProgramid();
                                Long objectindex = seriesProgramMapTmp.getMapindex();
                                insertMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                                if (tgType == targettype)
                                {
                                    stat = 1;
                                }
                                else
                                {
                                    stat = 0;
                                }
                                insertSyncPicTask(index,xml,correlateid,targetindex,batchid ,stat); //插入同步任务
                                
                                cntTargetSyncTemp.setSyncindex(seriesProgramMapTmp.getSyncindex());
                                if (synTargetType ==1)
                                {
                                	cntTargetSyncTemp.setOperresult(10);
                                }
                                else
                                {
                                	cntTargetSyncTemp.setOperresult(40);
                                }                            
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                CommonLogUtil.insertOperatorLog(seriesProgramMapTmp.getMappingid()+","+seriesProgramMapTmp.getTargetid(), 
                                		CommonLogConstant.MGTTYPE_SERIESSYN,
                                        CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                                sucessPub=true;
                        	    resultPubStr.append(PUBTARGETSYSTEM
                        	    		 +seriesProgramMapTmp.getTargetid()
                                        +": "+operSucess+"  ");                       			
                       		}

                          }
                       }                       		
    			   } 
    		   }
    		   }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			   seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

       	    	   if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   sereispromap.put("seriesProgramMap", seriesProgramMapList);
       	    	   
       	    	   int targettype = 0;
       	    	   
         			for (SeriesProgramMap seriesProgramMapTmp:seriesProgramMapList )
           			{
                        targettype = seriesProgramMapTmp.getTargettype();

        				boolean isstatus = true;
        				if (seriesProgramMapTmp.getTargetstatus()!=0)
        				{
                       	    resultPubStr.append(PUBTARGETSYSTEM
                    	    		 +seriesProgramMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                    +"  ");
                       	    isstatus = false;
        				}    			
        				if (isstatus)
        				{
        	            synType = 1;
           	            CntTargetSync cntTargetSync = new CntTargetSync();    		
           				boolean ispub = true;
           				cntTargetSync.setObjectindex(seriesProgramMapTmp.getSeriesindex());
           				cntTargetSync.setTargetindex(seriesProgramMapTmp.getTargetindex());
        				cntTargetSync.setObjecttype(4);
           				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
           				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
           				{
                       	    resultPubStr.append("该连续剧没有发布到"+TARGETSYSTEM
                      	    		 +seriesProgramMapTmp.getTargetid()
                                      +"  ");    	
                       	    ispub = false;
                       	}
        				if (ispub)
        				{
            	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();    		

            	            cntTargetSyncTmp.setObjectindex(seriesProgramMapTmp.getProgramindex());
            	            cntTargetSyncTmp.setTargetindex(seriesProgramMapTmp.getTargetindex());
            	            cntTargetSyncTmp.setObjecttype(3);
            	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
            				//点播内容在该平台已发布
            				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
            				{
                        	    resultPubStr.append("该单集没有发布到"+TARGETSYSTEM
                       	    		 +seriesProgramMapTmp.getTargetid()
                                       +"  ");    	
                        	    ispub = false;
                        	}
        				}           				
           				if (ispub)
           				{
                               int synTargetType =1;
                               if (seriesProgramMapTmp.getStatus()== 500) //修改发布    
                               {
                           		    synTargetType =2;
                	                synType = 2;
                               }
               				   
                               boolean isxml = true;

                               if ((seriesProgramMapTmp.getStatus()== 0||
                            		   seriesProgramMapTmp.getStatus()== 400||seriesProgramMapTmp.getStatus()== 500))  //2.0平台网元
                              	{
                              		String xml;
                              		if (synTargetType==1)
                              		{
                                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 36, 1); //新增发布
                                        if (null == xml)
                                        {
                                        	isxml = false;
                                       	    resultPubStr.append(PUBTARGETSYSTEM
                                     	    		 +seriesProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                     +"  ");
                                        }
                              		}
                              		else
                              		{
                                         xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 10, 2); //修改发布
                                         if (null == xml)
                                         {
                                             isxml = false;
                                        	 resultPubStr.append(PUBTARGETSYSTEM
                                      	    		 +seriesProgramMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                      +"  ");
                                         }
                              		}
                              		if (isxml)
                              		{
                                        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                                             		    
                                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                        Long targetindex =seriesProgramMapTmp.getTargetindex();
                                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        String objectidMapping = seriesProgramMapTmp.getMappingid();
                                        String parentid = seriesProgramMapTmp.getSeriesid();
                                        String  elementid = seriesProgramMapTmp.getProgramid();
                                        Long objectindex = seriesProgramMapTmp.getMapindex();
                                        String elementcode = seriesProgramMapTmp.getProcpcontentid();
                                        String parentcode = seriesProgramMapTmp.getCpcontentid();
                                        insertMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                        insertSyncPicTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务

                                       cntTargetSyncTemp.setSyncindex(seriesProgramMapTmp.getSyncindex());
                                       if (synTargetType ==1)
                                       {
                                       	   cntTargetSyncTemp.setOperresult(10);
                                       }
                                       else
                                       {
                                           cntTargetSyncTemp.setOperresult(40);
                                       }                            
                                       cntTargetSyncTemp.setStatus(200);
                                       cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                       CommonLogUtil.insertOperatorLog(seriesProgramMapTmp.getMappingid()+","+seriesProgramMapTmp.getTargetid(),
                                    		   CommonLogConstant.MGTTYPE_SERIESSYN,
                                               CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_INFO,
                                               CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                       sucessPub=true;
                               	       resultPubStr.append(PUBTARGETSYSTEM
                               	    		 +seriesProgramMapTmp.getTargetid()
                                               +": "+operSucess+"  ");                               			
                              		}

                                 }
                             }                          		
           				} 
    	       }
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    	}   

   
    public String delPublishseriesProMapPlatformSyn(String mapindex, String seriesid,String programid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
    		seriesProgramMap.setMapindex(Long.parseLong(mapindex));
    		seriesProgramMap.setSeriesid(seriesid);
    		seriesProgramMap.setProgramid(programid);
			List<SeriesProgramMap> seriesProgramMapList = new ArrayList<SeriesProgramMap>();
			
			seriesProgramMapList =seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
			if (seriesProgramMapList == null ||seriesProgramMapList.size()==0)
			{
				return "1:操作失败,连续剧关联单集记录不存在";
			}
    		String initresult="";
    		initresult =initData(seriesProgramMapList.get(0),"platform",type,3);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(36);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
 
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesProgramMap seriesProgramMapObj=new SeriesProgramMap();
            int  platformType = 1;
            synType = 3;

			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(seriesProgramMap.getMapindex());
            int stat = 1; //待发布
			int tgType = 2;
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

    	    	if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	sereispromap.put("seriesProgramMap", seriesProgramMapList);
    	    	int targettype = 0;
                Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
    	    	tgType = getdelTaskstatus(seriesProgramMapList);
    			for (SeriesProgramMap seriesProgramMapTmp:seriesProgramMapList )
    			{
    			    targettype = seriesProgramMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (seriesProgramMapTmp.getTargetstatus()!=0)
    				{
             	       resultPubStr.append(DELPUBTARGETSYSTEM
           	    		 +seriesProgramMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                           +"  ");   
             	       isstatus = false;
    				}
    				if (isstatus)
    				{
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    				boolean isxml = true;

                   	if ((seriesProgramMapTmp.getTargettype() ==1||seriesProgramMapTmp.getTargettype() ==2)
                   			&&(seriesProgramMapTmp.getStatus()== 300||seriesProgramMapTmp.getStatus()== 500))  //EPG或BMS系统
                   	{
                   	    String xml;
               			if (targettype ==1)
               			{
                            xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 3); //新增发布到EPG
                            if (null == xml)
                            {
                            	   isxml = false;
                        	       resultPubStr.append(DELPUBTARGETSYSTEM
                      	    		 +seriesProgramMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                      +"  ");                               
                        	 }  
               			}
               			else
               			{
                            xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 3); //新增发布到EPG
                            if (null == xml)
                            {
                            	   isxml = false;
                        	       resultPubStr.append(DELPUBTARGETSYSTEM
                      	    		 +seriesProgramMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                      +"  ");                               
                        	 } 
               			}
                   	                  		
                   		if (isxml)
                   		{
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                            Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                            Long targetindex =seriesProgramMapTmp.getTargetindex();
                            
                            Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                            Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                            String objectidMapping = seriesProgramMapTmp.getMappingid();
                            String parentid = seriesProgramMapTmp.getSeriesid();
                            String  elementid = seriesProgramMapTmp.getProgramid();
                            Long objectindex = seriesProgramMapTmp.getMapindex();
                            insertMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                            if (targettype == tgType)
                            {
                                stat = 1;
                            }
                            else
                            {
                                stat = 0;
                            }
                            insertSyncPicTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务                            
                            cntTargetSyncTemp.setSyncindex(seriesProgramMapTmp.getSyncindex());
                            cntTargetSyncTemp.setOperresult(70);                                             
                            cntTargetSyncTemp.setStatus(200);
                            cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                            CommonLogUtil.insertOperatorLog(seriesProgramMapTmp.getMappingid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                                    CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                            sucessPub=true;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                    	    		+seriesProgramMapTmp.getTargetid()+delPub+": "
                                    +operSucess+"  ");                      			
                   		   }
                   		}  
                   	 }
    			  } 
    		   }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			   seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

       	    	   if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   sereispromap.put("seriesProgramMap", seriesProgramMapList);
       	    	   int targettype = 0;
         		   for (SeriesProgramMap seriesProgramMapTmp:seriesProgramMapList )
           		   {  
                       targettype = seriesProgramMapTmp.getTargettype();

       				boolean isstatus = true;
       				if (seriesProgramMapTmp.getTargetstatus()!=0)
       				{
                	       resultPubStr.append(DELPUBTARGETSYSTEM
              	    		 +seriesProgramMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                              +"  ");   
                	       isstatus = false;
       				}
       				if (isstatus)
           		   {  
                       boolean isxml = true;
                       if ((seriesProgramMapTmp.getStatus()== 300||
                    		  seriesProgramMapTmp.getStatus()== 500))  //2.0平台网元
                       {
                           String xml;
                           xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 36, 3); //取消发布
                           if (null == xml)
                           {
                           	   isxml = false;
                       	       resultPubStr.append(DELPUBTARGETSYSTEM
                     	    		 +seriesProgramMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                     +"  ");                               
                       	   } 
                    	   if (isxml)
                    	   {
                              Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));                    	       
                              String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                              Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                              Long targetindex =seriesProgramMapTmp.getTargetindex();
                              Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                              Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                              String objectidMapping = seriesProgramMapTmp.getMappingid();
                              String parentid = seriesProgramMapTmp.getSeriesid();
                              String  elementid = seriesProgramMapTmp.getProgramid();
                              Long objectindex = seriesProgramMapTmp.getMapindex();
                              String elementcode = seriesProgramMapTmp.getProcpcontentid();
                              String parentcode = seriesProgramMapTmp.getCpcontentid();
                              insertMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                              insertSyncPicTask(index,xml,correlateid,targetindex,batchid,stat); //插入同步任务
                              cntTargetSyncTemp.setSyncindex(seriesProgramMapTmp.getSyncindex());
                              cntTargetSyncTemp.setOperresult(70);
                              cntTargetSyncTemp.setStatus(200);
                              cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                              CommonLogUtil.insertOperatorLog(seriesProgramMapTmp.getMappingid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                                      CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL_INFO,
                                      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                              sucessPub=true;
                      	      resultPubStr.append(DELPUBTARGETSYSTEM
                      	    		+seriesProgramMapTmp.getTargetid()+delPub+": "
                                      +operSucess+"  "); 		
                    		}                      
                    		}                      
                       }                          		
    	           }
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    	}   


    public String publishseriesProMapTarget(String mapindex, String targetindex,int type) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{       
    		SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
    		seriesProgramMap.setMapindex(Long.parseLong(mapindex));
    		seriesProgramMap.setTargetindex(Long.parseLong(targetindex));
    		seriesProgramMap.setObjecttype(36);
			List<SeriesProgramMap> seriesProgramMapList = new ArrayList<SeriesProgramMap>();
			
			seriesProgramMapList =seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
			if (seriesProgramMapList == null ||seriesProgramMapList.size()==0)
			{
				return "1:操作失败,连续剧关联单集记录不存在";
			}
    		int synType = 1;

    		String initresult="";
    		initresult =initData(seriesProgramMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(36);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
 
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesProgramMap seriesProgramMapObj=new SeriesProgramMap();
            int  platformType = 1;
            int stat = 1; //待发布            
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesProgramMapObj.setTargetindex(Long.parseLong(targetindex));
    			seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

    	    	if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (seriesProgramMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	int targettype = seriesProgramMapList.get(0).getTargettype();
    	    	sereispromap.put("seriesProgramMap", seriesProgramMapList);
	            CntTargetSync cntTargetSync = new CntTargetSync();    		
	            synType = 1;
				cntTargetSync.setObjectindex(seriesProgramMapList.get(0).getSeriesindex());
				cntTargetSync.setTargetindex(seriesProgramMapList.get(0).getTargetindex());
				cntTargetSync.setObjecttype(4);
				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
				{
                    return "1:操作失败,该连续剧没有发布到该网元";
            	}

	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();    		

	            cntTargetSyncTmp.setObjectindex(seriesProgramMapList.get(0).getProgramindex());
	            cntTargetSyncTmp.setTargetindex(seriesProgramMapList.get(0).getTargetindex());
	            cntTargetSyncTmp.setObjecttype(3);
	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
				//点播内容在该平台已发布
				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
				{
                    return "1:操作失败,该单集没有发布到该网元";

            	}

                int synTargetType =1;
                if (seriesProgramMapList.get(0).getStatus()== 500) //修改发布    
                {
            		synTargetType =2;
    	            synType = 2;
                }

               	if (targettype ==1||targettype ==2)  //EPG或BMS系统
               	{
               		String xml;
               		if (synTargetType==1)
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 1); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 2); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = seriesProgramMapList.get(0).getMappingid();
                    String parentid = seriesProgramMapList.get(0).getSeriesid();
                    String  elementid = seriesProgramMapList.get(0).getProgramid();
                    Long objectindex = seriesProgramMapList.get(0).getMapindex();
                    insertMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.parseLong(targetindex),synType);
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    insertSyncPicTask(index,xml,correlateid,Long.parseLong(targetindex),batchid,stat); //插入同步任务
                    cntTargetSyncTemp.setSyncindex(seriesProgramMapList.get(0).getSyncindex());
                    if (synTargetType ==1)
                    {
                    	cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                    	cntTargetSyncTemp.setOperresult(40);
                    }                            
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(seriesProgramMapList.get(0).getMappingid()+","+
                    seriesProgramMapList.get(0).getMappingid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                            CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                                       			                     
               }                       		
		   } 
 	       if (type == 2)
 	       {
       	       cntPlatformSync.setPlatform(1);
    		   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    		   seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
   			   seriesProgramMapObj.setTargetindex(Long.parseLong(targetindex));
 			   seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);
	    	   if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
	    	   {
                   return "1:操作失败,获取目标网元发布记录失败";
	    	   }
   	    	   if (seriesProgramMapList.get(0).getTargetstatus()!=0)
   	    	   {
                   return "1:操作失败,目标系统状态不正确";
   	    	   }
	    	   sereispromap.put("seriesProgramMap", seriesProgramMapList);
               int targettype = seriesProgramMapList.get(0).getTargettype();

 	           synType = 1;
	           CntTargetSync cntTargetSync = new CntTargetSync();    		
			   cntTargetSync.setObjectindex(seriesProgramMapList.get(0).getSeriesindex());
			   cntTargetSync.setTargetindex(seriesProgramMapList.get(0).getTargetindex());
 			   cntTargetSync.setObjecttype(4);
			   cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
  			   if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
   			   {
                   return "1:操作失败,该连续剧没有发布到该网元";

               }
	           CntTargetSync cntTargetSyncTmp = new CntTargetSync();    		
  	           cntTargetSyncTmp.setObjectindex(seriesProgramMapList.get(0).getProgramindex());
   	           cntTargetSyncTmp.setTargetindex(seriesProgramMapList.get(0).getTargetindex());
   	           cntTargetSyncTmp.setObjecttype(3);
   	           cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
			   //点播内容在该平台已发布
			   if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
			   {
                   return "1:操作失败,该单集没有发布到该网元";

       	       }

           int synTargetType =1;
           if (seriesProgramMapList.get(0).getStatus()== 500) //修改发布    
           {
       	       synTargetType =2;
               synType = 2;
           }


               String xml;
         	   if (synTargetType==1)
         	   {
                   xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 36, 1); //新增发布
                   if (null == xml)
                   {
                       return "1:操作失败,生成同步指令xml文件失败";
                   }
         		}
          		else
          		{
                     xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 36, 2); //修改发布
                     if (null == xml)
                     {
                         return "1:操作失败,生成同步指令xml文件失败";
                     }
          		}
               Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
               String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
               Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
               Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
               Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
               String objectidMapping = seriesProgramMapList.get(0).getMappingid();
               String parentid = seriesProgramMapList.get(0).getSeriesid();
               String  elementid = seriesProgramMapList.get(0).getProgramid();
               Long objectindex = seriesProgramMapList.get(0).getMapindex();
               String elementcode = seriesProgramMapList.get(0).getProcpcontentid();
               String parentcode = seriesProgramMapList.get(0).getCpcontentid();
               insertMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.parseLong(targetindex),synType);
               insertSyncPicTask(index,xml,correlateid,Long.parseLong(targetindex),batchid,stat); //插入同步任务

              cntTargetSyncTemp.setSyncindex(seriesProgramMapList.get(0).getSyncindex());
              if (synTargetType ==1)
              {
              	   cntTargetSyncTemp.setOperresult(10);
              }
              else
              {
                  cntTargetSyncTemp.setOperresult(40);
              }                            
              cntTargetSyncTemp.setStatus(200);
              cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
              CommonLogUtil.insertOperatorLog(seriesProgramMapList.get(0).getMappingid()+","+
                      seriesProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                              CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_INFO,
                              CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                               			

 	      }                        

		   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
           int[] targetSyncStatus = new int[cntTargetSyncList.size()];
           for(int i = 0;i<cntTargetSyncList.size() ;i++)
           {
           	   targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
           }
           int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
           if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
           {
               CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
               cntPlatformSyncTmp.setObjectindex(seriesProgramMap.getMapindex());
               cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
               cntPlatformSyncTmp.setStatus(status);
               cntPlatformSyncTmp.setPlatform(platformType);
               cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
           } 
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result; 

    }   

    public String delPublishseriesProMapTarget(String mapindex, String targetindex,int type) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{       
    		SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
    		seriesProgramMap.setMapindex(Long.parseLong(mapindex));
    		seriesProgramMap.setTargetindex(Long.parseLong(targetindex));
    		seriesProgramMap.setObjecttype(36);
			List<SeriesProgramMap> seriesProgramMapList = new ArrayList<SeriesProgramMap>();
			
			seriesProgramMapList =seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
			if (seriesProgramMapList == null ||seriesProgramMapList.size()==0)
			{
				return "1:操作失败,连续剧关联单集记录不存在";
			}
    		int synType = 3;

    		String initresult="";
    		initresult =initData(seriesProgramMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(36);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
 
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesProgramMap seriesProgramMapObj=new SeriesProgramMap();
            int  platformType = 1;
            int stat = 1; //待发布            
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesProgramMapObj.setTargetindex(Long.parseLong(targetindex));
    			seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);

    	    	if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (seriesProgramMapList.get(0).getTargetstatus()!=0)
       	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	
    	    	int targettype = seriesProgramMapList.get(0).getTargettype();
    	    	sereispromap.put("seriesProgramMap", seriesProgramMapList);
	            CntTargetSync cntTargetSync = new CntTargetSync();    		


               	if ((targettype ==1||targettype ==2))  //EPG或BMS系统
               	{
               		String xml;

           			if (targettype ==1)
           			{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 3); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
           			}
           			else
           			{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispromap, targettype, 36, 3); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
           			}

                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = seriesProgramMapList.get(0).getMappingid();
                    String parentid = seriesProgramMapList.get(0).getSeriesid();
                    String  elementid = seriesProgramMapList.get(0).getProgramid();
                    Long objectindex = seriesProgramMapList.get(0).getMapindex();
                    insertMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.parseLong(targetindex),synType);
                    Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    
                    insertSyncPicTask(index,xml,correlateid,Long.parseLong(targetindex),batchid,stat); //插入同步任务
                    
                    cntTargetSyncTemp.setSyncindex(seriesProgramMapList.get(0).getSyncindex());

                    cntTargetSyncTemp.setOperresult(70);
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(seriesProgramMapList.get(0).getMappingid()+","+seriesProgramMapList.get(0).getTargetid(),
                    		CommonLogConstant.MGTTYPE_SERIESSYN,
                            CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);  		
               	}                      
                                      		
		    } 
    	 
 	       if (type == 2)
 	       {
       	       cntPlatformSync.setPlatform(1);
    		   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    		   seriesProgramMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
   			   seriesProgramMapObj.setTargetindex(Long.parseLong(targetindex));
 			   seriesProgramMapList =seriesProgramMapDS.getnormalSeriesProTargetMapByCond(seriesProgramMapObj);
	    	   if (seriesProgramMapList == null || seriesProgramMapList.size() == 0)
	    	   {
                   return "1:操作失败,获取目标网元发布记录失败";
	    	   }
   	    	  if (seriesProgramMapList.get(0).getTargetstatus()!=0)
   	    	  {
                return "1:操作失败,目标系统状态不正确";
	    	   }
              int targettype = seriesProgramMapList.get(0).getTargettype();

	    	   sereispromap.put("seriesProgramMap", seriesProgramMapList);

 	           synType = 3;
	           CntTargetSync cntTargetSync = new CntTargetSync();    		

               boolean isxml = true;

                   String xml;

                   xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispromap, targettype, 36, 3); //新增发布
                   if (null == xml)
                   {
                       return "1:操作失败,生成同步指令xml文件失败";
                   }
                   Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));

                   String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                   Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                   Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                   Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                   String objectidMapping = seriesProgramMapList.get(0).getMappingid();
                   String parentid = seriesProgramMapList.get(0).getSeriesid();
                   String  elementid = seriesProgramMapList.get(0).getProgramid();
                   Long objectindex = seriesProgramMapList.get(0).getMapindex();
                   String elementcode = seriesProgramMapList.get(0).getProcpcontentid();
                   String parentcode = seriesProgramMapList.get(0).getCpcontentid();
                   insertMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.parseLong(targetindex),synType);
                   insertSyncPicTask(index,xml,correlateid,Long.parseLong(targetindex),batchid,stat); //插入同步任务

                   cntTargetSyncTemp.setSyncindex(seriesProgramMapList.get(0).getSyncindex());

                   cntTargetSyncTemp.setOperresult(70);
                   cntTargetSyncTemp.setStatus(200);
                   cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                   CommonLogUtil.insertOperatorLog(seriesProgramMapList.get(0).getMappingid()+","+
                   seriesProgramMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                           CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL_INFO,
                           CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                             			
 
 	       }
		   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
           int[] targetSyncStatus = new int[cntTargetSyncList.size()];
           for(int i = 0;i<cntTargetSyncList.size() ;i++)
           {
           	   targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
           }
           int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
           if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
           {
               CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
               cntPlatformSyncTmp.setObjectindex(seriesProgramMap.getMapindex());
               cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
               cntPlatformSyncTmp.setStatus(status);
               cntPlatformSyncTmp.setPlatform(platformType);
               cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
           } 
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result;   
    }   
    
    public String initData(SeriesProgramMap seriesProgramMap,String targetindex,int platfType,int synType)throws Exception
    {
    	try
    	{
    		CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(seriesProgramMap.getMapindex());
			cntPlatformSync.setObjecttype(36);
			CmsSeries cmsSeries = new CmsSeries();
			cmsSeries.setSeriesindex(seriesProgramMap.getSeriesindex());
			cmsSeries =cmsSeriesDS.getCmsSeries(cmsSeries);
    		if (cmsSeries == null)
    		{
                return "1:操作失败,连续剧不存在";
    		}
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(seriesProgramMap.getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		if (cmsProgram == null)
    		{
                return "1:操作失败,单集不存在";
    		}
    		if(platfType == 1 ||platfType == 2) //发布到3.0平台 或者2.0平台
    		{
        		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();

        		if (platfType == 1)
        		{
        			cntPlatformSync.setPlatform(2);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        			
        		}

        		if (platfType == 2)
        		{
        			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
        		
    			if (synType == 1) //发布
    			{
    				//状态为待发布、发布失败、修改发布失败才能发布
    				if(cntPlatformSynctmp.getStatus()!=0&&cntPlatformSynctmp.getStatus()!=400&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				//状态为发布成功、修改发布失败才能取消发布
    				if(cntPlatformSynctmp.getStatus()!=300&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    			
                /**
                 * 获取关联目标网元及状态
                 * */
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null||cntTargetSyncList.size()==0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType ==1)
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==0||cntTargetSync.getStatus()==400||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,获取目标网元失败或目标网元发布状态不正确";

                	}
                }
                else
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==300||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,获取目标网元失败或目标网元发布状态不正确";

                	}
                }                
    		}
    		if (platfType == 3) //发布到网元
    		{
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
    			CntTargetSync cntTargetSync = new CntTargetSync();
    			cntTargetSync.setObjectindex(seriesProgramMap.getMapindex());
    			cntTargetSync.setObjecttype(36);
    			cntTargetSync.setTargetindex(Long.valueOf(targetindex));
    			cntTargetSyncListTemp =cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
    			if (cntTargetSyncListTemp==null ||cntTargetSyncListTemp.size()==0)
    			{
                    return "1:操作失败,发布记录不存在或目标网元状态不正确";
    			}
    			if (synType == 1) //发布
    			{
    				if(cntTargetSyncListTemp.get(0).getStatus()!=0&&cntTargetSyncListTemp.get(0).getStatus()!=400
    						&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				if(cntTargetSyncListTemp.get(0).getStatus()!=300&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    		}
            List<UsysConfig> usysConfigList = null;
            UsysConfig usysConfig = new UsysConfig();
            usysConfig = usysConfigDS.getUsysConfigByCfgkey(FTPADDRESS);
            if (null == usysConfig || null == usysConfig.getCfgvalue()
                || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return "1:操作失败,获取同步内容实体文件FTP地址失败";
            } 
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return "success";
    }
    private void insertMappingRecord(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(36);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
     
    
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype, Long targetindex,int synType,String objectcode,int platType )
    {
    	ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncRecord.setSyncindex(syncindex);
        	objectSyncRecord.setTaskindex(taskindex);
        	objectSyncRecord.setObjectindex(objectindex);
        	objectSyncRecord.setObjectid(objectid);
        	objectSyncRecord.setDestindex(targetindex);
        	objectSyncRecord.setObjecttype(objecttype);
        	objectSyncRecord.setActiontype(synType);
        	objectSyncRecord.setObjectcode(objectcode);
        	objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecord.setObjectcode(objectcode);
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
    
    private void insertSyncPicTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid,int status)
    {
    	CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(status);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntSyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    
    private void insertMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, 
    		String elementid, String parentid,String elmentcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(36);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elmentcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }  	
	  
	
	
	/*************************连续剧关联单集发布 end ************************/
}

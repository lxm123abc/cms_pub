package com.zte.cms.channel.common;

/**
 * 
 * 节目单和收录计划模块状态、日志记录常量类
 * 
 */
public class CmsScheduleConstant
{

    // 100初始 110待审核 120审核中 130审核失败 0待发布 10新增同步中 20同步成功
    // 30同步失败 40修改同步中 50修改同步成功 60修改同步失败 70取消同步中 80取消同步成功 90取消同步失败
    public static final int STATUS_100 = 100;
    public static final int STATUS_110 = 110;
    public static final int STATUS_120 = 120;
    public static final int STATUS_130 = 130;
    public static final int STATUS_0 = 0;
    public static final int STATUS_10 = 10;
    public static final int STATUS_20 = 20;
    public static final int STATUS_30 = 30;
    public static final int STATUS_40 = 40;
    public static final int STATUS_50 = 50;
    public static final int STATUS_60 = 60;
    public static final int STATUS_70 = 70;
    public static final int STATUS_80 = 80;
    public static final int STATUS_90 = 90;
    public static final int STATUS_65 = 65;

    // 100初始 110待审核 120审核中 130审核失败 0待发布 10新增同步中 20同步成功
    // 30同步失败 40修改同步中 50修改同步成功 60修改同步失败 70取消同步中 80取消同步成功 90取消同步失败
    public static final String RESULT_CODE_110 = "110";
    public static final String RESULT_CODE_111 = "111";
    public static final String RESULT_CODE_112 = "112";
    public static final String RESULT_CODE_113 = "113";
    public static final String RESULT_CODE_114 = "114";
    public static final String RESULT_CODE_115 = "115";
    public static final String RESULT_CODE_116 = "116";
    public static final String RESULT_CODE_117 = "117";
    public static final String RESULT_CODE_118 = "118";
    public static final String RESULT_CODE_119 = "119";
    public static final String RESULT_CODE_201 = "201";
    public static final String RESULT_CODE_202 = "202";
    public static final String RESULT_CODE_203 = "203";
    public static final String RESULT_CODE_911 = "911";
    public static final String RESULT_CODE_912 = "912";
    public static final String RESULT_CODE_913 = "913";
    public static final String RESULT_CODE_914 = "914";
    public static final String RESULT_CODE_915 = "915";
    public static final String RESULT_CODE_916 = "916";
    public static final String RESULT_CODE_921 = "921";
    public static final String RESULT_CODE_922 = "922";
    public static final String RESULT_CODE_923 = "923";
    /*******************************************************************************************************************
     * 节目单业务日志
     ******************************************************************************************************************/
    /**
     * mgttype 节目单管理
     */
    public static final String MGTTYPE_CHANNELSCHEDULE = "log.channelSchedule.mgt";
    public static final String MGTTYPE_SCHEDULERECOR = "log.cmsSchedulerecord.mgt";

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_CMSSCHEDULE_ADJUSTTIME = "log.cmsSchedule.adjusttime";// 节目单时间调整
    public static final String OPERTYPE_CMSSCHEDULE_ADJUSTTIME_INFO = "log.cmsSchedule.adjusttime.info";// 节目单时间调整

    public static final String OPERTYPE_CMSSCHEDULE_AUDIT = "log.cmsSchedule.audit";// 节目单审核
    public static final String OPERTYPE_CMSSCHEDULE_AUDIT_INFO = "log.cmsSchedule.audit.info";// 节目单审核

    public static final String OPERTYPE_CMSSCHEDULERECORD_ADD = "log.cmsSchedulerecord.add";// 节目单时间调整
    public static final String OPERTYPE_CMSSCHEDULERECORD_ADD_INFO = "log.cmsSchedulerecord.add.info";// 节目单时间调整

    public static final String OPERTYPE_CMSSCHEDULERECORD_DEL = "log.cmsSchedulerecord.del";// 节目单时间调整
    public static final String OPERTYPE_CMSSCHEDULERECORD_DEL_INFO = "log.cmsSchedulerecord.del.info";// 节目单时间调整

    public static final String OPERTYPE_CMSSCHEDULERECORD_PUBLISH = "log.cmsSchedulerecord.publish";// 节目单时间调整
    public static final String OPERTYPE_CMSSCHEDULERECORD_PUBLISH_INFO = "log.cmsSchedulerecord.publish.info";// 节目单时间调整

    public static final String OPERTYPE_CMSSCHEDULERECORD_CANCLEPUBLISH = "log.cmsSchedulerecord.canclepublish";// 节目单时间调整
    public static final String OPERTYPE_CMSSCHEDULERECORD_CANCLEPUBLISH_INFO = "log.cmsSchedulerecord.canclepublish.info";// 节目单时间调整
    // 内容管理操作结果资源标识
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_CHANNELSCHEDULE_ADD = "log.channelSchedule.add";// 节目单新增
    public static final String OPERTYPE_CHANNELSCHEDULE_MOD = "log.channelSchedule.modify";// 节目单修改
    public static final String OPERTYPE_CHANNELSCHEDULE_DEL = "log.channelSchedule.remove";// 节目单删除
    public static final String OPERTYPE_CHANNELSCHEDULE_ID = "log.channelSchedule.id";// 节目单编码
    public static final String OPERTYPE_CHANNELSCHEDULE_RESON = "log.channelSchedule.reson";// 原因
    public static final String OPERTYPE_CHANNELSCHEDULE_UPDATERESON = "log.channelSchedule.updatereson";// 原因
    public static final String OPERTYPE_CHANNELSCHEDULE_STATUS = "log.channelSchedule.status";// 状态
    public static final String OPERTYPE_CHANNELSCHEDULE_PUBLISH = "log.channelSchedule.publish";// 节目单同步
    public static final String OPERTYPE_CHANNELSCHEDULE_NOPUBLISH = "log.channelSchedule.nopublish";// 取消节目单同步
    public static final String OPERTYPE_CHANNELSCHEDULE_UPRECORDING = "log.record.schedule.uprecording";// 取消收录节目单
    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_ADD = "log.signalChannelSchedule.add";// 节目单新增
    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_MOD = "log.signalChannelSchedule.modify";// 节目单修改
    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_DEL = "log.signalChannelSchedule.remove";// 节目单删除
    public static final String OPERTYPE_LIVERECIEVE = "log.liverecieve.mgt";// 收录管理

    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_CHANNELSCHEDULE_ADD_INFO = "log.channelSchedule.add.info";
    public static final String OPERTYPE_CHANNELSCHEDULE_UPRECORDING_INFO = "log.record.schedule.uprecording.info";
    public static final String OPERTYPE_CHANNELSCHEDULE_MOD_INFO = "log.channelSchedule.modify.info";
    public static final String OPERTYPE_CHANNELSCHEDULE_DEL_INFO = "log.channelSchedule.remove.info";
    public static final String OPERTYPE_CHANNELSCHEDULE_PUBLISH_INFO = "log.channelSchedule.publish.info";// 节目单同步
    public static final String OPERTYPE_CHANNELSCHEDULE_NOPUBLISH_INFO = "log.channelSchedule.nopublish.info";// 取消节目单同步

    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_ADD_INFO = "log.signalChannelSchedule.add.info";// 节目单新增
    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_MOD_INFO = "log.signalChannelSchedule.modify.info";// 节目单修改
    public static final String OPERTYPE_SIGNALCHANNELSCHEDULE_DEL_INFO = "log.signalChannelSchedule.remove.info";// 节目单删除

}

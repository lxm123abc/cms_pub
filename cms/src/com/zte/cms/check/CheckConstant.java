package com.zte.cms.check;

public class CheckConstant
{

    // 资源文件名
    public static final String CMS_SERVER_RESOURCE = "cms_server_resource";// 资源文件名

    /** 触发器名称 * */
    public static final String CHECK_CYCLE_TRIGGER_NAME = "cms.cycle.trigger.name";
    /** 组名称 * */
    public static final String CHECK_CYCLE_GROUP_NAME = "cms.cycle.trigger.groupname";
    /** JOB名称 * */
    public static final String CECK_CYCLE_JOB_NAME = "cms.cycle.trigger.jobname";
    /** 定时器是否需要生效* */
    public static final String CECK_CYCLE_ENABLE = "enable.timer";
    // 查看硬盘使用情况的时候，服务器的信息不存在
    public static final String SERVER_DOES_NOT_EXIST_IN_DATEBASE = "server.does.not.exist";
    // 服务器连接失败
    public static final String SERVER_CONNECTION_FAILED = "server.connecttion.failed";
    /** 任务触发的执行的beanid * */
    public static final String CHECK_JOB_BEANID = "cyclejob";

    /** 数据库服务器的ip地址 * */
    public static final String DB_SERVER_IP = "db.ip";
    /** 线程休眠时间长度 * */
    public static final String THREAD_SLEEP_TIME = "thread.sleep";
    // 文件系统
    public static final String CMS_SERVER_MONITOR_FILE_SYSTEM = "cms.server.monitor.file.system";
    // 总大小
    public static final String CMS_SERVER_MONITOR_SIZE = "cms.server.monitor.size";
    // 已用
    public static final String CMS_SERVER_MONITOR_USED = "cms.server.monitor.used";
    // 可用
    public static final String CMS_SERVER_MONITOR_AVALIABLE = "cms.server.monitor.avaliable";
    // 挂载点
    public static final String CMS_SERVER_MONITOR_MOUNTED_ON = "cms.server.monitor.mounted.on";
    // 使用百分比
    public static final String CMS_SERVER_MONITOR_USE_PERCENTAGE = "cms.server.monitor.use.percentagy";

}

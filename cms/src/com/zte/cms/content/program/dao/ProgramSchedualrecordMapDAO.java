
  package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.dao.IProgramSchedualrecordMapDAO;
import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ProgramSchedualrecordMapDAO extends DynamicObjectBaseDao implements IProgramSchedualrecordMapDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("insert programSchedualrecordMap starting...");
    	super.insert( "insertProgramSchedualrecordMap", programSchedualrecordMap );
    	log.debug("insert programSchedualrecordMap end");
    }
     
    public void updateProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("update programSchedualrecordMap by pk starting...");
       	super.update( "updateProgramSchedualrecordMap", programSchedualrecordMap );
       	log.debug("update programSchedualrecordMap by pk end");
    }

    public void updateProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("update programSchedualrecordMap by conditions starting...");
    	super.update( "updateProgramSchedualrecordMapByCond", programSchedualrecordMap );
    	log.debug("update programSchedualrecordMap by conditions end");
    }   

    public void deleteProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("delete programSchedualrecordMap by pk starting...");
       	super.delete( "deleteProgramSchedualrecordMap", programSchedualrecordMap );
       	log.debug("delete programSchedualrecordMap by pk end");
    }

    public void deleteProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("delete programSchedualrecordMap by conditions starting...");
    	super.delete( "deleteProgramSchedualrecordMapByCond", programSchedualrecordMap );
    	log.debug("update programSchedualrecordMap by conditions end");
    }    

    public ProgramSchedualrecordMap getProgramSchedualrecordMap( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("query programSchedualrecordMap starting...");
       	ProgramSchedualrecordMap resultObj = (ProgramSchedualrecordMap)super.queryForObject( "getProgramSchedualrecordMap",programSchedualrecordMap);
       	log.debug("query programSchedualrecordMap end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<ProgramSchedualrecordMap> getProgramSchedualrecordMapByCond( ProgramSchedualrecordMap programSchedualrecordMap )throws DAOException
    {
    	log.debug("query programSchedualrecordMap by condition starting...");
    	List<ProgramSchedualrecordMap> rList = (List<ProgramSchedualrecordMap>)super.queryForList( "queryProgramSchedualrecordMapListByCond",programSchedualrecordMap);
    	log.debug("query programSchedualrecordMap by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)throws DAOException
    {
    	log.debug("page query programSchedualrecordMap by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryProgramSchedualrecordMapListCntByCond",  programSchedualrecordMap)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<ProgramSchedualrecordMap> rsList = (List<ProgramSchedualrecordMap>)super.pageQuery( "queryProgramSchedualrecordMapListByCond" ,  programSchedualrecordMap , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query programSchedualrecordMap by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryProgramSchedualrecordMapListByCond", "queryProgramSchedualrecordMapListCntByCond", programSchedualrecordMap, start, pageSize, puEntity);
    }
    
} 
package com.zte.cms.ctgpgmap.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategoryProgramMapDAO
{
    /**
     * 新增CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DAOException dao异常
     */
    public void insertCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException;

    /**
     * 根据主键更新CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DAOException dao异常
     */
    public void updateCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException;

    /**
     * 根据主键删除CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @throws DAOException dao异常
     */
    public void deleteCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException;

    /**
     * 根据主键查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @return 满足条件的CategoryProgramMap对象
     * @throws DAOException dao异常
     */
    public CategoryProgramMap getCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DAOException;

    /**
     * 根据条件查询CategoryProgramMap对象
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @return 满足条件的CategoryProgramMap对象集
     * @throws DAOException dao异常
     */
    public List<CategoryProgramMap> getCategoryProgramMapByCond(CategoryProgramMap categoryProgramMap)
            throws DAOException;

    /**
     * 根据条件分页查询CategoryProgramMap对象，作为查询条件的参数
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CategoryProgramMap对象，作为查询条件的参数
     * 
     * @param categoryProgramMap CategoryProgramMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;
    /****** publish management *************/
    
    /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DAOException;    
    
     /**
 	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
 	  *
 	  * @param programPlatformSyn CntTargetsysSyn对象 
 	  * @param start 起始行
 	  * @param pageSize 页面大小
 	  * @return  查询结果
 	  * @throws DAOException dao异常
 	  */
      public PageInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DAOException;    
        

     /**
      * 根据条件查询CategoryProgramMap对象
      * 
      * @param categoryProgramMap CategoryProgramMap对象
      * @return 满足条件的CategoryProgramMap对象集
      * @throws DAOException dao异常
      */
     public List<CategoryProgramMap> getnormaltargetSynListByCond(CategoryProgramMap categoryProgramMap)
             throws DAOException;     
     
    /****** publish management end ********/    
}
package com.zte.ismp.applog.ls;

import net.sf.cglib.beans.BeanCopier;

import com.zte.ismp.applog.service.IAppLogDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.model.VSsblog;

/**
 * <p>
 * 文件名称: AppLogLS.java
 * </p>
 * <p>
 * 文件描述: 业务日志管理类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2010
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要完成业务日志的查询功能
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2010年08月10日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXV10 TVMSV1.01.01
 * @author 李梅
 */
public class AppLogLS implements IAppLogLS
{
    private IAppLogDS appLogDS = null;
    private Log log = SSBBus.getLog(getClass());

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(AppLogInfoEntity appLog, int start, int pageSize) throws Exception
    {
        log.debug("AppLogLS: pageInfoQuery starts");
        TableDataInfo data = null;
        PageUtilEntity puEntity = null;
        try
        {
            // 模糊查询字段转换
            if (appLog != null)
            {
                appLog.setUserId(EspecialCharMgt.conversion(appLog.getUserId()));
                appLog.setUserName(EspecialCharMgt.conversion(appLog.getUserName()));
                appLog.setOptObjecttype(EspecialCharMgt.conversion(appLog.getOptObjecttype()));
                appLog.setOptObject(EspecialCharMgt.conversion(appLog.getOptObject()));
                appLog.setOptType(EspecialCharMgt.conversion(appLog.getOptType()));
                appLog.setUserIp(EspecialCharMgt.conversion(appLog.getUserIp()));
            }

            puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("optTime");

            VSsblog vSsblog = new VSsblog();
            BeanCopier copier = BeanCopier.create(AppLogInfoEntity.class, VSsblog.class, false);
            copier.copy(appLog, vSsblog, null);

            data = appLogDS.pageInfoQuery(vSsblog, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            log.error("Error occurred in pageInfoQuery method", e);
            throw new Exception("0462299");// 查询失败
        }

        log.debug("AppLogLS: pageInfoQuery ends");
        return data;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(AppLogInfoEntity appLog, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception
    {
        log.debug("AppLogLS: pageInfoQuery starts");
        TableDataInfo data = null;
        try
        {
            // 模糊查询字段转换
            if (appLog != null)
            {
                appLog.setUserId(EspecialCharMgt.conversion(appLog.getUserId()));
                appLog.setUserName(EspecialCharMgt.conversion(appLog.getUserName()));
                appLog.setOptObjecttype(EspecialCharMgt.conversion(appLog.getOptObjecttype()));
                appLog.setOptObject(EspecialCharMgt.conversion(appLog.getOptObject()));
                appLog.setOptType(EspecialCharMgt.conversion(appLog.getOptType()));
                appLog.setUserIp(EspecialCharMgt.conversion(appLog.getUserIp()));
            }

            puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("optTime");

            VSsblog vSsblog = new VSsblog();
            BeanCopier copier = BeanCopier.create(AppLogInfoEntity.class, VSsblog.class, false);
            copier.copy(appLog, vSsblog, null);

            data = appLogDS.pageInfoQuery(vSsblog, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            log.error("Error occurred in pageInfoQuery method", e);
            throw new Exception("0462299");// 查询失败
        }

        log.debug("AppLogLS: pageInfoQuery ends");
        return data;
    }

    public void setAppLogDS(IAppLogDS appLogDS)
    {
        this.appLogDS = appLogDS;
    }
}

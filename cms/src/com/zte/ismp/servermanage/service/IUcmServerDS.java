package com.zte.ismp.servermanage.service;

import java.util.List;

import com.zte.umap.ucms.model.UcmServer;

public interface IUcmServerDS
{
    /**
     * <p>
     * 查询系统中所有的资源服务器记录
     * </p>
     * 
     * @return 返回服务器记录集合
     *         <p>
     *         完成日期：2010年7月30日
     *         </p>
     */
    public List<UcmServer> getResServer() throws Exception;
}

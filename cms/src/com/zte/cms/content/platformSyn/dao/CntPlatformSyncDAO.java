
  package com.zte.cms.content.platformSyn.dao;

import java.util.List;

import com.zte.cms.content.platformSyn.dao.ICntPlatformSyncDAO;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CntPlatformSyncDAO extends DynamicObjectBaseDao implements ICntPlatformSyncDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("insert cntPlatformSync starting...");
    	super.insert( "insertCntPlatformSync", cntPlatformSync );
    	log.debug("insert cntPlatformSync end");
    }
     
    public void updateCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("update cntPlatformSync by pk starting...");
       	super.update( "updateCntPlatformSync", cntPlatformSync );
       	log.debug("update cntPlatformSync by pk end");
    }


    public void deleteCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("delete cntPlatformSync by pk starting...");
       	super.delete( "deleteCntPlatformSync", cntPlatformSync );
       	log.debug("delete cntPlatformSync by pk end");
    }


    public CntPlatformSync getCntPlatformSync( CntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("query cntPlatformSync starting...");
       	CntPlatformSync resultObj = (CntPlatformSync)super.queryForObject( "getCntPlatformSync",cntPlatformSync);
       	log.debug("query cntPlatformSync end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<CntPlatformSync> getCntPlatformSyncByCond( CntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("query cntPlatformSync by condition starting...");
    	List<CntPlatformSync> rList = (List<CntPlatformSync>)super.queryForList( "queryCntPlatformSyncListByCond",cntPlatformSync);
    	log.debug("query cntPlatformSync by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntPlatformSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryCntPlatformSyncListCntByCond",  cntPlatformSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CntPlatformSync> rsList = (List<CntPlatformSync>)super.pageQuery( "queryCntPlatformSyncListByCond" ,  cntPlatformSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntPlatformSync by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(DynamicBaseObject cntPlatformSync, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryCntPlatformSyncListByCond", "queryCntPlatformSyncListCntByCond", cntPlatformSync, start, pageSize, puEntity);
    }

	public PageInfo pageInfoQuery(CntPlatformSync cntPlatformSync, int start,
			int pageSize, PageUtilEntity puEntity) throws DAOException
	{
		// TODO Auto-generated method stub
		return null;
	}
    
	public void deleteCntPlatformSyncByObjindex( CntPlatformSync cntPlatformSync )throws DAOException
    {
        log.debug("delete cntPlatformSync by conditions starting...");
        super.delete( "deleteCntPlatformSyncByObjindex", cntPlatformSync );
        log.debug("update cntPlatformSync by conditions end");
    }

    public CntPlatformSync getCntPlatformSync4Object(CntPlatformSync cntPlatformSync) throws DAOException
    {
        // TODO Auto-generated method stub
        log.debug("query cntPlatformSync4Object starting...");
        CntPlatformSync resultObj = (CntPlatformSync)super.queryForObject( "getCntPlatformSync4Object",cntPlatformSync);
        log.debug("query cntPlatformSync4Object end");
        return resultObj;
    }

    public int getCntPlatformSyncChildNumber(CntPlatformSync cntPlatformSync) throws DAOException
    {
        // TODO Auto-generated method stub
        return ((Integer) super.queryForObject("queryCntPlatformChildTargetsystemByCond",  cntPlatformSync)).intValue();
    }
} 
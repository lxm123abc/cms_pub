package com.zte.cms.basicdata.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsExtractedvafileInfo extends DynamicBaseObject
{
    private java.lang.Long fileindex;
    private java.lang.Integer sourcetype;
    private java.lang.Long streammediatype;
    private java.lang.Long streammediasubtype;
    private java.lang.Long isincludevideo;
    private java.lang.Long isincludeaudio;
    private java.lang.Long videocodingformat;
    private java.lang.Long videosubtype;
    private java.lang.Long cxsize;
    private java.lang.Long cysize;
    private java.lang.Long standardrate;
    private java.lang.Long standardscale;
    private java.lang.Long scanmode;
    private java.lang.Long colorformat;
    private java.lang.Long videobitrate;
    private java.lang.Long isconstantrate;
    private java.lang.Long gopsize;
    private java.lang.Long referenceperiod;
    private java.lang.Long isy16235;
    private java.lang.Long audiocodingformat;
    private java.lang.Long audiosubtype;
    private java.lang.Long channels;
    private java.lang.Long audiodatarate;
    private java.lang.Long audiosamplingfreq;
    private java.lang.Long audiobitdepth;
    private java.lang.Long blockalign;
    private java.lang.Long filesize;
    private java.lang.Long audiosamples;
    private java.lang.Long videoframes;
    private java.lang.String exattribute1;
    private java.lang.String exattribute2;
    private java.lang.String exattribute3;
    private java.lang.String exattribute4;
    private java.lang.String exattribute5;

    public java.lang.Long getFileindex()
    {
        return fileindex;
    }

    public void setFileindex(java.lang.Long fileindex)
    {
        this.fileindex = fileindex;
    }

    public java.lang.Integer getSourcetype()
    {
        return sourcetype;
    }

    public void setSourcetype(java.lang.Integer sourcetype)
    {
        this.sourcetype = sourcetype;
    }

    public java.lang.Long getStreammediatype()
    {
        return streammediatype;
    }

    public void setStreammediatype(java.lang.Long streammediatype)
    {
        this.streammediatype = streammediatype;
    }

    public java.lang.Long getStreammediasubtype()
    {
        return streammediasubtype;
    }

    public void setStreammediasubtype(java.lang.Long streammediasubtype)
    {
        this.streammediasubtype = streammediasubtype;
    }

    public java.lang.Long getIsincludevideo()
    {
        return isincludevideo;
    }

    public void setIsincludevideo(java.lang.Long isincludevideo)
    {
        this.isincludevideo = isincludevideo;
    }

    public java.lang.Long getIsincludeaudio()
    {
        return isincludeaudio;
    }

    public void setIsincludeaudio(java.lang.Long isincludeaudio)
    {
        this.isincludeaudio = isincludeaudio;
    }

    public java.lang.Long getVideocodingformat()
    {
        return videocodingformat;
    }

    public void setVideocodingformat(java.lang.Long videocodingformat)
    {
        this.videocodingformat = videocodingformat;
    }

    public java.lang.Long getVideosubtype()
    {
        return videosubtype;
    }

    public void setVideosubtype(java.lang.Long videosubtype)
    {
        this.videosubtype = videosubtype;
    }

    public java.lang.Long getCxsize()
    {
        return cxsize;
    }

    public void setCxsize(java.lang.Long cxsize)
    {
        this.cxsize = cxsize;
    }

    public java.lang.Long getCysize()
    {
        return cysize;
    }

    public void setCysize(java.lang.Long cysize)
    {
        this.cysize = cysize;
    }

    public java.lang.Long getStandardrate()
    {
        return standardrate;
    }

    public void setStandardrate(java.lang.Long standardrate)
    {
        this.standardrate = standardrate;
    }

    public java.lang.Long getStandardscale()
    {
        return standardscale;
    }

    public void setStandardscale(java.lang.Long standardscale)
    {
        this.standardscale = standardscale;
    }

    public java.lang.Long getScanmode()
    {
        return scanmode;
    }

    public void setScanmode(java.lang.Long scanmode)
    {
        this.scanmode = scanmode;
    }

    public java.lang.Long getColorformat()
    {
        return colorformat;
    }

    public void setColorformat(java.lang.Long colorformat)
    {
        this.colorformat = colorformat;
    }

    public java.lang.Long getVideobitrate()
    {
        return videobitrate;
    }

    public void setVideobitrate(java.lang.Long videobitrate)
    {
        this.videobitrate = videobitrate;
    }

    public java.lang.Long getIsconstantrate()
    {
        return isconstantrate;
    }

    public void setIsconstantrate(java.lang.Long isconstantrate)
    {
        this.isconstantrate = isconstantrate;
    }

    public java.lang.Long getGopsize()
    {
        return gopsize;
    }

    public void setGopsize(java.lang.Long gopsize)
    {
        this.gopsize = gopsize;
    }

    public java.lang.Long getReferenceperiod()
    {
        return referenceperiod;
    }

    public void setReferenceperiod(java.lang.Long referenceperiod)
    {
        this.referenceperiod = referenceperiod;
    }

    public java.lang.Long getIsy16235()
    {
        return isy16235;
    }

    public void setIsy16235(java.lang.Long isy16235)
    {
        this.isy16235 = isy16235;
    }

    public java.lang.Long getAudiocodingformat()
    {
        return audiocodingformat;
    }

    public void setAudiocodingformat(java.lang.Long audiocodingformat)
    {
        this.audiocodingformat = audiocodingformat;
    }

    public java.lang.Long getAudiosubtype()
    {
        return audiosubtype;
    }

    public void setAudiosubtype(java.lang.Long audiosubtype)
    {
        this.audiosubtype = audiosubtype;
    }

    public java.lang.Long getChannels()
    {
        return channels;
    }

    public void setChannels(java.lang.Long channels)
    {
        this.channels = channels;
    }

    public java.lang.Long getAudiodatarate()
    {
        return audiodatarate;
    }

    public void setAudiodatarate(java.lang.Long audiodatarate)
    {
        this.audiodatarate = audiodatarate;
    }

    public java.lang.Long getAudiosamplingfreq()
    {
        return audiosamplingfreq;
    }

    public void setAudiosamplingfreq(java.lang.Long audiosamplingfreq)
    {
        this.audiosamplingfreq = audiosamplingfreq;
    }

    public java.lang.Long getAudiobitdepth()
    {
        return audiobitdepth;
    }

    public void setAudiobitdepth(java.lang.Long audiobitdepth)
    {
        this.audiobitdepth = audiobitdepth;
    }

    public java.lang.Long getBlockalign()
    {
        return blockalign;
    }

    public void setBlockalign(java.lang.Long blockalign)
    {
        this.blockalign = blockalign;
    }

    public java.lang.Long getFilesize()
    {
        return filesize;
    }

    public void setFilesize(java.lang.Long filesize)
    {
        this.filesize = filesize;
    }

    public java.lang.Long getAudiosamples()
    {
        return audiosamples;
    }

    public void setAudiosamples(java.lang.Long audiosamples)
    {
        this.audiosamples = audiosamples;
    }

    public java.lang.Long getVideoframes()
    {
        return videoframes;
    }

    public void setVideoframes(java.lang.Long videoframes)
    {
        this.videoframes = videoframes;
    }

    public java.lang.String getExattribute1()
    {
        return exattribute1;
    }

    public void setExattribute1(java.lang.String exattribute1)
    {
        this.exattribute1 = exattribute1;
    }

    public java.lang.String getExattribute2()
    {
        return exattribute2;
    }

    public void setExattribute2(java.lang.String exattribute2)
    {
        this.exattribute2 = exattribute2;
    }

    public java.lang.String getExattribute3()
    {
        return exattribute3;
    }

    public void setExattribute3(java.lang.String exattribute3)
    {
        this.exattribute3 = exattribute3;
    }

    public java.lang.String getExattribute4()
    {
        return exattribute4;
    }

    public void setExattribute4(java.lang.String exattribute4)
    {
        this.exattribute4 = exattribute4;
    }

    public java.lang.String getExattribute5()
    {
        return exattribute5;
    }

    public void setExattribute5(java.lang.String exattribute5)
    {
        this.exattribute5 = exattribute5;
    }

    public void initRelation()
    {
        this.addRelation("fileindex", "FILEINDEX");
        this.addRelation("sourcetype", "SOURCETYPE");
        this.addRelation("streammediatype", "STREAMMEDIATYPE");
        this.addRelation("streammediasubtype", "STREAMMEDIASUBTYPE");
        this.addRelation("isincludevideo", "ISINCLUDEVIDEO");
        this.addRelation("isincludeaudio", "ISINCLUDEAUDIO");
        this.addRelation("videocodingformat", "VIDEOCODINGFORMAT");
        this.addRelation("videosubtype", "VIDEOSUBTYPE");
        this.addRelation("cxsize", "CXSIZE");
        this.addRelation("cysize", "CYSIZE");
        this.addRelation("standardrate", "STANDARDRATE");
        this.addRelation("standardscale", "STANDARDSCALE");
        this.addRelation("scanmode", "SCANMODE");
        this.addRelation("colorformat", "COLORFORMAT");
        this.addRelation("videobitrate", "VIDEOBITRATE");
        this.addRelation("isconstantrate", "ISCONSTANTRATE");
        this.addRelation("gopsize", "GOPSIZE");
        this.addRelation("referenceperiod", "REFERENCEPERIOD");
        this.addRelation("isy16235", "ISY16235");
        this.addRelation("audiocodingformat", "AUDIOCODINGFORMAT");
        this.addRelation("audiosubtype", "AUDIOSUBTYPE");
        this.addRelation("channels", "CHANNELS");
        this.addRelation("audiodatarate", "AUDIODATARATE");
        this.addRelation("audiosamplingfreq", "AUDIOSAMPLINGFREQ");
        this.addRelation("audiobitdepth", "AUDIOBITDEPTH");
        this.addRelation("blockalign", "BLOCKALIGN");
        this.addRelation("filesize", "FILESIZE");
        this.addRelation("audiosamples", "AUDIOSAMPLES");
        this.addRelation("videoframes", "VIDEOFRAMES");
        this.addRelation("exattribute1", "EXATTRIBUTE1");
        this.addRelation("exattribute2", "EXATTRIBUTE2");
        this.addRelation("exattribute3", "EXATTRIBUTE3");
        this.addRelation("exattribute4", "EXATTRIBUTE4");
        this.addRelation("exattribute5", "EXATTRIBUTE5");
    }
}

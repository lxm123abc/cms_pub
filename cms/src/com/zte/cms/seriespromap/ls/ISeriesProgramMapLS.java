package com.zte.cms.seriespromap.ls;

import java.util.List;

import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ISeriesProgramMapLS
{
    /**
     * 添加连续剧单集关系
     * 
     * @param seriesProgramMap 连续剧单集关系对象
     * @return 操作结果
     * @throws DomainServiceException ls异常
     */
    public String insertSeriesProgramMap(SeriesProgramMap seriesProgramMap) throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryForFrontPage(SeriesProgramMap seriesProgramMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 批量发布连续剧单集关联
     * 
     * @param mapIndexs 关联主键
     * @return 批量操作结果
     * @throws DomainServiceException ls异常
     */
    public String batchPublishMap(List<String> mapIndexs) throws Exception;

    /**
     * 批量取消发布连续剧单集关联
     * 
     * @param mapIndexs 关联主键
     * @return 批量操作结果
     * @throws DomainServiceException ls异常
     */
    public String batchCancelPublishMap(List<String> mapIndexs) throws Exception;

    /**
     * 根据关联主键删除关联关系
     * 
     * @param mapindex 连续剧单集关联主键
     * @return 删除操作结果
     * @throws Exception;
     */
    public String removeSeriesProgramMapByIndex(long mapindex) throws Exception;
    /**************************连续剧海报发布管理  begin*********************/
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */    
    public TableDataInfo pageInfoQueryseriesProMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws Exception;
    
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */    
    public TableDataInfo pageInfoQueryseriesProTargetMap(SeriesProgramMap seriesProgramMap, int start, int pageSize) throws Exception;
 
    //批量发布或取消发布连续剧关联点播内容
    public String batchPublishOrDelPublishseriesPro(List<SeriesProgramMap> list,String operType, int type) throws Exception;
    
    //发布连续剧关联点播内容到平台
    public String publishseriesProMapPlatformSyn(String mapindex, String seriesid,String programid,int type,int synType) throws Exception;

    //取消发布连续剧关联点播内容到平台
    public String delPublishseriesProMapPlatformSyn(String mapindex, String seriesid,String programid,int type,int synType) throws Exception;

    //取消发布连续剧关联点播内容到网元
    public String publishseriesProMapTarget(String mapindex, String targetindex,int type) throws Exception;

    //取消发布连续剧关联点播内容到网元
    public String delPublishseriesProMapTarget(String mapindex, String targetindex,int type) throws Exception;

    
    
    /**************************连续剧海报发布管理  end*********************/
}

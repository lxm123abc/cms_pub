package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsScheduleBufDS
{
    /**
	 * 新增CmsScheduleBuf对象
	 *
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常
	 */	
	public void insertCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException;
	  
    /**
	 * 更新CmsScheduleBuf对象
	 *
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException;

    /**
	 * 批量更新CmsScheduleBuf对象
	 *
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void updateCmsScheduleBufList( List<CmsScheduleBuf> cmsScheduleBufList )throws DomainServiceException;



	/**
	 * 删除CmsScheduleBuf对象
	 *
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException;

	/**
	 * 批量删除CmsScheduleBuf对象
	 *
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常
	 */
	public void removeCmsScheduleBufList( List<CmsScheduleBuf> cmsScheduleBufList )throws DomainServiceException;




	/**
	 * 查询CmsScheduleBuf对象
	 
	 * @param cmsScheduleBuf CmsScheduleBuf对象
	 * @return CmsScheduleBuf对象
	 * @throws DomainServiceException ds异常 
	 */
	 public CmsScheduleBuf getCmsScheduleBuf( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException; 
	 
	 /**
	  * 根据条件查询CmsScheduleBuf对象 
	  * 
	  * @param cmsScheduleBuf CmsScheduleBuf对象
	  * @return 满足条件的CmsScheduleBuf对象集
	  * @throws DomainServiceException ds异常
	  */
     public List<CmsScheduleBuf> getCmsScheduleBufByCond( CmsScheduleBuf cmsScheduleBuf )throws DomainServiceException;

	 /**
	  * 根据条件分页查询CmsScheduleBuf对象 
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CmsScheduleBuf对象 
	  *
	  * @param cmsScheduleBuf CmsScheduleBuf对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CmsScheduleBuf cmsScheduleBuf, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
}

package com.zte.ucm.chkconf;

import com.zte.ucm.Constants;
import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.config.ConfigItem;
import com.zte.ucm.config.ConfigParser;
import com.zte.ucm.config.XMLParser;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;
import com.zte.ucm.util.FileUtil;

import java.util.HashMap;
import java.util.List;

public class ChkAssign
{
  private static Logger log = LoggerFactory.getLogger(ChkAssign.class);

  public void exe()
  {
    try
    {
      log.info("Start to check the configs...");
      Object localObject1 = Constants.CONFIGAPPLYITEMSXML;

      XMLParser localXMLParser = new XMLParser();
      List<ConfigItem> localList1 = localXMLParser.parserXml((String)localObject1);

      HashMap localHashMap = new HashMap();
      for (ConfigItem localConfigItem : localList1)
      {
        ConfigParser localConfigParser = new ConfigParser();
        List<Config> localList2 = localConfigParser.getConfig(localConfigItem);
        for (Config localConfig : localList2)
        {
          Object localObject2;
          if (FileUtil.isXml(localConfig.getFile()))
          {
            localObject2 = new XmlConfigReader();
            ((XmlConfigReader)localObject2).readXMLConfig(localConfig);
          }
          else
          {
            localObject2 = new TxtConfigReader();
            ((TxtConfigReader)localObject2).readTXTConfig(localConfig);
          }
          new SaveConfigValue().saveToMap(localHashMap, localConfig);
        }
      }

      if (Resultlog.isSucc())
      {
        new SaveConfigValue().outputMap(localHashMap);
        log.info("The configs have been checked successfully.");
      }

      localObject1 = new Resultlog();
      ((Resultlog)localObject1).resultLog();
    }
    catch (Exception localException)
    {
      log.error("config check failure");
      localException.printStackTrace();
    }
    finally
    {
      Resultlog localResultlog1;
      Resultlog localResultlog2 = new Resultlog();
      localResultlog2.resultLog();
    }
  }
}
package com.zte.cms.content.common;

public class CntDelDstrOnOfflineConstant
{
    /*
     * log 错误码定义
     */
    // public static final String MSG_PREFIX = "msg.info.";
    // public static final String CNTOPRATE_SUCCESS = "350000"; //操作成功
    // public static final String CNTOPRATE_STATUS_ERROR = "350002"; //内容状态不正确
    // public static final String CNTOPRATE_PARTIAL_SUCCESS = "350098"; //部分操作成功
    // public static final String CNTOPRATE_FAIL = "350099"; //操作失败
    // public static final String CMS_UNKNOW_ERROR = "350091"; //未知
    public static final String CNTOPRATE_SUCCESS = "操作成功"; // 操作成功
    public static final String CNTOPRATE_ONLINE_SUCCESS = "内容上线操作成功";
    public static final String CNTOPRATE_OFFLINE_SUCCESS = "内容下线操作成功";
    public static final String CNTOPRATE_DELET_SUCCESS = "内容删除操作成功";
    public static final String CNTOPRATE_SYNC_SUCCESS = "内容重新同步成功";
    public static final String CNTOPRATE__DESTROY_SUCCESS = "内容销毁操作成功";
    public static final String CNTOPRATE_STATUS_ERROR = "内容状态不正确，请查看该选择项的操作记录"; // 内容状态不正确
    public static final String CNTOPRATE_PARTIAL_SUCCESS = "部分操作成功"; // 部分操作成功
    public static final String CNTOPRATE_FAIL = "操作失败"; // 操作失败
    public static final String CMS_UNKNOW_ERROR = "未知"; // 未知
    /*
     * 状态常量 status 内容状态：10初始 11待一审 12一审中 13待二审 14 二审中 15待三审 16三审中 17审核中 20待上线 21上线中(分解成以下状态：40上线同步到流媒体失败 41上线同步到流媒体成功
     * 42上线同步到BMS失败 43上线同步到流媒体中 44上线同步到BMS中)23已上线 24下线中(分解成以下状态：50下线同步到流媒体失败 51下线同步到流媒体成功 52下线同步到BMS失败 53下线同步到流媒体中
     * 54下线同步到BMS中) 24已下线 25已删除 30-39转码预留 90已销毁 91销毁失败
     */
    public static final String CNT_WAITONLINE = "20"; // 待上线
    public static final String CNT_ONLINEING = "21"; // 上线中
    public static final String CNT_ONLINEED = "23"; // 已上线
    public static final String CNT_OFFLINEED = "24"; // 已下线
    public static final String CNT_DELETED = "25"; // 已删除
    public static final String CNT_DESTROYED = "90"; // 已销毁
    public static final String CNT_DESTROY_FAIL = "91"; // 销毁失败
    public static final String SYNC_ONLINE_STRMEDIA_FAIL = "40"; // 上线同步到流媒体失败
    public static final String SYNC_ONLINE_STRMEDIA_SUCCESS = "41"; // 上线同步到流媒体成功
    public static final String SYNC_ONLINE_BMS_FAIL = "42"; // 上线同步到BMS失败
    public static final String SYNC_ONLINE_STRMEDIA_ING = "43"; // 上线同步到流媒体中
    public static final String SYNC_ONLINE_BMS_ING = "44"; // 上线同步到BMS中
    public static final String SYNC_OFFLINE_STRMEDIA_FAIL = "50"; // 下线同步到流媒体失败
    public static final String SYNC_OFFLINE_STRMEDIA_SUCCESS = "51"; // 下线同步到流媒体成功
    public static final String SYNC_OFFLINE_BMS_FAIL = "52"; // 下线同步到BMS失败
    public static final String SYNC_OFFLINE_STRMEDIA_ING = "53"; // 下线同步到流媒体中
    public static final String SYNC_OFFLINE_BMS_ING = "54"; // 下线同步到BMS中
}
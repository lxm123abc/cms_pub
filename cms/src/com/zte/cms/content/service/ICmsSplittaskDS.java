package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.CmsSplittask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsSplittaskDS
{
    /**
     * 新增CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 批量更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSplittaskList(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException;

    /**
     * 根据条件更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSplittaskListByCond(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException;

    /**
     * 删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 批量删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSplittaskList(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException;

    /**
     * 根据条件删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSplittaskListByCond(List<CmsSplittask> cmsSplittaskList) throws DomainServiceException;

    /**
     * 查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @return CmsSplittask对象
     * @throws DomainServiceException ds异常
     */
    public CmsSplittask getCmsSplittask(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 根据条件查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象
     * @return 满足条件的CmsSplittask对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSplittask> getCmsSplittaskByCond(CmsSplittask cmsSplittask) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSplittask对象
     * 
     * @param cmsSplittask CmsSplittask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerySplit(CmsSplittask cmsSplittask, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
package com.zte.cms.channel.update.wkfw.ls;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.service.ls.ServiceConstants;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelUpdateWkfwCheck implements IChannelUpdateWkfwCheck
{

    private ICmsChannelDS channelds = null;
    private ICmsChannelBufDS channelBufds = null;

    // 构造方法
    ChannelUpdateWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }

    /**
     * 
     * @return ICmsChannelDS
     */
    public ICmsChannelDS getChannelds()
    {
        return channelds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return ICmsChannelBufDS
     */
    public ICmsChannelBufDS getChannelBufds()
    {
        return channelBufds;
    }

    /**
     * 
     * @param channelBufds ICmsChannelBufDS
     */
    public void setChannelBufds(ICmsChannelBufDS channelBufds)
    {
        this.channelBufds = channelBufds;
    }

    /**
     * @param channelindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkChannelUpdateWkfw(Long channelindex) throws DomainServiceException
    {
        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(channelindex);
        channel = channelds.getCmsChannel(channel);

        if (channel == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS); // 频道不存在
        }
        // 状态不为待发布0，新增同步成功20 修改同步成功50 修改同步失败60 取消同步成功80
//        if ((channel.getStatus() != 0) && (channel.getStatus() != 20) && (channel.getStatus() != 50)
//                && (channel.getStatus() != 60) && (channel.getStatus() != 80))
//        {
//            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG); // 频道状态不正确
//        }
        // 已经发起工作流
        if (channel.getWorkflow() != null)
        {
            if (channel.getWorkflow() != 0)
            {
                return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_IN_ALREADY);
                // 已发起工作流未结束
            }
        }
        if (channel.getWorkflowlife() != null)
        {
            if (channel.getWorkflowlife() != 0)
            {
                return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG);
                // 工作流流程状态不正确
            }
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    /**
     * @param channelIndex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkChannelUpdateWkfwAudit(Long channelIndex) throws DomainServiceException
    {
        CmsChannelBuf channelbuf = new CmsChannelBuf();
        channelbuf.setChannelindex(channelIndex);
        channelbuf = channelBufds.getCmsChannelBuf(channelbuf);

        if (channelbuf == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG);
            // buf表中数据不存在
        }
        if (channelbuf.getWorkflow() != 2)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_NOT_IN_WKFW_OR_WRONG_WKFW);
            // 不在工作流中或所处工作流不正确
        }
        if (channelbuf.getWorkflowlife() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_STATUS_WRONG);
            // 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

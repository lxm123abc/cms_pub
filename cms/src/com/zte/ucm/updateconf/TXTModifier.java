package com.zte.ucm.updateconf;

import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class TXTModifier
{
  Logger log = LoggerFactory.getLogger(ConfigModifier.class);

  public void modifyTXTConfig(Config paramConfig) {
    String str1 = paramConfig.getKey();
    String str2 = "";
    String str3 = "";
    PrintWriter localPrintWriter = null;
    Resultlog localResultlog = new Resultlog();
    File localFile = new File(paramConfig.getFile());
    if (!localFile.exists()) {
      this.log.error("Operation ERROR:config file[" + paramConfig.getFile() + "] is not exist!!!");

      str3 = str3 + "1|Operation ERROR:config file[" + paramConfig.getFile() + "] is not exist!!!" + "\n";

      localResultlog.appendLog(str3);
      return;
    }
    try {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(paramConfig.getFile()), "utf-8"));

      String str4 = "";
      StringBuffer localStringBuffer = new StringBuffer();
      while ((str4 = localBufferedReader.readLine()) != null) {
        String str5 = str4;

        if ((!str4.contains(";")) && (!str4.trim().startsWith("#")) && (str4.contains("=")) && (str4.contains(str1)))
        {
          str2 = paramConfig.getValue();

          if (paramConfig.getFile().contains("ApplicationResources")) {
            str2 = utf8ToUnicode(str2);
          }

          str4 = str4.replace(str4, str2);
          this.log.info("Operation success:config file[" + paramConfig.getFile() + "]  from prevalue[" + str5 + "] to value[" + str4 + "]");
        }

        if ((str4.contains(";")) && (!str4.trim().startsWith("#")) && (str4.contains(str1))) {
          String[] arrayOfString = str4.split(";");
          if (str4.contains(str1)) {
            str4 = "";
            for (int i = 0; i < arrayOfString.length; i++) {
              if (arrayOfString[i].contains(str1)) {
                str2 = paramConfig.getValue();
                arrayOfString[i] = arrayOfString[i].replace(arrayOfString[i], str2);

                if (i < arrayOfString.length - 1) {
                  str4 = str4 + arrayOfString[i] + ";";
                }

                if (i == arrayOfString.length - 1)
                  str4 = str4 + arrayOfString[i];
              }
              else {
                if (i < arrayOfString.length - 1) {
                  str4 = str4 + arrayOfString[i] + ";";
                }

                if (i == arrayOfString.length - 1) {
                  str4 = str4 + arrayOfString[i];
                }
              }
            }
            this.log.info("Operation success:config file[" + paramConfig.getFile() + "]  from prevalue[" + str5 + "] to value[" + str4 + "]");
          }

        }

        localStringBuffer.append(str4);
        localStringBuffer.append("\n");
      }
      localPrintWriter = new PrintWriter(paramConfig.getFile(), "utf-8");
      localPrintWriter.write(localStringBuffer.toString());
      localPrintWriter.close();
      localBufferedReader.close();
    } catch (FileNotFoundException localFileNotFoundException) {
      localFileNotFoundException.printStackTrace();
    } catch (IOException localIOException) {
      localIOException.printStackTrace();
    }
  }

  public String utf8ToUnicode(String paramString) {
    char[] arrayOfChar = paramString.toCharArray();
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; i < paramString.length(); i++) {
      Character.UnicodeBlock localUnicodeBlock = Character.UnicodeBlock.of(arrayOfChar[i]);
      if (localUnicodeBlock == Character.UnicodeBlock.BASIC_LATIN)
      {
        localStringBuffer.append(arrayOfChar[i]);
      }
      else
      {
        int j;
        if (localUnicodeBlock == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS)
        {
          j = arrayOfChar[i] - 65248;
          localStringBuffer.append((char)j);
        }
        else {
          j = arrayOfChar[i];
          String str1 = Integer.toHexString(j);
          String str2 = "\\u" + str1;
          localStringBuffer.append(str2.toLowerCase());
        }
      }
    }
    return localStringBuffer.toString();
  }
}

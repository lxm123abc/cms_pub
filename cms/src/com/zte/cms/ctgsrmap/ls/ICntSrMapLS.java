package com.zte.cms.ctgsrmap.ls;

import java.util.List;
import java.util.Map;

import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

/**
 * 
 * @author Administrator
 * 
 */
public interface ICntSrMapLS
{
    /**
     * 
     * @param categoryProgramMap CategorySeriesMap
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     */
    public TableDataInfo queryCntPgMapList(CategorySeriesMap categoryProgramMap, int start, int pageSize);

    /**
     * 
     * @param categorycdn CategorySeriesMap
     * @return String
     */
    public String insertCtgsrmap(CategorySeriesMap categorycdn);

    /**
     * 
     * @param id Long
     * @return String
     */
    public String deletectgpgmap(Long id);

    /**
     * 
     * @param str CategorySeriesMap
     * @return String
     * @throws Exception Exception
     */
    public String publishCategory(CategorySeriesMap str[]) throws Exception;

    /**
     * 
     * @param str CategorySeriesMap
     * @return String
     * @throws Exception Exception
     */
    public String canclePuhlishProgramtype(CategorySeriesMap str[]) throws Exception;

    /**
     * 
     * @param mapid String
     * @return CategorySeriesMap
     */
    public CategorySeriesMap getCmsCategory(String mapid);

    /**
     * 
     * @param pgindex String
     * @return CmsSeries
     */
    public CmsSeries getCmsSeries(String pgindex);

    /**
     * 
     * @param file String
     * @return Map
     */
    public Map<String, String> getpgname(String file);

    /**
     * 
     * @param ctgindex String
     * @return Map
     */
    public Map<String, String> getctgname(String ctgindex);
    
    //
    public String publishCategorySeriesTargetMap (Long syncindex ) throws Exception; 
    public String publishCategorySeriesPlatformMap (Long syncindex ) throws Exception; 
    public String publishCategorySeriesBatchMap (List<CmsSeries> mappingList ) throws Exception; 
   // 
    public String delPublishCategorySeriesTargetMap (Long syncindex ) throws Exception; 
    public String delPublishCategorySeriesPlatformMap (Long syncindex ) throws Exception; 
    public String delPublishCategorySeriesBatchMap (List<CmsSeries> mappingList ) throws Exception; 
}
package com.zte.cms.content.ls;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.cms.EMBtask.service.ICmsEmbtaskDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.UpLoadFile;
import com.zte.cms.common.UpLoadResult;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.model.IcmSourcefile;
import com.zte.cms.content.service.IIcmSourcefileDS;
import com.zte.cms.content.service.IIcmTransfileDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class IcmSourcefileLS implements IIcmSourcefileLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private IIcmSourcefileDS icmSourcefileDS = null;
    private ICmsStorageareaLS cmsStorageareaLS = null;
    private ICmsFtptaskDS cmsFtptaskDS = null;
    private IUsysConfigDS usysConfigDS = null;
    private IIcmTransfileDS icmTransfileDS = null;
    private ICmsEmbtaskDS cmsEmbtaskDS = null;
    private IUcpBasicDS basicDS = null;

    /**
     * 增加母片
     * 
     * @param icmSourcefile 母片
     * @return 操作结果
     * @throws Exception
     */
    public String addSourcefile(IcmSourcefile icmSourcefile) throws Exception
    {
        log.debug("addSourcefile starting...");
        ReturnInfo rtnInfo = new ReturnInfo();
        int uploadtype = icmSourcefile.getUploadtype();
        String currenttime = null; // 当前时间
        try
        {

            UcpBasic cp = new UcpBasic();
            cp.setCpid(icmSourcefile.getCpid());
            cp.setCptype(1);
            List<UcpBasic> cpList = basicDS.getUcpBasicByCond(cp);
            if (cpList != null && cpList.size() > 0)
            {
                cp = cpList.get(0);
                if (cp == null || cp.getStatus() != 1)
                {
                    return "270009";
                }
            }
            else
            {
                return "270009";
            }

            String desPath = cmsStorageareaLS.getAddress("4");
            if (null == desPath || ("1056020").equals(desPath))
            {// 获取制作区地址失败
                return "270006";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "270007";
                }
                else
                {

                    Date date = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    currenttime = dateFormat.format(date);

                    if (uploadtype == 0)
                    {
                        // 判断目标文件夹是否存在，若不存在则创建
                        StringBuffer destfile = new StringBuffer("");
                        destfile.append(GlobalConstants.getMountPoint());
                        destfile.append(desPath);
                        destfile.append(File.separator);
                        destfile.append(currenttime);
                        destfile.append(File.separator);
                        File destDir = new File(destfile.toString());
                        if (!destDir.exists())
                        {// 目标文件夹不存在，创建
                            destDir.mkdirs();
                        }

                        UpLoadResult uploadResult = null;
                        String destpath = GlobalConstants.getMountPoint() + desPath + File.separator + currenttime;
                        uploadResult = UpLoadFile.singleFileUplord(icmSourcefile.getFilename(),
                                CntConstants.MAX_FILESIZE, destpath);

                        if (null != uploadResult)
                        {
                            String addresspath = desPath + File.separator + currenttime + File.separator
                                    + icmSourcefile.getFilename();// 文件路径
                            Integer sizeFlag = uploadResult.getFlag();
                            icmSourcefile.setFilesize(uploadResult.getFileSize().toString());
                            icmSourcefile.setStatus(12);
                            icmSourcefile.setAddresspath(addresspath);
                            icmSourcefileDS.insertIcmSourcefile(icmSourcefile);

                            if (-1 == sizeFlag.intValue())
                            {// 文件大小超过2G

                                return "280002";
                            }
                        }
                        else
                        {
                            // 文件上传失败

                            return "280003";
                        }
                    }
                    else if (uploadtype == 1)
                    {
                        StringBuffer srcaddress = new StringBuffer("");
                        StringBuffer destaddress = new StringBuffer("");

                        //CmsFtptask cmsFtptask = new CmsFtptask();

                        if (1 == uploadtype)
                        {
                            srcaddress.append(icmSourcefile.getAddresspath()); // 文件路径包含文件名
                        }

                        File uploadedFile = new File(GlobalConstants.getMountPoint() + srcaddress.toString());
                        if (!uploadedFile.exists())
                        {
                            return "270008";
                        }

                        destaddress.append(desPath);
                        destaddress.append(File.separator);
                        destaddress.append(currenttime);
                        destaddress.append(File.separator);
                        destaddress.append(icmSourcefile.getFilename());

                        icmSourcefile.setStatus(12);
                        icmSourcefile.setAddresspath(destaddress.toString());
                        icmSourcefileDS.insertIcmSourcefile(icmSourcefile);

                        // 插入文件迁移任务
                        /**UCDN母片上传到制作区 不插文件迁移任务
                        cmsFtptask.setFileindex(icmSourcefile.getFileindex());
                        cmsFtptask.setTasktype(Long.parseLong("2")); // 母片迁移
                        cmsFtptask.setUploadtype(2);
                        cmsFtptask.setSrcaddress(srcaddress.toString());
                        cmsFtptask.setDestaddress(destaddress.toString());
                        cmsFtptask.setStatus(0); // 0：待上传
                        cmsFtptask.setSrcfilehandle(1); // 0：删除
                        cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                         **/
                    }
                }
            }
            // 保存操作日志
            CommonLogUtil.insertOperatorLog(icmSourcefile.getFilename(), CommonLogConstant.MGTTYPE_CONTENT,
                    CommonLogConstant.OPERTYPE_CONTENT_SOURCEFILE_ADD,
                    CommonLogConstant.OPERTYPE_CONTENT_SOURCEFILE_ADD_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
            return "280003";
        }

        log.debug("addSourcefile end");
        return "280000";
    }

    /**
     * 删除母片
     * 
     * @param icmSourcefile 母片
     * @return 操作结果
     * @throws Exception
     */
    public String deleteSourcefile(IcmSourcefile icmSourcefile) throws Exception
    {
        log.debug("deleteSourcefile starting...");
        try
        {

            CmsEmbtask cmsEmbtask = new CmsEmbtask();
            cmsEmbtask.setSubcontentindex(icmSourcefile.getFileindex());
            List<CmsEmbtask> list = cmsEmbtaskDS.getCmsEmbtaskByCond(cmsEmbtask);
            if (list != null && list.size() > 0)
            {
                for (CmsEmbtask embtask : list)
                {
                    if (embtask.getStatus() != 4 && embtask.getStatus() != 3)
                    {
                        return "280007";
                    }
                }
            }

            IcmSourcefile sourcefile = icmSourcefileDS.getIcmSourcefile(icmSourcefile);
            if (sourcefile != null && sourcefile.getStatus() != 25)
            {
                sourcefile.setStatus(25);
                icmSourcefileDS.updateIcmSourcefile(sourcefile);
                // 保存操作日志
                CommonLogUtil.insertOperatorLog(sourcefile.getFilename(), CommonLogConstant.MGTTYPE_CONTENT,
                        CommonLogConstant.OPERTYPE_CONTENT_SOURCEFILE_DELETE,
                        CommonLogConstant.OPERTYPE_CONTENT_SOURCEFILE_DELETE_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                return "280001";

            }
        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
            return "280003";
        }

        log.debug("deleteSourcefile end");
        return "280000";
    }

    /**
     * 获取子文件预览路径
     * 
     * @param fileindex 子文件索引
     * @return 子文件预览路径
     * @throws Exception
     */
    public String getPreviewPath(Long fileindex) throws Exception
    {
        log.debug("getPreviewPath starting...");

        // IcmTransfile icmTransfile = new IcmTransfile();
        IcmSourcefile icmSourcefile = new IcmSourcefile();
        UsysConfig usysConfig = new UsysConfig();
        String previewPath = "";
        try
        {
            usysConfig.setCfgkey("cms.preview.path");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                icmSourcefile.setFileindex(fileindex);
                icmSourcefile = icmSourcefileDS.getIcmSourcefile(icmSourcefile);
                if (null == icmSourcefile)
                {// 素材不存在
                    throw new Exception(CntConstants.SUBCNT_NOT_EXIST);
                }
                else
                {
                    previewPath = usysConfig.getCfgvalue() + icmSourcefile.getAddresspath();
                }
            }
            else
            {// "272201"：未配置 系统参数WINDOWS共享访问点
                throw new Exception(CntConstants.NOCONFIG_WINDOWSHARE);
            }
        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("getPreviewPath end");
        return previewPath;
    }

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("icmSourcefileLS pageInfoQuery starting");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setOrderByColumn("fileindex");
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        try
        {
            if (null != icmSourcefile.getNamecn() && !icmSourcefile.getNamecn().equals(""))
            {
                String namecn = EspecialCharMgt.conversion(icmSourcefile.getNamecn());
                icmSourcefile.setNamecn(namecn);
            }

            if (null != icmSourcefile.getFilename() && !icmSourcefile.getFilename().equals(""))
            {
                String filename = EspecialCharMgt.conversion(icmSourcefile.getFilename());
                icmSourcefile.setFilename(filename);
            }
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            icmSourcefile.setOperid(operinfo.getOperid());
            tableInfo = icmSourcefileDS.pageInfoQuery(icmSourcefile, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("icmSourcefileLS end");
        return tableInfo;
    }

    /**
     * 根据条件分页查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryDesc(IcmSourcefile icmSourcefile, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("icmSourcefileLS pageInfoQuery starting");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setOrderByColumn("fileindex");
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        try
        {
            tableInfo = icmSourcefileDS.pageInfoQuery(icmSourcefile, start, pageSize, puEntity);

        }
        catch (Exception e)
        {
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("icmSourcefileLS end");
        return tableInfo;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }

    public void setIcmSourcefileDS(IIcmSourcefileDS icmSourcefileDS)
    {
        this.icmSourcefileDS = icmSourcefileDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setIcmTransfileDS(IIcmTransfileDS icmTransfileDS)
    {
        this.icmTransfileDS = icmTransfileDS;
    }

    public void setCmsEmbtaskDS(ICmsEmbtaskDS cmsEmbtaskDS)
    {
        this.cmsEmbtaskDS = cmsEmbtaskDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }
}

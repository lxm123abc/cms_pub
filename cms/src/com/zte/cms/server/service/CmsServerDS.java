package com.zte.cms.server.service;

import java.util.List;

import com.zte.cms.server.dao.ICmsServerDAO;
import com.zte.cms.server.model.CmsServer;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CmsServerDS implements ICmsServerDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IPrimaryKeyGenerator primaryKeyGenerator;

    private ICmsServerDAO dao = null;

    public void setDao(ICmsServerDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsServer(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("insert cmsServer starting...");
        try
        {
            Long index = (Long) primaryKeyGenerator.getPrimarykey("cms_server");
            cmsServer.setServerindex(index);

            dao.insertCmsServer(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsServer end");
    }

    public void insertCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException
    {
        log.debug("insert cmsServerList by pk starting...");
        if (null == cmsServerList || cmsServerList.size() == 0)
        {
            log.debug("there is no datas in cmsServerList");
            return;
        }
        try
        {
            dao.insertCmsServerList(cmsServerList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsServerList by pk end");
    }

    public void updateCmsServer(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("update cmsServer by pk starting...");
        try
        {
            dao.updateCmsServer(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsServer by pk end");
    }

    public void updateCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException
    {
        log.debug("update cmsServerList by pk starting...");
        if (null == cmsServerList || cmsServerList.size() == 0)
        {
            log.debug("there is no datas in cmsServerList");
            return;
        }
        try
        {
            dao.updateCmsServerList(cmsServerList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsServerList by pk end");
    }

    public void updateCmsServerByCond(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("update cmsServer by condition starting...");
        try
        {
            dao.updateCmsServerByCond(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsServer by condition end");
    }

    public void updateCmsServerListByCond(List<CmsServer> cmsServerList) throws DomainServiceException
    {
        log.debug("update cmsServerList by condition starting...");
        if (null == cmsServerList || cmsServerList.size() == 0)
        {
            log.debug("there is no datas in cmsServerList");
            return;
        }
        try
        {
            dao.updateCmsServerListByCond(cmsServerList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsServerList by condition end");
    }

    public void removeCmsServer(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("remove cmsServer by pk starting...");
        try
        {
            dao.deleteCmsServer(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsServer by pk end");
    }

    public void removeCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException
    {
        log.debug("remove cmsServerList by pk starting...");
        if (null == cmsServerList || cmsServerList.size() == 0)
        {
            log.debug("there is no datas in cmsServerList");
            return;
        }
        try
        {
            dao.deleteCmsServerList(cmsServerList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsServerList by pk end");
    }

    public void removeCmsServerByCond(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("remove cmsServer by condition starting...");
        try
        {
            dao.deleteCmsServerByCond(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsServer by condition end");
    }

    public void removeCmsServerListByCond(List<CmsServer> cmsServerList) throws DomainServiceException
    {
        log.debug("remove cmsServerList by condition starting...");
        if (null == cmsServerList || cmsServerList.size() == 0)
        {
            log.debug("there is no datas in cmsServerList");
            return;
        }
        try
        {
            dao.deleteCmsServerListByCond(cmsServerList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsServerList by condition end");
    }

    public CmsServer getCmsServer(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("get cmsServer by pk starting...");
        CmsServer rsObj = null;
        try
        {
            rsObj = dao.getCmsServer(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsServerList by pk end");
        return rsObj;
    }

    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer) throws DomainServiceException
    {
        log.debug("get cmsServer by condition starting...");
        List<CmsServer> rsList = null;
        try
        {
            rsList = dao.getCmsServerByCond(cmsServer);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsServer by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsServer page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsServer, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsServer>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsServer page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsServer page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsServer, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsServer>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsServer page info by condition end");
        return tableInfo;
    }

    public IPrimaryKeyGenerator getPrimaryKeyGenerator()
    {
        return primaryKeyGenerator;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }
}

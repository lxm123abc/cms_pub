var RES_WRITE = 1;        //可写权限
var RES_READONLY  = 2;    //只读权限
var RES_INVALIDATION = 3; //失效权限
var RES_HIDDEN = 4;       //隐藏权限

var SSB_RES = "res";
var Z_SSB_RES = "Z:RES";

var SSB_AUTH = "auth";
var Z_SSB_AUTH = "Z:AUTH";

var charString = "|";
var comma = ",";

//后台返回数据
var ria_rightObj = {};

/**
 * 权限初始化
 */
var ResTags_init = function ()
{
	//得到页面id
	var pageid = ria_getAuthTag();
	
	//判断页面id的存在
	if(pageid && pageid != "")
	{
		//调用后台
		callUrl("$/ssb/right/riaRightTest/getCompRight.ssm",pageid,"ria_rightObj","true");
		
		if(ria_rightObj && ria_rightObj.rtnValue && ria_rightObj.rtnValue.isRight)
		{
			//根据后台信息对页面进行处理
			ria_setResTags();
		}				
	}
};

//onload
addOnloadEvent(ResTags_init);

/**
 * 读取页面id
 */
var ria_getAuthTag = function ()
{	
	var nodes= document.getElementsByTagName(SSB_AUTH);
	
	try{
		//判断是否是IE浏览器
		if(navigator.appName == "Microsoft Internet Explorer")
		{
			nodes= document.getElementsByTagName(SSB_AUTH);
		}else
		{
			nodes= document.getElementsByTagName(Z_SSB_AUTH);
		}
	}catch(e)
	{
		log.info("error:"+e);
	}	
	var pageid = "";
	if(nodes[0])
	{
		pageid = nodes[0].getAttribute("pageid");
	}	
	return pageid;
};

/**
 * 设置页面控件权限
 */
var ria_setResTags = function ()
{
	var allResObj = ria_getRiaResTags();
	
	//若对象不存在，则直接返回
	if(!allResObj)
	{
		return ;
	}
	var resCtrlId = allResObj.resCtrlId;
	var resObj = allResObj.resObj;
	
	//遍历所有ID
	for(var i = 0;i<resCtrlId.length;i++)
	{
		ria_disposeAuthTag(resObj,resCtrlId[i]);
	}
};

/**
 * 根据页面控件id ，设置页面权限
 * @param {Object} id
 */
var ria_disposeAuthTag = function (resObj,id)
{
	//若页面没有此控件，直接返回
	if(!document.getElementById(id))
	{
		return ;
	}
	
	//隐藏
	if(resObj[id] == RES_HIDDEN)
	{
		document.getElementById(id).style.visibility = "hidden";
	}
	//失效
	else if(resObj[id] == RES_INVALIDATION)
	{
		document.getElementById(id).style.visibility = "visible";
		document.getElementById(id).disabled = true;
	}
	//只读
	else if(resObj[id] == RES_READONLY)
	{
		document.getElementById(id).blur();
		document.getElementById(id).style.visibility = "visible";
		document.getElementById(id).disabled = false;
		document.getElementById(id).readOnly = true;
	}
	//可写
	else if(resObj[id] == RES_WRITE)
	{
		document.getElementById(id).readOnly = false;
		document.getElementById(id).disabled = false;
		document.getElementById(id).style.visibility = "visible";	
	}
};

/**
 * 读取页面控件权限标签
 */
var ria_getRiaResTags = function()
{
	var nodes= document.getElementsByTagName(SSB_RES);
	try{
		//判断是否是IE浏览器
		if(navigator.appName == "Microsoft Internet Explorer")
		{
			nodes= document.getElementsByTagName(SSB_RES);
		}else
		{
			nodes= document.getElementsByTagName(Z_SSB_RES);
		}
	}catch(e)
	{
		log.info("error:"+e);
	}
	
	return ria_readResTag(nodes);

};

/**
 * 配置控件对应的权限数据
 * @param {Object} nodes
 */
var ria_readResTag = function (nodes)
{
	var allResObj = {};
	var resObj = {};
	var resCtrlId = new Array();
	if(!nodes)
	{
		return null;
	}
	for(var n = 0;n<nodes.length;n++)
	{
		var authid = nodes[n].getAttribute("authid");
		var ctrl = nodes[n].getAttribute("ctrl");
		
		//若无此权限配置，则权限为隐藏
		if(!ria_rightObj || !ria_rightObj.rtnValue || !ria_rightObj.rtnValue[authid])
		{
			ria_rightObj.rtnValue[authid] = RES_HIDDEN;
		}
		
		var ctrlIdArr = ctrl.split(comma);
		
		//遍历控件id
		for(var j=0;j<ctrlIdArr.length;j++)
		{			
			//以id为键，构建对象
			if(!resObj[ctrlIdArr[j]] || (resObj[ctrlIdArr[j]] && resObj[ctrlIdArr[j]] > ria_rightObj.rtnValue[authid]))
			{
				resObj[ctrlIdArr[j]] =  ria_rightObj.rtnValue[authid];
				resCtrlId.push(ctrlIdArr[j]);
			}
		}	
	}
	//封装数据
	allResObj.resObj = resObj;
	allResObj.resCtrlId = resCtrlId;
	return allResObj;
};



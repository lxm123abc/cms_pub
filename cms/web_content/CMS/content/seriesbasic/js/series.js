    function popcpsp(){
        var page = "../cpsp/popCpsp.html";
        var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
        var rtnobj = window.showModalDialog(page, "", sFeatures);
        
        if(typeof(rtnobj) != "undefined"){
	        $("#cpcnshortname").val(rtnobj.cpcnshortname);
	        $("#cpindex").val(rtnobj.cpindex);
	        $("#cpid").val(rtnobj.cpid);    
	     	var providValue="";
	    	for(var i=0;i<countryMap["valueList"].length;i++)
			{
				if(rtnobj.businessscope==countryMap["valueList"][i])
				{
					providValue=countryMap["textList"][i];
					break;
				}
			}
			$("#providValue").text(providValue);
			$("#provid").val(rtnobj.businessscope);
	        callSid('getCpBase',rtnobj.cpid);
        }
    }
    
    function popCatalog()
  	{
		var page = "../content/CatalogChooseWindow.html";                                //弹出窗口路径
		var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
		var name = window.showModalDialog(page, "", sFeatures);       
		$("#contenttags").val(name);                                                         //其中tagname为内容页面的标签输入框的id                                            

   	}
    
    
    
    function doValidPage()
    {
    	$("label.validFont").text("");
    	var isPass=false;
    	var namecn=document.getElementById("namecn");
    	isPass=isNull(namecn,"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}	
    	isPass=isMaxLength(namecn,"namecn_msg",namecn.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr1($.trim(namecn.value),"namecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	///内容名称校验结束
    	
    	var originalnamecn=document.getElementById("originalnamecn");
    	isPass=isMaxLength(originalnamecn,"originalnamecn_msg",originalnamecn.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	isPass=isSpecialStr($.trim(originalnamecn.value),"originalnamecn_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	//连续剧原来名称校验结束
    	
    	var cpid=document.getElementById("cpid");
    	isPass=isNull(cpid,"cpid_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
    	//验证内容提供商结束
    	
    	var licenseid=$("#licenseid").val();
    	if(licenseid=="")
    	{
    		$("#licenseid_msg").text($res_entry("msg.info.basicdata.032"));
    		return false;
    	}
    	
    	//验证牌照方结束
    	
    	var cpcontentid=document.getElementById("cpcontentid");
    	isPass=isMaxLength(cpcontentid,"cpcontentid_msg",cpcontentid.maxLength);
    	if(isPass==false)
    	{
    		return false;
    	}
    	
	   	//CP节目标识验证结束
	   	
	   	var ordernumber=document.getElementById("ordernumber");
	   	isPass=isPlusInteger(ordernumber,"ordernumber_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证节目订购编号结束
	   	
	   	var sortname=document.getElementById("sortname");
	   	isPass=isMaxLength(sortname,"sortname_msg",sortname.maxLength);
	   	if(isPass==false)
	   	{
			return false;
	   	}	   	
	   	isPass=isSpecialStr($.trim(sortname.value),"sortname_msg");
	   	if(isPass==false)
	   	{
	   		return false;
	   	}
	   	//验证索引名称结束
	   	
		var searchname=document.getElementById("searchname");
		isPass=isMaxLength(searchname,"searchname_msg",searchname.maxLength);
		if(isPass==false)
		{
			return false;
		}	   	
		isPass=isSpecialStr($.trim(searchname.value),"searchname_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证索引名称结束
		
		var contentversion=document.getElementById("contentversion");
		isPass=isMaxLength(contentversion,"contentversion_msg",contentversion.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(contentversion.value),"contentversion_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容版本结束
		
		var keywords=document.getElementById("keywords");
		isPass=isMaxLength(keywords,"keywords_msg",keywords.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(keywords.value),"keywords_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证关键字结束
		
		
		var copyrightcn=document.getElementById("copyrightcn");
		isPass=isMaxLength(copyrightcn,"copyrightcn_msg",copyrightcn.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyrightcn.value),"copyrightcn_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权信息结束
		isPass=betweenTimeValidate("effectivedate","expirydate","effectivedate_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容生效失效时间结束
		
		var exclusivevalidity=document.getElementById("exclusivevalidity");
		isPass=isPlusInteger(exclusivevalidity,"exclusivevalidity_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证独家有效期结束

		var copyrightexpiration=document.getElementById("copyrightexpiration");	
		isPass=isPlusInteger(copyrightexpiration,"copyrightexpiration_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权失效期结束
		
		var llicensofflinetime=document.getElementById("llicensofflinetime").value;
		if(llicensofflinetime!="")
		{
			isPass=validatedate("llicensofflinetime","llicensofflinetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}
		//验证牌照方自定义下线时间结束
		
		var deletetime=document.getElementById("deletetime").value;
		if(deletetime!="")
		{
			isPass=validatedate("deletetime","deletetime_msg");
		}
		if(isPass==false)
		{
			return false;
		}	
		//验证内容删除时间结束
		
		
		var copyproperty=document.getElementById("copyproperty");
		isPass=isMaxLength(copyproperty,"copyproperty_msg",copyproperty.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(copyproperty.value),"copyproperty_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证版权性质结束
		
		var country=document.getElementById("country");
		isPass=isMaxLength(country,"country_msg",country.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(country.value),"country_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证国家结束
		
		
		var importlisence=document.getElementById("importlisence");
		isPass=isMaxLength(importlisence,"importlisence_msg",importlisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(importlisence.value),"importlisence_msg");
		if(isPass==false)
		{
			return false;
		}
		
	
		//引进批准号结束
		
		var releaselisence=document.getElementById("releaselisence");
		isPass=isMaxLength(releaselisence,"releaselisence_msg",releaselisence.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(releaselisence.value),"releaselisence_msg");
		if(isPass==false)
		{
			return false;
		}
	
		
		//验证公映许可证结束
		
		var contenttags=document.getElementById("contenttags");
		isPass=isMaxLength(contenttags,"contenttags_msg",256);
		if(isPass==false)
		{
			return false;
		}
		//验证内容标签关联结束
		
		var pricetaxin=document.getElementById("pricetaxin");
		isPass=checkpRicetaxin(pricetaxin,"pricetaxin_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证列表定价结束
		
		var newcomedays=document.getElementById("newcomedays");
		isPass=isPlusInteger(newcomedays,"newcomedays_msg");
		if(isPass==false)
		{
			return false;
		}
		//新到天数结束
		var remainingdays=document.getElementById("remainingdays");
		isPass=isPlusInteger(remainingdays,"remainingdays_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证剩余天数结束
		
		var recommendstart=document.getElementById("recommendstart");
		isPass=isPlusInteger(recommendstart,"recommendstart_msg");
		if(isPass==false)
		{
			return false;
		}
		else
		{
			var recommendstartValue=$.trim(recommendstart.value);
			if(parseInt(recommendstartValue)>10||parseInt(recommendstartValue)<1)
			{
				$("#recommendstart_msg").text($res_entry("msg.info.content.194"));
				return false;
			}
			else
			{
				$("#recommendstart_msg").text("");
			}
		}
		//验证推荐星级结束
		
		var licensingstart=document.getElementById("licensingstart");
		isPass=isNull(licensingstart,"licensingstart_msg");
		if(isPass==false)
		{
			return false;
		}	
		var licensingend=document.getElementById("licensingend");
		isPass=isNull(licensingend,"licensingend_msg");
		if(isPass==false)
		{
			return false;
		}
		isPass=betweenTimeValidate("licensingstart","licensingend","licensingstart_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证有效时间结束
		
		var viewpoint=document.getElementById("viewpoint");
		isPass=isSpecialStr($.trim(viewpoint.value),"viewpoint_msg");
		if(isPass==false)
		{
			return false;
		}
		
		//验证看点结束
		
		var awards=document.getElementById("awards");
		isPass=isMaxLength(awards,"awards_msg",awards.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(awards.value),"awards_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证所含奖项结束
		var actors=document.getElementById("actors");		
		isPass=isMaxLength(actors,"actors_msg",actors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(actors.value),"actors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证演员结束
		
		var writers=document.getElementById("writers");		
		isPass=isMaxLength(writers,"writers_msg",writers.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(writers.value),"writers_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证作者结束
		
		var directors=document.getElementById("directors");		
		isPass=isMaxLength(directors,"directors_msg",directors.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(directors.value),"directors_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证导演结束
		
		
		var productcorp=document.getElementById("productcorp");		
		isPass=isMaxLength(productcorp,"productcorp_msg",productcorp.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(productcorp.value),"productcorp_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片商结束
		
		var producer=document.getElementById("producer");
		isPass=isMaxLength(producer,"producer_msg",producer.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(producer.value),"producer_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证制片人结束
		
		var publisher=document.getElementById("publisher");
		isPass=isMaxLength(publisher,"publisher_msg",publisher.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(publisher.value),"publisher_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证发行商结束
		
		
		var title=document.getElementById("title");
		isPass=isMaxLength(title,"title_msg",title.maxLength);
		if(isPass==false)
		{
			return false;
		}
		isPass=isSpecialStr($.trim(title.value),"title_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证内容标题结束
		
		var desccn=document.getElementById("desccn");
		isPass=isSpecialStr($.trim(desccn.value),"desccn_msg");
		if(isPass==false)
		{
			return false;
		}
		
		var volumncount=document.getElementById("volumncount");
		isPass=isNull(volumncount,"volumncount_msg");
    	if(isPass==false)
    	{
    		return false;
    	}
		isPass=isAboveZero(volumncount,"volumncount_msg");
		if(isPass==false)
		{
			return false;
		}
		//验证总集数结束
		return true;
    }
    
    
    
    function isAboveZero(input,input_msg)
    {
    	var inputValue=$.trim(input.value);
    	if(inputValue=="")
    	{
    		$("#"+input_msg).text("");
    		return true;
    	}
    	else
    	{
			var integerRex=/^[1-9]+[0-9]*$/;
			var rtnValue=integerRex.test(inputValue);
			if(rtnValue==false)
			{
					$("#"+input_msg).text($res_entry("msg.info.content.201"));
					return false;
			}
			else
			{
				var intValue=parseInt(inputValue);
				if(intValue<1)
				{
					$("#"+input_msg).text($res_entry("msg.info.content.201"));
					return false;
				}
				else
				{
					$("#"+input_msg).text("");
					return true;
				}
			}
    	}
    }
    
    function checkpRicetaxin(input,input_msg)
    {
    	
    	var isFLoasee=/^((0)|([1-9]+[0-9]*))((\.[0-9]{1,2})?)$/;
    	var pricetaxinValue=$.trim(input.value);
        
    	if(pricetaxinValue!="")
    	{
    		var flag=isFLoasee.test(pricetaxinValue);
    		if(!flag)
    		{
   	            $("#"+input_msg).text($res_entry("msg.info.content.202"));
   			    return false;
    		}
    		else
    		{
    			if(pricetaxinValue.indexOf(".")==-1){
   	 			if(pricetaxinValue.length>10){
   	 				$("#"+input_msg).text($res_entry("msg.info.content.202"));
   					return false;
   	 			} 				
    			}else{
    			   if(pricetaxinValue.indexOf(".")==0||pricetaxinValue.indexOf(".")==pricetaxinValue.length){
    			   		$("#"+input_msg).text($res_entry("msg.info.content.202"));
   					return false;
    			   }
    			   if(pricetaxinValue.substr(0,pricetaxinValue.indexOf(".")).length>10){
    			   		$("#"+input_msg).text($res_entry("msg.info.content.202"));
   					return false;
    			   }
    			}
   			$("#"+input_msg).text("");
   			return true;
    		}
    	}
    	return true;
    }
    
    
    //日期校验,所选日期应早于当前系统日期时间
    function validatedate(datetarget,date_msg){
             var date=formatDBDate(document.getElementById(datetarget).value,14);
             var nowdate=getTimefromUTC("yyyy-mm-dd hh:mm:ss", new Date());
             var date1=formatDBDate(nowdate,14);
             if((date-date1)<0){
             	$("#"+date_msg).html($res_entry("msg.info.cast.23"));
             	return false;
             }else{
             	$("#"+date_msg).html("");
             	return true;
             }
    }
    
    function isPlusInteger(input,input_msg)
    {
    	var inputValue=$.trim(input.value);
    	if(inputValue=="")
    	{
    		$("#"+input_msg).text("");
    		return true;
    	}
    	else
    	{
			var integerRex=/^((0)|([1-9]+[0-9]*))$/;
			var rtnValue=integerRex.test(inputValue);
			if(rtnValue==false)
			{
					$("#"+input_msg).text($res_entry("msg.info.content.161"));
					return false;
			}
			else
			{
				$("#"+input_msg).text("");
				return true;
			}
    	}
    }
    
    //验证输入框中字数是否超过最大长度
	function isMaxLength(input,input_msg,len)
	{
	
  		if(utf8_strlen2(trim(input.value)) > len){
      		document.getElementById(input_msg).innerText = $res_entry("msg.check.string.prefix")+len+$res_entry("msg.check.string.suffix");//"超过最大长度限制:";
     		// document.getElementById(input.id).focus();
      		return false;
  		}else{
       		document.getElementById(input_msg).innerText = "";   
       		return true;   
 		}
	}
    
    //判断不可为空
	function isNull(input,input_msg)
	{
		if(trim(input.value) == '' || input.value == null){
       	 	document.getElementById(input_msg).innerText =$res_entry("msg.info.basicdata.033"); //"此项不可为空";
       	 	document.getElementById(input.id).focus();
        return false;
    	}
    	else
    	{
       		 document.getElementById(input_msg).innerText = "";
        	return true;
   		 }
	}
    
    var specialStr="!@#$%^&*;|?()[]{}<>\'\"";
    function isSpecialStr(checkstr,msgBox_id,specialStrParam)
{
	var valSpecialStr=specialStr;
	if(specialStrParam!="undefined"&&specialStrParam!= null)
	{
		valSpecialStr=specialStrParam;
	}
	var allValid = true;
	for (i = 0;i<valSpecialStr.length;i++)
	{
		ch = valSpecialStr.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = false;
			break;
	 	}
  	}
	if(!allValid)
	{
		document.getElementById(msgBox_id).innerText=$res_entry("msg.info.channel.023")+valSpecialStr;
	}
	return allValid;
}


    var specialStr1=",!@#$%^&*;|?()[]{}<>\'\"";
    function isSpecialStr1(checkstr,msgBox_id,specialStrParam)
{
	var valSpecialStr=specialStr1;
	if(specialStrParam!="undefined"&&specialStrParam!= null)
	{
		valSpecialStr=specialStrParam;
	}
	var allValid = true;
	for (i = 0;i<valSpecialStr.length;i++)
	{
		ch = valSpecialStr.charAt(i);
	 	if(checkstr.indexOf(ch) > -1)
	 	{
			allValid = false;
			break;
	 	}
  	}
	if(!allValid)
	{
		document.getElementById(msgBox_id).innerText=$res_entry("msg.info.channel.023")+valSpecialStr;
	}
	return allValid;
}



    //开始时间必须小于结束时间      
   function betweenTimeValidate(startTimeTarget,endTimeTarget,date_msg)
   {
    	startTimeTarget=document.getElementById(startTimeTarget).value;
   	 	endTimeTarget=document.getElementById(endTimeTarget).value;
     if(startTimeTarget!="" && endTimeTarget!=""){
     	if(startTimeTarget>endTimeTarget){
	     	$("#"+date_msg).html($res_entry("msg.info.channel.007"));
	     	return false;
	    }
	    else
	    {
	    	return true;
	    }
	    
     }
     return true;
    }
    
    
    
    function calStatus(keyvalue)
    {
    	 keyvalue['status'] = statusMap[keyvalue['status']];
	 	return keyvalue;
    	
    }
    
    function convertDateTimeStrFromDB(dateTime)
    {
    		var year=dateTime.substring(0,4);
    		var month=dateTime.substring(4,6);
    		var day=dateTime.substring(6,8);
    		var hour=dateTime.substring(8,10);		
    		var minute=dateTime.substring(10,12);		
    		var second=dateTime.substring(12,14);	
    		var dateTime=year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
    		return dateTime;
    		
    }
    
    function calCreateTime(keyvalue)
    {
    	var createTimeValue=keyvalue['createtime'];
    	if(createTimeValue!="")
    	{
    		var dateTimeStr=convertDateTimeStrFromDB(createTimeValue);	
    	
    		keyvalue['createtime']=dateTimeStr;
    	}
    	else
    	{
    		keyvalue['createtime']=$res_entry("msg.info.content.050");
    	}
    	return keyvalue;
    }
    
    
    
    function addSeriesResult(result)
    {
    	var resultFlag=parseInt(result);
    	var url=escape("CMS/content/seriesContentManager.html?isLoad=true");
    	switch(resultFlag)
    	{
			case 0:openMsg("0:"+$res_entry("msg.info.content.208"),url,true);break;
			case 101:openMsg("1:"+$res_entry("msg.info.content.209"),url,false);break;
			case 102:openMsg("1:"+$res_entry("msg.info.content.210"),url,false);break;
			case 103:openMsg("1:"+$res_entry("msg.info.content.211"),url,false);break;
			case 104:openMsg("1:"+$res_entry("msg.info.content.212"),url,false);break;
			case 105:openMsg("1:"+$res_entry("msg.info.content.213"),url,false);break;
			default:openMsg("1:"+$res_entry("msg.info.content.129"),url,false);
    	}
    	
    }
    
    
    
    function updateSeriesResult(result)
    {
    	var resultFlag=parseInt(result);
    	var url=escape("CMS/content/seriesContentManager.html?isLoad=true");
    	switch(resultFlag)
    	{
			case 0:openMsg("0:"+$res_entry("msg.info.content.214"),url,false);break;
			case 100:openMsg("1:"+$res_entry("msg.info.content.054"),url,false);break;
			case 101:openMsg("1:"+$res_entry("msg.info.content.215"),url,false);break;
			case 102:openMsg("1:"+$res_entry("msg.info.content.060"),url,false);break;
			case 1056030:openMsg("1:"+$res_entry("msg.info.content.061"),url,false);break;
			case 1056040:openMsg("1:"+$res_entry("msg.info.content.063"),url,false);break;
			case 1056050:openMsg("1:"+$res_entry("msg.info.content.064"),url,false);break;
			case 1056060:openMsg("1:"+$res_entry("msg.info.content.065"),url,false);break;
			default:openMsg("1:"+$res_entry("msg.info.content.129"),url,false);
    	}
    }
    
    
    function convertedEffectTime(keyvalue)
    {
    	var time = getRiaCtrlValue('effectivedate');
   		return getTime(time);
    }
    function convertedExpiry(keyvalue)
    {
    	var time =getRiaCtrlValue('expirydate');
    	return getTime(time);
    }
    
    function convertedLlicensofflinet(keyvalue)
    {
    	var time=getRiaCtrlValue('llicensofflinetime');
    	return getTime(time);
    }
    
    function convertedDeleteT(keyvalue)
    {
    	var time=getRiaCtrlValue('deletetime');
    	return getTime(time);
    }
    
    //内容查看页面显示"地区类型"
    function countrytypeCalculate(keyvalue)
    {
   		 provinceMap=provinceMap.rtnValue;
   		 return provinceMap[getRiaCtrlValue("countrytype")];
    }
    
    //内容查看页面显示"区域"
    function providCalculate(keyvalue)
    {
   		countryMap=countryMap.rtnValue;
   		for(var i =0;i<countryMap["valueList"].length;i++) 
		{ 
			if(getRiaCtrlValue("provid")==countryMap["valueList"][i])
			{
				return countryMap["textList"][i];
				
			}
		}
    } 
    //内容查看页面显示"交易类别"
    function tradetypeCalculate(keyvalue)
    {
   		 feetypeMap=feetypeMap.rtnValue;
   		 return feetypeMap[getRiaCtrlValue("tradetype")];
    }
    
    //内容查看页面显示"推荐星级"
    function recommendstartCalculate(keyvalue)
    {
   		var recommendstart = getRiaCtrlValue("recommendstart");
	   	if(recommendstart>=1 && recommendstart<3){
	        return $res_entry("msg.info.content.009");
	    }else if(recommendstart>=3 && recommendstart<5){
	        return $res_entry("msg.info.content.010");
	    }else if(recommendstart>=5 && recommendstart<7){
	        return $res_entry("msg.info.content.011");
	    }else if(recommendstart>=7 && recommendstart<9){
	        return $res_entry("msg.info.content.012");
	    }else if(recommendstart>=9 && recommendstart<=10){
	        return $res_entry("msg.info.content.013");
	    }else{
	        return "";
	    }
    }
    
    function calOrgairdD(keyvalue)
    {
    	var time=getRiaCtrlValue("orgairdate");
    	return getTime(time);
    }
    
    
    function calMacrovision(keyvalue)
    {
    	var value=getRiaCtrlValue("macrovision");
    	if(value=="0")
    	{
    		return $res_entry("label.cms.channel.info.macrovision0");
    	}
    	else if(value=="1")
    	{
    		return $res_entry("label.cms.channel.info.macrovision1");
    	}
    }
    function calIsValid(keyvalue)
    {
    	var value=getRiaCtrlValue("isvalid");
		if(value=="0")
		{
			return $res_entry("msg.info.basicdata.022");
		}
		else if(value=="1")
		{
			return $res_entry("msg.info.basicdata.023");
		}
    }
    
    
    function calLicensingstart(keyvalue)
    {
    	var time=getRiaCtrlValue("licensingstart");
    	return getTime(time);
    }
    
    function calLicensingend(keyvalue)
    {
    	var time=getRiaCtrlValue("licensingend");
    	return getTime(time);
    }
    
     //内容查看页面显示"字幕语言"
    function calSubtitleL(keyvalue)
    {
   		return languageMap[getRiaCtrlValue("subtitlelanguage")];
    }
    
    //内容查看页面显示"配音语种"
    function dubbinglanguageCalculate(keyvalue)
    {
   		return languageMap[getRiaCtrlValue("dubbinglanguage")];
    }
    
    //内容查看页面显示"内容级别"
    function ratingCalculate(keyvalue)
    {
   		limitlevelMap=limitlevelMap.rtnValue;
   		return limitlevelMap[getRiaCtrlValue("rating")];
    }
   
    
	function getTime(str)
	{
		if (str.length > 8)
		{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
			
		}else{
		
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
		}
	}
    
    function setButtonState(btnID,state)
    {
    		if(!state)
    		{
				document.getElementById(btnID).style.display="none";
    		}
    }
    
    
    function initReleaseYear(comID)
    {
		var releaseYearEle=document.getElementById(comID);
		var option;
		var text;
		
		option=document.createElement("option");
	
		text=document.createTextNode($res_entry("label.selectdefault"));
		option.setAttribute("value","");
		option.appendChild(text);
		releaseYearEle.appendChild(option);
		
		for(var i=1900;i<=2050;i++)
		{
			option=document.createElement("option");
			text=document.createTextNode(i);
			option.setAttribute("value",i);
			option.appendChild(text);
			releaseYearEle.appendChild(option);
		}
    }
    
    function calLanguage()
    {
		return languageMap[getRiaCtrlValue("language")];
    }
    
function getMenuUrl(pref,url){
 //  alert("pref="+pref+"*url="+url);
  if(url.indexOf("http://")!=-1)
  return url;
  else
    return pref+url;
}

function isEmpty(str)
{
   var i;
   for (i = 0;i < str.length;i++)
   {
      if (str.charAt(i) != ' ')
      { 
         return false;
      }
   }
   return true;
}



function strlen(str)
{
   var i;
   var len;
   len = 0;
   for (i = 0; i < str.length; i++)
   {
      if (str.charCodeAt(i)>255)
      { 
          len+=2;
      }
      else
      {
           len++;
      }
   }
   return len;
}


function checkInvalidChar(field)
{
	for (i = 0; i < field.length; i++)
	{
		var c = field.charAt(i);

		if( c == '&' || c == '<' || c == ';' || c == '*' || c == '(' || c == ')'
		   || c == '>' || c == '"'||c == ','
		   || c == '@' || c == '#'|| c == "'" || c == '?'
		   || c == '$' || c == '^' ||  c == '~' || c == '%' || c == '!' || c == '=')
		{
			return true;
		}
	}
	return false;
}


function isEarlyDate(deployDate)
{
	var today = new Date();
	
	
	if(((parseInt(today.getYear())-parseInt(deployDate.substring(0,4))) * 356 +
        (parseInt(today.getMonth())-parseInt(deployDate.substring(5,7))+1) * 31 +
        (parseInt(today.getDate())-parseInt(deployDate.substring(8,10)))) > 0)
 	{
  		return true;
 	}
 	return false;
}
	 

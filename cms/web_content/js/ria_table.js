// @version 3.00.04

function getTableById(tib)
{
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var Mtable = {};//\u5bf9\u8c61\u6a21\u578b
    Mtable.id = tib;
 	//\u5c5e\u6027
    Mtable.table = table;
    Mtable.rowcount = table.rows.length; //\u8868\u683c\u603b\u884c\u6570
    Mtable.pagesize = tableModel_arr[tib].pg.perPageCount; //\u6bcf\u9875\u8bb0\u5f55\u6570
    Mtable.pageIndex = tableModel_arr[tib].pg.page;//\u5f53\u524d\u7b2c\u51e0\u9875
    Mtable.columncount = tableModel_arr[tib].cells.length;//\u603b\u5217\u6570
    Mtable.pagecount = tableModel_arr[tib].pg.pageCount;//\u603b\u9875\u6570
    Mtable.allcount = tableModel_arr[tib].pg.totalrow;//\u603b\u7684\u8bb0\u5f55\u6570
    Mtable.condition = tableModel_arr[tib].condition;//\u5f53\u524d\u67e5\u8be2\u6761\u4ef6
    Mtable.data = tableModel_arr[tib].pageData;//\u8868\u683c\u6570\u636e
    //\u65b9\u6cd5
	//\u83b7\u53d6\u6307\u5b9a\u5750\u6807\u7684\u5355\u5143\u4e2a\u5bf9\u8c61
    Mtable.getCell = function(x, y) {
        return table.rows[x].cells[y];
    };
	//\u83b7\u53d6\u6307\u5b9a\u5e8f\u5217\u7684\u884c\u5bf9\u8c61
    Mtable.getRow = function(m) {
        return table.rows[m];
    };
	//\u5728\u8868\u683c\u672b\u5c3e\u65b0\u589e\u52a0\u4e00\u884c
    /*1.\u65e0\u53c2\u6570\u65f6\u5220\u9664\u8868\u683c\u672b\u5c3e\u884c
      2.\u6709\u53c2\u6570\u65f6 n \u4ee3\u8868\u884c\u5e8f\u53f7*/
    Mtable.insertRow = function() {
        addOneRow(tib);
    };
    Mtable.deleteRow = function(n)
    {
        if (n == null || n == '')
        {
            n = parseInt(table.rows.length - 1);
        }
        var row = table.rows[n];
        deleteOneRow(tib, row);
    };
 	//\u53d6\u5f97\u8868\u683c\u5217\u5bf9\u8c61
    Mtable.getColumn = function(m)
    {
        var tb_arr = new Array();
        var rows = table.rows;
        for (var i = 0; i < rows.length; i++)
        {
            var row = rows[i];
            tb_arr.push(row.cells[m]);
        }
        return tb_arr;
    };
 	//\u53d6\u5f97\u8868\u683c\u884c\u503c\u5bf9\u8c61\u6570\u7ec4
    Mtable.getRowValues = function() {
        return getTable_objTrs(tib);
    };
 	//\u53d6\u5f97\u8868\u683c\u5217\u503c\u5bf9\u8c61\u6570\u7ec4
    Mtable.getColumnValues = function(index) {
        return getTable_Tds(tib, index);
    };
 	//\u53d6\u5f97\u8868\u683c\u884c\u3001\u5217\u4e8c\u7ef4\u503c\u6570\u7ec4
    Mtable.getAllValues = function() {
        return getTable_Trs(tib);
    };
 	//\u53d6\u5f97\u67d0\u4e00\u5217\u9009\u4e2d\u7684\u503c\u6570\u7ec4\uff08\u8be5\u5217\u4e3aradio\u6216checkbox\uff09
    Mtable.getCheckedValues = function(index) {
        return getTable_Td_CheckedValue(tib, index);
    };
 	//\u91cd\u7f6e\u53ef\u7f16\u8f91\u8868\u683c \u6e05\u7a7a\u6240\u6709\u5355\u5143\u683c\u7684\u503c
    Mtable.reset = function()
    {
        var initrow = parseInt(table.rows.length - 1);
        for (var i = initrow; i > 0; i --)
        {
            table.deleteRow(parseInt(i));
        }
        for (var i = 0; i < initrow; i ++)
        {
			//\u6e05\u7a7a\u4e00\u884c\uff0c\u4e5f\u5c31\u662f\u65b0\u5efa\u4e00\u884c
            create_new_EditTableRow(tableModel_arr[tib].tableId, tib);
        }
    };
    Mtable.refresh = function()//\u5237\u65b0\u5206\u9875\u8868\u683c
    {
        var condition = tableModel_arr[tib].condition;
        if (condition == null)
        {
            return;
        }
        if (tableModel_arr[tib].turnpage == "true")
        {
            var pageNum = this.pageIndex;
            var perPageCount = this.pagesize;
            
            if(tableModel_arr[tid].sidParamCount == 4 ) {
				getFourParamTurnpageData(tib, condition, pageNum, perPageCount); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
			} else {
            	getTurnPageData(tib, condition, pageNum, perPageCount); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
			}
			
        }
        else if (tableModel_arr[tib].editable == "true")
        {
            getEditPageData(tib, condition); //\u53ef\u7f16\u8f91\u8868\u683c
        }
    };
 	//\u83b7\u53d6\u8868\u683c\u4e2d\u9009\u62e9\u884c\u5bf9\u8c61\u503c\u6570\u7ec4
    Mtable.getCheckedRowValues = function() {
        return getCheckedRowsValue(tib);
    };

    getColumns = function()
    {
        var cells = new Array();
        for (var i = 0; i < Mtable.columncount; i ++)
        {
            cells.push(Mtable.getColumn(i));
        }
        return cells;
    };
 	
 	//\u8868\u683c\u4e2d\u6587\u4ef6\u4e0a\u4f20
    Mtable.upload = function(o, sid, field, anotherField) {
        uploadFile(o, sid, field, anotherField)
    };
 	//\u96c6\u5408
    Mtable.rows = table.rows;//\u5305\u542b\u884c\u5bf9\u8c61\u7684\u6570\u7ec4
    Mtable.columns = eval("getColumns()");//\u5305\u542b\u5217\u5bf9\u8c61\u7684\u6570\u7ec4

    return Mtable;
}
;

////\u53d6\u53ef\u7f16\u8f91\u8868\u683c\u4e2d\u67d0\u4e00\u5217\u4e2d\u7684input \u548c select\u7684value \u503c\u4ee5\u6570\u7ec4\u7684\u5f62\u5f0f\u8fd4\u56de
getTable_Tds = function(tib, td_index)
{

    var td_value_arr = new Array();
  ////////\u83b7\u53d6\u5185\u5b58\u8868
    var table = tableModel_arr[tib].table;

    var table_rows = table.rows;
    var table_cells = table_rows[1].cells;

  //\u53d6\u6821\u9a8c\u51fd\u6570
    var validate = tableModel_arr[tib].cells[td_index - 1].validate;
    var datatype = tableModel_arr[tib].cells[td_index - 1].datatype;
    var formate = tableModel_arr[tib].date_formate;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index - 1];
        var obj_select = obj.getElementsByTagName("SELECT");
        var obj_input = obj.getElementsByTagName("INPUT");
	
	//\u5faa\u73af\u53d6\u503c
        try {
            for (var m = 0; m < obj_select.length; m ++)
            {
                td_value_arr.push(obj_select[m].value);
            }
		//
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + obj_input[n].value + "')");
                }
                else if (datatype != null && datatype != "" && obj_input[n].type == "text")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase() == 'date')
                    {
                        if(formate == null || formate == "null") formate = "yyyy-mm-dd";
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
                td_value_arr.push(value);
            }
        } catch(ee) 
		{
            log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
            return false;
        }
    }

    return td_value_arr;
};

getCheckedRowsValue = function(tib)
{
    for (var i = 0; i < tableModel_arr[tib].cells.length; i++)
    {
        var iskey = tableModel_arr[tib].cells[i].getAttribute("iskey");
        if (iskey == "true")
        {
            return getCheckedValues(tib, i);
            break;
        }
    }
};

getCheckedValues = function (tib, td_index)
{
    var table = tableModel_arr[tib].table;
   //\u83b7\u5f97\u5217\u5c5e\u6027\u6570\u7ec4
    var checkedrows = new Array();
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index];
        var obj_input = obj.getElementsByTagName("INPUT");

        if (obj_input.length < 1)
            alert($res_entry('ui.control.table.checkvalue.001','\u7b2c') + parseInt(td_index + 1) + $res_entry('ui.control.table.checkvalue.002','\u5217\u6ca1\u6709\u53ef\u9009\u9879!'));

	//\u5faa\u73af\u53d6\u503c
        for (var n = 0; n < obj_input.length; n++)
        {
            if (obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
            {
                if (obj_input[n].checked == true)
                {
                    var oo = getTable_objTr(tib, i);
                    checkedrows.push(oo);
                    break;
                }
            }
        }
    }
    return checkedrows;
};

getTable_Td_CheckedValue = function (tib, td_index)
{
    var table = tableModel_arr[tib].table;
    var td_checkedvalue = new Array();
    var table_rows = table.rows;
    var table_cells = table_rows[1].cells;

    for (var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
    {
        var obj = table_rows[i].cells[td_index - 1];
        var obj_input = obj.getElementsByTagName("INPUT");

        if (obj_input.length < 1)
            //alert("\u7b2c" + td_index + "\u5217\u6ca1\u6709\u53ef\u9009\u9879!");
			//alert($res_entry('ui.control.table.checkvalue.001') + parseInt(td_index + 1) + $res_entry('ui.control.table.checkvalue.002'));
			continue;
	//\u5faa\u73af\u53d6\u503c
        for (var n = 0; n < obj_input.length; n++)
        {
            if (obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
            {
                if (obj_input[n].checked == true)
                {
                    // alert(obj_input[n].value);
                    td_checkedvalue.push(obj_input[n].value);
                }
            }
        }
    }
    return td_checkedvalue;
};


//\u83b7\u53d6\u53ef\u7f16\u8f91\u8868\u683c\u4e2d\u67d0\u4e00\u884c\u4e2d\u7684input \u548c select \u6807\u7b7e\u7684value \u503c\u4ee5\u6570\u7ec4\u7684\u5f62\u5f0f\u8fd4\u56de
getTable_Tr = function(tib, tr_index)
{
    var tr_value = new Array();
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var props = tableModel_arr[tib].props;
    var formate = tableModel_arr[tib].date_formate;
    var table_row = table.rows[tr_index];
    var table_cells = table_row.cells;

    try {
        for (var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
        {
            var obj = table_row.cells[i];
            var obj_select = obj.getElementsByTagName("SELECT");
            var obj_input = obj.getElementsByTagName("INPUT");
		//\u53d6\u6821\u9a8c\u51fd\u6570
            var validate = tableModel_arr[tib].cells[i].validate;
            var datatype = tableModel_arr[tib].cells[i].datatype;

		//\u5faa\u73af\u53d6\u503c
            for (var m = 0; m < obj_select.length; m++)
            {
                var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + obj_select[m].value + "')");
                }
                else if (datatype != null && datatype != "")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    //\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
                }
                if (rtn == false)
                {
                    obj_select[m].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }

                tr_value.push(obj_select[m].value);
            }
		//
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "")
                {
                    rtn = eval(validate + "('" + value + "')");
                }
                else if (datatype != null && datatype != "" && (obj_input[n].type == "text" || obj_input[n].type == "hidden"))//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    if (obj_input[n].isDate == "false") continue;
                    //2008-10-23 wxr:\u4e3a\u4ec0\u4e48\u4e5f\u8981\u5bf9\u9690\u85cf\u57df\u8fdb\u884c\u6821\u9a8c\u5462? \u9664\u975e\u5b83\u5185\u90e8\u6307\u5b9a\u4e86datatype
                    if(obj_input[n].type == "hidden")
                   	{
                   		var hide_datatype = obj_input[n].getAttribute('datatype');
                   		if(hide_datatype != null && hide_datatype != "")
                   			rtn = verifyInput(obj_input[n], hide_datatype);
                   	}
                   	else
                    rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase().indexOf("date") != -1 && obj_input[n].isDate == "true")
                    {
                    	if(formate == null || formate == "null") formate = "yyyy-mm-dd";
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
                tr_value.push(value);
            }
        }
    } catch(ee) 
	{
        log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
        return false;
    }
    return tr_value;
};

function getTable_objTr(tib, tr_index, isCheck)
{
    var tr_value = new Object();
    var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var props = tableModel_arr[tib].props;
    var formate = tableModel_arr[tib].date_formate;
    var table_row = table.rows[tr_index];
    var table_cells = table_row.cells;

    SSB_RIA_initReturnValueObject(tr_value, props);
    try {
        for (var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
        {
            var prop = props[i];
            var obj = table_row.cells[i];
            var obj_select = obj.getElementsByTagName("SELECT");
            var obj_input = obj.getElementsByTagName("INPUT");
            var validate = tableModel_arr[tib].cells[i].validate;
            var datatype = tableModel_arr[tib].cells[i].datatype;
		//\u5faa\u73af\u53d6\u503c
            for (var m = 0; m < obj_select.length; m ++)
            {
                var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "" && isCheck != false)
                {
                    rtn = eval(validate + "('" + obj_select[m].value + "')");
                }
                else if (datatype != null && datatype != "" && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    //\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
                }
                if (rtn == false)
                {
                    obj_select[m].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
                if (obj_select[m].getAttribute("prop") != null && obj_select[m].getAttribute("prop") != "")
                {
                    prop = obj_select[m].getAttribute("prop");
                }
			
			//\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
                if (prop != null && prop != "")
                {
                    eval("tr_value." + prop + "='" + obj_select[m].value + "'");
                }
            }
			
            for (var n = 0; n < obj_input.length; n++)
            {
                var rtn = true;
                var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
                if (validate != null && validate != "" && isCheck != false)
                {
                    rtn = eval(validate + "('" + value + "')");
                }
                else if (datatype != null && datatype != "" && (obj_input[n].type == "text" || obj_input[n].type == "hidden") && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
                {
                    if (obj_input[n].isDate == "false") continue;
                    //2008-10-23 wxr:\u4e3a\u4ec0\u4e48\u4e5f\u8981\u5bf9\u9690\u85cf\u57df\u8fdb\u884c\u6821\u9a8c\u5462? \u9664\u975e\u5b83\u5185\u90e8\u6307\u5b9a\u4e86datatype
                    if(obj_input[n].type == "hidden")
                   	{
                   		var hide_datatype = obj_input[n].getAttribute('datatype');
                   		if(hide_datatype != null && hide_datatype != "")
                   			rtn = verifyInput(obj_input[n], hide_datatype);
                   	}
                   	else
                    rtn = verifyInput(obj_input[n], datatype);
					//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
                    if (datatype.toLowerCase().indexOf("date") != -1 && $(obj_input[n]).attr("isdate") == "true")
                    {
                        if(formate == null || formate == "null") formate = "yyyy-mm-dd";
                        value = getUTCfromTime(formate, value);
                    }
                }
                if (rtn == false)
                {
                    obj_input[n].focus();
                    throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");
                }
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
                if (obj_input[n].getAttribute("prop") != null && obj_input[n].getAttribute("prop") != "")
                {
                    var ctr_prop = obj_input[n].getAttribute("prop");
                    eval("tr_value." + ctr_prop + "='" + value + "'");
                }
                    //\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
                else if (prop != null && prop != "")
                {
                    if (obj_input[n].type == 'radio' || obj_input[n].type == 'checkbox')
                    {
                        if (obj_input[n].checked == true)
                            eval("tr_value." + prop + "='" + value + "'");
                    }
                    else if (datatype && datatype.toLowerCase().indexOf("date") != -1)
                    {
                    	//\u5982\u679c\u662f\u65f6\u95f4\u7c7b\u578b, \u4ee5long\u578b\u8fd4\u56de, \u800c\u4e0d\u662f\u5b57\u7b26\u4e32
						if(value.constructor == String) {
							if(formate == null || formate == "null") formate = "yyyy-mm-dd";
							value = getUTCfromTime(formate, value);
						}
                        eval("tr_value." + prop + "=" + value);
                    }
                    else
                    {
                        eval("tr_value." + prop + "='" + value + "'");
                    }
                }
            }

        }
    } catch(ee) {
        //alert("ErrorMessage is:" + ee.message);
        log.info("ErrorMessage is:" + ee.message + "--description:" + ee.description);
        return false;
    }
    return tr_value;
}
;

getTable_Trs = function(tib)
{
    var trs_arr = new Array();//\u5217\u6570\u7ec4
    var table = tableModel_arr[tib].table;
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i ++)
    {
        var tr_value = getTable_Tr(tib, i);
        if (tr_value == false)
        {
            return false;
            break;
        }
        trs_arr.push(tr_value);
    }

    return trs_arr;
};

getTable_objTrs = function(tib, isCheck)
{
    var trs_arr = new Array();//\u5217\u6570\u7ec4
    var table = tableModel_arr[tib].table;
    var table_rows = table.rows;

    for (var i = 1; i < table_rows.length; i ++)
    {
        var tr_value = getTable_objTr(tib, i, isCheck);
        if (tr_value == false)
        {
            return false;
            break;
        }
        trs_arr.push(tr_value);
    }

    return trs_arr;
};

function getSortTurnPageData(tid, condition, pageNum, perPageCount, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount;
      	  
	 //\u67e5\u8be2\u6761\u4ef6
    var oCondition = condition;
 	 //\u8d77\u59cb\u884c
    var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
    var fetchSize = perPageCount;
    var oCondition = JSON.stringify(condition);
    var json;
		
	//\u5f53\u603b\u6761\u6570\u4e3a0\u6216\u8005\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u603b\u6761\u6570\u65f6\uff0c\u8003\u8651\u517c\u5bb9\u4ee5\u524d\u7684\u67e5\u8be2\u670d\u52a1\uff0c\u4f203\u4e2a\u53c2\u6570	
    if (tableModel_arr[tid].searchSize || tableModel_arr[tid].pg.totalrow == 0)
        json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
    //\u4e0d\u60f3\u6bcf\u6b21\u90fd\u67e5\u8be2\u603b\u6761\u6570\uff0c\u4f20\u56db\u4e2a\u53c2\u6570
    else if (!tableModel_arr[tid].searchSize)
	{
		if(tableModel_arr[tid].isserversort ==true)
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
		else
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + ", false]";
	}
	//add wanghao 
	if(sortColumn_table =="")
	{
		
	}
	else if (tableModel_arr[tid].isserversort ==true)
    {
		json = json.substring(0, json.lastIndexOf("]")) + ", {isAlwaysSearchSize:"+tableModel_arr[tid].searchSize+",orderByColumn:'"+sortColumn_table+"',isAsc:"+isAsc_table[sortColumn_table]+"}]";
	}
	
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callTSid(sid, preCondition, tid, json);
};

function getFourParamTurnpageData(tid, condition, pageNum, perPageCount, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount; 	
    var rangeStart = parseInt((pageNum - 1) * perPageCount);

	var newCondition = [];
	if(Object.prototype.toString.apply(condition) === '[object Array]' && condition.length > 0) {
		 newCondition.push(condition[0]);
	}
	else {
		 newCondition.push([{}]);
	}
    newCondition.push(rangeStart);
    newCondition.push(perPageCount);
    
    var the4thParam = null;
   
    if (tableModel_arr[tid].isserversort ==true && sortColumn_table && sortColumn_table != "") {
    	the4thParam = {};
    	the4thParam.isAlwaysSearchSize = tableModel_arr[tid].searchSize;
    	the4thParam.orderByColumn = sortColumn_table;
    	the4thParam.isAsc = isAsc_table[sortColumn_table];
	}
    
    newCondition.push(the4thParam);
    

    callTSid(tableModel_arr[tid].sid, preCondition, tid, JSON.stringify(newCondition));
};

function getTurnPageData(tid, condition, pageNum, perPageCount, preCondition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
    tableModel_arr[tid].pg.turnTo = pageNum;
    tableModel_arr[tid].pg.perPageCount = perPageCount;
      	  
	 //\u67e5\u8be2\u6761\u4ef6
    var oCondition = condition;
 	 //\u8d77\u59cb\u884c
    var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
    var fetchSize = perPageCount;
    var oCondition = JSON.stringify(condition);
    var json;
		
	//\u5f53\u603b\u6761\u6570\u4e3a0\u6216\u8005\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u603b\u6761\u6570\u65f6\uff0c\u8003\u8651\u517c\u5bb9\u4ee5\u524d\u7684\u67e5\u8be2\u670d\u52a1\uff0c\u4f203\u4e2a\u53c2\u6570	
    if (tableModel_arr[tid].searchSize || tableModel_arr[tid].pg.totalrow == 0)
        json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
        //\u4e0d\u60f3\u6bcf\u6b21\u90fd\u67e5\u8be2\u603b\u6761\u6570\uff0c\u4f20\u56db\u4e2a\u53c2\u6570
    else if (!tableModel_arr[tid].searchSize)
	{
		if(tableModel_arr[tid].isserversort ==true)
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + "]";
		else
			json = oCondition.substring(0, oCondition.lastIndexOf("]")) + "," + rangeStart + "," + fetchSize + ", false]";
	}
	
	
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callTSid(sid, preCondition, tid, json);
}
;

getEditPageData = function(tid, condition)
{
    //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
    tableModel_arr[tid].condition = condition;
	//RIA\u670d\u52a1id
    var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
    callSid(sid, condition, tid);
};

function searchTableData(sid, condition, tid)
{
    var perPage = 0;
    tableModel_arr[tid].sid = sid;
    if (tableModel_arr[tid].turnpage == "true" || tableModel_arr[tid].editable == "false")
    {
        perPage = tableModel_arr[tid].pg.perPageCount;// \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
        var bindObject = getBindObject(condition);
		//\u6570\u636e\u6821\u9a8c\u5931\u8d25\u5219\u76f4\u63a5\u9000\u51fa
        if (bindObject === false) {
			return;
		}
		
        //\u521d\u59cb\u5316\u6392\u5e8f\u56fe\u6807
        sortColumn_table = "";
        isAsc_table = {};
        $(tableModel_arr[tid].table.rows[0]).find("div[id^='ssbSortImg']").each(function(){
        	if (isIE())
	        	$(this).attr("className","IEsortASC");
	        else 
	            $(this).attr("className","FFsortASC");
        });
        
        if(tableModel_arr[tid].sidParamCount == 4 ) {
			getFourParamTurnpageData(tid, bindObject, '1', perPage, condition);
		} else {
	        getTurnPageData(tid, bindObject, '1', perPage, condition); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
		}
    }
    else if (tableModel_arr[tid].turnpage == "false" || tableModel_arr[tid].editable == "true")
    {
        perPage = tableModel_arr[tid].initrow;
        getEditPageData(tid, condition); //\u53ef\u7f16\u8f91\u8868\u683c
    }
    else if (tableModel_arr[tid].turnpage == tableModel_arr[tid].editable)
    {
        //alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u4e0d\u80fd\u76f8\u540c!");
        alert($res_entry('ui.control.table.searchtable.error.001','\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u4e0d\u80fd\u76f8\u540c!'));
    }
    else
    {
        //alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u914d\u7f6e\u6709\u8bef!");
         alert($res_entry('ui.control.table.searchtable.error.002','\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u914d\u7f6e\u6709\u8bef!'));
    }
}
;


setTableValue = function(mid, pageinfo)
{
    //\u91cd\u7f6e\u8868\u5934\u590d\u9009\u6846
    tableModel_arr[mid].pageData = null;
    var title_checkbox = document.getElementById(mid + "allCheck");
    if (title_checkbox) title_checkbox.checked = false;

	//\u5206\u9875\u8868\u683c\u586b\u5145
    if (tableModel_arr[mid].turnpage == "true")
    {
		 //\u5f53\u7528\u6237\u8bbe\u7f6e\u603b\u662f\u67e5\u8be2\u6216\u8005\u5f53\u524d\u603b\u6761\u6570\u4e3a0\uff0c\u5219\u91cd\u65b0\u8bbe\u7f6e\u8bb0\u5f55\u603b\u6761\u6570
        if (tableModel_arr[mid].searchSize || tableModel_arr[mid].pg.totalrow == 0)
            tableModel_arr[mid].pg.totalrow = pageinfo.totalCount; 

        tableModel_arr[mid].pg.printHtml(); // \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
        tableModel_arr[mid].pageData = pageinfo.data;
        pushData2ssbTable(mid, pageinfo.data);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
    }
	//\u53ef\u7f16\u8f91\u8868\u586b\u5145
    else 
    {
        tableModel_arr[mid].pageData = pageinfo;
        pushData2ssbTable(mid, pageinfo);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
    }
};

var checkedAll = function(td, tid)
{
    var id = '';
    if (typeof tid == 'object')
    {
        id = tid.id;
    }
    var input = td;
    while (td.tagName != "TH")
    {
        td = td.parentNode;
    }
    var tdindex = td.cellIndex;
    var table = getTableById(id);
    var columns = table.getColumn(tdindex);

    var selected = input.checked;

    if(selected == true){
        $(columns).find("input").each(function(index){
			this.checked = true;
            if(index > 0){
                var ck = $(this).attr("onclick");
				if(ck && ck.constructor == Function){
					ck = decodeURIComponent(ck);
					ck = ck.substring(ck.indexOf("{")+1, ck.lastIndexOf("}"));
				}
                try{
					eval(ck);
                } 
                catch (err){
                }
            }
        }); 
    }    
    else{
         $(columns).find("input").each(function(index){
		 	this.checked = false;
            if(index > 0){
               var ck = $(this).attr("onclick");
				if(ck && ck.constructor == Function){
					ck = decodeURIComponent(ck);
					ck = ck.substring(ck.indexOf("{")+1, ck.lastIndexOf("}"));
				}
                try{
					eval(ck);
                } 
                catch (err){
                }
            }            
        }); 
    }
};

//\u83b7\u53d6\u5168\u9009checkbox\u5bf9\u8c61
function getCheckBoxByTableId(tid)
{
	return document.getElementById(tid+"allCheck");
}

var tableModel_arr = new Array();//\u6a21\u578b\u6570\u7ec4
var ROW_CLASS_EVEN = "even";
var RIA_SCOPENAME = "Z";
var E_RESIZE = "e-resize";
var DISPLAYHEAD = "isDisplayHead";
var RIA_COLUMN = "z:COLUMN";
//\u662f\u5426\u5bfc\u51faExcel\u6587\u4ef6
var IS_EXCEL = "isexcel";
//\u6eda\u52a8\u6761\u8bbe\u7f6e
var ROW_NUM_FOR_SCROLL = "rownumforscroll";
var STYLE_FOR_SCROLL = "styleforscroll";
//\u5206\u9875\u9875\u9762\u8df3\u8f6c\u662f\u5426\u91c7\u7528\u4e0b\u62c9\u6846
var IS_LIST = "islist";
var COLUMN_TAGNAME = "column";
//\u662f\u5426\u603b\u662f\u66f4\u65b0\u8bb0\u5f55\u603b\u6570
var SEARCH_SIZE = "alwaysSearchSize";
//\u5bfc\u51faExcel\u6587\u4ef6\u65f6\u662f\u5426\u663e\u793a\u5217\u9009\u62e9\u754c\u9762
var IS_EXCEL_CONFIG = "isexcelconfig";
var IS_EXCEL_PROP = "isexcelprop";
var VSTATCAPTION = "vstatcaption";
var VSTATFORMULA = "vstatformula";
var CAPTIONSTYLE = "captionstyle";
var CAPTIONTIP = "captiontip";
var IS_EXCEL_STYLE = "isexcelstyle";
var EXCEL_TITLE_STYLE = "exceltitlestyle";
var EXCEL_FILE_STYLE = "excelfilestyle";
//add server sort
//add wanghao
var IS_SERVER_SORT = "isserversort";

var exportInfo = {
	filename:	"Excel.xls",	//\u5bfc\u51fa\u7684excel\u6587\u4ef6\u540d
	strategy:	"multisheet",	//xls\u5bfc\u51fa\u7b56\u7565 multisheet or multifile
	dateformat:	"yyyy-MM-dd",	//excel\u6587\u4ef6\u4e2d\u7684\u65e5\u671f\u683c\u5f0f\uff08Java\u8bed\u6cd5\uff09
	
	countperfetch:	5000,		//\u6bcf\u6b21\u53d6\u7684\u6570\u636e\u6761\u6570
	countpersheet:	65535		//\u8bbe\u7f6e\u6bcf\u4e2asheet\u9875\u5bb9\u7eb3\u6570\u636e\u6570\uff0c65535\u4e3a\u6700\u5927
}
var COUNTFORWARN = "countforwarn";

var init_ssb_tableModels = function(tb)
{
    // \u83b7\u53d6\u8868\u6a21\u578b\u5bf9\u8c61
    var Mobject = document.getElementById(tb);
	//\u5c06\u8868\u6a21\u578b\u5b58\u5165\u6a21\u578b\u6570\u7ec4
    tableModel_arr[tb] = Mobject;
    tableModel_arr[tb].isIE = isIE();
    var style = null;
	//\u83b7\u53d6className\u5c5e\u6027\u548cstyle\u5c5e\u6027
    if (isIE())
    {
        tableModel_arr[tb].MclassName = Mobject.className;
		try{
			style = Mobject.getAttribute("style").cssText;
		}
        catch(e){
			style=""
		}
    }
    else {
        tableModel_arr[tb].MclassName = Mobject.getAttribute("class");
        style = Mobject.getAttribute("style");
    }
    if (Mobject.style.display == 'none')
    {
        var Tstyle = style.replace('none', 'DISPLAY');
        tableModel_arr[tb].Mstyle = Tstyle;
    } else {
        Mobject.style.display = "none";//\u9690\u85cf\u6807\u7b7e
        tableModel_arr[tb].Mstyle = style;
    }
    tableModel_arr[tb].isExcel = $(Mobject).attr(IS_EXCEL);
    //tableModel_arr[tb].isExcel = Mobject.getAttribute(IS_EXCEL);
	////////////////\u5bfc\u51fa\u6587\u4ef6\u662f\u5426\u5e94\u7528\u6837\u5f0f
    if (Mobject.getAttribute(IS_EXCEL_STYLE) == "true")
        tableModel_arr[tb].IS_EXCEL_STYLE = true;
    else
        tableModel_arr[tb].IS_EXCEL_STYLE = false;
	
	////////////////\u9ed8\u8ba4\u663e\u793a\u8f93\u5165\u6846\u8df3\u8f6c\u6a21\u5f0f
    var naviBarStyle = $res_entry("ui.control.table.navbar.style","");
    if( naviBarStyle == "vpbx" ) {
		if (Mobject.getAttribute(IS_LIST) == "false")
			tableModel_arr[tb].isList = false;
		else
			tableModel_arr[tb].isList = true;
	} else {
		if (Mobject.getAttribute(IS_LIST) == "true")
			tableModel_arr[tb].isList = true;
		else
			tableModel_arr[tb].isList = false;
	}
	
	//\u9ed8\u8ba4\u8c03\u7528\u7684\u540e\u53f0\u670d\u52a1\u7684\u53c2\u6570\u4e2a\u6570
	if($(Mobject).attr("sidparamcount") == "4"){
		tableModel_arr[tb].sidParamCount = 4;
	} else {
		tableModel_arr[tb].sidParamCount = 3;
	}
		
	////////////////\u8d85\u8fc7\u591a\u5c11\u6761\u8bb0\u5f55\u663e\u793a\u6eda\u52a8\u6761
	var scrollnum = Mobject.getAttribute(ROW_NUM_FOR_SCROLL);
    if (!isNaN(scrollnum))
		tableModel_arr[tb].rownumforscroll = parseInt(scrollnum,10);
	else
		tableModel_arr[tb].rownumforscroll = "noscroll";
		
	////////////////\u662f\u5426\u4f7f\u7528\u8868\u5934\u62d6\u52a8\u6761
	var allowdragbar = Mobject.getAttribute("allowdragbar");
	var barStyle = $res_entry("ui.control.table.navbar.style","");
    if( barStyle == "vpbx" ) {
		if (allowdragbar=="true") 
			tableModel_arr[tb].allowdragbar = "true";
		else 
			tableModel_arr[tb].allowdragbar = "false";
	}
    else if (!allowdragbar || allowdragbar == "true") 
		tableModel_arr[tb].allowdragbar = "true";
	else
		tableModel_arr[tb].allowdragbar = "false";
	
	//xls\u5bfc\u51fa\u6761\u6570\u9650\u5236
	var countforwarn = Mobject.getAttribute(COUNTFORWARN);
	if(countforwarn && !isNaN(countforwarn)){
		tableModel_arr[tb].countforwarn = countforwarn;
	}
	else {
		tableModel_arr[tb].countforwarn = null;
	}
	
	var stylescroll = Mobject.getAttribute(STYLE_FOR_SCROLL);
    if (typeof(stylescroll) == "undefined" || stylescroll == null || stylescroll == "null")
		stylescroll = "";
	tableModel_arr[tb].styleforscroll = stylescroll;
		
	////////////////\u9ed8\u8ba4\u95ee\u9898\u66f4\u65b0\u603b\u8bb0\u5f55\u6570
    if (Mobject.getAttribute(SEARCH_SIZE) == "false")
        tableModel_arr[tb].searchSize = false;
    else
        tableModel_arr[tb].searchSize = true;
	
	//add wanghao	
	if (Mobject.getAttribute(IS_SERVER_SORT) == "true")
        tableModel_arr[tb].isserversort = true;
    else
        tableModel_arr[tb].isserversort = false;
	
    tableModel_arr[tb].isExcelConfig = Mobject.getAttribute(IS_EXCEL_CONFIG);
    var pg = new showPages('pg');//\u5206\u9875
    pg.tid = tb;
    tableModel_arr[tb].pg = pg;
    var props = new Array();//\u5217\u5c5e\u6027\u7684\u6570\u7ec4\u5bf9\u8c61
    var caption = new Array();//\u8868\u683c\u7684\u5217\u540d
    //\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u6a21\u578b
    var col = new Array();
    var clum = new Array();
    var excel_title = new Array();
    var excel_props = new Array();
    //\u8868\u5934\u6837\u5f0f\u6570\u7ec4
    var capstyles =  new Array();
    //\u8868\u5934\u63d0\u793a\u6570\u7ec4
    var captips =  new Array();
    //\u5bfc\u51faexcel\u6587\u4ef6\u6837\u5f0f,\u5982\u679c\u6ca1\u6709\u8bbe\u7f6etitle\u6837\u5f0f\u5219\u5168\u90e8\u4ee5file\u4e3a\u51c6, \u91c7\u7528\u952e\u503c\u5bf9\u7684\u65b9\u5f0f
    var excel_file_title = new Object();
    var excel_file = new Object();
    
    if (isIE())
    {
        // IE\u83b7\u53d6\u8868\u6a21\u578b\u7684\u5217\u6570\u7ec4
        var cd = Mobject.firstChild;
        while (cd != null)
        {
            if (cd.tagName === COLUMN_TAGNAME)
            {
                clum.push(cd);
            }
            cd = cd.nextSibling;
        }
    }
    else {
        clum = Mobject.getElementsByTagName(RIA_COLUMN);//\u706b\u72d0
    }
    for (var i = 0; i < clum.length; i ++)
    {
        if (clum[i].getAttribute("visible") != 'false')
        {
            col.push(clum[i]);
            capstyles.push(clum[i].getAttribute('CAPTIONSTYLE'));
            captips.push(clum[i].getAttribute('CAPTIONTIP'));
        }
        if (clum[i].getAttribute(IS_EXCEL_PROP) != 'false')
        {
			//\u8bbe\u7f6eExcel\u5bfc\u51fa\u65f6\u8981\u663e\u793a\u7684\u5217\u7684\u503c
            excel_title.push(clum[i].getAttribute("caption"));
            excel_props.push(clum[i].getAttribute("prop"));
            if(tableModel_arr[tb].IS_EXCEL_STYLE == true)
            {
            	//\u91c7\u7528\u4e0e\u5c5e\u6027\u7ed1\u5b9a\u7684\u65b9\u5f0f
            	eval('excel_file_title.' + excel_props[i]+ "= (clum[i].getAttribute(EXCEL_TITLE_STYLE) == null)?'':clum[i].getAttribute(EXCEL_TITLE_STYLE)");
            	eval('excel_file.' + excel_props[i]+ "= (clum[i].getAttribute(EXCEL_FILE_STYLE) == null)?'':clum[i].getAttribute(EXCEL_FILE_STYLE)");
    		}
        }
    }
	//\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u5bf9\u8c61\u96c6\u5408
    tableModel_arr[tb].cells = col;
    tableModel_arr[tb].clums = clum;
    tableModel_arr[tb].excel_title = excel_title;
    tableModel_arr[tb].excel_props = excel_props;
    for(var i=0; i<capstyles.length; i++)
    {
    	if(capstyles[i] == null || capstyles[i] == "null") capstyles[i]="";
    	if(captips[i] == null || captips[i] == "null") captips[i]="";
    }
    tableModel_arr[tb].capstyles = capstyles;
    tableModel_arr[tb].captiontips = captips;
	
	if(tableModel_arr[tb].IS_EXCEL_STYLE == true)
	{
		tableModel_arr[tb].excel_file_title = excel_file_title;
	    tableModel_arr[tb].excel_file = excel_file;
    }
    
	//////////////\u5982\u679c\u5217\u6570\u7ec4\u957f\u5ea6\u4e3a0\uff0c \u9000\u51fa\u521d\u59cb\u5316
    if (tableModel_arr[tb].clums.length == 0) {
        return false;
    }
	
	//\u521b\u5efa\u6a21\u578b\u8868\u5217\u7684\u5b50\u7ed3\u70b9\u6a21\u578b
    for (var j = 0; j < tableModel_arr[tb].cells.length; j++)
    {
        //\u83b7\u53d6\u5217\u6570\u503c\u6821\u9a8c\u51fd\u6570
        var checkfunction = tableModel_arr[tb].cells[j].getAttribute("validate");
        tableModel_arr[tb].cells[j].validate = checkfunction;

        var defaultCheck = tableModel_arr[tb].cells[j].getAttribute("datatype");
        tableModel_arr[tb].cells[j].datatype = defaultCheck;

        var td_Node_Children = tableModel_arr[tb].cells[j].childNodes;
        var td_Children = new Array();
        for (var m = 0; m < td_Node_Children.length; m ++)
        {
        	//\u5e94\u5f53\u628a\u6ce8\u91ca\u4e5f\u53bb\u6389nodevalue=8
            if (td_Node_Children[m].nodeType != 8 && td_Node_Children[m].tagName != null && td_Node_Children[m].tagName != "Z:COLUMN")
            {
                td_Children.push(td_Node_Children[m]);
            }
			
			else if(td_Node_Children[m].nodeType == 3 && $.trim(td_Node_Children[m].nodeValue).length > 0)
			{
 				var textvalue = td_Node_Children[m].nodeValue.match(/#nbsp;/gi);
				if(typeof(textvalue) != "undefined" && textvalue != null)
				{
					var space = document.createTextNode(" ");
					for(var i=0; i<textvalue.length; i++) td_Children.push(space);
				}
			}
        }
		
		//\u5c06td\u5b50\u7ed3\u70b9\u7684\u96c6\u5408\u653e\u5230\u6a21\u578b\u5217\u4e2d
        tableModel_arr[tb].cells[j].td_Children = td_Children;

    }
    //\u521b\u5efa\u5185\u5b58\u8868ID\u5c5e\u6027,\u5e76\u653e\u5165\u6a21\u578b\u5bf9\u8c61\u7684tableId\u5c5e\u6027\u4e2d,\u89c4\u5219\u662f\u6a21\u578bID+"_ssb"
    tableModel_arr[tb].tableId = tb + "_ssb";
    //\u6a21\u578bID
    tableModel_arr[tb].MtableId = tb;
	//\u83b7\u53d6SID
    var sid = Mobject.getAttribute("sid");
    tableModel_arr[tb].sid = sid;
    //\u662f\u5426\u5206\u9875
    if (Mobject.getAttribute('turnpage') != null)
        tableModel_arr[tb].turnpage = Mobject.getAttribute('turnpage');
    else if (Mobject.getAttribute('editable') != 'true')tableModel_arr[tb].turnpage = "true";
    else tableModel_arr[tb].turnpage = "false";
    if ((Mobject.getAttribute('turnpage') == null || Mobject.getAttribute('turnpage') == 'false') && (Mobject.getAttribute('editable') != null && Mobject.getAttribute('editable') == "true"))
        tableModel_arr[tb].editable = 'true';
    //\u8bbe\u7f6e\u6bcf\u9875\u663e\u793a\u591a\u5c11\u884c\u7684\u4e0b\u62c9\u5217\u8868\u6846
    if (tableModel_arr[tb].turnpage == "true")
    {
        if (Mobject.getAttribute('pagesizelist') == null || Mobject.getAttribute('pagesizelist') == "")
        {
            tableModel_arr[tb].pageSizeList = new Array("10", "20", "30", "50");
        }
        else {
            //var pagessize = Mobject.getAttribute('pagesizelist');
            var pagessize = $(Mobject).attr("pagesizelist");
			//\u589e\u52a0\u4e00\u4e2a\u5224\u65ad\uff0c\u5982\u679cpagesizelist\u5df2\u7ecf\u662f\u4e00\u4e2a\u6570\u7ec4\uff0c\u5219\u4e0d\u9700\u8981\u518d\u5904\u7406\uff0c\u7528\u4e8e\u52a8\u6001\u5730\u6539\u53d8\u5c5e\u6027\u5e76\u91cd\u7ed8\u8868\u683c
			if(pagessize.constructor != Array) 
            	tableModel_arr[tb].pageSizeList = pagessize.split(',');
            tableModel_arr[tb].pg.perPageCount = tableModel_arr[tb].pageSizeList[0];
        }
    }
    tableModel_arr[tb].props = props;
    tableModel_arr[tb].caption = caption;
	//\u5728\u8868\u683c\u4e2d\u70b9\u51fb\u67d0\u884c\u7684\u9f20\u6807\u4e8b\u4ef6
    if (Mobject.getAttribute('leftButtonEvent'))
        tableModel_arr[tb].leftButtonEvent = Mobject.getAttribute('leftButtonEvent');
    if (Mobject.getAttribute('rightButtonEvent'))
        tableModel_arr[tb].rightButtonEvent = Mobject.getAttribute('rightButtonEvent');
    if (Mobject.getAttribute('dbClickEvent'))
        tableModel_arr[tb].dbClickEvent = Mobject.getAttribute('dbClickEvent');
    if (Mobject.getAttribute(DISPLAYHEAD))
        tableModel_arr[tb].isDisplayHead = Mobject.getAttribute(DISPLAYHEAD);
    if (Mobject.getAttribute(VSTATCAPTION) != null) {
        tableModel_arr[tb].vstext = Mobject.getAttribute(VSTATCAPTION);
        tableModel_arr[tb].vstatcaption = true;
    }
    else tableModel_arr[tb].vstatcaption = false;
	//\u68c0\u67e5\u7528\u6237\u662f\u5426\u8bbe\u7f6e\u4e86\u521d\u59cb\u5316\u5b8c\u6210\u540e\u7684\u5904\u7406	
    if (Mobject.getAttribute("onFinished") && Mobject.getAttribute(VSTATCAPTION) != '')
        tableModel_arr[tb].onFinished = Mobject.getAttribute("onFinished");	
};

var init_table_CSS = function(Mobject, table)
{
    if (Mobject["border"] != null)
    {
        table.setAttribute("border", Mobject["border"]);
    }
    else {
        table.setAttribute("border", "0");
    }
    if (Mobject["cellpadding"] != null)
    {
        table.setAttribute("cellPadding", Mobject["cellpadding"]);
    }
    else {
        table.setAttribute("cellPadding", "0");
    }
    if (Mobject["cellspacing"] != null)
    {
        table.setAttribute("cellSpacing", Mobject["cellspacing"]);
    }
    else {
        table.setAttribute("cellSpacing", "1");
    }
    if (Mobject["width"] != null)
    {
        table.setAttribute("width", Mobject["width"]);
    }
    /*
    else {
        table.setAttribute("width", "100%");
    }
    */
    if (Mobject["height"] != null)
    {
        table.setAttribute("height", Mobject["height"]);
    }
};

var init_ssb_table_Tbody = function(tb)
{
    var Mobject = tableModel_arr[tb];
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');
	var thead = document.createElement('thead');
	var trTitle = document.createElement('tr');	
	
   //\u8868\u683c\u5217\u5bf9\u8c61\u96c6\u5408
    var col = tableModel_arr[tb].cells;
   //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
    var props = tableModel_arr[tb].props;
	//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
    var caption = tableModel_arr[tb].caption;
    var capstyles = tableModel_arr[tb].capstyles;
    var captiontips = tableModel_arr[tb].captiontips;
    
	//\u6eda\u52a8\u6761\u9700\u8981\u7684div
	var scDiv = document.createElement('div');
	//\u8bbe\u7f6e\u7528\u6237\u6eda\u52a8\u6761\u9ad8\u5ea6
	scDiv.setAttribute("style", tableModel_arr[tb].styleforscroll);
    if($.browser.msie)            
    	scDiv.style.cssText = tableModel_arr[tb].styleforscroll;	
			
	$(scDiv).attr("id",tb+"_scDiv").append(table);//.addClass('scrollContainer');//.css("height","0");
	
    //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
    if (isIE())//\u5982\u679c\u662fIE
    {
        //Mobject.parentElement.appendChild(table);
		Mobject.parentElement.appendChild(scDiv);
    }
    else {
	    //Mobject.parentNode.appendChild(table);
		Mobject.parentNode.appendChild(scDiv);
    }
	thead.appendChild(trTitle);
	table.appendChild(thead);	
	//tbody.setAttribute("id", tb+"_tbody")

    $(tbody).attr("id",tb+"_tbody").addClass(tb+"_tbody");
    table.appendChild(tbody);
    tableModel_arr[tb].table = table;
    table.setAttribute('id', tableModel_arr[tb].tableId);//\u8bbe\u7f6e\u8868\u5bf9\u8c61\u7684ID\u503c

    init_table_CSS(Mobject, table);
    //\u8bbe\u7f6e\u8868\u683cstyle\u5c5e\u6027
	//table.style.cssText = tableModel_arr[tb].Mstyle;//\u8be5\u4ee3\u7801\u53ea\u80fd\u5728FF\u4e0b\u6709\u6548\uff0c\u5728IE\u4e0b\u4e0d\u80fd\u8bfb\u53d6\u51fa\u6765\uff0c\u6682\u65f6\u6ce8\u91ca\u6389
	
    var tt = tableModel_arr[tb].table;
    //var objRow = tt.insertRow(-1);
    if (Mobject.isDisplayHead == 'false')
    {
        //objRow.style.display = "none";
    }
   //\u8bbe\u7f6eCSS\u6837\u5f0f
    if (tableModel_arr[tb].MclassName != null && tableModel_arr[tb].MclassName != "")    {
        table.className = tableModel_arr[tb].MclassName;
		$(trTitle).addClass("tr_detail");
//        objRow.className = "tr_detail";
    }
    else {
        table.className = "tb_datalist";//\u8bbe\u7f6e\u6837\u5f0f\u65f6\u7528className.
        //objRow.className = "tr_detail";
		$(trTitle).addClass("tr_detail");
    }

    for (var i = 0; i < col.length; i++)
    {
		var th = document.createElement('th');
		var thHtml = "";
		
        //\u521d\u59cb\u5316\u8868\u5934\u5217\u540d
        if (col[i].getAttribute("checkall") == 'true' && col[i].getAttribute("iskey") == 'true') {
            thHtml += '<input class="checkAll" type="checkbox" onclick="checkedAll(this, ' + tb + ')" name="allChecked" id="' + tb + 'allCheck"/>';
        }
        if (col[i].getAttribute("caption") != null) {
            thHtml += col[i].getAttribute("caption");
            caption[i] = col[i].getAttribute("caption");
        }
		
		//\u5217\u6392\u5e8f\u8bbe\u7f6e
        var sortClass = "IEsortASC";
		if (!isIE())  {
            sortClass = "FFsortASC";
        }
        if (col[i].getAttribute("allowsort") == 'true')  {
            thHtml += '<div id="ssbSortImg'+ i +'" class="' + sortClass + '" ></div>';
        }

		//\u5b8c\u6210\u6807\u9898\u53ca\u5176\u6837\u5f0f\u8bbe\u7f6e
        if (i == parseInt(col.length - 1))
        {
        	if (col[i].getAttribute("allowsort") == 'true')  {
            	th.innerHTML = "<SPAN onmouseover='this.style.cursor=\"pointer\";' style='"+capstyles[i]+"' title='"+captiontips[i]+ "' onclick='sortTable(" + tableModel_arr[tb].tableId + ", " + i + ", this.lastChild);'"+"'>" + thHtml + '</SPAN><span></span>';
        	}
        	else
            	th.innerHTML = "<SPAN style='"+capstyles[i]+"' title='"+captiontips[i]+"'>" + thHtml + '</SPAN><span></span>';
        } else {
        	if (col[i].getAttribute("allowsort") == 'true')  {
        		th.innerHTML = "<SPAN onmouseover='this.style.cursor=\"pointer\";'  style='"+capstyles[i]+"' title='"+captiontips[i]+"' onclick='sortTable(" + tableModel_arr[tb].tableId + ", " + i + ", this.lastChild);'"+"'>" + thHtml + '</SPAN>';
        	}
        	else {
            	th.innerHTML = "<SPAN style='"+capstyles[i]+"' title='"+captiontips[i]+"'>" + thHtml + '</SPAN>';
        	}
        	
        	if(tableModel_arr[tb].allowdragbar == "true") {
	        	th.innerHTML += '<span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,' + tableModel_arr[tb].tableId + ');"></span>';
	            th.lastChild.className = "resizeBar";
        	}
        }
		
        th.firstChild.className = "resizeTitle";
		th.setAttribute("width", col[i].getAttribute("width"));
		trTitle.appendChild(th);
		
		//\u4fdd\u5b58\u884c\u5bf9\u8c61\u7684\u5c5e\u6027\u540d\u79f0		
        props[i] = col[i].getAttribute('prop');
    }

};


init_ssb_table = function(tb)
{
    //\u521d\u59cb\u5316\u6a21\u578b
    init_ssb_tableModels(tb);
   
   //////////////\u5982\u679c\u5217\u6570\u7ec4\u957f\u5ea6\u4e3a0\uff0c \u9000\u51fa\u521d\u59cb\u5316
    if (tableModel_arr[tb].clums.length == 0) {
        return false;
    }
   
    var Mobject = tableModel_arr[tb];
   //\u521b\u5efa\u8868\u5934\u6807\u9898
    if (Mobject["title"] != null && Mobject["title"] != "")
    {
        //\u5f97\u5230\u6a21\u578b\u5bf9\u8c61
        var htable = document.createElement('table');//\u521b\u5efa\u8868\u683c
        var thead = document.createElement('thead');
        if (isIE())
        {
        	htable.appendChild(thead); 
        	Mobject.parentElement.appendChild(htable);
        }
        else 
        {
        	htable.appendChild(thead); 
            Mobject.parentNode.appendChild(htable);
        }
        
        //\u8bbe\u7f6e\u6837\u5f0f
        init_table_CSS(Mobject, htable);
        var table_title = "";
        if (table_title != null)
        {
            table_title = Mobject["title"];
        }
        var row = htable.insertRow(-1);//insertRow(-1)firfox\u652f\u6301
        //row["align"] = "left";
        var cell1 = row.insertCell(-1);//insertCell(-1) firfox\u652f\u6301
        cell1.innerHTML = table_title;
        //cell1.width = "95%";
		
		//2008-08-28 \u8bfb\u53d6\u8868\u683c\u6807\u9898title\u6837\u5f0f
		/*
        if(isIE())
	   {
	   		cell1.setAttribute("className","tableTitle");
	   }
	   else
	   {
	   		cell1.setAttribute("class","tableTitle");
	   }
	   */
	   
        var addButton = $res_entry('ui.control.table.addNewButton.name', '\u65b0\u589e');
        addButton = addButton.indexOf("$") == -1 ? addButton : "\u65b0\u589e";
	  //\u5982\u679c\u8868\u6a21\u578b\u5bf9\u8c61\u7684editable\u5c5e\u6027\u4e3a"true"\u5219\u521b\u5efa\u7684\u8868\u683c\u4e3a\u53ef\u7f16\u8f91\u8868\u683c
        if (Mobject["editable"] == "true")
        {
            var cell2 = row.insertCell(-1);
            cell2.innerHTML = "<input class='button' type='button' name='Submit52' onClick=create_new_EditTableRow('" + tableModel_arr[tb].tableId + "','" + tb + "'); return false; value=" + addButton + "  class='button'/>";
            //cell2.width = "5%"
        }
  	 //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
        if (isIE())//\u5982\u679c\u662fIE
        {
            //Mobject.parentElement.appendChild(htable);
            cell1.setAttribute("className","tableTitle");
            if(cell2)
        		cell2.setAttribute("className","newButton");
        }
        else {
            //Mobject.parentNode.appendChild(htable);
           	cell1.setAttribute("class","tableTitle");
        	if(cell2)
        		cell2.setAttribute("class","newButton");
        }
        htable.className = "tb_titlebar";
        
		//htable.style.cssText = tableModel_arr[tb].Mstyle;
    }
   //\u521b\u5efa\u8868\u683c\u4e3b\u4f53
    if (tableModel_arr[tb].table) return;
    init_ssb_table_Tbody(tb);
	//\u5982\u679c\u662f\u4e0d\u53ef\u7f16\u8f91\u8868\u683c
    if (Mobject["turnpage"] == 'true')
    {
        //\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
        var div_table = document.createElement("table");
        var table_body = document.createElement("tbody");
        var thead = document.createElement('thead');
        div_table.appendChild(thead);
        div_table.appendChild(table_body);
        var div_tr = document.createElement("tr");
        table_body.appendChild(div_tr);
        var dir_tr_td = document.createElement('td');
        div_tr.appendChild(dir_tr_td);
        tableModel_arr[tb].toolbarId = tb + "_pages";
        dir_tr_td.setAttribute('id', tableModel_arr[tb].toolbarId);
        dir_tr_td.setAttribute('align', 'right');
		
		 //2008-08-28 \u8bfb\u53d6\u5206\u9875\u680f\u7684\u76f8\u5173\u6837\u5f0f
        if(isIE())
		 {
		 	dir_tr_td.setAttribute("className","tableNavigator");
		 	Mobject.parentElement.appendChild(div_table);
		 }
		 else
		 {
		 	dir_tr_td.setAttribute("class","tableNavigator");
		 	Mobject.parentNode.appendChild(div_table);
		 }
		 //2008-08-28 \u8bfb\u53d6\u5206\u9875\u680f\u7684\u76f8\u5173\u6837\u5f0f end
	
        div_table.id = "div_" + tb;
        div_table.className = "toolbarTable";
        div_table.style.cssText = tableModel_arr[tb].Mstyle;
        tableModel_arr[tb].pg.printHtml();//\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
    }
        //\u5982\u679c\u662f\u53ef\u7f16\u8f91\u8868\u683c
    else if (Mobject["editable"] == 'true')
    {
        //\u521d\u59cb\u5316\u8868\u683c
		//2008-08-27 wxr: \u56e0\u4e3ainitrow\u5e76\u6ca1\u6709\u88ab\u5b9a\u4e49\u4e3a\u8868\u683c\u7684\u4e00\u4e2a\u5168\u5c40\u5c5e\u6027\uff0cMobject["initrow"]\u5728FF\u4e2d\u4e0d\u80fd\u53d6\u5f97\u503c\uff0c\u6240\u4ee5\u91c7\u7528\u901a\u7528\u7684getAttribute\u65b9\u6cd5
		var initrownum = Mobject.getAttribute("initrow");
        if (initrownum != null)
        {
            for (var i = 0; i < initrownum; i ++)
            {
                create_new_EditTableRow(Mobject["tableId"], tb);
            }
        }        
    }

    table_Header_Resize.resizeInit();
};

InsertRow = function(objTable, tb)
{
    var colNum = tableModel_arr[tb].cells.length;
    //var objRow = objTable.insertRow(-1);
	var tr = document.createElement("tr");
	var trhtml = "";
    for (var i = 0; i < colNum; i++)
    {
        //var objCell = objRow.insertCell(-1);
		//tr.appendChild()
		trhtml += "<td></td>";
    }
	$(tr).append(trhtml);
	$("#"+tb+"_tbody").append(tr);
	
	return tr;	
//    return objRow;
};

addOneRow = function(Mtid)
{
    var tableId = tableModel_arr[Mtid].tableId; //\u5185\u5b58\u8868
    return create_new_EditTableRow(tableId, Mtid);
};

deleteOneRow = function(tb, e, tf)
{
    var tableId = tableModel_arr[tb].tableId;
    var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
    var tr = e; //\u83b7\u53d6\u884c\u5bf9\u8c61
    while (tr.tagName.toLowerCase() != 'tr')
    {
        tr = tr.parentNode
    }
  //\u7b2c\u4e00\u884c\u662f\u8868\u5934,\u5e94\u8be5\u53bb\u6389
    var index = tr.rowIndex;

    if (tf == undefined || tf != false)
    {
        //if (confirm('\u786e\u5b9a\u5220\u9664\u5417?'))
        if (confirm($res_entry('ui.control.table.deleterow.confirm','\u786e\u5b9a\u5220\u9664\u5417?')))
        {
            objTable.deleteRow(index);
	  //\u5f53\u8bbe\u7f6e\u603b\u662f\u66f4\u65b0\u603b\u8bb0\u5f55\u6570\u65f6\u624d\u91cd\u7f6e
            if (tableModel_arr[tb].searchSize)
                reSetTableRowNum(tb, index);
            return true;
        }
        else
        {
            return false;
        }
    }
    else {
        objTable.deleteRow(index);
	  //\u5f53\u8bbe\u7f6e\u603b\u662f\u66f4\u65b0\u603b\u8bb0\u5f55\u6570\u65f6\u624d\u91cd\u7f6e
        if (tableModel_arr[tb].searchSize)
            reSetTableRowNum(tb, index);
        return true;
    }
};

function reSetTableRowNum(MtableId, rowIndex)
{
    var tableId = tableModel_arr[MtableId].tableId;
    var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
    var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9

    var rows = objTable.rows;
    if (rows.length == 1)
    {
        var t_inputs = objTable.getElementsByTagName("input");
        for (var i = 0; i < t_inputs.length; i ++)
        {
            if (t_inputs[i].type.toUpperCase() == "CHECKBOX")
                t_inputs[i].checked = false;
        }
    }
    for (var i = rowIndex; i < rows.length; i ++)
    {
        if (rows[i].rowIndex % 2 == 0)
        {
            rows[i].className = "even";
        } else {
            rows[i].className = "";
        }
        for (var j = 0; j < tdnodes.length; j++)
        {
            var prop = tableModel_arr[MtableId].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
            if (tableModel_arr[MtableId].cells[j].td_Children.length == 0)
            {
                if (prop == "$ROWNUM")
                {
                    rows[i].cells[j].innerHTML = rows[i].rowIndex;
                }
            }
        }
    }
    var props = tableModel_arr[MtableId].excel_props;
    for(j=0; j<props.length; j++)
    {
    	if (props[j] == "$ROWNUM")
         {
             break;
         }
    }
    //\u662f\u5426\u6709\u7edf\u8ba1\u884c
    if(tableModel_arr[MtableId].vstatcaption)
    {
    	var pagetable = new PageZTable(MtableId);
    	if(j > 0 && j < pagetable.columnLength)
    		objTable.rows[objTable.rows.length - 1].cells[j].innerHTML = "";
    	else if(j == 0)
    		objTable.rows[objTable.rows.length - 1].cells[j].innerHTML = tableModel_arr[MtableId].vstext;
    }
    	
    if (tableModel_arr[MtableId].turnpage == "true" || tableModel_arr[MtableId].editable == "false")
    {
        tableModel_arr[MtableId].pg.totalrow = tableModel_arr[MtableId].pg.totalrow > 0 ? tableModel_arr[MtableId].pg.totalrow - 1 : 0;
        tableModel_arr[MtableId].pg.getPageCount();
        //\u5982\u679c\u5f53\u524d\u9875\u53ea\u6709\u4e00\u6761\u6570\u636e\u65f6, \u5f53\u5220\u9664\u4e86\u8fd9\u6761\u6570\u636e\u540e,\u5e94\u5f53\u8fd4\u56de\u5230\u4e0a\u4e00\u9875,\u5e76\u67e5\u8be2\u51fa\u4e0a\u4e00\u9875\u7684\u6570\u636e
        update2NewPage(MtableId);
        tableModel_arr[MtableId].pg.printHtml();
    }
}
;
function update2NewPage(tid)
{
	var ztable = getTableById(tid);
	var deadline = 1;
	 //\u662f\u5426\u6709\u7edf\u8ba1\u884c
    if(tableModel_arr[tid].vstatcaption){
    	deadline = 2;
    }

	if(ztable.pageIndex-1 > 0)	{
		if(ztable.rowcount == deadline){
			if(tableModel_arr[tid].sidParamCount == 4 ) {
				getFourParamTurnpageData(ztable.id, ztable.condition, ztable.pageIndex-1, ztable.pagesize);
			} else {
				getTurnPageData(ztable.id, ztable.condition, ztable.pageIndex-1, ztable.pagesize);
			}
		}
	}
}

function updateStatisticRow(tid, index, statisticTitle, customTRString)
{
	var pagetable = new PageZTable(tid);
	var tableData = new Array();
	var length_rows = pagetable.rows.length - 1;
	if(!isIE())
	{						
		FF_innerText();
	}
	//1. \u53d6\u6570\u636e
	for(var i = 1; i < pagetable.columnLength; i++)
	{
		var columnDatas = pagetable.getColumnCells(i);
		for(var j = 0; j < columnDatas.length; j++)
		{
			//\u6ce8\u610fFF\u4e0d\u652f\u6301innerText, \u9700\u52a0\u5165\u517c\u5bb9\u8bbe\u7f6e
			columnDatas[j] = columnDatas[j].innerText;
		}
		
		tableData.push(columnDatas);
	}
	columnDatas = null;
	
	var statisdicProps = pagetable.getStatisticProps();
	var rowdata = new Array();
	//\u81ea\u5b9a\u4e49\u7edf\u8ba1\u680f\u548c\u9ed8\u8ba4\u7edf\u8ba1\u680f\u7684\u533a\u522b
	if(statisticTitle == null || statisticTitle == "")
		rowdata.push(pagetable.getStatisticText);
	else
		rowdata.push(statisticTitle);
		
	for(var k = 1; k < pagetable.columnLength; k++)
	{
		rowdata.push(enumerate_value(tableData[k-1],statisdicProps[k]));
	}
	
	
	//2, \u5220\u9664\u7edf\u8ba1\u884c
	if(pagetable.getStatisticText == pagetable.rows[pagetable.rows.length - 1].cells[0].innerText || statisticTitle == pagetable.rows[pagetable.rows.length - 1].cells[0].innerText)
	{
		pagetable.deleteRow(length_rows);
	}
	
	//3, \u65b0\u5efa\u7edf\u8ba1\u884c
	if(statisticTitle == null || statisticTitle == "")
	{
		//\u91c7\u7528\u9ed8\u8ba4\u7684\u65b9\u5f0f
		var statisticRow = pagetable.insertRow(-1);
	    if (statisticRow.rowIndex % 2 == 0)
	    {
	        statisticRow.className = "even";
	    }
		
		var cells = tableModel_arr[tid].cells;//\u6a21\u578b\u8868\u5217
		for (var j = 0; j < cells.length; j++)
	    {
	        var cell = statisticRow.insertCell(-1);
	        cell.innerHTML = rowdata[j];
	        
	        //\u8bbe\u7f6eTD\u6837\u5f0f
	        assignDataCSS(cells[j], cell);
	    }
	    reSetTableRowNum(tid, index);
    }
    else
    	eval(customTRString);
    	
    pagetable= tableData= statisdicProps= rowdata= cells= null;
}

function PageZTable(tid)
{
	var pageTable = new Object();
	pageTable = tableModel_arr[tid].table;//\u83b7\u53d6\u5185\u5b58\u8868
    var cells = tableModel_arr[tid].cells;
	var cells_length = cells.length;
	
	var statisticEnabled =  tableModel_arr[tid].vstatcaption;
	pageTable.columnLength = cells.length;
	pageTable.data = tableModel_arr[tid].pageData;//\u8868\u683c\u6570\u636e
	pageTable.getStatisticText = tableModel_arr[tid].vstext;
	
	//\u83b7\u53d6\u9875\u9762\u8868\u683c\u7684\u67d0\u4e00\u5217, \u4f46\u6392\u9664\u7edf\u8ba1\u884c\u5355\u5143\u683c
	pageTable.getColumnCells = function(mm)
	{
		var tb_arr = new Array();
		
        var rows = pageTable.rows;
        for (var i = 1; i < rows.length; i++)
        {
            var row = rows[i];
            tb_arr.push(row.cells[mm]);
        }
        
        if(statisticEnabled) tb_arr.pop();
        
        return tb_arr;
	}
	//\u83b7\u53d6\u6307\u5b9a\u5217\u4e2dcheckbox, radio\u88ab\u9009\u4e2d\u7684index\u6570\u7ec4
	pageTable.getCheckedIndexs = function(mm)
	{
		var tb_arr = new Array();
		
        var rows = pageTable.rows;
        for (var i = 1; i < rows.length; i++)
        {
            var row = rows[i];
            var inputArr = row.cells[mm].getElementsByTagName('INPUT');
            if(inputArr[0].checked == true)
            	tb_arr.push(i-1);
        }
        
        return tb_arr;
	}
	
	pageTable.getStatisticProps = function()
	{
		var statisticProps = new Array();
		for(var i = 0; i < cells.length; i++)
		{
			statisticProps.push(cells[i].getAttribute('vstatformula'));
		}
		
		return statisticProps;
	}
	
	pageTable.getCell = function(rowIndex, colIndex)
	{
		var rows = pageTable.rows;
		return rows[rowIndex].cells[colIndex];
	}
	/*
	//\u83b7\u53d6\u6307\u5b9a\u5217\u7684\u6240\u6709TD,\u5305\u542b\u8868\u5934
	pageTable.getColumnCells = function(colIndex)
	{
		var rtnValue = new Array();
		var rows = pageTable.rows;
		
		//\u53d6\u5f97\u6570\u636e\u6570\u636e\u7b2c\u4e00\u5217\u88ab\u9009\u4e2dindex
	    var checkedIndexArr = pageTable.getCheckedIndexs(0);
	    var datalength = pageTable.data.length;
	    //\u5982\u679c\u6ca1\u6709\u9009\u5b9a\u884c,\u5219\u8fd4\u56de\u5168\u90e8\u5217\u6570\u636e
	    if(checkedIndexArr.length == 0)
	    {
	    	for(var i=0; i<rows.length; i++)
			{
				rtnValue.push(rows[i].cells[colIndex]);
			}
	    }
	    else
	    {
	    	for(var i=0; i<checkedIndexArr.length; i++)
			{
				rtnValue.push(rows[checkedIndexArr[i]].cells[colIndex]);
			}
	    }
		
		return 	rtnValue;	
	}
	*/
	//\u83b7\u53d6\u6307\u5b9a\u5217\u7684\u503c
	pageTable.getColumnCellsValues = function (tid, td_index)
	{
	    var rtnValue = new Array();
	    //\u53ef\u80fd\u901a\u8fc7outerHTML\u5b9a\u4f4d\u54ea\u4e00\u5217\u4f7f\u7528\u4e86checkbox,\u6682\u65f6\u8ba4\u4e3a\u7b2c\u4e00\u5217\u4e3acheckbox\uff0c\u9700\u6539\u8fdb
	    
	    //\u53d6\u5f97\u6570\u636e\u6570\u636e\u7b2c\u4e00\u5217\u88ab\u9009\u4e2dindex
	    var checkedIndexArr = pageTable.getCheckedIndexs(0);
	    
	    //\u5982\u679c\u6ca1\u6709\u9009\u5b9a\u884c,\u5219\u8fd4\u56de\u5168\u90e8\u5217\u6570\u636e
	    if(checkedIndexArr.length == 0)
	    {
	    	for(var m=0; m<pageTable.rows.length-1; m++) 	{
	    		checkedIndexArr.push(m);
	    	}
	    }
	    
	    var checkedIndex;
	    var targetCell;
	    for(var i=0; i<checkedIndexArr.length; i++)
	    {
	    	checkedIndex =  checkedIndexArr[i];
	    	//getCell\u65b9\u6cd5\u8fd4\u56de\u6307\u5b9a\u7684td,\u5305\u542b\u8868\u5934
	    	targetCell = pageTable.getCell(checkedIndex+1, td_index);
	    	
	    	//\u53ea\u6709\u6587\u672c\u8282\u70b9,\u4e0d\u5305\u542b\u63a7\u4ef6
	    	if(targetCell.childNodes[0].nodeType == 3)
	    	{
	    		if(isIE()) 
	    			rtnValue.push(targetCell.innerText);
	    		else
	    			rtnValue.push(targetCell.textContent);
	    	}
	    	//\u6709\u63a7\u4ef6\u7684\u60c5\u51b5
	    	else
	    	{
	    		var targetCtr = targetCell.childNodes[0];
	    		switch(targetCtr.tagName)
	    		{
	    			case "SELECT":	    				
	    			case "INPUT":
	    				rtnValue.push($(targetCtr).val());
	    				break;
					case "LABEL":
	    			case "A":
	    				rtnValue.push($(targetCtr).text());
	    				break;
					case "Z:CALENDAR":
					case "Calendar":
						rtnValue.push($(targetCtr).find("input").val());
						break;
	    		}
	    	}
	    }
	     return rtnValue;
	}
	
	return pageTable;
}

function enumerate_value(tableData, vstat)
{
	if(vstat == null || vstat == 'null') return "";
    var rtn = "";
    var arr = tableData;
   
    switch (vstat.toUpperCase()) {
        case "MAX":
            arr.sort();
            rtn = arr[arr.length - 1];
            break;
        case "MIN":
            arr.sort();
            rtn = arr[0];
            break;
        case "AVG":
            var sum = 0;
            for (var i = 0; i < arr.length; i++)
            {
                sum += arr[i];
            }
            rtn = sum / arr.length;
            break;
        case "SUM":
            var sum = 0;
            for (var i = 0; i < arr.length; i++)
            {
                sum += parseFloat(arr[i]);
            }
            rtn = sum;
            break;
        default:
            break;
    }
    return rtn;
}

//\u8bbe\u7f6e\u884c\u7684css
var setRowCss = function(row, rowClass, index)
{
    var num = row.rowIndex;
    if (index != null)
    {
        num = index + 1;
    }
    if (num % 2 == 0)
    {
        row.className = ROW_CLASS_EVEN;
    } else {
        row.className = "";
    }
};

pushData2ssbTable = function(MtableId, tableData)
{
    var tObject = tableModel_arr[MtableId].table
   
   //\u521d\u59cb\u5316sortCol\u548c\u6392\u5e8f\u6309\u94ae\u56fe\u6807
    if (tableModel_arr[MtableId].table.sortCol != null && tableModel_arr[MtableId].table.sortCol.constructor == Number)
    {
		//FF\u4e0d\u652f\u6301children\u5c5e\u6027\uff0c\u52a0\u5165\u652f\u6301
    	if(!isIE()) { FF_children(); }
        var titleRow = tableModel_arr[MtableId].table.children[0].children[0];
        var imgs = titleRow.getElementsByTagName('IMG');
        for (var j = 0; j < imgs.length; j++)
        {
            if (isIE())
                imgs[j].className = "IEsortASC";
            else
                imgs[j].className = "FFsortASC";
        }
    }
    tableModel_arr[MtableId].table.sortCol = "";
    tableModel_arr[MtableId].table.softflag = "";
   
   //\u63d2\u5165\u6570\u636e\u524d\u6e05\u7a7a\u8868\u4e2d\u6570\u636e.
    var rows = tObject.rows.length;//\u5f53\u524d\u8868\u7684\u884c\u6570
    for (var j = rows; j > 1; j --)
    {
        tObject.deleteRow(j - 1);
    }
  //\u5faa\u73af\u63d2\u5165\u7a7a\u767d\u884c.
    for (var i = 0; i < tableData.length; i ++)
    {
        var row = InsertRow(tObject, MtableId);
        setRowCss(row, ROW_CLASS_EVEN);
        bindButtonEvent2Ctrl(row, MtableId);
    }
	
	//\u4e3adiv\u8bbe\u7f6e\u6eda\u52a8\u6837\u5f0f
	if(tableModel_arr[MtableId].rownumforscroll != "noscroll" && tableData.length > tableModel_arr[MtableId].rownumforscroll){
        if(!$("#"+MtableId+"_scDiv").hasClass("scrollContainer")){
            $("#"+MtableId+"_scDiv").addClass("scrollContainer");            
            $("#"+MtableId+"_tbody").addClass(MtableId+"_tbody");
            $("#"+MtableId+"_scDiv").attr("style",tableModel_arr[MtableId].styleforscroll);
        }        
    }		
	else{
        $("#"+MtableId+"_scDiv").removeClass("scrollContainer");
        $("#"+MtableId+"_scDiv").removeAttr("style");        
        $("#"+MtableId+"_tbody").removeClass(MtableId+"_tbody");        
    }	
	
	tObject = row = null;
  	// \u7ed9\u8868\u7684\u884c\u586b\u5145\u6570\u636e
    bindData2TableCtrl(MtableId, tableData);
};

//\u8868\u683c\u4e2d\u9f20\u6807\u70b9\u51fb\u4e8b\u4ef6\u5904\u7406
function bindButtonEvent2Ctrl(rowObj, Mtid)
{
    var model = tableModel_arr[Mtid];
    if (model.leftButtonEvent && model.rightButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 1)
                parseFunction(model.leftButtonEvent, rowObj);
            else if (event.button == 2)
                parseFunction(model.rightButtonEvent, rowObj);
        }
    }
    else if (model.leftButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 1)
                parseFunction(model.leftButtonEvent, rowObj);
        }
    }
    else if (model.rightButtonEvent) {
        rowObj.onmousedown = function() {
            if (event.button == 2)
                parseFunction(model.rightButtonEvent, rowObj);
        }
    }
    if (model.dbClickEvent)
    {
        rowObj.ondblclick = function() {
            parseFunction(model.dbClickEvent, rowObj);
        }
    }

}
;

var parseFunction = function (defaultParm, obj)
{
    var def_parms = defaultParm;
    if (def_parms != "") {
        var str_val = unescape(defaultParm);
        var ssbfunction = str_val;
        if (str_val.indexOf('this') != -1) {
            eval("var obj = obj");
            var ssbfunction = str_val.replace("this", "obj");
        }
        return eval(ssbfunction);
    }
};

var TIME_OFFSET = new Date().getTimezoneOffset() * 60 * 1000;//\u65f6\u533a\u5dee
var TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
getTimefromUTC = function(format, utcTime)
{
    if (utcTime == null || utcTime == '')
    {
        return '';
    }
    var utc_number = utcTime;
    
    if(utcTime.constructor == String) utc_number=parseInt(utc_number);
    
    var nd = new Date(utc_number);
    var yy = "-";
    var mm = "-";
    var _HH_MM_SS = "";
    if (format != null)
    {
        format = format.toLowerCase();
        var y_index = format.indexOf("y");
        var y_endindex = format.lastIndexOf("y");
        yy = format.substring(y_endindex + 1, y_endindex + 2);
        var m_index = format.indexOf("m");
        mm = format.substring(m_index + 2, m_index + 3);
        var hour = new String(nd.getHours()).length > 1 ? nd.getHours() : "0" + nd.getHours();
        var minutes = new String(nd.getMinutes()).length > 1 ? nd.getMinutes() : "0" + nd.getMinutes();
        var seconds = new String(nd.getSeconds()).length > 1 ? nd.getSeconds() : "0" + nd.getSeconds();

        if (format.indexOf("hh") != -1)
        {
            _HH_MM_SS = _HH_MM_SS + " " + hour;
        }
        if (format.lastIndexOf("mm") != m_index)
        {
            if (_HH_MM_SS == "")
                _HH_MM_SS = _HH_MM_SS + " " + minutes;
            else
                _HH_MM_SS = _HH_MM_SS + ":" + minutes;
        }
        if (format.indexOf("ss") != -1)
        {
            if (_HH_MM_SS == "")
                _HH_MM_SS = _HH_MM_SS + " " + seconds;
            else
                _HH_MM_SS = _HH_MM_SS + ":" + seconds;
        }
    }
    var month = nd.getMonth() + 1 + "";
    if (month.length == 1)
    {
        month = "0" + month;
    }
    var date = nd.getDate() + "";
    if (date.length == 1)
    {
        date = "0" + date;
    }
    var time = nd.getFullYear() + yy + month + mm + date + _HH_MM_SS;
    return time;
};


getUTCfromTime = function(format, time)
{
    if (time == null || time == '')
    {
        return null;
    }
    var utc = "";
    var yy = "-";
    var mm = "-";
    var date_format = new Array();
    var date_format1 = "";
    var date_format2 = "";
    var time_arr = new Array();
    var time1 = time;
    var time2 = "";
    if (format != null)
    {
        if (format.length > 10)
        {
            date_format = format.split(" ");
            date_format1 = date_format[0];
            date_format2 = date_format[1];
            time_arr = time.split(" ");
            time1 = time_arr[0];
            time2 = time_arr[1];
        } else {
            date_format1 = format;
            time1 = time;
        }
        var y_index = date_format1.indexOf("y");
        var y_endindex = date_format1.lastIndexOf("y");
        yy = date_format1.substring(y_endindex + 1, y_endindex + 2);

        var m_index = date_format1.indexOf("m");
        var m_endindex = date_format1.lastIndexOf("m");
        mm = date_format1.substring(m_endindex + 1, m_endindex + 2);
    }
    var YM = time.substring(time.indexOf(yy), time.indexOf(yy) + 1);
    var MD = time.substring(time.indexOf(mm), time.indexOf(mm) + 1);
    time1 = time1.replace(YM, ',');
    time1 = time1.replace(MD, ',');
    var tt = time1.split(",");
    if (time2.length < 1) {
        var date = new Date(tt[0], parseInt(tt[1] - 1), tt[2]);
        utc = date.getTime();
    } else {
        var HH = time2.split(":");
        var date = new Date(tt[0], parseInt(tt[1] - 1), tt[2], HH[0], HH[1], HH[2]);
        utc = date.getTime();
    }
    return parseInt(utc);
};

var SSB_RIA_CALENDAR = "../images/calendar.gif";
Calendar_Ctr_Processor = function(tagObject, value, rowindex, naturalline)
{   
    var tagetId = tagObject.getAttribute("target");
	
	if(naturalline != null)
	{
		tagObject.id = tagObject.id + naturalline + rowindex;
		
		if(!tagetId) tagetId="null";
		tagObject.setAttribute("target", tagetId + naturalline + rowindex);
	}
	else
	{
		tagObject.id = tagObject.id + rowindex;
		//tagObject.target = tagObject.target + rowindex;
		tagObject.setAttribute("target", tagetId + rowindex);
	}
	
	var calendarObj = init_SSB_calendar(tagObject, value);
	
	return calendarObj;
};

Lov_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    var targetId = retagObject.getAttribute('targetCompId');
    var jsevent = retagObject.getAttribute('jsEventId');
    if (jsevent && (jsevent != "" || jsevent != null)) {
        retagObject.setAttribute('jsEventId', retagObject.getAttribute('jsEventId') + rowindex);
    }
    else {
        retagObject.setAttribute('jsEventId', "");
    }
    var nid_arr = targetId.split(",");
    for (var i = 0; i < nid_arr.length; i++)
    {
        nid_arr[i] = nid_arr[i] + rowindex;
    }
    var ntargetId = nid_arr.join(",");
    retagObject.setAttribute('targetCompId', ntargetId);
    return retagObject;
};

autoComplete_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    var nctrId = retagObject.getAttribute('ctrlId') + rowindex;
    var nId = retagObject.getAttribute('id') + rowindex;
    retagObject.setAttribute('ctrlId', nctrId);
    retagObject.setAttribute('id', nId);
    return retagObject;
};

radioGroup_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    retagObject.innerHTML = '';
    return retagObject;
};

upload_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
{
    return retagObject;
};
 
//\u8ba9FF\u4e5f\u80fd\u652f\u6301outerHTML
function FFOuterHTML()
{
    if (window.HTMLElement) {
        HTMLElement.prototype.__defineSetter__("outerHTML", function(sHTML) {
            var r = this.ownerDocument.createRange();
            r.setStartBefore(this);
            var df = r.createContextualFragment(sHTML);
            this.parentNode.replaceChild(df, this);
            return sHTML;
        });

        HTMLElement.prototype.__defineGetter__("outerHTML", function() {
            var attr;
            var attrs = this.attributes;
            var str = "<" + this.tagName.toLowerCase();
            for (var i = 0; i < attrs.length; i++) {
                attr = attrs[i];
                if (attr.specified)
                    str += " " + attr.name + '="' + attr.value + '"';
            }
            if (!this.canHaveChildren)
                return str + ">";
            return str + ">" + this.innerHTML + "</" + this.tagName.toLowerCase() + ">";
        });

        HTMLElement.prototype.__defineGetter__("canHaveChildren", function() {
            switch (this.tagName.toLowerCase()) {
                case "area":
                case "base":
                case "basefont":
                case "col":
                case "frame":
                case "hr":
                case "img":
                case "br":
                case "input":
                case "isindex":
                case "link":
                case "meta":
                case "param":
                    return false;
            }
            return true;
        });
    }
}
//\u8ba9FF\u652f\u6301IE\u7684innerText\u5c5e\u6027
function FF_innerText()
{	
	HTMLElement.prototype.__defineGetter__("innerText",
    function(){
        var anyString = "";
        var childS = this.childNodes;
        for(var i=0; i<childS.length; i++) {
            if(childS[i].nodeType==1)
                anyString += childS[i].innerText;
            else if(childS[i].nodeType==3)
                anyString += childS[i].nodeValue;
        }
        return anyString;
    });
    HTMLElement.prototype.__defineSetter__("innerText",
    function(sText){
        this.textContent=sText;
    });
}
//\u8ba9FF\u652f\u6301IE\u7684children\u5c5e\u6027
function FF_children()
{
	HTMLElement.prototype.__defineGetter__("children", 
	function () 
	{ 
	 var returnValue = new Object(); 
	 var number = 0; 
	 for (var i=0; i<this.childNodes.length; i++) { 
		 if (this.childNodes[i].nodeType == 1) { 
			 returnValue[number] = this.childNodes[i]; 
			 number++; 
		 } 
	 } 
	 returnValue.length = number; 
	 return returnValue; 
	});
}

//\u8868\u8fbe\u5f0f\u5904\u7406\u5668
function Express_Factory(value, data, isAttribute)
{
	//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	this.paramConvert = function(value)
	{
		var exps = value.match(/\${.+?}/g); 
		for(var i=0; i<exps.length; i++)
		{				
			var loc = value.indexOf(exps[i]) ;
			if( value.charAt(loc - 1) != "'")
			{
				if(value.charAt(loc - 1) != "\"")
				{
					value = value.replace(exps[i], "'"+exps[i]+"'");
				}					
			}
		}
		return value;	
	}
	
	//\u53d6\u5f97\u8868\u8fbe\u5f0f\u7684\u503c
	this.getExpValue = function(prop)
	{
		var levels = prop.split(".");
    	var datastring = "data";
    	for(var j=0; j<levels.length; j++)
    	{
    		datastring += "."+levels[j];
    	}
    	
   		return eval(datastring);
	}
	
	this.processor = function()
	{
		if(value != null && value != '')
 		{
 			try{
				if(isAttribute)
				{
					//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
			    	if(value.indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
			    	{
			    		if(value.indexOf("${") != -1)
			    			value = this.paramConvert(value);
			    	}
			    	else if(value.indexOf("${") == -1)
			    	{
			    		//\u65e2\u6ca1\u6709\u62ec\u53f7\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f\uff0c \u76f4\u63a5\u9000\u51fa
		    			return value;
			    	}
				}
	 			
		 		do
		 		{
				    var start = value.indexOf("${");
				    if(start != -1)
				    {
				    	var old = value.substring(start, value.indexOf("}")+1);	//\u6bd4\u5982\uff1a${xxxx.xxx}
				    	var prop = old.substring(2,old.length-1);				//\u6bd4\u5982\uff1axxxx.xxx
				    	prop = this.getExpValue(prop);
				    	value = value.replace(old, prop);
					}
				}
				while(value.indexOf("${") != -1);
				
				if(isAttribute)
				{
					if(value.indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
			    	{
		    			value = eval(value);
			    	}
				}
			}
			catch(e){}
		}
		
 		return  value;
	}
	
	return this;
}

getOuterHtml = function(retagObject, data, usermethod, pp)
{
	//\u5982\u679c\u662f\u6ce8\u91ca\uff0c\u76f4\u63a5\u8fd4\u56de
	if(retagObject.nodeType == 8) { return;}
	
     //\u5982\u679c\u662f\u6587\u672c\u8282\u70b9\uff0c\u68c0\u67e5\u8282\u70b9\u503c\u662f\u5426\u662f\u8868\u8fbe\u5f0f\uff0c \u5982\u679c\u662f\u9700\u4f5c\u5904\u7406
 	if(retagObject.nodeName == "#text") 
 	{ 
		if(Trim(retagObject.nodeValue) != "")
		{
 			retagObject.nodeValue = new Express_Factory(retagObject.nodeValue, data).processor();
 		}
 		return;
 	}
 	
 	//\u4ece\u6700\u5e95\u5c42\u5f00\u59cb\u5904\u7406
    for(var j = 0; j < retagObject.childNodes.length; j++) 
    {
    	getOuterHtml(retagObject.childNodes[j], data, usermethod, pp);
    }
    
	//\u8ba9FF\u4e5f\u80fd\u652f\u6301outerHTML
   	if(!isIE()) 	FFOuterHTML();
	
	//\u901a\u8fc7outerHTML\u83b7\u53d6\u5c5e\u6027\u5217\u8868\uff0c\u5982\u679c\u5c5e\u6027\u4e2d\u6ca1\u6709\u8868\u8fbe\u5f0f\uff0c\u5219\u4e0d\u4f5c\u5904\u7406\u76f4\u63a5\u8fd4\u56de
	var tmpHTML = retagObject.outerHTML; 
	var attrs = new Array();
	splitArr = tmpHTML.split(/\s+/);
	for (var i = 0; i < splitArr.length; i++) {
	    var quoteflag = splitArr[i].indexOf("=");
	    if (quoteflag != -1) {
	        var name = splitArr[i].substring(0, quoteflag);
	        var value = splitArr[i].substring(quoteflag + 1);
	        var valmatch = value.match(/\${\w+}/);
	        
	        if (value.match(/\${\w+}/)) 
				attrs.push($.trim(name));	            
	    }
	}

	//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	var paramConvert = function(value)
	{
		var exps = value.match(/\${.+?}/g); 
		for(var i=0; i<exps.length; i++)
		{				
			var loc = value.indexOf(exps[i]) ;
			if( value.charAt(loc - 1) != "'")
			{
				if(value.charAt(loc - 1) != "\"")
				{
					value = value.replace(exps[i], "'"+exps[i]+"'");
				}					
			}
		}
		return value;	
	}
	
	//\u68c0\u67e5\u672c\u5bf9\u8c61\u662f\u5426\u6709\u7528\u6237\u8f6c\u6362\uff0c\u6709\u7684\u8bdd\u4ee5\u6b64\u4e3a\u51c6
	var tempmethod = retagObject.getAttribute('calculate');
	var tempprop = retagObject.getAttribute('prop');
	var isParams = false;
	if(tempmethod != null && tempmethod != "")
	{
		usermethod = tempmethod;
		//\u68c0\u67e5\u81ea\u5b9a\u4e49\u65b9\u6cd5\u662f\u5426\u6709\u53c2\u6570
		if(usermethod.indexOf("(") != -1)
		{
			 if(usermethod.indexOf(")") != -1)				         
			 {				
				contxt = usermethod;				        
				isParams = true;
			 }
			 else
				usermethod = null;
		}
	}
	
	if(tempprop != null && tempprop != "")
		pp = tempprop;
	
	
	//\u53d6\u5f97\u8868\u8fbe\u5f0f\u7684\u503c
	var getExpValue = function(prop)
	{
		var levels = prop.split(".");
		var expValue = "";
		if(data != null && data != "null")
		{
			try{
				var datastring = "data";
		    	for(var j=0; j<levels.length; j++)
		    	{
		    		datastring += "."+levels[j];
		    	}
		    	if(isParams)
		    		expValue = eval(datastring);
		    	else
		    		expValue = caculator_processor(usermethod, prop, eval(datastring));
			}
			catch(e){
				expValue = "Wrong Prop!";
			}

    		return expValue;
		}
	}
	
	//\u6839\u636e\u5c5e\u6027\u5217\u8868\uff0c\u66ff\u6362\u6389\u8868\u8fbe\u5f0f\uff0c\u8fd9\u4e00\u6b65\u5bf9\u6240\u6709\u5c5e\u6027\u4ee5\u53ca\u5185\u7f6e\u6587\u672c\u8d4b\u503c\uff0c\u5e76\u6ca1\u6709\u5904\u7406\u4e8b\u4ef6
 	for(var i = 0; i < attrs.length; i ++)
 	{ 		
 		var attrname = attrs[i];
		//\u6ce8\u610fINPUT\u5e76\u4e0d\u76f4\u63a5\u652f\u6301Width\u5c5e\u6027\uff0c\u7528\u6237\u8bbe\u7f6eWidth\u5fc5\u987b\u7528Style\uff1b\u76f4\u63a5\u8df3\u8fc7
		if(attrname.toUpperCase() == "WIDTH") continue;
		if(attrname.toUpperCase() == "STYLE") continue;
		if(attrname.toUpperCase() == "TYPE") continue;
		if(attrname.toUpperCase() == "PROP") continue;
		
 		//\u53d6\u51fa\u6bcf\u4e2a\u5c5e\u6027\u7684\u503c\uff0c\u5305\u62econclick\uff01
 		var value = decodeURIComponent(retagObject.getAttribute(attrname));
 		
		//modify by wanghao		
 		if(value.indexOf("about:") == 0)
 		{
 			if(value.indexOf("about:blank") == 0)
 				value = value.substr(11);
 			else
 				value = value.substr(6);
 		}
 					
 		//\u652f\u6301label\u91cc\u9762\u7684\u5c5e\u6027\uff0c\u6bd4\u5982title\u7b49
 		//if(retagObject.nodeName.toUpperCase() == "LABEL") 	  value = retagObject.innerHTML;
 		//\u5faa\u73af\u66ff\u6362\u8868\u8fbe\u5f0f\u7684\u503c
 		if(value != null && value != ''&& typeof value == 'string')
 		{
 			//\u68c0\u6d4b\u8868\u8fbe\u5f0f\u662f\u5426\u662f\u53c2\u6570\uff0c\u4ee5\u4e24\u8fb9\u6709\u5c0f\u62ec\u53f7\u4e3a\u7b26\u5408\u6761\u4ef6
	    	if(Trim(value).indexOf("(") != 0 && value.indexOf("(") != -1 && value.indexOf(")") != -1 && value.indexOf("(") <value.indexOf(")"))
	    	{
	    		if(value.indexOf("${") != -1)
	    			value = paramConvert(value);
	    	}
	 		do
	 		{
			    var start = value.indexOf("${");
			    if(start != -1)
			    {
			    	var old = value.substring(start, value.indexOf("}")+1);	//\u6bd4\u5982\uff1a${xxxx.xxx}
			    	var prop = old.substring(2,old.length-1);				//\u6bd4\u5982\uff1axxxx.xxx
			    	
			    	value = value.replace(old,getExpValue(prop));
				}
			}
			while(value.indexOf("${") != -1);
		}
 		var onstr = attrname.substring(0,2);
 		//\u5bf9\u4e8b\u4ef6\u5c5e\u6027\u7684\u8bbe\u5b9a, \u5168\u9762\u652f\u630120\u4e2ajs\u4e8b\u4ef6\uff0c\u5bf9IE\u548cFF\u7684\u5904\u7406\u65b9\u5f0f\u4e0d\u540c
 		if(onstr.toUpperCase()=="ON")
 		{
	 		if(isIE())
	 		{
	 			if(value.indexOf("{") != -1)
	 				value = value.substring(value.indexOf("{")+2,value.length-2);
	 			retagObject.setAttribute(attrname,value);
	 		}
			else
			{
 				retagObject.setAttribute(attrname,value);
 			}
 		}
 		else
 		{
 		retagObject.setAttribute(attrname,value);
 		}
 	}
 
	if(isIE()) {
		var eventNameArray = attrs.toString().match(/on\w+/g); 
		if(eventNameArray != null)
		{			
			for (var i = 0; i < eventNameArray.length; i++) 
			{
				var evstr = retagObject.getAttribute(eventNameArray[i]);
				var evname = eventNameArray[i].substring(2);
				var rpArr = evstr.match(/\W+this\W/);
				
				if(rpArr) {
					for(var i=0; i<rpArr.length; i++) {
						var begin = rpArr[i].substring(0, rpArr[i].indexOf("this"));
						var end = rpArr[i].substring(rpArr[i].indexOf("this")+4);					
						evstr = evstr.replace(rpArr[i], begin + "retagObject" + end);
					}
				}			
				
				var userCusMethod = function(){				
					eval(evstr);
				}
				
				EventManager.Add(retagObject, evname, userCusMethod);		
			}
		}
	}
 	
 	if(retagObject.tagName.toUpperCase() != "SELECT")
 	{ 	
        var text = "";
		if(isIE())
			text = retagObject.innerText;
		else
			text = retagObject.textContent;
		
		if(text != '')
		{
			var start = text.indexOf("${");
		    //\u5982\u679c\u6709\u53c2\u6570
		    if(start != -1)
		    {
		    	var end = text.indexOf("}")+1;
		    	var old = text.substring(start, end);
		    	var prop = text.substring(start+2,end-1);
		    	
			    text =text.replace(old,getExpValue(prop));
				//modify by wanghao
				innerText_function(retagObject, text);
			}
			
		}
 	}
 	//\u63a7\u4ef6\u4e2d\u6709calculate\u7528\u6237\u81ea\u5b9a\u4e49\u65b9\u6cd5\u7684\u7edf\u4e00\u5904\u7406
 	if(usermethod != null && usermethod != "")
 	{	
 		var isCal = false;
 		//1, \u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5, \u5e76\u4e14\u6709\u53c2\u6570
 		if(isParams)
	 	{
	 		usermethod = retagObject.getAttribute('calculate');
	 		value = caculator_processor(usermethod);
	 		isCal = true;
	 	}
	 	//2, \u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5,\u4f46\u6ca1\u6709\u53c2\u6570
	 	else if(tempmethod != null && tempmethod != "")
	 	{
			//\u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86prop\u5c5e\u6027
			if(pp != null && pp != "")
			{
				value = getExpValue(pp);
				isCal = true;
			}
			//\u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49prop\u5c5e\u6027, \u68c0\u67e5\u5728\u5217\u4e2d\u662f\u5426\u5b9a\u4e49\u4e86prop
			else if(prop != null && prop != "")
			{
				value = getExpValue(prop);
				isCal = true;
			}
	 	}
	 	//3, \u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49calculate\u81ea\u5b9a\u4e49\u65b9\u6cd5
	 	else
	 	{
			//\u7528\u6237\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49\u4e86prop\u5c5e\u6027
			if(pp != null && pp != "")
			{
				value = getExpValue(pp);
				isCal = true;
			}
			//\u7528\u6237\u672a\u5728\u63a7\u4ef6\u4e2d\u5b9a\u4e49prop\u5c5e\u6027, \u68c0\u67e5\u5728\u5217\u4e2d\u662f\u5426\u5b9a\u4e49\u4e86prop
			else if(prop != null && prop != "")
			{
				value = getExpValue(prop);
				isCal = true;
			}
	 	}
	 	
	 	if(isCal)
	 	{
	 		switch(retagObject.tagName.toUpperCase())
	 		{
	 			case "INPUT" : 
	            	if(isIE())
	 					retagObject.setAttribute('value',value);
		 			else
		 			{	//FF\u4e0d\u652f\u6301\u5728outerHTML\u8bbe\u7f6e\u7684\u5c5e\u6027\u503c
		 				retagObject.setAttribute('value',value);
		 				retagObject['value'] = value;
		 			}
	            	break;
	            	
				case "LABEL" :
	        	case "A" : 
	             	retagObject.innerHTML = value;
	            	break;
	 		}
	 	}
 	}
	
	data = attrs = null;
 	
  	  	return retagObject;
};

select_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, data)
{
    var option_values = new Array();
    var option_texts = new Array();
    var selectValue = '';
    var ctrlex = reTagObject.getAttribute("ctrlex");		   
   //\u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027
    if (ctrlex != null)
    {
        reTagObject.innerHTML = "";
        var ctrlarr = reTagObject.getAttribute("ctrlex").split(';');//\u83b7\u53d6\u6269\u5c55\u5c5e\u6027
        for (var i = 0; i < ctrlarr.length; i ++)
        {
            var arr = ctrlarr[i].split(':');
            if (arr.length == null)
            {
                break;
            }
	   	  //\u7ed9\u9009\u9879\u7684text\u548cvalue\u8d4b\u503c
            switch (arr[0].toUpperCase())
                    {
                case 'TEXT' :
                    var text_arr = new Array();
                    text_arr = arr[1].split('.');
                    var text_arr_options = evalParse(data, text_arr[0]);
                    text_arr_options = evalParse(text_arr_options, text_arr[1]);
                    for (var m = 0; m < text_arr_options.length; m++)
                    {
                        option_texts[m] = evalParse(text_arr_options[m], text_arr[2]);
                    }

                    break;

                case 'VALUE' :
                    var value_arr = new Array();
                    value_arr = arr[1].split('.');
                    var value_arr_options = evalParse(data, value_arr[0]);
                    value_arr_options = evalParse(value_arr_options, value_arr[1]);
                    for (var n = 0; n < value_arr_options.length; n++)
                    {
                        option_values[n] = evalParse(value_arr_options[n], value_arr[2]);
                    }
                    break;

                case 'DEFAULT' :

                    var select_arr = arr[1].split('.');
                    selectValue = evalParse(data, select_arr[0]);
                    selectValue = evalParse(selectValue, select_arr[1]);
                    break;

                default :

                    break;
            }//end switch
        }//end for

        //\u521b\u5efa\u4e0b\u62c9\u9009\u9879
        for (var i = 0; i < option_values.length; i++)
        {
            var option = document.createElement("option");
            option.value = option_values[i];

            option.innerHTML = option_texts[i];
		  
	   	  //\u8bbe\u7f6e\u9009\u4e2d\u9879
            if (option_values[i] == selectValue)
            {
                option.selected = "selected";
            }
            reTagObject.appendChild(option);
        }

    }//end if \u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027
    else {//\u5982\u679c\u6ca1\u6709\u6269\u5c55\u5c5e\u6027
        var sel_children = reTagObject.getElementsByTagName("option");
     //\u5c06\u63a7\u4ef6\u7684prop\u5c5e\u6027\u503c\u548c\u4e0b\u62c9\u9009\u9879\u7684value\u503c\u5bf9\u6bd4\uff0c\u5982\u679c\u76f8\u7b49\u5219\u9009\u4e2d
     	/*
        for (var j = 0; j < sel_children.length; j ++)
        {
        	//\u6655\uff0c\u5b83\u90fd\u4e0d\u5224\u65ad\u662f\u5426\u6709prop\u503c 2008-09-26
        	if(sel_children[j].getAttribute('prop') && sel_children[j].getAttribute('prop') != "")
	       	{
	       		if (new String(value) == sel_children[j].value)//\u5982\u679cprop\u5c5e\u6027\u503c\u548coption.value\u76f8\u7b49
	            {
	                sel_children[j].selected = "selected"; break;
	            }
	       	}
	       	else if (new String(value) == sel_children[j].value)//\u5f53option\u6ca1\u6709prop\u5c5e\u6027\u65f6, \u4f20\u9012\u8fc7\u6765\u7684value\u8fdb\u884c\u6bd4\u8f83
            {
                sel_children[j].selected = "selected";break;
            }
        }
        */
        for (var j = 0; j < sel_children.length; j ++)
        {
            if (new String(value) == sel_children[j].value)//\u5982\u679cprop\u5c5e\u6027\u503c\u548coption.value\u76f8\u7b49
            {
                sel_children[j].selected = "selected";
            }
        }
    }
    return reTagObject;
};

var A_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, data)
{
    var href = String(reTagObject.getAttribute('href'));
    //\u5bf9\u6709about\u7684\u60c5\u51b5\u8fdb\u884c\u5904\u7406
    if(href.indexOf("about:") == 0)
	{
		if(href.indexOf("about:blank") == 0)
			href = href.substr(11);
		else
			href = href.substr(6);
	}
	
    //1, \u5982\u679c\u7528\u6237\u6ca1\u6709\u8bbe\u7f6e\u5177\u4f53href\u8def\u5f84\uff0c\u5219\u9ed8\u8ba4\u4e3a\u4e0d\u8df3\u8f6c
    if(href == null || href == "" || href == "#")
    {
	    href = "#";
    }
    //2, \u5982\u679c\u4ee5'/'\u5f00\u5934\uff0c\u5219\u8ba4\u4e3a\u662f\u91c7\u7528\u4ee5\u524d\u7684\u65b9\u5f0f\uff0c\u5176\u8def\u5f84\u76f8\u5bf9\u4e8ewebcontent\u76ee\u5f55\uff0c\u4ece\u800c\u7ec4\u88c5\u8def\u5f84
    else if(href.charAt(0) == "/")
    {
    	var projectname = document.location.pathname.split('/')[1];
	    href = "http://" + document.location.host + "/" + projectname + href;
    }
    //3, \u9ed8\u8ba4\u7528\u6237\u91c7\u7528\u76f8\u5bf9\u4e8e\u5f53\u524d\u76ee\u5f55\u7684\u65b9\u5f0f\uff0c \u4e5f\u5c31\u662f../../
    reTagObject.setAttribute('href', href);

    return     reTagObject;
};

input_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, useExp)
{
    if ((reTagObject.getAttribute('type') != "button") && (reTagObject.getAttribute('type') != "reset"))
    {
    	//\u5fc5\u987b\u8981value\u6709\u503c\u624d\u8fdb\u884c\u66ff\u6362\uff0c\u5426\u5219\u4f7f\u7528\u539f\u6765\u7684\u503c
    	//\u6ce8\u610f\uff1a\u5982\u679c\u5bf9value\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f\uff0c\u5b83\u7684\u503c\u4f1a\u518d\u6b21\u88ab\u6539\u53d8; \u5982\u679c\u4f7f\u7528\u4e86calculate, \u5219\u503c\u4e0d\u5e94\u8be5\u88ab\u6539\u53d8
    	//val usermethod = reTagObject.getAttribute('calculate');
        if (value == null) value = "";
        //\u5982\u679cvalue\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u5219\u503c\u5e94\u5f53\u4e0d\u53d8
       	if(useExp == -1 && reTagObject.getAttribute('calculate') == null)   
       		reTagObject.value = value;
		
		//\u4e0b\u9762\u7684\u4ee3\u7801\u4f1a\u7ed9\u6240\u6709\u7684checkbox\u90fd\u9644\u52a0\u4e0a\u4e00\u4e2a\u989d\u5916\u7684click\u4e8b\u4ef6\uff0c\u663e\u7136\u4e0d\u5bf9\uff0c\u5e94\u5f53\u5728checkbox\u52a0\u5165DOM\u540e\uff0c\u6bd4\u8f83\u5b83\u4e0eallcheck\u6240\u5728\u5217\u6765\u5224\u65ad	
        //addEventForCheckBox(reTagObject, MtableId);
    }
    return reTagObject;
};

function changeColumnTitle(tid, cid, newtitle)
{
	//\u68c0\u67e5\u8be5\u5217\u662f\u5426\u5b58\u5728
	var canBeShown = false;
	for(var t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	if(!canBeShown)
	{
		alert($res_entry('ui.control.table.column.noColumn','\u8f93\u5165\u4fe1\u606f\u6709\u8bef\u6216\u8be5\u5217\u4e0d\u5b58\u5728\uff01\uff01'));
		return;
	}
	//\u53d6\u5f97index
	for(t=0; t<tableModel_arr[tid].cells.length; t++)
	{
		if (cid == tableModel_arr[tid].cells[t].id)
		{
			break;			
		}
	}
	
    var col = tableModel_arr[tid].cells;    
	var displayTB = tableModel_arr[tid].table;	
	var captions = tableModel_arr[tid].caption;
	var exceltitle = tableModel_arr[tid].excel_title;
	
	//FF\u517c\u5bb9\u5904\u7406
	if(!isIE())
	{						
		FF_innerText();
		FF_children();
	}
	
	var inner = displayTB.rows[0].cells[t].children[0].innerHTML;
	var tmpobj = document.createElement('td');
	
	tmpobj.innerHTML = captions[t];
	
	inner  = eval("inner.replace(/"+tmpobj.innerText+"/g, '"+newtitle+"')");
	//\u9700\u8bbe\u7f6e\u540e\u53f0\u5bf9\u5e94\u5217\u7684caption\u5c5e\u6027
	col[t].setAttribute("caption", inner);
	//\u9700\u8bbe\u7f6e\u6807\u7b7e\u961f\u5217\u4e2d\u5bf9\u5e94\u7684caption\u5c5e\u6027
	captions[t] = inner;	
	//\u4fee\u6539Excel\u5bfc\u51fa\u9875\u9762\u663e\u793a\u7684\u8868\u5934
	exceltitle[t] = newtitle;
	//\u6539\u53d8\u663e\u793a\u7684\u7ed3\u679c
	displayTB.rows[0].cells[t].children[0].innerHTML  = inner;	
	
	col = displayTB = captions = exceltitle = inner = tmpobj = null;
}

function createCaption(tid, cid, prop_caption_index)
{
    var columnIndex = prop_caption_index['index'];
    var col = tableModel_arr[tid].cells;
    var clums = tableModel_arr[tid].clums;
	var displayTB = tableModel_arr[tid].table;	
	//\u8868\u5934\u6837\u5f0f\u548c\u63d0\u793a
	var captiontips = tableModel_arr[tid].captiontips;
	var capstyles = tableModel_arr[tid].capstyles;
	
    var html = '';
    if (clums[columnIndex].getAttribute("checkall") == 'true' && clums[columnIndex].getAttribute("iskey") == 'true')
    {
        html += '<input style="vertical-align:middle;height: 18px;" type="checkbox" onclick="checkedAll(this, ' + tid + ')" name="allChecked" id="' + tid + 'allCheck"/>';
    }
    if (clums[columnIndex].getAttribute("caption") != null)
    {
        var title = clums[columnIndex].getAttribute("caption");
        html += title;
    }
	
	var th;
	if(col.length == prop_caption_index['preDisplayIndex']+1)
		th = displayTB.rows[0].appendChild(document.createElement("th"));
    else
		th = displayTB.rows[0].insertBefore(document.createElement("th"), displayTB.rows[0].cells[prop_caption_index['preDisplayIndex']]);
   
    var cellSort = clums[columnIndex].getAttribute("allowsort");
    var sortClass = "IEsortASC";
	
    if (!isIE())    {
        sortClass = "FFsortASC";
    }
    if (cellSort == 'true')    {
        html += '<div id="ssbSortImg'+ i +'" class="' + sortClass + '" ></div>';
    }

    th.setAttribute("width", clums[columnIndex].getAttribute("width"));

    if (columnIndex == parseInt(col.length - 1))//
    {
        	if (cellSort == 'true')  {
            	th.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+ "' onclick='sortTable(" + tableModel_arr[tid].tableId + ", " + columnIndex + ", this.lastChild);'"+"'>" + html + '</SPAN><span></span>';
        	}
        	else
            	th.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+"'>" + html + '</SPAN><span></span>';
        } else {
        
        	if (cellSort == 'true')  {
        		th.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+"' onclick='sortTable(" + tableModel_arr[tid].tableId + ", " + columnIndex + ", this.lastChild);'"+"'>" + html + '</SPAN><span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,' + tableModel_arr[tid].tableId + ');"></span>';
            	
        	}
        	else
            	th.innerHTML = "<SPAN style='"+capstyles[columnIndex]+"' title='"+captiontips[columnIndex]+"'>" + html + '</SPAN><span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,' + tableModel_arr[tid].tableId + ');"></span>';

        th.lastChild.className = "resizeBar";
    }

    th.firstChild.className = "resizeTitle";
    
    columnIndex = col = clums = displayTB = objCell = cellSort = sortHtml = sortClass = null;
}

function createDataTds(tid, cid, prop_caption_index)
{
	var index = prop_caption_index['preDisplayIndex'];	
    var clums = tableModel_arr[tid].clums;
    var col = tableModel_arr[tid].cells;
    var props = tableModel_arr[tid].props;
	var caption = tableModel_arr[tid].caption;
	
	var data = getTableById(tid).data;
	var displayTB = getTableById(tid).table;	
	var length = displayTB.rows.length;
	
	var tObject = tableModel_arr[tid].table;
    var tModle = tableModel_arr[tid];
    var autoCtrId_arr = new Array();
    var row = tObject.rows;
    
	for(var i=1; i<length; i++)
	{
		var naturalline = getKey()-length+1;
    	var objCell = displayTB.rows[i].insertCell(prop_caption_index['preDisplayIndex']);
    	var usermethod = col[index].getAttribute('calculate');

    	//\u5728\u8fd9\u91cc\u5e94\u5f53\u5148\u5224\u65ad\u662f\u5426\u6709\u5b50\u8282\u70b9\uff0c\u5982\u679c\u6709\u7684\u8bdd\u4e0e\u8868\u683c\u5904\u7406\u5668\u76f8\u5173\uff0c\u5426\u5219\u4e0eprop\u76f8\u5173
    	//\u9996\u5148\u5904\u7406\u65e0\u8282\u70b9\u573a\u666f
    	if (col[index].td_Children == null || col[index].td_Children.length == 0)
    	{	
    		try
    		{  			
    			var datatype = col[index].getAttribute("datatype");
    			var statusDesc = col[index].getAttribute("ctrlex");
    			var innertxt = col[index].innerHTML;
                var prop = props[index];
                
				var text;//, exps, levels;
				var isParams = false;
				/*
				\u7b2c\u4e00\u7c7b\uff1a\u6709prop\uff0c \u4f46\u662f\u540c\u65f6\u6709calcualte\u5c5e\u6027\uff0c \u6b64\u65f6\u4ee5calculate\u5c5e\u6027\u4e3a\u4e3b\uff0c \u5ffd\u7565prop\u5c5e\u6027
				*/
				if(prop != null && prop !="")
				{
					if(usermethod !=null && usermethod !="")
					{
						text = calculateUserMethod(data, i, usermethod, innertxt, prop, eval("data[" + parseInt(i - 1) + "]." + prop));
						isParams = true;
					}
					else
						text = eval("data[" + parseInt(i - 1) + "]." + prop);
				}
				/*
				\u7b2c\u4e8c\u7c7b\u60c5\u51b5\uff1a\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4f46\u6709calculate\u5c5e\u6027
				*/
				else if(usermethod !=null && usermethod !="")
				{
					text = calculateUserMethod(data, i, usermethod, innertxt);
					isParams = true;
				}
				/*
				\u7b2c\u4e09\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027, \u4f46\u7528\u6237\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f${}
				*/
				else if(innertxt != null && innertxt != "")
			   // else if(false)
				{
					text = calculateUserMethod(data, i, usermethod, innertxt, prop);
				}
				/*
				\u7b2c\u56db\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f${}
				*/
				else
				{
					text = innertxt;	
				}
				  		
	    			
				//text = innertxt;
     	  //\u884c\u5e8f\u53f7
                if (prop == '$ROWNUM')
                {
                    text = i;
                }
                    //\u5982\u679c\u662f\u65e5\u671f\u578b\u7684
                else if (datatype != null && datatype.toLowerCase().indexOf("date") != -1)
                {
                    var formate = null;
                    var f = datatype.toLowerCase();
                    var s = f.indexOf("(");
                    if (s != -1)
                    {
                        formate = f.substring(s + 1, f.indexOf(")"));
                    }
                    text = getTimefromUTC(formate, text);
                }
                
                if(!isParams)
                	text = caculator_processor(usermethod, prop, text);
				
     	  		//firfox \u4e2d\u4e0d\u652f\u6301innerText;
                column_ctrlex_Processor(objCell, statusDesc, text);
                
    		}	
    		catch(err)
    		{
    			//alert("\u521b\u5efa\u6570\u636e\u5355\u5143\u683c\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5\u5217caculate\u548cprop\u8bbe\u7f6e\uff01");
    			alert($res_entry('ui.control.table.createdatatds.error','\u521b\u5efa\u6570\u636e\u5355\u5143\u683c\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5\u5217caculate\u548cprop\u8bbe\u7f6e\uff01'));
    		}
    	}
    	//\u5904\u7406\u6709\u8282\u70b9\u573a\u666f:\u5f00\u59cb
    	else
    	{
  		    //\u5b58\u5728column\u7684prop\u5c5e\u6027
            var prop = '';
            if (col[index].getAttribute("prop") != null)
            {
                prop = col[index].getAttribute("prop");
            }
	
	  		//\u521b\u5efa\u5b50\u8282\u70b9\u65f6,\u5220\u9664\u65e7\u7684\u8282\u70b9
            for (var k = 0; k < row[i].cells[index].childNodes.length; k++)
            {
                row[i].cells[index].removeChild(row[i].cells[index].childNodes[k]);
            }
 	    	//\u5faa\u73af\u5217\u91cc\u9762\u7684\u63a7\u4ef6
            for (var m = 0; m < col[index].td_Children.length; m++)
            {
		  	//\u53d6\u5f97\u6a21\u677ftd\u91cc\u9762\u7684\u63a7\u4ef6
                contrlnode = col[index].td_Children[m];
				if (contrlnode.nodeType == 3) 
				{
					//row[i].cells[j].appendChild(contrlnode.cloneNode(true));
					objCell.appendChild(contrlnode.cloneNode(true));
					continue;
				}
                //\u5224\u65adtd\u91cc\u9762\u7684\u63a7\u4ef6\u662f\u5426\u5b58\u5728prop\u5c5e\u6027,\u5982\u679c\u5b58\u5728,\u4ee5\u63a7\u4ef6\u7684prop\u5c5e\u6027\u4e3a\u51c6
                if (contrlnode.getAttribute("prop") != null)
                {
                    prop = contrlnode.getAttribute("prop");
                }
                //\u53d6\u5f97\u63a7\u4ef6\u7c7b\u578b
                var value = "";
                if (prop != null && prop != "")
                {
                    value = eval("data[i-1]." + prop);
                }
                //\u5f97\u5230\u63a7\u4ef6\u5904\u7406\u5668\u7c7b\u578b
                var ctrTag = table_Ctr_Processor(contrlnode, value, i, tid, data[parseInt(i - 1)], usermethod, naturalline);
                
                //\u5982\u679c\u8fd4\u56de\u7684\u662fString
                if (typeof ctrTag == "string")
                {
                    objCell.innerHTML = ctrTag;
                }
                //\u5982\u679c\u662f\u5bf9\u8c61 
                else 
                {
                    objCell.appendChild(ctrTag);
                    var Ctr_tagName = ctrTag.tagName.toUpperCase();
                    //\u5982\u679c\u662fLOV\u6807\u7b7e
                    if (Ctr_tagName == 'Z:SSB_RIA_LOV' || Ctr_tagName == 'SSB_RIA_LOV')
                    {
                        readRiaLovTag(ctrTag);
                        var tt = document.getElementById(ctrTag.id);
                        tt.value = value;
                    }
                    //\u5982\u679c\u662f\u81ea\u52a8\u5b8c\u6210\u6807\u7b7e
                    if (Ctr_tagName == 'Z:AUTOCOMPLETE' || Ctr_tagName == 'AUTOCOMPLETE')   autoCtrId_arr.push(ctrTag.getAttribute('id'));
                    //\u662fradioGroup\u63a7\u4ef6
                    if (Ctr_tagName == 'Z:RADIOGROUP' || Ctr_tagName == 'RADIOGROUP')
                    {
                        radios = new RadioGroupTag(ctrTag.getAttribute('id'));
                        radios.init();
                        radios.setValue(value);
                    }
                    if (Ctr_tagName == 'Z:UPLOAD' || Ctr_tagName == 'UPLOAD')
                    {
                        var upload = UploadTag(ctrTag.getAttribute('id'));
                        upload.init();
                        var oInput = upload.getFileCtrl();
                        oInput.value = value;
                    }
                    //\u5982\u679c\u5b58\u5728type\u5c5e\u6027
                    var tagtype = ctrTag.getAttribute("type");
                    if (tagtype != null)
                    {
                        tagtype = tagtype.toUpperCase();
                        //\u5982\u679c\u662f\u5355\u9009\u6846\u6216\u590d\u9009\u6846\u65f6\u8bbe\u7f6e\u9009\u4e2d\u9879
                        if ((tagtype == "CHECKBOX" || tagtype == "RADIO") && ctrTag.getAttribute("ctrlex") != null)
                        {
                            var ctlex = ctrTag.getAttribute('ctrlex');
                            var select = evalParse(tableData[parseInt(i - 1)], ctlex);

                            if (select != 0 && select != null)
                            {
                                ctrTag.setAttribute('checked', 'true');
                            }
                        }
                    }
                }
              }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
    		}//\u5904\u7406\u6709\u8282\u70b9\u573a\u666f:\u7ed3\u675f
    	
    	assignDataCSS(col[index], objCell); 
    }	
    index = clums = col = props = caption = data = displayTB = tObject = tModle = autoCtrId_arr = row = null;
}

function calculateUserMethod(tableData, index, usermethod, contxt, prop, preText)
{
	var isParams = false;
	var text, exps, levels;
   //\u60c5\u51b5\u4e00\uff1a\u81ea\u5b9a\u4e49\u65b9\u6cd5\u662f\u5426\u6709\u53c2\u6570
	if(usermethod != null && usermethod.indexOf("(") != - 1)
	{
	   if(usermethod.indexOf(")") != - 1)
	   {
	      var paramConvert = function(value)
	      {
	         var exps = value.match(/\${.+?}/g);
	         for(var i = 0; i < exps.length; i ++ )
	         {
	            var loc = value.indexOf(exps[i]) ;
	            if( value.charAt(loc - 1) != "'")
	            {
	               if(value.charAt(loc - 1) != "\"")
	               {
	                  value = value.replace(exps[i], "'"+exps[i]+"'");
	               }
	            }
	         }
	         return value;
	      }
         contxt = paramConvert(usermethod);
         isParams = true;
      }
      else
      usermethod = null;
   }
   //\u60c5\u51b5\u4e8c\uff1a\u81ea\u5b9a\u4e49\u65b9\u6cd5\u65e0\u53c2\u6570\u7684\u5199\u6cd5
   else if(usermethod != null)
   {
   		text = caculator_processor(usermethod, prop, preText);
   		contxt = "";
   }
   //\u60c5\u51b5\u4e09\uff1a\u8868\u8fbe\u5f0f\u5904\u7406
   if(contxt != null && contxt != "")
   {
      exps = contxt.match(/\${.+?}/g);
      for(var nn = 0; nn < exps.length; nn ++ )
      {
         levels = exps[nn].substring(2, exps[nn].length - 1).split(".");
         var datastring = "tableData[" + parseInt(index - 1) + "]";
         for(var mm = 0; mm < levels.length; mm ++ )
         {
            datastring += "." + levels[mm];
         }
         if(isParams)
         datastring = eval(datastring);
         else
         datastring = caculator_processor(usermethod, exps[nn].substring(2, exps[nn].length - 1), eval(datastring));

         contxt = contxt.replace(exps[nn], datastring);
      }

      if(isParams)
      {
         usermethod = contxt;
         contxt = caculator_processor(usermethod);
      }
      text = contxt;
   }
   
   return text;
};

function caculator_processor(usermethod, key, value)
{
	var keyvalue = new Array();	
	if(usermethod != null && usermethod != "")
	{	
		try
		{
			if(usermethod.indexOf("(") != -1)
            {                 		         
                 value = eval(usermethod);	
            }
            else
            {
                keyvalue[key] = value;	
		        value = eval(usermethod +"(keyvalue);")[key];
            }
		}
		catch(err)
		{
			var msg = "\u4f7f\u7528\u81ea\u5b9a\u4e49\u8f6c\u6362\u65b9\u6cd5\u9519\u8bef\uff0c\u8bf7\u68c0\u67e5caculate\u5c5e\u6027\uff01";
			alert($res_entry('ui.control.table.caculator.error',msg));
		}	
		finally
		{
			keyvalue = null;
		}
	}
	
	return value;
}

function assignDataCSS(col, objCell)
{
	var align = col.getAttribute("align");
	var style = col.getAttribute("style");
	
	if(align != null && align != "")
	{	
		objCell.setAttribute("style", "TEXT-ALIGN:"+align);
        if(isIE())            
        	objCell.style.cssText = "TEXT-ALIGN:"+align;				
	}
	else if(style != null && style != "")
	{
       objCell.setAttribute("style", style);
       if(isIE())            
       		objCell.style.cssText = style.cssText;	
	}
	
	align = style = null;
}

function displayColumn(tid, cid)
{
	//\u5224\u65ad\u9875\u9762\u4e2d\u662f\u5426\u6709\u8fd9\u6837\u4e00\u5217\uff0c\u4ee5\u5217id\u4e3a\u51c6\uff1b
	var canBeShown = false;
	for(t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	//\u53ea\u6709\u8be5\u5217\u5b58\u5728\u624d\u53ef\u4ee5\u7ee7\u7eed
	if(!canBeShown)
	{
		//alert("\u8be5\u5217\u4e0d\u5b58\u5728\uff01");
		//alert($res_entry('ui.control.table.column.noColumn','\u8be5\u5217\u4e0d\u5b58\u5728\uff01'));
		return;
	}
	
	//\u5224\u65ad\u8be5\u5217\u662f\u5426\u5df2\u7ecf\u663e\u793a
	for(var t=0; t<tableModel_arr[tid].cells.length; t++)
	{
		if (cid == tableModel_arr[tid].cells[t].id)
		{
			//alert("\u8be5\u5217\u5df2\u7ecf\u663e\u793a\uff01");
			//alert($res_entry('ui.control.table.column.isExisted','\u8be5\u5217\u5df2\u7ecf\u663e\u793a\uff01'));
			return;
		}
	}
	
	//\u8868\u6a21\u578b\u4e2d\u6240\u6709\u7684\u5217
	var clumns = tableModel_arr[tid].clums;
	//\u8be5\u5217\u5728\u6240\u6709\u5217\u6570\u7ec4\u4e2d\u7684index
	var columnIndex;
	//\u952e\u503c\u5bf9\u8c61
	var prop_caption_index = [];
	//\u8be5\u5217\u5728cell\u6570\u7ec4\u4e2d\u7684index
	var preDisplayIndex = 0;
	//\u67d0\u5217\u662f\u5426\u53ef\u89c1
	var visible;
	
	for(var i=0; i< clumns.length; i++)
	{
		visible = clumns[i].getAttribute("visible");
		if(visible != false && visible != "false")
		{
			preDisplayIndex++;
		}
		if(clumns[i].id == cid)
		{
			prop_caption_index['preDisplayIndex'] = preDisplayIndex;
			prop_caption_index['index'] = i;
			prop_caption_index['prop'] = clumns[i].getAttribute('prop');
			prop_caption_index['caption'] = clumns[i].getAttribute('caption');
			prop_caption_index['column'] = clumns[i];
			
			//\u8be5\u5217\u4f1a\u88ab\u63d2\u5165\uff0c\u8bbe\u7f6e\u8be5\u5217\u7684visible\u4e3atrue
			clumns[i].setAttribute("visible","true");
			break;
		}
	}
	
    //\u8bbe\u7f6e\u8be5\u5217Cell\u7684\u6821\u9a8c\u51fd\u6570
    prop_caption_index['column'].validate = prop_caption_index['column'].getAttribute("validate");
    
	//\u8bbe\u7f6e\u8be5\u5217Cell\u7684\u6570\u636e\u7c7b\u578b
    prop_caption_index['column'].datatype = prop_caption_index['column'].getAttribute("datatype");
    
	//\u8bbe\u7f6e\u8be5\u5217Cell\u7684td_Children\u5c5e\u6027
    var td_Node_Children = prop_caption_index['column'].childNodes;
    var td_Children = new Array();
    for (var m = 0; m < td_Node_Children.length; m ++)
    {
		//\u5e94\u5f53\u628a\u6ce8\u91ca\u4e5f\u53bb\u6389nodevalue=8
        if (td_Node_Children[m].nodeType != 8 && td_Node_Children[m].tagName != null && td_Node_Children[m].tagName != "Z:COLUMN")
        {
            td_Children.push(td_Node_Children[m]);
        }
		else if(td_Node_Children[m].nodeType == 3 && $.trim(td_Node_Children[m].nodeValue).length > 0)
		{
			var textvalue = td_Node_Children[m].nodeValue.match(/#nbsp;/gi);
			if(typeof(textvalue) != "undefined" && textvalue != null)
			{
				var space = document.createTextNode(" ");
				for(var i=0; i<textvalue.length; i++) td_Children.push(space);
			}
		}
    }
    prop_caption_index['column'].td_Children = td_Children;
    //\u5728\u8868\u683c\u6a21\u578b\u4e2d\u63d2\u5165\u8be5\u5217\u76f8\u5173\u4fe1\u606f
    tableModel_arr[tid].props = insertAt(tableModel_arr[tid].props, prop_caption_index['preDisplayIndex'], prop_caption_index['prop']);
    tableModel_arr[tid].caption = insertAt(tableModel_arr[tid].caption, prop_caption_index['preDisplayIndex'], prop_caption_index['caption']);	
    tableModel_arr[tid].cells = insertAt(tableModel_arr[tid].cells, prop_caption_index['preDisplayIndex'], prop_caption_index['column']);	
    
    //\u521b\u5efa\u5217\u8868\u5934
	createCaption(tid, cid, prop_caption_index);
	//\u521b\u5efa\u5217\u6570\u636e
	createDataTds(tid, cid, prop_caption_index);
	//\u6e05\u9664\u6570\u636e\u7a7a\u95f4
	clumns = columnIndex = prop_caption_index = visible = td_Node_Children = td_Children = null;
}

function insertAt (arr, index, value ) 
{
	var part1 = arr.slice( 0, index );
	var part2 = arr.slice( index );
	part1.push( value );
	return( part1.concat( part2 ) );
}

function removeAt(arr, index )
{
	var part1 = arr.slice( 0, index+1 );
	var part2 = arr.slice( index+1 );
	part1.pop();
	return( part1.concat( part2 ) );
}

function hideColumn(tid, cid)
{
	//\u5224\u65ad\u9875\u9762\u4e2d\u662f\u5426\u6709\u8fd9\u6837\u4e00\u5217\uff0c\u4ee5\u5217id\u4e3a\u51c6\uff1b
	var canBeShown = false;
	for(t=0; t<tableModel_arr[tid].clums.length; t++)
	{
		if (cid == tableModel_arr[tid].clums[t].id)
		{
			canBeShown = true;
			break;
		}		
	}
	//\u53ea\u6709\u8be5\u5217\u5b58\u5728\u624d\u53ef\u4ee5\u7ee7\u7eed
	if(!canBeShown)
	{
		//alert("\u8be5\u5217\u4e0d\u5b58\u5728\uff01");
		//alert($res_entry('ui.control.table.column.noColumn','\u8be5\u5217\u4e0d\u5b58\u5728\uff01'));
		return;
	}
	
	//\u5224\u65ad\u8be5\u5217\u662f\u5426\u5df2\u7ecf\u9690\u85cf
	for(var valid=0; valid<tableModel_arr[tid].cells.length; valid++)
	{
		if (cid != tableModel_arr[tid].cells[valid].id) continue;
		break;
	}
	if( valid == tableModel_arr[tid].cells.length)
	{
		//alert("\u8be5\u5217\u5df2\u7ecf\u9690\u85cf\uff01");
		//alert($res_entry('ui.control.table.column.isHidden','\u8be5\u5217\u5df2\u7ecf\u9690\u85cf\uff01'));
		return;
	}
	
	//\u53d6\u5f97\u663e\u793a\u7684\u6240\u6709\u5217
	var cells = tableModel_arr[tid].cells;
	//\u53d6\u5f97\u5185\u5b58\u4e2d\u7684\u6240\u6709\u5217
	var clumns = tableModel_arr[tid].clums;
	//\u8981\u5220\u9664\u7684\u663e\u793a\u5217\u7684\u5e8f\u53f7
	var deleteColumnIndex;
	var displayTB = tableModel_arr[tid].table;	
	
	//\u901a\u8fc7\u5faa\u73af\u5f97\u5230\u5c06\u5220\u9664\u5217\u5728cell\u4e2d\u7684index
	for(var i=0; i< cells.length; i++)
	{
		if(cells[i].id == cid)
		{
			deleteColumnIndex = i;
			break;
		}
	}
	
	//\u8be5\u5217\u4f1a\u88ab\u5220\u9664\uff0c\u901a\u8fc7\u5faa\u73af\u5339\u914d\uff0c\u8bbe\u7f6e\u8be5\u5217\u7684visible\u4e3afalse
	for(var i=0; i< clumns.length; i++)
	{
		if(clumns[i].id == cid)
		{
			clumns[i].setAttribute("visible","false");
			break;
		}
	}
	
    //\u5728\u8868\u683c\u6a21\u578b\u4e2d\u5220\u9664\u8be5\u5217\u76f8\u5173\u4fe1\u606f
	tableModel_arr[tid].props = removeAt(tableModel_arr[tid].props,deleteColumnIndex);
    tableModel_arr[tid].caption = removeAt(tableModel_arr[tid].caption,deleteColumnIndex);	
    tableModel_arr[tid].cells = removeAt(tableModel_arr[tid].cells,deleteColumnIndex);	
    //tableModel_arr[tid].captiontips = removeAt(tableModel_arr[tid].captiontips,deleteColumnIndex);	
    
    if(!isIE())
    {
    	//\u8ba9FireFox\u652f\u6301children\u5c5e\u6027
    	FF_children();
    }
    
    //\u53d6\u5f97\u663e\u793a\u8868\u683c\uff0c\u5faa\u73af\u6bcf\u4e00\u884c\uff0c\u5220\u9664\u76f8\u5e94\u8282\u70b9
	for(var i=0; i<displayTB.rows.length; i++)
	{
    	displayTB.rows[i].removeChild(displayTB.rows[i].children[deleteColumnIndex]);
    }
    
    //\u6e05\u9664\u76f8\u5173\u53d8\u91cf
    valid = cells = clumns = deleteColumnIndex = displayTB = null;
}

function addEventForCheckBox(TagObject, MtableId)
{
    if (TagObject.getAttribute('type') && (TagObject.getAttribute('type')).toUpperCase() == "CHECKBOX")
    {
        var even = function() {
            if (TagObject.checked == false && document.getElementById(MtableId + 'allCheck'))
            {
                document.getElementById(MtableId + 'allCheck').checked = false;
            }
			else (TagObject.checked == true)
			{
				var td = TagObject.parentNode;
				while (td.tagName.toLowerCase() != 'td')    {
			        td = td.parentNode;
			    }
				//\u68c0\u67e5\u662f\u5426\u6570\u636e\u9879\u5df2\u5168\u90e8\u9009\u4e2d\uff0c\u5982\u679c\u9009\u4e2d\uff0c\u5219\u5c06\u6807\u9898\u4e2d\u7684checkbox\u8bbe\u7f6e\u4e3a\u9009\u4e2d
				var allcheck = false;
				var colobj = getTableById(MtableId).getColumn([td.cellIndex]);
				$(colobj).find("input[type='checkbox']").each(function(index){
					if(index > 0){
						if(this.checked == false) {
							allcheck = false;		return false;
						}
						else{
							allcheck = true;
						}
					}
				});
				document.getElementById(MtableId + 'allCheck').checked = allcheck;
			}
            //eval(onclick); //\u539f\u4ee3\u7801\u4f1a\u8ba9\u8868\u683c\u91cc\u9762\u7684\u6bcf\u4e00\u4e2acheckbox\u90fd\u6267\u884c\u4e24\u6b21
        };
        if (isIE())
        {
            //TagObject.attachEvent("onclick",even);
			//change by Daniel for attachevent memory leak ;
			EventManager.Add(TagObject,'click',even);
        } else {
            TagObject.addEventListener("click", even, false);
        }
    }
}
;

table_Ctr_Processor = function(tagObject, value, rowindex, MtableId, data, usermethod, naturalline)
{
    var ctrName = tagObject.tagName;
    var tagName = ctrName.toUpperCase();//\u6807\u7b7e\u540d\u79f0
    //var reTagObject = tagObject.cloneNode(true); //\u8fd4\u56de\u7684\u6807\u7b7e\u5bf9\u8c61
    
    var reTagObject = tagObject.cloneNode(true); 
     //\u4f18\u53161 \u4e0b\u62c9\u6846\u9009\u9879
     if(reTagObject.tagName.toUpperCase() == "SELECT")
 	 {
		reTagObject.selectedIndex = tagObject.selectedIndex;
 	 }
     
    if (reTagObject.id != null && reTagObject.tagName != "CALENDAR" && reTagObject.tagName != "Z:CALENDAR")  
    {
    	//\u52a0\u5f3a\u5224\u65ad\uff0c\u8ba9\u884c\u63a7\u4ef6ID\u90fd\u52a0\u4e0a\u4e00\u4e2a\u81ea\u7136\u5e8f\u53f7\uff0c\u786e\u4fdd\u65e0\u91cd\u590d
    	if(naturalline != null)
    		reTagObject.id = reTagObject.id + naturalline + rowindex;
    	else
    		reTagObject.id = reTagObject.id + rowindex;
    }
    		
    reTagObject = getOuterHtml(reTagObject, data, usermethod);
    switch (tagName)
            {
        case "INPUT" : //\u8f93\u5165\u6846
        	var useExp = tagObject.getAttribute("value");
        	//\u7528\u4e8e\u5224\u65ad\u662f\u5426\u4e3avalue\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u5982\u679c\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f, \u503c\u4e0d\u80fd\u88ab\u6539\u53d8
        	if(useExp != null)
        		useExp = useExp.indexOf("${");
        	else
        		useExp = -1;
            return input_Ctr_Processor(reTagObject, value, rowindex, MtableId, useExp);
            break;

        case "A" : //\u8d85\u8fde\u63a5\u6807\u7b7e
            return A_Ctr_Processor(reTagObject, value, rowindex, MtableId, data);
            break;

        case "SELECT" ://\u4e0b\u62c9\u5217\u8868
            return select_Ctr_Processor(reTagObject, value, rowindex, MtableId, data);
            break;

        case "CALENDAR": //\u65e5\u671f\u63a7\u4ef6
            return Calendar_Ctr_Processor(reTagObject, value, rowindex, naturalline);
            break;

        case "Z:CALENDAR": //\u65e5\u671f\u63a7\u4ef6
            return Calendar_Ctr_Processor(reTagObject, value, rowindex, naturalline);
            break;

        case "IMG": //\u56fe\u7247\u6807\u7b7e
            return reTagObject;
            break;

        case "Z:SSB_RIA_LOV"://lov\u63a7\u4ef6FF
            return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "SSB_RIA_LOV"://lov\u63a7\u4ef6IE
            return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "Z:UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
            return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;

        case "UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
            return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
            break;
        default :
            return reTagObject;
            break;
    }

};

bindData2TableCtrl = function(MtableId, tableData)
{
    var tObject = tableModel_arr[MtableId].table;
    var tModle = tableModel_arr[MtableId];
    var autoCtrId_arr = new Array();
    var row = tObject.rows;
    var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9
    for (var i = 1; i < row.length; i ++)//\u5faa\u73af\u5185\u5b58\u6bcf\u4e00\u884c
    {
        var rowcells = row[i].cells;//\u53d6\u884c\u5bf9\u8c61
		var dyn = getKey();//\u6bcf\u884c\u7684\u81ea\u7136\u5e8f\u53f7
        for (var j = 0; j < tdnodes.length; j++)//\u5faa\u73af\u6a21\u578b\u8868\u683c
        {            
			//2008-09-01 wxr \u4e3a\u6570\u636e\u6dfb\u52a0\u9ed8\u8ba4\u7684\u5bf9\u9f50\u65b9\u5f0f\uff0c\u8bfb\u53d6\u5e76\u8bbe\u7f6e\u5728<z:column/>\u4e2d\u7684style\u5c5e\u6027 begin		
			 assignDataCSS(tdnodes[j], rowcells[j]);
			//2008-09-01 wxr \u8bfb\u53d6\u5e76\u8bbe\u7f6e\u5728<z:column/>\u4e2d\u7684style\u5c5e\u6027 end			
			
			var datatype = tdnodes[j].getAttribute("datatype");
            var contrlnode = '';
            //\u7528\u6237\u81ea\u5b9a\u4e49\u8f6c\u6362\u65b9\u6cd5
			var usermethod = tdnodes[j].getAttribute('calculate');

     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6 
            if (tableModel_arr[MtableId].cells[j].td_Children.length == 0)
            {
                var statusDesc = tdnodes[j].getAttribute("ctrlex");
                var contxt = tdnodes[j].innerHTML;
                var prop = tableModel_arr[MtableId].props[j];//\u5f97\u5230column\u7684prop\u5c5e\u6027
                
				var text;//, exps, levels;
				
				var isParams = false;
				/*
				\u7b2c\u4e00\u7c7b\uff1a\u6709prop\uff0c \u4f46\u662f\u540c\u65f6\u6709calcualte\u5c5e\u6027\uff0c \u6b64\u65f6\u4ee5calculate\u5c5e\u6027\u4e3a\u4e3b\uff0c \u5ffd\u7565prop\u5c5e\u6027
				*/				
				if(prop != null && prop !="")
				{
					if(usermethod !=null && usermethod !="")
					{
						text = calculateUserMethod(tableData, i, usermethod, contxt, prop, eval("tableData[" + parseInt(i - 1) + "]." + prop));
						isParams = true;
					}
					else
						text = eval("tableData[" + parseInt(i - 1) + "]." + prop);
				}
				/*
				\u7b2c\u4e8c\u7c7b\u60c5\u51b5\uff1a\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4f46\u6709calculate\u5c5e\u6027
				*/
				else if(usermethod !=null && usermethod !="")
				{
					text = calculateUserMethod(tableData, i, usermethod, contxt);
					isParams = true;
				}
				/*
				\u7b2c\u4e09\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027, \u4f46\u7528\u6237\u4f7f\u7528\u4e86\u8868\u8fbe\u5f0f${}
				*/
				else if(contxt != null && contxt != "")
			   // else if(false)
				{
					text = calculateUserMethod(tableData, i, usermethod, contxt, prop);
				}
				/*
				\u7b2c\u56db\u7c7b\u60c5\u51b5\uff1a\u65e2\u6ca1\u6709prop\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709calculate\u5c5e\u6027\uff0c \u4e5f\u6ca1\u6709\u8868\u8fbe\u5f0f${}
				*/
				else
				{
					text = contxt;	
				}
				
     	  		//\u884c\u5e8f\u53f7
                if (prop == '$ROWNUM')
                {
                    text = i;
                }
                //\u5982\u679c\u662f\u65e5\u671f\u578b\u7684
                else if (datatype != null && datatype.toLowerCase().indexOf("date") != -1)
                {
                    var formate = null;
                    var f = datatype.toLowerCase();
                    var s = f.indexOf("(");
                    if (s != -1)
                    {
                        formate = f.substring(s + 1, f.indexOf(")"));
                    }
                    text = getTimefromUTC(formate, text);
                }
                
                if(!isParams)
                	text = caculator_processor(usermethod, prop, text);
                
     	  		//firfox \u4e2d\u4e0d\u652f\u6301innerText;
                column_ctrlex_Processor(rowcells[j], statusDesc, text);
            }
            else {//\u5305\u542b\u63a7\u4ef6
                var prop = '';
     	   //\u5b58\u5728column\u7684prop\u5c5e\u6027
                if (tdnodes[j].getAttribute("prop") != null)
                {
                    prop = tdnodes[j].getAttribute("prop");
                }
     	   //\u5faa\u73af\u5217\u91cc\u9762\u7684\u63a7\u4ef6

                for (var k = 0; k < row[i].cells[j].childNodes.length; k++)
                {
                    //\u521b\u5efa\u5b50\u8282\u70b9\u65f6,\u5220\u9664\u65e7\u7684\u8282\u70b9
                    row[i].cells[j].removeChild(row[i].cells[j].childNodes[k]);
                }
     	    
     	    //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
                for (var m = 0; m < tableModel_arr[MtableId].cells[j].td_Children.length; m++)
                {
                    //\u53d6\u5f97\u6a21\u677ftd\u91cc\u9762\u7684\u63a7\u4ef6
                    contrlnode = tableModel_arr[MtableId].cells[j].td_Children[m];
					if (contrlnode.nodeType == 3) 
					{
						row[i].cells[j].appendChild(contrlnode.cloneNode(true));
						continue;
					}
     	     //\u5224\u65adtd\u91cc\u9762\u7684\u63a7\u4ef6\u662f\u5426\u5b58\u5728prop\u5c5e\u6027,\u5982\u679c\u5b58\u5728,\u4ee5\u63a7\u4ef6\u7684prop\u5c5e\u6027\u4e3a\u51c6
                    if (contrlnode.getAttribute("prop") != null)
                    {
                        prop = contrlnode.getAttribute("prop");
                    }
     	      //\u53d6\u5f97\u63a7\u4ef6\u7c7b\u578b
                    var value = "";
                    if (prop != null && prop != "")
                    {
                        //value = evalParse(tableData[parseInt(i-1)],prop);
                        value = eval("tableData[" + parseInt(i - 1) + "]." + prop);
                    }
                    //\u5f97\u5230\u63a7\u4ef6\u5904\u7406\u5668\u7c7b\u578b;
                    var ctrTag = table_Ctr_Processor(contrlnode, value, i, MtableId, tableData[parseInt(i - 1)], usermethod, dyn);
                    //\u5982\u679c\u8fd4\u56de\u7684\u662fString
                    if (typeof ctrTag == "string")
                    {
                        row[i].cells[j].innerHTML = ctrTag;
                    }
                    else { //\u5982\u679c\u662f\u5bf9\u8c61
                        row[i].cells[j].appendChild(ctrTag);
                        var Ctr_tagName = ctrTag.tagName.toUpperCase();
				  //\u5982\u679c\u662fLOV\u6807\u7b7e
                        if (Ctr_tagName == 'Z:SSB_RIA_LOV' || Ctr_tagName == 'SSB_RIA_LOV')
                        {
                            readRiaLovTag(ctrTag);
                            var tt = document.getElementById(ctrTag.id);
                            tt.value = value;
                        }
				  //\u5982\u679c\u662f\u81ea\u52a8\u5b8c\u6210\u6807\u7b7e
                        if (Ctr_tagName == 'Z:AUTOCOMPLETE' || Ctr_tagName == 'AUTOCOMPLETE')   autoCtrId_arr.push(ctrTag.getAttribute('id'));
			      //\u662fradioGroup\u63a7\u4ef6
                        if (Ctr_tagName == 'Z:RADIOGROUP' || Ctr_tagName == 'RADIOGROUP')
                        {
                            radios = new RadioGroupTag(ctrTag.getAttribute('id'));
                            radios.init();
                            radios.setValue(value);
                        }
                        if (Ctr_tagName == 'Z:UPLOAD' || Ctr_tagName == 'UPLOAD')
                        {
                            var upload = UploadTag(ctrTag.getAttribute('id'));
                            upload.init();
                            var oInput = upload.getFileCtrl();
                            oInput.value = value;
                        }
                        //\u5904\u7406COLORBAR\u63a7\u4ef6
                         if (Ctr_tagName == 'Z:COLORBAR' || Ctr_tagName == 'COLORBAR')
                        {
                            var colorbar = ColorBarCtrObj.getColorBarTag(ctrTag.getAttribute('id'));
                            colorbar.initialize();                      
                            colorbar.setValue(value);
                        }
                       
                        //\u5904\u7406inputtext\u63a7\u4ef6
                        if (Ctr_tagName == 'Z:INPUTTEXT' || Ctr_tagName == 'INPUTTEXT')
                        {
                            var inputtext = ChangeableTextCtrObj.getChangeableTextTag(ctrTag.getAttribute('id'));
                            inputtext.initialize();                   
                            inputtext.setValue(value);
                        }
                        
                        
			      //\u5982\u679c\u5b58\u5728type\u5c5e\u6027
                        var tagtype = ctrTag.getAttribute("type");
                        if (tagtype != null)
                        {
                            tagtype = tagtype.toUpperCase();
				  	//\u5982\u679c\u662f\u5355\u9009\u6846\u6216\u590d\u9009\u6846\u65f6\u8bbe\u7f6e\u9009\u4e2d\u9879
                            if ((tagtype == "CHECKBOX" || tagtype == "RADIO") && ctrTag.getAttribute("ctrlex") != null)
                            {
                                //\u9009\u4e2d\u9879
                                var ctlex = ctrTag.getAttribute('ctrlex');
                                var select = evalParse(tableData[parseInt(i - 1)], ctlex);

                                if (select != 0 && select != null)//\u5982\u679c\u4e0d\u7b49\u4e8e0\u8868\u793a\u9009\u4e2d
                                {
                                    ctrTag.setAttribute('checked', 'true');
                                }
                            }
							else if (tagtype == "CHECKBOX"){
								var ack = document.getElementById(MtableId + 'allCheck');
								if(ack != null){
									var ack = ack.parentNode;
									while (ack.tagName.toLowerCase() != 'th')
								    {
								        ack = ack.parentNode;
								    }
									
									var td = ctrTag.parentNode;
									while (td.tagName.toLowerCase() != 'td')
								    {
								        td = td.parentNode;
								    }	
									
									if(ack.cellIndex == td.cellIndex)
										addEventForCheckBox(ctrTag, MtableId);
								}
							}
                        }
                    }//end else

                }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
                var prop = tableModel_arr[MtableId].props[j];//\u5f97\u5230column\u7684prop\u5c5e\u6027
                fillCellWithProp(row[i].cells[j], prop, tableData[parseInt(i - 1)]);
            }

        }//\u5217\u5faa\u73af

    }//\u884c\u5faa\u73af
    for (var i = 0; i < autoCtrId_arr.length; i ++)
    {
        autos = new AutoCompleteTag(autoCtrId_arr[i]);
        autos.init();
    }
    //\u5fc5\u987b\u8981vstext\u4e0d\u4e3a\u7a7a\u65f6\u624d\u81ea\u52a8\u521b\u5efa\u7edf\u8ba1\u884c, \u4ee5\u652f\u6301\u7528\u6237\u81ea\u5b9a\u4e49\u7edf\u8ba1\u884c
    if (tableModel_arr[MtableId].vstatcaption)
    	if (tableModel_arr[MtableId].vstext != "")
    	    Vstatcaption_Processor(MtableId, tableData);
	//onFinished \u4e8b\u4ef6\u5904\u7406
    if (tableModel_arr[MtableId].onFinished) {
        parseFunction(tableModel_arr[MtableId].onFinished, tableModel_arr[MtableId].table);
    }
	
	//\u5f53\u9f20\u6807\u5728\u4e0d\u540c\u6570\u636e\u884c\u4e0a\u79fb\u52a8\u65f6\u6539\u53d8\u5176\u80cc\u666f\u8272
	$(getTableById(MtableId).table).find("tr").each(function(index){
		 if (index != 0) 
			$(this).hover(function(){
				$(this).find("td").find("input").andSelf().addClass("trHover");
			}, function(){
				$(this).find("td").find("input").andSelf().removeClass("trHover");
			});
	});
	
	//\u5224\u65ad\u7528\u6237\u662f\u5426\u5b9a\u4e49\u4e86\u8df3\u8f6c\u540e\u8981\u6267\u884c\u7684\u4e8b\u4ef6 loadAfterTurnpage()
	if(typeof(loadAfterTurnpage) != "undefined" && loadAfterTurnpage.constructor == Function)
		loadAfterTurnpage(MtableId);
	
	//\u53d6\u6d88\u8d85\u94fe\u63a5\u7684\u9ed8\u8ba4\u884c\u4e3a
	$(tableModel_arr[MtableId].table).find("a[onclick]").each(function(){		
		var achor = this;
		EventManager.Add(achor, "click", function(event){	
			if(!isIE())
				event.preventDefault();
			else
				window.event.returnValue=false;
		});	
	});
	tableData = tObject = tModle = row = tdnodes = rowcels = null;
};

function fillCellWithProp(cell, prop, data)
{
	if(prop != null && prop != "null" && prop != "")
    {
    	var inputCtrs = cell.getElementsByTagName("input");
    	var selCtrs = cell.getElementsByTagName("select");
    	if(inputCtrs != null && inputCtrs.length > 0 && selCtrs.length == 0)
    	{
    		for(var nn=0; nn<inputCtrs.length; nn++)
    		{
    			if(inputCtrs[nn].type != "hidden") return;
    		}
			var novalue = "";
	    	if(isIE())
				novalue = cell.innerText;
			else
				novalue = cell.textContent;
			if($.trim(novalue) == "")
			{
				var ef = Express_Factory("", data);
				cell.appendChild(document.createTextNode(ef.getExpValue(prop)));
				/*
				if(isIE())
					cell.innerText = ef.getExpValue(prop);
				else
					cell.textContent = ef.getExpValue(prop);
				*/
			}
    	}
    }
};

function Vstatcaption_Processor(mid, tableData)
{
    var tr = addOneRow(mid);
    tr.className = "vstatcaption";
    var cells = tableModel_arr[mid].cells;
    var texttitle = tableModel_arr[mid].vstext;
    innerText_function(tr.cells[0], texttitle);
    tr.cells[0].className = "title";
    for (var i = 1; i < cells.length; i ++)
    {
        tr.cells[i].className = "data";
        var dd = cells[i].getAttribute(VSTATFORMULA);
        var tv = enumerate_value(cells[i], dd);
        if (cells[i].getAttribute("datatype") == "date")
        {
            var formate = tableModel_arr[mid].date_formate;
            tv = getTimefromUTC(formate, tv);
        }
        innerText_function(tr.cells[i], tv);
    }

    function enumerate_value(cell, vstat)
    {
    	if(vstat == null || vstat == "")
        	return "";
        var rtn = "";
        var arr = new Array();
        for (var i = 0; i < tableData.length; i++)
        {
            var prop = cell.getAttribute("prop");//\u5f97\u5230column\u7684prop\u5c5e\u6027
            var value = eval("tableData[" + parseInt(i) + "]." + prop);
            arr.push(value);
        }
        switch (vstat.toUpperCase()) {
            case "MAX":
                arr.sort();
                rtn = arr[arr.length - 1];
                break;
            case "MIN":
                arr.sort();
                rtn = arr[0];
                break;
            case "AVG":
                var sum = 0;
                for (var i = 0; i < arr.length; i++)
                {
                    sum += arr[i];
                }
                rtn = sum / arr.length;
                break;
            case "SUM":
                var sum = 0;
                for (var i = 0; i < arr.length; i++)
                {
                    sum += arr[i];
                }
                rtn = sum;
                break;
            default:
                break;
        }
        return rtn;
    }
    ;

}
;

function column_ctrlex_Processor(rowcells, statusDesc, text)
{
    if (text == null) rowcells.innerHTML = "";
    else if (statusDesc != null && statusDesc != '')
    {
        var _key_value_arrs = statusDesc.split(';');

        for (var i = 0; i < _key_value_arrs.length; i ++)
        {
            var _key_value_maps = _key_value_arrs[i].split(':');
            if (_key_value_maps[0] != "")
            {
                if (new String(text) == _key_value_maps[0])
                {
                    if (_key_value_maps[2] != "")  innerText_function(rowcells, _key_value_maps[2]);
                    else innerText_function(rowcells, text);
                    if (_key_value_maps[1] != "")
                        rowcells.style.cssText = "background:" + _key_value_maps[1];
                }
            }
            else
            {
                //alert("ctrlex \u5c5e\u6027\u914d\u7f6e\u9519\u8bef!");
                alert($res_entry('ui.control.table.column.ctrlex',"ctrlex \u5c5e\u6027\u914d\u7f6e\u9519\u8bef!"));
            }
        }
    }
    else
    {
        innerText_function(rowcells, text);
    }
}
;

function innerText_function(DOM_ctrl, text)
{
    if (isIE()) DOM_ctrl.innerText = text;
    else DOM_ctrl.textContent = text;
}
;

var _primary_key = 0;
getKey = function()
{
    return _primary_key = _primary_key + 1;
};

create_new_EditTableRow = function(tableId, tb)//\u6ce8\uff1atableId\u5e76\u6ca1\u6709\u7528\u5230
{
    var objTable = tableModel_arr[tb].table;
    var Mtable = tableModel_arr[tb];
    var tdnodes = tableModel_arr[tb].cells;//\u6a21\u578b\u8868\u5217
    var objRow = objTable.insertRow(-1);
	
    var autoCtrId_arr = new Array();
    this.primaryKey = getKey() + "" + objRow.rowIndex;
    if (objRow.rowIndex % 2 == 0)
    {
        objRow.className = "even";
    }
	 //\u5faa\u73af\u6a21\u578b\u8868\u683c
    for (var j = 0; j < tdnodes.length; j++)
    {
        var contrlnode = '';
        var cell = objRow.insertCell(-1);
        
        //\u8bbe\u7f6eTD\u6837\u5f0f
        assignDataCSS(tdnodes[j], cell);

		//\u5f97\u5230column\u7684prop\u5c5e\u6027
        var prop = tableModel_arr[tb].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
        if (tableModel_arr[tb].cells[j].td_Children.length == 0)
        {
            if (prop == "$ROWNUM")
            {
                cell.innerHTML = objRow.rowIndex;
            }
        }
        else {//\u5305\u542b\u63a7\u4ef6
            var tmpTag, opt;
            //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
            for (var m = 0; m < tableModel_arr[tb].cells[j].td_Children.length; m++)
            {
                var ctrTag = tableModel_arr[tb].cells[j].td_Children[m].cloneNode(true);
				if (ctrTag.nodeType == 3) 
				{
					cell.appendChild(ctrTag.cloneNode(true));
					//row[i].cells[j].appendChild(contrlnode.cloneNode(true));
					continue;
				}
                //2008-09-21 \u7f16\u8f91\u8868\u683c\u7684\u4e0b\u62c9\u6846\u652f\u6301\u7528\u6237\u9ed8\u8ba4\u9009\u9879 begin
                if(ctrTag.tagName.toUpperCase() == "SELECT")
            	{
            		tmpTag = tableModel_arr[tb].cells[j].td_Children[m];
            		if(!isIE()) FF_children();
            		for(var sltindex = 0; sltindex < tmpTag.children.length; sltindex++)
            		{
            			opt = tmpTag.children[sltindex];
            			
            			if(opt.getAttribute('selected') || opt.getAttribute('selected')=="true")
            			{
            				ctrTag.selectedIndex = sltindex;
            				break;
            			}
            		}
            		tmpTag = opt = null;
            	}
                //2008-09-21 \u7f16\u8f91\u8868\u683c\u7684\u4e0b\u62c9\u6846\u652f\u6301\u7528\u6237\u9ed8\u8ba4\u9009\u9879 end
                var tagName = ctrTag.tagName.toUpperCase();
				//\u5982\u679c\u5b50\u7ed3\u70b9\u4e2d\u6709Select\u6807\u7b7e
                if (tagName == 'SELECT')
                {
                    //\u521b\u5efa\u4e0b\u62c9\u5217\u8868\u6846\u9009\u9879
                    var option = document.createElement('option');
                    option.value = '';
                    option.innerHTML = '';
					//\u52a0\u5165\u6807\u7b7e
                    cell.appendChild(ctrTag);
                }
                else if (tagName == "CALENDAR" || tagName == "Z:CALENDAR")//\u5982\u679c\u662f\u65e5\u5386\u6807\u7b7e
                {
                    if (ctrTag.getAttribute("default") && ctrTag.getAttribute("default") != '')
                    {
                        var defaultProp = ctrTag.getAttribute("default");
                        value = eval(defaultProp);
                    }
                    var calendarObj = Calendar_Ctr_Processor(ctrTag, value, objRow.rowIndex, _primary_key);
                    cell.appendChild(calendarObj);
                }
                else if (tagName == "Z:SSB_RIA_LOV" || tagName == "SSB_RIA_LOV")
                {
                    var value = null;
                    var new_ctrTag = table_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_ctrTag);
                    readRiaLovTag(new_ctrTag);
                }
                else if (tagName == 'Z:AUTOCOMPLETE' || tagName == 'AUTOCOMPLETE')
                {
                    var value = null;
                    var new_autoCtr = autoComplete_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_autoCtr);
                    autoCtrId_arr.push(new_autoCtr.getAttribute('id'));
                }
                else if (tagName == 'Z:RADIOGROUP' || tagName == 'RADIOGROUP')
                {
                    var value = null;
                    var new_radioCtr = radioGroup_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    var nid = new_radioCtr.getAttribute('id') + this.primaryKey;
                    new_radioCtr.setAttribute('id', nid);
                    cell.appendChild(ctrTag);
                    radios = new RadioGroupTag(new_radioCtr.getAttribute('id'));
                    radios.init();
                }
                else if (tagName == 'Z:UPLOAD' || tagName == 'UPLOAD')
                {
                    var newCtrTag = upload_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    var nid = newCtrTag.getAttribute('id') + this.primaryKey;
                    newCtrTag.setAttribute('id', nid);
                    cell.appendChild(newCtrTag);
                    var upload = UploadTag(newCtrTag.getAttribute('id'));
                    upload.init();
                }
                else if (tagName == "A")
                {
                	var value = null;
                    var new_ctrTag = table_Ctr_Processor(ctrTag, value, this.primaryKey, tb);
                    cell.appendChild(new_ctrTag);
                }
                else//\u5176\u4ed6\u6807\u7b7e\u76f4\u63a5\u63d2\u5165
                {
                    if (ctrTag.id != null) ctrTag.id = ctrTag.id + this.primaryKey;
                    cell.appendChild(ctrTag);

					if (ctrTag.type && ctrTag.type.toUpperCase() == "CHECKBOX"){
						var ack = document.getElementById(tb + 'allCheck');
						if(ack != null){
							var ack = ack.parentNode;
							while (ack.tagName.toLowerCase() != 'td')	    {
						        ack = ack.parentNode;
						    }
							
							var td = ctrTag.parentNode;
							while (td.tagName.toLowerCase() != 'td')	    {
						        td = td.parentNode;
						    }
							
							if(ack.cellIndex == td.cellIndex)
								addEventForCheckBox(ctrTag, tb);
								
							if (ack && ack.checked == true)                 {
		                        ctrTag.checked = true;
		                    }
						}
					}
                }

            }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
        }
    }//\u5217\u5faa\u73af
    for (var i = 0; i < autoCtrId_arr.length; i ++)
    {
        autos = new AutoCompleteTag(autoCtrId_arr[i]);
        autos.init();
    }

	$(objRow).hover(
		function(){
			$(this).find("td").find("input").andSelf().addClass("trHover");
		}, 
		function(){
			$(this).find("td").find("input").andSelf().removeClass("trHover");
	});
	
	if(tableModel_arr[tb].rownumforscroll != "noscroll" && objRow.rowIndex > tableModel_arr[tb].rownumforscroll)
		$("#"+tableId.replace("_ssb","")+"_scDiv").addClass("scrollContainer");
	
	//\u53d6\u6d88\u8d85\u94fe\u63a5\u7684\u9ed8\u8ba4\u884c\u4e3a
	$(objRow).find("a[onclick]").each(function(){		
		var achor = this;
		EventManager.Add(achor, "click", function(event){	
			if(!isIE())
				event.preventDefault();
			else
				window.event.returnValue=false;
		});	
	});
    return objRow;
};

showPages = function(name)
{ //\u521d\u59cb\u5316\u5c5e\u6027
    this.name = name;      //\u5bf9\u8c61\u540d\u79f0
    this.page = 1;         //\u5f53\u524d\u9875\u6570
    this.pageCount = 0;    //\u603b\u9875\u6570
    this.argName = 'page'; //\u53c2\u6570\u540d
    this.showTimes = 1;    //\u6253\u5370\u6b21\u6570
    this.perPageCount = 10;	//\u6bcf\u9875\u663e\u793a\u6761\u6570
    this.perCount = 10;	//\u663e\u793a\u51fa\u7684\u8df3\u8f6c\u9875\u7801\u6570,\u5982[1][2][3][4][5][6][7][8][9][10]
    this.totalrow = 0;
    this.turnTo;
    this.tid;

    this.text1 = $res_entry('ui.control.table.001', '\u7b2c');
    this.text2 = $res_entry('ui.control.table.002', '\u9875');
    this.text3 = $res_entry('ui.control.table.003', '\u5171');
    this.text4 = $res_entry('ui.control.table.004', '\u9875');
    this.text5 = $res_entry('ui.control.table.005', '\u5171');
    this.text6 = $res_entry('ui.control.table.006', '\u6761\u8bb0\u5f55');
    this.text7 = $res_entry('ui.control.table.007', '\u6bcf\u9875');
    this.text8 = $res_entry('ui.control.table.008', '\u884c');
    //add
    this.text9 = $res_entry('ui.control.table.009', '\u6761\u8bb0\u5f55');
    this.text10 = $res_entry('ui.control.table.010', '\u6bcf\u9875');
    this.text11 = $res_entry('ui.control.table.011', '\u5f53\u524d\u9875');
        //this.pageSizeList = new Array();
};

showPages.prototype.getPage = function()
{ //\u4e1burl\u83b7\u5f97\u5f53\u524d\u9875\u6570,\u5982\u679c\u53d8\u91cf\u91cd\u590d\u53ea\u83b7\u53d6\u6700\u540e\u4e00\u4e2a
    this.page = this.turnTo;
};

showPages.prototype.checkPages = function()
{ //\u8fdb\u884c\u5f53\u524d\u9875\u6570\u548c\u603b\u9875\u6570\u7684\u9a8c\u8bc1
    if (isNaN(parseInt(this.page))) this.page = 1;
    if (isNaN(parseInt(this.pageCount))) this.pageCount = 1;
    if (this.page < 1) this.page = 1;
    if (this.pageCount < 1) this.pageCount = 0;
    if (this.page > this.pageCount) this.page = this.pageCount;
    this.page = parseInt(this.page);
    this.pageCount = parseInt(this.pageCount);

};

showPages.prototype.createDefaultNavBar = function(){
	var strHtml = '';
    var prevPage = this.page - 1;//\u524d\u4e00\u9875
    var nextPage = this.page + 1;// \u4e0b\u4e00\u9875
    var isdisabled = "";
    if (this.totalrow == 0)
    {
        this.page = 0;
        isdisabled = "disabled"
    }

    var ml = this.getMultilanguage_resource();
	strHtml += ml[0] + '<span>' + this.page + '</span>' + ml[1] + '<span>/</span> ' + ml[2] + '<span id="ssb_ria_table_pageCount">' + this.pageCount + '</span>' + ml[3];
    strHtml += '&nbsp;&nbsp;&nbsp;' + ml[4] + '<span id="ssb_ria_table_totalrow">' + this.totalrow + '</span>' + ml[5] + '&#160; ';
    strHtml += ml[6];
    strHtml += '<select name="perPageCount" onchange="tableModel_arr.' + this.tid + '.' + this.name + '.initPre(this);"' + isdisabled + '>';
    for (var i = 0; i < tableModel_arr[this.tid].pageSizeList.length; i++)
    {
        //\u5224\u65ad\u5f53\u524d\u6bcf\u9875\u591a\u5c11\u884c
        var chkSelect;
        if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pageSizeList[i])
        {
            chkSelect = ' selected="selected"';
        }
            //\u5224\u65ad\u662f\u5426\u662f\u663e\u793a\u5168\u90e8
        else if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pg.totalrow && isNaN(tableModel_arr[this.tid].pageSizeList[i]))
        {
            chkSelect = ' selected="selected"';
        }
        else {
            chkSelect = '';
        }
        strHtml += '<option value="' + tableModel_arr[this.tid].pageSizeList[i] + '"' + chkSelect + '>' + tableModel_arr[this.tid].pageSizeList[i] + '</option>';
    }
    strHtml += '</select> ' + ml[7] + '&#160;&nbsp;&nbsp;';
    if (prevPage < 1) {
        strHtml += '<span ><input class="pageNav firstPageD" disabled type=button></span>';
        strHtml += '<span ><input class="pageNav prevPageD" disabled type=button></span>';

    } else {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.first","\u7b2c\u4e00\u9875")+'"><input class="pageNav firstPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(1);"></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.prev","\u4e0a\u4e00\u9875")+'"><input class="pageNav prevPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + prevPage + ');"></span>';
    }
          
          //\u5224\u65ad\u662f\u5426\u663e\u793a\u6587\u672c\u6846\u8df3\u8f6c
    if (!tableModel_arr[this.tid].isList)
    {
        if (this.page != 0)
        {
            strHtml += '<input id="'+ this.tid+'_ssbInputToPage" class="toPage" value="' + this.page + '"/>&nbsp;';
            strHtml += '<input type="button" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
        else
        {
            strHtml += '<input id="'+ this.tid+'_ssbInputToPage" class="toPage" disabled="disabled" value="' + this.page + '"/>&nbsp;';
            strHtml += '<input type="button"  disabled="disabled" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
    }
        //\u5426\u5219\u4f7f\u7528\u4e0b\u62c9\u6846
    else
    {
        if (this.pageCount < 1) {
            strHtml += '<select name="toPage" disabled="disabled">';
            strHtml += '<option value="0">0</option>';
        } else {
            var chkSelect;
            strHtml += '<select name="toPage" onchange="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this);">';
            for (var i = 1; i <= this.pageCount; i++) {
                if (this.page == i) chkSelect = ' selected="selected"';
                else chkSelect = '';
                strHtml += '<option value="' + i + '"' + chkSelect + '>' + i + '</option>';
            }
        }
        strHtml += '</select>';
    }

    if (nextPage > this.pageCount) {
        strHtml += '<span ><input class="pageNav nextPageD" disabled type=button></span>';
        strHtml += '<span ><input class="pageNav lastPageD" disabled type=button></span>';
    } else {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.next","\u4e0b\u4e00\u9875")+'"><input class="pageNav nextPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + nextPage + ');"></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.last","\u672b\u5c3e\u9875")+'"><input class="pageNav lastPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + this.pageCount + ');"></span>';
    }

    if (tableModel_arr[this.tid].isExcel == 'true')
    {
        if (tableModel_arr[this.tid].isExcelConfig == 'true')
        {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:configExcel(' + this.tid + ')"></span>';
        } else {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:ExportExcel(' + this.tid + ')"></span>';
        }
    }
    strHtml += '';
    return strHtml;
}

showPages.prototype.createVPBXBar = function() {
	var strHtml = '';
    var prevPage = this.page - 1;
    var nextPage = this.page + 1;
    var isdisabled = "";
    if (this.totalrow == 0)
    {
        this.page = 0;
        isdisabled = "disabled"
    }

    var ml = this.getMultilanguage_resource();
	strHtml += '&nbsp;&nbsp;&nbsp;' + '' + '<span id="ssb_ria_table_totalrow">' + this.totalrow + '</span>' + '&nbsp;' + ml[8] + '&nbsp;' + ml[10] + '&nbsp;';
    strHtml += ml[0] + '<span>' + this.page + '</span>' + ml[1] + '<span>/</span>' + ml[2] + '<span id="ssb_ria_table_pageCount">' + this.pageCount + '</span>' + '&nbsp;&nbsp;';
    if (prevPage < 1) {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.first","\u7b2c\u4e00\u9875")+'"><input class="pageNav firstPageD" disabled type=button></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.prev","\u4e0a\u4e00\u9875")+'"><input class="pageNav prevPageD" disabled type=button></span>';

    } else {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.first","\u7b2c\u4e00\u9875")+'"><input class="pageNav firstPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(1);"></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.prev","\u4e0a\u4e00\u9875")+'"><input class="pageNav prevPage" type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + prevPage + ');"></span>';
    }
    if (!tableModel_arr[this.tid].isList)
    {
        if (this.page != 0)
        {
            if (isIE())
                strHtml += '<input id="toPage" class="toPage" value="' + this.page + '" onkeyup="javascript:if ( this.value.charAt(0) == \'0\') this.value=\'\'" onkeypress="javascript:if(event.keyCode<48||event.keyCode>57)event.returnValue=false;">';
            else
                strHtml += '<input id="toPage" class="toPage" value="' + this.page + '" onkeyup="javascript:if ( this.value.charAt(0) == \'0\') this.value=\'\'" onkeypress="javascript:if(event.which<48&&event.which!=8||event.which>57) {event.preventDefault();}">';

            strHtml += '&nbsp;';
            strHtml += '<input type="button" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
        else
        {
            if (isIE())
                strHtml += '<input id="toPage" class="toPage" disabled="true" value="' + this.page + '" onkeyup="javascript:if ( this.value.charAt(0) == \'0\') this.value=\'\'" onkeypress="javascript:if(event.keyCode<48||event.keyCode>57)event.returnValue=false;">';
            else
                strHtml += '<input id="toPage" class="toPage" disabled="true" value="' + this.page + '" onkeyup="javascript:if ( this.value.charAt(0) == \'0\') this.value=\'\'" onkeypress="javascript:if(event.which<48&&event.which!=8||event.which>57) {event.preventDefault();}">';

            strHtml += '&nbsp;';
            strHtml += '<input type="button"  disabled="true" value="Go" id="showPage" class="showPage" onclick="tableModel_arr.' + this.tid + '.' + this.name + '.toPage(this.previousSibling.previousSibling.value);">';
        }
    }
    if (nextPage > this.pageCount) {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.next","\u4e0b\u4e00\u9875")+'"><input class="pageNav nextPageD" disabled type=button></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.last","\u672b\u5c3e\u9875")+'"><input class="pageNav lastPageD" disabled type=button></span>';
    } else {
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.next","\u4e0b\u4e00\u9875")+'"><input class="pageNav nextPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + nextPage + ');"></span>';
        strHtml += '<span title="'+$res_entry("ui.control.table.nav.last","\u672b\u5c3e\u9875")+'"><input class="pageNav lastPage"  type=button onclick="javascript:tableModel_arr.' + this.tid + '.' + this.name + '.toPage(' + this.pageCount + ');"></span>';
    }
	
    strHtml += ml[6];
	
    strHtml += '<select name="perPageCount" onchange="tableModel_arr.' + this.tid + '.' + this.name + '.initPre(this);"' + isdisabled + '>';
   	
	for (var i = 0; i < tableModel_arr[this.tid].pageSizeList.length; i++)
    {        
        var chkSelect;
        if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pageSizeList[i])
        {
            chkSelect = ' selected="selected"';
        }
            
        else if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pg.totalrow && isNaN(tableModel_arr[this.tid].pageSizeList[i]))
        {
            chkSelect = ' selected="selected"';
        }
        else {
            chkSelect = '';
        }
        strHtml += '<option value="' + tableModel_arr[this.tid].pageSizeList[i] + '"' + chkSelect + '>' + tableModel_arr[this.tid].pageSizeList[i] + '</option>';
    }
    strHtml += '</select> ' + ml[7] + '&#160;';
    

    if (tableModel_arr[this.tid].isExcel == 'true')
    {
        if (tableModel_arr[this.tid].isExcelConfig == 'true')
        {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:configExcel(' + this.tid + ')"></span>';
        } else {
            strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:ExportExcel(' + this.tid + ')"></span>';
        }
    }
    strHtml += ml[9];
    return strHtml;
}

showPages.prototype.createHtml = function(mode)
{ //\u751f\u6210html\u4ee3\u7801
	var naviBarStyle = $res_entry("ui.control.table.navbar.style","");
    if( naviBarStyle == "vpbx" ) {
		return this.createVPBXBar();
	} else {
		return this.createDefaultNavBar();
	}
};

showPages.prototype.createUrl = function (page)
{
    //\u751f\u6210\u9875\u9762\u8df3\u8f6curl
    if (isNaN(parseInt(page))) page = 1;
    if (page < 1) page = 1;
    if (page > this.pageCount) page = this.pageCount;
    var url = location.protocol + '//' + location.host + location.pathname;
    var args = location.search;
    var reg = new RegExp('([\?&]?)' + this.argName + '=[^&]*[&$]?', 'gi');
    args = args.replace(reg, '$1');
    if (args == '' || args == null) {
        args += '?' + this.argName + '=' + page;
    } else if (args.substr(args.length - 1, 1) == '?' || args.substr(args.length - 1, 1) == '&') {
        args += this.argName + '=' + page;
    } else {
        args += '&' + this.argName + '=' + page;
    }
    return url + args;
};

showPages.prototype.toPage = function(page)
{
	//\u5224\u65ad\u7528\u6237\u662f\u5426\u5b9a\u4e49\u4e86\u8df3\u8f6c\u524d\u8981\u6267\u884c\u7684\u4e8b\u4ef6 saveBeforeTurnpage()
	if(typeof(saveBeforeTurnpage) != "undefined" && saveBeforeTurnpage.constructor == Function)
		saveBeforeTurnpage(this.tid);
		
    //\u9875\u9762\u8df3\u8f6c
    this.turnTo = 1;
   
    //if(isNaN(page)) return ;

    if (typeof(page) == 'object')
    {
        this.turnTo = page.options[page.selectedIndex].value;
    }
    else if (this.pageCount < page)
        this.turnTo = this.pageCount;
    else if (page <= 0)
        this.turnTo = 1;
    else
        this.turnTo = page;
	//\u7ffb\u9875
	if(tableModel_arr[this.tid].sidParamCount == 4 ) {		
		getFourParamTurnpageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount);
	}
	else if (tableModel_arr[this.tid].isserversort == true){
		getSortTurnPageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount);
	}
	else {
		getTurnPageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount);
	}
};

showPages.prototype.printHtml = function(mode)
{
    //\u663e\u793ahtml\u4ee3\u7801
    this.getPageCount();//\u8ba1\u7b97\u603b\u9875\u6570
    this.getPage();//\u83b7\u53d6\u5f53\u524d\u9875\u7801
    this.checkPages();//\u68c0\u6d4b\u9875\u7801\u5408\u6cd5\u6027
    this.showTimes += 1;
       //\u9875\u7801\u9009\u62e9\u6846      
    //\u83b7\u53d6\u5206\u9875\u680f\u7684table\u5bf9\u8c61
    var dir_tr_td = document.getElementById(this.tid + '_pages');
    
    //\u89e3\u51b3\u5206\u9875\u680f\u5e03\u5c40\u95ee\u9898
    dir_tr_td.style.textAlign = $res_entry("ui.control.table.navbar.layout","right");
     //\u521b\u5efa\u5206\u9875
    dir_tr_td.innerHTML = this.createHtml();
	
	var inputEvent = function(e){
		var evt = e || window.event;  
    	
		if(evt && evt.preventDefault){
			if((evt.which<48&&evt.which!=8&&evt.which!=0 || evt.which>57)) evt.preventDefault();
			}
		else {
			if((evt.keyCode<48 || evt.keyCode>57))   evt.returnValue=false;
		}
		if((evt.target || evt.srcElement).value.charAt(0) == "0")
			(evt.target || evt.srcElement).value = "";
	};
	
	var ssbinput = document.getElementById(this.tid+"_ssbInputToPage");
    
    EventManager.Add(ssbinput, "keyup", inputEvent);
    EventManager.Add(ssbinput, "keypress", inputEvent);
    EventManager.Add(ssbinput, "paste", function(){return false;});
};

showPages.prototype.formatInputPage = function(e)
{ //\u9650\u5b9a\u8f93\u5165\u9875\u6570\u683c\u5f0f
    var ie = navigator.appName == "Microsoft Internet Explorer" ? true : false;
    if (!ie) var key = e.which;
    else var key = event.keyCode;
    if (key == 8 || key == 46 || (key >= 48 && key <= 57)) return true;
    return false;
};

showPages.prototype.initPre = function(e)
{ 
	//\u5224\u65ad\u7528\u6237\u662f\u5426\u5b9a\u4e49\u4e86\u8df3\u8f6c\u524d\u8981\u6267\u884c\u7684\u4e8b\u4ef6 saveBeforeTurnpage()
	if(typeof(saveBeforeTurnpage) != "undefined" && saveBeforeTurnpage.constructor == Function)
		saveBeforeTurnpage(this.tid);
	

	//\u8bbe\u5b9a\u6bcf\u9875\u663e\u793a\u884c\u6570     
    if (e != null)
    {
        if (e.value == "\u5168\u90e8" || e.value.toLowerCase() == "all")
            this.perPageCount = this.totalrow;
        else
            this.perPageCount = e.value;
    }
    else {
        this.perPageCount = 10; //\u6ca1\u9875\u591a\u5c11\u884c
    }


    tableModel_arr[this.tid].pg.perPageCount = this.perPageCount;

    this.getPageCount();
     //\u5982\u679c\u662f\u521d\u59cb\u8868\u683c, \u4e0d\u663e\u793a\u6570\u636e
    if (this.totalrow != 0)
    {
        //\u91cd\u7f6e\u6bcf\u9875\u663e\u793a\u6570\u636e\u6570\u540e,\u91cd\u65b0\u67e5\u8be2\u6570\u636e
        if(tableModel_arr[this.tid].sidParamCount == 4 ) {
			getFourParamTurnpageData(this.tid, tableModel_arr[this.tid].condition, '1', this.perPageCount);
		}
		else {
	        getTurnPageData(this.tid, tableModel_arr[this.tid].condition, '1', this.perPageCount);
		}
	     // \u586b\u6570\u636e
        // setTableValue(tableModel_arr[this.tid].MtableId, pageinfo);
    }

};
showPages.prototype.getMultilanguage_resource = function()
{
    var res_entity = new Array();
    var text1 = this.text1.indexOf("$") == -1 ? this.text1 : "\u7b2c";
    var text2 = this.text2.indexOf("$") == -1 ? this.text2 : "\u9875";
    var text3 = this.text3.indexOf("$") == -1 ? this.text3 : "\u5171";
    var text4 = this.text4.indexOf("$") == -1 ? this.text4 : "\u9875";
    var text5 = this.text5.indexOf("$") == -1 ? this.text5 : "\u5171";
    var text6 = this.text6.indexOf("$") == -1 ? this.text6 : "\u6761\u8bb0\u5f55";
    var text7 = this.text7.indexOf("$") == -1 ? this.text7 : "\u6bcf\u9875";
    var text8 = this.text8.indexOf("$") == -1 ? this.text8 : "\u884c";
    //add
    var text9 = this.text9.indexOf("$") == -1 ? this.text9 : "\u6761\u8bb0\u5f55";
    var text10 = this.text10.indexOf("$") == -1 ? this.text10 : "\u6bcf\u9875";
    var text11 = this.text11.indexOf("$") == -1 ? this.text11 : "\u5f53\u524d\u9875";

    res_entity.push(text1);
    res_entity.push(text2);
    res_entity.push(text3);
    res_entity.push(text4);
    res_entity.push(text5);
    res_entity.push(text6);
    res_entity.push(text7);
    res_entity.push(text8);
	//add
	res_entity.push(text9);
	res_entity.push(text10);
	res_entity.push(text11);
	
    return res_entity;
};

showPages.prototype.getPageCount = function()//\u8ba1\u7b97\u9875\u7801
{
    this.pageCount = 0;
     //\u5982\u679c\u65e0\u8bb0\u5f55,\u76f4\u63a5\u8fd4\u56de0
    if (this.totalrow == 0)
    {
        return this.pageCount;
    }
    else {
        if (this.totalrow % this.perPageCount == 0)
        {
            this.pageCount = parseInt(this.totalrow / this.perPageCount);
        }
        else {
            this.pageCount = parseInt(this.totalrow / this.perPageCount + 1);
        }
    }
};

LTrim = function(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1)
    {
        var j = 0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
        {
            j++;
        }
        s = s.substring(j, i);
    }
    return s;
};
RTrim = function(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length - 1)) != -1)
    {
        var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
        {
            i--;
        }
        s = s.substring(0, i + 1);
    }
    return s;
};
Trim = function(str)
{
    return RTrim(LTrim(str));
};

isIE = function()
{
    browser_name = navigator.appName;
    if (browser_name == "Microsoft Internet Explorer")
    {
        return true;
    }
    return false;
};

var table_Header_Resize = new Object();

var TableContants = {
    EC_ID : "zteTable_ssb",
    MIN_COL_WIDTH : 10,
    MIN_COLWIDTH : "30",
    DRAG_BUTTON_COLOR : "#3366ff",
    IE_WIDTH_FIX_A : 1,
    IE_WIDTH_FIX_B : 2,
    FF_WIDTH_FIX_A : -3,
    FF_WIDTH_FIX_B : -6

};

table_Header_Resize.onDragobj = false;
table_Header_Resize.Dragobj = null;
table_Header_Resize.DragobjSibling = null;
table_Header_Resize.MinColWidth = TableContants.MIN_COLWIDTH;

table_Header_Resize.DragobjBodyCell = null;
table_Header_Resize.DragobjBodyCellSibling = null;
table_Header_Resize.DragFormid = null;

table_Header_Resize.StartResize = function(event, obj, formid)
{
    var table = document.getElementById(formid);
    var e = event || window.event;
    if (!formid)
    {
        formid = TableContants.EC_ID;
    }

    obj.focus();
    document.body.style.cursor = E_RESIZE;
    var sibling = table_Header_Resize.getNextElement(obj.parentNode);

    var dx = e.screenX;

    obj.parentTdW = obj.parentNode.clientWidth;
    if (sibling == null)
    {
        return true;
        document.body.style.cursor = "";
    }
    obj.siblingW = sibling.clientWidth;
    obj.mouseDownX = dx;
    obj.totalWidth = obj.siblingW + obj.parentTdW;

    obj.oldSiblingRight = table_Header_Resize.getPosRight(sibling);

    table_Header_Resize.Dragobj = obj;
    table_Header_Resize.DragobjSibling = sibling;
    table_Header_Resize.onDragobj = true;

    table_Header_Resize.MinColWidth = TableContants.MIN_COL_WIDTH;

    if (!table_Header_Resize.MinColWidth || table_Header_Resize.MinColWidth == '' || table_Header_Resize.MinColWidth < 1)
    {
        table_Header_Resize.MinColWidth = TableContants.MIN_COLWIDTH;
    }
    table_Header_Resize.Dragobj.style.backgroundColor = TableContants.DRAG_BUTTON_COLOR;

    table_Header_Resize.Dragobj.parentTdW -= table_Header_Resize.Dragobj.mouseDownX;

    var cellIndex = table_Header_Resize.Dragobj.parentNode.cellIndex;

    try {
        table_Header_Resize.DragobjBodyCell = table.rows[0].cells[cellIndex];
        table_Header_Resize.DragobjBodyCellSibling = table_Header_Resize.getNextElement(table_Header_Resize.DragobjBodyCell);
    }
    catch(e) {
        table_Header_Resize.DragobjBodyCell = null;
    }

};

table_Header_Resize.DoResize = function(event)
{
    var e = event || window.event;

    if (table_Header_Resize.Dragobj == null)
    {
        return true;
    }
    if (!table_Header_Resize.Dragobj.mouseDownX)
    {
        return false;
    }
    document.body.style.cursor = E_RESIZE;

    var dx = e.screenX;

    var newWidth = table_Header_Resize.Dragobj.parentTdW + dx;

    var newSiblingWidth = 0;

    /* fix different from ie to ff . but I don't know why  */
    if (isIE())
    {
        newWidth = newWidth + TableContants.IE_WIDTH_FIX_A;
        newSiblingWidth = table_Header_Resize.Dragobj.totalWidth - newWidth + TableContants.IE_WIDTH_FIX_B;
    }
    else {
        newWidth = newWidth + TableContants.FF_WIDTH_FIX_A;
        newSiblingWidth = table_Header_Resize.Dragobj.totalWidth - newWidth + TableContants.FF_WIDTH_FIX_B;
    }
    if (newWidth > table_Header_Resize.MinColWidth && newSiblingWidth > table_Header_Resize.MinColWidth)
    {
        table_Header_Resize.Dragobj.parentNode.style.width = newWidth + "px";
        table_Header_Resize.DragobjSibling.style.width = newSiblingWidth + "px";
        try {
            table_Header_Resize.DragobjBodyCell.style.width = newWidth + "px";
            table_Header_Resize.DragobjBodyCellSibling.style.width = newSiblingWidth + "px";
            table_Header_Resize.DragobjBodyCell.width = newWidth + "px";
            table_Header_Resize.DragobjBodyCellSibling.width = newSiblingWidth + "px";
        } catch(e) {
        }
    }
    table_Header_Resize.onDragobj = true;

};

table_Header_Resize.EndResize = function(event)
{
    if (table_Header_Resize.Dragobj == null)
    {
        return false;
    }
    table_Header_Resize.Dragobj.mouseDownX = 0;
    document.body.style.cursor = "";
    table_Header_Resize.Dragobj.style.backgroundColor = "";
    table_Header_Resize.Dragobj = null;
    table_Header_Resize.DragobjSibling = null;

};

table_Header_Resize.resizeInit = function()
{
    document.onmousemove = table_Header_Resize.DoResize;
    document.onmouseup = table_Header_Resize.EndResize;
    document.body.ondrag = function()
    {
        return false;
    };
    document.body.onselectstart = function()
    {
        return table_Header_Resize.Dragobj == null;
    };

};

table_Header_Resize.getNextElement = function(node)
{
    if (!node)
    {
        return null;
    }
    var tnode = node.nextSibling;
    while (tnode != null)
    {
        if (tnode.nodeType == 1)
        {
            return tnode;
        }
        tnode = tnode.nextSibling;
    }
    return null;
};

table_Header_Resize.getPosLeft = function(elm)
{
    var left = elm.offsetLeft;
    while ((elm = elm.offsetParent) != null)
    {
        left += elm.offsetLeft;
    }
    return left;
};

table_Header_Resize.getPosRight = function(elm)
{
    return table_Header_Resize.getPosLeft(elm) + elm.offsetWidth;
};


convert = function(sValue, datatype)
{
    var re = /[^0-9.+-]/g;
    value = (sValue.nodeName == "#text") ? sValue.nodeValue : (sValue.value == null ? sValue.firstChild.data : sValue.value);
    if (value)
    {
        datatype = (datatype == null) ? "" : datatype;
        switch (datatype.toLowerCase())
                {
            case "int":
                rvalue = value.replace(re, '');
                return parseInt(rvalue);
            case "float":
                return parseFloat(value);
            default:
                return value.toString();
        }
    }

};


generateCompare = function(iCol, datatype)
{
    return function compareTRs(oTR1, oTR2)
    {
        var value1 = "";
        var value2 = "";
        if (oTR1.cells[iCol].firstChild != null)
        {
            value1 = convert(oTR1.cells[iCol].firstChild, datatype);
        }
        if (oTR2.cells[iCol].firstChild != null)
        {
            value2 = convert(oTR2.cells[iCol].firstChild, datatype);
        }
        if (value1 < value2)
        {
            return -1;
        }
        else if (value1 > value2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    };
};

var sortColumn_table = "";
var isAsc_table = {};
sortTable = function(sTableId, iCol, imgEl)
{
   if (typeof sTableId == 'object')    {
        var oTable = sTableId;
        var tableId = sTableId.id;
    }
    else {
        var oTable = document.getElementById(sTableId);
        var tableId = sTableId;
    }
    
    //\u9488\u5bf9\u540e\u53f0\u6392\u5e8f\u53d6\u6b63\u786e\u5217\u7684\u65b9\u5f0f
   	var tableId = tableId.substring(0, tableId.indexOf("_ssb"));
	
	var title = $(imgEl.parentNode).text();
	for(var i=0; i<tableModel_arr[tableId].clums.length; i++) {
		if ($(tableModel_arr[tableId].clums[i]).attr("caption") == title) {
			iCol = i;
		}
	}
	
    var oTBody = oTable.tBodies[0];
    var oRows = oTBody.rows;
    if (oRows.length == 1) return;
    var aTRs = new Array();
	
	var sortASC = null;
    var sortDESC = null;
    if (isIE())
    {
        sortASC = "IEsortASC";
        sortDESC = "IEsortDESC";
    } else {
        sortASC = "FFsortASC";
        sortDESC = "FFsortDESC";
    }
    var sortflag = "";
    
   	$(tableModel_arr[tableId].table.rows[0]).find("div[id^='ssbSortImg']").each(function(){
   		if(this.id == imgEl.id)
   		{
   			if(this.className.toLowerCase().indexOf("asc") != -1) 
   				sortflag = "up";
   			else
   				sortflag = "down";
   		}
   		else
   			this.className = sortASC;
    });
	//\u4e0d\u7ba1\u524d\u53f0\u8fd8\u662f\u540e\u53f0\u6392\u5e8f, \u4e0a\u9762\u7684\u4ee3\u7801\u90fd\u9700\u8981\u6267\u884c
	
	//\u5224\u65ad\u662f\u5426\u540e\u53f0\u6392\u5e8f
	if(tableModel_arr[tableId].isserversort == true)
	{
		//\u4e0d\u80fd\u4f7f\u7528excel_props\uff0c\u5b83\u6709\u88ab\u6539\u53d8\u7684\u53ef\u80fd\uff0c\u5e94\u5f53\u7528clums,\u5b83\u4e0d\u53ef\u80fd\u88ab\u6539\u53d8
		//sortColumn_table = tableModel_arr[tableId].excel_props[iCol];
		
		sortColumn_table = tableModel_arr[tableId].clums[iCol].getAttribute("prop");

		if(sortColumn_table != "")
			isAsc_table[sortColumn_table] = true;
		
		if (sortflag == 'down')
	    {
	        isAsc_table[sortColumn_table] = false;
            imgEl.className = sortASC;
	    }
	    else 
		{
			isAsc_table[sortColumn_table] = true;
            imgEl.className = sortDESC;
	    }
		//\u91cd\u65b0\u4ece\u540e\u53f0\u67e5\u8be2\u6570\u636e, \u5982\u679c\u7528\u6237\u8bbe\u7f6e\u4e86\u7edf\u8ba1\u884c\u7684\u8bdd, \u5b83\u4e5f\u4f1a\u81ea\u52a8\u66f4\u65b0
		getSortTurnPageData(tableId, tableModel_arr[tableId].condition, tableModel_arr[tableId].pg.page, tableModel_arr[tableId].pg.perPageCount);
		
		return;
	}
	
	//\u524d\u53f0\u6392\u5e8f	
	var isIE6 = (window.navigator.userAgent.indexOf("MSIE 6.0") != -1)? true : false;
	if(isIE6) {
		//\u9488\u5bf9ie6\u590d\u9009\u6846\u72b6\u6001\u4e0d\u80fd\u4fdd\u5b58\u7684\u7279\u6b8a\u5904\u7406
		var ckdIdArr = new Array();
		$(tableModel_arr[tableId].table).find(":checked").each(function(){
			ckdIdArr.push(this.id);
		});	
	}
	
    var util = tableModel_arr[tableId].vstatcaption;
    for (var i = 0; i < oRows.length; i++)
    {
        aTRs.push(oRows[i]);
    }
	
	//2008-09-02 wxr\uff1a\u5982\u679c\u4f7f\u7528\u4e86\u5408\u8ba1\uff0c\u5219\u5e94\u5148\u5c06\u6700\u540e\u4e00\u884c\u53d6\u51fa\u6765
	if(util)	aTRs.pop();	

	//\u9488\u5bf9\u524d\u53f0\u6392\u5e8f\u53d6\u9009\u5b9a\u5217\u7684\u65b9\u5f0f
	var tmp = imgEl;
	while(true){
		if(tmp.tagName == "TH") break;
		tmp = tmp.parentNode;
	}
	var iCol=0;
	while(tmp.previousSibling && tmp.previousSibling.tagName == "TH"){
		iCol++;
		tmp = tmp.previousSibling;
	}
	
	//\u4e25\u683c\u68c0\u67e5sortCol\uff0c\u53ea\u6709\u5f53\u5b83\u662f\u6570\u636e\u65f6\u624d\u6709\u6548
    if (oTable.sortCol.constructor == Number && oTable.sortCol == iCol)
    {
        aTRs.reverse();
        if (sortflag == 'down')
        {
            imgEl.className = sortASC;
        }
        else {
            imgEl.className = sortDESC;
        }

    }
    else {
        aTRs.sort(generateCompare(iCol, tableModel_arr[tableId].cells[iCol].datatype));
        oTable.sortflag = 'down';
        imgEl.className = sortDESC;
    }

    var oFragment = document.createDocumentFragment();
    oFragment.appendChild(oRows[0]);
	
	
    for (var i = 0; i < aTRs.length; i++)
    {
        setRowCss(aTRs[i], ROW_CLASS_EVEN, i);
        oFragment.appendChild(aTRs[i]);
    }

	//2008-09-02 wxr\uff1a\u5982\u679c\u4f7f\u7528\u4e86\u5408\u8ba1\uff0c\u5219\u5e94\u5728\u5c06\u628a\u5408\u8ba1\u884c\u653e\u5230\u6700\u540e
	if(util)	oFragment.appendChild(oRows[oRows.length-1]);	
	
    oTBody.appendChild(oFragment);
	
	//\u9488\u5bf9ie6\u590d\u9009\u6846\u72b6\u6001\u4e0d\u80fd\u4fdd\u5b58\u7684\u7279\u6b8a\u5904\u7406
	if(isIE6) {		
		for(var i=0; i<ckdIdArr.length; i++) {		
			$(tableModel_arr[tableId].table).find(":checkbox[id^='"+ ckdIdArr[i] +"']")[0].checked = "checked";			
		}		
	}
	
    oTable.sortCol = iCol;
};

SSB_Table_Init = function()
{
    //\u83b7\u53d6\u9875\u9762\u6240\u6709\u7684table\u6807\u7b7e
    if (isIE())
    {
        var IEtables = document.getElementsByTagName("table");
        for (var i = 0; i < IEtables.length; i ++)
        {
            //\u5982\u679ctable\u7684\u547d\u540d\u7a7a\u95f4\u662fz
            if (IEtables[i].scopeName.toUpperCase() == RIA_SCOPENAME)
            {
                var IEid = IEtables[i].id;   	  			
                if (IEid != null)
                {
                    init_ssb_table(IEid);
                }
            }
        }
    }
    else {//\u5728FF\u4e0b
        var FFtables = document.getElementsByTagName("z:table");
        for (var j = 0; j < FFtables.length; j ++)
        {
            var FFid = FFtables[j].getAttribute("id");
            if (FFid != null)
            {
                init_ssb_table(FFid);
            }
        }
    }

};
var evalParse = function ()
{
    var str = eval("arguments[0][arguments[1]]");
    if (str == undefined) str = "";
    return str;
};
   //\u8c03\u7528\u6846\u67b6\u521d\u59cb\u65b9\u6cd5
addOnloadEvent(SSB_Table_Init);

var cap = new Array();//excel\u6587\u4ef6\u5217\u6807\u9898
var pop = new Array();//\u5b9e\u4f53\u5c5e\u6027
var setCaptions = function(captions)
{
    cap = captions;
};
var setProps = function(props)
{
    pop = props;
};
var getCaptions = function(tid)
{
    var captions = tableModel_arr[tid].excel_title;
    return captions;
};
var getProps = function(tid)
{
    //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
    var props = tableModel_arr[tid].excel_props;
    return props;
};

var configExcel = function(tid)
{
	//\u53d6\u5f97z\uff1atable\u6807\u8bc6
    if (typeof tid == "object")	{
		tid = tid.id;
	}
	//\u5bfc\u51fa\u524d\u63d0\u4f9b\u7684\u7528\u6237\u63a5\u53e3,\u662f\u5426\u5141\u8bb8\u5bfc\u51fa
	var allowexport = true;
	if(typeof(beforeXlsExport) != "undefined" && beforeXlsExport.constructor == Function){
		try{
			allowexport = beforeXlsExport.apply(tid);
		}
		catch(e){
			allowexport = false;
			alert($res_entry('ui.control.table.excel.beforeXlsErr','\u9875\u9762\u65b9\u6cd5beforeXlsExport()\u6709\u9519\uff0c\u8bf7\u68c0\u67e5\uff01'));
		}
	}
	if(!allowexport) return false;
	
	//\u68c0\u67e5\u914d\u7f6e\u6587\u4ef6\u662f\u5426\u5b58\u5728
	var checkConfigFile = function(filepath) {
		var rtn = true;
		var responseText = $.ajax({url: filepath, async: false}).responseText;	
		
		if ($.trim(responseText).length == 0 ||responseText.indexOf("HTTP Status 404") != -1) rtn = false;
		
		return rtn;
	}
	
	var filepath = "./ExcelConfig.html";
	//\u5224\u65ad\u5f53\u524d\u8def\u5f84\u6709\u6ca1\u6709\u914d\u7f6e\u6587\u4ef6
    if (!checkConfigFile(filepath))    {
    	//\u5982\u679c\u5f53\u524d\u8def\u5f84\u6ca1\u6709\u914d\u7f6e\u6587\u4ef6\uff0c\u5219\u68c0\u67e5webapp/common/excel/\u8def\u5f84
		var docPath = document.location.pathname.toString();        
        filepath = docPath.match(/\/.+?\//)[0]+"common/excel/ExcelConfig.html";
		//\u5982\u679c\u4ecd\u7136\u627e\u4e0d\u5230\u914d\u7f6e\u6587\u4ef6\uff0c\u63d0\u793a\u7528\u6237\u5e76\u9000\u51fa
		if (!checkConfigFile(filepath)) {
			alert($res_entry('ui.control.table.excel.noconfigfile', '\u65e0\u6cd5\u627e\u5230ExcelConfig.html\u6587\u4ef6\uff0c\u8bf7\u6b63\u786e\u914d\u7f6e\uff01'));
			return false;
		}
    }
    	
	document.open(filepath+'?tid=' + tid, '_blank', 'toolbar=0,location=0,direct=0,menubar=0,scrollbars=1, width=500,height=500');    
};
  
  //\u5bfc\u51fa\u662f\u521b\u5efa\u8868\u5355\u548c\u63d0\u4ea4\u8868\u5355
var ExportExcel = function(tid)
{
	callUrl('$/ssb/uiloader/loginMgt/getUserInfo.ssm',null,'','true');
	
	//\u8868\u683cz\uff1atable\u6807\u8bc6
    if (typeof tid == "object")    {
        tid = tid.id;
    }
    
	//\u5bfc\u51fa\u524d\u63d0\u4f9b\u7684\u7528\u6237\u63a5\u53e3,\u662f\u5426\u5141\u8bb8\u5bfc\u51fa
	var showconfig = tableModel_arr[tid].isExcelConfig;
	if(!showconfig || showconfig == "false" || showconfig == "")	{
		var allowexport = true;
		if(typeof(beforeXlsExport) != "undefined" && beforeXlsExport.constructor == Function){
			try{
				allowexport = beforeXlsExport.apply(tid);
			}
			catch(e){
				allowexport = false;
				alert($res_entry('ui.control.table.excel.beforeXlsErr','\u7528\u6237\u65b9\u6cd5beforeXlsExport()\u6709\u9519\uff0c\u8bf7\u68c0\u67e5\uff01'));
			}
		}
		if(!allowexport) return false;
	}
	
	//\u8d85\u8fc7countforwarn\u7684\u63d0\u793a
	if(tableModel_arr[tid].countforwarn) {
		var allowexport = true;
		if(tableModel_arr[tid].pg.totalrow > tableModel_arr[tid].countforwarn) {
			allowexport = window.confirm($res_entry('ui.control.table.excel.numbertowar1',"\u5bfc\u51fa\u8bb0\u5f55\u603b\u6570\u8d85\u8fc7 ") + tableModel_arr[tid].countforwarn + $res_entry('ui.control.table.excel.numbertowar2'," \u6761\uff0c\u662f\u5426\u7ee7\u7eed\u5bfc\u51fa\uff1f"))
		}
		if(!allowexport) {
			return false;
		}
	}
	
	//\u6821\u9a8cexportInfo\u4fe1\u606f
	var msgArr = new Array();
	if(exportInfo) {
		with(exportInfo) {			
			if(!strategy || (strategy != "multisheet")&& (strategy != "multifile")) {
				msgArr.push("strategy");
			}
			if(!countperfetch || countperfetch.toString().match(/^[1-9]\d*$/g) == null) {
				msgArr.push("countperfetch");
			}
			if(!countpersheet || countperfetch.toString().match(/^[1-9]\d*$/g) == null || countperfetch>65535) {
				msgArr.push("countpersheet");
			}
		}
	}
	if(!exportInfo || msgArr.length>0){
		alert($res_entry('ui.control.table.excel.exportinfo.error',"\u60a8\u4fee\u6539\u4e86\u5bfc\u51fa\u4fe1\u606fexportInfo\uff0c\u4f46\u6ca1\u6709\u6b63\u786e\u8bbe\u7f6e\u5c5e\u6027\uff01\u8bf7\u68c0\u67e5")+ "\uff1a\n" + JSON.stringify(exportInfo));
		return false;
	}

    var table = getTableById(tid);
    if(tableModel_arr[tid].IS_EXCEL_STYLE == true)
    {
    	var excel_file_title = tableModel_arr[tid].excel_file_title;
    	var excel_file = tableModel_arr[tid].excel_file;
    }
     //\u5982\u679c\u8868\u683c\u67e5\u51fa\u7684\u6570\u636e\u5927\u5c0f\u4e0d\u4e3a\u7a7a\uff0c\u5219\u53ef\u4ee5\u5bfc\u51fa\u6587\u4ef6
    if (table.allcount != null && table.allcount != 0)
    {
        var condition = tableModel_arr[tid].condition;//\u67e5\u8be2\u6570\u636e\u7684\u6761\u4ef6
        var sid = tableModel_arr[tid].sid;//table\u7684sid
        var url = document.getElementById(sid).getAttribute('url');//\u83b7\u53d6z:sevice \u6807\u7b7e\u7684URL
        var arr = url.split(".");
        
        var action = arr[0] + ".ssbxls";
        var form = document.getElementById('excel');
        form = null;
        if (form == null)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);
            form.style.display = "none";
            form.action = action;
            form.id = 'excel';
            form.method = 'post';
            if (tableModel_arr[tid].isExcelConfig == 'true')
            {
                //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
                var props = pop;
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
                var captions = cap;
            } else {
                //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
                var props = tableModel_arr[tid].props;
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
                var captions = tableModel_arr[tid].caption;
            }
            var con = JSON.stringify(condition);
            var json = con.substring(0, con.lastIndexOf("]")) + ",0" + "," + new String(table.allcount) + "]";
            var input = document.createElement("input");
            form.appendChild(input);
            input.name = "condition";
            input.value = json;

            var input = document.createElement("input");
            form.appendChild(input);
            input.name = "tableID";
            input.value = tid;
			
			//\u5bfc\u51fa\u6587\u4ef6\u540d
			var input_filename = document.createElement("input");
			form.appendChild(input_filename);
            input_filename.name = "excelfilename";
            input_filename.value = exportInfo.filename;
            
			//\u5bfc\u51fa\u6587\u4ef6\u751f\u6210\u7b56\u7565
			var input_excelstrategy = document.createElement("input");
			form.appendChild(input_excelstrategy);
            input_excelstrategy.name = "excelstrategy";
            input_excelstrategy.value = exportInfo.strategy;
			
			//\u5bfc\u51fa\u6587\u4ef6\u7684\u65f6\u95f4\u683c\u5f0f
			var input_exceldateformat = document.createElement("input");
			form.appendChild(input_exceldateformat);
            input_exceldateformat.name = "exceldateformat";
            input_exceldateformat.value = exportInfo.dateformat;
			
			//\u5bfc\u51fa\u6587\u4ef6\u6bcf\u6b21\u53d6\u7684\u8bb0\u5f55\u6570
			var input_fetchcount = document.createElement("input");
			form.appendChild(input_fetchcount);
            input_fetchcount.name = "countperfetch";
            input_fetchcount.value = exportInfo.countperfetch;
			
			//\u5bfc\u51fa\u6587\u4ef6\u6bcf\u4e2asheet\u7684\u8bb0\u5f55\u603b\u6570
			var input_sheetcount = document.createElement("input");
			form.appendChild(input_sheetcount);
            input_sheetcount.name = "countpersheet";
            input_sheetcount.value = exportInfo.countpersheet;
            
            if(tableModel_arr[tid].sidParamCount == 4 ) {
            	//\u9700\u8981\u8c03\u7528\u56db\u4e2a\u53c2\u6570\u7684\u67e5\u8be2\u65b9\u6cd5
				var input_sortcolumn = document.createElement("input");
				form.appendChild(input_sortcolumn);
	            input_sortcolumn.name = "sortcolumn";
	            input_sortcolumn.value = "sortcolumn";
			}
			
			//\u67e5\u8be2\u51fa\u7684\u8bb0\u5f55\u603b\u6570
			var input_queriedcount = document.createElement("input");
			form.appendChild(input_queriedcount);
            input_queriedcount.name = "queriedcount";
            input_queriedcount.value = tableModel_arr[tid].pg.totalrow;
			
			if(tableModel_arr[tid].IS_EXCEL_STYLE == true)
			{
				for (var i = 0; i < props.length; i ++)
	            {
	                var prop = document.createElement("input");
	                form.appendChild(prop);
	                prop.type = "text";
	                prop.name = "prop";
	                prop.value = props[i];
	
	                var caption = document.createElement("input");
	                form.appendChild(caption);
	                caption.type = "text";
	                caption.name = "caption";
	                caption.value = captions[i];
	                
	                var excelfilestyle = document.createElement("input");
	                form.appendChild(excelfilestyle);
	                excelfilestyle.type = "text";
	                excelfilestyle.name = "excelfilestyle";
	                excelfilestyle.value = eval('excel_file.'+props[i]);
	                
	                var exceltitlestyle = document.createElement("input");
	                form.appendChild(exceltitlestyle);
	                exceltitlestyle.type = "text";
	                exceltitlestyle.name = "exceltitlestyle";
	                exceltitlestyle.value = eval('excel_file_title.'+props[i]);
	            }
			}
			else
			{
				for (var i = 0; i < props.length; i ++)
	            {
	                var prop = document.createElement("input");
	                form.appendChild(prop);
	                prop.type = "text";
	                prop.name = "prop";
	                prop.value = props[i];
	
	                var caption = document.createElement("input");
	                form.appendChild(caption);
	                caption.type = "text";
	                caption.name = "caption";
	                caption.value = captions[i];
	            }
			}
        }
		
		var createIFrame=function(){
			var oIFrame={};
			try{
				oIFrame = document.createElement("<IFRAME name='iframe_riaxls' />");
			}catch(e){
				oIFrame=document.createElement("IFRAME");
				oIFrame.name='iframe_riaxls';
			}
			oIFrame.name ='iframe_riaxls';
			oIFrame.width=0;
			oIFrame.height=0;
			oIFrame.frameBorder=0;
			oIFrame.id='iframe_riaxls';
			document.body.appendChild(oIFrame);
			return oIFrame;
		};
		
		var oFrame=createIFrame();
		
		var xlsCallback = function(){
	        var thisDocument = oFrame.contentDocument || oFrame.contentWindow.document;
	        var html = thisDocument.body.innerHTML;
	        if (html == "undefined" || html == null || html.trim() == "") {
			}
			else {
				alert(html);
			}
		        setTimeout(function(){
		            try {
		                form.parentNode.removeChild(form);
		                oFrame.parentNode.removeChild(oFrame);
		            } 
		            catch (e) {
		                throw e;
		            }
		        }, 100);
	    };
	    if (window.attachEvent) {
	        EventManager.Add(oFrame, 'load', xlsCallback);
	    }
	    else {
	        oFrame.addEventListener('load', xlsCallback, false);
	    }
		
		form.setAttribute("target",oFrame.name);
        form.submit();
    }
    else {
		alert($res_entry('ui.control.table.excel.exportinfo.error',"\u6ca1\u6709\u53ef\u5bfc\u51fa\u7684\u6570\u636e\uff01"));
	}
};
 //\u521d\u59cb\u5316\u914d\u7f6e\u9875\u9762   
var initExcel = function()
{
    window.onblur = function() {
        window.focus();
    };
    var table = document.getElementById('config');
    var url = document.location.toString();
    var cookieID = window.opener.location.pathname.toString();
    var arr = url.split('=');
    var tid = arr[1];
    var tid_input = document.createElement("input");
    tid_input.type = "hidden";
    tid_input.id = "tid";
    tid_input.value = tid;
    document.body.appendChild(tid_input);
    var captions = window.opener.getCaptions(tid);
    var props = window.opener.getProps(tid);
    var prop = getCookie(cookieID + "prop");
    for (var i = 0; i < captions.length; i ++)
    {
		if(props[i] == null || props[i] == "null") continue;
        var row = table.insertRow(-1);
        var input = document.createElement('input');
        input.type = "checkbox";
        input.name = "name";
        input.value = props[i];
        row.insertCell(-1).appendChild(input);
        setRowCss(row, ROW_CLASS_EVEN);
        
        // Modified on 2008-08-28
        var ocell=row.insertCell(-1);
        ocell.innerHTML=captions[i];
        // innerText_function(row.insertCell(-1), captions[i]);
        // \u5224\u65ad\u662f\u5426\u4e3aIE\u6d4f\u89c8\u5668,\u5426\u5219\u9700\u7ed9\u975eIE\u751f\u6210\u4e00\u4e2ainnerText.
        if(!isIE()){
            HTMLElement.prototype.__defineGetter__("innerText",
                function(){
                   var anyString = "";
                   var childS = this.childNodes;
                   for(var i=0; i<childS.length; i++) {
                       if(childS[i].nodeType==1)
                       anyString += childS[i].innerText;
                       else if(childS[i].nodeType==3)
                       anyString += childS[i].nodeValue;
                   }
                   return anyString;
             });
             HTMLElement.prototype.__defineSetter__("innerText",
                 function(sText){
                 this.textContent=sText;
             });
        } 
        innerText_function(ocell, ocell.innerText);
        innerText_function(row.insertCell(-1), props[i]);

        if (prop != null)
        {
            var offset = prop.indexOf(props[i]);
            if (offset != -1)input.checked = "true";
        }
    }
};
var allcheck = function(o)
{
    var table = document.getElementById('config');
    var rows = table.rows;
    for (var i = 1; i < rows.length; i++)
    {
        var input = rows[i].getElementsByTagName('input');
        input[0].checked = o.checked;
    }
};
var next = function()
{
    var props = new Array();
    var captions = new Array();
    var caption = '';
    var prop = '';
    var table = document.getElementById('config');
    var rows = table.rows;
    for (var i = 1; i < rows.length; i++)
    {
        var input = rows[i].getElementsByTagName('input');
        if (input[0].checked == true)
        {
            caption += ";";
            prop += ";";
            var cells = rows[i].cells;
            if (isIE())
            {
                caption += cells[1].innerText;
                prop += cells[2].innerText;
                captions.push(cells[1].innerText);
                props.push(cells[2].innerText);
            } else {
                caption += cells[1].textContent;
                prop += cells[2].textContent;
                captions.push(cells[1].textContent);
                props.push(cells[2].textContent);
            }
        }
    }
    var cookieID = window.opener.location.pathname.toString();
    var utc = Date.UTC(2010, 12, 30);
    var date = new Date(utc);
    document.cookie = cookieID + "prop=" + escape(prop) + ";expires=" + date.toGMTString();
    var tid = document.getElementById("tid").value;
    window.opener.setCaptions(captions);
    window.opener.setProps(props);
    window.close();
    if (tid != '' && tid != null)
    {
        window.opener.ExportExcel(tid);
    }
};
var getCookie = function(cookie_name)
{
    var allcookies = document.cookie;
    var cookie_pos = allcookies.indexOf(cookie_name);
    if (cookie_pos != -1)
    {
        cookie_pos += cookie_name.length + 1;
        var cookie_end = allcookies.indexOf(";", cookie_pos - 1);
        if (cookie_end == -1)
        {
            cookie_end = allcookies.length;
        }
        var value = unescape(allcookies.substring(cookie_pos, cookie_end));
    }
    return value;
};
function uploadFile(o, sid, field, anotherField)
{
    var tr = o;
	//wanghao
    while (tr.tagName.toUpperCase() != 'TR')
    {
        tr = tr.parentNode;
        if (tr.tagName.toUpperCase() == 'BODY')
        {
            break;
        }
    }
    var obj = {};
    var list = new Array();
    obj.field = "";
    obj.anotherField = "";
    if (field != null)  obj.field = field;
    if (anotherField != null)  obj.anotherField = anotherField;
    var filepath = "";
    var desc = "uploadfile";
    var arr_upload = null;

    if (isIE())
    {
        arr_upload = tr.getElementsByTagName("upload");
    }
    else {
        arr_upload = tr.getElementsByTagName("z:upload");
    }
	
	//\u4fee\u6539\u8bb0\u5f552: wanghao
    if (arr_upload.length <= 0)
    {
        arr_upload = [];
        arr_upload.push(tr);
    }
	
	//wanghao
    __file_List = [];

    for (var i = 0; i < arr_upload.length; i ++)
    {
        var input_upload = arr_upload[i].getElementsByTagName("input");
        for (var i = 0; i < input_upload.length; i++)
        {
            if (input_upload[i].type.toUpperCase() == 'FILE')
            {
                filepath = input_upload[i].value;
			//wanghao
                __file_List.push(input_upload[i]);
            }
        }
        list.push(filepath);
        list.push('upload');
        list.push(filepath);
        obj.uploadtest = list;
	    
		//wanghao
        callSid(sid, obj);
    }
}
;
function SSB_RIA_initReturnValueObject(rtnObject, props)
{
    for (var i = 0; i < props.length; i ++)
    {
        if (props[i] != null)
        {
            var paras = props[i].split(".");
            if (paras.length > 1)
            {
                for (var j = 0; j < paras.length - 1; j++)
                {
                    var temp = paras[0];
                    for (var k = 1; k <= j; k ++)
                        temp = temp + "." + paras[k];
                    if (eval("rtnObject." + paras[j]) == undefined || eval("rtnObject." + paras[j]) == null)
                    {
                        eval("rtnObject." + temp + "= {};");
                    }
                }
            }
        }
    }

    return rtnObject;
}
;

//wanghao add
function uploadSingleFile(o, sid)
{
    var tr = o;

    while (tr.tagName.toUpperCase() != 'TR')
    {
        tr = tr.parentNode;
        if (tr.tagName.toUpperCase() == 'BODY')
        {
            break;
        }
    }

    var desc = "uploadfile";
    var arr_upload = null;

    if (isIE())
    {
        arr_upload = tr.getElementsByTagName("upload");
    }
    else {
        arr_upload = tr.getElementsByTagName("z:upload");
    }

    if (arr_upload.length <= 0)
    {
        arr_upload = [];
        arr_upload.push(tr);
    }

    __file_List = [];

    for (var i = 0; i < arr_upload.length; i ++)
    {
        var input_upload = arr_upload[i].getElementsByTagName("input");
        for (var i = 0; i < input_upload.length; i++)
        {
            if (input_upload[i].type.toUpperCase() == 'FILE')
            {
                __file_List.push(input_upload[i]);
            }
        }

        callSid(sid);
    }
}

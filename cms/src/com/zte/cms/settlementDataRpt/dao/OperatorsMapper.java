package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.ssb.framework.base.util.PageInfo;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:02 2019/6/27
 * @Description:
 */
public interface OperatorsMapper {

    PageInfo selectOperators(OperInformation operInformation);
}

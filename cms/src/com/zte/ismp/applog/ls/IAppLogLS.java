package com.zte.ismp.applog.ls;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

/**
 * <p>
 * 文件名称: IAppLogLS.java
 * </p>
 * <p>
 * 文件描述: 业务日志管理接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2010
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供业务日志的查询接口
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2010年08月10日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXV10 TVMSV1.01.01
 * @author 李梅
 */

public interface IAppLogLS
{

    /**
     * 根据条件分页查询AppLog对象，super操作员可以查询所有的业务日志，其他操作员只可以查询自己的业务日志
     * 
     * @param appLog AppLogInfoEntity对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception 异常
     */
    public TableDataInfo pageInfoQuery(AppLogInfoEntity appLog, int start, int pageSize) throws Exception;

    /**
     * 根据条件分页查询AppLog对象，super操作员可以查询所有的业务日志，其他操作员只可以查询自己的业务日志
     * 
     * @param appLog AppLogInfoEntity对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws Exception 异常
     */
    public TableDataInfo pageInfoQuery(AppLogInfoEntity appLog, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;
}

package com.zte.cms.content.util;

import com.zte.cms.content.common.CntConstants;
import com.zte.ismp.common.ResourceManager;

public class CntUtils
{
    /**
     * <p>
     * 根据errorCode读取资源文件中的文本
     * </p>
     * 
     * @param errorCode 错误码
     * @return 返回资源文件中的文本
     * @since CMS
     */
    public static String getResourceText(String errorCode)
    {
        String codeText = ResourceManager.getResourceText(CntConstants.MSG_PREFIX + errorCode);
        return codeText;
    }
}

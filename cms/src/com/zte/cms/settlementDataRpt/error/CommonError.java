package com.zte.cms.settlementDataRpt.error;

/**
 * ProjectName: miaosha-com.miaoshaDemo.error
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 16:17 2019/1/30
 * @Description:
 */
public interface CommonError {

    int getErrorCode();
    String getErrorMsg();
    CommonError setErrorMsg(String errorMsg);


}

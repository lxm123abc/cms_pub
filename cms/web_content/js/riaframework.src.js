if (!Object.prototype.toJSONString) {

    Array.prototype.toJSONString = function () {
        var a = [],     // The array holding the partial texts.
                i,          // Loop counter.
                l = this.length,
                v;          // The value to be stringified.


        // For each value in this array...

        for (i = 0; i < l; i += 1) {
            v = this[i];
            switch (typeof v) {
                case 'object':

                // Serialize a JavaScript object value. Ignore objects thats lack the
                // toJSONString method. Due to a specification error in ECMAScript,
                // typeof null is 'object', so watch out for that case.

                    if (v) {
                        if (typeof v.toJSONString === 'function') {
                            a.push(v.toJSONString());
                        }
                        else if(typeof v.toJSONString == 'object')
						            {
							              a.push(v.toJSONString());
						            }
                    } else {
                        a.push('null');
                    }
                    break;

                case 'string':
                case 'number':
                case 'boolean':
                    a.push(v.toJSONString());

            // Values without a JSON representation are ignored.

            }
        }

// Join all of the member texts together and wrap them in brackets.

        return '[' + a.join(',') + ']';
    };


    Boolean.prototype.toJSONString = function () {
        return String(this);
    };


    Date.prototype.toJSONString = function () {

        // Eventually, this method will be based on the date.toISOString method.

        function f(n) {

            // Format integers to have at least two digits.

            return n < 10 ? '0' + n : n;
        }

        return '"' + this.getUTCFullYear() + '-' +
               f(this.getUTCMonth() + 1) + '-' +
               f(this.getUTCDate()) + 'T' +
               f(this.getUTCHours()) + ':' +
               f(this.getUTCMinutes()) + ':' +
               f(this.getUTCSeconds()) + 'Z"';
    };


    Number.prototype.toJSONString = function () {

        // JSON numbers must be finite. Encode non-finite numbers as null.

        return isFinite(this) ? String(this) : 'null';
    };


    Object.prototype.toJSONString = function () {
        var a = [],     // The array holding the partial texts.
                k,          // The current key.
                v;          // The current value.

        // Iterate through all of the keys in the object, ignoring the proto chain
        // and keys that are not strings.

        for (k in this) {
            if (typeof k === 'string' &&
                Object.prototype.hasOwnProperty.apply(this, [k])) {
                v = this[k];
                switch (typeof v) {
                    case 'object':

                    // Serialize a JavaScript object value. Ignore objects that lack the
                    // toJSONString method. Due to a specification error in ECMAScript,
                    // typeof null is 'object', so watch out for that case.

                        if (v) {
                            if (typeof v.toJSONString === 'function') {
                                a.push(k.toJSONString() + ':' + v.toJSONString());
                            }
                        } else {
                            a.push(k.toJSONString() + ':null');
                        }
                        break;

                    case 'string':
                    case 'number':
                    case 'boolean':
                        a.push(k.toJSONString() + ':' + v.toJSONString());

                // Values without a JSON representation are ignored.

                }
            }
        }

// Join all of the member texts together and wrap them in braces.

        return '{' + a.join(',') + '}';
    };


    (function (s) {

        // Augment String.prototype. We do this in an immediate anonymous function to
        // avoid defining global variables.

        // m is a table of character substitutions.

        var m = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        };


        s.parseJSON = function (filter) {
            var j;

            function walk(k, v) {
                var i;
                if (v && typeof v === 'object') {
                    for (i in v) {
                        if (Object.prototype.hasOwnProperty.apply(v, [i])) {
                            v[i] = walk(i, v[i]);
                        }
                    }
                }
                return filter(k, v);
            }


// Parsing happens in three stages. In the first stage, we run the text against
            // a regular expression which looks for non-JSON characters. We are especially
            // concerned with '()' and 'new' because they can cause invocation, and '='
            // because it can cause mutation. But just to be safe, we will reject all
            // unexpected characters.

            // We split the first stage into 3 regexp operations in order to work around
            // crippling deficiencies in Safari's regexp engine. First we replace all
            // backslash pairs with '@' (a non-JSON character). Second we delete all of
            // the string literals. Third, we look to see if only JSON characters
            // remain. If so, then the text is safe for eval.

            if (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/.test(this.
                    replace(/\\./g, '@').
                    replace(/"[^"\\\n\r]*"/g, ''))) {

                // In the second stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval('(' + this + ')');

// In the optional third stage, we recursively walk the new structure, passing
                // each name/value pair to a filter function for possible transformation.

                return typeof filter === 'function' ? walk('', j) : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('parseJSON');
        };


        s.toJSONString = function () {

            // If the string contains no control characters, no quote characters, and no
            // backslash characters, then we can simply slap some quotes around it.
            // Otherwise we must also replace the offending characters with safe
            // sequences.

            if (/["\\\x00-\x1f]/.test(this)) {
                return '"' + this.replace(/[\x00-\x1f\\"]/g, function (a) {
                    var c = m[a];
                    if (c) {
                        return c;
                    }
                    c = a.charCodeAt();
                    return '\\u00' +
                           Math.floor(c / 16).toString(16) +
                           (c % 16).toString(16);
                }) + '"';
            }
            return '"' + this + '"';
        };
    })(String.prototype);
}
//\u89e3\u6790\u6807\u7b7e\u5f02\u5e38
var RIA_ERROR_PARSETAG = 100;//\u89e3\u6790\u6807\u7b7e
var RIA_ERROR_PARSETAG_SERVICE = 101;//\u89e3\u6790\u670d\u52a1\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_BIND = 102;//\u89e3\u6790\u90a6\u5b9a\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_INIT = 103;//\u89e3\u6790\u521d\u59cb\u5316\u51fa\u9519
var RIA_ERROR_PARSETAG_REQUEST = 104;//\u89e3\u6790\u8bf7\u6c42\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_RESPONSE = 105;//\u89e3\u6790\u8fd4\u56de\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_FORWORD = 106;//\u89e3\u6790\u8df3\u8f6c\u65f6\u51fa\u9519
//\u83b7\u53d6\u6d4f\u89c8\u5668\u51fa\u9519\uff0c\u5224\u65ad\u6d4f\u89c8\u5668\u662f\u5426\u652f\u6301
var RIA_ERROR_SUPPORT_BROWSER = 110;
//\u5c01\u88c5\u9875\u9762\u53c2\u6570\u5931\u8d25
var RIA_ERROR_ENCAPSULATION_URL = 111;
//HTTP\u54cd\u5e94\u5f02\u5e38
var RIA_ERROR_HTTPRES = 200;
var RIA_ERROR_HTTPRES_STATS = 201;
//\u4e1a\u52a1\u5904\u7406\u5f02\u5e38
var RIA_ERROR_OPERATION = 300;
//\u8c03\u7528\u670d\u52a1\u65f6callSid\u662f\u5fc5\u987b\u6307\u5b9a\u4e00\u4e2a\u7533\u660e\u7684\u670d\u52a1
var RIA_ERROR_CALLSID = 301;
var RIA_ERROR_GETCTRL = 302;
var RIA_FRAMEWORK_EXCEPTION = "RiaFrameWork";
//\u670d\u52a1\u56de\u8c03\u5f02\u5e38
var RIA_ERROR_DISPOSE = 401;//\u670d\u52a1\u5bb9\u5668\u5f02\u5e38


var riaLog = function() {
};
riaLog.prototype.info = function() {
};
riaLog.prototype.error = function(excepCode,excepDesc) {
	  //alert(excepCode+" : "+excepDesc);
	  alert("\u5904\u7406\u5f02\u5e38,\u8be6\u60c5\u8bf7\u770b\u9519\u8bef\u65e5\u5fd7");
};

/**
 * \u7528\u6237\u5728\u5bfc\u5165RIA\u7684js\u4e4b\u524d\uff0c\u9700\u8981\u5728\u9875\u9762\u4e2d\u589e\u52a0\u5982\u4e0b\u7684\u914d\u7f6e\uff1a
 *   var RIAConfig {
 *     filePath:c:\dddd.log,
 *     isLogger:true  // \u6b64\u5904\u914d\u7f6etrue\uff0c\u65e5\u5fd7\u529f\u80fd\u624d\u8d77\u4f5c\u7528\u3002
 *   }
 * \u5982\u679c\u7528\u6237\u4e0d\u914d\u7f6e\u5982\u4e0a\u4fe1\u606f\uff0c\u5219\u9ed8\u8ba4\u7528\u6237\u5173\u95ed\u65e5\u5fd7\u5f00\u5173\u3002
 */
var LogFactory = {
    getLog:function() {
        var logjs;
        try {
            logjs = new Log4js.getLogger(RIA_FRAMEWORK_EXCEPTION);
            logjs.setLevel(Log4js.Level.ALL);
            logjs.addAppender(new Log4js.ConsoleAppender(false));
			// \u5199\u65e5\u5fd7\u7684\u65b9\u5f0f\u4e0d\u5b89\u5168\uff0c\u6240\u4ee5\uff0c\u8fd8\u662f\u9009\u62e9\u63a7\u5236\u53f0\u6253\u5370
            // var filePath = "C:\\somefile.log";
            //logjs.addAppender(new Log4js.FileAppender(filePath));
        }
        catch(e) {
            logjs = new riaLog();
        }
        return logjs;
    }
};
var log = new LogFactory.getLog();
/////////////////////////////////////////////////////////////////////
//////////////////////////////\u6570\u636e\u6821\u9a8c///////////////////////////////
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u957f\u5ea6 \u6c49\u5b57\u7b97\uff12\u4e2a\u5b57\u8282*/
function strlen(str)
{	var i;
	var len;
	len = 0;
	for (i=0;i<str.length;i++)
	{
		if (str.charCodeAt(i)>255) len+=2; else len++;
	}
	return len;
};
/*\u68c0\u6d4b\u662f\u5426\u662f\u5b57\u7b26\u6216\u6570\u5b57*/
function ischar(str)
{
	var s2=/[^A-Za-z0-9]/;
   if(s2.exec(str))
		return false;
	else
 		return true;
};
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u7a7a */
function isnull(str)
{
	if(str == null) return true;
	var i;
 	for (i=0;i<str.length;i++)
	{
  		if (str.charAt(i)!=' ') return false;
	}
 	return true;
};

/*\u5224\u65ad\u662f\u5426\u8f93\u5165\u4e86\u4e2d\u6587\u7684\u5b57\u7b26*/
function isExsitChineseChar(str){
   if(str.length!=strlen(str))
        return true;
   else
        return false;
};

//\u68c0\u6d4b\u8d27\u5e01
function ismoney(str)
{
    var pos1 = str.indexOf(".");
    var pos2 = str.lastIndexOf(".");
    if ((pos1 != pos2)||!isnumber(str)) {
        return false;
    }
    return true;
};
//\u68c0\u6d4b\u5e74(YYYY\u5e74)
function  isyear(str)
{
  if(!isnumber(str))
     return false;
   //if(str.length != 4 ) {
    //  return false;
    //}

	if((str<'1950')||(str>'2050'))
	{
		return false;
	}

   return true;

};

//\u68c0\u6d4b\u975e\u7a7a
function checklength(val,maxlen)
{
    var str = trim(document.forms[0].elements[val].value);
    if ( str == "" && maxlen==null ) {
        alert ("\u6b64\u9879\u4e0d\u53ef\u4e3a\u7a7a\uff01");
        document.forms[0].elements[val].focus();
        document.forms[0].elements[val].select();
        return false;
    }else if(str!="" && maxlen!=null){
        if (str.length>maxlen){
            return false;
        }
    }
    document.forms[0].elements[val].value = str;
    return true;
};
//\u9a8c\u8bc1\u6570\u5b57
function checknum(val)
{
   if(isNaN(document.forms[0].elements[val].value)) {
       alert ("\u8bf7\u8f93\u5165\u6570\u5b57\uff01");
       document.forms[0].elements[val].focus();
       document.forms[0].elements[val].select();
       return false;
   }
   return true;
};
//\u53bb\u7a7a\u683c
function trim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
};
//\u68c0\u6d4b\u7535\u8bdd\u53f7\u7801
function isphone(str)
{
	var number_chars = "()-1234567890";
	var i;
	for (i=0;i<str.length;i++)
	{
	if (number_chars.indexOf(str.charAt(i))==-1) return false;
	}
	return true;
};

function replaceAll(str){
	var i;
	for (i=0;i<str.length;i++)
	{/*
		if(str.charAt(i)=='&')
			str=str.replace('&','&amp;');
		if(str.charAt(i)=='<')
			str=str.replace('<','&lt;');
		if(str.charAt(i)=='>')
			str=str.replace('>','&gt;');*/
		if(str.charAt(i)=='\'')
			str=str.replace('\'','\\uff07');
		if(str.charAt(i)=='\"')
			str=str.replace('\"','\\uff02');
	}
			return str;
};
//\u68c0\u67e5\u8868\u5355\u6240\u6709\u5143\u7d20
function verifyAll(myform)
{
	var i;
	for (i=0;i<myform.elements.length;i++)
	{
		/*\u5c06\u534a\u89d2\u8f6c\u6362\u4e3a\u5168\u89d2*///comment by chris:\u4ec5\u4ec5\u662f\u5c06\u4e2d\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7\u6362\u6210\u82f1\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7
		if (myform.elements[i].type=="textarea"||myform.elements[i].type=="text")
		{
			myform.elements[i].value=replaceAll(myform.elements[i].value);
		}

		/* \u975e\u81ea\u5b9a\u4e49\u5c5e\u6027\u7684\u5143\u7d20\u4e0d\u4e88\u7406\u776c */
		if (myform.elements[i].chname+""=="undefined") continue;

		/* \u6821\u9a8c\u5f53\u524d\u5143\u7d20 */
		if (verifyInput(myform.elements[i])==false)
		{
			myform.elements[i].focus();
			myform.elements[i].select();
			return false;
		}
	}
	return true;
};

/* \u68c0\u6d4b\u6307\u5b9a\u6587\u672c\u6846\u8f93\u5165\u662f\u5426\u5408\u6cd5 */
function verifyInput(input,datatype)
{
	var i;
	var error = false;
	var isnotnull=false;
	input.datatype = datatype;
	input.chname = input.name;
	/* \u975e\u7a7a\u6821\u9a8c */
	if (isnull(input.value))
	{
		//alert("\""+input.chname+"\"\u4e0d\u80fd\u4e3a\u7a7a");
		//error = true;
		return true;
	}

	if(input.maxlen+""!="undefined" && strlen(input.value)>input.maxlen)
	{
		alert("\""+input.chname+"\"\u8d85\u8fc7\u9650\u5236\u957f\u5ea6!");
		error = true;
		return false;
	}
	if(input.datatype+""!="undefined")
	{
	 
	 var type = input.datatype;
	 var datasize = null;
	 var size = null;
	 if(type.indexOf("(") != -1)
	 {
	 	type = input.datatype.substring(0, type.indexOf("("));
	 	datasize = input.datatype.substring(input.datatype.indexOf("(") + 1, input.datatype.indexOf(")"));
	 	size = datasize.split(",");
	 }

	/* \u6570\u636e\u7c7b\u578b\u6821\u9a8c */
		switch(type)
		{
			case "posiint":
			
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          alert("\""+input.chname+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57\uff01");
                          error = true;
                     }
                     else{
                         if (isPositiveInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                             alert("\""+input.chname+"\"\u503c\u5e94\u4e3a\u6b63\u6574\u6570\uff01");
                             error = true;
                         }
                         if(size+""!="undefined" && strlen(input.value)>size)
                         {
                             alert("\""+input.chname+"\"\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6\uff01");
                             error = true
                         } 
                     }    
                  }
                  
                  break;
                  
            case "negaint":
			
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          alert("\""+input.chname+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57\uff01");
                          error = true;
                     }
                     else{
                         if (isNegativeNumber(input.value)==false)//\u975e\u6570\u5b57
                         {
                             alert("\""+input.chname+"\"\u503c\u5e94\u4e3a\u8d1f\u6574\u6570\uff01");
                             error = true;
                         }
                         if(size+""!="undefined" && strlen(input.value)>size)
                         {
                             alert("\""+input.chname+"\"\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6\uff01");
                             error = true
                         } 
                     }    
                  }
                  
                  break;  
                  
             case "int":
			
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          alert("\""+input.chname+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57\uff01");
                          error = true;
                     }
                     else{
                         if (isInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                             alert("\""+input.chname+"\"\u503c\u5e94\u4e3a\u6574\u6570\uff01");
                             error = true;
                         }
                         if(size+""!="undefined" && strlen(input.value)>size)
                         {
                             alert("\""+input.chname+"\"\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6\uff01");
                             error = true
                         } 
                     }    
                  }
                  
                  break;        
                      
			case "char": 
			     if (ischar(input.value)==false)
					{
						alert("\""+input.chname+"\"\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd");
						error = true;
					}
					
					break;
					
            case "mobile":
            
                 if(trim(input.value)!="")
                 {            
               		if(checkMobile(trim(input.value))==false)
                    {
                        alert("\""+input.chname+"\"\u683c\u5f0f\u4e0d\u6b63\u786e!");
                        error = true;
                    }
                 }      
                        
                 break;
                 
            case "postcode":
            
                if(checkPostCode(trim(input.value))==false)
                {
                    alert("\""+input.chname+"\"\u53ea\u80fd\u662f\u957f\u5ea6\u4e3a6\u4f4d\u7684\u6570\u5b57\uff01");
                    error = true;
                }
                
                break;
                
            case "posidecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                
                 input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkPositiveDecimal(input)==false)
                  {
                      error=true;
                  }
                }  
                
               break;
               
           case "negedecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                
                input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkNegativeDecimal(input)==false)
                  {
                      error=true;
                  }
                }  
                
               break;    
            
           case "decimal"://\u4e3a\u6b63\u5c0f\u6570\uff0c\u4e5f\u53ef\u4ee5\u4e3a\u8d1f\u5c0f\u6570
               
               input.precision = datasize;
               if(trim(input.value)!="")
               {
                 if(checkDecimal(input)==false)
                 {
                     error=true;
                 }
               }  
                 break;

           case "numberchar":
              if(input.value!="")
              {
                 if(isNumberOrChar(trim(input.value))==false)
                 {
                     alert("\""+input.chname+"\"\u53ea\u80fd\u662f\u6570\u5b57\u6216\u5b57\u6bcd\uff01");
                     error = true;
                 }
              }
              
                 break;
                 
			case "phone":
                if(input.value!="")
                {
                   if((trim(input.value)).length<6){
                        alert("\""+input.chname+"\"\u957f\u5ea6\u5fc5\u987b6\u4f4d\u4ee5\u4e0a\uff01");
                        error = true;
                        break;
                    }
                   if (isphone(input.value)==false)
                       {
                          alert("\""+input.chname+"\"\u53ea\u53ef\u5305\u542b-\u3001()\u548c\u6570\u5b57\uff01");
                          error = true;
                       }
                  }
                  
                  break;
                  
			 case "money": 
			 
			      if(ismoney(input.value)==false)
	 				{
						alert("\""+input.chname+"\"\u53ea\u53ef\u5305\u542b\u4e00\u4e2a.\u548c\u6570\u5b57\uff01");
						error = true;
					}
					
					break;
					
			 case "year": 	
			 
				   if(isyear(input.value)==false)
	 				{
						alert("\""+input.chname+"\"\u5e94\u8be5\u662f\u4e00\u4e2a\u5e74\u4efd,\u8303\u56f4\u57281950\uff0d2050\u4e4b\u95f4\uff01");
						error = true;
					}
					break;
					
			 case "idcard": 
			 
			        if(input.value!="")
	                {
		                return validIdCard(input.value,input.chname);
					}
					break;
					
			 case "email":  
			 
			       if(input.value!="")
					{
		                return emailCheck(input.value,input.chname);
					}
					
					break;
					
			 case "date":
			 
                   if(isExsitChineseChar(input.value))
                   {
                       alert("\""+input.chname+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01");
                       error = true;
                   }
                   else
                       if(input.value != "")
                           //return isDate(input);
                           return true;
                  
                  break;  
			 case "percent"	:
			      if(isExsitChineseChar(input.value))
				  {
                       alert("\""+input.chname+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01");
                       error = true;				  	
				  }else if(input.value != "")
				  {
				  	return checkPercent(input.value);
				  }else
				  {
				  	return true;
				  }
			                      
                     
			/* \u5728\u8fd9\u91cc\u53ef\u4ee5\u6dfb\u52a0\u591a\u4e2a\u81ea\u5b9a\u4e49\u6570\u636e\u7c7b\u578b\u7684\u6821\u9a8c\u5224\u65ad */
			/*  case datatype1: ... ; break;        */
			/*  case datatype2: ... ; break;        */
			/*  ....................................*/
			default	: 			
				var regu =type;
				var re = new RegExp(regu);
				if (!re.test(input.value)) 
				{
				   error = true;				 
				}	
			   break;
		}
	}
	/* \u6839\u636e\u6709\u65e0\u9519\u8bef\u8bbe\u7f6e\u6216\u53d6\u6d88\u8b66\u793a\u6807\u5fd7 */
	if (error)
	{
		return false;
	}
	else
	{
		return true;
	}
};
//\u9a8c\u8bc1percent
function checkPercent(regEx)
{
	var regu =/^-?([1-9]\d*|0)(\.\d+)?%$/;
	var re = new RegExp(regu);
	if (re.test(regEx)) 
	{
	   return false;				 
	}
	else
	{
	  return true;
	}
}
//\u5c4f\u853d\u952e\u76d8\u5feb\u6377\u952e
function checkKey()
{
	var srce = window.event.srcElement.type;
	var shield = false;
	//\u6587\u672c\u6846\u5185\u4e0d\u5c4f\u853d\u5feb\u6377\u952e
	if(window.event.keyCode == 8 && srce =="password") return;
	if(window.event.keyCode == 8 && srce =="file") return;
	if (window.event.keyCode == 8 && srce =="text")	return;
	if (window.event.keyCode == 8 && srce =="textarea")	return;

	//\u5c4f\u853d\u6587\u672c\u6846\u5185\u7684\u56de\u8f66
	if (window.event.keyCode == 13 && srce =="password"){
		event.keyCode=0;
	    return false;
	}

	if (window.event.keyCode == 13 && srce =="file"){
		event.keyCode=0;
	    return false;
	}
	if (window.event.keyCode == 13 && srce =="text"){
		event.keyCode=0;
	    return false;
	}
	if (window.event.keyCode == 13 && srce =="textarea"){
		event.keyCode=0;
	    return false;
	}

	if ((event.keyCode==8) || //\u5c4f\u853d\u9000\u683c\u5220\u9664\u952e
		(event.keyCode==116)|| //\u5c4f\u853d F5 \u5237\u65b0\u952e
		(event.ctrlKey && event.keyCode==82)){ //Ctrl + R
		event.keyCode=0;
	    return false;
	}
	if ((window.event.altKey)&&
		((window.event.keyCode==37)|| //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2190
		(window.event.keyCode==39))){ //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2192
		alert("\u4e0d\u51c6\u4f60\u4f7f\u7528ALT+\u65b9\u5411\u952e\u524d\u8fdb\u6216\u540e\u9000\u7f51\u9875\uff01");
		return false;
	}
	if ((event.ctrlKey)&&(event.keyCode==78)) //\u5c4f\u853d Ctrl+n
		return false;
	if (window.event.srcElement.tagName == "A" && window.event.shiftKey)
		return false; //\u5c4f\u853d shift \u52a0\u9f20\u6807\u5de6\u952e\u65b0\u5f00\u4e00\u7f51\u9875
};
function compareDate(date1,date2){
		arrDatef = date1.split("-");
		arrDates = date2.split("-");
		for(i=0;i< arrDatef.length;i++){
			if(parseFloat(arrDatef[i]) > parseFloat(arrDates[i]))
				return "d1>d2";
			if(parseFloat(arrDatef[i]) < parseFloat(arrDates[i]))
				return "d1<d2";
		}
		return "d1=d2";
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u7684\u65e5\u671f\u662f\u5426\u7b26\u5408 yyyy-MM-dd
\u8f93\u5165\uff1a
	value\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDate(elem)
{
    var date=elem.value;
	var length=date.length;
	var year=date.substr(0,4);
	var month=date.substr(5,2);
	var day=date.substr(8,2);
	var var1=date.substr(4,1);
	var var2=date.substr(7,1);

	if(length == '10' && var1==var2 && (var1 == '-'||var1 == '/'||var1 == '.'))
	{
		if(isNumber(year) && isNumber(month) && isNumber(day))
		{
                    if(month>"12" || month< "01") {
                       alert("\""+elem.chname+"\"\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4\uff01");
                       return false;
                    }
                    if(day>getMaxDay(year,month) || day< "01") {
                       alert("\""+elem.chname+"\"\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408\uff01");
                       return false;
                    }
                    return true;
		}
		else {
                   alert("\""+elem.chname+"\"\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57\uff01");
                   return false;
              }
	}
	else {
	     
	      
			 alert("\"" + elem.chname + "\"\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD,YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f\uff01");
			 return false;
            
    }
};
function getMaxDay(year,month) {
	if(month==4||month==6||month==9||month==11)
		return "30";
	if(month==2)
		if(year%4==0&&year%100!=0 || year%400==0)
			return "29";
		else
			return "28";
	return "31";
};

//\u8eab\u4efd\u8bc1\u4e2d\u7684\u7c4d\u8d2f\u4fe1\u606f
function IdCardArea(){
    var area={11:"\u5317\u4eac",12:"\u5929\u6d25",13:"\u6cb3\u5317",14:"\u5c71\u897f",15:"\u5185\u8499\u53e4",21:"\u8fbd\u5b81",22:"\u5409\u6797",23:"\u9ed1\u9f99\u6c5f",31:"\u4e0a\u6d77",32:"\u6c5f\u82cf",33:"\u6d59\u6c5f",34:"\u5b89\u5fbd",35:"\u798f\u5efa",36:"\u6c5f\u897f",37:"\u5c71\u4e1c",41:"\u6cb3\u5357",42:"\u6e56\u5317",43:"\u6e56\u5357",44:"\u5e7f\u4e1c",45:"\u5e7f\u897f",46:"\u6d77\u5357",50:"\u91cd\u5e86",51:"\u56db\u5ddd",52:"\u8d35\u5dde",53:"\u4e91\u5357",54:"\u897f\u85cf",61:"\u9655\u897f",62:"\u7518\u8083",63:"\u9752\u6d77",64:"\u5b81\u590f",65:"\u65b0\u7586",71:"\u53f0\u6e7e",81:"\u9999\u6e2f",82:"\u6fb3\u95e8",00:"\u56fd\u5916"};
    return area;
};

//\u8eab\u4efd\u8bc1\u7684\u9a8c\u8bc1		<!--{idcard}-->
function validIdCard(strVal,name)
{
        //add by chris on 2006-03-20,\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad
        var areaArray=IdCardArea();

	var count = 0;
	var numArray = "0123456789";

	strVal =trim(strVal);
	var strSfzhm = strVal;

        if(isExsitChineseChar(strVal))
        {
           alert("\""+name+"\"\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\uff01");
        }

	if ((strSfzhm.length !=15 ) && (strSfzhm.length!=18))
	{
		alert("["+name+"]\u5904\u5e94\u8f93\u516515\u4f4d\u621618\u4f4d!");
		return false;
	}
	else
	{
		if (strSfzhm.length == 15)
		{
			for(var i=0;i<15;i++)
			{
				tmpChar = strSfzhm.charAt(i);
				if ( numArray.indexOf(tmpChar) == -1 )
				{
					count = count+1;
				}
			}
			if(count!=0)
			{
				alert("["+name+"]\u590415\u4f4d\u6570\u5b57\u8f93\u5165\u6709\u8bef!");
				return false;
			}
		}
		else			//18\u4f4d\u7684SFZHM\u6821\u9a8c
		{
			for(i=0;i<17;i++)
			{
				tmpChar = strSfzhm.charAt(i);
				if(numArray.indexOf(tmpChar)==-1)
				{
					alert("["+name+"]\u590418\u4f4d\u65f6\u524d17\u6570\u5b57\u8f93\u5165\u6709\u8bef!");
					return false;
				}
			}
			var lastchar = strSfzhm.charAt(17).toLowerCase();
			var denominator='abcdefghijklmnopqrstuvwxyz0123456789';
			if(denominator.indexOf(lastchar) == -1)
			{
				alert("["+name+"]18\u4f4d\u5e94\u4e3a\u6570\u5b57\u6216\u5b57\u6bcd!");
				return false;
			}
		}
                    //\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad\uff0cadd by chris on 2006-03-20
                     var sub=strVal.substr(0,2);
                     if((typeof areaArray[sub]) =="undefined"){
                        alert("["+name+"]\u524d\u4e24\u4f4d\u6570\u5b57\u5bf9\u5e94\u7684\u7c4d\u8d2f\u4e3a\"\u4e0d\u660e\u7701\u4efd\"!");
                        return false;
                     }
	}
	return true;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u624b\u673a\u53f7\u7801\u662f\u5426\u6b63\u786e
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function checkMobile( s ){
	var regu =/^[1][3][0-9]{9}$/;
	var re = new RegExp(regu);
	if (re.test(s)) {
	  return true;
	}else{
	  return false;
	}
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u90ae\u7f16\u683c\u5f0f\u662f\u5426\u6b63\u786e
*/
function checkPostCode(s){
   if(s.length==6&&isNumber(s)){
      return true;
   }
   else
     return false;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u6b63\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumber( s ){
	var regu = "^[0-9]+$";
	var re = new RegExp(regu);
	if (s.search(re) != -1) {
	   return true;
	} else {
	   return false;
	}
};

function isInteger( s ){
	var regu1 = "^[0-9]+$";
	var regu2 = "^-[0-9]+$";
	var re1 = new RegExp(regu1);
	var re2 = new RegExp(regu2);

	if (s.search(re1) != -1 || s.search(re2) != -1) {
	   return true;
	} else {
	   return false;
	}
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u8d1f\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNegativeNumber( s ){
	var regu = "^-[0-9]+$";
	var re = new RegExp(regu);
	if (s.search(re) != -1) {
	   return true;
	} else {
	   return false;
	}
};

/*
  \u7528\u9014:\u65e5\u671f\u8054\u60f3\u8f93\u5165\u6cd5
*/
function smartDay(obj){
  var year,month,day;
  var date=obj.value;
  if(date.length==8){
     if(isNumber(obj.value)){
       	year=date.substr(0,4);
	month=date.substr(4,2);
	day=date.substr(6,2);
        obj.value=year+"-"+month+"-"+day;
    }
  }

};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u53ea\u7531\u82f1\u6587\u5b57\u6bcd\u548c\u6570\u5b57\u7ec4\u6210
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumberOrChar( s ){    //\u5224\u65ad\u662f\u5426\u662f\u6570\u5b57\u6216\u5b57\u6bcd
	var regu = "^[0-9a-zA-Z]+$";
	var re = new RegExp(regu);
	if (re.test(s)) {
	  return true;
	}else{
	  return false;
	}
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u53ef\u4ee5\u662f\u8d1f\u6570
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDecimal( str ){
        if(isNumber(str)||isNegativeNumber(str)) return true;
	var re = /^[-]{0,1}(\d+)[\.]+(\d+)$/;
	if (re.test(str)) {
	   //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
	   return true;
	} else {
	   return false;
	}
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveDecimal( str ){
       if(isNumber(str)) return true;

	var re = /^(\d+)[\.](\d+)$/;
	if (re.test(str)) {
	   //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
	   return true;
	} else {
	   return false;
	}
};
function isNegativeDecimal( str ){
       if(isNegativeNumber(str)) return true;

	var re = /^-(\d+)[\.](\d+)$/;
	if (re.test(str)) {
	   //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
	   return true;
	} else {
	   return false;
	}
};
/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
	s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
	\u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveInteger( str ){
       if(isNumber(str)) return true;
	var re = /^(\d+)(\d+)$/;
	if (re.test(str)) {
	   if(RegExp.$1==0&&RegExp.$2==0) return false;
	   return true;
	} else {
	   return false;
	}
};

function checkNegativeDecimal(obj){

	var strTemp;
	var numpric;
	var numLen;
	var strArr;
	try{
                if(isNegativeDecimal(obj.value)==false){
                   alert("\""+obj.chname+"\"\u8f93\u5165\u5fc5\u987b\u4e3a\u8d1f\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u8d1f\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206\uff01");
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
			   if(numType != null&& numType !=""&&numType.indexOf(",")!=-1){
	                  // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
	                   strArr = numType.split(",");
	                   numLen = Math.abs( strArr[0] );
	                   numpric = Math.abs( strArr[1] );
	                  
	                  return f_checkNumLenPrec(obj,numLen, numpric);
	             }
            }
	}catch(e){
		alert("in checkDecimal = " + e);
	}
};

function checkPositiveDecimal(obj){

	var strTemp;
	var numpric;
	var numLen;
	var strArr;
	try{
                if(isPositiveDecimal(obj.value)==false){
                   alert("\""+obj.chname+"\"\u8f93\u5165\u5fc5\u987b\u4e3a\u6b63\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6b63\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206\uff01");
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
			   if(numType != null&& numType !=""&&numType.indexOf(",")!=-1){
	                  // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
	                   strArr = numType.split(",");
	                   numLen = Math.abs( strArr[0] );
	                   numpric = Math.abs( strArr[1] );
	                  
	                  return f_checkNumLenPrec(obj,numLen, numpric);
	             }
            }
	}catch(e){
		alert("in checkDecimal = " + e);
	}
};

function checkDecimal(obj){

	var strTemp;
	var numpric;
	var numLen;
	var strArr;
	try{
            if(isDecimal(obj.value)==false){
               alert("\""+obj.chname+"\"\u8f93\u5165\u5fc5\u987b\u4e3a\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206\uff01");
               return false;
            }
            if(obj.precision){
                 var numType=obj.precision;
                   
			   if(numType != null&& numType !=""&&numType.indexOf(",")!=-1){
	                  // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
	                   strArr = numType.split(",");
	                   numLen = Math.abs( strArr[0] );
	                   numpric = Math.abs( strArr[1] );
	                  
	                  return f_checkNumLenPrec(obj,numLen, numpric);
	             }
            }
	}catch(e){
		alert("in checkDecimal = " + e);
	}
};

function f_checkNumLenPrec(obj, len, pric){
	var numReg;
	var value = obj.value;
	var strValueTemp, strInt, strDec;
	//alert(value + "=====" + len + "====="+ pric);
	try{
	
		numReg =/[\-]/;
		strValueTemp = value.replace(numReg, "");
		strValueTemp = strValueTemp.replace(numReg, "");
		//\u6574\u6570
		if(pric==0){
			numReg =/[\.]/;
			//alert(numReg.test(value));
			if(numReg.test(value) == true){
				alert("\""+obj.chname+"\"\u8f93\u5165\u5fc5\u987b\u4e3a\u6574\u6570\u7c7b\u578b!");
				return false;
			}
		}

		if(strValueTemp.indexOf(".") < 0 ){
			//alert("lennth==" + strValueTemp);
			if(strValueTemp.length > len){
				alert("\""+obj.chname+"\"\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7"+ len +"\u4f4d");
				return false;
			}

		}else{
			strInt = strValueTemp.substr( 0, strValueTemp.indexOf(".") );
			//alert("lennth==" + strInt);
			if(strInt.length > len ){
				alert("\""+obj.chname+"\"\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7"+ len  +"\u4f4d");
				return false;
			}

			strDec = strValueTemp.substr( (strValueTemp.indexOf(".")+1), strValueTemp.length );
			//alert("pric==" + strDec);
			if(strDec.length > pric){
				alert("\""+obj.chname+"\"\u5c0f\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7"+  pric +"\u4f4d");
				return false;
			}
		}

		return true;
	}catch(e){
		alert("in f_checkNumLenPrec = " + e);
		return false;
	}
};


function emailCheck (emailStr,name)
{
	var checkTLD=1;
	var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
	var matchArray=emailStr.match(emailPat);

	if (matchArray==null)
	{
		alert("["+name+"]\u65e0\u6548,\u5fc5\u987b\u542b\u6709'@'\u548c'.'\uff01");
		return false;
	}
	var user=matchArray[1];
	var domain=matchArray[2];

	for (i=0; i<user.length; i++)
	{
		if (user.charCodeAt(i)>127)
		{
			alert("["+name+"]\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01");
			return false;
		}
	}
	for (i=0; i<domain.length; i++) {
		if (domain.charCodeAt(i)>127) {
			alert("["+name+"]\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01");
			return false;
		}
	}

	if (user.match(userPat)==null)
	{
		alert("["+name+"]\u6709\u8bef!");
		return false;
	}

	var IPArray=domain.match(ipDomainPat);
	if (IPArray!=null)
	{
		for (var i=1;i<=4;i++)
		{
			if (IPArray[i]>255)
			{
				alert("["+name+"]\u8f93\u5165\u6709\u8bef!");
				return false;
			}
		}
		return true;
	}

	var atomPat=new RegExp("^" + atom + "$");
	var domArr=domain.split(".");
	var len=domArr.length;
	for (i=0;i<len;i++)
	{
		if (domArr[i].search(atomPat)==-1)
		{
			alert("["+name+"]\u4e2d\u7684\u57df\u540d\u6709\u8bef\uff01");
			return false;
		}
	}

	if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1)
	{
		alert("["+name+"]\u4e2d\u5fc5\u987b\u4ee5\u4e00\u4e2a\u6709\u7528\u57df\u540d\u7ed3\u675f\uff01");
		return false;
	}

	if (len<2)
	{
		alert("["+name+"]\u6ca1\u6709\u8f93\u5165\u4e3b\u673a\u5730\u5740!");
		return false;
	}
	return true;
} ;
window.addOnloadEvent = function(f) {
    // If already loaded, just invoke f() now.
    if (addOnloadEvent.loaded) f();
    // Otherwise, store it for later
    else addOnloadEvent.funcs.push(f);
};
// The array of functions to call when the document loads
addOnloadEvent.funcs = [];
// The functions have not been run yet.
addOnloadEvent.loaded = false;

// Run all registered functions in the order in which they were registered.
// It is safe to call addOnloadEvent.run() more than once: invocations after the
// first do nothing. It is safe for an initialization function to call
// addOnloadEvent() to register another function.
addOnloadEvent.run = function() {
    // If we've already run, do nothing
    if (addOnloadEvent.loaded) return;
    for(var i = 0; i < addOnloadEvent.funcs.length; i++) {
        try { addOnloadEvent.funcs[i](); }
        catch(e) { /* An exception in one function shouldn't stop the rest */ }
    }
    // Remember that we've already run once.
    addOnloadEvent.loaded = true;
    // But don't remember the functions themselves.
    delete addOnloadEvent.funcs;
    // And forget about this function too!
    delete addOnloadEvent.run;
};

// Register addOnloadEvent.run() as the onload event handler for the window
if (window.addEventListener)
    window.addEventListener("load", addOnloadEvent.run, false);
else if (window.attachEvent) window.attachEvent("onload", addOnloadEvent.run);
else window.onload = addOnloadEvent.run;
//\u6570\u636e\u7ed1\u5b9a\u5230\u63a7\u4ef6
function BindDataToCtrl(id, eleVal,ctrlext) {
    //\u901a\u8fc7id\u83b7\u53d6\u63a7\u4ef6\u5bf9\u8c61\u6570\u7ec4\u3002
    var obj = $("#" + id) ;

	//\u53ea\u80fd\u9488\u5bf9\u5176\u4e2d\u4e00\u4e2a\uff08id\u552f\u4e00\uff09
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d\u79f0
        var tagNam = getTageName(obj[0]);
			//input
        if ("input" == tagNam) {
            //\u6587\u672c\u6846
			
            if ("text" == obj[0].type) {
                obj[0].value = eleVal == null ? "" : eleVal;
            } else
				//\u5355\u9009\u6309\u94ae
            if ("radio" == obj[0].type) {
                BindDataToRadioCtrl(obj, eleVal);
            } else 
				//\u590d\u9009\u6309\u94ae
            if ("checkbox" == obj[0].type) {
                BindDataToRadioMultCtrl(obj, eleVal);
            } else 
            if ("hidden" == obj[0].type) {
                obj[0].value = eleVal == null ? "" : eleVal;
            } else
			{
				
				obj[0].value = eleVal;
			}
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            BindDataToSelectCtrl(obj, eleVal,ctrlext);
        }
        //\u6811\u578b\u6309\u94ae
        else if ("tree" == tagNam) {
            var tree = new Tree_Factory(eleVal, id);
            tree.initTree();
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
           SSB_RadioGroup.radioGroupObjs[id].setValue(eleVal);
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            setCalendarValue(id, eleVal);
        }
        //table
        else if ("table" == tagNam) {
            setTableValue(id, eleVal);
        }
        //Label
        else if ("label" == tagNam) {
            if (!isIE()) {
                obj[0].textContent = eleVal;
            } else {
                obj[0].innerText = eleVal;
            }
        }
        //ip
        else if("ip" == tagNam) {
        	SSB_IP.ipObjs[id].setValue(eleVal);
        }
        //\u9ed8\u8ba4\u60c5\u51b5
        else
        {
            obj[0].value = eleVal;
        }
    }
};

//\u4ece\u63a7\u4ef6\u4e0a\u53d6\u503c
function GetDataFromCtrl(id) {
    //\u63a7\u4ef6\uff08\u6570\u7ec4\uff09
    var obj = $("#" + id) ;
	//id\u552f\u4e00\uff0c\u6240\u4ee5obj\u552f\u4e00
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d
        var tagNam = getTageName(obj[0]);
	        //\u6587\u672c\u6846
        if ("input" == tagNam) {
            if ("text" == obj[0].type) {
                return obj[0].value;
            } else if ("radio" == obj[0].type) {
                return GetDataFromRadioCtrl(obj);
            } else if ("checkbox" == obj[0].type) {
                return GetDataFromRadioMultCtrl(obj);
            } else if ("hidden" == obj[0].type) {
                return obj[0].value;
            } else if("file" == obj[0].type){
				__file_List.push(document.getElementById(id));
				return obj[0].value;
			}else{				
   			    return obj[0].value;
			} 
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            return GetDataFromSelectCtrl(obj[0]);
        }
        //\u81ea\u5b9a\u4e49\u6811
        else if ("tree" == tagNam) {
            var tree = SSB_Tree.trees[obj.attr("id")];
            var ret = tree.getValue();
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
            var ret = SSB_RadioGroup.radioGroupObjs[obj.attr("id")].getValue();
            return ret;
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            var ret = getCalendarValue(id);
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u8868\u683c
        else if ("table" == tagNam) {
            var ret = getTable_Trs(id);
            return ret;
        }
        //Label
        else if ("label" == tagNam) {
            if (!isIE()) {
                return obj[0].textContent;
            } else {
                return obj[0].innerText;
            }
        }else if("upload"==tagNam){
        	return GetDataFromUploadCtrl(obj[0]);
        }
//ip
        else if("ip" == tagNam) {
        	var ret = SSB_IP.ipObjs[obj.attr("id")].getValue();
            return ret;	
        }else{
            //\u9ed8\u8ba4
            return obj[0].value;
        }
    }
};


//\u53d6\u6807\u7b7e\u540d\u79f0\uff0c\u907f\u514did\u548cfirefox\u6709\u4e0d\u540c\u7684\u6807\u7b7e\u540d\u79f0
function getTageName(obj) {
    //\u6587\u672c\u6846
    if ("input" == (obj.tagName).toLowerCase()) {
        return "input";
    }
	//\u4e0b\u62c9
    if ("select" == (obj.tagName).toLowerCase()) {
        return "select";
    }
	//\u81ea\u5b9a\u4e49\u6309\u94ae\u7ec4
    if ("radiogroup" == (obj.tagName).toLowerCase() || "z:radiogroup" == (obj.tagName).toLowerCase()) {
        return "radiogroup";
    }
	//\u81ea\u5b9a\u4e49\u65e5\u5386\u63a7\u4ef6
    if ("calendar" == (obj.tagName).toLowerCase() || "z:calendar" == (obj.tagName).toLowerCase()) {
        return "calendar";
    }
	//\u81ea\u5b9a\u4e49\u8868\u683c\u63a7\u4ef6
    if ("table" == (obj.tagName).toLowerCase() || "z:table" == (obj.tagName).toLowerCase()) {
        return "table";
    }
	//\u81ea\u5b9a\u4e49\u6811\u578b\u63a7\u4ef6
    if ("tree" == (obj.tagName).toLowerCase() || "z:tree" == (obj.tagName).toLowerCase()) {
        return "tree";
    }
	//label\u6807\u7b7e
    if ("label" == (obj.tagName).toLowerCase()) {
        return "label";
    }
    if("upload"==(obj.tagName).toLowerCase()||"z:upload"==(obj.tagName).toLowerCase()){
    	return "upload";
    }
     //ip\u6807\u7b7e
    if("ip" == (obj.tagName).toLowerCase() || "z:ip" == (obj.tagName).toLowerCase()) {
    	return "ip";
    }
    return "other";
};


//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioMultCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
	//\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
	//\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
    for (var i = 0; i < radioObjs.length; i++) {
        for (var j = 0; j < elementvalue.length; j++) {
            var radioObj = radioObjs[i] ;
            var varV = elementvalue[j] ;
			//\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9					
            if (varV == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
        }
    }
};

//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
	//\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
	//\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
    for (var i = 0; i < radioObjs.length; i++) {
       
            var radioObj = radioObjs[i] ;
            var varV = elementvalue;
			//\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9					
            if (varV == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
       
    }
};
		
//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioMultCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn = new Array();
    var j = 0 ;
	//\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    for (var i = 0; i < radioObjs.length; i++) {
        var radioObj = radioObjs[i] ;
        if (radioObj.checked) {
            rtn[j++] = radioObj.getAttribute("value");
        }
    }
    return rtn;
};

//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn = "";
    var j = 0 ;
	//\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    for (var i = 0; i < radioObjs.length; i++) {
        var radioObj = radioObjs[i] ;
        if (radioObj.checked) {
            return radioObj.getAttribute("value");
        }
    }
    return rtn;
};

//\u7ed1\u5b9a\u503c\u5230\u4e0b\u62c9\u6846
function BindDataToSelectCtrl(obj, elementvalue,ctrlext ) {

    //\u6709\u91cd\u590d\u64cd\u4f5c\uff0c\u4f18\u5316\uff01
    var selectObj = document.getElementById(obj.attr("id"));
	var refObj = elementvalue ;

    if (undefined == ctrlext )
    {
		for (var i = 0; i < selectObj.length; i++) {
            //\u5224\u65ad\u88ab\u9009\u4e2d\u7684\u4e0b\u62c9\u5143\u7d20
            var optA = selectObj.options[i] ;
            if (!selectObj.getAttribute('multiple') && elementvalue == optA.value) {
                optA.selected = true;
            }
            else if(selectObj.getAttribute('multiple'))
            {
            	for(var m = 0;m < elementvalue.length; m++)
            	{
            		if(elementvalue[m] == optA.value)
            		{
            			optA.selected = true;
            		}
            	}
            }
        }
    } else {
		var textName = ctrlext.text;
		var valueName = ctrlext.value;	
	    var textArr = refObj[textName] ;
		var valueArr = refObj[valueName];
		var defaultValue = refObj[ctrlext.defaultValue];
		    	
        var k = selectObj.length;
        for (var j = 0; j < k; j++) {
            selectObj.remove(0);
        }
		//\u91cd\u65b0\u751f\u6210option
        for (var i = 0; i < textArr.length  && textArr.length  == valueArr.length ; i++) {
            var optA = document.createElement("OPTION");
            optA.text = textArr[i];
            optA.value = valueArr[i];
            selectObj.options.add(optA);
			//\u5224\u65ad\u88ab\u9009\u62e9\u7684\u4e0b\u62c9\u5143\u7d20
            if (!selectObj.getAttribute('multiple') && defaultValue == optA.value) {								
                optA.selected = true;                
            }
            else if(selectObj.getAttribute('multiple'))
            {
            	for(var n = 0;n < elementvalue.length; n++)
            	{
            		if(defaultValue[n] == optA.value)
            		{
            			optA.selected = true;
            		}
            	}            	
            }                  
        }
	}
};


//\u83b7\u53d6\u4e0b\u62c9\u7684\u9009\u4e2d\u503c
function GetDataFromSelectCtrl(obj) {
    var ret = new Array();
    var j = 0 ;
    if (!obj.getAttribute('multiple')) {
        return obj.value;
    }
	//\u4ece\u6240\u6709\u7684\u4e0b\u62c9\u5143\u7d20\u4e2d\u5224\u65ad\u88ab\u9009\u62e9\u7684\u5143\u7d20\u503c
    for (var i = 0; i < obj.length; i++) {
        var optA = obj.options[i] ;
        if (optA.selected) {
            ret[j++] = optA.value;
        }
    }
    return ret;
};

//\u5224\u65adie\u7c7b\u578b
function isIE() {
    return window.navigator.userAgent.indexOf("MSIE") != -1;
};
var __file_List=[];
//\u83b7\u53d6\u4e0a\u4f20\u6587\u4ef6\u63a7\u4ef6\u7684\u503c,\u53ea\u662f\u6587\u4ef6\u8def\u5f84
function GetDataFromUploadCtrl(obj){
	var values=[];
	var inputs=obj.getElementsByTagName('INPUT');
	for(var i=0;i<inputs.length;i++){
		if(inputs[i].type=='file')
		{	
					
			__file_List.push(inputs[i]);				
		}

		values.push(inputs[i].value);
	}
	return values;
}
/**
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a1
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-18
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9: \u6dfb\u52a0\u7c7b\u5c5e\u6027
 * \u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00396183
 */
//constant
var RIA_BIND_TAG = "bind";
var RIA_S_TAG = "s";
var RIA_Z_TAG = "z:";
var RIA_INSTANCE_TAG = "instance";
var RIA_SERVICE_TAG = "service";
var RIA_SETVALUE_TAG = "setvalue";
var RIA_INIT_TAG = "init";
var RIA_REQUEST_TAG = "request";
var RIA_RESPONSE_TAG = "response";
var RIA_FORWARD_TAG = "forward";
var RIA_LOADING_TAG = "loading";
var RIA_ZREQUEST_TAG = "Z:REQUEST";
var RIA_ZRESPONSE_TAG = "Z:RESPONSE";
var RIA_ZFORWARD_TAG = "Z:FORWARD";
var RIA_ZLOADING_TAG = "Z:LOADING";
var RIA_DOT_TAG = ".";
//loadingtag class properties
var LOADING_ID_TAG ="id";
var LOADING_ISSYN_TAG ="isSyn";
var LOADING_DIVID_TAG = "divId";
var LOADING_IMG_TAG ="img";
var LOADING_DELAY_TAG ="delay";
var LOADING_WIDTH_TAG ="width";
var LOADING_HEIGHT_TAG ="height";

//Bindtag class properties
var BIND_ID_TAG = 'id';

var BIND_CTRL_TAG = 'ctrl';

var BIND_REF_TAG = 'ref';

var BIND_AUTOCHECK_TAG = 'autocheck';

var BIND_DATATYPE_TAG = 'datatype';

var BIND_VALIDATE_TAG = 'validate';

var BIND_CTRLEXT_TAG = 'ctrlext';

//RequestTag class properties

var REQUEST_ID_TAG = 'id';

var REQUEST_PARA_TAG = 'para';

var REQUEST_PREREQUEST_TAG = 'prerequest';

var REQUEST_POSTREQUEST_TAG = 'postrequest';

//ResponseTag class properties

var RESPONSE_ID_TAG = 'id';

var RESPONSE_RTN_TAG = 'rtn';

var RESPONSE_PRERESPONSE_TAG = 'preResponse';

var RESPONSE_POSTRESPONSE_TAG = 'postResponse';

//InstanceTag class properties

var INSTANCE_OBJNAME_TAG = 'var';

var INSTANCE_OBJTYPE_TAG = 'type';

//ForwardTag class properties

var FORWARD_ID_TAG = 'id';

var FORWARD_TARGET_TAG = 'target';

var FORWARD_TEST_TAG = 'test';

var FORWARD_ACTION_TAG = 'action';

var FORWARD_ONFORWARD_TAG = 'onforward';

//ServiceTag class properties

var SERVICE_ID_TAG = 'id';

var SERVICE_URL_TAG = 'url';

var SERVICE_ISSYN_TAG = 'isSyn';

var SERVICE_CACHE_TAG = 'cache';

var SERVICE_EXPIRES_TAG = 'expires';

var SERVICE_ONERROR_TAG = 'onError';

var SERVICE_REQUESTOBJ_TAG = 'reqeuestObj';

var SERVICE_RESPONSEOBJ_TAG = 'responseObj';

var SERVICE_FORWARDOBJ_TAG = 'forwardObj';

//InitTag class properties

var INIT_ID_TAG = 'id';

var INIT_CTRLID_TAG = 'ctrl';

var INIT_REF_TAG = 'ref';

var INIT_VALUE_TAG = 'value';

//\u53c2\u6570\u5b9e\u4f53
var _para = {};

//define loading class
var LoadingTag = function()
{
	this.id='';
	this.isSyn='';
	this.divId='';
	this.img='';
    this.delay='';
	this.width='';
    this.height='';

};

//define BindTag class

var BindTag = function ()
{

    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.autocheck = '';

    this.datatype = '';

    this.validate = '';

    this.ctrlext = '';

};

//define RequestTag class

var RequestTag = function ()
{

    this.id = '';

    this.para = '';

    this.preRequest = '';

    this.postRequest = '';
};

//define ResponseTag class

var ResponseTag = function ()
{

    this.id = "";

    this.rnt = '';

    this.preResponse = '';

    this.postResponse = '';

};

//define  InstanceTag class

var InstanceTag = function ()
{

    this.objName = '';

    this.objType = '';
};

//define ForwardTag class 

var ForwardTag = function ()
{

    this.id = '';

    this.target = '';

    this.test = '';

    this.action = '';

    this.onforward = '';

};

//define ServiceTag class

var ServiceTag = function ()
{
    this.id = '';

    this.method = '';

    this.url = '';
	
	this.isSyn = '';
     
	this.expires = '';
	
	this.cache = '';
	 
    this.requestObj = '';

    this.responseObj = '';

    this.forwardObj = '';
    
	this.loadingObj ='';
	
    this.onError = "";
};

//define InitTag class 
var InitTag = function()
{
    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.value = '';

};

//define PageContextCache class
var PageContextCache = function () {
    PageContextCache.prototype.bindArr = new Array();

    PageContextCache.prototype.instanceArr = new Array();

    PageContextCache.prototype.serviceArr = new Array();

    PageContextCache.prototype.initArr = new Array();	
};

//define PageContextCache class method getService

PageContextCache.prototype.getService = function(serviceId)
{
    if (serviceId == null || serviceId == "")
        log.info("errorCode is" + RIA_ERROR_CALLSID + " error description:Error encountered when retrieving the service id,please check if there is a corresponding service id declared.");
    var serviceTags = new ServiceTag();
    for (var i = 0; i < PageContextCache.prototype.serviceArr.length; i++) {
        if (PageContextCache.prototype.serviceArr[i].id == serviceId) {
            serviceTags = PageContextCache.prototype.serviceArr[i];
        }
    }
    return serviceTags;
};

//define PageContextCache class method getBind

PageContextCache.prototype.getBind = function(ctrlid)
{
    var bindtags = new BindTag();
    for (var i = 0; i < PageContextCache.prototype.bindArr.length; i++) {
        if (PageContextCache.prototype.bindArr[i].ctrlid == ctrlid) {
            bindtags = PageContextCache.prototype.bindArr[i];
        }
    }

    return bindtags;
};

//define PageContextCache class method getInstance

PageContextCache.prototype.getInstance = function(objName)
{
    var instanceTags = new InstanceTag();
    for (var i = 0; i < PageContextCache.prototype.instanceArr.length; i++) {
        if (PageContextCache.prototype.instanceArr[i].objName == objName) {
            instanceTags = PageContextCache.prototype.instanceArr[i];
        }
    }
    return instanceTags;
};

//define PageContextCache class method getRequest

PageContextCache.prototype.getRequest = function(serviceId)
{
    var requestTags = new RequestTag();
    requestTags = PageContextCache.prototype.getService(serviceId).requestObj;
    return requestTags;
};

//define PageContextCache class method getResponse

PageContextCache.prototype.getResponse = function(serviceId)
{
    var responseTags = new ResponseTag();
    responseTags = PageContextCache.prototype.getService(serviceId).responseObj;
    return responseTags;
};
//define PageContextCache class method getForward

PageContextCache.prototype.getForward = function(serviceId)
{
    var forwardTags = new Array();
    forwardTags = PageContextCache.prototype.getService(serviceId).forwardObj;
    return forwardTags;
};
//define PageContextCache class method getloading
PageContextCache.prototype.getLoading = function(serviceId)
{
    var loadingTags = new Array();
    loadingTags = PageContextCache.prototype.getService(serviceId).loadingObj;
    return loadingTags;
};
//define pageContextCache class menthod getUrl
PageContextCache.prototype.getUrl = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceUrl = serviceTag.url;
    return serviceUrl;
};

//define pageContextCache class menthod getIsSyn
PageContextCache.prototype.getIsSyn = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceIsSyn = serviceTag.isSyn;
	
	//\u8fd4\u56de\u662f\u5426\u5f02\u6b65
    return serviceIsSyn;
};

var pageContextCache = new PageContextCache();
//\u5b9a\u4e49\u91cd\u590d\u63d0\u4ea4\u6309\u94ae\u7684\u7f13\u5b58
var cacheMultiUrl = new Array();
var riatags_init = function () {

    //------bindTag----------------------------------
    var bindtag = new BindTag();
    var binds = getTagValue(RIA_BIND_TAG, RIA_S_TAG);
	
	//\u5c01\u88c5\u5230Array
    for (var i = 0; i < binds.length; i++) {
        var tmp1 = encapsBindTag(binds[i]);
        if (tmp1.autocheck || tmp1.autocheck == "true")
        {
            //datatype \u4e0d\u80fd\u4e3a\u7a7a
            if (tmp1.datatype != null && tmp1.datatype != "" && document.getElementById(tmp1.ctrlid)) {
                var input = {};
                input.datatype = tmp1.datatype;
                input.name = document.getElementById(tmp1.ctrlid).name;
                input.value = document.getElementById(tmp1.ctrlid).value;
				document.getElementById(tmp1.ctrlid).onblur = function(){
					verifyInput(input,input.datatype);
				};
            }

        }
//		if(tmp1.ctrlext)
//		{
//			var ctrlextObj = new Array();
//			ctrlextObj[0]=tmp1.ctrlid;
//			var objectctrl = new Object();
//		    var temparr =tmp1.ctrlext.split(";");
//			for(var j =0;j<temparr.length;j++)
//			{
//				var singletext =temparr[j].split(":");
//				var firsttext =singletext[0];
//				var secondtext = singletext[1];
//				objectctrl[firsttext]=secondtext;
//			}
//			GetDataFromCtrl(ctrlextObj);
//		}
        pageContextCache.bindArr.push(tmp1);
    }
	
//-------instanceTag----------------------------------------
    var instances = getTagValue(RIA_INSTANCE_TAG, RIA_S_TAG);
	
	//\u5c01\u88c5\u5230Array
    for (var i = 0; i < instances.length; i++) {
        pageContextCache.instanceArr.push(encapsInstanceTag(instances[i]));
    }

	
//---------ServiceTag---------------------------------------
    var services = getTagValue(RIA_SERVICE_TAG, "");
	
	//\u5c01\u88c5\u5230Array
    for (var i = 0; i < services.length; i++) {
        pageContextCache.serviceArr.push(encapsServiceTag(services[i]));
    }
	
//-------initTag----------------------------

    var initTag = new InitTag();
    var inits = getTagValue(RIA_SETVALUE_TAG, "");
	
	//\u5c01\u88c5\u5230Array
    for (var i = 0; i < inits.length; i++) {
        pageContextCache.initArr.push(encapsInitTag(inits[i]));
    }
	
	//_para\u5bf9\u8c61
    creatParaBean();
	
	//\u521d\u59cb\u5316init\u6807\u7b7e
    setValues();

    return pageContextCache;
};

/**
 * \u5c01\u88c5bind\u4fe1\u606f
 * @param {Object} node
 */
var encapsBindTag = function (node) {
    var newbindtag = new BindTag();
    newbindtag.id = node.getAttribute(BIND_ID_TAG);
    newbindtag.ctrlid = node.getAttribute(BIND_CTRL_TAG);
    newbindtag.ref = node.getAttribute(BIND_REF_TAG);
    newbindtag.autocheck = node.getAttribute(BIND_AUTOCHECK_TAG);
    newbindtag.datatype = node.getAttribute(BIND_DATATYPE_TAG);
    newbindtag.validate = node.getAttribute(BIND_VALIDATE_TAG);
    newbindtag.ctrlext = node.getAttribute(BIND_CTRLEXT_TAG);

    return newbindtag;
};

/**
 * \u5c01\u88c5InstanceTag
 * @param {Object} node
 */
var encapsInstanceTag = function (node) {
    var newinstanceTag = new InstanceTag();
    newinstanceTag.objName = node.getAttribute(INSTANCE_OBJNAME_TAG);
    newinstanceTag.objType = node.getAttribute(INSTANCE_OBJTYPE_TAG);
    return newinstanceTag;
};
/**
 * \u5c01\u88c5InitTag
 * @param {Object} node
 */
var encapsInitTag = function (node)
{
    var newinitTag = new InitTag();
    newinitTag.id = node.getAttribute(INIT_ID_TAG);
    newinitTag.ctrlid = node.getAttribute(INIT_CTRLID_TAG);
    newinitTag.ref = node.getAttribute(INIT_REF_TAG);
    newinitTag.value = node.getAttribute(INIT_VALUE_TAG);
    return newinitTag;
};
/**
 * \u5c01\u88c5\u670d\u52a1
 * @param {Object} node
 */
var encapsServiceTag = function (node)
{
    var newserviceTag = new ServiceTag();
    newserviceTag.id = node.getAttribute(SERVICE_ID_TAG);
    newserviceTag.url = node.getAttribute(SERVICE_URL_TAG);
	newserviceTag.isSyn = node.getAttribute(SERVICE_ISSYN_TAG);
	newserviceTag.onError = node.getAttribute(SERVICE_ONERROR_TAG);
    //\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
	newserviceTag.cache = node.getAttribute(SERVICE_CACHE_TAG);
	newserviceTag.expires = node.getAttribute(SERVICE_EXPIRES_TAG);
	//\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f 
    if (isIEorFF())
    {
        var forwards = new Array();
        for (var i = 0; i < node.childNodes.length; i++) {
            //request\u5bf9\u8c61
            if (node.childNodes[i].tagName == RIA_REQUEST_TAG)
            {
                newserviceTag.requestObj = encapsRequestTag(node.childNodes[i]);
            }
            //response\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_RESPONSE_TAG)
            {
                newserviceTag.responseObj = encapsResponseTag(node.childNodes[i]);
            }
            //forward\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_FORWARD_TAG)
            {
                forwards.push(encapsForwardTag(node.childNodes[i]));
            }
			//loading\u5bf9\u8c61
			else if (node.childNodes[i].tagName == RIA_LOADING_TAG)
			{
				newserviceTag.loadingObj=encapsLoadingTag(node.childNodes[i]);
			}
        }
        newserviceTag.forwardObj = forwards;
    }

    else {
        var forwords = new Array();
        while (true) {
            //\u5224\u65ad\u662f\u5426\u5b58\u5728\u5bf9\u8c61
            if (node.childNodes[1] == undefined) {
                break;
            } else {
                node = node.childNodes[1];
				
				//request\u5bf9\u8c61
                if (node.tagName == RIA_ZREQUEST_TAG)
                {
                    newserviceTag.requestObj = encapsRequestTag(node);
                }
                //response\u5bf9\u8c61
                else if (node.tagName == RIA_ZRESPONSE_TAG)
                {
                    newserviceTag.responseObj = encapsResponseTag(node);
                }
                //forward\u5bf9\u8c61
                else if (node.tagName == RIA_ZFORWARD_TAG)
                {
                    forwords.push(encapsForwardTag(node));
                }
				//loading\u5bf9\u8c61
				else if (node.tagName == RIA_ZLOADING_TAG)
				{
					newserviceTag.loadingObj=encapsLoadingTag(node);
				}
            }
        }
        newserviceTag.forwardObj = forwords;
    }

    return newserviceTag;
};

/**
 * \u5c01\u88c5RequestTag
 * @param {Object} node
 */
var encapsRequestTag = function (node) {
    var newrequestTag = new RequestTag();
    newrequestTag.id = node.getAttribute(REQUEST_ID_TAG);
    newrequestTag.para = node.getAttribute(REQUEST_PARA_TAG);
    newrequestTag.preRequest = node.getAttribute(REQUEST_PREREQUEST_TAG);
    newrequestTag.postRequest = node.getAttribute(REQUEST_POSTREQUEST_TAG);

    return newrequestTag;
};
var encapsLoadingTag = function (node){
	var newloadingTag = new LoadingTag();
	newloadingTag.id = node.getAttribute(LOADING_ID_TAG);
	newloadingTag.isSyn = node.getAttribute(LOADING_ISSYN_TAG);
	newloadingTag.img = node.getAttribute(LOADING_IMG_TAG);
	newloadingTag.delay = node.getAttribute(LOADING_DELAY_TAG);
	newloadingTag.divId = node.getAttribute(LOADING_DIVID_TAG);
	newloadingTag.height = node.getAttribute(LOADING_HEIGHT_TAG);
	newloadingTag.width = node.getAttribute(LOADING_WIDTH_TAG);
	return newloadingTag;
};
/**
 * \u5c01\u88c5ResponseTag
 * @param {Object} node
 */
var encapsResponseTag = function (node) {
    var newresponseTag = new ResponseTag();
    newresponseTag.id = node.getAttribute(RESPONSE_ID_TAG);
    newresponseTag.rtn = node.getAttribute(RESPONSE_RTN_TAG);
    newresponseTag.preResponse = node.getAttribute(RESPONSE_PRERESPONSE_TAG);
    newresponseTag.postResponse = node.getAttribute(RESPONSE_POSTRESPONSE_TAG);

    return newresponseTag;
};

/**
 * \u5c01\u88c5ForwardTag
 * @param {Object} node
 */
var encapsForwardTag = function (node) {
    var newforwardTag = new ForwardTag();
    newforwardTag.id = node.getAttribute(FORWARD_ID_TAG);
    newforwardTag.target = node.getAttribute(FORWARD_TARGET_TAG);
    newforwardTag.test = node.getAttribute(FORWARD_TEST_TAG);
    newforwardTag.action = node.getAttribute(FORWARD_ACTION_TAG);
    newforwardTag.onforward = node.getAttribute(FORWARD_ONFORWARD_TAG);

    return newforwardTag;
};


/**
 * \u63d0\u53d6\u5143\u7d20\u4fe1\u606f
 * @param {Object} node
 */
var getTagValue = function (node, str) {
    try {
        if (isIEorFF())
        {
            return getIETagValue(node + str);
        }
        else {
            return getFFTagVlaue(RIA_Z_TAG + node);
        }
    }
    catch(e) {
        log.info("errorcode is:" + RIA_ERROR_SUPPORT_BROWSER + "errordescription is:" + e.message);
    }
};

/**
 * IE\u6d4f\u89c8\u5668
 * @param {Object} node
 */
var getIETagValue = function (node) {

    if (document.getElementsByTagName(node)[0] == undefined)
    {
        return new Array();
    }
    var ieNode = document.getElementsByTagName(node)[0].children;
    if (node == RIA_SERVICE_TAG || node == RIA_SETVALUE_TAG) {
        ieNode = document.getElementsByTagName(node);
    }
    return ieNode;
};

/**
 * FireFox\u6d4f\u89c8\u5668
 * @param {Object} node
 */
var getFFTagVlaue = function (node) {

    var length = document.getElementsByTagName(node).length;
    var foxNode = new Array(length);
    for (var i = 0; i < length; i++) {
        foxNode[i] = document.getElementsByTagName(node).item(i);
    }
    return foxNode;
};

/**
 * \u662f\u5426IE
 * @param {Object} node
 */
var isIEorFF = function () {
    if (navigator.appName == "Microsoft Internet Explorer")
    {
        return true;
    }
    return false;
};

/**
 * \u89e3\u6790init
 */
var setValues = function ()
{
    var initArr = pageContextCache.initArr;

    var initArr_index = initArr.length;
    if (initArr_index > 0)
    {
        for (var i = 0; i < initArr_index; i++)
        {
            var initTag = initArr[i];
            var init_ctrlid = initTag.ctrlid;
            var init_value = initTag.value;
            var init_ref = initTag.ref;
            if (init_ctrlid == null || init_ctrlid == undefined || init_ctrlid == "")
            {
                continue;
            }
            if (init_value)
            {
                document.getElementById(init_ctrlid).value = init_value;
            }
            else if (init_ref)
            {
                var init_ref_index = init_ref.indexOf(RIA_DOT_TAG);
                var para_value = _para[init_ref.substring(init_ref_index + 1)];
                document.getElementById(init_ctrlid).value = para_value == undefined ? "" : para_value;
            }
        }
    }
};

/**
 * \u83b7\u53d6url\u4e2d\u7684\u53c2\u6570\u4fe1\u606f \u5c01\u88c5\u5230para\u5bf9\u8c61\u4e2d
 */
var getRequestParaUrl = function ()
{
    //\u83b7\u53d6url
    var url = decodeURI(document.URL);
    return url;

};

/**
 * 
 * @param {Object}strrul
 */
var paraValue = function ()
{
    var str = new String();
    str = getRequestParaUrl();
    var i = 0;
    if (str != "")
    {
        i = str.indexOf("?");
        i = i + 1;
        str = str.substring(i, str.length);
        var arr = str.split("&");
        return arr;
    }
};
/**
 * \u521b\u5efapara\u5b9e\u4f53\u62e5\u6709\u7684\u5c5e\u6027\u7684
 */
var creatParaBean = function ()
{
    try {
        var value = paraValue();
        if (value != "")
        {
            for (var i = 0; i < value.length; i++)
            {
                var name = value[i].split("=");
                _para[name[0]] = name[1];
            }
        }
    } catch(e)
    {
        log.info("errorcode is " + RIA_ERROR_ENCAPSULATION_URL + "errordescription is" + e.message);
    }
};
//\u521d\u59cb\u5316\u6240\u6709\u6570\u636e
addOnloadEvent(riatags_init);
var FactoryXMLHttpRequest = function () {
    if( window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else if( window.ActiveXObject) {
        var msxmls = new Array(
            'Msxml2.XMLHTTP.5.0',
            'Msxml2.XMLHTTP.4.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP');
        for (var i = 0; i < msxmls.length; i++) {
            try {
                return new ActiveXObject(msxmls[i]);
            } catch (e) {
            }
        }
    }
    throw new Error( "Could not instantiate factory");
};


var OrganizeData = function(inParas) {
    this.inParas = inParas;
    this.pageContext = pageContextCache;
	//this.processorFactory = new ProcessorFactory();
};
OrganizeData.prototype.buildData = function(sid,$M)
{
    var jsonvalue = this.parseParas(sid,$M);
	var requestBySid = this.pageContext.getRequest(sid);
	var preRequestMethod = requestBySid.preRequest; 
	//\u4e8b\u4ef6\u8bf7\u6c42\u524d\u65b9\u6cd5
	if(requestBySid && preRequestMethod && preRequestMethod.trim())
	{
		var requestevent = parseValueById(preRequestMethod);
		evalEvent($M,requestevent);
	} 
	jsonvalue = jsonvalue.toJSONString();
    return jsonvalue;
};

/**
 * \u901a\u8fc7ctrlid\u83b7\u53d6\u7ed1\u5b9a\u7684\u6570\u636e\u4fe1\u606f
 * @param ctrlId
 */
OrganizeData.prototype.getInfoByCtrlId = function(ctrlId)
{

};
function isIncludeSymbol(requestPara,organizeData,$M)
{
    var inparasValue;
	var group;
	var typePara = typeof(requestPara);
   if(typePara =="string")
	{
	    if (requestPara.indexOf("[") == -1 && requestPara.indexOf("]") == -1)
	    {
	        inparasValue = "[" + requestPara + "]";
	    }
	    if (requestPara.indexOf("[") != -1 && requestPara.indexOf("]") != -1)
	    {
	        inparasValue = requestPara;
	    }
	    group = organizeData.splitParaValue(inparasValue, organizeData, group,$M);	
		return group;		
	}else
	{
		var objArr = new Array();
		objArr[0] = requestPara;
		group = objArr;
		return group;		
	}

};
/**
 *  \u89e3\u6790sid\u5bf9\u5e94\u7684\u53c2\u6570\u503c\uff0c\u5982\u89e3\u6790para='[$M.obj1,$M.obj2]'
 * @param sid
 */
OrganizeData.prototype.parseParas = function(sid,$M)
{
    var group;
    var organizeData = this;
    if (organizeData.inParas != null && organizeData.inParas != "")
    {		
        group = isIncludeSymbol(organizeData.inParas,organizeData,$M);
		return group;
    }
	else{
	    var requestBySid = organizeData.pageContext.getRequest(sid);
		//\u83b7\u53d6request\u6807\u7b7e
	    if (requestBySid != null && requestBySid != "" && (this.inParas == null || this.inParas == ""))
	    {
	        var requestPara = requestBySid.para;
			//\u83b7\u53d6request\u6807\u7b7e\u4e0b\u9762\u7684para\u53c2\u6570
	        if (requestPara != null && requestPara != "")
	        {
	          group = isIncludeSymbol(requestPara,organizeData,$M);
			  return group;
	        }else
			{
				return new Array();
			}
		  	
	    }else
		{
			return new Array();
		}
		 
	}
   
};
/**
 * \u8c03\u7528\u754c\u9762\u7ec4\u7ec7\u6570\u636e
 * @param elementId
 */
OrganizeData.prototype.invokeFunction = function(elementId)
{
    var result = {};
    try {
        result = GetDataFromCtrl(elementId);
    } catch(e)
    {
        //logErro(RIA_ERROR_GETCTRL, "get controlId erro! The controlId is [" + elementId + "]", 3);
		log.info("errorcode is"+RIA_ERROR_GETCTRL+"errordescription is:get controlId erro! The controlId is [" + elementId + "]");
        return;
    }
    return result;
};

/**
 * \u5206\u79bbrequest\u6807\u7b7e\u4e2d\u5b58\u5728\u7684 para\u5c5e\u6027
 * \u6216\u8005\u662f\u5728call\u4e2d\u5b58\u5728\u7684para
 * @param requestPara
 * @param organizeData
 * @param group
 */
OrganizeData.prototype.splitParaValue = function(requestPara, organizeData, group,$M)
{
	if(requestPara == null)
	{
		return new Array();
	}
    var group = new Object();
	var splitPara;
	var para;
	//call url\u7684\u65f6\u5019\u83b7\u53d6\u53c2\u6570\u7684\u7c7b\u578b\uff0c$m.obj.name 
	//"number," "string," "boolean," "object," "function," \u548c "undefined."
	var typePara = typeof(requestPara); 
    switch(typePara)
	{
		case "string" :
		    var subPara; 
		    if(requestPara.indexOf("[")!=-1&&requestPara.indexOf("]")!=-1)
			{
			    var paraStart = requestPara.indexOf("[");
			    var paraEnd = requestPara.indexOf("]");
			    subPara = requestPara.substring(paraStart + 1, paraEnd);
			    splitPara = subPara.split(",");					
			}
            else
			{
				splitPara = requestPara.split(",");
			}
			break;
		case "object" ://\u5982\u679c\u662f\u6570\u7ec4\u5f62\u5f0f\u7684\u4e5f\u5f53\u4f5cobject\u6765\u5904\u7406
		    var arrobj = new Array();
			arrobj[0] = requestPara;
			para =	arrobj;
			return para;
		case "number":
			para =	requestPara;
			return para;
		case "boolean"://false\u30010\u3001null\u3001 NaN
		   	para = requestPara;
			return para;
		case "undefined":
		    return new Array();					
	}	
    para = new Array(splitPara.length);
    var stortBindValue = "";
    var f = 0;
	//\u5206\u79bb\u591a\u4e2a\u53c2\u6570\u7684\u5f62\u5f0f 
    for (var i = 0; i < splitPara.length; i++)
    {
        var singlePara = splitPara[i];
        var j = singlePara.indexOf("$");
		//\u5982\u679c\u5e26\u6709$\u7b26\u53f7
         //\u5904\u7406\u5bf9\u8c61\u6a21\u578b\uff0c\u5982 $M.obj
        if (j != -1)
        {
            //\u5982\u679c\u6709\u70b9\u53f7
            if (singlePara.indexOf(".") != -1)
            {
                var requestModel = singlePara.substring(singlePara.indexOf(".") + 1, singlePara.length);
                if (requestModel != null && requestModel != "")
                {
                    var requestModelArr = requestModel.split(".");
                    var modelArrLength = requestModelArr.length;
                    requestModelArr[modelArrLength - 1] = new Object();
                    group = requestModelArr[modelArrLength - 1];
                    var bindObject = new Object();
					//\u5faa\u73afbinds\u6807\u7b7e\u91cc\u9762\u7684ref\u5c5e\u6027\uff0c\u53d6\u51fa\u4e0erequestModel\u503c\u76f8\u540c\u7684\u503c						        
                    var bindTagArr = organizeData.pageContext.bindArr;
                    var d = 0;
                    //var arrAllBindRef = [];
                    //var arrAllBindCtrl = [];
                    //\u5c01\u88c5\u6240\u6709\u5b58\u5728requestModel \u4e2d\u7684bind\u6807\u7b7e
                    var bindTempArr = [];
                    for (var k = 0; k < bindTagArr.length; k++)
                    {
                        var bindRef = bindTagArr[k].ref;
                        var bindCtrl = bindTagArr[k].ctrlid;
                        var bindId = bindTagArr[k].id;
                        var bindAutoCheck = bindTagArr[k].autocheck;
                        var bindDatatype = bindTagArr[k].datatype;
                        var bindValidate = bindTagArr[k].validate;
                        var bindCtrlext = bindTagArr[k].ctrlext;
                        var regexp = new RegExp("^" + requestModel + "[.]");
                        if (regexp.test(bindRef) || bindRef == requestModel)
                        {
                            //1.\u628a\u5728modelRequest\u91cc\u9762\u7684\u503c\u53d6\u51fa\u6765\u653e\u5728\u4e00\u4e2a\u6570\u7ec4\u91cc\u9762
                            //arrAllBindRef[d] = bindRef.replace(requestModel+".","");
                            //arrAllBindCtrl[d] = bindCtrl;
                            var newBind = new BindTag();
                            newBind.ref = bindRef.replace(requestModel + ".", "");
                            newBind.ctrlid = bindCtrl;
                            newBind.datatype = bindDatatype;
                            newBind.autocheck = bindAutoCheck;
                            newBind.ctrlext = bindCtrlext;
                            newBind.id = bindId;
                            newBind.validate = bindValidate;
                            bindTempArr[d] = newBind;
                            d++;
                        }
                    }
					//2.\u5728\u53d6\u51fa\u6570\u7ec4\u91cc\u9762\u67d0\u4e2a\u5177\u4f53\u7684\u503c\u8fdb\u884c\u5904\u7406
                    for (var q = 0; q < bindTempArr.length; q++)
                    {
                        var singleBindRefArr = (bindTempArr[q].ref).split(".");
                        var singleBindRefCtrl = bindTempArr[q].ctrlid;
                        if (singleBindRefCtrl != null && singleBindRefCtrl != "") {
                            if (singleBindRefArr.length >= 1 && bindTempArr[q].ref == requestModel)
                            {
                                var validatefunction = bindTempArr[q].validate;
								//\u6821\u9a8c\u51fd\u6570
                                if (validatefunction != null && validatefunction != "") eval(validatefunction);
                                var ctrlResult = "";
								//ui\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728
								if(document.getElementById(singleBindRefCtrl) != null) ctrlResult =this.invokeFunction(singleBindRefCtrl);
								//\u5b9a\u4e49\u6587\u672c\u6846\u7684\u540d\u79f0                             
								if(bindTempArr[q].datatype!= null && bindTempArr[q].datatype!= ""&&document.getElementById(singleBindRefCtrl) != null)
								{
									var inputname = document.getElementById(singleBindRefCtrl).name;
	                                var ble = validateDataType(ctrlResult, bindTempArr[q], inputname);
	                                if (!ble)
	                                {
										document.getElementById(singleBindRefCtrl).focus();
										return;                                    
	                                }									
								}
								bindObject = ctrlResult;
                                break;
                            }
                            this.getObject(bindObject, singleBindRefArr, 1, singleBindRefCtrl, bindTempArr[q]);
                        }
                    }
					$M[requestModel] = bindObject;
                    para[i] = group = bindObject;
                }
            }
        }
     else 
        {
			var evalparatype = typeof(singlePara);	
			if(evalparatype =='string')
			{
				var nn = new RegExp("\^[0-9]");
				if(nn.test(singlePara))
				{
					para[i] = singlePara;
				}else{
					try{
						var evalPara = eval(singlePara);
						if(evalPara == undefined)
						{
							para[i] = singlePara;
						}				
						else
						{
							para[i] = evalPara;
						}
					}
					catch(e){
						para[i] = singlePara;
					}
				}		
			}
		     else
			{
				var evalPara = eval(singlePara);				
				para[i] = evalPara;
			}			
        }
    }
    return     para;
};
var validateDataType = function (ctrlResult, bindTemp, inputname)
{
    var datatype = bindTemp.datatype;
    var input = {};
    input.datatype = datatype;
    input.value = ctrlResult;
    input.name = inputname;
    return verifyInput(input,datatype);
};
function validateTypeOfDate(str)
{
   var regexp = new RegExp(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])$/);
  if(regexp.test(str))
  {
      return true;
  }
  else
  {
      return false;
  } 
};
var  riaDate2UTC = function(format, time){
  	var format = format.substring(6,format.length-2);
  	if(time == null || time == '')
  	{
  		return null;
  	}
  	var utc = "";
	var yy = "-";
	var mm = "-";
	var date_format = new Array();
	var date_format1 = "";
	var date_format2 = "";
	var time_arr = new Array();
	var time1 = time;
	var time2 = "";
	if(format != null)
	{
		if(format.length > 10)
		{
		    date_format = format.split(" ");
		    date_format1 = date_format[0];
		    date_format2 = date_format[1];
		    
		    time_arr = time.split(" ");
		    time1 = time_arr[0];
		    time2 = time_arr[1];
		}else{
			date_format1 = format;
			time1 = time;
		}
		var y_index = date_format1.indexOf("y");
		var y_endindex = date_format1.lastIndexOf("y");
		yy = date_format1.substring(y_endindex+1,y_endindex+2);	

	    var m_index = date_format1.indexOf("m");
		var m_endindex = date_format1.lastIndexOf("m");
		mm = date_format1.substring(m_endindex+1, m_endindex+2);
	}
	var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
	var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);

	time1 = time1.replace(YM,',');
	time1 = time1.replace(MD,',');
    var tt = time1.split(",");
  
    if(time2.length < 1){
    	var date = new Date(tt[0],parseInt(tt[1]-1),tt[2]);
    	utc = date.getTime();
    }else{
    	var HH = time2.split(":");
    	 var date = new Date(tt[0],parseInt(tt[1]-1),tt[2],HH[0],HH[1],HH[2]);
    	 utc = date.getTime();
    }
	return utc;
  };
OrganizeData.prototype.getObject = function(obj, propertyPath, level, ctrlid, bindtemp)
{
    if (level == propertyPath.length)
    {
        //\u6821\u9a8c\u51fd\u6570
        if (bindtemp&&bindtemp.validate != null && bindtemp.validate != "") eval(bindtemp.validate);
        var ctrlResult ="";
		//\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728\u7684\u60c5\u51b5
		if(document.getElementById(ctrlid)!=null) ctrlResult=this.invokeFunction(ctrlid);
		if(bindtemp&&bindtemp.datatype != null && bindtemp.datatype != ""&& document.getElementById(ctrlid)!=null)
		{
            var inputname = document.getElementById(ctrlid).name;			
	        var bl = validateDataType(ctrlResult, bindtemp, inputname);
	        if(!bl) 
			{
				document.getElementById(ctrlid).focus();
				return;
			}
			if(validateTypeOfDate(ctrlResult))
			{			  
		       //var year = parseInt(ctrlResult.substring(0,4));
		       //var month = parseInt(ctrlResult.substring(5,7));
		       //var day = parseInt(ctrlResult.substring(8,10));		       
			   //obj[propertyPath[level - 1]] = new Date(year,month,day).getTime();
			   obj[propertyPath[level - 1]] = riaDate2UTC(bindtemp.datatype,ctrlResult);
			   return ;
			}			
		}
		obj[propertyPath[level - 1]] = ctrlResult;
        return;
    }
    if (!obj[propertyPath[level - 1]])
    {
        obj[propertyPath[level - 1]] = {};
    }
    this.getObject(obj[propertyPath[level - 1]], propertyPath, level + 1, ctrlid, bindtemp);
};
/**
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a1
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-9
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1a\u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00393889 \u56fe\u7247\u591a
 */

/**
 * \u521b\u5efa\u670d\u52a1\u5bf9\u8c61
 * @param {Object} sid      \u670d\u52a1ID
 * @param {Object} outParas \u8fd4\u56de\u53c2\u6570
 * @param {Object} autoPara \u662f\u5426\u914d\u6709bind
 */
var CallService = function(sid, outParas, autoPara, obj, fun,mm,cacheurl) {
    this._xmlhttp = new FactoryXMLHttpRequest();
    this.username = null;
    this.password = null;
    this.sid = sid;
    this.outParas = outParas;
    this.autoPara = autoPara;
    this.pageContext = pageContextCache;
    this._object = obj;
    this.onSuccess = fun;
	this.mm = mm;
	this.cacheurl = cacheurl;
};
var processBarHidden = function(serviceId,instance)
{
   if(instance._xmlhttp.readyState == 4){
  
     if(instance._xmlhttp.status == 12029||instance._xmlhttp.status == 408)
    {   
        if(serviceId)
        {
            var loadingobj = pageContextCache.getLoading(serviceId);
		    var issyn = loadingobj.isSyn;
		    var divid = loadingobj.divId;
		    if(issyn==null||issyn==""||issyn=="false"||issyn==false)
		    {
		        hiddendDivImg(divid);
		        return;
		    }else if(issyn==true||issyn=="true")
		    {
		       removeObj();
		       alert("request time out");
		       return;
		    }         
        }
    } 
     }
};
/**
 * url\u8c03\u7528\u670d\u52a1
 * @param {Object} action
 * @param {Object} url
 * @param {Object} data
 */
var CallService_call = function(action, url, data,isSyn) {
    var instance = this;
    var hiddensid = this.sid;
	if(isSyn && isSyn=='true')
	{
		//\u540c\u6b65
		isSyn = false;
	}
	else
	{
		//\u5f02\u6b65
		isSyn = true;
	}
    this._xmlhttp.open(action, url, isSyn);
    this.openCallback(this._xmlhttp);
    this._xmlhttp.onreadystatechange = function() {
    //\u670d\u52a1\u8d85\u65f6\u8bbe\u7f6e\u56fe\u7247\u9690\u85cf
    processBarHidden(hiddensid,instance);
        switch (instance._xmlhttp.readyState) {
            case 1:
                instance.loading();
                break;
            case 2:
                instance.loaded();
                break;
            case 3:
                instance.interactive();
                break;
            case 4:
                if (instance.complete) {
                    try {
                        instance.complete(instance._xmlhttp.status, instance._xmlhttp.statusText,
                                instance._xmlhttp.responseText, instance._xmlhttp.responseXML);
                    }
                    catch (e) {
                    }
                }
                break;
        }
    };
    try {
        this._xmlhttp.send(data);
			//\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
       var serviceRequest = pageContextCache.getRequest(this.sid);
	   //\u8bf7\u6c42\u76f8\u5e94\u540e\u4e8b\u4ef6
	   if(serviceRequest && serviceRequest.postRequest&&serviceRequest.postRequest.trim())
	   {
		   	var requesteventafter = parseValueById(serviceRequest.postRequest);
			evalEvent(this.mm,requesteventafter);
	   }
	   
    }
    catch(e) {
    }
};

var CallService_get = function(url) {
    this.call("GET", url, null);
};

var CallService_put = function(url, mimetype, datalength, data) {
    this.openCallback = function(xmlhttp) {
        xmlhttp.setRequestHeader("Content-type", mimetype);
        xmlhttp.setRequestHeader("Content-Length", datalength);
    };
    this.call("PUT", url, data);
};

var CallService_delete = function (url) {
    this.call("DELETE", url, null);
};

var CallService_post = function(url, mimetype, datalength, data,isSyn) {
    var thisReference = this;
    this.userOpenCallback = this.openCallback;
    this.openCallback = function(xmlhttp) {
        //xmlhttp.setRequestHeader( "Content-type", mimetype);
        //xmlhttp.setRequestHeader( "Content-Length", datalength);
        thisReference.userOpenCallback(xmlhttp);
        thisReference.openCallback = thisReference.userOpenCallback;
    };
    this.call("POST", url, data,isSyn);
};

var CallService_openCallback = function (obj) {
};
var CallService_loading = function () {
};
var CallService_loaded = function () {
};
var CallService_interactive = function () {
};
/**
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a2
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-15
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1a\u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00395021
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a3
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-18
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1a\u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00396183
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a4
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-23
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1abug\u7f16\u53f7\uff1aCMMDB00398687
 *
 * \u4fee\u6539\u8bb0\u5f55\uff1a5
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-24
 * \u4fee \u6539 \u4eba\uff1a\u7530\u6ce2
 * \u4fee\u6539\u5185\u5bb9\uff1a\u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00400045
 * 
 * \u4fee\u6539\u8bb0\u5f55\uff1a6
 * \u4fee\u6539\u65e5\u671f\uff1a2008-1-24
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1a\u53d8\u66f4\u7f16\u53f7\uff1aCMMDB00399480
 *
 * \u4fee\u6539\u8bb0\u5f55\uff1a7
 * \u4fee\u6539\u65e5\u671f\uff1a2008-5-7
 * \u4fee \u6539 \u4eba\uff1a\u7530\u5e94\u6743
 * \u4fee\u6539\u5185\u5bb9\uff1abug\u53f7\uff1aCMMDB00425481 \u53d8\u6362js\u6587\u4ef6
 */
var SHOWDATA_OPRST_TAG = "opRst";
var SHOWDATA_RTN_TAG = "rtn";
var SHOWDATA_EXPMODEL_TAG = "expModel";
var SHOWDATA_DOT_TAG = ".";
var ERR_ERRORCODE ="excepCode";
var ERR_ERRORDESC = "excepDesc";

var CODE_ERR_CONSOLE = "4";
var RIA_NOLOGIN_OPRST = "6";
var RIA_NOPOPE_OPRST = "7";
var CODE_ERR_SIZELIMITEXCEEDED="8";
/**
 * \u589e\u52a0\u4e00\u4e2a trim \u7684\u51fd\u6570
 */ 
String.prototype.trim = function()
{
    // \u7528\u6b63\u5219\u8868\u8fbe\u5f0f\u5c06\u524d\u540e\u7a7a\u683c
    // \u7528\u7a7a\u5b57\u7b26\u4e32\u66ff\u4ee3\u3002
    return this.replace(/(^\s*)|(\s*$)/g, "");
};
//\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
//\u5904\u7406\u591a\u6b21\u63d0\u4ea4\u7f13\u5b58
function addCacheUrl(url)
{	
   var cacheUrl ="";
   //\u904d\u5386\u83b7\u53d6\u6240\u6709\u7684\u7f13\u5b58url 
	for(cacheUrl in cacheMultiUrl)
	{   
	    //\u5982\u679c\u5728\u7f13\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728\u4e86
	    if(url == cacheUrl)
		{
			return false;
		}							  	
	}
	//\u5728\u7f13\u5b58\u4e2d\u4e0d\u5b58\u5728\u6dfb\u52a0url
	cacheMultiUrl.push(url);
	return true;
};
//\u79fb\u51fa\u5185\u5b58\u4e2durl\u5730\u5740
function removeCacheUrl(cacheurl)
{
	//\u904d\u5386\u591a\u6b21\u53d6\u5f97url
	for(var i=0;i<cacheMultiUrl.length;i++)
	{
		//\u5982\u679c\u5728\u5185\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728
		if(cacheMultiUrl[i]==cacheurl)
		{
			cacheMultiUrl.splice(i,i);
		}
	}
};
//\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f
/**
 * \u56de\u8c03\u670d\u52a1
 * @param {Object} status
 * @param {Object} statusText
 * @param {Object} responseText
 * @param {Object} responseHTML
 */
var CallService_complete = function (status, statusText, responseText, responseHTML) {
	
	log.info("Call Service complete:status=" + status);
	log.info("Call Service complete:statusText=" + statusText);
	log.info("Call Service Complete:responseText=" + responseText);
	
    var outPara = this.outParas;
    var serviceId = this.sid;
    var autoPara = this.autoPara;
    var _object = this._object;
    var onSuccess = this.onSuccess;
	//success
    if (status == 200)
    {		
		removeCacheUrl(this.cacheurl);
		//\u4fee\u6539\u8bb0\u5f552 \u5f00\u59cb
		//\u7528\u6237\u5b58\u5728\u56fe\u7247\u8fdb\u5ea6\u6761\u7684\u663e\u793a
		if(serviceId){

			var loadingobj = pageContextCache.getLoading(serviceId);
			//\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
			if(loadingobj)
			{
				var issyn = loadingobj.isSyn;
				var delay = loadingobj.delay;				
				//\u4fee\u6539\u8bb0\u5f556 \u5f00\u59cb
                if(delay == ""||delay==null||delay==undefined) delay="500";				
				//\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
				if(issyn==null||issyn==""||issyn=="false"||issyn==false)
				{
					var divid = loadingobj.divId;
					var numObj = new Number(delay);
					//\u5ef6\u65f6\u529f\u80fd
					if(delay)
					{
						window.setTimeout("hiddendDivImg('"+divid+"');",parseInt(delay));
					}else//\u4e0d\u5ef6\u65f6
					{
						hiddendDivImg(divid);
					}
					
				}else if(issyn==true||issyn=="true")//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
				{
					//\u5ef6\u65f6\u529f\u80fd
					if(delay)
					{
						window.setTimeout("removeObj();",parseInt(delay));
					}else//\u4e0d\u5ef6\u65f6
					{
						removeObj();
					}								
				}
				//\u4fee\u6539\u8bb0\u5f556 \u7ed3\u675f
			}			
		}
		//\u4fee\u6539\u8bb0\u5f552 \u7ed3\u675f
		
        if (!responseText)
        {
            alert("no data retrieved from the server!");
            return;
        }

        var response = eval("(" + responseText + ")");

		//\u4ecejson\u4e2d\u63d0\u53d6\u6570\u636e
		var rtn_value = response[SHOWDATA_RTN_TAG];	
		//\u4fee\u6539\u8bb0\u5f553 \u5f00\u59cb
		if(serviceId)
		{
			var servicetagid = pageContextCache.getService(serviceId);
			//\u662f\u5426\u9700\u8981cookies\u7f13\u5b58
			//\u4fee\u6539\u8bb0\u5f554 \u5f00\u59cb
			if(servicetagid.cache=="true"||servicetagid.cache==true)
			{
				var expiresvalue = servicetagid.expires;
				
				setCookie(getNameCookie(serviceId),rtn_value.toJSONString(),expiresvalue);
			}
			//\u4fee\u6539\u8bb0\u5f554 \u7ed3\u675f
		}
		//\u4fee\u6539\u8bb0\u5f553 \u7ed3\u675f	
	    //\u8fd4\u56de\u72b6\u6001
        var res_oprst = response[SHOWDATA_OPRST_TAG] + "";
		
		//var rtn_expDesc = response[SHOWDATA_EXPDESC_TAG];
		var res_expModel = response[SHOWDATA_EXPMODEL_TAG];
		switch (res_oprst) {
	            case "0":
					//\u8fd4\u56de\u4fe1\u606f\u663e\u793a
					response_success(outPara,serviceId,autoPara,_object,onSuccess,rtn_value);					
					break;
				case RIA_NOLOGIN_OPRST:
					//\u6ca1\u6709\u767b\u5f55
					isLogin(rtn_value);
					break;
				case RIA_NOPOPE_OPRST:
					//\u6ca1\u6709\u6743\u9650
					isPrivilege(rtn_value);
					break;
				case CODE_ERR_SIZELIMITEXCEEDED:
					//\u4e0a\u4f20\u8d85\u51fa\u6700\u5927\u503c
					response_rtn_expDesc(res_oprst,"FileUploadException - "+res_expModel,serviceId,onSuccess);
					log.info("Call Service complete:expcetion desc=" + res_expModel.toJSONString());
					break;
				case CODE_ERR_CONSOLE:
				     log.info("Call Service complete:expcetion desc=" + res_expModel.toJSONString());
					 response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess);
				     break;	
				default:
					//\u8fd4\u56de\u9519\u8bef\u4fe1\u606f
					log.info("Call Service complete:expcetion desc=" + res_expModel.toJSONString());
					response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess);
					break;
									
		};
    }
};

/**
 * \u8fd4\u56de\u6210\u529f
 * @param {Object} outPara   \u51fa\u53c2
 * @param {Object} serviceId \u670d\u52a1Id
 * @param {Object} autoPara  callUrl\u6807\u793a
 * @param {Object} _object   callMethod\u4e2d\u65b9\u6cd5\u5bf9\u8c61
 * @param {Object} onSuccess callMethod\u6807\u793a
 * @param {Object} rtn_value \u8fd4\u56de\u503c
 */
var response_success = function (outPara,serviceId,autoPara,_object,onSuccess,rtn_value)
{

	//\u82e5\u8c03\u7528\u7684\u662fcallMethod
    if (onSuccess)
    {
		if((typeof _object) != "object")
		{
			_object = this;
		}
        onSuccess.call(_object, 0, rtn_value);
        return;
    }
	
	//\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var responseTags = serviceId == null ? {} : pageContextCache.getResponse(serviceId);
	
	var resp_rtn = "";
	
	//\u5224\u65ad\u7528\u6237\u662f\u5426\u914d\u7f6eoutparas\u5c5e\u6027
    if (outPara)
    {
        resp_rtn = outPara;
    }//\u82e5\u6ca1\u914d\u7f6e\u5219\u76f4\u63a5\u89e3\u6790response
    else {
        resp_rtn = responseTags.rtn;
    }
	
	//\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
	
	//\u521b\u5efa\u5bf9\u8c61
	var $M = {};
	$M[rtn_string] = rtn_value;
	
	//\u670d\u52a1\u8bf7\u6c42\u524d\u54cd\u5e94\u7684\u4e8b\u4ef6
    if (responseTags.preResponse && responseTags.preResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var preResponse_event = parseValueById(responseTags.preResponse);
		evalEvent($M,preResponse_event);
    }
				
	//\u81ea\u52a8\u76f4\u63a5\u56de\u9009
    if (autoPara)
    {
		log.info("Call Service Complete:autoPara=" + autoPara);
		log.info("Call Service Complete:resp_rtn=" + resp_rtn);
		log.info("Call Service Complete:rtn_value=" + rtn_value);
        if(!document.getElementById(resp_rtn))
		{
			var respRtn = eval(resp_rtn);
			respRtn.rtnValue = $M[rtn_string];				
		}else{
			try{
				BindDataToCtrl(resp_rtn, $M[rtn_string]);
			}
	        catch(e){
				log.error(RIA_ERROR_DISPOSE,e.message);
			}
		}
    }
				
	//\u5206\u6790\u914d\u7f6e\u8fd4\u56de\u503c\u4fe1\u606f
    else if (resp_rtn != null)
    {
        //\u8fd4\u56de\u503c\u914d\u7f6e\u82e5\u662f\u5e26$M\u7b26\u53f7
        if ("$" == resp_rtn.substring(0,1))
        {
            //\u5224\u65ad\u4f20\u8fc7\u6765\u7684\u6570\u636e\u662f\u4ec0\u4e48\u7c7b\u578b
            if ((typeof rtn_value) == "object")
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string]);

            } else if ((typeof rtn_value) == "string" || (typeof rtn_value) == "boolean")
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string]);
            } else if ((typeof rtn_value) == "undefined")
            {
                alert("undefined");
            }
			//\u7c7b\u578b\u5224\u65ad\u7ed3\u675f.

        }//\u82e5\u662f\u63a7\u4ef6ID
        else {
			log.info("Call Service Complete:resp_rtn=" + resp_rtn);
			log.info("Call Service Complete:rtn_value=" + $M[rtn_string]);
            
			try{
				BindDataToCtrl(resp_rtn, $M[rtn_string]);
			}
            catch(e){
				log.error(RIA_ERROR_DISPOSE,e.message);
			}
        }
    }
	
	//\u670d\u52a1\u8bf7\u6c42\u540e\u54cd\u5e94\u7684\u4e8b\u4ef6		
    if (responseTags.postResponse && responseTags.postResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
		var postResponse_event = parseValueById(responseTags.postResponse);
        evalEvent($M,postResponse_event);
    }
	//forward\u9875\u9762\u8df3\u8f6c
    getForwards(serviceId,$M);
       
};

var evalEvent = function (m,para)
{
	eval("var $M = arguments[0];");
	eval(para);
};

/**
 * \u8fd4\u56de\u4fe1\u606f\u5931\u8d25
 * @param {Object} rtn_expDesc \u5931\u8d25\u4fe1\u606f
 * @param {Object} serviceId   \u670d\u52a1id
 * @param {Object} onSuccess   callMethod
 */
var response_rtn_expDesc = function(errorCode,res_expModel,serviceId,onSuccess)
{
	
    if (onSuccess)
    {		
		if((typeof _object) != "object")
		{
			_object = this;
		}
        onSuccess.call(_object, 1, res_expModel);
        return;
    }
	
	//\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var serviceTags = serviceId == null ? {} : pageContextCache.getService(serviceId);
	if (serviceTags.onError && serviceTags.onError.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var onError_event = parseValueById(serviceTags.onError);
        if(errorCode == CODE_ERR_CONSOLE|| errorCode == 4)
		{
			ssb_update_eval(onError_event,res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
		}
		else
		{
			eval(onError_event);
		}
		return;
    }
    log.error(errorCode,rtn_expDesc);
};

var ssb_update_eval = function()
{
	var onError_event = arguments[0];
	if(onError_event.indexOf("(") != -1 && onError_event.indexOf(")") != -1)
	{
	    var eventStr = onError_event.substring(onError_event.indexOf("("),onError_event.indexOf(")"));
	    if(eventStr.indexOf("errorCode") != -1)
	    {
		     onError_event = onError_event.replace("errorCode","arguments[1]");
	    }
	    if(eventStr.indexOf("rtn_expDesc") != -1)
	    {
		     onError_event = onError_event.replace("rtn_expDesc","arguments[2]");
	    }
	}
	
	eval(onError_event);
};

/**
 * \u89e3\u6790\u8fd4\u56de\u4fe1\u606f
 * @param {Object} pageContext \u521d\u59cb\u6570\u636e
 * @param {Object} resp_rtn    \u9875\u9762\u4e0a\u7684rtn\u5bf9\u5e94\u7684\u5b57\u7b26\u4e32
 * @param {Object} rtn_values  \u540e\u53f0\u8fd4\u56de\u503c
 */
var parseRtn = function (pageContext, resp_rtn, rtn_values)
{
    //\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
	
	//\u904d\u5386\u6240\u6709bind
    for (var j = 0; j < pageContext.bindArr.length; j++)
    {
		var rtn_value = rtn_values;
        var typeValues = new Array();
        var bind_ref = pageContext.bindArr[j].ref;
		var ctrlid = pageContext.bindArr[j].ctrlid;
		var datatype = pageContext.bindArr[j].datatype;
		var ctrlext = pageContext.bindArr[j].ctrlext;
		

        //\u6bd4\u8f83bind\u7684ref\u548crtn\u503c
        var bindre = new RegExp('^' + rtn_string);
        if (bindre.test(bind_ref))
        {
			var bind_refs = bind_ref.substring(rtn_string.length);
			
            var bind_ref_index = bind_refs.indexOf(SHOWDATA_DOT_TAG);
            var bind_ref_values = new Array();		
				
			var bind_array = /^\[/;
			var bind_arrays = /\]$/;
			if(bind_array.test(bind_refs))
			{
				var array_index = bind_refs.substring(bind_refs.indexOf("[")+1,bind_refs.indexOf("]"));
				rtn_value = rtn_values[parseInt(array_index)];
			}
			
            if (bind_ref_index != -1)
            {
                bind_ref_values = bind_refs.substring(bind_ref_index + 1).split(SHOWDATA_DOT_TAG);
                
                for (var n = 0; n < bind_ref_values.length; n++)
                {
                    //\u4fee\u6539\u8bb0\u5f557 \u5f00\u59cb
                    if(rtn_value == null) return;
                    //\u4fee\u6539\u8bb0\u5f557 \u7ed3\u675f
                    rtn_value = rtn_value[bind_ref_values[n]];
					
					//\u82e5rtn_value\u4e2d\u6ca1\u6709\u5c01\u88c5\u6b64\u5bf9\u8c61,\u5219\u76f4\u63a5\u8df3\u51fa\u6b64\u65b9\u6cd5
                    if (rtn_value == undefined)
                    {
                        //error\u65e5\u5fd7
						//log.error("Call Service Complete:rtn_value1=" + rtn_value);
                        break;
                    }
                }
				log.info("Call Service Complete:ctrlid=" + ctrlid);
				log.info("Call Service Complete:rtn_value=" + rtn_value);
				try{
					rtn_value = rtnCalendarValue(datatype,rtn_value);
					BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext));
				}
	            catch(e){
					log.info(RIA_ERROR_DISPOSE+":"+e.message);
				}
				
            }else
			{
				if(bind_ref == rtn_string || (bind_array.test(bind_refs)&&bind_arrays.test(bind_refs)))
				{
					log.info("Call Service Complete:ctrlid=" + ctrlid);
					log.info("Call Service Complete:rtn_value=" + rtn_value);
					try{
						rtn_value = rtnCalendarValue(datatype,rtn_value);
						BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext));
					}
		            catch(e){
						log.info(RIA_ERROR_DISPOSE+":"+e.message);
					}
				}
			}

        }

    }
};

var isctrlext = function (ctrlext)
{
	var obj = {};
	if(ctrlext)
	{
		var ctrlArr = ctrlext.split(";");
		for(var i =0;i<ctrlArr.length;i++)
		{
			var cArr = ctrlArr[i].split(":");
			obj[cArr[0]] = cArr[1];
		}
		return obj;
	}
	else{
		return null;
	}
			
};

var isLogin = function(rtn_url)
{
	openPath(rtn_url);
};

var isPrivilege = function(rtn_url)
{
	openPath(rtn_url);
};

var openPath = function (rtn_url){
	window.open(rtn_url,"_self");
};


/**
 * \u65e5\u671f\u8f6c\u6362\uff0c\u628aUTC\u65f6\u95f4\u8f6c\u6362\u4e3a\u9700\u8981\u7684\u683c\u5f0f
 * @param {Object} datatype \u683c\u5f0f
 * @param {Object} value    UTC\u65f6\u95f4
 */
function rtnCalendarValue(datatype,value)
  {
  	
  	if(datatype)
	{
		var type =  datatype.trim();
		var datatypes = type.indexOf("date");
		
		if(datatypes == -1)
		{
			return value;
		}
		if(value == null || value == "")
		{
			return "";
		}
		var format = "yyyy-MM-dd";
		if(type.indexOf("(") != -1)
		{
			format = type.substring(type.indexOf("(")+1,type.indexOf(")"));
		}
		
		// \u4fee\u6539\u8bb0\u5f555 \u5f00\u59cb
		format = format.toLowerCase();
		if(format.indexOf("hh24") != -1)
		{
			format = format.replace("hh24","hh");
		}
		// \u4fee\u6539\u8bb0\u5f555 \u7ed3\u675f
		
		var time_value = new Date(value);
	
		var yTime = rtn_replace(format,"yyyy",time_value.getFullYear());
		
		var MTime = rtn_replace(yTime,"mm",numTimes(parseInt(time_value.getMonth()+1)+""));
		
		var dTime = rtn_replace(MTime,"dd",numTimes(time_value.getDate()));
		
		var hTime = rtn_replace(dTime,"hh",time_value.getHours());
		
		var mTime = rtn_replace(hTime,"mi",time_value.getMinutes());
		
		var sTime = rtn_replace(mTime,"ss",time_value.getSeconds());
   
		return sTime;
	}
	return value;
   
  };

/**
 * \u4fdd\u6301\u4e24\u4f4d\u6570\u5b57
 * @param {Object} num
 */
var numTimes = function (num)
{
	if(parseInt(num)<10)
	{
		return "0"+num;
	}
	return num;
};

/**
 * \u66ff\u6362\u65b9\u6cd5
 * @param {Object} format \u539f\u5b57\u7b26\u4e32
 * @param {Object} str    \u88ab\u66ff\u6362\u5b57\u7b26\u4e32
 * @param {Object} value  \u66ff\u6362\u5b57\u7b26\u4e32
 */
var rtn_replace = function (format,str,value)
{
	if(format.indexOf(str) != -1)
	{
		return  format.replace(str,value);
	}
	return format;
};

CallService.prototype.openCallback = CallService_openCallback;
CallService.prototype.loading = CallService_loading;
CallService.prototype.loaded = CallService_loaded;
CallService.prototype.interactive = CallService_interactive;
CallService.prototype.complete = CallService_complete;
CallService.prototype.get = CallService_get;
CallService.prototype.put = CallService_put;
CallService.prototype.del = CallService_delete;
CallService.prototype.post = CallService_post;

CallService.prototype.call = CallService_call;

CallService.prototype.response_success = response_success;
CallService.prototype.response_rtn_expDesc = response_rtn_expDesc;
CallService.prototype.parseRtn = parseRtn;
var DEFAULT_METHOD = "POST";
var DEFAULT_DOTSSM_TAG = ".ssm";
RIAControl = function(serviceId, inParas, outParas, autoPara, obj, fun,$M,cacheurl) {
	this.sid = serviceId;
    this.inParas = inParas;
    this.outParas = outParas;
    this.pageContext = pageContextCache;
    this.organizedata = new OrganizeData(inParas);
    this.callService = new CallService(serviceId, outParas, autoPara, obj, fun,$M,cacheurl);
};

RIAControl.prototype.formatGetUrl = function(url, paras) {
    return url;
};


RIAControl.prototype.callByUrl = function(url, methods, paras) {
    this.process(false, url, methods, paras);
};

RIAControl.prototype.callBySid = function(sid, methods, paras) {
    var url = this.pageContext.getUrl(sid);
    this.process(url, methods, paras);
};

RIAControl.prototype.process = function(url, methods, paras,isSyn) {
    var meth1 = methods || "post";
    var meth = meth1.toUpperCase();
    switch (meth) {
        case 'POST':
            this.callService.post(url, null, null, paras,isSyn);
            break;
        case 'GET':
            var furl = this.formatGetUrl(url, paras);
            this.callService.get(furl);
            break;
        case 'DELETE':
            this.callService.del(url);
            break;
        case 'PUT':
            this.callService.put(url, null, null, paras);
            break;
        default:
            break;
    }

};

RIAControl.prototype.processUpload = function(paraMap,url){
	var id =new Date().getTime();
	var createForm=function(){
		var oForm=document.createElement('FORM');
		oForm.id='form_'+id;
		oForm.action=url;
		oForm.method='POST';
		if(oForm.encoding){
    		oForm.encoding = 'multipart/form-data';
    	}
    	else{
    		oForm.enctype = 'multipart/form-data';
    	}
    	
    	var jsonInput=document.createElement('INPUT');
    	jsonInput.value=paraMap;
    	jsonInput.name='_JasonParameters';
    	jsonInput.type='hidden';
    	oForm.appendChild(jsonInput);
    	document.body.appendChild(oForm);
	};
	var createIFrame=function(){
		var oIFrame={};
		try{
			oIFrame = document.createElement("<IFRAME name=frame_"+id+" />");
		}catch(e){
			oIFrame=document.createElement("IFRAME");
			oIFrame.name='frame_'+ id;
		}
		oIFrame.name ='frame_'+ id;
		oIFrame.width=0;
		oIFrame.height=0;
		oIFrame.frameBorder=0;
		oIFrame.id='frame_'+id;
		document.body.appendChild(oIFrame);
		return oIFrame;
	};
	createForm();
	var oFrame=createIFrame();
	var oForm=document.getElementById('form_'+id);
	var thisCtrl=this;
	var uploadCallback=function(){
		var thisDocument=oFrame.contentDocument||oFrame.contentWindow.document;
		var html=thisDocument.body.innerHTML;
		if(html.charAt(0)=='{'){
			responseText=html;
		}else if(html.substring(0,5)=='<pre>'){
			responseText=html.substring(5,html.length-6);
		}
		else
			responseText=thisDocument.getElementById('result').value;
		try{
			thisCtrl.callService.complete(200,'success',responseText,'');
		}catch(e){
			throw "\u4e0a\u4f20\u5f02\u5e38:"+e;
		}
		setTimeout(function(){try{
								oForm.parentNode.removeChild(oForm);
								oFrame.parentNode.removeChild(oFrame);
								} catch(e){
									throw e;
								}
							  }, 100);
	};
	
	//wanghao
	var oldParentNodes = [];
	var oldNodes = [];
	
	//wanghao
	var clonedNodes = [];
	for(var i=0;i<__file_List.length;i++){
		if(__file_List[i].type!='file'||__file_List[i].value=='') continue;
		var old=__file_List[i];
		var cloned=__file_List[i].cloneNode(true);
		
		//\u4fee\u6539\u8bb0\u5f552: wanghao
		cloned.onchange=old.onchange;
		old.parentNode.insertBefore(cloned,old);
		old.id=id+'_file_'+i;
		if(old.hideFocus != true)
			old.style.visibility = "hidden";
		
		oldNodes.push(old);
		clonedNodes.push(cloned);
		oldParentNodes.push(	old.parentNode);			
		oForm.appendChild(old);
		/*cloned.onchange=old.onchange;
		old.parentNode.insertBefore(cloned,old);
		old.id=id+'_file_'+i;
		old.style.display='none';
		oForm.appendChild(old);*/
		//\u4fee\u6539\u8bb0\u5f551: wanghao
		/*oldNodes.push(old);
		oldParentNodes.push(old.parentNode);			
		oForm.appendChild(old);*/
	}
	if(window.attachEvent){
    	oFrame.attachEvent('onload', uploadCallback);
    }
    else{
        oFrame.addEventListener('load', uploadCallback, false);
    }
    oForm.setAttribute("target",oFrame.name);
    oForm.submit();
	
	/*for(var j=0;j<oldParentNodes.length;j++)
    {
    	oldParentNodes[j].appendChild(oldNodes[j]);
    }*/
	//\u4fee\u6539\u8bb0\u5f552: wanghao
    for(var j=0;j<oldParentNodes.length;j++)
    {
    	if(oldNodes[j].hideFocus == true)
    	{
    		oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
    		continue;
    	}
    	if(oldNodes[j].style.visibility == "hidden")
    		oldNodes[j].style.visibility = "visible";
    	oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
    }
};
function cacheCookies(outParas,jsonvalue,sid)
{	
    var rtnserviec=pageContextCache.getService(sid);
	var rtnsrponse =rtnserviec.responseObj;
	var rtn =rtnsrponse.rtn;
    if(sid){
	    if(outParas&&outParas.indexOf("$")!=-1)
		{
			parseRtn(pageContextCache,outParas,eval('('+jsonvalue+')'));
		}else if(!outParas)
		{
			if(rtn&&rtn.indexOf("$")!=-1)
			{
				parseRtn(pageContextCache,rtn,eval('('+jsonvalue+')'));
			}else
			{
                 BindDataToCtrl(rtn,eval('('+jsonvalue+')'));					
			}
			
		}		
	}	
};
window.callSid = function(sid, inParas, outParas) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
	var obj = getCacheCookie(getNameCookie(sid));
	//\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
	if(obj&&sid)
	{
	    cacheCookies(outParas,obj,sid);	
		return;
	}
	var $M ={};
	var url = pageContextCache.getUrl(sid);
	var blean = addCacheUrl(url);
	//\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
	if(blean)
	{
		//\u663e\u793a\u8fdb\u5ea6\u6761
		var loadingobj = pageContextCache.getLoading(sid);
		//\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
		if(loadingobj)
		{
			var issyn = loadingobj.isSyn;
			//\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
			if(issyn==null||issyn==""||issyn=="false"||issyn==false)
			{
				displayDivImg(loadingobj);
			}else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
			{
				divalert(loadingobj);
			}
		}
	    var control = new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
	    var paraMap = control.organizedata.buildData(sid,$M);
	    
		var isSyn = pageContextCache.getIsSyn(sid);
	    log.info("call service: url=" + url);
	    log.info("call service: input parameter=\n" + paraMap);

	    if(__file_List.length&&__file_List.length>0){
			control.processUpload(paraMap,url);
			//\u4fee\u6539:wanghao
			//__file_List=[]; 
		return;
	}
	    control.process(url, DEFAULT_METHOD, paraMap,isSyn);		
	}


};
window.callTSid = function(sid, inParas, outParas,jsonValue) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
	var obj = getCacheCookie(getNameCookie(sid));
	//\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
	if(obj&&sid)
	{
	    cacheCookies(outParas,obj,sid);	
		return;
	}
	var $M ={};
	var url = pageContextCache.getUrl(sid);
	var blean = addCacheUrl(url);
	//\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
	if(blean)
	{
		//\u663e\u793a\u8fdb\u5ea6\u6761
		var loadingobj = pageContextCache.getLoading(sid);
		//\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
		if(loadingobj)
		{
			var issyn = loadingobj.isSyn;
			//\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
			if(issyn==null||issyn==""||issyn=="false"||issyn==false)
			{
				displayDivImg(loadingobj);
			}else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
			{
				divalert(loadingobj);
			}
		}
		
	    var control = new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
	    control.organizedata.buildData(sid,$M);
	    
        var paraMap = jsonValue;
		var isSyn = pageContextCache.getIsSyn(sid);
	    log.info("call service: url=" + url);
	    log.info("call service: input parameter=\n" + paraMap);

	    control.process(url, DEFAULT_METHOD, paraMap,isSyn);		
	}


};

window.setBindObject = function(resp_rtn,rtn_values)
{
    var pagecontext = pageContextCache;
    parseRtn(pagecontext,resp_rtn,rtn_values);
};
window.getBindObject = function(inParas)
{
    var $M ={};
    var control = new RIAControl(null, inParas, null,true,null,null,$M);
	var paraMaps;
    paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);
	return 	paraMaps;	
};
window.callUrl = function(url, inParas, outParas,isSyn) {
    var blean = addCacheUrl(url);
	//\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
	if(blean)
	{
		var $M ={};
	    var control = new RIAControl(null, inParas, outParas,true,null,null,$M,url);
		var paraMaps;
		paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);
		var paraMap = paraMaps.toJSONString();
		log.info("call url: url=" + url);
	    log.info("call url: input parameter=\n" + paraMap);
	    control.process(url, DEFAULT_METHOD, paraMap,isSyn);
	}
};

window.callMethod = function(sidOrUrl, inParas, obj, fun)
{
   
    var index_url = sidOrUrl.indexOf(DEFAULT_DOTSSM_TAG);
    var url = "";
    if (index_url == -1)
    {
        url = pageContextCache.getUrl(sidOrUrl);
    }
    else
    {
        url = sidOrUrl;
    }
	var blean = addCacheUrl(url);
	//\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
	if(blean)
	{
		var control = new RIAControl(null, null, null, null, obj, fun,url);
	    var paraMap = inParas.toJSONString();
	    control.process(url, DEFAULT_METHOD, paraMap);
	}
};
var _timer_names = new Array();

function callSidTimer(sid, timer)
{
   
   var name = setInterval("callSid('"+sid+"')",timer*1000);

   _timer_names[sid] = name;
};

function intTimerServ(sid)
{
   var name = _timer_names[sid];
   clearInterval(name);
};

function intCurrServ()
{
  throw true;
  
};

window.onerror=function()
{
  return true;
};
/**
 * \u9875\u9762\u8df3\u8f6c
 * @param {Object} sid        \u670d\u52a1ID
 * @param {Object} rtn_value  \u8fd4\u56de\u503c
 */
var getForwards = function (sid,m)
{
	eval("var $M = arguments[1];");
    var forwardTags = pageContextCache.getForward(sid);
	//alert(forwardTags.length);
    var forwardTags_length = forwardTags.length;
    if (forwardTags_length <= 0)
    {
        return;
    }
	
	//\u904d\u5386forward
    for (var i = 0; i < forwardTags_length; i++)
    {
        var forward_onforward = forwardTags[i].onforward;
        var forward_str = parseForwardValueById(forward_onforward);

		//\u5f97\u5230onforward\u4e2d\u7684\u7b97\u5f0f\u662f\u5426\u6210\u7acb
        var forward_s = eval(forward_str);
        if (forward_s)
        {
            var forward_action = parseToString_action(m,forwardTags[i].action);
            var forward_target = parseValueById(forwardTags[i].target);
            window.open(encodeURI(forward_action), forward_target);
			//return;		
        } else {
            //alert(forward_s);
            continue;
        }

    }

};

/**
 * \u89e3\u6790\u5e26${}\u7684\u5b57\u7b26\u4e32,\u5e76\u8fd4\u56deString
 * @param {Object} str \u8981\u89e3\u6790\u7684\u5b57\u7b26\u4e32
 */
var parseForwardValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
        }
		else
		{
			id_value = id;
		}
        str = str.replace(strArr[i], id_value);
    }
    return str;
};

/**
 * \u89e3\u6790\u5e26${}\u7684\u5b57\u7b26\u4e32,\u5e76\u8fd4\u56deString
 * @param {Object} str \u8981\u89e3\u6790\u7684\u5b57\u7b26\u4e32
 */
var parseValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
			str = str.replace(strArr[i], '"'+id_value+'"');
        }
		else
		{
			str = str.replace(strArr[i], id);
		}
        
    }
    return str;
};

/**
 * \u89e3\u6790action\u4e2d\u5e26$M\u7684\u5b57\u7b26\u4e32,\u5e76\u8fd4\u56deString
 * @param {Object} $M \u8fd4\u56de\u4fe1\u606f
 * @param {Object} parsedStr \u8981\u89e3\u6790\u7684$M\u5b57\u7b26\u4e32
 */
var parseToString_action = function(m, _parsedStr)
{
	eval("var $M = arguments[0];");
    var parsedStr = parseForwardValueById(_parsedStr);
    var parseArr = parseLabel(parsedStr);
    var str = parsedStr;
    if (parseArr.length != 0)
    {
        for (var j = 0; j < parseArr.length; j++)
        {
			var rtn_str_value = eval(parseArr[j]);           
			var str = str.replace(parseArr[j], rtn_str_value);
        }
    }
    return str;
};

/**
 * \u5206\u6790ria\u4e2d\u5e26$\uff2d\u8868\u8fbe\u5f0f\u7684\u5b57\u7b26\u4e32\uff0c\u5e76\u8fd4\u56de$M\u8868\u8fbe\u5f0f\u7ec4\u6210\u7684\u6570\u7ec4
 * @param {Object} str \u9700\u8981\u5206\u6790\u7684\u5b57\u7b26\u4e32
 */
var parseLabel = function (str)
{
    var strArr = new Array();
    var str_index = str.indexOf("$M");
    if (str_index != -1)
    {
        strArr = parseArr(str, strArr);
    }

    return strArr;
};

/**
 * \u628a$M\u8868\u8fbe\u5f0f\u4ece\u5b57\u7b26\u4e32\u4e2d\u63d0\u53d6\u51fa\u6765,\u7ec4\u6210\u4e00\u4e2a\u6570\u7ec4\u5e76\u8fd4\u56de.
 * @param {Object} str     \u63d0\u53d6\u8868\u8fbe\u5f0f\u7684\u5b57\u7b26\u4e32
 * @param {Object} strArr  \u7ec4\u6210\u7684\u6570\u7ec4
 */
var parseArr = function (str, strArr)
{
    var strArrs = str.split("");
    var parse_str = "";
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i - 1] == "$" && strArrs[i] == "M")
        {
            parse_str = str.substring(i);
            var strings = "$";
            var parse_strArrs = parse_str.split("");
            for (var j = 0; j < parse_strArrs.length; j++)
            {
                if ((/[^0-9a-zA-Z_\[\].]/g).test(parse_strArrs[j]))
                {
                    strArr.push(strings);
                    strings = "$";
                    var parse_string = parse_str.substring(j);
                    strArr = parseArr(parse_string, strArr);
                    return strArr;
                }
                else {
                    strings = strings + parse_strArrs[j];
                }
            }
            strArr.push(strings);
            return strArr;
        }
    }
    return strArr;
};

/**
 * \u628a${}\u8868\u8fbe\u5f0f\u4e2d\u7684ID\u4ece\u5b57\u7b26\u4e32\u4e2d\u63d0\u53d6\u51fa\u6765,\u7ec4\u6210\u4e00\u4e2a\u6570\u7ec4\u5e76\u8fd4\u56de.
 * @param {Object} str      \u63d0\u53d6\u8868\u8fbe\u5f0f\u7684ID\u5b57\u7b26\u4e32
 * @param {Object} strArr   \u7ec4\u6210\u7684\u6570\u7ec4
 */

var rtnValueById = function (str)
{
    var strArr = new Array();
    var strArrs = str.split("");
    var parse_str = "";
    var TORF = false;
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i] == "$" && strArrs[i + 1] == "{")
        {
            TORF = true;
        }
        if (strArrs[i] == "}")
        {
            parse_str = parse_str + strArrs[i];
            strArr.push(parse_str);
            parse_str = "";
            TORF = false;
        }
        if (TORF)
        {
            parse_str = parse_str + strArrs[i];
        }
    }
    return strArr;
};
    function divalert(loadingObj)
	{
	   var msgw,msgh,bordercolor;
	   msgw=400;//\u63d0\u793a\u7a97\u53e3\u7684\u5bbd\u5ea6
	   msgh=100;//\u63d0\u793a\u7a97\u53e3\u7684\u9ad8\u5ea6
	   titleheight=25;//\u63d0\u793a\u7a97\u53e3\u6807\u9898\u9ad8\u5ea6
	   bordercolor="#336699";//\u63d0\u793a\u7a97\u53e3\u7684\u8fb9\u6846\u989c\u8272
	   titlecolor="#99CCFF";//\u63d0\u793a\u7a97\u53e3\u7684\u6807\u9898\u989c\u8272
	   var sWidth,sHeight;
	   sWidth=document.body.offsetWidth;//\u6d4f\u89c8\u5668\u5de5\u4f5c\u533a\u57df\u5185\u9875\u9762\u5bbd\u5ea6
	   sHeight=screen.height;//\u5c4f\u5e55\u9ad8\u5ea6\uff08\u5782\u76f4\u5206\u8fa8\u7387\uff09
				
	   //\u80cc\u666f\u5c42\uff08\u5927\u5c0f\u4e0e\u7a97\u53e3\u6709\u6548\u533a\u57df\u76f8\u540c\uff0c\u5373\u5f53\u5f39\u51fa\u5bf9\u8bdd\u6846\u65f6\uff0c\u80cc\u666f\u663e\u793a\u4e3a\u653e\u5c04\u72b6\u900f\u660e\u7070\u8272  
	   var bgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09
	   bgObj.setAttribute('id','bgDiv');
	   bgObj.style.position="absolute";
	   bgObj.style.top="0";
	   bgObj.style.background="#777";
	   bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";
	   bgObj.style.opacity="0.6";
	   bgObj.style.left="0";
	   bgObj.style.width=sWidth + "px";
	   bgObj.style.height=sHeight + "px";
	   bgObj.style.zIndex = "10000";
	   document.body.appendChild(bgObj);//\u5728body\u5185\u6dfb\u52a0\u8be5div\u5bf9\u8c61
	
	   var msgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
	   msgObj.setAttribute("id","msgDiv");
	   msgObj.setAttribute("align","center");
	   msgObj.style.background="white";
	   msgObj.style.border="1px solid " + bordercolor;
	   msgObj.style.position = "absolute";
	   msgObj.style.left = "50%";
	   msgObj.style.top = "50%";
	   msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";
	   msgObj.style.marginLeft = "-225px" ;
	   msgObj.style.marginTop = -75+document.documentElement.scrollTop+"px";
	   msgObj.style.width = msgw + "px";
	   msgObj.style.height =msgh + "px";
	   msgObj.style.textAlign = "center";
	   msgObj.style.lineHeight ="25px";
	   msgObj.style.zIndex = "10001";
	
	   var title=document.createElement("h4");//\u521b\u5efa\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
	   title.setAttribute("id","msgTitle");
	   title.setAttribute("align","right");
	   title.style.margin="0";
	   title.style.padding="3px";
	   title.style.background=bordercolor;
	   title.style.filter="progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";
	   title.style.opacity="0.75";
	   title.style.border="1px solid " + bordercolor;
	   title.style.height="18px";
	   title.style.font="12px Verdana, Geneva, Arial, Helvetica, sans-serif";
	   title.style.color="white";
	   title.style.cursor="pointer";
	   title.innerHTML="\u6b63\u5728\u88c5\u8f7d\u4e2d\u8bf7\u7a0d\u5019......";
	
	   var image = document.createElement("IMG");//\u5b9a\u4e49\u4e00\u4e2a\u80cc\u666f\u56fe\u7247
		var imgscr = "";//\u56fe\u7247\u8def\u5f84
		var width = ""; //\u56fe\u7247\u5bbd\u5ea6
		var height =""; //\u56fe\u7247\u9ad8\u5ea6
		//\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
		if(loadingObj)
		{
			imgscr = loadingObj.img;
			width = loadingObj.width;
			height = loadingObj.height;
		}	   
	   image.setAttribute("src",imgscr);
	   image.setAttribute("align","middle");
	   image.setAttribute("width",width);
	   image.setAttribute("height",height);
	      
	   document.body.appendChild(msgObj);//\u5728body\u5185\u6dfb\u52a0\u63d0\u793a\u6846div\u5bf9\u8c61msgObj
	   document.getElementById("msgDiv").appendChild(title);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u6807\u9898\u680f\u5bf9\u8c61title
	   document.getElementById("msgDiv").appendChild(image);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u56fe\u7247\u5bf9\u8c61image
	}; 
	function removeObj()
    {
		 //\u70b9\u51fb\u6807\u9898\u680f\u89e6\u53d1\u7684\u4e8b\u4ef6
		 var msgObj=document.getElementById("msgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
		 var title=document.getElementById("msgTitle");//\u83b7\u53d6\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
		 var bgObj=document.getElementById("bgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09 
	     document.body.removeChild(bgObj);//\u5220\u9664\u80cc\u666f\u5c42Div
	     document.getElementById("msgDiv").removeChild(title);//\u5220\u9664\u63d0\u793a\u6846\u7684\u6807\u9898\u680f
	     document.body.removeChild(msgObj);//\u5220\u9664\u63d0\u793a\u6846\u5c42
    };
	/**
	 * \u663e\u793adiv
	 * @param {Object} divid
	 */
	function displayDivImg(loadingObj)
	{
		var imgscr = "";//\u56fe\u7247\u8def\u5f84
		var width = ""; //\u56fe\u7247\u5bbd\u5ea6
		var height =""; //\u56fe\u7247\u9ad8\u5ea6
		var divid = ''; //\u8bbe\u7f6e\u8981\u663e\u793a\u7684div
		//\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
		if(loadingObj)
		{
			imgscr = loadingObj.img;
			width = loadingObj.width;
			height = loadingObj.height;
			divid = loadingObj.divId;
		}
		//\u8be5div\u662f\u5426\u5b58\u5728
		if(divid)
		{
	       document.getElementById(divid).style.display="";  
	       document.getElementById(divid).innerHTML="<img src='"+imgscr+"' width='"+width+"' height='"+height+"'/>";
		}
	};
	/**
	 * \u9690\u85cfdiv
	 * @param {Object} divid
	 */
	function hiddendDivImg(divid)
	{
	  //\u8be5div\u662f\u5426\u5b58\u5728
	  if(divid)
	  {
	  	 document.getElementById(divid).style.display="none";
	  }
	};
	//\u8fd9\u662f\u6709\u8bbe\u5b9a\u8fc7\u671f\u65f6\u95f4\u7684\u4f7f\u7528\u793a\u4f8b\uff1a
	//s20\u662f\u4ee3\u886820\u79d2
	//h\u662f\u6307\u5c0f\u65f6\uff0c\u598212\u5c0f\u65f6\u5219\u662f\uff1ah12
	//d\u662f\u5929\u6570\uff0c30\u5929\u5219\uff1ad30	
	//\u5199cookies
	function setCookie(name,value,time){
		var strsec = getsec(time);
		document.cookie = name + "="+ encodeURIComponent (value) + ";expires=" + strsec;
	};
	function getNameCookie(sid)
	{
		var path = document.location.pathname.toString();
		path = path+":"+sid;
		return path;		
	};
	//\u83b7\u53d6\u7528\u4e8e\u8fc7\u671f\u7edf\u8ba1
	function getsec(strtime){
		//\u9a8c\u8bc1\u65f6\u95f4\u683c\u5f0fyyyy-mm-dd
		var timearr = strtime.split("-");
		//\u662f\u65f6\u95f4\u683c\u5f0f
		if(timearr.length==3)
		{
			var objdate =new Object();
			objdate.value=strtime;			
			var bl = isDate(objdate);
			if(bl==undefined)
			{
				bl = true;
			}			
			//\u662f\u6b63\u786e\u7684\u65f6\u95f4\u683c\u5f0f
			if(bl)
			{
				var yeararr = strtime.split("-");
                var year =yeararr[0];
				var month = yeararr[1];
				var day = yeararr[2];
				var utc=Date.UTC(year,month-1,day);
				var date = new Date(utc);
				return date.toGMTString();
			}			
		}else if(strtime)
		{
			//\u5982\u679c\u662f\u4e0b\u4e2a\u6708
			if(strtime.indexOf("DAY")!=-1)
			{
			    var monthday =strtime.substring(3,strtime.length);
				var nowtime = new Date();
				var nowmonth = nowtime.getMonth();
				var nowyear = nowtime.getFullYear();
				if(nowmonth ==11)
				{
					nowyear += 1;
					nowmonth = "01";					
				}
				var utc=Date.UTC(nowyear,nowmonth,monthday);
				var date = new Date(utc);
				return date.toGMTString();				
				
			}else//\u5982\u679c\u662f\u4e0b\u4e2a\u661f\u671f\u6216\u8005\u662f\u5f53\u5929\u7684\u67d0\u4e2a\u65f6\u523b
			{
				var weekday = new Date();
				var d = weekday.getDay();
				if(d==0)
				{
					d=7;
				}	            
				switch(strtime)
				{
					case "MON":
                       return getDisDate(d,1);						
					case "TUE":
					   return getDisDate(d,2);
					case "WED":
					   return getDisDate(d,3);
					case "THU":
					   return getDisDate(d,4);
					case "FRI":
					   return getDisDate(d,5);
					case "SAT":
					   return getDisDate(d,6);
					case "SUN":
					   return getDisDate(d,7);																						
				}				
			}
		}
	};
	function getDisDate(disDate,passweekday)
	{
		
		var numObj ;
		if(disDate>passweekday)
		{
			numObj = passweekday - disDate+7;
		}else if(disDate==passweekday)
		{
			numObj=7;
		}else if(disDate<passweekday)
		{
			numObj =passweekday - disDate;
		}		
		var utcm = numObj*60*60*24*1000;
		var cdate = new Date();
		var year = cdate.getYear();
		var month = cdate.getMonth();
		var day = cdate.getDate();

		var utc=Date.UTC(year,month,day);
		utc+=utcm;
		var date = new Date(utc);
		return date;		
	};
	//\u8bfb\u53d6cookies
	function getCacheCookie(name)
	{
		var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
		arr=document.cookie.match(reg);
		if(arr){ return decodeURIComponent(arr[2]);}
		else {return null;}
	};
	//\u5220\u9664\u4e00\u4e2acookies
	function iterativeCookie(name)
	{
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval=getCacheCookie(name);
		if(cval!=null) {document.cookie= name + "="+cval+";expires="+exp.toGMTString();}			
	};
	//\u591a\u4e2asid\u7f13\u5b58\u7684\u5220\u9664
	function delCookie(name)
	{
	    //\u5220\u9664\u6240\u6709\u7684
		//\u5982\u679c\u53ea\u6709\u4e00\u4e2asid
		if(!name)
		{
			var allservice = pageContextCache.serviceArr;
			for(var i=0;i<allservice.length;i++)
			{
				if(allservice[i].cache==true||allservice[i].cache=='true') iterativeCookie(allservice[i].id);
			}
			return;
		}
		if(name.indexOf(";")==-1)
		{
			  iterativeCookie(getNameCookie(name));
		      return;
		}else//\u5982\u679c\u6709\u591a\u4e2asid
		{
			var namearr=name.split(";");
			for(var i=0;i<namearr.length;i++)
			{
				if(namearr[i]) 
				{
					iterativeCookie(getNameCookie(namearr[i]));
				}
			}
			return;
		}
	};
			//
			//
			//\u6811\u7684\u7236\u7c7b
			//
var Tree_init=function(){};

//addOnloadEvent(Tree_init);

var SSB_Tree=function (treeData,div_id){
	var obj={};
	this.nodeListeners={};
	this.model={};
	this.setData(treeData);
	
	this.setObjLabel(div_id);
	
	var field_id="id";

	var field_name="name";

	var field_children="children";
	
	var field_parentId="parentId";
	
	var field_enabled="enabled";
	
	var target="_self";
	
	var href="";
	
	var delay=false;
	
	var service="";
	
	//\u83dc\u5355\u662f\u5426\u53ef\u7528\u7684\u6807\u5fd7\u3002 Y\u53ef\u7528\uff0c N\u4e0d\u53ef\u7528 ,\u5f53\u83dc\u5355\u4e0d\u88ab\u7981\u7528\u65f6\uff0c\u83dc\u5355\u53d8\u7070
	var enabledFlag ="0";
	//\u6570\u636e\u662f\u5426\u5df2\u7ecf\u662f\u683c\u5f0f\u5316\u7684\u6811\u5f62\u6570\u636e
	this.isFormated=true;
	
	//\u6811\u7684onselectchange\u4e8b\u4ef6\u7684\u5904\u7406\u51fd\u6570
	this.onselectchange=function(pre,last){};

	//properties
	this.setField_id=function(f_id){
		field_id=f_id;
	};
	this.setField_name=function(f_name){
		field_name=f_name;
	};
	this.setField_children=function(f_children){
		field_children=f_children;
	};
	this.setField_parentId=function(f_parentId){
		//\u8bbe\u7f6e\u8fd9\u4e2a\u5b57\u6bb5\u8bf4\u660e\u6ca1\u6709\u88ab\u683c\u5f0f\u5316
		this.isFormated=false;
		field_parentId=f_parentId;
	};
	this.setField_enabled=function(f_enabled)
	{
	     field_enabled=f_enabled;
	};
	this.setEnabledFlag=function(flg)
	{
	     enabledFlag=flg;
	};
	this.setHref=function(h){
		href=h;
	};
	this.setTarget=function(t){
		target=t;
	};
	this.setDelay=function(d){
		delay=d;
	};
	this.setService=function(s){
		service=s;
	};
	
	this.getField_id=function(){
		return field_id;
	};
	this.getField_name=function(){
		return field_name;
	};
	this.getField_children=function(){
		return field_children;
	};
	this.getField_parentId=function(){
		return field_parentId;
	};
	this.getField_enabled=function()
	{
	   return field_enabled;
	};
    this.getEnabledFlag=function()
    {
        return enabledFlag;
    };
	this.getHref=function(){
		return href;
	};
	this.getTarget=function(){
		return target;
	};
	this.getDelay=function(){
		return delay;
	};
	this.getService=function(){
		return service;
	};
	
	this.setOnselectchange=function(fun){
		this.onselectchange=fun;
	};

	/**
	 * private function
	 * \u83b7\u5f97\u7f3a\u7701\u7684\u56fe\u6807
	 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
	 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
	 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
	 * @param {HTMLDivElement}	levelDiv	\u5f53\u524d\u5c42\u6b21\u7684DIV\u6807\u7b7e\u5bf9\u8c61
	 * @return{HTMLImageElement}			\u7f3a\u7701\u7684\u56fe\u6807
	 */
	
	this.getDefaultIcon=function(element,level,index,parent){
		//\u5bf9\u8c61ID
		var _icon=document.createElement("INPUT");
		_icon.type='button';
		var src="";
		var length=this.getChildren(parent).length;
		var div_id="div_"+SSB_Tree.Path;
		var isLast=(1+index)==length;
		switch(true){
			case this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.leafIcon;break;
			case this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lleafIcon;break;
			case !this.isLeaf(element)&&!isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u4f46\u4e0d\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.closeIcon;
				_icon["onclick"]=SSB_Tree.defaultIconEvent;break;
			case !this.isLeaf(element)&&isLast://\u5982\u679c\u5f53\u524d\u5bf9\u8c61\u4e0d\u662f\u53f6\u7ed3\u70b9\u5e76\u4e14\u662f\u5f53\u524d\u5c42\u7684\u6700\u540e\u4e00\u4e2a
				src=this.lcloseIcon;
				_icon["onclick"]=SSB_Tree.defaultLIconEvent;break;
		}
		_icon.setAttribute("tree_id",this.tree_id);
		_icon.setAttribute("div_id",div_id);		
		_icon.className=src;
		//\u8c03\u7528public\u65b9\u6cd5(\u7f3a\u7701\u4e3a\u7a7a)
		this.setIcon(element,level,index,_icon);
		return _icon;
	};
	
	/** 
	 * private function
	 * \u83b7\u5f97\u7f3a\u7701\u7684\u8282\u70b9\u7684HTML\u5143\u7d20
	 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
	 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
	 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
	 * @param {HTMLDivElement}	levelDiv	\u5f53\u524d\u5c42\u6b21\u7684DIV\u6807\u7b7e\u5bf9\u8c61
	 * @return{HTMLSpanElement}				\u7f3a\u7701\u7684\u8282\u70b9\u7684HTML\u5143\u7d20
	 */ 
	 this.getDefaultNode=function(element,level,index,parent){
		var _node=document.createElement("DIV");
		_node.className='nodeDiv';
		_node.setAttribute("id",'id_'+SSB_Tree.Path);
		_node.setAttribute("level",level);
		_node.setAttribute("istreeleaf",this.isLeaf(element));
		_node.setAttribute("tree_id",this.tree_id);
		_node["model"]=element;
		//\u8bbe\u7f6e\u5f53\u524d\u8282\u70b9\u7684isLast\u5c5e\u6027\uff0c\u56e0\u4e3a\u662f\u5426\u6700\u540e\u4e00\u4e2a\u5143\u7d20\u5bf9\u9875\u9762\u663e\u793a\u6709\u5f71\u54cd
		_node.setAttribute("isLast",1+index==this.getChildren(parent).length);
		return _node;
	};
	
	/** 
	 * private function
	 * \u8bbe\u7f6e\u8282\u70b9\u7684\u8fde\u7ebf\u7684\u663e\u793a
	 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
	 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
	 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
	 * @param {HTMLDivElement}	levelDiv	\u5f53\u524d\u5c42\u6b21\u7684DIV\u6807\u7b7e\u5bf9\u8c61
	 */ 
	this.getLine=function(element,level,index,nodeDiv){
		var lineIcon={};
		var src='';
		var iconBlank=this.blankIcon;
		var iconLine=this.lineIcon;
		//SSB_Tree.treeNodeStack\u662f\u8282\u70b9\u961f\u5217\uff0c\u56e0\u4e3a\u9700\u8981\u77e5\u9053\u5f53\u524d\u8282\u70b9\u8def\u5f84\u4e2d\u7684\u7236\u8282\u70b9\u662f\u5426\u662f\u5f53\u524d\u5c42\u7684\u6700\u6709\u4e00\u4e2a
		for(var i=0;i<SSB_Tree.treeNodeStack.length;i++){
			//\u5982\u679c\u8def\u5f84\u4e2d\u7684\u67d0\u4e00\u5c42\u662f\u6700\u540e\u4e00\u4e2a\u90a3\u4e48\u4e0d\u5e94\u8be5\u663e\u793a\u8fde\u7ebf
			var isLast=SSB_Tree.treeNodeStack[i];
			if(isLast){
				src=iconBlank;
			//\u5426\u5219\u663e\u793a\u8fde\u7ebf
			}else{
				src=iconLine;
			}
			lineIcon=document.createElement("INPUT");
			lineIcon.type='button';
			lineIcon.className=src;
			nodeDiv.appendChild(lineIcon);
		}
	};
	
	/** 
	 * private function
	 * \u521b\u5efa\u4e0b\u4e00\u5c42\u6811\u7684DIV\u5143\u7d20
	 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
	 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
	 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
	 * @param {HTMLDivElement}	parentDiv	\u4e0a\u4e00\u5c42DIV\u6807\u7b7e\u5bf9\u8c61
	 * @return{HTMLDivElement}				\u65b0\u7684\u5c42\u7684DIV\u6807\u7b7e\u5bf9\u8c61
     */
	this.nextLevel=function(element,level,index,parent){
		var level_id="div_"+SSB_Tree.Path;
		var div=document.createElement("DIV");
		var node_id="node_"+SSB_Tree.Path;
		div.setAttribute("id",level_id);
		//\u8bbe\u7f6e\u4e0b\u4e00\u5c42\u6240\u6709\u7236\u8282\u70b9\u7684ID
		div.setAttribute("node_id",node_id);
		div.style.display="none";
		div.setAttribute("level",level);
		div.setAttribute("newLevel","newLevel");
		return div;
	};
	this.getNodeText=function(element,level,index,node){
		var span=document.createElement('SPAN');
		span.innerHTML=element[this.getField_name()];
		var enabledFlg=element[this.getField_enabled()];
		span.setAttribute("tree_id",this.tree_id);
		span["model"]=element;
		//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u53d8\u7070
		if(this.isGray(enabledFlg))
		{
		    span.style.color="gray";
		}

		span['onclick']=SSB_Tree.eventRouter;	
		span.className='nodeText';
		return span;
	};
	this.getNode=function(element,level,index,parent){
		var nodeDiv=this.getDefaultNode(element,level,index,parent);
		this.getLine(element,level,index,nodeDiv);
		nodeDiv.appendChild(this.getDefaultIcon(element,level,index,parent));
		var userElement=this.appendBeforeText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		nodeDiv.appendChild(this.getNodeText(element,level,index,parent));
		userElement=this.appendAfterText(element,level,index,parent);
		if(userElement){
			nodeDiv.appendChild(userElement);
		}
		return nodeDiv;
	};
	/** 
	 * object function
	 * \u7528\u6765\u6784\u9020\u6811\u7684\u9012\u5f52\u65b9\u6cd5
	 * @param{Array}			tree		\u6811\u7684\u6570\u636e
	 * @param{Integer}			level   	\u6811\u7684\u5c42\u6b21
	 * @param{HTMLDivElement}	levelDiv	\u5305\u542b\u6811\u8282\u70b9\u7684DIV\u5143\u7d20
	 */
	this.plantTree=function(tree,level,parent,levelDiv){
		var nodeDiv;
		var current;
		for(var i=0;i<tree.length;i++){
			//\u5f53\u524d\u4e0b\u6807\u8fdb\u6808
			current=tree[i];
			SSB_Tree.pathStack.push(current[this.getField_id()]);
			SSB_Tree.Path = SSB_Tree.pathStack.join('_');
		
			nodeDiv=this.getNode(current,level,i,parent);

			//\u5f53\u524d\u8282\u70b9\u8fdb\u6808
			SSB_Tree.treeNodeStack.push(nodeDiv.getAttribute('isLast')+""=='true');

			this.setNode(nodeDiv);

			levelDiv.appendChild(nodeDiv);

			//\u5982\u679c\u5f53\u524d\u8282\u70b9\u4e0d\u662f\u53f6\u7ed3\u70b9\u5c31\u5904\u7406\u4ed6\u7684\u5b50\u8282\u70b9
			if(this.isBranch(current)){
				var div=this.nextLevel(current,level,i,parent);
				levelDiv.appendChild(div);
				this.plantTree(this.getChildren(current),1+level,current,div);
			}
			//\u628a\u5904\u7406\u5b8c\u7684\u8282\u70b9\u4e0b\u8868\u51fa\u6808
			SSB_Tree.pathStack.pop();
			//\u5904\u7406\u5b8c\u7684\u8282\u70b9\u51fa\u6808
			SSB_Tree.treeNodeStack.pop();
		}
	};
};

SSB_Tree.prototype={
	
	//\u7a7a\u767d\u56fe\u7247
	blankIcon:"treeIcon tree_white",
	//\u8fde\u7ebf
	lineIcon:"treeIcon tree_line",
	//\u5173\u95ed\u7684\u975e\u53f6\u8282\u70b9
	closeIcon:"treeIcon tree_plus",
	//\u6253\u5f00\u7684\u975e\u53f6\u8282\u70b9
	openIcon:"treeIcon tree_minus",
	//\u53f6\u7ed3\u70b9\u56fe\u7247
	leafIcon:"treeIcon tree_blank",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u5173\u95ed\u65f6\u7684\u56fe\u7247
	lcloseIcon:"treeIcon tree_plusl",
	//\u6700\u540e\u4e00\u4e2a\u8282\u70b9\u4e3a\u975e\u53f6\u8282\u70b9\u6253\u5f00\u65f6\u7684\u56fe\u7247
	lopenIcon:"treeIcon tree_minusl",
	//\u6700\u540e\u4e00\u4e2a\u53f6\u7ed3\u70b9
	lleafIcon:"treeIcon tree_blankl",
	
	setObjLabel:function(div_id){
		this.tree_id=div_id;
		this.objLabel=document.getElementById(div_id);
	},
	/**
	 * public function
	 * \u8bbe\u7f6e\u6811\u7684\u6570\u636e
	 * @param {Object} treeData	\u6811\u7684\u6570\u636e
	 */
	setData:function(treeData){
		if(treeData instanceof String||"string"==typeof treeData){
			var obj=eval(treeData);
		}else{
			var obj=treeData;
		}
		if(obj instanceof Array)
			this.model=obj;
		else
			this.model=[obj];
	},
	/**
	 * \u628a\u4e00\u7ef4\u6570\u7ec4\u683c\u5f0f\u5316\u6210\u4e00\u4e2a\u6811
	 */
	formatData:function(){
		var id=this.getField_id();
		var children=this.getField_children();
		var parentId=this.getField_parentId();
		var model=this.model;
		var hashMap={};
		
		var current={};
		var root={};
		root[id]='0';
		model.splice(0,0,root);		
		var length=this.model.length;
		for(var i=0;i<length;i++){
			current=model[i];
			current[children]=[];
			hashMap[current[id]]=current;
		}
		var parent={};
		for(var i=0;i<length;i++){
			current=model[i];
			parent=hashMap[current[parentId]];
			if(parent)
				parent[children].push(current);
		}
		return root[children];
	},
	
	/**
	 * public function
	 * \u5411\u7528\u6237\u5f00\u653e\u7684\u8bbe\u7f6e\u8282\u70b9\u7684\u65b9\u6cd5
	 * @param {HTMLDivElement} node	\u5f53\u524d\u8282\u70b9
	 */
	setNode:function(node){

	},
	
	setIcon:function(element,level,index,icon){
		return icon;
	},
	appendBeforeText:function(element,level,index,parent){
		return null;
	},
	appendAfterText:function(element,level,index,parent){
		return null;
	},
	isLeaf:function(element){
		if(this.getDelay()){
			var children=this.getChildren(element);
			if(!children)return true;
			return parseInt(children)==0;
		}
		return !this.isBranch(element);
	},
	isBranch:function(element){
		var children= this.getChildren(element);
		return children instanceof Array && children!=null && children.length!=0;
	},
	getChildren:function(element){
		return element[this.getField_children()];
	},

    isGray:function(enabledFlag)
    {
         var flg=this.getEnabledFlag();
         if(enabledFlag == flg)
         {
            return true;
         }
         else
         {
            return false;
         }
    },
	
	/**
	 * \u5bf9\u8c61\u521d\u59cb\u5316\u65b9\u6cd5
	 */
	initTree:function(){
		if(!this.objLabel) return;
		this.destroy();
		if(!this.getDelay()&&!this.isFormated){
			this.model=this.formatData();
		}
		var id=this.getField_id();
		var children=this.getField_children();
		this.root={};
		this.root[id]='0';
		this.root[children]=this.model;
		SSB_Tree.pathStack=[];
		SSB_Tree.treeNodeStack=[];
		SSB_Tree.Path='';
		if(!this.model)
		{
			return;
		}
		this.plantTree(this.model,1,this.root,this.objLabel);
		SSB_Tree.trees[this.tree_id]=this;
		//\u8bbe\u7f6e\u7f3a\u7701\u7684\u5355\u51fb\u4e8b\u4ef6
		this.addNodeListener("onclick",this.defaultNodeOnClick);
		
	},

	/**
	 * \u7528\u65b0\u7684\u6570\u636e\u5237\u65b0\u6811
	 * @param {Array} treeData
	 */
	refresh:function(treeData){
		if(treeData)
			this.setData(treeData);
		if(this.objLabel){
			this.objLabel.innerHTML="";
			this.initTree();
		}
	},

	/**
	 * \u9500\u6bc1\u6811
	 */
	destroy:function(){
		//\u628a\u6807\u7b7e\u5185\u7684HTML\u7f6e\u7a7a
		if(this.objLabel)
			this.objLabel.innerHTML="";
		if(SSB_Tree.trees[this.tree_id]){
			//\u5220\u6389SSB_Tree.trees\u4e2d\u7684\u8fd9\u4e2a\u6811
			delete SSB_Tree.trees[this.tree_id];
			//\u628a\u5f53\u524d\u4e66\u7684\u76d1\u542c\u5668\u5bf9\u8c61\u7f6e\u7a7a
			this.nodeListeners={};
		}
	},

	gotoHref:function(node){
		 var evalHref=function(el,level,href){
		 	eval("var element=arguments[0];");
			var el_re=/\$\{([^\}]+)\}/;
			var tmp=href;
			var result=[];
			var a=[];
			if(!(a=el_re.exec(tmp))){
				return href;
			}
			while(a){
				result.push(RegExp.leftContext);
				result.push(eval(a[1]));
				a=el_re.exec(RegExp.rightContext);
			}
			result.push(RegExp.rightContext);
			return result.join("");
		};
		var href=this.getHref();
		if(href){
			href=evalHref(node["model"],node.getAttribute("level"),href);
			window.open(href,this.getTarget());
		}
	},
	defaultNodeOnClick:function(oEvent){
		var current_tree =SSB_Tree.getTreeByNode(this);
		if(current_tree.lastclick)
			current_tree.lastclick.style.fontWeight='normal';
		this.style.fontWeight='bolder';
		current_tree.lastclick=this;
		oEvent=oEvent||window.event;
		var node=oEvent.target||oEvent.srcElement;
		current_tree.gotoHref(node);
	},

	/**
	 * public function
	 * \u7ed9\u8282\u70b9\u6dfb\u52a0\u4e8b\u4ef6\u5904\u7406\u51fd\u6570
	 * @param {String} 		event_type	\u4e8b\u4ef6\u7c7b\u578b
	 * @param {Function} 	handler		\u4e8b\u4ef6\u5904\u7406\u51fd\u6570
	 */
	addNodeListener:function(event_type,handler){
		if(!this.nodeListeners[event_type]){
			this.nodeListeners[event_type]=[];
		}
		//\u5f53\u8be5\u4e8b\u4ef6\u7c7b\u578b \u5df2\u7ecf\u5b58\u5728\u8be5\u4e8b\u4ef6\u5904\u7406\u51fd\u6570\u65f6\uff0c\u4e0d\u80fd\u91cd\u590d\u6dfb\u52a0\u6b64\u51fd\u6570 \uff0c\u907f\u514d\u8be5\u51fd\u6570\u88ab\u6267\u884c\u591a\u6b21
		var listener=this.nodeListeners[event_type];		
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				return;
			}
		}
		
		this.nodeListeners[event_type].push(handler);		
		
		var spans = document.getElementById(this.tree_id).getElementsByTagName("SPAN");
		//\u5c06\u6240\u6709\u83dc\u5355\u8282\u70b9\u52a0\u4e0a\u4e8b\u4ef6 2008-5-5
		for(var i=0;i<spans.length;i++)
		{
			spans[i][event_type] = SSB_Tree.eventRouter;
		}
	
	},
	
	removeNodeListener:function(event_type,handler){
		var listener=this.nodeListeners[event_type];
		if(!listener){
			return;
		}
		for(var i=0;i<listener.length;i++){
			if(listener[i]==handler){
				listener.splice(i,1);
				return;
			}
		}
	},
	doselectchange:function(last){
		if(this.lastclick&&this.lastclick!=last)
			this.onselectchange(this.lastclick,last);
	},
	getParams:function(node){
		var evalParam=function(el,str){
			eval("var element=arguments[0];");
			return eval(str);
		};
		var params=[];
		if(!this.params)return;
		for(var i=0;i<this.params.length;i++){
			if(typeof this.params[i]=='object'){
				params.push(this.params[i].value);
			}else
				params.push(evalParam(node['model'],this.params[i]));
		}
		return params;
	},
	selectNode:function(element){
		var spans=this.objLabel.getElementsByTagName('SPAN');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}

		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				if(spans[i].fireEvent){
					spans[i].fireEvent('onclick');
				}else if(spans[i].dispatchEvent){
					spans[i].dispatchEvent('click');
				}
				return true;
			}
		}
		return false;			
	},
	openNode:function(element){
		var spans=this.objLabel.getElementsByTagName('SPAN');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		var elementId='';
		if(typeof element=='object')
			elementId=element[id];
		else if(typeof element=='string'){
			elementId=element;
		}
		var isTreeLeaf='';
		var divNode={};
		var inputs={};
		for(var i=0;i<length;i++){
			//\u627e\u5230\u8282\u70b9
			if(spans[i]['model'][id]==elementId){
				divNode=spans[i].parentNode;
				isTreeLeaf=divNode.getAttribute('istreeleaf');
				//\u975e\u53f6\u8282\u70b9
				if(isTreeLeaf=='false'||!isTreeLeaf){
					inputs=divNode.getElementsByTagName('INPUT');
					for(var j=0;j<inputs.length;j++){
						if(inputs[j].getAttribute('tree_id')){
							if(inputs[j].fireEvent){
								inputs[j].fireEvent('onclick');
							}else if(inputs[j].dispatchEvent){
								inputs[j].dispatchEvent('click');
							}
							break;
						}
					}
				}
				return true;
			}
		}
		return false;
	},
	addNode:function(element){
		var spans=this.objLabel.getElementsByTagName('SPAN');
		var length=spans.length;
		var pid=this.getField_parentId();
		var id=this.getField_id();
		var children=this.getField_children();
		//\u5982\u679c\u6dfb\u52a0\u7684\u662f\u7b2c\u4e00\u5c42\u8282\u70b9
		if(element[pid]=='0'){
			this.model.push(element);
			this.objLabel.innerHTML='';
			SSB_Tree.pathStack=[];
			SSB_Tree.treeNodeStack=[];
			SSB_Tree.Path='';
			this.plantTree(this.model,1,this.root,this.objLabel);
		}
		for(var i=0;i<length;i++){
			if(element[pid]==spans[i]['model'][id]){
				var parentNode=spans[i].parentNode;
				//\u5982\u679c\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d
				// \u674e\u666f\u6797\u4fee\u6539  
				if(!SSB_Tree.noUpdate(parentNode) || !this.getDelay()){
					var parent=spans[i]['model'];
					var sibling=this.getChildren(parent);
					if(sibling == null || typeof(sibling) == 'undefined')
					{
						sibling = new Array();
						parent.children = sibling;
					}
					var index=sibling.length;
					var levelDiv=parentNode.nextSibling;
					var parentLevel=parentNode.parentNode;
					//\u5148\u628a\u65e7\u7684\u8282\u70b9\u5c42\u79fb\u9664\u6389
					// \u674e\u666f\u6797\u4fee\u6539  \u5220\u9664\u9519\u8bef\u7684\u95ee\u9898
					if(levelDiv != null) parentLevel.removeChild(levelDiv);
					//\u628a\u65b0\u7684\u6a21\u578b\u5143\u7d20\u52a0\u5165\u5b50\u8282\u70b9\u4e2d
					sibling.push(element);
					//\u7136\u540e\u518d\u66f4\u65b0\u4ed6
					SSB_Tree.updateChildren.call(parentNode,0,sibling);
				}else{
				   //\u5982\u679c\u8fd8\u6ca1\u6709\u88ab\u52a0\u8f7d, \u6216\u8005\u8282\u70b9\u4e3a\u60f0\u6027\u52a0\u8f7d\u65f6\uff0c\u8981\u53bb\u8c03\u7528\u540e\u53f0\u670d\u52a1\u67e5\u627e\u8282\u70b9\u4fe1\u606f
					var params=this.getParams(parentNode);
					callMethod(this.getService(),params,parentNode,SSB_Tree.updateChildren);
				}
				break;
			}
		}
	},
	refreshNode:function(element){
		var spans=this.objLabel.getElementsByTagName('SPAN');
		var length=spans.length;
		var field_Id=this.getField_id();
		var name=this.getField_name();
		var children=this.getField_children();
		var id;
		if(typeof element=='object')
			id=element[field_Id];
		else if(typeof element=='string'){
			id=element;
		}
		for(var i=0;i<length;i++){
			if(id==spans[i]['model'][field_Id]){
				spans[i].innerHTML=element[name];
				var old=spans[i]['model'];
				spans[i]['model']=element;
				var divNode=spans[i].parentNode;
				divNode['model']=element;
				var model=[];
				if(divNode.parentNode.tagName!='DIV')
					model=this.model;
				else{
					var parentNode=divNode.parentNode.previousSibling;
					model=parentNode['model'][children];
				}
				for(var j=0;j<model.length;j++){
					if(model[j]==old){
						model.splice(i,1,element);break;
					}
				}
				break;
			}
		}
	},
	deleteNode:function(deleted){
		//check argument
		if(deleted == null)
		{
			return;
		}
		//if is not array ,add arg into array
		if(! (deleted instanceof Array))
		{
			deleted = [deleted];
		}
		var spans=this.objLabel.getElementsByTagName('SPAN');
		var length=spans.length;
		var length2=deleted.length;
		var id=this.getField_id();
		var children = this.getField_children();
		for(var i=length -1 ;i>= 0;i--){
			for(var j=0;j<length2;j++){
				var element=spans[i]['model'];
				if(deleted[j]==element[id]){
					var nodeDiv=spans[i].parentNode;
					var levelDiv=nodeDiv.parentNode;
					var tmp=nodeDiv.previousSibling;
					var previousNode=tmp!=null&&tmp.getAttribute('newLevel')=='newLevel'?tmp.previousSibling:tmp;
					var childrenDiv = nodeDiv.nextSibling;
					var parentNode = levelDiv.previousSibling;

					//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u90a3\u8981\u4fee\u6539\u5012\u6570\u7b2c\u4e8c\u4e2a\u8282\u70b9
					if(nodeDiv.getAttribute('isLast')+''=='true'){
						if(previousNode){//\u662f\u6700\u540e\u4e00\u4e2a\u4f46\u4e0d\u552f\u4e00\u3002\u6709\u524d\u7eed\u8282\u70b9\u3002
							var previousIcon = SSB_Tree.getIconByNode(previousNode);
							previousIcon.className=previousIcon.className+'l';
							//\u5982\u679c\u88ab\u5220\u9664\u7684\u662f\u6700\u540e\u4e00\u4e2a,\u5012\u6570\u7b2c\u4e8c\u4e2a\u53c8\u4e0d\u662f\u53f6\u7ed3\u70b9
							if(previousNode.getAttribute('istreeleaf')+''=='false'){
								//\u4fee\u6539onclick\u4e8b\u4ef6,\u56e0\u4e3a\u66f4\u6539\u7684\u56fe\u6807\u4f1a\u4e0d\u4e00\u6837
								previousIcon.className=this.lcloseIcon;
								previousIcon['onclick']=SSB_Tree.defaultLIconEvent;
								//\u5982\u679c\u4e4b\u524d\u7684\u8282\u70b9\u7684\u5b50\u8282\u70b9\u5df2\u7ecf\u88ab\u52a0\u8f7d\u5c31\u628a\u4ed6\u4eec\u5220\u9664\uff0c\u8ba9\u4ed6\u91cd\u65b0\u52a0\u8f7d\uff0c\u4e0d\u7136\u4f1a\u51fa\u95ee\u9898
								if(!SSB_Tree.noUpdate(previousNode)){
									levelDiv.removeChild(tmp);
								}
							}
							levelDiv.removeChild(nodeDiv);
							//\u5982\u679c\u6709\u5b50\u8282\u70b9\u4e5f\u4e00\u8d77\u5220\u9664
							if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
								levelDiv.removeChild(childrenDiv);
							}
							previousNode.setAttribute('isLast','true');
						}else{//\u552f\u4e00\u7684
								//\u5982\u679c\u8fd9\u4e2a\u8282\u70b9\u662f\u7b2c\u4e00\u5c42\u7684\u5e76\u4e14\u552f\u4e00
							if(levelDiv.tagName!='DIV'){
								this.objLabel.innerHTML='';
							}else{
								//\u4e0d\u662f\u7b2c\u4e00\u5c42\u7684\u8282\u70b9
								//\u88ab\u5220\u9664\u7684\u662f\u552f\u4e00\u8282\u70b9,\u8981\u628a\u4ed6\u7684\u7236\u8282\u70b9\u8bbe\u7f6e\u6210\u53f6\u8282\u70b9
								var parentLevel = levelDiv.parentNode;
								var parentIcon = SSB_Tree.getIconByNode(parentNode);
								var className = parentIcon.className;
								if(className.charAt(-1+className.length)=='l')
									parentIcon.className=this.lleafIcon;
								else
									parentIcon.className=this.leafIcon;
									//\u8fd9\u4e2a\u8282\u70b9\u6240\u5728\u7684\u5c42\u5c06\u88ab\u4e00\u8d77\u5220\u9664
								parentLevel.removeChild(levelDiv);
								parentNode.setAttribute('istreeleaf','true');
								parentNode['model'][children]=0;
							}
						}
					}
					//\u5982\u679c\u4e0d\u662f\u6700\u540e\u4e00\u4e2a\u53ea\u7ba1\u5220\u9664\u5b83\u548c\u5b83\u7684\u5b57\u8282\u70b9
					else {
						levelDiv.removeChild(nodeDiv);
						//\u5982\u679c\u6709\u5b50\u8282\u70b9
						if(childrenDiv&&childrenDiv.getAttribute('newLevel')){
							levelDiv.removeChild(childrenDiv);
						}
					}
					//\u66f4\u65b0model
					var childElement=[];
					if(element[this.getField_parentId()]=='0'){
						childElement=this.model;
					}else{
						childElement=parentNode['model'][children];
					}
					for(var i=0;i<childElement.length;i++){
						if(childElement[i]==element){
							childElement.splice(i,1);break;
						}
					}
				}
			}
		}
	}
};
	//static properties and function
	//\u4e0b\u6807\u5806\u6808
SSB_Tree.pathStack = [];
SSB_Tree.Path='';
	//\u8282\u70b9\u961f\u5217
SSB_Tree.treeNodeStack=[];

SSB_Tree.trees={};

SSB_Tree.getTreeByNode=function(obj){
	return SSB_Tree.trees[obj.getAttribute("tree_id")];
};

SSB_Tree.eventRouter=function(oEvent){
	var oEvent = oEvent || window.event;
	var event_type="on"+oEvent.type;
		//\u83b7\u53d6\u53d1\u751f\u4e8b\u4ef6\u7684\u6811
	var tree = SSB_Tree.getTreeByNode(this);
	tree.doselectchange(this);
		//\u83b7\u53d6\u5bf9\u5e94\u4e8b\u4ef6\u76d1\u542c\u5668\u5217\u8868
	var listeners=tree.nodeListeners[event_type];
	if(!listeners)return;
	for(var i=0;i<listeners.length;i++){
		listeners[i].call(this,oEvent);
	}
};
SSB_Tree.getIconByNode = function(node){
	var divs=node.getElementsByTagName('INPUT');
	var length=divs.length;
	for(var i=0;i<length;i++){
		if(divs[i].getAttribute('div_id')!=null){
			return divs[i];
		}
	}
};
SSB_Tree.updateChildren=function(s,children){
	var tree=SSB_Tree.getTreeByNode(this);
	var divNode=this;
	var img=SSB_Tree.getIconByNode(divNode);
	//\u5982\u679c\u6ca1\u6709\u53ef\u66f4\u65b0\u7684\u5b57\u8282\u70b9
	if(children.length==0){
		if(divNode.getAttribute('isLast')+''=='true') img.className=tree.lleafIcon;
		else img.className=tree.leafIcon;
		img.onclick=null;
		return;
	}
	//\u8bbe\u7f6e\u8282\u70b9\u8def\u5f84\u4fe1\u606f
	SSB_Tree.setNodePath(divNode);
	var level=parseInt(divNode.getAttribute("level"));
	//\u65b0\u5c42\u6b21\u7684DIV
	var div=tree.nextLevel({},level,0,{});
	div.style.display='block';
	divNode['model'][tree.getField_children()]=children;
	//\u5f80\u65b0\u5c42\u6b21\u7684DIV\u4e2d\u6dfb\u52a0\u65b0\u7684\u8282\u70b9
	tree.plantTree(children,1+level,divNode['model'],div);
	
	var obj=divNode.nextSibling;
	var parent=divNode.parentNode;
	//\u628a\u65b0\u7684\u5c42\u6b21\u52a0\u5165\u5230\u6811\u4e2d
	if(obj)
		parent.insertBefore(div,obj);
	else
		parent.appendChild(div);
	var isLast=divNode.getAttribute('isLast')+'';
	//\u6539\u53d8\u88ab\u66f4\u65b0\u7684\u8282\u70b9\u7684\u56fe\u6807
	if(isLast=='true') {
		img.className=tree.lopenIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultLIconEvent;
		}
	}
	else {
		img.className=tree.openIcon;
		if(!img['onclick']){
			img['onclick']=SSB_Tree.defaultIconEvent;
		}
	}
	divNode.setAttribute('istreeleaf','false');
};

SSB_Tree.setNodePath=function(node){
	var div={};
	SSB_Tree.pathStack=[];
	SSB_Tree.treeNodeStack=[];
	SSB_Tree.Path='';
	var tree=SSB_Tree.getTreeByNode(node);
	do{
		SSB_Tree.treeNodeStack.push(node.getAttribute('isLast')+''=='true');
		SSB_Tree.pathStack.push(node['model'][tree.getField_id()]);
		div=node.parentNode;
		node=div.previousSibling;
	}while(div.tagName=='DIV');
	SSB_Tree.pathStack.reverse();
	SSB_Tree.treeNodeStack.reverse();
	SSB_Tree.Path=SSB_Tree.pathStack.join('_');
};

SSB_Tree.noUpdate=function(node){
	var isLast=node.getAttribute('isLast')+'';
	if(isLast=='true')
		return node.nextSibling==null;
	return node.nextSibling.getAttribute('newLevel')!='newLevel';
};



SSB_Tree.handleUpdate=function(img,tree){
	if(tree.getDelay()){
		var divNode=img.parentNode;
		var params=tree.getParams(divNode);
		callMethod(tree.getService(),params,divNode,SSB_Tree.updateChildren);
	}
};
SSB_Tree.defaultIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree = SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.openIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.closeIcon;
	}
};

SSB_Tree.defaultLIconEvent=function(){
	var div=document.getElementById(this.getAttribute("div_id"));
	var current_tree =SSB_Tree.getTreeByNode(this);
	if(current_tree.getDelay()&&SSB_Tree.noUpdate(this.parentNode))
		SSB_Tree.handleUpdate(this,current_tree);
	else if(div.style.display=="none"){
		div.style.display="block";
		this.className=current_tree.lopenIcon;
	}else{
		div.style.display="none";
		this.className=current_tree.lcloseIcon;
	}
};


			//
			//\u591a\u9009\u6811
			//
function SSB_MultiTree(treeData,div_id,values,autoselect){
	SSB_Tree.call(this, treeData,div_id);
	this.setValues(values);
	this.autoselect=autoselect||false;
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_MultiTree.prototype=new SSB_Tree();
//\u91cd\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_MultiTree.prototype.constructor =SSB_MultiTree;


/**
 * public function
 * \u91cd\u5199\u4e86SSB_Tree\u4e2d\u7684setNode\u65b9\u6cd5
 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
 * @param {HTMLSpanElement}	node
 * @return{HTMLDivElement}	\u65b0\u7684\u8282\u70b9
 */
SSB_MultiTree.prototype.appendBeforeText=function(element,level,index,parent){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	var box=[];
	box.push("<INPUT type=checkbox style='margin:0px' value='");
	box.push(value);
	box.push("' name='checkbox_");
	box.push(this.tree_id);
	box.push("' onclick='SSB_MultiTree.checkTheBoxs(this)' ");
	for(var i=0;i<this.selectedValues.length;i++){
		if(value==this.selectedValues[i]){
			box.push("checked=true");
		}
	}
	box.push(">");
	div.innerHTML=box.join("");
	div.style.display="inline";
	
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u591a\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

//\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
SSB_MultiTree.prototype.selectedValues=[];

/**
 * \u8bbe\u7f6e\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
 * @param {Array} values	\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
 */
SSB_MultiTree.prototype.setValues=function(values){
	if(values&&values instanceof Array){
		this.selectedValues=values;
	}
	else{
		this.selectedValues=[];
	}
};

/**
 * \u83b7\u5f97\u9009\u5b9a\u7684\u503c
 * @return{Array}		\u9009\u5b9a\u7684\u503c\u6570\u7ec4
 */
SSB_MultiTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName('INPUT');
	var values=[];
	for(var i=0;i<checkboxs.length;i++){		
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].value);
		}
	}
	return values;
};
SSB_MultiTree.prototype.getObjValue=function(){
	var treeDiv=this.objLabel;
	var checkboxs=treeDiv.getElementsByTagName("INPUT");
	var values=[];
	for(var i=0;i<checkboxs.length;i++){
		if(checkboxs[i].checked&&checkboxs[i].type.toLowerCase()=='checkbox'){
			values.push(checkboxs[i].parentNode.parentNode['model']);
		}
	}
	return values;
};
SSB_MultiTree.prototype.refresh=function(treeData,values){
	if(treeData)
		this.setData(treeData);
	if(values)
		this.setValues(values);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};
SSB_MultiTree.checkTheBoxs=function(box){
	var name=box.getAttribute("name");
	var treeIdRE=/^checkbox_(\S+)$/;
	var treeId=treeIdRE.exec(name)[1];
	var tree=SSB_Tree.trees[treeId];
	if(tree&&tree.autoselect){
		var span=box.parentNode.nextSibling;
		if(span.getAttribute('istreeleaf')=='false'||!span.getAttribute('istreeleaf')){
			var levelDiv=span.parentNode.nextSibling;
			if(!levelDiv||!levelDiv.getAttribute("newLevel"))return;
			var checkboxs=levelDiv.getElementsByTagName('INPUT');
			for(var i=0;i<checkboxs.length;i++){
				checkboxs[i].checked=box.checked;
			}
		}
	}
};



			//
			//\u5355\u9009\u6811
			//
var SSB_SingleTree=function(treeData,div_id,value){
	SSB_Tree.call(this, treeData,div_id);
	//\u9ed8\u8ba4\u7684\u9009\u5b9a
	this.selectedValue="";
	this.setValue(value);
};
//\u7ee7\u627fSSB_Tree\u5bf9\u8c61\u7684\u5c5e\u6027
SSB_SingleTree.prototype=new SSB_Tree();
//\u4ece\u65b0\u8bbe\u7f6e SSB_MultiTree.prototype.constructor \u5c5e\u6027
SSB_SingleTree.prototype.constructor =SSB_SingleTree;

/**
 * public function
 * \u91cd\u5199\u4e86SSB_Tree\u4e2d\u7684setNode\u65b9\u6cd5
 * @param {Object} 			element		\u5f53\u524d\u5bf9\u8c61
 * @param {Integer} 		level		\u5f53\u524d\u5c42\u6b21
 * @param {Integer} 		index		\u5143\u7d20\u5728\u5f53\u524d\u5c42\u7684\u4e0b\u6807
 * @param {HTMLSpanElement}	node
 * @return{HTMLDivElement}	\u65b0\u7684\u8282\u70b9
 */
SSB_SingleTree.prototype.appendBeforeText=function(element,level,index,node){
	var div=document.createElement("DIV");
	var value=element[this.getField_id()];
	
	//\u5728IE\u4e2d\u7528document.createElement\u52a8\u6001\u521b\u5efa\u7684HTML\u5bf9\u8c61\uff0c\u4e0d\u80fd\u52a8\u6001\u5730\u8bbe\u7f6e\u5b83\u7684name\u5c5e\u6027\uff0c
	//\u800c\u8fd9\u91cc\u5fc5\u987b\u7528radio button\u7684name\u5c5e\u6027\uff0c\u6240\u4ee5\u8fd9\u91cc\u7528\u62fc\u5b57\u7b26\u4e32\u7684\u65b9\u5f0f\u3002
	var radio=[];
	radio.push("<INPUT");
	radio.push("type=radio");
	radio.push("value="+value);
	radio.push("name=radio_"+this.tree_id);
	if(value==this.selectedValue){
		radio.push("checked=true");
	}
	radio.push(">");

	div.innerHTML=radio.join(" ");

	div.style.display="inline";
	//var enabledFlg = element[this.getField_enabled()];
	//\u5c06\u7981\u7528\u7684\u83dc\u5355\u8282\u70b9\u7684\u5355\u9009\u6846\u6309\u94ae\u53d8\u7070
	//if(this.isGray(enabledFlg))
	//{
	//   div.disabled=true;
	//}
	//\u8fd4\u56de\u65b0\u7684\u8282\u70b9\u5143\u7d20
	return div;
};

/**
 * \u8bbe\u7f6e\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
 * @param {Array} values	\u9ed8\u8ba4\u7684\u9009\u5b9a\u503c\u6570\u7ec4
 */
SSB_SingleTree.prototype.setValue=function(value){
	if(value&&""!=value){
		this.selectedValue=value;
	}
	else{
		this.selectedValue="";
	}
};

/**
 * \u83b7\u5f97\u9009\u5b9a\u7684\u503c
 * @return{Array}		\u9009\u5b9a\u7684\u503c\u6570\u7ec4
 */
SSB_SingleTree.prototype.getValue=function(){
	var treeDiv=this.objLabel;
	var radios=treeDiv.getElementsByTagName("INPUT");

	for(var i=0;i<radios.length;i++){
		if(radios[i].checked&&radios[i].type.toLowerCase()=='radio'){
			return radios[i].value;
		}
	}
};

SSB_SingleTree.prototype.refresh=function(treeData,value){
	if(treeData)
		this.setData(treeData);
	if(value)
		this.setValue(value);
	if(this.objLabel){
		this.objLabel.innerHTML="";
		this.init();
	}
};



			//
			//\u6811\u5de5\u5382\uff0c\u6839\u636e\u6807\u7b7eID\u8fd4\u56de\u6811\u5bf9\u8c61
			//
var Tree_Factory=function (data,label_id){
	var labelObj=document.getElementById(label_id);
	var treeType=labelObj.getAttribute("treeType");
	var field_id=labelObj.getAttribute("field_id");
	var field_name=labelObj.getAttribute("field_name");
	var field_children=labelObj.getAttribute("field_children");
	var field_parentId=labelObj.getAttribute("field_parentId");
	var field_enabled=labelObj.getAttribute("field_enabled");
	var enabledFlag=labelObj.getAttribute("enabledFlag");
	var href=labelObj.getAttribute('href');
	var target=labelObj.getAttribute('target');
	var autoselect=labelObj.getAttribute('autoselect')=="true";
	var params=labelObj.getAttribute('params');
	var service=labelObj.getAttribute("service");
	var delay=labelObj.getAttribute("delay")=='true'; 
	if(data.treeType){
		treeType=data.treeType;
	}
	var initValue=data.initValue;
	var selectedValue=data.selectedValue;
	initValue=!initValue?data:initValue;
	var tree={};
	switch(treeType){
		case "0":tree=new SSB_Tree(initValue,label_id);break;
		case "1":tree=new SSB_SingleTree(initValue,label_id,selectedValue);break;
		case "2":tree=new SSB_MultiTree(initValue,label_id,selectedValue,autoselect);break;
		default:tree=new SSB_Tree(initValue,label_id);break;
	}
	if(field_id)
		tree.setField_id(field_id);
	if(field_name)
		tree.setField_name(field_name);
	if(field_children)
		tree.setField_children(field_children);
	if(field_parentId)
		tree.setField_parentId(field_parentId);
    if(field_enabled)
        tree.setField_enabled(field_enabled);
    if(enabledFlag)
        tree.setEnabledFlag(enabledFlag);
	if(href)
		tree.setHref(href);
	if(target)
		tree.setTarget(target);
	if(service){
		tree.setService(service);
	}
	tree.setDelay(delay);
	if(params){
		var re=/(value|ctrl):([^,]+)/;
		var el=/^\$\{([^\}]+)\}$/;
		tree.params=[];
		var a=[];
		while((a=re.exec(params))){
			params=RegExp.rightContext;
			if(a[1]=='value'){
				var value=el.exec(a[2]);
				if(value)
					tree.params.push(value[1]);
				else 
					tree.params.push(a[2]);
			}else if(a[1]=='ctrl'){
				tree.params.push(document.getElementById(a[2]));
			}
		}
	}
	return tree;
};

// \u5c55\u5f00\u6240\u6709\u8282\u70b9
SSB_Tree.openAllNode = function(treeID)
{
	var spanList = document.getElementById(treeID).getElementsByTagName("SPAN");
	if(!spanList)
	{
		return;
	}
	for(var i = 0;i<spanList.length;i++)
	{
		var node = spanList[i];
		var pNode = node.parentNode;
		if(!pNode.istreeleaf)
		{
			var childNode = pNode.childNodes;
			for(var j = 0;j<childNode.length;j++)
			{
				if(childNode[j].tagName)
				{
					var tagNames = childNode[j].tagName.toUpperCase();
					if(tagNames == "INPUT")
					{
						childNode[j].fireEvent('onclick');
					}
				}
			}
		}
	}
};
function Radios_init(){
	var rtags=document.getElementsByTagName(_RADIOGROUP);
	if(!rtags||rtags.length==0){
		rtags=document.getElementsByTagName(_ZRADIOGROUP);	
	}
	var radios={};
	for(var i=0;i<rtags.length;i++){
		radios=new RadioGroupTag(rtags[i].getAttribute("id"));
		radios.init();
	}
};
addOnloadEvent(Radios_init);
				//
				//radiogroup\u7684\u6807\u7b7e\u7c7b
				//
function RadioGroupTag(id){
	var label=document.getElementById(id);
	var value=label.getAttribute("value");
	var text=label.getAttribute("text");
	var checkedvalue=label.getAttribute("checkedvalue");
	var onclick=label.getAttribute("onclickfun");
	return new SSB_RadioGroup(value,text,checkedvalue,id,onclick);
};

				//
				//\u5355\u9009\u6309\u94ae\u7ec4
				//
function SSB_RadioGroup(value,text,checkedValue,id,onclick){
	function setValueArr(value){
		if(!value)return [];
		if(value instanceof Array){
			this.valueArr=value;
		}else if(value instanceof String||"string"==typeof value){
			this.valueArr=value.split(",");
		}
	};
	function setTextArr(text){
		if(!text)return [];
		if(text instanceof Array){
			this.textArr=text;
		}else if(text instanceof String||"string"==typeof text){
			this.textArr=text.split(",");
		}
	};
	setValueArr.call(this,value);
	setTextArr.call(this,text);
	this.onclick=onclick;
	this.checkedvalue=checkedValue||"";
	this.groupObj=document.getElementById(id);
	this.group_id=id;
};
SSB_RadioGroup.prototype={
	init:function(){
		var html=[];
		var checkedvalue=this.checkedvalue;
		var value={};
		for(var i=0;i<this.valueArr.length||i<this.textArr.length;i++){
			var radio=[];
			value=this.valueArr[i]||"";
			radio.push("<INPUT");
			radio.push("name=radio_"+this.group_id);
			radio.push("type=radio");
			radio.push("value="+value);
			radio.push("onclick="+this.onclick);
			if(checkedvalue==value){
				radio.push("checked=true");
			}
			radio.push(">");
			radio.push(this.textArr[i]||"");
			html.push(radio.join(" "));
		}
		this.groupObj.innerHTML=html.join(" ");
		SSB_RadioGroup.radioGroupObjs[this.group_id]=this;
	},
	getValue:function(){
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].checked){
				return radios[i].getAttribute("value");
			}
		}
	},
	setValue:function(checkedValue){
		if(checkedValue)
			this.checkedvalue=checkedValue;
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].value==checkedValue+""){
				radios[i].checked=true;
				return;
			}
		}
	}
};

SSB_RadioGroup.radioGroupObjs={};

var _RADIOGROUP="RADIOGROUP";
var _ZRADIOGROUP="Z:RADIOGROUP";
Function.prototype.bindAsEventListener = function(object) {
	function $A(arr){
		var a=[];
		for(var i=0;i<arr.length;i++){
			a.push(arr[i]);
		}
		return a;
	};
  	var __method = this;
	var args = $A(arguments), 
	object = args.shift();
  	return function(event) {
    	return __method.apply(object, [event || window.event].concat(args));
  	};
};

function AUTO_init(){
	var atags=document.getElementsByTagName(_ZAUTO_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_AUTO_TAG);	
	}
	var autos={};
	for(var i=0;i<atags.length;i++){
		autos=new AutoCompleteTag(atags[i].getAttribute("id"));
		autos.init();
	}
};

addOnloadEvent(AUTO_init);

function AutoCompleteTag(id){
	var options={};
	var autoLabel=document.getElementById(id);
	options.matchAnywhere=autoLabel.getAttribute("matchAnywhere")||'true';
	options.ignoreCase=autoLabel.getAttribute("ignoreCase")||'false';
	options.queryId=autoLabel.getAttribute("queryId");
	options.count=autoLabel.getAttribute("count")||10;
	var params=autoLabel.getAttribute("params");
	var url=autoLabel.getAttribute("url");
	var ctlId=autoLabel.getAttribute("ctrlId");
	return new SSB_AutoComplete(id,ctlId,url,options,params);
};

	/**
	 * \u6784\u9020\u51fd\u6570
	 * @param {String} anId		\u6807\u7b7eID
	 * @param {String} url		\u8bf7\u6c42\u5730\u5740
	 * @param {Object} options	\u8bbe\u7f6e\u9009\u9879
	 */
function SSB_AutoComplete(labelId,anId,url,options,params){
	this.labelId=labelId;
    this.id          = anId;
    var browser	   = navigator.userAgent.toLowerCase();
    this.isIE        = browser.indexOf("msie") != -1;
    this.isOpera     = browser.indexOf("opera")!= -1;
    this.textInput   = document.getElementById(this.id);
    this.suggestions = [];
    this.setOptions(options);
    this.initAjax(url);
	this.setParams(params);
	this.nonReturn='';
};

SSB_AutoComplete.prototype = {

	/**
	 * 
	 * @param {String} url	\u8bf7\u6c42\u5730\u5740
	 */
	initAjax: function(url) {
		this.url=url;
   },
	/**
	 * \u53c2\u6570\u53ef\u80fd\u6765\u81eaHTML\u9875\u9762\u7684\u5176\u4ed6html\u6807\u7b7e\uff0c\u4e5f\u6709\u53ef\u80fd\u662f\u56fa\u5b9a\u7684\u503c\u3002
	 * \u5982\u679c\u6765\u81ea\u5176\u4ed6\u7684html\u6807\u7b7e\u5148\u4fdd\u5b58\u8fd9\u4e2ahtml\u5bf9\u8c61\uff0c\u5982\u679c\u662f\u5e38\u91cf\u5c31\u4fdd\u5b58\u8fd9\u4e2a\u503c
	 * @param {Object} params
	 */
	setParams:function(params){
		if(!params){//\u5982\u679c\u6ca1\u6709\u8bbe\u7f6e\u53c2\u6570\u5c31\u628a\u5f53\u524dinput\u7684\u503c\u52a0\u8fdb\u6765
			params='ctrl:'+this.id;
		}
		this.params=[];
		var param_re=new RegExp("(ctrl|value):([^,]+)", "i");
		var a=[];
		var tmp=params;
		while((a=param_re.exec(tmp))!=null){
			if(a[1].toUpperCase()=='CTRL'){
				this.params.push(document.getElementById(a[2]));
			}else if(a[1].toUpperCase()=='VALUE'){
				this.params.push(a[2]);
			}
			tmp=RegExp.rightContext;
		}
	},
	/**
	 * \u8bbe\u7f6e\u9009\u9879
	 * @param {Object} options \u8bbe\u7f6e\u9009\u9879
	 */
	setOptions: function(options) {
		function extend(obj1,obj2){
			for(var property in obj1){
				if(obj2[property]){
					obj1[property]=obj2[property];
				}
			}
		};
		this.options = {
	     	//\u81ea\u52a8\u5b8c\u6210\u4e0b\u62c9\u83dc\u5355DIV\u5143\u7d20\u7684css class
			suggestDivClassName	: 'auto_list',
		 	//\u5f53\u9f20\u6807\u79bb\u5f00\u9009\u9879\u65f6\u7684css class
			mouseoutClassName	: 'auto_item_mouseout',
		 	//\u5f53\u9f20\u6807\u79fb\u52a8\u5230\u9009\u9879\u65f6\u7684css class
			mouseoverClassName	: 'auto_item_mouseover',
		 	//\u88ab\u5339\u914d\u7684\u6587\u672c\u7684css class
			matchClassName     	: 'auto_item_match',
		 	//\u662f\u5426\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
			matchAnywhere      	: 'true',
		 	//\u662f\u5426\u533a\u5206\u5927\u5c0f\u5199
			ignoreCase			: 'false',
		 	//\u4e0b\u62c9\u83dc\u5355\u4e2d\u6761\u76ee\u4e2a\u6570
			count              	: 10,
		 	//\u67e5\u8be2\u8bed\u53e5ID
			queryId				: ""
		};

	  extend(this.options,options);
   },
	/**
	 * \u521d\u59cb\u5316\u65b9\u6cd5
	 */
   	init: function() {
      	this.textInput.setAttribute("autocomplete","off");
		
      	var keyEventHandler = new TextSuggestKeyHandler(this);

      	this.createSuggestionsDiv();

	  	SSB_AutoComplete.autoObjs[this.labelId]=this;
   	},
	/**
	 * @return {String} \u9009\u4e2d\u7684\u503c
	 */
	getValue:function(){
		return document.getElementById(this.id).value;
	},

   	handleTextInput: function() {
     	var previousRequest    = this.lastRequestString;
     	this.lastRequestString = this.textInput.value;
     	if ( this.lastRequestString == "" )
        	this.hideSuggestions();
     	else if ( this.lastRequestString != previousRequest ) {
     		if(this.nonReturn!=''&&this.lastRequestString.indexOf(this.nonReturn)!=-1)
     			return;
     		else
        		this.sendRequestForSuggestions();
     	}else if(this.suggestions.length!=0){
     		this.showSuggestions();
     	}
   	},

	moveSelectionUp: function() {
		if ( this.selectedIndex > 0 ) {
			this.updateSelection(this.selectedIndex - 1);
		}
	},

	moveSelectionDown: function() {
		if ( this.selectedIndex < (this.suggestions.length - 1)  ) {
			this.updateSelection(this.selectedIndex + 1);
		}
	},

	updateSelection: function(n) {
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoutClassName;
		}
		this.selectedIndex = n;
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoverClassName;
		}
   	},

	sendRequestForSuggestions: function() {
		if ( this.handlingRequest ) {
			this.pendingRequest = true;
			return;
		}
		this.handlingRequest = true;
		this.callRIAService();
	},

	getParams:function(callParms){
		for(var i=0;i<this.params.length;i++){
			if('string'==typeof this.params[i]){
				callParms.push(this.params[i]);
			}else if('object'==typeof this.params[i]){
				//\u5982\u679c\u8fd9\u4e2aHTML\u5bf9\u8c61\u662f\u5f53\u524d\u81ea\u52a8\u5b8c\u6210input\u5bf9\u8c61
				if(this.params[i]==this.textInput){
					var value=this.lastRequestString;
					//\u5982\u679c\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
					if(this.options.matchAnywhere=='true'){
						value='%'+value+'%';
					}//\u5982\u679c\u5339\u914d\u5f00\u59cb\u4f4d\u7f6e
					else{
						value=value+'%';	
					}
					if(this.options.ignoreCase=='true'){
						value=value.toUpperCase();
					}
					callParms.push(value);	
				//\u5982\u679c\u662f\u5176\u4ed6HTML\u5bf9\u8c61
				}else{//\u8fd9\u91cc\u5e94\u8be5\u7528RIA\u6846\u67b6\u7684processor\u5f97\u5230\u503c
					callParms.push(this.params[i].value);	
				}
			}
		}
	},

   	callRIAService: function() {
    	var callParms = [];
		this.getParams(callParms);
		var param={};//\u53c2\u6570\u5bf9\u8c61
		param.count=10;
		param.ignoreCase=this.options.ignoreCase;
		param.paras=callParms;
		param.queryId=this.options.queryId;

		callMethod(this.url||SSB_AutoComplete.URL,[param],this,this.setValue);
	},
	
	setValue:function(s,data){
		this.suggestions=data;
		this.nonReturn=data.length==0?this.lastRequestString:'';
		this.processSuggestions();
	},
	
	processSuggestions:function(){
		if ( this.suggestions.length==0 ) {
			this.suggestionsDiv.innerHTML='';
         	this.hideSuggestions();
      	}
      	else {
        	this.updateSuggestionsDiv();
         	this.showSuggestions();
         	this.updateSelection(0);
      	}

      	this.handlingRequest = false;

      	if ( this.pendingRequest ) {
         	this.pendingRequest    = false;
         	this.lastRequestString = this.textInput.value;
         	this.sendRequestForSuggestions();
      	}
	},
	
   	setInputFromSelection: function() {
     	var suggestion  = this.suggestions[ this.selectedIndex ];
     	if(!suggestion||this.suggestionsDiv.style.display=='none')return;
	 	this.textInput.value = suggestion;
		this.lastRequestString = suggestion;
     	this.hideSuggestions();
   	},

   	showSuggestions: function() {
      	var divStyle = this.suggestionsDiv.style;
      	if ( divStyle.display == 'block' )
         	return;
      	this.positionSuggestionsDiv();
      	divStyle.display = 'block';
		if(window.navigator.userAgent.indexOf("MSIE") == -1)
			divStyle.overflow='auto';
   	},

	positionSuggestionsDiv: function() {
		var selectedPosX = 0;
        var selectedPosY = 0;
		var divStyle = this.suggestionsDiv.style;
        var theElement = this.textInput;
        if (!theElement) return;
        var theElemHeight = theElement.offsetHeight;
        var theElemWidth = theElement.offsetWidth;
        while(theElement != null){
          selectedPosX += theElement.offsetLeft;
          selectedPosY += theElement.offsetTop;
          theElement = theElement.offsetParent;
        }
		divStyle.width = theElemWidth;
        divStyle.left = selectedPosX;
       	divStyle.top = selectedPosY + theElemHeight;
   },

   	hideSuggestions: function() {
      	this.suggestionsDiv.style.display = 'none';
   	},

   	createSuggestionsDiv: function() {
      	this.suggestionsDiv = document.createElement("div");
      	this.suggestionsDiv.className = this.options.suggestDivClassName;

      	var divStyle = this.suggestionsDiv.style;
      	divStyle.zIndex   = 101;
      	divStyle.display  = "none";

      	this.textInput.parentNode.appendChild(this.suggestionsDiv);
   	},

   	updateSuggestionsDiv: function() {
      	this.suggestionsDiv.innerHTML = "";
      	var suggestLines = this.createSuggestionSpans();
      	for ( var i = 0 ; i < suggestLines.length ; i++ )
         	this.suggestionsDiv.appendChild(suggestLines[i]);
   	},

   	createSuggestionSpans: function() {
      	var regExpFlags = "";
      	if ( this.options.ignoreCase == 'true')
         	regExpFlags = 'i';
     	var startRegExp = "^";
      	if ( this.options.matchAnywhere == 'true')
         	startRegExp = '';

      	var regExp  = new RegExp( startRegExp + this.lastRequestString, regExpFlags );

      	var suggestionSpans = [];
      	for ( var i = 0 ; i < this.suggestions.length ; i++ )
         	suggestionSpans.push( this.createSuggestionSpan( i, regExp ) );

      	return suggestionSpans;
	},

   	createSuggestionSpan: function( n, regExp ) {
      	var suggestion = this.suggestions[n];

      	var suggestionSpan = document.createElement("span");
      	suggestionSpan.className 	 = this.options.mouseoutClassName;
      	suggestionSpan.style.display = 'block';
      	suggestionSpan.id            = this.id + "_" + n;
      	suggestionSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	suggestionSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);

	  	var suggestionText={};

	  	suggestionText=suggestion;

      	var textValues=this.splitTextValues(suggestionText ,
                                             this.lastRequestString.length,
                                             regExp );

      	var textMatchSpan = document.createElement("span");
      	textMatchSpan.id            = this.id + "_match_" + n;
      	textMatchSpan.className     = this.options.matchClassName;
      	textMatchSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	textMatchSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);
      	
      	textMatchSpan.appendChild( document.createTextNode(textValues.mid) );
      	
      	suggestionSpan.appendChild( document.createTextNode( textValues.start ) );
      	suggestionSpan.appendChild( textMatchSpan );
      	suggestionSpan.appendChild( document.createTextNode( textValues.end ) );
      	
      	return suggestionSpan;
   	},
   	
   	mouseoverHandler: function(e) {
		e=e||window.event;
   	   	var src = e.srcElement||e.target;
   	   	var index = parseInt(src.id.substring(src.id.lastIndexOf('_')+1));
   	   	this.updateSelection(index);
   	},
   	
   	itemClickHandler: function(e) {
   	   	this.mouseoverHandler(e);
   	   	if ( this.suggestionsDiv.style.display == 'block' )
   	   	   	this.setInputFromSelection();
   	   	this.hideSuggestions();
   	   	this.textInput.focus();
   	},
   	
   	splitTextValues: function( text, len, regExp ) {
   	   	var startPos  = text.search(regExp);
   	   	var matchText = text.substring( startPos, startPos + len );
   	   	var startText = startPos == 0 ? "" : text.substring(0, startPos);
   	   	var endText   = text.substring( startPos + len );
   	   	return { start: startText, mid: matchText, end: endText };
   	},
   	
   	getElementContent: function(element) {
   	   	return element.firstChild.data;
   	}
};

SSB_AutoComplete.autoObjs={};

function TextSuggestKeyHandler(autoObj){
   	this.autoObj = autoObj;
   	this.input   = this.autoObj.textInput;
   	this.addKeyHandling();
};
SSB_AutoComplete.getKeyCode=function(e){
	e=e||window.event;
	return e.keyCode||e.which;
};
TextSuggestKeyHandler.prototype = {

   	addKeyHandling: function() {
   	   	this.input.onkeydown = this.preventDefault.bindAsEventListener(this);
   	   	this.input.onkeyup   = this.keyupHandler.bindAsEventListener(this);
   	   	this.input.onblur    = this.onblurHandler.bindAsEventListener(this);
   	   	this.input.onfocusout = this.onblurHandler.bindAsEventListener(this);
   	   	if ( this.isOpera )
   	   	   	this.input.onkeypress = this.keyupHandler.bindAsEventListener(this);
   	},
	preventDefault:function(e){
		var enterKey=13;
		var escKey=27;
		var key=SSB_AutoComplete.getKeyCode(e);
		if(key==enterKey||key==escKey){
			if(e.preventDefault){
				e.preventDefault();
			}else{
				e.returnValue=false;
			}
		}
	},
   	keyupHandler: function(e) {
   	   	var upArrow   = 38;
   	   	var downArrow = 40;
   	   	var enterKey  = 13;
   	   	var escKey	  = 27;
   	   	if ( this.input.value.length == 0 && !this.isOpera ){
   	      	this.autoObj.hideSuggestions();
   	      	return;
   	   	}
   	   	switch(SSB_AutoComplete.getKeyCode(e)){
	   	   	case upArrow:	this.autoObj.moveSelectionUp();
	   	   	   			 	setTimeout( function(){}, 1 );
	   	   				 	break;
	   	   	case downArrow:	this.autoObj.moveSelectionDown();
	   	   					break;
	   	   	case enterKey:	if(this.autoObj.suggestionsDiv.style.display=='none')
	   	   						this.autoObj.handleTextInput();
	   	   					else
	   	   						this.autoObj.setInputFromSelection();
	   	   					break;
	   	   	case escKey:	this.autoObj.hideSuggestions();
	   	   					break;
	   	   	default:this.autoObj.handleTextInput();
	   	}
   	},

   	onblurHandler: function(oEvent) {
   	   	if(this.autoObj.suggestionsDiv.style.display == 'none')
   	   	   	return;
   	   	var event=window.event||oEvent;
		var toElement=event.explicitOriginalTarget||event.toElement;
		if(!toElement) return;

		var autolist=this.autoObj.suggestionsDiv;
		do{
			if(toElement==autolist)
				return;
			toElement=toElement.parentNode;
		}while(toElement);
		autolist.style.display="none";
   	}
};

SSB_AutoComplete.URL="autocomplete/getInitValueArr.ssm";

var _ZAUTO_TAG='Z:AUTOCOMPLETE';
var _AUTO_TAG='AUTOCOMPLETE';
function NAVI_init(){
	var atags=document.getElementsByTagName(_ZNAVI_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_NAVI_TAG);	
	}
	var navis={};
	for(var i=0;i<atags.length;i++){
		navis=new NaviTag(atags[i].getAttribute("id"));
		navis.init();
	}
};

addOnloadEvent(NAVI_init);


function NaviTag(id){
	var labelObj=document.getElementById(id);
	var menuId=labelObj.getAttribute("menuId");
	var text=labelObj.getAttribute("text");
	var url=labelObj.getAttribute("url");
	var _ria_location = labelObj.getAttribute("caption");
	if(_ria_location==null||_ria_location=='null'||_ria_location==undefined||_ria_location=='undefined'){_ria_location="\u5f53\u524d\u4f4d\u7f6e\uff1a";}
	var re = /caption_text/g; 
	SSB_Navi.STARTHTML=SSB_Navi.STARTHTML.replace(re,_ria_location);
	return new SSB_Navi(menuId,text,url,id);
};

function SSB_Navi(menuId,text,url,labelId){
	this.menuId=menuId;
	this.setText(text);
	this.url=url;
	this.setObject(labelId);
	this.menuPath=[];
};
SSB_Navi.prototype={
	setText:function(text){
		if(text instanceof Array){
			this.text=text;
		}else if(text instanceof String ||'string'==typeof text){
			this.text=text.split(',');
		}else 
			this.text=[];
	},
	setObject:function(id){
		this.labelObj=document.getElementById(id);
		this.labelId=id;
	},
	init:function(){
		SSB_Navi.naviObjs[this.labelId]=this;
		this.sendAjaxRequest();
	},
	sendAjaxRequest:function(){
		callMethod(this.url||SSB_Navi.URL,[this.menuId],this,this.setValue);
	},
	setValue:function(s,pathStr){
		this.handleResponse(pathStr);
		this.concatPath();
		this.getMenuPath();
	},
	handleResponse:function(pathStr){
		if(null!=pathStr&&""!=pathStr){
			this.menuPath=pathStr.split(',');
		}
	},
	concatPath:function(){
		this.menuPath=this.menuPath.concat(this.text);
	},
	getMenuPath:function(){
		var html=[];
		html.push(SSB_Navi.STARTHTML);
		var length=this.menuPath.length-1;
		if(length>=0){
			var i=0;
			for(i=0;i<length;i++){
				html.push(this.menuPath[i]);
				html.push(SSB_Navi.SPANHTML);
			}
			html.push(this.menuPath[i]);
		}
		html.push(SSB_Navi.ENDHTML);
		this.displayPath(html);
	},
	displayPath:function(html){
		this.labelObj.innerHTML=html.join("");
	}
};

SSB_Navi.naviObjs={};

SSB_Navi.URL="naviService/getPathString.ssm";

SSB_Navi.STARTHTML="<TABLE width=100%><TBODY><TR><TD noWrap width=56%><DIV class=div_subtitle>caption_text";
SSB_Navi.SPANHTML="<SPAN class=arrow_subtitle>&gt;</SPAN>";
SSB_Navi.ENDHTML="</DIV></TD><TD vAlign=bottom noWrap align=right width=44%>&nbsp;</TD></TR></TBODY></TABLE>";

var _NAVI_TAG="NAVI";
var _ZNAVI_TAG="Z:NAVI";
 /********************************
  * \u83b7\u53d6\u8868\u5bf9\u8c61
  * @param {Object} tib  \u8868ID
  * @author zhanghongwei
  ********************************/
  function getTableById(tib)
 {
 	var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868	
 	var Mtable = {};//\u5bf9\u8c61\u6a21\u578b
 	Mtable.id = tib;
 	//\u5c5e\u6027
 	Mtable.table = table;
 	Mtable.rowcount  = table.rows.length; //\u8868\u683c\u603b\u884c\u6570	
 	Mtable.pagesize  = tableModel_arr[tib].pg.perPageCount; //\u6bcf\u9875\u8bb0\u5f55\u6570
 	Mtable.pageIndex = tableModel_arr[tib].pg.page;//\u5f53\u524d\u7b2c\u51e0\u9875
 	Mtable.columncount = tableModel_arr[tib].cells.length;//\u603b\u5217\u6570
 	Mtable.pagecount = tableModel_arr[tib].pg.pageCount;//\u603b\u9875\u6570
 	Mtable.allcount  = tableModel_arr[tib].pg.totalrow;//\u603b\u7684\u8bb0\u5f55\u6570
    Mtable.condition = tableModel_arr[tib].condition;//\u5f53\u524d\u67e5\u8be2\u6761\u4ef6
    Mtable.data      = tableModel_arr[tib].pageData;//\u8868\u683c\u6570\u636e
 	//\u65b9\u6cd5
 	Mtable.getCell   = function(x,y){return table.rows[x].cells[y];};//\u83b7\u53d6\u6307\u5b9a\u5750\u6807\u7684\u5355\u5143\u4e2a\u5bf9\u8c61
 	Mtable.getRow    = function(m){return table.rows[m];};//\u83b7\u53d6\u6307\u5b9a\u5e8f\u5217\u7684\u884c\u5bf9\u8c61
 	Mtable.insertRow = function(){addOneRow(tib);};//\u5728\u8868\u683c\u672b\u5c3e\u65b0\u589e\u52a0\u4e00\u884c
 	/*1.\u65e0\u53c2\u6570\u65f6\u5220\u9664\u8868\u683c\u672b\u5c3e\u884c
 	  2.\u6709\u53c2\u6570\u65f6 n \u4ee3\u8868\u884c\u5e8f\u53f7*/
 	Mtable.deleteRow = function(n)
 	{
 		if(n == null || n == '')
 		{
 			n = parseInt(table.rows.length - 1);
 		}
 		var row = table.rows[n];
 		deleteOneRow(tib, row);
 	};
 	//\u53d6\u5f97\u8868\u683c\u5217\u5bf9\u8c61
 	Mtable.getColumn = function(m)
 	{
 		var tb_arr = new Array();
 		var rows = table.rows;
 		for(var i = 0; i < rows.length; i++)
 		{
 			var row = rows[i];
 			tb_arr.push(row.cells[m]);
 		}
 		return tb_arr;
 	};
 	//\u53d6\u5f97\u8868\u683c\u884c\u503c\u5bf9\u8c61\u6570\u7ec4
 	Mtable.getRowValues    = function(){return getTable_objTrs(tib);};
 	//\u53d6\u5f97\u8868\u683c\u5217\u503c\u5bf9\u8c61\u6570\u7ec4
 	Mtable.getColumnValues = function(index){return getTable_Tds(tib, index);};
 	//\u53d6\u5f97\u8868\u683c\u884c\u3001\u5217\u4e8c\u7ef4\u503c\u6570\u7ec4
 	Mtable.getAllValues    = function(){return getTable_Trs(tib);};
 	//\u53d6\u5f97\u67d0\u4e00\u5217\u9009\u4e2d\u7684\u503c\u6570\u7ec4\uff08\u8be5\u5217\u4e3aradio\u6216checkbox\uff09
 	Mtable.getCheckedValues = function(index){return getTable_Td_CheckedValue(tib, index);};
 	//\u91cd\u7f6e\u8868\u683c \u6e05\u7a7a\u6240\u6709\u5355\u5143\u683c\u7684\u503c
 	Mtable.reset           = function()
 	{
 		var initrow = parseInt(table.rows.length - 1);
 		for(var i = initrow; i > 0; i --)
   		{ 
   			table.deleteRow(parseInt(i));
   		}
 		for(var i = 0; i < initrow; i ++)
   		{ 
   			create_new_EditTableRow(tableModel_arr[tib].tableId , tib);
   		}
 	};
 	Mtable.refresh         = function()//\u5237\u65b0\u8868\u683c
 	{
 		var condition = tableModel_arr[tib].condition;
 		if(condition == null)
 		{
 			return ;
 		}
 		 if(tableModel_arr[tib].turnpage == "true" )
	     {
	       var pageNum = this.pageIndex;
	       var perPageCount = this.pagesize;
	       getTurnPageData(tib, condition,pageNum,perPageCount); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
	     }
	     else if(tableModel_arr[tib].editable == "true")
	 	 {
		   getEditPageData(tib, condition); //\u53ef\u7f16\u8f91\u8868\u683c
     	 }	     
 	};
 	//\u83b7\u53d6\u8868\u683c\u4e2d\u9009\u62e9\u884c\u5bf9\u8c61\u503c\u6570\u7ec4
 	Mtable.getCheckedRowValues = function(){return getCheckedRowsValue(tib);};
 	
 	getColumns  = function()
 	{
 		var cells = new Array();
 		for(var i = 0; i < Mtable.columncount; i ++)
 		{
 			cells.push(Mtable.getColumn(i));
 		}
 		return cells;
 	};
 	
 	//\u8868\u683c\u4e2d\u6587\u4ef6\u4e0a\u4f20
 	Mtable.upload = function(o,sid,field,anotherField){uploadFile(o,sid,field,anotherField)};
 	//\u96c6\u5408
 	Mtable.rows      = table.rows;//\u5305\u542b\u884c\u5bf9\u8c61\u7684\u6570\u7ec4
 	Mtable.columns   = eval("getColumns()");//\u5305\u542b\u5217\u5bf9\u8c61\u7684\u6570\u7ec4
 	
    return Mtable;
 };
 
 /********************************
  * \u83b7\u53d6\u5217\u6570\u7ec4
  * @param {Object} tib  \u8868ID
  * @param {Object} td_index
  * @author zhanghongwei
  *******************************/
  getTable_Tds = function(tib, td_index)
 {
  
  var td_value_arr = new Array();
  var table_rows = table.rows;
  var table_cells =  table_rows[1].cells;

  //\u53d6\u6821\u9a8c\u51fd\u6570
	var validate = tableModel_arr[tib].cells[td_index-1].validate;
	var datatype = tableModel_arr[tib].cells[td_index-1].datatype;
    var formate = tableModel_arr[tib].date_formate;

  for(var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
  {
  	var obj = table_rows[i].cells[td_index-1];
	var obj_select = obj.getElementsByTagName("SELECT");
	var obj_input = obj.getElementsByTagName("INPUT");	
	
	//\u5faa\u73af\u53d6\u503c
	try{
		for(var m = 0; m < obj_select.length; m ++)
		{
			td_value_arr.push(obj_select[m].value);
		}
		//
		for(var n = 0; n < obj_input.length; n++)
		{
		    var rtn = true;
		    var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
			if(validate != null && validate != "")
			{
				rtn = eval(validate+"('"+obj_input[n].value+"')");
			}
			else if(datatype != null && datatype != "" && obj_input[n].type == "text")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
			{
				rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
				if(datatype.toLowerCase() == 'date')
				{
					value = getUTCfromTime(formate, value);
				}
			}  
		    if(rtn == false)
			{				
				obj_input[n].focus();
				throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");  
			}	
			td_value_arr.push(value);
		}
	}catch(ee){
	    log.info("ErrorMessage is:" + ee.message+"--description:"+ee.description);
	   return false;
	}
	
  }
  
   return td_value_arr;
 }; 

  /********************************
  * \u6839\u636e\u8868\u4e2d\u7684checkbox,radio
  * \u7684checked\u83b7\u53d6\u9009\u4e2d\u7684\u884c\u5bf9\u8c61\u503c\u7684\u6570\u7ec4
  * @param {Object} tib  \u8868ID
  * @param {Object} td_index
  * @author zhanghongwei
  *******************************/
 getCheckedRowsValue = function(tib)
 {
 	
 	for(var i = 0; i < tableModel_arr[tib].cells.length; i++)
 	{
 		var iskey = tableModel_arr[tib].cells[i].getAttribute("iskey");
 		if(iskey == "true")
 		{ 			
 			return getCheckedValues(tib, i);
 			break;
 		}
 	}
 };
 /********************************
  * \u6839\u636e\u67d0\u5217\u4e2d\u7684checkbox,radio
  * \u7684checked\u83b7\u53d6\u9009\u4e2d\u7684\u884c\u5bf9\u8c61\u503c\u7684\u6570\u7ec4
  * @param {Object} tib  \u8868ID
  * @param {Object} td_index
  * @author zhanghongwei
  *******************************/
 getCheckedValues = function (tib, td_index)
 {

  var table = tableModel_arr[tib].table;
   //\u83b7\u5f97\u5217\u5c5e\u6027\u6570\u7ec4
  var checkedrows = new Array();  
  var table_rows = table.rows;
    
  for(var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
  {
  	var obj = table_rows[i].cells[td_index];
  	var obj_input = obj.getElementsByTagName("INPUT");
	
	if(obj_input.length < 1) 
	  alert("\u7b2c"+parseInt(td_index+1)+"\u5217\u6ca1\u6709\u53ef\u9009\u9879!");

	//\u5faa\u73af\u53d6\u503c
	for(var n = 0; n < obj_input.length; n++)
	{
		if(obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
		{
		    if(obj_input[n].checked == true)
		    {
		        var oo = getTable_objTr(tib, i);
		    	checkedrows.push(oo);
		    	break;
		    }		
		}
		
	}
	
  }

   return checkedrows;
 }; 

 /********************************
  * \u83b7\u53d6\u5217\u7684\u9009\u62e9\u9879
  * @param {Object} tib  \u8868ID
  * @param {Object} td_index
  * @author zhanghongwei
  *******************************/
 getTable_Td_CheckedValue = function (tib, td_index)
 {
  var table = tableModel_arr[tib].table;
  var td_checkedvalue = new Array();
  var table_rows = table.rows;
  var table_cells =  table_rows[1].cells;

  for(var i = 1; i < table_rows.length; i++)//\u884c\u5faa\u73af
  {
  	var obj = table_rows[i].cells[td_index-1];
  	var obj_input = obj.getElementsByTagName("INPUT");
	
	if(obj_input.length < 1) 
	  alert("\u7b2c"+td_index+"\u5217\u6ca1\u6709\u53ef\u9009\u9879!");

	//\u5faa\u73af\u53d6\u503c
	for(var n = 0; n < obj_input.length; n++)
	{
		if(obj_input[n].type == "checkbox" || obj_input[n].type == "radio")
		{
		    if(obj_input[n].checked == true)
		    {
		       // alert(obj_input[n].value);
		    	td_checkedvalue.push(obj_input[n].value);
		    }		
		}		
	}	
  }
   return td_checkedvalue;
 }; 
 
/*********************************
 * \u83b7\u53d6\u8868\u683c\u5355\u884c\u6570\u636e
 * @param {Object} tib \u8868\u683cID
 * @param {Object} tr_index \u884c\u5e8f\u53f7
 * @author zhanghongwei
 *********************************/
 getTable_Tr = function(tib, tr_index)
{
	  var tr_value = new Array();
	  var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868	
	  var props = tableModel_arr[tib].props;
	  var formate = tableModel_arr[tib].date_formate;
	  var table_row = table.rows[tr_index];  
	  var table_cells =  table_row.cells;

  try{
	  for(var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
	  {
	  	var obj = table_row.cells[i];
	    var obj_select = obj.getElementsByTagName("SELECT");
	    var obj_input = obj.getElementsByTagName("INPUT");
		//\u53d6\u6821\u9a8c\u51fd\u6570
		var validate = tableModel_arr[tib].cells[i].validate;
		var datatype = tableModel_arr[tib].cells[i].datatype;

		//\u5faa\u73af\u53d6\u503c
		for(var m = 0; m < obj_select.length; m ++)
		{
		    var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
			if(validate != null && validate != "")
			{
				rtn = eval(validate+"('"+obj_select[m].value+"')");
			}
			else if(datatype != null && datatype != "")//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
			{
				//\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
			}
			if(rtn == false)
			{			
				obj_select[m].focus();
				throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");   
			}	
			
			tr_value.push(obj_select[m].value);
		}
		//
		for(var n = 0; n < obj_input.length; n++)
		{
			var rtn = true;
			var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
			if(validate != null && validate != "")
			{
				rtn = eval(validate+"('"+value+"')");
			}
			else if(datatype != null && datatype != "" && (obj_input[n].type == "text"||obj_input[n].type == "hidden"))//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
			{
				if(obj_input[n].isDate=="false") continue;
				rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
				if(datatype.toLowerCase().indexOf("date") != -1 && obj_input[n].isDate=="true")
				{
					value = getUTCfromTime(formate, value);
				}
			}  
		    if(rtn == false)
			{				
				obj_input[n].focus();
				throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");  
			}	
			tr_value.push(value);
		}
		
		
	  }
    }catch(ee){
	  //alert("ErrorMessage is:" + ee.message);
	  log.info("ErrorMessage is:" + ee.message+"--description:"+ee.description);
	   return false;
	}
    return tr_value;
};

/*********************************
 * \u83b7\u53d6\u8868\u683c\u884c\u5bf9\u8c61\u6570\u636e
 * @param {Object} tib \u8868\u683cID
 * @param {Object} tr_index \u884c\u5e8f\u53f7
 * @author zhanghongwei
 *********************************/
 getTable_objTr = function(tib, tr_index, isCheck)
{
	  var tr_value = new Object;
	  var table = tableModel_arr[tib].table;//\u83b7\u53d6\u5185\u5b58\u8868	
	  var props = tableModel_arr[tib].props;
	  var formate = tableModel_arr[tib].date_formate;
	  var table_row = table.rows[tr_index];  
	  var table_cells =  table_row.cells;

  try{
	  for(var i = 0; i < table_cells.length; i++)//\u884c\u5faa\u73af
	  {
	    var prop = props[i];
	  	var obj = table_row.cells[i];
	    var obj_select = obj.getElementsByTagName("SELECT");
	    var obj_input = obj.getElementsByTagName("INPUT");
		var validate = tableModel_arr[tib].cells[i].validate;
		var datatype = tableModel_arr[tib].cells[i].datatype;

		//\u5faa\u73af\u53d6\u503c
		for(var m = 0; m < obj_select.length; m ++)
		{
		    var rtn = true;
		   	//\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
			if(validate != null && validate != "" && isCheck != false)
			{
				rtn = eval(validate+"('"+obj_select[m].value+"')");
			}
			else if(datatype != null && datatype != "" && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
			{
				//\u4e0b\u62c9\u5217\u8868\u6846\u7684\u503c\u8981\u6821\u9a8c\u5417
			}
			if(rtn == false)
			{			
				obj_select[m].focus();
				throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");   
			}	
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
			if(obj_select[m].getAttribute("prop") != null && obj_select[m].getAttribute("prop") != "")
			{
				prop = obj_select[m].getAttribute("prop");
			}
			
			//\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
			if(prop != null && prop != "")
			{
				tr_value[prop] = obj_select[m].value;
			}
		}
		//
		for(var n = 0; n < obj_input.length; n++)
		{
			var rtn = true;
			var value = obj_input[n].value;
		    //\u5982\u679c\u6709\u7528\u6237\u81ea\u5b9a\u4e49\u6821\u9a8c\u51fd\u6570,\u5219\u4f18\u5148\u6267\u884c\u81ea\u5b9a\u4e49\u51fd\u6570
			if(validate != null && validate != "" && isCheck != false)
			{
				rtn = eval(validate+"('"+value+"')");
			}
			else if(datatype != null && datatype != "" && (obj_input[n].type == "text"||obj_input[n].type == "hidden") && isCheck != false)//\u5426\u5219\u5982\u679c\u5b58\u5728datatype
			{
				if(obj_input[n].isDate=="false") continue;
				rtn = verifyInput(obj_input[n], datatype);
				//\u5982\u679c\u662f\u65e5\u671f\u7c7b\u578b
				if(datatype.toLowerCase().indexOf("date") != -1 && obj_input[n].isDate=="true")
				{
					value = getUTCfromTime(formate, value);
				}
			}  
		    if(rtn == false)
			{				
				obj_input[n].focus();
				throw new String("\u6570\u636e\u6821\u9a8c\u5931\u8d25!");  
			}	
			
			//\u5982\u679c\u5b50\u63a7\u4ef6\u6709prop\u5c5e\u6027\uff0c\u4ee5\u5b50\u63a7\u4ef6\u4e3a\u4e3b
			if(obj_input[n].getAttribute("prop") != null && obj_input[n].getAttribute("prop") != "")
			{
				var ctr_prop = obj_input[n].getAttribute("prop");
				tr_value[ctr_prop] = value;
			}
			//\u5982\u679cprop\u5c5e\u6027\u4e0d\u4e3a\u7a7a
			else if(prop != null && prop != "")
			{  
				if(obj_input[n].type == 'radio' || obj_input[n].type == 'checkbox')
				{
					if(obj_input[n].checked == true) tr_value[prop] = value;
				}
				else{		
				    tr_value[prop] = value;
				}
			}	
		}
		
	  }
    }catch(ee){
	  //alert("ErrorMessage is:" + ee.message);
	  log.info("ErrorMessage is:" + ee.message+"--description:"+ee.description);
	   return false;
	}
    return tr_value;
};
/****************************
 * \u83b7\u53d6\u8868\u683c\u6240\u6709\u884c\u6570\u636e \u4ee5\u6570\u7ec4
 * \u5f62\u5f0f\u8fd4\u56de
 * @param {Object} tib
 * @author zhanghongwei
 ****************************/
 getTable_Trs = function(tib)
{
	 var trs_arr = new Array();//\u5217\u6570\u7ec4
     var table = tableModel_arr[tib].table;       
     var table_rows = table.rows;
	 
	 for(var i = 1; i < table_rows.length; i ++)
	 {
	 	var tr_value = getTable_Tr(tib, i);
	 	if(tr_value == false)
	 	{ 
	 	  return false;
	 	  break ;
	 	}	 	
		trs_arr.push(tr_value);
	 }

	 return trs_arr;
};
/****************************
 * \u83b7\u53d6\u8868\u683c\u6240\u6709\u884c\u6570\u636e\u5bf9\u8c61\u4ee5\u6570\u7ec4
 * \u5f62\u5f0f\u8fd4\u56de
 * @param {Object} tib
 * @author zhanghongwei
 ****************************/
 getTable_objTrs = function(tib, isCheck)
{
	 var trs_arr = new Array();//\u5217\u6570\u7ec4
     var table = tableModel_arr[tib].table;       
     var table_rows = table.rows;
	 
	 for(var i = 1; i < table_rows.length; i ++)
	 {
	 	var tr_value = getTable_objTr(tib, i, isCheck);
	 	if(tr_value == false)
	 	{ 
	 	  return false;
	 	  break ;
	 	}	 	
		trs_arr.push(tr_value);
	 }

	 return trs_arr;
};
/***********************************************
 * \u5206\u9875\u8868\u683c\u8c03\u7528RIA\u6846\u67b6\u670d\u52a1,\u83b7\u53d6\u6570\u636e\u56de\u4f20\u7ed9 \u8868\u683c\u5904\u7406
 * @param {Object} sid  \u670d\u52a1ID
 * @param {Object} condition  \u67e5\u8be2\u6761\u4ef6
 * @param {Object} pageNum  \u9875\u7801
 * @param {Object} perPageCount \u6bcf\u9875\u663e\u793a\u591a\u5c11\u884c
 * @author zhanghongwei

  *@ getTurnPageData('zteTable','$M.obj1', '3', '10');
  *@ \u8bb0\u5fc6\u67e5\u8be2\u529f\u80fd\u5b9e\u73b0\u8c03\u7528\u7684\u65b9\u6cd5 
  *@ 'zteTable' \u81ea\u5b9a\u4e49\u8868ID
  *@ '$M.obj1' \u67e5\u8be2\u6761\u4ef6
  *@ '3' \u8fd4\u56de\u7684\u9875\u7801
  *@ '10' \u663e\u793a\u7684\u884c\u6570
  ***********************************************/
 function getTurnPageData(tid, condition, pageNum, perPageCount,preCondition)
{
	  //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
	  tableModel_arr[tid].condition = condition;
      tableModel_arr[tid].pg.turnTo = pageNum; 
      tableModel_arr[tid].pg.perPageCount = perPageCount;
      tableModel_arr[tid].table.sortCol = "";
	//\u67e5\u8be2\u6761\u4ef6
	 var oCondition = condition;	 
 	 //\u8d77\u59cb\u884c
	 var rangeStart = parseInt((pageNum - 1) * perPageCount);
	 //\u6570\u636e\u957f\u5ea6
	 var fetchSize = perPageCount;	 
	 var oCondition = condition.toJSONString();
     var json = oCondition.substring(0,oCondition.lastIndexOf("]"))+","+rangeStart+","+fetchSize+"]";
	//RIA\u670d\u52a1id
	 var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
	 callTSid(sid, preCondition,tid,json);
};

/********************************
 * \u53ef\u7f16\u8f91\u8868\u683c\u7684\u8c03\u7528RIA\u7684\u65b9\u6cd5
 * @param {Object} tid
 * @param {Object} condition
 * @author zhanghongwei
 *******************************/
 getEditPageData = function(tid, condition)
{
	 //\u5c06\u67e5\u8be2\u6761\u4ef6\u653e\u5165\u8868\u6a21\u578b
	tableModel_arr[tid].condition = condition;
	//RIA\u670d\u52a1id
	 var sid = tableModel_arr[tid].sid;
	 //\u8c03\u7528RIA
	 callSid(sid, condition ,tid);
};
 
/********************************
  *\u8c03\u7528\u8c03\u7528\u540e\u53f0\u670d\u52a1
  *pageinfo \u8868\u4fe1\u606f
  *tid \u6a21\u578b\u8868ID
  *sid \u670d\u52a1ID
  *condition \u67e5\u8be2\u6761\u4ef6
  * @author zhanghongwei
 ********************************/
  function searchTableData(sid, condition, tid)
  {
  	var perPage = 0;	
	tableModel_arr[tid].sid = sid;
    if(tableModel_arr[tid].turnpage == "true" || tableModel_arr[tid].editable == "false")
     {
       perPage = tableModel_arr[tid].pg.perPageCount;// \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
       var bindObject = getBindObject(condition);
	    getTurnPageData(tid, bindObject,'1',perPage, condition); //pg.perPageCount \u6bcf\u9875\u591a\u5c11\u884c
     }
     else if(tableModel_arr[tid].turnpage == "false" || tableModel_arr[tid].editable == "true")
	 {
     	perPage = tableModel_arr[tid].initrow;
		 getEditPageData(tid, condition); //\u53ef\u7f16\u8f91\u8868\u683c
     }     
     else if(tableModel_arr[tid].turnpage == tableModel_arr[tid].editable)
     {
     	alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u4e0d\u80fd\u76f8\u540c!");
     }
     else
     {
     	alert("\u8868\u683c\u63a7\u4ef6turnpage, editable \u5c5e\u6027\u914d\u7f6e\u6709\u8bef!");
     }
  };
  
/********************************
  *\u8c03\u7528\u8868\u683c\u5904\u7406\u5668
  *mid \u6a21\u578bID;
  *pageinfo \u8868\u4fe1\u606f
  * @author zhanghongwei
 ********************************/
 
 setTableValue = function(mid,pageinfo)
{
	//\u91cd\u7f6e\u8868\u5934\u590d\u9009\u6846
	var title_checkbox = document.getElementById(mid+"allCheck");
	if(title_checkbox) title_checkbox.checked =false;
	
   if(tableModel_arr[mid].turnpage == "true")
   {
   	 tableModel_arr[mid].pg.totalrow = pageinfo.totalCount;  //\u8bbe\u7f6e\u8bb0\u5f55\u603b\u6761\u6570
     tableModel_arr[mid].pg.printHtml(); // \u521d\u59cb\u5316\u5206\u9875\u5de5\u5177\u680f
      tableModel_arr[mid].pageData = pageinfo.data;
     pushData2ssbTable(mid, pageinfo.data);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
   }
   if(tableModel_arr[mid].editable == "true")
   {
   	  tableModel_arr[mid].pageData = pageinfo;
   	  pushData2ssbTable(mid, pageinfo);//\u663e\u793a\u8868\u683c\u5e76\u4e14\u586b\u5165\u6570\u636e
   }
};
/*********************************
 * checkbox \u5168\u9009
 * @author zhanghongwei
 *********************************/
var checkedAll = function(td, tid)
{
	var id = '';
	if(typeof tid == 'object')
	{
		id = tid.id;
	}
    var input = td;
	while(td.tagName != "TD")
	{
		td = td.parentNode;
	}
	var tdindex = td.cellIndex;
    var table = getTableById(id);
    var columns = table.getColumn(tdindex);
    for(var i = 1; i < columns.length; i++)
    {
    	if(isIE())
    	{
    		var inputs = columns[i].children;
    	}else{
    		var inputs = columns[i].childNodes;
    	}
    	
    	for(var j=0; j<inputs.length; j++)
    	{
    		if(inputs[j].type.toLowerCase() == "checkbox")
    		{
    			inputs[j].checked = input.checked;    			
    		} 
    	}    
    }
	
};

/********************************
  *\u521b\u5efa\u8868\u7684\u6a21\u578b\u5bf9\u8c61
  *\u521d\u59cb\u5316\u6a21\u578b\u5bf9\u8c61
  * @author \u5f20\u5b8f\u4f1f
  * @param tb \u8868\u683c\u8868\u683c\u6807\u7b7eID
 ********************************/
var tableModel_arr = new Array();//\u6a21\u578b\u6570\u7ec4
var ROW_CLASS_EVEN = "even";
var RIA_SCOPENAME  = "Z";
var E_RESIZE       = "e-resize";
var DISPLAYHEAD    = "isDisplayHead";
var RIA_COLUMN     = "z:COLUMN";
var IS_EXCEL       = "isexcel";
var IS_EXCEL_CONFIG= "isexcelconfig";
var IS_EXCEL_PROP  = "isexcelprop";
var VSTATCAPTION   = "vstatcaption";
var VSTATFORMULA   = "vstatformula"; 
var init_ssb_tableModels = function(tb)
{
    // \u83b7\u53d6\u8868\u6a21\u578b\u5bf9\u8c61
	var Mobject = document.getElementById(tb);
	//\u5c06\u8868\u6a21\u578b\u5b58\u5165\u6a21\u578b\u6570\u7ec4
	tableModel_arr[tb] = Mobject;
	tableModel_arr[tb].isIE = isIE();
	var style = null;
	//\u83b7\u53d6className\u5c5e\u6027\u548cstyle\u5c5e\u6027
	if(isIE())
	{
		tableModel_arr[tb].MclassName = Mobject.className;
		style = Mobject.getAttribute("style").cssText;
	}	
    else{
    	tableModel_arr[tb].MclassName = Mobject.getAttribute("class");
    	style = Mobject.getAttribute("style");	
    }
    if(Mobject.style.display == 'none')
    {
	    var Tstyle = style.replace('none','DISPLAY');
	    tableModel_arr[tb].Mstyle = Tstyle;
    }else{
    	Mobject.style.display="none";//\u9690\u85cf\u6807\u7b7e
    	tableModel_arr[tb].Mstyle = style;
    }	
	tableModel_arr[tb].isExcel = Mobject.getAttribute(IS_EXCEL);
	tableModel_arr[tb].isExcelConfig = Mobject.getAttribute(IS_EXCEL_CONFIG);
	var pg = new showPages('pg');//\u5206\u9875
	pg.tid = tb;
    tableModel_arr[tb].pg = pg;
 	var props = new Array();//\u5217\u5c5e\u6027\u7684\u6570\u7ec4\u5bf9\u8c61
    var caption = new Array();//\u8868\u683c\u7684\u5217\u540d
 	//\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u6a21\u578b	
	var col = new Array();
	var clum = new Array();
	var excel_title = new Array();
	var excel_props = new Array();
	if(isIE())
	{
		clum = Mobject.children;// IE\u83b7\u53d6\u8868\u6a21\u578b\u7684\u5217\u6570\u7ec4		
	}
	else{
		clum = Mobject.getElementsByTagName(RIA_COLUMN);//\u706b\u72d0
	}
    for(var i = 0; i < clum.length; i ++)
	{
		if(clum[i].getAttribute("visible") != 'false')
		{
			col.push(clum[i]);
		}
		if(clum[i].getAttribute(IS_EXCEL_PROP) != 'false')
		{
			excel_title.push(clum[i].getAttribute("caption"));
		    excel_props.push(clum[i].getAttribute("prop"));
		}		
	}
	//\u521b\u5efa\u6a21\u578b\u8868\u7684\u5217\u5bf9\u8c61\u96c6\u5408
	tableModel_arr[tb].cells = col;
	tableModel_arr[tb].clums = clum;
	tableModel_arr[tb].excel_title = excel_title;
	tableModel_arr[tb].excel_props = excel_props;
	//\u521b\u5efa\u6a21\u578b\u8868\u5217\u7684\u5b50\u7ed3\u70b9\u6a21\u578b
	for(var j = 0; j < tableModel_arr[tb].cells.length; j++ )
	{
	    //\u83b7\u53d6\u5217\u6570\u503c\u6821\u9a8c\u51fd\u6570
	    var checkfunction = tableModel_arr[tb].cells[j].getAttribute("validate");
	    tableModel_arr[tb].cells[j].validate = checkfunction;
	    
	    var defaultCheck  = tableModel_arr[tb].cells[j].getAttribute("datatype");    
	    tableModel_arr[tb].cells[j].datatype = defaultCheck;
	    	    
		var td_Node_Children = tableModel_arr[tb].cells[j].childNodes;
		var td_Children = new Array();
		for (var m = 0; m < td_Node_Children.length; m ++)
		{
			if(td_Node_Children[m].tagName != null && td_Node_Children[m].tagName != "Z:COLUMN")
			{
				td_Children.push(td_Node_Children[m]);
			}
		}
		
		//\u5c06td\u5b50\u7ed3\u70b9\u7684\u96c6\u5408\u653e\u5230\u6a21\u578b\u5217\u4e2d
		tableModel_arr[tb].cells[j].td_Children = td_Children;
		
	}
    //\u521b\u5efa\u5185\u5b58\u8868ID\u5c5e\u6027,\u5e76\u653e\u5165\u6a21\u578b\u5bf9\u8c61\u7684tableId\u5c5e\u6027\u4e2d,\u89c4\u5219\u662f\u6a21\u578bID+"_ssb"
    tableModel_arr[tb].tableId = tb+"_ssb";
    //\u6a21\u578bID
    tableModel_arr[tb].MtableId = tb;
	//\u83b7\u53d6SID
	var sid = Mobject.getAttribute("sid");
    tableModel_arr[tb].sid = sid;
    //\u662f\u5426\u5206\u9875
    if(Mobject.getAttribute('turnpage') != null)
     tableModel_arr[tb].turnpage = Mobject.getAttribute('turnpage');
    else if(Mobject.getAttribute('editable') != 'true')tableModel_arr[tb].turnpage = "true";
    else tableModel_arr[tb].turnpage = "false";
    if(Mobject.getAttribute('turnpage') == 'false' && Mobject.getAttribute('editable') == null)
    tableModel_arr[tb].editable = 'true';
    //\u8bbe\u7f6e\u6bcf\u9875\u663e\u793a\u591a\u5c11\u884c\u7684\u4e0b\u62c9\u5217\u8868\u6846
    if(tableModel_arr[tb].turnpage == "true")
    {    	
        if(Mobject.getAttribute('pagesizelist') == null || Mobject.getAttribute('pagesizelist') == "")
        {
        	tableModel_arr[tb].pageSizeList = new Array("10","20","30","50");
        }
        else{
			var pagessize = Mobject.getAttribute('pagesizelist');
			tableModel_arr[tb].pageSizeList = pagessize.split(',');
			tableModel_arr[tb].pg.perPageCount = tableModel_arr[tb].pageSizeList[0];
		}
	}
	tableModel_arr[tb].props = props;
	tableModel_arr[tb].caption = caption;
	if(Mobject.getAttribute('leftButtonEvent')) 
	tableModel_arr[tb].leftButtonEvent = Mobject.getAttribute('leftButtonEvent');
	if(Mobject.getAttribute('rightButtonEvent'))
	tableModel_arr[tb].rightButtonEvent = Mobject.getAttribute('rightButtonEvent');
	if(Mobject.getAttribute('dbClickEvent'))
	tableModel_arr[tb].dbClickEvent = Mobject.getAttribute('dbClickEvent');
	if(Mobject.getAttribute(DISPLAYHEAD))
	tableModel_arr[tb].isDisplayHead = Mobject.getAttribute(DISPLAYHEAD);
	if(Mobject.getAttribute(VSTATCAPTION) && Mobject.getAttribute(VSTATCAPTION)!=''){		
		tableModel_arr[tb].vstext = Mobject.getAttribute(VSTATCAPTION);
		tableModel_arr[tb].vstatcaption = true;
	}
	else tableModel_arr[tb].vstatcaption = false;
	if(Mobject.getAttribute("onFinished") && Mobject.getAttribute(VSTATCAPTION)!='')
	tableModel_arr[tb].onFinished = Mobject.getAttribute("onFinished");	
};

var init_table_CSS = function(Mobject, table)
{
	if(Mobject.getAttribute("border") != null)
   {
   	table.setAttribute("border",Mobject.getAttribute("border"));
   }
   else{
   	table.setAttribute("border","0");
   }
   if(Mobject.getAttribute("cellpadding") != null)
   {
   	table.setAttribute("cellPadding",Mobject.getAttribute("cellpadding"));
   }
   else{
   	 table.setAttribute("cellPadding","0");
   }
   if(Mobject.getAttribute("cellspacing") != null)
   {
   	table.setAttribute("cellSpacing",Mobject.getAttribute("cellspacing"));
   }
   else{
   	 table.setAttribute("cellSpacing","1");
   }
  if(Mobject.getAttribute("width") != null)
   {
   	table.setAttribute("width",Mobject.getAttribute("width"));
   }
   else{
   	table.setAttribute("width","100%");
   }   
   if(Mobject.getAttribute("height") != null)
   {
   	table.setAttribute("height",Mobject.getAttribute("height"));
   }  
};
/********************************
  *\u521b\u5efa\u8868\u4f53\u4e3b\u8868\u8868\u5bf9\u8c61
  *\u8bbe\u7f6e\u8868\u7684\u8868\u4f53\u5217,\u8bbe\u7f6e\u8868\u4f53\u7684\u5c5e\u6027 \u6837\u5f0f\u7b49
  * @author zhanghongwei
  * @param 
  * @return table
 ********************************/ 
 var init_ssb_table_Tbody = function(tb)
 {
 	var Mobject = tableModel_arr[tb];
 	 var table = document.createElement('table');  
   var tbody = document.createElement('tbody');
   //\u8868\u683c\u5217\u5bf9\u8c61\u96c6\u5408
   var col = tableModel_arr[tb].cells;
   //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
   var	props = tableModel_arr[tb].props;
	//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
   var 	caption = tableModel_arr[tb].caption;
   
    //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
	  if(isIE())//\u5982\u679c\u662fIE
	  {
	  	 Mobject.parentElement.appendChild(table);	  	  
	  }
	  else{//
	  	 Mobject.parentNode.appendChild(table);
	  }
    table.appendChild(tbody);
	tableModel_arr[tb].table = table;
	table.setAttribute('id',tableModel_arr[tb].tableId);//\u8bbe\u7f6e\u8868\u5bf9\u8c61\u7684ID\u503c 
	
	init_table_CSS(Mobject, table);   
    //\u8bbe\u7f6e\u8868\u683cstyle\u5c5e\u6027
    table.style.cssText=tableModel_arr[tb].Mstyle;   
    var tt = document.getElementById(tableModel_arr[tb].tableId);
    var objRow = tt.insertRow(-1);
    if(Mobject.isDisplayHead == 'false')
    {
    	objRow.style.display = "none";
    }
   //\u8bbe\u7f6eCSS\u6837\u5f0f
    if(tableModel_arr[tb].MclassName != null && tableModel_arr[tb].MclassName != "")
    {
   	   table.className = tableModel_arr[tb].MclassName;
   	   objRow.className = "tr_detail";
    }
    else{
 
      table.className = "tb_datalist";//\u8bbe\u7f6e\u6837\u5f0f\u65f6\u7528className.
      objRow.className = "tr_detail";	
    }
   
    for(var i = 0; i < col.length; i++)
	{
        //\u521d\u59cb\u5316\u8868\u5934\u5217\u540d
	    var html = '';
	    if(col[i].getAttribute("checkall") == 'true' && col[i].getAttribute("iskey") == 'true')
	    {
	    	html += '<input style="vertical-align:middle;height: 18px;" type="checkbox" onclick="checkedAll(this, '+tb+')" name="allChecked" id="'+tb+'allCheck"/>';
	    }
	    if(col[i].getAttribute("caption") != null)
	    {
	    	var title = col[i].getAttribute("caption");
	    	html += title;
	    	caption[i] = title;
	    }

		var withd = col[i].getAttribute("width");
		var objCell = objRow.insertCell(-1);
		//\u8868\u6392\u5e8f
		var cellSort = col[i].getAttribute("allowsort");
		var sortHtml = '';
		var srotClass = null;
		if(isIE())
		{
			srotClass = "IEsortASC";
		}else{
			srotClass = "FFsortASC";
		}
		if(cellSort == 'true')
		{
			sortHtml += '<IMG id=img0 class='+srotClass+' style="cursor:pointer;" onclick="sortTable('+tableModel_arr[tb].tableId+', '+i+', this);">';
		}
		
		objCell.setAttribute("width", withd);
		objCell.setAttribute("style","TEXT-ALIGN: left");//style="TEXT-ALIGN: left"
		
		html += sortHtml;
		
		if(i == parseInt(col.length-1))//
		{
			objCell.innerHTML = '<SPAN >'+html+'</SPAN><span></span>';
		}else{
			objCell.innerHTML = '<SPAN >'+html+'</SPAN><span onmouseup="table_Header_Resize.EndResize(event);"  onmousedown="table_Header_Resize.StartResize(event,this,'+tableModel_arr[tb].tableId+');"></span>';
			objCell.lastChild.className = "resizeBar";
		}
		objCell.firstChild.className = "resizeTitle";
		
		//\u4fdd\u5b58\u884c\u5bf9\u8c61\u7684\u5c5e\u6027\u540d\u79f0		
		props[i] = col[i].getAttribute('prop');
	}
	
 };
/********************************
  *\u521d\u59cb\u8868\u7684\u65b9\u6cd5
  *1.\u5982\u679c\u662f\u5206\u9875\u8868,
  *  \u521d\u59cb\u5176\u8868\u5934,\u8868\u6a21\u578b\u5bf9\u8c61 \u548c \u5206\u9875\u5de5\u5177\u680f \u4e0d\u663e\u793a\u6570\u636e
  *2.\u5982\u679c\u662f\u53ef\u7f16\u8f91\u8868
  *  \u521d\u59cb\u5176\u8868\u5934,\u8868\u6a21\u578b\u5bf9\u8c61 \u6309\u7167\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027\u521d\u59cb\u5176\u6570\u636e \u548c\u884c
  * @author zhanghongwei 
 ********************************/

 init_ssb_table = function(tb)
{
	//\u521d\u59cb\u5316\u6a21\u578b
   init_ssb_tableModels(tb);
   //\u521b\u5efa\u8868\u5934\u6807\u9898
   var Mobject = tableModel_arr[tb];
   //\u5982\u679c\u6709title\u5c5e\u6027
   if(Mobject.getAttribute("title") != null &&Mobject.getAttribute("title") != "")
   {
  	  //\u5f97\u5230\u6a21\u578b\u5bf9\u8c61 
	   var htable = document.createElement('table');//\u521b\u5efa\u8868\u683c
	   //\u8bbe\u7f6e\u6837\u5f0f
	   init_table_CSS(Mobject, htable);   
	   var table_title = "";
	   if(table_title != null)
	   {
	   	table_title  = Mobject.getAttribute("title");
	   }
	   var row = htable.insertRow(-1);//insertRow(-1)firfox\u652f\u6301 
	   row.setAttribute("align", "left");
	   var cell1 = row.insertCell(-1);//insertCell(-1) firfox\u652f\u6301
	   cell1.innerHTML=table_title;
	   cell1.width = "95%";
	   var addButton = $res_entry('ui.control.table.addNewButton.name');
	   addButton = addButton.indexOf("$") == -1?addButton:"\u65b0\u589e";
	  //\u5982\u679c\u8868\u6a21\u578b\u5bf9\u8c61\u7684editable\u5c5e\u6027\u4e3a"true"\u5219\u521b\u5efa\u7684\u8868\u683c\u4e3a\u53ef\u7f16\u8f91\u8868\u683c
	  if(Mobject.getAttribute("editable") == "true")
	  {
	  	var cell2 = row.insertCell(-1);
	    cell2.innerHTML="<input class='button' type='button' name='Submit52' onClick=create_new_EditTableRow('"+tableModel_arr[tb].tableId+"','"+tb+"'); return false; value="+addButton+"  class='button'>";
	    cell2.width = "5%"
	  } 
  	 //\u5c06\u521b\u5efa\u7684\u8868\u5bf9\u8c61,\u52a0\u5165\u5230\u5f53\u524d\u7684DOM\u5bf9\u8c61\u4e2d
	  if(isIE())//\u5982\u679c\u662fIE
	  {
	  	 Mobject.parentElement.appendChild(htable);
	  }
	  else{//
	  	 Mobject.parentNode.appendChild(htable);
	  }
	  htable.className = "tb_titlebar";
	  htable.style.cssText=tableModel_arr[tb].Mstyle;
   }
   //\u521b\u5efa\u8868\u683c\u4e3b\u4f53
      if(tableModel_arr[tb].table) return;   
    init_ssb_table_Tbody(tb);
	//\u5982\u679c\u662f\u4e0d\u53ef\u7f16\u8f91\u8868\u683c
	if(Mobject.getAttribute("turnpage") == 'true')
	{
        //\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
		var div_table = document.createElement("table");
		var table_body = document.createElement("tbody");
		div_table.appendChild(table_body);
		var div_tr = document.createElement("tr");
		 table_body.appendChild(div_tr);		
		var dir_tr_td = document.createElement('td');	 
	     div_tr.appendChild(dir_tr_td);	
		 tableModel_arr[tb].toolbarId = tb+"_pages";
		 dir_tr_td.setAttribute('id',tableModel_arr[tb].toolbarId);
		 dir_tr_td.setAttribute('align','right');
		if(isIE())
		{
			Mobject.parentElement.appendChild(div_table);
		}
		else{
			Mobject.parentNode.appendChild(div_table);
		}
        div_table.id = "div_"+tb;
		div_table.className="toolbarTable";	
		div_table.style.cssText=tableModel_arr[tb].Mstyle;
		tableModel_arr[tb].pg.printHtml();//\u521b\u5efa\u5206\u9875\u5de5\u5177\u680f
	}
	//\u5982\u679c\u662f\u53ef\u7f16\u8f91\u8868\u683c
    else if(Mobject.getAttribute("editable") == 'true')
	{  
		tableModel_arr[tb].editable = Mobject.getAttribute("editable");
	    //\u521d\u59cb\u5316\u8868\u683c
	   if(Mobject.getAttribute("initrow") != null)
	   {	   		
	        tableModel_arr[tb].initrow = Mobject.getAttribute('initrow');
			//alert(tableModel_arr[tb].initrow);
	   		for(var i = 0; i < tableModel_arr[tb].initrow; i ++)
	   		{ 
	   			create_new_EditTableRow(tableModel_arr[tb].tableId , tb);
	   		}
	   }
		
	}
   /***\u521d\u59cb\u5316\u52a8\u6001\u8c03\u6574\u8868\u5217\u5bbd\u53c2\u6570***/
  table_Header_Resize.resizeInit();
};

 /*******************************
  *\u65b0\u589e\u7a7a\u767d\u884c.
  *objTable \u8868\u683c\u5bf9\u8c61
  *tb \u6a21\u578b\u8868ID
 ********************************/
  InsertRow = function(objTable,tb)
  {
	 var colNum = tableModel_arr[tb].cells.length;
	 var objRow = objTable.insertRow(-1);
	 for(var i = 0; i < colNum; i++)
	 {
	   var objCell = objRow.insertCell(-1); 
	 }
	  return objRow;
 };
 
 /********************************
  *\u8868\u683c\u65b0\u589e\u4e00\u884c.
  *Mtid \u6a21\u578b\u8868\u6807\u7b7eID
  ********************************/
  addOneRow = function(Mtid)
 {  
  var tableId = tableModel_arr[Mtid].tableId; //\u5185\u5b58\u8868
  return create_new_EditTableRow(tableId, Mtid);
 };
 
 /********************************
  *\u5220\u9664\u8868\u683c\u7684\u4e00\u884c.
  *tableId \u8868\u683c\u5bf9\u8c61ID
  *e \u884c\u5bf9\u8c61\u5c5e\u6027
  ********************************/
  deleteOneRow = function(tb,e,tf)
 { 
  var tableId = tableModel_arr[tb].tableId;
  var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
  var tr = e; //\u83b7\u53d6\u884c\u5bf9\u8c61
  while(tr.tagName.toLowerCase() != 'tr')
  {
  	 tr = tr.parentNode
  }
  //\u7b2c\u4e00\u884c\u662f\u8868\u5934,\u5e94\u8be5\u53bb\u6389
  var index = tr.rowIndex;
  
 if(tf == undefined || tf != false)
 {
	 if(confirm('\u786e\u5b9a\u5220\u9664\u5417?'))
	 {
	  objTable.deleteRow(index);
	  reSetTableRowNum(tb, index);
	  return true;
	 }
	 else
	 {
	   return false;
	 }
 }
 else{
 	  objTable.deleteRow(index);
	  reSetTableRowNum(tb, index);
	  return true;
 }
};

function reSetTableRowNum(MtableId, rowIndex)
{
	var tableId = tableModel_arr[MtableId].tableId;
    var objTable = document.getElementById(tableId); //\u83b7\u53d6\u8868\u5bf9\u8c61
    var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9
    
    var rows =  objTable.rows;
    if(rows.length == 1)
    {
    	var t_inputs = objTable.getElementsByTagName("input");
    	for(var i = 0; i < t_inputs.length; i ++)
    	{
    		if(t_inputs[i].type.toUpperCase() == "CHECKBOX")
    		  t_inputs[i].checked = false;
    	}
    }
    for(var i = rowIndex; i < rows.length; i ++)
    {
    	if(rows[i].rowIndex % 2 == 0)
	    {
		   rows[i].className="even";
	    }else{
	       rows[i].className="";
	    }
	  for(var j =0; j < tdnodes.length; j++)
      {
	    var prop=tableModel_arr[MtableId].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
     	if(tableModel_arr[MtableId].cells[j].td_Children.length == 0)
     	{
     	  if(prop == "$ROWNUM")
     	  {
     	      rows[i].cells[j].innerHTML=rows[i].rowIndex;
     	  }
     	}
      }
    }
    if(tableModel_arr[MtableId].turnpage == "true" || tableModel_arr[MtableId].editable == "false")
    {
    tableModel_arr[MtableId].pg.totalrow = tableModel_arr[MtableId].pg.totalrow > 0? tableModel_arr[MtableId].pg.totalrow-1:0;
    tableModel_arr[MtableId].pg.getPageCount();
    tableModel_arr[MtableId].pg.printHtml();
    }
}; 
//\u8bbe\u7f6e\u884c\u7684css
var setRowCss = function(row, rowClass,index)
{
	 var num = row.rowIndex;
	 if(index != null)
	 {
	 	num = index+1;
	 }
	 if(num % 2 == 0)
	 {
		row.className = ROW_CLASS_EVEN;
	 }else{
	 	row.className = "";
	 } 
};
/********************************
  *\u7ed9\u8868\u683c\u5bf9\u8c61\u521b\u5efa\u884c.
  *MtableId \u8868\u683c\u6a21\u677fID
  *tableData \u884c\u5bf9\u8c61
 ********************************/
 pushData2ssbTable = function(MtableId,tableData)
{
   var tableId = tableModel_arr[MtableId].tableId; //\u83b7\u53d6\u5185\u5b58\u8868\u7684ID
   var tObject = document.getElementById(tableId); //\u5185\u5b58\u8868\u5bf9\u8c61
   //\u63d2\u5165\u6570\u636e\u524d\u6e05\u7a7a\u8868\u4e2d\u6570\u636e.
   var rows = tObject.rows.length;//\u5f53\u524d\u8868\u7684\u884c\u6570
   for(var j = rows ; j > 1; j --)
   {        
   	   tObject.deleteRow(j-1);  	
   }
  //\u5faa\u73af\u63d2\u5165\u7a7a\u767d\u884c.
  for (var i = 0; i < tableData.length; i ++)
  {	    
     var row = InsertRow(tObject,MtableId);	   
	 setRowCss(row,ROW_CLASS_EVEN);
	 bindButtonEvent2Ctrl(row, MtableId);
  }
  // \u7ed9\u8868\u7684\u884c\u586b\u5145\u6570\u636e
   bindData2TableCtrl(MtableId,tableData);
};

 bindButtonEvent2Ctrl = function(rowObj, Mtid)
 {
 	var model = tableModel_arr[Mtid];
 	if(model.leftButtonEvent && model.rightButtonEvent){
 		rowObj.onmousedown = function(){
	 		if(event.button == 1)
 			parseFunction(model.leftButtonEvent, rowObj);
		 	else if(event.button == 2)
 			parseFunction(model.rightButtonEvent, rowObj);
 		}	
 	}
 	else if(model.leftButtonEvent){
 		rowObj.onmousedown = function(){
	 		if(event.button == 1)
	 		parseFunction(model.leftButtonEvent, rowObj);
	 	}
 	}
 	else if(model.rightButtonEvent){
	 	rowObj.onmousedown = function(){
	 		if(event.button == 2)
	 		parseFunction(model.rightButtonEvent, rowObj);
	 	}
 	}
 	if(model.dbClickEvent)
 	{
 		rowObj.ondblclick = function(){
 			parseFunction(model.dbClickEvent, rowObj);
 		}
 	}
 	
 };
 
 var parseFunction = function (defaultParm,obj)
{
	var def_parms = defaultParm;
    if(def_parms != ""){
        var str_val = unescape(defaultParm);
        var ssbfunction =  str_val;     
         if(str_val.indexOf('this') != -1){            	
           	eval("var obj = obj");           
           	var ssbfunction = str_val.replace("this","obj");
         }
        return eval(ssbfunction);
    }
};
  /***************************************
   * \u4f20\u5165\u4e00\u4e2aUTC\u65f6\u95f4 \u8fd4\u56de\u4e00\u4e2a\u65e5\u671f\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
   * @param utcTime UTC\u65f6\u95f4
   ***************************************/
  var TIME_OFFSET = new Date().getTimezoneOffset()*60*1000;//\u65f6\u533a\u5dee
  var TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  getTimefromUTC = function(format, utcTime)
  {
  	if(utcTime == null || utcTime == '')
  	{
  		return '';
  	}
	var utc_number = utcTime;
	var nd = new Date(utc_number);
	var yy = "-";
	var mm = "-";
	var _HH_MM_SS = "";
	if(format != null)
	{
		format = format.toLowerCase();
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		yy = format.substring(y_endindex+1,y_endindex+2);
	    var m_index = format.indexOf("m");		
		mm = format.substring(m_index+2, m_index+3);
		var hour = new String(nd.getHours()).length > 1? nd.getHours():"0"+nd.getHours();
		var minutes = new String(nd.getMinutes()).length > 1? nd.getMinutes():"0"+nd.getMinutes();
		var seconds = new String(nd.getSeconds()).length > 1? nd.getSeconds():"0"+nd.getSeconds();
		
		if(format.indexOf("hh") != -1)
		{
			_HH_MM_SS = _HH_MM_SS + " " + hour;			
		}
		if(format.lastIndexOf("mm") != m_index)
		{
			if(_HH_MM_SS == "")
			  _HH_MM_SS = _HH_MM_SS + " " + minutes;
			else 
			  _HH_MM_SS = _HH_MM_SS + ":" + minutes;
		}
		if(format.indexOf("ss") != -1)
		{
			if(_HH_MM_SS == "")
			  _HH_MM_SS = _HH_MM_SS + " " + seconds;
			else 
			  _HH_MM_SS = _HH_MM_SS + ":" + seconds;			
		}
	}
    var month = nd.getMonth()+1+"";
    if(month.length == 1)
    {
    	month = "0" + month; 	
    }
    var date  = nd.getDate()+"";
    if(date.length == 1)
    {
    	date  = "0" + date;
    }
	var time = nd.getFullYear() + yy + month + mm + date + _HH_MM_SS;
	return time;
  };
  
  /***************************************
   * \u5c06\u65e5\u671f\u683c\u5f0f\u7684\u6570\u636e\u8f6c\u6362\u6210UTC\u3002
   * @author \u5f20\u5b8f\u4f1f
   * @param format \u65e5\u671f\u663e\u793a\u7684\u683c\u5f0f
   * @param Time \u8981\u8f6c\u6362\u6210UTC\u65f6\u95f4\u7684\u65e5\u671f
   ***************************************/
  
  getUTCfromTime = function(format, time)
  {
  	if(time == null || time == '')
  	{
  		return null;
  	}
  	var utc = "";
	var yy = "-";
	var mm = "-";
	var date_format = new Array();
	var date_format1 = "";
	var date_format2 = "";
	var time_arr = new Array();
	var time1 = time;
	var time2 = "";
	if(format != null)
	{
		if(format.length > 10)
		{
		    date_format = format.split(" ");
		    date_format1 = date_format[0];
		    date_format2 = date_format[1];
		    time_arr = time.split(" ");
		    time1 = time_arr[0];
		    time2 = time_arr[1];
		}else{
			date_format1 = format;
			time1 = time;
		}
		var y_index = date_format1.indexOf("y");
		var y_endindex = date_format1.lastIndexOf("y");
		yy = date_format1.substring(y_endindex+1,y_endindex+2);	

	    var m_index = date_format1.indexOf("m");
		var m_endindex = date_format1.lastIndexOf("m");
		mm = date_format1.substring(m_endindex+1, m_endindex+2);
	}
	var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
	var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);
	time1 = time1.replace(YM,',');
	time1 = time1.replace(MD,',');
    var tt = time1.split(",");
    if(time2.length < 1){
    	var date = new Date(tt[0],parseInt(tt[1]-1),tt[2]);
    	utc = date.getTime();
    }else{
    	var HH = time2.split(":");
    	 var date = new Date(tt[0],parseInt(tt[1]-1),tt[2],HH[0],HH[1],HH[2]);
    	 utc = date.getTime();
    }
	return parseInt(utc);
  };

/********************************
  *\u8868\u683c\u63a7\u4ef6\u65e5\u671f\u7c7b\u578b\u5904\u7406\u5668
  *tageObject \u6807\u7b7e\u5bf9\u8c61
  *prop  \u6807\u7b7e\u5c5e\u6027\u53c2\u6570
 ********************************/
 var SSB_RIA_CALENDAR = "../images/calendar.gif";
  Calendar_Ctr_Processor = function(tagObject, value, rowindex, MtableId)
 {
 	var _onchange_event = (tagObject.getAttribute("onchange")!=undefined)?tagObject.getAttribute("onchange"):"";
    var inputTarget = tagObject.getAttribute('target')+rowindex;	//\u83b7\u53d6\u5185\u5b58\u8f93\u5165\u6846\u7684ID
   	var date_formate = tagObject.getAttribute('formate'); //\u83b7\u53d6\u65e5\u671f\u683c\u5f0f
   	var mid = 	tagObject.getAttribute('id');
   	var imgurl = tagObject.getAttribute("imgurl");
   	var _calendar_WIDTH = tagObject.getAttribute("width");
    var _calendar_size = "16";	
    var inputTarget_show = inputTarget;
    if(_calendar_WIDTH != null && _calendar_WIDTH != "")
	{
		_calendar_size = _calendar_WIDTH;
	} 
   	if(imgurl == null || imgurl == "")
	{
		imgurl = SSB_RIA_CALENDAR;
	}
	var nullflag = 'yes';
	var nullable = tagObject.getAttribute('nullable');
	if(nullable != null && nullable != '')
	{
		nullflag = 'no';
	}
	
   	//\u5c06\u65e5\u671f\u7c7b\u578b\u5b58\u5165\u5168\u5c40\u6a21\u578b
   	if(tableModel_arr[MtableId].date_formate == null || tableModel_arr[MtableId].date_formate == '')
	{
		tableModel_arr[MtableId].date_formate = date_formate;
	}
   	var time = getTimefromUTC(date_formate, value);
   	var Hms  = time;
   	var type1 = "text";
	var type2 = "hidden";		 
	var html = '<INPUT type="'+type2+'" id="'+inputTarget+"_"+mid+'" readonly="readonly" ';
		html += ' value="'+Hms+'"  size="'+_calendar_size+'" isDate="true">&nbsp; ';
		html += '<INPUT type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'"  onchange="'+_onchange_event+'"';
		html +=	'readonly= true  value="'+time+'" isDate="false" size="'+_calendar_size+'">';
	if(tagObject.getAttribute("isFullDate") == "false"){
	 	type1 = "hidden";
	 	type2 = "text";
	 	Hms   = time.split(' ')[1];
	 	inputTarget_show = inputTarget+"_"+mid;
	 	html = '<INPUT type="'+type1+'" id="'+inputTarget+'" name="'+inputTarget+'"  onchange="'+_onchange_event+'"';
		html +=	'readonly= true  value="'+time+'" isDate="true" size="'+_calendar_size+'">';
		html += '<INPUT type="'+type2+'" id="'+inputTarget_show+'" readonly="readonly" ';
		html += ' value="'+Hms+'"  size="'+_calendar_size+'" isDate="false">&nbsp; ';
	    
	 }	
	    html += '<img style="cursor:pointer;" border="0" src="'+imgurl+'" width="16" height="15"  ';
		html += 'onClick="fPopCalendar(\''+inputTarget+'\',\''+inputTarget+'\',\''+date_formate+'\',\''+inputTarget_show+'\');return false"> ';				
	
	return html;
 };
 /*****************************
  * LOV\u5904\u7406\u5668
  * @param
  * @return
  *****************************/
 Lov_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
 {
    var targetId = retagObject.getAttribute('targetCompId');
    var jsevent = retagObject.getAttribute('jsEventId');
    if(jsevent&&(jsevent != ""||jsevent != null)){retagObject.setAttribute('jsEventId',retagObject.getAttribute('jsEventId')+rowindex);}
    else {retagObject.setAttribute('jsEventId',"");}
    var nid_arr = targetId.split(",");
    for(var i = 0; i < nid_arr.length; i++)
    {
    	nid_arr[i] = nid_arr[i] + rowindex;
    }
    var ntargetId = nid_arr.join(",");
    retagObject.setAttribute('targetCompId', ntargetId);    
    return retagObject;
 };
 /*****************************
  * autoComplete\u5904\u7406\u5668
  * @param
  * @return
  *****************************/
 autoComplete_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
 {
    var nctrId = retagObject.getAttribute('ctrlId') + rowindex;
    var nId = retagObject.getAttribute('id') + rowindex;
    retagObject.setAttribute('ctrlId', nctrId);
    retagObject.setAttribute('id', nId);
    return retagObject;
 };
 /*****************************
  * \u5355\u9009\u6846\u63a7\u4ef6\u5904\u7406\u5668
  * @param
  * @return
  *****************************/
 radioGroup_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
 {
 	retagObject.innerHTML = '';
 	return retagObject;
 };
 
 upload_Ctr_Processor = function(retagObject, value, rowindex, MtableId)
 {
    return retagObject;
 };
  /*****************************
  * \u5904\u7406EL\u8868\u8fbe\u5f0f
  * @param
  * @return
  *****************************/
 getOuterHtml = function(retagObject,data)
 {
 	//eval("var obj=retagObject;");
 	if(retagObject.nodeName == "#text")
 	{
 		 return;
 	}
 	var attrs = new Array();
    attrs.push("value");
    attrs.push("name");
    attrs.push("id");
    attrs.push("href");    
    attrs.push("src");     
    for(var j = 0; j < retagObject.childNodes.length; j++) 
    {
    	getOuterHtml(retagObject.childNodes[j], data);
    }
 	for(var i = 0; i < attrs.length; i ++)
 	{ 		
 		var attrname = attrs[i];
 		var value = decodeURIComponent(retagObject.getAttribute(attrname));
 		if(retagObject.nodeName.toUpperCase() == "LABEL") 	   
 		   value = retagObject.innerHTML;
 		if(value != null && value != ''&& typeof value == 'string')
 		do{
			    var start = value.indexOf("${");	    
			    //\u5982\u679c\u6709\u53c2\u6570
			    if(start != -1)
			    {
			    	var end = value.indexOf("}")+1;
			    	var old = value.substring(start, end);
			    	var prop = value.substring(start+2,end-1); 	
			    	nn = data[prop];
			    	//nn = eval("data."+prop);			    	
				    value =value.replace(old,nn);
				    if(retagObject.nodeName.toUpperCase() == "LABEL") 	   
 		             innerText_function(retagObject, value); 		            
 		             retagObject.setAttribute(attrname, value);
				    //eval("obj."+attrname+"=\""+value+"\";");				   		  
				}
		  }while(value.indexOf("${") != -1);
 	}
 	if(retagObject.tagName.toUpperCase() != "SELECT")
 	{ 	
        var text = "";
		if(isIE())
		{
			text = retagObject.innerText;
		}else{
			text = retagObject.textContent;
		}
		if(text != '')
		{
			var start = text.indexOf("${");
		    //\u5982\u679c\u6709\u53c2\u6570
		    if(start != -1)
		    {
		    	var end = text.indexOf("}")+1;
		    	var old = text.substring(start, end);
		    	var text1 = text.substring(start+2,end-1);
		    	//mm = evalParse(data,text1);
		    	mm = data[text1];
			    text =text.replace(old,mm);
			}
			
			innerText_function(retagObject, text);
		}
 	}
  return retagObject;
 };
/*****************************
  * \u4e0b\u6765\u5217\u8868\u6846\u5904\u7406\u5668
  * @param
  * @return
  *****************************/
 select_Ctr_Processor = function(reTagObject, value, rowindex, MtableId, data)
 {
 	var option_values = new Array();
    var option_texts = new Array();
    var selectValue = '';
    var ctrlex = reTagObject.getAttribute("ctrlex");		   
   //\u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027
   if(ctrlex != null)
   {
   	   reTagObject.innerHTML = "";
   	   var ctrlarr = reTagObject.getAttribute("ctrlex").split(';');//\u83b7\u53d6\u6269\u5c55\u5c5e\u6027		 
	   for (var i = 0; i < ctrlarr.length; i ++)
	   {
	   	  var arr = ctrlarr[i].split(':');
	   	  if(arr.length == null)
	   	  {
	   	     break;
	   	  }
	   	  //\u7ed9\u9009\u9879\u7684text\u548cvalue\u8d4b\u503c
	   	  switch (arr[0].toUpperCase()) 
	   	  {
	   	  	case 'TEXT' :
	   	  	    var text_arr = new Array();
	   	  	    text_arr = arr[1].split('.');
	   	  	    var text_arr_options = evalParse(data,text_arr[0]);
	   	  	    text_arr_options = evalParse(text_arr_options,text_arr[1]);
	   	  	    for(var m = 0; m < text_arr_options.length; m++)
	   	  	    {
	   	  	    	option_texts[m] = evalParse(text_arr_options[m],text_arr[2]);
	   	  	    }
	   	  	   
	   	  		break;
	   	  		
   	  		case 'VALUE' :
	   	  	    var value_arr = new Array();
	   	  	    value_arr = arr[1].split('.');
	   	  	    var value_arr_options = evalParse(data, value_arr[0]);
	   	  	    value_arr_options = evalParse(value_arr_options, value_arr[1]);
	   	  	    for(var n = 0; n < value_arr_options.length; n++)
	   	  	    {
	   	  	    	option_values[n] = evalParse(value_arr_options[n],value_arr[2]);
	   	  	    }
	   	  		break;
	   	  		
   	  		case 'DEFAULT' :
   	  			 	   
   	  			var select_arr = arr[1].split('.'); 	   
	   	  	    selectValue = evalParse(data,select_arr[0]);
	   	  	    selectValue = evalParse(selectValue,select_arr[1]);	   	  	
	   	  		break;
	   	  		
   	  		default :	   	  		
   	  		    
   	  			break;
	   	  }//end switch
	   }//end for

       //\u521b\u5efa\u4e0b\u62c9\u9009\u9879
	   for(var i =0; i < option_values.length; i++)
	   {
	   	  var option = document.createElement("option");
	   	  option.value = option_values[i];
		  
	   	  option.innerHTML = option_texts[i];
		  
	   	  //\u8bbe\u7f6e\u9009\u4e2d\u9879
	   	  if(option_values[i] == selectValue)
	   	  {
	   	      option.selected="selected";
	   	  }
	   	  reTagObject.appendChild(option);
	   }
   
   }//end if \u5982\u679c\u6709\u6269\u5c55\u5c5e\u6027   
   else{//\u5982\u679c\u6ca1\u6709\u6269\u5c55\u5c5e\u6027
     var sel_children = reTagObject.getElementsByTagName("option");
     //\u5c06\u63a7\u4ef6\u7684prop\u5c5e\u6027\u503c\u548c\u4e0b\u62c9\u9009\u9879\u7684value\u503c\u5bf9\u6bd4\uff0c\u5982\u679c\u76f8\u7b49\u5219\u9009\u4e2d
     for(var j = 0; j < sel_children.length; j ++ )
     {
     	if(new String(value) == sel_children[j].value)//\u5982\u679cprop\u5c5e\u6027\u503c\u548coption.value\u76f8\u7b49
     	{
     		sel_children[j].selected = "selected";
     	}
     }
   }
   return reTagObject;
 };
 /*******************************
  * \u8d85\u8fde\u63a5\u6807\u7b7e\u5904\u7406\u5668
  * @author
  * @param
  *******************************/
 var A_Ctr_Processor = function(reTagObject, value, rowindex, MtableId,data)
 {
 	var href = String(reTagObject.getAttribute('href'));
    var http = document.location;
    var host = http.host.toString();
    var pathname = http.pathname.toString();
    var arr = pathname.split('/');
	var newArr = new Array();
	var hrefarr = href.split('/');
	for(var k = 1; k < hrefarr.length; k++)
	{
		newArr.push(hrefarr[k]);
	}
	var nhref = newArr.join('/');
	nhref = "http://"+host+"/"+arr[1]+"/"+nhref;			
	if(href.indexOf("#") != -1 || href == "")
	{
		nhref = "#";
	}
	reTagObject.setAttribute('href',nhref);
	
	 if(reTagObject.getAttribute('onclick')!=null)
	  {
		var onclick = reTagObject.getAttribute('onclick').toString();
		var start = onclick.indexOf("${");
	    if(start != -1)
	    {
	    	var end = onclick.indexOf("}")+1;
	    	var old = onclick.substring(start, end);
	    	var prop = onclick.substring(start+2,end-1);	    	
	    	nn = evalParse(data,prop);
		    onclick =onclick.replace(old,nn);
            reTagObject.setAttribute("onclick", null);
            reTagObject.setAttribute("onclick", onclick);
		}
		var even = function(){eval(onclick);};	
		if(isIE())
		{
			reTagObject.attachEvent("onclick",even);
		}else{
			reTagObject.addEventListener("onclick",even, false);
		}
	  }
    return 	reTagObject;
 };
 /*******************************
  * input\u6807\u7b7e\u5904\u7406\u5668
  * @author
  * @param
  *******************************/
 input_Ctr_Processor = function(reTagObject, value, rowindex, MtableId)
 {
 	if((reTagObject.getAttribute('type') != "button") && (reTagObject.getAttribute('type') != "reset"))
	{				
		if(value != null)   reTagObject.value = value;
		addEventForCheckBox(reTagObject, MtableId);
	}
	return reTagObject;
 };
 function addEventForCheckBox(TagObject, MtableId)
 {
 	if(TagObject.getAttribute('type') && (TagObject.getAttribute('type')).toUpperCase() == "CHECKBOX")
		{
			   var onclick = "";
			   if(TagObject.getAttribute('onclick')!=null)
			    {
					onclick = TagObject.getAttribute('onclick').toString();
					var start = onclick.indexOf("${");
				    if(start != -1)
				    {
				    	var end = onclick.indexOf("}")+1;
				    	var old = onclick.substring(start, end);
				    	var prop = onclick.substring(start+2,end-1);	    	
				    	nn = evalParse(data,prop);
					    onclick =onclick.replace(old,nn);
					}
			    }	
				var even = function(){
					
					if(TagObject.checked == false)
					{
						document.getElementById(MtableId+'allCheck').checked = TagObject.checked;
					}
					eval(onclick);
				};	
				if(isIE())
				{
					TagObject.attachEvent("onclick",even);
				}else{
					TagObject.addEventListener("onclick",even, false);
				}			 
		}
		
	///return 	TagObject;
 };
/********************************
  *\u8868\u683c\u63a7\u4ef6\u5904\u7406\u5668
  *@param tageObject \u6807\u7b7e\u5bf9\u8c61
  *@param prop  \u6807\u7b7e\u5c5e\u6027\u53c2\u6570
 ********************************/
 table_Ctr_Processor = function(tagObject, value, rowindex, MtableId,data)
{
    var ctrName = tagObject.tagName;
	var tagName = ctrName.toUpperCase();//\u6807\u7b7e\u540d\u79f0
	var reTagObject = tagObject.cloneNode(true); //\u8fd4\u56de\u7684\u6807\u7b7e\u5bf9\u8c61
    if(reTagObject.id != null)  reTagObject.id = reTagObject.id+rowindex;
    reTagObject = getOuterHtml(reTagObject,data);
	switch (tagName)
	{
		case "INPUT" : //\u8f93\u5165\u6846
			return input_Ctr_Processor(reTagObject, value, rowindex, MtableId);
			break;
		
		case "A" : //\u8d85\u8fde\u63a5\u6807\u7b7e
			return A_Ctr_Processor(reTagObject, value, rowindex, MtableId,data);			
			break;
			
		case "SELECT" ://\u4e0b\u62c9\u5217\u8868
			return select_Ctr_Processor(reTagObject, value, rowindex, MtableId, data);
			break;			
		
		case "CALENDAR": //\u65e5\u671f\u63a7\u4ef6
		    return Calendar_Ctr_Processor(reTagObject, value, rowindex, MtableId);		
			break;
	
	   case "Z:CALENDAR": //\u65e5\u671f\u63a7\u4ef6
		    return Calendar_Ctr_Processor(reTagObject, value, rowindex, MtableId);		
			break;
			
	   case "IMG": //\u56fe\u7247\u6807\u7b7e
		    return reTagObject;
			break;
				
	   case "Z:SSB_RIA_LOV"://lov\u63a7\u4ef6FF
	        return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "SSB_RIA_LOV"://lov\u63a7\u4ef6IE
	        return Lov_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "Z:AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
	        return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "AUTOCOMPLETE"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
	        return autoComplete_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	          
	   case "Z:RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
	        return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "RADIOGROUP"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
	        return radioGroup_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "Z:UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6FF
	        return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;
	        
	   case "UPLOAD"://\u81ea\u52a8\u5b8c\u6210\u63a7\u4ef6IE
	        return upload_Ctr_Processor(reTagObject, value, rowindex, MtableId);
	        break;            
	   default :
		    return reTagObject;
			break;

	}

};
/****************************
  *\u7ed9\u8868\u683c\u5bf9\u8c61\u8d4b\u503c.
  *MtableId \u8868\u683c\u6a21\u677fID
  *tableData \u884c\u5bf9\u8c61
 **************************/
 bindData2TableCtrl = function(MtableId,tableData)
{
   var tableId = tableModel_arr[MtableId].tableId;// \u83b7\u53d6\u5185\u5b58\u8868\u683cID   
   var tObject = document.getElementById(tableId);//\u5185\u5b58\u8868\u683c
   var tModle = document.getElementById(MtableId);//\u8868\u683c\u6a21\u677f
   var autoCtrId_arr = new Array();
   var row =  tObject.rows;
   var tdnodes = tableModel_arr[MtableId].cells; //\u83b7\u5f97\u6a21\u578b\u5bf9\u8c61\u7684\u6240\u6709\u5b50\u7ed3\u70b9
  for (var i = 1; i < row.length; i ++)//\u5faa\u73af\u5185\u5b58\u6bcf\u4e00\u884c
  {    
     
     var rowcells = row[i].cells;//\u53d6\u884c\u5bf9\u8c61
          	    
     for(var j =0; j < tdnodes.length; j++)//\u5faa\u73af\u6a21\u578b\u8868\u683c
     {
        var datatype = tdnodes[j].getAttribute("datatype");
     	var contrlnode='';

     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
     	if(tableModel_arr[MtableId].cells[j].td_Children.length == 0)
     	{
     		var statusDesc = tdnodes[j].getAttribute("ctrlex");
     	  var prop=tableModel_arr[MtableId].props[j];//\u5f97\u5230column\u7684prop\u5c5e\u6027
     	  //var text = evalParse(tableData[parseInt(i-1)],prop);
     	  var text = eval("tableData["+parseInt(i-1)+"]."+prop);
     	  //\u884c\u5e8f\u53f7
     	  if(prop == '$ROWNUM')
     	  {
     	  	text = i;
     	  }
     	  //\u5982\u679c\u662f\u65e5\u671f\u578b\u7684
     	  else if(datatype != null && datatype.toLowerCase().indexOf("date")!= -1)
     	  {
     	  	var formate = null;
     	  	var f = datatype.toLowerCase();
     	  	var s = f.indexOf("(");
     	  	if(s != -1)
     	  	{
     	  		formate = f.substring(s+1, f.indexOf(")"));
     	  	}
     	  	text = getTimefromUTC(formate, text);
     	  }
     	  //firfox \u4e2d\u4e0d\u652f\u6301innerText;
         column_ctrlex_Processor(rowcells[j], statusDesc, text);
     	}
     	else{//\u5305\u542b\u63a7\u4ef6     	   
     	   var prop='';
     	   //\u5b58\u5728column\u7684prop\u5c5e\u6027
     	   if(tdnodes[j].getAttribute("prop") != null)
     	   {
     	      prop=tdnodes[j].getAttribute("prop");
   	       }
     	   //\u5faa\u73af\u5217\u91cc\u9762\u7684\u63a7\u4ef6
     	   
     	    for(var k = 0; k < row[i].cells[j].childNodes.length; k++ )
     	    {     	    	
     	     //\u521b\u5efa\u5b50\u8282\u70b9\u65f6,\u5220\u9664\u65e7\u7684\u8282\u70b9
     	     row[i].cells[j].removeChild(row[i].cells[j].childNodes[k]);
     	    }
     	    
     	    //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
     	   for(var m=0; m<tableModel_arr[MtableId].cells[j].td_Children.length; m++)
     	   {
     	     //\u53d6\u5f97\u6a21\u677ftd\u91cc\u9762\u7684\u63a7\u4ef6
     	     contrlnode=tableModel_arr[MtableId].cells[j].td_Children[m];
     	     //\u5224\u65adtd\u91cc\u9762\u7684\u63a7\u4ef6\u662f\u5426\u5b58\u5728prop\u5c5e\u6027,\u5982\u679c\u5b58\u5728,\u4ee5\u63a7\u4ef6\u7684prop\u5c5e\u6027\u4e3a\u51c6
     	     if(contrlnode.getAttribute("prop") != null)
     	     {   
     	        prop = contrlnode.getAttribute("prop");
     	     }
     	      //\u53d6\u5f97\u63a7\u4ef6\u7c7b\u578b
     	      var value = "";
     	      if(prop != null && prop != "")
     	      {
     	         //value = evalParse(tableData[parseInt(i-1)],prop);
     	         value = eval("tableData["+parseInt(i-1)+"]."+prop);
     	      }
     	      var ctrTag=table_Ctr_Processor(contrlnode,value,i,MtableId,tableData[parseInt(i-1)]);//\u5f97\u5230\u63a7\u4ef6\u5904\u7406\u5668\u7c7b\u578b;
     	      //\u5982\u679c\u8fd4\u56de\u7684\u662fString
     	      if(typeof ctrTag == "string")
			  {
			  	 row[i].cells[j].innerHTML=ctrTag; 
			  }
     	      else{ //\u5982\u679c\u662f\u5bf9\u8c61
			  	 row[i].cells[j].appendChild(ctrTag);
				  var Ctr_tagName = ctrTag.tagName.toUpperCase();
				  //\u5982\u679c\u662fLOV\u6807\u7b7e
				  if(Ctr_tagName == 'Z:SSB_RIA_LOV' || Ctr_tagName == 'SSB_RIA_LOV')
				  {
				  	  readRiaLovTag(ctrTag);
				  	  var tt = document.getElementById(ctrTag.id);
				  	  tt.value = value;
				  }
				  //\u5982\u679c\u662f\u81ea\u52a8\u5b8c\u6210\u6807\u7b7e
				  if(Ctr_tagName == 'Z:AUTOCOMPLETE' || Ctr_tagName == 'AUTOCOMPLETE')   autoCtrId_arr.push(ctrTag.getAttribute('id'));
			      //\u662fradioGroup\u63a7\u4ef6
			      if(Ctr_tagName == 'Z:RADIOGROUP' || Ctr_tagName == 'RADIOGROUP')
			      {			      	  
			      	  radios=new RadioGroupTag(ctrTag.getAttribute('id'));
                      radios.init();
                      radios.setValue(value);
			      }
			      if(Ctr_tagName == 'Z:UPLOAD' || Ctr_tagName == 'UPLOAD')
			      {			      	  
			      	  var upload = UploadTag(ctrTag.getAttribute('id'));
		              upload.init();
		              var oInput=upload.getFileCtrl();
		              oInput.value = value;
			      }
			      //\u5982\u679c\u5b58\u5728type\u5c5e\u6027
	     	      var tagtype = ctrTag.getAttribute("type");
				  if(tagtype != null)
				  {
				  	  tagtype = tagtype.toUpperCase();
				  	//\u5982\u679c\u662f\u5355\u9009\u6846\u6216\u590d\u9009\u6846\u65f6\u8bbe\u7f6e\u9009\u4e2d\u9879
		     	      if((tagtype == "CHECKBOX" || tagtype == "RADIO") && ctrTag.getAttribute("ctrlex") != null)
		     	      {  
	     	          //\u9009\u4e2d\u9879  
						  var ctlex = ctrTag.getAttribute('ctrlex');   	          
		     	          var select = evalParse(tableData[parseInt(i-1)],ctlex);
		     	          
		     	          if(select != 0 && select != null)//\u5982\u679c\u4e0d\u7b49\u4e8e0\u8868\u793a\u9009\u4e2d
		     	          {
		     	          	 ctrTag.setAttribute('checked','true');
		     	          }
	     	     	  }
				  }
     	      }//end else
			 
     	   }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
     	}   	
     	
     }//\u5217\u5faa\u73af
     
  }//\u884c\u5faa\u73af
  for(var i = 0; i < autoCtrId_arr.length; i ++)
  {
  	 autos=new AutoCompleteTag(autoCtrId_arr[i]);
     autos.init();
  }
  if(tableModel_arr[MtableId].vstatcaption)
  Vstatcaption_Processor(MtableId , tableData);
  //onFinished \u4e8b\u4ef6\u5904\u7406
  if(tableModel_arr[MtableId].onFinished){
  	parseFunction(tableModel_arr[MtableId].onFinished,tableModel_arr[MtableId].table);
  }  
};
function Vstatcaption_Processor(mid, tableData)
{
	var tr = addOneRow(mid);
	tr.className = "vstatcaption";
	var cells = tableModel_arr[mid].cells;
	var texttitle = tableModel_arr[mid].vstext;
	innerText_function(tr.cells[0],texttitle);
	tr.cells[0].className = "title";
	for(var i = 1; i < cells.length; i ++)
	{
		tr.cells[i].className = "data";
		var dd = cells[i].getAttribute(VSTATFORMULA);
		var tv = enumerate_value(cells[i],dd);
		if(cells[i].getAttribute("datatype") == "date")
		{
			var formate = tableModel_arr[mid].date_formate;
			tv = getTimefromUTC(formate,tv);
		}
		innerText_function(tr.cells[i],tv);
	}
	
	function enumerate_value(cell, vstat)
	{
		var rtn ="";
		var arr = new Array();
		for(var i = 0; i < tableData.length; i++)
		{
			var prop=cell.getAttribute("prop");//\u5f97\u5230column\u7684prop\u5c5e\u6027
     	    var value = eval("tableData["+parseInt(i)+"]."+prop);
     	    arr.push(value);
		}		
		switch(vstat.toUpperCase()){
			case "MAX":
			  arr.sort();
			  rtn = arr[arr.length - 1];
			  break;
			case "MIN":
			  arr.sort();
			  rtn = arr[0];
			  break;
			case "AVG":
			  var sum = 0;
			  for(var i=0;i<arr.length;i++)
			  {
			  	sum += arr[i];
			  }
			  rtn = sum/arr.length;
			  break;
			case "SUM":
			  var sum = 0;
			  for(var i=0;i<arr.length;i++)
			  {
			  	sum += arr[i];
			  }
			  rtn = sum;
			  break;     
			default:
			   break;  
		}
		return rtn;
    };
	
};

function column_ctrlex_Processor(rowcells,statusDesc, text)
{
    if(text == null) rowcells.innerHTML="";
    else if(statusDesc != null && statusDesc != '')
     {
        var _key_value_arrs = statusDesc.split(';');
        
        for(var i =0; i < _key_value_arrs.length; i ++)
        {
           var _key_value_maps = _key_value_arrs[i].split(':');
           if(_key_value_maps[0] != "")
           {
	           if(new String(text) == _key_value_maps[0])
	           {
	             if(_key_value_maps[2] != "")  innerText_function(rowcells, _key_value_maps[2]);
	             else innerText_function(rowcells, text);
	             if(_key_value_maps[1] != "")
	              rowcells.style.cssText="background:"+_key_value_maps[1];
	           }
	       }
	       else
	       {
	           alert("ctrlex \u5c5e\u6027\u914d\u7f6e\u9519\u8bef!");
	       }
        } 
     }
     else
     {     	     
 	    innerText_function(rowcells, text);
 	 }
};

function innerText_function(DOM_ctrl, text)
{
	if(isIE()) DOM_ctrl.innerText = text;
	else DOM_ctrl.textContent = text;
};

var _primary_key = 0;
getKey = function()
{
	return _primary_key = _primary_key+1;
};
/******************************
  *\u884c\u7f16\u8f91\u8868\u683c\u521d\u59cb\u5316\u51fd\u6570
  *MtableId \u8868\u683c\u6a21\u677fID
  *tableData \u884c\u5bf9\u8c61
 *****************************/
 create_new_EditTableRow = function(tableId,tb)
{
 var objTable = document.getElementById(tableId);//\u5185\u5b58\u8868
 var Mtable = document.getElementById(tb);
 var tdnodes = tableModel_arr[tb].cells;//\u6a21\u578b\u8868\u5217
 var objRow = objTable.insertRow(-1);	
 var autoCtrId_arr = new Array();
 this.primaryKey = getKey()+""+objRow.rowIndex;
	if(objRow.rowIndex % 2 == 0)
	 {
		objRow.className="even";
	 }
	 //\u5faa\u73af\u6a21\u578b\u8868\u683c
	 for(var j =0; j < tdnodes.length; j++)
     {
     
     	var contrlnode='';
     	var cell = objRow.insertCell(-1);
		//\u5f97\u5230column\u7684prop\u5c5e\u6027
       var prop=tableModel_arr[tb].props[j];
       
     	//\u5982\u679c\u7ed3\u70b9\u4e0d\u5305\u542b\u5b50\u7ed3\u70b9\u542b\u63a7\u4ef6  
     	if(tableModel_arr[tb].cells[j].td_Children.length == 0)
     	{ 
     	  if(prop == "$ROWNUM")
     	  {
     	      cell.innerHTML=objRow.rowIndex;
     	  }
     	}
     	else{//\u5305\u542b\u63a7\u4ef6
     	    //\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
     	   for(var m=0; m<tableModel_arr[tb].cells[j].td_Children.length; m++)
     	   {
     	     var ctrTag = tableModel_arr[tb].cells[j].td_Children[m].cloneNode(true);
     	     var tagName = ctrTag.tagName.toUpperCase();			 
     	   	 //\u5982\u679c\u5b50\u7ed3\u70b9\u4e2d\u6709Select\u6807\u7b7e
     	     if(tagName == 'SELECT')
     	     {
     	        //\u521b\u5efa\u4e0b\u62c9\u5217\u8868\u6846\u9009\u9879
     	     	var option = document.createElement('option');
     	     	option.value ='';
     	     	option.innerHTML ='';
     	        //\u52a0\u5165\u6807\u7b7e
     	     	cell.appendChild(ctrTag);
     	     }
			 else if(tagName == "CALENDAR" || tagName == "Z:CALENDAR")//\u5982\u679c\u662f\u65e5\u5386\u6807\u7b7e
			 {
			 	if(ctrTag.getAttribute("default") && ctrTag.getAttribute("default") != '')
			 	{
			 		var defaultProp = ctrTag.getAttribute("default");
			 		value = eval(defaultProp);
			 	}			 	
			 	var html = Calendar_Ctr_Processor(ctrTag,value,this.primaryKey,tb);			
				cell.innerHTML = html;
			 }
			 else if(tagName == "Z:SSB_RIA_LOV" || tagName == "SSB_RIA_LOV")
			 {
			 	var value = null;
			 	var new_ctrTag=table_Ctr_Processor(ctrTag,value,this.primaryKey,tb);
			 	cell.appendChild(new_ctrTag);
			 	readRiaLovTag(new_ctrTag);
			 }
			 else if(tagName == 'Z:AUTOCOMPLETE' || tagName == 'AUTOCOMPLETE')
			 {
			 	var value = null;
			 	var new_autoCtr = autoComplete_Ctr_Processor(ctrTag,value,this.primaryKey,tb);
			 	cell.appendChild(new_autoCtr);
			 	autoCtrId_arr.push(new_autoCtr.getAttribute('id'));			 	
			 }
			 else if(tagName == 'Z:RADIOGROUP' || tagName == 'RADIOGROUP')
		     {
		      	  var value = null;
		      	  var new_radioCtr = radioGroup_Ctr_Processor(ctrTag,value,this.primaryKey,tb);	
		      	  var nid = new_radioCtr.getAttribute('id') + this.primaryKey;
		      	  new_radioCtr.setAttribute('id', nid);
		      	  cell.appendChild(ctrTag);	      	  
		      	  radios=new RadioGroupTag(new_radioCtr.getAttribute('id'));
	              radios.init();                 
		     }
		     else if(tagName == 'Z:UPLOAD' || tagName == 'UPLOAD')
		     {
		     	var newCtrTag = upload_Ctr_Processor(ctrTag,value,this.primaryKey,tb);
		     	var nid = newCtrTag.getAttribute('id') + this.primaryKey;
		     	newCtrTag.setAttribute('id', nid);
		     	cell.appendChild(newCtrTag);		     	
		     	var upload = UploadTag(newCtrTag.getAttribute('id'));
		        upload.init();
		     }
     	     else//\u5176\u4ed6\u6807\u7b7e\u76f4\u63a5\u63d2\u5165
     	     {
     	     	  if(ctrTag.id != null) ctrTag.id = ctrTag.id+this.primaryKey;
     	     	  if(ctrTag.value != null ) ctrTag.value = '';
     	          cell.appendChild(ctrTag);
     	          addEventForCheckBox(ctrTag, tb);
     	          var checkctrl = document.getElementById(tb+"allCheck");   	          
     	          if(ctrTag.type != null && ctrTag.type.toLowerCase() == 'checkbox' && checkctrl && checkctrl.checked == true)
     	          {
     	          	ctrTag.checked = true;
     	          }
     	     }
     	 
     	   }//\u5faa\u73aftd\u91cc\u9762\u7684\u63a7\u4ef6
     	
     	}   	
     	
     }//\u5217\u5faa\u73af
  for(var i = 0; i < autoCtrId_arr.length; i ++)
  {
  	 autos=new AutoCompleteTag(autoCtrId_arr[i]);
     autos.init();
  } 
  return objRow;
};  

/*************************************
 * \u5206\u9875\u63a7\u4ef6
 * @param {Object} name
 * @author zhanghongwei 
 *************************************/
 showPages = function(name)
 { //\u521d\u59cb\u5316\u5c5e\u6027
        this.name = name;      //\u5bf9\u8c61\u540d\u79f0
        this.page = 1;         //\u5f53\u524d\u9875\u6570
        this.pageCount = 0;    //\u603b\u9875\u6570
        this.argName = 'page'; //\u53c2\u6570\u540d
        this.showTimes = 1;    //\u6253\u5370\u6b21\u6570
        this.perPageCount =10;	//\u6bcf\u9875\u663e\u793a\u6761\u6570
        this.perCount = 10;	//\u663e\u793a\u51fa\u7684\u8df3\u8f6c\u9875\u7801\u6570,\u5982[1][2][3][4][5][6][7][8][9][10]
        this.totalrow = 0;	
        this.turnTo ;
        this.tid;
        
        this.text1 = $res_entry('ui.control.table.001');
        this.text2 = $res_entry('ui.control.table.002');
        this.text3 = $res_entry('ui.control.table.003');
        this.text4 = $res_entry('ui.control.table.004');
        this.text5 = $res_entry('ui.control.table.005');
        this.text6 = $res_entry('ui.control.table.006');
        this.text7 = $res_entry('ui.control.table.007');
        this.text8 = $res_entry('ui.control.table.008');
        //this.pageSizeList = new Array();
};
/*************************************
 * \u83b7\u53d6\u8c03\u8f6c\u7684\u9875\u53f7
 ************************************/
showPages.prototype.getPage = function()
{ //\u4e1burl\u83b7\u5f97\u5f53\u524d\u9875\u6570,\u5982\u679c\u53d8\u91cf\u91cd\u590d\u53ea\u83b7\u53d6\u6700\u540e\u4e00\u4e2a
       // alert("this.turnTo:"+this.turnTo);
        this.page = this.turnTo;
};

/*************************************
 * \u8f6c\u6362\u9875\u7801
 ************************************/
showPages.prototype.checkPages = function()
{ //\u8fdb\u884c\u5f53\u524d\u9875\u6570\u548c\u603b\u9875\u6570\u7684\u9a8c\u8bc1
        if (isNaN(parseInt(this.page))) this.page = 1;
        if (isNaN(parseInt(this.pageCount))) this.pageCount = 1;
        if (this.page < 1) this.page = 1;
        if (this.pageCount < 1) this.pageCount = 0;
        if (this.page > this.pageCount) this.page = this.pageCount;
        this.page = parseInt(this.page);
        this.pageCount = parseInt(this.pageCount);
       
};

/*************************************
 * \u751f\u6210\u5206\u9875\u63a7\u4ef6
 * @param {Object} mode
 ************************************/
showPages.prototype.createHtml = function(mode)
{ //\u751f\u6210html\u4ee3\u7801
       
        var strHtml = '';
        var prevPage = this.page - 1;//\u524d\u4e00\u9875
        var nextPage = this.page + 1;// \u4e0b\u4e00\u9875
        var isdisabled = "";
        if( this.totalrow == 0)
        {
        	this.page = 0;
        	isdisabled="disabled"
        }
             
        var ml = this.getMultilanguage_resource();      
      
           strHtml += ml[0]+'<span>' + this.page + '</span>'+ml[1]+'<span>/</span> '+ml[2]+'<span id="ssb_ria_table_pageCount">' + this.pageCount + '</span>'+ml[3];
           strHtml += '&nbsp;&nbsp;&nbsp;'+ml[4]+'<span id="ssb_ria_table_totalrow">'+this.totalrow+'</span>'+ml[5]+'&#160; ';
           strHtml += ml[6];
           strHtml += '<select name="perPageCount" onchange="tableModel_arr.'+this.tid+'.'+ this.name +'.initPre(this);"'+isdisabled+'>';
           			for (var i = 0; i < tableModel_arr[this.tid].pageSizeList.length; i++)
           			 {
           			      //\u5224\u65ad\u5f53\u524d\u6bcf\u9875\u591a\u5c11\u884c
           			      var chkSelect;
                           if (tableModel_arr[this.tid].pg.perPageCount == tableModel_arr[this.tid].pageSizeList[i])
                           { 
                          	 chkSelect=' selected="selected"';
                           }
                           else {
                          	 chkSelect='';
                           }
                           strHtml += '<option value="' + tableModel_arr[this.tid].pageSizeList[i] + '"' + chkSelect + '>' + tableModel_arr[this.tid].pageSizeList[i] + '</option>';
                    }
           strHtml += '</select> '+ml[7]+'&#160;&nbsp;&nbsp;';
           if (prevPage < 1) {
                   strHtml += '<span ><input class="pageNav firstPageD" disabled type=button></span>';
                   strHtml += '<span ><input class="pageNav prevPageD" disabled type=button></span>';
                  
           } else {
                   strHtml += '<span ><input class="pageNav firstPage" type=button onclick="javascript:tableModel_arr.'+this.tid+'.'+ this.name + '.toPage(1);"></span>';
                   strHtml += '<span ><input class="pageNav prevPage" type=button onclick="javascript:tableModel_arr.'+this.tid+'.'+ this.name + '.toPage(' + prevPage + ');"></span>';
           }
           
           if (this.pageCount < 1) {
                   strHtml += '<select name="toPage" disabled="disabled">';
                   strHtml += '<option value="0">0</option>';
           } else {
                   var chkSelect;
                   strHtml += '<select name="toPage" onchange="tableModel_arr.'+this.tid+'.'+ this.name + '.toPage(this);">';
                   for (var i = 1; i <= this.pageCount; i++) {
                           if (this.page == i) chkSelect=' selected="selected"';
                           else chkSelect='';
                           strHtml += '<option value="' + i + '"' + chkSelect + '>' + i + '</option>';
                   }
           }
           strHtml += '</select>';
          
           if (nextPage > this.pageCount) {
                   strHtml += '<span ><input class="pageNav nextPageD" disabled type=button></span>';
                   strHtml += '<span ><input class="pageNav lastPageD" disabled type=button></span>';
           } else {
                   strHtml += '<span ><input class="pageNav nextPage"  type=button onclick="javascript:tableModel_arr.'+this.tid+'.'+ this.name + '.toPage(' + nextPage + ');"></span>';
                   strHtml += '<span ><input class="pageNav lastPage"  type=button onclick="javascript:tableModel_arr.'+this.tid+'.'+ this.name + '.toPage(' + this.pageCount + ');"></span>';
           }

           if(tableModel_arr[this.tid].isExcel == 'true')
           {
           	    if(tableModel_arr[this.tid].isExcelConfig == 'true')
           	    {
           	    	strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:configExcel('+this.tid+')"></span>';
           	    }else{
           	    	strHtml += '<span ><input class="pageNav exportXls"  type=button onclick="javascript:ExportExcel('+this.tid+')"></span>';
           	    }
           }
           strHtml += '';
        return strHtml;
};

/*************************************
 * \u751f\u6210\u9875\u9762\u8df3\u8f6curl
 * @param {Object} page
 ************************************/
showPages.prototype.createUrl = function (page)
 { 
 		//\u751f\u6210\u9875\u9762\u8df3\u8f6curl
        if (isNaN(parseInt(page))) page = 1;
        if (page < 1) page = 1;
        if (page > this.pageCount) page = this.pageCount;
        var url = location.protocol + '//' + location.host + location.pathname;
        var args = location.search;
        var reg = new RegExp('([\?&]?)' + this.argName + '=[^&]*[&$]?', 'gi');
        args = args.replace(reg,'$1');
        if (args == '' || args == null) {
                args += '?' + this.argName + '=' + page;
        } else if (args.substr(args.length - 1,1) == '?' || args.substr(args.length - 1,1) == '&') {
                        args += this.argName + '=' + page;
        } else {
                        args += '&' + this.argName + '=' + page;
        }
        return url + args;
};

/**************************************
 * \u524d\u540e\u7ffb\u9875\u8c03\u8f6c
 * @param {Object} page
 **************************************/
showPages.prototype.toPage = function(page)
{ 
		//\u9875\u9762\u8df3\u8f6c       
        this.turnTo = 1;
        if (typeof(page) == 'object') {
                this.turnTo = page.options[page.selectedIndex].value;
        } else {
                this.turnTo = page;
        }
       //\u7ffb\u9875
       getTurnPageData(this.tid, tableModel_arr[this.tid].condition, this.turnTo, this.perPageCount); 
    
};

/*************************************
 * \u6253\u5370\u5206\u9875\u63a7\u4ef6
 * @param {Object} mode
 *************************************/
showPages.prototype.printHtml = function(mode)
{ 
       //\u663e\u793ahtml\u4ee3\u7801        
        this.getPageCount();//\u8ba1\u7b97\u603b\u9875\u6570        
        this.getPage();//\u83b7\u53d6\u5f53\u524d\u9875\u7801
        this.checkPages();//\u68c0\u6d4b\u9875\u7801\u5408\u6cd5\u6027
        this.showTimes += 1;  
       //\u9875\u7801\u9009\u62e9\u6846      
     //\u83b7\u53d6\u5206\u9875\u680f\u7684table\u5bf9\u8c61
     var dir_tr_td = document.getElementById(this.tid+'_pages');  
     //\u521b\u5efa\u5206\u9875
     dir_tr_td.innerHTML = this.createHtml();
      
};

/**********************************
 * \u683c\u5f0f\u5316\u9875\u7801
 * @param {Object} e
 ***********************************/
showPages.prototype.formatInputPage = function(e)
{ //\u9650\u5b9a\u8f93\u5165\u9875\u6570\u683c\u5f0f
        var ie = navigator.appName=="Microsoft Internet Explorer"?true:false;
        if(!ie) var key = e.which;
        else var key = event.keyCode;
        if (key == 8 || key == 46 || (key >= 48 && key <= 57)) return true;
        return false;
};

/******************************
 * \u521d\u59cb\u8bbe\u5b9a\u6bcf\u9875\u663e\u793a\u884c\u6570
 * @param {Object} e
 ******************************/
showPages.prototype.initPre = function(e)
{ //\u8bbe\u5b9a\u6bcf\u9875\u663e\u793a\u884c\u6570     
     if(e != null)
     {
     	if(e.value == "\u5168\u90e8" || e.value.toLowerCase() == "all")
     	 this.perPageCount = this.totalrow;
     	else 
     	 this.perPageCount = e.value;
     }
     else{
         this.perPageCount = 10; //\u6ca1\u9875\u591a\u5c11\u884c
     }
     
	 
    tableModel_arr[this.tid].pg.perPageCount = this.perPageCount;
	
     this.getPageCount();
     //\u5982\u679c\u662f\u521d\u59cb\u8868\u683c, \u4e0d\u663e\u793a\u6570\u636e
     if(this.totalrow != 0)
     {  
	     //\u91cd\u7f6e\u6bcf\u9875\u663e\u793a\u6570\u636e\u6570\u540e,\u91cd\u65b0\u67e5\u8be2\u6570\u636e
	    getTurnPageData(this.tid, tableModel_arr[this.tid].condition, '1', this.perPageCount);
	     // \u586b\u6570\u636e
	    // setTableValue(tableModel_arr[this.tid].MtableId, pageinfo);
	 }
    
};
showPages.prototype.getMultilanguage_resource = function()
{
     var res_entity = new Array();
     text1 = this.text1.indexOf("$") == -1?this.text1:"\u7b2c";
     text2 = this.text2.indexOf("$") == -1?this.text2:"\u9875";
     text3 = this.text3.indexOf("$") == -1?this.text3:"\u5171";
     text4 = this.text4.indexOf("$") == -1?this.text4:"\u9875";
     text5 = this.text5.indexOf("$") == -1?this.text5:"\u5171";
     text6 = this.text6.indexOf("$") == -1?this.text6:"\u6761\u8bb0\u5f55";
     text7 = this.text7.indexOf("$") == -1?this.text7:"\u6bcf\u9875";
     text8 = this.text8.indexOf("$") == -1?this.text8:"\u884c";
     res_entity.push(text1);
     res_entity.push(text2);
     res_entity.push(text3);
     res_entity.push(text4);
     res_entity.push(text5);
     res_entity.push(text6);
     res_entity.push(text7);
     res_entity.push(text8);
     
     return res_entity;
};
/**********************************
 * \u8ba1\u7b97\u603b\u5171\u591a\u5c11\u9875
 * @param {Object} totalRow
 * @param {Object} perPage
 **********************************/
 showPages.prototype.getPageCount = function()//\u8ba1\u7b97\u9875\u7801
{
     this.pageCount = 0;
     //\u5982\u679c\u65e0\u8bb0\u5f55,\u76f4\u63a5\u8fd4\u56de0
     if(this.totalrow == 0)
     {
     	return this.pageCount;
     }
     else{
     	if(this.totalrow%this.perPageCount == 0)
     	{
     		this.pageCount = parseInt(this.totalrow/this.perPageCount);
     	}
     	else{
     		this.pageCount = parseInt(this.totalrow/this.perPageCount + 1);
     	}     	
     }
};

/**************************
 * \u53bb\u9664\u7a7a\u683c
 * @param {Object} str
 **************************/
 LTrim = function(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
};
 RTrim = function(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};
 Trim = function(str) 
{
	return RTrim(LTrim(str));
};

/*************************************
 * \u5224\u65ad\u6d4f\u89c8\u5668\u7c7b\u578b
 ************************************/
 isIE = function()
{
	browser_name = navigator.appName;
	if (browser_name == "Microsoft Internet Explorer") 
	{
			return true;
	}
	return false;
};


/*************************************
 * @method \u52a8\u6001\u7684\u5217\u5bbd\u8c03\u6574
 * @author zhanghongwei
 ************************************/
var table_Header_Resize = new Object();

var TableContants={	
	EC_ID : "zteTable_ssb",
	MIN_COL_WIDTH : 10,
	MIN_COLWIDTH : "30",	
	DRAG_BUTTON_COLOR : "#3366ff",
	IE_WIDTH_FIX_A : 1,
	IE_WIDTH_FIX_B : 2,
	FF_WIDTH_FIX_A : -3,
	FF_WIDTH_FIX_B : -6

};

table_Header_Resize.onDragobj=false;
table_Header_Resize.Dragobj=null; 
table_Header_Resize.DragobjSibling=null;
table_Header_Resize.MinColWidth=TableContants.MIN_COLWIDTH;

table_Header_Resize.DragobjBodyCell=null;
table_Header_Resize.DragobjBodyCellSibling=null;
table_Header_Resize.DragFormid=null;

/*************************************
 * 
 * @param {Object} event
 * @param {Object} obj
 * @param {Object} formid
 ************************************/
table_Header_Resize.StartResize=function(event,obj,formid)
{
    var table = document.getElementById(formid);
	var e = event||window.event;
	if (!formid)
	{
		formid=TableContants.EC_ID;
	}
	
	obj.focus();
	document.body.style.cursor = E_RESIZE;
	var sibling = table_Header_Resize.getNextElement(obj.parentNode);

	var dx=e.screenX;

	obj.parentTdW=obj.parentNode.clientWidth;
	if(sibling == null )
	{ 
		return true;
		document.body.style.cursor = "";
	}
	obj.siblingW = sibling.clientWidth;
	obj.mouseDownX=dx;
	obj.totalWidth = obj.siblingW + obj.parentTdW;

	obj.oldSiblingRight=table_Header_Resize.getPosRight(sibling);

    table_Header_Resize.Dragobj=obj;
	table_Header_Resize.DragobjSibling=sibling;
	table_Header_Resize.onDragobj=true;

    table_Header_Resize.MinColWidth=TableContants.MIN_COL_WIDTH;

	if (!table_Header_Resize.MinColWidth||table_Header_Resize.MinColWidth=='' || table_Header_Resize.MinColWidth<1)
	{
		table_Header_Resize.MinColWidth=TableContants.MIN_COLWIDTH;
	}
	 table_Header_Resize.Dragobj.style.backgroundColor=TableContants.DRAG_BUTTON_COLOR;

	 table_Header_Resize.Dragobj.parentTdW-=table_Header_Resize.Dragobj.mouseDownX;

	 var cellIndex=table_Header_Resize.Dragobj.parentNode.cellIndex;
	 
	try{
     table_Header_Resize.DragobjBodyCell=table.rows[0].cells[cellIndex];	 
	 table_Header_Resize.DragobjBodyCellSibling=table_Header_Resize.getNextElement(table_Header_Resize.DragobjBodyCell);
	}
	catch(e){
		table_Header_Resize.DragobjBodyCell=null;
	}
	 
};

/*************************************
 * 
 * @param {Object} event
 ************************************/
table_Header_Resize.DoResize=function(event)
{
	var e = event||window.event;

	if(table_Header_Resize.Dragobj==null)
	{
        return true;
	}
    if(!table_Header_Resize.Dragobj.mouseDownX)
    {
        return false;
	}
	document.body.style.cursor = E_RESIZE;

	var dx=e.screenX;

	var newWidth=table_Header_Resize.Dragobj.parentTdW+dx;

	var newSiblingWidth=0; 

	/* fix different from ie to ff . but I don't know why  */
	if (isIE())
	{
		newWidth=newWidth+TableContants.IE_WIDTH_FIX_A;			
		newSiblingWidth=table_Header_Resize.Dragobj.totalWidth-newWidth+TableContants.IE_WIDTH_FIX_B; 
	}
	else{
		newWidth= newWidth+ TableContants.FF_WIDTH_FIX_A;			
		newSiblingWidth=table_Header_Resize.Dragobj.totalWidth-newWidth + TableContants.FF_WIDTH_FIX_B; 
	}
	if(newWidth>table_Header_Resize.MinColWidth && newSiblingWidth>table_Header_Resize.MinColWidth)
	 {
        table_Header_Resize.Dragobj.parentNode.style.width = newWidth+"px";
		table_Header_Resize.DragobjSibling.style.width = newSiblingWidth+"px";
		try{
			table_Header_Resize.DragobjBodyCell.style.width = newWidth+"px";
			table_Header_Resize.DragobjBodyCellSibling.style.width = newSiblingWidth+"px";
			table_Header_Resize.DragobjBodyCell.width = newWidth+"px";
			table_Header_Resize.DragobjBodyCellSibling.width= newSiblingWidth+"px";
		}catch(e){}
    }
	table_Header_Resize.onDragobj=true;

};

/*************************************
 * 
 * @param {Object} event
 ************************************/
table_Header_Resize.EndResize=function(event)
{	
	if(table_Header_Resize.Dragobj==null)
	{
        return false;
	}
    table_Header_Resize.Dragobj.mouseDownX=0;
	document.body.style.cursor = "";
	table_Header_Resize.Dragobj.style.backgroundColor="";
	table_Header_Resize.Dragobj=null; 
	table_Header_Resize.DragobjSibling=null;

};
/*************************************
 * 
 ************************************/
table_Header_Resize.resizeInit=function()
{	
	document.onmousemove = table_Header_Resize.DoResize;
	document.onmouseup = table_Header_Resize.EndResize;
	document.body.ondrag = function()
	{
		return false;
	};
    document.body.onselectstart = function() 
    {
		return table_Header_Resize.Dragobj==null;
	};
	 
};

/*************************************
 * 
 * @param {Object} node
 ************************************/
table_Header_Resize.getNextElement=function(node)
{
	if (!node)
	{
		return null;
	}
	var tnode=node.nextSibling;
	while ( tnode!=null )
	{
		if (tnode.nodeType==1) 
		{
			return tnode;
		}
		tnode=tnode.nextSibling;
	}
	return null;
};

/*************************************
 * 
 * @param {Object} elm
 ************************************/
table_Header_Resize.getPosLeft=function(elm) 
{
	var left = elm.offsetLeft;
	while((elm = elm.offsetParent) != null)	
	{
		left += elm.offsetLeft;
	}
	return left;
};
/*************************************
 * 
 * @param {Object} elm
 ************************************/
table_Header_Resize.getPosRight=function(elm)
{
	return table_Header_Resize.getPosLeft(elm)+elm.offsetWidth;
};

/*************************************
 * \u6570\u636e\u7c7b\u578b\u8f6c\u6362
 * @param {Object} sValue
 * @author zhanghongwei 
 ************************************/
 
  convert = function(sValue, datatype)
 {
 	var re = /[^0-9.+-]/g;    
 	value=(sValue.nodeName == "#text")?sValue.nodeValue:(sValue.value == null?sValue.firstChild.data:sValue.value);
 	if(value)
 	{
	    datatype = (datatype == null)? "":datatype;
		switch(datatype.toLowerCase())
		{
			case "int":
			    rvalue = value.replace(re,'');
				return parseInt(rvalue);
			case "float":
				return parseFloat(value);		
			default:
				return value.toString();
		}
 	}

  };
	
/*************************************
 * \u751f\u6210\u6bd4\u8f83\u51fd\u6570
 * @param {Object} iCol
 ************************************/
	
	generateCompare = function(iCol,datatype)
	{
		return function compareTRs(oTR1,oTR2)
		{
            var value1 = "";
			var value2 = "";
			if(oTR1.cells[iCol].firstChild != null)
			{
				value1 = convert(oTR1.cells[iCol].firstChild, datatype);				
			}
			if(oTR2.cells[iCol].firstChild != null)
			{
				value2 = convert(oTR2.cells[iCol].firstChild, datatype);
			}
			if(value1 < value2)
			{
				return -1;
			}
			else if(value1 > value2)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		};
	};
	
	/************************************
	 * \u6392\u5e8f\u51fd\u6570
	 * @param {Object} sTableId
	 * @param {Object} iCol
	 ************************************/
	
	sortTable = function(sTableId, iCol, imgEl)
	{		
	  
		if(typeof sTableId == 'object')
		{
			var oTable = sTableId;
			var tableId = sTableId.id;
		}
		else{
			var oTable = document.getElementById(sTableId);
			var tableId = sTableId;
		}
		tableId = tableId.substring(0, tableId.indexOf("_ssb"));
		var oTBody = oTable.tBodies[0];
		var oRows = oTBody.rows;
		if(oRows.length == 1) return;
		var aTRs = new Array();
		var sortASC  = null;
		var sortDESC = null;
		if(isIE())
		{
			sortASC  = "IEsortASC";
			sortDESC = "IEsortDESC";
		}else{
			sortASC  = "FFsortASC";
			sortDESC = "FFsortDESC";
		}
		
		for(var i = 1; i < oRows.length; i++)
		{
			aTRs.push(oRows[i]);
		}
		
		if(oTable.sortCol == iCol)
		{
			aTRs.reverse();	
			if(oTable.sortflag == 'down')
			{
				oTable.sortflag = 'up';
				imgEl.className=sortASC;
			}
			else{
				oTable.sortflag = 'down';
				imgEl.className=sortDESC;
			}		
			
		}
		else{
			aTRs.sort(generateCompare(iCol, tableModel_arr[tableId].cells[iCol].datatype));
			oTable.sortflag = 'down';
			imgEl.className=sortDESC;
		}
		
		var oFragment = document.createDocumentFragment();
		oFragment.appendChild(oRows[0]);
		for(var i = 0; i < aTRs.length; i++)
		{
			setRowCss(aTRs[i],ROW_CLASS_EVEN,i);			
			oFragment.appendChild(aTRs[i]);
		}
		
		oTBody.appendChild(oFragment);
		oTable.sortCol = iCol;		
		
	};
	
   /************************************
	 * \u521d\u59cb\u5316\u51fd\u6570\uff0c\u7531RIA\u6846\u67b6\u8d1f\u8d23\u8c03\u7528
	 * \u521d\u59cb\u5316\u9875\u9762\u4e0a\u6240\u6709\u7684z:table\u6807\u7b7e
	 ************************************/
   SSB_Table_Init = function()
   {
   	  //\u83b7\u53d6\u9875\u9762\u6240\u6709\u7684table\u6807\u7b7e
   	  if(isIE())
   	  {
   	  	var IEtables = document.getElementsByTagName("table");
   	  	for(var i = 0; i < IEtables.length; i ++)
   	  	{
   	  		 //\u5982\u679ctable\u7684\u547d\u540d\u7a7a\u95f4\u662fz
   	  		if(IEtables[i].scopeName.toUpperCase() == RIA_SCOPENAME)
   	  		{
   	  			var IEid = IEtables[i].id;
   	  			//alert("IE:"+IEid);
   	  			if(IEid != null)
   	  			{
   	  			   init_ssb_table(IEid);
   	  			}
   	  		}
   	  	}
   	  }
   	  else{//\u5728FF\u4e0b
   	  	var FFtables = document.getElementsByTagName("z:table");
   	  	for(var j = 0; j < FFtables.length; j ++)
   	  	{
   	  		var FFid = FFtables[j].getAttribute("id");
   	  		if(FFid != null)
   	  		{
   	  			init_ssb_table(FFid);
   	  		}
   	  	}
   	  }
   	  
   };
   var evalParse = function ()
		{
			var str = eval("arguments[0][arguments[1]]");
		if(str == undefined) str = "";
			return str;
		};
   //\u8c03\u7528\u6846\u67b6\u521d\u59cb\u65b9\u6cd5
   addOnloadEvent(SSB_Table_Init); 
 /**********************************
  * @ Excel \u6587\u4ef6\u5bfc\u51fa\u670d\u52a1
  * @author zhanghongwei
  * @throws
  * @return
  *********************************/	
  var cap = new Array();//excel\u6587\u4ef6\u5217\u6807\u9898
  var pop = new Array();//\u5b9e\u4f53\u5c5e\u6027
  var setCaptions = function(captions)
  {
  	cap = captions;
  };
  var setProps = function(props)
  {
  	pop = props;
  };
  var getCaptions = function(tid)
  {
  	 var captions = tableModel_arr[tid].excel_title;
  	 return captions;
  };
  var getProps = function(tid)
  {
  	 //\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
	  var props = tableModel_arr[tid].excel_props;	
	  return props;
  };
  var configExcel = function(tid)
  {
  	if(typeof tid == "object")
   	 {
   	 	tid = tid.id;
   	 }
  	 var configpage = document.open('ExcelConfig.html?tid='+tid,'_blank','toolbar=0,location=0,direct=0,menubar=0,scrollbars=1, width=500,height=500'); 
  };
  //\u5bfc\u51fa\u662f\u521b\u5efa\u8868\u5355\u548c\u63d0\u4ea4\u8868\u5355
   var ExportExcel = function(tid)
   { 
   	 if(typeof tid == "object")
   	 {
   	 	tid = tid.id;
   	 }
	   var table = getTableById(tid);
     //\u5982\u679c\u8868\u683c\u67e5\u51fa\u7684\u6570\u636e\u5927\u5c0f\u4e0d\u4e3a\u7a7a\uff0c\u5219\u53ef\u4ee5\u5bfc\u51fa\u6587\u4ef6
     if(table.allcount != null && table.allcount != 0)
     {
       var condition = tableModel_arr[tid].condition;//\u67e5\u8be2\u6570\u636e\u7684\u6761\u4ef6
	     //var cc = getBindObject(condition);//\u83b7\u53d6\u67e5\u8be2\u6761\u4ef6
	     var sid = tableModel_arr[tid].sid;//table\u7684sid
	     var url = document.getElementById(sid).getAttribute('url');//\u83b7\u53d6z:sevice \u6807\u7b7e\u7684URL
	     var arr = url.split(".");
	     var action = arr[0]+".xls";
	     var form = document.getElementById('excel');
	     form = null;
	     if(form == null)
	     {
		     var form = document.createElement('form');
		     document.body.appendChild(form);
		     form.style.display="none";
		     form.action = action;
		     form.id = 'excel';
		     form.method = 'post';
		     if(tableModel_arr[tid].isExcelConfig == 'true')
		     {
		     	//\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
		     	var props = pop;	
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
				var captions = cap;
		     }else{
		     	//\u8bbe\u7f6e\u6a21\u578b\u5bf9\u8c61\u7684\u5c5e\u6027
		     	var props = tableModel_arr[tid].props;	
				//\u8bbe\u7f6e\u6a21\u578b\u8868\u5f97\u5217\u540d
				var captions = tableModel_arr[tid].caption;
		     }
		     var con = condition.toJSONString();
		     var json = con.substring(0,con.lastIndexOf("]"))+",0"+","+new String(table.allcount)+"]";
		     var input = document.createElement("input");
		     form.appendChild(input);
		     input.name  = "condition";
		     input.value = json;
			
		     var input = document.createElement("input");
		     form.appendChild(input);
		     input.name  = "tableID";
		     input.value = tid;
		     
		     for(var i = 0; i < props.length; i ++)
		     {
		         var prop = document.createElement("input");
			     form.appendChild(prop);
			     prop.type = "text";
			     prop.name = "prop";
			     prop.value = props[i];
			     
			     var caption = document.createElement("input");
			     form.appendChild(caption);
			     caption.type = "text";
			     caption.name = "caption";
			     caption.value = captions[i];
			 }    
		 }    
	      form.submit();
     }     
   };
 //\u521d\u59cb\u5316\u914d\u7f6e\u9875\u9762   
 var initExcel = function()
 {   
 	window.onblur=function(){
 		window.focus();
 	};
 	var table = document.getElementById('config');
    var url = document.location.toString();
    var cookieID = window.opener.location.pathname.toString();
    var arr = url.split('=');
    var tid = arr[1];
    var tid_input = document.createElement("input");
    tid_input.type  = "hidden";
    tid_input.id    = "tid";
    tid_input.value = tid;
    document.body.appendChild(tid_input);    
    var captions = window.opener.getCaptions(tid);
    var props    = window.opener.getProps(tid); 	
 	var prop  = getCookie(cookieID+"prop");
 	for(var i = 0; i < captions.length; i ++)
 	{
 		var row = table.insertRow(-1); 		
 		var input = document.createElement('input');
 		input.type = "checkbox";
 		input.name = "name";
 		input.value = props[i];  		
 		row.insertCell(-1).appendChild(input); 
		setRowCss(row, ROW_CLASS_EVEN);
		innerText_function(row.insertCell(-1),captions[i]);
		innerText_function(row.insertCell(-1),props[i]);

 		if(prop != null)
 		{
	 		var offset = prop.indexOf(props[i]);
	 		if(offset != -1)input.checked = "true";
 		} 				 		
 	}
 };
 var allcheck = function(o)
 {
    var table = document.getElementById('config');
 	var rows = table.rows;
 	for(var i = 1; i< rows.length; i++)
 	{
 	    var input = rows[i].getElementsByTagName('input');
 		input[0].checked = o.checked;
 	}
 };
 var next = function()
 {
   var props    = new Array();
   var captions = new Array();
   var caption  = '';
   var prop     = '';
   var table = document.getElementById('config');
   var rows = table.rows;
   for(var i = 1; i< rows.length; i++)
   {
        var input = rows[i].getElementsByTagName('input');
        if(input[0].checked == true)
        {
            caption += ";";
 	    	prop += ";";
        	var cells = rows[i].cells;
        	if(isIE())
        	{
        	 caption += cells[1].innerText; 	    	
 	    	 prop += cells[2].innerText;
 	    	 captions.push(cells[1].innerText);
 	    	 props.push(cells[2].innerText); 
        	}else{
 	    	 caption += cells[1].textContent; 	    	
 	    	 prop += cells[2].textContent;
 	    	 captions.push(cells[1].textContent);
 	    	 props.push(cells[2].textContent); 
 	    	} 	    		    	
        } 	    
   }
   var cookieID = window.opener.location.pathname.toString();
   var utc = Date.UTC(2010,12,30);
   var date = new Date(utc);
   document.cookie = cookieID+"prop="+escape(prop)+";expires="+date.toGMTString();
   var tid = document.getElementById("tid").value;
   window.opener.setCaptions(captions);
   window.opener.setProps(props);
   window.close();
   if(tid != '' && tid != null)
   {
   	  window.opener.ExportExcel(tid);
   }
 };
 var getCookie = function(cookie_name)
{
	var allcookies = document.cookie;
	var cookie_pos = allcookies.indexOf(cookie_name);	
	if (cookie_pos != -1)
	{
		cookie_pos += cookie_name.length + 1;
		var cookie_end = allcookies.indexOf(";", cookie_pos-1);		
		if (cookie_end == -1)
		{
			cookie_end = allcookies.length;
		}
		var value = unescape(allcookies.substring(cookie_pos, cookie_end));
    }
	return value;
}; 
function uploadFile(o,sid,field,anotherField)
{			    
    var tr = o;
	//wanghao
    while(tr.tagName.toUpperCase() != 'TR')
    {
    	tr = tr.parentNode;
    	if(tr.tagName.toUpperCase() == 'BODY')
    	{
    		break;
    	}
    }
    var obj = {};
    var list = new Array();
    obj.field  = "";
    obj.anotherField = "";
    if(field != null)  obj.field  = field;
    if(anotherField != null)  obj.anotherField = anotherField;  
    var filepath = "";
    var desc = "uploadfile";
    var arr_upload = null;
    
    if(isIE())
    {
       arr_upload = tr.getElementsByTagName("upload");			      
    }
    else{
      arr_upload = tr.getElementsByTagName("z:upload");			     
    }
	
	//\u4fee\u6539\u8bb0\u5f552: wanghao
	if(arr_upload.length<=0)
	{
   		arr_upload = [];
   		arr_upload.push(tr);
   	}
	
	//wanghao
	__file_List = [];
   
    for(var i = 0; i < arr_upload.length; i ++)
    {
       var input_upload = arr_upload[i].getElementsByTagName("input");
       for(var i = 0; i < input_upload.length; i++)
       {
          if(input_upload[i].type.toUpperCase() == 'FILE')
          {
          	filepath = input_upload[i].value;
			//wanghao
        	__file_List.push(input_upload[i]);
          }
       }
    
	    list.push(filepath);
	    list.push('upload');
	    list.push(filepath);
	    obj.uploadtest = list;
	    
		//wanghao
	    callSid(sid, obj);
    }
};
function SSB_SingleUpload(ctrlId){
	this.ctrlId=ctrlId;
	this.labelObj=document.getElementById(ctrlId)||{};
	var that=this;
	this.labelObj.doUpload=function(paraMap,url,contrlObj){
		that.doUpload(paraMap,url,contrlObj);
	};
	this.text="\u6d4f\u89c8...";
	this.desc="";
	this.maxfiles=-1;
	this.count=0;
	this.onaddfile=null;
};
SSB_SingleUpload.prototype={
	initCtrl:function( ){
		var html=[];
		var ctrlId=this.ctrlId;
		html.push('<input id="fake_'+this.ctrlId+'" class="filePath" title="'+this.desc+
				'"><div style="display:inline" id="div_'+ctrlId+'"><input class=fakefile type=button value="'+
				this.text+'"></div>');
		this.labelObj.innerHTML=html.join(' ');
	},
	addCtrl:function( ){
		var oInput=this.getFileCtrl();
		//wanghao
		__file_List = [];
		
		//\u4fee\u6539\u8bb0\u5f55 \u5f20\u5b8f\u4f1f
		__file_List.push(oInput);
		oInput.className='multi_input';
		document.getElementById('div_'+this.ctrlId).appendChild(oInput);
	},
	getFileCtrl:function( ){
		var oInput=document.createElement('INPUT');
		var that=this;
		oInput.id = "upload_"+this.ctrlId;
		oInput.name = "upload_"+this.ctrlId;
		oInput.type = "file";
		oInput.hideFocus = true;
		oInput.size = 1;
		oInput['onchange']=function(event){
			that.addUpload(event||window.event);
		};
		return oInput;
	},
	init:function( ){
		this.initCtrl( );
		this.addCtrl( );
		SSB_Upload.uploads[this.ctrlId]=this;
	},
	addUpload:function(event){
		var ctrlId=this.ctrlId;
		function setValue(value){
			document.getElementById('fake_'+ctrlId).value=value;
		};
		var onaddfile=this.onaddfile;
		var ok=false;
		var obj=event.srcElement||event.target;
		if(onaddfile==''||!onaddfile){
			setValue(obj.value);
			return;
		}
		if(onaddfile.indexOf('this') != -1){
			var o = document.getElementById(ctrlId);
			ok = parseFunction(onaddfile,o);			
		}		
		else{
		ok=eval(onaddfile);
		}
		if(ok){
			setValue(obj.value);
			return;
		}else{
			obj.parentNode.removeChild(obj);
			this.addCtrl();
		}
	},
	delUpload:function(index){

	},
	setText:function(text){
		this.text=text;
	},
	setDesc:function(desc){
		this.desc=desc||this.desc;
	},
	setMaxFiles:function(){

	},
	setOnaddfile:function(onaddfile){
		this.onaddfile=onaddfile;
	},
	clean:function(){
		this.init();
	}
};

var _imgMap={};
	_imgMap["css"] = _imgMap["txt"] = _imgMap["js"] = "text";
	_imgMap["html"] = _imgMap["htm"] = _imgMap["shtml"] = _imgMap["hta"] = "ie";
	_imgMap["xml"] = _imgMap["xsl"] = "xml";
	_imgMap["pdf"] = "pdf";
	_imgMap["mdb"] = "msaccess";
	_imgMap["doc"] = "msword";
	_imgMap["xls"] = "msexcel";
	_imgMap["ppt"] = "mspowerpoint";
	_imgMap["exe"] = _imgMap["dll"] = _imgMap["sys"] = _imgMap["bat"] = "system";
	_imgMap["zip"] = _imgMap["rar"] = _imgMap["gzip"] = _imgMap["jar"] = _imgMap["lzh"] = "archive";
	_imgMap["ini"] = _imgMap["inf"] = "conf";
	_imgMap["csv"] = "csv";
	_imgMap["psd"] = _imgMap["jpg"] = _imgMap["gif"] = "image";
	_imgMap["ra"] = _imgMap["rm"] = _imgMap["rmvb"] = _imgMap["mp3"] = _imgMap["wma"] = _imgMap["avi"] = "media";
	
function SSB_MultiUpload(ctrlId){
	SSB_SingleUpload.call(this,ctrlId);
	this.url='';
	this.uploading=false;
	this.count=0;  //\u4e0a\u4f20\u6587\u4ef6\u5217\u8868\u957f\u5ea6
	this.currentInput={};
};
SSB_MultiUpload.prototype=new SSB_SingleUpload();
SSB_MultiUpload.prototype.constructor=SSB_MultiUpload;
SSB_MultiUpload.prototype.iconMap=_imgMap;

SSB_MultiUpload.prototype.initCtrl=function(){
	var ctrlId=this.ctrlId;
	var a= '<a hidefocus="" id="btn_'+ctrlId+'"><input class=button type=button value="'+this.text+'"></a>&nbsp;'+
				'<div class="font_notice addfile_notice" style="display:inline" id="notice_'+ctrlId+'">'+this.desc+'</div>' +
		   '<table id="tab_'+ctrlId+'" class="filelist" cellSpacing=0 cellPadding=0>' +
				'<THEAD><TR class="filelist_title"><TH class="path">\u6587\u4ef6\u8def\u5f84</TH><TH class="operate">\u64cd\u4f5c</TH></TR>' +
				'</THEAD>' +
				'<TBODY id="upload_list_'+ctrlId+'">' +
					'<tr id="no_upload_'+ctrlId+'" class="nofile_notice">' +
						'<td colSpan=2>\u5c1a\u672a\u6dfb\u52a0\u6587\u4ef6...</td>' +
					'</tr>' +
				'</TBODY>' +
			'</table>';
	this.labelObj.innerHTML=a;
};
SSB_MultiUpload.prototype.addCtrl=function(){
	var oInput=this.getFileCtrl();
	oInput.className='multi_input';
	this.currentInput=oInput;
	document.getElementById('btn_'+this.ctrlId).appendChild(oInput);
};
SSB_MultiUpload.prototype.addUpload=function(event){
	var oInput=event.srcElement||event.target;
	var filePath=oInput.value;
	var that=this;
	if(this.url.indexOf(filePath)>0){
		alert("\u5f53\u524d\u76ee\u5f55\u4e0b\u5df2\u6709\u76f8\u540c\u6587\u4ef6\uff0c\u65e0\u987b\u91cd\u590d\u4e0a\u4f20");
		this.delRepeatInput(oInput);
		this.addCtrl();
		return;
	}
	if(this.maxfiles!=-1&&this.count>=this.maxfiles){
		alert("\u6700\u591a\u4e0a\u4f20"+this.maxfiles+"\u4e2a\u6587\u4ef6\u3002");
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	var ok=true;
	var onaddfile=this.onaddfile;
	if(onaddfile){
		try{
			ok=eval(onaddfile);
		}catch(e){
			this.delUpload(oInput);
			this.addCtrl();
			throw e;
		}
	}
	if(!ok){
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	this.url += "|"+filePath;
	var sIcon='unknown';
	var ext=filePath.substring(1+filePath.lastIndexOf("."),filePath.length);
	sIcon=this.iconMap[ext]?this.iconMap[ext]:sIcon;
	var oFileList = document.getElementById('upload_list_'+this.ctrlId);
	var oTr = document.createElement('TR');
	oFileList.appendChild(oTr);
	var oTd = document.createElement('TD');
	oTr.appendChild(oTd);
	var oIcon = document.createElement('DIV');
	oIcon.className='file_icon icon_'+sIcon;
	var oStr=document.createTextNode(filePath);
	oTd.appendChild(oIcon);
	oTd.appendChild(oStr);
	oTd=document.createElement('TD');
	oTr.appendChild(oTd);
	var oA=document.createElement('A');
	oA.className="option_del";
	oA.onclick=function(event){
		event=event||window.event;
		var a=event.srcElement||event.target;
		that.delUpload(a['oInput']);
	};
	oA.href="javascript:void(0);";
	oTd.appendChild(oA);
	oIcon=document.createElement('DIV');
	oIcon.className='file_icon icon_del';

	oStr=document.createTextNode('\u5220\u9664');
	oA.appendChild(oIcon);
	oA.appendChild(oStr);

	document.getElementById('no_upload_'+this.ctrlId).style.display='none';
	//\u4e92\u76f8\u4fdd\u5b58\u5f15\u7528
	oIcon['oInput']=oInput;
	oA['oInput']=oInput;
	oInput['oTr']=oTr;
	oInput.style.display='none';
	this.count++;			//\u6587\u4ef6\u589e\u52a01\u4e2a
	this.addCtrl();
};
SSB_MultiUpload.prototype.delUpload=function(oInput){
	if(this.uploading){
		alert("\u6587\u4ef6\u4e0a\u4f20\u4e2d\uff0c\u8bf7\u7b49\u5f85");
		return;
	}
	var oTr=oInput['oTr'];
	this.url = this.url.replace(oInput.value,"");
	this.url = this.url.replace("||","");
	oInput.parentNode.removeChild(oInput);
	if(oTr){
		oTr.parentNode.removeChild(oTr);
		this.count--;		//\u6587\u4ef6\u51cf\u5c11\u4e00\u4e2a
	}
	if(this.count==0){
		document.getElementById("no_upload_"+this.ctrlId).style.display='';
	}
};
//\u5220\u9664\u91cd\u590d\u7684 input \u6807\u7b7e\u3002
SSB_MultiUpload.prototype.delRepeatInput=function(oInput){
	oInput.parentNode.removeChild(oInput);
};
SSB_MultiUpload.prototype.setMaxFiles=function(maxfiles){
	this.maxfiles=isNaN(maxfiles)||maxfiles<1?-1:maxfiles;
};
SSB_MultiUpload.prototype.clean=function(){
	this.init();
	this.count=0;
	this.url='';
	this.currentInput={};
};

function UploadTag(id){
	var tagObj=document.getElementById(id);
	var type=tagObj.getAttribute('type');
	var upload={};
	switch(type){
		case 'single':upload= new SSB_SingleUpload(id);break;
		case 'multi':upload= new SSB_MultiUpload(id);break;
		default:upload= new SSB_SingleUpload(id);
	}
	upload.setText(tagObj.getAttribute("text"));
	upload.setDesc(tagObj.getAttribute("desc"));
	upload.setMaxFiles(parseInt(tagObj.getAttribute("maxfiles")));
	upload.setOnaddfile(tagObj.getAttribute("onaddfile"));
	return upload;
};

var _ZUPLOAD_TAG="Z:UPLOAD";
var _UPLOAD_TAG="UPLOAD";
var _ZTABLE_CLUMN ="Z:COLUMN";
var _TABLE_CLUMN ="COLUMN";

function Upload_init(){
	var atags=document.getElementsByTagName(_ZUPLOAD_TAG);
	if(!atags||atags.length==0)
	{
		atags=document.getElementsByTagName(_UPLOAD_TAG);	
	}
	var upload={};
	for(var i=0;i<atags.length;i++)
	{
		var tagType = atags[i].parentNode;
		var ptn = tagType.tagName.toUpperCase();
		//\u5982\u679c\u63a7\u4ef6\u4e0d\u5728\u8868\u683c\u63a7\u4ef6\u4e2d\u65f6
		if(ptn != _ZTABLE_CLUMN && ptn != _TABLE_CLUMN)
		{
			upload=new UploadTag(atags[i].getAttribute("id"));
		    upload.init();
		}		
	}
};
SSB_Upload={};
SSB_Upload.uploads={};
addOnloadEvent(Upload_init);

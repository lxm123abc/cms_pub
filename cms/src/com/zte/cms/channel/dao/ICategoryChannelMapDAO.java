package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategoryChannelMapDAO
{
    /**
     * 新增CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DAOException dao异常
     */
    public void insertCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据主键更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DAOException dao异常
     */
    public void updateCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据条件更新CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap更新条件
     * @throws DAOException dao异常
     */
    public void updateCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据主键删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @throws DAOException dao异常
     */
    public void deleteCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据条件删除CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap删除条件
     * @throws DAOException dao异常
     */
    public void deleteCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据主键查询CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @return 满足条件的CategoryChannelMap对象
     * @throws DAOException dao异常
     */
    public CategoryChannelMap getCategoryChannelMap(CategoryChannelMap categoryChannelMap) throws DAOException;

    /**
     * 根据条件查询CategoryChannelMap对象
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @return 满足条件的CategoryChannelMap对象集
     * @throws DAOException dao异常
     */
    public List<CategoryChannelMap> getCategoryChannelMapByCond(CategoryChannelMap categoryChannelMap)
            throws DAOException;

    /**
     * 根据条件分页查询CategoryChannelMap对象，作为查询条件的参数
     * 
     * @param categoryChannelMap CategoryChannelMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CategoryChannelMap categoryChannelMap, int start, int pageSize) throws DAOException;
    
    public List<CategoryChannelMap> getCategoryChannelMapByCond2(
			CategoryChannelMap categoryChannelMap) throws DAOException ;
}
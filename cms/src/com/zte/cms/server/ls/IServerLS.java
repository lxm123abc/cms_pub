package com.zte.cms.server.ls;

import java.util.List;

import com.zte.cms.server.model.CmsServer;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;

public interface IServerLS
{

    public String checkExist(String index) throws Exception;

    public String insertCmsServer(CmsServer cmsServer) throws Exception;

    public String updateCmsServer(CmsServer cmsServer) throws Exception;

    public String deleteCmsServer(String index) throws Exception;

    public String batchDeleCmsServer(List<CmsServer> serverList) throws Exception;

    public CmsServer getCmsServer(String index) throws Exception;

    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws Exception;

    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

}

package com.zte.ismp.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import com.zte.ismp.common.ResourceManager;

/**
 * 日期工具类，用于统计
 * 
 * @author
 * 
 */
public class DateUtil
{

    /**
     * 获取某年某天是第几年第几周，返回6位，例如：传入20090101，返回200901
     * 
     * @param String 8位
     * @return String
     */
    public String getTheWeek(String thisdate)
    {
        String retvalue = "";

        String year = thisdate.substring(0, 4);
        String month = thisdate.substring(4, 6);
        String day = thisdate.substring(6, 8);

        int weeknum = getWeekNum(Integer.parseInt(year));
        for (int i = 1; i <= weeknum; i++)
        {
            String startday = getWeekFirstDay(Integer.parseInt(year), i);
            String endday = getWeekLastDay(Integer.parseInt(year), i);
            if (thisdate.compareTo(startday) >= 0 && thisdate.compareTo(endday) <= 0)
            {
                retvalue = year + getFixMonth("" + i);
                return retvalue;
            }
        }

        retvalue = (Integer.parseInt(year) + 1) + "01";

        return retvalue;
    }

    /**
     * 取得某年某月最后一天的日期
     * 
     * @return String 返回8位日期
     */
    public String getCurrentDate()
    {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = sDateFormat.format(gCalendar.getTime());
        return retvalue;
    }

    /**
     * 取得某年某月最后一天的日期
     * 
     * @param year int
     * @param month int
     * @return String 返回8位日期
     */
    public String getMonthLastDay(int year, int month)
    {
        // 这里用int是为了防止出现"08"
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = "";

        gCalendar.set(GregorianCalendar.YEAR, year);
        gCalendar.set(GregorianCalendar.MONTH, month - 1);
        gCalendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
        gCalendar.add(GregorianCalendar.MONTH, 1);

        gCalendar.add(GregorianCalendar.DAY_OF_MONTH, -1);

        retvalue = sDateFormat.format(gCalendar.getTime());
        return retvalue;
    }

    /**
     * 取得某年某月第一一天的日期
     * 
     * @param year int
     * @param month int
     * @return String 返回8位日期
     */
    public String getMonthFirstDay(int year, int month)
    {
        // 第一天，当然是年+月+01了
        String yearstr = String.valueOf(year);
        String monthstr = String.valueOf(month);
        if (monthstr.length() < 2)
        {
            monthstr = "0" + monthstr;
        }
        String retvalue = "";
        retvalue = yearstr + monthstr + "01";
        return retvalue;
    }

    /**
     * 取得某年某周第一天的日期，中文环境第一天是周一，英文环境第一天是周日，是中文环境的前一天
     * 
     * @param year int
     * @param week int
     * @return String 返回8位日期
     */
    public String getWeekFirstDay(int year, int week)
    {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = "";
        String lan = ResourceManager.getResourceText("webdeploylanguage");

        if (lan.equals("zh"))
        {
            // 中文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.setFirstDayOfWeek(2);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            retvalue = sDateFormat.format(gCalendar.getTime());
        }
        else
        {
            // 英文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            retvalue = sDateFormat.format(gCalendar.getTime());
        }
        // System.out.println(retvalue);
        return retvalue;
    }

    /**
     * 取得某年某周最后一天的日期，中文环境最后一天是周日，英文环境最后一天是周六，是中文环境的前一天
     * 
     * @param year int
     * @param week int
     * @return String 返回8位日期
     */
    public String getWeekLastDay(int year, int week)
    {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = "";
        String lan = ResourceManager.getResourceText("webdeploylanguage");

        if (lan.equals("zh"))
        {
            // 中文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.setFirstDayOfWeek(2);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            gCalendar.roll(GregorianCalendar.DAY_OF_WEEK, 6);
            retvalue = sDateFormat.format(gCalendar.getTime());
        }
        else
        {
            // 英文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            gCalendar.roll(GregorianCalendar.DAY_OF_WEEK, 6);
            retvalue = sDateFormat.format(gCalendar.getTime());
        }
        return retvalue;
    }

    /**
     * 取得某年某周第几天的日期，第几天是从0到6，中文环境中，第0天是周一，而英文环境中，第0天是周日，为中文环境的前一天
     * 
     * @param year int
     * @param week int
     * @param day int
     * @return String 返回8位日期
     */
    public String getWeekAnyDay(int year, int week, int day)
    {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = "";
        String lan = ResourceManager.getResourceText("webdeploylanguage");

        if (lan.equals("zh"))
        {
            // 中文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.setFirstDayOfWeek(2);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            gCalendar.roll(GregorianCalendar.DAY_OF_WEEK, day);
            retvalue = sDateFormat.format(gCalendar.getTime());
        }
        else
        {
            // 英文
            gCalendar.set(GregorianCalendar.YEAR, year);
            gCalendar.set(GregorianCalendar.WEEK_OF_YEAR, week);
            gCalendar.set(GregorianCalendar.DAY_OF_WEEK, gCalendar.getFirstDayOfWeek());
            gCalendar.roll(GregorianCalendar.DAY_OF_WEEK, day);
            retvalue = sDateFormat.format(gCalendar.getTime());
        }

        return retvalue;
    }

    /**
     * 获取某个时间段的每天日期，例如：传入为20090227，20090301，则返回list包括20090227,20090228,20090301
     * 
     * @param startdate String
     * @param enddate String
     * @return List<String>
     */
    public List<String> getDates(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startmonth = startdate.substring(4, 6);
        String startday = startdate.substring(6, 8);

        gCalendar.set(GregorianCalendar.YEAR, Integer.valueOf(startyear));
        gCalendar.set(GregorianCalendar.MONTH, Integer.valueOf(startmonth) - 1);
        gCalendar.set(GregorianCalendar.DAY_OF_MONTH, Integer.valueOf(startday));
        // System.out.println(sDateFormat.format(gCalendar.getTime()));

        String tmpdate = sDateFormat.format(gCalendar.getTime());

        while (tmpdate.compareTo(enddate) <= 0)
        {
            // System.out.println(tmpdate);

            retvalue.add(tmpdate);

            if (tmpdate.substring(4, 8).equals("1231"))
            {
                gCalendar.roll(GregorianCalendar.YEAR, 1);
                gCalendar.roll(GregorianCalendar.DAY_OF_YEAR, 1);
                tmpdate = sDateFormat.format(gCalendar.getTime());
            }
            else
            {
                gCalendar.roll(GregorianCalendar.DAY_OF_YEAR, 1);
                tmpdate = sDateFormat.format(gCalendar.getTime());
            }
        }

        return retvalue;
    }

    /**
     * 根据开始年月、结束年月，获取每个年月第一天日期，例如：传入200801，200803，返回20080101，20080201，20080301
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return List<String>
     */
    public List<String> getMonthFirstDates(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startmonth = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endmonth = enddate.substring(4, 6);

        String lastday = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= 12; month++)
            {
                lastday = getMonthFirstDay(year, month);
                retvalue.add(lastday);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    lastday = getMonthFirstDay(year, month);
                    retvalue.add(lastday);
                }
            }

            for (int year = Integer.parseInt(endyear), month = 1; month <= Integer.parseInt(endmonth); month++)
            {
                lastday = getMonthFirstDay(year, month);
                retvalue.add(lastday);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= Integer
                    .parseInt(endmonth); month++)
            {
                lastday = getMonthFirstDay(year, month);
                retvalue.add(lastday);
            }
        }

        return retvalue;
    }

    /**
     * 根据开始年月、结束年月，获取每个年月最后一天日期，例如：传入200801，200803，返回20080131，20080229，20080331
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return List<String>
     */
    public List<String> getMonthLastDates(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startmonth = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endmonth = enddate.substring(4, 6);

        String lastday = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= 12; month++)
            {
                lastday = getMonthLastDay(year, month);
                retvalue.add(lastday);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    lastday = getMonthLastDay(year, month);
                    retvalue.add(lastday);
                }
            }

            for (int year = Integer.parseInt(endyear), month = 1; month <= Integer.parseInt(endmonth); month++)
            {
                lastday = getMonthLastDay(year, month);
                retvalue.add(lastday);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= Integer
                    .parseInt(endmonth); month++)
            {
                lastday = getMonthLastDay(year, month);
                retvalue.add(lastday);
            }
        }

        return retvalue;
    }

    /**
     * 根据开始年周、结束年周，获取每个年周最后一天日期，例如：传入200801，200803，返回20080105，20080112，20080119
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return List<String>
     */
    public List<String> getWeekLastDates(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startweek = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endweek = enddate.substring(4, 6);

        String lastday = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= getWeekNum(year); week++)
            {
                lastday = getWeekLastDay(year, week);
                retvalue.add(lastday);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int week = 1; week <= getWeekNum(year); week++)
                {
                    lastday = getWeekLastDay(year, week);
                    retvalue.add(lastday);
                }
            }

            for (int year = Integer.parseInt(endyear), week = 1; week <= Integer.parseInt(endweek); week++)
            {
                lastday = getWeekLastDay(year, week);
                retvalue.add(lastday);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= Integer
                    .parseInt(endweek); week++)
            {
                lastday = getWeekLastDay(year, week);
                retvalue.add(lastday);
            }
        }

        return retvalue;
    }

    /**
     * 根据开始年周、结束年周，获取每个年周第一天日期，例如：传入200801，200803，返回20071230，20080106，20080113
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return List<String>
     */
    public List<String> getWeekFirstDates(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startweek = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endweek = enddate.substring(4, 6);

        String firstday = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= getWeekNum(year); week++)
            {
                firstday = getWeekFirstDay(year, week);
                retvalue.add(firstday);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int week = 1; week <= getWeekNum(year); week++)
                {
                    firstday = getWeekFirstDay(year, week);
                    retvalue.add(firstday);
                }
            }

            for (int year = Integer.parseInt(endyear), week = 1; week <= Integer.parseInt(endweek); week++)
            {
                firstday = getWeekFirstDay(year, week);
                retvalue.add(firstday);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= Integer
                    .parseInt(endweek); week++)
            {
                firstday = getWeekFirstDay(year, week);
                retvalue.add(firstday);
            }
        }

        return retvalue;
    }

    /**
     * 获取某个日期后面一天的日期，例如：传入20090312，传出20090313
     * 
     * @param curdate String
     * @return String
     */
    public String getNextDate(String curdate)
    {
        if (curdate == null || curdate.equals(""))
        {
            return null;
        }
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMdd");
        GregorianCalendar gCalendar = new GregorianCalendar();
        String retvalue = "";

        String startyear = curdate.substring(0, 4);
        String startmonth = curdate.substring(4, 6);
        String startday = curdate.substring(6, 8);

        gCalendar.set(GregorianCalendar.YEAR, Integer.valueOf(startyear));
        gCalendar.set(GregorianCalendar.MONTH, Integer.valueOf(startmonth) - 1);
        gCalendar.set(GregorianCalendar.DAY_OF_MONTH, Integer.valueOf(startday));

        if (curdate.substring(4, 8).equals("1231"))
        {
            gCalendar.roll(GregorianCalendar.YEAR, 1);
            gCalendar.roll(GregorianCalendar.DAY_OF_YEAR, 1);
            curdate = sDateFormat.format(gCalendar.getTime());
        }
        else
        {
            gCalendar.roll(GregorianCalendar.DAY_OF_YEAR, 1);
            curdate = sDateFormat.format(gCalendar.getTime());
        }
        // gCalendar.roll(GregorianCalendar.DAY_OF_YEAR, 1);

        retvalue = sDateFormat.format(gCalendar.getTime());

        // System.out.println(retvalue);

        return retvalue;
    }

    /**
     * 获取sql条件，比如snapdate>'20090101' and snapdate<'20090201'，终端统计中使用，主要用于写sql查询累加值
     * 
     * @param condition String，例如snapdate
     * @param startdate String，6位
     * @param enddate String，6位
     * @return List<String>
     */
    public List<String> getMonthStatStr(String condition, String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals("") || condition == null
                || condition.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        List lastdates = new ArrayList();

        lastdates = getMonthLastDates(startdate, enddate);

        Iterator it = lastdates.iterator();

        String sqlstr = "";

        while (it.hasNext())
        {
            String laststr = (String) (it.next());
            String year = laststr.substring(0, 4);
            String month = laststr.substring(4, 6);

            String firstdate = getMonthFirstDay(Integer.parseInt(year), Integer.parseInt(month));

            sqlstr = " " + condition + ">='" + firstdate + "' and " + condition + "<='" + laststr + "' ";
            retvalue.add(sqlstr);
        }

        return retvalue;
    }

    /**
     * 获取sql条件，比如snapdate>='20090101' and snapdate=<'20090107'，终端统计中使用，主要用于写sql查询累加值
     * 
     * @param condition String，例如snapdate
     * @param startdate String，6位
     * @param enddate String，6位
     * @return String
     */
    public List<String> getWeekStatStr(String condition, String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals("") || condition == null
                || condition.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        List lastdates = new ArrayList();
        List firstdates = new ArrayList();

        firstdates = getWeekFirstDates(startdate, enddate);
        lastdates = getWeekLastDates(startdate, enddate);

        Iterator it = lastdates.iterator();
        Iterator it2 = firstdates.iterator();

        String sqlstr = "";

        while (it.hasNext())
        {
            String firststr = (String) (it2.next());
            String laststr = (String) (it.next());

            sqlstr = " " + condition + ">='" + firststr + "' and " + condition + "<='" + laststr + "' ";
            retvalue.add(sqlstr);
        }

        return retvalue;
    }

    /**
     * 获取年周日期名称，例如：传入200805，返回2008-05
     * 
     * @param convdate String，6位
     * @return String
     */
    public String getWeekName(String convdate)
    {
        if (convdate == null || convdate.equals(""))
        {
            return null;
        }
        String retvalue = "";

        String year = convdate.substring(0, 4);
        String week = convdate.substring(4, 6);
        int weekint = Integer.parseInt(week);

        String lan = ResourceManager.getResourceText("webdeploylanguage");
        if (lan.equals("zh"))
        {
            retvalue = year + "-" + getFixMonth("" + weekint);
        }
        else
        {
            retvalue = year + "-" + getFixMonth("" + weekint);
        }

        return retvalue;
    }

    /**
     * 获取年月日期名称，例如：传入200805，返回2008-05
     * 
     * @param convdate String，6位
     * @return String
     */
    public String getMonthName(String convdate)
    {
        if (convdate == null || convdate.equals(""))
        {
            return null;
        }
        String retvalue = "";

        String year = convdate.substring(0, 4);
        String month = convdate.substring(4, 6);
        int monthint = Integer.parseInt(month);

        String lan = ResourceManager.getResourceText("webdeploylanguage");
        if (lan.equals("zh"))
        {
            retvalue = year + "-" + getFixMonth("" + monthint);
        }
        else
        {
            retvalue = year + "-" + getFixMonth("" + monthint);
        }

        return retvalue;
    }

    /**
     * 获取年月日日期名称，例如：传入20080502，返回2008-05-02
     * 
     * @param convdate String，8位
     * @return String
     */
    public String getDayName(String convdate)
    {
        if (convdate == null || convdate.equals(""))
        {
            return null;
        }
        String retvalue = "";

        String year = convdate.substring(0, 4);
        String month = convdate.substring(4, 6);
        String day = convdate.substring(6, 8);
        int monthint = Integer.parseInt(month);
        int dayint = Integer.parseInt(day);

        String lan = ResourceManager.getResourceText("webdeploylanguage");
        if (lan.equals("zh"))
        {
            retvalue = year + "-" + getFixMonth("" + monthint) + "-" + getFixMonth("" + dayint);
        }
        else
        {
            retvalue = year + "-" + getFixMonth("" + monthint) + "-" + getFixMonth("" + dayint);
        }

        return retvalue;
    }

    /**
     * 获取年月日日期名称
     * 
     * @param convdate String 8位
     * @return String
     */
    public String getName(String convdate)
    {
        if (convdate == null || convdate.equals(""))
        {
            return null;
        }
        String retvalue = "";

        int length = convdate.length();

        if (length == 8)
        {
            String year = convdate.substring(0, 4);
            String month = convdate.substring(4, 6);
            String day = convdate.substring(6, 8);
            int monthint = Integer.parseInt(month);
            int dayint = Integer.parseInt(day);

            String lan = ResourceManager.getResourceText("webdeploylanguage");
            if (lan.equals("zh"))
            {
                retvalue = year + "-" + getFixMonth("" + monthint) + "-" + getFixMonth("" + dayint);
            }
            else
            {
                retvalue = year + "-" + getFixMonth("" + monthint) + "-" + getFixMonth("" + dayint);
            }
        }
        if (length == 6)
        {
            String year = convdate.substring(0, 4);
            String month = convdate.substring(4, 6);
            int monthint = Integer.parseInt(month);

            String lan = ResourceManager.getResourceText("webdeploylanguage");
            if (lan.equals("zh"))
            {
                retvalue = year + "-" + getFixMonth("" + monthint);
            }
            else
            {
                retvalue = year + "-" + getFixMonth("" + monthint);
            }

            return retvalue;
        }

        return retvalue;
    }

    /**
     * 根据月，补足两位数，周也能用
     * 
     * @param month String
     * @return String
     */
    public String getFixMonth(String month)
    {
        // 这里补足方法没有调用strUtil的原因是：如果util互相调用不大好，应该独立
        if (month == null || month.equals(""))
        {
            return null;
        }
        String retvalue = "";

        if (month.length() == 1)
        {
            retvalue = "0" + month;
        }
        else
        {
            retvalue = month;
        }

        return retvalue;
    }

    /**
     * 根据开始年月、结束年月，获取每个年月，例如：传入200901，200903，返回200901，200902，200903
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return List<String>
     */
    public List<String> getEveryMonth(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startmonth = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endmonth = enddate.substring(4, 6);

        String yearmonth = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= 12; month++)
            {
                yearmonth = year + getFixMonth(String.valueOf(month));
                retvalue.add(yearmonth);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    yearmonth = year + getFixMonth(String.valueOf(month));
                    retvalue.add(yearmonth);
                }
            }

            for (int year = Integer.parseInt(endyear), month = 1; month <= Integer.parseInt(endmonth); month++)
            {
                yearmonth = year + getFixMonth(String.valueOf(month));
                retvalue.add(yearmonth);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), month = Integer.parseInt(startmonth); month <= Integer
                    .parseInt(endmonth); month++)
            {
                yearmonth = year + getFixMonth(String.valueOf(month));
                retvalue.add(yearmonth);
            }
        }

        return retvalue;
    }

    /**
     * 根据开始年周、结束年周，获取每个年周，例如，传入200902，200904，返回200902，200903，200904
     * 
     * @param startdate String 6位
     * @param enddate String 6位
     * @return String
     */
    public List<String> getEveryWeek(String startdate, String enddate)
    {
        if (startdate == null || startdate.equals("") || enddate == null || enddate.equals(""))
        {
            return null;
        }
        List retvalue = new ArrayList();

        String startyear = startdate.substring(0, 4);
        String startweek = startdate.substring(4, 6);
        String endyear = enddate.substring(0, 4);
        String endweek = enddate.substring(4, 6);

        String yearweek = "";

        if (startyear.compareTo(endyear) < 0)
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= getWeekNum(year); week++)
            {
                yearweek = year + getFixMonth(String.valueOf(week));
                retvalue.add(yearweek);
            }
            startyear = String.valueOf((Integer.parseInt(startyear) + 1));

            for (int year = Integer.parseInt(startyear); year < Integer.parseInt(endyear); year++)
            {
                for (int week = 1; week <= getWeekNum(year); week++)
                {
                    yearweek = year + getFixMonth(String.valueOf(week));
                    retvalue.add(yearweek);
                }
            }

            for (int year = Integer.parseInt(endyear), week = 1; week <= Integer.parseInt(endweek); week++)
            {
                yearweek = year + getFixMonth(String.valueOf(week));
                retvalue.add(yearweek);
            }
        }
        else
        {
            for (int year = Integer.parseInt(startyear), week = Integer.parseInt(startweek); week <= Integer
                    .parseInt(endweek); week++)
            {
                yearweek = year + getFixMonth(String.valueOf(week));
                retvalue.add(yearweek);
            }
        }

        return retvalue;
    }

    /**
     * 判断1年多少周
     * 
     * @param year int
     * @return int
     */
    public int getWeekNum(int year)
    {
        int retvalue = 0;

        // 最后一天日期，用于比较
        String lastday = year + "1231";

        String lastweekday = getWeekLastDay(year, 53);

        if (lastweekday.compareTo(lastday) <= 0)
        {
            // 53周最后一天在本年，本年有53周
            retvalue = 53;
        }
        else
        {
            retvalue = 52;
        }

        return retvalue;
    }

    public static void main(String[] args)
    {
        DateUtil wutil = new DateUtil();
        wutil.getWeekFirstDay(2009, 2);
        wutil.getWeekLastDay(2009, 2);
        wutil.getWeekAnyDay(2009, 2, 5);

        DateUtil wutil1 = new DateUtil();
        // wutil.getWeekFirstDay(2009,2);
        String str = wutil1.getMonthLastDay(2009, 2);
        System.out.println(str);
        // wutil.getWeekAnyDay(2009, 2, 5);
        str = wutil1.getMonthFirstDay(2009, 2);
        System.out.println(str);
        str = wutil1.getMonthFirstDay(2009, 10);
        System.out.println(str);

        DateUtil wuti2 = new DateUtil();
        List mylist = wuti2.getDates("20090701", "20091010");
        Iterator it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println(wuti2.getNextDate("20091231"));

        System.out.println("开始1");

        mylist = wuti2.getMonthLastDates("200801", "200912");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始2");

        mylist = wuti2.getWeekLastDates("200902", "200912");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始3");

        mylist = wuti2.getMonthStatStr("snapdate", "200801", "200910");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始4");

        mylist = wuti2.getWeekStatStr("snapdate", "200801", "200910");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始5");

        mylist = wuti2.getEveryMonth("200801", "200910");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始6");

        mylist = wuti2.getEveryWeek("200801", "200910");
        it = mylist.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始7");
        System.out.println(wuti2.getWeekLastDay(2008, 53));

        System.out.println("开始8");
        System.out.println(wuti2.getWeekNum(2011));

        System.out.println("开始9");
        List mylist2 = wuti2.getDates("20071231", "20080113");
        it = mylist2.iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println("开始10");
        System.out.println(wuti2.getTheWeek("20090101"));
    }
}

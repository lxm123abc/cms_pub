package com.zte.cms.content.series.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.picture.service.ISeriesPictureMapDS;

import com.zte.cms.seriespromap.service.ISeriesProgramMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsSeriesPcitureLS extends DynamicObjectBaseDS implements ICmsSeriesPcitureLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private ISeriesProgramMapDS seriesProgramMapDS;
    private ICmsProgramDS cmsProgramDS;
    private ICmsSeriesDS cmsSeriesDS;
    private IUsysConfigDS usysConfigDS;
    private ITargetsystemDS targetSystemDS;
    private ICntSyncTaskDS ntsyncTaskDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ICntSyncRecordDS cntSyncRecordDS;
    private ISeriesPictureMapDS seriesPictureMapDS;
	private IPictureDS pictureDS;
	private ICntPlatformSyncDS cntPlatformSyncDS;
	private ICntTargetSyncDS cntTargetSyncDS;
	private IObjectSyncRecordDS objectSyncRecordDS;
	private ICntSyncTaskDS cntSyncTaskDS;
    public final static String CNTSYNCXML = "xmlsync";
    public final static String SERIES_PROM_DIR = "seriesprogram";
    public final static int OBJECTTYPE = 36;
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    public static final String BATCHID = "ucdn_task_batch_id";// 任务批次号
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
	private Map sereispicmap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress="";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private String result = "0:操作成功";
    // Action 类型
    public static final int TASKTYPE_REGIST = 1;
    public static final int TASKTYPE_UPDATE = 2;
    public static final int TASKTYPE_DELETE = 3;

    // 目标系统
    public static final int DESTTYPE_BMS = 1;
    public static final int DESTTYPE_EPG = 2;
    public static final int DESTTYPE_CDN = 3;


    public ICntSyncTaskDS getNtsyncTaskDS()
    {
        return ntsyncTaskDS;
    }

    public void setNtsyncTaskDS(ICntSyncTaskDS ntsyncTaskDS)
    {
        this.ntsyncTaskDS = ntsyncTaskDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public ICmsSeriesDS getCmsSeriesDS()
    {
        return cmsSeriesDS;
    }

    public void setCmsSeriesDS(ICmsSeriesDS cmsSeriesDS)
    {
        this.cmsSeriesDS = cmsSeriesDS;
    }

    public ICmsProgramDS getCmsProgramDS()
    {
        return cmsProgramDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public ISeriesProgramMapDS getSeriesProgramMapDS()
    {
        return seriesProgramMapDS;
    }

    public void setSeriesProgramMapDS(ISeriesProgramMapDS seriesProgramMapDS)
    {
        this.seriesProgramMapDS = seriesProgramMapDS;
    }

    public ICmsStorageareaLS getCmsStorageareaLS()
    {
        return cmsStorageareaLS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

	public void setSeriesPictureMapDS(ISeriesPictureMapDS seriesPictureMapDS)
	{
		this.seriesPictureMapDS = seriesPictureMapDS;
	}
    
    public void setPictureDS(IPictureDS pictureDS)
	{
		this.pictureDS = pictureDS;
	}	
    
	public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
	{
		this.cntPlatformSyncDS = cntPlatformSyncDS;
	}
    

	public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
	{
		this.cntTargetSyncDS = cntTargetSyncDS;
	}    
    

	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
	{
		this.objectSyncRecordDS = objectSyncRecordDS;
	}	
	
	public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
	{
		this.cntSyncTaskDS = cntSyncTaskDS;
	}
    /****************************连续剧海报发布 begin*******************************/
    //查询平台发布记录
    public TableDataInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            seriesPictureMap.setCreateStarttime(DateUtil2.get14Time(seriesPictureMap.getCreateStarttime()));
            seriesPictureMap.setCreateEndtime(DateUtil2.get14Time(seriesPictureMap.getCreateEndtime()));
            seriesPictureMap.setOnlinetimeStartDate(DateUtil2.get14Time(seriesPictureMap.getOnlinetimeStartDate()));
            seriesPictureMap.setOnlinetimeEndDate(DateUtil2.get14Time(seriesPictureMap.getOnlinetimeEndDate()));
            seriesPictureMap.setCntofflinestarttime(DateUtil2.get14Time(seriesPictureMap.getCntofflinestarttime()));
            seriesPictureMap.setCntofflineendtime(DateUtil2.get14Time(seriesPictureMap.getCntofflineendtime()));

            if(seriesPictureMap.getPictureid()!=null&&!"".equals(seriesPictureMap.getPictureid())){
            	seriesPictureMap.setPictureid(EspecialCharMgt.conversion(seriesPictureMap.getPictureid()));
            }           
            dataInfo = seriesPictureMapDS.pageInfoQueryseriesPicMap(seriesPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    } 
	
    //查询网友发布记录
    public TableDataInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            dataInfo = seriesPictureMapDS.pageInfoQuerypseriesPicTargetMap(seriesPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }    
	    
    public String batchPublishOrDelPublishseriesPic(List<SeriesPictureMap> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
            if (operType.equals("0"))  //批量发布
            {
                for (SeriesPictureMap seriesPictureMap : list)
                {
                    message = publishseriesPicMapPlatformSyn(seriesPictureMap.getMapindex().toString(), seriesPictureMap.getSeriesid(),seriesPictureMap.getPictureid(),type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                for (SeriesPictureMap seriesPictureMap : list)
                {
                    message = delPublishseriesPicMapPlatformSyn(seriesPictureMap.getMapindex().toString(), seriesPictureMap.getSeriesid(),seriesPictureMap.getPictureid(),type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), seriesPictureMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("CmsSeriesPictureLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
    public String publishseriesPicMapPlatformSyn(String mapindex, String seriesid,String pictureid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
    		seriesPictureMap.setMapindex(Long.parseLong(mapindex));
    		seriesPictureMap.setSeriesid(seriesid);
    		seriesPictureMap.setPictureid(pictureid);
			List<SeriesPictureMap> seriesPictureMapList = new ArrayList<SeriesPictureMap>();
			seriesPictureMapList =seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);
			if (seriesPictureMapList == null ||seriesPictureMapList.size()==0)
			{
				return "1:操作失败,连续剧关联海报记录不存在";
			}
    		String initresult="";
    		initresult =initDataPic(seriesPictureMapList.get(0),"platform",type,1);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(38);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(seriesPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}
	    	sereispicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(38);
	    	sereispicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesPictureMap seriesPictureMapObj=new SeriesPictureMap();
            int  platformType = 1;
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(seriesPictureMap.getMapindex());
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
    	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	sereispicmap.put("pictureMap", seriesPictureMapList);
    			for (SeriesPictureMap seriesPictureMapTmp:seriesPictureMapList )
    			{
    			    int targettype = seriesPictureMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (seriesPictureMapTmp.getTargetstatus()!=0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                 	    		 +seriesPictureMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                 +"  ");
                   	    isstatus = false;
    				}
    				if (isstatus)
    				{
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    	            synType = 1;
    				boolean ispub = true;
    				cntTargetSync.setObjectindex(seriesPictureMapTmp.getSeriesindex());
    				cntTargetSync.setTargetindex(seriesPictureMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(4);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                	    resultPubStr.append("该连续剧没有发布到"+TARGETSYSTEM
               	    		 +seriesPictureMapTmp.getTargetid()
                               +"  ");    	
                	    ispub = false;
                	}
    				if (ispub)
    				{
                        int synTargetType =1;
                        if (seriesPictureMapTmp.getStatus()== 500) //修改发布    
                        {
                    		synTargetType =2;
            	            synType = 2;
                        }
        				boolean isxml = true;

                       	if (targettype ==2&&(seriesPictureMapTmp.getStatus()== 0||
                       			seriesPictureMapTmp.getStatus()== 400||seriesPictureMapTmp.getStatus()== 500))  //EPG系统
                       	{
                       		String xml;
                       		if (synTargetType==1)
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 1); //新增发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    		 +seriesPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		else
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 2); //修改发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    		 +seriesPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                }
                       		}
                       		
                       		if (isxml)
                       		{
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                Long targetindex =seriesPictureMapTmp.getTargetindex();
                                
                                Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                Long objectPicindex = seriesPictureMapTmp.getPictureindex();
                                String objectid = seriesPictureMapTmp.getPictureid();
                                String objectcode = seriesPictureMapTmp.getPicturecode();
                                insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                                Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                String objectidMapping = seriesPictureMapTmp.getMappingid();
                                String parentid = seriesPictureMapTmp.getPictureid();
                                String  elementid = seriesPictureMapTmp.getSeriesid();
                                Long objectindex = seriesPictureMapTmp.getMapindex();
                                insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                                
                                insertSyncPicTask(index,xml,correlateid,targetindex); //插入同步任务
                                
                                cntTargetSyncTemp.setSyncindex(seriesPictureMapTmp.getSyncindex());
                                if (synTargetType ==1)
                                {
                                	cntTargetSyncTemp.setOperresult(10);
                                }
                                else
                                {
                                	cntTargetSyncTemp.setOperresult(40);
                                }                            
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                CommonLogUtil.insertOperatorLog(seriesPictureMapTmp.getMappingid()+","+seriesPictureMapTmp.getTargetid(), 
                                		CommonLogConstant.MGTTYPE_SERIESSYN,
                                        CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                                sucessPub=true;
                                resultPubStr.append(PUBTARGETSYSTEM
                        	    		+seriesPictureMapTmp.getTargetid()+": "
                                        +operSucess+"  ");                       			
                       		}

                          }
                       }                       		
    			   } 
    		   }
    		   }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			    seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);

       	    	   if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   sereispicmap.put("pictureMap", seriesPictureMapList);
         			for (SeriesPictureMap seriesPictureMapTmp:seriesPictureMapList )
           			{
                        int targettype = seriesPictureMapTmp.getTargettype();
        				boolean isstatus = true;
        				if (seriesPictureMapTmp.getTargetstatus()!=0)
        				{
                       	    resultPubStr.append(PUBTARGETSYSTEM
                     	    		 +seriesPictureMapTmp.getTargetid()+": "+"目标系统状态不正确"
                                     +"  ");
                       	    isstatus = false;
        				}
        				if (isstatus)
        				{
        	            synType = 1;
           	            CntTargetSync cntTargetSync = new CntTargetSync();    		
           				boolean ispub = true;
           				cntTargetSync.setObjectindex(seriesPictureMapTmp.getSeriesindex());
           				cntTargetSync.setTargetindex(seriesPictureMapTmp.getTargetindex());
        				cntTargetSync.setObjecttype(4);
           				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
           				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
           				{
                       	    resultPubStr.append("该连续剧没有发布到"+TARGETSYSTEM
                      	    		 +seriesPictureMapTmp.getTargetid()
                                      +"  ");    	
                       	    ispub = false;
                       	}
           				if (ispub)
           				{
                               int synTargetType =1;
                               if (seriesPictureMapTmp.getStatus()== 500) //修改发布    
                               {
                           		    synTargetType =2;
                	                synType = 2;
                               }
               				   
                               boolean isxml = true;

                               if ((seriesPictureMapTmp.getStatus()== 0||
                              			seriesPictureMapTmp.getStatus()== 400||seriesPictureMapTmp.getStatus()== 500))  //2.0平台网元
                              	{
                              		String xml;
                              		if (synTargetType==1)
                              		{
                                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 1); //新增发布
                                        if (null == xml)
                                        {
                                        	isxml = false;
                                       	    resultPubStr.append(PUBTARGETSYSTEM
                                     	    		 +seriesPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                     +"  ");
                                        }
                              		}
                              		else
                              		{
                                         xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 2); //修改发布
                                         if (null == xml)
                                         {
                                             isxml = false;
                                        	 resultPubStr.append(PUBTARGETSYSTEM
                                      	    		 +seriesPictureMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                                      +"  ");
                                         }
                              		}
                              		if (isxml)
                              		{
                                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                        Long targetindex =seriesPictureMapTmp.getTargetindex();
                                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                                        Long objectPicindex = seriesPictureMapTmp.getPictureindex();
                                        String objectid = seriesPictureMapTmp.getPictureid();
                                        String objectcode = seriesPictureMapTmp.getPicturecode();
                                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                        String objectidMapping = seriesPictureMapTmp.getMappingid();
                                        String parentid = seriesPictureMapTmp.getPictureid();
                                        String  elementid = seriesPictureMapTmp.getSeriesid();
                                        Long objectindex = seriesPictureMapTmp.getMapindex();
                                        String elementcode = seriesPictureMapTmp.getCpcontentid();
                                        String parentcode = objectcode;
                                        insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                        insertSyncPicTask(index,xml,correlateid,targetindex); //插入同步任务

                                       cntTargetSyncTemp.setSyncindex(seriesPictureMapTmp.getSyncindex());
                                       if (synTargetType ==1)
                                       {
                                       	   cntTargetSyncTemp.setOperresult(10);
                                       }
                                       else
                                       {
                                           cntTargetSyncTemp.setOperresult(40);
                                       }                            
                                       cntTargetSyncTemp.setStatus(200);
                                       cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                       CommonLogUtil.insertOperatorLog(seriesPictureMapTmp.getMappingid()+ ","+seriesPictureMapTmp.getTargetid(),
                                    		   CommonLogConstant.MGTTYPE_SERIESSYN,
                                               CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_INFO,
                                               CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                       sucessPub=true;
                                       resultPubStr.append(PUBTARGETSYSTEM
                               	    		+seriesPictureMapTmp.getTargetid()+": "
                                               +operSucess+"  ");                               			
                              		}

                                 }
                             }                          		
           				} 
    	       }
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    	}   


	public String delPublishseriesPicMapPlatformSyn(String mapindex, String seriesid,String pictureid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
    	try
    	{       
    		SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
    		seriesPictureMap.setMapindex(Long.parseLong(mapindex));
    		seriesPictureMap.setSeriesid(seriesid);
    		seriesPictureMap.setPictureid(pictureid);
			List<SeriesPictureMap> seriesPictureMapList = new ArrayList<SeriesPictureMap>();
			seriesPictureMapList = seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);
			if (seriesPictureMapList == null ||seriesPictureMapList.size()==0)
			{
				return "1:操作失败,连续剧关联海报记录不存在";
			}
    		String initresult="";
    		initresult =initDataPic(seriesPictureMapList.get(0),"platform",type,3);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(38);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(seriesPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
    			picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}    		
	    	sereispicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(38);
	    	sereispicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesPictureMap seriesPictureMapObj=new SeriesPictureMap();
            int  platformType = 1;
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(seriesPictureMap.getMapindex());
    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			
    			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
    	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	sereispicmap.put("pictureMap", seriesPictureMapList);
    	    	int targettype = 0;
    			for (SeriesPictureMap seriesPictureMapTmp:seriesPictureMapList )
    			{
    			    targettype = seriesPictureMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (seriesPictureMapTmp.getTargetstatus()!=0)
    				{
                	    resultPubStr.append(DELPUBTARGETSYSTEM
              	    	+seriesPictureMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                        +"  "); 
                	    isstatus = false;
    				}
    				if (isstatus)
    				{
                    boolean isxml = true;
                   	if (targettype ==2&&(seriesPictureMapTmp.getStatus()==300||seriesPictureMapTmp.getStatus()==500))  //EPG系统
                   	{
                   		String xml;
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 3); //取消发布到EPG                       
                        if (null == xml)
                        {
                            isxml = false;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                  	    	+seriesPictureMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                            +"  ");                               
                    	}  
                        if (isxml)
                        {
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long targetindex =seriesPictureMapTmp.getTargetindex();
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectPicindex = seriesPictureMapTmp.getPictureindex();
                        String objectid = seriesPictureMapTmp.getPictureid();
                        String objectcode = seriesPictureMapTmp.getPicturecode();
                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = seriesPictureMapTmp.getMappingid();
                        String parentid = seriesPictureMapTmp.getPictureid();
                        String  elementid = seriesPictureMapTmp.getSeriesid();
                        Long objectindex = seriesPictureMapTmp.getMapindex();
                        insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                        insertSyncPicTask(index,xml,correlateid,targetindex); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(seriesPictureMapTmp.getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                            CommonLogUtil.insertOperatorLog(seriesPictureMapTmp.getMappingid()+","+seriesPictureMapTmp.getTargetid(), 
                            		CommonLogConstant.MGTTYPE_SERIESSYN,
                                CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                        sucessPub=true;
                    	       resultPubStr.append(DELPUBTARGETSYSTEM
                    	    		+seriesPictureMapTmp.getTargetid()+delPub+": "
                                    +operSucess+"  "); 
                        }
                   }                       		
    			} 
    		  }
    	    }
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			   seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
       	    	   if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   sereispicmap.put("pictureMap", seriesPictureMapList);
                   int targettype = 0 ;

         			for (SeriesPictureMap seriesPictureMapTmp:seriesPictureMapList )
           			{
                        targettype = seriesPictureMapTmp.getTargettype();
        				boolean isstatus = true;
        				if (seriesPictureMapTmp.getTargetstatus()!=0)
        				{
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                  	    	+seriesPictureMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                            +"  "); 
                    	    isstatus = false;
        				}
        				if (isstatus)
           			{
         				boolean isxml = true;
                        if ((seriesPictureMapTmp.getStatus()==300||seriesPictureMapTmp.getStatus()==500))  //2.0平台网元
                        {
                              String xml;
                              xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 3); //新增发布
                              if (null == xml)
                              {
                                  isxml = false;
                          	      resultPubStr.append(DELPUBTARGETSYSTEM
                        	    	+seriesPictureMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                                  +"  ");                               
                          	  }  
                              if (isxml)
                              {
                              String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                              Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                              Long targetindex =seriesPictureMapTmp.getTargetindex();
                              Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                              Long objectPicindex = seriesPictureMapTmp.getPictureindex();
                              String objectid = seriesPictureMapTmp.getPictureid();
                              String objectcode = seriesPictureMapTmp.getPicturecode();
                              insertObjectRecord( syncindex, index, objectPicindex, objectid,10, targetindex, synType,objectcode,1);  //插入对象日志发布表
                              Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                              String objectidMapping = seriesPictureMapTmp.getMappingid();
                              String parentid = seriesPictureMapTmp.getPictureid();
                              String  elementid = seriesPictureMapTmp.getSeriesid();
                              Long objectindex = seriesPictureMapTmp.getMapindex();
                              String elementcode = seriesPictureMapTmp.getCpcontentid();
                              String parentcode = objectcode;
                              insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                              insertSyncPicTask(index,xml,correlateid,targetindex); //插入同步任务
                              cntTargetSyncTemp.setSyncindex(seriesPictureMapTmp.getSyncindex());
                              cntTargetSyncTemp.setOperresult(70);                          
                              cntTargetSyncTemp.setStatus(200);
                              cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                  CommonLogUtil.insertOperatorLog(seriesPictureMapTmp.getMappingid()+","+seriesPictureMapTmp.getTargetid(), 
                                  		CommonLogConstant.MGTTYPE_SERIESSYN,
                                      CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL_INFO,
                                      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                              sucessPub=true;
                          	      resultPubStr.append(DELPUBTARGETSYSTEM
                          	    		+seriesPictureMapTmp.getTargetid()+delPub+": "
                                          +operSucess+"  "); 
                              }
                              }
           				} 
           				} 
    	       }
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();
    	return rtnPubStr;   
    	}  
    
    public String publishseriesPicMapTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
    		seriesPictureMap.setMapindex(Long.parseLong(mapindex));
    		seriesPictureMap.setTargetindex(Long.parseLong(targetindex));
    		seriesPictureMap.setObjecttype(38);
			List<SeriesPictureMap> seriesPictureMapList = new ArrayList<SeriesPictureMap>();
			
			seriesPictureMapList = seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);
			if (seriesPictureMapList == null ||seriesPictureMapList.size()==0)
			{
				return "1:操作失败,连续剧关联海报记录不存在";
			}
    		int synType = 1;
            String initresult="";
    		initresult =initDataPic(seriesPictureMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(seriesPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
                picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}     		
	    	sereispicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(38);
	    	sereispicmap.put("type", typeList);
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(seriesPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(38);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesPictureMap seriesPictureMapObj=new SeriesPictureMap();
            int  platformType = 1;
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesPictureMapObj.setTargetindex(Long.parseLong(targetindex));
    			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);

    	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (seriesPictureMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
                int targettype = seriesPictureMapList.get(0).getTargettype();

    	    	sereispicmap.put("pictureMap", seriesPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
				cntTargetSync.setObjectindex(seriesPictureMapList.get(0).getSeriesindex());
				cntTargetSync.setTargetindex(seriesPictureMapList.get(0).getTargetindex());
				cntTargetSync.setObjecttype(4);
				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
				{
                    return "1:操作失败,该连续剧没有发布到该网元";
            	}
                int synTargetType =1;
                if (seriesPictureMapList.get(0).getStatus()== 500) //修改发布    
                {
            		synTargetType =2;
            		synType = 2;
                }
               	if (targettype ==2)  //EPG系统
               	{
               		String xml;
               		if (synTargetType==1)
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 1); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 2); //修改发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}    					
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long objectPicindex = seriesPictureMapList.get(0).getPictureindex();
                    String objectid = seriesPictureMapList.get(0).getPictureid();
                    String objectcode = seriesPictureMapList.get(0).getPicturecode();
                    insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = seriesPictureMapList.get(0).getMappingid();
                    String parentid = seriesPictureMapList.get(0).getPictureid();
                    String  elementid = seriesPictureMapList.get(0).getSeriesid();
                    Long objectindex = seriesPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    insertSyncPicTask(index,xml,correlateid,Long.valueOf(targetindex)); //插入同步任务
                    cntTargetSyncTemp.setSyncindex(seriesPictureMapList.get(0).getSyncindex());
                    if (synTargetType ==1)
                    {
                    	cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                    	cntTargetSyncTemp.setOperresult(40);
                    }                            
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(seriesPictureMapList.get(0).getMappingid()+","+
                    seriesPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                            CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			seriesPictureMapObj.setTargetindex(Long.parseLong(targetindex));
        			
        			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
        	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	if (seriesPictureMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
        	    	int targettype = seriesPictureMapList.get(0).getTargettype();
        	    	sereispicmap.put("pictureMap", seriesPictureMapList);
                    CntTargetSync cntTargetSync = new CntTargetSync();
    				cntTargetSync.setObjectindex(seriesPictureMapList.get(0).getSeriesindex());
    				cntTargetSync.setTargetindex(seriesPictureMapList.get(0).getTargetindex());
    				cntTargetSync.setObjecttype(4);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                        return "1:操作失败,该连续剧没有发布到该网元";
                	}
                    int synTargetType =1;
                    if (seriesPictureMapList.get(0).getStatus()== 500) //修改发布    
                    {
                		synTargetType =2;
        	            synType = 2;
                    }

                   		String xml;
               			if (synTargetType ==1)
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 1); //新增发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }
               			}
               			else
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 2); //修改发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }
               			}
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectPicindex = seriesPictureMapList.get(0).getPictureindex();
                        String objectid = seriesPictureMapList.get(0).getPictureid();
                        String objectcode = seriesPictureMapList.get(0).getPicturecode();
                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = seriesPictureMapList.get(0).getMappingid();
                        String parentid = seriesPictureMapList.get(0).getPictureid();
                        String  elementid = seriesPictureMapList.get(0).getSeriesid();
                        Long objectindex = seriesPictureMapList.get(0).getMapindex();
                        String elementcode = seriesPictureMapList.get(0).getCpcontentid();
                        String parentcode = objectcode;
                        insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        insertSyncPicTask(index,xml,correlateid,Long.valueOf(targetindex)); //插入同步任务
                        cntTargetSyncTemp.setSyncindex(seriesPictureMapList.get(0).getSyncindex());
                        if (synTargetType ==1)
                        {
                        	cntTargetSyncTemp.setOperresult(10);
                        }
                        else
                        {
                        	cntTargetSyncTemp.setOperresult(40);
                        }                            
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(seriesPictureMapList.get(0).getMappingid()+","+
                                seriesPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_SERIESSYN,
                                CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     

                }
   			   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	   targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(seriesPictureMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result; 
    }
    public String delPublishseriesPicMapTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
    		seriesPictureMap.setMapindex(Long.parseLong(mapindex));
    		seriesPictureMap.setTargetindex(Long.parseLong(targetindex));
    		seriesPictureMap.setObjecttype(38);
			List<SeriesPictureMap> seriesPictureMapList = new ArrayList<SeriesPictureMap>();
			seriesPictureMapList = seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);

			if (seriesPictureMapList == null ||seriesPictureMapList.size()==0)
			{
				return "1:操作失败,连续剧关联海报记录不存在";
			}
            String initresult="";
            int synType = 3;
    		initresult =initDataPic(seriesPictureMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
    		List<Picture> pictureList = new ArrayList<Picture>();
     		Picture picture = new Picture();
     		picture.setPictureindex(seriesPictureMapList.get(0).getPictureindex());
    		pictureList=pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
    		for (Picture picturetmp:pictureList)
    		{
    			picturetmp.setPictureurl(picturetmp.getPictureurl());
    		}     		    		
	    	sereispicmap.put("picture",pictureList);
	    	List<Integer> typeList = new ArrayList();
	    	typeList.add(38);
	    	sereispicmap.put("type", typeList);
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(seriesPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(38);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            SeriesPictureMap seriesPictureMapObj=new SeriesPictureMap();
            int  platformType = 1;
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			seriesPictureMapObj.setTargetindex(Long.parseLong(targetindex));
    			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
    	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (seriesPictureMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	int targettype = seriesPictureMapList.get(0).getTargettype();
    	    	sereispicmap.put("pictureMap", seriesPictureMapList);
               	if (targettype ==2)  //EPG系统
               	{
               		String xml;
                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(sereispicmap, targettype, 10, 3); //新增发布到EPG
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                    Long objectPicindex = seriesPictureMapList.get(0).getPictureindex();
                    String objectid = seriesPictureMapList.get(0).getPictureid();
                    String objectcode = seriesPictureMapList.get(0).getPicturecode();
                    insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = seriesPictureMapList.get(0).getMappingid();
                    String parentid = seriesPictureMapList.get(0).getPictureid();
                    String  elementid = seriesPictureMapList.get(0).getSeriesid();
                    Long objectindex = seriesPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    insertSyncPicTask(index,xml,correlateid,Long.valueOf(targetindex)); //插入同步任务                    
                    cntTargetSyncTemp.setSyncindex(seriesPictureMapList.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);                          
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(seriesPictureMapList.get(0).getMappingid()+","+
                    seriesPictureMapList.get(0).getTargetid(), 
                    		CommonLogConstant.MGTTYPE_SERIESSYN,
                            CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			seriesPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			seriesPictureMapObj.setTargetindex(Long.parseLong(targetindex));
        			seriesPictureMapList = seriesPictureMapDS.getSeriesnormalPictureMapByCond(seriesPictureMapObj);
        	    	if (seriesPictureMapList == null || seriesPictureMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	if (seriesPictureMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
                    int targettype = seriesPictureMapList.get(0).getTargettype();

        	    	sereispicmap.put("pictureMap", seriesPictureMapList);

                   		String xml;
                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(sereispicmap, targettype, 10, 3); //取消发布
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); //获取taskindex
                        Long objectPicindex = seriesPictureMapList.get(0).getPictureindex();
                        String objectid = seriesPictureMapList.get(0).getPictureid();
                        String objectcode = seriesPictureMapList.get(0).getPicturecode();
                        insertObjectRecord( syncindex, index, objectPicindex, objectid,10, Long.valueOf(targetindex), synType,objectcode,1);  //插入对象日志发布表
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = seriesPictureMapList.get(0).getMappingid();
                        String parentid = seriesPictureMapList.get(0).getPictureid();
                        String  elementid = seriesPictureMapList.get(0).getSeriesid();
                        Long objectindex = seriesPictureMapList.get(0).getMapindex();
                        String elementcode = seriesPictureMapList.get(0).getCpcontentid();
                        String parentcode = objectcode;
                        insertPicMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, 
                        		parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        insertSyncPicTask(index,xml,correlateid,Long.valueOf(targetindex)); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(seriesPictureMapList.get(0).getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);      
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(seriesPictureMapList.get(0).getMappingid()+","+seriesPictureMapList.get(0).getTargetid(),
                        		CommonLogConstant.MGTTYPE_SERIESSYN,
                                CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     

                }
   			   List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(seriesPictureMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return result; 
    }
    public String initDataPic(SeriesPictureMap seriesPictureMap,String targetindex,int platfType,int synType)throws Exception
    {
    	try
    	{
    		CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(seriesPictureMap.getMapindex());
			cntPlatformSync.setObjecttype(38);
			CmsSeries cmsSeries = new CmsSeries();
			cmsSeries.setSeriesindex(seriesPictureMap.getSeriesindex());
			cmsSeries =cmsSeriesDS.getCmsSeries(cmsSeries);
    		if (cmsSeries == null)
    		{
                return "1:操作失败,连续剧不存在";
    		}
    		Picture picture = new Picture();
    		picture.setPictureindex(seriesPictureMap.getPictureindex());
    		picture=pictureDS.getPicture(picture);
    		if (picture == null)
    		{
                return "1:操作失败,海报不存在";
    		}
    		if(platfType == 1 ||platfType == 2) //发布到3.0平台 或者2.0平台
    		{
        		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
        		if (platfType == 1)
        		{
        			cntPlatformSync.setPlatform(2);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
        		if (platfType == 2)
        		{
        			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
    			if (synType == 1) //发布
    			{
    				if(cntPlatformSynctmp.getStatus()!=0&&cntPlatformSynctmp.getStatus()!=400&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}	
    			}
    			else  //取消发布
    			{   
    				if(cntPlatformSynctmp.getStatus()!=300&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                if (platfType == 1)
                {
                    cntTargetSyncTemp.setTargettype(2); //海报只发布到EPG
                }
                else
                {
                    cntTargetSyncTemp.setPlatform(1); // 海报只发布到2.0网元
                }
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null||cntTargetSyncList.size()==0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType ==1)
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==0||cntTargetSync.getStatus()==400||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";
                	}
                }
                else
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==300||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";
                	}
                }                
    		}
    		if (platfType == 3) //发布到网元
    		{
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
    			CntTargetSync cntTargetSync = new CntTargetSync();
    			cntTargetSync.setObjectindex(seriesPictureMap.getMapindex());
    			cntTargetSync.setObjecttype(38);
    			cntTargetSync.setTargetindex(Long.valueOf(targetindex));
    			cntTargetSyncListTemp =cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
    			if (cntTargetSyncListTemp==null ||cntTargetSyncListTemp.size()==0)
    			{
                    return "1:操作失败,发布记录不存在";
    			}
    			if (synType == 1) //发布
    			{
    				if(cntTargetSyncListTemp.get(0).getStatus()!=0&&cntTargetSyncListTemp.get(0).getStatus()!=400
    						&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				if(cntTargetSyncListTemp.get(0).getStatus()!=300&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布状态不正确";
    				}
    			}
    		}

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	return "success";
    }
    private void insertPicMappingRecord(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(38);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
     
    
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype, Long targetindex,int synType,String objectcode,int platType )
    {
    	ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncRecord.setSyncindex(syncindex);
        	objectSyncRecord.setTaskindex(taskindex);
        	objectSyncRecord.setObjectindex(objectindex);
        	objectSyncRecord.setObjectid(objectid);
        	objectSyncRecord.setDestindex(targetindex);
        	objectSyncRecord.setObjecttype(objecttype);
        	objectSyncRecord.setActiontype(synType);
        	objectSyncRecord.setObjectcode(objectcode);
        	objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecord.setObjectcode(objectcode);
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
    
    private void insertSyncPicTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex)
    {
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
    	CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setBatchid(batchid);
        	cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    
    private void insertPicMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindexMovie, String objectidMapping, 
    		String elementid, String parentid,String elmentcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindexMovie);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(38);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elmentcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }  	
	   
    
    /****************************连续剧海报发布 end*******************************/
}

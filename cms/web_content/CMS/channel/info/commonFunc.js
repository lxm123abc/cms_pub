    //特殊字符过滤
	function filtrate(object){
        object.value = object.value.replace(/[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\<\>\/?]/g,'');
	}    
    
	//日期格式转换成字符串
    function formatDBDate(str, len) {
        str = str.replace(/:/g,"");<!-- replace :     -->
        str = str.replace(/ /g,"");<!-- replace blank -->
        str = str.replace(/-/g,"");<!-- replace -     -->
        if (str == "") {
            str = "00000000000000";
        }
        return str.substr(0, len);
    }

    //字符串转换成日期格式
	function formatFromDBDate(str) {
	    var result = "";
	    str = trim(str);
	    if (str.length == 0 || str.charAt(0) == '0')
	        return result;
	        
	    if (str.length == 10 || str.length == 19)
	        return str;
	    
	    result = str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    if (str.length == 14)
	        result += " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	        
	    return result;
	}

    //消除空格
    function trim(str){
        return ltrim(rtrim(str));
    }
    function ltrim(str){
        var sRet="";
        regular = /\s*(\S*)/; 
        sRet=str.replace(regular, "$1");
        return sRet;
    }
    function rtrim(str){
        var sRet="";
        regular = /(\S*)\s*$/; 
        sRet=str.replace(regular, "$1");
        return sRet;
    }
    
    //得到字符串的长度（汉字按3个字符计）
    function getstrlength(str){
        var l=str.length;
        var n=l;
        for(var i=0;i<l;i++){
    		    if(str.charCodeAt(i)<0 || str.charCodeAt(i)>255){
    		    	n+=2;
    		    }
    	  }
        return n;
    }
    
    //判断字符串是否由数字或字母组成
    function isNumOrChar(str){
        var pattern = /[^a-zA-Z0-9]/;
		if(pattern.test(str)){
			return false;
		}else{
			return true;
		}
    }
    
    //检查字符串是否是纯数字
    function isUnsignedInteger(strInteger) { 
        return (strInteger.search(/^\d+$/) != -1); 
    }

	function checkNull(inputStr) {
	
	    return ( Trim(inputStr).length==0);
	
	}
	

(function($) {
  $.regexpCommon = function(regexpDesc) {
    return $.regexpCommon.regexpPattern[regexpDesc].call();
  };

  $.regexpCommon.regexpPattern = {
    // numbers
    numberInteger : function() {
      return /^[-+]?[1-9]\d*\.?[0]*$/;
    },
    numberFloat : function() {
      return /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/;
    },
    // email
    email : function() {
      return /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    },
    ssn : function() {
      return /^\d{3}-\d{2}-\d{4}$/;
    },
    url : function() {
      return /^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$/;
    },
    phoneNumberUS : function() {
      return /^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$/;
    },
    zipCodeUS : function() {
      return /^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d)$/;
    },
    currencyUS : function() {
      return /^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/;
    }, 
    htmlHexCode : function() {
      return /^#([a-fA-F0-9]){3}(([a-fA-F0-9]){3})?$/;
    },
    dottedQuadIP : function() {
      return /^(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4] \d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4] \d|25[0-5])$/;
    },
    macAddress : function() {
      return /^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$/;
    }
  };
}) (jQuery);

//判断是否包含有中文
function hasChineseChars(sCheck){
  for (var nIndex=0; nIndex<sCheck.length; nIndex++){
     cCheck = sCheck.charCodeAt(nIndex);
     if (cCheck > 255)
       return true;
  }
  return false;
}

//判断下拉列表是否选中
function checkSelectVal(id){
  var value=$('#'+id).val();
  if(value==""){
    $('#'+id+'_info').text("请选择");
    return false;
  }
  return true;
}

//设置下拉列表选中值
function setSelectVal(id,val){
  var objs=document.getElementById(id);
  for(var i=0;i<objs.options.length;i++){
    if(val==objs.options[i].value){
      objs.options[i].selected=true;
      break;
    }
  }
}

//判断单选按钮是否选中
function checkRadioVal(name,msgId){
  var objs=document.getElementsByName(name);
  var list=new Array();
  for(var i=0;i<objs.length;i++){
    if(objs[i].checked){
	  list.push(objs[i].value);
	}
  }
  if(list.length<1){
	$('#'+msgId).text="此项不能为空";
    return false;
  }
  return true;
}

//设置单选按钮选中值
function setRadioVal(name,val){
  var objs=document.getElementsByName(name);
  for(var i=0;i<objs.length;i++){
    if(objs[i].value==val){
	  objs[i].checked=true;
	  break;
	}
  }
}

//清空所有错误消息
function clearAllMsg(){
  $("label[id$='_info']").html("");
}

//清空所有查询条件
function clearAllCondition(){
  $("input[id$='_soso']").val("");
}

//判断是否是以某字符为开头
function startWithStr(id,str){
  var value=$('#'+id).val();
  var result=false;
  if(str==null||str==""||str.length>this.length||value.length==0)
   return false;
  if(value.substr(0,str.length)==str)
     return true;
  else
     return false;
  return true;
}

//判断数组中有没有某元素
function hashVal(array,val){
  var result=-1;
  for(var i=0;i<array.length;i++){
    if(array[i]==val){
      result=i;
    }
  }
  return result;
}

//移除数组中的元素
function delVal(array,val){
  var i=hashVal(array,val);
  if(i!=-1)
    array.splice(i,1);
}

//去除数组中的重复元素
function distinct(arr){
  var rs=new Array();
  for(var i=0;i<arr.length;i++){
    var s=","+rs.join(",")+",";
	if(!s.match(","+arr[i]+",")){
      rs.push(arr[i]);
    }
  }
  return rs;
}

function getTime2(str){
  if(str!=null){
	if (str.length > 6){
	  return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	}else{
	  return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
    }
  }else{
    return "";
  }
}

var historyStr ="";
function checkMaxInput2(_this,maxleng,leftInforId) 
{
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 

    var remainnum =parseInt(maxleng)-len; 
 
    if(remainnum < 0) 
    {
       _this.value = historyStr;
       return ;
    } 
    historyStr = _this.value;
    remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum;  
}

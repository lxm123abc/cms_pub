package com.zte.cms.content.dao;

import java.util.ArrayList;
import java.util.List;

import com.zte.cms.content.model.IcmTagContent;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class IcmTagContentDAO extends DynamicObjectBaseDao implements IIcmTagContentDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmTagContent(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("insert icmTagContent starting...");
        super.insert("insertIcmTagContent", icmTagContent);
        log.debug("insert icmTagContent end");
    }

    public void updateIcmTagContent(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("update icmTagContent by pk starting...");
        super.update("updateIcmTagContent", icmTagContent);
        log.debug("update icmTagContent by pk end");
    }

    public void deleteIcmTagContent(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("delete icmTagContent by pk starting...");
        super.delete("deleteIcmTagContent", icmTagContent);
        log.debug("delete icmTagContent by pk end");
    }

    public IcmTagContent getIcmTagContent(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("query icmTagContent starting...");
        IcmTagContent resultObj = (IcmTagContent) super.queryForObject("getIcmTagContent", icmTagContent);
        log.debug("query icmTagContent end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<IcmTagContent> getIcmTagContentByCond(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("query icmTagContent by condition starting...");
        List<IcmTagContent> rList = (List<IcmTagContent>) super.queryForList("queryIcmTagContentListByCond",
                icmTagContent);
        log.debug("query icmTagContent by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmTagContent icmTagContent, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryIcmTagContentListByCond", "queryIcmTagContentListCntByCond", icmTagContent,
                start, pageSize, puEntity);
    }

    public void removeTagContentByTagindex(Long tagindex) throws DAOException
    {
        log.debug("remove TagContent By Tagindex starting...");
        super.delete("removeTagContentByTagIndex", tagindex);
        log.debug("remove TagContent By Tagindex end");
    }

    public void removeRelationByTagindexAndConIDs(IcmTagContent icmTagContent) throws DAOException
    {
        log.debug("remove Relation By Tagindex And ConIDs startind...");
        super.delete("delIcmTagContentByTagindexAndContentid", icmTagContent);
        log.debug("remove Relation By Tagindex And ConIDs end");

    }

    @SuppressWarnings("unchecked")
    public List<String> getConCpidListByTagIndex(Long tagindex) throws DAOException
    {
        log.debug("getConCpidListByTagIndex starting...");
        List<String> cpidList = (List<String>) super.queryForList("getCpidListFromRelation", tagindex);
        if (cpidList == null)
        {
            cpidList = new ArrayList<String>();
        }
        log.debug("getConCpidListByTagIndex end");
        return cpidList;
    }

}
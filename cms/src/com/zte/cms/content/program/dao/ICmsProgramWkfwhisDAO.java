package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsProgramWkfwhisDAO
{
    /**
     * 新增CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void insertCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 新增CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void insertCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException;

    /**
     * 根据主键更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void updateCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 根据主键更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void updateCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException;

    /**
     * 根据条件更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 根据条件更新CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException;

    /**
     * 根据主键删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 根据主键删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramWkfwhisList(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException;

    /**
     * 根据条件删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 根据条件删除CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramWkfwhisListByCond(List<CmsProgramWkfwhis> cmsProgramWkfwhisList) throws DAOException;

    /**
     * 根据主键查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @return 满足条件的CmsProgramWkfwhis对象
     * @throws DAOException dao异常
     */
    public CmsProgramWkfwhis getCmsProgramWkfwhis(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    /**
     * 根据条件查询CmsProgramWkfwhis对象
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @return 满足条件的CmsProgramWkfwhis对象集
     * @throws DAOException dao异常
     */
    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis) throws DAOException;

    public List<CmsProgramWkfwhis> getCmsProgramWkfwhisByCond(CmsProgramWkfwhis cmsProgramWkfwhis,
            PageUtilEntity puEntity) throws DAOException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象，作为查询条件的参数
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsProgramWkfwhis对象，作为查询条件的参数
     * 
     * @param cmsProgramWkfwhis CmsProgramWkfwhis对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgramWkfwhis cmsProgramWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
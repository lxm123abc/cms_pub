package com.zte.cms.channel.wkfw.service;

import java.util.List;

import com.zte.cms.channel.wkfw.dao.IChannelWkfwProcessWithMeDAO;
import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.model.ChannelWkfwProcessWithMe;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ChannelWkfwProcessWithMeDS implements IChannelWkfwProcessWithMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IChannelWkfwProcessWithMeDAO channelWkfwProcessWithMedao = null;

    public void setChannelWkfwProcessWithMedao(IChannelWkfwProcessWithMeDAO channelWkfwProcessWithMedao)
    {
        this.channelWkfwProcessWithMedao = channelWkfwProcessWithMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#queryStrategyWkfwProcessWithMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryChannelWkfwProcessWithMeListCntByCond(ChannelWkfwCondition channelWkfwProcessWithMeCondition)
            throws DomainServiceException
    {
        log.debug("query ChannelWkfwProcessWithMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(channelWkfwProcessWithMeCondition);
            totalCnt = channelWkfwProcessWithMedao
                    .queryChannelWkfwProcessWithMeListCntByCond(channelWkfwProcessWithMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query ChannelWkfwProcessWithMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get ChannelWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(channelWkfwProcessWithMeCondition);
            pageInfo = channelWkfwProcessWithMedao.pageInfoQuery(channelWkfwProcessWithMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get ChannelWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get ChannelWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(channelWkfwProcessWithMeCondition);
            pageInfo = channelWkfwProcessWithMedao.pageInfoQuery(channelWkfwProcessWithMeCondition, start, pageSize,
                    puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ChannelWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ChannelWkfwCondition channelWkfwProcessWithMeCondition)
    {
        channelWkfwProcessWithMeCondition.setChannelid(EspecialCharMgt.conversion(channelWkfwProcessWithMeCondition
                .getChannelid()));

        channelWkfwProcessWithMeCondition.setChannelname(EspecialCharMgt.conversion(channelWkfwProcessWithMeCondition
                .getChannelname()));
    }
}

package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.common.DbUtil;
import com.zte.cms.cpsp.service.FtpUploadService;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.cms.settlementDataRpt.common.CommonReturnType;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;
import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;
import com.zte.cms.settlementDataRpt.service.SettlementEntDownFileService;
import com.zte.ismp.systemconfig.ls.IIsysConfigLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:27 2019/6/28
 * @Description:
 */
public class SettlementEntDownFileControllerImpl implements SettlementEntDownFileController {

    private SettlementEntDownFileService settlementDataRptService;

    public void setSettlementDataRptService(SettlementEntDownFileService settlementDataRptService) {
        this.settlementDataRptService = settlementDataRptService;
    }

    @Override
    public CommonReturnType insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile, String uploadResFileCtrl) throws DAOException {
        try {
            settlementDataRptService.insertSettlementEntDownFile(settlementEntDownFile);
        } catch (DAOException e) {
            e.printStackTrace();
            return CommonReturnType.create("新增失败");
        }
        return CommonReturnType.create("新增成功");
    }

    @Override
    public CommonReturnType updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile, String uploadResFileCtrl) throws DAOException {
        try {
            settlementDataRptService.updateSettlementEntDownFile(settlementEntDownFile);
        } catch (DAOException e) {
            e.printStackTrace();
            return CommonReturnType.create("修改失败");
        }
        return CommonReturnType.create("修改成功");
    }

    @Override
    public SettlementEntDownFile selectSettlementEntDownFileBySpid(String spid) throws DAOException {
        return settlementDataRptService.selectSettlementEntDownFileBySpid(spid);
    }

    @Override
    public CommonReturnType deleteSettlementEntDownFileName(SettlementEntDownFile settlementEntDownFile) throws DAOException {
        String operfilename = settlementEntDownFile.getOperfilename();
        if (StringUtils.isEmpty(operfilename)){
            return CommonReturnType.create("未选择删除目标","fail");
        }
        String filename = settlementEntDownFile.getFilename();
        settlementEntDownFile.setFilename(filename.replaceAll(operfilename + "\\|",""));
        try {
            settlementDataRptService.updateSettlementEntDownFile(settlementEntDownFile);
            settlementDataRptService.deleteAttach(settlementEntDownFile);
        } catch (DAOException e) {
            e.printStackTrace();
            return CommonReturnType.create("删除失败");
        }
        return CommonReturnType.create("删除成功");
    }

    @Override
    public CommonReturnType getSettlementCommonFile() throws DomainServiceException {
        //通过系统配置项获取目标FTP服务器的ip port user password
        IIsysConfigLS configLs = (IIsysConfigLS)SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.SettlementDataRpt.commonFile");
        if (StringUtils.isEmpty(ftpaddress)){
            return CommonReturnType.create(null,"fail");
        }
        String[] split = ftpaddress.split("\\|");
        return CommonReturnType.create(split);
    }

    @Override
    public CommonReturnType uploadCommonFile() throws IOException, DomainServiceException {
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        for (FileItem fileItem : fileList) {
            String newname = UUID.randomUUID().toString().replaceAll("-", "");
            String path = fileItem.getName();
            String oldname = "";
            if (path.indexOf("/") != -1) {
                //linux
                oldname = path.substring(path.lastIndexOf("/") + 1);
            } else {
                //windows
                oldname = path.substring(path.lastIndexOf("\\") + 1);
            }
            String suffix = oldname.substring(oldname.indexOf("."));
            newname += suffix;
            try {
                FtpAttachment.upload2FTP(fileItem,newname);

                StringBuffer sb = new StringBuffer();
                sb.append("update zxdbm_umap.usys_config set cfgvalue='");
                sb.append(oldname.concat("|").concat(newname));
                sb.append("' where cfgkey='cms.SettlementDataRpt.commonFile'");
                DbUtil dbutil = new DbUtil();
                dbutil.excute(sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
                return CommonReturnType.create("上传失败");
            }

        }
        return CommonReturnType.create("上传成功");
    }
}

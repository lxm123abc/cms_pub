
function checkServerInfo(){  
   var flag = true;
   var servername = document.getElementById("servername");
   if (checkNull(servername.value)) {
      document.getElementById("servername_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null");
     // servername.focus();    
     // return false;
      flag = false;
    }else if (CheckSpecialString2(servername,"servername_msg") == false){
      //  return false;
    //  servername.focus(); 
      flag = false;
    } else if(checkMaxLength(servername,100,"servername_msg") == false){
     //   return false;
    // servername.focus(); 
     flag = false;
    }
    
   var inipaddress = document.getElementById("inipaddress");
   if (checkNull(inipaddress.value)) {
      document.getElementById("inipaddress_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); 
    //  inipaddress.focus();
     // return false;
      flag = false;
    } else if (checkIpFormat(inipaddress, "inipaddress_msg") == false) {
   //  return false;
     flag = false;
    }

  /* var outipaddress =  document.getElementById("outipaddress");
   if (checkNull(outipaddress.value)) {
      document.getElementById("outipaddress_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); 
      outipaddress.focus();
      return false;
    }  
    if (checkIpFormat(outipaddress, "outipaddress_msg") == false){ 
      return false;

    }*/
    var ftpaccount = document.getElementById("ftpaccount");
    if (checkNull(ftpaccount.value)){
        document.getElementById("ftpaccount_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); 
       // ftpaccount.focus();
       // return false;
       flag = false;
    }else if (CheckSpecialString(ftpaccount,"ftpaccount_msg") == false){
      //  return false;
      flag = false;
    }else if(checkMaxLength(ftpaccount,100,"ftpaccount_msg") == false){
      //  return false;
      flag = false;
    }
    var ftppassword = document.getElementById("ftppassword");
    if (ftppassword != null && trim(ftppassword.value) == ""){
        document.getElementById("ftppassword_msg").innerHTML= $res_entry("msg.info.server.item.cannot.null"); 
       // ftppassword.focus();
       // return false;
       flag = false;
    }
  //  var exp = new RegExp("[A-Za-z0-9-_]");
  //  var password = ftppassword.value;
  //  for (var i = 0 ; i < password.length ; i++){
   //     var reg = password.charAt(i).match(exp);
   //     if (reg == null) {
    //       document.getElementById("ftppassword_msg").innerHTML=$res_entry("msg.info.server.ftppassword.invalid");
  //         ftppassword.focus();
   //        return false;
    //    }
   // }
    else if (checkMaxLength(ftppassword,100,"ftppassword_msg") == false){
       // return false;
       flag = false;
    }
    var description = document.getElementById("description");
    if(!checkMaxLength(description,255,"description_msg")){
    	//return false;
    	flag = false;
	}
    return flag;
}

    function checkMaxLength(input,len,input_msg){	
        if (utf8_strlen2(trim(input.value)) > len){
            document.getElementById(input_msg).innerText = $res_entry("msg.info.server.item.toolong")+len+$res_entry("msg.info.server.item.toolong.tail");
            //document.getElementById(input.id).focus();
            return false;
        }else{
           document.getElementById(input_msg).innerText = "";   
           return true;   
        }
    }

function checkIpFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
   
     var reg = checkIPaddr(trim(input.value));
     if(!reg){
        document.getElementById(input_msg).innerText = $res_entry("msg.info.server.ipformat.invalid"); 
       // document.getElementById(input.id).focus();
        return false;         
     }else{
        document.getElementById(input_msg).innerText = ""; 
    }   
  
   return true; 
  }
}
    function checkPath(path){
        var first = path.charAt(0);
        var last = path.charAt(path.length-1);
        if (first!='/'){
           return false;
        }
        if (last!='/'){
            return false;
        }
        if (path.indexOf('//')!=-1){
            return false;
        }
        return true;
    } 

    function checkPort(port){   
        var newPar=/^\d+$/;   
        return newPar.test(port);
    }
    
    function checkNull(inputStr)
{
    return ( Trim(inputStr).length==0)
}

function CheckSpecialString2(input,input_msg)
{ 
   if(checkstring("$%'/\\:*&?\"<>|",input.value))
   {
      
      document.getElementById(input_msg).innerText =$res_entry("msg.info.notinclude") +"$ % ' / \\ : * & ? \" < > |";
     
     // input.focus();
    
      return false;

   }else
   {
     return true;
   }
}

function CheckSpecialString(input,input_msg)
{ 
   if(checkstring("~`!@#^()-+={}[];,.$%'/\\:*&?\"<>|",input.value))
   {
      
      document.getElementById(input_msg).innerText =$res_entry("msg.info.notinclude") +"~ ` ! @ # ^ ( ) - + = { } [ ] ; , . $ % ' / \\ : * & ? \" < > |";
     
      //input.focus();
    
      return false;

   }else
   {
     return true;
   }
}
function checkstring(checkstr,userinput)
{
	var allValid = false;	
	for (i = 0;i<userinput.length;i++) {
	    ch = userinput.charAt(i);
	    if(checkstr.indexOf(ch) >= 0) {
	        allValid = true;
	        break;
	      }
	  }
	 return allValid;
}

function checkIPaddr(ipaddr)
{
    if(ipaddr!=""&&ipaddr!=null){
      var reg1=/^(((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|([1-9]))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d))\.((2[0-4]\d)|(25[0-5])|(1\d{2})|([1-9]\d)|(\d)))$/;
      return reg1.test(ipaddr);
    }
 }

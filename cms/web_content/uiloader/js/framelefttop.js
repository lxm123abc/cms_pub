    function document.oncontextmenu()
    {
        event.returnValue = false;
    }
    
    var strColumns_Current = "145,*";
    var strRows_Current    = "22,*,40";
    
    function movenext()
    {
        strRows_Current = window.parent.leftFrameSet.rows;
        window.parent.leftFrameSet.rows = "22,*,0";    		
    }
    
    function moveprevious()
    {
        if (strRows_Current == "22,*,0") strRows_Current="22,*,40";
        window.parent.leftFrameSet.rows = strRows_Current;
    }
      
    function expandall()
    {
        parent.leftup.document.getElementById("BtnExpandAll").click();
    }
      
    function collapseall()
    {

        parent.leftup.document.getElementById("BtnCollapseAll").click();
    }

    function hidetoc()
    {

        strColumns_Current = window.top.mainFrameSet.cols;
        window.top.mainFrameSet.cols = "0,*";
        window.parent.rightup.document.all("showtoc").style.display = "block";      
    }

    function showtoc()
    {
        if (strColumns_Current == "0,*")
        {
            strColumns_Current = "145,*";
        }
        window.top.mainFrameSet.cols = strColumns_Current;
        window.parent.rightup.document.all("showtoc").style.display = "none";
    }
      
    function synctoc()
    {
        parent.location = parent.location.pathname + "?url=" + parent.content.location.pathname + parent.content.location.search + parent.content.location.hash;
    }    
    
    function displaybutton()
    {
        if (window.top.mainFrameSet.cols == "0.*")
        {
            window.parent.rightup.document.all("showtoc").style.display = "block";
        }
    }
    
    function mouseover(item)
    {
        switch (item)
        {
        case "moveprevious" :
            window.status = "向下滚动菜单";
            document.all.imgMovePrevious.src = "../images/moveprevious2.gif";
            break;
        
        case "movenext" :
            window.status = "向上滚动菜单";
            document.all.imgMoveNext.src = "../images/movenext2.gif";
            break;
        
        //case "synctoc" :
        //  window.status = "Synchronize the tree with the current page";
        //  document.all.imgSyncToc.src = "../images/synctoc2.gif"
        //  break;
        
        case "hidetoc" :
            window.status = "关闭对象实体";
            document.all.imgHideToc.src = "../images/hidetoc2.gif"
            break;
        
        case "expandall" :
            window.status = "全部展开";
            document.all.imgExpandAll.src = "../images/expandall2.gif";
            break;
        
        case "collapseall" :
            window.status = "全部收缩";
            document.all.imgCollapseAll.src = "../images/collapseall2.gif";
            break;
        }    
    }
    
    function mouseout(item)
    {
        switch (item)
        {
        case "moveprevious" :
            window.status = "";
            document.all.imgMovePrevious.src = "../images/moveprevious1.gif";
            break;
        
        case "movenext" :
            window.status = "";
            document.all.imgMoveNext.src = "../images/movenext1.gif";
            break;
        
        case "synctoc" :
            window.status = "";
            document.all.imgSyncToc.src = "../images/synctoc1.gif"
            break;
        
        case "hidetoc" :
            window.status = "";
            document.all.imgHideToc.src = "../images/hidetoc1.gif"
            break;
        
        case "expandall" :
            window.status = "";
            document.all.imgExpandAll.src = "../images/expandall1.gif";
            break;
            
        case "collapseall" :
            window.status = "";
            document.all.imgCollapseAll.src = "../images/collapseall1.gif";
            break;  
        }
    }
    
    function selectstart()
    {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
        return false;
    }
    


	var iTemp = 1;
	function mscroll(iScrollNumber,OBJdivObject)
	{
			iTemp          = iScrollNumber;                                       
			var OBJTemp    = parent.leftup.document.getElementById(OBJdivObject); 
			var iScrollTop = OBJTemp.scrollTop																 
			
			if (iTemp==0)
			{
				return;                                                      
			}
			OBJTemp.scrollTop=OBJTemp.scrollTop + iTemp;                       
		if (iScrollTop==OBJTemp.scrollTop)
		{
    		
		return;
		}
			setTimeout("mscroll(iTemp,'" + OBJdivObject + "')",80);	   
	}    
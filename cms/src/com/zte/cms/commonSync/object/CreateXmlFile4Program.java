package com.zte.cms.commonSync.object;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.framework.base.ISysCode;
import com.zte.ismp.common.util.StrUtil;

public class CreateXmlFile4Program extends AbstractCreateXmlFile4Object
{
    private static final String CONTENT_SINGERLANG_CODEKEY="cms_content_singerlang";
    private static final String SERVICE_KEY="CMS";
        
    
    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        String fileName = "program_movie_20wg"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"program"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");       
        List<CmsProgram> programList = (List<CmsProgram>)objectMap.get("programList");
        mapList = new ArrayList<Map>();
        if (null == programList || programList.size()==0)
        {
            return null;
        }
        for (int i =0; i<programList.size(); i++)
        {
            Element progobjectElement = null;                
            Element progpertyElement = null;
            // 元素节点
            progobjectElement = objectsElement.addElement("Object");
            progobjectElement.addAttribute("ElementType", "Program");
            progobjectElement.addAttribute("ID", programList.get(i).getCpcontentid());
            progobjectElement.addAttribute("Code", programList.get(i).getCpcontentid());
            progobjectElement.addAttribute("Action", action);
            
            writeElement(progobjectElement, progpertyElement, "Name", programList.get(i).getNamecn(), true);
            writeElement(progobjectElement, progpertyElement, "OrderNumber", programList.get(i).getOrdernumber(), false);
            writeElement(progobjectElement, progpertyElement, "OriginalName", programList.get(i).getOriginalnamecn(), true);
            writeElement(progobjectElement, progpertyElement, "SortName", programList.get(i).getSortname(), true);
            writeElement(progobjectElement, progpertyElement, "SearchName", programList.get(i).getSearchname(), true);
            writeElement(progobjectElement, progpertyElement, "Genre",programList.get(i).getGenre() , true);
            writeElement(progobjectElement, progpertyElement, "ActorDisplay", programList.get(i).getActors(), true);
            writeElement(progobjectElement, progpertyElement, "WriterDisplay", programList.get(i).getWriters(), true);
            writeElement(progobjectElement, progpertyElement, "OriginalCountry", programList.get(i).getCountry(), false);
            writeElement(progobjectElement, progpertyElement, "Language", langMap.get(programList.get(i).getLanguage()), false);
            writeElement(progobjectElement, progpertyElement, "ReleaseYear", programList.get(i).getReleaseyear(), false);
            writeElement(progobjectElement, progpertyElement, "OrgAirDate", programList.get(i).getOrgairdate(), false);
            writeElement(progobjectElement, progpertyElement, "LicensingWindowStart", programList.get(i).getLicensingstart(), false);
            writeElement(progobjectElement, progpertyElement, "LicensingWindowEnd", programList.get(i).getLicensingend(), false);
            writeElement(progobjectElement, progpertyElement, "DisplayAsNew", programList.get(i).getNewcomedays(), false);
            writeElement(progobjectElement, progpertyElement, "DisplayAsLastChance", programList.get(i).getRemainingdays(), false);
            writeElement(progobjectElement, progpertyElement, "Macrovision", programList.get(i).getMacrovision(), false);
            writeElement(progobjectElement, progpertyElement, "Description", programList.get(i).getDesccn(), true);
            writeElement(progobjectElement, progpertyElement, "PriceTaxIn", programList.get(i).getPricetaxin(), true);
            writeElement(progobjectElement, progpertyElement, "Status", programList.get(i).getIsvalid(), false);
            writeElement(progobjectElement, progpertyElement, "SourceType", programList.get(i).getSourcetype(), false);
            writeElement(progobjectElement, progpertyElement, "SeriesFlag", programList.get(i).getSeriesflag(), false);   
            writeElement(progobjectElement, progpertyElement, "StorageType", "", false);   
            writeElement(progobjectElement, progpertyElement, "rmediacode", "", false);   
            writeElement(progobjectElement, progpertyElement, "Type",programList.get(i).getProgramtype() , false);
            
            writeElement(progobjectElement, progpertyElement, "KeyWords", programList.get(i).getKeywords(), true);
            writeElement(progobjectElement, progpertyElement, "Tags", programList.get(i).getContenttags(), true);
            
            writeElement(progobjectElement, progpertyElement, "Reserve1", programList.get(i).getReserve1(), false);
            writeElement(progobjectElement, progpertyElement, "Reserve2", programList.get(i).getReserve2(), false);
            writeElement(progobjectElement, progpertyElement, "Reserve3", programList.get(i).getReserve3(), false);
            writeElement(progobjectElement, progpertyElement, "Reserve4", programList.get(i).getReserve4(), false);
            writeElement(progobjectElement, progpertyElement, "Reserve5", programList.get(i).getReserve5(), false);
            
            /***
             * 获取发布生成program和movie相应的MAP关系 修改同步时 不带任何maping
             */          
        }       
        List<CmsMovie> movieList = (List<CmsMovie>)objectMap.get("movieList");
        for (int i =0; i<movieList.size(); i++)
        {
            Element movObjectElement = null;
            Element movPertyElement = null;
            // 元素节点
            movObjectElement = objectsElement.addElement("Object");
            movObjectElement.addAttribute("ElementType", "Movie");
            movObjectElement.addAttribute("ID", movieList.get(i).getCpcontentid());
            movObjectElement.addAttribute("Code", movieList.get(i).getCpcontentid());
            movObjectElement.addAttribute("Action", action);

            writeElement(movObjectElement, movPertyElement, "Type",movieList.get(i).getMovietype().toString(), false);
            writeElement(movObjectElement, movPertyElement, "FileURL",movieList.get(i).getFileurl(), true);
            writeElement(movObjectElement, movPertyElement, "SourceDRMType", movieList.get(i).getSourcedrmtype(), false);
            writeElement(movObjectElement, movPertyElement, "DestDRMType", movieList.get(i).getDestdrmtype(), false);
            writeElement(movObjectElement, movPertyElement, "AudioType", movieList.get(i).getAudiotype(), false);
            writeElement(movObjectElement, movPertyElement, "ScreenFormat", movieList.get(i).getScreenformat(), false);
            writeElement(movObjectElement, movPertyElement, "ClosedCaptioning", movieList.get(i).getClosedcaptioning(), false);    
            writeElement(movObjectElement, movPertyElement, "OCSURL", "", false); 
            writeElement(movObjectElement, movPertyElement, "Duration", movieList.get(i).getDuration(), false); 
            writeElement(movObjectElement, movPertyElement, "FileSize", movieList.get(i).getFilesize(), false); 
            writeElement(movObjectElement, movPertyElement, "BitRateType", movieList.get(i).getBitratetype(), false); 
            writeElement(movObjectElement, movPertyElement, "VideoType", movieList.get(i).getVideotype(), false); 
            writeElement(movObjectElement, movPertyElement, "AudieoType", movieList.get(i).getAudiotrack(), false); 
            writeElement(movObjectElement, movPertyElement, "Resolution", movieList.get(i).getResolution(), false); 
            writeElement(movObjectElement, movPertyElement, "VideoProfile", movieList.get(i).getVideoprofile(), false); 
            writeElement(movObjectElement, movPertyElement, "SystemLayer", movieList.get(i).getSystemlayer(), false); 
            /***
             * 获取发布生成program和movie相应的MAP关系 修改同步时 不带任何maping
             */          
               if(syncType!=2){
                   mappingMap = new HashMap<String, String>();
            	   mappingMap.put("ParentID", programList.get(0).getCpcontentid());
                   mappingMap.put("ID", "");
                   //mappingMap.put("ParentID", movieList.get(i).getCpcontentid());
                   //mappingMap.put("ObjectID", objectId);
                   mappingMap.put("ElementID", movieList.get(i).getCpcontentid());
                   mappingMap.put("Type", "");
                   mappingMap.put("Sequence", "");
                   mappingMap.put("ValidStart", "");
                   mappingMap.put("ValidEnd", "");
                   mapList.add(mappingMap);
               }            
        }
        
        // 生成Mappings节点
        Element mappingElement = adiElement.addElement("Mappings");
        if(mapList!=null&&mapList.size()>0)
        {
            for (Map map2 : mapList)
        {
            Element mapObjectElement = null;
            Element mapPertyElement = null;
            mapObjectElement = mappingElement.addElement("Mapping");
            mapObjectElement.addAttribute("ParentType", "Program");
                mapObjectElement.addAttribute("ID", map2.get("ID").toString());
                mapObjectElement.addAttribute("ParentID", map2.get("ParentID").toString());
                mapObjectElement.addAttribute("ParentCode", map2.get("ParentID").toString());
                mapObjectElement.addAttribute("ElementType", "Movie");
                mapObjectElement.addAttribute("ElementID", map2.get("ElementID").toString());
                mapObjectElement.addAttribute("ElementCode", map2.get("ElementID").toString());
            mapObjectElement.addAttribute("Action", action);

            writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
            writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);
            }
        }        
        
        //把对象写入xml文件
        writeDom(dom,fileFullName);
        
        return retPath;
    }

    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;

        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        String fileName = "program_movie_20ys"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"program"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");       
        List<CmsProgram> programList = (List<CmsProgram>)objectMap.get("programList");
        mapList = new ArrayList<Map>();
        if (null == programList || programList.size()==0)
        {
            return null;
        }
        for (int i =0; i<programList.size(); i++)
        {
            Element progobjectElement = null;                
            Element progpertyElement = null;
            // 元素节点
            progobjectElement = objectsElement.addElement("Object");
            progobjectElement.addAttribute("ElementType", "Program");
            progobjectElement.addAttribute("ID", programList.get(i).getCpcontentid());
            progobjectElement.addAttribute("Code", programList.get(i).getCpcontentid());
            progobjectElement.addAttribute("Action", action);
            
            writeElement(progobjectElement, progpertyElement, "Name", programList.get(i).getNamecn(), true);
            writeElement(progobjectElement, progpertyElement, "OrderNumber", programList.get(i).getOrdernumber(), false);
            writeElement(progobjectElement, progpertyElement, "OriginalName", programList.get(i).getOriginalnamecn(), true);
            writeElement(progobjectElement, progpertyElement, "SortName", programList.get(i).getSortname(), true);
            writeElement(progobjectElement, progpertyElement, "SearchName", programList.get(i).getSearchname(), true);
            writeElement(progobjectElement, progpertyElement, "ActorDisplay", programList.get(i).getActors(), true);
            writeElement(progobjectElement, progpertyElement, "WriterDisplay", programList.get(i).getWriters(), true);
            writeElement(progobjectElement, progpertyElement, "OriginalCountry", programList.get(i).getCountry(), false);
            writeElement(progobjectElement, progpertyElement, "Language", langMap.get(programList.get(i).getLanguage()), false);
            writeElement(progobjectElement, progpertyElement, "ReleaseYear", programList.get(i).getReleaseyear(), false);
            writeElement(progobjectElement, progpertyElement, "OrgAirDate", programList.get(i).getOrgairdate(), false);
            writeElement(progobjectElement, progpertyElement, "LicensingWindowStart", programList.get(i).getLicensingstart(), false);
            writeElement(progobjectElement, progpertyElement, "LicensingWindowEnd", programList.get(i).getLicensingend(), false);
            writeElement(progobjectElement, progpertyElement, "DisplayAsNew", programList.get(i).getNewcomedays(), false);
            writeElement(progobjectElement, progpertyElement, "DisplayAsLastChance", programList.get(i).getRemainingdays(), false);
            writeElement(progobjectElement, progpertyElement, "Macrovision", programList.get(i).getMacrovision(), false);
            writeElement(progobjectElement, progpertyElement, "Description", programList.get(i).getDesccn(), true);
            writeElement(progobjectElement, progpertyElement, "PriceTaxIn", programList.get(i).getPricetaxin(), true);
            writeElement(progobjectElement, progpertyElement, "Status", programList.get(i).getIsvalid(), false);
            writeElement(progobjectElement, progpertyElement, "SourceType", programList.get(i).getSourcetype(), false);
            writeElement(progobjectElement, progpertyElement, "SeriesFlag", programList.get(i).getSeriesflag(), false);               
            /**处理插入点播内容与角色的相关的人物名称  begin***/
            List<ProgramCrmMap> programCrmMaplist = (List<ProgramCrmMap>)objectMap.get("programCrmMapList");
            StringBuffer kpeopleStrBuffer = new StringBuffer("") ;
            StringBuffer directorStrBuffer = new StringBuffer("") ;
            if (null != programCrmMaplist && programCrmMaplist.size() > 0)
            {
                for (int j = 0; j < programCrmMaplist.size(); j++)
                {
                    String castname = programCrmMaplist.get(j).getCastname();
                    int roletype = programCrmMaplist.get(j).getRoletype();
                    String tempStr;
                    if (roletype == 1)
                    {   tempStr = directorStrBuffer.toString() + " " + castname;
                        if (StrUtil.exlen(tempStr.trim(),"en") <= 50)
                        {
                            directorStrBuffer.append(" ").append(castname);
                        }  
                    }
                    else
                    {
                        tempStr = kpeopleStrBuffer.toString() + " " + castname;
                        if (StrUtil.exlen(tempStr.trim(),"en") <= 150)
                        {
                            kpeopleStrBuffer.append(" ").append(castname);
                        } 
                    }
                }           
            }            
            writeElement(progobjectElement, progpertyElement, "Kpeople", (programList.get(i).getKpeople()==null||programList.get(i).getKpeople().equals(""))? kpeopleStrBuffer.toString().trim() : programList.get(i).getKpeople(), true);   
            writeElement(progobjectElement, progpertyElement, "Director", (programList.get(i).getDirectors()==null||programList.get(i).getDirectors().equals(""))? directorStrBuffer.toString().trim() : programList.get(i).getDirectors(), true);   
            writeElement(progobjectElement, progpertyElement, "ScriptWriter", programList.get(i).getScriptwriter(), true);   
            writeElement(progobjectElement, progpertyElement, "Compere", programList.get(i).getCompere(), true);   
            writeElement(progobjectElement, progpertyElement, "Guest", programList.get(i).getGuest(), true);   
            writeElement(progobjectElement, progpertyElement, "Reporter", programList.get(i).getReporter(), true);   
            writeElement(progobjectElement, progpertyElement, "OPIncharge", programList.get(i).getOpincharge(), true);   
                                    
            /***
             * 获取发布生成program和movie相应的MAP关系 修改同步时 不带任何maping
             */          
        }
        
        List<CmsMovie> movieList = (List<CmsMovie>)objectMap.get("movieList");
        for (int i =0; i<movieList.size(); i++)
        {
            Element movObjectElement = null;
            Element movPertyElement = null;

            // 元素节点
            movObjectElement = objectsElement.addElement("Object");
            movObjectElement.addAttribute("ElementType", "Movie");
            movObjectElement.addAttribute("ID", movieList.get(i).getCpcontentid());
            movObjectElement.addAttribute("Code", movieList.get(i).getCpcontentid());
            movObjectElement.addAttribute("Action", action);

            writeElement(movObjectElement, movPertyElement, "Type",movieList.get(i).getMovietype().toString(), false);
            writeElement(movObjectElement, movPertyElement, "FileURL",movieList.get(i).getFileurl(), true);
            writeElement(movObjectElement, movPertyElement, "SourceDRMType", movieList.get(i).getSourcedrmtype(), false);
            writeElement(movObjectElement, movPertyElement, "DestDRMType", movieList.get(i).getDestdrmtype(), false);
            writeElement(movObjectElement, movPertyElement, "AudioType", movieList.get(i).getAudiotype(), false);
            writeElement(movObjectElement, movPertyElement, "ScreenFormat", movieList.get(i).getScreenformat(), false);
            writeElement(movObjectElement, movPertyElement, "ClosedCaptioning", movieList.get(i).getClosedcaptioning(), false);    
            /***
             * 获取发布生成program和movie相应的MAP关系 修改同步时 不带任何maping
             */          
               if(syncType!=2){
                   mappingMap = new HashMap<String, String>();
                   mappingMap.put("ParentID", programList.get(0).getCpcontentid());
                   mappingMap.put("ID", "");
                   //mappingMap.put("ParentID", movieList.get(i).getCpcontentid());
                   //mappingMap.put("ObjectID", objectId);
                   mappingMap.put("ElementID", movieList.get(i).getCpcontentid());
                   mappingMap.put("Type", "");
                   mappingMap.put("Sequence", "");
                   mappingMap.put("ValidStart", "");
                   mappingMap.put("ValidEnd", "");
                   mapList.add(mappingMap);
               }            
        }
        
        // 生成Mappings节点
        Element mappingElement = adiElement.addElement("Mappings");
        if(mapList!=null&&mapList.size()>0)
        {
            for (Map map2 : mapList)
        {
            Element mapObjectElement = null;
            Element mapPertyElement = null;
            mapObjectElement = mappingElement.addElement("Mapping");
            mapObjectElement.addAttribute("ParentType", "Program");
                mapObjectElement.addAttribute("ID", map2.get("ID").toString());
                mapObjectElement.addAttribute("ParentID", map2.get("ParentID").toString());
                mapObjectElement.addAttribute("ParentCode", map2.get("ParentID").toString());
                mapObjectElement.addAttribute("ElementType", "Movie");
                mapObjectElement.addAttribute("ElementID", map2.get("ElementID").toString());
                mapObjectElement.addAttribute("ElementCode", map2.get("ElementID").toString());
            mapObjectElement.addAttribute("Action", action);

            writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
            writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);
            }
        }        
        
        //把对象写入xml文件
        writeDom(dom,fileFullName);
        
        return retPath;
    }
    
    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {   
        String action = null;
        // 获取发布Action类型
        if (1 == syncType || 2 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;        
        String targetTypeName ="";
        if (targetType == 1)
        {
        	targetTypeName = "program_bms";
        }
        else if (targetType == 2)
        {
        	targetTypeName = "program_movie_epg";
        }
        else if (targetType == 3)
        {
        	targetTypeName = "movie_cdn";
        }
        Date date = new Date();
        String fileName = targetTypeName+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"program"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        
        String fileFullName = absolutePath + fileName;
        
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");       
        if (1==targetType || 2==targetType)
        {
            List<CmsProgram> programList = (List<CmsProgram>)objectMap.get("programList");
            List<String> programTypeList = (List<String>)objectMap.get("programtypeid");
            String programtypeid = programTypeList.get(0);
            if (null == programList || programList.size()==0)
            {
                return null;
            }
            for (int i =0; i<programList.size(); i++)
            {
                Element progobjectElement = null;                
                Element progpertyElement = null;
                // 元素节点
                progobjectElement = objectsElement.addElement("Object");
                progobjectElement.addAttribute("ElementType", "Program");
                progobjectElement.addAttribute("ContentID", programList.get(i).getProgramid());
                progobjectElement.addAttribute("Action", action);
                
                writeElement(progobjectElement, progpertyElement, "Name", programList.get(i).getNamecn(), true);
                writeElement(progobjectElement, progpertyElement, "CPContentID", programList.get(i).getCpcontentid(), false);
                writeElement(progobjectElement, progpertyElement, "OrderNumber", programList.get(i).getOrdernumber(), false);
                writeElement(progobjectElement, progpertyElement, "OriginalName", programList.get(i).getOriginalnamecn(), true);
                writeElement(progobjectElement, progpertyElement, "SortName", programList.get(i).getSortname(), true);
                writeElement(progobjectElement, progpertyElement, "SearchName", programList.get(i).getSearchname(), true);
                writeElement(progobjectElement, progpertyElement, "Genre",programList.get(i).getGenre() , true);
                writeElement(progobjectElement, progpertyElement, "ActorDisplay", programList.get(i).getActors(), true);
                writeElement(progobjectElement, progpertyElement, "WriterDisplay", programList.get(i).getWriters(), true);
                writeElement(progobjectElement, progpertyElement, "OriginalCountry", programList.get(i).getCountry(), false);
                writeElement(progobjectElement, progpertyElement, "Language", langMap.get(programList.get(i).getLanguage()), false);
                writeElement(progobjectElement, progpertyElement, "ReleaseYear", programList.get(i).getReleaseyear(), false);
                writeElement(progobjectElement, progpertyElement, "OrgAirDate", programList.get(i).getOrgairdate(), false);
                writeElement(progobjectElement, progpertyElement, "LicensingWindowStart", programList.get(i).getLicensingstart(), false);
                writeElement(progobjectElement, progpertyElement, "LicensingWindowEnd", programList.get(i).getLicensingend(), false);
                writeElement(progobjectElement, progpertyElement, "DisplayAsNew", programList.get(i).getNewcomedays(), false);
                writeElement(progobjectElement, progpertyElement, "DisplayAsLastChance", programList.get(i).getRemainingdays(), false);
                writeElement(progobjectElement, progpertyElement, "Macrovision", programList.get(i).getMacrovision(), false);
                writeElement(progobjectElement, progpertyElement, "Description", programList.get(i).getDesccn(), true);
                writeElement(progobjectElement, progpertyElement, "PriceTaxIn", programList.get(i).getPricetaxin(), true);
                writeElement(progobjectElement, progpertyElement, "Status", programList.get(i).getIsvalid(), false);
                writeElement(progobjectElement, progpertyElement, "SourceType", programList.get(i).getSourcetype(), false);
                writeElement(progobjectElement, progpertyElement, "SeriesFlag", programList.get(i).getSeriesflag(), false);   
                writeElement(progobjectElement, progpertyElement, "ContentProvider", programList.get(i).getCpid(), false);
                
                writeElement(progobjectElement, progpertyElement, "KeyWords", programList.get(i).getKeywords(), true);
                writeElement(progobjectElement, progpertyElement, "Tags", programList.get(i).getContenttags(), true);
                writeElement(progobjectElement, progpertyElement, "ViewPoint", programList.get(i).getViewpoint(), true);
                writeElement(progobjectElement, progpertyElement, "StarLevel", programList.get(i).getRecommendstart(), false);
                writeElement(progobjectElement, progpertyElement, "Rating", programList.get(i).getRating(), true);
                writeElement(progobjectElement, progpertyElement, "Awards", programList.get(i).getAwards(), true);
                writeElement(progobjectElement, progpertyElement, "Length", programList.get(i).getDuration(), false);
                writeElement(progobjectElement, progpertyElement, "ProgramType", programtypeid, true);

                writeElement(progobjectElement, progpertyElement, "Reserve1", programList.get(i).getReserve1(), false);
                writeElement(progobjectElement, progpertyElement, "Reserve2", programList.get(i).getReserve2(), false);
                writeElement(progobjectElement, progpertyElement, "Reserve3", programList.get(i).getReserve3(), false);
                writeElement(progobjectElement, progpertyElement, "Reserve4", programList.get(i).getReserve4(), false);
                writeElement(progobjectElement, progpertyElement, "Reserve5", programList.get(i).getReserve5(), false);
                writeElement(progobjectElement, progpertyElement, "UniContentId", programList.get(i).getUnicontentid(), true);
            }
        }
        
        if (2==targetType || 3==targetType)
        {
            mapList = new ArrayList<Map>();
            List<CmsMovie> movieList = (List<CmsMovie>)objectMap.get("movieList");
            for (int i =0; i<movieList.size(); i++)
            {
                Element movObjectElement = null;
                Element movPertyElement = null;

                // 元素节点
                movObjectElement = objectsElement.addElement("Object");
                movObjectElement.addAttribute("ElementType", "Movie");
                movObjectElement.addAttribute("PhysicalContentID", movieList.get(i).getMovieid());
                movObjectElement.addAttribute("Action", action);
                writeElement(movObjectElement, movPertyElement, "Type",  movieList.get(i).getMovietype().toString(), false);                
                writeElement(movObjectElement, movPertyElement, "FileURL",  movieList.get(i).getFileurl(), true);
                writeElement(movObjectElement, movPertyElement, "CPContentID", movieList.get(i).getCpcontentid(), false);
                writeElement(movObjectElement, movPertyElement, "SourceDRMType", movieList.get(i).getSourcedrmtype(), false);
                writeElement(movObjectElement, movPertyElement, "DestDRMType", movieList.get(i).getDestdrmtype(), false);
                writeElement(movObjectElement, movPertyElement, "AudioType", movieList.get(i).getAudiotype(), false);
                writeElement(movObjectElement, movPertyElement, "ScreenFormat", movieList.get(i).getScreenformat(), false);
                writeElement(movObjectElement, movPertyElement, "ClosedCaptioning", movieList.get(i).getClosedcaptioning(), false);    
                writeElement(movObjectElement, movPertyElement, "Duration", movieList.get(i).getDuration(), false);
                writeElement(movObjectElement, movPertyElement, "FileSize", movieList.get(i).getFilesize(), false);
                writeElement(movObjectElement, movPertyElement, "BitRateType", movieList.get(i).getBitratetype(), false);
                writeElement(movObjectElement, movPertyElement, "VideoType", movieList.get(i).getVideotype(), false);
                writeElement(movObjectElement, movPertyElement, "AudioEncodingType", movieList.get(i).getAudiotrack(), false);
                writeElement(movObjectElement, movPertyElement, "Resolution", movieList.get(i).getResolution(), false);
                writeElement(movObjectElement, movPertyElement, "VideoProfile", movieList.get(i).getVideoprofile(), false);
                writeElement(movObjectElement, movPertyElement, "System Layer", movieList.get(i).getSystemlayer(), false);
                writeElement(movObjectElement, movPertyElement, "Domain", movieList.get(i).getDomain(), false);
                writeElement(movObjectElement, movPertyElement, "Hotdegree", movieList.get(i).getHotdegree(), false);
                if (targetType == 2) // 获取发布EPG系统生成相应的MAP关系
                {
                    mappingMap = new HashMap<String, String>();
                    mappingMap.put("ParentID", movieList.get(i).getProgramid());
                    mappingMap.put("ObjectID", movieList.get(i).getMappingid());
                    mappingMap.put("ElementID", movieList.get(i).getMovieid());
                    mappingMap.put("Type", "");
                    mappingMap.put("Sequence", "");
                    mappingMap.put("ValidStart", "");
                    mappingMap.put("ValidEnd", "");
                    mapList.add(mappingMap);
                }                
            }
        }
        
        if (2==targetType && 2!=syncType)//mapping关系只发到epg，且更新时不带mapping
        {
            // 生成Mappings节点
            Element mappingElement = adiElement.addElement("Mappings");
            for (Map map2 : mapList)
            {
                Element mapObjectElement = null;
                Element mapPertyElement = null;
                mapObjectElement = mappingElement.addElement("Mapping");
                mapObjectElement.addAttribute("ParentType", "Program");
                mapObjectElement.addAttribute("ParentID",map2.get("ParentID").toString());
                mapObjectElement.addAttribute("ElementType", "Movie");
                mapObjectElement.addAttribute("ElementID", map2.get("ElementID").toString());
                mapObjectElement.addAttribute("Action", action);
                mapObjectElement.addAttribute("ObjectID", map2.get("ObjectID").toString());

                writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
                writeElement(mapObjectElement, mapPertyElement, "Sequence", "", false);
                writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
                writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);
            }
        }        
        
        //把对象写入xml文件
        writeDom(dom,fileFullName);
        
        return retPath;
    }

}

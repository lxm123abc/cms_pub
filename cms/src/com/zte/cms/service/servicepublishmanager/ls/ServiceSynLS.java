package com.zte.cms.service.servicepublishmanager.ls;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceContentBindDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceSeriesMapDS;
import com.zte.cms.service.servicepublishmanager.model.ServicePlatformSyn;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.cms.service.servicepublishmanager.service.IServicePlatformSynDS;
import com.zte.cms.service.servicepublishmanager.service.IServiceTargetSynDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class ServiceSynLS extends DynamicObjectBaseDS implements IServiceSynLS
{
    
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_USER,
            getClass());
    
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败   
    
    private    IServicePlatformSynDS servicePlatformSynDS;
    private    IServiceTargetSynDS  serviceTargetSynDS;
    private ITargetsystemDS targetSystemDS = null;
    private ICntSyncTaskDS cntSyncTaskDS = null;                                           
    private IServiceDS  serviceDS = null;
    private IObjectSyncRecordDS  objectSyncRecordDS = null;
    private ICmsStorageareaLS cmsStorageareaLS;
    //mapping时新增
    private IServiceProgramMapDS serviceProgramMapDS =null;
    private IServiceSeriesMapDS serviceSeriesMapDS = null;
    private IServiceChannelMapDS serviceChannelMapDS = null;
    
    private ICmsProgramDS cmsProgramDS = null;
    private ICmsChannelDS cmsChannelDS = null;
    private IPhysicalchannelDS physicalchannelDS = null;
    private ICmsSeriesDS cmsSeriesDS = null;
    

    public IServiceProgramMapDS getServiceProgramMapDS()
    {
        return serviceProgramMapDS;
    }

    public void setServiceProgramMapDS(IServiceProgramMapDS serviceProgramMapDS)
    {
        this.serviceProgramMapDS = serviceProgramMapDS;
    }

    public IServiceSeriesMapDS getServiceSeriesMapDS()
    {
        return serviceSeriesMapDS;
    }

    public void setServiceSeriesMapDS(IServiceSeriesMapDS serviceSeriesMapDS)
    {
        this.serviceSeriesMapDS = serviceSeriesMapDS;
    }

    public IServiceChannelMapDS getServiceChannelMapDS()
    {
        return serviceChannelMapDS;
    }

    public void setServiceChannelMapDS(IServiceChannelMapDS serviceChannelMapDS)
    {
        this.serviceChannelMapDS = serviceChannelMapDS;
    }

    public ICmsProgramDS getCmsProgramDS()
    {
        return cmsProgramDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public ICmsChannelDS getCmsChannelDS()
    {
        return cmsChannelDS;
    }

    public void setCmsChannelDS(ICmsChannelDS cmsChannelDS)
    {
        this.cmsChannelDS = cmsChannelDS;
    }

    public ICmsSeriesDS getCmsSeriesDS()
    {
        return cmsSeriesDS;
    }

    public void setCmsSeriesDS(ICmsSeriesDS cmsSeriesDS)
    {
        this.cmsSeriesDS = cmsSeriesDS;
    }



    private IServiceContentBindDS serviceCntBindDS;

    public IServiceContentBindDS getServiceCntBindDS()
    {
        return serviceCntBindDS;
    }

    public void setServiceCntBindDS(IServiceContentBindDS serviceCntBindDS)
    {
        this.serviceCntBindDS = serviceCntBindDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public ICntSyncTaskDS getCntSyncTaskDS()
    {
        return cntSyncTaskDS;
    }

    public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
    {
        this.cntSyncTaskDS = cntSyncTaskDS;
    }


    public IServiceDS getServiceDS()
    {
        return serviceDS;
    }

    public void setServiceDS(IServiceDS serviceDS)
    {
        this.serviceDS = serviceDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public ICmsStorageareaLS getCmsStorageareaLS()
    {
        return cmsStorageareaLS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setServicePlatformSynDS(IServicePlatformSynDS servicePlatformSynDS) {
        this.servicePlatformSynDS = servicePlatformSynDS;
    }
    
    public void setServiceTargetSynDS(IServiceTargetSynDS  serviceTargetSynDS) {
        this.serviceTargetSynDS = serviceTargetSynDS;
    }
    

    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    /**
     *    获取服务发布总状态表数据
     * */    
    public TableDataInfo pagePlatformInfoQuery(ServicePlatformSyn servicePlatformSyn, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQuery  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        // 格式化内容带有时间的字段
        servicePlatformSyn.setCreatetime1(DateUtil2.get14Time(servicePlatformSyn.getCreatetime1()));
        servicePlatformSyn.setCreatetime2(DateUtil2.get14Time(servicePlatformSyn.getCreatetime2()));
        
        servicePlatformSyn.setPublishtime1(DateUtil2.get14Time(servicePlatformSyn.getPublishtime1()));
        servicePlatformSyn.setPublishtime2(DateUtil2.get14Time(servicePlatformSyn.getPublishtime2()));
        
        servicePlatformSyn.setCancelpubtime1(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime1()));
        servicePlatformSyn.setCancelpubtime2(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime2()));
        //对特殊字符的处理
        if(servicePlatformSyn.getServiceid()!=null && !"".equals(servicePlatformSyn.getServiceid())){
            servicePlatformSyn.setServiceid(EspecialCharMgt.conversion(servicePlatformSyn.getServiceid()));
        }
        if(servicePlatformSyn.getServicename()!=null && !"".equals(servicePlatformSyn.getServicename())){
            servicePlatformSyn.setServicename(EspecialCharMgt.conversion(servicePlatformSyn.getServicename()));
        }
        
        servicePlatformSyn.setOrderCond("objectid desc");       
         try {
            dataInfo = servicePlatformSynDS.pageInfoQuery(servicePlatformSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;
    }
    
    /**
     *  获取服务网元发布状态表数据
     * */    
    public TableDataInfo pageTargetInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQuery  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        serviceTargetSyn.setOrderCond("objectid desc");   
        try {
            dataInfo = serviceTargetSynDS.pageInfoQuery(serviceTargetSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;
 
    }
    /**
     *  获取服务网元发布状态表数据
     * */    
    public TableDataInfo pageTargetInfoQueryAll(Service service, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQuery  by service starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();  
        try {
            dataInfo = serviceTargetSynDS.pageInfoQueryAll(service, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  by service end.................................");
        return dataInfo;
 
    }
    
    public ServiceTargetSyn getServiceTargetSynByID(Long syncindex) throws DomainServiceException
    {
        log.debug( "get serviceTargetSyn by pk starting..." );
        ServiceTargetSyn serviceTargetSyn = null;
        try
        {
            serviceTargetSyn= serviceTargetSynDS.getServiceTargetSynByID(syncindex);
        }
        catch( DomainServiceException daoEx )
        {
            log.error( "serviceTargetSynDS exception"+daoEx.getMessage());
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get serviceTargetSyn by pk end" );
        return serviceTargetSyn;
    } 

    /**
     *   服务对网元发布
     * */    
    public String targetPublishSyn(Long syncindex) throws Exception
    {
        log.debug("publish target service start...");        
       boolean tempPath = getTempPath();
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }        
       
       //根据主键获得服务发布网元的对象
        ServiceTargetSyn service =  serviceTargetSynDS.getServiceTargetSynByID(syncindex);

      //查询服务是否存在,服务是状态是否为审核通过
       Service  ser = new Service();
       ser.setServiceindex(service.getObjectindex());
       Service serExist= serviceDS.getService(ser);
       if(serExist == null){
           //服务不存在退出；
           return  "1:服务不存在 ";
       }
       if(serExist.getStatus() != 0){           
           //服务状态不为审核通过，退出
           return "1:服务状态不正确 ";
       }              
     //目标网元是否存在
      Targetsystem targetSystem = new Targetsystem();
      targetSystem.setTargetindex(service.getTargetindex());
      Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
      
      if(targetSystemExist == null){
          //目标网元不存在，退出
          return "1:服务发布的网元不存在 ";
      }
      //目标网元状态是否正常
      if(targetSystemExist.getStatus() !=0){
          //目标网元状态不是正常
          return "1:服务发布的网元状态不正确 ";
      }
      
      String targetid = targetSystemExist.getTargetid();
      
      //包装生成xml文件接口中需要的map对象
      Map map = new HashMap<String, List>();
      List<ServiceTargetSyn> serviceList = new ArrayList<ServiceTargetSyn>();
      serviceList.add(service);
      map.put("service", serviceList);
      
      //该服务发布状态是否为可发布状态？    
      int targetResult = service.getOperresult();     
      int targettype =  service.getTargettype();
      String xmlPath = "";
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(service.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();//获得平台类型 1--2.0平台；2--3.0平台
     
      if( targetResult == 0 
              || targetResult == 30 
              || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
      {
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 1);
      }else {          
          if ( targetResult == 60 ) //修改同步失败
          {
              xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 2);
          }else 
          {
              //网元发布状态为不可发布状态，退出
              return  "1:服务在网元" +targetid+"上的发布状态不正确 ";  
          }       
      }
      if(xmlPath == null){
          return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
      }

          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = service.getTargetindex();
       
        //插入对象日志表 //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = service.getObjectindex();
       String objectid = service.getObjectid();
       int objecttype = service.getObjecttype();
       String objectcode = service.getServicecode();
       if(targetResult == 60){
           insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode,targetindex, 2,platformNum);
       }else{
           insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode,targetindex, 1,platformNum);
       }
       Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
       insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
 
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( service, 1); //新增、修改为1，删除为3//修改发布只是对修改同步失败60状态做个判断
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      // 返回信息
//     operateInfo = new OperateInfo(String.valueOf(1), service.getServiceid(), ResourceMgt
//                        .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
//                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
//     returnInfo.appendOperateInfo(operateInfo);     
       log.debug("publish target service end...");
      String returnInfo ="0:操作成功 ";
       return returnInfo;
   }        
    
    private String targetPubforPlatform(Long syncindex,Long batchid,int firTargetType) throws Exception {

        log.debug("publish target service start...");        
       boolean tempPath = getTempPath();
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }        
       
       //根据主键获得服务发布网元的对象
        ServiceTargetSyn service =  serviceTargetSynDS.getServiceTargetSynByID(syncindex);

      //查询服务是否存在,服务是状态是否为审核通过
       Service  ser = new Service();
       ser.setServiceindex(service.getObjectindex());
       Service serExist= serviceDS.getService(ser);
       if(serExist == null){
           //服务不存在退出；
           return  "1:服务不存在 ";
       }
       if(serExist.getStatus() != 0){           
           //服务状态不为审核通过，退出
           return "1:服务状态不正确 ";
       }              
     //目标网元是否存在
      Targetsystem targetSystem = new Targetsystem();
      targetSystem.setTargetindex(service.getTargetindex());
      Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
      
      if(targetSystemExist == null){
          //目标网元不存在，退出
          return "1:服务发布的网元不存在 ";
      }
      //目标网元状态是否正常
      if(targetSystemExist.getStatus() !=0){
          //目标网元状态不是正常
          return "1:服务发布的网元状态不正确 ";
      }
      
      String targetid = targetSystemExist.getTargetid();
      
      //包装生成xml文件接口中需要的map对象
      Map map = new HashMap<String, List>();
      List<ServiceTargetSyn> serviceList = new ArrayList<ServiceTargetSyn>();
      serviceList.add(service);
      map.put("service", serviceList);
      
      //该服务发布状态是否为可发布状态？    
      int targetResult = service.getOperresult();     
      int targettype =  service.getTargettype();
      String xmlPath = "";
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(service.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();//获得平台类型 1--2.0平台；2--3.0平台
     
      if( targetResult == 0 
              || targetResult == 30 
              || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
      {
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 1);
      }else {          
          if ( targetResult == 60 ) //修改同步失败
          {
              xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 2);
          }else 
          {
              //网元发布状态为不可发布状态，退出
              return  "1:服务在网元" +targetid+"上的发布状态不正确 ";  
          }       
      }
      if(xmlPath == null){
          return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
      }

          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = service.getTargetindex();
       
        //插入对象日志表 //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = service.getObjectindex();
       String objectid = service.getObjectid();
       int objecttype = service.getObjecttype();
       String objectcode = service.getServicecode();
       if(targetResult == 60){
           insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode,targetindex, 2,platformNum);
       }else{
           insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode,targetindex, 1,platformNum);
       }
       if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
           Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
       }else if(firTargetType == service.getTargettype()){
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       }else{
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
       }
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( service, 1); //新增、修改为1，删除为3//修改发布只是对修改同步失败60状态做个判断
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      // 返回信息

       log.debug("publish target service end...");
      String returnInfo = "0:发布到目标系统"+targetid+":操作成功 ";
       return returnInfo;     
    }
    

    /**
     *     服务对平台发布
     * */    
    public String  platformPublishSyn(Long syncindex) throws Exception{            
        log.debug("publish platform service start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
        
        ServicePlatformSyn  servicePlatform = new ServicePlatformSyn();
        servicePlatform.setSyncindex(syncindex);
        servicePlatform = servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatform);
        if(servicePlatform == null ){
            return  "1:平台发布数据不存在 ";           
        }
        int platformStatus =  servicePlatform.getStatus();
        if(platformStatus== 200 || platformStatus == 300 ){
            return   "1:平台发布状态不正确 ";        
        }
        
       ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();
       serviceTargetSyn.setRelateindex(syncindex);
      List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getServiceTargetSynByCond(serviceTargetSyn);
      if(serviceList == null || serviceList.size()==0){
          return  "1:服务没有关联网元 ";
      }
      Long batchid = null;
      
      int firTargetType = findFirTargettype(serviceList,0);
      if(firTargetType != 99)
          batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
      
      int size = serviceList.size();
      for(int i=0; i<size; i++){
          ServiceTargetSyn service = serviceList.get(i);
          Long index = service.getSyncindex();    
          
          Targetsystem targetSystem = new Targetsystem();
          targetSystem.setTargetindex(service.getTargetindex());
          Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
          String targetid = targetSystemExist.getTargetid();
          
          if(service.getStatus() == 200 || service.getStatus() == 300 ){
              allresult.append("服务在网元"+targetid+"上的发布状态不正确 ");  
              continue;             
          }
                   
          String rstInfo = targetPubforPlatform(index,batchid,firTargetType); 
          if ( rstInfo.substring(0, 1).equals("0"))
          {
              successFlg = true;
              allresult.append(rstInfo.substring(2));
          }else
          {
              allresult.append(rstInfo.substring(2));
       }
          }                       
       log.debug("publish platform service start... end...");
      if(successFlg){
          return "0:"+allresult.toString();         
      }else{//全部失败
          return "1:"+allresult.toString();            
      }   
   }
    
    
    /**
     *  服务对批量发布
     * */   
    public String batchPublishSyn(List<ServiceTargetSyn> serviceList) throws Exception
    {
        log.debug("publish  batch  service start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
       String retInfo = "";
       
       int size = serviceList.size();     
        for(ServiceTargetSyn service:serviceList){
            
            Long syncindex = service.getSyncindex();
        
         retInfo=  platformPublishSyn(syncindex);  
         if(retInfo.substring(0, 1).equals("0")){
             iSuccessed++;
             String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
             operateInfo = new OperateInfo(String.valueOf(rstindex++),service.getServiceid(), 
              resultStr.substring(2, resultStr.length()), retInfo.substring(2));
             returnInfo.appendOperateInfo(operateInfo);
         }else{
             ifail++;
             String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
             operateInfo = new OperateInfo(String.valueOf(rstindex++), service.getServiceid(), 
              resultStr.substring(2, resultStr.length()), retInfo.substring(2));
             returnInfo.appendOperateInfo(operateInfo);            
         } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量：" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量：" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + iSuccessed + "  失败数量：" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("publish batch service end...");
        return returnInfo.toString();        
    }
      
  /**
   * 服务网元取消发布
   * */  
    public String targetDelPublishSyn(Long syncindex) throws Exception
    {
        log.debug("target delete publish batch start...");
        boolean tempPath = getTempPath();
        if(!tempPath ){
            return "1:获取临时区失败";
        } 
   
        //包装服务对象为生成xml的入参
        ServiceTargetSyn service =  serviceTargetSynDS.getServiceTargetSynByID(syncindex);
        Map map = new HashMap<String, List>();
        List<ServiceTargetSyn> serviceList = new ArrayList<ServiceTargetSyn>();
        serviceList.add(service);
        map.put("service", serviceList);

      //查询服务是否存在,服务是状态是否为审核通过
       Service  ser = new Service();
       ser.setServiceindex(service.getObjectindex());
       Service serExist= serviceDS.getService(ser);
       if(serExist == null){
           //服务不存在退出；
           return  "1:服务不存在 ";
       }
       if(serExist.getStatus() != 0){           
           //服务状态不为审核通过，退出
           return "1:服务状态不正确 ";
       }         
       
       //该服务关联关系发布是否存在发布中200，发布成功300和修改发布失败状态500，如果存在，那么就不能进行取消发布
       ServiceTargetSyn serviceMap= new  ServiceTargetSyn();
       serviceMap.setParentid(service.getObjectid());
       serviceMap.setTargetindex(service.getTargetindex());
       List<ServiceTargetSyn> serviceMapList = serviceTargetSynDS.getSameParentTargetMappingSyn(serviceMap);
       for(int i=0; i<serviceMapList.size(); i++){
           ServiceTargetSyn serviceMapping = serviceMapList.get(i);      
           int mappingStatus = serviceMapping.getStatus();
           if(mappingStatus == 200 || mappingStatus ==300 || mappingStatus ==500){//发布中200，发布成功300，修改发布
               int objectType = serviceMapping.getObjecttype();
               String elementName = "";
               if(objectType == 21){                
                   elementName = "点播内容";
               }
               if(objectType == 22){                
                   elementName = "连续剧";
               }
               if(objectType == 23){                
                   elementName = "直播频道";
               }           
               if(mappingStatus == 200){
                   return  "1:服务打包"+ elementName + "发布中，不能取消发布";      
               }else{
                   return  "1:服务打包"+ elementName + "已发布，不能取消发布";                
               }                   
           }     
       }
       
     //目标网元是否存在
      Targetsystem targetSystem = new Targetsystem();
      targetSystem.setTargetindex(service.getTargetindex());
      Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
      if(targetSystemExist == null){
          //目标网元不存在，退出
          return "1:服务发布的网元不存在 ";
      }
      //目标网元状态是否正常
      if(targetSystemExist.getStatus() !=0){
          //目标网元状态不是正常
          return "1:服务发布的网元状态不正确 ";
      }
      
      String targetid = targetSystemExist.getTargetid();
      
     //该服务发布状态是否为可取消发布状态？                   
       //生成xml文件    
      int targettype =  service.getTargettype();
      String xmlPath = "";
      
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(service.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = platfrom.getPlatform();
      
      int targetResult = service.getOperresult();
      if( targetResult == 20 // 新增同步成功
              || targetResult == 50 // 修改同步成功
              ||targetResult == 60 // 修改同步失败
              || targetResult == 90 ) //取消同步失败
      {
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 3);
        
      }    else{
          //网元发布状态为不可发布状态，退出
          return  "1:服务在网元" +targetid+"上的发布状态不正确 ";  
          
      }  
      if(xmlPath == null){
          return "1:从目标系统"+targetid+"取消发布:生成xml文件失败";
      }
     
  
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = service.getTargetindex();
      
       
        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = service.getObjectindex();
       String objectid = service.getObjectid();
       int objecttype = service.getObjecttype();
       String objectcode = service.getServicecode();
       insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode, targetindex, 3,platformNum);
       Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
       insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( service, 3);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      // 返回信息
//     operateInfo = new OperateInfo(String.valueOf(1), service.getServiceid(), ResourceMgt
//                        .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
//                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
//     returnInfo.appendOperateInfo(operateInfo);     
        log.debug("publish target service end...");

        String returnInfo ="0:操作成功";
        return returnInfo;   
    }

    
    private String targetDelPubforPlatform(Long syncindex,Long batchid,int firTargetType) throws Exception{

        log.debug("target delete publish batch start...");
        boolean tempPath = getTempPath();
        if(!tempPath ){
            return "1:获取临时区失败";
        } 
   
        //包装服务对象为生成xml的入参
        ServiceTargetSyn service =  serviceTargetSynDS.getServiceTargetSynByID(syncindex);
        Map map = new HashMap<String, List>();
        List<ServiceTargetSyn> serviceList = new ArrayList<ServiceTargetSyn>();
        serviceList.add(service);
        map.put("service", serviceList);

      //查询服务是否存在,服务是状态是否为审核通过
       Service  ser = new Service();
       ser.setServiceindex(service.getObjectindex());
       Service serExist= serviceDS.getService(ser);
       if(serExist == null){
           //服务不存在退出；
           return  "1:服务不存在 ";
       }
       if(serExist.getStatus() != 0){           
           //服务状态不为审核通过，退出
           return "1:服务状态不正确 ";
       }         
       
       //该服务关联关系发布是否存在发布中200，发布成功300和修改发布失败状态500，如果存在，那么就不能进行取消发布
       ServiceTargetSyn serviceMap= new  ServiceTargetSyn();
       serviceMap.setParentid(service.getObjectid());
       serviceMap.setTargetindex(service.getTargetindex());
       List<ServiceTargetSyn> serviceMapList = serviceTargetSynDS.getSameParentTargetMappingSyn(serviceMap);
       for(int i=0; i<serviceMapList.size(); i++){
           ServiceTargetSyn serviceMapping = serviceMapList.get(i);      
           int mappingStatus = serviceMapping.getStatus();
           if(mappingStatus == 200 || mappingStatus ==300 || mappingStatus ==500){//发布中200，发布成功300，修改发布
               int objectType = serviceMapping.getObjecttype();
               String elementName = "";
               if(objectType == 21){                
                   elementName = "点播内容";
               }
               if(objectType == 22){                
                   elementName = "连续剧";
               }
               if(objectType == 23){                
                   elementName = "直播频道";
               }           
               if(mappingStatus == 200){
                   return  "1:服务打包"+ elementName + "发布中，不能取消发布";                
               }else{
                   return  "1:服务打包"+ elementName + "已发布，不能取消发布";                
               }           
           }     
       }
       
     //目标网元是否存在
      Targetsystem targetSystem = new Targetsystem();
      targetSystem.setTargetindex(service.getTargetindex());
      Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
      if(targetSystemExist == null){
          //目标网元不存在，退出
          return "1:服务发布的网元不存在 ";
      }
      //目标网元状态是否正常
      if(targetSystemExist.getStatus() !=0){
          //目标网元状态不是正常
          return "1:服务发布的网元状态不正确 ";
      }
      
      String targetid = targetSystemExist.getTargetid();
      
     //该服务发布状态是否为可取消发布状态？                   
       //生成xml文件    
      int targettype =  service.getTargettype();
      String xmlPath = "";
      
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(service.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = platfrom.getPlatform();
      
      int targetResult = service.getOperresult();
      if( targetResult == 20 // 新增同步成功
              || targetResult == 50 // 修改同步成功
              ||targetResult == 60 // 修改同步失败
              || targetResult == 90 ) //取消同步失败
      {
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 3);
        
      }    else{
          //网元发布状态为不可发布状态，退出
          return  "1:服务在网元" +targetid+"上的发布状态不正确 ";  
          
      }  
      if(xmlPath == null){
          return "1:从目标系统"+targetid+"取消发布:生成xml文件失败";
      }
     
  
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = service.getTargetindex();
      
       
        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = service.getObjectindex();
       String objectid = service.getObjectid();
       int objecttype = service.getObjecttype();
       String objectcode = service.getServicecode();
       insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode, targetindex, 3,platformNum);
       
       if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
           Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
       }else if(firTargetType == service.getTargettype()){
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       }else{
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
       }
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( service, 3);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(service.getServiceid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      // 返回信息
//     operateInfo = new OperateInfo(String.valueOf(1), service.getServiceid(), ResourceMgt
//                        .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
//                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
//     returnInfo.appendOperateInfo(operateInfo);     
        log.debug("publish target service end...");

        String returnInfo ="0:从目标系统"+targetid+"取消发布: 操作成功 ";
        return returnInfo;   
    
        
    }
    /**
     * 服务平台取消发布
     * */  
    public String platformDelPublishSyn(Long synsindex) throws Exception
    {
        log.debug("platform delete publish batch start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
  
        ServicePlatformSyn  servicePlatform = new ServicePlatformSyn();
        servicePlatform.setSyncindex(synsindex);
        servicePlatform = servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatform);
        if(servicePlatform == null ){
            return  "1:平台发布数据不存在 ";           
        }
        int platformStatu =  servicePlatform.getStatus();
        if(platformStatu== 0 || platformStatu == 200 || platformStatu == 400 ){
            return   "1:平台发布状态不正确 ";        
        }       
        //根据平台syncindex获取平台信息，再根据平台信息获得该服务在该平台上的关联关系的发布信息
        ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
        servicePlatfoermSyn.setSyncindex(synsindex);
        ServicePlatformSyn platfromService =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
        ServicePlatformSyn servicePlatfrom = new ServicePlatformSyn();
        servicePlatfrom.setPlatform(platfromService.getPlatform());
        servicePlatfrom.setParentid(platfromService.getObjectid());
        List<ServicePlatformSyn> servicePlatfromList = servicePlatformSynDS.getSameParentPlatformSyn(servicePlatfrom);
        
        for(int i = 0; i<servicePlatfromList.size(); i++){            
            int platformStatus = servicePlatfromList.get(i).getStatus();
            int objectType = servicePlatfromList.get(i).getObjecttype();
            String elementName = "";
            if(objectType == 21){                
                elementName = "点播内容";
            }
            if(objectType == 22){                
                elementName = "连续剧";
            }
            if(objectType == 23){                
                elementName = "直播内容";
            }
            if(platformStatus == 200){
                return  "1:服务打包"+ elementName + "发布中，不能取消发布";                
            }
            if(platformStatus == 300 || platformStatus == 500){
                return  "1:服务打包"+ elementName + "已发布，不能取消发布";                
            }            
        }  
        
        ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();
        serviceTargetSyn.setRelateindex(synsindex);
       List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getServiceTargetSynByCond(serviceTargetSyn);    
       if(serviceList == null || serviceList.size()==0){
           return  "1:服务没有关联网元";
       }
       
       int size = serviceList.size();
       for(int i = 0; i< size; i++){
           Long targetindex = serviceList.get(i).getTargetindex();
           Targetsystem target = new Targetsystem();
           target.setTargetindex(targetindex);
           target = targetSystemDS.getTargetsystem(target);
           
           if(target.getTargettype() == 2){
               
               ServiceTargetSyn serviceMap= new  ServiceTargetSyn();
               serviceMap.setParentid(serviceList.get(i).getObjectid());
               serviceMap.setTargetindex(targetindex);
               List<ServiceTargetSyn> serviceMapList = serviceTargetSynDS.getSameParentTargetMappingSyn(serviceMap);
               
               for(ServiceTargetSyn targetmapping : serviceMapList){
                   
                   int mappingStatus = targetmapping.getStatus();
                   int objectType = targetmapping.getObjecttype();
                   String elementName = "";
                   if(objectType == 21){                
                       elementName = "点播内容";
                   }
                   if(objectType == 22){                
                       elementName = "连续剧";
                   }
                   if(objectType == 23){                
                       elementName = "直播内容";
                   }
                   if(mappingStatus == 200){
                       return  "1:服务打包"+ elementName + "在"+target.getTargetid()+"发布中，不能取消发布";                
                   }
                   if(mappingStatus == 300 || mappingStatus == 500){
                       return  "1:服务打包"+ elementName + "在"+target.getTargetid()+"已发布，不能取消发布";                
                   } 
                   
               }
           }
       }
       
       
       
       Long batchid = null;
       int firTargetType = findFirTargettype(serviceList,2);
       if(firTargetType != 99)
           batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
       
       
       for(int i=0; i<size; i++){
           ServiceTargetSyn service = serviceList.get(i);
           Long index = service.getSyncindex();       
           
           Targetsystem targetSystem = new Targetsystem();
           targetSystem.setTargetindex(service.getTargetindex());
           Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
           String targetid = targetSystemExist.getTargetid();
           
           if(service.getStatus() == 0 || service.getStatus() == 200 || service.getStatus() == 400){
               allresult.append("服务在网元"+targetid+"上的发布状态不正确");  
               continue;
           }
           
           String rstInfo= targetDelPubforPlatform(index, batchid, firTargetType);  
           if ( rstInfo.substring(0, 1).equals("0"))
           {
               successFlg = true;
               allresult.append(rstInfo.substring(2));
           }else
           {
               allresult.append(rstInfo.substring(2));
        }
           }                       
        log.debug("platform delete publish batch end...");
       if(successFlg){
           return "0:"+allresult.toString();         
       }else{//全部失败
           return "1:"+allresult.toString();            
       }     
    }
    /**
     * 批量取消发布
     * */  
    public String batchDelPublishSyn(List<ServiceTargetSyn> serviceList) throws Exception
    {
        log.debug("batch delete publish batch start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
       String retInfo = "";
       
       int size = serviceList.size();     
        for(ServiceTargetSyn service:serviceList){
            Long syncindex = service.getSyncindex();
        
         retInfo =  platformDelPublishSyn(syncindex);  
         if(retInfo.substring(0, 1).equals("0")){
             iSuccessed++;
             String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
             operateInfo = new OperateInfo(String.valueOf(rstindex++),service.getServiceid(), 
              resultStr.substring(2, resultStr.length()), retInfo.substring(2));
             returnInfo.appendOperateInfo(operateInfo);
         }else{
             ifail++;
             String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
             operateInfo = new OperateInfo(String.valueOf(rstindex++), service.getServiceid(), 
              resultStr.substring(2, resultStr.length()), retInfo.substring(2));
             returnInfo.appendOperateInfo(operateInfo);            
         } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量：" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量：" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + iSuccessed + "  失败数量：" +ifail);
                returnInfo.setFlag("2");
            }
        }  
  
        log.debug("batch delete publish batch end..."); 
        return returnInfo.toString();  
    }  
    
   
    /**
     * 
     * 判断服务绑定的关系发布状态
     * 如果状态是否存在发布中200的状态，如果存在不允许进行修改发布
     * 返回结果：flase表示该服务的发布状态为发布中    ture表示该服务可以进行服务修改发布操作
     * */  
    public boolean checkServiceStatus(Long objectindex) throws Exception{
        log.debug("check  Service   Status start..."); 
        boolean flg = true;
        ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();
        serviceTargetSyn.setObjectindex(objectindex);
        List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getServiceTargetSynByCond(serviceTargetSyn);
        int size = serviceList.size();
        if(serviceList == null || size ==0 ){  //该服务没有关联平台，直接可以修改         
            return flg;
        }
        for(int i = 0; i<size; i++){
            ServiceTargetSyn service = serviceList.get(i);             
            if(service.getStatus() == 200){//200发布中，如果是待发布状态，该服务不能进行修改操作；
               flg = false;
               return flg;
            }          
        } 
        log.debug("check  Service   Status end..."); 
        return flg;
    }
    
   
    /**
     * TODO  服务修改发布接口
     * 修改服务发布接口
     * 返回值：0：没有绑定关联关系//0：修改发布成功//
     *     1：各种不能进行修改发布的错误
     * */  
    
    public String modPublishSyn(String objectid) throws Exception
    {
        log.debug("batch modified publish service start..."); 
        //*******************修改发布服务的代码********************** 
        //判断临时区地址是否正常
        boolean tempPath = getTempPath();
        if(!tempPath ){
            return "1:获取临时区失败 ";
        } 
        
        
        ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();       
        serviceTargetSyn.setObjectid(objectid);
        serviceTargetSyn.setObjecttype(1);
        List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getAllServiceTargetSynById(serviceTargetSyn);//获取所有serviceid相同的服务在不同的网元
        
        int size = serviceList.size();
        if(serviceList == null || size ==0 ){
            return "0:服务没有关联网元 ";
        }
        
        Long batchid = null;
        int firTargetType = findFirTargettype(serviceList,1);
        if(firTargetType != 99)  
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        String serviceid = "" ;
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();
        //************对每个网元进行处理*****************
        for(int i = 0; i<size; i++){
            
            ServiceTargetSyn service = serviceList.get(i);               
           serviceid =  service.getServiceid();
           //判断是否是可修改发布状态，如果不是可修改发布状态，则进行下一个网元的修改发布操作
           if(service.getStatus() == 0 || service.getStatus() == 400){//0待发布，400发布失败,网元处于这两个状态情况下，不进行修改发布；
              
               Targetsystem targetSystem = new Targetsystem();
               targetSystem.setTargetindex(service.getTargetindex());
               Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
               String targetid = targetSystemExist.getTargetid();
               
               allresult.append("服务在网元"+targetid+"上的发布状态不正确 ");
               continue;
           }
            
            //包装服务对象为生成xml的入参            
            Map map = new HashMap<String, List>();
            List<ServiceTargetSyn> serviceMap = new ArrayList<ServiceTargetSyn>();
            serviceMap.add(service);
            map.put("service", serviceMap);
            
          //查询服务是否存在,服务是状态是否为审核通过
            Service  ser = new Service();
            ser.setServiceindex(service.getObjectindex());
            Service serExist= serviceDS.getService(ser);
            if(serExist == null){
                //服务不存在退出；
                allresult.append("服务不存在 ");
                continue;                
            }
            //当审核通过后，服务的状态为审核通过0，所以需要再次判断服务的状态，如果审核失败，就不进行修改发布。
            //*************注意这个问题**************
            if(serExist.getStatus() != 0){           
                //服务状态不为审核通过，退出
                allresult.append("服务状态不正确 ");
                continue;               
            }  
            
          //目标网元是否存在
           Targetsystem targetSystem = new Targetsystem();
           targetSystem.setTargetindex(service.getTargetindex());
           Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
           if(targetSystemExist == null){
               //目标网元不存在，退出
               allresult.append("服务发布的网元不存在 ");
               continue;
           }
           //目标网元状态是否正常
           if(targetSystemExist.getStatus() !=0){
               //目标网元状态不是正常
               allresult.append("服务发布的网元状态不正确 ");
               continue;
           }
           String targetid = targetSystemExist.getTargetid();
            //该服务发布状态是否为可发布状态？    
           
           int targettype =  service.getTargettype();
           String xmlPath = "";
           
           ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
           servicePlatfoermSyn.setSyncindex(service.getRelateindex());
           ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
           int platformNum = platfrom.getPlatform();
           
           int targetResult = service.getOperresult();
           if( targetResult == 20
                   || targetResult == 50                
                   || targetResult == 90
                   ||  targetResult == 60
                   )  // 20新增同步成功// 50修改同步成功//90取消同步失败  //60修改同步失败 
           {
               xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, service.getObjecttype(), 2);
             
           }    else{
               //网元发布状态为不可修改发布状态，检测另一个网元
               allresult.append("服务在网元"+targetid+"上的发布状态不正确 ");
               continue;         
           }            
       
               //插入同步任务表
           String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
           Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
           Long targetindex = service.getTargetindex();
           
            
             //修改对象日志表
            //service对象日志记录
            Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
            Long objectindex = service.getObjectindex();
            int objecttype = service.getObjecttype();
            String objectcode = service.getServicecode();
            insertObjectRecord( syncIndex, taskindex, objectindex, objectid, objecttype,objectcode, targetindex, 2, platformNum);
            int thetargettype = service.getTargettype();
            
            if(thetargettype != 1 && thetargettype != 2 && thetargettype != 3){
                Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
            }else if(firTargetType == thetargettype){
                insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
            }else{
                insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
            }
            
          //修改网元发布状态和总状态表状态
           modTargetSynStatus( service, 2); 
           
           successFlg = true;
           String returnInfo = "从目标系统"+targetid+"修改发布: 操作成功 ";
          allresult.append(returnInfo );        
          // 写操作日志
        CommonLogUtil.insertOperatorLog(serviceid+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
        CommonLogConstant.OPERTYPE_SERVICE_UPDATE,       
        CommonLogConstant.OPERTYPE_SERVICE_UPDATE_INFO,
        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }

        log.debug("batch modified publish  service end..."); 
        if(successFlg){
             return "0:" + allresult;
        }else{
            return  "1:" + allresult;
        }
        }
  
    /**
     * 修改服务对网元的发布状态以及平台的发布状态
     * 根据优先级来确定其状态
     * */  
    private void modTargetSynStatus(ServiceTargetSyn service , int action) throws Exception
    {   
        //这里只修改了服务网元的发布状态，对于总的发布状态没有修改！！！！！！
       ServiceTargetSyn  targetSyn = new ServiceTargetSyn();
       // ServicePlatformSyn platformSyn = new ServicePlatformSyn();     
        // 判断服务状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录   
       int result = service.getOperresult();
       if(action ==1){      
        if (result== 0)//待发布
        {    
            targetSyn.setOperresult(10);
        }
        else if(result== 60)  //修改同步失败
        {
            targetSyn.setOperresult(40);  //修改同步中
        }
        else        
        {     // 新增同步失败 或者取消同步失败
            targetSyn.setOperresult(10);// 新增同步中
         }
        targetSyn.setStatus(200);
        targetSyn.setSyncindex(service.getSyncindex());
       serviceTargetSynDS.updateServiceTargetSyn(targetSyn);//修改网元发布状态   
       }
       if(action == 2){
           if( result == 20 ||  
                result == 50 || 
                result == 90 || 
                result == 60 )  //20新增同步成功//50修改同步成功 //90取消同步失败  //60改同步失败  
           { 
               targetSyn.setOperresult(40); //修改同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(service.getSyncindex());
          serviceTargetSynDS.updateServiceTargetSyn(targetSyn);//修改网元发布状态     
       }
     
       if(action == 3){
           if(result == 20 ||  //新增同步成功
              result == 50 ||  //修改同步成功
               result == 60 ||   //修改同步失败
               result == 90  )  //取消同步失败                   
           { 
               targetSyn.setOperresult(70); //取消同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(service.getSyncindex());
          serviceTargetSynDS.updateServiceTargetSyn(targetSyn);//修改网元发布状态     
       }
       //获取其他网元状态，确定总发布状态表中的状态
       //判断其他网元状态有没有待发布（0），发布失败（400），修改发布失败（500）的，如果有，则总状态修改为上述状态，其优先级也是按照
       //上面的排列来的，如果没有，则总表中的状态修改我发布中（200）。
       ServiceTargetSyn  sameRelateindexTargetSyn = new ServiceTargetSyn();
       Long targetRelateindex = service.getRelateindex();
       sameRelateindexTargetSyn.setRelateindex(targetRelateindex);    
       List<ServiceTargetSyn> serviceList = serviceTargetSynDS.getServiceTargetSynByCond(sameRelateindexTargetSyn);     
       int size = serviceList.size();
       int targetStatusArrary[] = new int[size];      
       for(int i=0; i<size; i++){
           targetStatusArrary[i] = StatusTranslator.getTargetSyncStatus(serviceList.get(i).getOperresult()); 
       }
       int platformStatus = StatusTranslator.getPlatformSyncStatus(targetStatusArrary);
       ServicePlatformSyn servicePlatformSyn = new  ServicePlatformSyn();
       servicePlatformSyn.setSyncindex(targetRelateindex);
       servicePlatformSyn.setStatus(platformStatus);
      servicePlatformSynDS.updateServicePlatformSyn(servicePlatformSyn);       
    }
    
    /**
     *  判断临时区地址是否可以正确获取
     * */  
    private boolean getTempPath() throws DomainServiceException{
        //获取临时区地址
        boolean flg = true;
        String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
        if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
        {
            flg = false;
            return flg;
        }
        else
        {
            File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
            if (!makePath.exists())
            {// 找不到目标地址，请检查存储区绑定
                flg = false;
                return flg;
            }             
        }
        return flg;
        }
    
    /**
     *  插入对象日志表
     * */  
    private void insertObjectRecord( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype,String servicecode, Long targetindex,int synType, int platform)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setObjectcode(servicecode);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            if(platform == 1){
                objectSyncRecord.setObjectid(objectid);
                //TODO 需要必要时候的修改 servicecode，2.0 objectid的值最后确定为objectid不是文广code
            }else{
                objectSyncRecord.setObjectid(objectid);               
            }           
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }  
       catch (Exception e)
       {
           log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    /**
     *  插入对象日志表(mapping)
     * */  
    private void insertObjectRecordForMap( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype,String parentid, String elementid, Long targetindex,int synType, int platform,String parentcode,String elementcode)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setParentid(parentid);
            objectSyncRecord.setElementid(elementid);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            if(platform == 1)
            {
                objectSyncRecord.setParentcode(parentcode);
                objectSyncRecord.setElementcode(elementcode);
                
            }
            
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }  
       catch (Exception e)
       {
           log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    
    
    
 /**
    * 插入同步任务表 
    * */  
    private void insertSyncTask(Long taskindex, String xmlPath, String correlateid, Long targetindex,Long batchid,int status)
    {
 
            CntSyncTask cntSyncTask = new CntSyncTask();
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            try
            {
                cntSyncTask.setTaskindex(taskindex);
                cntSyncTask.setStatus(status);
                cntSyncTask.setBatchid(batchid);
                cntSyncTask.setPriority(5);
                cntSyncTask.setRetrytimes(3);
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(xmlPath);
                cntSyncTask.setDestindex(Long.valueOf(targetindex));
                cntSyncTask.setSource(1);
                cntSyncTask.setStarttime(dateformat.format(new Date()));
                cntSyncTaskDS.insertCntSyncTask(cntSyncTask);
            }
            catch (Exception e)
            {
                log.error("cmsProgramLS exception:" + e.getMessage());
            }
    }
    
    
    
//*****************************服务绑定关系发布管理***********************************
    //***********************************页面展示查询功能***************************
    public TableDataInfo pageInfoQueryforServiceProgramBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws Exception
    {
        log.debug("pageInfoQuery  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        // 格式化内容带有时间的字段
        servicePlatformSyn.setCreatetime1(DateUtil2.get14Time(servicePlatformSyn.getCreatetime1()));
        servicePlatformSyn.setCreatetime2(DateUtil2.get14Time(servicePlatformSyn.getCreatetime2()));
        
        servicePlatformSyn.setPublishtime1(DateUtil2.get14Time(servicePlatformSyn.getPublishtime1()));
        servicePlatformSyn.setPublishtime2(DateUtil2.get14Time(servicePlatformSyn.getPublishtime2()));
        
        servicePlatformSyn.setCancelpubtime1(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime1()));
        servicePlatformSyn.setCancelpubtime2(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime2()));
        //对特殊字符的处理
        if(servicePlatformSyn.getProgramid()!=null && !"".equals(servicePlatformSyn.getProgramid())){
            servicePlatformSyn.setProgramid(EspecialCharMgt.conversion(servicePlatformSyn.getProgramid()));
        }
        if(servicePlatformSyn.getNamecn()!=null && !"".equals(servicePlatformSyn.getNamecn())){
            servicePlatformSyn.setNamecn(EspecialCharMgt.conversion(servicePlatformSyn.getNamecn()));
        }     
        
        servicePlatformSyn.setOrderCond("objectid desc");       

         try {
            dataInfo = servicePlatformSynDS.pageInfoQueryforServiceProgramBing(servicePlatformSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;
    
    }

    public TableDataInfo pageInfoQueryforServiceSeriesBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws Exception
    {
        log.debug("pageInfoQuery  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        // 格式化内容带有时间的字段
        servicePlatformSyn.setCreatetime1(DateUtil2.get14Time(servicePlatformSyn.getCreatetime1()));
        servicePlatformSyn.setCreatetime2(DateUtil2.get14Time(servicePlatformSyn.getCreatetime2()));
        
        servicePlatformSyn.setPublishtime1(DateUtil2.get14Time(servicePlatformSyn.getPublishtime1()));
        servicePlatformSyn.setPublishtime2(DateUtil2.get14Time(servicePlatformSyn.getPublishtime2()));
        
        servicePlatformSyn.setCancelpubtime1(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime1()));
        servicePlatformSyn.setCancelpubtime2(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime2()));
        //对特殊字符的处理
        if(servicePlatformSyn.getSeriesid()!=null && !"".equals(servicePlatformSyn.getSeriesid())){
            servicePlatformSyn.setSeriesid(EspecialCharMgt.conversion(servicePlatformSyn.getSeriesid()));
        }
        if(servicePlatformSyn.getNamecn()!=null && !"".equals(servicePlatformSyn.getNamecn())){
            servicePlatformSyn.setNamecn(EspecialCharMgt.conversion(servicePlatformSyn.getNamecn()));
        }
  
        servicePlatformSyn.setOrderCond("objectid desc");       

         try {
            dataInfo = servicePlatformSynDS.pageInfoQueryforServiceSeriesBing(servicePlatformSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;
    
    }

    public TableDataInfo pageInfoQueryforServiceChannelBing(ServicePlatformSyn servicePlatformSyn, int start,
            int pageSize) throws Exception
    {
        log.debug("pageInfoQuery  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        // 格式化内容带有时间的字段
        servicePlatformSyn.setCreatetime1(DateUtil2.get14Time(servicePlatformSyn.getCreatetime1()));
        servicePlatformSyn.setCreatetime2(DateUtil2.get14Time(servicePlatformSyn.getCreatetime2()));
        
        servicePlatformSyn.setPublishtime1(DateUtil2.get14Time(servicePlatformSyn.getPublishtime1()));
        servicePlatformSyn.setPublishtime2(DateUtil2.get14Time(servicePlatformSyn.getPublishtime2()));
        
        servicePlatformSyn.setCancelpubtime1(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime1()));
        servicePlatformSyn.setCancelpubtime2(DateUtil2.get14Time(servicePlatformSyn.getCancelpubtime2()));
        //对特殊字符的处理
        if(servicePlatformSyn.getChannelid()!=null && !"".equals(servicePlatformSyn.getChannelid())){
            servicePlatformSyn.setChannelid(EspecialCharMgt.conversion(servicePlatformSyn.getChannelid()));
        }
        if(servicePlatformSyn.getChannelname()!=null && !"".equals(servicePlatformSyn.getChannelname())){
            servicePlatformSyn.setChannelname(EspecialCharMgt.conversion(servicePlatformSyn.getChannelname()));
        }       
        servicePlatformSyn.setOrderCond("objectid desc");       

         try {
            dataInfo = servicePlatformSynDS.pageInfoQueryforServiceChannelBing(servicePlatformSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuery  end.................................");
        return dataInfo;    
    }

    public TableDataInfo pageSerProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws Exception
    {
        log.debug("pageSerProMapTargetBing  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        serviceTargetSyn.setOrderCond("objectid desc");   
        try {
            dataInfo = serviceTargetSynDS.serProMapTargetBing(serviceTargetSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageSerProMapTargetBing  end.................................");
        return dataInfo;
 
    }

    public TableDataInfo pageSerSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws Exception
    {
        log.debug("pageSerSerMapTargetBing  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        serviceTargetSyn.setOrderCond("objectid desc");   
        try {
            dataInfo = serviceTargetSynDS.serSerMapTargetBing(serviceTargetSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageSerSerMapTargetBing  end.................................");
        return dataInfo;
 
    }

    public TableDataInfo pageSerChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws Exception
    {
        log.debug("pageSerChaMapTargetBing  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        serviceTargetSyn.setOrderCond("objectid desc");   
        try {
            dataInfo = serviceTargetSynDS.serChaMapTargetBing(serviceTargetSyn, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageSerChaMapTargetBing  end.................................");
        return dataInfo;
    }
    
    
    
    //***********************************服务绑定关系发布功能实现******************************
    
    //服务与关联关系网元发布
    public String bingTargetPublish(Long syncindex) throws Exception
    {

        log.debug("publish serviceObeject mapping start...");        
       boolean tempPath = getTempPath();
       String bingObject = "";
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }  
          
       //获得网mapping元发布对象
        ServiceTargetSyn mapping = new ServiceTargetSyn();
        mapping.setSyncindex(syncindex);
        mapping =serviceTargetSynDS.getMappingTargetSyn(mapping);
        
       List<ServiceTargetSyn> ysmappingList = new ArrayList<ServiceTargetSyn>();
        
        List<Physicalchannel> physicalchannelList = new ArrayList<Physicalchannel>();
        
        if(mapping == null){
            return "1:没有可发布的打包关系 ";
        }
        
       //判断mapping关系是否存在
      int type = mapping.getObjecttype();
      if(type == 21)//表示服务与点播内容的绑定关系
      {
          bingObject = "点播内容";
          ServiceProgramMap spmap = new ServiceProgramMap();
          spmap.setMapindex(mapping.getObjectindex());  
          spmap = serviceProgramMapDS.getServiceProgramMap(spmap);
       
          if(spmap == null){
              return "1:服务与点播内容打包关系不存在 ";
          }
          CmsProgram program = new CmsProgram();
          program.setProgramid(mapping.getElementid());
          program =  cmsProgramDS.getCmsProgramById(program);
          if(program == null){
              return "1:点播内容不存在 ";             
          }
          if(program.getStatus() != 0){
              return "1:点播内容状态不正确 ";             
          }        
          mapping.setCpcontentid(program.getCpcontentid());     
          mapping.setSequence(spmap.getSequence());        
 
      }
      if(type == 22)//表示服务与连续剧的绑定关系
      {
          bingObject = "连续剧";
          ServiceSeriesMap ssmap = new ServiceSeriesMap();
          ssmap.setMapindex(mapping.getObjectindex());
          ssmap = serviceSeriesMapDS.getServiceSeriesMap(ssmap);
          if(ssmap == null){
              return "1:服务与连续剧打包关系不存在 ";
          }
          CmsSeries series = new CmsSeries();
          series.setSeriesid(mapping.getElementid());
         series = cmsSeriesDS.getCmsSeriesById(series) ;   
         if(series == null){
             return "1:连续剧不存在 ";             
         }
         if(series.getStatus() != 0){
             return "1:连续剧状态不正确 "; 
         }
          mapping.setCpcontentid(series.getCpcontentid());
          mapping.setSequence(ssmap.getSequence());
          
      }
      if(type == 23)//表示服务与直播频道的绑定关系
      {
          bingObject = "直播频道";  
          ServiceChannelMap scmap = new ServiceChannelMap();
          scmap.setMapindex(mapping.getObjectindex());
          scmap = serviceChannelMapDS.getServiceChannelMap(scmap);
          if(scmap == null){
              return "1:服务与直播频道打包关系不存在 ";
          }
          CmsChannel channel = new CmsChannel();
          channel.setChannelid(mapping.getElementid());
          channel = cmsChannelDS.getCmsChannelById(channel) ;  
          
          Physicalchannel physicalchannle = new Physicalchannel();
          physicalchannle.setChannelid(mapping.getElementid());
          physicalchannelList = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannle);
          
          if(channel == null){
              return "1:直播频道不存在 ";             
          }
          if(channel.getStatus() != 0){
              return "1:直播频道状态不正确 ";
          }
          if(physicalchannelList.size() ==0){
              return "1:直播频道下物理不存在 ";
          }
          mapping.setCpcontentid(channel.getCpcontentid());
          mapping.setSequence(scmap.getSequence());
          
          for(int i=0; i<physicalchannelList.size(); i++){
              ServiceTargetSyn ysmapping = new ServiceTargetSyn();
              ysmapping.setCpcontentid(physicalchannelList.get(i).getCpcontentid());
              ysmapping.setSequence(scmap.getSequence());
              ysmappingList.add(ysmapping);  
          }
          
      }
       
   
        //要发布的目标网元是否存在
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargetindex(mapping.getTargetindex());
        Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
        if(targetSystemExist == null){
            //目标网元不存在，退出
            return "1:服务与"+bingObject+"的打包关系发布的网元不存在 ";
        }
        //目标网元状态是否正常
        if(targetSystemExist.getStatus() !=0){
            //目标网元状态不是正常
            return "1:服务与"+bingObject+"的打包关系发布的网元状态不正确 ";
        }       
        
        String targetid = targetSystemExist.getTargetid();
        
        //判断绑定对象的发布状态是否为发布成功
        ServiceTargetSyn parent = new ServiceTargetSyn();
        parent.setObjectid(mapping.getParentid());
        parent.setTargetindex(mapping.getTargetindex());
        parent = serviceTargetSynDS.getObjectTargetSyn(parent);
        
        if(parent.getStatus()!=300 && parent.getStatus()!=500){
            
            return "1:服务在网元"+targetid+"上的发布状态不正确 ";

        }

        //判断被绑定对象的发布状态是否为发布成功
        ServiceTargetSyn element = new ServiceTargetSyn();
        element.setObjectid(mapping.getElementid());
        element.setTargetindex(mapping.getTargetindex());
        element = serviceTargetSynDS.getObjectTargetSyn(element);
        
        if(element.getStatus()!=300 && parent.getStatus()!=500){
                
                return "1:"+bingObject+"在网元"+targetid+"上的发布状态不正确 ";
    
        }

       //获取目标系统类型    
      int targettype =targetSystemExist.getTargettype();
    
      //获取平台类型
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(mapping.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();
     
      String xmlPath = "";
      int targetResult = mapping.getOperresult();
      
      Service service = new Service();
      service.setServiceid(mapping.getParentid());
      service = serviceDS.getServiceById(service);
      if(service == null){
          return "1:服务不存在 ";
      }
      int serviceStatus = service.getStatus();
      if(serviceStatus != 0){
        return "1:服务状态不正确 ";
          
      } 
      mapping.setServicecode(service.getServicecode());
      
     if(targettype == 10){
         for(int i=0; i<ysmappingList.size(); i++){
             ysmappingList.get(i).setObjectid(mapping.getObjectid());
             ysmappingList.get(i).setServicecode(mapping.getServicecode());
         }
     }
      
      // TODO 包装map
      Map map = new HashMap<String, List>(); 
      List<ServiceTargetSyn> serviceMapList = new ArrayList<ServiceTargetSyn>();
      serviceMapList.add(mapping);
      
      map.put("servicemapping", serviceMapList);
      map.put("ysservicechannelmapping", ysmappingList);
      if(targetResult == 0 
              || targetResult == 30 
              || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
      {       //生成xml文件    
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
      }else {                   
              //网元发布状态为不可发布状态，退出
              return  "1:服务打包"+bingObject+"在网元" +targetid+	"上的发布状态不正确 ";      
      }
      
      if(xmlPath == null){
          return "1:发布到目标系统"+targetid+":生成xml文件失败";
      }
 
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = mapping.getTargetindex();
     
       
        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = mapping.getObjectindex();
       String objectid = mapping.getObjectid();
       int objecttype = mapping.getObjecttype();
       String parentid = mapping.getParentid();
       String elementid = mapping.getElementid();
       String parentcode = mapping.getServicecode();
       String elementcode = mapping.getCpcontentid();
       insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
       
       //insertSyncTask(taskindex,xmlPath,correlateid,targetindex); //插入同步任务
       Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
       insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( mapping, 1);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_BING_PUBLISH,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_MAP_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      log.debug("publish  serviceObjectMapping end...");
      
      String returnInfo ="0:操作成功";
      return returnInfo;    
    }
    
    private String bingTargetPubforPlatform(Long syncindex,Long batchid,int firTargetType) throws Exception{


        log.debug("publish serviceObeject mapping start...");        
       boolean tempPath = getTempPath();
       String bingObject = "";
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }  
          
       //获得网mapping元发布对象
        ServiceTargetSyn mapping = new ServiceTargetSyn();
        mapping.setSyncindex(syncindex);
        mapping =serviceTargetSynDS.getMappingTargetSyn(mapping);
        if(mapping == null){
            return "1:没有可发布的打包关系 ";
        }
        
        List<ServiceTargetSyn> ysmappingList = new ArrayList<ServiceTargetSyn>();
        List<Physicalchannel> physicalchannelList = new ArrayList<Physicalchannel>();
       //判断mapping关系是否存在
      int type = mapping.getObjecttype();
      if(type == 21)//表示服务与点播内容的绑定关系
      {
          bingObject = "点播内容";
          ServiceProgramMap spmap = new ServiceProgramMap();
          spmap.setMapindex(mapping.getObjectindex());  
          spmap = serviceProgramMapDS.getServiceProgramMap(spmap);
       
          if(spmap == null){
              return "1:服务与点播内容打包关系不存在 ";
          }
          CmsProgram program = new CmsProgram();
          program.setProgramid(mapping.getElementid());
          program =  cmsProgramDS.getCmsProgramById(program);
          if(program == null){
              return "1:点播内容不存在 ";             
          }
          if(program.getStatus() != 0){
              return "1:点播内容状态不正确 ";             
          }        
          mapping.setCpcontentid(program.getCpcontentid());     
          mapping.setSequence(spmap.getSequence());        
 
      }
      if(type == 22)//表示服务与连续剧的绑定关系
      {
          bingObject = "连续剧";
          ServiceSeriesMap ssmap = new ServiceSeriesMap();
          ssmap.setMapindex(mapping.getObjectindex());
          ssmap = serviceSeriesMapDS.getServiceSeriesMap(ssmap);
          if(ssmap == null){
              return "1:服务与连续剧打包关系不存在 ";
          }
          CmsSeries series = new CmsSeries();
          series.setSeriesid(mapping.getElementid());
         series = cmsSeriesDS.getCmsSeriesById(series) ;   
         if(series == null){
             return "1:连续剧不存在 ";             
         }
         if(series.getStatus() != 0){
             return "1:连续剧状态不正确 "; 
         }
          mapping.setCpcontentid(series.getCpcontentid());
          mapping.setSequence(ssmap.getSequence());
          
      }
      if(type == 23)//表示服务与直播频道的绑定关系
      {
          bingObject = "直播频道";  
          ServiceChannelMap scmap = new ServiceChannelMap();
          scmap.setMapindex(mapping.getObjectindex());
          scmap = serviceChannelMapDS.getServiceChannelMap(scmap);
          if(scmap == null){
              return "1:服务与直播频道打包关系不存在 ";
          }
          CmsChannel channel = new CmsChannel();
          channel.setChannelid(mapping.getElementid());
          channel = cmsChannelDS.getCmsChannelById(channel) ;    
          
          Physicalchannel physicalchannle = new Physicalchannel();
          physicalchannle.setChannelid(mapping.getElementid());
          physicalchannelList = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannle);
          
          if(channel == null){
              return "1:直播频道不存在 ";             
          }
          if(channel.getStatus() != 0){
              return "1:直播频道状态不正确 ";
          }
          if(physicalchannelList.size() ==0){
              return "1:直播频道下物理不存在 ";
          }
          mapping.setCpcontentid(channel.getCpcontentid());
          mapping.setSequence(scmap.getSequence());
          
          for(int i=0; i<physicalchannelList.size(); i++){
              ServiceTargetSyn ysmapping = new ServiceTargetSyn();
              ysmapping.setCpcontentid(physicalchannelList.get(i).getCpcontentid());
              ysmapping.setSequence(scmap.getSequence());
              ysmappingList.add(ysmapping);  
          }
          
          
          
      }
    
        //要发布的目标网元是否存在
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargetindex(mapping.getTargetindex());
        Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
        if(targetSystemExist == null){
            //目标网元不存在，退出
            return "1:服务与"+bingObject+"的打包关系发布的网元不存在 ";
        }
        //目标网元状态是否正常
        if(targetSystemExist.getStatus() !=0){
            //目标网元状态不是正常
            return "1:服务与"+bingObject+"的打包关系发布的网元状态不正确 ";
        }       
        
        String targetid = targetSystemExist.getTargetid();
        
        //判断绑定对象的发布状态是否为发布成功
        ServiceTargetSyn parent = new ServiceTargetSyn();
        parent.setObjectid(mapping.getParentid());
        parent.setTargetindex(mapping.getTargetindex());
        parent = serviceTargetSynDS.getObjectTargetSyn(parent);
        
        if(parent.getStatus()!=300 && parent.getStatus()!=500){
            
            return "1:服务在网元"+targetid+"上的发布状态不正确 ";

        }

        //判断被绑定对象的发布状态是否为发布成功
        ServiceTargetSyn element = new ServiceTargetSyn();
        element.setObjectid(mapping.getElementid());
        element.setTargetindex(mapping.getTargetindex());
        element = serviceTargetSynDS.getObjectTargetSyn(element);
        
        if(element.getStatus()!=300 && parent.getStatus()!=500){
                
                return "1:"+bingObject+"在网元"+targetid+"上的发布状态不正确 ";
    
        }

       //获取目标系统类型    
      int targettype =targetSystemExist.getTargettype();
    
      //获取平台类型
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(mapping.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();
     
      String xmlPath = "";
      int targetResult = mapping.getOperresult();
      
      Service service = new Service();
      service.setServiceid(mapping.getParentid());
      service = serviceDS.getServiceById(service);
      if(service == null){
          return "1:服务不存在 ";
      }
      int serviceStatus = service.getStatus();
      if(serviceStatus != 0){
        return "1:服务状态不正确 ";
          
      } 
      mapping.setServicecode(service.getServicecode());
      
      if(targettype == 10){
          for(int i=0; i<ysmappingList.size(); i++){
              ysmappingList.get(i).setObjectid(mapping.getObjectid());
              ysmappingList.get(i).setServicecode(mapping.getServicecode());
          }
      }
      
      // TODO 包装map
      Map map = new HashMap<String, List>(); 
      List<ServiceTargetSyn> serviceMapList = new ArrayList<ServiceTargetSyn>();
      serviceMapList.add(mapping);
      
      map.put("servicemapping", serviceMapList);
      map.put("ysservicechannelmapping", ysmappingList);
  
      if(targetResult == 0 
              || targetResult == 30 
              || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
      {       //生成xml文件    
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
      }else {                   
              //网元发布状态为不可发布状态，退出
              return  "1:服务打包"+bingObject+"在网元" +targetid+   "上的发布状态不正确 ";   
      }
      
      if(xmlPath == null){
          return "1:发布到目标系统"+targetid+":生成xml文件失败";
      }
 
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = mapping.getTargetindex();
     
       
        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = mapping.getObjectindex();
       String objectid = mapping.getObjectid();
       int objecttype = mapping.getObjecttype();
       String parentid = mapping.getParentid();
       String elementid = mapping.getElementid();
       String parentcode = mapping.getServicecode();
       String elementcode = mapping.getCpcontentid();
       insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
       
       if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
           Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
       }else if(firTargetType == targettype){
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       }else{
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
       }
       //insertSyncTask(taskindex,xmlPath,correlateid,targetindex); //插入同步任务
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( mapping, 1);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_BING_PUBLISH,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_MAP_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      log.debug("publish  serviceObjectMapping end...");
      
      String returnInfo ="0:发布到目标系统"+targetid+":操作成功 ";
      return returnInfo;    
    
        
    }
    
    /**
     *  服务与打包关系平台发布
     * 
     * */
    
    public String bingPlatformPublish(Long syncindex)throws Exception{
        
      log.debug("publish platform for mapping start...");
      boolean successFlg = false;
      StringBuffer allresult = new StringBuffer();  
      
      ServicePlatformSyn  servicePlatform = new ServicePlatformSyn();
      servicePlatform.setSyncindex(syncindex);
      servicePlatform = servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatform);
      if(servicePlatform == null ){
          return  "1:平台发布数据不存在 ";           
      }
      int platformStatus =  servicePlatform.getStatus();
      if(platformStatus== 200 || platformStatus == 300 ){
          return   "1:平台发布状态不正确 ";        
      }      
      ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();
      serviceTargetSyn.setRelateindex(syncindex);
      List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getServiceTargetSynByCond(serviceTargetSyn);
      if(serviceList == null || serviceList.size()==0){
          return  "1:该打包关系没有关联网元";
      }
      String BMSPub = getBMSPub(serviceList);
      if(BMSPub.substring(0,1).equals("1")){
          return BMSPub;
      }
      
      
      int size = serviceList.size();
      Long batchid = null;
      
      int firTargetType = findFirTargettype(serviceList,0);
      if(firTargetType != 99)
          batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
      
      
      for(int i=0; i< size; i++){
          ServiceTargetSyn service = serviceList.get(i);
          Long index = service.getSyncindex();  
          int objecttype = service.getObjecttype();
          
          Targetsystem targetSystem = new Targetsystem();
          targetSystem.setTargetindex(service.getTargetindex());
          Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
          String targetid = targetSystemExist.getTargetid();
          
          
          if(service.getStatus() == 200 || service.getStatus() == 300 ){
              if( objecttype == 21){
                    allresult.append("服务打包点播内容在网元"+targetid+"上的发布状态不正确 ");  
              }
              if( objecttype == 22){
                  allresult.append("服务打包连续剧在网元"+targetid+"上的发布状态不正确 ");  
            }
              if( objecttype == 23){
                  allresult.append("服务打包直播频道在网元"+targetid+"上的发布状态不正确 ");  
            }            
              continue; 
          }
          String rstInfo= bingTargetPubforPlatform(index,batchid,firTargetType);  
          if ( rstInfo.substring(0, 1).equals("0"))
          {
              successFlg = true;
              allresult.append(rstInfo.substring(2));
          }else
          {
              allresult.append(rstInfo.substring(2));
       }
          }                       
       log.debug("publish platform for  series castrolemap mapping end...");
      if(successFlg){
          return "0:"+allresult.toString();         
      }else{//全部失败
          return "1:"+allresult.toString();            
      }
    }

    
    /**
     *  服务与打包关系批量发布
     * 
     * */
    
    public String bingBatchPublish(List<ServiceTargetSyn> mappingList)throws Exception{

        log.debug("publish batch for mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
       
        int size = mappingList.size();     
        for(ServiceTargetSyn mapping:mappingList){
            
            Long syncindex = mapping.getSyncindex();
            retInfo=  bingPlatformPublish(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                   
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量：" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量：" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + iSuccessed + "  失败数量：" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("publish batch for mapping end...");
        return returnInfo.toString();          
    }

   
    /**
     *  服务与打包关系网元取消发布
     * 
     * */

    public String bingTargetDelPublish(Long syncindex) throws Exception
    {

        log.debug("delete publish serviceObeject mapping start...");        
       boolean tempPath = getTempPath();
       String bingObject = "";
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }  
          
       //获得网mapping元发布对象
        ServiceTargetSyn mapping = new ServiceTargetSyn();
        mapping.setSyncindex(syncindex);
        mapping =serviceTargetSynDS.getMappingTargetSyn(mapping);
        if(mapping == null){
            return "1:没有可发布的打包关系 ";
        }
        List<ServiceTargetSyn> ysmappingList = new ArrayList<ServiceTargetSyn>();
        List<Physicalchannel> physicalchannelList = new ArrayList<Physicalchannel>();
       //判断mapping关系是否存在
      int type = mapping.getObjecttype();
      if(type == 21)//表示服务与点播内容的绑定关系
      {
          bingObject = "点播内容";
          ServiceProgramMap spmap = new ServiceProgramMap();
          spmap.setMapindex(mapping.getObjectindex());  
          spmap = serviceProgramMapDS.getServiceProgramMap(spmap);
       
          if(spmap == null){
              return "1:服务与点播内容打包关系不存在 ";
          }
          CmsProgram program = new CmsProgram();
          program.setProgramid(mapping.getElementid());
          program =  cmsProgramDS.getCmsProgramById(program);
          if(program != null){
              mapping.setCpcontentid(program.getCpcontentid());   
              mapping.setSequence(spmap.getSequence());
          }          
      }
      if(type == 22)//表示服务与连续剧的绑定关系
      {
          bingObject = "连续剧";
          ServiceSeriesMap ssmap = new ServiceSeriesMap();
          ssmap.setMapindex(mapping.getObjectindex());
          ssmap = serviceSeriesMapDS.getServiceSeriesMap(ssmap);
          if(ssmap == null){
              return "1:服务与连续剧打包关系不存在 ";
          }
          CmsSeries series = new CmsSeries();
          series.setSeriesid(mapping.getElementid());
         series = cmsSeriesDS.getCmsSeriesById(series) ;   
         if(series != null){
              mapping.setCpcontentid(series.getCpcontentid());
              mapping.setSequence(ssmap.getSequence());
         } 
      }
      if(type == 23)//表示服务与直播频道的绑定关系
      {
          bingObject = "直播频道";  
          ServiceChannelMap scmap = new ServiceChannelMap();
          scmap.setMapindex(mapping.getObjectindex());
          scmap = serviceChannelMapDS.getServiceChannelMap(scmap);
          if(scmap == null){
              return "1:服务与直播频道打包关系不存在 ";
          }
          CmsChannel channel = new CmsChannel();
          channel.setChannelid(mapping.getElementid());
          channel = cmsChannelDS.getCmsChannelById(channel) ;
          
          Physicalchannel physicalchannle = new Physicalchannel();
          physicalchannle.setChannelid(mapping.getElementid());
          physicalchannelList = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannle);
          if(channel != null){
              mapping.setCpcontentid(channel.getCpcontentid());
              mapping.setSequence(scmap.getSequence());
              
              for(int i=0; i<physicalchannelList.size(); i++){
                  ServiceTargetSyn ysmapping = new ServiceTargetSyn();
                  ysmapping.setCpcontentid(physicalchannelList.get(i).getCpcontentid());
                  ysmapping.setSequence(scmap.getSequence());
                  ysmappingList.add(ysmapping);  
              }
          }       
      }
      
        //要发布的目标网元是否存在
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargetindex(mapping.getTargetindex());
        Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
        if(targetSystemExist == null){
            //目标网元不存在，退出
            return "1:服务与"+bingObject+"打包关系发布的网元不存在 ";
        }
        //目标网元状态是否正常
        if(targetSystemExist.getStatus() !=0){
            //目标网元状态不是正常
            return "1:服务与"+bingObject+"打包关系布的网元状态不正确 ";
        }       
        
        String targetid = targetSystemExist.getTargetid();

       //获取目标系统类型    
      int targettype =targetSystemExist.getTargettype();
    
      //获取平台类型
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(mapping.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();
     
      String xmlPath = "";
      int targetResult = mapping.getOperresult();
      
      Service service = new Service();
      service.setServiceid(mapping.getParentid());
      service = serviceDS.getServiceById(service);
      if(service != null){
              mapping.setServicecode(service.getServicecode()); 
      }
      
      if(targettype == 10){
          for(int i=0; i<ysmappingList.size(); i++){
              ysmappingList.get(i).setObjectid(mapping.getObjectid());
              ysmappingList.get(i).setServicecode(mapping.getServicecode());
          }
      }
      
      // TODO 包装map
      Map map = new HashMap<String, List>(); 
      List<ServiceTargetSyn> serviceMapList = new ArrayList<ServiceTargetSyn>();
      serviceMapList.add(mapping);
      
      
      map.put("servicemapping", serviceMapList);
      map.put("ysservicechannelmapping", ysmappingList);
      if(targetResult == 20           
              || targetResult == 90)// 20新增同步成功// 90取消同步失败
      {       //生成xml文件    
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
      }else {                   
              //网元发布状态为不可发布状态，退出                
              return  "1:服务打包"+bingObject+"在网元" +targetid+   "上的发布状态不正确 ";   
      }
      
      if(xmlPath == null){
          return "1:从目标系统"+targetid+"取消发布:生成xml文件失败";
      }
 
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = mapping.getTargetindex();

        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = mapping.getObjectindex();
       String objectid = mapping.getObjectid();
       int objecttype = mapping.getObjecttype();
       String parentid = mapping.getParentid();
       String elementid = mapping.getElementid();   
       String parentcode = mapping.getServicecode();
       String elementcode = mapping.getCpcontentid();
       insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
       
       Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
       insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( mapping, 3);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_BING_PUBLISH_DEL,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL_MAP_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      log.debug(" delete publish  serviceObjectMapping end...");
      String returnInfo ="0:操作成功";
      return returnInfo;  
    }
    private String bingTargetDelPubforPlatform(Long syncindex,Long batchid,int firTargetType) throws Exception{


        log.debug("delete publish serviceObeject mapping start...");        
       boolean tempPath = getTempPath();
       String bingObject = "";
       if(!tempPath ){
           return "1:获取临时区失败 ";
       }  
          
       //获得网mapping元发布对象
        ServiceTargetSyn mapping = new ServiceTargetSyn();
        mapping.setSyncindex(syncindex);
        mapping =serviceTargetSynDS.getMappingTargetSyn(mapping);
        if(mapping == null){
            return "1:没有可发布的打包关系 ";
        }
        List<ServiceTargetSyn> ysmappingList = new ArrayList<ServiceTargetSyn>();
        List<Physicalchannel> physicalchannelList = new ArrayList<Physicalchannel>();
        
       //判断mapping关系是否存在
      int type = mapping.getObjecttype();
      if(type == 21)//表示服务与点播内容的绑定关系
      {
          bingObject = "点播内容";
          ServiceProgramMap spmap = new ServiceProgramMap();
          spmap.setMapindex(mapping.getObjectindex());  
          spmap = serviceProgramMapDS.getServiceProgramMap(spmap);
       
          if(spmap == null){
              return "1:服务与点播内容打包关系不存在 ";
          }
          CmsProgram program = new CmsProgram();
          program.setProgramid(mapping.getElementid());
          program =  cmsProgramDS.getCmsProgramById(program);
          if(program != null){
              mapping.setCpcontentid(program.getCpcontentid());   
              mapping.setSequence(spmap.getSequence());
          }          
      }
      if(type == 22)//表示服务与连续剧的绑定关系
      {
          bingObject = "连续剧";
          ServiceSeriesMap ssmap = new ServiceSeriesMap();
          ssmap.setMapindex(mapping.getObjectindex());
          ssmap = serviceSeriesMapDS.getServiceSeriesMap(ssmap);
          if(ssmap == null){
              return "1:服务与连续剧打包关系不存在 ";
          }
          CmsSeries series = new CmsSeries();
          series.setSeriesid(mapping.getElementid());
         series = cmsSeriesDS.getCmsSeriesById(series) ;   
         if(series != null){
              mapping.setCpcontentid(series.getCpcontentid());
              mapping.setSequence(ssmap.getSequence());
         } 
      }
      if(type == 23)//表示服务与直播频道的绑定关系
      {
          bingObject = "直播频道";  
          ServiceChannelMap scmap = new ServiceChannelMap();
          scmap.setMapindex(mapping.getObjectindex());
          scmap = serviceChannelMapDS.getServiceChannelMap(scmap);
          if(scmap == null){
              return "1:服务与直播频道打包关系不存在 ";
          }
          CmsChannel channel = new CmsChannel();
          channel.setChannelid(mapping.getElementid());
          channel = cmsChannelDS.getCmsChannelById(channel) ; 
          
          Physicalchannel physicalchannle = new Physicalchannel();
          physicalchannle.setChannelid(mapping.getElementid());
          physicalchannelList = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannle);
          if(channel != null){
              mapping.setCpcontentid(channel.getCpcontentid());
              mapping.setSequence(scmap.getSequence());
              
              for(int i=0; i<physicalchannelList.size(); i++){
                  ServiceTargetSyn ysmapping = new ServiceTargetSyn();
                  ysmapping.setCpcontentid(physicalchannelList.get(i).getCpcontentid());
                  ysmapping.setSequence(scmap.getSequence());
                  ysmappingList.add(ysmapping);  
              }
          }        
      }
      
        //要发布的目标网元是否存在
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargetindex(mapping.getTargetindex());
        Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
        if(targetSystemExist == null){
            //目标网元不存在，退出
            return "1:服务与"+bingObject+"打包关系发布的网元不存在 ";
        }
        //目标网元状态是否正常
        if(targetSystemExist.getStatus() !=0){
            //目标网元状态不是正常
            return "1:服务与"+bingObject+"打包关系布的网元状态不正确 ";
        }       
        
        String targetid = targetSystemExist.getTargetid();

       //获取目标系统类型    
      int targettype =targetSystemExist.getTargettype();
    
      //获取平台类型
      ServicePlatformSyn  servicePlatfoermSyn = new ServicePlatformSyn();
      servicePlatfoermSyn.setSyncindex(mapping.getRelateindex());
      ServicePlatformSyn platfrom =  servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatfoermSyn);
      int platformNum = (int)platfrom.getPlatform();
     
      String xmlPath = "";
      int targetResult = mapping.getOperresult();
      
      Service service = new Service();
      service.setServiceid(mapping.getParentid());
      service = serviceDS.getServiceById(service);
      if(service != null){
              mapping.setServicecode(service.getServicecode()); 
      }
      
      if(targettype == 10){
          for(int i=0; i<ysmappingList.size(); i++){
              ysmappingList.get(i).setObjectid(mapping.getObjectid());
              ysmappingList.get(i).setServicecode(mapping.getServicecode());
          }
      }
      
      // TODO 包装map
      Map map = new HashMap<String, List>(); 
      List<ServiceTargetSyn> serviceMapList = new ArrayList<ServiceTargetSyn>();
      serviceMapList.add(mapping);
      
      map.put("servicemapping", serviceMapList);
      map.put("ysservicechannelmapping", ysmappingList);
      if(targetResult == 20           
              || targetResult == 90)// 20新增同步成功// 90取消同步失败
      {       //生成xml文件    
          xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
      }else {                   
              //网元发布状态为不可发布状态，退出
              return  "1:服务打包"+bingObject+"在网元" +targetid+   "上的发布状态不正确 ";   
      }
      
      if(xmlPath == null){
          return "1:从目标系统"+targetid+"取消发布:生成xml文件失败";
      }
 
          //插入同步任务表
      String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
      Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
      Long targetindex = mapping.getTargetindex();

        //修改对象日志表
       //service对象日志记录
       Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
       Long objectindex = mapping.getObjectindex();
       String objectid = mapping.getObjectid();
       int objecttype = mapping.getObjecttype();
       String parentid = mapping.getParentid();
       String elementid = mapping.getElementid();   
       String parentcode = mapping.getServicecode();
       String elementcode = mapping.getCpcontentid();
       insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
       
       //insertSyncTask(taskindex,xmlPath,correlateid,targetindex); //插入同步任务
       if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
           Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
       }else if(firTargetType == targettype){
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       }else{
           insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
       }
       
     //修改网元发布状态和总状态表状态
      modTargetSynStatus( mapping, 3);
      
     //写日志
    // 写操作日志
      CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, CommonLogConstant.MGTTYPE_SERVICEPUBLISHSYN,
      CommonLogConstant.OPERTYPE_SERVICE_BING_PUBLISH_DEL,
      CommonLogConstant.OPERTYPE_SERVICE_PUBLISH_DEL_MAP_INFO,
      CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
      log.debug(" delete publish  serviceObjectMapping end...");
      String returnInfo ="0:从目标系统"+targetid+"取消发布: 操作成功 ";
      return returnInfo;  
    
        
    }


    /**
     *  服务与打包关系平台取消发布
     * 
     * */
    public String bingPlatformDelPublish(Long syncindex) throws Exception
    {
        
        log.debug("delete publish platform for mapping start...");
        
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
        
        ServicePlatformSyn  servicePlatform = new ServicePlatformSyn();
        servicePlatform.setSyncindex(syncindex);
        servicePlatform = servicePlatformSynDS.getServicePlatformSynByIndex(servicePlatform);
        if(servicePlatform == null ){
            return  "1:平台发布数据不存在 ";           
        }
        int platformStatu =  servicePlatform.getStatus();
        if(platformStatu== 0 || platformStatu == 200 || platformStatu == 400 ){
            return   "1:平台发布状态不正确 ";        
        }   
   
        ServiceTargetSyn serviceTargetSyn = new ServiceTargetSyn();
        serviceTargetSyn.setRelateindex(syncindex);
        List<ServiceTargetSyn>  serviceList = serviceTargetSynDS.getServiceTargetSynByCond(serviceTargetSyn);
        if(serviceList == null || serviceList.size()==0){
            return  "1:该打包关系没有关联网元";
        }
        
        Long batchid = null;
        
        int firTargetType = findFirTargettype(serviceList,2);
        if(firTargetType != 99)
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        int size = serviceList.size();
        for(int i=0; i<size; i++){
            ServiceTargetSyn service = serviceList.get(i);
            Long index = service.getSyncindex();    
            int objecttype = service.getObjecttype();
            
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(service.getTargetindex());
            Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
            String targetid = targetSystemExist.getTargetid();

            if(service.getStatus() == 0 || service.getStatus() == 200  || service.getStatus() == 400){
                if( objecttype == 21){
                    allresult.append("服务打包点播内容在网元"+targetid+"上的发布状态不正确 ");  
              }
              if( objecttype == 22){
                  allresult.append("服务打包连续剧在网元"+targetid+"上的发布状态不正确 ");  
            }
              if( objecttype == 23){
                  allresult.append("服务打包直播频道在网元"+targetid+"上的发布状态不正确 ");  
            }    
                continue;
            }
            
            String rstInfo= bingTargetDelPubforPlatform(index,batchid,firTargetType);  
            if ( rstInfo.substring(0, 1).equals("0"))
            {
                successFlg = true;
                allresult.append(rstInfo.substring(2));
            }else
            {
                allresult.append(rstInfo.substring(2));
         }
            }                       
        log.debug("delete publish platform for mapping end...");
        if(successFlg){
            return "0:"+allresult.toString();         
        }else{//全部失败
            return "1:"+allresult.toString();            
        }
    }

    /**
     *  服务与打包关系批量取消发布
     * 
     * */
    public String bingBatchDelPublish(List<ServiceTargetSyn> mappingList) throws Exception
    {


        log.debug("delete publish batch for mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
       
        int size = mappingList.size();     
        for(ServiceTargetSyn mapping:mappingList){
            
            Long syncindex = mapping.getSyncindex();
            retInfo=  bingPlatformDelPublish(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                   
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量：" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量：" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + iSuccessed + "  失败数量：" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("delete publish batch for mapping end...");
        return returnInfo.toString();              
    }    
    private int findFirTargettype(List<ServiceTargetSyn> serviceTargetSynList,int optype ) throws DomainServiceException{
        int type=99;
        for (ServiceTargetSyn targetSync : serviceTargetSynList){
            int status = targetSync.getStatus();
            if(optype == 0){//01表示新增
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 0 || status == 400 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 0 || status == 400 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 0 || status == 400 || status == 500))type =2;
            }else if(optype ==1){//1表示修改
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 300 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 300 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 300 || status == 500))type =2;
            }
            else{//2表示取消（删除）
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 2 && (status == 300 || status == 500))type =2;
                if(targettype == 1 && (type==99 || type==3) && (status == 300 || status == 500))type =1;
                if(targettype == 3 && type ==99 && (status == 300 || status == 500))type =3;
            }
        }
        return type;
    }
    private String getBMSPub(List<ServiceTargetSyn> serviceList) throws DomainServiceException{
        for (ServiceTargetSyn serviceTarget : serviceList){
            Targetsystem target = new Targetsystem();
            target.setTargetindex(serviceTarget.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            //只有存在EPG网元且能够取消发布的时候才去判断是否有mapping已发布
            if (target.getTargettype() == 1)
            {
                ServiceTargetSyn parent = new ServiceTargetSyn();
                parent.setObjectid(serviceTarget.getParentid());
                parent.setTargetindex(serviceTarget.getTargetindex());
                parent = serviceTargetSynDS.getObjectTargetSyn(parent);
                
                if(parent.getStatus()!=300 && parent.getStatus()!=500){
                    
                    return "1:服务在网元"+target.getTargetid()+"上未发布，不能进行发布操作 ";

                }
                //判断被绑定对象的发布状态是否为发布成功
                ServiceTargetSyn element = new ServiceTargetSyn();
                element.setObjectid(serviceTarget.getElementid());
                element.setTargetindex(serviceTarget.getTargetindex());
                element = serviceTargetSynDS.getObjectTargetSyn(element);
                
                if(element.getStatus()!=300 && parent.getStatus()!=500){
                    int type = serviceTarget.getObjecttype();
                    String bindObject ="";
                    if(type == 21) bindObject = "点播内容";
                    if(type == 22) bindObject = "连续剧";
                    if(type == 23) bindObject = "直播频道";
                        
                        return "1:"+bindObject+"在网元"+target.getTargetid()+"上未发布，不能进行发布操作  ";
            
                }
                
            }
        }
        return "0";
        
    }
}

package com.zte.cms.content.program.ls;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.Generator;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.apply.ICntApply;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.modwkfw.ICntModApply;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.cms.content.movie.service.ICmsMovieDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramWkfwhis;
import com.zte.cms.content.program.programsync.ls.IProgramPlatformSynLS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.program.service.ICmsProgramWkfwhisDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.ctgpgmap.service.ICategoryProgramMapDS;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.picture.service.IProgramPictureMapDS;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
public class CmsProgramLS extends DynamicObjectBaseDS implements ICmsProgramLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private ICmsProgramDS cmsProgramDS;
    private ICmsMovieDS cmsMovieDS;
    private IUcpBasicDS ucpBasicDS;
    private ICmsFtptaskDS cmsFtptaskDS;
    private IProgramCrmMapDS programCrmMapDS;
    private ICategoryProgramMapDS ctgpgmapDS;
    private IServiceProgramMapDS serviceProgramMapDS;
    private IProgramPictureMapDS programPictureMapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ICmsProgramWkfwhisDS cmsProgramWkfwhisDS;
    private IPictureDS pictureDS;
    private ITargetsystemDS targetSystemDS;
    private ICntApply cntApply = null;
    private ICntModApply cntModApply = null;
    private ICmsLpopNoauditDS cmsLpopNoauditDS;
    private IProgramPlatformSynLS programPlatformSynLS;
    public Long syncindex = null;

    /**
     * 新增CmsProgram对象
     */
    public String insertCmsProgram(CmsProgram cmsProgram) throws Exception
    {
        log.debug("insertCmsProgram starting...");
        String result = ResourceMgt.findDefaultText("cnt.do.success"); 

        try
        {
            // 验证CP和牌照方是否已删闿
            String CPID = cmsProgram.getCpid();
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCpid(CPID);
            ucpBasic.setCptype(1);
            List<UcpBasic> cpList = ucpBasicDS.getUcpBasicByCond(ucpBasic);
            if (cpList != null && cpList.size() > 0)
            {
                ucpBasic = cpList.get(0);
                if (ucpBasic.getStatus() != 1)
                {
                    return ResourceMgt.findDefaultText("do.fail.for.cp.status.wrong");
                }
                else
                {
                    cmsProgram.setCpindex(ucpBasic.getCpindex());
                }
            }
            else
            {
                return ResourceMgt.findDefaultText("do.fail.for.cp.not.exist");
            }
            String LPID = cmsProgram.getLicenseid();
            UcpBasic lpBasic = new UcpBasic();
            lpBasic.setCpid(LPID);
            lpBasic.setCptype(2);
            List<UcpBasic> lpList = ucpBasicDS.getUcpBasicByCond(lpBasic);
            if (lpList != null && lpList.size() > 0)
            {
                lpBasic = lpList.get(0);
                if (lpBasic.getStatus() != 1)
                {
                    return ResourceMgt.findDefaultText("do.fail.for.Lp.status.wrong");
                }
                else
                {
                    cmsProgram.setLicenseindex(lpBasic.getCpindex());
                }
            }
            else
            {
                return ResourceMgt.findDefaultText("do.fail.for.lp.not.exist");
            }

            // 获取内容ID
            String programid = Generator.getContentId(Long.parseLong(cmsProgram.getCpid()), ObjectType.PROG);
            cmsProgram.setProgramid(programid);

            // 格式化内容带有时间的字段
            cmsProgram.setEffectivedate(DateUtil2.get14Time(cmsProgram.getEffectivedate()));
            cmsProgram.setExpirydate(DateUtil2.get14Time(cmsProgram.getExpirydate()));
            cmsProgram.setLlicensofflinetime(DateUtil2.get14Time(cmsProgram.getLlicensofflinetime()));
            cmsProgram.setDeletetime(DateUtil2.get14Time(cmsProgram.getDeletetime()));
            cmsProgram.setLicensingstart(DateUtil2.get14Time(cmsProgram.getLicensingstart()));
            cmsProgram.setLicensingend(DateUtil2.get14Time(cmsProgram.getLicensingend()));
            if (cmsProgram.getOrgairdate() != null && !cmsProgram.getOrgairdate().equals(""))
            {
                // 首播日期䶿位时长
                cmsProgram.setOrgairdate(DateUtil2.get14Time(cmsProgram.getOrgairdate()).substring(0, 8));
            }

            cmsProgramDS.insertCmsProgram(cmsProgram);

            // 写操作日彿
            CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid(), CommonLogConstant.MGTTYPE_PROGRAM,
                    CommonLogConstant.OPERTYPE_PROGRAM_ADD, CommonLogConstant.OPERTYPE_PROGRAM_ADD_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("insertCmsProgram end");
        return result;
    }

    /**
     * 修改CmsProgram对象
     */
    public String cntModApply(CmsProgram cmsProgram) throws Exception
    {
        log.debug("updateCmsProgram starting...");

        CmsProgram oldCnt = new CmsProgram();
        String result = "";
        try
        {
            oldCnt.setProgramindex(cmsProgram.getProgramindex());
            oldCnt = cmsProgramDS.getCmsProgram(oldCnt);

            if (null == oldCnt)
            {
                return ResourceMgt.findDefaultText("cnt.cntnot.exist");
            }

            // 验证CP和牌照方是否已删闿
            String CPID = cmsProgram.getCpid();
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCpid(CPID);
            ucpBasic.setCptype(1);
            List<UcpBasic> cpList = ucpBasicDS.getUcpBasicByCond(ucpBasic);
            if (cpList != null && cpList.size() > 0)
            {
                ucpBasic = cpList.get(0);
                if (ucpBasic.getStatus() != 1)
                {
                    return ResourceMgt.findDefaultText("cp.status.wrong");
                }
            }
            else
            {
                return ResourceMgt.findDefaultText("cp.not.exist");
            }

            String LPID = cmsProgram.getLicenseid();
            UcpBasic lpBasic = new UcpBasic();
            lpBasic.setCpid(LPID);
            lpBasic.setCptype(2);
            List<UcpBasic> lpList = ucpBasicDS.getUcpBasicByCond(lpBasic);
            if (lpList != null && lpList.size() > 0)
            {
                lpBasic = lpList.get(0);
                if (lpBasic.getStatus() != 1)
                {
                    return ResourceMgt.findDefaultText("lp.status.wrong");
                }
            }
            else
            {
                return ResourceMgt.findDefaultText("lp.not.exist");
            }

            // 格式化内容带有时间的字段
            cmsProgram.setEffectivedate(DateUtil2.get14Time(cmsProgram.getEffectivedate()));
            cmsProgram.setExpirydate(DateUtil2.get14Time(cmsProgram.getExpirydate()));
            cmsProgram.setLlicensofflinetime(DateUtil2.get14Time(cmsProgram.getLlicensofflinetime()));
            cmsProgram.setDeletetime(DateUtil2.get14Time(cmsProgram.getDeletetime()));
            cmsProgram.setLicensingstart(DateUtil2.get14Time(cmsProgram.getLicensingstart()));
            cmsProgram.setLicensingend(DateUtil2.get14Time(cmsProgram.getLicensingend()));
            if (cmsProgram.getOrgairdate() != null && !cmsProgram.getOrgairdate().equals(""))
            {
                cmsProgram.setOrgairdate(DateUtil2.get14Time(cmsProgram.getOrgairdate()).substring(0, 8));
            }

            boolean modSyncFlag = false;
            //点播内容在下游平台是否发布，默认为未发布
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(3);
            target.setObjectid(cmsProgram.getProgramid());
            List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(targetList!=null&&targetList.size()>0){
                for(CntTargetSync cntTarget:targetList){
                    int cntSyncStatus = cntTarget.getStatus();
                    //点播内容发布状态为发布中  200：发布中
                    if(cntSyncStatus==200){
                        return "1:点播内容正在发布中，不允许修改";
                    }else if(cntSyncStatus==300||cntSyncStatus==500){
                        //点播内容发布状态为已发布和修改发布失败时，修改发布标识为已发布 300：已发布，500：修改发布失败
                        modSyncFlag = true;
                    }
                        
                }
            }
            
            int status = oldCnt.getStatus();
            switch (status)
            {
                case 110: // 待审核
                case 130: // 一审失败
                case 150: // 二审失败
                case 180: // 三审失败
                    cmsProgram.setLastupdatetime(getCurrentTime());
                    cmsProgramDS.updateCmsProgram(cmsProgram);
                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid(), CommonLogConstant.MGTTYPE_PROGRAM,
                            CommonLogConstant.OPERTYPE_PROGRAM_MODIFY, CommonLogConstant.OPERTYPE_PROGRAM_MODIFY_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    log.debug("updateCmsProgram end");
                    return ResourceMgt.findDefaultText("cnt.do.success");
                case 0:  //审核通过
                    CmsLpopNoaudit cmsLpopNoaudit = new CmsLpopNoaudit();
                    cmsLpopNoaudit.setCpid(cmsProgram.getCpid());
                    cmsLpopNoaudit.setLptype(4);
                    List<CmsLpopNoaudit> cmsLpopNoauditList = cmsLpopNoauditDS.getCmsLpopNoauditByCond(cmsLpopNoaudit);
                    //判断CP是否免审，若是不走审核流程状态置为审核通过
                    if(cmsLpopNoauditList != null && cmsLpopNoauditList.size() >0)
                    {
                        cmsProgram.setLastupdatetime(getCurrentTime());
                        cmsProgram.setStatus(0);
                        cmsProgramDS.updateCmsProgram(cmsProgram);
                        CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid(), CommonLogConstant.MGTTYPE_PROGRAM,
                                CommonLogConstant.OPERTYPE_PROGRAM_MODIFY, CommonLogConstant.OPERTYPE_PROGRAM_MODIFY_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        //若点播内容已发布，且CP免审，则program修改需触发修改发布流程
                        if (modSyncFlag)
                        {
                            programPlatformSynLS.modifyProgramPlatformSyn(cmsProgram.getProgramindex().toString());
                        }
                        log.debug("updateCmsProgram end");
                        return ResourceMgt.findDefaultText("cnt.do.success");
                    }else 
                    {
                        result = cntModApply.runModApply(cmsProgram);

                        log.debug("updateCmsProgram end");
                        if (!CntConstants.CNT_SUCCESS.equals(result))
                        {
                            return "1:" + CntUtils.getResourceText(result);
                        }
                        else
                        {
                            return "0:" + CntUtils.getResourceText(result);
                        }
                    }
                    
                default:
                    return ResourceMgt.findDefaultText("cnt.status.wrong");
            }
            
        }
        catch (Exception e)
        {
            log.error("Error occurred in cntModApply method" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }
    }

    /**
     * 获取CmsProgram对象
     */
    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws Exception
    {
        log.debug("getCmsProgram starting...");
        CmsProgram program = null;
        try
        {
            program = cmsProgramDS.getCmsProgram(cmsProgram);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("getCmsProgram starting...");
        return program;
    }

    /**
     * 获取CmsProgram对象
     */
    public CmsProgram getCmsProgramByCond(CmsProgram cmsProgram) throws Exception
    {
        log.debug("getCmsProgramByCond starting...");
        CmsProgram program = null;
        try
        {
            List<CmsProgram> list = cmsProgramDS.getCmsProgramByCond(cmsProgram);
            if (list != null && list.size() > 0)
            {
                program = list.get(0);
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("getCmsProgramByCond starting...");
        return program;
    }

    /**
     * 删除CmsProgram对象
     */
    public String deleteCmsProgram(String programIndex) throws Exception
    {
        log.debug("deleteCmsProgram starting...");
        String result = ResourceMgt.findDefaultText("cnt.do.success");;
        CmsProgram program = null;
        List<CmsMovie> movieList = null;
        
        try
        {
            program = new CmsProgram();
            program.setProgramindex(Long.valueOf(programIndex));
            program = cmsProgramDS.getCmsProgram(program);
            if (program == null)
            {
                return ResourceMgt.findDefaultText("cnt.cntnot.exist");
            }

            CmsMovie movie = new CmsMovie();
            movie.setProgramid(program.getProgramid());
            movieList = cmsMovieDS.getCmsMovieByCond(movie);

            ServiceProgramMap serviceProgramMap = new ServiceProgramMap();
            serviceProgramMap.setProgramid(program.getProgramid());
            List<ServiceProgramMap> serviceProgramMapList = serviceProgramMapDS
                    .getServiceProgramMapByCond(serviceProgramMap);
            if (serviceProgramMapList != null && serviceProgramMapList.size() > 0)
            {
                return ResourceMgt.findDefaultText("cnt.related.service.please.delete");
            }

            // 判断内容的状态是否可以删闿
            int status = program.getStatus();
            if (status == 0||status == 110||status == 130||status == 150||status == 160||status == 180)
            {
            	//判断内容下不能有处于提交加密和加密中状态的子内容
            	if(movieList!=null&&movieList.size()>0){
            		for(CmsMovie cmsMovie:movieList){
            			//2：提交加密，3：加密中
            			if(cmsMovie.getEncryptionstatus()==2 || cmsMovie.getEncryptionstatus()==3){
            				return ResourceMgt.findDefaultText("subcnt.status,not.encrypting");
            			}
            			
            		}
            	}
                
            	CntPlatformSync platform = new CntPlatformSync();
            	platform.setObjecttype(3);
                platform.setObjectid(program.getProgramid());
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(3);
                target.setObjectid(program.getProgramid());
                List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            	List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
            	if(targetList!=null&&targetList.size()>0){
            	    for(CntTargetSync cnttarget:targetList){
            	        int cntSyncStatus = cnttarget.getStatus();
            	        if(cntSyncStatus==200||cntSyncStatus==300||cntSyncStatus==500||cntSyncStatus==600){
            	            return "1:" + ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete");
            	        }
            	    }
            	    cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
            	    cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
            	}
            	
            	if(movieList!=null&&movieList.size()>0){
                    for(CmsMovie cmsMovie:movieList){
                        // 判断cms_ftptask表中是否存在相同的文件删除任务，若不存在插入文件删除任务，否则需要插僿
                        List<CmsFtptask> cmsFtptaskList = null;
                        CmsFtptask cmsFtptaskCond = new CmsFtptask();

                        cmsFtptaskCond.setContentindex(cmsMovie.getProgramindex());
                        cmsFtptaskCond.setFileindex(cmsMovie.getMovieindex());
                        cmsFtptaskCond.setUploadtype(5); // 5本地文件删除
                        cmsFtptaskCond.setSrcaddress(cmsMovie.getFileurl());
                        cmsFtptaskCond.setStatus(0); // 0：待上传

                        try
                        {
                            cmsFtptaskList = cmsFtptaskDS.getCmsFtptaskByCond(cmsFtptaskCond);
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e);
                        }

                        if (null == cmsFtptaskList || cmsFtptaskList.size() == 0)
                        {   // 不存在相同的文件删除任务
                            CmsFtptask cmsFtptask = new CmsFtptask();    
                            cmsFtptask.setFiletype(1);
                            cmsFtptask.setTasktype(Long.parseLong(1+""));
                            cmsFtptask.setContentindex(cmsMovie.getProgramindex());
                            cmsFtptask.setFileindex(cmsMovie.getMovieindex());
                            cmsFtptask.setUploadtype(5); // 5本地文件删除
                            cmsFtptask.setSrcaddress(cmsMovie.getFileurl());
                            cmsFtptask.setStatus(0); // 0：待上传
                            cmsFtptask.setSrcfilehandle(0); // 0：不处理
                            try
                            {
                                cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
                            }
                            catch (Exception e)
                            {
                                throw new Exception((e));
                            }
                            
                            //当子内容加密状态为已加密时，删除加密后文件
                            int encryptionstatus = cmsMovie.getEncryptionstatus();
                            if(encryptionstatus==1 && cmsMovie.getEncryptiontype() == 1){
                            	CmsFtptask task = new CmsFtptask();
                            	task.setFiletype(1);
                            	task.setTasktype(Long.parseLong(1+""));
                            	task.setFileindex(cmsMovie.getMovieindex());
                            	task.setUploadtype(5); // 5本地文件删除
                            	task.setSrcaddress(cmsMovie.getEncryptionfilepath());
                            	task.setStatus(0); // 0：待上传
                            	task.setSrcfilehandle(0); // 0：不处理

                                try
                                {
                                    cmsFtptaskDS.insertCmsFtptask(task);
                                }
                                catch (Exception e)
                                {
                                	throw new Exception((e));
                                }
                            }else if(encryptionstatus==1 && cmsMovie.getEncryptiontype() == 2){
                            	CmsFtptask task = new CmsFtptask();
                            	task.setFiletype(1);
                            	task.setTasktype(Long.parseLong(1+""));
                            	task.setFileindex(cmsMovie.getMovieindex());
                            	task.setUploadtype(6); // 6删除文件夹
                            	task.setSrcaddress(cmsMovie.getEncryptionfilepath());
                            	task.setStatus(0); // 0：待上传
                            	task.setSrcfilehandle(0); // 0：不处理

                                try
                                {
                                    cmsFtptaskDS.insertCmsFtptask(task);
                                }
                                catch (Exception e)
                                {
                                	throw new Exception((e));
                                }
                            }
                        }                        
                    }
                }
            	
            	ProgramPictureMap programPictureMap = new ProgramPictureMap();
                programPictureMap.setProgramid(program.getProgramid());
                List<ProgramPictureMap> pictureMapList = programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
                if (pictureMapList != null && pictureMapList.size() > 0)
                {
                    List plist = new ArrayList();
                    for(int i=0;i<pictureMapList.size();i++){
                        Picture p = new Picture();
                        ProgramPictureMap sMap = pictureMapList.get(i);
                        p.setPictureindex(sMap.getPictureindex());
                        p = pictureDS.getPicture(p);
                        if(p==null){
                            continue;
                        }
                        plist.add(p);
                        //插入删除文件的文件迁移任务
                        CmsFtptask ftptask = new CmsFtptask();
                        ftptask.setTasktype(1L); // 1-普通类型
                        ftptask.setFiletype(2); // 2-picture
                        ftptask.setFileindex(p.getPictureindex());
                        ftptask.setUploadtype(5); // 5-本地文件删除
                        ftptask.setSrcaddress(p.getPictureurl());
                        ftptask.setStatus(0); // 0-待上传
                        ftptask.setSrcfilehandle(1); // 1-删除
                        cmsFtptaskDS.insertCmsFtptask(ftptask);
                        CommonLogUtil.insertOperatorLog(p.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                                PictureConstants.OPERTYPE_PICTURE_DEL, PictureConstants.OPERTYPE_PICTURE_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        //删除海报以及其关联关系关联平台记录
                        platform.setObjecttype(35);
                        platform.setObjectid(pictureMapList.get(i).getMappingid());
                        target.setObjecttype(35);
                        target.setObjectid(pictureMapList.get(i).getMappingid());
                        List<CntTargetSync> picprogmapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> picprogmapPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        if(picprogmapTargetList!=null&&picprogmapTargetList.size()>0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(picprogmapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(picprogmapTargetList);
                        }
                    }
                    pictureDS.removePictureList(plist);//删除关联到的海报
                    programPictureMapDS.removeProgramPictureMapList(pictureMapList);//删除同海报对应关系
                }
                
                //删除关联角色
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                programCrmMap.setProgramid(program.getProgramid());
                List<ProgramCrmMap> programCrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                if (programCrmMapList != null && programCrmMapList.size() > 0)
                {
                    for(ProgramCrmMap pcrmMap:programCrmMapList)
                    {
                        //删除关联角色与平台关联数据
                        platform.setObjecttype(34);
                        platform.setObjectid(pcrmMap.getMappingid());
                        target.setObjecttype(34);
                        target.setObjectid(pcrmMap.getMappingid());
                        List<CntTargetSync> pcrmMapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> pcrmMapPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        if(pcrmMapTargetList!=null&&pcrmMapTargetList.size()>0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(pcrmMapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(pcrmMapTargetList);
                        }
                    }
                    
                    programCrmMapDS.removeProgramCrmMapList(programCrmMapList);
                }
                
                
                //删除关联栏目
                CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
                categoryProgramMap.setProgramid(program.getProgramid());
                List<CategoryProgramMap> categoryProgramMapList = ctgpgmapDS
                        .getCategoryProgramMapByCond(categoryProgramMap);
                if (categoryProgramMapList != null && categoryProgramMapList.size() > 0)
                {
                    for(CategoryProgramMap pctgMap:categoryProgramMapList)
                    {
                        //删除关联栏目与平台关联数据
                        platform.setObjecttype(26);
                        platform.setObjectid(pctgMap.getMappingid());
                        target.setObjecttype(26);
                        target.setObjectid(pctgMap.getMappingid());
                        List<CntTargetSync> pctgMapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> pctgMapPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        if(pctgMapTargetList!=null&&pctgMapTargetList.size()>0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(pctgMapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(pctgMapTargetList);
                        }
                    }
                    
                    ctgpgmapDS.removeCategoryProgramMapList(categoryProgramMapList);
                }
                
                //删除内容工作流历史表记录
                CmsProgramWkfwhis cmsProgramWkfwhis = new CmsProgramWkfwhis();
                cmsProgramWkfwhis.setProgramid(program.getProgramid());
                List<CmsProgramWkfwhis> cmsProgramWkfwhisList = cmsProgramWkfwhisDS.getCmsProgramWkfwhisByCond(cmsProgramWkfwhis);
                if (cmsProgramWkfwhisList != null && cmsProgramWkfwhisList.size() > 0)
                {
                    cmsProgramWkfwhisDS.removeCmsProgramWkfwhisList(cmsProgramWkfwhisList);
                }
            	
                cmsMovieDS.removeCmsMovieList(movieList);
                cmsProgramDS.removeCmsProgram(program);   

                // 写操作日彿
                CommonLogUtil.insertOperatorLog(program.getProgramid(), CommonLogConstant.MGTTYPE_PROGRAM,
                        CommonLogConstant.OPERTYPE_PROGRAM_DELETE, CommonLogConstant.OPERTYPE_PROGRAM_DELETE_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            }
            else
            {
                return ResourceMgt.findDefaultText("cnt.status.wrong");
            }

        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("deleteCmsProgram starting...");
        return result;
    }

    /**
     * 批量删除内容
     */
    public String batchDeleteCmsProgram(List<CmsProgram> list) throws Exception
    {
        log.debug("batchDeleteCmsProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            int success = 0;
            int fail = 0;
            int index = 1;

            String message = "";
            String result = "";

            for (CmsProgram program : list)
            {
                message = deleteCmsProgram(program.getProgramindex().toString());
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    fail++;
                    result = message.substring(2);
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                }

                operateInfo = new OperateInfo(String.valueOf(index++), program.getProgramid(), message, result);
                returnInfo.appendOperateInfo(operateInfo);

            }

            if (success == list.size())
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count")+ fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count")+ success + ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

        log.debug("batchDeleteCmsProgram end");
        return returnInfo.toString();
    }

    /**
     * 根据条件分页查询CmsProgram对象
     */
    public TableDataInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try
        {
            // 格式化内容带有时间的字段
            cmsProgram.setCreateStarttime(DateUtil2.get14Time(cmsProgram.getCreateStarttime()));
            cmsProgram.setCreateEndtime(DateUtil2.get14Time(cmsProgram.getCreateEndtime()));
            cmsProgram.setOnlinetimeStartDate(DateUtil2.get14Time(cmsProgram.getOnlinetimeStartDate()));
            cmsProgram.setOnlinetimeEndDate(DateUtil2.get14Time(cmsProgram.getOnlinetimeEndDate()));
            cmsProgram.setCntofflinestarttime(DateUtil2.get14Time(cmsProgram.getCntofflinestarttime()));
            cmsProgram.setCntofflineendtime(DateUtil2.get14Time(cmsProgram.getCntofflineendtime()));

            if (null != cmsProgram.getNamecn() && !cmsProgram.getNamecn().equals(""))
            {
                String namecn = EspecialCharMgt.conversion(cmsProgram.getNamecn());
                cmsProgram.setNamecn(namecn);
            }

            if (null != cmsProgram.getProgramid() && !cmsProgram.getProgramid().equals(""))
            {
                String programid = EspecialCharMgt.conversion(cmsProgram.getProgramid());
                cmsProgram.setProgramid(programid);
            }

            if (null != cmsProgram.getKeywords() && !cmsProgram.getKeywords().equals(""))
            {
                String keywords = EspecialCharMgt.conversion(cmsProgram.getKeywords());
                cmsProgram.setKeywords(keywords);
            }

            if (null != cmsProgram.getActors() && !cmsProgram.getActors().equals(""))
            {
                String actors = EspecialCharMgt.conversion(cmsProgram.getActors());
                cmsProgram.setActors(actors);
            }

            if (null != cmsProgram.getDirectors() && !cmsProgram.getDirectors().equals(""))
            {
                String directors = EspecialCharMgt.conversion(cmsProgram.getDirectors());
                cmsProgram.setDirectors(directors);
            }

            if (null != cmsProgram.getContenttags() && !cmsProgram.getContenttags().equals(""))
            {
                String contenttags = EspecialCharMgt.conversion(cmsProgram.getContenttags());
                cmsProgram.setContenttags(contenttags);
            }
            if (null != cmsProgram.getCpid() && !cmsProgram.getCpid().equals(""))
            {
                String cpid = EspecialCharMgt.conversion(cmsProgram.getCpid());
                cmsProgram.setCpid(cpid);
            }
            if (null != cmsProgram.getCpcnshortname() && !cmsProgram.getCpcnshortname().equals(""))
            {
                String cpcnshortname = EspecialCharMgt.conversion(cmsProgram.getCpcnshortname());
                cmsProgram.setCpcnshortname(cpcnshortname);
            }

            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsProgram.setOperid(operinfo.getOperid());
            dataInfo = cmsProgramDS.pageInfoQueryForPage(cmsProgram, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return dataInfo;
    }

    /*******************************************************************************************************************
     * 批量提审方法
     * 
     * @return
     */
    public String batchApply(List<String> programidList) throws Exception
    {
        log.debug("CmsProgramLS: batchApply starting....");
        int ifail = 0;
        int isucess = 0;
        String rowindex = "";
        String programid = "";
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;

        for (int i = 0; i < programidList.size(); i++)
        {
            rowindex = (i + 1) + "";
            programid = programidList.get(i);

            String result = "";
            try
            {
                result = cntApply.runApply(programid); // 调用底层提审方法
                if (CntConstants.CNT_SUCCESS.equals(result))
                {
                    operateInfo = new OperateInfo(rowindex, programid, CntUtils
                            .getResourceText(GlobalConstants.SUCCESS), CntUtils.getResourceText(result));
                    returnInfo.appendOperateInfo(operateInfo);
                    isucess++;
                }
                else
                {// 内容已不存在
                    operateInfo = new OperateInfo(rowindex, programid, CntUtils.getResourceText(GlobalConstants.FAIL),
                            CntUtils.getResourceText(result));
                    returnInfo.appendOperateInfo(operateInfo);
                    ifail++;
                }
            }
            catch (Exception e)
            {
                log.debug("Error occurred in apply method");
                log.debug(e);

                String message = e.getMessage();
                ifail++;
                if (!"".equals(message) && ("25").equals(message.substring(0, 2)) && message.length() == 6)
                {// CntConstants.CNT_FAIL="250099"
                    operateInfo = new OperateInfo(rowindex, programid, CntUtils.getResourceText(GlobalConstants.FAIL),
                            ResourceManager.getResourceText(e));
                }
                else
                {
                    operateInfo = new OperateInfo(rowindex, programid, CntUtils.getResourceText(GlobalConstants.FAIL),
                            CntUtils.getResourceText(CntConstants.CNT_FAIL));
                }
                returnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }

        if (programidList.size() == 0 || ifail == programidList.size())
        {// 全部操作失败
            returnInfo.setReturnMessage(CntConstants.FAIL_COUNT + ifail);
            returnInfo.setFlag(GlobalConstants.FAIL);
        }
        else if (ifail > 0)
        {// 部分操作失败
            returnInfo.setReturnMessage(CntConstants.SUCESS_COUNT + isucess + "  " + CntConstants.FAIL_COUNT + ifail);
            returnInfo.setFlag(GlobalConstants.SUCCESS);
        }
        else
        {// 全部操作成功
            returnInfo.setReturnMessage(CntConstants.SUCESS_COUNT + isucess);
            returnInfo.setFlag(GlobalConstants.SUCCESS);
        }

        log.debug("CmsProgramLS: batchApply end");
        return returnInfo.toString();
    }

    public String cntAudit(String[] wkfwParam) throws Exception
    {
        log.debug("CmsProgramLS: audit starting...");
        ReturnInfo returnInfo = new ReturnInfo();

        try
        {
            String result = cntApply.runAudit(wkfwParam);
            if (CntConstants.CNT_SUCCESS.equals(result))
            {
                returnInfo.setFlag(GlobalConstants.SUCCESS);
            }
            else
            {
                returnInfo.setFlag(GlobalConstants.FAIL);
            }
            returnInfo.setReturnMessage(CntUtils.getResourceText(result));
        }
        catch (Exception e)
        {
            log.debug("Error occurred in audit method");
            log.debug(e);

            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("CmsProgramLS: audit end");
        return returnInfo.toString();
    }

    public String cntModAudit(String[] wkfwParam) throws Exception
    {
        log.debug("CmsProgramLS: audit starting...");
        ReturnInfo returnInfo = new ReturnInfo();

        try
        {
            String result = cntModApply.runModAudit(wkfwParam);
            if (CntConstants.CNT_SUCCESS.equals(result))
            {
                returnInfo.setFlag(GlobalConstants.SUCCESS);
            }
            else
            {
                returnInfo.setFlag(GlobalConstants.FAIL);
            }
            returnInfo.setReturnMessage(CntUtils.getResourceText(result));
        }
        catch (Exception e)
        {
            log.debug("Error occurred in audit method");
            log.debug(e);

            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("CmsProgramLS: audit end");
        return returnInfo.toString();
    }

    public String batchUpdateProgram(String programindexs, CmsProgram program)
    {
        log.debug("batchUpdateProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

        try
        {
            int success = 0;
            int fail = 0;
            int index = 1;

            String message = "";
            String result = "";

            String[] programindexArray = programindexs.split(",");
            Class programClass = CmsProgram.class;
            for(String programindex : programindexArray)
            {
                CmsProgram cmsProgram =new CmsProgram();
                cmsProgram.setProgramindex(Long.valueOf(programindex));
                cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
                try
                {
                    Field[] fieldArray = programClass.getDeclaredFields();
                    for(Field field : fieldArray)
                    {
                        String getFiledname = "get"+field.getName().substring(0,1).toUpperCase()+field.getName().substring(1);
                        String setFiledname = "set"+field.getName().substring(0,1).toUpperCase()+field.getName().substring(1);
                        Method getMethod = programClass.getDeclaredMethod(getFiledname);
                        Object value =getMethod.invoke(program);
                        if(value !=null && !value.toString().trim().equals(""))
                        {
                            String filetype = field.getType().getName();
                            Class setClass = Class.forName(filetype);
                            Method setMethod = programClass.getDeclaredMethod(setFiledname,setClass);
                            setMethod.invoke(cmsProgram, value);
                        }
    
                    }
                    message = cntModApply(cmsProgram);
                }
                catch (Exception e) 
                {
                    //message ="1:操作失败";
                	message = ResourceMgt.findDefaultText("cnt.do.fail");
                }

                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    fail++;
                    result = message.substring(2);
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                }

                operateInfo = new OperateInfo(String.valueOf(index++), cmsProgram.getProgramid(), message, result);
                returnInfo.appendOperateInfo(operateInfo);

            }

            if (success == programindexArray.length)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count")+ success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == programindexArray.length)
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("batchUpdateProgram end");
        return returnInfo.toString();
    }
    
    /**
     * 内容批量关联运营平台，多个内容关联多个运营平台
     * 
     * @param targetsystemList Targetsystem对象列表
     * @param programindexs 内容主键
     * @return String
     * @throws DomainServiceException ds异常
     */
    public String bindBatchProgramTargetsync(List<Targetsystem> platformList,List<CmsProgram> cmsProgramList)
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalProgramindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < cmsProgramList.size(); i++)  //遍历要操作的内容主键，依次操作要关联的内容
            {
                totalProgramindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
                
                CmsProgram cmsProgramtemp = new CmsProgram();
                cmsProgramtemp.setProgramindex(cmsProgramList.get(i).getProgramindex());
                CmsProgram cmsProgramObj = cmsProgramDS.getCmsProgram(cmsProgramtemp);  
                if (cmsProgramObj == null ){
                   proStatusStr = "操作失败,内容不存在";
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), cmsProgramList.get(i).getProgramid(), result
                           .substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }
                
                //内容状态不是审核通过，则操作失败
                if (cmsProgramObj != null && cmsProgramObj.getStatus() != 0) 
                {
                    proStatusStr = "操作失败,内容状态不正确";
                    failCount++;
                    result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                    operateInfo = new OperateInfo(String.valueOf(num), cmsProgramObj.getProgramid(), result
                                  .substring(2, result.length()), proStatusStr);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                
                if (cmsProgramObj != null && cmsProgramObj.getStatus() == 0) //内容状态审核通过
                {
                    operdesc = bindProgramTargetsync(platformList,cmsProgramObj);  //单个内容关联运营平台
                    if (operdesc.substring(0, 1).equals("0"))  //内容关联运营平台成功
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), cmsProgramObj.getProgramid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), cmsProgramObj.getProgramid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        if ((successCount) == totalProgramindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalProgramindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
    }
    
    public String bindProgramTargetsync(List<Targetsystem> platformList,CmsProgram cmsProgram)
    {
        log.debug("bindProgramTargetsys starting...");
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        try
        {
            for (int i = 0; i < platformList.size(); i++)  //遍历要关联的平台
            {
                Targetsystem targtemp = new Targetsystem();
                targtemp.setTargetindex(platformList.get(i).getTargetindex());
                targtemp = targetSystemDS.getTargetsystem(targtemp);                 
                if (targtemp != null && targtemp.getStatus() == 0)  //0:正常
                {
                    CntTargetSync target = new CntTargetSync();
                    target.setObjecttype(3);
                    target.setObjectid(cmsProgram.getProgramid());
                    target.setTargetindex(targtemp.getTargetindex());
                    List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    if(targetSyncList !=null && targetSyncList.size()>0)
                    {
                        resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                             + platformList.get(i).getTargetid()+ ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)+"  ");
                                        
                        //continue;
                    }else
                    {
                        CntPlatformSync platform = new CntPlatformSync();
                        platform.setObjecttype(3);
                        platform.setObjectid(cmsProgram.getProgramid());
                        platform.setPlatform(platformList.get(i).getPlatform());
                        List<CntPlatformSync> platformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        //若目标系统为3.0网元，且平台表里面已存在数据，只需更新平台表里面发布总状态为0：待发布
                        if(platformSyncList !=null && platformSyncList.size()>0)  
                        {
                            syncindex = platformSyncList.get(0).getSyncindex();
                            platform = platformSyncList.get(0);
                            platform.setStatus(0);
                            cntPlatformSyncDS.updateCntPlatformSync(platform);   
                        }else{
                            insertPlatformSyncTask(3,cmsProgram.getProgramindex(),cmsProgram.getProgramid(),"","",
                                    platformList.get(i).getPlatform(),0);
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid().toString() + ", " + platformList.get(i).getTargetid(), 
                                    CommonLogConstant.MGTTYPE_PROGRAM,
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_ADD), 
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_ADD_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        
                        insertTargetSyncTask(syncindex,3,cmsProgram.getProgramindex(),cmsProgram.getProgramid(),"","",
                                targtemp.getTargetindex(),0,0);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid().toString() + ", " + platformList.get(i).getTargetid(), 
                                CommonLogConstant.MGTTYPE_PROGRAM,
                                ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PROGRAM_BIND_ADD), 
                                ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_ADD_INFO),
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        ProgramPictureMap programPictureMap = new ProgramPictureMap();
                        programPictureMap.setProgramid(cmsProgram.getProgramid());
                        List<ProgramPictureMap> pictureMapList = programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
                        if (pictureMapList != null && pictureMapList.size() > 0)
                        {
                            //0-2.0文广.2:EPG,10:2.0央视
                            if(targtemp.getTargettype() == 0 ||targtemp.getTargettype() == 2 || targtemp.getTargettype() == 10) 
                            {
                                for(ProgramPictureMap picprogmap:pictureMapList)
                                {
                                    CntPlatformSync picprogmapPlatform = new CntPlatformSync();
                                    picprogmapPlatform.setObjecttype(35);
                                    picprogmapPlatform.setObjectid(picprogmap.getMappingid());
                                    picprogmapPlatform.setPlatform(platformList.get(i).getPlatform());
                                    List<CntPlatformSync> picprogmapPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(picprogmapPlatform);
                                    if(picprogmapPlatSyncList !=null && picprogmapPlatSyncList.size()>0)  
                                    {
                                        syncindex = picprogmapPlatSyncList.get(0).getSyncindex();
                                        picprogmapPlatform = picprogmapPlatSyncList.get(0);
                                        picprogmapPlatform.setStatus(0);
                                        cntPlatformSyncDS.updateCntPlatformSync(picprogmapPlatform);   
                                    }else{
                                        insertPlatformSyncTask(35,picprogmap.getMapindex(),picprogmap.getMappingid(),picprogmap.getProgramid()
                                                ,picprogmap.getPictureid(),platformList.get(i).getPlatform(),0);
                                        // 写操作日志
                                        CommonLogUtil.insertOperatorLog(picprogmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), 
                                                CommonLogConstant.MGTTYPE_PROGRAM,
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PICPROGMAP_BIND_ADD), 
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PICPROGMAP_BIND_ADD_INFO),
                                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    }
                                    
                                    insertTargetSyncTask(syncindex,35,picprogmap.getMapindex(),picprogmap.getMappingid(),picprogmap.getProgramid()
                                            ,picprogmap.getPictureid(),targtemp.getTargetindex(),0,0);
                                    // 写操作日志
                                    CommonLogUtil.insertOperatorLog(picprogmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), 
                                            CommonLogConstant.MGTTYPE_PROGRAM,
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PICPROGMAP_BIND_ADD), 
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PICPROGMAP_BIND_ADD_INFO),
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                }
                            }
                        }
                        
                        sucess = true;
                        resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                                + ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)+"  ");
                    }
                 
                    ProgramCrmMap programCrmMap = new ProgramCrmMap();
                    programCrmMap.setProgramid(cmsProgram.getProgramid());
                    List<ProgramCrmMap> programCrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                    if (programCrmMapList != null && programCrmMapList.size() > 0)
                    {
                        for(ProgramCrmMap pcrm:programCrmMapList)
                        {
                            target.setObjecttype(12);
                            target.setObjectid(pcrm.getCastrolemapid());
                            target.setTargetindex(targtemp.getTargetindex());
                            List<CntTargetSync> crmtargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                            //点播内容关联角色关联平台，前提是角色已关联该平台
                            if (crmtargetSyncList != null && crmtargetSyncList.size() > 0)
                            {
                                target.setObjecttype(34);
                                target.setObjectid(pcrm.getMappingid());
                                target.setTargetindex(targtemp.getTargetindex());
                                List<CntTargetSync> pcrmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                                if(pcrmTargetSyncList.size() == 0){
                                    CntPlatformSync pcrmmapPlatform = new CntPlatformSync();
                                    pcrmmapPlatform.setObjecttype(34);
                                    pcrmmapPlatform.setObjectid(pcrm.getMappingid());
                                    pcrmmapPlatform.setPlatform(platformList.get(i).getPlatform());
                                    List<CntPlatformSync> pcrmPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(pcrmmapPlatform);
                                    if(pcrmPlatSyncList !=null && pcrmPlatSyncList.size()>0)  
                                    {
                                        syncindex = pcrmPlatSyncList.get(0).getSyncindex();
                                        pcrmmapPlatform = pcrmPlatSyncList.get(0);
                                        pcrmmapPlatform.setStatus(0);
                                        cntPlatformSyncDS.updateCntPlatformSync(pcrmmapPlatform);   
                                    }else{
                                        insertPlatformSyncTask(34,pcrm.getMapindex(),pcrm.getMappingid(),pcrm.getCastrolemapid(),pcrm.getProgramid(),
                                                platformList.get(i).getPlatform(),0);
                                        // 写操作日志
                                        CommonLogUtil.insertOperatorLog(pcrm.getMappingid().toString() + ", " + platformList.get(i).getTargetid(),
                                                CommonLogConstant.MGTTYPE_PROGRAM,
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD), 
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD_INFO),
                                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    }
                                    
                                    insertTargetSyncTask(syncindex,34,pcrm.getMapindex(),pcrm.getMappingid(),pcrm.getCastrolemapid(),
                                            pcrm.getProgramid(),targtemp.getTargetindex(),0,0);
                                    // 写操作日志
                                    CommonLogUtil.insertOperatorLog(pcrm.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), 
                                            CommonLogConstant.MGTTYPE_PROGRAM,
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_ADD), 
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_ADD_INFO),
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                }
                            }
                        }
                    }
                    
                    CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
                    categoryProgramMap.setProgramid(cmsProgram.getProgramid());
                    List<CategoryProgramMap> categoryProgramMapList = ctgpgmapDS
                            .getCategoryProgramMapByCond(categoryProgramMap);
                    if (categoryProgramMapList != null && categoryProgramMapList.size() > 0)
                    {
                        for(CategoryProgramMap ctgprogmap:categoryProgramMapList)
                        {
                            target.setObjecttype(2);
                            target.setObjectid(ctgprogmap.getCategoryid());
                            target.setTargetindex(targtemp.getTargetindex());
                            List<CntTargetSync> ctgtargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                            //点播内容关联栏目关联平台，前提是栏目已关联该平台
                            if(ctgtargetSyncList !=null && ctgtargetSyncList.size()>0)
                            {
                                target.setObjecttype(26);
                                target.setObjectid(ctgprogmap.getMappingid());
                                target.setTargetindex(targtemp.getTargetindex());
                                List<CntTargetSync> pctgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                                if(pctgTargetSyncList.size() == 0){
                                    CntPlatformSync ctgprogmapPlatform = new CntPlatformSync();
                                    ctgprogmapPlatform.setObjecttype(26);
                                    ctgprogmapPlatform.setObjectid(ctgprogmap.getMappingid());
                                    ctgprogmapPlatform.setPlatform(platformList.get(i).getPlatform());
                                    List<CntPlatformSync> ctgprogmapPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(ctgprogmapPlatform);
                                    if(ctgprogmapPlatSyncList !=null && ctgprogmapPlatSyncList.size()>0)  
                                    {
                                        syncindex = ctgprogmapPlatSyncList.get(0).getSyncindex();
                                        ctgprogmapPlatform = ctgprogmapPlatSyncList.get(0);
                                        ctgprogmapPlatform.setStatus(0);
                                        cntPlatformSyncDS.updateCntPlatformSync(ctgprogmapPlatform);   
                                    }else{
                                        insertPlatformSyncTask(26,ctgprogmap.getMapindex(),ctgprogmap.getMappingid(),ctgprogmap.getProgramid(),
                                                ctgprogmap.getCategoryid(),platformList.get(i).getPlatform(),0);
                                        // 写操作日志
                                        CommonLogUtil.insertOperatorLog(ctgprogmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), 
                                                CommonLogConstant.MGTTYPE_PROGRAM,
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD), 
                                                ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_CTGPROGMAP_BIND_ADD_INFO),
                                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    }
                                    
                                    insertTargetSyncTask(syncindex,26,ctgprogmap.getMapindex(),ctgprogmap.getMappingid(),ctgprogmap.getProgramid(),
                                            ctgprogmap.getCategoryid(),targtemp.getTargetindex(),0,0);
                                    // 写操作日志
                                    CommonLogUtil.insertOperatorLog(ctgprogmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), 
                                            CommonLogConstant.MGTTYPE_PROGRAM,
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD), 
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_ADD_INFO),
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                }
                            }
                        }
                    }
                    
                }else{
                    resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                            + ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_HAS_DELETED)+"  ");
                }
            }
        }catch(Exception e){
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("bindProgramTargetsys end...");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }
    
    public String deleteProgramBindTarget(Long programindex,Long targetindex) throws Exception
    {
        CmsProgram cmsProgram = new CmsProgram();
        Long relateindex = null;
        int platformStatus = 0;
        
        try{
            cmsProgram.setProgramindex(programindex);
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if(cmsProgram == null){
                return "1:点播内容不存在";
            }
            
            Targetsystem tagertsystem = new Targetsystem();
            tagertsystem.setTargetindex(targetindex);
            tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
            
            CntTargetSync target = new CntTargetSync();
            CntTargetSync allTarget = new CntTargetSync();
            CntTargetSync cntTargetSync = new CntTargetSync();
            target.setObjecttype(3);
            target.setObjectid(cmsProgram.getProgramid());
            target.setTargetindex(targetindex);
            List<CntTargetSync> programTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(programTargetList != null && programTargetList.size() > 0 ){
                int programSyncStatus = programTargetList.get(0).getStatus();
                if(programSyncStatus == 200||programSyncStatus == 300|| programSyncStatus== 500){
                    return "1:点播内容在该网元已发布，关联关系不能删除";
                }
                
                relateindex = programTargetList.get(0).getRelateindex();
                CntPlatformSync platform = new CntPlatformSync();
                platform.setSyncindex(relateindex);
                platform = cntPlatformSyncDS.getCntPlatformSync(platform);
                allTarget.setRelateindex(relateindex);
                List<CntTargetSync> onePlatTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                if(onePlatTargetList.size() > 1)
                {
                    //删除网元数据
                    cntTargetSync.setSyncindex(programTargetList.get(0).getSyncindex());
                    cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                    
                    //更新平台状态
                    allTarget.setRelateindex(relateindex);
                    List<CntTargetSync> allCastTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                    int[] castTargetStatus = new int[allCastTargetList.size()];
                    if(allCastTargetList != null && allCastTargetList.size() > 0){
                        for(int i = 0; i< allCastTargetList.size(); i++){
                            castTargetStatus[i] = allCastTargetList.get(i).getStatus();
                        }
                    }
                    platformStatus = StatusTranslator.getPlatformSyncStatus(castTargetStatus);
                    platform.setStatus(platformStatus);
                    cntPlatformSyncDS.updateCntPlatformSync(platform);
                    
                }else
                {
                    //删除网元数据
                    cntTargetSync.setSyncindex(programTargetList.get(0).getSyncindex());
                    cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                    //删除平台数据
                    cntPlatformSyncDS.removeCntPlatformSync(platform);
                    CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid().toString() + ", " + tagertsystem.getTargetid(),
                            CommonLogConstant.MGTTYPE_PROGRAM,
                            ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_DELETE), 
                            ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_DELETE_INFO),
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid().toString() + ", " + tagertsystem.getTargetid(), 
                        CommonLogConstant.MGTTYPE_PROGRAM,
                        ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PROGRAM_BIND_DELETE), 
                        ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PROGRAM_BIND_DELETE_INFO),
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                
                ProgramCrmMap programCrmMap = new ProgramCrmMap();
                programCrmMap.setProgramid(cmsProgram.getProgramid());
                List<ProgramCrmMap> programCrmMapList = programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
                if (programCrmMapList != null && programCrmMapList.size() > 0)
                {
                    for(ProgramCrmMap pcrm:programCrmMapList)
                    {
                        target.setObjecttype(34);
                        target.setObjectid(pcrm.getMappingid());
                        target.setTargetindex(targetindex);
                        List<CntTargetSync> pcrmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        if(pcrmTargetList != null && pcrmTargetList.size() > 0)
                        {
                            relateindex = pcrmTargetList.get(0).getRelateindex();
                            CntTargetSync pcrmTargetSync = new CntTargetSync();
                            CntPlatformSync pcrmPlatform = new CntPlatformSync();
                            pcrmPlatform.setSyncindex(relateindex);
                            pcrmPlatform = cntPlatformSyncDS.getCntPlatformSync(pcrmPlatform);
                            allTarget.setRelateindex(relateindex);
                            List<CntTargetSync> onePlatCpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                            if(onePlatCpTargetList.size() > 1)
                            {
                                //删除网元数据
                                pcrmTargetSync.setSyncindex(pcrmTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(pcrmTargetSync);
                                
                                //更新平台状态
                                allTarget.setRelateindex(relateindex);
                                List<CntTargetSync> allPcrmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                                int[] pcrmTargetStatus = new int[allPcrmTargetList.size()];
                                if(allPcrmTargetList != null && allPcrmTargetList.size() > 0){
                                    for(int i = 0; i< allPcrmTargetList.size(); i++){
                                        pcrmTargetStatus[i] = allPcrmTargetList.get(i).getStatus();
                                    }
                                }
                                platformStatus = StatusTranslator.getPlatformSyncStatus(pcrmTargetStatus);
                                pcrmPlatform.setStatus(platformStatus);
                                cntPlatformSyncDS.updateCntPlatformSync(pcrmPlatform);
                            }else
                            {
                                //删除网元数据
                                pcrmTargetSync.setSyncindex(pcrmTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(pcrmTargetSync);
                                //删除平台数据
                                cntPlatformSyncDS.removeCntPlatformSync(pcrmPlatform);
                                CommonLogUtil.insertOperatorLog(pcrm.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                        CommonLogConstant.MGTTYPE_PROGRAM,
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_DELETE), 
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_DELETE_INFO),
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            CommonLogUtil.insertOperatorLog(pcrm.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                    CommonLogConstant.MGTTYPE_PROGRAM,
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_DELETE), 
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_DELETE_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        
                    }
                }
                
                CategoryProgramMap categoryProgramMap = new CategoryProgramMap();
                categoryProgramMap.setProgramid(cmsProgram.getProgramid());
                List<CategoryProgramMap> categoryProgramMapList = ctgpgmapDS
                        .getCategoryProgramMapByCond(categoryProgramMap);
                if (categoryProgramMapList != null && categoryProgramMapList.size() > 0)
                {
                    for(CategoryProgramMap ctgprogmap:categoryProgramMapList)
                    {
                        target.setObjecttype(26);
                        target.setObjectid(ctgprogmap.getMappingid());
                        target.setTargetindex(targetindex);
                        List<CntTargetSync> ctgpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        if(ctgpTargetList != null && ctgpTargetList.size() > 0)
                        {
                            relateindex = ctgpTargetList.get(0).getRelateindex();
                            CntTargetSync ctgpTargetSync = new CntTargetSync();
                            CntPlatformSync ctgpPlatform = new CntPlatformSync();
                            ctgpPlatform.setSyncindex(relateindex);
                            ctgpPlatform = cntPlatformSyncDS.getCntPlatformSync(ctgpPlatform);
                            allTarget.setRelateindex(relateindex);
                            List<CntTargetSync> onePlatCpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                            if(onePlatCpTargetList.size() > 1)
                            {
                                //删除网元数据
                                ctgpTargetSync.setSyncindex(ctgpTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(ctgpTargetSync);
                                
                                //更新平台状态
                                allTarget.setRelateindex(relateindex);
                                List<CntTargetSync> allCtgpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                                int[] ctgpTargetStatus = new int[allCtgpTargetList.size()];
                                if(allCtgpTargetList != null && allCtgpTargetList.size() > 0){
                                    for(int i = 0; i< allCtgpTargetList.size(); i++){
                                        ctgpTargetStatus[i] = allCtgpTargetList.get(i).getStatus();
                                    }
                                }
                                platformStatus = StatusTranslator.getPlatformSyncStatus(ctgpTargetStatus);
                                ctgpPlatform.setStatus(platformStatus);
                                cntPlatformSyncDS.updateCntPlatformSync(ctgpPlatform);
                            }else
                            {
                                //删除网元数据
                                ctgpTargetSync.setSyncindex(ctgpTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(ctgpTargetSync);
                                //删除平台数据
                                cntPlatformSyncDS.removeCntPlatformSync(ctgpPlatform);
                                CommonLogUtil.insertOperatorLog(ctgprogmap.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                        CommonLogConstant.MGTTYPE_PROGRAM,
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_DELETE), 
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_CTGPROGMAP_BIND_DELETE_INFO),
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            CommonLogUtil.insertOperatorLog(ctgprogmap.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                    CommonLogConstant.MGTTYPE_PROGRAM,
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_DELETE), 
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_CTGPROGMAP_BIND_DELETE_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        
                    }
                }
                
                ProgramPictureMap programPictureMap = new ProgramPictureMap();
                programPictureMap.setProgramid(cmsProgram.getProgramid());
                List<ProgramPictureMap> pictureMapList = programPictureMapDS.getProgramPictureMapByCond(programPictureMap);
                if (pictureMapList != null && pictureMapList.size() > 0)
                {
                    for(ProgramPictureMap picprogmap:pictureMapList)
                    {
                        target.setObjecttype(35);
                        target.setObjectid(picprogmap.getMappingid());
                        target.setTargetindex(targetindex);
                        List<CntTargetSync> ppTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        if(ppTargetList != null && ppTargetList.size() > 0)
                        {
                            relateindex = ppTargetList.get(0).getRelateindex();
                            CntTargetSync ppTargetSync = new CntTargetSync();
                            CntPlatformSync ppPlatform = new CntPlatformSync();
                            ppPlatform.setSyncindex(relateindex);
                            ppPlatform = cntPlatformSyncDS.getCntPlatformSync(ppPlatform);
                            allTarget.setRelateindex(relateindex);
                            List<CntTargetSync> onePlatCpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                            if(onePlatCpTargetList.size() > 1)
                            {
                                //删除网元数据
                                ppTargetSync.setSyncindex(ppTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(ppTargetSync);
                                
                                //更新平台状态
                                allTarget.setRelateindex(relateindex);
                                List<CntTargetSync> allPpTargetList = cntTargetSyncDS.getCntTargetSyncByCond(allTarget);
                                int[] ppTargetStatus = new int[allPpTargetList.size()];
                                if(allPpTargetList != null && allPpTargetList.size() > 0){
                                    for(int i = 0; i< allPpTargetList.size(); i++){
                                        ppTargetStatus[i] = allPpTargetList.get(i).getStatus();
                                    }
                                }
                                platformStatus = StatusTranslator.getPlatformSyncStatus(ppTargetStatus);
                                ppPlatform.setStatus(platformStatus);
                                cntPlatformSyncDS.updateCntPlatformSync(ppPlatform);
                            }else
                            {
                                //删除网元数据
                                ppTargetSync.setSyncindex(ppTargetList.get(0).getSyncindex());
                                cntTargetSyncDS.removeCntTargetSync(ppTargetSync);
                                //删除平台数据
                                cntPlatformSyncDS.removeCntPlatformSync(ppPlatform);
                                CommonLogUtil.insertOperatorLog(picprogmap.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                        CommonLogConstant.MGTTYPE_PROGRAM,
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PICPROGMAP_BIND_DELETE), 
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PICPROGMAP_BIND_DELETE_INFO),
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            CommonLogUtil.insertOperatorLog(picprogmap.getMappingid().toString() + ", " + tagertsystem.getTargetid(), 
                                    CommonLogConstant.MGTTYPE_PROGRAM,
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PICPROGMAP_BIND_DELETE), 
                                    ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PICPROGMAP_BIND_DELETE_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        
                    }
                }
                
            }else
            {
                return "1:与网元关联关系不存在";
            }
            
        }catch (Exception e)
        {
            log.error("CastTargetSyncLS exception:" + e.getMessage());
            return "1:操作失败";
        }
        
        return "0:操作成功";
    }
    
    /**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 送入回收穿
     * @param programindex
     * @return
     * @throws Exception
     */
    public String recycleProgram(Long programindex )
    {
        log.debug("recycleProgram start.....");
        String result = ResourceMgt.findDefaultText("cnt.do.success");;
        try
        {
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(programindex);
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if(cmsProgram == null)
            {
                return  ResourceMgt.findDefaultText("do.fail.cnt.not.exist");
            }
            else if(cmsProgram.getStatus() != 150 && cmsProgram.getStatus() != 180)
            {
                return ResourceMgt.findDefaultText("do.fail.cnt.status.wrong");
            }
            else 
            {
                cmsProgram.setStatus(160);
                cmsProgramDS.updateCmsProgram(cmsProgram);
                
                List<CmsMovie> list = null;
                CmsMovie cmsMovie = new CmsMovie();
                cmsMovie.setProgramindex(programindex);
                list = cmsMovieDS.getCmsMovieByCond(cmsMovie);
                if(list !=null && list.size() > 0)
                {
                    for(CmsMovie movie : list)
                    {
                        movie.setStatus(160);
                        cmsMovieDS.updateCmsMovie(movie);
                    }
                }
                
                // 写操作日彿
                CommonLogUtil.insertOperatorLog(cmsProgram.getProgramid(), CommonLogConstant.MGTTYPE_PROGRAM,
                        CommonLogConstant.OPERTYPE_CONTENT_RECYCLE, CommonLogConstant.OPERTYPE_CONTENT_RECYCLE_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        
        log.debug("recycleProgram end");
        return result;
    }
    /**
     * 根据连续index获取内容对象
     * 
     * @param programindex 內容Index
     * @return 内容对象
     */
    public CmsProgram getCmsProgramByIndex(Long programindex)throws DomainServiceException 
    {
        log.debug("getCmsProgramByIndex starting...");
        CmsProgram cmsProgram = new CmsProgram();
        try 
        {
	        cmsProgram.setProgramindex(programindex);
	        cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
        }
        catch (DomainServiceException e) 
        {
	        log.error("getCmsProgramByIndex exception:" + e);
	        throw e;
        }
        log.debug("getCmsProgramByIndex end");
        return cmsProgram;
    }
    
    // 获取时间 如"20111002"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }


    public void setCmsMovieDS(ICmsMovieDS cmsMovieDS)
    {
        this.cmsMovieDS = cmsMovieDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }

    public void setCntApply(ICntApply cntApply)
    {
        this.cntApply = cntApply;
    }

    public void setCntModApply(ICntModApply cntModApply)
    {
        this.cntModApply = cntModApply;
    }

    public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
    {
        this.programCrmMapDS = programCrmMapDS;
    }

    public void setServiceProgramMapDS(IServiceProgramMapDS serviceProgramMapDS)
    {
        this.serviceProgramMapDS = serviceProgramMapDS;
    }

    public void setProgramPictureMapDS(IProgramPictureMapDS programPictureMapDS)
    {
        this.programPictureMapDS = programPictureMapDS;
    }

    public void setCtgpgmapDS(ICategoryProgramMapDS ctgpgmapDS)
    {
        this.ctgpgmapDS = ctgpgmapDS;
    }
    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }
    public ICmsProgramWkfwhisDS getCmsProgramWkfwhisDS()
    {
        return cmsProgramWkfwhisDS;
    }

    public void setCmsProgramWkfwhisDS(ICmsProgramWkfwhisDS cmsProgramWkfwhisDS)
    {
        this.cmsProgramWkfwhisDS = cmsProgramWkfwhisDS;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setProgramPlatformSynLS(IProgramPlatformSynLS programPlatformSynLS)
    {
        this.programPlatformSynLS = programPlatformSynLS;
    }
    
    public void setCmsLpopNoauditDS(ICmsLpopNoauditDS cmsLpopNoauditDS)
    {
        this.cmsLpopNoauditDS = cmsLpopNoauditDS;
    }

}

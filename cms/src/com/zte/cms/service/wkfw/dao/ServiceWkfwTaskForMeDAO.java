package com.zte.cms.service.wkfw.dao;

import java.util.List;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwTaskForMe;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ServiceWkfwTaskForMeDAO extends DynamicObjectBaseDao implements IServiceWkfwTaskForMeDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#queryStrategyWkfwTaskForMeListByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public List<ServiceWkfwTaskForMe> queryServiceWkfwTaskForMeListByCond(
            ServiceWkfwCondition serviceWkfwTaskForMeCondition) throws DAOException
    {
        log.debug("query serviceWkfwTaskForMeList by condition starting...");
        List<ServiceWkfwTaskForMe> rList = (List<ServiceWkfwTaskForMe>) super.queryForList(
                "queryServiceWkfwTaskForMeListByCond", serviceWkfwTaskForMeCondition);
        log.debug("query serviceWkfwTaskForMeList by condition end");
        return rList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#queryStrategyWkfwTaskForMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public int queryserviceWkfwTaskForMeListCntByCond(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DAOException
    {
        log.debug("query serviceWkfwTaskForMeList count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryServiceWkfwTaskForMeListCntByCond",
                serviceWkfwTaskForMeCondition)).intValue();
        log.debug("query serviceWkfwTaskForMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query serviceWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceWkfwTaskForMeListCntByCond",
                serviceWkfwTaskForMeCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceWkfwTaskForMe> rsList = (List<ServiceWkfwTaskForMe>) super.pageQuery(
                    "queryServiceWkfwTaskForMeListByCond", serviceWkfwTaskForMeCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query strategyWkfwTaskForMeList page info by condition end");
        return pageInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwTaskForMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryServiceWkfwTaskForMeListByCond",
                "queryServiceWkfwTaskForMeListCntByCond", serviceWkfwTaskForMeCondition, start, pageSize, puEntity);
    }
}
package com.zte.ucm.util;

import com.zte.cms.common.DbUtil;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;

import java.util.List;
import java.util.Map;


/**
 * 权限工具类
 */
public class PermissionsUtil {

    /**
     * 检查是否是管理员
     * @param userId
     * @return
     */
    public static boolean isAdministrator(String userId){
        //查询该id用户的对象判断是否为pc
        StringBuffer sb = new StringBuffer();
        sb.append("select * from zxinsys.OPER_RIGHTS where OPERID='");
        sb.append(userId);
        sb.append("' and OPERGRPID='1000' and SERVICEKEY='CMS'");
        DbUtil dbutil = new DbUtil();
        List reslist = null;
        try {
            reslist= dbutil.getQuery(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (reslist==null){
            throw new RuntimeException(EmunBusinessError.NUKNOWN_ERROR.getErrorMsg());
        }
        if (reslist.size()==0){
            return false;
        }else {
            return true;
        }
    }

    /**
     * 进行operid和cpname的校验
     * @param userId
     * @param cpName
     * @return
     */
    public static boolean checkCpCodeAndCpName(String userId,String cpName){
        //查询该id用户的对象判断是否为pc
        StringBuffer sb = new StringBuffer();
        sb.append("select  count(*) from zxdbm_umap.cms_operator_power where cpid='");
        sb.append(userId);
        sb.append("' and cpcnshortname='");
        sb.append(cpName);
        sb.append("'");
        DbUtil dbutil = new DbUtil();
        List reslist = null;
        try {
            reslist= dbutil.getQuery(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (reslist==null){
            throw new RuntimeException(EmunBusinessError.NUKNOWN_ERROR.getErrorMsg());
        }
        Map<String, String> tmpMap = (Map<String, String>) reslist.get(0);
        if ("0".equals(tmpMap.get("count(*)").toString())){
            return false;
        }else {
            return true;
        }
    }

}

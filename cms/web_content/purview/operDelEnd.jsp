<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    try {
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        purview.setServiceKey(serviceKey); 
        HashMap map = new HashMap();
        map.put("OPERID",(String)request.getParameter("operID"));
        map.put("SESSIONOPERID",(String)session.getAttribute("OPERID"));
        map.put("SESSIONOPERNAME",(String)session.getAttribute("OPERNAME"));
        if(session.getAttribute("OPER_TYPE_ATTRIBUTE")!= null)
          map.put("OPER_TYPE_ATTRIBUTE",session.getAttribute("OPER_TYPE_ATTRIBUTE"));
        if(session.getAttribute("YWYY_CODE_ATTRIBUTE")!= null){
          map.put("YWYY_CODE_ATTRIBUTE",session.getAttribute("YWYY_CODE_ATTRIBUTE")); 
        }         
        purview.delOper(map);
        
%>
<html>
<head>
<title><fmt:message key="operDel.deleteoper"/></title>
</head>
<body>
<script language="javascript">
    window.opener.refresh();
    window.close();
</script>
</body>
</html>
<%
    }
    catch (Exception e) {
        out.println(e.getMessage());
    }
%>

package com.zte.cms.task.dao;

import com.zte.cms.task.model.TaskCmsauditRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;

/**
 * @Author: Liangxiaomin
 * @Date Created in 16:16 2019/7/10
 * @Description:
 */
public interface ICmsTaskAuditRecordDAO {

    public PageInfo pageInfoQuery(TaskCmsauditRecord record, int start, int pageSize) throws DAOException;

    boolean insert(TaskCmsauditRecord record);

}

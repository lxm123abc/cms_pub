
  package com.zte.cms.content.CStargetSyn.dao;

import java.util.List;

import com.zte.cms.content.CStargetSyn.dao.ICSCntTargetSyncDAO;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CSCntTargetSyncDAO extends DynamicObjectBaseDao implements ICSCntTargetSyncDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntTargetSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("csqueryCntTargetSyncListCntByCond",  cntTargetSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CSCntTargetSync> rsList = (List<CSCntTargetSync>)super.pageQuery( "csqueryCntTargetSyncListByCond" ,  cntTargetSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntTargetSync by condition end");
    	return pageInfo;
    }
	
	public PageInfo pageInfoQueryCha(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntTargetSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("csqueryCntTargetSyncListCntByCondCha",  cntTargetSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CSCntTargetSync> rsList = (List<CSCntTargetSync>)super.pageQuery( "csqueryCntTargetSyncListByCondCha" ,  cntTargetSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntTargetSync by condition end");
    	return pageInfo;
    }
	
	public PageInfo pageInfoQueryCach(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntTargetSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("csqueryCntTargetSyncListCntByCond",  cntTargetSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CSCntTargetSync> rsList = (List<CSCntTargetSync>)super.pageQuery( "csqueryCntTargetSyncListByCondCach" ,  cntTargetSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntTargetSync by condition end");
    	return pageInfo;
    }
	
	public PageInfo pageInfoQuerySche(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntTargetSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("csqueryCntTargetSyncListCntByCond",  cntTargetSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CSCntTargetSync> rsList = (List<CSCntTargetSync>)super.pageQuery( "csqueryCntTargetSyncListByCondSche" ,  cntTargetSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntTargetSync by condition end");
    	return pageInfo;
    }
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(DynamicBaseObject cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "csqueryCntTargetSyncListByCond", "csqueryCntTargetSyncListCntByCond", cntTargetSync, start, pageSize, puEntity);
    }

	public PageInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start,
			int pageSize, PageUtilEntity puEntity) throws DAOException
	{
		// TODO Auto-generated method stub
		return null;
	}
    
	

  
    
} 
package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.service.dao.IServiceBufDAO;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceBufDAO extends DynamicObjectBaseDao implements IServiceBufDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceBuf(ServiceBuf serviceBuf) throws DAOException
    {
        log.debug("insert serviceBuf starting...");
        super.insert("insertServiceBuf", serviceBuf);
        log.debug("insert serviceBuf end");
    }

    public void updateServiceBuf(ServiceBuf serviceBuf) throws DAOException
    {
        log.debug("update serviceBuf by pk starting...");
        super.update("updateServiceBuf", serviceBuf);
        log.debug("update serviceBuf by pk end");
    }

    public void deleteServiceBuf(ServiceBuf serviceBuf) throws DAOException
    {
        log.debug("delete serviceBuf by pk starting...");
        super.delete("deleteServiceBuf", serviceBuf);
        log.debug("delete serviceBuf by pk end");
    }

    public ServiceBuf getServiceBuf(ServiceBuf serviceBuf) throws DAOException
    {
        log.debug("query serviceBuf starting...");
        ServiceBuf resultObj = (ServiceBuf) super.queryForObject("getServiceBuf", serviceBuf);
        log.debug("query serviceBuf end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceBuf> getServiceBufByCond(ServiceBuf serviceBuf) throws DAOException
    {
        log.debug("query serviceBuf by condition starting...");
        List<ServiceBuf> rList = (List<ServiceBuf>) super.queryForList("queryServiceBufListByCond", serviceBuf);
        log.debug("query serviceBuf by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceBuf by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceBufListCntByCond", serviceBuf)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceBuf> rsList = (List<ServiceBuf>) super.pageQuery("queryServiceBufListByCond", serviceBuf,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceBuf by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceBufListByCond", "queryServiceBufListCntByCond", serviceBuf, start,
                pageSize, puEntity);
    }

}
package com.zte.cms.channel.ls;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsScheduleRecordLS
{

    /**
     * 插入收录计划
     * 
     * @param String channelid
     * @return 返回详细信息
     * @throws Exception
     */
    public String insertScheduleRecord(List<String> scheduleList, String channelid, String physicChannelid)
            throws Exception;

    /**
     * 查询物理频道是否存在 状态是否是已同步 及该频道的CP是否不是删除状态
     * 
     * @param String physicchannelid
     * @return 查询结果 sucess为频道可用
     * @throws Exception
     */
    public String getPhysicChannelByid(String physicchannelid) throws Exception;

    /**
     * 查询CmsSchedule cmsSchedule对象
     * 
     * @param 节目单ID
     * @return String "true"节目单存在 "false"节目单不存在
     * @throws DomainServiceException ds异常
     */
    public String getCmsSchedule(String scheduleid) throws Exception;

    /**
     * 查询CmsSchedule cmsSchedule对象
     * 
     * @param 节目单ID
     * @return CmsSchedule 对象
     * @throws DomainServiceException ds异常
     */
    public CmsSchedule getScheduleById(String scheduleid) throws Exception;

    /**
     * 根据条件分页查询CmsSchedule cmsSchedule对象
     * 
     * @param CmsSchedule cmsSchedule对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize) throws Exception;

    /**
     * 查询CmsSchedulerecord 对象
     * 
     * @param 节目单收录ID scheduleRecordId
     * @return CmsSchedulerecord 对象
     * @throws Exception 异常
     */
    public CmsSchedulerecord getScheduleRecordById(String scheduleRecordId) throws Exception;

    /**
     * 批量删除CmsSchedulerecord 对象
     * 
     * @param 节目单收录ID scheduleRecordId集合
     * @return String 对象
     * @throws Exception 异常
     */
    public String removeCmsScheduleRecordList(String[] scheduleidList) throws Exception;




    public String insertScheduleRecordmorePhy(List<String> scheduleList,
			String channelid, String physicChannelid) throws Exception ;
    public String insertScheduleRecordmorePhyBatch(List<String> scheduleList,
			String channelid)throws Exception;
    public TableDataInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord,
            int start, int pageSize) throws Exception ;
}

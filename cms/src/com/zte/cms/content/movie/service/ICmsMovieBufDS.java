package com.zte.cms.content.movie.service;

import java.util.List;
import com.zte.cms.content.movie.model.CmsMovieBuf;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsMovieBufDS
{
    /**
     * 新增CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 新增CmsMovieBuf对象，不重新获取主键
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsMovieBufNoPK(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 新增CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException;

    /**
     * 更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 批量更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException;

    /**
     * 根据条件更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException;

    /**
     * 删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 批量删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieBufList(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException;

    /**
     * 根据条件删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieBufListByCond(List<CmsMovieBuf> cmsMovieBufList) throws DomainServiceException;

    /**
     * 查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @return CmsMovieBuf对象
     * @throws DomainServiceException ds异常
     */
    public CmsMovieBuf getCmsMovieBuf(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 根据条件查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象
     * @return 满足条件的CmsMovieBuf对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsMovieBuf> getCmsMovieBufByCond(CmsMovieBuf cmsMovieBuf) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsMovieBuf对象
     * 
     * @param cmsMovieBuf CmsMovieBuf对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsMovieBuf cmsMovieBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
/***
    连接页面，并保存页面位置
***/
function forwordtothisurl(url){ 
        savePageStatus('zteTable');
        document.location.href=url;
}


//清空label的提示信息方法
function showErrorText(id, text){
        $("label[class='validFont']").each(hidevalidFont);
        var obj = document.getElementById(id + "_msg");
        if (obj){
            if (text != ""){
                obj.innerText = text;
            }else{
                obj.innerText = $res_entry("note.iptv.channelManage.16");
            }
        }
}


function hidevalidFont(i){
        if (i > 0){
            this.innerText = "";
        }
}

/***
    为空效验
***/
function htmlElemValueIsEmpty(elemValue)
{   
    return elemValue==""||elemValue==null
}
/**
    错误提示
***/
function setMsgCtrlText(id,text)
{
    var ctrlObj = document.getElementById(id);
    msgId = id + "_msg";
    var msgObj = document.getElementById(msgId);
    msgObj.innerText=text;
}
function setMsgCtrlTextAndFocus(id,text){ 
    var ctrlObj = document.getElementById(id);
    msgId = id + "_msg";
    var msgObj = document.getElementById(msgId);
    msgObj.innerText=text;
    ctrlObj.focus();
}
/**
    获取文本框的值
***/
function getHtmlCtrlTrimValue(id)
{
     str= strTrim(document.getElementById(id).value);
     document.getElementById(id).value=str;
     return str;
}




/**
    清空前后空格
***/
function strTrim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
}



/***
    特殊字符效验
**/
function isSafe(str,reg){
        return  !reg.test(str);   
}

// 校验内容基本信息：内容名称，内容描述，计费价格，分成比例
function checkBasicInfo(){
        if (document.getElementById("namecn"))
        {
            var namecn = document.getElementById("namecn").value;
            if (utf8_strlen2(namecn) > 128)
            {
                showErrorText("namecn",$res_entry("label.page.content.name.extralong","")); return false;
            }
        }
        if (document.getElementById("firstletter"))
        {
            var namecn = document.getElementById("firstletter").value;
            if (utf8_strlen2(namecn) > 200)
            {
                showErrorText("firstletter",$res_entry("label.page.content.firstletter.extralong","")); return false;
            }
        }
        if (document.getElementById("cntDesc"))
        {
            var cntDesc = document.getElementById("cntDesc").value;
            if (utf8_strlen2(cntDesc) > 1024)
            {
                showErrorText("cntDesc",$res_entry("label.page.content.desc.extralong","")); return false; 
            }
            if (utf8_strlen2(cntDesc) > 1024)
            {
                showErrorText("cntDesc",$res_entry("label.page.content.desc.extralong","")); return false; 
            }
        }
        return true;
    }

    // 校验加密信息
    function checkEncryptInfo(){
        if (document.getElementsByName('encrypttag'))
        {
            var encrypttag = document.getElementsByName('encrypttag')[0].checked;
            if(!encrypttag){
                var encratio = document.getElementById("encratio").value;
                if (encratio == "")
                {
                    showErrorText("encratio","");
                    return false;
                }
                if (encratio < 0 || encratio > 100)
                {
                    showErrorText("encratio",$res_entry("label.page.content.encratio.illegal",""));  
                    return false;
                }
            }
            return true;
        }
    }

    // 校验片花信息
    function checkSnapInfo(){
        if (document.getElementsByName('snapexist'))
        {
            var snapexist = document.getElementsByName('snapexist')[0].checked;
            if (!snapexist)
            {
                var snapbegintime = $("#snapbegintime").val();
                var snapendtime = $("#snapendtime").val();
                if (snapbegintime == "")
                {
                    showErrorText("snapbegintime","");
                    return false;
                }
                if (snapendtime == "")
                {
                    showErrorText("snapendtime","");
                    return false;
                }
                var t1 = snapbegintime.replace(/\D/g,"");
                var t2 = snapendtime.replace(/\D/g,"");
                if (snapendtime != "" && snapbegintime != "")
                {
                    if (t1 >= t2)
                    {
                        showErrorText("snapendtime",$res_entry("label.page.content.snapendtime.starttimeex","")); 
                        return false;
                    }
                }
            }else{
            $("#snapbegintime").val("");
            $("#snapendtime").val("");
            }
            return true;
        }
    }

    // 校验ftp信息
    function checkFTPInfo(){
        var sendType = document.getElementsByName('sendType')[0].checked;
        if (!sendType)
        {
            var addfilepath = document.getElementById("addfilepath").value;
            if (addfilepath == "")
            {
                showErrorText("addfilepath","");
                return false;
            }
            if (document.getElementById("addfilename"))
            {
                var addfilename = document.getElementById("addfilename").value;
                if (addfilename == "")
                {
                    showErrorText("addfilename","");
                    return false;
                }
                if (addfilename.indexOf('.') ==-1)
                {
                    showErrorText("addfilename",$res_entry("label.iptv.content.filesuffix",""));
                    return false;
                }
                if (utf8_strlen2(addfilename) > 60)
                {
                    showErrorText("addfilename",$res_entry("label.page.content.addfilename.extralong","")); return false;  
                }
            }
            return true;
        }else{
            var ip = $("#ip").val();
            var port = $("#port").val();
            var useraccount = $("#useraccount").val();
            var password = $("#password").val();
            var filepath = $("#filepath").val();
            if (ip == "") {
                showErrorText("ip",""); return false;
            }
            if (!isIp(ip))
            {
                showErrorText("ip",$res_entry("label.page.content.format.error","")); return false;  
            }
            if (port == "") {
                showErrorText("port",""); return false;
            }
            if (port <0 || port > 65535)
            {
                showErrorText("port",$res_entry("label.page.content.port.notinrange","")); return false;  
            }
            if (useraccount == "") {
                showErrorText("useraccount",""); return false;
            }
            if (password == "") {
                showErrorText("password",""); return false;
            }
            if (filepath == "") {
                showErrorText("filepath",""); return false;
            }
            if (document.getElementById("filename"))
            {
                var filename = $("#filename").val();
                if (filename == "") {
                    showErrorText("filename",""); return false;
                }
                if (filename.indexOf('.') ==-1)
                {
                    showErrorText("filename",$res_entry("label.iptv.content.filesuffix",""));
                    return false;
                }
                if (utf8_strlen2(filename) > 60)
                {
                    showErrorText("",$res_entry("label.page.content.filename.extralong","")); return false; 
                }
            }
            return true;
        }
    }

	/**
	 * 校验资源的生效时间，终止时间
	 */
	function checkTime(propertyName1,propertyName2){
	
	    //var prop1 = propertyName1.charAt(0).toUpperCase() + propertyName1.substr(1);
	    //var prop2 = propertyName2.charAt(0).toUpperCase() + propertyName2.substr(1);
	    //获取控件的id
	    //页面提交校验
	    var id1 = propertyName1 + "_msg";
	    var id2 = propertyName2 + "_msg";
	    var vstarttime = document.getElementById(propertyName1).value;
	    var vendtime = document.getElementById(propertyName2).value; 
	    
	    if(vstarttime!="" && vendtime!=""){
	        if(!checkDateMatchWide(vstarttime,vendtime)) {
	            document.getElementById(id1).innerText = "";
	            document.getElementById(id1).innerText =$res_entry("msg.cpspmanager.check.12");
	            return 0 ;
	        }
	    }
	    else{
	         return 1;
	    }  
	       
	}  
	
	function validaTime(timeid){
	    var timeid = document.getElementById(timeid).value;
	    if( timeid==""||timeid==null){
	       return false;
	    }else{
	       return timeid;
	    }
	}
	
	
    /**
	 * 非空
	 * @param {String} input_id
	 *  null,and reutrn false;
	 */
    function checkEmptyWide(input_id){
	    var whitespace = new String(" \t\n\r");
        var s = new String(str);
     if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	    {
	        var i = s.length - 1;
	        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
	        {
	             i--;
	        }
	        s = s.substring(0, i+1);
	    }
	    return s;
	    }

    
    
    /**
	 * 非空
	 * @param {String} input_id
	 *  null,and reutrn false;
	 */
	function checkEmptyWideVal(input_id){
	    var bool=strTrim($('#'+input_id).val());
	    $('#'+input_id).val(bool);
	    return bool?false:true;
	}
    
    
	/**
	 * 时间比较
	 * @param {String} begintime
	 * @param {String} endtime
	 * The end time is less than start time, it returns false 
	 */
	function checkDateMatchWide(begintime,endtime){
	    var begindate=new Date();
	    begintime=begintime.split(' ');
	    begintime=begintime[0].split('-');
	    begindate.setFullYear(begintime[0],begintime[1]-1,begintime[2]);
	
	    var enddate=new Date();
	    endtime=endtime.split(' ');
	    endtime=endtime[0].split('-');
	    enddate.setFullYear(endtime[0],endtime[1]-1,endtime[2]);
	
	    if(begindate<enddate)
	        return true;
	    else
	        return false;
	}

    // 校验生效时间和失效filename时间
    function checkStartEndTime(){
        if (document.getElementById("starttime") && document.getElementById("endtime"))
        {
            var starttime = document.getElementById("starttime").value;
            var endtime = document.getElementById("endtime").value;
            var t1 = starttime.replace(/\D/g,"");
            var t2 = endtime.replace(/\D/g,"");
            if (starttime.length > 0){
                var cur = curDateTime().replace(/\D/g,"");
                /*if (cur >= t1)
                {
                    showErrorText("starttime",$res_entry("label.page.content.starttime.lessthancurrent","")); 
                    return false;
                }*/
            }else{
            showErrorText("starttime",$res_entry("msg.iptv.product.shouldnot.be.null","")); 
            return false;
            }
            if (endtime.length > 0){
                var cur = curDateTime().replace(/\D/g,"");
                if (cur >= t2)
                {
                    showErrorText("endtime",$res_entry("label.page.content.endtime.lessthancurrent",""));  
                    return false;
                }
            }else{
            showErrorText("endtime",$res_entry("msg.iptv.product.shouldnot.be.null","")); 
            return false;
            }
            if (starttime.length > 0 && endtime.length > 0)
            {
                if (t1 >= t2)
                {
                    showErrorText("starttime",$res_entry("label.page.content.starttime.extracurrent",""));
                    return false;
                }
            }           
        }
        return true;
}


//计算UTF8字符串占用字节数
function utf8_strlen2(str) 
{ 
    var cnt = 0; 
    for( i=0; i<str.length; i++) 
    { 
        var value = str.charCodeAt(i); 
        if( value < 0x080) 
        { 
            cnt += 1; 
        } 
        else if( value < 0x0800) 
        { 
            cnt += 2; 
        } 
        else  if(value < 0x010000)
        { 
            cnt += 3; 
        }
        else
        {
            cnt += 4;
        }
    } 
    return cnt; 
}
//检验电话号码  数字、字母、-
function isMobile(str)
{
    if(str.length > 20)
    {
        return false;
    }
    else
    {
        var regex=/^[0-9a-zA-Z\-]*$/;
        return regex.test(str);
    }
}
//检验邮编  数字、字母、-
function isZipCode(str)
{
    if(str.length > 20)
    {
        return false;
    }
    else
    {
        var regex=/^[0-9a-zA-Z\-]*$/;
        return regex.test(str);
    }
}
//在做提交前清空所有的提示信息
function clearAllMsgLabel()
{
    $("label[@id$='_msg']").html("");
}

//动态改变select 框的长度
function dynChgSelectWidth(id)
{
    var selectObj = document.getElementById(id);
    if(selectObj != undefined && selectObj != null)
    {
        var v  = selectObj.options[selectObj.selectedIndex].text;
        var width=7*utf8_strlen2(v);
        width = (width<130 ? 130 : width);
        selectObj.style.width=width;
    }
}


//最大值
var historyStr ="";
function checkMaxInput(_this,maxleng,leftInforId) 
{ 
    var left=document.getElementById(leftInforId); 
    var len=utf8_strlen2(_this.value); 
    var remainnum =parseInt(maxleng)-len; 
    left.innerHTML = remainnum; 
    if(remainnum < 0) 
    { 
        if(_this.value.length!=len) 
        { 
            if((len-_this.value.length)>(maxleng/3)) 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng/3); 
            } 
            else 
            { 
                _this.value = historyStr;
            //_this.value=_this.value.substring(0,maxleng-(len-_this.value.length)); 
            } 
        }else{ 
            _this.value=_this.value.substring(0,maxleng); 
        } 
            left.value=0; 
            return; 
    } 
    historyStr = _this.value;
}


/**
 * 控件展示方式

function setComponentWide(propertyName,visible,span,txt,lbl,btn,txtMsg,note,lblOld){
    var prop = propertyName.charAt(0).toUpperCase() + propertyName.substr(1);
    var id0 = "span" + prop;
    var id1 = "txt" + prop;
    var id2 = "lbl" + prop;
    var id3 = "btn" + prop;
    var id4 = "txt" + prop + "_msg";
    var id5 = "note" + prop;
    var id6 = "lbl" + prop + "_old"; 
    var id7 = "th" + prop;
    var id8 = "td" + prop;
    if(visible == 0){
        $("#"+id7).attr("style","display:none");
        $("#"+id8).attr("style","display:none");
        return;
    }else{
        $("#"+id7).attr("style","display:");
        $("#"+id8).attr("style","display:");
    }
    if(span == 0){
        $("#"+id0).attr("style","display:none");
    }else{
        $("#"+id0).attr("style","display:");
    }    
    if(txt == 0){
        try{
           document.getElementById(id1).style.display = "none";
        }catch(e){
           $("#"+id1).attr("style","display:none");
        }
      }else{
         $("#"+id1).attr("style","display:");
     }
     if(lbl == 0){
        $("#"+id2).attr("style","display:none");
     }else{
         $("#"+id2).attr("style","display:");
     } 
     if(btn == 0){
        $("#"+id3).attr("style","display:none");
     }else{
        $("#"+id3).attr("style","display:");
     }   
     if(txtMsg == 0){
        $("#"+id4).attr("style","display:none");
     }else{
        $("#"+id4).attr("style","display:");
     }  
     if(note == 0){
        $("#"+id5).attr("style","display:none");
     }else{
        $("#"+id5).attr("style","display:");
     }          
     if(lblOld == 0){
        $("#"+id6).attr("style","display:none");
     }else{
        $("#"+id6).attr("style","display:");
     }  
}

  
  function setAllComponent(visible,span,txt,lbl,btn,txtMsg,note,lblOld){    
        var properties = ["namecn","nameen","contenttype","keywords","effectiveStartdate","expiryEnddate","country","director","actor","producer","premieredateTime","recommendedstar","playtimeTime","releasedateTime","publisher","subtitlelanguage","dubbinglanguage","languages","introduction"];
        for(var index = 0;index<properties.length;index++){
            setComponentWide(properties[index],visible,span,txt,lbl,btn,txtMsg,note,lblOld);
        }
         document.getElementById('divdes').style.display="none";
    }
    
    function init(){
        setAllComponent(1,0,0,1,0,0,0,0);       
    }
 */
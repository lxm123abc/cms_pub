package com.zte.cms.content.wkfw.dao;

import java.util.List;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwProcessResult;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class IcmContentWkfwProcessResultDAO extends DynamicObjectBaseDao implements IIcmContentWkfwProcessResultDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @SuppressWarnings("unchecked")
    public List<IcmContentWkfwProcessResult> getIcmContentWkfwProcessResultByCond(
            IcmContentWkfwCondition contentWkfwCondition) throws DAOException
    {
        log.debug("query IcmContentWkfwProcessResult by condition starting...");
        List<IcmContentWkfwProcessResult> rList = (List<IcmContentWkfwProcessResult>) super.queryForList(
                "queryIcmContentWkfwProcessResultListByCond", contentWkfwCondition);
        log.debug("query IcmContentWkfwProcessResult by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query IcmContentWkfwProcessResult by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmContentWkfwProcessResultListCntByCond",
                contentWkfwCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmContentWkfwProcessResult> rsList = (List<IcmContentWkfwProcessResult>) super.pageQuery(
                    "queryIcmContentWkfwProcessResultListByCond", contentWkfwCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query IcmContentWkfwProcessResult by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryIcmContentWkfwProcessResultListByCond",
                "queryIcmContentWkfwProcessResultListCntByCond", contentWkfwCondition, start, pageSize, puEntity);
    }

    public long getMyStartWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition) throws DAOException
    {
        long result = super.getTotalCount("queryIcmContentWkfwProcessResultListCntByCond", contentWkfwCondition);
        return result;
    }

}
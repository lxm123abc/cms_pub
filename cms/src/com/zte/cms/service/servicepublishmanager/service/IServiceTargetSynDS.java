

package com.zte.cms.service.servicepublishmanager.service;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceTargetSynDS
{
   

	/**
	 * 查询serviceTargetSyn对象
	 
	 * @param ServicePlatformSyn serviceTargetSyn对象
	 * @return serviceTargetSyn对象
	 * @throws DomainServiceException ds异常 
	 */
	// public ServicePlatformSyn getserviceTargetSyn( ServicePlatformSyn ServicePlatformSyn )throws DomainServiceException; 



	 /**
	  * 根据条件分页查询serviceTargetSyn对象 
	  *
	  * @param ServicePlatformSyn serviceTargetSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DomainServiceException;
     
     /**
      * 根据条件分页查询serviceTargetSyn对象 
      *
      * @param Service service对象，作为查询条件的参数 
      * @param start 起始行
      * @param pageSize 页面大小
      * @param puEntity 排序空置参数@see PageUtilEntity
      * @return  查询结果
      * @throws DomainServiceException ds异常
      */
     public TableDataInfo pageInfoQueryAll(Service service, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询serviceTargetSyn对象 
	  *
	  * @param ServicePlatformSyn serviceTargetSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;

     /**
      * 更新ServiceTargetSyn对象
      * 
      * @param targetsystem ServiceTargetSyn对象
      * @throws DomainServiceException ds异常
      */
     public void updateServiceTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException;
     
     /**
      * 根据主键获取ServiceTargetSyn对象
      * 
      * @param serviceTargetSyn ServiceTargetSyn对象
      * @throws DAOException dao异常
      */
     
     public ServiceTargetSyn getServiceTargetSynByID( Long syncindex ) throws DomainServiceException;

     
     public List<ServiceTargetSyn> getServiceTargetSynByCond(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException;
     
     public TableDataInfo serProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DomainServiceException;
     public TableDataInfo serSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DomainServiceException;
     public TableDataInfo serChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)throws DomainServiceException;
     
     //根据syncindex获取网元发布数据
     public ServiceTargetSyn getMappingTargetSyn( ServiceTargetSyn serviceTargetSyn ) throws DomainServiceException;
     //根据objectid和targetindex获得mapping关系在该网元发布状态表数据
     public ServiceTargetSyn getObjectTargetSyn( ServiceTargetSyn serviceTargetSyn ) throws DomainServiceException;
     
     public List<ServiceTargetSyn> getAllServiceTargetSynById( ServiceTargetSyn serviceTargetSyn ) throws DomainServiceException;
     
     //根据parentid和targetindex获得所有具有相同网元和相同父节点的mapping关系
     public List<ServiceTargetSyn> getSameParentTargetMappingSyn(ServiceTargetSyn serviceTargetSyn )  throws DomainServiceException;



}
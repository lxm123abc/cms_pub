/**
 * QueryMediaInfoPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public interface QueryMediaInfoPort extends java.rmi.Remote
{
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponse queryMediaInfo(
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoRequest queryMediaInfoRequest)
            throws java.rmi.RemoteException;
}

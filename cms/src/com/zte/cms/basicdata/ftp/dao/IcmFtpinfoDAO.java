package com.zte.cms.basicdata.ftp.dao;

import java.util.List;

import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class IcmFtpinfoDAO extends DynamicObjectBaseDao implements IIcmFtpinfoDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("insert icmFtpinfo starting...");
        super.insert("insertIcmFtpinfo", icmFtpinfo);
        log.debug("insert icmFtpinfo end");
    }

    public void updateIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("update icmFtpinfo by pk starting...");
        super.update("updateIcmFtpinfo", icmFtpinfo);
        log.debug("update icmFtpinfo by pk end");
    }

    public void updateIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("update icmFtpinfo by conditions starting...");
        super.update("updateIcmFtpinfoByCond", icmFtpinfo);
        log.debug("update icmFtpinfo by conditions end");
    }

    public void deleteIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("delete icmFtpinfo by pk starting...");
        super.delete("deleteIcmFtpinfo", icmFtpinfo);
        log.debug("delete icmFtpinfo by pk end");
    }

    public void deleteIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("delete icmFtpinfo by conditions starting...");
        super.delete("deleteIcmFtpinfoByCond", icmFtpinfo);
        log.debug("update icmFtpinfo by conditions end");
    }

    public IcmFtpinfo getIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("query icmFtpinfo starting...");
        IcmFtpinfo resultObj = (IcmFtpinfo) super.queryForObject("getIcmFtpinfo", icmFtpinfo);
        log.debug("query icmFtpinfo end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<IcmFtpinfo> getIcmFtpinfoByCond(IcmFtpinfo icmFtpinfo) throws DAOException
    {
        log.debug("query icmFtpinfo by condition starting...");
        List<IcmFtpinfo> rList = (List<IcmFtpinfo>) super.queryForList("queryIcmFtpinfoListByCond", icmFtpinfo);
        log.debug("query icmFtpinfo by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmFtpinfo by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmFtpinfoListCntByCond", icmFtpinfo)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmFtpinfo> rsList = (List<IcmFtpinfo>) super.pageQuery("queryIcmFtpinfoListByCond", icmFtpinfo,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmFtpinfo by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryIcmFtpinfoListByCond", "queryIcmFtpinfoListCntByCond", icmFtpinfo, start,
                pageSize, puEntity);
    }

    public IcmFtpinfo getIcmFtpinfoByinfoindex(Long infoindex) throws DAOException
    {

        return (IcmFtpinfo) super.queryForObject("getIcmFtpinfoByinfoindex", infoindex);

    }

}

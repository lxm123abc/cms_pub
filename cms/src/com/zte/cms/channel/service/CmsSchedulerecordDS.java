package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.dao.ICmsSchedulerecordDAO;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.common.Generator;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.cms.common.Generator;

public class CmsSchedulerecordDS implements ICmsSchedulerecordDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsSchedulerecordDAO dao = null;
    private IPrimaryKeyGenerator primaryKeyGenerator = null;
    String schedulerecordId = "";

    public void setDao(ICmsSchedulerecordDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("insert cmsSchedulerecord starting...");
        try
        {
            Long scheduleRecordindex = (Long) this.primaryKeyGenerator.getPrimarykey("cms_schedulerecord");
            schedulerecordId = Generator.getPhysicalContentId("SCHERD");
            cmsSchedulerecord.setSchedulerecordindex(scheduleRecordindex);
            cmsSchedulerecord.setCpcontentid(schedulerecordId);
            cmsSchedulerecord.setSchedulerecordid(schedulerecordId);
            dao.insertCmsSchedulerecord(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsSchedulerecord end");
    }

    public void updateCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("update cmsSchedulerecord by pk starting...");
        try
        {
            dao.updateCmsSchedulerecord(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedulerecord by pk end");
    }

    public void updateCmsSchedulerecordList(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException
    {
        log.debug("update cmsSchedulerecordList by pk starting...");
        if (null == cmsSchedulerecordList || cmsSchedulerecordList.size() == 0)
        {
            log.debug("there is no datas in cmsSchedulerecordList");
            return;
        }
        try
        {
            for (CmsSchedulerecord cmsSchedulerecord : cmsSchedulerecordList)
            {
                dao.updateCmsSchedulerecord(cmsSchedulerecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedulerecordList by pk end");
    }

    public void updateCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("update cmsSchedulerecord by condition starting...");
        try
        {
            dao.updateCmsSchedulerecordByCond(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedulerecord by condition end");
    }

    public void updateCmsSchedulerecordListByCond(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException
    {
        log.debug("update cmsSchedulerecordList by condition starting...");
        if (null == cmsSchedulerecordList || cmsSchedulerecordList.size() == 0)
        {
            log.debug("there is no datas in cmsSchedulerecordList");
            return;
        }
        try
        {
            for (CmsSchedulerecord cmsSchedulerecord : cmsSchedulerecordList)
            {
                dao.updateCmsSchedulerecordByCond(cmsSchedulerecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSchedulerecordList by condition end");
    }

    public void removeCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("remove cmsSchedulerecord by pk starting...");
        try
        {
            dao.deleteCmsSchedulerecord(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedulerecord by pk end");
    }

    public void removeCmsSchedulerecordList(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException
    {
        log.debug("remove cmsSchedulerecordList by pk starting...");
        if (null == cmsSchedulerecordList || cmsSchedulerecordList.size() == 0)
        {
            log.debug("there is no datas in cmsSchedulerecordList");
            return;
        }
        try
        {
            for (CmsSchedulerecord cmsSchedulerecord : cmsSchedulerecordList)
            {
                dao.deleteCmsSchedulerecord(cmsSchedulerecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedulerecordList by pk end");
    }

    public void removeCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("remove cmsSchedulerecord by condition starting...");
        try
        {
            dao.deleteCmsSchedulerecordByCond(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedulerecord by condition end");
    }

    public void removeCmsSchedulerecordListByCond(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException
    {
        log.debug("remove cmsSchedulerecordList by condition starting...");
        if (null == cmsSchedulerecordList || cmsSchedulerecordList.size() == 0)
        {
            log.debug("there is no datas in cmsSchedulerecordList");
            return;
        }
        try
        {
            for (CmsSchedulerecord cmsSchedulerecord : cmsSchedulerecordList)
            {
                dao.deleteCmsSchedulerecordByCond(cmsSchedulerecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSchedulerecordList by condition end");
    }

    public CmsSchedulerecord getCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord by pk starting...");
        CmsSchedulerecord rsObj = null;
        try
        {
            rsObj = dao.getCmsSchedulerecord(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSchedulerecordList by pk end");
        return rsObj;
    }

    public List<CmsSchedulerecord> getCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord)
            throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord by condition starting...");
        List<CmsSchedulerecord> rsList = null;
        try
        {
            rsList = dao.getCmsSchedulerecordByCond(cmsSchedulerecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSchedulerecord by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSchedulerecord, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedulerecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedulerecord page info by condition end");
        return tableInfo;
    }
    
    public TableDataInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerychannel(cmsSchedulerecord, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedulerecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedulerecord page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSchedulerecord, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSchedulerecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSchedulerecord page info by condition end");
        return tableInfo;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    @Override
    public List<CmsSchedulerecord> getCmsSchedulerecordByID(CmsSchedulerecord cmsSchedulerecord)
            throws DomainServiceException
    {
        log.debug("get cmsSchedulerecord by scheduleid starting...");
        List<CmsSchedulerecord> rsList = null;
        try
        {
            rsList = dao.getCmsSchedulerecordByID(cmsSchedulerecord);
            
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSchedulerecord by scheduleid end");
        return rsList;
    }
    
}

package com.zte.cms.content.program.dao;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgram;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsProgramDAO
{
    /**
     * 新增CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DAOException dao异常
     */
    public void insertCmsProgram(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据主键更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DAOException dao异常
     */
    public void updateCmsProgram(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据条件更新CmsProgram对象
     * 
     * @param cmsProgram CmsProgram更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsProgramByCond(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据主键删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @throws DAOException dao异常
     */
    public void deleteCmsProgram(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据条件删除CmsProgram对象
     * 
     * @param cmsProgram CmsProgram删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsProgramByCond(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据主键查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @return 满足条件的CmsProgram对象
     * @throws DAOException dao异常
     */
    public CmsProgram getCmsProgram(CmsProgram cmsProgram) throws DAOException;
    public CmsProgram getCmsProgramById(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据条件查询CmsProgram对象
     * 
     * @param cmsProgram CmsProgram对象
     * @return 满足条件的CmsProgram对象集
     * @throws DAOException dao异常
     */
    public List<CmsProgram> getCmsProgramByCond(CmsProgram cmsProgram) throws DAOException;

    /**
     * 根据条件分页查询CmsProgram对象，作为查询条件的参数
     * 
     * @param cmsProgram CmsProgram对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsProgram对象，作为查询条件的参数
     * 
     * @param cmsProgram CmsProgram对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsProgram cmsProgram, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件查询CmsProgram对象数量
     * 
     * @param cmsProgram
     * @return 查询结果
     * @throws DAOException
     */
    public int getCmsProgramCount(CmsProgram cmsProgram) throws DAOException;
    
  //super操作员查询时使用
    public PageInfo pageInfoQueryForPage(CmsProgram cmsProgram, int start, int pageSize) throws DAOException;
    
    //其他操作员查询时使用
    public PageInfo pageInfoQueryForPageForOper(CmsProgram cmsProgram, int start, int pageSize) throws DAOException;
}
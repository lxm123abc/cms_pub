package com.zte.cms.cast.service;

import java.util.List;
import com.zte.cms.cast.model.Castcdn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastcdnDS
{
    /**
     * 新增Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public String insertCastcdn(Castcdn castcdn) throws DomainServiceException;

    /**
     * 更新Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastcdn(Castcdn castcdn) throws DomainServiceException;

    /**
     * 批量更新Castcdn对象
     * 
     * @param castList Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public void updateCastcdnList(List<Castcdn> castList) throws DomainServiceException;

    /**
     * 根据条件更新Castcdn对象
     * 
     * @param castcdn Castcdn更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCastcdnByCond(Castcdn castcdn) throws DomainServiceException;

    /**
     * 根据条件批量更新Castcdn对象
     * 
     * @param castcdnList Castcdn更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCastcdnListByCond(List<Castcdn> castcdnList) throws DomainServiceException;

    /**
     * 删除Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastcdn(Castcdn castcdn) throws DomainServiceException;

    /**
     * 批量删除Castcdn对象
     * 
     * @param castcdnList Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public void removeCastcdnList(List<Castcdn> castcdnList) throws DomainServiceException;

    /**
     * 根据条件删除Castcdn对象
     * 
     * @param castcdn Castcdn删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCastcdnByCond(Castcdn castcdn) throws DomainServiceException;

    /**
     * 根据条件批量删除Castcdn对象
     * 
     * @param castcdnList Castcdn删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCastcdnListByCond(List<Castcdn> castcdnList) throws DomainServiceException;

    /**
     * 查询Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @return Castcdn对象
     * @throws DomainServiceException ds异常
     */
    public Castcdn getCastcdn(Castcdn castcdn) throws DomainServiceException;
    
    public Castcdn getCastById(Castcdn castcdn) throws DomainServiceException;

    /**
     * 根据条件查询Castcdn对象
     * 
     * @param castcdn Castcdn对象
     * @return 满足条件的Castcdn对象集
     * @throws DomainServiceException ds异常
     */
    public List<Castcdn> getCastcdnByCond(Castcdn castcdn) throws DomainServiceException;

    /**
     * 根据条件分页查询Castcdn对象
     * 
     * @param castcdn Castcdn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Castcdn对象
     * 
     * @param castcdn Castcdn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件分页查询已发布过的Castcdn对象
     * 
     * @param castcdn Castcdn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo querySynedCastcdn(Castcdn castcdn, int start, int pageSize) throws DomainServiceException;

}
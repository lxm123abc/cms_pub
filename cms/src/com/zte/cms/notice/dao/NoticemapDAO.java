package com.zte.cms.notice.dao;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.zte.cms.common.DbUtil;
import com.zte.cms.notice.model.Noticemap;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.purview.access.model.OperInformation;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;

public class NoticemapDAO extends DynamicObjectBaseDao implements INoticemapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize) throws DAOException
    {
        log.info("page query cms_message by condition starting...");
        PageInfo pageInfo = null;
        String operateCnt;
        String operate;
        if(noticemap.getMessagetype().equals(1)) 
        {
        	operateCnt = "queryNoticeListCntByCond";
        	operate = "queryNoticemapListByCond";
        }
        else 
        {
        	operateCnt = "queryNoticeLetterListCntByCond";
        	operate = "queryNoticeLettermapListByCond";
        }
        int totalCnt = ((Integer) super.queryForObject(operateCnt, noticemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<Noticemap> rsList = (List<Noticemap>) super.pageQuery(operate,
                                                                       noticemap,
                                                                       start,
                                                                       fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("page query cms_message by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                         throws DAOException
    {
        return super.indexPageQuery("queryNoticemapListByCond",
                                    "queryNoticeListCntByCond",
                                    noticemap,
                                    start,
                                    pageSize,
                                    puEntity);
    }

    public Noticemap getNoticemap(Noticemap noticemap) throws DAOException
    {
        log.info("query noticemap starting...");
        Noticemap resultObj = (Noticemap) super.queryForObject("getNoticemap", noticemap);
        log.info("query noticemap end");
        return resultObj;
    }
    
    public Noticemap getNoticeLettermap(Noticemap noticemap) throws DAOException
    {
        log.info("query noticemap starting...");
        Noticemap resultObj = (Noticemap) super.queryForObject("getNoticeLettermap", noticemap);
        log.info("query noticemap end");
        return resultObj;
    }

    public void insertNoticemap(Noticemap noticemap) throws DAOException
    {
        log.info("insert noticemap starting...");
        super.insert("insertNoticemap", noticemap);
        log.info("insert noticemap end");
    }

    public void insertCmsMessageCp(Noticemap noticemap) throws DAOException
    {
        log.info("insert cms_message_cp starting...");
        super.insert("insertCmsMessageCp", noticemap);
        log.info("insert cms_message_cp end");
    }

    public void insertCmsMessageView(Noticemap noticemap) throws DAOException
    {
        log.info("insert cms_message_view starting...");
        super.insert("insertCmsMessageView", noticemap);
        log.info("insert cms_message_view end");
    }

    public void deleteNoticemap(Noticemap noticemap) throws DAOException
    {
        log.info("delete cms_message starting...");
        super.delete("deleteNoticemap", noticemap);
        log.info("delete cms_message  end");
    }
    
    public void revokeNoticemap(Noticemap noticemap) throws DAOException
    {
        log.info("revoke cms_message starting...");
        super.update("revokeNoticemap", noticemap);
        log.info("revoke cms_message  end");
    }
    
    public void deleteNoticeMessageCp(Noticemap noticemap) throws DAOException
    {
        log.info("delete cms_message_cp starting...");
        super.delete("deleteNoticeMessageCp", noticemap);
        log.info("delete cms_message_cp  end");
    }

    public void deleteNoticeCmsMessageView(Noticemap noticemap) throws DAOException
    {
        log.info("delete cms_message_view starting...");
        super.delete("deleteNoticeCmsMessageView", noticemap);
        log.info("delete cms_message_view  end");
    }

    public void deleteNoticeCmsReplay(Noticemap noticemap) throws DAOException
    {
        log.info("delete cms_replay starting...");
        super.delete("deleteNoticeCmsReplay", noticemap);
        log.info("delete cms_replay end");
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize) throws DAOException
    {
        log.info("page query cms_replay by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryReplayListCntByCond", noticemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<Noticemap> rsList = (List<Noticemap>) super.pageQuery("queryreplayListByCond",
                                                                       noticemap,
                                                                       start,
                                                                       fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("page query cms_replay by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                               throws DAOException
    {
        return super.indexPageQuery("queryreplayListByCond",
                                    "queryReplayListCntByCond",
                                    noticemap,
                                    start,
                                    pageSize,
                                    puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize) throws DAOException
    {
        log.info("page query cms_message_view by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryViewListCntByCond", noticemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<Noticemap> rsList = (List<Noticemap>) super.pageQuery("queryViewListByCond",
                                                                       noticemap,
                                                                       start,
                                                                       fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("page query cms_message_view by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity)
                                                                                                             throws DAOException
    {
        return super.indexPageQuery("queryViewListByCond",
                                    "queryViewListCntByCond",
                                    noticemap,
                                    start,
                                    pageSize,
                                    puEntity);
    }

    public void updateNoticemap(Noticemap notictmap) throws DAOException
    {
        log.info("update notictmap by pk starting...");
        super.update("updateNoticemap", notictmap);
        log.info("update notictmap by pk end");
    }

    @SuppressWarnings("unchecked")
    public PageInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize) throws DAOException
    {
        log.info("notification page query noticemap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryNotificationListCntByCond", noticemap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<Noticemap> rsList =null;
            if(noticemap.getMessagetype()==1){
                rsList = (List<Noticemap>) super.pageQuery("queryNotificationListByCond",
                                                                           noticemap,
                                                                           start,
                                                                           fetchSize);
            }else if(noticemap.getMessagetype()==2){
                rsList = (List<Noticemap>) super.pageQuery("queryNotificationLetterListByCond",
                                                                       noticemap,
                                                                       start,
                                                                       fetchSize);
            }
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("notification page query noticemap by condition end");
        return pageInfo;
    }

    public Noticemap getNotificationDetail(Noticemap noticemap) throws DAOException
    {
        log.info("query getNotificationDetail starting...");
        Noticemap resultObj = (Noticemap) super.queryForObject("getNotificationDetail", noticemap);
        log.info("query getNotificationDetail end");
        return resultObj;
    }
    
    @SuppressWarnings("unchecked")
    public List<Noticemap> getNotificationDetails(Noticemap noticemap) throws DAOException
    {
        log.info("notification page query noticemap by condition starting...");
        List<Noticemap> rsList = (List<Noticemap>) super.queryForList("getNotificationDetail", noticemap);
        log.info("notification page query noticemap by condition end");
        return rsList;
    }
    
    public Noticemap getNotificationDetailOnlyMes(Noticemap noticemap) throws DAOException
    {
        log.info("query getNotificationDetail starting...");
        Noticemap resultObj = (Noticemap) super.queryForObject("getNotificationDetailOnlyMes", noticemap);
        log.info("query getNotificationDetail end");
        return resultObj;
    }
    
    public List<Noticemap> getNotificationDetailOnlyMess(Noticemap noticemap) throws DAOException
    {
        log.info("notification page query noticemap by condition starting...");
        List<Noticemap> rsList = (List<Noticemap>) super.queryForList("getNotificationDetailOnlyMes", noticemap);
        log.info("notification page query noticemap by condition end");
        return rsList;
    }
    public Noticemap getCmsOperatorPowerInfo(Noticemap noticemap) throws DAOException
    {
        log.info("query getCmsOperatorPowerInfo starting...");
        Noticemap resultObj = (Noticemap) super.queryForObject("getCmsOperatorPowerInfo", noticemap);
        log.info("query getCmsOperatorPowerInfo end");
        return resultObj;
    }

    public void updateView(Noticemap noticemap) throws DAOException
    {
        log.info("update View starting...");
        super.update("updateView", noticemap);
        log.info("update View end");
    }
    
    public void updateReceiver(Noticemap noticemap) throws DAOException
    {
        log.info("update Receiver starting...");
        if(noticemap.getMessagetype()==1){
            super.update("updateReceiver", noticemap);
        }
        if(noticemap.getMessagetype()==2){
            super.update("updateLetterReceiver", noticemap);
        }
        log.info("update Receiver end");
    }
    
    public void updateReplay(Noticemap noticemap) throws DAOException
    {
        log.info("update replay starting...");
        super.update("updateReplay", noticemap);
        log.info("update replay end");
    }

    public void setStatusRead(Noticemap noticemap) throws DAOException
    {
        log.info("update Status Read starting...");
        super.update("setStatusRead", noticemap);
        log.info("update Status Read end");
    }

    public void setNoticeViewcpnum(Noticemap noticemap) throws DAOException
    {
        log.info("update viewcpnum Read starting...");
        super.update("setNoticeViewcpnum", noticemap);
        log.info("update viewcpnum Read end");
    }

    public void insertReplay(Noticemap noticemap) throws DAOException
    {
        log.info("insert replay starting...");
        super.insert("insertReplay", noticemap);
        super.update("updateMessage_Replay", noticemap);   
        super.update("updateViewToNotReader", noticemap);      
        log.info("insert replay end");
    }

    public void deleteReplay(Noticemap noticemap) throws DAOException
    {
        log.info("delete replay_message starting...");
        super.delete("deleteReplay", noticemap);
        log.info("delete replay_message  end");
    }

    public int getNumOfUnView(Noticemap noticemap)
    {
        log.info("Query the num of unView starting...");
        int result = super.getTotalCount("getNumOfUnView", noticemap);
        log.info("Query the num of unView end");
        return result;
    }
    
    public void insertAttach(Noticemap noticemap) throws DAOException
    {
        log.info("insert Attachment starting...");
        super.insert("insertAttach", noticemap);
        try {
			try {
				FtpAttachment.upload2FTP(noticemap.getAttachurl());
			} catch (DomainServiceException e) {
				log.error("Got FTP information failed during adding.");
				e.printStackTrace();
			}
		} catch (IOException e) {
			log.error("error occured while adding attachment to FTP server!");
			e.printStackTrace();
		}
        log.info("insert Attachment end");
    }
    
    public void insertAttach(FileItem tmp, Noticemap noticemap) throws DAOException
    {
    	log.info("insert Attachment starting...");
    	super.insert("insertAttach", noticemap);
    	try {
    		try {
    			FtpAttachment.upload2FTP(tmp, noticemap.getAttachurl());
    		} catch (DomainServiceException e) {
    			log.error("Got FTP information failed during adding.");
    			e.printStackTrace();
    		}
    	} catch (IOException e) {
    		log.error("error occured while adding attachment to FTP server!");
    		e.printStackTrace();
    	}
    	log.info("insert Attachment end");
    }
    /*public void insertAttach2(FileItem tmp, Noticemap noticemap) throws DAOException
    {
    	log.info("insert Attachment starting...");
    	super.insert("insertAttachff", noticemap);
    	try {
    		try {
    			FtpAttachment.upload2FTP(tmp, noticemap.getAttachurl());
    		} catch (DomainServiceException e) {
    			log.error("Got FTP information failed during adding.");
    			e.printStackTrace();
    		}
    	} catch (IOException e) {
    		log.error("error occured while adding attachment to FTP server!");
    		e.printStackTrace();
    	}
    	log.info("insert Attachment end");
    }*/
    public void insertMessageForward(Noticemap noticemap) throws DAOException
    {
        log.info("insert MessageForward starting...");
        super.insert("insertMessageForward", noticemap);
        log.info("insert MessageForward end");
    }
    
    public void deleteAttach(Noticemap noticemap) throws DAOException
    {
        log.info("delete attachment starting...");
        super.delete("deleteAttach", noticemap);
        try {
			try {
				FtpAttachment.deleteFile(noticemap.getAttachurl());
			} catch (DomainServiceException e) {
				log.error("Got FTP information failed during deleting");
				e.printStackTrace();
			}
		} catch (IOException e) {
			log.error("error occured while deleting attachment in FTP server!");
			e.printStackTrace();
		}
        log.info("delete attachment  end");
    }
    
    private void updateOperStatusText(OperInformation operInfo)
    {
      int operstatus = operInfo.getOperstatus().intValue();
      String operStatusText = null;
      String locale_lang = RIAContext.getCurrentInstance().getRequest().getSession().getServletContext().getInitParameter("com.zte.ssb.ria.webdeploylanguage");
      if (locale_lang.equalsIgnoreCase("zh"))
      {
        if (operstatus == 0)
        {
          operStatusText = "正常";
        }
        else if (operstatus == 1)
        {
          operStatusText = "永久锁定";
        }
        else if (operstatus == 2)
        {
          operStatusText = "黑名单";
        }
        else if (operstatus == 3)
        {
          operStatusText = "禁止";
        }
        else if (operstatus == 4)
        {
          operStatusText = "永久锁定/禁止";
        }
        else if (operstatus == 5)
        {
          operStatusText = "限时锁定";
        }
        else if (operstatus == 8)
        {
          operStatusText = "限时锁定/禁止";
        }
        else if (operstatus > 9)
        {
          operStatusText = "黑名单";
        }
        else
        {
          operStatusText = String.valueOf(operstatus) + ": 未知状态";
        }

      } else if (locale_lang.equalsIgnoreCase("en"))
      {
        if (operstatus == 0)
        {
          operStatusText = "normal";
        }
        else if (operstatus == 1)
        {
          operStatusText = "lock";
        }
        else if (operstatus == 2)
        {
          operStatusText = "blacklist";
        }
        else if (operstatus == 3)
        {
          operStatusText = "forbiden";
        }
        else if (operstatus == 4)
        {
          operStatusText = "lock/forbiden";
        }
        else if (operstatus == 5)
        {
          operStatusText = "limitedLock";
        }
        else if (operstatus == 8)
        {
          operStatusText = "limitedLock/forbiden";
        }
        else if (operstatus > 9)
        {
          operStatusText = "blacklist";
        }
        else
        {
          operStatusText = String.valueOf(operstatus) + ": unknow status";
        }
      }
      operInfo.setOperStatusText(operStatusText);
    }
    
    public PageInfo adminInfoQuery(OperInformation operInfo,int start, int pageSize) throws DAOException
    {
    	log.info("page query admin_information starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryAdminInfoListCnt", operInfo)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<OperInformation> rsList = (List<OperInformation>) super.pageQuery("queryAdminInfoList",
            														   operInfo,
                                                                       start,
                                                                       fetchSize);
            Iterator<OperInformation> iterator = rsList.iterator();
            while(iterator.hasNext()) {
            	updateOperStatusText(iterator.next());
            }
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("page query admin_information end");
        return pageInfo;
    }
    
    public PageInfo operInfoQuery(OperInformation operInfo,int start, int pageSize) throws DAOException
    {
    	log.info("page query oper_information starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryOperInfoListCnt", operInfo)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
                                                          : pageSize;
            List<OperInformation> rsList = (List<OperInformation>) super.pageQuery("queryOperInfoList",
            														   operInfo,
                                                                       start,
                                                                       fetchSize);
            Iterator<OperInformation> iterator = rsList.iterator();
            while(iterator.hasNext()) {
            	updateOperStatusText(iterator.next());
            }
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.info("page query admin_information end");
        return pageInfo;
    }
    
    public void updateNoticeReplay(Noticemap notictmap) throws DAOException
    {
        log.info("update notice replay information by pk starting...");
        super.update("updateNoticeReplay", notictmap);
        log.info("update notice replay information by pk end");
    }
}
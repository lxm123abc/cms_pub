    //特殊字符过滤
	function filtrate(object){
        object.value = object.value.replace(/[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\<\>\/?]/g,'');
	}

    //判断是否包含不允许输入的字符(不包含-)
    function checkUnallowedCharacter(str){
		var pattern = /[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\.\<\>\/?]/;
		return pattern.test(str);
    }



    //判断是否包含不允许输入的字符(包含-)
    function checkUnallowedCharacter2(str){
		var pattern = /[`~!@#$%^&*()=+\\\-|\[\]\{\};:\'\",\.\<\>\/?]/;
		return pattern.test(str);
    }
    
    
    
	//日期格式转换成字符串
    function formatDBDate(str, len) {
        str = str.replace(/:/g,"");<!-- replace :     -->
        str = str.replace(/ /g,"");<!-- replace blank -->
        str = str.replace(/-/g,"");<!-- replace -     -->
        if (str == "") {
            str = "00000000000000";
        }
        return str.substr(0, len);
    }

    //字符串转换成日期格式
	function formatFromDBDate(str) {
	    var result = "";
	    str = trim(str);
	    if (str.length == 0 || str.charAt(0) == '0')
	        return result;
	        
	    if (str.length == 10 || str.length == 19)
	        return str;
	    
	    result = str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2);
	    if (str.length == 14)
	        result += " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
	        
	    return result;
	}

    //消除空格
    function trim(str){
        return ltrim(rtrim(str));
    }
    function ltrim(str){
        var sRet="";
        regular = /\s*(\S*)/; 
        sRet=str.replace(regular, "$1");
        return sRet;
    }
    function rtrim(str){
        var sRet="";
        regular = /(\S*)\s*$/; 
        sRet=str.replace(regular, "$1");
        return sRet;
    }
    
    //得到字符串的长度（汉字按3个字符计）
    function getstrlength(str){
        var l=str.length;
        var n=l;
        for(var i=0;i<l;i++){
    		    if(str.charCodeAt(i)<0 || str.charCodeAt(i)>255){
    		    	n+=2;
    		    }
    	  }
        return n;
    }
    
    //判断字符串是否由数字或字母组成
    function isNumOrChar(str){
        var pattern = /[^a-zA-Z0-9]/;
		if(pattern.test(str)){
			return false;
		}else{
			return true;
		}
    }
    
    //检查字符串是否是纯数字
    function isUnsignedInteger(strInteger) { 
        return (strInteger.search(/^\d+$/) != -1); 
    }

	function checkNull(inputStr) {
	
	    return ( Trim(inputStr).length==0);
	
	}

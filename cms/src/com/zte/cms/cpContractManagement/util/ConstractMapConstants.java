package com.zte.cms.cpContractManagement.util;


public class ConstractMapConstants {

    public static final String CPCODE_CN = "CP-code";
    public static final String CPCODE = "cpCode";

    public static final String CPNAME_CN = "CP名称";
    public static final String CPNAME = "cpName";

    public static final String CONTRACTID_CN = "合同编号";
    public static final String CONTRACTID = "contractId";


    public static final String CONTRACTNAME_CN = "合同名称";
    public static final String CONTRACTNAME= "contractName";

    public static final String BEGINDATE_CN = "合同开始时间";
    public static final String BEGINDATE = "beginDate";

    public static final String ENDDATE_CN = "合同结束时间";
    public static final String ENDDATE = "endDate";


    public static final String ENCLOSURE_CN = "附件（多个）";
    public static final String ENCLOSURE = "enclosure";


}

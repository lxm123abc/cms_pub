package com.zte.ismp.servermanage.common;

import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

public class ServerUtil
{
    public static void addLog(String optObjectType, String optObject, String optDetail)
    {
        // 获取业务日志写入对象
        AppLogInfoEntity logInfo = new AppLogInfoEntity();
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.SYSTEM_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();
        // 操作类型
        String optType = ResourceManager.getResourceText("log.server.opttype.maintain");// 服务器管理
        // 操作对象类型optObjectType
        // 操作对象optObject
        // 操作详细说明optDetail
        optDetail = optDetail.replaceAll("\\[opername\\]", operName);
        logInfo.setUserId(operId);
        logInfo.setOptType(optType);
        logInfo.setOptObjecttype(optObjectType);
        logInfo.setOptObject(optObject);
        logInfo.setOptTime("");
        logInfo.setOptDetail(optDetail);
        logInfo.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        LogInfoMgt.doLog(logInfo);
    }
}

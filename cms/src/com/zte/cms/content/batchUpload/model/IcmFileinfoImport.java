package com.zte.cms.content.batchUpload.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmFileinfoImport extends DynamicBaseObject
{
    private java.lang.Long importindex;
    private java.lang.Long contentindex;
    private java.lang.String contentid;
    private java.lang.Long fileindex;
    private java.lang.Long mapindex;
    private java.lang.Integer filetype;
    private java.lang.String filename;
    private java.lang.String srcaddresspath;
    private java.lang.String destaddresspath;

    public java.lang.Long getImportindex()
    {
        return importindex;
    }

    public void setImportindex(java.lang.Long importindex)
    {
        this.importindex = importindex;
    }

    public java.lang.Long getContentindex()
    {
        return contentindex;
    }

    public void setContentindex(java.lang.Long contentindex)
    {
        this.contentindex = contentindex;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.Long getFileindex()
    {
        return fileindex;
    }

    public void setFileindex(java.lang.Long fileindex)
    {
        this.fileindex = fileindex;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Integer getFiletype()
    {
        return filetype;
    }

    public void setFiletype(java.lang.Integer filetype)
    {
        this.filetype = filetype;
    }

    public java.lang.String getFilename()
    {
        return filename;
    }

    public void setFilename(java.lang.String filename)
    {
        this.filename = filename;
    }

    public java.lang.String getSrcaddresspath()
    {
        return srcaddresspath;
    }

    public void setSrcaddresspath(java.lang.String srcaddresspath)
    {
        this.srcaddresspath = srcaddresspath;
    }

    public java.lang.String getDestaddresspath()
    {
        return destaddresspath;
    }

    public void setDestaddresspath(java.lang.String destaddresspath)
    {
        this.destaddresspath = destaddresspath;
    }

    public void initRelation()
    {
        this.addRelation("importindex", "IMPORTINDEX");
        this.addRelation("contentindex", "CONTENTINDEX");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("fileindex", "FILEINDEX");
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("filetype", "FILETYPE");
        this.addRelation("filename", "FILENAME");
        this.addRelation("srcaddresspath", "SRCADDRESSPATH");
        this.addRelation("destaddresspath", "DESTADDRESSPATH");
    }

}

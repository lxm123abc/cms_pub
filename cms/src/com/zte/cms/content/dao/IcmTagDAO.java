package com.zte.cms.content.dao;

import java.util.ArrayList;
import java.util.List;

import com.zte.cms.content.model.IcmTag;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class IcmTagDAO extends DynamicObjectBaseDao implements IIcmTagDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmTag(IcmTag icmTag) throws DAOException
    {
        log.debug("insert icmTag starting...");
        super.insert("insertIcmTag", icmTag);
        log.debug("insert icmTag end");
    }

    public void updateIcmTag(IcmTag icmTag) throws DAOException
    {
        log.debug("update icmTag by pk starting...");
        super.update("updateIcmTag", icmTag);
        log.debug("update icmTag by pk end");
    }

    public void deleteIcmTag(IcmTag icmTag) throws DAOException
    {
        log.debug("delete icmTag by pk starting...");
        super.delete("deleteIcmTag", icmTag);
        log.debug("delete icmTag by pk end");
    }

    public IcmTag getIcmTag(IcmTag icmTag) throws DAOException
    {
        log.debug("query icmTag starting...");
        IcmTag resultObj = (IcmTag) super.queryForObject("getIcmTag", icmTag);
        log.debug("query icmTag end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<IcmTag> getIcmTagByCond(IcmTag icmTag) throws DAOException
    {
        log.debug("query icmTag by condition starting...");
        List<IcmTag> rList = (List<IcmTag>) super.queryForList("queryIcmTagListByCond", icmTag);
        log.debug("query icmTag by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmTag by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmTagListCntByCond", icmTag)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmTag> rsList = (List<IcmTag>) super.pageQuery("queryIcmTagListByCond", icmTag, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmTag by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoUnclearQuery(IcmTag icmTag, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoUnclearQuery starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryicmtagUnclearListCnt", icmTag)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmTag> rsList = (List<IcmTag>) super.pageQuery("queryicmtagUnclearList", icmTag, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoUnclearQuery end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize, PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryIcmTagListByCond", "queryIcmTagListCntByCond", icmTag, start, pageSize,
                puEntity);
    }

    @SuppressWarnings("unchecked")
    public List<IcmTag> getIcmTagList(IcmTag icmTag) throws DAOException
    {
        log.debug("getIcmTagList starting...");
        List<IcmTag> icmTagList = null;
        icmTagList = (List<IcmTag>) super.queryForList("queryicmtagList", icmTag);
        if (icmTagList == null)
        {
            icmTagList = new ArrayList<IcmTag>();
        }
        log.debug("getIcmTagList end");
        return icmTagList;
    }

}
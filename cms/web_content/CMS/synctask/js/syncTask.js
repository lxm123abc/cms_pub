// 任务监控页面--页面跳转
function toOtherPage(url,destindex)
{
  	//页面状态保存
    savePageStatus('zteTable');
	url=url+destindex;
	document.location.href=url;
}

// 任务监控页面--查询
function search()
{
	clearInputMsg('starttime_msg');
    clearInputMsg('endtime_msg');
    clearInputMsg('taskindex_msg');
    clearInputMsg('batchid_msg');
	
	if( checkindexInput('taskindex', 'taskindex_msg')==false)
    {
	    return;
    }
	if( checkindexInput('batchid', 'batchid_msg')==false)
    {
	    return;
    }
		
    var error2 = true;
    error2 = validSearchTime('starttime1','starttime2','starttime_msg');
	if(error2 == false)
	{
		return;
	}
	
	//查询时对于结束时间段的判断
    var error3 = true;
    error3 = validSearchTime('endtime1','endtime2','endtime_msg');
    if(error3 == false)
	{
	    return;
	}
		
	//判断开始时间的最小值是否小于结束时间的最大值
	error4 = validBeginAndEndTime('starttime1','endtime2','endtime_msg',"开始","结束");
	if(error4 == false)
	{
	    return;
	}
	
	searchTableData('searchSync','$M.synctask','zteTable'); 
}

// 任务监控页面--清空
function clearData()
{
    clearInputText('taskindex');
    clearInputText('batchid');
    clearSelect('status');	
    clearSelect('targettype');
    clearSelect('platform');		    
    clearInputText('starttime1');
    clearInputText('starttime2');
    clearInputText('endtime1');
    clearInputText('endtime2');    
    clearInputText('resultdesc'); 
    
    clearInputMsg('starttime_msg');
    clearInputMsg('endtime_msg');
    clearInputMsg('taskindex_msg');   
    clearInputMsg('batchid_msg'); 
}

function checkindexInput(input,input_msg)
{
 	var inputValue= document.getElementById(input).value;      	
   	if(inputValue=="")
	{
		$("#"+input_msg).text("");
		return true;
 	}
 	else
 	{
 	    var regExp = new RegExp("^[1-9]+[0-9]*$");  
 	    if(regExp.test(inputValue))
        {
            return true;
        }   
        else
        {
            $("#"+input_msg).text("请输入大于0的整数");
            return false;
        }	    
 	}
}

// 任务监控页面--任务状态转化
function getstatus(keyvalue)
{
    keyvalue['status'] = taskstatusMap[keyvalue['status']];
	return keyvalue;
}

// 任务监控页面--平台类型转换
function getplatform(keyvalue)
{
    keyvalue['platform'] = platformMap[keyvalue['platform']];
	return keyvalue;
}

//任务监控页面--工单来源类型转换
function getsource(keyvalue)	
{  
	keyvalue['source'] = sourceMap[keyvalue['source']];
	return keyvalue;
}   

// 任务监控页面--预览请求xml文件 
var urlObj = {};
function view(taskindex)
{
	if(!reqDetailRight)
	{

            openMsg("1:对不起,您没有查看请求xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	 callUrl('cntSyncTaskLSFacade/getPreviewXMLPath.ssm',taskindex,'urlObj',true);
	 var configUrl = urlObj.rtnValue;
	 
	 if(configUrl !=null)
	 {		
		 var URL = configUrl; 
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(URL, "", sFeatures);
	 }
	 else
	 {
		 alert("该文件不存在");		 
	 }	
}

//重新执行方法
function rerun(taskindex)
{
	if(!rerunRight)
	{

            openMsg("1:对不起,您没有重新执行权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	if(confirm('确认重新执行？'))
    {	index=taskindex;
		callSid('rerunSync','[index]'); 
    }
}

//批量重新执行方法
function batchrerun()
{	
	taskArray=new Array();
	var chk= document.getElementsByName("chkBox"); 
	
	for(var i =1 ;i<chk.length ; i++)
	{
		if(chk[i].checked == true){
			var obj = new Object();
			obj["taskindex"] = chk[i].taskindex;
			obj["batchid"] = chk[i].batchid;	
			taskArray.push(obj);
		}
	}
	
	if(taskArray.length==0)
	{
		openMsgBoxSize("请选择需要操作的内容");
		return;
	}
	  
	if(confirm( "确定进行批量重新执行吗?"))
	{
		savePageStatus('zteTable');	
		callSid('batchRerunSync','taskArray'); 
	}
	
}
// 任务监控页面--预览应答xml文件
function viewResult(resultfileurl)
{
	if(!rspDetailRight)
	{

            openMsg("1:对不起,您没有查看应答xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	 if( resultfileurl != '' && resultfileurl != 'null' && resultfileurl != "undefined")
	 {
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(resultfileurl, "", sFeatures);
	 } 
	 else
	 {
		 alert("该文件不存在");			 
	 }
}

//同批次任务的‘返回’按钮，回到对象日志理页面
function toTaskMainPage(flg)
{
	if(flg == 1)
		location.href = "syncObjectMain.html";
	if(flg == 0)
		location.href = "synctaskMain.html";
}

// 任务监控页面--xml文件预览链接，文件不存在时不显示该链接
var data={};
function setDataTable(table,obj)
{
	data = getTableById(obj).data;	
	noDataTrips(table,obj);
	
	$("a[name='lnkviewxml']").each(query_Reqxml);  
	$("a[name='lnkviewresultxml']").each(query_Rspxml);  
	
	$("a[name='lnkDetail']").each(query_detail);  
	$("a[name='lnkObjectDetail']").each(query_objectddetail);  
	
	$("a[name='lnkreRun']").each(query_rerun);

	$("a[name='lnksamebatchid']").each(query_samebatchid);
}
//同步对象监控页面
function setDataTable1(table,obj)
{
	data = getTableById(obj).data;	
	noDataTrips(table,obj);
	$("a[name='lnksamebatchid_obj']").each(query_samebatchid_obj);  
}
//设置重新执行权限
function query_rerun(i)
{
	if(i>0)
	{
		var req = data[i-1].status;	
		//alert(req);
		if(req != 4 || !rerunRight)//不展示重新执行连接
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//设置浏览请求xml权限
function query_Reqxml(i)
{
	if(i>0)
	{
		var req = data[i-1].contentmngxmlurl;	
		if(req == null || !reqDetailRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//设置浏览应答xml权限
function query_Rspxml(i)
{
	if(i>0)
	{
		var rsp = data[i-1].resultfileurl;
		if(rsp == null || !rspDetailRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//设置查看详情权限
function query_detail(i)
{
	if(i>0)
	{
		if(detailRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}
//对象日志页面设置查看同批次任务
function query_samebatchid_obj(i)
{
	if(i>0)
	{
		if(sameBatchidTask_obj)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}
//同步任务页面设置查看同批次任务
function query_samebatchid(i)
{
	if(i>0)
	{
		if(sameBatchidTask)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}
//设置查看对象日志权限
function query_objectddetail(i)
{
	if(i>0)
	{
		if(objectDetailRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}


// 对象监控页面--查询
function queryData()
{
	clearInputMsg('taskindex_msg');
    clearInputMsg('starttime_msg');  
    clearInputMsg('endtime_msg');     
    
    if( checkindexInput('taskindex', 'taskindex_msg')==false)
    {
	    return;
    }
   /* if( checkindexInput('batchid', 'batchid_msg')==false)
    {
	    return;
    }*/

    var error2 = true;
    error2 = validSearchTime('starttime1','starttime2','starttime_msg');
	if(error2 == false)
	{
	    return;
	}

	//查询时对于结束时间段的判断
    var error3 = true;
    error3 = validSearchTime('endtime1','endtime2','endtime_msg');
    if(error3 == false)
	{
	    return;
	}
   
	//判断开始时间的最小值是否小于结束时间的最大值
	error4 = validBeginAndEndTime('starttime1','endtime2','endtime_msg',"开始","结束");
	if(error4 == false)
	{
	    return;
	}

	searchTableData('getobjectSyncRecordList','$M.obj','zteTable');		
}

// 对象监控页面--清空
function doClear()
{
    clearSelect('objecttype');
    clearSelect('actiontype');
    clearSelect('targettype');
    clearSelect('resultcode');
    
    clearInputText('taskindex');
    clearInputText('objectid');
    clearInputText('starttime1');
    clearInputText('starttime2');  
    clearInputText('endtime1');
    clearInputText('endtime2'); 
    
    clearInputMsg('taskindex_msg');
    //clearInputMsg('batchid_msg');
    clearInputMsg('starttime_msg');  
    clearInputMsg('endtime_msg');
}

// 对象监控页面--查看任务
function toDetail(type,taskindex)
{
	if(type == 1)
		{
		if(getPurview("statistic.cnttask.monitor.task.view") !=1)
		{
	            openMsg("1:对不起,您没有同步任务查看权限",'./uiloader/forward.html',true);  //跳转到登录页面
				return;           
		}	
		savePageStatus('zteTable');	
		location.href = "synctaskDetail.html?index="+taskindex+"&rtnFlag="+type;
		}
	else{
		if(getPurview("statistic.cnttask.monitor.object.view") !=1)
		{
	            openMsg("1:对不起,您没有同步对象任务查看权限",'./uiloader/forward.html',true);  //跳转到登录页面
				return;           
		}	
		savePageStatus('zteTable');	
		location.href = "synctaskDetail_object.html?index="+taskindex+"&rtnFlag="+type;
	}

}
function toSameBatchidtTask(taskindex)
{
	savePageStatus('zteTable');	
	location.href = "view_samebatchid_synctask_object.html?isLoad=false&taskindex="+taskindex;
	
}
function toOpentSameBatchidtTask(taskindex)
{
	savePageStatus('zteTable');	
	location.href = "view_samebatchid_synctask.html?isLoad=false&taskindex="+taskindex;	
}

// 开始时间格式转换
function formatSDate(keyvalue)
{
    if(keyvalue['starttime']==null)
    {
       keyvalue['starttime']= $res_entry("no.time.data");
    }
    else
    {
    	 keyvalue['starttime'] = translateStrToDate(keyvalue['starttime']); 
    }     
    return keyvalue;
}

// 结束时间格式转换
function formatEDate(keyvalue)
{
    if(keyvalue['endtime']==null)
    {
      keyvalue['endtime']=  $res_entry("no.time.data");
    }
    else
    {
      keyvalue['endtime'] = translateStrToDate(keyvalue['endtime']);    
    }
        
    return keyvalue;
}
 
// 目标系统类型转化
function getTargettype(keyvalue) 
{
     keyvalue['targettype'] = targettypeMap[keyvalue['targettype']];
     return keyvalue;
}

// 对象监控页面--对象类型转换
function getObjecttype(keyvalue) 
{
     keyvalue['objecttype'] = objecttypeMap[keyvalue['objecttype']];
     return keyvalue;
}

// 对象监控页面--操作类型转换
function getActiontype(keyvalue) 
{
     keyvalue['actiontype'] = actiontypeMap[keyvalue['actiontype']];
     return keyvalue;
}

// 对象监控页面--操作结果转换
function getResultcode(keyvalue) 
{
	//alert("转化方法执行了！");
	if(keyvalue['resultcode'] ==0)
	{
		   keyvalue['resultcode'] = "成功";
	}
	else
	{
		if(keyvalue['resultcode'] != null)
		{					
		 keyvalue['resultcode'] = "失败";
		}
	}     
    return keyvalue;
}

//批量重新发布批量提示框展示。
function showBatchResult(rtn)
{
	   var rtnUrl = 'CMS/synctask/syncTaskMain.html?isLoad=true';
	    rtnUrl = encodeURIComponent(rtnUrl);
	    openMsgBatch(rtn,rtnUrl,true);
}
//增加权限
var detailRight = false;//查看权限
var objectDetailRight = false;//对象日志查看权限
var reqDetailRight = false;//预览请求xml权限
var rspDetailRight = false;	//预览应答xml权限
var taskDetailRight = false;	//查看任务权限

var rerunRight = false; //重新执行权限
var sameBatchidTask = false;   //查看同批次号任务权限
var sameBatchidTask_obj = false;   //查看同批次号任务,对象日志权限
function showPurview()
{

	if(getPurview("statistic.cnttask.monitor.task") !=1)
	{

            openMsg("1:对不起,您没有内容同步任务监控访问权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	if(getPurview("statistic.cnttask.monitor.task.view") ==1)detailRight = true;//查看权限

	if(getPurview("statistic.cnttask.monitor.task.objview") ==1)objectDetailRight = true;//对象日志查看权限

	if(getPurview("statistic.cnttask.monitor.task.reqview") ==1)reqDetailRight = true;//预览请求xml权限
	
	if(getPurview("statistic.cnttask.monitor.task.rspview") ==1)rspDetailRight = true;	//预览应答xml权限
	
	if(getPurview("statistic.cnttask.monitor.task.rerun") ==1)rerunRight = true;	//重新申请权限
	
	if(getPurview("statistic.cnttask.monitor.task.viewbid") ==1)sameBatchidTask = true;	//查看同批次号任务权限
	if(getPurview("statistic.cnttask.monitor.object.viewbid") ==1)sameBatchidTask_obj = true;	//查看同批次号任务,对象日志权限
	
} 

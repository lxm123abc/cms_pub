package com.zte.cms.content.CSplatformSyn.dao;

import java.util.List;

import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICSCntPlatformSyncDAO
{
     /**
	  * 新增CntPlatformSync对象 
	  * 
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */	
     public void insertCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
	  * 根据主键更新CntPlatformSync对象
	  * 
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public void updateCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException;


     /**
	  * 根据主键删除CntPlatformSync对象
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public void deleteCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException;
     
     
     /**
	  * 根据主键查询CntPlatformSync对象
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @return 满足条件的CntPlatformSync对象
	  * @throws DAOException dao异常
	  */
     public CSCntPlatformSync getCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
	  * 根据条件查询CntPlatformSync对象 
	  *
	  * @param cntPlatformSync CntPlatformSync对象
	  * @return 满足条件的CntPlatformSync对象集
	  * @throws DAOException dao异常
	  */
     public List<CSCntPlatformSync> getCntPlatformSyncByCond( CSCntPlatformSync cntPlatformSync )throws DAOException;

     /**
	  * 根据条件分页查询CntPlatformSync对象，作为查询条件的参数
	  *
	  * @param cntPlatformSync CntPlatformSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntPlatformSync对象，作为查询条件的参数
	  *
	  * @param cntPlatformSync CntPlatformSync对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     
     /**
      * 根据objecttype和objectindex删除CntPlatformSync对象
      *
      * @param cntPlatformSync CntPlatformSync对象
      * @throws DAOException dao异常
      */
     public void deleteCntPlatformSyncByObjindex( CSCntPlatformSync cntPlatformSync )throws DAOException;
        
     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync对象
      * @param cntPlatformSync
      * @return
      * @throws DAOException
      */
     public CSCntPlatformSync getCntPlatformSync4Object( CSCntPlatformSync cntPlatformSync )throws DAOException;
     
     /**
      * 根据objecttype、objectindex和platform查找CntPlatformSync的对应的子网元数目
      * @param cntPlatformSync
      * @return
      * @throws DAOException
      */
     public int getCntPlatformSyncChildNumber( CSCntPlatformSync cntPlatformSync )throws DAOException;
	 public PageInfo pageInfoQueryCha(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException;
	 public PageInfo pageInfoQuerySche(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException;
	 public PageInfo pageInfoQueryCach(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException;
}
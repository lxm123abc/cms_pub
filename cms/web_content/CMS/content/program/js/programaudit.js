
 function openFile(fullName)
	{
	   // var fullName = "F:\\1318406612248_vitas_opera1.avi";
		var inpoint = 0;
		var outpoint = 0;
		Msshow2.WorkMode = false;
		Msshow2.OpenMode = true; //after open, play directly
		Msshow2.WorkType = 1;
		Msshow2.VideoStandard = 1;
		Msshow2.ShowMode = 3; //support full screen and 1:1 
		Msshow2.Open(fullName, inpoint, outpoint);
		//以上代码用于大洋播放器下面的用于QuickTime播放器
        // document.movie.SetURL(fullName);    
	}
	
	function setPicAdjust(tableId)
	{
		//由于要与上面的表格对其，故先获取上面表格的宽度
		var $tableWidth=$("#"+tableId).width();
		var $picBoxWidth=$tableWidth-($tableWidth*0.2);//减去20%的右边单元的说明
		$picBoxWidth=($picBoxWidth-10);//计算图片最大宽度要求，减去10个像素(div中有4个像素是空白像素，减去10是为了页面各栏保证对齐)
		//$("div.imgOverClass").css("width",$picBoxWidth);
		//遍历每个图片
		$("div.imgOverClass image" ).each(function(){	
			var $picWidth=$(this).width();//计算图片的宽度
			var $picHeigth=$(this).height();//计算图片的高度
			//如果图片的宽度大于最大宽度要求，则进行If语句
			if(($picWidth)>=($picBoxWidth))
			{
				//计算压缩的比例
				var $zipPara=($picBoxWidth)/($picWidth);
				//把最大宽度要求赋值给图片的width属性
				$picWidth=$picBoxWidth;
				//高度进行同比例压缩
				$picHeigth=($picHeigth)*($zipPara);
				//取整数部分
				$picHeigth=Math.round($picHeigth);
				//把属性赋值给图片
				$(this).attr("width",$picWidth);
				$(this).attr("height",$picHeigth);
			}
		});
		
	}
	
	var dd=0;
	function showimagelist(imagelist){
		dd = 0;
		if(imagelist!=""){
			for(var index = 0; index < imagelist.length; index++){
				makeimageitem("imagebase");
				if(imagelist[index]["pictureurl"] !=null){
				    $("#lblprop02"+index).text(imagelist[index]["pictureurl"]);
				    $("#tdaddresspath"+index).attr("src",imagelist[index]["pictureurl"]);
				}
			}
			//图片自适应
		setPicAdjust("basicinfoID");  
		}
		callSid("getMovieinfoByCond",programid); 
	}
	
	function makeimageitem(table) {
		//prop02  图片分辨率
		//addresspath 地址
		//playnums 1:JPG 2:GIF 3:PNG
		var tbody = document.getElementById(table);
		newTr=tbody.insertRow();
		var th=document.createElement('td');
		th.innerHTML ='<div class = "notsubtitle"><label  id="lblprop02'+dd+'"></label></div>';
		newTr.appendChild(th);
		newTr1=tbody.insertRow();
		var td=document.createElement('td');
		td.setAttribute("width","100%");
		td.innerHTML='<td id="tdimage>'+dd+'"></td>'
		            +'<div class="imgOverClasses"><image id="tdaddresspath'+dd+'"/></div>';
		newTr1.appendChild(td); 
		dd++;
    }

	function makefilevideo(){
		//encodingformat    编码格式（H264、Mpeg4等）
		//resolution        视频分辨率
		//bitrate           媒体码流大小（50K、150K、10M等） 
		var filevideodiv=document.getElementById("filevideoinfo");
		var div=document.createElement('div');
		div.innerHTML='<div id="div'+cc+'">'
					+'<label id="lblprogramname'+cc+'" style="display:none"></label>'
					+'<label class="title">'+$res_entry("label.cms.channel.info.bitratetype")+':&nbsp;&nbsp;</label>'
					+'<label id="lblbitrate'+cc+'"></label>'
					+'<br/>'
					+'<label class="title">'+$res_entry("msg.info.content.199")+'&nbsp;&nbsp;</label>'
					+'<label id="lblencodingformat'+cc+'"></label>'
					+'<label id="lblfileindex'+cc+'" style="display:none"></label>'
					+'<br/>'
					+'<label class="title">'+$res_entry("msg.info.content.200")+'&nbsp;&nbsp;</label>'
					+'<label id="lblresolution'+cc+'"></label>'
					+'<label id="lbladdresspath'+cc+'" style="display:none"></label>'
					+'<br/>'
					+'<input type="button" id="btn'+cc+'" value="' + $res_entry("msg.info.content.135") + '" onclick="change(this)" class="inputButton"/>'
					+'&nbsp;&nbsp;&nbsp;<input type="button" id="btn1'+cc+'" value="' + $res_entry("label.videoaudioinfo") + '"  onclick="showVideoAudio(this)" class="inputButton"/>'
					+'</div><br>'
		filevideodiv.appendChild(div);
		cc++;
	}
	
	var cc=0;
	var bitrateMap = {};
	var videotypeMap = {};
	var resolutionMap = {};
	function showfilevideolist(videolist){
		cc=0;
		if(videolist!=""){
	       videolistlength = videolist.length;	
			for(var i =0;i<videolist.length;i++){
				makefilevideo();
				//if(videolist[i]["programname"]!=null){
				//	$("#lblprogramname"+i).text(videolist[i]["programname"]);
				//}
				if(videolist[i]["bitratetype"]!=null){
					 callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_movie_bitratetype", ' + videolist[i]["bitratetype"] + ']', 'bitrateMap', true);
					 $("#lblbitrate"+i).text(bitrateMap.rtnValue); 
					//$("#lblbitrate"+i).text(videolist[i]["bitratetype"]);
				}
				if(videolist[i]["movieindex"]!=null){
					$("#lblfileindex"+i).text(videolist[i]["movieindex"]);
				}
				if(videolist[i]["videotype"]!=null){
			        callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_movie_videotype", ' + videolist[i]["videotype"] + ']', 'videotypeMap', true);
					$("#lblencodingformat"+i).text(videotypeMap.rtnValue);
				}
				if(videolist[i]["resolution"]!=null){
					callUrl('sysCode/getSysCodeValueByPK.ssm', '["cms_movie_resolution", ' + videolist[i]["resolution"] + ']', 'resolutionMap', true);
					$("#lblresolution"+i).text(resolutionMap.rtnValue);
				}
				if(videolist[i]["fileurl"]!=null){					
					$("#lbladdresspath"+i).text(videolist[i]["fileurl"]);
				}
			}
		}
	}
	
	  function txtResultFunc(){
         var txtRadioValue = "";
         var flag = _para.flag;
         if(flag == 1 || flag == 3){
           txtRadioValue = SSB_RadioGroup.radioGroupObjs["txtResult"].getValue();
         }
         if(flag == 2){
           txtRadioValue = SSB_RadioGroup.radioGroupObjs["txtResult2"].getValue();
         }
           
	     if(txtRadioValue == 0){
	         $("#txtopopinion_msg").text("");
	          document.getElementById("reasonSpan").innerHTML = $res_entry("msg.info.content.195");
	         document.getElementById("nopassReason").style.display="block";
	         document.getElementById("oppniontr").style.display="block";
	         nopassReasonFunc();
	     }else if(txtRadioValue == 1){
	           document.getElementById("oppniontr").style.display="none";
	           $("#txtopopinion_msg").text("");
	           document.getElementById("nopassReason").style.display="none";
	           document.getElementById("opopinion").disabled="";
	           document.getElementById("opopinion").value="";
	     }else if(txtRadioValue == 2){
	           $("#txtopopinion_msg").text("");
	            document.getElementById("reasonSpan").innerHTML = $res_entry("msg.info.content.196");
	           document.getElementById("nopassReason").style.display="block";
	           document.getElementById("oppniontr").style.display="block";
	           nopassReasonFunc();
	     }
   }
   
      function nopassReasonFunc(){
          var txtReasonValue = SSB_RadioGroup.radioGroupObjs["txtReason"].getValue();
	     if(txtReasonValue==1){
	     $("#txtopopinion_msg").text("");
	           document.getElementById("opopinion").value="";
	           document.getElementById("opopinion").disabled="";
	           document.getElementById("opopinion").value=$res_entry("msg.info.content.005");
	          document.getElementById("opopinion").disabled="disabled";
	         
	     }else if(txtReasonValue==2){
	       $("#txtopopinion_msg").text("");
	           document.getElementById("opopinion").value="";
	            document.getElementById("opopinion").disabled="";
	          document.getElementById("opopinion").value=$res_entry("msg.info.content.006");
	          document.getElementById("opopinion").disabled="disabled";
	     }else if(txtReasonValue==3){
	       $("#txtopopinion_msg").text("");
	          document.getElementById("opopinion").value="";
	          document.getElementById("opopinion").disabled="";
	     }
   }

    var previewpath = {};
	function change(btn){
		if(btn.value==$res_entry("msg.info.content.135")){
			btn.value=$res_entry("msg.info.content.015");
			var len=btn.id.length;
			var btnid=btn.id;
			var c=btnid.charAt(len-1);	
			var movieindex = document.getElementById("lblfileindex"+c).innerText;
			previewpath = {};
			callUrl('cmsMovieLSFacade/getPreviewPath.ssm', '[' + movieindex + ']', 'previewpath', true); 
			previewpath = previewpath.rtnValue;
			openFile(previewpath);
		    //$("#mov123").html(QT_WriteOBJECT1(addresspath, '480', '360', '','autostart','True','cache','True','EnableJavaScript', 'True', 'emb#NAME' , 'movie1' , 'obj#id' , 'mov') );
		    document.getElementById("vedioAddress").innerText = previewpath;
		}else{
			btn.value=$res_entry("msg.info.content.135");
			document.getElementById("vedioAddress").innerText="";
			Msshow2.Close();//用于大洋播放器
		    //document.movie.SetURL("uiloader/images/quicktimeLogo.bmp");
		    //$("#mov123").html(QT_WriteOBJECT1("", '480', '360', '','autostart','True','cache','True','EnableJavaScript', 'True', 'emb#NAME' , 'movie1' , 'obj#id' , 'mov') );
		}
	}
	
	//查看视音频信息
	function showVideoAudio(btn)
    {
        var len = btn.id.length;
	    var btnid = btn.id;
		var c = btnid.charAt(len-1);	
        var fileindex = document.getElementById("lblfileindex"+c).innerText;
		var sourcetype = 3;      //视频文件来源：子内容

 		document.getElementById("movieindex").value=fileindex;
 		callSid('getVAInfo','['+ fileindex+','+ sourcetype +']');
    }   
	
	var opopinion="";
	var wkfwParam = [];  //单条审核  wkfwParam工作流参数：内容ID、工单任务号、工单流程号、工单节点名称、审核意见、审核结果
	function apply(){
		clearAllMsgWide();
		opopinion=$("#opopinion").val();
	    
	    wkfwParam = [];
	    wkfwParam[0] = _para.contentid;   //内容ID
		wkfwParam[1] = _para.taskid;      //工单任务号
		wkfwParam[2] = _para.processId;   //工单流程号
		wkfwParam[3] = _para.nodeName;    //工单节点名称
		
		var result = "";
		 var flag = _para.flag;
         if(flag == 1 || flag == 3){
           result = SSB_RadioGroup.radioGroupObjs["txtResult"].getValue();
         }
         if(flag == 2){
           result = SSB_RadioGroup.radioGroupObjs["txtResult2"].getValue();
         }
		if(result == 0){
			if(""==opopinion.trim()){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.133"));
				focusAndSelect("opopinion",true);
				return false;
			}
			if(!checkUnallowedcharWide("opopinion",true)){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.134"));
				focusAndSelect("opopinion",true);
				return false;
			}
			if (confirm($res_entry("msg.info.content.003"))){
			       wkfwParam[4] = opopinion;
			       wkfwParam[5] = $res_entry("label.WorkFlow.Fail");
                   callSid('auditcontent',wkfwParam);
            }else{
                return false;
            }
		}else if(result == 1){
			if (confirm($res_entry("msg.info.content.004"))){
			       wkfwParam[4] = $res_entry("label.WorkFlow.Pass");
			       wkfwParam[5] = $res_entry("label.WorkFlow.Pass");
                   callSid('auditcontent',wkfwParam);
            }else{
                return false;
            }
		}else if(result == 2){
		     if(""==opopinion.trim()){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.133"));
				focusAndSelect("opopinion",true);
				return false;
			 }
			if(!checkUnallowedcharWide("opopinion",true)){
				$("#txtopopinion_msg").text($res_entry("msg.info.content.134"));
				focusAndSelect("opopinion",true);
				return false;
			}
		    if (confirm($res_entry("msg.info.content.197"))){
			       wkfwParam[4] = opopinion;
			       wkfwParam[5] = $res_entry("msg.info.content.198");
                   callSid('auditcontent',wkfwParam);
            }else{
                return false;
            }
		}
	}
	
	
    function toBackPrePage() {
        document.getElementById("opopinion").value="";
    	var flag=_para.flag;
    	if(flag==1)
    	{
			window.location.href="program_wkfw.html?isLoad=true&flag=1";
		}
		else if(flag==2)
		{
			window.location.href="program_wkfw.html?isLoad=true&flag=2";
		}
		else if(flag==3)
		{
			window.location.href="program_wkfw.html?isLoad=true&flag=3";
		}
	}	
		
	/**
	 * 在做提交前清空所有的提示信息
	 */
	function clearAllMsgWide(){
		$("label[@id*='_msg']").text("");
	}
	
	/**
	 * 特殊字符
	 * @param {String} input_id
	 * @param (boolean) bool
	 * unallowed character, and returns false;
	 */
	function checkUnallowedcharWide(input_id,bool){
		var pattern = /[\\|\'\"\<\>]/;
		if(pattern.test($('#'+input_id).val())){
			focusAndSelect(input_id,bool);
			return false;
		}else{
			return true;	
		}
	}
	
	/**
	 * 选中当前错误数据
	 * @param {String} input_id
	 * @param (boolean) bool
	 * Select the current error data 
	 */
	function focusAndSelect(input_id,bool){
		bool?$('#'+input_id).one('click',function(){$(this).focus();$(this).select();}):0;
		$('#'+input_id).click()
	}
    
	function clearData(){
		clearAllMsgWide();
	}
	
   function onErrorTrips(){
      // var flag=_para.flag;
	  // if(_para.batchFlag=="yes"){
           openMsg("1:"+$res_entry("msg.info.content.007"),escape("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag),true);
      // }else if(_para.batchFlag=="no"){
      //     openMsg("1:工作流异常,请稍后重试",escape("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag),true);
      // }
   }
   
   
   
    
function showVAInfo(vainfo)
{   
    $("#mov123").show();
    if(null != vainfo && typeof(vainfo) != "undefined" && typeof(vainfo.streammediatype) !="undefined")
    {  
        if($("#mediaDiv").css("display")=="none"){
          $("#mediaDiv").show();
        }
         formatData(vainfo);        
    }
    else
    {
        var errorback = false;
        if (typeof(vainfo) != "undefined") {
            vainfo = vainfo + ":" + $res_entry("msg.info.status.17");
        }else if(vainfo.indexOf('0:')==0){
			errorback = true;
		}
		$("#mediaDiv").hide();
        openMsg(vainfo, "", errorback);
    }
}

function dealErrorResult(errorCode, rtn_expDesc)
{alert("dealErrorResult:" + "rtn_expDesc=" + rtn_expDesc);
	var flag = _para.flag;
	var rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=false&flag="+flag);
	openMsg("1:" + rtn_expDesc, rtnUrl, true)
}

function showResult(returnInfo) 
    {
    	var flag = _para.flag;
    	var rtnUrl = "";
    	if (_para.batchFlag=="yes")
    	{
    		rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=true&flag="+flag);
        }
    	else
    	{
    		rtnUrl = encodeURIComponent("CMS/content/program/program_wkfw.html?isLoad=true&flag="+flag);
    	} 

	    var rtnInfo = eval(returnInfo);
        var rtnMain = rtnInfo.main;
        openMsg(rtnMain, rtnUrl, true);
    }

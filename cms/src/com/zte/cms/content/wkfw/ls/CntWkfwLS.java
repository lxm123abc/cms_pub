package com.zte.cms.content.wkfw.ls;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.content.wkfw.common.IcmContentWkfwConstants;
import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.service.ICntWkfwDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;

/**
 * <p>
 * 文件名称: CntWkfwLS.java
 * </p>
 * <p>
 * 文件描述: 内容审核查询类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供内容审核查询方法
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntWkfwLS implements ICntWkfwLS
{
    private Log log = SSBBus.getLog(getClass());

    private ICntWkfwDS cntWkfwDS = null;

    // 将查询条件进行特殊字符转换
    private void setCntWkfwCondition(CntWkfwCondition cntWkfwCondition)
    {
        if (null != cntWkfwCondition.getContentid() && !("").equals(cntWkfwCondition.getContentid()))
        {
            cntWkfwCondition.setContentid(EspecialCharMgt.conversion(cntWkfwCondition.getContentid()));
        }
        if (null != cntWkfwCondition.getContentname() && !("").equals(cntWkfwCondition.getContentname()))
        {
            cntWkfwCondition.setContentname(EspecialCharMgt.conversion(cntWkfwCondition.getContentname()));
        }
        if (null != cntWkfwCondition.getCpid() && !("").equals(cntWkfwCondition.getCpid()))
        {
            cntWkfwCondition.setCpid(EspecialCharMgt.conversion(cntWkfwCondition.getCpid()));
        }
        if (null != cntWkfwCondition.getCpcnshortname() && !("").equals(cntWkfwCondition.getCpcnshortname()))
        {
            cntWkfwCondition.setCpcnshortname(EspecialCharMgt.conversion(cntWkfwCondition.getCpcnshortname()));
        }
    }

    // 查询条件中必须增加业务键
    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("getMyNeedHandleWorkflowItemList start...");
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        log.debug("getMyNeedHandleWorkflowItemList end");
        return this.getMyNeedHandleWorkflowItemList(cntWkfwCondition, start, pageSize, puEntity);
    }

    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = null;
        if (loginUser != null)
        {
            executorId = loginUser.getUserId();
        }
        // 获得当前用户ID
        OperInfo operInfo = LoginMgt.getOperInfo(null);
        cntWkfwCondition.setOperid(Long.parseLong(operInfo.getOperid()));
        cntWkfwCondition.setExecutorId(executorId);
        cntWkfwCondition.setTaskStatus("N");
        setCntWkfwCondition(cntWkfwCondition);
        
        if (cntWkfwCondition.getNodeName().equals("一审"))
        {
            cntWkfwCondition.setAduitflag(1);
        }else if(cntWkfwCondition.getNodeName().equals("二审"))
        {
            cntWkfwCondition.setAduitflag(2);
        }else if(cntWkfwCondition.getNodeName().equals("三审"))
        {
            cntWkfwCondition.setAduitflag(3);
        }

        TableDataInfo result = this.cntWkfwDS.getMyNeedHandleWorkflowItemList(cntWkfwCondition, start, pageSize,
                puEntity);
        return result;
    }

    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntWkfwCondition) throws DomainServiceException
    {
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = null;
        if (loginUser != null)
        {
            executorId = loginUser.getUserId();
        }
        // 获得当前用户ID
        OperInfo operInfo = LoginMgt.getOperInfo(null);
        cntWkfwCondition.setOperid(Long.parseLong(operInfo.getOperid()));
        cntWkfwCondition.setExecutorId(executorId);
        cntWkfwCondition.setTaskStatus("N");
        setCntWkfwCondition(cntWkfwCondition);
        
        if (cntWkfwCondition.getNodeName().equals("一审"))
        {
            cntWkfwCondition.setAduitflag(1);
        }else if(cntWkfwCondition.getNodeName().equals("二审"))
        {
            cntWkfwCondition.setAduitflag(2);
        }else if(cntWkfwCondition.getNodeName().equals("三审"))
        {
            cntWkfwCondition.setAduitflag(3);
        }

        long result = this.cntWkfwDS.getMyNeedHandleWorkflowItemListCnt(cntWkfwCondition);
        return result;
    }

    /**
     * 内容审核失败信息
     */
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwis, int start, int pageSize) throws DomainServiceException
    {
        log.debug("queryCmsProgramWkfwhisListByCond LS begin....");
        TableDataInfo datainfo = null;
        String cpid = "";
        try
        {
            if (null != cntWkfwis.getCpid() && !cntWkfwis.getCpid().equals(""))
            {
                cpid = EspecialCharMgt.conversion(cntWkfwis.getCpid());
                cntWkfwis.setCpid(cpid);
            }
            if (null != cntWkfwis.getCpcnshortname() && !cntWkfwis.getCpcnshortname().equals(""))
            {
                String Cpcnshortname = EspecialCharMgt.conversion(cntWkfwis.getCpcnshortname());
                cntWkfwis.setCpcnshortname(Cpcnshortname);
            }
            if (null != cntWkfwis.getProgramid() && !cntWkfwis.getProgramid().equals(""))
            {
                String programid = EspecialCharMgt.conversion(cntWkfwis.getProgramid());
                cntWkfwis.setProgramid(programid);
            }
            if (null != cntWkfwis.getNamecn() && !cntWkfwis.getNamecn().equals(""))
            {
                String Namecn = EspecialCharMgt.conversion(cntWkfwis.getNamecn());
                cntWkfwis.setNamecn(Namecn);
            }
            
            // 获得当前用户ID
            UserInfo loginUser = LoginMgt.getUserInfo();
            String executorId = loginUser.getUserId();
            if ("super".equals(executorId))
            {
                executorId = null;
            }
            
            // 获得当前用户ID
            OperInfo operInfo = LoginMgt.getOperInfo(null);
            cntWkfwis.setOperid(operInfo.getOperid());
            
            List<String> faillist = new ArrayList<String>();
            faillist.add(ResourceMgt.findDefaultText("cnt.not.pass"));
            faillist.add(ResourceMgt.findDefaultText("cnt.to.recycle"));
            cntWkfwis.setFaillist(faillist);
            PageUtilEntity puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("pro.historyindex");
            datainfo = cntWkfwDS.pageInfoQuery(cntWkfwis, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsProgramWkfwhisListByCond LS error.", e);
        }
        log.debug("queryCmsProgramWkfwhisListByCond end ...");
        return datainfo;
    }

    /**
     * 内容审核失败信息
     */
    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis) throws DomainServiceException
    {
        log.debug("queryCmsProgramWkfwhisListByCond LS begin....");
        TableDataInfo datainfo = null;
        String cpid = "";
        long result = 0;
        try
        {
            
            if (null != cntWkfwis.getCpid() && !cntWkfwis.getCpid().equals(""))
            {
                cpid = EspecialCharMgt.conversion(cntWkfwis.getCpid());
                cntWkfwis.setCpid(cpid);
            }
            if (null != cntWkfwis.getCpcnshortname() && !cntWkfwis.getCpcnshortname().equals(""))
            {
                String Cpcnshortname = EspecialCharMgt.conversion(cntWkfwis.getCpcnshortname());
                cntWkfwis.setCpcnshortname(Cpcnshortname);
            }
            if (null != cntWkfwis.getProgramid() && !cntWkfwis.getProgramid().equals(""))
            {
                String programid = EspecialCharMgt.conversion(cntWkfwis.getProgramid());
                cntWkfwis.setProgramid(programid);
            }
            if (null != cntWkfwis.getNamecn() && !cntWkfwis.getNamecn().equals(""))
            {
                String Namecn = EspecialCharMgt.conversion(cntWkfwis.getNamecn());
                cntWkfwis.setNamecn(Namecn);
            }
            
            // 获得当前用户ID
            UserInfo loginUser = LoginMgt.getUserInfo();
            String executorId = loginUser.getUserId();
            if ("super".equals(executorId))
            {
                executorId = null;
            }
            
            // 获得当前用户ID
            OperInfo operInfo = LoginMgt.getOperInfo(null);
            cntWkfwis.setOperid(operInfo.getOperid());
            
            List<String> faillist = new ArrayList<String>();
            faillist.add(ResourceMgt.findDefaultText("cnt.not.pass"));
            faillist.add(ResourceMgt.findDefaultText("cnt.to.recycle"));
            cntWkfwis.setFaillist(faillist);
            PageUtilEntity puEntity = new PageUtilEntity();
            puEntity.setIsAlwaysSearchSize(true);
            puEntity.setIsAsc(false);
            puEntity.setOrderByColumn("pro.historyindex");
            result = cntWkfwDS.getNopassWorkflowCnt(cntWkfwis);
        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsProgramWkfwhisListByCond LS error.", e);
        }
        log.debug("queryCmsProgramWkfwhisListByCond end ...");
        return result;
    }

    /**
     * 内容工单历史查询
     * 
     * @param cntWkfwis
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getMyHaveHandleWorkflowUcdn(CntWkfwhis cntWkfwis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("getMyHaveHandleWorkflow method of CntWkfwhis class begin ...");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = null;
        if (loginUser != null)
        {
            executorId = loginUser.getUserId();
        }
        
        if ("super".equals(executorId))
        {
            executorId = null;
        }
        cntWkfwis.setOpername(executorId);
        
        // 获得当前用户ID
        OperInfo operInfo = LoginMgt.getOperInfo(null);
        cntWkfwis.setOperid(operInfo.getOperid());
        
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        cntWkfwis.setOrderCond("pro.historyindex desc");
        cntWkfwis.setTaskindex(0L);

        // 先进行时间格式的转换
        String starttime = cntWkfwis.getProcessStartTimeStart();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            cntWkfwis.setProcessStartTimeStart(starttime1);
        }
        String starttime2 = cntWkfwis.getProcessStartTimeEnd();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            cntWkfwis.setProcessStartTimeEnd(starttime3);
        }
        if (null != cntWkfwis.getCpid() && !cntWkfwis.getCpid().equals(""))
        {
            String cpid = EspecialCharMgt.conversion(cntWkfwis.getCpid());
            cntWkfwis.setCpid(cpid);
        }
        if (null != cntWkfwis.getCpcnshortname() && !cntWkfwis.getCpcnshortname().equals(""))
        {
            String Cpcnshortname = EspecialCharMgt.conversion(cntWkfwis.getCpcnshortname());
            cntWkfwis.setCpcnshortname(Cpcnshortname);
        }
        if (null != cntWkfwis.getProgramid() && !cntWkfwis.getProgramid().equals(""))
        {
            String programid = EspecialCharMgt.conversion(cntWkfwis.getProgramid());
            cntWkfwis.setProgramid(programid);
        }
        if (null != cntWkfwis.getNamecn() && !cntWkfwis.getNamecn().equals(""))
        {
            String Namecn = EspecialCharMgt.conversion(cntWkfwis.getNamecn());
            cntWkfwis.setNamecn(Namecn);
        }
        log.debug("getMyHaveHandleWorkflow method of CntWkfwhis class end ...");
        return cntWkfwDS.pageInfoQuery(cntWkfwis, start, pageSize, puEntity);
    }

    /**
     * 内容流程跟踪查询
     * 
     * @param cntWkfwis
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getWorkflowTrack(CntWkfwhis cntWkfwis, int start, int pageSize) throws DomainServiceException
    {
        log.debug("getWorkflowTrack method of CntWkfwhis class begin ...");
        cntWkfwis.setOrderCond("pro.historyindex");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = null;
        if (loginUser != null)
        {
            executorId = loginUser.getUserId();
        }

        if ("super".equals(executorId))
        {
            executorId = null;
        }
        cntWkfwis.setOpername(executorId);
        TableDataInfo datainfo = cntWkfwDS.pageInfoQuery(cntWkfwis, start, pageSize);
        log.debug("getWorkflowTrack method of CntWkfwhis class end ...");
        return datainfo;
    }

    /**
     * 内容审核历史信息查看
     */
    public CntWkfwhis getCmsProgramWkfwhis(long historyindex) throws DomainServiceException
    {
        log.debug("getCmsProgramWkfwhis CntWkfwhis begin...");
        CntWkfwhis cmsProgramWkfwhis = new CntWkfwhis();
        cmsProgramWkfwhis.setHistoryindex(historyindex);
        cmsProgramWkfwhis = cntWkfwDS.getCmsProgramWkfwhis(cmsProgramWkfwhis);
        log.debug("getCmsProgramWkfwhis CntWkfwhis end...");
        return cmsProgramWkfwhis;
    }

    public void setCntWkfwDS(ICntWkfwDS cntWkfwDS)
    {
        this.cntWkfwDS = cntWkfwDS;
    }

}

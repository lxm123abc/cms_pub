package com.zte.cms.cmsframe.service;

import java.util.List;
import com.zte.cms.cmsframe.model.CmsFrame;
import com.zte.cms.cmsframe.dao.ICmsFrameDAO;
import com.zte.cms.cmsframe.service.ICmsFrameDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsFrameDS extends DynamicObjectBaseDS implements ICmsFrameDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsFrameDAO dao = null;

    public void setDao(ICmsFrameDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("insert cmsFrame starting...");
        Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_frame_index");
        cmsFrame.setFrameindex(id);
        try
        {
            dao.insertCmsFrame(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsFrame end");
    }

    public void updateCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("update cmsFrame by pk starting...");
        try
        {
            dao.updateCmsFrame(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFrame by pk end");
    }

    public void updateCmsFrameList(List<CmsFrame> cmsFrameList) throws DomainServiceException
    {
        log.debug("update cmsFrameList by pk starting...");
        if (null == cmsFrameList || cmsFrameList.size() == 0)
        {
            log.debug("there is no datas in cmsFrameList");
            return;
        }
        try
        {
            for (CmsFrame cmsFrame : cmsFrameList)
            {
                dao.updateCmsFrame(cmsFrame);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsFrameList by pk end");
    }

    public void removeCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("remove cmsFrame by pk starting...");
        try
        {
            dao.deleteCmsFrame(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFrame by pk end");
    }

    public void removeCmsFrameList(List<CmsFrame> cmsFrameList) throws DomainServiceException
    {
        log.debug("remove cmsFrameList by pk starting...");
        if (null == cmsFrameList || cmsFrameList.size() == 0)
        {
            log.debug("there is no datas in cmsFrameList");
            return;
        }
        try
        {
            for (CmsFrame cmsFrame : cmsFrameList)
            {
                dao.deleteCmsFrame(cmsFrame);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsFrameList by pk end");
    }

    public CmsFrame getCmsFrame(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by pk starting...");
        CmsFrame rsObj = null;
        try
        {
            rsObj = dao.getCmsFrame(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrameList by pk end");
        return rsObj;
    }

    public List<CmsFrame> getCmsFrameByCond(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by condition starting...");
        List<CmsFrame> rsList = null;
        try
        {
            rsList = dao.getCmsFrameByCond(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrame by condition end");
        return rsList;
    }
    
    public List<CmsFrame> getCmsFrameByName(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by condition starting...");
        List<CmsFrame> rsList = null;
        try
        {
            rsList = dao.getCmsFrameByName(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrame by condition end");
        return rsList;
    }
    
    public List<CmsFrame> queryCmsFrameListCntByNameUpdate(CmsFrame cmsFrame) throws DomainServiceException
    {
        log.debug("get cmsFrame by condition starting...");
        List<CmsFrame> rsList = null;
        try
        {
            rsList = dao.queryCmsFrameListCntByNameUpdate(cmsFrame);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsFrame by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsFrame page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsFrame, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsFrame>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsFrame page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsFrame page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsFrame, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsFrame>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsFrame page info by condition end");
        return tableInfo;
    }
}

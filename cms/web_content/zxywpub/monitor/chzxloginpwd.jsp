<%@page language="java" contentType="text/html"%>
<!-- JSP programs -->
<%
response.addHeader("Cache-Control", "no-cache");
response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<jsp:useBean id="db" class="com.zte.zxywpub.smapadm" scope="page" />
	
		
<% 
com.zte.zxywpub.Config config1 = new com.zte.zxywpub.Config("");

String Remoteoper=(String)session.getAttribute("OPERNAME");
String servletPath = request.getServletPath();
String rootPath = (servletPath.split("zxywpub"))[0];
if(Remoteoper==null){
  %><script language="javascript1.2">
    alert("Please login first");
	top.location.href = ' <%=rootPath%>' + "zxywpub";
  </script><%
    return;
}
  String strMsg="";	
  String Operpwd ="";
  int cmdint=0;
  if(request.getParameter("cmd")!=null){
   cmdint=java.lang.Integer.parseInt(request.getParameter("cmd"));
  }
  if(cmdint==2){
      Operpwd =request.getParameter("operpwd").trim();
      if(config1.changezxloginpass(Operpwd)){
        strMsg="<font color='#018BD8'> Password has been changed successfully.! </font>";
      }else{ 
        strMsg="<font color='#FF0000'> Failed changed pass! </font>";
      }
%>
		<script>
				alert("<%=Operpwd%>");
				alert("<%=strMsg%>");
		</script>
<%
  }
%>
<!-- JSP programs end -->
<HTML>
<HEAD>
<TITLE>SMP--Home Page</TITLE>
<link REL="STYLESHEET" HREF="../inc/style.css" TYPE="text/css">
<!-- Head javascripts -->
<!-- Head javascripts end -->
</HEAD>
<BODY topmargin="0" leftmargin="0" style="background-color: rgb(249,250,244)">
<!-- Navigator bar -->
<TABLE border="0" cellSpacing="0" cellPadding="2" width="100%" height="5%">
  <TR bgColor="#DAF0FA" style="font-size: 9pt">
    <TD><font color=red>CurrentLocation:</font><font color=#018BD8> Change zxlogin Password</font></TD>
    <td align=right> </td>
  </TR>
</TABLE>
<!-- Navigator bar end -->
<table border="0" cellPadding="5" cellSpacing="1" width="100%" height="95%" bgcolor="#C0C0C0">
   <tr height="95%">
      <td vAlign="top" bgcolor="#ffffff">
<!-- Main display info -->
<form name="inForm" method="POST" action="chzxloginpwd.jsp" onSubmit="return checkInput()">
  <p align="center">
     <span style="font-size: 11pt; font-style: normal;">
        Change zxinlogin Password
     </span>
  </p>
  <hr size="1">
  <blockquote>
  <table border="0">
    <tr>
      <td>New Password</td>
      <td><input type="password" name="passwd1" size="10" maxlength="8" value=""></td>
    </tr>
    <tr>
      <td>New Password Confirmation</td>
      <td><input type="password" name="passwd2" size="10" maxlength="8" value=""></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" value="Modify" name="Mod">  <input type="hidden" name="cmd"><input type="hidden" value="" name="operpwd"></td>
    </tr>
  </table>
</blockquote>
</form>
<!-- Main display info end -->
      </td>
   </tr>
<!-- Messages -->
   <tr height="5%"><td bgcolor="#f4f9fa" align="right">
<%	if(cmdint>0){
      out.println(strMsg);
 } %>
   </td></tr>
<!-- Messages end -->
</table>
<!-- Body javascripts -->
<script language="JavaScript1.2">

function checkInput(){
  if(document.inForm.passwd1.value=="zxin"||document.inForm.passwd1.value=="zxin10"||document.inForm.passwd1.value=="zte"){
    alert("Please select another one as your password.");
     return false;
  }
  if(document.inForm.passwd2.value.length<4){
    alert("Password must have 4 digits at least.");
    document.inForm.passwd2.focus();
    return false;
 }

    if(document.inForm.passwd1.value!=document.inForm.passwd2.value){
      alert("New Password is not same as the New Password Confirmation.");
      document.inForm.passwd1.value="";
      document.inForm.passwd2.value="";
      document.inForm.passwd1.focus();
    }else{    	
      document.inForm.cmd.value="2";
      document.inForm.operpwd.value=document.inForm.passwd1.value;
      return true
    }

  return false;
}

</script>
<!-- Body javascripts end -->
</BODY>
</HTML>
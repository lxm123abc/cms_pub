package com.zte.cms.epgtempltsync.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class UpstreamEPGTaskDetail extends DynamicBaseObject
{
    /**
     * 展示字段
     */
    private java.lang.String taskid;//任务编号
    private java.lang.String spindex;//csplsp 
    private java.lang.String epggroup;//EPG编号    
    private java.lang.String fileid;//EPG模板文广编号
    private java.lang.String begintime;//模板创建时间
    private java.lang.String createtime;//模板创建时间
    private java.lang.String begintime1;//模板创建时间
    private java.lang.String begintime2;//模板创建时间 
    private java.lang.String operatetime;//操作时间
    private java.lang.Integer state;//处理状态
    private java.lang.Integer result;//处理结果
    private java.lang.String description;//任务执行结果详细描述
    
    /**
     * XML文件相关
     */
    private java.lang.String ftpip;//该task对应的xml文件的ftp地址
    private java.lang.String ftpuser;//该task对应的xml文件的ftp用户名
    private java.lang.String ftppwd;//该task对应的xml文件的ftp密码
    private java.lang.String ftpport;//该task对应的xml文件的ftp端口
    private java.lang.String ftppath;//该task对应的xml文件的ftp路径
    private java.lang.String filename;//该task对应的xml文件名称  
    
    /**
     * 剩余的zxdbm_cms.imp_epg_task_info表里信息
     */
    private java.lang.String correlateid;//该task对应的相关性标识  
    private java.lang.String epgfilenum;//该task的epg file数
    private java.lang.String systemfile;//该task的文件集是否为系统层文件
    private java.lang.String needuntar;//该task的文件是否需要解压缩 0：不需要 1：需要 
    private java.lang.String epgtype;//该任务的模板类型 0：bestv 1：其他模板
    private java.lang.String alartflag ;//告警执行标志，0：未告警 1：已告警
    private java.lang.String cmdxmlurl;//文广下发xml本地路径 
    private java.lang.String notifyxmlurl;//应答文广xml本地路径 
    private java.lang.String impcode;//接口机编码 
    
    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub
        this.addRelation("taskid","TASKID"); 
        this.addRelation("spindex","SPINDEX"); 
        this.addRelation("epggroup","EPGGROUP"); 
        this.addRelation("fileid","FILEID"); 
        this.addRelation("begintime","BEGINTIME");  
        this.addRelation("createtime","CREATETIME");  
        this.addRelation("begintime1","BEGINTIME1"); 
        this.addRelation("begintime2","BEGINTIME2");
        this.addRelation("operatetime","OPERATETIME"); 
        this.addRelation("state","STATE"); 
        this.addRelation("result","RESULT"); 
        this.addRelation("description","DESCRIPTION"); 
        //XML文件相关
        this.addRelation("ftpip","FTPIP"); 
        this.addRelation("ftpuser","FTPUSER");
        this.addRelation("ftppwd","FTPPWD");
        this.addRelation("ftpport","FTPPORT");
        this.addRelation("ftppath","FTPPATH");
        this.addRelation("filename","FILENAME");
        //其他字段
        this.addRelation("correlateid","CORRELATEID");
        this.addRelation("epgfilenum","EPGFILENUM");
        this.addRelation("systemfile","SYSTEMFILE");
        this.addRelation("needuntar","NEEDUNTAR");
        this.addRelation("epgtype","EPGTYPE");
        this.addRelation("alartflag","ALARTFLAG");
        this.addRelation("cmdxmlurl","CMDXMLURL");
        this.addRelation("notifyxmlurl","NOTIFYXMLURL");
        this.addRelation("impcode","IMPCODE");
    }

    public java.lang.String getNotifyxmlurl()
    {
        return notifyxmlurl;
    }



    public void setNotifyxmlurl(java.lang.String notifyxmlurl)
    {
        this.notifyxmlurl = notifyxmlurl;
    }



    public java.lang.String getImpcode()
    {
        return impcode;
    }



    public void setImpcode(java.lang.String impcode)
    {
        this.impcode = impcode;
    }



    public java.lang.String getTaskid()
    {
        return taskid;
    }


    public void setTaskid(java.lang.String taskid)
    {
        this.taskid = taskid;
    }


    public java.lang.String getSpindex()
    {
        return spindex;
    }


    public void setSpindex(java.lang.String spindex)
    {
        this.spindex = spindex;
    }


    public java.lang.String getEpggroup()
    {
        return epggroup;
    }


    public void setEpggroup(java.lang.String epggroup)
    {
        this.epggroup = epggroup;
    }


    public java.lang.String getFileid()
    {
        return fileid;
    }


    public void setFileid(java.lang.String fileid)
    {
        this.fileid = fileid;
    }


    public java.lang.String getBegintime()
    {
        return begintime;
    }


    public void setBegintime(java.lang.String begintime)
    {
        this.begintime = begintime;
    }


    public java.lang.String getBegintime1()
    {
        return begintime1;
    }


    public void setBegintime1(java.lang.String begintime1)
    {
        this.begintime1 = begintime1;
    }


    public java.lang.String getBegintime2()
    {
        return begintime2;
    }


    public void setBegintime2(java.lang.String begintime2)
    {
        this.begintime2 = begintime2;
    }


    public java.lang.String getOperatetime()
    {
        return operatetime;
    }


    public void setOperatetime(java.lang.String operatetime)
    {
        this.operatetime = operatetime;
    }

    public java.lang.Integer getState()
    {
        return state;
    }

    public void setState(java.lang.Integer state)
    {
        this.state = state;
    }


    public java.lang.Integer getResult()
    {
        return result;
    }


    public void setResult(java.lang.Integer result)
    {
        this.result = result;
    }


    public java.lang.String getDescription()
    {
        return description;
    }


    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }


    public java.lang.String getFtpip()
    {
        return ftpip;
    }


    public void setFtpip(java.lang.String ftpip)
    {
        this.ftpip = ftpip;
    }


    public java.lang.String getFtpuser()
    {
        return ftpuser;
    }


    public void setFtpuser(java.lang.String ftpuser)
    {
        this.ftpuser = ftpuser;
    }


    public java.lang.String getFtppwd()
    {
        return ftppwd;
    }


    public void setFtppwd(java.lang.String ftppwd)
    {
        this.ftppwd = ftppwd;
    }


    public java.lang.String getFtpport()
    {
        return ftpport;
    }


    public void setFtpport(java.lang.String ftpport)
    {
        this.ftpport = ftpport;
    }


    public java.lang.String getFtppath()
    {
        return ftppath;
    }


    public void setFtppath(java.lang.String ftppath)
    {
        this.ftppath = ftppath;
    }


    public java.lang.String getFilename()
    {
        return filename;
    }


    public void setFilename(java.lang.String filename)
    {
        this.filename = filename;
    }


    public java.lang.String getCreatetime()
    {
        return createtime;
    }


    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }
    
    public java.lang.String getCorrelateid()
    {
        return correlateid;
    }


    public void setCorrelateid(java.lang.String correlateid)
    {
        this.correlateid = correlateid;
    }


    public java.lang.String getEpgfilenum()
    {
        return epgfilenum;
    }


    public void setEpgfilenum(java.lang.String epgfilenum)
    {
        this.epgfilenum = epgfilenum;
    }


    public java.lang.String getSystemfile()
    {
        return systemfile;
    }


    public void setSystemfile(java.lang.String systemfile)
    {
        this.systemfile = systemfile;
    }


    public java.lang.String getNeeduntar()
    {
        return needuntar;
    }


    public void setNeeduntar(java.lang.String needuntar)
    {
        this.needuntar = needuntar;
    }


    public java.lang.String getEpgtype()
    {
        return epgtype;
    }


    public void setEpgtype(java.lang.String epgtype)
    {
        this.epgtype = epgtype;
    }


    public java.lang.String getAlartflag()
    {
        return alartflag;
    }


    public void setAlartflag(java.lang.String alartflag)
    {
        this.alartflag = alartflag;
    }


    public java.lang.String getCmdxmlurl()
    {
        return cmdxmlurl;
    }


    public void setCmdxmlurl(java.lang.String cmdxmlurl)
    {
        this.cmdxmlurl = cmdxmlurl;
    }
    
}

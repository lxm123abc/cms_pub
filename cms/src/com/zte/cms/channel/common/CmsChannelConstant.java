package com.zte.cms.channel.common;

/**
 * 
 * 频道模块日志常量类
 * 
 */
/**
 * @author Administrator
 */
public class CmsChannelConstant
{
    // 频道
    public static final String MGTTYPE_CHANNELCONTENT = "log.channelcontent.operator";
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";

    public static final String OPERTYPE_CHANNELCONTENT_ADD = "log.channelcontent.add";
    public static final String OPERTYPE_CHANNELCONTENT_UPD = "log.channelcontent.update";
    public static final String OPERTYPE_CHANNELCONTENT_DEL = "log.channelcontent.del";
    public static final String OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE = "log.channelcontent.prepareOffline";

    public static final String OPERTYPE_CHANNELCONTENT_ADD_INFO = "log.channelcontent.add.info";
    public static final String OPERTYPE_CHANNELCONTENT_UPD_INFO = "log.channelcontent.update.info";
    public static final String OPERTYPE_CHANNELCONTENT_DEL_INFO = "log.channelcontent.delete.info";
    public static final String OPERTYPE_CHANNELCONTENT_PREPAREOFFLINE_INFO = "log.channelcontent.prepateOffline.info";

    public static final String OPERTYPE_CHANNELCONTENT_PUBLISH = "log.channelcontent.publish";
    public static final String OPERTYPE_CHANNELCONTENT_CANCLEPUBLISH = "log.channelcontent.canclepublish";

    public static final String OPERTYPE_CHANNELCONTENT_PUBLISH_INFO = "log.channelcontent.publish.info";
    public static final String OPERTYPE_CHANNELCONTENT_CANCLEPUBLISH_INFO = "log.channelcontent.canclepublish.info";

    // 物理频道
    public static final String OPERTYPE_PHYSICALCHANNEL_ADD = "log.physicalchannel.add";
    public static final String OPERTYPE_PHYSICALCHANNEL_UPD = "log.physicalchannel.update";
    public static final String OPERTYPE_PHYSICALCHANNEL_DEL = "log.physicalchannel.del";

    public static final String OPERTYPE_PHYSICALCHANNEL_ADD_INFO = "log.physicalchannel.add.info";
    public static final String OPERTYPE_PHYSICALCHANNEL_UPD_INFO = "log.physicalchannel.update.info";
    public static final String OPERTYPE_PHYSICALCHANNEL_DEL_INFO = "log.physicalchannel.delete.info";

    public static final String OPERTYPE_PHYSICALCHANNEL_PUBLISH = "log.physicalchannel.publish";
    public static final String OPERTYPE_PHYSICALCHANNEL_CANCLEPUBLISH = "log.physicalchannel.canclepublish";

    public static final String OPERTYPE_PHYSICALCHANNEL_PUBLISH_INFO = "log.physicalchannel.publish.info";
    public static final String OPERTYPE_PHYSICALCHANNEL_CANCLEPUBLISH_INFO = "log.physicalchannel.canclepublish.info";

    // 频道栏目关联
    public static final String OPERTYPE_CATEGORYCHANNEL_ADD = "log.categorychannel.add";
    public static final String OPERTYPE_CATEGORYCHANNEL_DEL = "log.categorychannel.del";

    public static final String OPERTYPE_CATEGORYCHANNEL_ADD_INFO = "log.categorychannel.add.info";
    public static final String OPERTYPE_CATEGORYCHANNEL_DEL_INFO = "log.categorychannel.delete.info";

    public static final String OPERTYPE_CATEGORYCHANNEL_PUBLISH = "log.categorychannel.publish";
    public static final String OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH = "log.categorychannel.canclepublish";

    public static final String OPERTYPE_CATEGORYCHANNEL_PUBLISH_INFO = "log.categorychannel.publish.info";
    public static final String OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH_INFO = "log.categorychannel.canclepublish.info";

}

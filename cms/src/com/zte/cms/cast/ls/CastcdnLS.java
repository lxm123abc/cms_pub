package com.zte.cms.cast.ls;

import java.util.ArrayList;
import java.util.List;

import com.zte.cms.cast.castsync.ls.ICastTargetSyncLS;
import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.model.CastcdnConstant;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.ICastPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CastcdnLS extends DynamicObjectBaseDS implements ICastcdnLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CONTENT, getClass());

    private ICastcdnDS castds;
    private ICastrolemapDS castrolemapDS;
    private IPictureDS pictureDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ICmsFtptaskDS cmsFtptaskDS;
    private ICastPictureMapDS castPictureMapDS;
    private ITargetsystemDS targetSystemDS;
    private ICastTargetSyncLS castTargetSyncLS;
    
    Long syncindex;

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCastPictureMapDS(ICastPictureMapDS castPictureMapDS)
    {
        this.castPictureMapDS = castPictureMapDS;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public void setCastds(ICastcdnDS castds)
    {
        this.castds = castds;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }
    
    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }
    
    public void setCastrolemapDS(ICastrolemapDS castrolemapDS)
    {
        this.castrolemapDS = castrolemapDS;
    }

    public void setCastTargetSyncLS(ICastTargetSyncLS castTargetSyncLS)
    {
        this.castTargetSyncLS = castTargetSyncLS;
    }

    public String deleteCastcdn(String castindex) throws DomainServiceException
    {
        log.debug("deleteCastcdn start ");
        ReturnInfo rtnInfo;
        try
        {
            Castcdn castObj = new Castcdn();
            castObj.setCastindex(Long.valueOf(castindex));
            castObj = castds.getCastcdn(castObj);
            rtnInfo = new ReturnInfo();
            if (castObj == null)
            {
                rtnInfo.setFlag(CastcdnConstant.FALSE);
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.character.notexist"));
                return rtnInfo.toString();
            }
            
            Castrolemap crm = new Castrolemap();
            crm.setCastindex(Long.valueOf(castindex));
            List<Castrolemap> crmList = castrolemapDS.getCastrolemapByCond(crm);
            if(crmList != null && crmList.size() > 0){
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.has.bind.castrole"));
                return rtnInfo.toString();
            }
            
            CntPlatformSync platform = new CntPlatformSync();
            platform.setObjecttype(11);
            platform.setObjectid(castObj.getCastid());
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(11);
            target.setObjectid(castObj.getCastid());
            List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            List<CntPlatformSync> castPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
            if(castTargetList!=null&&castTargetList.size()>0){
                for(CntTargetSync targetlist:castTargetList){
                    int castSyncStatus = targetlist.getStatus();
                    if(castSyncStatus==200||castSyncStatus==300||castSyncStatus==500){
                        rtnInfo.setFlag("1");
                        rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));//"角色不存在");
                        return rtnInfo.toString();
                    }
                }
                cntPlatformSyncDS.removeCntPlatformSynListByObjindex(castPlatformList);
                cntTargetSyncDS.removeCntTargetSynListByObjindex(castTargetList);
            }
            
            CastPictureMap castpicmap = new CastPictureMap();
            castpicmap.setCastindex(castObj.getCastindex());
            List<CastPictureMap> listpictures = castPictureMapDS.getCastPictureMapByCond(castpicmap);
            if (listpictures != null && listpictures.size() > 0){
                List plist = new ArrayList();
                for(int i=0;i<listpictures.size();i++){
                    Picture p = new Picture();
                    CastPictureMap sMap = listpictures.get(i);
                    p.setPictureindex(sMap.getPictureindex());
                    p = pictureDS.getPicture(p);
                    if(p==null){
                        continue;
                    }
                    plist.add(p);
                    //插入删除文件的文件迁移任务
                    CmsFtptask ftptask = new CmsFtptask();
                    ftptask.setTasktype(1L); // 1-普通类型
                    ftptask.setFiletype(2); // 2-picture
                    ftptask.setFileindex(p.getPictureindex());
                    ftptask.setUploadtype(5); // 5-本地文件删除
                    ftptask.setSrcaddress(p.getPictureurl());
                    ftptask.setStatus(0); // 0-待上传
                    ftptask.setSrcfilehandle(1); // 1-删除
                    cmsFtptaskDS.insertCmsFtptask(ftptask);
                    CommonLogUtil.insertOperatorLog(p.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                            PictureConstants.OPERTYPE_PICTURE_DEL, PictureConstants.OPERTYPE_PICTURE_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    
                    //删除海报以及其关联关系关联平台记录
                    platform.setObjecttype(41);
                    platform.setObjectid(sMap.getMappingid());
                    target.setObjecttype(41);
                    target.setObjectid(sMap.getMappingid());
                    List<CntTargetSync> cpmapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    List<CntPlatformSync> cpmapPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                    if(cpmapTargetList!=null && cpmapTargetList.size()>0){
                        cntPlatformSyncDS.removeCntPlatformSynListByObjindex(cpmapPlatformList);
                        cntTargetSyncDS.removeCntTargetSynListByObjindex(cpmapTargetList);
                    }
                }
                pictureDS.removePictureList(plist);//删除关联到的海报
                castPictureMapDS.removeCastPictureMapList(listpictures);//删除同海报对应关系
            }
                
            castds.removeCastcdn(castObj);

            CommonLogUtil.insertOperatorLog(castObj.getCastid(), CastcdnConstant.MGTTYPE_CAST,
                    CastcdnConstant.OPERTYPE_CAST_DEL, CastcdnConstant.OPERTYPE_CAST_DEL_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            rtnInfo.setFlag(CastcdnConstant.SUCCESS);
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));
        }
        catch (Exception e)
        {
            log.error("Error occurred in deleteCastcdn method");
            throw new DomainServiceException();
        }

        log.debug(" deleteCastcdn end ");
        return rtnInfo.toString();
    }

    public Castcdn getCastcdn(Long castindex)
    {
        Castcdn cast = new Castcdn();
        cast.setCastindex(castindex);
        try
        {
            cast = castds.getCastcdn(cast);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in getCastcdn method");
            return null;
        }
        return cast;
    }

    public String insertCastcdn(Castcdn castcdn) throws DomainServiceException
    {
        log.debug("insertCastcdn start ");

        ReturnInfo rtnInfo = new ReturnInfo();
        try
        {
            castcdn.setStatus(CastcdnConstant.CAST_STATUS_WAITPUBLISH);
            String castid = castds.insertCastcdn(castcdn);

            CommonLogUtil.insertOperatorLog(castid, CastcdnConstant.MGTTYPE_CAST, CastcdnConstant.OPERTYPE_CAST_ADD,
                    CastcdnConstant.OPERTYPE_CAST_ADD_INFO, CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            rtnInfo.setFlag(CastcdnConstant.SUCCESS);
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));
        }
        catch (Exception e)
        {
            log.error("Error occurred in insertCastcdn method");
            throw new DomainServiceException();
        }
        log.debug("insertCastcdn end ");
        return rtnInfo.toString();

    }

    public String updateCastcdn(Castcdn castcdn) throws Exception
    {

        log.debug("updateCastcdn start ");

        Castcdn castObj = null;
        ReturnInfo rtnInfo = new ReturnInfo();
        try
        {
            castObj = castds.getCastcdn(castcdn);
            if (castObj == null)
            {
                rtnInfo.setFlag(CastcdnConstant.FALSE);
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cast.character.notexist"));
                return rtnInfo.toString();
            }
            
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(11);
            target.setObjectid(castObj.getCastid());
            List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(castTargetList!=null&&castTargetList.size()>0){
                for(CntTargetSync castTarget:castTargetList){
                    if(castTarget.getStatus()==200){
                        rtnInfo.setFlag(CastcdnConstant.FALSE);
                        rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("syncstatus.wrong.cannot.modify"));
                        return rtnInfo.toString();
                    }
                }
            }
            
            castds.updateCastcdn(castcdn);
            
            if(castTargetList != null && castTargetList.size() > 0){
                for(CntTargetSync castTargetSync:castTargetList)
                {
                    if(castTargetSync.getStatus() == 300 || castTargetSync.getStatus() == 500)
                    {
                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(castTargetSync.getTargetindex());
                        targetSystem = targetSystemDS.getTargetsystem(targetSystem);
                        String result = castTargetSyncLS.publishCastToTarget(castcdn.getCastindex(),
                                castTargetSync.getTargetindex(),targetSystem.getPlatform(),2);
                    }
                }
            }
            CommonLogUtil.insertOperatorLog(castObj.getCastid(), CastcdnConstant.MGTTYPE_CAST,
                    CastcdnConstant.OPERTYPE_CAST_MOD, CastcdnConstant.OPERTYPE_CAST_MOD_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            rtnInfo.setFlag(CastcdnConstant.SUCCESS);
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in updateCast method");
            throw new Exception(e);
        }
        log.debug("updateCastcdn end ");
        return rtnInfo.toString();
    }

    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize) throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            castcdn.setCastid(EspecialCharMgt.conversion(castcdn.getCastid()));
            castcdn.setCastname(EspecialCharMgt.conversion(castcdn.getCastname()));
            castcdn.setPersondisplayname(EspecialCharMgt.conversion(castcdn.getPersondisplayname()));

            tableInfo = castds.pageInfoQuery(castcdn, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;

    }

    public TableDataInfo pageInfoQuery(Castcdn castcdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            castcdn.setCastid(EspecialCharMgt.conversion(castcdn.getCastid()));
            castcdn.setCastname(EspecialCharMgt.conversion(castcdn.getCastname()));
            castcdn.setPersondisplayname(EspecialCharMgt.conversion(castcdn.getPersondisplayname()));

            tableInfo = castds.pageInfoQuery(castcdn, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;
    }


    public String batchDelCastcdnList(List<String> castindexs) throws Exception
    {
        log.debug("batchDelCastcdnList start ");

        Integer ifail = 0;
        Integer iSuccessed = 0;
        Castcdn cast = new Castcdn();
        Castcdn castObj = null;
        com.zte.cms.content.batchUpload.ls.ReturnInfo rtnInfo = new com.zte.cms.content.batchUpload.ls.ReturnInfo();
        com.zte.cms.content.batchUpload.ls.OperateInfo operateInfo = null;
        String opindex = "";
        String castindex = "";
        String flag = "";
        boolean isBreak = false;
        for (int i = 0; i < castindexs.size(); i++)
        {
            opindex = (i + 1) + "";
            try
            {
                castindex = castindexs.get(i);
                cast.setCastindex(Long.parseLong(castindex));
                castObj = castds.getCastcdn(cast);

                // 人物不存在
                if (castObj == null || castObj.getStatus() == null)
                {
                    ifail++;
                    operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castObj.getCastid(), ResourceMgt.findDefaultText("do.fail"),
                            ResourceMgt.findDefaultText("cast.character.notexist"));//人物不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                Castrolemap crm = new Castrolemap();
                crm.setCastindex(Long.valueOf(castindex));
                List<Castrolemap> crmList = castrolemapDS.getCastrolemapByCond(crm);
                if(crmList != null && crmList.size() > 0){
                    ifail++;
                    operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castObj.getCastid(), ResourceMgt.findDefaultText("do.fail"),
                            ResourceMgt.findDefaultText("cast.has.bind.castrole"));
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                CntPlatformSync platform = new CntPlatformSync();
                platform.setObjecttype(11);
                platform.setObjectid(castObj.getCastid());
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(11);
                target.setObjectid(castObj.getCastid());
                List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                List<CntPlatformSync> castPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                if(castTargetList!=null&&castTargetList.size()>0){
                    for(CntTargetSync targetlist:castTargetList){
                        int castSyncStatus = targetlist.getStatus();
                        if(castSyncStatus==200||castSyncStatus==300||castSyncStatus==500){
                            ifail++;
                            operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castObj.getCastid(), ResourceMgt.findDefaultText("do.fail"),
                                    ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete")); //人物发布状态不正确
                            rtnInfo.appendOperateInfo(operateInfo);
                            isBreak = true;
                            break;
                        }
                    }
                    if(isBreak == true){
                        continue;
                    }
                    cntPlatformSyncDS.removeCntPlatformSynListByObjindex(castPlatformList);
                    cntTargetSyncDS.removeCntTargetSynListByObjindex(castTargetList);
                }
                
                CastPictureMap castpicmap = new CastPictureMap();
                castpicmap.setCastindex(castObj.getCastindex());
                List<CastPictureMap> listpictures = castPictureMapDS.getCastPictureMapByCond(castpicmap);
                if (listpictures != null && listpictures.size() > 0){
                    List plist = new ArrayList();
                    for(int j=0;j<listpictures.size();j++){
                        Picture p = new Picture();
                        CastPictureMap sMap = listpictures.get(j);
                        p.setPictureindex(sMap.getPictureindex());
                        p = pictureDS.getPicture(p);
                        if(p==null){
                            continue;
                        }
                        plist.add(p);
                        //插入删除文件的文件迁移任务
                        CmsFtptask ftptask = new CmsFtptask();
                        ftptask.setTasktype(1L); // 1-普通类型
                        ftptask.setFiletype(2); // 2-picture
                        ftptask.setFileindex(p.getPictureindex());
                        ftptask.setUploadtype(5); // 5-本地文件删除
                        ftptask.setSrcaddress(p.getPictureurl());
                        ftptask.setStatus(0); // 0-待上传
                        ftptask.setSrcfilehandle(1); // 1-删除
                        cmsFtptaskDS.insertCmsFtptask(ftptask);
                        CommonLogUtil.insertOperatorLog(p.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                                PictureConstants.OPERTYPE_PICTURE_DEL, PictureConstants.OPERTYPE_PICTURE_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        //删除海报以及其关联关系关联平台记录
                        platform.setObjecttype(41);
                        platform.setObjectid(listpictures.get(j).getMappingid());
                        target.setObjecttype(41);
                        target.setObjectid(listpictures.get(j).getMappingid());
                        List<CntTargetSync> catPicmapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> catPicmapPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                        if(catPicmapTargetList!=null && catPicmapTargetList.size()>0){
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(catPicmapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(catPicmapTargetList);
                        }
                    }
                    pictureDS.removePictureList(plist);//删除关联到的海报
                    castPictureMapDS.removeCastPictureMapList(listpictures);//删除同海报对应关系
                }
                
                castds.removeCastcdn(castObj);
                iSuccessed++;
                operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castObj.getCastid(),ResourceMgt.findDefaultText("do.success"), ResourceMgt.findDefaultText("do.success"));
                rtnInfo.appendOperateInfo(operateInfo);

                CommonLogUtil.insertOperatorLog(castObj.getCastid(), CastcdnConstant.MGTTYPE_CAST,
                        CastcdnConstant.OPERTYPE_CAST_DEL, CastcdnConstant.OPERTYPE_CAST_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                
            }
            catch (Exception e)
            {
                ifail++;
                operateInfo = new com.zte.cms.content.batchUpload.ls.OperateInfo(opindex, castObj.getCastid(), ResourceMgt.findDefaultText("do.fail"), ResourceMgt.findDefaultText("character.status.wrong"));//数据库异常
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (castindexs.size() == 0 || ifail == castindexs.size())
        {
            flag = CastcdnConstant.FALSE; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = CastcdnConstant.PARTIALSUCCESS; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed +"  "+ ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + castindexs.size());
        }

        log.debug("batchDelCastcdnList end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }
    
    public String bindBatchCastTargetsync(List<Targetsystem> platformList,List<Castcdn> castindexs)
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCastindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < castindexs.size(); i++)  //遍历要操作的人物主键，依次操作要关联的内容
            {
                totalCastindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
                
                Castcdn castObj = new Castcdn();
                castObj.setCastindex(castindexs.get(i).getCastindex());
                castObj = castds.getCastcdn(castObj);
                if (castObj == null ){
                   proStatusStr = ResourceMgt.findDefaultText("cast.character.notexist");
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), castindexs.get(i).getCastindex().toString(), 
                           result.substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }else{
                    operdesc = bindCastTargetsync(platformList,castObj);  //单个内容关联运营平台
                    if (operdesc.substring(0, 1).equals("0"))  //内容关联运营平台成功
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), castObj.getCastid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), castObj.getCastid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        if ((successCount) == totalCastindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCastindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
    }
    
    public String bindCastTargetsync(List<Targetsystem> platformList,Castcdn castcdn)
    {
        log.debug("bindCastTargetsync starting...");
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        try
        {
            for (int i = 0; i < platformList.size(); i++)  //遍历要关联的平台
            {
                Targetsystem targtemp = new Targetsystem();
                targtemp.setTargetindex(platformList.get(i).getTargetindex());
                targtemp = targetSystemDS.getTargetsystem(targtemp);                 
                if (targtemp != null && targtemp.getStatus() == 0)  //0:正常
                {
                    CntTargetSync target = new CntTargetSync();
                    target.setObjecttype(11);
                    target.setObjectid(castcdn.getCastid());
                    target.setTargetindex(platformList.get(i).getTargetindex());
                    //运营平台存在
                    List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    if(targetSyncList !=null && targetSyncList.size()>0)
                    {
                        resultStr.append(ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                             + platformList.get(i).getTargetid()+ ResourceMgt.findDefaultText(CntConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)+"  ");
                                        
                        continue;
                    } 
                    CntPlatformSync platform = new CntPlatformSync();
                    platform.setObjecttype(11);
                    platform.setObjectid(castcdn.getCastid());
                    platform.setPlatform(platformList.get(i).getPlatform());
                    List<CntPlatformSync> platformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                    if(platformSyncList !=null && platformSyncList.size()>0)  
                    {
                        syncindex = platformSyncList.get(0).getSyncindex();
                        platform = platformSyncList.get(0);
                        platform.setStatus(0);
                        cntPlatformSyncDS.updateCntPlatformSync(platform);   
                    }else{
                        insertPlatformSyncTask(11,castcdn.getCastindex(),castcdn.getCastid(),"","",
                                platformList.get(i).getPlatform(),0);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(castcdn.getCastid().toString() + ", " + platformList.get(i).getTargetid(), CastcdnConstant.MGTTYPE_CAST,
                                ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_PLATFORM_CAST_BIND_ADD), ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_PLATFORM_CAST_BIND_ADD_INFO),
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    
                    insertTargetSyncTask(syncindex,11,castcdn.getCastindex(),castcdn.getCastid(),"","",
                            targtemp.getTargetindex(),0,0);
                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(castcdn.getCastid().toString() + ", " + platformList.get(i).getTargetid(), CastcdnConstant.MGTTYPE_CAST,
                            ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_TARGET_CAST_BIND_ADD), ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_PLATFORM_CAST_BIND_ADD_INFO),
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                 
                    
                    CastPictureMap castpicmap = new CastPictureMap();
                    castpicmap.setCastindex(castcdn.getCastindex());
                    List<CastPictureMap> listpictures = castPictureMapDS.getCastPictureMapByCond(castpicmap);
                    if (listpictures != null && listpictures.size() > 0)
                    {
                        for(CastPictureMap piccastmap:listpictures)
                        {
                            CntPlatformSync cpPlatform = new CntPlatformSync();
                            cpPlatform.setObjecttype(41);
                            cpPlatform.setObjectid(piccastmap.getMappingid());
                            cpPlatform.setPlatform(platformList.get(i).getPlatform());
                            List<CntPlatformSync> castpicplatformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(cpPlatform);
                            if(castpicplatformSyncList !=null && castpicplatformSyncList.size()>0)  
                            {
                                syncindex = castpicplatformSyncList.get(0).getSyncindex();
                                cpPlatform = castpicplatformSyncList.get(0);
                                cpPlatform.setStatus(0);
                                cntPlatformSyncDS.updateCntPlatformSync(cpPlatform);   
                            }else{
                                insertPlatformSyncTask(41,piccastmap.getMapindex(),piccastmap.getMappingid(),piccastmap.getCastid()
                                        ,piccastmap.getPictureid(),platformList.get(i).getPlatform(),0);
                                // 写操作日志
                                CommonLogUtil.insertOperatorLog(piccastmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), CastcdnConstant.MGTTYPE_CAST,
                                        ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_TARGET_CASTPICMAP_BIND_ADD), ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_PLATFORM_CASTPICMAP_BIND_ADD_INFO),
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                            
                            insertTargetSyncTask(syncindex,41,piccastmap.getMapindex(),piccastmap.getMappingid(),piccastmap.getCastid()
                                    ,piccastmap.getPictureid(),targtemp.getTargetindex(),0,0);
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(piccastmap.getMappingid().toString() + ", " + platformList.get(i).getTargetid(), CastcdnConstant.MGTTYPE_CAST,
                                    ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_TARGET_CASTPICMAP_BIND_ADD), ResourceManager.getResourceText(CastcdnConstant.OPERTYPE_PLATFORM_CASTPICMAP_BIND_ADD_INFO),
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                    }
                    sucess = true;
                    resultStr.append(ResourceMgt.findDefaultText(CntConstants
                            .CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                            + ResourceMgt.findDefaultText(CntConstants
                                    .CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)+"  ");
                        
                    
                }else{
                    resultStr.append(ResourceMgt.findDefaultText(CntConstants
                            .CMS_TARGETSYSTEM_MSG_TARGETSYSTEM) + platformList.get(i).getTargetid()
                            + ResourceMgt.findDefaultText(CntConstants
                                    .CMS_TARGETSYSTEM_MSG_HAS_DELETED)+"  ");
                }
            }
        }catch(Exception e){
            log.error("CastcdnLS exception:" + e.getMessage());
        }
        log.debug("bindCastTargetsync end...");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }
    
    /**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
}

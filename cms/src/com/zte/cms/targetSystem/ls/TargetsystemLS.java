package com.zte.cms.targetSystem.ls;

import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.common.TargetSystemConstant;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

/**
 * 目标系统的LS实现层
 * 
 * @author sanya
 * 
 */
public class TargetsystemLS implements ITargetsystemLS {

	private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_USER,
			getClass());
	private ITargetsystemDS targetSystemDS;

	/**
	 * targetSystemDS的set方法
	 * 
	 * @param targetSystemDS ITargetsystemDS
	 */
	public void setTargetSystemDS(ITargetsystemDS targetSystemDS) {
		this.targetSystemDS = targetSystemDS;
	}

	/**
	 * 新增Targetsystem对象
	 * 
	 * @param targetsystem Targetsystem对象
	 * @return String
	 * @throws DomainServiceException ds异常
	 */
	public String insertTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		// TODO Auto-generated method stub
		log.debug("insertTargetsystem    stating.............");
		ResourceMgt
				.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
		targetSystemDS.insertTargetsystem(targetsystem);
		CommonLogUtil.insertOperatorLog(targetsystem.getTargetid(),
				TargetSystemConstant.MGTTYPE_TARGETSYSTEM,
				TargetSystemConstant.OPERTYPE_TARGET_ADD,
				TargetSystemConstant.OPERTYPE_TARGET_ADD_INFO,
				TargetSystemConstant.RESOURCE_OPERATION_SUCCESS);
		log.debug("insertTargetsystem    end.............");
		return "0:操作成功";
	}

	/**
	 * 查询Targetsystem对象
	 * 
	 * @param targetsystem Targetsystem对象
	 * @return Targetsystem对象
	 * @throws DomainServiceException ds异常
	 */
	public Targetsystem getTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("getTargetsystem     starting...........");
		Targetsystem targetsystemNew = targetSystemDS
				.getTargetsystem(targetsystem);
		log.debug("getTargetsystem     end...........");
		return targetsystemNew;
	}

	/**
	 * 更新Targetsystem对象
	 * 
	 * @param targetsystem Targetsystem对象
	 * @return String
	 * @throws DomainServiceException ds异常
	 */
	public String updateTargetsystem(Targetsystem targetsystem)
			throws DomainServiceException {
		log.debug("updateTargetsystem     starting...........");
		Targetsystem targetsystemNew = targetSystemDS
				.getTargetsystem(targetsystem);
		ResourceMgt
				.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
		String result = null;
		if (targetsystemNew != null) {
			targetSystemDS.updateTargetsystem(targetsystem);
			CommonLogUtil.insertOperatorLog(targetsystem.getTargetid(),
					TargetSystemConstant.MGTTYPE_TARGETSYSTEM,
					TargetSystemConstant.OPERTYPE_TARGET_MOD,
					TargetSystemConstant.OPERTYPE_TARGET_MOD_INFO,
					TargetSystemConstant.RESOURCE_OPERATION_SUCCESS);
			result = ResourceMgt.findDefaultText("oper.success.with.0");//"0:操作成功"
		} else {
			result = ResourceMgt.findDefaultText("do.failed.object.notexist");//"1:操作失败，该目标系统不存在或是已被删除"
		}
		log.debug("updateTargetsystem     end...........");
		return result;
	}

	/**
	 * 删除目标系统
	 * @param targetIndex 目标系统的索引
	 * @return String
	 * @throws DomainServiceException 抛出异常信息
	 */
	public String deleteTargetsystem(String targetIndex)
			throws DomainServiceException {

		log.debug("deleteTargetsystem     starting...........");
		ResourceMgt
				.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
		Targetsystem targetsystem = new Targetsystem();
		targetsystem.setTargetindex(Long.parseLong(targetIndex));
		Targetsystem targetsystemNew = targetSystemDS
				.getTargetsystem(targetsystem);
		String result = null;
		if (targetsystemNew != null) {
			targetSystemDS.removeTargetsystem(targetsystem);
			CommonLogUtil.insertOperatorLog(targetsystemNew.getTargetid(),
					TargetSystemConstant.MGTTYPE_TARGETSYSTEM,
					TargetSystemConstant.OPERTYPE_TARGET_DEL,
					TargetSystemConstant.OPERTYPE_TARGET_DEL_INFO,
					TargetSystemConstant.RESOURCE_OPERATION_SUCCESS);
			result = ResourceMgt.findDefaultText("oper.success.with.0");//"0:操作成功"
		} else {
			result = ResourceMgt.findDefaultText("do.failed.object.notexist");//"1:操作失败,目标系统不存在或是已被删除"
		}
		log.debug("deleteTargetsystem     end...........");
		return result;
	}

	/**
	 * 根据条件分页查询Targetsystem对象
	 * 
	 * @param targetsystem Targetsystem对象，作为查询条件的参数
	 * @param start 起始行
	 * @param pageSize 页面大小
	 * @return TableDataInfo 查询结果
	 */
	public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start,
			int pageSize) {
		log.debug("pageInfoQuery  starting.................................");
		TableDataInfo dataInfo = new TableDataInfo();
		targetsystem.setTargetid(EspecialCharMgt.conversion(targetsystem
				.getTargetid()));
		targetsystem.setTargetname(EspecialCharMgt.conversion(targetsystem
				.getTargetname()));
		targetsystem.setOrderCond("changetime desc");
		try {
			dataInfo = targetSystemDS.pageInfoQuery(targetsystem, start,
					pageSize);
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("pageInfoQuery  end.................................");
		return dataInfo;
	}

	/**
	 * 平台关联页面的公共查询方法
	 */
	public TableDataInfo popForPub(Targetsystem targetsystem, int start,
			int pageSize) {

		String objtype = targetsystem.getObjtype();
		if (objtype.equals("2") || objtype.equals("1") || objtype.equals("4")) {//栏目,服务,连续剧
			targetsystem.setPlatforms("1,2");
			targetsystem.setTargettypes("0,1,2,10");
		}
		if (objtype.equals("3") || objtype.equals("5") || objtype.equals("6")) {//内容,频道,节目单
			targetsystem.setPlatforms("1,2");
			targetsystem.setTargettypes("0,1,2,3,10");
		}
		if (objtype.equals("11") || objtype.equals("12")) {//cast,castrolemap,央视不选
			targetsystem.setPlatforms("1,2");
			targetsystem.setTargettypes("0,2");
		}
		targetsystem.setStatus(0);//0是正常
		log.debug("pageInfoQuery  starting.................................");
		TableDataInfo dataInfo = new TableDataInfo();
		targetsystem.setTargetid(EspecialCharMgt.conversion(targetsystem
				.getTargetid()));
		targetsystem.setTargetname(EspecialCharMgt.conversion(targetsystem
				.getTargetname()));
		targetsystem.setOrderCond("changetime desc");
		try {
			dataInfo = targetSystemDS.pageInfoQueryPop(targetsystem, start,
					pageSize);
		} catch (Exception e) {
			log.error(e);
		}
		log.debug("pageInfoQuery  end.................................");
		return dataInfo;
	}

	public String changeStatus(String targetIndex,int status)
			throws DomainServiceException {
		log.debug("changeStatus     starting...........");
		Targetsystem targetsystem = new Targetsystem();
		targetsystem.setTargetindex(Long.parseLong(targetIndex));
		Targetsystem targetsystemNew = targetSystemDS.getTargetsystem(targetsystem);
		String result = "";
		if (targetsystemNew != null &&( targetsystemNew.getStatus() == 0 || targetsystemNew.getStatus() == 1)) {

			targetsystemNew.setStatus(status);			
			targetSystemDS.updateTargetsystem(targetsystemNew);
			if(status ==1 ){			    
			    	CommonLogUtil.insertOperatorLog(targetsystemNew.getTargetid(),
					TargetSystemConstant.MGTTYPE_TARGETSYSTEM,
					TargetSystemConstant.OPERTYPE_TARGET_PAU,
					TargetSystemConstant.OPERTYPE_TARGET_PAU_INFO,
					TargetSystemConstant.RESOURCE_OPERATION_SUCCESS);
			}else{
			    	 CommonLogUtil.insertOperatorLog(targetsystemNew.getTargetid(),
					TargetSystemConstant.MGTTYPE_TARGETSYSTEM,
					TargetSystemConstant.OPERTYPE_TARGET_DEL,
					TargetSystemConstant. OPERTYPE_TARGET_DEL_INFO,
					TargetSystemConstant.RESOURCE_OPERATION_SUCCESS);			    
			}
			result = ResourceMgt.findDefaultText("oper.success.with.0");//"0:操作成功"
		} else {
			if (targetsystemNew == null) {
				result = "1:操作失败,目标系统不存在或是已被删除";				  
			} else {
				result = "1:操作失败,目标系统状态错误";
			}
		}
		log.debug("changeStatus     end...........");
		return result;
	}
    
    /**
     * 查询服务已经绑定的目标平台
     */
    public TableDataInfo getServiceBindTargetList (Long serviceIndex, int start, int pageSize) throws Exception
    {
        // TODO Auto-generated method stub
        Service service = new Service();
        service.setServiceindex(serviceIndex);
        
        log.debug("query servicelist starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = targetSystemDS.queryServiceBindTargetListByCond(service, start, pageSize);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query service list ends");
        return tableInfo;
    }

}

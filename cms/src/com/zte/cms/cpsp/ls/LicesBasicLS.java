package com.zte.cms.cpsp.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.cpsp.CpspLogConstant;
import com.zte.cms.cpsp.LicesConstant;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.cms.cpsp.service.ICmsCpbindDS;
import com.zte.cms.cpsp.service.ICmsUcpBasicDS;
import com.zte.purview.access.model.OperInformation;
import com.zte.purview.access.model.OperObjectinfo;
import com.zte.purview.access.model.OperService;
import com.zte.purview.business.service.IOperInformationDS;
import com.zte.purview.business.service.IOperObjectinfoDS;
import com.zte.purview.business.service.IOperServiceDS;
import com.zte.purview.util.PurviewUtils;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.ls.IUcpManageLS;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.model.UcpContact;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.cpsp.common.service.IUcpContactDS;
import com.zte.umap.sys.model.UsysProvince;
import com.zte.umap.sys.service.IUsysProvinceDS;
import com.zte.umap.terminal.ls.UhandsetConstant;
import com.zte.zxywpub.encrypt;

public class LicesBasicLS implements ILicesBasicLS {

    private IUcpBasicDS ucpBasicDS;
    private IUcpContactDS ucpContactDS;
    private ICmsCpbindDS cmsCpbindDS;
    private IUcpManageLS passLicesManageLS = null;
    private IOperServiceDS operServiceDS;
    private IUsysProvinceDS usysProvinceDS = null;
    private Log log = SSBBus.getLog(getClass());
    private IOperObjectinfoDS operObjectinfoDS;
    private static final String MES_LISCES_NAME_EXIST = "该牌照名称已经被使用过了";
    private static final String MES_OK = "ok";
    private static final String SERVICE_KEY = "CMS";
    private static final String MES_LISCES_DEL_OR_ID_USED = "该ID已经被使用或者已经被删除";
    private static final String MES_LISCES_HAS_DEL = "该牌照方已经被删除";
    private static final String MES_CANNOT_DEL_FOR_STATUS = "牌照方状态不正确，不能删除";
    private static final String MES_CANNOT_DEL_FOR_BINDED = "该牌照方已经绑定操作员，不能进行删除，请先去绑定";
    private static final String MES_LISCES_HAS_BIND_CP = "该牌照方已经绑定了CP，请先去绑定，再进行删除";
    private static final String MES_LISCES_CANNOT_DELSYN_FOR_BIND_CP = "1:该牌照方已经绑定了CP，请先去绑定，再进行删除同步";
    private static final String MES_CANNOT_DELSYN_FOR_BINDED = "1:该牌照方已经绑定操作员，不能进行删除同步，请先去绑定";
    private static final String ADD = "add";
    private static final String MODIFY = "modify";
    private static final String MES_ERROR = "系统错误";
    private static final String MES_CANNOT_UPDATE_FOR_STATUS = "牌照方状态不正确，不能进行修改";
    private static final String MES_CANNOT_UPDATE_FOR_DEL = "该牌照方信息已经被删除，无法进行修改";
    private static final String SUCCESS_CODE = "070000";
    private static final String RESERVE06_VALUE = "1";
    private static final String APPLY_SYN_FAIL_FOR_STATUS = "1:牌照方状态不正确，不能发起同步请求";
    private static final String DEL_SYN_APPLY_FAIL_FOR_STATUS = "1:牌照方状态不正确，不能发起删除同步请求";
    private static final String SYN = "syn";
    private static final String CANCEL_SYN = "cancelSyn";
    private static final String SYSTEM_ERROR_FOR_OPENMSG = "1:系统错误";
    private static final String ERROR = "error";
    private static final String OPERATION_SUCCESS_CODE = "070000";
    private static final String OPER_SUCCESS_WITH_0 = "0:操作成功";
    private IOperInformationDS operInformationDS;
    private ICmsUcpBasicDS cmsUcpBasicDS;

    public void setCmsUcpBasicDS(ICmsUcpBasicDS cmsUcpBasicDS) {
        this.cmsUcpBasicDS = cmsUcpBasicDS;
    }

    public void setOperInformationDS(IOperInformationDS operInformationDS) {
        this.operInformationDS = operInformationDS;
    }

    public void setOperObjectinfoDS(IOperObjectinfoDS operObjectinfoDS) {
        this.operObjectinfoDS = operObjectinfoDS;
    }

    public void setOperServiceDS(IOperServiceDS operServiceDS) {
        this.operServiceDS = operServiceDS;
    }

    public void setUsysProvinceDS(IUsysProvinceDS usysProvinceDS) {
        this.usysProvinceDS = usysProvinceDS;
    }

    public void setCmsCpbindDS(ICmsCpbindDS cmsCpbindDS) {
        this.cmsCpbindDS = cmsCpbindDS;
    }

    public void setPassLicesManageLS(IUcpManageLS passLicesManageLS) {
        this.passLicesManageLS = passLicesManageLS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS) {
        this.ucpBasicDS = ucpBasicDS;
    }

    public void setUcpContactDS(IUcpContactDS ucpContactDS) {
        this.ucpContactDS = ucpContactDS;
    }

    public UcpBasic getUcpBasic(UcpBasic ucpBasic) throws DomainServiceException {

        return ucpBasicDS.getUcpBasic(ucpBasic);

    }

    public List<UcpBasic> getUcpBasicByCond(UcpBasic ucpBasic) throws DomainServiceException {

        return ucpBasicDS.getUcpBasicByCond(ucpBasic);
    }


    // 检查牌照方名称是否被用
    private boolean checkCpName(UcpBasic ucpBasic) throws DomainServiceException {
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS checkCpName start...");
        boolean flag = false;
        try {
            UcpBasic tempBasic = new UcpBasic();
            tempBasic.setCpcnshortname(ucpBasic.getCpcnshortname());
            tempBasic.setCptype(LicesConstant.LISCE_TYPE_2);
            List<UcpBasic> ucpBasicList = ucpBasicDS.getUcpBasicExactByCond(tempBasic);
            if (ucpBasicList == null || ucpBasicList.size() == 0) {
                flag = true;
            }
        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS checkCpName exception:" + e);
            throw e;
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS checkCpName end");
        return flag;
    }

    public String insertUcpBasic(UcpBasic ucpBasic, List<UcpContact> ucpContactList) throws DomainServiceException {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS insertUcpBasic start...");
        String result = "";
        try {

            boolean cpidtr = true;
            if (!checkCpID(ucpBasic)) {
                cpidtr = false; //牌照方ID已存在
                result = "该牌照方ID已经被使用过了";

            } else {
                cpidtr = true;
            }
            if (cpidtr) {
                if (checkCpName(ucpBasic)) {
                    ucpBasic.setServicekey(SERVICE_KEY);// 设置服务KEY为CMS
                    for (int i = 0; i < ucpContactList.size(); i++) {
                        ucpContactList.get(i).setServicekey(SERVICE_KEY);
                    }
                    ucpBasic.setServicekey("CMS");
                    // 调用新增牌照方以及联系人接口.
                    String inserMsg = passLicesManageLS.addCPSPRegister(ucpBasic, ucpContactList, null);
                    result = MES_OK;

                } else {

                    result = "该牌照方名称已经被使用过了";
                }
            }
        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS insertUcpBasic exception:" + e);
            throw e;
        }

        if (result.equals(MES_OK)) {
            CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                    CpspLogConstant.OPERTYPE_LISENCE_ADD, CpspLogConstant.OPERTYPE_LISENCE_ADD_INFO,
                    CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS insertUcpBasic end");
        return result;
    }

    private boolean checkCpID(UcpBasic ucpBasic) {
        UcpBasic ucpBasicCheck = new UcpBasic();
        ucpBasicCheck.setCpid(ucpBasic.getCpid());
        ucpBasicCheck.setCptype(2);
        ucpBasicCheck.setServicekey(SERVICE_KEY);
        List<UcpBasic> sameNameCpList = null;
        try {
            sameNameCpList = ucpBasicDS.getUcpBasicByCond(ucpBasicCheck);
        } catch (Exception e) {
            log.error("ds exception:" + e.getMessage());
        }
        if (sameNameCpList.size() > 0) //已经存在相同的cpid
        {
            return false;
        }
        return true;
    }

    public String removeLicesBasicByCond(UcpBasic ucpBasic) throws DomainServiceException {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS removeLicesBasicByCond start...");
        String rtnCode = "";
        try {

            ucpBasic = this.getUcpBasicByIndex(ucpBasic.getCpindex());
            // status=4表示已删除
            if (ucpBasic == null || ucpBasic.getStatus() == LicesConstant.STATUS_DEL_4) {
                rtnCode = ResourceMgt.findDefaultText("mes.lisces.has.del");
                return rtnCode;
            }
            // status=1 表示正常
            if (ucpBasic.getStatus() != LicesConstant.STATUS_ID_USED_1) {
                rtnCode = ResourceMgt.findDefaultText("mes.cannot.del.for.status");
                return rtnCode;
            }
            OperService operService = new OperService();
            operService.setSerindex(new Long(ucpBasic.getCpindex()));
            // 如果有操作员绑定了此牌照方，则不允许删除
            List<OperService> operServiceList = operServiceDS.getOperServices(operService);
            if (operServiceList != null && operServiceList.size() > 0) {
                // return MES_CANNOT_DEL_FOR_BINDED;
                for (OperService service : operServiceList) {
                    operServiceDS.removeOperService(service);
                }
            }

            CmsCpbind cmsCpbind = new CmsCpbind();
            cmsCpbind.setLpid(ucpBasic.getCpid());
            List<CmsCpbind> cmsCpbindList = cmsCpbindDS.getCmsCpbindByCond(cmsCpbind);
            if (cmsCpbindList != null && cmsCpbindList.size() > 0) {
                return ResourceMgt.findDefaultText("mes.lisces.has.bind.cp");
            }
            List<UcpContact> ucpContactList = this.getUcpContactList(ucpBasic);
            ucpBasic.setStatus(LicesConstant.STATUS_DEL_4);// 设置牌照方状态为被删除状态
            rtnCode = passLicesManageLS.updateCPSPAllInfo(ucpBasic, ucpContactList, null, null);
            // 删除牌照方在操作员--牌照方关系表中的注册信息
            OperObjectinfo objectinfo = new OperObjectinfo();
            objectinfo.setObjectid(ucpBasic.getCpindex());
            List<OperObjectinfo> objList = operObjectinfoDS.findOperObjectinfos(objectinfo);
            if (objList != null) {
                for (int i = 0; i < objList.size(); i++) {
                    operObjectinfoDS.removeOperObjectinfo(objList.get(i));
                }
            }

            // rtnCode = passLicesManageLS.deleteCPSPAllInfo(ucpCond);
        } catch (DomainServiceException ex) {
            ex.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS removeLicesBasicByCond exception:" + ex);
            throw ex;
        }
        if (rtnCode.equals(OPERATION_SUCCESS_CODE)) {
            CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                    CpspLogConstant.OPERTYPE_LISENCE_DEL, CpspLogConstant.OPERTYPE_LISENCE_DEL_INFO,
                    CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS removeLicesBasicByCond end");
        return rtnCode;
    }

    public UcpBasic getUcpBasicByIndex(Long cpIndex) throws DomainServiceException {
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS getUcpBasicByIndex start...");
        UcpBasic upBasic = null;
        try {
            upBasic = ucpBasicDS.getUcpBasicByIndex(cpIndex);

        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS getUcpBasicByIndex exception:" + e);
            return null;
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS getUcpBasicByIndex end");
        return upBasic;
    }

    public List<UcpContact> getUcpContactList(UcpBasic ucpBasic) throws DomainServiceException {
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS getUcpContactList start...");

        List<UcpContact> ucpContactList = null;
        try {
            UcpContact ucpContact = new UcpContact();
            ucpContact.setCpindex(ucpBasic.getCpindex());
            ucpContactList = ucpContactDS.getUcpContactByCond(ucpContact);

        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS getUcpContactList exception:" + e);
            return null;
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS getUcpContactList end");
        return ucpContactList;
    }

    public String applyDataByMode(UcpBasic ucpBasic, List<UcpContact> ucpContactList, String mode)
            throws DomainServiceException {
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS applyDataByMode starting...");
        String result = null;
        try {
            // 新增牌照方
            if (mode.equals(ADD)) {
                result = insertUcpBasic(ucpBasic, ucpContactList);
            }
            // 修改牌照方
            else if (mode.equals(MODIFY)) {
                result = this.updateUcpBasice(ucpBasic, ucpContactList);
            } else {
                result = ResourceMgt.findDefaultText("mes.error");
            }
        } catch (DomainServiceException e) {
            log.debug("com.zte.cms.cpsp.ls LicesBasicLS applyDataByMode exception:" + e);
            throw e;
        }

        log.debug("com.zte.cms.cpsp.ls LicesBasicLS applyDataByMode end");
        return result;
    }

    public HashMap getUsysProvinceMap() {
        log.debug("select usysProvinceMap starting...");
        HashMap<String, Object> map = new HashMap<String, Object>();
        List<String> optionList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        try {

            UsysProvince usysProvince = new UsysProvince();
            List<UsysProvince> list = usysProvinceDS.getUsysProvinceByCond(usysProvince);// 获取省份集合

            for (int i = 0; i < list.size(); i++) { // 循环CPID集合
                optionList.add(list.get(i).getProvname());
                valueList.add(list.get(i).getProvid());
            }

            map.put("textList", optionList);
            map.put("valueList", valueList);
            map.put("defaultValue", "0");

        } catch (Exception e) {

            log.error("dao exception:" + e.getMessage());
        }
        log.debug("select usysProvinceMap end");
        return map;
    }

    public String updateUcpBasice(UcpBasic ucpBasic, List<UcpContact> ucpContactList) throws DomainServiceException {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasice start...");
        String result = "";
        boolean isNameChange = true;
        try {
            // 根据主键查询牌照方信息
            UcpBasic indexByUcpBasic = ucpBasicDS.getUcpBasicByIndex(ucpBasic.getCpindex());
            if (indexByUcpBasic != null) {
                // status=4表示已删除
                if (indexByUcpBasic.getStatus() == LicesConstant.STATUS_DEL_4) {
                    return ResourceMgt.findDefaultText("mes.lisces.has.del");
                }
                // status=1 表示正常; status=41表示新增同步失败
                if ((indexByUcpBasic.getStatus().intValue() != LicesConstant.STATUS_NORMAL_1)
                        && (indexByUcpBasic.getStatus().intValue() != LicesConstant.STATUS_NEWSYNFAIL_41)) {
                    return ResourceMgt.findDefaultText("mes.cannot.update.for.status");
                }
                // 根据名称查询数据
                UcpBasic nameByUcpBasic = new UcpBasic();
                nameByUcpBasic.setCptype(LicesConstant.LISCE_TYPE_2);
                nameByUcpBasic.setCpcnshortname(ucpBasic.getCpcnshortname());
                List<UcpBasic> ucpBasicList = ucpBasicDS.getUcpBasicExactByCond(nameByUcpBasic);
                // 如果根据牌照方名称查询数据库有记录，则进行判断
                if (ucpBasicList.size() > 0) {
                    // 因为牌照方名称是不允许重复的，即查到的数据最多只能为一条，所以选中第0个记录
                    nameByUcpBasic = ucpBasicList.get(0);
                    // 如果根据名称查询的牌照方主键与根据主键查询的牌照方，两者的主键不一致，则说明修改牌照方
                    // 的名称已经存在。之所以这样判断是因为，可能用户对牌照方名称不进行修改，这样防止根据名称
                    // 查询牌照方信息是同一条记录。
                    if ((nameByUcpBasic.getCpindex().longValue()) != (indexByUcpBasic.getCpindex().longValue())) {
                        return ResourceMgt.findDefaultText("mes.lisces.name.exist");
                    } else {
                        isNameChange = false;
                    }

                }
                // 修改牌照方信息关键语句
                ucpBasic.setServicekey("CMS");
                result = passLicesManageLS.updateCPSPAllInfo(ucpBasic, ucpContactList, null, null);
                // 如果牌照方名称被修改，则修改在操作员--牌照方关系表中的信息；因为此表冗余了牌照方表的名称信息
                if (isNameChange) {
                    OperObjectinfo operObjectinfo = null;
                    operObjectinfo = new OperObjectinfo();
                    operObjectinfo.setObjectid(ucpBasic.getCpindex());
                    List<OperObjectinfo> operObjList = operObjectinfoDS.findOperObjectinfos(operObjectinfo);
                    if (operObjList != null) {
                        for (int i = 0; i < operObjList.size(); i++) {
                            operObjectinfo = operObjList.get(i);
                            operObjectinfo.setObjectname(ucpBasic.getCpcnshortname());
                            operObjectinfoDS.updateOperObjectinfo(operObjectinfo);
                        }
                    }
                }
            } else {
                result = ResourceMgt.findDefaultText("mes.cannot.update.for.del");
            }
        } catch (DomainServiceException e) {
            e.printStackTrace();
            log.error("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasice exception:" + e);
            throw e;
        }

        if (result.equals(SUCCESS_CODE)) {
            CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                    CpspLogConstant.OPERTYPE_LISENCE_MOD, CpspLogConstant.OPERTYPE_LISENCE_MOD_INFO,
                    CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasice end");
        return result;
    }

    private void updateBasicStatusToDB(UcpBasic ucpBasic) throws DomainServiceException {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasic start...");
        try {
            ucpBasic.setCptype(LicesConstant.LISCE_TYPE_2);
            ucpBasicDS.updateUcpBasic(ucpBasic);
            CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                    CpspLogConstant.OPERTYPE_LISENCE_MOD, CpspLogConstant.OPERTYPE_LISENCE_MOD_INFO,
                    CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
        } catch (DomainServiceException e) {
            log.error("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasic exception:" + e);
            throw e;
        }

        log.debug("com.zte.cms.cpsp.ls LicesBasicLS updateUcpBasic end");
    }

    public String updateSynStatusByCpidAndStatus(String cpid, Integer status) throws DomainServiceException {
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS setSynStatusByCpidAndStatus start...");
        String message = "";
        try {
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCpid(cpid);
            ucpBasic.setCptype(LicesConstant.LISCE_TYPE_2);
            List<UcpBasic> ucpBasicList = ucpBasicDS.getUcpBasicByCond(ucpBasic);
            // ucpBasic = ucpBasicDS.getUcpBasic(ucpBasic);
            if (ucpBasicList != null && ucpBasicList.size() > 0) {
                ucpBasic = ucpBasicList.get(0);
                ucpBasic.setStatus(status);
                updateBasicStatusToDB(ucpBasic);
                message = MES_OK;
            } else {
                message = ERROR;
            }
        } catch (DomainServiceException e) {
            log.error("com.zte.cms.cpsp.ls LicesBasicLS setSynStatusByCpidAndStatus exception:" + e);
            throw e;
        }
        log.debug("com.zte.cms.cpsp.ls LicesBasicLS setSynStatusByCpidAndStatus end");
        return message;
    }

    public TableDataInfo pageInfoQueryForTable(UcpBasic ucpBasic, int start, int pageSize)
            throws DomainServiceException {
        log.debug("pageInfoQueryForTable starting...");
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("cpid");
        OperInfo operinfo = LoginMgt.getOperInfo("CMS");
        ucpBasic.setSagid(operinfo.getOperid());
        TableDataInfo ucpDataInfo = null;
        if (ucpBasic.getCpid() != null && !ucpBasic.getCpid().equals("")) {
            ucpBasic.setCpid(EspecialCharMgt.conversion(ucpBasic.getCpid()));
        }
        if (ucpBasic.getCpcnshortname() != null && !ucpBasic.getCpcnshortname().equals("")) {
            ucpBasic.setCpcnshortname(EspecialCharMgt.conversion(ucpBasic.getCpcnshortname()));
        }
        ucpDataInfo = cmsUcpBasicDS.pageInfoQuery(ucpBasic, start, pageSize, puEntity);
        log.debug("pageInfoQueryForTable end");
        return ucpDataInfo;
    }

    /**
     * 暂停、恢得
     */
    public String pauseAndRework(String cpindex, String opertype) {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        log.debug("pauseAndRework starting....");
        String result = "070000";// 设置操作结果为真
        try {
            UcpBasic ucpBasic = ucpBasicDS.getUcpBasicByIndex(Long.parseLong(cpindex));
            if (ucpBasic != null && (ucpBasic.getStatus() == 1 || ucpBasic.getStatus() == 2)) {
                if ("1".equals(opertype)) {
                    if (ucpBasic.getStatus() == 2) {
                        return "0711113"; // CP的状态不正确
                    }
                    ucpBasic.setPortstatus(1);
                    ucpBasic.setStatus(2);// 暂停
                } else {
                    if (ucpBasic.getStatus() == 1) {
                        return "0711113"; // CP的状态不正确
                    }
                    ucpBasic.setPortstatus(0);
                    ucpBasic.setStatus(1); // 正常
                }

                OperService operService = new OperService();
                operService.setSerindex(Long.parseLong(cpindex));
                List<OperService> list = operServiceDS.getOperServices(operService);
                if (list != null && list.size() > 0) {
                    for (OperService oservice : list) {
                        long operid = oservice.getOperid();
                        OperInformation operinfo = operInformationDS.getOperInformationById(operid);// 密码为原始密码
                        if (operinfo != null) {
                            operinfo.setOperpwd(encrypt.VarLenEncrypt(PurviewUtils.clearPwd(operinfo.getOperpwd()
                                    + "11"), 30));// 密码重新加密
                            if ("1".equals(opertype)) {
                                operinfo.setOperstatus(3); // 3:禁止
                            } else {
                                operinfo.setOperstatus(0);// 0:正常
                            }
                            operInformationDS.updateOperInformation(operinfo);
                        }
                    }
                    ucpBasicDS.updateUcpBasic(ucpBasic);
                    if ("1".equals(opertype)) {
                        CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                                CpspLogConstant.OPERTYPE_LISENCE_POUSE, CpspLogConstant.OPERTYPE_LISENCE_POUSE_INFO,
                                CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                    } else {
                        CommonLogUtil.insertOperatorLog(ucpBasic.getCpid(), CommonLogConstant.MGTTYPE_CPAZF,
                                CpspLogConstant.OPERTYPE_LISENCE_REWORK, CpspLogConstant.OPERTYPE_LISENCE_REWORK_INFO,
                                CpspLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                } else {
                    if ("1".equals(opertype)) {
                        return "0711111";// CP没有关联操作员，暂停无效
                    } else {
                        return "0711112";// CP没有关联操作员，恢复无效
                    }
                }
            } else {
                return "0711113"; // CP的状态不正确
            }
        } catch (DomainServiceException e) {
            result = "070001"; // 设置操作结果为假
            log.error("dao exception:" + e.getMessage());
        }
        log.debug("pauseAndRework end");
        return result;
    }

    /**
     * 判断牌照方是否关联的有操作员
     */
    public String validOper(String cpindex) {
        log.debug("validOper starting....");
        String result = "success";
        try {
            OperService operService1 = new OperService();
            operService1.setSerindex(Long.parseLong(cpindex));
            List<OperService> list1 = operServiceDS.getOperServices(operService1); // 根据cpindex查询CP和操作员关联对象集合
            if (list1 != null && list1.size() > 0) {
                result = "fail";
            }
        } catch (Exception e) {
            log.error("dao exception:" + e.getMessage());
            result = "fail";
            return result;
        }
        log.debug("validOper end");
        return result;
    }

}

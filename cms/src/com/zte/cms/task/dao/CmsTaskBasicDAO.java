package com.zte.cms.task.dao;

import java.util.List;

import com.zte.cms.task.model.TaskCmsaudit;
import com.zte.cms.task.model.TaskFunction;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.cpsp.common.model.UcpBasic;

public class CmsTaskBasicDAO extends DynamicObjectBaseDao implements ICmsTaskBasicDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    public PageInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize) throws DAOException
    {
        log.debug("page query taskBasic by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("pageQureyCmsTaskBasicCnt", taskBasic)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<UcpBasic> rsList = (List<UcpBasic>) super.pageQuery("pageQureyCmsTaskBasic",
                    taskBasic, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query taskBasic by condition end");
        return pageInfo;
    }
    
    public PageInfo pageInfoQuery(TaskCmsaudit taskBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("pageQureyCmsTaskBasic", "pageQureyCmsTaskBasicCnt", taskBasic, start, pageSize, puEntity);
    }
    
    public void insertTask(TaskCmsaudit taskBasic) throws DAOException
    {
        log.info("insert taskBasic starting...");
        super.insert("insertTask", taskBasic);
        log.info("insert taskBasic end");
    }
    public TaskCmsaudit getTaskmap(TaskCmsaudit taskmap)  throws DAOException
    {
    	log.info("query getTaskmap starting...");
    	TaskCmsaudit resultObj = (TaskCmsaudit) super.queryForObject("getTaskmap", taskmap);
        log.info("query getTaskmap end");
        return resultObj;
    }
    
    public TaskFunction getTaskFunction(TaskFunction taskfunction)  throws DAOException
    {
    	log.info("query getTaskFunction starting...");
    	TaskFunction resultObj = (TaskFunction) super.queryForObject("getTaskFunction", taskfunction);
    	log.info("query getTaskFunction end");
    	return resultObj;
    }
    
    public void updateTaskmap(TaskCmsaudit noticemap) throws DAOException
    {
    	 log.info("update notictmap by pk starting...");
         super.update("updateTaskmap", noticemap);
         log.info("update notictmap by pk end");
    }
    
    public List<TaskCmsaudit> getattchList(TaskCmsaudit taskmap) throws DAOException
    {
    	log.debug("page query taskBasic by condition starting...");
    	List<TaskCmsaudit> rList = (List<TaskCmsaudit>) super.queryForList("getAttach",
    			taskmap);
        log.debug("page query taskBasic by condition end");
        return rList;
    }
    
    public TaskCmsaudit getAttachByid(String attachid) throws DAOException
    {
    	log.info("query getAttachByid starting...");
    	TaskCmsaudit att = new TaskCmsaudit();
    	att.setAttachid(attachid);
    	TaskCmsaudit resultObj = (TaskCmsaudit) super.queryForObject("getAttachByid", att);
        log.info("query getAttachByid end");
        return resultObj;
    }
    
    public void deleteTaskBasic(TaskCmsaudit taskmap) throws DAOException
    {
        log.info("delete deleteTaskBasic starting...");
        super.delete("deleteTaskBasic", taskmap);
        log.info("delete deleteTaskBasic  end");
    }
    public void toAuditTask(TaskCmsaudit taskmap) throws DAOException
    {
    	log.info("toAuditTask starting...");
    	super.delete("toAuditTask", taskmap);
    	log.info("toAuditTask  end");
    }
    
    public List<TaskFunction> getTaskFunctList(TaskFunction taskfunction)  throws DAOException
    {
    	log.info("query getTaskFunction starting...");
    	List<TaskFunction> resultObj = (List<TaskFunction>) super.queryForList("getTaskFunction", taskfunction);
    	log.info("query getTaskFunction end");
    	return resultObj;
    }

    
}
package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.IcmTagContent;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmTagContentDAO
{
    /**
     * 新增IcmTagContent对象
     * 
     * @param icmTagContent IcmTagContent对象
     * @throws DAOException dao异常
     */
    public void insertIcmTagContent(IcmTagContent icmTagContent) throws DAOException;

    /**
     * 根据主键更新IcmTagContent对象
     * 
     * @param icmTagContent IcmTagContent对象
     * @throws DAOException dao异常
     */
    public void updateIcmTagContent(IcmTagContent icmTagContent) throws DAOException;

    /**
     * 根据主键删除IcmTagContent对象
     * 
     * @param icmTagContent IcmTagContent对象
     * @throws DAOException dao异常
     */
    public void deleteIcmTagContent(IcmTagContent icmTagContent) throws DAOException;

    /**
     * 根据主键查询IcmTagContent对象
     * 
     * @param icmTagContent IcmTagContent对象
     * @return 满足条件的IcmTagContent对象
     * @throws DAOException dao异常
     */
    public IcmTagContent getIcmTagContent(IcmTagContent icmTagContent) throws DAOException;

    /**
     * 根据条件查询IcmTagContent对象
     * 
     * @param icmTagContent IcmTagContent对象
     * @return 满足条件的IcmTagContent对象集
     * @throws DAOException dao异常
     */
    public List<IcmTagContent> getIcmTagContentByCond(IcmTagContent icmTagContent) throws DAOException;

    /**
     * 根据条件分页查询IcmTagContent对象，作为查询条件的参数
     * 
     * @param icmTagContent IcmTagContent对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmTagContent icmTagContent, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据标签删除内容关联表
     * 
     * @param tagindex 标签主键
     */
    public void removeTagContentByTagindex(Long tagindex) throws DAOException;

    /**
     * 批量删除内容关联记录
     * 
     * @param tagindex 内容标签主键
     * @param contentid 内容ID
     * 
     */
    public void removeRelationByTagindexAndConIDs(IcmTagContent icmTagContent) throws DAOException;


    /**
     * 根据tagindex查询，关联内容的所有CPID
     * 
     * @param tagindex 内容标签主键
     * @return 与该标签关联内容的CPID列表
     */
    public List<String> getConCpidListByTagIndex(Long tagindex) throws DAOException;
}
function isMaxLengthWide(input_id,len,bool){

	if(utf8_strlenWide(TrimWide($('#'+input_id).val())) > len){
		focusAndSelect(input_id,bool);
		return false;
    }else{
       return true;   
    }
 }
 
function utf8_strlenWide(str) 
{ 
    var cnt = 0; 
    for( i=0; i<str.length; i++) 
    { 
        var value = str.charCodeAt(i); 
        if( value < 0x080) 
        { 
            cnt += 1; 
        } 
        else if( value < 0x0800) 
        { 
            cnt += 2; 
        } 
        else  if(value < 0x010000)
        { 
            cnt += 3; 
        }
		else
		{
			cnt += 4;
		}
    } 
    return cnt; 
}

/**
 * @param {String} input_id
 * @param (boolean) bool
 * Select the current error data 
 */
function focusAndSelect(input_id,bool){
	bool?$('#'+input_id).one('click',function(){$(this).focus();$(this).select();}):0;
	$('#'+input_id).click()
}

/**
 * 
 * @param {Object} str
 */
function TrimWide(str) 
{
	return RTrimWide(LTrimWide(str));
};

function RTrimWide(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};

function LTrimWide(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
}

function checkNull(str){
     if(str.trim()==""){
       return true;
     }
     return false;
}

function popcpsp(){
      var page = "../cpsp/popCpsp.html";
      var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
      var rtnobj = window.showModalDialog(page, "", sFeatures);

      if(typeof(rtnobj) != "undefined"){
	   $("#cpcnshortname").val(rtnobj.cpcnshortname);
	   $("#cpindex").val(rtnobj.cpindex);
	   $("#cpid").val(rtnobj.cpid);    
	   callSid('getCpBase',rtnobj.cpid);
     }
} 
    
 //查看输入的值是不是按照规定字符开头
String.prototype.startWith=function(str){
  	if(str==null||str==""||this.length==0||str.length>this.length)
   		return false;
    if(this.substr(0,str.length)==str)
       return true;
    else
        return false;
    return true;
}
	  
//特殊字符判断
function checkUnallowedCharacter(str){

	var pattern = /[!@#$%\^&\*;\|\?\(\)\[\]\{\}'"<>]/;
	return pattern.test(str);
} 

    var data={};
function setTableData(tableData,tableId)
{		
        ctrlSelectCheckbox("cmsProgramtypeTable");
	    data = getTableById('cmsProgramtypeTable').data;
        noDataTrips(tableData, tableId);
    
        $("A[name='modify_link']").each(query_modify_action);
        $("A[name='del_link']").each(query_del_action);
        //$("A[name='del_link']").each(query_action);
        $("A[name='view_link']").each(query_view_action);
        //$("input[name='typ_ch']").each(query_action);
}

function query_modify_action(i){   
   
    if (i > 0)
      {
        var status=data[i-1].publishstatus;
        if (status!=10&&right4modify)
        {
            this.style.display = "";
        }
        else
        {
            this.style.display = "none";
        }
      }
      
      }
      
      function query_del_action(i){   
   
    if (i > 0)
      {
        var status=data[i-1].publishstatus;
        if (status!=10&&right4dele)
        {
            this.style.display = "";
        }
        else
        {
            this.style.display = "none";
        }
      }
      }
    
   
   function query_view_action(i){   
   
    if (i > 0)
      {
        if (right4view)
        {
            this.style.display = "";
        }
        else
        {
            this.style.display = "none";
        }
      }
   
/**
   var modEx=/^mod.*$/;//这里匹配的是所有name以mod为开头的超链接
   var viewEx=/^view.*$/;
   var delEx=/^del.*$/;	
   var modResult=aSelectorByID(this.id,modEx);
   var viewResult=aSelectorByID(this.id,viewEx);
   var delResult=aSelectorByID(this.id,delEx);
   if(i>0)
	{
		var status=data[i-1].publishstatus;
		if(viewResult){
		this.style.display="none";
		}
		if(modResult){
		this.style.display="none";
		}
		if(delResult){
		this.style.display="none";
		}
	}
/**
	var modEx=/^mod.*$/;//这里匹配的是所有name以mod为开头的超链接
	var delEx=/^del.*$/;	
	var viewEx=/^view.*$/;	
	var typeidEx=/^typ.*$/;
	var modResult=aSelectorByID(this.id,modEx);
	var delResult=aSelectorByID(this.id,delEx);
	var viewResult=aSelectorByID(this.id,viewEx);
	var typeidResult=aSelectorByID(this.id,typeidEx);
	alert(modResult+"--"+delResult+"--"+viewResult+"--"+typeidResult);
    
	if(i>0)
	{
		var status=data[i-1].publishstatus;
		var typeindex=data[i-1].typeindex;
		var typeid=data[i-1].typeid;
		if(typeindex==0){
		  		  
		  if(delResult){
		    this.style.display="none";	
		  }
		}
		
		if(viewResult&&!right4view){
		  this.style.display="none";
		}

		if(!right4modify&&modResult){
		  this.style.display="none";
		}
		
		if(!right4dele&&delResult){
		  this.style.display="none";
		}
			
		if(status==21){
		  if(delResult){
			this.style.display="none";		
	      }
		  if(modResult){
			this.style.display="none";
		  }				
		}
			
		if(status==23){
		  if(delResult){
			this.style.display="none";
		  }
		  if(modResult){
			this.style.display="none";
		  }
		}
			
		if(status==41){
		  if(delResult){
			this.style.display="none";
		  }
		}
			
		if(status==42){ 
		  if(modResult){
			this.style.display="none";
		  }
		  if(delResult){
			this.style.display="none";
		  }
		}
	}
	**/
}

function aSelectorByID(id,exp){
		return exp.test(id);
}

var right4view = false;
	var right4add = false;
    var right4modify = false;
    var right4dele = false;
    var right4publish = false;
    var right4canclepublish = false;
    function getCPPurview(){
    	if (getPurview("sys.basicdata.programtype.add") == "1") right4add = true;
	    if (getPurview("sys.basicdata.programtype.modify") == "1") right4modify = true;
	    if (getPurview("sys.basicdata.programtype.del") == "1") right4dele = true;
	    if (getPurview("sys.basicdata.programtype.detail") == "1") right4view = true;
	    if (getPurview("sys.basicdata.programtype.addsync") == "1") right4publish = true;
	    if (getPurview("sys.basicdata.programtype.delsync") == "1") right4canclepublish = true;
	    
	    if (right4add) {
            $("#btn_add").show();
        } else {
            $("#btn_add").hide();
        }
   		if (right4publish) {
            $("#btn_publish").show();
        } else {
            $("#btn_publish").hide();
        }
    	if (right4canclepublish) {
            $("#btn_canclepublish").show();
        } else {
            $("#btn_canclepublish").hide();
        }
    }
    
    
    function setStatus(keyvalue){
     var haschild=keyvalue['publishstatus'];
  	 if(haschild == 10) 
  	   keyvalue['publishstatus']=$res_entry("msg.info.status.13");
  	    
  	 else if(haschild == 21) 
  	    keyvalue['publishstatus']=$res_entry("msg.info.status.10");
  	    
  	 if(haschild == 23) 
  	   keyvalue['publishstatus']=$res_entry("msg.info.status.11"); 
  	   
  	 else if(haschild == 41) 
  	    keyvalue['publishstatus']=$res_entry("msg.info.status.03");
  	    
 	 else if(haschild == 42)
 	    keyvalue['publishstatus']=$res_entry("msg.info.status.12");
 	
  	 return keyvalue;
  	
   } 
   
   function publishProgramtype(clslevel){
     if(clslevel=="1"){
     	savePageStatus("cmsProgramtypeTable","programtypeClslevel1");
     }else{
     	savePageStatus("cmsProgramtypeTable","programtype_Clslevel_MoreThan1");
     }
     var checkedIndexList = $('input[name="typ_ch"][checked]');
     if(checkedIndexList.length != 0){
     
       if(window.confirm($res_entry("msg.info.basicdata.016"))){
	     var cmsList = new Array(); 
     
         for(var i=0;i<checkedIndexList.length;i++){
           var cmsprogramtype={};
           cmsprogramtype['typeid'] = checkedIndexList[i]['typeid'];
           cmsprogramtype['typename'] = checkedIndexList[i]['typename'];
           cmsList.push(cmsprogramtype);
         }
	     
         callSid('publishProgramtype',cmsList);
       }    
     }     
     else{
       openMsgBoxSize($res_entry("msg.info.basicdata.017"));
       return false;
     }      
   }
   
   //取消发布
   function canclePuhlish(clslevel){
     if(clslevel=="1"){
     	savePageStatus("cmsProgramtypeTable","programtypeClslevel1");
     }else{
     	savePageStatus("cmsProgramtypeTable","programtype_Clslevel_MoreThan1");
     }
     var checkedIndexList = $('input[name="typ_ch"][checked]');
     if(checkedIndexList.length != 0){
     
       if(window.confirm($res_entry("msg.info.basicdata.117"))){
	     var cmsList = new Array(); 
         for(var i=0;i<checkedIndexList.length;i++){
           var cmsprogramtype={};
           cmsprogramtype['typeid'] = checkedIndexList[i]['typeid'];
           cmsprogramtype['typename'] = checkedIndexList[i]['typename'];
       
           cmsList.push(cmsprogramtype);
         }
	     
         callSid('cancelPulishProgramtype',cmsList);
       }    
     }     
     else{
       openMsgBoxSize($res_entry("msg.info.basicdata.018"));
       return false;
     }     
   }
   
    //修改刷新
   function freshParent(obj){    
     var cmsprogramtype  = obj['cmsprogramtype'];
     var errorcode=obj['result'];

	 programtype=new Object();
     programtype['typeid']=cmsprogramtype['parentid'];
     callUrl("cmsProgramtypeFacade/getProgramtypeByCond.ssm",programtype,'rtnProgramtype',true);//得到父节点
     if(rtnProgramtype!=null){
     	rtnProgramtype=rtnProgramtype.rtnValue;
     	var treeTypeindex=rtnProgramtype["typeindex"];
     }
     var code=errorcode.split(":");
     var url="CMS/basicdata/cmspProgramtypeList.html?typeindex="+treeTypeindex;
     if(errorcode=="1"){//对象不存在
     	if(cmsprogramtype['clslevel']=="1"){
     		parent.initTree();
     	}else{     	    
     	    if(rtnProgramtype==null){//如果父节点也不存在
     	    	rtnProgramtype=new Object();
     	    	callUrl("cmsProgramtypeFacade/getFirstRootProgramtype.ssm",null,"rtnProgramtype",true);//得到第一个根节点
			    rtnProgramtype=rtnProgramtype.rtnValue;
			    var programtype=rtnProgramtype[0];
			    url="CMS/basicdata/cmspProgramtypeList.html?typeindex="+programtype['typeindex'];
     	    }else{
     			url='CMS/basicdata/programtypeList.html?cpid='+_para.cpid+'&clslevel='+_para.clslevel+"&typeindex="+_para.parindex+"&treeClslevel="+_para.treeClslevel;
     	    }
     	}
     	openMsg("1:"+$res_entry("msg.info.basicdata.048"),encodeURIComponent(url),true);
     }else{
     	if(cmsprogramtype['clslevel']==1){//如果是一级节点
     		if(code[0]=="0"){//操作成功
     			parent.freshTree(cmsprogramtype);
     			openMsg("0:"+$res_entry("msg.info.basicdata.028"),encodeURIComponent(url),true);
     		}else{
     			openMsg(errorcode,encodeURIComponent(url),true);
     		}
     	}else{
     		url='CMS/basicdata/programtypeList.html?cpid='+_para.cpid+'&clslevel='+_para.clslevel+"&typeindex="+_para.parindex+"&treeClslevel="+_para.treeClslevel;
     		if(code[0]=="0"){//修改成功
     			parent.freshTree(cmsprogramtype);
     			openMsg("0:"+$res_entry("msg.info.basicdata.028"),encodeURIComponent(url),true);
     		}else{
     			parent.freshTree(cmsprogramtype);
     			openMsg(errorcode,encodeURIComponent(url),true);
     		}
     	}
     }
   }
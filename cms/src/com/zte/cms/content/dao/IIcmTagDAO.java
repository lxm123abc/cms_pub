package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.IcmTag;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmTagDAO
{
    /**
     * 新增IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DAOException dao异常
     */
    public void insertIcmTag(IcmTag icmTag) throws DAOException;

    /**
     * 根据主键更新IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DAOException dao异常
     */
    public void updateIcmTag(IcmTag icmTag) throws DAOException;

    /**
     * 根据主键删除IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DAOException dao异常
     */
    public void deleteIcmTag(IcmTag icmTag) throws DAOException;

    /**
     * 根据主键查询IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @return 满足条件的IcmTag对象
     * @throws DAOException dao异常
     */
    public IcmTag getIcmTag(IcmTag icmTag) throws DAOException;

    /**
     * 根据条件查询IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @return 满足条件的IcmTag对象集
     * @throws DAOException dao异常
     */
    public List<IcmTag> getIcmTagByCond(IcmTag icmTag) throws DAOException;

    /**
     * 根据条件分页查询IcmTag对象，作为查询条件的参数
     * 
     * @param icmTag IcmTag对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmTag对象，作为查询条件的参数
     * 
     * @param icmTag IcmTag对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize, PageUtilEntity puEntity) throws DAOException;

    /**
     * 返回内容标签列表
     */
    public List<IcmTag> getIcmTagList(IcmTag icmTag) throws DAOException;

    /**
     * 根据条件分页查询IcmTag对象，作为查询条件的参数
     * 
     * @param icmTag IcmTag对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoUnclearQuery(IcmTag icmTag, int start, int pageSize) throws DAOException;

}
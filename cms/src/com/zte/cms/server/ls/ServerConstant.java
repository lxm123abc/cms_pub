package com.zte.cms.server.ls;

public class ServerConstant
{

    public static final String CMS_SERV_RES_FILE_NAME = "cms_server_resource";// 资源文件名
    public static final String CMS_SERV_RES_PREFIX = "cms.server.msg.";// 操作码前缀

    public static final String RES_CMS_SERV_LOG_OBJ_SEROBJ = "cms.server.log.obj";
    public static final String RES_CMS_SERV_UNKNOW = "cms.server.log.unknow";

    public static final String RES_CMS_SERV_LOG_ADD = "cms.server.log.add";
    public static final String RES_CMS_SERV_LOG_UPDATE = "cms.server.log.update";
    public static final String RES_CMS_SERV_LOG_DELETE = "cms.server.log.delete";

    public static final String RES_CMS_SERV_LOG_ADDED = "cms.server.log.added";
    public static final String RES_CMS_SERV_LOG_UPDATED = "cms.server.log.updated";
    public static final String RES_CMS_SERV_LOG_DELETED = "cms.server.log.deleted";

    public static final String RES_CMS_SERV_LOG_SUCCESS = "cms.server.log.success";
    public static final String RES_CMS_SERV_LOG_FAIL = "cms.server.log.false";

    public static final String RES_CMS_SERV_LOG_OPER_PREFIX = "cms.server.log.oper.prefix";

    public static final String RES_CMS_SERV_SERVER_EXIST = "cms.server.msg.server.exist";
    public static final String RES_CMS_SERV_SERVER_INIP_EXIST = "cms.server.msg.server.inip.exist";
    public static final String RES_CMS_SERV_SERVER_OUTIP_EXIST = "cms.server.msg.server.outip.exist";
    public static final String RES_CMS_SERV_SERVER_ADD_SUCC = "cms.server.msg.server.add.success";
    public static final String RES_CMS_SERV_SERVER_UPDATE_SUCC = "cms.server.msg.server.update.success";
    public static final String RES_CMS_SERV_SERVER_NOTEXIST = "cms.server.msg.server.notexist";
    public static final String RES_CMS_SERV_SERVER_DELETE_SUCC = "cms.server.msg.server.del.success";
    public static final String RES_CMS_SERV_SERVER_OPER_SUCCESSFUL = "cms.serv.msg.oper.successful";
}

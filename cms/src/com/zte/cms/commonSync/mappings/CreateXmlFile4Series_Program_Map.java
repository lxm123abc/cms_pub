package com.zte.cms.commonSync.mappings;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.framework.base.ISysCode;

public class CreateXmlFile4Series_Program_Map extends AbstractCreateXmlFile4Object
{

    private static final String CONTENT_SINGERLANG_CODEKEY="cms_content_singerlang";
    private static final String SERVICE_KEY="CMS";

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;

        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        String fileName = "series_program_20wg"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"series"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");   
        Element mappingElement = adiElement.addElement("Mappings");
        List<SeriesProgramMap> seriesProgramMapList = (List<SeriesProgramMap>)objectMap.get("seriesProgramMap");
        if (null == seriesProgramMapList || 0 == seriesProgramMapList.size())
        {
            return null;
        }
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");
        mapObjectElement.addAttribute("ParentType", "Series");
        mapObjectElement.addAttribute("ID", "");
        mapObjectElement.addAttribute("ParentID", seriesProgramMapList.get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("ParentCode", seriesProgramMapList.get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("ElementType", "Program");
        mapObjectElement.addAttribute("ElementID", seriesProgramMapList.get(0).getProcpcontentid().toString());
        mapObjectElement.addAttribute("ElementCode", seriesProgramMapList.get(0).getProcpcontentid().toString());
        mapObjectElement.addAttribute("Action", action);
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", seriesProgramMapList.get(0).getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);        
        writeDom(dom,fileFullName);      
        return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType) throws DomainServiceException
    {
        String action = null;
        if (1 == syncType || 2 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
        return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;        
        String targetTypeName ="";
        if (targetType == 1)
        {
        	targetTypeName = "series_program_bms";
        }
        else if (targetType == 2)
        {
        	targetTypeName = "series_program_epg";
        }
        Date date = new Date();
        String fileName = targetTypeName+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"series"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects"); 
        if ((2==targetType && 2!=syncType)||(1==targetType &&2!=syncType))//mapping关系只发到epg,bms，且更新时不带mapping
        {
            List<SeriesProgramMap> seriesProgramMapList = (List<SeriesProgramMap>)objectMap.get("seriesProgramMap");  
            if (null == seriesProgramMapList || 0 == seriesProgramMapList.size())
            {
                return null;
            }
            Element mappingElement = adiElement.addElement("Mappings");
            Element mapObjectElement = null;
            Element mapPertyElement = null;
            mapObjectElement = mappingElement.addElement("Mapping");
            mapObjectElement.addAttribute("ParentType", "Series");
            mapObjectElement.addAttribute("ParentID",seriesProgramMapList.get(0).getSeriesid().toString());
            mapObjectElement.addAttribute("ElementType", "Program");
            mapObjectElement.addAttribute("ElementID", seriesProgramMapList.get(0).getProgramid().toString());
            mapObjectElement.addAttribute("ObjectID", seriesProgramMapList.get(0).getMappingid().toString());
            mapObjectElement.addAttribute("Action", action);
            writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
            writeElement(mapObjectElement, mapPertyElement, "Sequence", seriesProgramMapList.get(0).getSequence().toString(), false);
            writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
            writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);            
        }
        writeDom(dom,fileFullName);
        return retPath;
    }

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {

        String action = null;

        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        Map<String, String> mappingMap = null;
        List<Map> mapList = null;  
        Date date = new Date();
        String fileName = "series_program_20ys"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"series"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
        ISysCode sysCode=(ISysCode)SSBBus.findDomainService("sysCode");
        HashMap<String ,String > langMap=sysCode.getSysCodeMapByCodekey(CONTENT_SINGERLANG_CODEKEY,SERVICE_KEY);
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");   
        Element mappingElement = adiElement.addElement("Mappings");
        List<SeriesProgramMap> seriesProgramMapList = (List<SeriesProgramMap>)objectMap.get("seriesProgramMap");
        if (null == seriesProgramMapList || 0 == seriesProgramMapList.size())
        {
            return null;
        }
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");
        mapObjectElement.addAttribute("ParentType", "Series");
        mapObjectElement.addAttribute("ID", "");
        mapObjectElement.addAttribute("ParentID", seriesProgramMapList.get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("ParentCode", seriesProgramMapList.get(0).getCpcontentid().toString());
        mapObjectElement.addAttribute("ElementType", "Program");
        mapObjectElement.addAttribute("ElementID", seriesProgramMapList.get(0).getProcpcontentid().toString());
        mapObjectElement.addAttribute("ElementCode", seriesProgramMapList.get(0).getProcpcontentid().toString());
        mapObjectElement.addAttribute("Action", action);
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", seriesProgramMapList.get(0).getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);        
        writeDom(dom,fileFullName);      
        return retPath;
    
    }

}

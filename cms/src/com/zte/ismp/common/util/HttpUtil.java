package com.zte.ismp.common.util;

import java.net.*;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.log4j.Logger;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.*;

/**
 * <p>
 * 文件名称: HttpUtil.java
 * </p>
 * <p>
 * 文件描述: Http操作类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2004
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: Http操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2006年7月12日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * <p>
 * 修改记录2：
 * </p>
 * 
 * @version 1.0
 * @author 孔建华
 */

public class HttpUtil
{
    // ****** 代码段: 常量定义 *******************************************************************************/
    public static final int STATE_SUCCESS = 0;
    public static final int STATE_FAIL = 1;

    // ****** 代码段: 变量定义 *******************************************************************************/
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private URL url = null;
    private HttpURLConnection httpUrlConn = null;
    private String urlStr = null;
    private String responseContent = null;

    // ****** 代码段: 构造方法 *******************************************************************************/
    public HttpUtil()
    {
    }

    // ****** 代码段: 公共方法 *******************************************************************************/
    public void setUrlStr(String urlStr)
    {
        this.urlStr = urlStr;
    }

    public String getResponseContent()
    {
        return responseContent;
    }

    private void setResponseContent(String responseContent)
    {
        this.responseContent = responseContent;
    }

    /**
     * 通过设置参数发送URL，借用HttpURLConnection
     * 
     * @param param String
     * @return int
     */
    public int sendUrl(String param)
    {
        try
        {
            url = new URL(urlStr);
            httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setRequestMethod("POST");
            httpUrlConn.setDoOutput(true);
            httpUrlConn.getOutputStream().write(param.getBytes());
            httpUrlConn.getOutputStream().flush();
            httpUrlConn.getOutputStream().close();

            InputStream in = httpUrlConn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(in));
            StringBuffer tempStr = new StringBuffer();
            while (rd.read() != -1)
            {
                tempStr.append(rd.readLine());
            }
            setResponseContent(new String(tempStr));
            return this.STATE_SUCCESS;
        }
        catch (Exception e)
        {
            logger.error("[HTTP-ERROR]: ", e);
            return this.STATE_FAIL;
        }
        finally
        {
            if (httpUrlConn != null)
            {
                httpUrlConn.disconnect();
            }
        }
    }

    /**
     * 通过URL和Param发送HTTP命令
     * 
     * @param urlStr String
     * @param param String
     * @return int
     */
    public int sendUrl(String urlStr, String param)
    {
        try
        {
            url = new URL(urlStr);
            httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setRequestMethod("POST");
            httpUrlConn.setDoOutput(true);
            httpUrlConn.getOutputStream().write(param.getBytes());
            httpUrlConn.getOutputStream().flush();
            httpUrlConn.getOutputStream().close();

            InputStream in = httpUrlConn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(in));
            StringBuffer tempStr = new StringBuffer();
            while (rd.read() != -1)
            {
                tempStr.append(rd.readLine());
            }
            setResponseContent(new String(tempStr));
            return this.STATE_SUCCESS;
        }
        catch (Exception e)
        {
            logger.error("[HTTP-ERROR]: ", e);
            return this.STATE_FAIL;
        }
        finally
        {
            if (httpUrlConn != null)
            {
                httpUrlConn.disconnect();
            }
        }
    }

    /**
     * 发送Url，以数据流的方式
     * 
     * @param urlStr String
     * @param param String
     * @return String
     */
    public String sendUrlByStream(String urlStr, String param)
    {
        try
        {
            // 建立连接
            url = new URL(urlStr + "?" + param);
            URLConnection urlconn = (URLConnection) url.openConnection();
            // 设置连接属性
            urlconn.setDoOutput(true);
            urlconn.setUseCaches(false);
            urlconn.setRequestProperty("Content-type", "application/octest-stream");

            // 生成数据流
            String buf = "aaaaaaaa";
            DataOutputStream dataout = new DataOutputStream(urlconn.getOutputStream());
            dataout.writeUTF(buf);
            dataout.flush();
            dataout.close();
            // 接受servlet的响应数据
            DataInputStream in = new DataInputStream(urlconn.getInputStream());
            String response = in.readUTF();
            // System.out.println("read from server :" + response);
            in.close();
            return response;
        }
        catch (Exception e)
        {
            logger.error("[HTTP-ERROR]: ", e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 发送URL和BUFF流
     * 
     * @param urlStr String
     * @param param String
     * @param buff String
     * @return String
     */
    public String sendUrlByStream(String urlStr, String param, String buff)
    {
        try
        {
            // 建立连接
            url = new URL(urlStr + "?" + param);
            URLConnection urlconn = (URLConnection) url.openConnection();
            // 设置连接属性
            urlconn.setDoOutput(true);
            urlconn.setUseCaches(false);
            urlconn.setRequestProperty("Content-type", "application/octest-stream");

            // 生成数据流
            DataOutputStream dataout = new DataOutputStream(urlconn.getOutputStream());
            dataout.writeUTF(buff);
            dataout.flush();
            dataout.close();
            // 接受servlet的响应数据
            DataInputStream in = new DataInputStream(urlconn.getInputStream());
            String response = in.readUTF();
            in.close();
            return response;
        }
        catch (Exception e)
        {
            logger.error("[HTTP-ERROR]: ", e);
            e.printStackTrace();
            return null;
        }
    }

    public String sendUrlByStream(String urlStr, String param, File file)
    {
        try
        {
            // 建立连接
            url = new URL(urlStr + "?" + param + "&filename" + file.getName());
            URLConnection urlconn = (URLConnection) url.openConnection();
            // 设置连接属性
            urlconn.setDoOutput(true);
            urlconn.setUseCaches(false);
            urlconn.setRequestProperty("Content-type", "application/octest-stream");

            // 生成数据流
            DataOutputStream dataout = new DataOutputStream(urlconn.getOutputStream());
            FileInputStream is = new FileInputStream(file);
            byte[] buf = new byte[2048];
            int len;
            while ((len = is.read(buf)) > 0)
            {
                String buf1 = new String(buf, 0, len);
                dataout.write(buf, 0, len);
            }
            is.close();
            dataout.flush();
            dataout.close();
            // 接受servlet的响应数据
            DataInputStream in = new DataInputStream(urlconn.getInputStream());
            String response = in.readUTF();
            // System.out.println("read from server :" + response);
            in.close();
            return response;
        }
        catch (Exception e)
        {
            logger.error("[HTTP-ERROR]: ", e);
            return null;
        }
    }

    // ****** 代码段: 调试信息 *******************************************************************************/
    public static void main(String[] args)
    {
        HttpUtil http = new HttpUtil();
        // http.setUrlStr("http://127.0.0.1:8083/download/client/");
        // http.sendUrl("zipfilepath=/client/fightlord.zip&unzipdir=/client/fightlord&flag=unzip");
        // String res = http.getResponse_content();
        // "http://localhost:8083/DataReceiveServlet?userid=lhz&tel=1111111111&type=0&length=8"
        String url = "http://10.40.43.17:8083/download/DataReceiveServlet";
        // String url = "http://10.40.43.17:8083/ad/datachangedlistener";
        String param = "ACTION=DEL&uri_from=/client/fightlord.zip&uri_to=/client/fightlord&cmdcode=unzip";
        // File f = new File("e:/iptvams111.zip");
        // String res = http.sendUrl(url, param, f);
        // String res = http.sendUrlByStream(url, param);
        http.sendUrl(url, param);

        // res = http.sendUrl(url,"filepath=/client/fightlord/suite.jad&flag=readprop");
        // res = http.sendUrl(url,"filepath=/client/fightlord.zip&flag=delfile");
        // System.out.println(res);
    }

}

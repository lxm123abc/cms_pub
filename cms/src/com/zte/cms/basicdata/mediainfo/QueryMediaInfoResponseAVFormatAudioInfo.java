/**
 * QueryMediaInfoResponseAVFormatAudioInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoResponseAVFormatAudioInfo implements java.io.Serializable
{
    /* 音频编码类型 */
    private long audioCodingFormat;

    /* 音频编码子类型 */
    private long audioSubType;

    /* 声道数目 */
    private long channels;

    /* 音频码率 bits/s */
    private long audioDataRate;

    /* 每秒采样数量 */
    private long audioSamplingFreq;

    /* 采样位数 */
    private long audioBitDepth;

    /* 每采样字节数量 */
    private long blockAlign;

    public QueryMediaInfoResponseAVFormatAudioInfo()
    {
    }

    public QueryMediaInfoResponseAVFormatAudioInfo(long audioCodingFormat, long audioSubType, long channels,
            long audioDataRate, long audioSamplingFreq, long audioBitDepth, long blockAlign)
    {
        this.audioCodingFormat = audioCodingFormat;
        this.audioSubType = audioSubType;
        this.channels = channels;
        this.audioDataRate = audioDataRate;
        this.audioSamplingFreq = audioSamplingFreq;
        this.audioBitDepth = audioBitDepth;
        this.blockAlign = blockAlign;
    }

    /**
     * Gets the audioCodingFormat value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return audioCodingFormat * 音频编码类型
     */
    public long getAudioCodingFormat()
    {
        return audioCodingFormat;
    }

    /**
     * Sets the audioCodingFormat value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param audioCodingFormat * 音频编码类型
     */
    public void setAudioCodingFormat(long audioCodingFormat)
    {
        this.audioCodingFormat = audioCodingFormat;
    }

    /**
     * Gets the audioSubType value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return audioSubType * 音频编码子类型
     */
    public long getAudioSubType()
    {
        return audioSubType;
    }

    /**
     * Sets the audioSubType value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param audioSubType * 音频编码子类型
     */
    public void setAudioSubType(long audioSubType)
    {
        this.audioSubType = audioSubType;
    }

    /**
     * Gets the channels value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return channels * 声道数目
     */
    public long getChannels()
    {
        return channels;
    }

    /**
     * Sets the channels value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param channels * 声道数目
     */
    public void setChannels(long channels)
    {
        this.channels = channels;
    }

    /**
     * Gets the audioDataRate value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return audioDataRate * 音频码率 bits/s
     */
    public long getAudioDataRate()
    {
        return audioDataRate;
    }

    /**
     * Sets the audioDataRate value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param audioDataRate * 音频码率 bits/s
     */
    public void setAudioDataRate(long audioDataRate)
    {
        this.audioDataRate = audioDataRate;
    }

    /**
     * Gets the audioSamplingFreq value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return audioSamplingFreq * 每秒采样数量
     */
    public long getAudioSamplingFreq()
    {
        return audioSamplingFreq;
    }

    /**
     * Sets the audioSamplingFreq value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param audioSamplingFreq * 每秒采样数量
     */
    public void setAudioSamplingFreq(long audioSamplingFreq)
    {
        this.audioSamplingFreq = audioSamplingFreq;
    }

    /**
     * Gets the audioBitDepth value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return audioBitDepth * 采样位数
     */
    public long getAudioBitDepth()
    {
        return audioBitDepth;
    }

    /**
     * Sets the audioBitDepth value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param audioBitDepth * 采样位数
     */
    public void setAudioBitDepth(long audioBitDepth)
    {
        this.audioBitDepth = audioBitDepth;
    }

    /**
     * Gets the blockAlign value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @return blockAlign * 每采样字节数量
     */
    public long getBlockAlign()
    {
        return blockAlign;
    }

    /**
     * Sets the blockAlign value for this QueryMediaInfoResponseAVFormatAudioInfo.
     * 
     * @param blockAlign * 每采样字节数量
     */
    public void setBlockAlign(long blockAlign)
    {
        this.blockAlign = blockAlign;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj)
    {
        if (!(obj instanceof QueryMediaInfoResponseAVFormatAudioInfo))
            return false;
        QueryMediaInfoResponseAVFormatAudioInfo other = (QueryMediaInfoResponseAVFormatAudioInfo) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null)
        {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && this.audioCodingFormat == other.getAudioCodingFormat()
                && this.audioSubType == other.getAudioSubType() && this.channels == other.getChannels()
                && this.audioDataRate == other.getAudioDataRate()
                && this.audioSamplingFreq == other.getAudioSamplingFreq()
                && this.audioBitDepth == other.getAudioBitDepth() && this.blockAlign == other.getBlockAlign();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode()
    {
        if (__hashCodeCalc)
        {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAudioCodingFormat();
        _hashCode += getAudioSubType();
        _hashCode += getChannels();
        _hashCode += getAudioDataRate();
        _hashCode += getAudioSamplingFreq();
        _hashCode += getAudioBitDepth();
        _hashCode += getBlockAlign();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            QueryMediaInfoResponseAVFormatAudioInfo.class, true);

    static
    {
        typeDesc
                .setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>>QueryMediaInfoResponse>AVFormat>AudioInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioCodingFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioCodingFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioSubType");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioSubType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channels");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "Channels"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioDataRate");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioDataRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioSamplingFreq");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioSamplingFreq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audioBitDepth");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AudioBitDepth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blockAlign");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "BlockAlign"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}

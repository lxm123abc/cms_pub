package com.zte.cms.service.ls;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;

/**
 * 服务模块的管理接口，包括模块的增删改查、审核、发布、取消发布、打包内容操作
 * 
 * @author shaoyan
 * 
 */

public interface IServiceLS
{

    /**
     * 查询服务列表 参数为三个，调用四个参数的方法
     * 
     * @param service 查询的条件对象
     * @param start 起始行
     * @param pageSize 每页显示行数
     * @return 返回查询的列表
     * @throws Exception
     */

    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize) throws Exception;

    /**
     * 查询服务列表 参数为四个参数
     * 
     * @param service 传入对象
     * @param start 起始行
     * @param pageSize 每页显示行数
     * @param puEntity 默认排序
     * @return 查询得到的列表
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * 根据index查询服务
     * 
     * @param service
     * @return
     * @throws Exception
     */
    public Service getService(Service service) throws Exception;

    /**
     * 删除服务
     * 
     * @param service
     * @return
     * @throws Exception
     */
    public String removeService(Service service) throws Exception;

    /**
     * 新增服务
     * 
     * @param service
     * @return
     * @throws Exception
     */
    public String addService(Service service) throws Exception;

    /**
     * 修改服务
     * 
     * @param service
     * @return
     * @throws Exception
     */
    public String updateService(Service service) throws Exception;

    /**
     * 得到历史表
     * 
     * @param serviceWkfwhis
     * @return
     * @throws Exception
     */
    public ServiceWkfwhis getServiceWkfwHis(ServiceWkfwhis serviceWkfwhis) throws Exception;

    /**
     * 发布操作
     * 
     * @param serviceList
     * @return
     * @throws Exception
     */
    public String servicePublish(String serviceIndexAll) throws Exception;

    /**
     * 查询用来发布的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize) throws Exception;

    /**
     * 查询用来发布的内容
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * 查询可以取消发布的操作
     * 
     * @param service
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize) throws Exception;

    /**
     * 查询可以取消发布的服务
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * 服务取消发布操作，取消新增发布
     * 
     * @param serviceIndexAll 页面选择的所有希望取消发布的服务的index拼接而成的字符串，以“：”分隔
     * @return
     * @throws Exception
     */
    // 服务的取消发布操作,批量取消发布
    public String serviceDelPublish(String serviceIndexAll) throws Exception;

    /**
     * 修改工作流审核通过后，生成发布任务
     * 
     * @param serviceIndex
     * @throws Exception
     */
    public void serviceUpdatePublish(Long serviceIndex) throws Exception;

    /**
     * 审核页面展示的信息为修改后的信息
     * 
     * @param servicebuf
     * @return
     * @throws Exception
     */
    public ServiceBuf getServiceBuf(ServiceBuf servicebuf) throws Exception;
    
    /**
     * 批量添加服务和目标系统的关联关系
     * @param serviceMaps 必须包含serviceinde信息
     * @param targetsystemMaps 必须包含targerindex信息和paltform信息
     * @return
     * @throws Exception
     */
    public String insertTargetBindMap(List<Targetsystem> targetsystemList,List<Service> serviceList )throws Exception;
    
    /**
     * 用于在服务添加新的网元后，对其打包的直播频道在此网元上添加关联关系
     * @param service
     * @param targetsystem
     * @throws Exception
     */
    public void addServiceChannelMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception;

    /**
     * 用于在服务添加新的网元后，对其打包的电视剧内容在此网元上添加关联关系
     * @param service
     * @param targetsystem
     * @throws Exception
     */
    public void addServiceSeriesMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception;

    /**
     * 用于在服务添加新的网元后，对其打包的点播内容在此网元上添加关联关系
     * @param service
     * @param targetsystem
     * @throws Exception
     */
    public void addServiceProgramMapInAddTargetsystem(Service service, Targetsystem targetsystem) throws Exception;

    /**
     * 删除服务已经关联的平台
     * @param serviceindex 服务索引
     * @param targetsystem 关联的平台
     * @return
     * @throws Exception
     */
    public String removeTargetFromBind(Long serviceindex,Long targetindex)throws Exception;
}

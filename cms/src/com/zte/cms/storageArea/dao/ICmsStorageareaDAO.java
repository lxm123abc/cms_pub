package com.zte.cms.storageArea.dao;

import java.util.List;

import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStorageareaDAO
{
    /**
     * 新增CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DAOException dao异常
     */
    public void insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException;

    /**
     * 根据主键更新CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DAOException dao异常
     */
    public void updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException;

    /**
     * 根据主键删除CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DAOException dao异常
     */
    public void deleteCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException;

    /**
     * 根据主键查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return 满足条件的CmsStoragearea对象
     * @throws DAOException dao异常
     */
    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException;

    /**
     * 根据条件查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return 满足条件的CmsStoragearea对象集
     * @throws DAOException dao异常
     */
    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DAOException;

    /**
     * 根据条件分页查询CmsStoragearea对象，作为查询条件的参数
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsStoragearea对象，作为查询条件的参数
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
package com.zte.cms.service.servicepublishmanager.service;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.service.servicepublishmanager.dao.IServiceTargetSynDAO;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class ServiceTargetSynDS implements IServiceTargetSynDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IServiceTargetSynDAO dao = null;
	
	public void setDao(IServiceTargetSynDAO dao)
	{
	    this.dao = dao;
	}





    public TableDataInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug( "get serviceTargetSyn page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceTargetSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get serviceTargetSyn page info by condition end" );
        return tableInfo;
    
    }


    public TableDataInfo pageInfoQueryAll(Service service, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug( "get serviceTargetSyn page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryAll(service, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get serviceTargetSyn page info by condition end" );
        return tableInfo;
    
    }
    public TableDataInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug( "get cntTargetsysSyn page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceTargetSyn, start, pageSize, puEntity);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get cntTargetsysSyn page info by condition end" );
        return tableInfo;
    }


    public void updateServiceTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException
    {

        log.debug("update serviceTargetSyn by pk starting...");
        try {
            dao.updateServiceTargetSyn(serviceTargetSyn);
        } catch (DAOException daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceTargetSyn by pk end");
            
    }

    public ServiceTargetSyn getServiceTargetSynByID(Long syncindex) throws DomainServiceException
    {

        log.debug( "get serviceTargetSyn by pk starting..." );
        ServiceTargetSyn rsObj = new ServiceTargetSyn();
        rsObj.setSyncindex(syncindex);
        try
        {     
            rsObj = dao.getServiceTargetSyn( rsObj );
                        
        }
        catch( Exception daoEx )
        {
            log.error( "dao exception:", daoEx );           
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get serviceTargetSyn by pk end" );
        return rsObj;
    
    }

    public List<ServiceTargetSyn> getServiceTargetSynByCond(ServiceTargetSyn serviceTargetSyn)
            throws DomainServiceException
    {
        log.debug("get serviceTargetSyn list by condition starting...");
        List<ServiceTargetSyn> rsList = null;
        try {
            rsList = dao.getServiceTargetSynByCond(serviceTargetSyn);
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceTargetSyn list by condition end");
        return rsList;
    }


    public TableDataInfo serProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug( "get serProMapTargetBing page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryforSerProMapTargetBing(serviceTargetSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get serProMapTargetBing page info by condition end" );
        return tableInfo;

    }


    public TableDataInfo serSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug( "get serProMapTargetBing page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryforSerSerMapTargetBing(serviceTargetSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get serProMapTargetBing page info by condition end" );
        return tableInfo;

    }

    public TableDataInfo serChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug( "get serProMapTargetBing page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryforSerChaMapTargetBing(serviceTargetSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get serProMapTargetBing page info by condition end" );
        return tableInfo;

    }





    public ServiceTargetSyn getMappingTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException
    {

        log.debug( "get getMappingTargetSyn by pk starting..." );
        ServiceTargetSyn rsObj = new ServiceTargetSyn();
        try
        {     
            rsObj = dao.getMappingTargetSyn(serviceTargetSyn);
                        
        }
        catch( Exception daoEx )
        {
            log.error( "dao exception:", daoEx );           
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get getMappingTargetSyn by pk end" );
        return rsObj;
    
    }





    public ServiceTargetSyn getObjectTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException
    {

        log.debug( "get getMappingTargetSyn by pk starting..." );
        ServiceTargetSyn rsObj = new ServiceTargetSyn();
        try
        {     
            rsObj = dao.getObjectTargetSyn(serviceTargetSyn);
                        
        }
        catch( Exception daoEx )
        {
            log.error( "dao exception:", daoEx );           
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get getMappingTargetSyn by pk end" );
        return rsObj;
    
    }

    public List<ServiceTargetSyn> getAllServiceTargetSynById(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException
    {
        log.debug("get getAllServiceTargetSynById list by condition starting...");
        List<ServiceTargetSyn> rsList = null;
        try {
            rsList = dao.getAllServiceTargetSynById(serviceTargetSyn);
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getAllServiceTargetSynById list by condition end");
        return rsList;
    }


    public List<ServiceTargetSyn> getSameParentTargetMappingSyn(ServiceTargetSyn serviceTargetSyn)
            throws DomainServiceException
    {
        log.debug("get getSameParentTargetMappingSyn list by condition starting...");
        List<ServiceTargetSyn> rsList = null;
        try {
            rsList = dao.getSameParentTargetMappingSyn(serviceTargetSyn);
        } catch (Exception daoEx) {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get getSameParentTargetMappingSyn list by condition end");
        return rsList;
    
    }
}

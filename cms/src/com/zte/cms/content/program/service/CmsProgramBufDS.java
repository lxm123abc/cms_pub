package com.zte.cms.content.program.service;

import java.util.List;

import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.CmsProgramBuf;
import com.zte.cms.content.program.dao.ICmsProgramBufDAO;
import com.zte.cms.content.program.service.ICmsProgramBufDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramBufDS implements ICmsProgramBufDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsProgramBufDAO dao = null;

    public void setDao(ICmsProgramBufDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("insert cmsProgramBuf starting...");
        try
        {
            dao.insertCmsProgramBuf(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgramBuf end");
    }

    public void insertCmsProgramBufNoPK(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("insert cmsProgramBuf starting...");
        try
        {
            dao.insertCmsProgramBuf(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgramBuf end");
    }

    public void insertCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException
    {
        log.debug("insert cmsProgramBufList by pk starting...");
        if (null == cmsProgramBufList || cmsProgramBufList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramBufList");
            return;
        }
        try
        {
            dao.insertCmsProgramBufList(cmsProgramBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsProgramBufList by pk end");
    }

    public void updateCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("update cmsProgramBuf by pk starting...");
        try
        {
            dao.updateCmsProgramBuf(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramBuf by pk end");
    }

    public void updateCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException
    {
        log.debug("update cmsProgramBufList by pk starting...");
        if (null == cmsProgramBufList || cmsProgramBufList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramBufList");
            return;
        }
        try
        {
            dao.updateCmsProgramBufList(cmsProgramBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramBufList by pk end");
    }

    public void updateCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("update cmsProgramBuf by condition starting...");
        try
        {
            dao.updateCmsProgramBufByCond(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramBuf by condition end");
    }

    public void updateCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException
    {
        log.debug("update cmsProgramBufList by condition starting...");
        if (null == cmsProgramBufList || cmsProgramBufList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramBufList");
            return;
        }
        try
        {
            dao.updateCmsProgramBufListByCond(cmsProgramBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsProgramBufList by condition end");
    }

    public void removeCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("remove cmsProgramBuf by pk starting...");
        try
        {
            dao.deleteCmsProgramBuf(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramBuf by pk end");
    }

    public void removeCmsProgramBufList(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException
    {
        log.debug("remove cmsProgramBufList by pk starting...");
        if (null == cmsProgramBufList || cmsProgramBufList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramBufList");
            return;
        }
        try
        {
            dao.deleteCmsProgramBufList(cmsProgramBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramBufList by pk end");
    }

    public void removeCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("remove cmsProgramBuf by condition starting...");
        try
        {
            dao.deleteCmsProgramBufByCond(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramBuf by condition end");
    }

    public void removeCmsProgramBufListByCond(List<CmsProgramBuf> cmsProgramBufList) throws DomainServiceException
    {
        log.debug("remove cmsProgramBufList by condition starting...");
        if (null == cmsProgramBufList || cmsProgramBufList.size() == 0)
        {
            log.debug("there is no datas in cmsProgramBufList");
            return;
        }
        try
        {
            dao.deleteCmsProgramBufListByCond(cmsProgramBufList);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsProgramBufList by condition end");
    }

    public CmsProgramBuf getCmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("get cmsProgramBuf by pk starting...");
        CmsProgramBuf rsObj = null;
        try
        {
            rsObj = dao.getCmsProgramBuf(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramBufList by pk end");
        return rsObj;
    }

    /**
     * 获取CmsProgram对象
     */
    public CmsProgramBuf getcmsProgramBuf(CmsProgramBuf cmsProgramBuf) throws Exception
    {
        log.debug("getCmsProgramByCond starting...");
        CmsProgramBuf program = null;
        try
        {
            List<CmsProgramBuf> list = this.getCmsProgramBufByCond(cmsProgramBuf);
            if (list != null && list.size() > 0)
            {
                program = list.get(0);
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        log.debug("getCmsProgramByCond starting...");
        return program;
    }

    public List<CmsProgramBuf> getCmsProgramBufByCond(CmsProgramBuf cmsProgramBuf) throws DomainServiceException
    {
        log.debug("get cmsProgramBuf by condition starting...");
        List<CmsProgramBuf> rsList = null;
        try
        {
            rsList = dao.getCmsProgramBufByCond(cmsProgramBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsProgramBuf by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsProgramBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramBuf, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramBuf page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsProgramBuf cmsProgramBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsProgramBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsProgramBuf, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsProgramBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramBuf page info by condition end");
        return tableInfo;
    }
}

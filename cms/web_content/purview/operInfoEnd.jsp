<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="mangroup" class="com.zte.purviewext.ManGroup" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<%@ include file="JavaFun.jsp" %>

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    try {
        HashMap map = new HashMap();
        String flag = request.getParameter("flag") == null ? "" : (String)request.getParameter("flag");
        String creatorID = (String)session.getAttribute("OPERID");
        String creatorName = (String)session.getAttribute("OPERNAME");
        String operID = request.getParameter("operID");
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        String selectedServiceKey = request.getParameter("servicekey");
        if(selectedServiceKey!=null && selectedServiceKey.trim().length()>0){
          purview.setServiceKey(selectedServiceKey);
        }
        else{
          purview.setServiceKey(serviceKey);
        }
        map.put("SESSIONOPERID",creatorID);
        map.put("SESSIONOPERNAME",creatorName);
        if (operID != null)
            map.put("OPERID",operID);
        if (flag.equalsIgnoreCase("add"))
            map.put("CREATORID",creatorID);
        map.put("OPERNAME",(String)request.getParameter("operName") == null ? "" : transferString(((String)request.getParameter("operName")).trim()));
        map.put("OPERALLNAME",(String)request.getParameter("operAllName") == null ? "" : transferString(((String)request.getParameter("operAllName")).trim()));
        map.put("OPERDESCRIPTION",(String)request.getParameter("operDescription") == null ? "" : transferString(((String)request.getParameter("operDescription")).trim()));
        map.put("OPERPWD",(String)request.getParameter("operPwd") == null ? "" : ((String)request.getParameter("operPwd")).trim());
		String notifyType = (String)request.getParameter("notify");
		if("0".equals(notifyType)){
			map.put("PHONENUMBER","");
			map.put("EMAIL","");
		}
		if("1".equals(notifyType)){
        map.put("PHONENUMBER",(String)request.getParameter("phonenumber") == null ? "" : transferString(((String)request.getParameter("phonenumber")).trim()));
			map.put("EMAIL","");
		}
		if("2".equals(notifyType)){
			map.put("PHONENUMBER","");
        map.put("EMAIL",(String)request.getParameter("email") == null ? "" : ((String)request.getParameter("email")).trim());
		}
        map.put("NOTIFY",(String)request.getParameter("notify"));
        String nextUpdPwd = (String)request.getParameter("nextUpdPwd") == null ? "" : (String)request.getParameter("nextUpdPwd");
        if (nextUpdPwd.equalsIgnoreCase("on"))      // ??��?��???����D??��???��??
            map.put("NEXTUPDPWD","1");
        else
            map.put("NEXTUPDPWD","0");
        String pwdNeverUpd = (String)request.getParameter("pwdNeverUpd") == null ? "" : (String)request.getParameter("pwdNeverUpd");
        if (pwdNeverUpd.equalsIgnoreCase("on"))     // ��??��2?��??��???��??
            map.put("PWDNEVERUPD","1");
        else
            map.put("PWDNEVERUPD","0");
        String pwdMustChange = (String)request.getParameter("pwdMustChange") == null ? "" : (String)request.getParameter("pwdMustChange");
        if (pwdMustChange.equalsIgnoreCase("on"))   // ?��??����??��DD��
            map.put("PWDMUSTCHANGE","0");
        else
            map.put("PWDMUSTCHANGE","30");
        String pwdMustChange1 = (String)request.getParameter("pwdMustChange1") == null ? "" : ((String)request.getParameter("pwdMustChange1")).trim();
        if (pwdMustChange1.equalsIgnoreCase("on"))  // ?��??��DD��?��?T??
            try {
                map.put("PWDMUSTCHANGE",Integer.parseInt(request.getParameter("passwordChange")) + "");
            }
            catch (Exception e) {
                map.put("PWDMUSTCHANGE","0");
            }
        int operStatus = 0;                         // ?��o?���䨬?
        if (request.getParameter("userForbid") != null)
            operStatus = operStatus + 3;
        if (request.getParameter("userLocked") != null)
            operStatus = operStatus + 1;
        if (flag.equalsIgnoreCase("update"))
            if (purview.isBlacklist(operID))
                operStatus = 10;
        map.put("OPERSTATUS","" + operStatus);
        String maxNoUseDays = "0";                  // ?��o?��?�䨮?D??����??
        if (request.getParameter("maxNoUseDays") != null)
            maxNoUseDays = (String)request.getParameter("noUseDays") == null ? "0" : ((String)request.getParameter("noUseDays")).trim();
        map.put("MAXNOUSEDAYS",maxNoUseDays);
        String maxWrgLog = "0";                     // ��?D?�䨪?����???��?��y
        if (request.getParameter("maxWrongTimes") != null)
            maxWrgLog = (String)request.getParameter("wrongTimes") == null ? "0" : ((String)request.getParameter("wrongTimes")).trim();
        map.put("MAXWRGLOG",maxWrgLog);
        String useDate = "999";                     // ?��o?��DD��?��
        if (request.getParameter("useDate") != null)
            useDate = (String)request.getParameter("endDate") == null ? "999" : ((String)request.getParameter("endDate")).trim();
        map.put("USEDATE",useDate);
        // ��???����??
        String workDay = "1111111";
        String onLimit = (String)request.getParameter("onLimit") == null ? "0" : ((String)request.getParameter("onLimit")).trim();
        if (onLimit.equals("0")) {
            map.put("BEGINTIME","999");
            map.put("ENDTIME","23:59:59");
        }
        else {
            workDay = request.getParameter("sunday") == null ? "0" : "1";
            workDay = workDay + (request.getParameter("monday") == null ? "0" : "1");
            workDay = workDay + (request.getParameter("tuesday") == null ? "0" : "1");
            workDay = workDay + (request.getParameter("wednesday") == null ? "0" : "1");
            workDay = workDay + (request.getParameter("thursday") == null ? "0" : "1");
            workDay = workDay + (request.getParameter("friday") == null ? "0" : "1");
            workDay = workDay + (request.getParameter("saturday") == null ? "0" : "1");
            map.put("BEGINTIME",(String)request.getParameter("beginTime"));
            map.put("ENDTIME",(String)request.getParameter("endTime"));
        }
        map.put("WORKDAY",workDay);
        // ��????��?��
        ArrayList ipList = new ArrayList();
        HashMap ipMap = new HashMap();
        String allowIP = (String)request.getParameter("allowIP");
        String noAllowIP = (String)request.getParameter("noAllowIP");
        // ��????��D����???��?IP
        int index = allowIP.indexOf(";");
        while (index > 0) {
            ipMap = new HashMap();
            ipMap.put("IPADDR",(allowIP.substring(0,index)).trim());
            ipMap.put("ALLOWFLAG","1");
            if (index + 1 >= allowIP.length())
                allowIP = "";
            else
                allowIP = allowIP.substring(index + 1,allowIP.length());
            index = allowIP.indexOf(";");
            ipList.add(ipMap);
        }
        // ��??????1��???��?IP
        index = noAllowIP.indexOf(";");
        while (index > 0) {
            ipMap = new HashMap();
            ipMap.put("IPADDR",(noAllowIP.substring(0,index)).trim());
            ipMap.put("ALLOWFLAG","0");
            noAllowIP = noAllowIP.substring(index + 1,noAllowIP.length());
            index = noAllowIP.indexOf(";");
            ipList.add(ipMap);
        }

       //2������?������??D??��
        int serindex = 0;
        String serflag = (String)request.getParameter("serflag") == null ? "0" : (String)request.getParameter("serflag");
        if (serflag.equals("1"))   //sp1������?��
           serindex = Integer.parseInt(request.getParameter("spindex"));
        else if(serflag.equals("3")){  //?����?1������?��
            String groupid = (String)request.getParameter("groupid") == null ? "" : (String)request.getParameter("groupid");
            if(groupid.equals(""))
               throw new Exception("group id is required.");
            serindex = Integer.parseInt(groupid);
        }
        
        map.put("SERFLAG",serflag);

        map.put("SERINDEX",serindex+"");
        map.put("IPLIST",ipList);
        if(session.getAttribute("OPER_TYPE_ATTRIBUTE")!= null)
          map.put("OPER_TYPE_ATTRIBUTE",session.getAttribute("OPER_TYPE_ATTRIBUTE"));
        else if(serflag.equals("1")||serflag.equals("3")){
          map.put("OPER_TYPE_ATTRIBUTE",serflag);  
        }          
        if(session.getAttribute("YWYY_CODE_ATTRIBUTE")!= null)
          map.put("YWYY_CODE_ATTRIBUTE",session.getAttribute("YWYY_CODE_ATTRIBUTE"));  
        else if(serflag.equals("1")||serflag.equals("3")){
          map.put("YWYY_CODE_ATTRIBUTE",(String)map.get("SERINDEX"));  
        }       
        if (flag.equalsIgnoreCase("add")){
            purview.addOper(map);
        }
        else{
            purview.updateOper(map);
        }
%>
<html>
<head>
<title><%= flag.equalsIgnoreCase("add") ? i18n.getMessage("operInfo.add") : i18n.getMessage("operInfo.update") %><fmt:message key="operInfo.operator"/></title>
</head>
<body>
<script language="javascript">
    window.opener.refresh();
    window.close();
</script>
</body>
</html>
<%
    }
    catch (Exception e) {
        e.printStackTrace();
%>
<script language="javascript">
    alert('<%=e.getMessage()%>');
    window.close();
</script>

<%    }
%>

package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.CmsTasksourceMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsTasksourceMapDAO
{
    /**
     * 新增CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DAOException dao异常
     */
    public void insertCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据主键更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DAOException dao异常
     */
    public void updateCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据条件更新CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据主键删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @throws DAOException dao异常
     */
    public void deleteCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据条件删除CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据主键查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @return 满足条件的CmsTasksourceMap对象
     * @throws DAOException dao异常
     */
    public CmsTasksourceMap getCmsTasksourceMap(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据条件查询CmsTasksourceMap对象
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @return 满足条件的CmsTasksourceMap对象集
     * @throws DAOException dao异常
     */
    public List<CmsTasksourceMap> getCmsTasksourceMapByCond(CmsTasksourceMap cmsTasksourceMap) throws DAOException;

    /**
     * 根据条件分页查询CmsTasksourceMap对象，作为查询条件的参数
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsTasksourceMap对象，作为查询条件的参数
     * 
     * @param cmsTasksourceMap CmsTasksourceMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsTasksourceMap cmsTasksourceMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
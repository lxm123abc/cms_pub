package com.zte.cms.channel.ls;

import java.util.ArrayList;
import java.util.List;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.channel.common.CmsChannelConstant;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelBufDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.update.apply.IChannelUpdateApply;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class PhysicalChannelLS implements IPhysicaChannellLS
{

    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private IPhysicalchannelDS physicalchannelDS;
    private IPhysicalchannelBufDS physicalchannelbufDS;
    private IUcpBasicDS basicDS;
    private ICmsChannelDS channelDS;
    private ITargetsystemDS targetSystemDS;
    private IUsysConfigDS usysConfigDS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private ICntSyncRecordDS cntSyncRecordDS;
    private IChannelUpdateApply channelupdateApply;
    private ICntTargetSyncDS cntTargetSyncDS;
    
    private ICmsChannelSyncLS cmsChannelSyncLS;

    
    
    public ICmsChannelSyncLS getCmsChannelSyncLS()
    {
        return cmsChannelSyncLS;
    }

    public void setCmsChannelSyncLS(ICmsChannelSyncLS cmsChannelSyncLS)
    {
        this.cmsChannelSyncLS = cmsChannelSyncLS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public IPhysicalchannelBufDS getPhysicalchannelbufDS()
    {
        return physicalchannelbufDS;
    }

    public void setPhysicalchannelbufDS(IPhysicalchannelBufDS physicalchannelbufDS)
    {
        this.physicalchannelbufDS = physicalchannelbufDS;
    }

    public IChannelUpdateApply getChannelupdateApply()
    {
        return channelupdateApply;
    }

    public void setChannelupdateApply(IChannelUpdateApply channelupdateApply)
    {
        this.channelupdateApply = channelupdateApply;
    }

    /**
     * 列表展示物理频道
     */
    public TableDataInfo pageInfo(Physicalchannel physicalchannel, int start, int pageSize)
    {
        TableDataInfo dataInfo = null;
        try
        {

            if (physicalchannel.getPhysicalchannelid() != null && !physicalchannel.getPhysicalchannelid().equals(""))
            {
                String physicalchannelid = EspecialCharMgt.conversion(physicalchannel.getPhysicalchannelid());
                physicalchannel.setPhysicalchannelid(physicalchannelid);
            }
            if (physicalchannel.getNamecn() != null && !physicalchannel.getNamecn().equals(""))
            {
                String namecn = EspecialCharMgt.conversion(physicalchannel.getNamecn());
                physicalchannel.setNamecn(namecn);
            }
            dataInfo = physicalchannelDS.pageInfoQuery(physicalchannel, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        return dataInfo;
    }

    /**
     * 物理频道新增,修改判断若免审则状态直接改为审核通过
     */
    public String insertPhysicalchannel(Physicalchannel physicalchannel)
    {
        try
        {
            String channelid = physicalchannel.getChannelid();
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(channelid);
            List list = channelDS.getChannelByCond(channel);
            if (list.size() == 0 || list == null)
            {// 频道不存在
                return null;
            }
            else
            {
                // 如果频道内容是120审核中、0待发布、10新增同步中、20新增同步成功、30新增同步失败、70取消同步中、80取消同步成功、90取消同步失败时，物理频道只能查看
                channel = (CmsChannel) list.get(0);
                int status = channel.getStatus();
                if (status == 120 || status == 0)
                {
                    return "1:频道内容状态不正确";
                }
                List validPhysicalByNamecn = physicalchannelDS.getPhysicalchannelByCond(physicalchannel);
                if (validPhysicalByNamecn != null && validPhysicalByNamecn.size() > 0)
                {// 频道名称重复
                    return "1:物理频道名称重复";
                }

                /*
                 * if (physicalchannel.getCpcontentid()!=null&&!physicalchannel.getCpcontentid().equals("")) {
                 * Physicalchannel cpcontentPhysicalchannel=new Physicalchannel();
                 * cpcontentPhysicalchannel.setCpcontentid(physicalchannel.getCpcontentid()); List
                 * cpcontentidPhysicalchannelList=physicalchannelDS.getPhysicalchannelByCond(cpcontentPhysicalchannel);
                 * if (cpcontentidPhysicalchannelList!=null&&cpcontentidPhysicalchannelList.size()>0) { return
                 * "1:CP对于物理频道的标识重复"; } }
                 */
            }

            // 查看是否免审
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            int flag = 0;
            usysConfig.setCfgkey("cms.channel.noaudit.switch");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                flag = Integer.parseInt(usysConfig.getCfgvalue());//
            }
            if (flag == 0)
            {
                // 免审，不发起工作流，改频道，物理频道状态为审核通过
                physicalchannel.setStatus(0);// 待发布
                physicalchannel.setWorkflow(0);// 未启动
                physicalchannel.setWorkflowlife(0);// 未启动

                channel.setStatus(0);// 待发布
                channel.setWorkflow(0);// 未启动
                channel.setWorkflowlife(0);// 未启动
                
                Physicalchannel ph = new Physicalchannel();
                ph.setChannelid(channel.getChannelid());
                List listphy = physicalchannelDS.listPhysicalchannelByChannelid(ph);
                if (listphy != null && listphy.size() > 0)
                {
                    for(int kk =0;kk<listphy.size();kk++){
                        Physicalchannel phy = (Physicalchannel)listphy.get(kk);
                        phy.setStatus(0);
                        phy.setWorkflow(0);
                        phy.setWorkflowlife(0);
                        physicalchannelDS.updatePhysicalchannel(phy);
                    }
                }
                
            }
            else
            {
                physicalchannel.setStatus(110);
            }
            if(physicalchannel.getDomain()!=null){
                physicalchannel.setDomain(physicalchannel.getDomain());
            }
            physicalchannelDS.insertPhysicalchannel(physicalchannel);
            channelDS.updateCmsChannel(channel);
            
            CommonLogUtil
                    .insertOperatorLog(physicalchannel.getPhysicalchannelid(),
                            CmsChannelConstant.MGTTYPE_CHANNELCONTENT, CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_ADD,
                            CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_ADD_INFO,
                            CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
            return "0:操作成功";
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    /**
     * 物理频道修改
     * 
     * @throws Exception
     */
    public String updatePhysicalchannel(Physicalchannel physicalchannel) throws Exception
    {
        try
        {
            String rtn = "";
            Physicalchannel rtnPhysicalchannel = new Physicalchannel();
            rtnPhysicalchannel.setPhysicalchannelindex(physicalchannel.getPhysicalchannelindex());
            rtnPhysicalchannel = getPhysicalchannel(rtnPhysicalchannel);
            if (rtnPhysicalchannel == null)
            {// 频道不存在
                return "1:物理频道不存在";
            }
            else
            {
                // 10 30 40 70 90 120
                if (rtnPhysicalchannel.getStatus() == 120)
                {
                    return "1:物理频道状态不正确";
                }

                // 如果频道内容是120待审核、10新增同步中、20新增同步成功、30新增同步失败、70取消同步中、80取消同步成功、90取消同步失败时，物理频道只能查看
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(physicalchannel.getChannelid());
                channel = (CmsChannel) channelDS.getChannelByCond(channel).get(0);
                int status = channel.getStatus();
                if (status == 120)
                {
                    return "1:频道内容状态不正确";
                }

                /*
                 * if (physicalchannel.getCpcontentid()!=null&&!physicalchannel.getCpcontentid().equals("")) {
                 * Physicalchannel cpcontentPhysicalchannel=new Physicalchannel();
                 * cpcontentPhysicalchannel.setCpcontentid(physicalchannel.getCpcontentid()); List
                 * cpcontentidPhysicalchannelList=physicalchannelDS.getPhysicalchannelByCond(cpcontentPhysicalchannel);
                 * if (cpcontentidPhysicalchannelList!=null&&cpcontentidPhysicalchannelList.size()>1) { return
                 * "1:CP对于物理频道的标识重复"; }else { if (cpcontentidPhysicalchannelList.size()==1) {
                 * cpcontentPhysicalchannel=(Physicalchannel) cpcontentidPhysicalchannelList.get(0); if
                 * (!physicalchannel.getPhysicalchannelid().equals(cpcontentPhysicalchannel.getPhysicalchannelid())) {
                 * return "1:CP对于物理频道的标识重复"; } } } }
                 */
                // 不在此处修改，审核通过后修改物理频道数据
                // physicalchannelDS.updatePhysicalchannel(physicalchannel);
                if (channel.getStatus() == 110 || channel.getStatus() == 130)
                {// 未审核
                    physicalchannelDS.updatePhysicalchannel(physicalchannel);
                }
                else
                // 原来审核通过的
                {
                    // 查看是否免审
                    UsysConfig usysConfig = new UsysConfig();
                    List<UsysConfig> usysConfigList = null;
                    int flag = 0;
                    usysConfig.setCfgkey("cms.channel.noaudit.switch");
                    usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
                    if (null != usysConfigList && usysConfigList.size() > 0)
                    {
                        usysConfig = usysConfigList.get(0);
                        flag = Integer.parseInt(usysConfig.getCfgvalue());//
                    }
                    if (flag == 0)
                    {
                        // 免审，不发起工作流，改频道，物理频道状态为审核通过,查看是否发布过,直接更新本地物理频道
                        physicalchannelDS.updatePhysicalchannel(physicalchannel);
                        
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        cntTargetSync.setObjectindex(channel.getChannelindex());
                        cntTargetSync.setObjecttype(5);

                        List tarlist = new ArrayList();
                        try
                        {
                            tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                        }
                        catch (DomainServiceException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        List<CntTargetSync> publist = new ArrayList();
                        if (tarlist != null && tarlist.size() > 0)
                        {
                            for (int i = 0; i < tarlist.size(); i++)
                            {
                                cntTargetSync = (CntTargetSync) tarlist.get(i);
                                int statuscha = cntTargetSync.getStatus();
                                if (statuscha == 300 || statuscha == 500)
                                {
                                    publist.add(cntTargetSync);
                                   
                                }

                            }
                            rtn = cmsChannelSyncLS.publishChannelcontentTar(publist);// 可以发布到的网元
                        }                        
                    }
                    else//不是免审
                    {
                        // 插入物理频道buf表数据，发起频道审核的工作流
                        PhysicalchannelBuf physicalchannelBuf = new PhysicalchannelBuf();
                        BeanCopier basicObjDBCopy = BeanCopier.create(Physicalchannel.class, PhysicalchannelBuf.class,
                                true);
                        basicObjDBCopy.copy(physicalchannel, physicalchannelBuf, new BeanPropertyConvert());
                        physicalchannelbufDS.insertPhysicalchannelBuf(physicalchannelBuf);
                        channelupdateApply.apply(channel);// 物理频道发起修改审核的工作流
                    }
                }
                CommonLogUtil.insertOperatorLog(physicalchannel.getPhysicalchannelid(),
                        CmsChannelConstant.MGTTYPE_CHANNELCONTENT, CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_UPD,
                        CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_UPD_INFO,
                        CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                return "0:操作成功";
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    /**
     * 物理频道删除
     */
    public String deletePhysicalchannel(Physicalchannel physicalchannel)
    {
        try
        {
            Physicalchannel rtnPhysicalchannel = getPhysicalchannel(physicalchannel);
            if (rtnPhysicalchannel == null)
            {
                return "1:物理频道不存在";
            }
            else if (rtnPhysicalchannel.getStatus() != 110 && rtnPhysicalchannel.getStatus() != 130)
            {
                return "1:当前状态不能删除";
            }
            else
            {
                // 频道是审核中时物理频道只能查看
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(rtnPhysicalchannel.getChannelid());
                channel = (CmsChannel) channelDS.getChannelByCond(channel).get(0);

                // 如果频道内容是120审核中、0待发布、10新增同步中、20新增同步成功、30新增同步失败、70取消同步中、80取消同步成功、90取消同步失败时，物理频道只能查看
                int status = channel.getStatus();
                if (status == 120 || status == 0 || status == 10 || status == 20 || status == 30 || status == 40
                        || status == 50 || status == 60 || status == 70 || status == 80 || status == 90)
                {
                    return "1:频道内容状态不正确";
                }
                else
                {
                    CommonLogUtil.insertOperatorLog(physicalchannel.getPhysicalchannelid(),
                            CmsChannelConstant.MGTTYPE_CHANNELCONTENT, CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_DEL,
                            CmsChannelConstant.OPERTYPE_PHYSICALCHANNEL_DEL_INFO,
                            CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                    physicalchannelDS.removePhysicalchannel(physicalchannel);

                    return "0:操作成功";
                }
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    /**
     * 按物理频道id得到物理频道
     */
    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel)
    {
        Physicalchannel rtnPhysicalchannel = null;
        try
        {
            rtnPhysicalchannel = physicalchannelDS.getPhysicalchannel(physicalchannel);
            if (rtnPhysicalchannel == null)
            {
                return null;
            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        return rtnPhysicalchannel;
    }

    public List getPhysicalchannelBychannelindex(Long channelindex)
    {
        List list = new ArrayList();
        CmsChannel channel = new CmsChannel();
        channel.setChannelindex(channelindex);
        Physicalchannel ph = new Physicalchannel();
        try
        {
            channel = channelDS.getCmsChannel(channel);
            if (channel != null)
            {

                ph.setChannelid(channel.getChannelid());
                list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
                // if(list!=null&&list.size()>0){
                // ph = (Physicalchannel)list.get(0);
                // }
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 列表展示物理频道为节目单 added by fc
     */
    public TableDataInfo pageInfoschedule(CmsSchedule cmsSchedule, int start, int pageSize)
    {
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = physicalchannelDS.pageInfoQueryschedule(cmsSchedule, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        return dataInfo;
    }

    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    public IUcpBasicDS getBasicDS()
    {
        return basicDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }

    public ICmsChannelDS getChannelDS()
    {
        return channelDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public ICntSyncTaskDS getCntsyncTaskDS()
    {
        return cntsyncTaskDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public ICmsSchedulerecordDS getCmsSchedulerecordDS()
    {
        return cmsSchedulerecordDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

}

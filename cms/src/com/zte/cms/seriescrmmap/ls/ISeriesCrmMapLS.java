package com.zte.cms.seriescrmmap.ls;

import java.util.List;

import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ISeriesCrmMapLS
{

    /**
     * 批量删除连续剧与角色的关联关系
     * 
     * @param programCrmMap
     * @return
     * @throws Exception
     */
    String batchDeleteSeriescrmmap(Long[] mapindexList, String seriesindex) throws DomainServiceException;

    /**
     * 删除连续剧与角色的关联关系
     * 
     * @param programCrmMap
     * @return
     * @throws Exception
     */
    String deleteSeriescrmmap(Long mapindex, String seriesindex) throws DomainServiceException;

    /**
     * 获取单个连续剧与角色的关联关系
     * 
     * @param mapindex 主键
     * @return
     * @throws Exception
     */
    SeriesCrmMap getSeriescrmmap(Long mapindex);

    /**
     * 新增连续剧与角色的关联关系
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String insertSeriescrmmap(Long[] castrolemapindexList, String seriesindex) throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象 支持排序
     * 
     * @param programCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 新增同步
     * 
     * @param ProgramCrmMap对象主键
     * @throws Exception
     */
    public String publish(Long mapindex, String seriesindex) throws Exception;

    /**
     * 取消同步
     * 
     * @param Castrolemap对象主键
     * @throws Exception
     */
    public String cancelPublish(Long mapindex, String seriesindex) throws Exception;

    /**
     * 批量新增同步
     * 
     * @param mapindexs 主键集合
     * @throws Exception
     */
    public String batchPublishList(Long[] mapindexList, String seriesindex) throws Exception;

    /**
     * 批量取消同步
     * 
     * @param mapindexs 主键集合
     * @throws Exception
     */
    public String batchCancelPublishList(Long[] mapindexList, String seriesindex) throws Exception;
    
    
    //
    public String publishSeriesCastTargetMap (Long syncindex ) throws Exception; 
    public String publishSeriesCastPlatformMap (Long syncindex ) throws Exception; 
    public String publishSeriesCastBatchMap (List<CmsSeries> mappingList ) throws Exception; 
   // 
    public String delPublishSeriesCastTargetMap (Long syncindex ) throws Exception; 
    public String delPublishSeriesCastPlatformMap (Long syncindex ) throws Exception; 
    public String delPublishSeriesCastBatchMap (List<CmsSeries> mappingList ) throws Exception; 

}
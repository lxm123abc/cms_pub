function checkUnallowedCharacter(propertyname)
	{
	    var tempStr=trim(document.getElementById(propertyname).value);		
	    var pattern = /[`~!@#$%^&*()=+\\|\[\]\{\};:\'\",\<\>\/?]/;
	    if(!pattern.test(tempStr))
	    {
	        document.getElementById(propertyname + "_msg").innerText="";
			document.getElementById(propertyname + "_msg").style.display="none";
			return true;
	    }
	    else
	    {
	        document.getElementById(propertyname + "_msg").style.display="";
		    document.getElementById(propertyname + "_msg").innerText = $res_entry("msg.cms.storageManage.msg.include.unallowedcharacter");
		    document.getElementById(propertyname).focus();
		    return false;
	    }	
   
   }
<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="db" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    String operID = (String)session.getAttribute("OPERID");
    String operName = (String)session.getAttribute("OPERNAME");
    String spIndex = (String)session.getAttribute("SPINDEX");
    String spCode = (String)session.getAttribute("SPCODE");
    Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
    String serviceKey = (String)session.getAttribute("SERVICEKEY");
    db.setServiceKey(serviceKey); 
    String sysTime = "";
    Vector sysInfo = (Vector)application.getAttribute("SYSINFO");
    String strMsg = "";
    try {
       String  errmsg = "";
       boolean flag =true;
       if (operID  == null){
          errmsg = i18n.getMessage("checkRingLog.cl.loginfirst");
          flag = false;
       }
       if(flag){

        int cmdint=-1;
        String  optOperGroup = "";
        String  optServiceList = "";
        ArrayList serviceList = db.getServiceList(serviceKey);
        String selectedServiceKey = (String)request.getParameter("servicekey");
        // ?D?????����?����???����?��?1???��D��??33??��
        if (selectedServiceKey == null||selectedServiceKey.trim().length()==0)
        {
            if (serviceList.size() > 0)
                selectedServiceKey = (String)((HashMap)serviceList.get(0)).get("SERVICEKEY");
            else
                selectedServiceKey = serviceKey;
        }
        String cmd = request.getParameter("cmd");  
        if(cmd!=null && cmd.trim().length()>0){
          
          cmdint = Integer.parseInt(request.getParameter("cmd"));
        }
        String  Opergrpid = "";
        if(request.getParameter("opergrpid")!=null) Opergrpid=request.getParameter("opergrpid");
        String operate = request.getParameter("operate") == null ? "" : (String)request.getParameter("operate");        // ?�����¡䨲o?
        String grpScript = request.getParameter("grpScript") == null ? "" : (String)request.getParameter("grpScript");  // ��a???����?2������?��?����?
        HashMap map = new HashMap();
        Vector vFgrp=new Vector();
        Vector lststr=new Vector();
        if(cmdint==2){
          HashMap modMap = new HashMap();
          modMap.put("SERVICEKEY",serviceKey);
          modMap.put("SESSIONOPERID",operID);
          modMap.put("SESSIONOPERNAME",operName);
          modMap.put("REQUEST",request);
          if(db.grp_rightsmod(modMap)){
            strMsg="<font color='#0000FF'>";
          }else{
            strMsg="<font color='#FF0000'>";
          }
          strMsg=strMsg+ db.getStrmsg(db.geterrorCode()) + "</font>";
        }
        for (int i = 0; i < serviceList.size(); i++) {
            map = (HashMap)serviceList.get(i);
            String serv = (String)map.get("SERVICEKEY");
            if(serv.equals(selectedServiceKey))
              optServiceList = optServiceList + "<option value=\"" +  (String)map.get("SERVICEKEY") +"\" selected >" + (String)map.get("DESCRIPTION") + "</option>";
            else
              optServiceList = optServiceList + "<option value=\"" +  (String)map.get("SERVICEKEY") +"\" >" + (String)map.get("DESCRIPTION") + "</option>";
        }        
  %>

<html>
<head>
<title><fmt:message key="operGrpRight.allot"/></title>
<link rel="stylesheet" href="style.css" type="text/css"></link>
</head>
<body topmargin="0" leftmargin="0" onload="initform(this.document.forms[0])">
<script language="JavaScript">
	if(parent.frames.length>0)
		//parent.document.all.main.style.height="600";
</script>
<form name="inForm" method="POST" action="operGrpRight.jsp" onSubmit="return submitchk()">
   <input type="hidden" value="" name="cmd">
   <input type="hidden" value="" name="rgstr">
   <table border=0 height="480" width="80%" align=center>
   <tr>
   <td>
   <table border=0  width="100%" align=center  class="table-style2">
   <tr>
      <td colspan="2" height="26" align="center" class="text-title" valign="middle" background="button/n-9.gif" ><fmt:message key="operGrpRight.rightsconfig"/>
   </td>
   <tr >
   <td class="table-style5"> <fmt:message key="allotrights.managesystem"/> &nbsp;
   <select NAME="servicekey" size="1" tabindex="4" onChange="onServiceKey()" style="width:200px">
     <% out.println(optServiceList); %>
   </select>
   </td>
   <td class="table-style5"> <fmt:message key="operGrpRight.choosegrp"/>&nbsp;
    <select NAME="opergrpid" size="1" tabindex="4" onChange="getgrpinf()" style="width:120px">
    <%
      
      if(!selectedServiceKey.equals("")){
        lststr=db.oper_grpscriptqry(selectedServiceKey,operID);
        int k=db.oper_grpscriptqryrows();
        String strtmp = "";
        for(int i=0;i<lststr.size();i=i+k){
          if(i==0)
             strtmp = lststr.get(i).toString();
          out.print("<OPTION value='"+lststr.get(i)+"'> "+lststr.get(i+1)+" </OPTION>");
        }
       if(cmdint<0 && !strtmp.equals("")){
         Opergrpid = strtmp;
       }
       if(lststr.size()>0)
          lststr.clear();
      }
    %>
    </select>
   </td>
   <td>
     <input type="button" value="<%= i18n.getMessage("operGrpRight.authenticate") %>" name="Add" onClick="actadd()" >
   </td>
   </tr>
   <tr>
   <td align="center" width="100%" colspan="2">
              <table align="center" class="table-style5" border="1" cellpadding="6" cellspacing="0" width="100%"  bordercolorlight="#77BEEE" bordercolordark="#E0E0E0" height="398">
                <tr>
                  <td width="50%" align="center"><font class="font"><strong><fmt:message key="operGrpRight.funcgrplist"/></strong></font>
                  </td>
                  <td width="50%" align="center"><font class="font"><strong><fmt:message key="operGrpRight.rightsconfig"/></strong></font></td>
      </tr>

      <tr>
      <td align="center">
      <select NAME="funcgrpid"  size="22" onChange="getfuncgrpinf()" style="width: 200px">
      <%
         if(selectedServiceKey.equals("")){
            out.println("<OPTION value=''> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </OPTION>");
         }else{
          lststr=db.oper_funcgrpqry(selectedServiceKey);
          int k = db.oper_funcgrpqryrows();
          for(int i=0;i<lststr.size();i=i+k){
            if(i==0)
              out.print("<OPTION value='"+lststr.get(i)+"' selected > "+lststr.get(i+1)+" </OPTION>");
           else
             out.print("<OPTION value='"+lststr.get(i)+"'> "+lststr.get(i+1)+" </OPTION>");
            vFgrp.addElement(lststr.get(i).toString());
          }
          lststr.clear();
         }
      %>
      </select></td>
      <td bgcolor="#FFFFFF" valign="top">
      <%
         Vector vFunc=new Vector();
         String sTmp="";
         if(serviceKey.equals("")){
         }else{
          int ii=0;
          int ik=db.oper_functionqryrows();
          String sTmp2="";
          for(int i=0;i<vFgrp.size();i++){
            sTmp=vFgrp.get(i).toString();
            out.println("<div id='Func_"+sTmp+"' STYLE='display:none;'>");
            lststr=db.oper_functionqry(selectedServiceKey,sTmp);
            for(ii=0;ii<lststr.size();ii=ii+ik){
              String str = lststr.get(ii).toString()+"-"+lststr.get(ii+1).toString();
              if(purviewList.get(str) == null){
                sTmp2 = "disabled";
              }else{
                sTmp2="";
              }
              out.print(" <input type='checkbox' name='Func_"+lststr.get(ii)+"_"+lststr.get(ii+1)+"' value='0'"+sTmp2+">"+lststr.get(ii+2).toString()+"<br>");
              vFunc.addElement(lststr.get(ii).toString()+"_"+lststr.get(ii+1).toString());
            }
            lststr.clear();
            out.println("</div>");
          }
         }
      %>
      </td>
      </tr>
     </table>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
</form>

<script language="JavaScript1.2">
function getfuncgrpinf(){
  var sTmp=document.inForm.funcgrpid.value;
  document.all('Func_'+sTmp).style.display='';
}

function actadd(){
  if(!(document.inForm.opergrpid.value==""||document.inForm.servicekey.value=="")){
    var sTmp1="";
    document.inForm.rgstr.value=sTmp1.substring(0,sTmp1.length-1);
    document.inForm.cmd.value="2";
    document.inForm.submit();
  }
}

function initform(pform){

}

function submitchk(){
  if(document.inForm.cmd.value==""){
    return false;
  }else{
    return true;
  }
}


function actadd(){
  if(!(document.inForm.opergrpid.value==""||document.inForm.servicekey.value=="")){
    var sTmp1="";
<%
  for(int i=0;i<vFunc.size();i++){
    sTmp=vFunc.get(i).toString();
    out.println(" if(document.all('Func_"+sTmp+"').checked==true&&document.all('Func_"+sTmp+"').disabled==false){");
    out.println("  sTmp1=sTmp1+'"+sTmp+"|';");
    out.println(" }");
  }
%>
    document.inForm.rgstr.value=sTmp1.substring(0,sTmp1.length-1);
    document.inForm.cmd.value="2";
    document.inForm.submit();
  }
}

function getfuncgrpinf(){
<%
  for(int i=0;i<vFgrp.size();i++){
    sTmp=vFgrp.get(i).toString();
    out.println("document.all('Func_"+sTmp+"').style.display='none';");
  }
%>
  var sTmp=document.inForm.funcgrpid.value;
  document.all('Func_'+sTmp).style.display='';
}

 function onServiceKey(){
    document.forms[0].submit();
 }

 function getgrpinf(){
  if(document.inForm.opergrpid.value==""){
  }else{
    document.inForm.cmd.value="0";
    document.inForm.submit();
  }
 }

document.inForm.Add.disabled=true;
<%
  if(vFgrp.size()>0)out.println("document.all('Func_"+vFgrp.get(0)+"').style.display='';");
  if(!Opergrpid.equals("")){
    lststr=db.oper_grpdef(Opergrpid,selectedServiceKey);
    int k=db.oper_grpdefrows();
    for(int i=0;i<lststr.size();i=i+k){
      out.println("document.all('Func_"+lststr.get(i)+"_"+lststr.get(i+1)+"').value='1';");
      out.println("document.all('Func_"+lststr.get(i)+"_"+lststr.get(i+1)+"').checked=true;");
    }
    lststr.clear();
    if(db.opergrpinf(selectedServiceKey,Opergrpid)){
      out.println("document.inForm.opergrpid.value="+Opergrpid+";");
      if(db.getCreatorid()==java.lang.Integer.parseInt(operID))out.println("document.inForm.Add.disabled=false;");
    }
  }
 %>

</script>
</BODY>
</HTML>
<%
        }
        else {
            if(operID == null){
              %>
              <script language="javascript">
                    alert( "<fmt:message key="allotrights.loginfirst"/>");
                    document.URL = '../enter.jsp';
              </script>
              <%
                      }
                      else{
               %>
              <script language="javascript">
                   alert( "<fmt:message key="allotrights.forbidden"/>");
              </script>
              <%

              }
        }
    }
   catch (Exception e) {
%>
<html>
<body>
<table border="1" width="100%" bordercolorlight="#000000" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
</body>
</html>
<%
    }
%>
</body>
</html>

package com.zte.ucm.config;

import java.util.ArrayList;

public class Config
{
  private String tag;
  private String code;
  private String file;
  private String xpath;
  private String type = "0";
  private String attrname;
  private String key;
  private String value;
  private ArrayList errorinfo;
  private String filename;

  public String getFilename()
  {
    return this.filename;
  }

  public void setFilename(String paramString) {
    this.filename = paramString;
  }

  public ArrayList getErrorinfo()
  {
    return this.errorinfo;
  }

  public void setErrorinfo(ArrayList paramArrayList) {
    this.errorinfo = paramArrayList;
  }

  public String getTag()
  {
    return this.tag;
  }

  public void setTag(String paramString)
  {
    this.tag = paramString;
  }

  public String getFile()
  {
    return this.file;
  }

  public void setFile(String paramString)
  {
    this.file = paramString;
  }

  public String getValue()
  {
    return this.value;
  }

  public void setValue(String paramString)
  {
    this.value = paramString;
  }

  public String getXpath()
  {
    return this.xpath;
  }

  public void setXpath(String paramString)
  {
    this.xpath = paramString;
  }

  public String getType()
  {
    return this.type;
  }

  public void setType(String paramString)
  {
    this.type = paramString;
  }

  public String getAttrname()
  {
    return this.attrname;
  }

  public void setAttrname(String paramString)
  {
    this.attrname = paramString;
  }

  public String getKey() {
    return this.key;
  }

  public void setKey(String paramString) {
    this.key = paramString;
  }

  public String getCode()
  {
    return this.code;
  }

  public void setCode(String paramString)
  {
    this.code = paramString;
  }
}
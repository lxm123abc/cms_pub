package com.zte.cms.content.series.ls;

import java.util.List;

import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsSeriesPcitureLS
{

    /**************************连续剧海报发布管理  begin*********************/
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws Exception;

    
    /**
     * 根据条件分页查询SeriesProgramMap对象
     * 
     * @param seriesProgramMap SeriesProgramMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize) throws Exception;
   
    
    public String batchPublishOrDelPublishseriesPic(List<SeriesPictureMap> list,String operType, int type) throws Exception;

    public String publishseriesPicMapPlatformSyn(String mapindex, String seriesid,String pictureid,int type,int synType) throws Exception;

	public String delPublishseriesPicMapPlatformSyn(String mapindex, String seriesid,String pictureid,int type,int synType) throws Exception;

    public String publishseriesPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;

    public String delPublishseriesPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;

    
    
    /**************************连续剧海报发布管理  end*********************/
}

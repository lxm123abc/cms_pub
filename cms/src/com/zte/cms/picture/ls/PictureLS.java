package com.zte.cms.picture.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.Generator;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.UpLoadFile;
import com.zte.cms.common.UpLoadResult;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.picture.model.ChannelPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.picture.model.ServicePictureMap;
import com.zte.cms.picture.service.ICastPictureMapDS;
import com.zte.cms.picture.service.ICategoryPictureMapDS;
import com.zte.cms.picture.service.IChannelPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.picture.service.IProgramPictureMapDS;
import com.zte.cms.picture.service.ISeriesPictureMapDS;
import com.zte.cms.picture.service.IServicePictureMapDS;
import com.zte.cms.programcrmmap.model.ProgramCrmMapConstant;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.IPrimaryKeyGenerator;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 海报业务逻辑处理层
 * 
 * @author hardybird
 */
public class PictureLS extends DynamicObjectBaseDS implements IPictureLS
{
    private Log log = SSBBus.getLog(getClass());

    private IServiceDS serviceDS;
    private IServicePictureMapDS servicePictureMapDS;
    private ICategorycdnDS categoryDS;
    private ICategoryPictureMapDS categoryPictureMapDS;
    private ICmsProgramDS cmsProgramDS;
    private IProgramPictureMapDS programPictureMapDS;
    private ICmsSeriesDS seriesDS;
    private ISeriesPictureMapDS seriesPictureMapDS;
    private ICmsChannelDS channelDS;
    private IChannelPictureMapDS channelPictureMapDS;
    private ICastcdnDS castDS;
    private ICastPictureMapDS castPictureMapDS;

    private ICmsStorageareaLS cmsStorageareaLS;
    private IPrimaryKeyGenerator primaryKeyGenerator;
    private IPictureDS pictureDS;
    private IUsysConfigDS usysConfigDS = null;
    private ITargetsystemDS targetSystemDS;
    private ICmsFtptaskDS cmsFtptaskDS = null;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    public Long syncindex = null;

    PictureLS()
    {
        ResourceMgt.addDefaultResourceBundle("iptv_post_resource");
    }

    /**
     * 上传海报
     * 
     * @param pic Picture
     * @param picMaptype Integer
     * @param objType Integer 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
     * @param relativeIndex Long 对象index
     * @param fileNames String[]
     * @return String 0:操作成功 1:失败原因
     * @throws Exception
     */
    public String addPicture(Picture pic, Integer picMaptype, Integer objType, Long relativeIndex, String[] fileNames)
            throws Exception
    {
        log.debug("PictureLS: addPicture starting..."); // 文件名
        ReturnInfo rtnInfo = new ReturnInfo(); // 返回信息类
        Object obj = null;
        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                Service service = new Service();
                service.setServiceindex(relativeIndex);
                obj = serviceDS.getService(service);
                break;
            case PictureConstants.CATAGORY:
                Categorycdn catagory = new Categorycdn();
                catagory.setCategoryindex(relativeIndex);
                obj = categoryDS.getCategorycdn(catagory);
                break;
            case PictureConstants.PROGRAM:
                CmsProgram program = new CmsProgram();
                program.setProgramindex(relativeIndex);
                obj = cmsProgramDS.getCmsProgram(program);
                break;
            case PictureConstants.SERIES:
                CmsSeries series = new CmsSeries();
                series.setSeriesindex(relativeIndex);
                obj = seriesDS.getCmsSeries(series);
                break;
            case PictureConstants.CHANNEL:
                CmsChannel channel = new CmsChannel();
                channel.setChannelindex(relativeIndex);
                obj = channelDS.getCmsChannel(channel);
                break;
            case PictureConstants.CAST:
                Castcdn cast = new Castcdn();
                cast.setCastindex(relativeIndex);
                obj = castDS.getCastcdn(cast);
                break;
            default:
                break;
        }

        if (null == obj)
        {
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(PictureConstants.PIC_OBJECT_NOTEXIST); // "100006"
            return rtnInfo.toString();
        }

        String onlineArea = null;
        // 获取在线区地址
        try
        {
            onlineArea = cmsStorageareaLS.getAddress(PictureConstants.STORAGEAREA_ONLINE); // 2：在线区
        }
        catch (Exception e)
        {
            log.error("Error occurred in getAddress method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }

        if (null == onlineArea || PictureConstants.PIC_GET_STORAGEAREA_ERROR.equals(onlineArea))
        {// 1056020:获取在线区地址失败
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(PictureConstants.PIC_ONLINEAREA_NOTFOUND); // "100001"
            return rtnInfo.toString();
        }
        // 判断目标地址是否存在
        File online = new File(GlobalConstants.getMountPoint() + onlineArea + File.separator);
        if (!online.exists())
        {// 找不到目标地址，请检查存储区绑定
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(PictureConstants.PIC_DEST_NOTFOUND); // "100002"
            return rtnInfo.toString();
        }

        String currentDate = null; // 当前时间
        String timeStamp = null;
        // 获取当前时间
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        currentDate = dateFormat.format(date);
        timeStamp = String.valueOf(date.getTime());

        // 判断目标文件夹是否存在，若不存在则创建
        StringBuffer destfile = new StringBuffer("");
        destfile.append(GlobalConstants.getMountPoint());
        destfile.append(onlineArea);
        destfile.append(File.separator);
        destfile.append(currentDate);
        destfile.append(File.separator);
        File destDir = new File(destfile.toString());
        if (!destDir.exists())
        {// 目标文件夹不存在，创建
            try
            {
                destDir.mkdirs();
            }
            catch (Exception e)
            {
                log.error("Error occurred in mkdirs method", e);
                throw new Exception(ResourceManager.getResourceText(e));
            }
        }

        // 获取源文件名
        String oldname = fileNames[0].substring(fileNames[0].lastIndexOf(PictureConstants.WINDOWS_SEPERATOR) + 1);
        // 文件重命名
        String filename = timeStamp + "_" + oldname;
        // 拼装文件路径
        StringBuffer fileUrl = new StringBuffer(""); // 文件地址
        fileUrl.append(onlineArea);
        fileUrl.append(File.separator);
        fileUrl.append(currentDate);
        fileUrl.append(File.separator);
        fileUrl.append(filename);

        UpLoadResult uploadResult = null;
        String destpath = GlobalConstants.getMountPoint() + onlineArea + File.separator + currentDate;
        uploadResult = UpLoadFile.singleFileUplord(filename, PictureConstants.MAX_PIC_FILESIZE, destpath);
        if (null != uploadResult)
        {
            Integer sizeFlag = uploadResult.getFlag();
            if (-1 == sizeFlag.intValue())
            {// 文件大小超过10M
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage(PictureConstants.PIC_SIZE_TOOLARGE); // "100004"
                return rtnInfo.toString();
            }
        }
        else
        {// 文件上传失败
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(PictureConstants.PIC_UPLOAD_FAIL); // "100005"
            return rtnInfo.toString();
        }

        // HTTP上传成功,数据入库
        Long pictureIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_picture_index");
        String pictureId = Generator.getContentId(0L,ObjectType.PICT);
        pic.setPictureindex(pictureIndex);
        pic.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH); // 0:待发布
        pic.setPictureid(pictureId);
        pic.setPicturecode(pic.getPictureid());
        pic.setPictureurl(fileUrl.toString());
        pictureDS.insertPicture(pic);

        int paratype = 0;
        int objecttype = 0;
        Long objectindex = null;
        String objectid = "";
        String elementid = "";
        String parentid = "";
        Long mapIndex = null;
        String mappingid = "";
        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                ServicePictureMap servicePicMap = new ServicePictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_service_picture_map_index");
                mappingid = String.format("%02d", ObjectType.SERVICEPICTUREMAP_TYPE)+ String.format("%030d", mapIndex);
                servicePicMap.setMapindex(mapIndex);
                servicePicMap.setMappingid(mappingid);
                servicePicMap.setServiceindex(relativeIndex);
                servicePicMap.setServiceid(((Service) obj).getServiceid());
                servicePicMap.setPictureindex(pictureIndex);
                servicePicMap.setPictureid(pictureId);
                servicePicMap.setMaptype(picMaptype);
                servicePicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                servicePictureMapDS.insertServicePictureMap(servicePicMap);
                
                paratype = 1;
                objecttype = 35;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((Service) obj).getServiceid();
                parentid= pictureId;
                break;
            case PictureConstants.CATAGORY:
                CategoryPictureMap categoryPicMap = new CategoryPictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_category_picture_map_index");
                mappingid = String.format("%02d", ObjectType.CATEGORYPICTUREMAP_TYPE)+ String.format("%030d", mapIndex);
                categoryPicMap.setMapindex(mapIndex);
                categoryPicMap.setMappingid(mappingid);
                categoryPicMap.setCategoryindex(relativeIndex);
                categoryPicMap.setCategoryid(((Categorycdn) obj).getCategoryid());
                categoryPicMap.setPictureindex(pictureIndex);
                categoryPicMap.setPictureid(pictureId);
                categoryPicMap.setMaptype(picMaptype);
                categoryPicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                categoryPictureMapDS.insertCategoryPictureMap(categoryPicMap);
                
                paratype = 2;
                objecttype = 31;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((Categorycdn) obj).getCategoryid();
                parentid= pictureId;
                break;
            case PictureConstants.PROGRAM:
                ProgramPictureMap programPicMap = new ProgramPictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_program_picture_map_index");
                mappingid = String.format("%02d", ObjectType.PROGRAMPICTURE_TYPE)+ String.format("%030d", mapIndex);
                programPicMap.setMapindex(mapIndex);
                programPicMap.setMappingid(mappingid);
                programPicMap.setProgramindex(relativeIndex);
                programPicMap.setProgramid(((CmsProgram) obj).getProgramid());
                programPicMap.setPictureindex(pictureIndex);
                programPicMap.setPictureid(pictureId);
                programPicMap.setMaptype(picMaptype);
                programPicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                programPictureMapDS.insertProgramPictureMap(programPicMap);
                
                paratype = 3;
                objecttype = 35;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((CmsProgram) obj).getProgramid();
                parentid= pictureId;
                break;
            case PictureConstants.SERIES:
                SeriesPictureMap seriesPicMap = new SeriesPictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_series_picture_map_index");
                mappingid = String.format("%02d", ObjectType.SERIESPICTUREMAP_TYPE)+ String.format("%030d", mapIndex);
                seriesPicMap.setMapindex(mapIndex);
                seriesPicMap.setMappingid(mappingid);
                seriesPicMap.setSeriesindex(relativeIndex);
                seriesPicMap.setSeriesid(((CmsSeries) obj).getSeriesid());
                seriesPicMap.setPictureindex(pictureIndex);
                seriesPicMap.setPictureid(pictureId);
                seriesPicMap.setMaptype(picMaptype);
                seriesPicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                seriesPictureMapDS.insertSeriesPictureMap(seriesPicMap);
                
                paratype = 4;
                objecttype = 38;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((CmsSeries) obj).getSeriesid();
                parentid= pictureId;
                break;
            case PictureConstants.CHANNEL:
                ChannelPictureMap channelPicMap = new ChannelPictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_channel_picture_map_index");
                mappingid = String.format("%02d", ObjectType.CHANNELPICTUREMAP_TYPE)+ String.format("%030d", mapIndex);
                channelPicMap.setMapindex(mapIndex);
                channelPicMap.setMappingid(mappingid);
                channelPicMap.setChannelindex(relativeIndex);
                channelPicMap.setChannelid(((CmsChannel) obj).getChannelid());
                channelPicMap.setPictureindex(pictureIndex);
                channelPicMap.setPictureid(pictureId);
                channelPicMap.setMaptype(picMaptype);
                channelPicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                channelPictureMapDS.insertChannelPictureMap(channelPicMap);
                
                paratype = 5;
                objecttype = 39;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((CmsChannel) obj).getChannelid();
                parentid= pictureId;
                break;
            case PictureConstants.CAST:
                CastPictureMap castPicMap = new CastPictureMap();
                mapIndex = (Long) primaryKeyGenerator.getPrimarykey("cms_cast_picture_map_index");
                mappingid = String.format("%02d", ObjectType.CASTPICTURE_TYPE)+ String.format("%030d", mapIndex);
                castPicMap.setMapindex(mapIndex);
                castPicMap.setMappingid(mappingid);
                castPicMap.setCastindex(relativeIndex);
                castPicMap.setCastid(((Castcdn) obj).getCastid());
                castPicMap.setPictureindex(pictureIndex);
                castPicMap.setPictureid(pictureId);
                castPicMap.setMaptype(picMaptype);
                castPicMap.setStatus(PictureConstants.PIC_STATUS_TOPUBLISH);
                castPictureMapDS.insertCastPictureMap(castPicMap);
                
                paratype = 11;
                objecttype = 41;
                objectindex = mapIndex;
                objectid = mappingid;
                elementid = ((Castcdn) obj).getCastid();
                parentid= pictureId;
                break;
            default:
                break;
        }
        
        CntTargetSync target = new CntTargetSync();
        target.setObjecttype(paratype);
        target.setObjectindex(relativeIndex);
        //运营平台存在
        List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
        if(targetSyncList !=null && targetSyncList.size()>0){
            Targetsystem targetSystem = new Targetsystem();
            for(CntTargetSync targetSync:targetSyncList){
                targetSystem.setTargetindex(targetSync.getTargetindex());
                targetSystem = targetSystemDS.getTargetsystem(targetSystem);
                if(targetSystem.getTargettype() == 0 ||targetSystem.getTargettype() == 2 || targetSystem.getTargettype() == 10) //0-2.0文广.2:EPG,10:2.0央视
                {
                    CntPlatformSync platform = new CntPlatformSync();
                    platform.setObjecttype(objecttype);
                    platform.setObjectindex(objectindex);
                    platform.setPlatform(targetSystem.getPlatform());
                    List<CntPlatformSync> pcrmPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                    if(pcrmPlatSyncList !=null && pcrmPlatSyncList.size()>0)  
                    {
                        syncindex = pcrmPlatSyncList.get(0).getSyncindex();
                        platform = pcrmPlatSyncList.get(0);
                        platform.setStatus(0);
                        cntPlatformSyncDS.updateCntPlatformSync(platform);   
                    }else{
                        
                        insertPlatformSyncTask(objecttype,objectindex,objectid,elementid,parentid,
                                targetSystem.getPlatform(),0);
                    }
                    
                    insertTargetSyncTask(syncindex,objecttype,objectindex,objectid,elementid,parentid,
                            targetSystem.getTargetindex(),0,0);
                }
            }
        }

        // 写操作日志
        CommonLogUtil.insertOperatorLog(pic.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                PictureConstants.OPERTYPE_PICTURE_ADD, PictureConstants.OPERTYPE_PICTURE_ADD_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        rtnInfo.setFlag(GlobalConstants.SUCCESS);
        rtnInfo.setReturnMessage(PictureConstants.PIC_SUCCESS); // "100000":操作成功
        log.debug("PictureLS: addPicture end");

        return rtnInfo.toString();
    }

    /**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }
    
    /**
     * @param picIndex Long
     * @param objType Integer
     * @param relativeIndex Long
     * @return String
     * @throws Exception
     */
    public String removePictureandMap(Long picIndex, Integer objType, Long relativeIndex) throws Exception
    {
        log.debug("PictureLS: removePictureandMap starting..."); // 文件名
        ReturnInfo rtnInfo = new ReturnInfo(); // 返回信息类
        int objecttype = 0;
        Long objectindex = null;

        Picture cond = new Picture();
        cond.setPictureindex(picIndex);
        Picture pic = null;
        pic = pictureDS.getPicture(cond);
        if (null == pic)
        {
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(PictureConstants.PIC_OBJECT_NOTEXIST); // "100006"
            return rtnInfo.toString();
        }

        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                ServicePictureMap servicePicMap = new ServicePictureMap();
                servicePicMap.setServiceindex(relativeIndex);
                servicePicMap.setPictureindex(picIndex);
                List<ServicePictureMap> servicePicMapList = servicePictureMapDS
                        .getServicePictureMapByCond(servicePicMap);
                if (servicePicMapList != null && servicePicMapList.size() > 0)
                {
                    objecttype = 25;
                    objectindex = servicePicMapList.get(0).getMapindex();
                }
                break;
            case PictureConstants.CATAGORY:
                CategoryPictureMap categoryPicMap = new CategoryPictureMap();
                categoryPicMap.setCategoryindex(relativeIndex);
                categoryPicMap.setPictureindex(picIndex);
                List<CategoryPictureMap> categoryPicMapList = categoryPictureMapDS
                        .getCategoryPictureMapByCond(categoryPicMap);
                if (categoryPicMapList != null && categoryPicMapList.size() > 0)
                {
                    objecttype = 31;
                    objectindex = categoryPicMapList.get(0).getMapindex();
                }
                break;
            case PictureConstants.PROGRAM:
                ProgramPictureMap programPicMap = new ProgramPictureMap();
                programPicMap.setProgramindex(relativeIndex);
                programPicMap.setPictureindex(picIndex);
                List<ProgramPictureMap> programPictureMapList = programPictureMapDS
                        .getProgramPictureMapByCond(programPicMap);
                if (programPictureMapList != null && programPictureMapList.size() > 0)
                {
                    objecttype = 35;
                    objectindex = programPictureMapList.get(0).getMapindex();
                }
                break;
            case PictureConstants.SERIES:
                SeriesPictureMap seriesPicMap = new SeriesPictureMap();
                seriesPicMap.setSeriesindex(relativeIndex);
                seriesPicMap.setPictureindex(picIndex);
                List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS
                        .getSeriesPictureMapByCond(seriesPicMap);
                if (seriesPictureMapList != null && seriesPictureMapList.size() > 0)
                {
                    objecttype = 38;
                    objectindex = seriesPictureMapList.get(0).getMapindex();
                }
                break;
            case PictureConstants.CHANNEL:
                ChannelPictureMap channelPicMap = new ChannelPictureMap();
                channelPicMap.setChannelindex(relativeIndex);
                channelPicMap.setPictureindex(picIndex);
                List<ChannelPictureMap> channelPictureMapList = channelPictureMapDS
                        .getChannelPictureMapByCond(channelPicMap);
                if (channelPictureMapList != null && channelPictureMapList.size() > 0)
                {
                    objecttype = 39;
                    objectindex = channelPictureMapList.get(0).getMapindex();
                }
                break;
            case PictureConstants.CAST:
                CastPictureMap castPicMap = new CastPictureMap();
                castPicMap.setCastindex(relativeIndex);
                castPicMap.setPictureindex(picIndex);
                List<CastPictureMap> castPictureMapList = castPictureMapDS.getCastPictureMapByCond(castPicMap);
                if (castPictureMapList != null && castPictureMapList.size() > 0)
                {
                    objecttype = 41;
                    objectindex = castPictureMapList.get(0).getMapindex();
                }
                break;
            default:
                break;
        }
        
        CntPlatformSync platform = new CntPlatformSync();
        platform.setObjecttype(objecttype);
        platform.setObjectindex(objectindex);
        CntTargetSync target = new CntTargetSync();
        target.setObjecttype(objecttype);
        target.setObjectindex(Long.valueOf(objectindex));
        List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
        List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
        if(targetList!=null&&targetList.size()>0){
            for(CntTargetSync cnttarget:targetList){
                int cntSyncStatus = cnttarget.getStatus();
                if(cntSyncStatus==200||cntSyncStatus==300){
                    rtnInfo.setFlag(GlobalConstants.FAIL);
                    rtnInfo.setReturnMessage(PictureConstants.PIC_STATUS_NOTRIGHT);
                    return rtnInfo.toString();
                }
            }
            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
            cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
        }
        
        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                ServicePictureMap servicePicMap = new ServicePictureMap();
                servicePicMap.setServiceindex(relativeIndex);
                servicePicMap.setPictureindex(picIndex);
                List<ServicePictureMap> servicePicMapList = servicePictureMapDS
                        .getServicePictureMapByCond(servicePicMap);
                if (servicePicMapList != null && servicePicMapList.size() > 0)
                {
                    servicePictureMapDS.removeServicePictureMapList(servicePicMapList);
                }
                break;
            case PictureConstants.CATAGORY:
                CategoryPictureMap categoryPicMap = new CategoryPictureMap();
                categoryPicMap.setCategoryindex(relativeIndex);
                categoryPicMap.setPictureindex(picIndex);
                List<CategoryPictureMap> categoryPicMapList = categoryPictureMapDS
                        .getCategoryPictureMapByCond(categoryPicMap);
                if (categoryPicMapList != null && categoryPicMapList.size() > 0)
                {
                    categoryPictureMapDS.removeCategoryPictureMapList(categoryPicMapList);
                }
                break;
            case PictureConstants.PROGRAM:
                ProgramPictureMap programPicMap = new ProgramPictureMap();
                programPicMap.setProgramindex(relativeIndex);
                programPicMap.setPictureindex(picIndex);
                List<ProgramPictureMap> programPictureMapList = programPictureMapDS
                        .getProgramPictureMapByCond(programPicMap);
                if (programPictureMapList != null && programPictureMapList.size() > 0)
                {
                    programPictureMapDS.removeProgramPictureMapList(programPictureMapList);
                }
                break;
            case PictureConstants.SERIES:
                SeriesPictureMap seriesPicMap = new SeriesPictureMap();
                seriesPicMap.setSeriesindex(relativeIndex);
                seriesPicMap.setPictureindex(picIndex);
                List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS
                        .getSeriesPictureMapByCond(seriesPicMap);
                if (seriesPictureMapList != null && seriesPictureMapList.size() > 0)
                {
                    seriesPictureMapDS.removeSeriesPictureMapList(seriesPictureMapList);
                }
                break;
            case PictureConstants.CHANNEL:
                ChannelPictureMap channelPicMap = new ChannelPictureMap();
                channelPicMap.setChannelindex(relativeIndex);
                channelPicMap.setPictureindex(picIndex);
                List<ChannelPictureMap> channelPictureMapList = channelPictureMapDS
                        .getChannelPictureMapByCond(channelPicMap);
                if (channelPictureMapList != null && channelPictureMapList.size() > 0)
                {
                    channelPictureMapDS.removeChannelPictureMapList(channelPictureMapList);
                }
                break;
            case PictureConstants.CAST:
                CastPictureMap castPicMap = new CastPictureMap();
                castPicMap.setCastindex(relativeIndex);
                castPicMap.setPictureindex(picIndex);
                List<CastPictureMap> castPictureMapList = castPictureMapDS.getCastPictureMapByCond(castPicMap);
                if (castPictureMapList != null && castPictureMapList.size() > 0)
                {
                    castPictureMapDS.removeCastPictureMapList(castPictureMapList);
                }
                break;
            default:
                break;
        }

        pictureDS.removePicture(cond);
        //插入删除文件的文件迁移任务
        CmsFtptask ftptask = new CmsFtptask();
        ftptask.setTasktype(1L); // 1-普通类型
        ftptask.setFiletype(2); // 2-picture
        ftptask.setFileindex(pic.getPictureindex());
        ftptask.setUploadtype(5); // 5-本地文件删除
        ftptask.setSrcaddress(pic.getPictureurl());
        ftptask.setStatus(0); // 0-待上传
        ftptask.setSrcfilehandle(1); // 1-删除
        cmsFtptaskDS.insertCmsFtptask(ftptask);
        rtnInfo.setFlag(GlobalConstants.SUCCESS);
        rtnInfo.setReturnMessage(PictureConstants.PIC_SUCCESS); // "100000":操作成功
        CommonLogUtil.insertOperatorLog(pic.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                PictureConstants.OPERTYPE_PICTURE_DEL, PictureConstants.OPERTYPE_PICTURE_DEL_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        log.debug("PictureLS: removePictureandMap end");
        return rtnInfo.toString();
    }

    /**
     * 批量删除海报，同时删除其与对象关联的Map关系
     * 
     * @param picIndexList 海报主键(以逗号隔开)
     * @param objType 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
     * @param relativeIndex 相关对象index
     * @return 0:操作成功 每条的操作详细结果 1:失败原因 每条的操作详细结果
     * @throws Exception
     */
    public String batchRemovePicMap(List<String> picIndexList, Integer objType, Long relativeIndex) throws Exception
    {
        log.debug("PictureLS: batchRemovePicMap starting..."); // 文件名
        com.zte.cms.picture.common.ReturnInfo rtnInfo = new com.zte.cms.picture.common.ReturnInfo(); // 返回信息类
        com.zte.cms.picture.common.OperateInfo operateInfo = null;
        int totalCnt = 0;
        int errorCnt = 0;
        int successCnt = 0;
        int infoCnt = 0;
        int objecttype = 0;
        Long objectindex = null;
        boolean isBreak = false;

        if (null != picIndexList && picIndexList.size() > 0)
        {
            totalCnt = picIndexList.size();
            for (int i = 0; i < totalCnt; i++)
            {
                Picture cond = new Picture();
                cond.setPictureindex(Long.parseLong(picIndexList.get(i)));
                Picture pic = null;
                pic = pictureDS.getPicture(cond);
                if (null == pic)
                {
                    operateInfo = new com.zte.cms.picture.common.OperateInfo(String.valueOf(++infoCnt), picIndexList
                            .get(i), ResourceManager.getResourceTextByCode(PictureConstants.PIC_FAILED),
                            ResourceManager.getResourceTextByCode(PictureConstants.PIC_OBJECT_NOTEXIST));
                    rtnInfo.appendOperateInfo(operateInfo);
                    errorCnt++;
                    continue;
                }
                
                switch (objType.intValue())
                {
                    case PictureConstants.SERVIEC:
                        ServicePictureMap servicePicMap = new ServicePictureMap();
                        servicePicMap.setServiceindex(relativeIndex);
                        servicePicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ServicePictureMap> servicePicMapList = servicePictureMapDS
                                .getServicePictureMapByCond(servicePicMap);
                        if (servicePicMapList != null && servicePicMapList.size() > 0)
                        {
                            objecttype = 25;
                            objectindex = servicePicMapList.get(0).getMapindex();
                        }
                        break;
                    case PictureConstants.CATAGORY:
                        CategoryPictureMap categoryPicMap = new CategoryPictureMap();
                        categoryPicMap.setCategoryindex(relativeIndex);
                        categoryPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<CategoryPictureMap> categoryPicMapList = categoryPictureMapDS
                                .getCategoryPictureMapByCond(categoryPicMap);
                        if (categoryPicMapList != null && categoryPicMapList.size() > 0)
                        {
                            objecttype = 31;
                            objectindex = categoryPicMapList.get(0).getMapindex();
                        }
                        break;
                    case PictureConstants.PROGRAM:
                        ProgramPictureMap programPicMap = new ProgramPictureMap();
                        programPicMap.setProgramindex(relativeIndex);
                        programPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ProgramPictureMap> programPictureMapList = programPictureMapDS
                                .getProgramPictureMapByCond(programPicMap);
                        if (programPictureMapList != null && programPictureMapList.size() > 0)
                        {
                            objecttype = 35;
                            objectindex = programPictureMapList.get(0).getMapindex();
                        }
                        break;
                    case PictureConstants.SERIES:
                        SeriesPictureMap seriesPicMap = new SeriesPictureMap();
                        seriesPicMap.setSeriesindex(relativeIndex);
                        seriesPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS
                                .getSeriesPictureMapByCond(seriesPicMap);
                        if (seriesPictureMapList != null && seriesPictureMapList.size() > 0)
                        {
                            objecttype = 38;
                            objectindex = seriesPictureMapList.get(0).getMapindex();
                        }
                        break;
                    case PictureConstants.CHANNEL:
                        ChannelPictureMap channelPicMap = new ChannelPictureMap();
                        channelPicMap.setChannelindex(relativeIndex);
                        channelPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ChannelPictureMap> channelPictureMapList = channelPictureMapDS
                                .getChannelPictureMapByCond(channelPicMap);
                        if (channelPictureMapList != null && channelPictureMapList.size() > 0)
                        {
                            objecttype = 39;
                            objectindex = channelPictureMapList.get(0).getMapindex();
                        }
                        break;
                    case PictureConstants.CAST:
                        CastPictureMap castPicMap = new CastPictureMap();
                        castPicMap.setCastindex(relativeIndex);
                        castPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<CastPictureMap> castPictureMapList = castPictureMapDS.getCastPictureMapByCond(castPicMap);
                        if (castPictureMapList != null && castPictureMapList.size() > 0)
                        {
                            objecttype = 41;
                            objectindex = castPictureMapList.get(0).getMapindex();
                        }
                        break;
                    default:
                        break;
                }
                
                CntPlatformSync platform = new CntPlatformSync();
                platform.setObjecttype(objecttype);
                platform.setObjectindex(objectindex);
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(objecttype);
                target.setObjectindex(objectindex);
                List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                if(targetList!=null&&targetList.size()>0){
                    for(CntTargetSync cnttarget:targetList){
                        int cntSyncStatus = cnttarget.getStatus();
                        if(cntSyncStatus==200||cntSyncStatus==300){
                            operateInfo = new com.zte.cms.picture.common.OperateInfo(String.valueOf(++infoCnt), picIndexList
                                    .get(i), ResourceManager.getResourceTextByCode(PictureConstants.PIC_FAILED),
                                    ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));
                            rtnInfo.appendOperateInfo(operateInfo);
                            errorCnt++;
                            isBreak = true;
                            break;
                        }
                    }
                    if(isBreak == true){
                        continue;
                    }
                    cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
                    cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
                }

                switch (objType.intValue())
                {
                    case PictureConstants.SERVIEC:
                        ServicePictureMap servicePicMap = new ServicePictureMap();
                        servicePicMap.setServiceindex(relativeIndex);
                        servicePicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ServicePictureMap> servicePicMapList = servicePictureMapDS
                                .getServicePictureMapByCond(servicePicMap);
                        if (servicePicMapList != null && servicePicMapList.size() > 0)
                        {
                            servicePictureMapDS.removeServicePictureMapList(servicePicMapList);
                        }
                        break;
                    case PictureConstants.CATAGORY:
                        CategoryPictureMap categoryPicMap = new CategoryPictureMap();
                        categoryPicMap.setCategoryindex(relativeIndex);
                        categoryPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<CategoryPictureMap> categoryPicMapList = categoryPictureMapDS
                                .getCategoryPictureMapByCond(categoryPicMap);
                        if (categoryPicMapList != null && categoryPicMapList.size() > 0)
                        {
                            categoryPictureMapDS.removeCategoryPictureMapList(categoryPicMapList);
                        }
                        break;
                    case PictureConstants.PROGRAM:
                        ProgramPictureMap programPicMap = new ProgramPictureMap();
                        programPicMap.setProgramindex(relativeIndex);
                        programPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ProgramPictureMap> programPictureMapList = programPictureMapDS
                                .getProgramPictureMapByCond(programPicMap);
                        if (programPictureMapList != null && programPictureMapList.size() > 0)
                        {
                            objecttype = 35;
                            objectindex = programPictureMapList.get(0).getMapindex();
                            programPictureMapDS.removeProgramPictureMapList(programPictureMapList);
                        }
                        break;
                    case PictureConstants.SERIES:
                        SeriesPictureMap seriesPicMap = new SeriesPictureMap();
                        seriesPicMap.setSeriesindex(relativeIndex);
                        seriesPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS
                                .getSeriesPictureMapByCond(seriesPicMap);
                        if (seriesPictureMapList != null && seriesPictureMapList.size() > 0)
                        {
                            seriesPictureMapDS.removeSeriesPictureMapList(seriesPictureMapList);
                        }
                        break;
                    case PictureConstants.CHANNEL:
                        ChannelPictureMap channelPicMap = new ChannelPictureMap();
                        channelPicMap.setChannelindex(relativeIndex);
                        channelPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<ChannelPictureMap> channelPictureMapList = channelPictureMapDS
                                .getChannelPictureMapByCond(channelPicMap);
                        if (channelPictureMapList != null && channelPictureMapList.size() > 0)
                        {
                            channelPictureMapDS.removeChannelPictureMapList(channelPictureMapList);
                        }
                        break;
                    case PictureConstants.CAST:
                        CastPictureMap castPicMap = new CastPictureMap();
                        castPicMap.setCastindex(relativeIndex);
                        castPicMap.setPictureindex(Long.parseLong(picIndexList.get(i)));
                        List<CastPictureMap> castPictureMapList = castPictureMapDS.getCastPictureMapByCond(castPicMap);
                        if (castPictureMapList != null && castPictureMapList.size() > 0)
                        {
                            objecttype = 41;
                            objectindex = castPictureMapList.get(0).getMapindex();
                            castPictureMapDS.removeCastPictureMapList(castPictureMapList);
                        }
                        break;
                    default:
                        break;
                }
                
                pictureDS.removePicture(cond);
                //插入删除文件的文件迁移任务
                CmsFtptask ftptask = new CmsFtptask();
                ftptask.setTasktype(1L); // 1-普通类型
                ftptask.setFiletype(2); // 2-picture
                ftptask.setFileindex(pic.getPictureindex());
                ftptask.setUploadtype(5); // 5-本地文件删除
                ftptask.setSrcaddress(pic.getPictureurl());
                ftptask.setStatus(0); // 0-待上传
                ftptask.setSrcfilehandle(1); // 1-删除
                cmsFtptaskDS.insertCmsFtptask(ftptask);

                operateInfo = new com.zte.cms.picture.common.OperateInfo(String.valueOf(++infoCnt),
                        pic.getPictureid(), ResourceManager.getResourceTextByCode(PictureConstants.PIC_SUCCESS),
                        ResourceManager.getResourceTextByCode(PictureConstants.PIC_SUCCESS));
                rtnInfo.appendOperateInfo(operateInfo);
                successCnt++;

                CommonLogUtil.insertOperatorLog(pic.getPictureid(), PictureConstants.MGTTYPE_PICTURE_MGT,
                        PictureConstants.OPERTYPE_PICTURE_DEL, PictureConstants.OPERTYPE_PICTURE_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
        }

        if (errorCnt > 0)
        {
            if (errorCnt == totalCnt)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage("失败数量：" + errorCnt);
            }
            else
            {
                rtnInfo.setFlag("2");
                rtnInfo.setReturnMessage("成功数量：" + successCnt + " 失败数量：" + errorCnt);
            }

        }
        else if (successCnt == totalCnt)
        {
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage("成功数量：" + successCnt);
        }

        // rtnInfo.setFlag(GlobalConstants.SUCCESS);
        // rtnInfo.setReturnMessage(PictureConstants.PIC_SUCCESS); //"100000":操作成功
        log.debug("PictureLS: batchRemovePicMap end");
        return rtnInfo.toString();
    }

    public List<Picture> getPictureByType(Integer objType, Long relativeIndex) throws DomainServiceException
    {
        // 关联对象类型:1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
        log.debug("getPictureByType starting..........................");
        List<Picture> result = null;
        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                result = pictureDS.getPictureByServiceIndex(relativeIndex);
                break;
            case PictureConstants.CATAGORY:
                result = pictureDS.getPictureByCatagoryIndex(relativeIndex);
                break;
            case PictureConstants.PROGRAM:
                result = pictureDS.getPictureByProgramIndex(relativeIndex);
                break;
            case PictureConstants.SERIES:
                result = pictureDS.getPictureBySeriesIndex(relativeIndex);
                break;
            case PictureConstants.CHANNEL:
                result = pictureDS.getPictureByChannelIndex(relativeIndex);
                break;
            case PictureConstants.CAST:
                result = pictureDS.getPictureByCastIndex(relativeIndex);
                break;
            default:
                break;
        }
        log.debug("getPictureByType  end.................................");
        return result;
    }

    /**
     * <p>
     * 获取文件预览时的文件路径
     * </p>
     * 
     * @param fileindex 海报主键
     * @return 返回文件路径
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.picture
     * @see com.zte.picture.model.IcmFile
     * @since UCDN
     */
    public String getPreviewPath(Long fileindex) throws Exception
    {
        log.debug("PictureLS: getPreviewPath starting...");

        String previewPath = "";

        List<UsysConfig> usysConfigList = null;
        UsysConfig usysConfig = new UsysConfig();

        // usysConfig.setCfgkey(PictureConstants.HTTP_PREWIEW_PATH_CFG); //公网http方式文件预览路径
        usysConfig.setCfgkey(PictureConstants.CMS_PREWIEW_PATH_CFG); // 公网http方式文件预览路径
        try
        {
            log.debug("UsysConfigDS: getUsysConfigByCond starting...");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            log.debug("UsysConfigDS: getUsysConfigByCond end");

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                Picture picture = new Picture();
                picture.setPictureindex(fileindex);
                try
                {
                    picture = pictureDS.getPicture(picture);
                }
                catch (Exception e)
                {
                    log.error("Error occurred in getPicture method", e);
                    throw new Exception(ResourceManager.getResourceText(e));
                }

                if (null == picture)
                {// 海报不存在
                    throw new Exception(PictureConstants.PIC_OBJECT_NOTEXIST);
                }
                else
                {
                    // 获取海报相对路径pictureurl并替换掉pictureurl中多余的linux文件分隔符为单一的windows分隔符
                    String addresspath = (picture.getPictureurl()).replaceAll("/+", "/");
                    previewPath = usysConfig.getCfgvalue() + addresspath;
                }
            }
            else
            {// "100008"：未配置 系统参数WINDOWS共享访问点
                throw new Exception(PictureConstants.PIC_NOCONFIG_WINDOWSHARE);
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in getPreviewPath method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("PictureLS: getPreviewPath end");
        return previewPath;
    }

    /**
     * 根据 programindex 查询图片信息
     */
    public List<Picture> queryPictureListByProgramIndex(String programindex) throws Exception
    {
        log.debug("queryPictureListByProgramIndex begin.......");
        List<Picture> pictureList = null;

        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        String previewPath = "";
        usysConfig.setCfgkey("cms.preview.path");
        try
        {
            log.debug("UsysConfigDS: getUsysConfigByCond starting...");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            log.debug("UsysConfigDS: getUsysConfigByCond end");
        }
        catch (Exception e)
        {
            log.error("Error occurred in getUsysConfigByCond method", e);
        }
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            previewPath = usysConfig.getCfgvalue();
        }
        pictureList = pictureDS.getPictureByProgramIndex(Long.parseLong(programindex));
        if (null != pictureList && pictureList.size() > 0)
        {
            for (int i = 0; i < pictureList.size(); i++)
            {
                Picture pic = pictureList.get(i);
                String path = pic.getPictureurl();
                path = path.replace("/", "\\");
                pic.setPictureurl(previewPath + path);
            }
        }
        log.debug("queryPictureListByProgramIndex end.......");
        return pictureList;
    }

    /**
     * 根据channelindex查询图片信息
     */
    public List<Picture> queryChannelPictureListByChnanelindex(Long channelindex) throws Exception
    {
        log.debug("queryPictureListByChannelIndex begin.......");
        List<Picture> pictureList = null;
        UsysConfig usysConfig = new UsysConfig();
        List<UsysConfig> usysConfigList = null;
        String previewPath = "";
        usysConfig.setCfgkey("cms.preview.path");
        try
        {
            log.debug("UsysConfigDS: getUsysConfigByCond starting...");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            log.debug("UsysConfigDS: getUsysConfigByCond end");
        }
        catch (Exception e)
        {
            log.error("Error occurred in getUsysConfigByCond method", e);
        }
        if (null != usysConfigList && usysConfigList.size() > 0)
        {
            usysConfig = usysConfigList.get(0);
            previewPath = usysConfig.getCfgvalue();
        }
        pictureList = pictureDS.queryChannelPictureListByChnanelindex(channelindex);
        if (null != pictureList && pictureList.size() > 0)
        {
            for (int i = 0; i < pictureList.size(); i++)
            {
                Picture pic = pictureList.get(i);
                String path = pic.getPictureurl();
                pic.setPictureurl(previewPath + path);
            }
        }
        log.debug("queryPictureListByChannelIndex end.......");
        return pictureList;
    }

    public TableDataInfo queryPictureByType(Integer objType, Long relativeIndex, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQueryPictureByType starting....");
        try
        {
            TableDataInfo tableDataInfo = pictureDS.pageInfoQueryByType(objType, relativeIndex, start, pageSize);
            return tableDataInfo;
        }
        catch (Exception e)
        {
            log.error("pageInfoQueryPictureByType starting...." + e.getMessage());
        }
        return null;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public void setPrimaryKeyGenerator(IPrimaryKeyGenerator primaryKeyGenerator)
    {
        this.primaryKeyGenerator = primaryKeyGenerator;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setCastPictureMapDS(ICastPictureMapDS castPictureMapDS)
    {
        this.castPictureMapDS = castPictureMapDS;
    }

    public void setCastDS(ICastcdnDS castDS)
    {
        this.castDS = castDS;
    }

    public ICmsStorageareaLS getCmsStorageareaLS()
    {
        return cmsStorageareaLS;
    }

    public void setServiceDS(IServiceDS serviceDS)
    {
        this.serviceDS = serviceDS;
    }

    public void setServicePictureMapDS(IServicePictureMapDS servicePictureMapDS)
    {
        this.servicePictureMapDS = servicePictureMapDS;
    }

    public void setCategoryDS(ICategorycdnDS categoryDS)
    {
        this.categoryDS = categoryDS;
    }

    public void setCategoryPictureMapDS(ICategoryPictureMapDS categoryPictureMapDS)
    {
        this.categoryPictureMapDS = categoryPictureMapDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setProgramPictureMapDS(IProgramPictureMapDS programPictureMapDS)
    {
        this.programPictureMapDS = programPictureMapDS;
    }

    public void setSeriesPictureMapDS(ISeriesPictureMapDS seriesPictureMapDS)
    {
        this.seriesPictureMapDS = seriesPictureMapDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public void setChannelPictureMapDS(IChannelPictureMapDS channelPictureMapDS)
    {
        this.channelPictureMapDS = channelPictureMapDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setSeriesDS(ICmsSeriesDS seriesDS)
    {
        this.seriesDS = seriesDS;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    
}

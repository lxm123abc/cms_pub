package com.zte.cms.picture.ls;

import java.util.List;

import com.zte.cms.picture.model.Picture;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IPictureLS
{

    /**
     * 查询Picture对象
     * 
     * @param
     * @return IcmChannelSchedule对象
     * @throws DomainServiceException ds异常
     */
    public abstract List<Picture> getPictureByType(Integer objType, Long relativeIndex) throws DomainServiceException;

    public abstract TableDataInfo queryPictureByType(Integer objType, Long relativeIndex, int start, int pageSize)
            throws Exception;

    /**
     * <p>
     * 获取文件预览时的文件路径
     * </p>
     * 
     * @param fileindex 海报主键
     * @return 返回文件路径
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表zxdbm_cms.picture
     * @see com.zte.picture.model.IcmFile
     * @since UCDN
     */
    public String getPreviewPath(Long fileindex) throws Exception;

    /**
     * 上传海报
     * 
     * @param objType 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
     * @param relativeIndex 对象index
     * @return 0:操作成功 1:失败原因
     */
    public abstract String addPicture(Picture pic, Integer picMaptype, Integer objType, Long relativeIndex,
            String[] fileNames) throws Exception;

    /**
     * 删除海报，同时删除其与对象关联的Map关系
     * 
     * @param picIndex 海报主键
     * @param objType 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
     * @param relativeIndex 相关对象index
     * @return 0:操作成功 1:失败原因
     * @throws Exception
     */
    public abstract String removePictureandMap(Long picIndex, Integer objType, Long relativeIndex) throws Exception;

    /**
     * 批量删除海报，同时删除其与对象关联的Map关系
     * 
     * @param picIndexList 海报主键List
     * @param objType 对象类型：1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
     * @param relativeIndex 相关对象index
     * @return 0:操作成功 每条的操作详细结果 1:失败原因 每条的操作详细结果
     * @throws Exception
     */
    public abstract String batchRemovePicMap(List<String> picIndexList, Integer objType, Long relativeIndex)
            throws Exception;

    /**
     * 根据 programindex 查询关联的海报信息
     * 
     * @param programindex
     * @return
     * @throws Exception
     */
    public abstract List<Picture> queryPictureListByProgramIndex(String programindex) throws Exception;

    /**
     * 根据 channelindex和maptype 查询关联的海报信息
     * 
     * @param channelindex
     * @return
     * @throws Exception
     */
    public List<Picture> queryChannelPictureListByChnanelindex(Long channelindex) throws Exception;
}
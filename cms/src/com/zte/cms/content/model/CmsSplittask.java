package com.zte.cms.content.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsSplittask extends DynamicBaseObject
{
    private java.lang.Long taskindex;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer status;
    private java.lang.Integer tasktype;
    private java.lang.String destaddress;
    private java.lang.String rtntime;
    private java.lang.Long rtncode;
    private java.lang.String rtnresult;
    private java.lang.String opername;
    private java.lang.String cpid;

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getTasktype()
    {
        return tasktype;
    }

    public void setTasktype(java.lang.Integer tasktype)
    {
        this.tasktype = tasktype;
    }

    public java.lang.String getDestaddress()
    {
        return destaddress;
    }

    public void setDestaddress(java.lang.String destaddress)
    {
        this.destaddress = destaddress;
    }

    public java.lang.String getRtntime()
    {
        return rtntime;
    }

    public void setRtntime(java.lang.String rtntime)
    {
        this.rtntime = rtntime;
    }

    public java.lang.Long getRtncode()
    {
        return rtncode;
    }

    public void setRtncode(java.lang.Long rtncode)
    {
        this.rtncode = rtncode;
    }

    public java.lang.String getRtnresult()
    {
        return rtnresult;
    }

    public void setRtnresult(java.lang.String rtnresult)
    {
        this.rtnresult = rtnresult;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public void initRelation()
    {
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("status", "STATUS");
        this.addRelation("tasktype", "TASKTYPE");
        this.addRelation("destaddress", "DESTADDRESS");
        this.addRelation("rtntime", "RTNTIME");
        this.addRelation("rtncode", "RTNCODE");
        this.addRelation("rtnresult", "RTNRESULT");
        this.addRelation("opername", "OPERNAME");
        this.addRelation("cpid", "CPID");
    }
}

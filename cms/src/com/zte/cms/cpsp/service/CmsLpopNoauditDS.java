package com.zte.cms.cpsp.service;

import java.util.List;

import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.cpsp.dao.ICmsLpopNoauditDAO;
import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CmsLpopNoauditDS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsLpopNoauditDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsLpopNoauditDAO dao = null;

    public void setDao(ICmsLpopNoauditDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("insert cmsLpopNoaudit starting...");
        try
        {
            Long mapinex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_lpop_noaudit");
            cmsLpopNoaudit.setMapindex(mapinex);
            dao.insertCmsLpopNoaudit(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsLpopNoaudit end");
    }

    public void updateCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("update cmsLpopNoaudit by pk starting...");
        try
        {
            dao.updateCmsLpopNoaudit(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsLpopNoaudit by pk end");
    }

    public void updateCmsLpopNoauditList(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException
    {
        log.debug("update cmsLpopNoauditList by pk starting...");
        if (null == cmsLpopNoauditList || cmsLpopNoauditList.size() == 0)
        {
            log.debug("there is no datas in cmsLpopNoauditList");
            return;
        }
        try
        {
            for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
            {
                dao.updateCmsLpopNoaudit(cmsLpopNoaudit);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsLpopNoauditList by pk end");
    }

    public void updateCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("update cmsLpopNoaudit by condition starting...");
        try
        {
            dao.updateCmsLpopNoauditByCond(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsLpopNoaudit by condition end");
    }

    public void updateCmsLpopNoauditListByCond(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException
    {
        log.debug("update cmsLpopNoauditList by condition starting...");
        if (null == cmsLpopNoauditList || cmsLpopNoauditList.size() == 0)
        {
            log.debug("there is no datas in cmsLpopNoauditList");
            return;
        }
        try
        {
            for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
            {
                dao.updateCmsLpopNoauditByCond(cmsLpopNoaudit);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsLpopNoauditList by condition end");
    }

    public void removeCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("remove cmsLpopNoaudit by pk starting...");
        try
        {
            dao.deleteCmsLpopNoaudit(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsLpopNoaudit by pk end");
    }

    public void removeCmsLpopNoauditList(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException
    {
        log.debug("remove cmsLpopNoauditList by pk starting...");
        if (null == cmsLpopNoauditList || cmsLpopNoauditList.size() == 0)
        {
            log.debug("there is no datas in cmsLpopNoauditList");
            return;
        }
        try
        {
            for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
            {
                dao.deleteCmsLpopNoaudit(cmsLpopNoaudit);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsLpopNoauditList by pk end");
    }

    public void removeCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("remove cmsLpopNoaudit by condition starting...");
        try
        {
            dao.deleteCmsLpopNoauditByCond(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsLpopNoaudit by condition end");
    }

    public void removeCmsLpopNoauditListByCond(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException
    {
        log.debug("remove cmsLpopNoauditList by condition starting...");
        if (null == cmsLpopNoauditList || cmsLpopNoauditList.size() == 0)
        {
            log.debug("there is no datas in cmsLpopNoauditList");
            return;
        }
        try
        {
            for (CmsLpopNoaudit cmsLpopNoaudit : cmsLpopNoauditList)
            {
                dao.deleteCmsLpopNoauditByCond(cmsLpopNoaudit);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsLpopNoauditList by condition end");
    }

    public CmsLpopNoaudit getCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit by pk starting...");
        CmsLpopNoaudit rsObj = null;
        try
        {
            rsObj = dao.getCmsLpopNoaudit(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsLpopNoauditList by pk end");
        return rsObj;
    }

    public List<CmsLpopNoaudit> getCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit by condition starting...");
        List<CmsLpopNoaudit> rsList = null;
        try
        {
            rsList = dao.getCmsLpopNoauditByCond(cmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsLpopNoaudit by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsLpopNoaudit, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsLpopNoaudit>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsLpopNoaudit page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsLpopNoaudit, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();

        tableInfo.setData((List<CmsLpopNoaudit>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsLpopNoaudit page info by condition end");
        return tableInfo;
    }

    public TableDataInfo pageInfoQuery(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(ucpcmsLpopNoaudit, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();

        tableInfo.setData((List<UcpCmsLpopNoaudit>) pageInfo.getResult());

        tableInfo.setTotalCount((int) pageInfo.getTotalCount());

        log.debug("get cmsLpopNoaudit page info by condition end");
        return tableInfo;
    }

    public List<UcpCmsLpopNoaudit> getCmsLpopNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit)
            throws DomainServiceException
    {
        log.debug("get cmsLpopNoaudit by condition starting...");
        List<UcpCmsLpopNoaudit> rsList = null;
        try
        {
            rsList = dao.getCmsLpopNoauditList(ucpCmsLpopNoaudit);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsLpopNoaudit by condition end");
        return rsList;
    }

    /**
     * 查询CP信息 以table展示
     */
    public TableDataInfo pageInfoQueryForOPTable(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize)
            throws DAOException
    {
        log.debug("get cmsLpopNoaudit page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryForOPTable(ucpcmsLpopNoaudit, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            try
            {
                throw new DomainServiceException(daoEx);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<UcpCmsLpopNoaudit>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsLpopNoaudit page info by condition end");
        return tableInfo;
    }

}

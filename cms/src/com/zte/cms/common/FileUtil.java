package com.zte.cms.common;

import java.io.*;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

public class FileUtil
{
    private Log log = SSBBus.getLog(getClass());

    /**
     * Constructor
     */
    public FileUtil()
    {
    }

    /**
     * 新建文件夹
     * 
     * @param folderPath 文件夹路径
     */
    public void newFolder(String folderPath)
    {
        try
        {
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            if (!myFilePath.exists())
            {
                myFilePath.mkdir();
            }
        }
        catch (Exception e)
        {
            System.out.println("新建文件夹失败");
            e.printStackTrace();
        }
    }

    /**
     * 新建文件
     * 
     * @param filePathAndName String �ļ�·������� ��c:/fqf.txt
     * @param fileContent String �ļ�����
     */
    public void newFile(String filePathAndName, String fileContent)
    {

        try
        {
            String filePath = filePathAndName;
            filePath = filePath.toString();
            File myFilePath = new File(filePath);
            if (!myFilePath.exists())
            {
                myFilePath.createNewFile();
            }
            FileWriter resultFile = new FileWriter(myFilePath);
            PrintWriter myFile = new PrintWriter(resultFile);
            String strContent = fileContent;
            myFile.println(strContent);
            resultFile.close();

        }
        catch (Exception e)
        {
            System.out.println("新建文件失败");
            e.printStackTrace();

        }

    }

    /**
     * ɾ���ļ�
     * 
     * @param filePathAndName String �ļ�·������� ��c:/fqf.txt
     */
    public void delFile(String filePathAndName)
    {
        try
        {
            String filePath = filePathAndName;
            filePath = filePath.toString();
            java.io.File myDelFile = new java.io.File(filePath);
            myDelFile.delete();

        }
        catch (Exception e)
        {
            System.out.println("ɾ���ļ�������");
            e.printStackTrace();

        }

    }

    /**
     * ɾ���ļ���
     * 
     * @param folderPath String �ļ���·������� ��c:/fqf
     */
    public void delFolder(String folderPath)
    {
        try
        {
            delAllFile(folderPath); // ɾ����������������
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            myFilePath.delete(); // ɾ����ļ���

        }
        catch (Exception e)
        {
            System.out.println("ɾ���ļ��в�����");
            e.printStackTrace();

        }

    }

    /**
     * ɾ���ļ�������������ļ�
     * 
     * @param path String �ļ���·�� �� c:/fqf
     */
    public void delAllFile(String path)
    {
        File file = new File(path);
        if (!file.exists())
        {
            return;
        }
        if (!file.isDirectory())
        {
            return;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++)
        {
            if (path.endsWith(File.separator))
            {
                temp = new File(path + tempList[i]);
            }
            else
            {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isFile())
            {
                temp.delete();
            }
            if (temp.isDirectory())
            {
                delAllFile(path + "/" + tempList[i]); // ��ɾ���ļ���������ļ�
                delFolder(path + "/" + tempList[i]); // ��ɾ����ļ���
            }
        }
    }

    /**
     * ���Ƶ����ļ�
     * 
     * @param oldPath String ԭ
     * @param newPath String t
     * @throws Exception Exception
     */
    public void copyFile(String oldFullPath, String newPath, String newName) throws Exception
    {
        try
        {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldFullPath);
            if (oldfile.exists())// 源文件如果存在
            {
                InputStream inStream = new FileInputStream(oldFullPath);
                // 如果目标目录不存在则创建目录
                File newFilePath = new File(newPath);
                if (!newFilePath.exists())
                {
                    newFilePath.mkdirs();
                }
                FileOutputStream fs = new FileOutputStream(newPath + newName);
                byte[] buffer = new byte[1444];
                int length;
                while ((byteread = inStream.read(buffer)) != -1)
                {
                    bytesum += byteread; // �ֽ��� �ļ���С
                    // System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                fs.close();
                inStream.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("复制文件失败");
            e.printStackTrace();
            throw e;
        }

    }

    /**
     * ��������ļ�������
     * 
     * @param oldPath String ԭ�ļ�·�� �磺c:/fqf
     * @param newPath String ���ƺ�·�� �磺f:/fqf/ff
     * @throws Exception Exception
     */
    public void copyFolder(String oldPath, String newPath) throws Exception
    {

        try
        {
            (new File(newPath)).mkdirs(); // ����ļ��в����� ��b���ļ���
            File a = new File(oldPath);
            String[] file = a.list();
            File temp = null;
            for (int i = 0; i < file.length; i++)
            {
                if (oldPath.endsWith("/"))
                {
                    temp = new File(oldPath + file[i]);
                }
                else
                {
                    temp = new File(oldPath + "/" + file[i]);
                }

                if (temp.isFile())
                {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(newPath + "/" + (temp.getName()).toString());
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ((len = input.read(b)) != -1)
                    {
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory())
                { // ��������ļ���
                    copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("��������ļ������ݲ�����");
            e.printStackTrace();
            throw e;

        }

    }

    /**
     * 移动文件
     * 
     * @param oldPath String �磺c:/fqf.txt
     * @param newPath String �磺d:/fqf.txt
     * @throws Exception Exception
     */
    public void moveFile(String oldFullPath, String newPath, String newName) throws Exception
    {
        try
        {
            copyFile(oldFullPath, newPath, newName);
            delFile(oldFullPath);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * �ƶ��ļ���ָ��Ŀ¼
     * 
     * @param oldPath String �磺c:/fqf.txt
     * @param newPath String �磺d:/fqf.txt
     * @throws Exception Exception
     */
    public void moveFolder(String oldPath, String newPath) throws Exception
    {
        try
        {
            copyFolder(oldPath, newPath);
            delFolder(oldPath);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }
    /**
     * 增加一个功能，判断当拷到的目标路径中有该文件存在时，return,不拷贝
     * @param oldFullPath
     * @param newPath
     * @param newName
     * @throws Exception
     * added by 冯超
     */
    public void copyFileCheckExits(String oldFullPath, String newPath, String newName) throws Exception
    {
        try
        {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldFullPath);
            if (oldfile.exists())// 源文件如果存在
            {
                InputStream inStream = new FileInputStream(oldFullPath);
                // 如果目标目录不存在则创建目录
                File newFilePath = new File(newPath);
                if (!newFilePath.exists())
                {
                    return;
                }
                //如果目标文件不存在则创建目标文件
                File newFilePathName = new File(newPath+ newName);
                if (newFilePathName.exists())
                {
                    return;
                }
                FileOutputStream fs = new FileOutputStream(newPath + newName);
                byte[] buffer = new byte[1444];
                int length;
                while ((byteread = inStream.read(buffer)) != -1)
                {
                    bytesum += byteread; // �ֽ��� �ļ���С
                    // System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                fs.close();
                inStream.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("复制文件失败");
            e.printStackTrace();
            throw e;
        }

    }
    
}

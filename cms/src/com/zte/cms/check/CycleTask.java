package com.zte.cms.check;

import java.util.Date;

import org.quartz.Trigger;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;

import com.zte.ssb.device.customtimer.service.ISchedulerService;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.cms.check.CheckConstant;
import com.zte.cms.check.ICycleTask;
import com.zte.cms.common.GlobalConstants;
import com.zte.ismp.common.ConfigUtil;

/**
 * <p>
 * 文件名称: CycleTask.java
 * </p>
 * <p>
 * 文件描述: 定时任务管理
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 定时任务操作类
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年1月20日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 *
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 *
 * <p>
 * 修改记录2：
 * </p>
 *
 * @author 黄杰弟
 * @version 1.0
 */

public class CycleTask implements ICycleTask {

    private Log log = SSBBus.getLog(getClass());

    /**
     * 初始化，如果没有定时器，则启动定时器。在index.html中调用
     */
    public String init() throws Exception {
        String result = "";
        log.debug("start CycleTask.init...................");
        try {

            ISchedulerService schedulerService = (ISchedulerService) SSBBus.findDomainService("schedulerService");

            // Trigger trigger = null;
            // trigger = schedulerService.getTrigger(CheckConstant.CHECK_CYCLE_TRIGGER_NAME,
            // CheckConstant.CHECK_CYCLE_GROUP_NAME);
            int i = schedulerService.getTriggerState(ConfigUtil.get(CheckConstant.CHECK_CYCLE_TRIGGER_NAME), ConfigUtil
                    .get(CheckConstant.CHECK_CYCLE_GROUP_NAME));
            int enable = Integer.parseInt(ConfigUtil.get(CheckConstant.CECK_CYCLE_ENABLE));

            if (enable == 0) {
                deleteCycleTimer(schedulerService);
            } else {
                if (i == -1 || i == 3) {

                    deleteCycleTimer(schedulerService);
                    saveCycleTimer(schedulerService);
                }
            }
            // else{
            // deleteCycleTimer(schedulerService);
            // saveCycleTimer(schedulerService);
            // }
        } catch (Exception e) {
            log.error("error occur in CycleTask.init()");
            log.error(e);
        }
        log.debug("end of CycleTask.init..................");
        return result;
    }

    /**
     * 删除定时器
     *
     * @param schedulerService
     * @throws Exception
     */
    public void deleteCycleTimer(ISchedulerService schedulerService) throws Exception {
        log.debug("start CycleTask.deleteCycleTimer..........");
        try {
            String groupName = ConfigUtil.get(CheckConstant.CHECK_CYCLE_GROUP_NAME);
            String triggerName = ConfigUtil.get(CheckConstant.CHECK_CYCLE_TRIGGER_NAME);
            String jobName = ConfigUtil.get(CheckConstant.CECK_CYCLE_JOB_NAME);

            schedulerService.unscheduleTrigger(triggerName, groupName);
            schedulerService.deleteJob(jobName, groupName);

        } catch (Exception e) {
            log.error("error occur in CycleTask.deleteCycleTimer:");
            log.error(e);
            throw e;
        }
        System.out.println("success delete timer................");
        log.debug("end CycleTask.deleteCycleTimer..........");
    }

    /**
     * 增加定时器，改定时器写死了每个小时的头一分钟被激活
     *
     * @param schedulerService
     * @return
     * @throws Exception
     */
    public Date saveCycleTimer(ISchedulerService schedulerService) throws Exception {
        log.debug("start CycleTask.saveCycleTimer..........");
        Date firstTaskDate = null;
        // 增加定时任务使用
        String groupName = ConfigUtil.get(CheckConstant.CHECK_CYCLE_GROUP_NAME);
        String triggerName = ConfigUtil.get(CheckConstant.CHECK_CYCLE_TRIGGER_NAME);
        String jobName = ConfigUtil.get(CheckConstant.CECK_CYCLE_JOB_NAME);

        String expression = ConfigUtil.get(GlobalConstants.CYCLE_SEQUENCE);

        try {
            CronTrigger trigger = schedulerService.createCronTrigger(triggerName, groupName, expression);
            JobDetail job = schedulerService.createJob(CheckConstant.CHECK_JOB_BEANID, jobName, groupName, null);
            // 向JOB记录中存入参数
            JobDataMap map = new JobDataMap();

            firstTaskDate = schedulerService.addJob(job, trigger);
        } catch (Exception e) {
            log.error("error occur in CycleTask.saveCycleTimer:");
            log.error(e);
            throw e;
        }
        System.out.println("finish add timer------------------------");
        log.debug("end of CycleTask.saveCycleTimer........");
        return firstTaskDate;
    }
}

package com.zte.cms.content.service;

import java.util.List;

import com.zte.cms.content.dao.IIcmTagContentDAO;
import com.zte.cms.content.model.IcmTagContent;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class IcmTagContentDS extends DynamicObjectBaseDS implements IIcmTagContentDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmTagContentDAO dao = null;

    public void setDao(IIcmTagContentDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmTagContent(IcmTagContent icmTagContent) throws DomainServiceException
    {
        log.debug("insert icmTagContent starting...");
        try
        {
            Long tagcontentindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_tag_content");
            icmTagContent.setTagcontentindex(tagcontentindex);
            dao.insertIcmTagContent(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmTagContent end");
    }

    public void updateIcmTagContent(IcmTagContent icmTagContent) throws DomainServiceException
    {
        log.debug("update icmTagContent by pk starting...");
        try
        {
            dao.updateIcmTagContent(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTagContent by pk end");
    }

    public void updateIcmTagContentList(List<IcmTagContent> icmTagContentList) throws DomainServiceException
    {
        log.debug("update icmTagContentList by pk starting...");
        if (null == icmTagContentList || icmTagContentList.size() == 0)
        {
            log.debug("there is no datas in icmTagContentList");
            return;
        }
        try
        {
            for (IcmTagContent icmTagContent : icmTagContentList)
            {
                dao.updateIcmTagContent(icmTagContent);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update icmTagContentList by pk end");
    }

    public void removeIcmTagContent(IcmTagContent icmTagContent) throws DomainServiceException
    {
        log.debug("remove icmTagContent by pk starting...");
        try
        {
            dao.deleteIcmTagContent(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTagContent by pk end");
    }

    public void removeIcmTagContentList(List<IcmTagContent> icmTagContentList) throws DomainServiceException
    {
        log.debug("remove icmTagContentList by pk starting...");
        if (null == icmTagContentList || icmTagContentList.size() == 0)
        {
            log.debug("there is no datas in icmTagContentList");
            return;
        }
        try
        {
            for (IcmTagContent icmTagContent : icmTagContentList)
            {
                dao.deleteIcmTagContent(icmTagContent);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove icmTagContentList by pk end");
    }

    public IcmTagContent getIcmTagContent(IcmTagContent icmTagContent) throws DomainServiceException
    {
        log.debug("get icmTagContent by pk starting...");
        IcmTagContent rsObj = null;
        try
        {
            rsObj = dao.getIcmTagContent(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTagContentList by pk end");
        return rsObj;
    }

    public List<IcmTagContent> getIcmTagContentByCond(IcmTagContent icmTagContent) throws DomainServiceException
    {
        log.debug("get icmTagContent by condition starting...");
        List<IcmTagContent> rsList = null;
        try
        {
            rsList = dao.getIcmTagContentByCond(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmTagContent by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmTagContent icmTagContent, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get icmTagContent page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmTagContent, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmTagContent>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmTagContent page info by condition end");
        return tableInfo;
    }

    public void removeTagContentByTagindex(Long tagindex) throws DomainServiceException
    {
        log.debug("remove Tag Content By Tagindex starting...");
        try
        {
            dao.removeTagContentByTagindex(tagindex);
        }
        catch (DAOException daoEx)
        {
            log.error("remove Tag Content By Tagindex exception:" + daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove Tag Content By Tagindex end");
    }

    public void removeRelationByTagindexAndConIDs(Long tagindex, String contentid) throws DomainServiceException
    {
        log.debug("remove Relation By Tagindex And ConIDs starting...");
        try
        {
            IcmTagContent icmTagContent = new IcmTagContent();
            icmTagContent.setTagindex(tagindex);
            icmTagContent.setContentid(contentid);
            dao.removeRelationByTagindexAndConIDs(icmTagContent);
        }
        catch (DAOException daoEx)
        {
            log.debug("remove Relation By Tagindex And ConIDs exception:" + daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove Relation By Tagindex And ConIDs end");

    }

    public List<String> getConCpidListByTagIndex(Long tagindex) throws DomainServiceException
    {
        log.debug("getConCpidListByTagIndex starting...");
        List<String> cpidList = null;
        try
        {
            cpidList = dao.getConCpidListByTagIndex(tagindex);
        }
        catch (DAOException daoEx)
        {
            log.error("getConCpidListByTagIndex exception:" + daoEx);
            throw new DomainServiceException(daoEx);
        }
        log.debug("getConCpidListByTagIndex end");
        return cpidList;
    }

}

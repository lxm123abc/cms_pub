package com.zte.cms.cpContractManagement.model;


import com.zte.ssb.dynamicobj.DynamicBaseObject;


import java.util.Date;

public class ContractMap extends DynamicBaseObject {

    private Integer    id;                  //合同号唯一id
    private String cpCode;              //
    private String cpName;              //
    private String contractId;          //合同号
    private String contractNum;         //合同流水号
    private String contractName;        //合同名称
    private String contractPartyName;   //相对方名称
    private String enclosure;           //附件
    private Date   beginDate;           //开始时间
    private Date   endDate;             //结束时间
    private Date   createDate;          //导入时间


    private String   showBeginDate;           //开始时间
    private String   showEndDate;             //结束时间

    private String   attachid;             //附件id
    private String   attachname;             //附件名称
    private String   attachurl;             //附件路径

    private  String message;            //提示信息

    public String getAttachid() {
        return attachid;
    }

    public void setAttachid(String attachid) {
        this.attachid = attachid;
    }

    public String getAttachname() {
        return attachname;
    }

    public void setAttachname(String attachname) {
        this.attachname = attachname;
    }

    public String getAttachurl() {
        return attachurl;
    }

    public void setAttachurl(String attachurl) {
        this.attachurl = attachurl;
    }

    public String getCpCode() {
        return cpCode;
    }

    public void setCpCode(String cpCode) {
        this.cpCode = cpCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractNum() {
        return contractNum;
    }

    public void setContractNum(String contractNum) {
        this.contractNum = contractNum;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractPartyName() {
        return contractPartyName;
    }

    public void setContractPartyName(String contractPartyName) {
        this.contractPartyName = contractPartyName;
    }

    public String getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(String enclosure) {
        this.enclosure = enclosure;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public String getShowBeginDate() {
        return showBeginDate;
    }

    public void setShowBeginDate(String showBeginDate) {
        this.showBeginDate = showBeginDate;
    }

    public String getShowEndDate() {
        return showEndDate;
    }

    public void setShowEndDate(String showEndDate) {
        this.showEndDate = showEndDate;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public void initRelation() {
        this.addRelation("id", "ID");
        this.addRelation("cpCode", "CPCODE");
        this.addRelation("cpName", "CPNAME");
        this.addRelation("contractId", "CONTRACTID");
        this.addRelation("contractNum", "CONTRACTNUM");
        this.addRelation("contractName", "CONTRACTNAME");
        this.addRelation("contractPartyName", "CONTRACTPARTYNAME");
        this.addRelation("enclosure", "ENCLOSURE");
        this.addRelation("beginDate", "BEGINDATE");
        this.addRelation("endDate", "ENDDATE");
        this.addRelation("createDate", "CREATEDATE");
        this.addRelation("endDate", "ENDDATE");
        this.addRelation("createDate", "CREATEDATE");
    }
}

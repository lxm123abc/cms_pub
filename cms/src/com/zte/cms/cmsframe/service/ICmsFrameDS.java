package com.zte.cms.cmsframe.service;

import java.util.List;
import com.zte.cms.cmsframe.model.CmsFrame;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsFrameDS
{
    /**
     * 新增CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsFrame(CmsFrame cmsFrame) throws DomainServiceException;

    /**
     * 更新CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFrame(CmsFrame cmsFrame) throws DomainServiceException;

    /**
     * 批量更新CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsFrameList(List<CmsFrame> cmsFrameList) throws DomainServiceException;

    /**
     * 删除CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFrame(CmsFrame cmsFrame) throws DomainServiceException;

    /**
     * 批量删除CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsFrameList(List<CmsFrame> cmsFrameList) throws DomainServiceException;

    /**
     * 查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @return CmsFrame对象
     * @throws DomainServiceException ds异常
     */
    public CmsFrame getCmsFrame(CmsFrame cmsFrame) throws DomainServiceException;

    /**
     * 根据条件查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象
     * @return 满足条件的CmsFrame对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsFrame> getCmsFrameByCond(CmsFrame cmsFrame) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsFrame对象
     * 
     * @param cmsFrame CmsFrame对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsFrame cmsFrame, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
    
    public List<CmsFrame> getCmsFrameByName(CmsFrame cmsFrame) throws DomainServiceException;
    public List<CmsFrame> queryCmsFrameListCntByNameUpdate(CmsFrame cmsFrame) throws DomainServiceException;

}

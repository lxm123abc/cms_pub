package com.zte.cms.ctgsrmap.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.series.common.SeriesConfig;
import com.zte.cms.content.series.ls.ICmsSeriesLS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.cms.ctgsrmap.model.CategorySeriesMapConstants;
import com.zte.cms.ctgsrmap.service.ICategorySeriesMapDS;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 
 * @author Administrator
 * 
 */
public class CntSrMapLS extends
		com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements
		ICntSrMapLS {
	private static final String RESOURCE_ASSESSRULE_CANCELNOTFORCONDITION = "cmsprogramtype.canclepublish.notforCondition";
	// 节目分类信息取消发布不符合条件
	private static final String RESOURCE_ASSESSRULE_ISNOTEXIST = "cmsprogramtype.reason.isnotExist";
	// 节目分类信息不存在
	private static final String FAIL = "cmsprogramtype.fail";
	private static final String SUCCESS = "cmsprogramtype.success";
	private static final String CANCLE_PUBLISH_SUCCESS = "cmsprogramtype.canclepublish.success";
	private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
	private final static String CMSPROGRAMTYPE_CNTSYNCXML = "xmlsync";
	private ICategorySeriesMapDS ctgsrmapDS;
	private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL,
			getClass());
	private ICntSyncTaskDS cntSyncTaskDS;
	private ICmsSeriesDS cmsSeriesDS;

	private IUsysConfigDS usysConfigDS = null;

	private ICntSyncRecordDS cntSyncRecordDS = null;
	private DbUtil db = new DbUtil();
	private ITargetsystemDS targetSystemDS;
	private DbUtil dbUtil = new DbUtil();
	private ICategorycdnDS catDS;
	private    IObjectSyncRecordDS  objectSyncRecordDS;
	private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;

	public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }
    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }
    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

	public ICategorycdnDS getCatDS() {
		return catDS;
	}

	public void setCatDS(ICategorycdnDS catDS) {
		this.catDS = catDS;
	}

	
    /**
	 * 
	 * @return ITargetsystemDS
	 */
	public ITargetsystemDS getTargetSystemDS() {
		return targetSystemDS;
	}

	/**
	 * 
	 * @param targetSystemDS
	 *            ITargetsystemDS
	 */
	public void setTargetSystemDS(ITargetsystemDS targetSystemDS) {
		this.targetSystemDS = targetSystemDS;
	}

	/**
	 * 
	 * @return ICmsSeriesDS
	 */
	public ICmsSeriesDS getCmsSeriesDS() {
		return cmsSeriesDS;
	}

	/**
	 * 
	 * @param cmsSeriesDS
	 *            ICmsSeriesDS
	 */
	public void setCmsSeriesDS(ICmsSeriesDS cmsSeriesDS) {
		this.cmsSeriesDS = cmsSeriesDS;
	}

	/**
	 * 
	 * @return ICategorySeriesMapDS
	 */
	public ICategorySeriesMapDS getCtgsrmapDS() {
		return ctgsrmapDS;
	}

	/**
	 * 
	 * @param ctgsrmapDS
	 *            ICategorySeriesMapDS
	 */
	public void setCtgsrmapDS(ICategorySeriesMapDS ctgsrmapDS) {
		this.ctgsrmapDS = ctgsrmapDS;
	}

	/**
	 * 
	 * @return IUsysConfigDS
	 */
	public IUsysConfigDS getUsysConfigDS() {
		return usysConfigDS;
	}

	/**
	 * 
	 * @param usysConfigDS
	 *            IUsysConfigDS
	 */
	public void setUsysConfigDS(IUsysConfigDS usysConfigDS) {
		this.usysConfigDS = usysConfigDS;
	}

	/**
	 * 
	 * @return ICntSyncRecordDS
	 */
	public ICntSyncRecordDS getCntSyncRecordDS() {
		return cntSyncRecordDS;
	}

	/**
	 * 
	 * @param cntSyncRecordDS
	 *            ICntSyncRecordDS
	 */
	public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS) {
		this.cntSyncRecordDS = cntSyncRecordDS;
	}

	/**
	 * 
	 * @return ICntSyncTaskDS
	 */
	public ICntSyncTaskDS getCntSyncTaskDS() {
		return cntSyncTaskDS;
	}

	/**
	 * 
	 * @param cntSyncTaskDS
	 *            ICntSyncTaskDS
	 */
	public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS) {
		this.cntSyncTaskDS = cntSyncTaskDS;
	}

	/**
	 * 列表
	 */
	/**
	 * @param categorySeriesMap
	 *            CategorySeriesMap
	 * @param start
	 *            int
	 * @param pageSize
	 *            int
	 * @return TableDataInfo
	 */
	public TableDataInfo queryCntPgMapList(CategorySeriesMap categorySeriesMap,
			int start, int pageSize) {
		log.debug("queryCntPgMapList start...");
		TableDataInfo dataInfo = null;
		try {
			dataInfo = ctgsrmapDS.pageInfoQuery(categorySeriesMap, start,
					pageSize);

		} catch (DomainServiceException e) {
			log.error("queryCntPgMapList error");
		}
		log.debug("queryCntPgMapList end");
		return dataInfo;
	}

	/**
	 * 产品分类信息删除
	 */
	/**
	 * @param id
	 *            Long
	 * @return CategorySeriesMap
	 */
	public String deletectgpgmap(Long id) {
		log.debug("deleteProgramtype start...");
		CategorySeriesMap cpmap = new CategorySeriesMap();
		try 
		{   
		    
	        cpmap.setMapindex(id);
			cpmap = ctgsrmapDS.getCategorySeriesMap(cpmap);
    		if (cpmap != null) 
    		{
    			    
    		    CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectid(cpmap.getMappingid());
                cntTargetSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
                List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                boolean isPublished = false;
                if(null != cntTargetSyncList && cntTargetSyncList.size() > 0)
                {
                    
                    for (CntTargetSync targetSync:cntTargetSyncList)
                    {
                        if (StatusTranslator.STATUS_PUBLISHING == targetSync.getStatus() || StatusTranslator.STATUS_PUBLISHED == targetSync.getStatus()||StatusTranslator.STATUS_MODIFYPUBFAILED == targetSync.getStatus())
                        {
                            isPublished = true;
                            break;
                        }
                    }
                    
                    if (!isPublished)
                    {
                        for (int i = 0; i < cntTargetSyncList.size(); i++)
                        {
                            cntTargetSync = cntTargetSyncList.get(i);
                            cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
    
                            // 获取平台发布信息
                            CntPlatformSync cntPlatformSync = new CntPlatformSync();
                            cntPlatformSync.setObjectid(cntTargetSync.getObjectid());
                            cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                            cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                            cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    
                            // 查询子网元发布信息
                            CntTargetSync childTargetSync = new CntTargetSync();
                            childTargetSync.setObjectid(cntTargetSync.getObjectid());
                            childTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                            childTargetSync.setObjecttype(cntTargetSync.getObjecttype());
                            List<CntTargetSync> childTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(childTargetSync);
    
                            if (childTargetSyncList.size() < 1)
                            {
                                // 删除主平台关联发布信息
                                cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                                // 更新电视剧与其他对象的mapping是否有关联平台发布，没有就删除它们之间的mapping关系（暂不实现）
                                
                            }
                            else
                            {
                                // 更改主平台关联发布状态
                                int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                                for (int j = 0; j < childTargetSyncList.size(); j++)
                                {
                                    childTargetSyncStatus[j] = childTargetSyncList.get(j).getStatus();
                                }
                                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }
                }
                
                if (!isPublished)
                {
                    ctgsrmapDS.removeCategorySeriesMap(cpmap);
                    CommonLogUtil.insertOperatorLog(cpmap.getMapindex().toString(),
                            CategorySeriesMapConstants.MGTTYPE_CTGSRMAP,
                            CategorySeriesMapConstants.OPERTYPE_CTGSR_DELETE,
                            CategorySeriesMapConstants.OPERTYPE_CTGSR_DELETE_INFO,
                            CategorySeriesMapConstants.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    return "1:连续剧关联栏目已发布，不能删除";
                }
    		} 
    		else 
    		{
    			return "1:连续剧关联栏目已不存在";
    		}
		} catch (DomainServiceException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

		
		log.debug("deleteProgramtype end");
		return "0:操作成功";
	}

	/**
	 * 产品分类信息添加
	 */
	/**
	 * @param categorySeriesMap
	 *            CategorySeriesMap
	 * @return String
	 */
	public String insertCtgsrmap(CategorySeriesMap categorySeriesMap) {
		log.debug("insertCtgpgmap starting...");
		String msg = "";
		try {
			CmsSeries cmsSeries = new CmsSeries();
			Categorycdn cat = new Categorycdn();
			if (categorySeriesMap.getSeriesindex() != null) 
			{
				cmsSeries.setSeriesindex(categorySeriesMap.getSeriesindex());
				cmsSeries = cmsSeriesDS.getCmsSeries(cmsSeries);
				cat.setCategoryindex(categorySeriesMap.getCategoryindex());
				cat = catDS.getCategorycdn(cat);
				if (null == cat) 
				{
					msg = ResourceMgt.findDefaultText("category.not.exsit");//"1:此栏目已不存在"
					return msg;
				}else if (null == cmsSeries) 
				{
					msg = ResourceMgt.findDefaultText("series.not.exist");//"1:此连续剧已不存在"
					return msg;
				}
				
				categorySeriesMap.setSeriesid(cmsSeries.getSeriesid());
                categorySeriesMap.setStatus(0);
                ctgsrmapDS.insertCategorySeriesMap(categorySeriesMap);
                msg = ResourceMgt.findDefaultText("category.add.success");//"0:添加成功"
				
				CntTargetSync seriesTargetSync = new CntTargetSync();
				seriesTargetSync.setObjectid(cmsSeries.getSeriesid());
				seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
	            List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
	            
	            if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
	            {
	                CntTargetSync ctgTargetSync = new CntTargetSync();
	                ctgTargetSync.setObjectid(cat.getCategoryid());
	                ctgTargetSync.setObjecttype(ObjectType.CATEGORY_TYPE);
	                List<CntTargetSync> ctgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(ctgTargetSync);
	                if (null != ctgTargetSyncList && ctgTargetSyncList.size() > 0)
	                {
	                    for (CntTargetSync srTargetSync:seriesTargetSyncList)
	                    {
	                        for (CntTargetSync ctgTSync:ctgTargetSyncList)
	                        {
	                            if (srTargetSync.getTargetindex().intValue() == ctgTSync.getTargetindex().intValue())
	                            {
	                                ICmsSeriesLS cmsSeriesLS = (ICmsSeriesLS) SSBBus.findDomainService("cmsSeriesLS");
	                                Targetsystem target = new Targetsystem();
	                                target.setTargetindex(srTargetSync.getTargetindex());
	                                target.setStatus(0);//正常状态
	                                List<Targetsystem> targetList = targetSystemDS.getTargetsystemByCond(target);
	                                if (null != targetList && targetList.size() > 0)
	                                {
	                                    
	                                    CntPlatformSync cntPlatformSync = new CntPlatformSync();
	                                    cntPlatformSync.setObjectindex(categorySeriesMap.getMapindex());
	                                    cntPlatformSync.setObjectid(categorySeriesMap.getMappingid());
	                                    cntPlatformSync.setElementid(cmsSeries.getSeriesid());
	                                    cntPlatformSync.setParentid(cat.getCategoryid());
	                                    cntPlatformSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
	                                    String categorySeriesMapOperdesc = cmsSeriesLS.bind2Platform(targetList.get(0),cntPlatformSync);
	                                    if (categorySeriesMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
	                                    {
	                                        CommonLogUtil
	                                        .insertOperatorLog(
	                                                categorySeriesMap.getMappingid() + ", " + targetList.get(0).getTargetid(),
	                                                SeriesConfig.SERIES_MGT,
	                                                SeriesConfig.SERIESCATE_BINDTARGET,
	                                                SeriesConfig.SERIESCATE_BINDTARGET_INFO,
	                                                SeriesConfig.RESOURCE_OPERATION_SUCCESS);
	                                    }
	                                }
	                            }
	                        }
	                    }
	                }
	                
	            }
	            
	            CommonLogUtil.insertOperatorLog(categorySeriesMap.getMapindex()
	                    .toString(), CategorySeriesMapConstants.MGTTYPE_CTGSRMAP,
	                    CategorySeriesMapConstants.OPERTYPE_CTGSR_ADD,
	                    CategorySeriesMapConstants.OPERTYPE_CTGSR_ADD_INFO,
	                    CategorySeriesMapConstants.RESOURCE_OPERATION_SUCCESS);
			}
		} catch (DomainServiceException e) {
			log.error(e);
			return ResourceMgt.findDefaultText("category.add.fail");//"1:添加失败"
		}

		log.debug("insertCtgpgmap end");
		return msg;
	}

	/**
	 * 按照typeid查询，得到cmsprogramtype
	 * 
	 * @param mapid
	 *            String
	 * @return CategorySeriesMap
	 */
	public CategorySeriesMap getCmsCategory(String mapid) {
		log.debug("getCmsProgramtype start...");
		CategorySeriesMap map = new CategorySeriesMap();
		map.setMapindex(new Long(mapid));
		try {
			map = ctgsrmapDS.getCategorySeriesMap(map);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("getCmsProgramtype end");
		return map;
	}

	/**
	 * @param pgindex
	 *            String
	 * @return CmsSeries
	 */
	public CmsSeries getCmsSeries(String pgindex) {
		log.debug("getCmsProgramtype start...");
		CmsSeries pg = new CmsSeries();
		pg.setSeriesindex(new Long(pgindex));
		try {
			pg = cmsSeriesDS.getCmsSeries(pg);
		} catch (DomainServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("getCmsProgramtype end");
		return pg;
	}

	/**
	 * 
	 * @return Logger
	 */
	public Logger getLog() {
		return log;
	}

	/**
	 * 
	 * @param log
	 *            Logger
	 */
	public void setLog(Logger log) {
		this.log = log;
	}

	/**
	 * @param str
	 *            CategorySeriesMap
	 * @return String
	 * @throws Exception
	 *             Exception
	 */
	public String publishCategory(CategorySeriesMap str[]) throws Exception {
		log.debug("publishProgramtype start...");
		Targetsystem targetSystembms = new Targetsystem();
		targetSystembms.setTargettype(1);
		List<Targetsystem> listtsystembms = targetSystemDS
				.getTargetsystemByCond(targetSystembms);
		if (listtsystembms != null) {
			targetSystembms = listtsystembms.get(0);
		}
		Targetsystem targetSystemepg = new Targetsystem();
		targetSystemepg.setTargettype(2);
		List<Targetsystem> listtsystemepg = targetSystemDS
				.getTargetsystemByCond(targetSystemepg);
		if (listtsystemepg != null) {
			targetSystemepg = listtsystemepg.get(0);
		}
		ReturnInfo returnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		String flag = "";
		String errorcode = "";

		ResourceMgt
				.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
		Integer ifail = 0;
		Integer iSuccessed = 0;

		CategorySeriesMap cmsprogramtype = new CategorySeriesMap();
		String typeid = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		int index = 1;

		String tempAreaPath = getTempAreAdd();
		if (tempAreaPath == null || tempAreaPath.equals("1056020")) {
			operateInfo = new OperateInfo(String.valueOf(index++), "",
					ResourceMgt.findDefaultText(FAIL), ResourceMgt.findDefaultText("category.basic.get.temporary.failed"));//"获取临时区失败"
			returnInfo.appendOperateInfo(operateInfo);

			flag = GlobalConstants.FAIL; // "1"
			errorcode = ResourceMgt.findDefaultText("fail.count") + 1;
			returnInfo.setFlag(flag);
			returnInfo.setReturnMessage(errorcode);
			return returnInfo.toString();
		}
		String synXMLFTPAdd = getSynXMLFTPAddr();
		if (synXMLFTPAdd == null) {
			return ResourceMgt.findDefaultText("basicls.get.ftpforxml.failed");//"31:获取同步XML文件FTP地址失败"
		}

		try {
			List<CategorySeriesMap> listCmsProgramtype = new ArrayList<CategorySeriesMap>();
			for (int i = 0; i < str.length; i++) {

				cmsprogramtype = ctgsrmapDS.getCategorySeriesMap(str[i]);
				if (cmsprogramtype == null) {
					ifail++;
					operateInfo = new OperateInfo(
							String.valueOf(index++),
							str[i].getCategoryid(),
							ResourceMgt.findDefaultText(FAIL),
							ResourceMgt
									.findDefaultText(RESOURCE_ASSESSRULE_ISNOTEXIST));
					returnInfo.appendOperateInfo(operateInfo);
					continue;
				} else {
					if (cmsprogramtype.getStatus() != 0// 待发布
							&& cmsprogramtype.getStatus() != 30// 新增同步失败
							&& cmsprogramtype.getStatus() != 80// 取消同步成功
							&& cmsprogramtype.getStatus() != 90) {// 取消同步失败
						ifail++;
						operateInfo = new OperateInfo(String.valueOf(index++),
								cmsprogramtype.getCategoryid(), ResourceMgt
										.findDefaultText(FAIL), ResourceMgt.findDefaultText("basicls.cnt.cannot.add.sync.forstatus"));//"当前状态不能新增同步"
						returnInfo.appendOperateInfo(operateInfo);
						continue;
					} else {
						iSuccessed++;
						listCmsProgramtype.add(cmsprogramtype);
						operateInfo = new OperateInfo(String.valueOf(index++),
								cmsprogramtype.getCategoryid(), ResourceMgt
										.findDefaultText(SUCCESS), ResourceMgt
										.findDefaultText(PUBLISH_SUCCESS));
						returnInfo.appendOperateInfo(operateInfo);

					}
				}
			}

			if (listCmsProgramtype.size() > 0) {
				// 生成节目单xml,返回生成文件全路径。

				long taskid = -1;
				for (CategorySeriesMap cmsProgramtype : listCmsProgramtype) {
					String fileName = createProgramtypeXml(cmsProgramtype,
							tempAreaPath, synXMLFTPAdd, "publishCategory");
					String batchid = this.getPrimaryKeyGenerator()
							.getPrimarykey("ucdn_task_batch_id").toString();
					CommonLogUtil
							.insertOperatorLog(
									cmsProgramtype.getMapindex().toString(),
									CategorySeriesMapConstants.MGTTYPE_CTGSRMAP,
									CategorySeriesMapConstants.OPERTYPE_CTGSR_PUBLISH,
									CategorySeriesMapConstants.OPERTYPE_CTGSR_PUBLISH_INFO,
									CategorySeriesMapConstants.RESOURCE_OPERATION_SUCCESS);

					if (cmsProgramtype.getStatus() == 90
							|| cmsProgramtype.getStatus() == 30)
					// 取消同步失败,发布时判断如果BMS、EPG都存在分发记录，则直接改状态为“新增同步成功”，不需要生成同步任务；否则下个状态为“新增同步中”
					{
						CntSyncRecord cntSyncRecord = new CntSyncRecord();
						cntSyncRecord.setDesttype(1); // 目标系统类型：1 BMS，2 EPG，3
						cntSyncRecord.setObjindex(cmsProgramtype.getMapindex()); // CDN
						cntSyncRecord.setObjtype(27);

						CntSyncRecord cntSyncRecorde = new CntSyncRecord();
						cntSyncRecorde.setDesttype(2); // 目标系统类型：1 BMS，2 EPG，3
						cntSyncRecorde.setObjtype(27); // CDN
						cntSyncRecorde
								.setObjindex(cmsProgramtype.getMapindex());

						boolean flagb = false;
						List listbms = cntSyncRecordDS
								.getCntSyncRecordByCond(cntSyncRecord);
						if (listbms != null && listbms.size() > 0) {
							// cntSyncRecord = (CntSyncRecord)listbms.get(0);
							// cntSyncRecord = (CntSyncRecord)listbms.get(0);
							flagb = false;
						} else {
							String correlateid = this.getPrimaryKeyGenerator()
									.getPrimarykey("ucdn_task_correlate_id")
									.toString();
							CntSyncTask cntsyncTask = new CntSyncTask();

//							cntsyncTask.setDesttype(1); // 目标系统类型：1 BMS，2 EPG，3
							// CDN
//							cntsyncTask.setObjindex(cmsProgramtype
//									.getMapindex());
							cntsyncTask.setStatus(1);
							cntsyncTask.setPriority(2);
//							cntsyncTask.setBatchid(batchid);
							cntsyncTask.setCorrelateid(correlateid);
							cntsyncTask.setContentmngxmlurl(fileName);
//							cntsyncTask.setObjtype(27);
//							cntsyncTask.setBatchsingleflag(1);
//							cntsyncTask.setTasksingleflag(1);
							cntsyncTask.setDestindex(targetSystembms
									.getTargetindex());
//							cntsyncTask.setTasktype(1); // 同步类型：1 REGIST，2
							// UPDATE，3 DELETE
							cntSyncTaskDS.insertCntSyncTask(cntsyncTask);
							flagb = true;
						}
						boolean flagr = false;
						List listepg = cntSyncRecordDS
								.getCntSyncRecordByCond(cntSyncRecorde);
						if (listepg != null && listepg.size() > 0) {
							flagr = false;
						} else {

							String correlateid = this.getPrimaryKeyGenerator()
									.getPrimarykey("ucdn_task_correlate_id")
									.toString();
							CntSyncTask cntsyncTaskEPG = new CntSyncTask();
							cntsyncTaskEPG.setCorrelateid(cmsProgramtype
									.getCategoryid()); // 同步信息类型
//							cntsyncTaskEPG.setDesttype(2); // 目标系统类型：1 BMS，2
							// EPG，3 CDN
//							cntsyncTaskEPG.setObjindex(cmsProgramtype
//									.getMapindex());
							cntsyncTaskEPG.setStatus(1);
							cntsyncTaskEPG.setPriority(2);
//							cntsyncTaskEPG.setBatchid(batchid);
							cntsyncTaskEPG.setCorrelateid(correlateid);
							cntsyncTaskEPG.setContentmngxmlurl(fileName);
//							cntsyncTaskEPG.setObjtype(27);
//							cntsyncTaskEPG.setBatchsingleflag(1);
//							cntsyncTaskEPG.setTasksingleflag(1);
							cntsyncTaskEPG.setDestindex(targetSystemepg
									.getTargetindex());
//							cntsyncTaskEPG.setTasktype(1); // 同步类型：1 REGIST，2
							// UPDATE，3 DELETE

							cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

							flagr = true;
						}
						if ((flagr == false) && (flagb == false)) {// 如果2个都有记录，新增成功
							cmsProgramtype.setStatus(20); // 新增同步成功
						} else {
							cmsProgramtype.setStatus(10); // 新增同步中
						}
					} else {

						String correlateid = this.getPrimaryKeyGenerator()
								.getPrimarykey("ucdn_task_correlate_id")
								.toString();

						CntSyncTask cntsyncTask = new CntSyncTask();
						// 同步信息类型
//						cntsyncTask.setDesttype(1); // 目标系统类型：1 BMS，2 EPG，3 CDN
//						cntsyncTask.setObjindex(cmsProgramtype.getMapindex());
						cntsyncTask.setStatus(1);
						cntsyncTask.setPriority(2);
//						cntsyncTask.setBatchid(batchid);
						cntsyncTask.setCorrelateid(correlateid);
						cntsyncTask.setContentmngxmlurl(fileName);
//						cntsyncTask.setObjtype(27);
//						cntsyncTask.setBatchsingleflag(1);
//						cntsyncTask.setTasksingleflag(1);
						cntsyncTask.setDestindex(targetSystembms
								.getTargetindex());
//						cntsyncTask.setTasktype(1); // 同步类型：1 REGIST，2 UPDATE，3
						// DELETE
						cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

						correlateid = this.getPrimaryKeyGenerator()
								.getPrimarykey("ucdn_task_correlate_id")
								.toString();

						CntSyncTask cntsyncTaskEPG = new CntSyncTask();
						cntsyncTaskEPG.setCorrelateid(cmsProgramtype
								.getCategoryid()); // 同步信息类型
//						cntsyncTaskEPG.setDesttype(2); // 目标系统类型：1 BMS，2 EPG，3
						// CDN
//						cntsyncTaskEPG
//								.setObjindex(cmsProgramtype.getMapindex());
						cntsyncTaskEPG.setStatus(1);
						cntsyncTaskEPG.setPriority(2);
//						cntsyncTaskEPG.setBatchid(batchid);
						cntsyncTaskEPG.setCorrelateid(correlateid);
						cntsyncTaskEPG.setContentmngxmlurl(fileName);
//						cntsyncTaskEPG.setObjtype(27);
//						cntsyncTaskEPG.setBatchsingleflag(1);
//						cntsyncTaskEPG.setTasksingleflag(1);
						cntsyncTaskEPG.setDestindex(targetSystemepg
								.getTargetindex());
//						cntsyncTaskEPG.setTasktype(1); // 同步类型：1 REGIST，2
						// UPDATE，3 DELETE
						cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

						// 分发记录表

						cmsProgramtype.setStatus(10); // 新增同步中
					}

					cmsProgramtype
							.setPublishtime(dateFormat.format(new Date()));
					ctgsrmapDS.updateCategorySeriesMap(cmsProgramtype);
				}

			}

		} catch (Exception e) {
			log.error(e);
			throw e;
		}

		if (str.length == 0 || ifail == str.length) {
			flag = GlobalConstants.FAIL; // "1"
			errorcode = ResourceMgt.findDefaultText("fail.count") + ifail;
		} else if (ifail > 0 && ifail < str.length) {
			flag = "2";
			errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail; // "350098"
		} else {
			flag = GlobalConstants.SUCCESS; // "0"
			errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed;
		}
		returnInfo.setFlag(flag);
		returnInfo.setReturnMessage(errorcode);
		log.debug("publishColumn end...");
		return returnInfo.toString();
	}

	// 生成节目单xml,返回生成文件全路径。
	private String createProgramtypeXml(CategorySeriesMap categorycdn,
			String tempAreaPath, String synXMLFTPAdd, String type)
			throws Exception
	// tempAreaPath
	// /NAS/临时区

	{
		log.debug("createCmsprogramtypeXml starting...");
		Date date = new Date();
		String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint(); // CMS
		String fileName = date.getTime() + "_categorysrmap.xml"; // 当前时间精确到毫秒作为文件名的一部分加上后缀"_schedule"

		int year = 1900 + date.getYear();
		int month = date.getMonth() + 1;

		int day = date.getDate();

		StringBuffer datecata = new StringBuffer();
		if (month < 10 && day < 10) {
			datecata.append(year).append("0").append(month).append("0").append(
					day);
		} else if (day < 10) {
			datecata.append(year).append(month).append("0").append(day);
		} else if (month < 10) {
			datecata.append(year).append("0").append(month).append(day);
		} else {
			datecata.append(year).append(month).append(day);
		}
		String tempFilePath = tempAreaPath + File.separator
				+ CMSPROGRAMTYPE_CNTSYNCXML + File.separator + datecata
				+ File.separator + "catogorySeriesmap" + File.separator; // 日期
		String mountAndTempfilePath = mountPoint + tempFilePath;
		// String retPath = synXMLFTPAdd + File.separator + tempFilePath
		// + fileName;//synXMLFTPAdd:ftp路径
		String retPath = tempFilePath + fileName; // synXMLFTPAdd:ftp路径
		File file = new File(mountAndTempfilePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String fileFullName = mountAndTempfilePath + fileName;
		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Mappings");
		Element objectElement = null;
		Element propertyElement = null;

		// 元素节点

		objectElement = objectsElement.addElement("Mapping");

		if (type != null && type.equals("publishCategory")) {
			objectElement.addAttribute("Action", "REGIST or UPDATE");
		} else {
			objectElement.addAttribute("Action", "DELETE");
		}
		objectElement.addAttribute("ParentType", "Category");
		objectElement.addAttribute("ElementType", "Series");
		objectElement.addAttribute("ParentID", categorycdn.getCategoryid());
		objectElement.addAttribute("ElementID", categorycdn.getSeriesid());

		Long mapidx = categorycdn.getMapindex();
		String pattern = "000000000000000000000000000000";
		DecimalFormat format = new DecimalFormat(pattern);
		String patterntp = "00";
		DecimalFormat formattp = new DecimalFormat(patterntp);
		String mapindex = format.format(mapidx);
		String typeindex = formattp.format(27);
		String objectid = typeindex + mapindex;
		System.out.println("objectid      " + objectid);
		objectElement.addAttribute("ObjectID", objectid);

		// 属性节点
		propertyElement = objectElement.addElement("Property");
		propertyElement.addAttribute("Name", "Sequence");
		propertyElement.addText(categorycdn.getSequence().toString());

		XMLWriter writer = null;
		try {
			writer = new XMLWriter(new FileOutputStream(fileFullName));
			writer.write(dom);
		} catch (Exception e) {
			log.error("createScheduleXml exception:" + e);
			throw e;
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (Exception e) {
					log.error("createScheduleXml exception:" + e);
					throw e;
				}
			}
		}

		log.debug("createScheduleXml end");
		return filterSlashStr(retPath);
	}

	private String getTempAreAdd() throws Exception {
		ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus
				.findDomainService("cmsStorageareaLS");
		String stroageareaPath = storageareaLs.getAddress("1"); // 临时区
		return stroageareaPath;
	}

	// 取出同步XML文件FTP地址
	private String getSynXMLFTPAddr() {
		log.debug("getSynXMLFTPAdd starting...");
		UsysConfig usysConfig = null;
		String synXMLFTPAddress = "";
		try {
			usysConfig = usysConfigDS
					.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
			if (null == usysConfig && null == usysConfig.getCfgvalue()
					&& (usysConfig.getCfgvalue().endsWith(""))) {
				return null;
			} else {
				synXMLFTPAddress = usysConfig.getCfgvalue();
			}
		} catch (DomainServiceException e) {
			log.error("getSynXMLFTPAdd exception:" + e);
			return null;

		}
		log.debug("getSynXMLFTPAdd end");
		return synXMLFTPAddress;
	}

	/**
	 * 节目分类信息删除同步
	 * 
	 * @param str
	 *            CategorySeriesMap
	 * @return String
	 * @throws Exception
	 *             Exception
	 */
	public String canclePuhlishProgramtype(CategorySeriesMap str[])
			throws Exception {
		log.debug("canclePuhlishProgramtype start...");
		ResourceMgt
				.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

		Targetsystem targetSystembms = new Targetsystem();
		targetSystembms.setTargettype(1);
		List<Targetsystem> listtsystembms = targetSystemDS
				.getTargetsystemByCond(targetSystembms);
		if (listtsystembms != null) {
			targetSystembms = listtsystembms.get(0);
		}
		Targetsystem targetSystemepg = new Targetsystem();
		targetSystemepg.setTargettype(2);
		List<Targetsystem> listtsystemepg = targetSystemDS
				.getTargetsystemByCond(targetSystemepg);
		if (listtsystemepg != null) {
			targetSystemepg = listtsystemepg.get(0);
		}

		ReturnInfo returnInfo = new ReturnInfo();
		OperateInfo operateInfo = null;
		String flag = "";
		String errorcode = "";

		Integer ifail = 0; // 记录失败的条数
		Integer iSuccessed = 0; // 记录成功的条数
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		CategorySeriesMap cmsProgramtype = null;
		int index = 1;

		String tempAreaPath = getTempAreAdd();
		if (tempAreaPath == null || tempAreaPath.equals("1056020")) {
			return ResourceMgt.findDefaultText("basic.get.temporary.failed");//"31:获取临时区失败"
		}
		String synXMLFTPAdd = getSynXMLFTPAddr();
		if (synXMLFTPAdd == null) {
			return ResourceMgt.findDefaultText("basicls.get.ftpforxml.failed");//"31:获取同步XML文件FTP地址失败"
		}

		List<CategorySeriesMap> listCmsProgramtype = new ArrayList<CategorySeriesMap>();
		try {
			for (int i = 0; i < str.length; i++) {
				cmsProgramtype = ctgsrmapDS.getCategorySeriesMap(str[i]);
				if (cmsProgramtype == null) {
					ifail++;
					operateInfo = new OperateInfo(
							String.valueOf(index++),
							str[i].getCategoryid(),
							ResourceMgt.findDefaultText(FAIL),
							ResourceMgt
									.findDefaultText(RESOURCE_ASSESSRULE_ISNOTEXIST));
					returnInfo.appendOperateInfo(operateInfo);
					continue;
				} else {
					int state = cmsProgramtype.getStatus();
					if (state == 20 || state == 30 || state == 50
							|| state == 60 || state == 90) { // 状态为删除同步失败或者已同步的可以取消发布

						iSuccessed++;
						listCmsProgramtype.add(cmsProgramtype);
						operateInfo = new OperateInfo(
								String.valueOf(index++),
								str[i].getCategoryid(),
								ResourceMgt.findDefaultText(SUCCESS),
								ResourceMgt
										.findDefaultText(CANCLE_PUBLISH_SUCCESS));
						returnInfo.appendOperateInfo(operateInfo);

					} else {
						ifail++;
						operateInfo = new OperateInfo(String.valueOf(index++),
								cmsProgramtype.getMapindex().toString(),
								ResourceMgt.findDefaultText(FAIL), ResourceMgt.findDefaultText("basicls.this.status.cannot.delete.sync"));//"当前状态不能删除同步"
						returnInfo.appendOperateInfo(operateInfo);
						continue;
					}
				}
			}
			if (listCmsProgramtype.size() > 0) {
				// 生成节目单xml,返回生成文件全路径。

				for (CategorySeriesMap categorycdn : listCmsProgramtype) {
					String fileName = createProgramtypeXml(categorycdn,
							tempAreaPath, synXMLFTPAdd, "cancleCategory");
					String batchid = this.getPrimaryKeyGenerator()
							.getPrimarykey("ucdn_task_batch_id").toString();
					if (categorycdn.getStatus() == 30
							|| categorycdn.getStatus() == 90) {// 新增取消失败
						CntSyncRecord cntSyncRecord = new CntSyncRecord();
						cntSyncRecord.setDesttype(1); // 目标系统类型：1 BMS，2 EPG，3
						// CDN
						cntSyncRecord.setObjindex(categorycdn.getMapindex());
						cntSyncRecord.setObjtype(27);

						CntSyncRecord cntSyncRecorde = new CntSyncRecord();
						cntSyncRecorde.setDesttype(2); // 目标系统类型：1 BMS，2 EPG，3
						cntSyncRecorde.setObjtype(27); // CDN
						cntSyncRecorde.setObjindex(categorycdn.getMapindex());

						boolean flagb = false;
						List listbms = new ArrayList();
						listbms = cntSyncRecordDS
								.getCntSyncRecordByCond(cntSyncRecord);
						if (listbms != null && listbms.size() > 0) {//
							String correlateid = this.getPrimaryKeyGenerator()
									.getPrimarykey("ucdn_task_correlate_id")
									.toString();

							CntSyncTask cntsyncTask = new CntSyncTask();
//							cntsyncTask.setBatchid(batchid);
							cntsyncTask.setCorrelateid(correlateid); // 同步信息类型
//							cntsyncTask.setDesttype(1); // 目标系统类型：1 BMS，2 EPG，3
							// CDN
//							cntsyncTask.setObjindex(categorycdn.getMapindex());
							cntsyncTask.setStatus(1);
							cntsyncTask.setPriority(2);
							cntsyncTask.setContentmngxmlurl(fileName);
//							cntsyncTask.setObjtype(27);
//							cntsyncTask.setBatchsingleflag(1);
//							cntsyncTask.setTasksingleflag(1);
							cntsyncTask.setDestindex(targetSystembms
									.getTargetindex());
//							cntsyncTask.setTasktype(3); // 同步类型：1 REGIST，2
							// UPDATE，3 DELETE

							cntSyncTaskDS.insertCntSyncTask(cntsyncTask);
							flagb = false;
						} else {

							flagb = true;
						}
						boolean flagr = false;
						List listepg = new ArrayList();
						listepg = cntSyncRecordDS
								.getCntSyncRecordByCond(cntSyncRecorde);
						if (listepg != null && listepg.size() > 0) {
							String correlateid = this.getPrimaryKeyGenerator()
									.getPrimarykey("ucdn_task_correlate_id")
									.toString();
							CntSyncTask cntsyncTaskEPG = new CntSyncTask();
//							cntsyncTaskEPG.setBatchid(batchid);
							cntsyncTaskEPG.setCorrelateid(correlateid); // 同步信息类型
//							cntsyncTaskEPG.setDesttype(2); // 目标系统类型：1 BMS，2
							// EPG，3 CDN
//							cntsyncTaskEPG.setObjindex(categorycdn
//									.getMapindex());
							cntsyncTaskEPG.setStatus(1);
							cntsyncTaskEPG.setPriority(2);
							cntsyncTaskEPG.setContentmngxmlurl(fileName);
//							cntsyncTaskEPG.setObjtype(27);
//							cntsyncTaskEPG.setBatchsingleflag(1);
//							cntsyncTaskEPG.setTasksingleflag(1);
							cntsyncTaskEPG.setDestindex(targetSystemepg
									.getTargetindex());
//							cntsyncTaskEPG.setTasktype(3);
							cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);
							flagr = false;
						} else {

							flagr = true;
						}
						if ((flagr == true) && (flagb == true)) {// 如果2个都没有记录，取消成功，新增失败
							categorycdn.setStatus(80); // 取消同步成功
						} else {
							categorycdn.setStatus(70); // 取消同步中
						}
					} else {

						categorycdn.setStatus(70);

						CommonLogUtil
								.insertOperatorLog(
										cmsProgramtype.getMapindex().toString(),
										CategorySeriesMapConstants.MGTTYPE_CTGSRMAP,
										CategorySeriesMapConstants.OPERTYPE_CTGSR_UNPUBLISH,
										CategorySeriesMapConstants.OPERTYPE_CTGSR_UNPUBLISH_INFO,
										CategorySeriesMapConstants.RESOURCE_OPERATION_SUCCESS);

						String correlateid = this.getPrimaryKeyGenerator()
								.getPrimarykey("ucdn_task_correlate_id")
								.toString();
						CntSyncTask cntsyncTask = new CntSyncTask(); // 实例化一个同步信息用于插入列表
//						cntsyncTask.setBatchid(batchid);
						cntsyncTask.setCorrelateid(correlateid);
						cntsyncTask.setStatus(1);
						cntsyncTask.setPriority(2);
//						cntsyncTask.setDesttype(1);
//						cntsyncTask.setTasktype(3);
						cntsyncTask.setContentmngxmlurl(fileName);
//						cntsyncTask.setObjtype(27);
//						cntsyncTask.setBatchsingleflag(1);
//						cntsyncTask.setTasksingleflag(1);
						cntsyncTask.setDestindex(targetSystembms
								.getTargetindex());
//						cntsyncTask.setObjindex(categorycdn.getMapindex());
						cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

						String correlateide = this.getPrimaryKeyGenerator()
								.getPrimarykey("ucdn_task_correlate_id")
								.toString();

						CntSyncTask cntsyncTaske = new CntSyncTask(); // 实例化一个同步信息用于插入列表
//						cntsyncTaske.setBatchid(batchid);
						cntsyncTaske.setCorrelateid(correlateide);
						cntsyncTaske.setStatus(1);
						cntsyncTaske.setPriority(2);
//						cntsyncTaske.setDesttype(2);
//						cntsyncTaske.setTasktype(3);
						cntsyncTaske.setContentmngxmlurl(fileName);
//						cntsyncTaske.setObjtype(27);
//						cntsyncTaske.setBatchsingleflag(1);
//						cntsyncTaske.setTasksingleflag(1);
						cntsyncTaske.setDestindex(targetSystemepg
								.getTargetindex());
//						cntsyncTaske.setObjindex(categorycdn.getMapindex());
						cntSyncTaskDS.insertCntSyncTask(cntsyncTaske);
					}
					categorycdn.setPublishtime(dateFormat.format(new Date()));
					ctgsrmapDS.updateCategorySeriesMap(categorycdn);
				}
			}
		} catch (Exception e) {
			log.error(e);
			ifail++;
			operateInfo = new OperateInfo(
					String.valueOf(index++),
					cmsProgramtype.getCategoryid(),
					ResourceMgt.findDefaultText(FAIL),
					ResourceMgt
							.findDefaultText(RESOURCE_ASSESSRULE_CANCELNOTFORCONDITION));

			returnInfo.appendOperateInfo(operateInfo);
		}

		if (str.length == 0 || ifail == str.length) {
			flag = GlobalConstants.FAIL; // "1"
			errorcode = ResourceMgt.findDefaultText("fail.count") + ifail; // "350099"
		} else if (ifail > 0 && ifail < str.length) {
			flag = "2";
			errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed +ResourceMgt.findDefaultText("blank.fail.count")+ ifail + ""; // "350098"
		} else {
			flag = GlobalConstants.SUCCESS; // "0"
			errorcode = ResourceMgt.findDefaultText("success.count") + iSuccessed;
		}
		log.debug("batchSyncOnline start....");
		returnInfo.setFlag(flag);
		returnInfo.setReturnMessage(errorcode);

		log.debug("canclePuhlishProgramtype end...");
		return returnInfo.toString();
	}

	// 将字符中的"\"转换成"/"
	private static String filterSlashStr(String str) {
		String rtnStr = "";
		if (!str.trim().equals("")) {
			if (str.indexOf("\\", 0) != -1) {
				while (str.length() > 0) {
					if (str.indexOf("\\", 0) > -1) {
						rtnStr += str.subSequence(0, str.indexOf("\\", 0));
						rtnStr += "/";
						str = str.substring(str.indexOf("\\", 0) + 1, str
								.length());
					} else {
						rtnStr += str;
						str = "";
					}
				}
			} else {

				rtnStr = str;
			}
		}
		return rtnStr;
	}

	// 收录内容的收录源的获取
	/**
	 * @param seriesindex
	 *            String
	 * @return Map
	 */
	public Map<String, String> getpgname(String seriesindex) {
		Map<String, String> rsMap = new HashMap<String, String>();

		try {

			String sql = "";

			sql = "select seriesindex ,namecn from cms_series ";

			if (!seriesindex.equals("")) {
				sql = sql + " where seriesindex=" + seriesindex;
			}
			List rtnlist = dbUtil.getQuery(sql);
			Map tmpMap = new HashMap();
			for (int i = 0; i < rtnlist.size(); i++) {
				tmpMap = (Map) rtnlist.get(i);
				rsMap.put((String) tmpMap.get("seriesindex"), (String) tmpMap
						.get("namecn"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsMap;
	}

	/**
	 * @param ctgindex
	 *            String
	 * @return Map
	 */
	public Map<String, String> getctgname(String ctgindex) {
		Map<String, String> rsMap = new HashMap<String, String>();

		try {

			String sql = "";

			sql = "select ctg.categoryindex ,ctg.categoryname from categorycdn ctg, category_series_map m ";

			if (!ctgindex.equals("")) {
				sql = sql
						+ "  where ctg.categoryindex=m.categoryindex and m.seriesindex="
						+ ctgindex;
			}
			List rtnlist = dbUtil.getQuery(sql);
			Map tmpMap = new HashMap();
			for (int i = 0; i < rtnlist.size(); i++) {
				tmpMap = (Map) rtnlist.get(i);
				rsMap.put((String) tmpMap.get("categoryindex"), (String) tmpMap
						.get("categoryname"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsMap;
	}

    public String publishCategorySeriesTargetMap(Long syncindex) throws Exception
    {
        log.debug("publish category series mapping start...");  
        String bingObject = "";
        CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
         mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
             return "1:没有可发布的mapping关系";
         }
       int type = mapping.getObjecttype();
       if(type == 27)//表示服务与点播内容的绑定关系
       {
           bingObject = "栏目";
           CategorySeriesMap csmap = new CategorySeriesMap();
           csmap.setMapindex(mapping.getObjectindex());  
           csmap = ctgsrmapDS.getCategorySeriesMap(csmap);        
           if(csmap == null){
               return "1:栏目与连续剧关联关系不存在 ";
           }
          Categorycdn category = new  Categorycdn();
          category.setCategoryid(mapping.getParentid());             
          category = catDS.getCategoryByCategoryId(category);
          if(category == null){
              return "1:栏目不存在 ";   
          }        
           mapping.setReserve02(category.getCatagorycode());     
           mapping.setReserve01(csmap.getSequence().longValue());          
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getElementid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series == null){
           return "1:连续剧不存在 ";
       }
       String elementcode =series.getCpcontentid();  //添加子节点的文广code
        mapping.setCancelpubtime(series.getCpcontentid());     
      
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:栏目与连续剧的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:栏目与连续剧的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
       
       CntTargetSync parent = new CntTargetSync();
       parent.setObjectid(mapping.getParentid());
       parent.setTargetindex(mapping.getTargetindex());
       parent =cntTargetSyncDS.getCntTargetSync(parent);
       if(parent == null){
           return "1:栏目的发布数据不存在 ";
       }
       if(parent.getStatus()!=300 && parent.getStatus()!=500){
           return "1:栏目在网元"+targetid+"上的发布状态不正确 ";
       }
       CntTargetSync element = new CntTargetSync();
       element.setObjectid(mapping.getElementid());
       element.setTargetindex(mapping.getTargetindex());
       element = cntTargetSyncDS.getCntTargetSync(element);
       if(element == null){
           return "1:连续剧的发布数据不存在 ";
       }
       if(element.getStatus()!=300 && parent.getStatus()!=500){
        
               return "1:连续剧在网元"+targetid+"上的发布状态不正确 ";
       }
       
       CntPlatformSync  platformSync = new CntPlatformSync();
       platformSync.setSyncindex(mapping.getRelateindex());
       platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
       int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 0 
               || targetResult == 30 
               || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
       }else {                   
               return  "1:栏目和连续剧关联关系在网元"+targetid+"上的发布状态不正确 "; 
       }
       if(xmlPath == null){
           return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String parentcode = mapping.getReserve02();//获得栏目文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       modTargetSynStatus( mapping, 1);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.CATEGORY_SERIES_MAPPING_PUBLISH,
       SeriesConfig.CATEGORY_SERIES_MAPPING_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("publish  category series mapping end...");
       String returnInfo ="0:操作成功 ";
       return returnInfo;    
    }
    private String publishCategorySeriesTarget(Long syncindex,Long batchid,int firTargetType) throws Exception
    {
        log.debug("publish category series mapping start...");  
        String bingObject = "";
        CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
         mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
             return "1:没有可发布的mapping关系";
         }
       int type = mapping.getObjecttype();
       if(type == 27)//表示服务与点播内容的绑定关系
       {
           bingObject = "栏目";
           CategorySeriesMap csmap = new CategorySeriesMap();
           csmap.setMapindex(mapping.getObjectindex());  
           csmap = ctgsrmapDS.getCategorySeriesMap(csmap);        
           if(csmap == null){
               return "1:栏目与连续剧关联关系不存在 ";
           }
          Categorycdn category = new  Categorycdn();
          category.setCategoryid(mapping.getParentid());             
          category = catDS.getCategoryByCategoryId(category);
          if(category == null){
              return "1:栏目不存在 ";   
          }        
           mapping.setReserve02(category.getCatagorycode());     
           mapping.setReserve01(csmap.getSequence().longValue());          
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getElementid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series == null){
           return "1:连续剧不存在 ";
       }
       String elementcode =series.getCpcontentid();  //添加子节点的文广code
        mapping.setCancelpubtime(series.getCpcontentid());    
     
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:栏目与连续剧的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:栏目与连续剧的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
       
       CntTargetSync parent = new CntTargetSync();
       parent.setObjectid(mapping.getParentid());
       parent.setTargetindex(mapping.getTargetindex());
       parent =cntTargetSyncDS.getCntTargetSync(parent);
       if(parent == null){
           return "1:栏目的发布数据不存在 ";
       }
       if(parent.getStatus()!=300 && parent.getStatus()!=500){
           return "1:栏目在网元"+targetid+"上的发布状态不正确 ";
       }
       CntTargetSync element = new CntTargetSync();
       element.setObjectid(mapping.getElementid());
       element.setTargetindex(mapping.getTargetindex());
       element = cntTargetSyncDS.getCntTargetSync(element);
       if(element == null){
           return "1:连续剧的发布数据不存在 ";
       }
       if(element.getStatus()!=300 && parent.getStatus()!=500){
           return "1:连续剧在网元"+targetid+"上的发布状态不正确 ";    
       }
       
       CntPlatformSync  platformSync = new CntPlatformSync();
       platformSync.setSyncindex(mapping.getRelateindex());
       platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
       int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 0 
               || targetResult == 30 
               || targetResult == 80)// 0待发布 // 30新增同步失败// 80取消同步成功
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 1);
       }else {                   
               return  "1:栏目和连续剧关联关系在网元"+targetid+"上的发布状态不正确 ";      
       }
       if(xmlPath == null){
           return "1:发布到目标系统"+targetid+":生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String parentcode = mapping.getReserve02();//获得栏目文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 1,platformNum,parentcode,elementcode);
        if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
            Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
        }else if(firTargetType == targettype){
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
        }else{
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
        }
       modTargetSynStatus( mapping, 1);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.CATEGORY_SERIES_MAPPING_PUBLISH,
       SeriesConfig.CATEGORY_SERIES_MAPPING_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("publish  category series mapping end...");
       String returnInfo ="0:发布到目标系统"+targetid+":操作成功 ";
       return returnInfo;    
    }
    public String publishCategorySeriesPlatformMap(Long syncindex) throws Exception
    {
        log.debug("publish platform for category series mapping start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
        
        CntPlatformSync platformsync = new  CntPlatformSync();
        platformsync.setSyncindex(syncindex);
        platformsync = cntPlatformSyncDS.getCntPlatformSync(platformsync);
        if(platformsync == null){
            return  "1:平台发布数据不存在 ";            
        }
        int platformStatus = platformsync.getStatus();
        if(platformStatus== 200 || platformStatus == 300 ){
            return   "1:平台发布状态不正确 ";        
        }  
        
        CntTargetSync targetSyn = new CntTargetSync();
        targetSyn.setRelateindex(syncindex);
        List<CntTargetSync>  mappingList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyn);
        if(mappingList == null || mappingList.size()==0){
            return  "1:该关联关系没有关联网元 ";
        }
        String BMSPub = getBMSPub(mappingList);
        if(BMSPub.substring(0,1).equals("1")){
            return BMSPub;
        }
        
        Long batchid = null;
        
        int firTargetType = findFirTargettype(mappingList,0);
        if(firTargetType != 99)
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        int size = mappingList.size();
        for(int i=0; i<size; i++){
            CntTargetSync map = mappingList.get(i);
            Long index = map.getSyncindex();    
            
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(map.getTargetindex());
            Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
            String targetid = targetSystemExist.getTargetid();
            
            if(map.getStatus() == 200 || map.getStatus() == 300 ){//对发布中或者发布的成功的网元不做操作
                allresult.append("栏目与连续剧关联关系在网元"+targetid+"上的发布状态不正确 ");  
                continue;
            }
            String rstInfo= publishCategorySeriesTarget(index,batchid,firTargetType);  
            if ( rstInfo.substring(0, 1).equals("0"))
            {
                successFlg = true;
                allresult.append(rstInfo.substring(2));
            }else
            {
                allresult.append(rstInfo.substring(2));
         }
            }                       
         log.debug("publish platform for  category series mapping end...");
        if(successFlg){
            return "0:"+allresult.toString();         
        }else{//全部失败
            return "1:"+allresult.toString();            
        }
    }


    public String publishCategorySeriesBatchMap(List<CmsSeries> mappingList) throws Exception
    {
        log.debug("publish batch for category series mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
        int size = mappingList.size();     
        for(CmsSeries mapping:mappingList){
            Long syncindex = mapping.getSyncindex();
            retInfo=  publishCategorySeriesPlatformMap(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量:" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量:" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量:" + iSuccessed + "  失败数量:" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("publish batch for category series mapping end...");
        return returnInfo.toString();   
    }   
    public String delPublishCategorySeriesTargetMap(Long syncindex) throws Exception
    {
       log.debug("delete publish category series mapping  for target start...");  
        String bingObject = "";
       CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
        mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
            return "1:没有可发布的mapping关系";
         }
       int type = mapping.getObjecttype();
       if(type == 27)//表示服务与点播内容的绑定关系
       {
           bingObject = "栏目";
           CategorySeriesMap csmap = new CategorySeriesMap();
           csmap.setMapindex(mapping.getObjectindex());  
           csmap = ctgsrmapDS.getCategorySeriesMap(csmap);        
           if(csmap == null){
               return "1:栏目与连续剧关联关系不存在 ";
           }
          Categorycdn category = new  Categorycdn();
          category.setCategoryid(mapping.getParentid());             
          category = catDS.getCategoryByCategoryId(category);
          if(category != null){       
              mapping.setReserve02(category.getCatagorycode());     
              mapping.setReserve01(csmap.getSequence().longValue());          
          }                
       }else{
           return "1:栏目与连续剧关联关系类型不匹配 ";
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getElementid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series!= null){
            mapping.setCancelpubtime(series.getCpcontentid());//把连续剧文广code放到取消发布时间字段中先
       }
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:栏目与连续剧的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:栏目与连续剧的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
         CntPlatformSync  platformSync = new CntPlatformSync();
         platformSync.setSyncindex(mapping.getRelateindex());
         platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
         int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 20       
               || targetResult == 90)// 20新增同步成功// 90取消同步失败
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
       }else {                   
               return   "1:栏目和连续剧关联关系在网元"+targetid+"上的发布状态不正确 "; 
       }
       if(xmlPath == null){
           return "1:从目标系统"+targetid+"取消发布:生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String parentcode = mapping.getReserve02();//获得栏目文广code
        String elementcode = mapping.getCancelpubtime();//获得连续剧文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
       modTargetSynStatus( mapping, 3);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.CATEGORY_SERIES_MAPPING_DEL_PUBLISH,
       SeriesConfig.CATEGORY_SERIES_MAPPING_DEL_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("delete publish  category series mapping  for target end...");
       String returnInfo ="0:操作成功 ";
       return returnInfo;
    }
    
    private String delPublishCategorySeriesTarget(Long syncindex,Long batchid,int firTargetType) throws Exception
    {
       log.debug("delete publish category series mapping  for target start...");  
        String bingObject = "";
       CntTargetSync mapping = new CntTargetSync();
         mapping.setSyncindex(syncindex);      
        mapping =cntTargetSyncDS.getCntTargetSync(mapping);     
         if(mapping == null){
            return "1:没有可发布的mapping关系 ";
         }
       int type = mapping.getObjecttype();
       if(type == 27)//表示服务与点播内容的绑定关系
       {
           bingObject = "栏目";
           CategorySeriesMap csmap = new CategorySeriesMap();
           csmap.setMapindex(mapping.getObjectindex());  
           csmap = ctgsrmapDS.getCategorySeriesMap(csmap);        
           if(csmap == null){
               return "1:栏目与连续剧关联关系不存在 ";
           }
          Categorycdn category = new  Categorycdn();
          category.setCategoryid(mapping.getParentid());             
          category = catDS.getCategoryByCategoryId(category);
          if(category != null){       
              mapping.setReserve02(category.getCatagorycode());     
              mapping.setReserve01(csmap.getSequence().longValue());          
          }                
       }
       CmsSeries series = new CmsSeries();
       series.setSeriesid(mapping.getElementid());
       series = cmsSeriesDS.getCmsSeriesById(series) ;  
       if(series!= null){
            mapping.setCancelpubtime(series.getCpcontentid());//把连续剧文广code放到取消发布时间字段中先
       }
         Targetsystem targetSystem = new Targetsystem();
         targetSystem.setTargetindex(mapping.getTargetindex());
         Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
         if(targetSystemExist == null){
             return "1:栏目与连续剧的关联关系发布的网元不存在 ";
         }
         if(targetSystemExist.getStatus() !=0){
             return "1:栏目与连续剧的关联关系发布的网元状态不正确 ";
         }       
       int targettype =targetSystemExist.getTargettype();
       String targetid = targetSystemExist.getTargetid();
         CntPlatformSync  platformSync = new CntPlatformSync();
         platformSync.setSyncindex(mapping.getRelateindex());
         platformSync =  cntPlatformSyncDS.getCntPlatformSync(platformSync);     
         int platformNum = (int)platformSync.getPlatform();
       String xmlPath = "";
       int targetResult = mapping.getOperresult();
       Map map = new HashMap<String, List>(); 
       List<CntTargetSync> seriesMapList = new ArrayList<CntTargetSync>();
       seriesMapList.add(mapping);
       map.put("seriesmapping", seriesMapList);
       if(targetResult == 20       
               || targetResult == 90)// 20新增同步成功// 90取消同步失败
       {       //生成xml文件    
           xmlPath = StandardImpFactory.getInstance().create(platformNum).createXmlFile(map, targettype, type, 3);
       }else {                   
               return  "1:栏目和连续剧关联关系在网元"+targetid+"上的发布状态不正确 "; 
       }
       if(xmlPath == null){
           return "1:从目标系统"+targetid+"取消发布:生成xml文件失败 ";
       }
       String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();
       Long taskindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
       Long targetindex = mapping.getTargetindex();
        Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
        Long objectindex = mapping.getObjectindex();
        String objectid = mapping.getObjectid();
        int objecttype = mapping.getObjecttype();
        String parentid = mapping.getParentid();
        String elementid = mapping.getElementid();
        String parentcode = mapping.getReserve02();//获得栏目文广code
        String elementcode = mapping.getCancelpubtime();//获得连续剧文广code
        insertObjectRecordForMap( syncIndex, taskindex, objectindex, objectid, objecttype,parentid,elementid,targetindex, 3,platformNum,parentcode,elementcode);
        if(firTargetType != 1 && firTargetType != 2 && firTargetType != 3){
            Long batchid2 = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid2,1); //插入同步任务
        }else if(firTargetType == targettype){
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,1); //插入同步任务
        }else{
            insertSyncTask(taskindex,xmlPath,correlateid,targetindex,batchid,0); //插入同步任务 
        }
       modTargetSynStatus( mapping, 3);
       CommonLogUtil.insertOperatorLog(mapping.getObjectid()+","+targetid, SeriesConfig.SERIES_MAPPING_PUBLISH_MANAGE,
       SeriesConfig.CATEGORY_SERIES_MAPPING_DEL_PUBLISH,
       SeriesConfig.CATEGORY_SERIES_MAPPING_DEL_PUBLISH_INFO,
       SeriesConfig.RESOURCE_OPERATION_SUCCESS);
       log.debug("delete publish  category series mapping  for target end...");
       String returnInfo ="0:从目标系统"+targetid+"取消发布: 操作成功 ";
       return returnInfo;
    }


    public String delPublishCategorySeriesPlatformMap(Long syncindex) throws Exception
    {
        log.debug("delete publish platform for category series mapping start...");
        boolean successFlg = false;
        StringBuffer allresult = new StringBuffer();  
        
        CntPlatformSync platformsync = new  CntPlatformSync();
        platformsync.setSyncindex(syncindex);
        platformsync = cntPlatformSyncDS.getCntPlatformSync(platformsync);
        if(platformsync == null){
            return  "1:平台发布数据不存在 ";            
        }
        int platformStatus = platformsync.getStatus();
        if(platformStatus== 0 || platformStatus == 200 || platformStatus == 400 ){
            return   "1:平台发布状态不正确 ";        
        }     
        
        CntTargetSync targetSyn = new CntTargetSync();
        targetSyn.setRelateindex(syncindex);
        List<CntTargetSync>  mappingList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyn);
        if(mappingList == null || mappingList.size()==0){
            return  "1:该关联关系没有关联网元 ";
        }
        
        Long batchid = null;
        
        int firTargetType = findFirTargettype(mappingList,2);
        if(firTargetType != 99)
            batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        
        int size = mappingList.size();
        for(int i=0; i<size; i++){
            CntTargetSync map = mappingList.get(i);
            Long index = map.getSyncindex();    
            
            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(map.getTargetindex());
            Targetsystem targetSystemExist =  targetSystemDS.getTargetsystem(targetSystem);
            String targetid = targetSystemExist.getTargetid();
            
            if(map.getStatus() == 0 || map.getStatus() == 200 || map.getStatus() == 400 ){//对待发布，发布中或者发布的失败的网元不做操作
                allresult.append("栏目与连续剧关联关系在网元"+targetid+"上的发布状态不正确 ");  
                continue;
            }
            String rstInfo= delPublishCategorySeriesTarget(index,batchid,firTargetType);  
            if ( rstInfo.substring(0, 1).equals("0"))
            {
                successFlg = true;
                allresult.append(rstInfo.substring(2));
            }else
            {
                allresult.append(rstInfo.substring(2));
         }
            }                       
         log.debug("delete publish platform for  category series mapping end...");
        if(successFlg){
            return "0:"+allresult.toString();         
        }else{//全部失败
            return "1:"+allresult.toString();            
        }
    }
    public String delPublishCategorySeriesBatchMap(List<CmsSeries> mappingList) throws Exception
    {
        log.debug("delete publish batch for category series mapping start...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int rstindex = 1;
        String retInfo = "";
        int size = mappingList.size();     
        for(CmsSeries mapping:mappingList){
            Long syncindex = mapping.getSyncindex();
            retInfo=  delPublishCategorySeriesPlatformMap(syncindex);  
           if(retInfo.substring(0, 1).equals("0")){
                 iSuccessed++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++),mapping.getObjectid(), 
                  resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);
                 }else{
                     ifail++;
                     String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                     operateInfo = new OperateInfo(String.valueOf(rstindex++), mapping.getObjectid(), 
                      resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                     returnInfo.appendOperateInfo(operateInfo);            
                 } 
        } 
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量:" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量:" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量:" + iSuccessed + "  失败数量:" +ifail);
                returnInfo.setFlag("2");
            }
        }  
        log.debug("delete publish batch for category series mapping end...");
        return returnInfo.toString();   
    }
    private void insertObjectRecordForMap( Long syncindex,  Long taskindex,Long objectindex,String objectid, int objecttype,String parentid, String elementid, Long targetindex,int synType, int platform,String parentcode,String elementcode)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setParentid(parentid);
            objectSyncRecord.setElementid(elementid);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            if(platform == 1)
            {
                objectSyncRecord.setParentcode(parentcode);
                objectSyncRecord.setElementcode(elementcode);
            }
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }  
       catch (Exception e)
       {
           log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    private void insertSyncTask(Long taskindex, String xmlPath, String correlateid, Long targetindex,Long batchid,int status)
    {
            CntSyncTask cntSyncTask = new CntSyncTask();
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            try
            {
                cntSyncTask.setTaskindex(taskindex);
                cntSyncTask.setBatchid(batchid);
                cntSyncTask.setStatus(status);
                cntSyncTask.setPriority(5);
                cntSyncTask.setRetrytimes(3);
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(xmlPath);
                cntSyncTask.setDestindex(Long.valueOf(targetindex));
                cntSyncTask.setSource(1);
                cntSyncTask.setStarttime(dateformat.format(new Date()));
                cntSyncTaskDS.insertCntSyncTask(cntSyncTask);
            }
            catch (Exception e)
            {
                log.error("cmsProgramLS exception:" + e.getMessage());
            }
    }
    private void modTargetSynStatus(CntTargetSync mapping , int action) throws Exception
    {   
        CntTargetSync  targetSyn = new CntTargetSync();
       int result = mapping.getOperresult();
       if(action ==1){      
        if (result== 0)//待发布
        {    
            targetSyn.setOperresult(10);
        }
        else if(result== 60)  //修改同步失败
        {
            targetSyn.setOperresult(40);  //修改同步中
        }
        else        
        {     // 新增同步失败 或者取消同步失败
            targetSyn.setOperresult(10);// 新增同步中
         }
        targetSyn.setStatus(200);
        targetSyn.setSyncindex(mapping.getSyncindex());
        cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态  
       }
       if(action == 2){
           if( result == 20 ||  
                result == 50 || 
                result == 90 || 
                result == 60 )  //20新增同步成功//50修改同步成功 //90取消同步失败  //60改同步失败  
           { 
               targetSyn.setOperresult(40); //修改同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(mapping.getSyncindex());
           cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态     
       }
       if(action == 3){
           if(result == 20 ||  //新增同步成功
              result == 50 ||  //修改同步成功
               result == 60 ||   //修改同步失败
               result == 90  )  //取消同步失败                   
           { 
               targetSyn.setOperresult(70); //取消同步中
           }
           targetSyn.setStatus(200); //服务队网元发布状态为发布中
           targetSyn.setSyncindex(mapping.getSyncindex());
           cntTargetSyncDS.updateCntTargetSync(targetSyn);//修改网元发布状态     
       }
       CntTargetSync  sameRelateindexTargetSyn = new CntTargetSync();
       Long targetRelateindex = mapping.getRelateindex();
       sameRelateindexTargetSyn.setRelateindex(targetRelateindex);    
       List<CntTargetSync> mappingList = cntTargetSyncDS.getCntTargetSyncByCond(sameRelateindexTargetSyn);
       int size = mappingList.size();
       int targetStatusArrary[] = new int[size];      
       for(int i=0; i<size; i++){
           targetStatusArrary[i] = StatusTranslator.getTargetSyncStatus(mappingList.get(i).getOperresult()); 
       }
       int platformStatus = StatusTranslator.getPlatformSyncStatus(targetStatusArrary);
       CntPlatformSync platformSyn = new  CntPlatformSync();
       platformSyn.setSyncindex(targetRelateindex);
       platformSyn.setStatus(platformStatus);
       cntPlatformSyncDS.updateCntPlatformSync(platformSyn);   
    }
    private int findFirTargettype(List<CntTargetSync> serviceTargetSynList,int optype ) throws DomainServiceException{
        int type=99;
        for (CntTargetSync targetSync : serviceTargetSynList){
            int status = targetSync.getStatus();
            if(optype == 0 || optype ==1){//0,1表示新增和修改
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 0 || status == 400 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 0 || status == 400 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 0 || status == 400 || status == 500))type =2;
            }else{//2表示取消（删除）
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 2 && (status == 300 || status == 500))type =2;
                if(targettype == 1 && (type==99 || type==3) && (status == 300 || status == 500))type =1;
                if(targettype == 3 && type ==99 && (status == 300 || status == 500))type =3;
            }
        }
        return type;
    }
    private String getBMSPub(List<CntTargetSync> mappingList) throws DomainServiceException{
        for (CntTargetSync mappingTarget : mappingList){
            Targetsystem target = new Targetsystem();
            target.setTargetindex(mappingTarget.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            //只有存在EPG网元且能够取消发布的时候才去判断是否有mapping已发布
            if (target.getTargettype() == 1)
            {
                CntTargetSync parent = new CntTargetSync();
                parent.setObjectid(mappingTarget.getParentid());
                parent.setTargetindex(mappingTarget.getTargetindex());
                parent.setObjecttype(ObjectType.CATEGORY_TYPE);
                List<CntTargetSync> parentList = cntTargetSyncDS.getCntTargetSyncByCond(parent);
                
                if (null != parentList && parentList.size() > 0)
                {
                    for(CntTargetSync targetSync:parentList )
                    {
                        if(targetSync.getStatus() != 300 && targetSync.getStatus()!=500){
                            return "1:栏目在网元"+target.getTargetid()+"上未发布，不能进行发布操作 ";
                        }
                       
                    }
                }

                //判断被绑定对象的发布状态是否为发布成功
                CntTargetSync element = new CntTargetSync();
                element.setObjectid(mappingTarget.getElementid());
                element.setTargetindex(mappingTarget.getTargetindex());
                element.setObjecttype(ObjectType.SERIES_TYPE);
                List<CntTargetSync> elementList = cntTargetSyncDS.getCntTargetSyncByCond(element);
                
                if (null != elementList && elementList.size() > 0)
                {
                    for(CntTargetSync targetSync:elementList )
                    {
                        if(targetSync.getStatus() != 300 && targetSync.getStatus()!=500){
                            return "1:连续剧在网元"+target.getTargetid()+"上未发布，不能进行发布操作 ";
                        }
                       
                    }
                }
                
            }
        }
        return "0";
        
    }
}

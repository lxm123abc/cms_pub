
function clearHiddenHTML() {
	$("#divhidden").html("");
}

function clearQueryProp(rowIndex) {
	var queryBodyObj = document.getElementById("queryBody");
	queryBodyObj.deleteRow(rowIndex-1);
}

function clearQueryHTML() {
	var queryBodyObj = document.getElementById("queryBody");
	while(queryBodyObj.firstChild) {
		queryBodyObj.removeChild(queryBodyObj.firstChild);
	}
}

function clearTableHTML() {
	var tableBodyObj = document.getElementById("tableBody");
	while (tableBodyObj.rows.length > 1) {
		tableBodyObj.deleteRow(tableBodyObj.rows.length-1);
	}
	$("#xtable").contents().find("#xtable").html("");
}

function showDiv(id) {
	$("#divtab").find("td[id^='tab'][class='on']").attr("class", "");
	$("#divtab").find("td[id='tab"+id+"']").attr("class", "on");
	var qtitle = $("#divquery").find("label[id='lblQueryTitle']");
	var qtip = $("#divquery").find("label[id='lblQueryTip']");
	var ttitle = $("#divtable").find("label[id='lblTableTitle']");
	var ttip = $("#divtable").find("label[id='lblTableTip']");
	if (id == "bpel") {
		qtitle.text("业务流定义 ");
		qtip.text("以->表示引用 ->this表示引用自身 调用入参以arg0...arg9命名 逻辑判断以statement命名");
		ttitle.text("业务流查看 ");
		ttip.text("双击第三列可以设置断点");
	}
	else if (id == "runnable") {
		qtitle.text("多线程定义 ");
		qtip.text("");
		ttitle.text("多线程查看 ");
		ttip.text("");
	}
	else if (id == "service") {
		qtitle.text("查询条件 ");
		qtip.text("自定义对象的值支持JSON表达式 自定义查询的参数总数不能超过10个");
		ttitle.text("查询列表 ");
		ttip.text("仅分页查询支持排序 *号表示以当前列排序 A表示升序 D表示降序");
	}
	else {
		qtitle.text("查询条件 ");
		qtip.text("");
		ttitle.text("查询列表 ");
		ttip.text("");
	}
	clearQueryHTML();
	clearTableHTML();
	$("#divcode").hide(); 
	$("#divlogin").hide(); 
	$("#divbpel").hide(); 
	$("#divservice").hide(); 
	$("#divrunnable").hide(); 
	$("#div"+id).show();
}

function getDivId() {
	return $("#divtab").find("td[id^='tab'][class='on']").attr("id").substr(3);
}
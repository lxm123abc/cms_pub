
var treeObject = {};
var pageObject = {};
var tree;

function buildTreeHTMLByFSO() {
	tree = new dhtmlXTreeObject("dhtmltree","100%","100%",0);
	tree.setImagePath("imgs/");
	tree.enableCheckBoxes(0);
	tree.setOnClickHandler(readFile);
	tree.enableDragAndDrop(true, false);
	tree.setDragHandler(moveFile);
	treeObject = {};
	callUrl("fileSystemManagerDS/getTagNodeList.ssm", null, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue)) return;
	var jsObj = treeObject.rtnValue;
	for (var obj in jsObj) {
		var item = jsObj[obj];
		if (item.isdir) {
			tree.insertNewItem(item.pid, item.id, item.name, "", "folderClosed.gif");
		}
		else {
			tree.insertNewItem(item.pid, item.id, item.name);
		}
	}
}

function createFolder() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("folder");
}

function createFile() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt")) {
		alert("请选择一个目录");
		return;
	}
	$("#itemName").show();
	$("#itemName").find("input[id='txtItemActor']").val("file");
}

function createFolderOrFile() {
	var sid = tree.getSelectedItemId();
	if (!sid || sid.endWith(".txt")) {
		alert("请选择一个目录");
		return;
	}
	var actor = $("#itemName").find("input[id='txtItemActor']").val();
	var name = $("#itemName").find("input[id='txtItemName']").val();
	if (isEmpty(name)) {
		alert("请输入目录名或者不带扩展名的文件名");
		return;
	}
	treeObject = {};
	if (actor == "folder") {
		callUrl("fileSystemManagerDS/createFolder.ssm", '[' + sid + ',' + name + ']', 'treeObject', true);
	}
	else {
		name = name + ".txt";
		$("#itemName").find("input[id='txtItemJsonStr']").val("");
		parent.frames["imain"].readPageObject();
		pageObject = {};
		pageObject.jsonstr = $("#itemName").find("input[id='txtItemJsonStr']").val();
		callUrl("fileSystemManagerDS/createFile.ssm", '[' + sid + ',' + name + ',pageObject]', 'treeObject', true);
	}
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("创建失败");
		return;
	}
	if (actor == "folder") {
		tree.insertNewItem(sid, treeObject.rtnValue, name, "", "folderClosed.gif");
	}
	else {
		tree.insertNewItem(sid, treeObject.rtnValue, name);
	}
	$("#itemName").hide();
}

function saveFile() {
	var sid = tree.getSelectedItemId();
	if (!sid || !sid.endWith(".txt")) {
		alert("请选择一个文件");
		return;
	}
	if (!confirm("确认覆盖当前的Tag?")) return;
	$("#itemName").find("input[id='txtItemJsonStr']").val("");
	parent.frames["imain"].readPageObject();
	pageObject = {};
	pageObject.jsonstr = $("#itemName").find("input[id='txtItemJsonStr']").val();
	callUrl("fileSystemManagerDS/saveFile.ssm", '[' + sid + ',pageObject]', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("保存失败");
		return;
	}
}

function deleteFile() {
	var sid = tree.getSelectedItemId();
	if (!sid) {
		alert("请选择一个目录或者文件");
		return;
	}
	if (!confirm("确认删除?")) return;
	treeObject = {};
	callUrl("fileSystemManagerDS/deleteFile.ssm", sid, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("删除失败");
		return;
	}
	tree.deleteItem(sid);	
}

function readFile() {
	$("#itemName").hide();
	var sid = tree.getSelectedItemId();
	if (!sid || !sid.endWith(".txt")) return;
	treeObject = {};
	callUrl("fileSystemManagerDS/readFile.ssm", sid, 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || isEmpty(treeObject.rtnValue.jsonstr)) {
		return;
	}
	$("#itemName").find("input[id='txtItemJsonStr']").val(treeObject.rtnValue.jsonstr);
	parent.frames["imain"].writePageObject();
}

function moveFile(sid, pid) {
	if (!sid || !sid.endWith(".txt")) return false;
	if (!pid || pid.endWith(".txt")) return false;
	treeObject = {};
	callUrl("fileSystemManagerDS/moveFile.ssm", '[' + sid + ',' + pid + ']', 'treeObject', true);
	if (isEmpty(treeObject) || isEmpty(treeObject.rtnValue) || treeObject.rtnValue == "null") {
		alert("移动文件失败");
		return false;
	}
	tree.changeItemId(sid,treeObject.rtnValue);
	return true;
}
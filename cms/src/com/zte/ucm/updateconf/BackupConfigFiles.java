package com.zte.ucm.updateconf;

import com.zte.ucm.Constants;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;
import com.zte.ucm.util.DateUtil;
import com.zte.ucm.util.FileUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class BackupConfigFiles
{
  private static Logger log = LoggerFactory.getLogger(BackupConfigFiles.class);

  public static String bakupdir = "";

  public static Map<String, String> fileMap = new HashMap();

  public BackupConfigFiles()
  {
    if ("".equals(bakupdir))
    {
      bakupdir = Constants.CONFIGFILEPATH + "bakfiles/" + DateUtil.getDate("yyyyMMdd-HHmmss") + "/";

      File localFile = new File(bakupdir);

      if (!localFile.exists())
      {
        localFile.mkdirs();
      }
      log.info("Back-up dir:" + bakupdir);
    }
  }

  public void exe(Config paramConfig)
    throws Exception
  {
    String str1 = paramConfig.getCode();
    String str2 = paramConfig.getFile();
    String str3 = str2.substring(str2.lastIndexOf("/") + 1);

    String str4 = str3 + "_" + str1;
    if (!fileMap.containsKey(str2))
    {
      fileMap.put(str2, str4);
      String str5 = (String)fileMap.get(str2);
      FileUtil localFileUtil = new FileUtil();
      boolean bool = localFileUtil.copyFile(str2, bakupdir + str5);
      if (bool)
      {
        log.info("Backup success:" + str2 + "-->" + bakupdir + str5);
      }
      else
      {
        log.error("Backup failure:" + str2 + "-->" + bakupdir + str5);
      }
    }
  }
}
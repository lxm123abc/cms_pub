package com.zte.cms.service.categoryservicemap.service;

import java.util.List;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.cms.service.categoryservicemap.dao.ICategoryServiceMapDAO;
import com.zte.cms.service.categoryservicemap.service.ICategoryServiceMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CategoryServiceMapDS extends DynamicObjectBaseDS implements ICategoryServiceMapDS 
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private ICategoryServiceMapDAO dao = null;
	
	public void setDao(ICategoryServiceMapDAO dao)
	{
	    this.dao = dao;
	}

	public void insertCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException
	{
		log.debug("insert categoryServiceMap starting...");
		Long id = (Long) this.getPrimaryKeyGenerator().getPrimarykey("category_service_map");
		categoryServiceMap.setMapindex(id);
		
		try
		{
			dao.insertCategoryServiceMap( categoryServiceMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert categoryServiceMap end");
	}
	
	public void updateCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException
	{
		log.debug("update categoryServiceMap by pk starting...");
	    try
		{
			dao.updateCategoryServiceMap( categoryServiceMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update categoryServiceMap by pk end");
	}

	public void updateCategoryServiceMapList( List<CategoryServiceMap> categoryServiceMapList )throws DomainServiceException
	{
		log.debug("update categoryServiceMapList by pk starting...");
		if(null == categoryServiceMapList || categoryServiceMapList.size() == 0 )
		{
			log.debug("there is no datas in categoryServiceMapList");
			return;
		}
	    try
		{
			for( CategoryServiceMap categoryServiceMap : categoryServiceMapList )
			{
		    	dao.updateCategoryServiceMap( categoryServiceMap );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update categoryServiceMapList by pk end");
	}



	public void removeCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException
	{
		log.debug( "remove categoryServiceMap by pk starting..." );
		try
		{
			dao.deleteCategoryServiceMap( categoryServiceMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove categoryServiceMap by pk end" );
	}

	public void removeCategoryServiceMapList ( List<CategoryServiceMap> categoryServiceMapList )throws DomainServiceException
	{
		log.debug( "remove categoryServiceMapList by pk starting..." );
		if(null == categoryServiceMapList || categoryServiceMapList.size() == 0 )
		{
			log.debug("there is no datas in categoryServiceMapList");
			return;
		}
		try
		{
			for( CategoryServiceMap categoryServiceMap : categoryServiceMapList )
			{
				dao.deleteCategoryServiceMap( categoryServiceMap );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove categoryServiceMapList by pk end" );
	}



	public CategoryServiceMap getCategoryServiceMap( CategoryServiceMap categoryServiceMap )throws DomainServiceException
	{
		log.debug( "get categoryServiceMap by pk starting..." );
		CategoryServiceMap rsObj = null;
		try
		{
			rsObj = dao.getCategoryServiceMap( categoryServiceMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get categoryServiceMapList by pk end" );
		return rsObj;
	}

	public List<CategoryServiceMap> getCategoryServiceMapByCond( CategoryServiceMap categoryServiceMap )throws DomainServiceException
	{
		log.debug( "get categoryServiceMap by condition starting..." );
		List<CategoryServiceMap> rsList = null;
		try
		{
			rsList = dao.getCategoryServiceMapByCond( categoryServiceMap );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get categoryServiceMap by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get categoryServiceMap page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(categoryServiceMap, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CategoryServiceMap>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get categoryServiceMap page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(CategoryServiceMap categoryServiceMap, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get categoryServiceMap page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(categoryServiceMap, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<CategoryServiceMap>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get categoryServiceMap page info by condition end" );
		return tableInfo;
	}
}

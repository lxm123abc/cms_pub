package com.zte.cms.cast.castsync.model;

public class CastTargetSync
{
    private java.lang.Long syncindex;
    private java.lang.Long relateindex;
    private java.lang.Integer objecttype;
    private java.lang.Long objectindex;
    private java.lang.String objectid;
    private java.lang.String elementid;
    private java.lang.String parentid;
    private java.lang.Long targetindex;
    private java.lang.Integer status;
    private java.lang.Integer operresult;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private java.lang.Integer typetarget;
    
    private java.lang.String onlinetimeStartDate;
    private java.lang.String onlinetimeEndDate;
    private java.lang.String cntofflinestarttime;
    private java.lang.String cntofflineendtime;
    private java.lang.String createStarttime;
    private java.lang.String createEndtime;
    
    private java.lang.Long castindex;
    private java.lang.String castid;
    private java.lang.String castname;
    private java.lang.String createtime;
    private java.lang.String targetname;
    private java.lang.String targetid;
    private java.lang.Integer targettype;
    
    private java.lang.String statuses;
    
    public java.lang.Long getSyncindex()
    {
        return syncindex;
    }

    public void setSyncindex(java.lang.Long syncindex)
    {
        this.syncindex = syncindex;
    }

    public java.lang.Long getRelateindex()
    {
        return relateindex;
    }

    public void setRelateindex(java.lang.Long relateindex)
    {
        this.relateindex = relateindex;
    }

    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    }

    public void setObjecttype(java.lang.Integer objecttype)
    {
        this.objecttype = objecttype;
    }

    public java.lang.Long getObjectindex()
    {
        return objectindex;
    }

    public void setObjectindex(java.lang.Long objectindex)
    {
        this.objectindex = objectindex;
    }

    public java.lang.String getObjectid()
    {
        return objectid;
    }

    public void setObjectid(java.lang.String objectid)
    {
        this.objectid = objectid;
    }

    public java.lang.String getElementid()
    {
        return elementid;
    }

    public void setElementid(java.lang.String elementid)
    {
        this.elementid = elementid;
    }

    public java.lang.String getParentid()
    {
        return parentid;
    }

    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }

    public java.lang.Long getTargetindex()
    {
        return targetindex;
    }

    public void setTargetindex(java.lang.Long targetindex)
    {
        this.targetindex = targetindex;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getOperresult()
    {
        return operresult;
    }

    public void setOperresult(java.lang.Integer operresult)
    {
        this.operresult = operresult;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.Integer getTypetarget()
    {
        return typetarget;
    }

    public void setTypetarget(java.lang.Integer typetarget)
    {
        this.typetarget = typetarget;
    }

    public java.lang.String getOnlinetimeStartDate()
    {
        return onlinetimeStartDate;
    }

    public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
    {
        this.onlinetimeStartDate = onlinetimeStartDate;
    }

    public java.lang.String getOnlinetimeEndDate()
    {
        return onlinetimeEndDate;
    }

    public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
    {
        this.onlinetimeEndDate = onlinetimeEndDate;
    }

    public java.lang.String getCntofflinestarttime()
    {
        return cntofflinestarttime;
    }

    public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
    {
        this.cntofflinestarttime = cntofflinestarttime;
    }

    public java.lang.String getCntofflineendtime()
    {
        return cntofflineendtime;
    }

    public void setCntofflineendtime(java.lang.String cntofflineendtime)
    {
        this.cntofflineendtime = cntofflineendtime;
    }

    public java.lang.String getCreateStarttime()
    {
        return createStarttime;
    }

    public void setCreateStarttime(java.lang.String createStarttime)
    {
        this.createStarttime = createStarttime;
    }

    public java.lang.String getCreateEndtime()
    {
        return createEndtime;
    }

    public void setCreateEndtime(java.lang.String createEndtime)
    {
        this.createEndtime = createEndtime;
    }

    public java.lang.Long getCastindex()
    {
        return castindex;
    }

    public void setCastindex(java.lang.Long castindex)
    {
        this.castindex = castindex;
    }

    public java.lang.String getCastid()
    {
        return castid;
    }

    public void setCastid(java.lang.String castid)
    {
        this.castid = castid;
    }

    public java.lang.String getCastname()
    {
        return castname;
    }

    public void setCastname(java.lang.String castname)
    {
        this.castname = castname;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }

    public java.lang.String getTargetid()
    {
        return targetid;
    }

    public void setTargetid(java.lang.String targetid)
    {
        this.targetid = targetid;
    }

    public java.lang.Integer getTargettype()
    {
        return targettype;
    }

    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }

    public java.lang.String getStatuses()
    {
        return statuses;
    }

    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }

}

package com.zte.cms.targetSystem.dao;

import java.util.List;

import com.zte.cms.service.model.Service;
import com.zte.cms.targetSystem.dao.ITargetsystemDAO;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class TargetsystemDAO extends DynamicObjectBaseDao implements ITargetsystemDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertTargetsystem(Targetsystem targetsystem) throws DAOException
    {
        log.debug("insert targetsystem starting...");
        super.insert("insertTargetsystem", targetsystem);
        log.debug("insert targetsystem end");
    }

    public void updateTargetsystem(Targetsystem targetsystem) throws DAOException
    {
        log.debug("update targetsystem by pk starting...");
        super.update("updateTargetsystem", targetsystem);
        log.debug("update targetsystem by pk end");
    }

    public void updateTargetsystemByCond(Targetsystem targetsystem) throws DAOException
    {
        log.debug("update targetsystem by conditions starting...");
        super.update("updateTargetsystemByCond", targetsystem);
        log.debug("update targetsystem by conditions end");
    }

    public void deleteTargetsystem(Targetsystem targetsystem) throws DAOException
    {
        log.debug("delete targetsystem by pk starting...");
        super.delete("deleteTargetsystem", targetsystem);
        log.debug("delete targetsystem by pk end");
    }

    public void deleteTargetsystemByCond(Targetsystem targetsystem) throws DAOException
    {
        log.debug("delete targetsystem by conditions starting...");
        super.delete("deleteTargetsystemByCond", targetsystem);
        log.debug("update targetsystem by conditions end");
    }

    public Targetsystem getTargetsystem(Targetsystem targetsystem) throws DAOException
    {
        log.debug("query targetsystem starting...");
        Targetsystem resultObj = (Targetsystem) super.queryForObject("getTargetsystem", targetsystem);
        log.debug("query targetsystem end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Targetsystem> getTargetsystemByCond(Targetsystem targetsystem) throws DAOException
    {
        log.debug("query targetsystem by condition starting...");
        List<Targetsystem> rList = (List<Targetsystem>) super.queryForList("queryTargetsystemListByCond", targetsystem);
        log.debug("query targetsystem by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize) throws DAOException
    {
        log.debug("page query targetsystem by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryTargetsystemListCntByCond", targetsystem)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Targetsystem> rsList = (List<Targetsystem>) super.pageQuery("queryTargetsystemListByCond",
                    targetsystem, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query targetsystem by condition end");
        return pageInfo;
    }
    public PageInfo pageInfoQueryPop(Targetsystem targetsystem, int start, int pageSize) throws DAOException
    {
        log.debug("page query targetsystem by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryTargetsystemListCntByCondPop", targetsystem)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Targetsystem> rsList = (List<Targetsystem>) super.pageQuery("queryTargetsystemListByCondPop",
                    targetsystem, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query targetsystem by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryTargetsystemListByCond", "queryTargetsystemListCntByCond", targetsystem,
                start, pageSize, puEntity);
    }

    /**
     * 检查目标系统的id是否重复
     * @param targetsystemID
     * @return boolean
     * @throws DomainServiceException
     */
	public int checkTargetsystemID(String targetsystemID)
			throws DAOException {
		int totalCnt = ((Integer) super.queryForObject("queryTargetsystemIdCount", targetsystemID)).intValue();
		return totalCnt;
	}
	
	/**
     * 查询服务关联目标系统
     * @param service 服务信息，此处主要使用其serviceindex 
     * @param start 
     * @param pageSize
     * @return
     * @throws DAOException
     */
    public PageInfo queryServiceBindTargetListByCond(Service service, int start, int pageSize)throws DAOException
    {
        log.debug("page query targetsystem for service starting...");
        PageInfo pageInfo = null;
        
        int totalCnt = ((Integer) super.queryForObject("queryServiceBindTargetListCntByCond", service)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Targetsystem> rsList = (List<Targetsystem>) super.pageQuery("queryServiceBindTargetListByCond",
                    service, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query targetsystem for service end");
        return pageInfo;
    }
    
}
package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.dao.ICastPictureMapDAO;
import com.zte.cms.picture.service.ICastPictureMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CastPictureMapDS extends DynamicObjectBaseDS implements ICastPictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICastPictureMapDAO dao = null;

    public void setDao(ICastPictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException
    {
        log.debug("insert castPictureMap starting...");
        try
        {
            if (null == castPictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_cast_picture_map_index");
                String mappingid = String.format("%02d", ObjectType.CASTPICTURE_TYPE)+ String.format("%030d", mapIndex);
                castPictureMap.setMapindex(mapIndex);
                castPictureMap.setMappingid(mappingid);
            }
            dao.insertCastPictureMap(castPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert castPictureMap end");
    }

    public void updateCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException
    {
        log.debug("update castPictureMap by pk starting...");
        try
        {
            dao.updateCastPictureMap(castPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castPictureMap by pk end");
    }

    public void updateCastPictureMapList(List<CastPictureMap> castPictureMapList) throws DomainServiceException
    {
        log.debug("update castPictureMapList by pk starting...");
        if (null == castPictureMapList || castPictureMapList.size() == 0)
        {
            log.debug("there is no datas in castPictureMapList");
            return;
        }
        try
        {
            for (CastPictureMap castPictureMap : castPictureMapList)
            {
                dao.updateCastPictureMap(castPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castPictureMapList by pk end");
    }

    public void removeCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException
    {
        log.debug("remove castPictureMap by pk starting...");
        try
        {
            dao.deleteCastPictureMap(castPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castPictureMap by pk end");
    }

    public void removeCastPictureMapList(List<CastPictureMap> castPictureMapList) throws DomainServiceException
    {
        log.debug("remove castPictureMapList by pk starting...");
        if (null == castPictureMapList || castPictureMapList.size() == 0)
        {
            log.debug("there is no datas in castPictureMapList");
            return;
        }
        try
        {
            for (CastPictureMap castPictureMap : castPictureMapList)
            {
                dao.deleteCastPictureMap(castPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castPictureMapList by pk end");
    }

    public CastPictureMap getCastPictureMap(CastPictureMap castPictureMap) throws DomainServiceException
    {
        log.debug("get castPictureMap by pk starting...");
        CastPictureMap rsObj = null;
        try
        {
            rsObj = dao.getCastPictureMap(castPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castPictureMapList by pk end");
        return rsObj;
    }

    public List<CastPictureMap> getCastPictureMapByCond(CastPictureMap castPictureMap) throws DomainServiceException
    {
        log.debug("get castPictureMap by condition starting...");
        List<CastPictureMap> rsList = null;
        try
        {
            rsList = dao.getCastPictureMapByCond(castPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castPictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get castPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CastPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castPictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CastPictureMap castPictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get castPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castPictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CastPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castPictureMap page info by condition end");
        return tableInfo;
    }
    
    public TableDataInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get castPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryCastPicMap(castPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castPictureMap page info by condition end");
        return tableInfo;
    }
    
    public TableDataInfo pageInfoQueryCastPicTargetMap(CastPictureMap castPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get castPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryCastPicTargetMap(castPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castPictureMap page info by condition end");
        return tableInfo;
    }    
}

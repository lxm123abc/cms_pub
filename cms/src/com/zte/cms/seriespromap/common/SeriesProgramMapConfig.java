package com.zte.cms.seriespromap.common;

public interface SeriesProgramMapConfig
{
    public static final String SERIES_PROGRAM_MGT = "log.series.program.mgt";

    public static final String SERIES_PROGRAM_MAP_ADD = "series.program.map.add";
    public static final String SERIES_PROGRAM_MAP_DELETE = "series.program.map.delete";
    public static final String SERIES_PROGRAM_MAP_PUBLISH = "series.program.map.publish";
    public static final String SERIES_PROGRAM_MAP_CANCEL_PUBLISH = "series.program.map.cancel.publish";

    public static final String SERIES_PROGRAM_MAP_ADD_INFO = "series.program.map.add.info";
    public static final String SERIES_PROGRAM_MAP_DELETE_INFO = "series.program.map.delete.info";
    public static final String SERIES_PROGRAM_MAP_PUBLISH_INFO = "series.program.map.publish.info";
    public static final String SERIES_PROGRAM_MAP_CANCEL_PUBLISH_INFO = "series.program.map.cancel.publish.info";

    // 操作结果
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
}

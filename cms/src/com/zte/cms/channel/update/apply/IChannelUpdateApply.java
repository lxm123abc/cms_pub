package com.zte.cms.channel.update.apply;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.content.targetSyn.model.CntTargetSync;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IChannelUpdateApply
{

    /**
     * 修改工作流申请
     * 
     * @param channel CmsChannel
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsChannel channel) throws DomainServiceException, Exception;

    /**
     * 
     * @param channel CmsChannel
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsChannel channel, String priority) throws DomainServiceException, Exception;

    /**
     * 修改工作流审核
     * 
     * @param channelbuf CmsChannelBuf
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     * @throws Exception 
     */
    public abstract String audit(CmsChannelBuf channelbuf, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException, Exception;

    /**
     * 
     * @param channelbuf CmsChannelBuf
     * @return CmsChannelBuf
     * @throws Exception Exception
     */
    public CmsChannelBuf getChannelBuf(CmsChannelBuf channelbuf) throws Exception;
   
}
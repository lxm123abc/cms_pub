package com.zte.cms.categorycdn.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.categorycdn.common.CategoryConstant;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.cms.categorycdn.service.CategorycdnDS;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.channel.service.ICategoryChannelMapDS;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.cms.content.CSplatformSyn.service.ICSCntPlatformSyncDS;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.cms.content.CStargetSyn.service.ICSCntTargetSyncDS;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.CntPlatformSyncDS;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.CntTargetSyncDS;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.ctgpgmap.service.ICategoryProgramMapDS;
import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.cms.ctgsrmap.service.ICategorySeriesMapDS;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.ICategoryPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.cms.service.categoryservicemap.service.ICategoryServiceMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/** @author Administrator */
public class CmsCategoryLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsCategoryLS
{

    private static final String CATEGORY_CANCELPUB_POSTER = "category.canclepublish.hasposter";
    private static final String RESOURCE_ASSESSRULE_CANCELNOTFORCONDITION = "cmsprogramtype.canclepublish.notforCondition"; // 节目分类信息取消发布不符合条件
    private static final String RESOURCE_ASSESSRULE_ISNOTEXIST = "cmsprogramtype.reason.isnotExist";
    // 节目分类信息不存在
    private static final String FAIL = "cmsprogramtype.fail";
    private static final String SUCCESS = "cmsprogramtype.success";
    private static final String CANCLE_PUBLISH_SUCCESS = "cmsprogramtype.canclepublish.success";
    private static final String CMSPROGRAMTYPE_CNTSYNCXML = "xmlsync";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
    private ICategorycdnDS cmsCategoryDS;
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private ICntSyncTaskDS cntSyncTaskDS;
    private String result = "0:操作成功";
    public final static String CNTSYNCXML = "xmlsync";
    public final static String PROGRAM_DIR = "program";

    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败

    // 内容相关对象同步任务表 objectType
    public final static int PROGRAM_OBJECTTYPE = 1;
    public final static int MOVIE_OBJECTTYPE = 2;

    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    String xmladdress = "";
    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
    public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private ICmsStorageareaLS cmsStorageareaLS;

    private Map catpicmap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象
    // 和 其子内容对象
    private IUsysConfigDS usysConfigDS = null;

    private ICntSyncRecordDS cntSyncRecordDS = null;
    private DbUtil db = new DbUtil();
    private ITargetsystemDS targetSystemDS;
    private IPictureDS pictureDS;
    private ICategoryPictureMapDS categoryPictureMapDS;
    private ICategoryProgramMapDS categoryProgramMapDS;
    private ICategorySeriesMapDS categorySeriesMapDS;
    private ICategoryChannelMapDS categoryChannelMapDS;
    private ICategoryServiceMapDS categoryServiceMapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICSCntPlatformSyncDS cscntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ICSCntTargetSyncDS CScntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public ICSCntPlatformSyncDS getCscntPlatformSyncDS()
    {
        return cscntPlatformSyncDS;
    }

    public void setCscntPlatformSyncDS(ICSCntPlatformSyncDS cscntPlatformSyncDS)
    {
        this.cscntPlatformSyncDS = cscntPlatformSyncDS;
    }

    public ICSCntTargetSyncDS getCScntTargetSyncDS()
    {
        return CScntTargetSyncDS;
    }

    public void setCScntTargetSyncDS(ICSCntTargetSyncDS scntTargetSyncDS)
    {
        CScntTargetSyncDS = scntTargetSyncDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(CntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setCntPlatformSyncDS(CntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICategoryServiceMapDS getCategoryServiceMapDS()
    {
        return categoryServiceMapDS;
    }

    public void setCategoryServiceMapDS(ICategoryServiceMapDS categoryServiceMapDS)
    {
        this.categoryServiceMapDS = categoryServiceMapDS;
    }

    public ICategoryChannelMapDS getCategoryChannelMapDS()
    {
        return categoryChannelMapDS;
    }

    public void setCategoryChannelMapDS(ICategoryChannelMapDS categoryChannelMapDS)
    {
        this.categoryChannelMapDS = categoryChannelMapDS;
    }

    public ICategorySeriesMapDS getCategorySeriesMapDS()
    {
        return categorySeriesMapDS;
    }

    public void setCategorySeriesMapDS(ICategorySeriesMapDS categorySeriesMapDS)
    {
        this.categorySeriesMapDS = categorySeriesMapDS;
    }

    public ICategoryProgramMapDS getCategoryProgramMapDS()
    {
        return categoryProgramMapDS;
    }

    public void setCategoryProgramMapDS(ICategoryProgramMapDS categoryProgramMapDS)
    {
        this.categoryProgramMapDS = categoryProgramMapDS;
    }

    /** @return IPictureDS */
    public IPictureDS getPictureDS()
    {
        return pictureDS;
    }

    /**
     * @param pictureDS
     *            IPictureDS
     */
    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    /** @return ICategoryPictureMapDS */
    public ICategoryPictureMapDS getCategoryPictureMapDS()
    {
        return categoryPictureMapDS;
    }

    /**
     * @param categoryPictureMapDS
     *            ICategoryPictureMapDS
     */
    public void setCategoryPictureMapDS(ICategoryPictureMapDS categoryPictureMapDS)
    {
        this.categoryPictureMapDS = categoryPictureMapDS;
    }

    /** @return ITargetsystemDS */
    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    /**
     * @param targetSystemDS
     *            ITargetsystemDS
     */
    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    /** @return IUsysConfigDS */
    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    /**
     * @param usysConfigDS
     *            IUsysConfigDS
     */
    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    /** @return ICntSyncRecordDS */
    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    /**
     * @param cntSyncRecordDS
     *            ICntSyncRecordDS
     */
    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    /** @return ICntSyncTaskDS */
    public ICntSyncTaskDS getCntSyncTaskDS()
    {
        return cntSyncTaskDS;
    }

    /**
     * @param cntSyncTaskDS
     *            ICntSyncTaskDS
     */
    public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
    {
        this.cntSyncTaskDS = cntSyncTaskDS;
    }

    /** 按条件显示树 */
    /**
     * @return List<Categorycdn>
     * @param categoryid
     *            String
     */
    public List<Categorycdn> listMenuTreeByParentid(String categoryid)
    {
        log.debug("listMenuTreeByParentid start...");
        List<Categorycdn> listCategorycdn = null;
        listCategorycdn = cmsCategoryDS.listMenuTreeByParentid(categoryid);
        log.debug("listMenuTreeByParentid end");
        return listCategorycdn;
    }

    public List<Categorycdn> listMenuTreeByParentidcategoryAll(String categoryid)
    {
        log.debug("listMenuTreeByParentid start...");
        List<Categorycdn> listCategorycdn = null;
        listCategorycdn = cmsCategoryDS.listMenuTreeByParentidcategoryAll(categoryid);
        log.debug("listMenuTreeByParentid end");
        return listCategorycdn;
    }

    /** 按条件显示树,弹出框 */
    /**
     * @param categoryid
     *            String
     * @return List<Categorycdn>
     */
    public List<Categorycdn> listMenuTreeByParentidPub(String categoryid)
    {
        log.debug("listMenuTreeByParentid start...");
        List<Categorycdn> listCategorycdn = null;
        listCategorycdn = cmsCategoryDS.listMenuTreeByParentidPub(categoryid);
        log.debug("listMenuTreeByParentid end");
        return listCategorycdn;
    }

    /** 按关联关系查的弹出框 */
    public List<Categorycdn> listMenuTreeByParentidPubCk(String parentindex, String type, String obindex)
    {
        log.debug("listMenuTreeByParentid start...");
        List<Categorycdn> listCategorycdn = null;
        listCategorycdn = cmsCategoryDS.listMenuTreeByParentidPub(parentindex, type, obindex);
        log.debug("listMenuTreeByParentid end");
        return listCategorycdn;
    }

    /** 按照id查询cmsprogramtype信息实际是按index */
    /**
     * @param id
     *            List<Categorycdn>
     * @return Categorycdn
     */
    public Categorycdn getCmsCategoryById(Long id)
    {
        log.debug("getCmsCategoryById start...");
        Categorycdn categorycdn = null;
        try
        {
            categorycdn = cmsCategoryDS.getCmsCategoryById(id);
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            log.error("getCmsCategoryById error");
        }
        log.debug("getCmsCategoryById end");
        return categorycdn;
    }

    /** 列表 */
    /**
     * @param categorycdn
     *            Categorycdn
     * @param start
     *            int
     * @param pageSize
     *            int
     * @return TableDataInfo
     */
    public TableDataInfo queryCmsCategory(Categorycdn categorycdn, int start, int pageSize)
    {
        log.debug("queryCmsCategory start...");
        TableDataInfo dataInfo = null;
        if (categorycdn.getStartcreatetime() != null && !categorycdn.getStartcreatetime().equals(""))
        {
            categorycdn.setStartcreatetime(DateUtil2.get14Time(categorycdn.getStartcreatetime()));
        }
        if (categorycdn.getEndcreatetime() != null && !categorycdn.getEndcreatetime().equals(""))
        {
            categorycdn.setEndcreatetime(DateUtil2.get14Time(categorycdn.getEndcreatetime()));
        }
        try
        {
            dataInfo = cmsCategoryDS.queryCmsCategory(categorycdn, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsCategory error");
        }
        log.debug("queryCmsCategory end");
        return dataInfo;
    }

    public TableDataInfo queryCtgPltSync(CSCntPlatformSync cntPlatformSync, int start, int pageSize)
    {
        log.debug("queryCmsCategory start...");
        TableDataInfo dataInfo = null;
        try
        {
            // 格式化内容带有时间的字段

            cntPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeStartDate()));
            cntPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(cntPlatformSync.getOnlinetimeEndDate()));
            cntPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(cntPlatformSync.getCntofflinestarttime()));
            cntPlatformSync.setCntofflineendtime(DateUtil2.get14Time(cntPlatformSync.getCntofflineendtime()));
            dataInfo = cscntPlatformSyncDS.pageInfoQuery(cntPlatformSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsCategory error");
        }
        log.debug("queryCmsCategory end");
        return dataInfo;
    }

    public TableDataInfo queryCtgTarSync(CSCntTargetSync CScntTargetSync, int start, int pageSize)
    {
        log.debug("queryCtgTarSync start...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = CScntTargetSyncDS.pageInfoQuery(CScntTargetSync, start, pageSize);

        }
        catch (DomainServiceException e)
        {
            log.error("queryCtgTarSync error");
        }
        log.debug("queryCtgTarSync end");
        return dataInfo;
    }

    /** 栏目信息删除 */
    /**
     * @param id
     *            Long
     * @return HashMap
     */
    public HashMap deleteCategory(Long id)
    {
        log.debug("deleteCategory start...");

        HashMap result = new HashMap();
        String msg = "";
        Categorycdn categorycdn = getCmsCategoryById(id);
        result.put("category", categorycdn);
        // 如果存在海报，则不能删除，需要先清除海报
        CategoryPictureMap map = new CategoryPictureMap();

        map.setCategoryindex(id);
        String resultCode = "";
        List<CategoryPictureMap> list = new ArrayList();
        ArrayList maplist = new ArrayList();
        List<Picture> piclist = new ArrayList();
        try
        {

            list = categoryPictureMapDS.getCategoryPictureMapByCond(map);
            for (int i = 0; i < list.size(); i++)
            {
                map = list.get(0);
                Picture picture = new Picture();
                picture.setPictureid(map.getPictureid());
                piclist.add(picture);
            }
            // if (list != null && list.size() > 0) {
            // msg = "1:请删除此栏目下的海报";
            // result.put("msg", msg);
            // return result;
            // }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (categorycdn != null)
        {

            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setObjecttype(2);
            cntTargetSync.setObjectindex(categorycdn.getCategoryindex());

            CntPlatformSync cntplatform = new CntPlatformSync();
            cntplatform.setObjecttype(2);
            cntplatform.setObjectindex(categorycdn.getCategoryindex());

            List tarlist = new ArrayList();
            try
            {
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            List pltlist = new ArrayList();
            try
            {
                pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntplatform);
            }
            catch (DomainServiceException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            if (tarlist != null && tarlist.size() > 0)
            {
                for (int i = 0; i < tarlist.size(); i++)
                {
                    cntTargetSync = (CntTargetSync) tarlist.get(i);
                    int status = cntTargetSync.getStatus();
                    // 发布中，不允许修改
                    if (status == 200 || status == 300 || status == 500)
                    {
                        result.put("msg", "1:栏目已发布不能删除");
                        return result;
                    }
                }
            }

            try
            {
                // 删除与频道的mapping
                CategoryChannelMap catchamap = new CategoryChannelMap();
                catchamap.setCategoryindex(id);
                List<CategoryChannelMap> channelcategorylist = new ArrayList();
                channelcategorylist = categoryChannelMapDS.getCategoryChannelMapByCond2(catchamap);
                if (channelcategorylist != null && channelcategorylist.size() > 0)
                {

                    for (CategoryChannelMap cctgMap : channelcategorylist)
                    {
                        // 删除关联内容与平台关联数据
                        CntPlatformSync platform = new CntPlatformSync();
                        platform.setObjecttype(28);
                        platform.setObjectindex(cctgMap.getMapindex());

                        CntTargetSync target = new CntTargetSync();
                        target.setObjecttype(28);
                        target.setObjectindex(cctgMap.getMapindex());
                        List<CntTargetSync> pctgMapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> pctgMapPlatformList = cntPlatformSyncDS
                                .getCntPlatformSyncByCond(platform);
                        if (pctgMapTargetList != null && pctgMapTargetList.size() > 0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(pctgMapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(pctgMapTargetList);
                        }
                    }

                    categoryChannelMapDS.removeCategoryChannelMapList(channelcategorylist);
                }

                // 删除与连续剧的mapping
                CategorySeriesMap catSermap = new CategorySeriesMap();
                catSermap.setCategoryindex(id);
                List<CategorySeriesMap> seriescategorylist = new ArrayList();
                seriescategorylist = categorySeriesMapDS.getCategorySeriesMapByCond(catSermap);
                if (seriescategorylist != null && seriescategorylist.size() > 0)
                {

                    for (CategorySeriesMap sctgMap : seriescategorylist)
                    {
                        // 删除关联内容与平台关联数据
                        CntPlatformSync platform = new CntPlatformSync();
                        platform.setObjecttype(27);
                        platform.setObjectindex(sctgMap.getMapindex());

                        CntTargetSync target = new CntTargetSync();
                        target.setObjecttype(27);
                        target.setObjectindex(sctgMap.getMapindex());
                        List<CntTargetSync> pctgMapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> pctgMapPlatformList = cntPlatformSyncDS
                                .getCntPlatformSyncByCond(platform);
                        if (pctgMapTargetList != null && pctgMapTargetList.size() > 0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(pctgMapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(pctgMapTargetList);
                        }
                    }

                    categorySeriesMapDS.removeCategorySeriesMapList(seriescategorylist);
                }

                // 删除与内容的mapping
                CategoryProgramMap catPromap = new CategoryProgramMap();
                catPromap.setCategoryindex(id);
                List<CategoryProgramMap> procategorylist = new ArrayList();
                procategorylist = categoryProgramMapDS.getCategoryProgramMapByCond(catPromap);
                if (procategorylist != null && procategorylist.size() > 0)
                {

                    for (CategoryProgramMap pctgMap : procategorylist)
                    {
                        // 删除关联内容与平台关联数据
                        CntPlatformSync platform = new CntPlatformSync();
                        platform.setObjecttype(26);
                        platform.setObjectindex(pctgMap.getMapindex());

                        CntTargetSync target = new CntTargetSync();
                        target.setObjecttype(26);
                        target.setObjectindex(pctgMap.getMapindex());
                        List<CntTargetSync> pctgMapTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                        List<CntPlatformSync> pctgMapPlatformList = cntPlatformSyncDS
                                .getCntPlatformSyncByCond(platform);
                        if (pctgMapTargetList != null && pctgMapTargetList.size() > 0)
                        {
                            cntPlatformSyncDS.removeCntPlatformSynListByObjindex(pctgMapPlatformList);
                            cntTargetSyncDS.removeCntTargetSynListByObjindex(pctgMapTargetList);
                        }
                    }

                    categoryProgramMapDS.removeCategoryProgramMapList(procategorylist);
                }

                cmsCategoryDS.removeCategorycdn(categorycdn);
                if (tarlist != null && tarlist.size() > 0)
                {
                    cntTargetSyncDS.removeCntTargetSyncList(tarlist);
                }
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSyncDS.removeCntPlatformSyncList(pltlist);
                }
                if (list != null && list.size() > 0)
                {
                    pictureDS.removePictureList(piclist);// 删除海报
                    categoryPictureMapDS.removeCategoryPictureMapList(list);
                }

            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                result.put("msg", "1:删除失败");
                return result;

            }
            CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                    CategoryConstant.OPERTYPE_CATEGORY_DELETE, CategoryConstant.OPERTYPE_CATEGORY_DELETE_INFO,
                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            result.put("msg", "0:删除成功");
        }
        else
        {
            CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                    CategoryConstant.OPERTYPE_CATEGORY_DELETE, CategoryConstant.OPERTYPE_CATEGORY_DELETE_INFO,
                    CategoryConstant.RESOURCE_OPERATION_FAIL);
            result.put("msg", "1:此栏目已不存在");
            return result;
        }
        log.debug("deleteCategory end");

        return result;
    }

    // 删除网元的发布数据，删除网元和平台信息,在网元全部删除后删除平台
    public HashMap deletecntsync(long syncindex)
    {
        log.debug("deleteCategory start...");

        HashMap result = new HashMap();
        String msg = "";
        CntTargetSync sync = new CntTargetSync();
        sync.setSyncindex(syncindex);
        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(sync);
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (list != null && list.size() > 0)
        {

            for (int i = 0; i < list.size(); i++)
            {
                sync = (CntTargetSync) list.get(i);
                int status = sync.getStatus();
                // 发布中，不允许修改
                if (status == 200 || status == 300 || status == 500)
                {
                    result.put("msg", "1:在该状态下不允许删除");
                    return result;
                }
            }

            try
            {
                CntTargetSync cntsync = new CntTargetSync();
                cntsync.setRelateindex(sync.getRelateindex());// 得到相关网元

                CntPlatformSync platsync = new CntPlatformSync();
                platsync.setSyncindex(cntsync.getRelateindex());// 得到平台
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(platsync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    platsync = (CntPlatformSync) pltlist.get(0);
                }
                long targetindex = sync.getTargetindex();// 频道关联的网元index
                // 如果海报也关联到这个网元，删除这个海报与频道的mapping的发布数据
                CntTargetSync picsync = new CntTargetSync();
                picsync.setTargetindex(targetindex);
                picsync.setObjecttype(31);// 海报
                picsync.setElementid(sync.getObjectid());// 得到频道id
                List picmaplist = cntTargetSyncDS.getCntTargetSyncByCond(picsync);
                if (picmaplist != null && picmaplist.size() > 0)
                {
                    cntTargetSyncDS.removeCntTargetSyncList(picmaplist);// 删除海报频道的mapping发布数据
                    // 删除平台相应发布数据
                    for (int num = 0; num < picmaplist.size(); num++)
                    {

                        CntPlatformSync platpicsync = new CntPlatformSync();

                        platpicsync.setSyncindex(((CntTargetSync) picmaplist.get(num)).getRelateindex());// 得到海报mapping平台
                        List pltpiclist = cntPlatformSyncDS.getCntPlatformSyncByCond(platpicsync);
                        if (pltpiclist != null && pltpiclist.size() > 0)
                        {
                            platpicsync = (CntPlatformSync) pltpiclist.get(0);
                        }
                        CntTargetSync picsyncall = new CntTargetSync();
                        picsyncall.setRelateindex(((CntTargetSync) picmaplist.get(num)).getRelateindex());

                        List allpiclist = cntTargetSyncDS.getCntTargetSyncByCond(picsyncall);
                        if (allpiclist != null && allpiclist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[allpiclist.size()];
                            for (int count = 0; count < allpiclist.size(); count++)
                            {
                                targetSyncStatus[count] = ((CntTargetSync) (allpiclist.get(count))).getStatus();
                            }
                            int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                            int originalstatus = platpicsync.getStatus();// 平台的原始状态
                            if (originalstatus != statusfinal)
                            {
                                platpicsync.setStatus(statusfinal);
                                cntPlatformSyncDS.updateCntPlatformSync(platpicsync);
                            }
                        }
                        else
                        {

                            cntPlatformSyncDS.removeCntPlatformSync(platpicsync);// 删除平台海报

                        }
                    }
                }
                cntTargetSyncDS.removeCntTargetSyncList(list);// 删除网元关联数据,其实就一条数据

                // 获得有共同平台的所有targetlist
                List alllist = cntTargetSyncDS.getCntTargetSyncByCond(cntsync);
                if (alllist != null && alllist.size() > 0)
                {
                    int[] targetSyncStatus = new int[alllist.size()];
                    for (int count = 0; count < alllist.size(); count++)
                    {
                        targetSyncStatus[count] = ((CntTargetSync) (alllist.get(count))).getStatus();
                    }
                    int statusfinal = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
                    int originalstatus = platsync.getStatus();// 平台的原始状态
                    if (originalstatus != statusfinal)
                    {
                        platsync.setStatus(statusfinal);
                        cntPlatformSyncDS.updateCntPlatformSync(platsync);
                    }
                }
                else
                {

                    cntPlatformSyncDS.removeCntPlatformSync(platsync);// 删除平台

                }

            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                result.put("msg", "1:删除失败");
                return result;

            }
            CommonLogUtil.insertOperatorLog(sync.getObjectid(), CategoryConstant.MGTTYPE_CATEGORY,
                    "log.target.category.bind.delete", "log.target.category.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            result.put("msg", "0:删除成功");
        }
        else
        {
            CommonLogUtil.insertOperatorLog(sync.getObjectid().toString(), CategoryConstant.MGTTYPE_CATEGORY,
                    "log.target.category.bind.delete", "log.target.category.bind.delete.info",
                    CategoryConstant.RESOURCE_OPERATION_FAIL);
            result.put("msg", "1:此条数据已被删除");
            return result;
        }
        log.debug("deleteCategory end");

        return result;
    }

    // 查询节点下是否有子节点
    /**
     * @param id
     *            Long
     * @return List<Categorycdn>
     */
    public List<Categorycdn> queryChildCmsCategory(Long id)
    {
        log.debug("queryChildCmsProgramtype start...");
        List<Categorycdn> list = null;
        list = (List<Categorycdn>) cmsCategoryDS.queryChildCmsCategory(id);
        log.debug("queryChildCmsProgramtype end");
        return list;
    }

    /**
     * 栏目信息修改 栏目修改部分代码修改
     * 
     * @param categorycdn
     *            Categorycdn
     * @return Categorycdn
     */
    public Categorycdn updateCategory(Categorycdn categorycdn)
    {
        log.debug("categoryUpdate start...");
        try
        {
            List tarlist = new ArrayList();
            Categorycdn cdn = getCmsCategory(categorycdn.getCategoryid());
            if (categorycdn != null && cdn != null)
            {
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(cdn.getCategoryindex());
                cntTargetSync.setObjecttype(2);
                tarlist = new ArrayList();
                try
                {
                    tarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (tarlist != null && tarlist.size() > 0)
                {
                    for (int i = 0; i < tarlist.size(); i++)
                    {
                        cntTargetSync = (CntTargetSync) tarlist.get(i);
                        int status = cntTargetSync.getStatus();
                        // 发布中，不允许修改
                        if (status == 200)
                        {
                            String msg = "1:发布中，不允许修改";
                            return null;
                        }
                    }
                }
            }

            if (categorycdn != null && cdn != null)
            {
                try
                {
                    categorycdn.setSequence(cdn.getSequence());
                    categorycdn.setCatagorycode(cdn.getCatagorycode());// 此处注意不能把code改为id
                    // categorycdn.setActivestatus(cdn.getActivestatus());
                    categorycdn.setCreatetime(cdn.getCreatetime());

                    List<CntTargetSync> publist = new ArrayList();
                    if (tarlist != null && tarlist.size() > 0)
                    {
                        for (int i = 0; i < tarlist.size(); i++)
                        {
                            CntTargetSync cntTargetSync = (CntTargetSync) tarlist.get(i);
                            int status = cntTargetSync.getStatus();
                            if (status == 300 || status == 500)
                            {
                                publist.add(cntTargetSync);
                            }

                        }
                    }
                    cmsCategoryDS.updateCategorycdn(categorycdn);
                    try
                    {
                        if (publist != null && publist.size() > 0)
                        {
                            publishCategoryTar(publist);
                        }
                    }
                    catch (Exception e1)
                    {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                        CategoryConstant.OPERTYPE_CATEGORY_UPDATE, CategoryConstant.OPERTYPE_CATEGORY_UPDATE_INFO,
                        CategoryConstant.RESOURCE_OPERATION_SUCCESS);

            }
            else
            {
                CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                        CategoryConstant.OPERTYPE_CATEGORY_UPDATE, CategoryConstant.OPERTYPE_CATEGORY_UPDATE_INFO,
                        CategoryConstant.RESOURCE_OPERATION_FAIL);
                return null;
            }

        }
        catch (RuntimeException e)
        {
            log.error(e);
            return null;
        }
        log.debug("categoryUpdate end");
        return categorycdn;
    }

    /** 产品分类信息添加 */
    /**
     * @param categorycdn
     *            Categorycdn
     * @return HashMap
     */
    public HashMap insertCategory(Categorycdn categorycdn)
    {
        HashMap map = new HashMap();
        log.debug("categoryInsert starting...");
        // categorycdn.setClslevel(categorycdn.getClslevel() + 1);

        // 考虑到并发的出现，添加前要判断父节点是否存在

        // 判断重名,说明重复

        Categorycdn categoryCdn = getCmsCategory(categorycdn.getParentid());
        Categorycdn cdn;
        try
        {

            if (categoryCdn != null)
            {
                cdn = cmsCategoryDS.categoryInsert(categorycdn);
                map.put("msg", "0:新增成功");

                // 写日志

                CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                        CategoryConstant.OPERTYPE_CATEGORY_ADD, CategoryConstant.OPERTYPE_CATEGORY_ADD_INFO,
                        CategoryConstant.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                CommonLogUtil.insertOperatorLog(categorycdn.getCategoryid(), CategoryConstant.MGTTYPE_CATEGORY,
                        CategoryConstant.OPERTYPE_CATEGORY_ADD, CategoryConstant.OPERTYPE_CATEGORY_ADD_INFO,
                        CategoryConstant.RESOURCE_OPERATION_FAIL);
                map.put("msg", "1:父节点已不存在");
                return map;

            }
        }
        catch (RuntimeException e)
        {
            log.error(e);
            map.put("msg", "1:操作失败");
            return map;
        }
        map.put("category", cdn);
        log.debug("categoryInsert end");
        return map;
    }

    /** 按照typeid查询 用于添加时判断是否有相同的typeid */
    /**
     * @param categorycdn
     *            Categorycdn
     * @return Integer
     */
    public Integer queryByCategoryid(Categorycdn categorycdn)
    {
        log.debug("deleteProgramtype start...");
        Integer isExists = 0;
        isExists = cmsCategoryDS.queryByCategoryid(categorycdn);
        log.debug("deleteProgramtype end");
        return isExists;
    }

    /** 按照typeid和typename进行查询 用于修改和添加时判断在同一节点写是否拥有同名的节目分类信息 */
    /**
     * @param categorycdn
     *            Categorycdn
     * @return Integer
     */
    public Integer queryByCategoryname(Categorycdn categorycdn)
    {
        log.debug("queryByTypename start...");
        Integer count = 0;
        count = cmsCategoryDS.queryByCategoryname(categorycdn);
        log.debug("queryByTypename end");
        return count;
    }

    /** 判断同一节点下是否有同名的节目分类信息 */
    /**
     * @param categorycdn
     *            Categorycdn
     * @return Long
     */
    public Long queryCategoryindexByCategoryname(Categorycdn categorycdn)
    {
        log.debug("queryTypeindexByTypename start...");
        Long typeindex = null;
        typeindex = cmsCategoryDS.queryCategoryindexByCategoryname(categorycdn);
        log.debug("queryTypeindexByTypename end");
        return typeindex;
    }

    /** 在节目分类信息（取消）发布时，如果节点下有子节点，要将子节点一起（取消）发布 */
    // public List<CmsProgramtype> listCmsprogramtypeByParentid(
    // CmsProgramtype cmsProgramtype) {
    // log.debug("listCmsprogramtypeByParentid start...");
    // List<CmsProgramtype> listCmsprogramtype = null;
    // listCmsprogramtype = cmsProgramtypeDS
    // .listCmsprogramtypeByParentid(cmsProgramtype);
    // log.debug("listCmsprogramtypeByParentid end");
    // return listCmsprogramtype;
    // }
    /**
     * 按照parentid查询
     * 
     * @param typeid
     *            String
     * @return Categorycdn
     */

    public Categorycdn queryByParentidCategory(String typeid)
    {
        log.debug("queryByParentid start...");
        Categorycdn categorycdn = null;
        categorycdn = cmsCategoryDS.queryByParentid(typeid);
        log.debug("queryByParentid end");
        return categorycdn;
    }

    /**
     * 按照typeid查询，得到cmsprogramtype
     * 
     * @param typeid
     *            String
     * @return Categorycdn
     */
    public Categorycdn getCmsCategory(String typeid)
    {
        log.debug("getCmsProgramtype start...");
        Categorycdn categorycdn = null;
        categorycdn = cmsCategoryDS.getCmsCategory(typeid);
        log.debug("getCmsProgramtype end");
        return categorycdn;
    }

    /** @return Logger */
    public Logger getLog()
    {
        return log;
    }

    /**
     * @param log
     *            Logger
     */
    public void setLog(Logger log)
    {
        this.log = log;
    }

    /** @return ICategorycdnDS */
    public ICategorycdnDS getCmsCategoryDS()
    {
        return cmsCategoryDS;
    }

    /**
     * @param cmsCategoryDS
     *            ICategorycdnDS
     */
    public void setCmsCategoryDS(ICategorycdnDS cmsCategoryDS)
    {
        this.cmsCategoryDS = cmsCategoryDS;
    }

    private String getTempAreAdd() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1"); // 临时区
        return stroageareaPath;
    }

    // 取出同步XML文件FTP地址
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * @param index
     *            String
     * @return String
     */
    public String getIsChildexists(String index)
    {
        Categorycdn cdn = getCmsCategoryById(new Long(index));
        if (cdn != null)
        {
            if (cdn.getIschildexists() != null)
            {
                if (cdn.getIschildexists() == 1)
                {
                    return "success";
                }
                else
                {
                    return null;
                }
            }
        }
        return null;
    }

    // 新的发布方式

    /**
     * 关联发布平台方法，生成平台发布数据
     * 
     * @param plattype
     *            平台类型 1-2.0平台，2-3.0平台,objindex 对象index
     * @return result
     */
    public String preparetopublish(Targetsystem tar, long objindex, String objid, int objtype)
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());// 2.0或3.0平台
        cntPlatformSync.setObjectid(objid);// 对象id
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)// 如果有，应该判断状态是否需要更新
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();

            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTarget(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex);
        return operdesc;
    }

    public String preparetopublishForMap(Targetsystem tar, long objindex, String objid, int objtype, String elementid,
            String parentid)
    {
        String operdesc = null;
        CntPlatformSync cntPlatformSync = new CntPlatformSync();
        cntPlatformSync.setObjecttype(objtype);// 栏目
        cntPlatformSync.setObjectindex(objindex);
        cntPlatformSync.setPlatform(tar.getPlatform());
        cntPlatformSync.setObjectid(objid);
        cntPlatformSync.setElementid(elementid);
        cntPlatformSync.setParentid(parentid);
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        try
        {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            }
            else
            {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 200-发布中 300-发布成功
                // 400-发布失败

                // 500-修改发布失败 600-预下线
                pltsyncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try
                {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                }
                catch (DomainServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        operdesc = publishToTargetForMap(tar.getTargetindex(), objindex, objid, objtype, pltsyncindex, elementid,
                parentid);
        return operdesc;
    }

    public String publishToTargetForMap(Long targetindex, long objindex, String objid, int objtype, Long relateindex,
            String elementid, String parentid)
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);
        cntTargetSync.setParentid(parentid);
        cntTargetSync.setElementid(elementid);

        List list = new ArrayList();
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
                CommonLogUtil.insertOperatorLog(objid + ", " + targetindex, "log.category.manage",
                        "log.target.category.bind.add", "log.target.category.bind.add.info",
                        CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    /**
     * 关联发布网元方法，生成网元发布数据
     * 
     * @param targetindex
     *            网元index
     * @return result
     */
    public String publishToTarget(Long targetindex, long objindex, String objid, int objtype, Long relateindex)
    {
        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;
        CntTargetSync cntTargetSync = new CntTargetSync();

        cntTargetSync.setObjecttype(objtype);// 对象类型：1-Service，2-Category，3-Program，4-Series......
        cntTargetSync.setObjectindex(objindex);
        cntTargetSync.setTargetindex(targetindex);
        cntTargetSync.setObjectid(objid);

        Targetsystem target = new Targetsystem();
        target.setTargetindex(targetindex);
        List tarlist;
        try
        {
            tarlist = targetSystemDS.getTargetsystemByCond(target);
            if (tarlist != null && tarlist.size() > 0)
            {
                target = (Targetsystem) tarlist.get(0);
            }
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        List list = new ArrayList();
        try
        {

            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + targetindex + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_HAS_BINDED)
                        + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);// 操作结果：0-待发布 10-新增同步中 20-新增同步成功
                cntTargetSync.setRelateindex(relateindex);

                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(relateindex);
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);
                }
                if (cntPlatformSync.getStatus() != 0)
                {
                    cntPlatformSync.setStatus(0);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }

                CommonLogUtil.insertOperatorLog(objid + ", " + target.getTargetid(), "log.category.manage",
                        "log.category.bindtarget", "log.category.bindtarget.info",
                        CategoryConstants.RESOURCE_OPERATION_SUCCESS);
                resultStr.append(ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_TARGETSYSTEM)
                        + objid + ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL)
                        + "  ");// 目标系统名称
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }

    /**
     * 发布方法,写日志
     * 
     * @param 发布对象任务编号
     * @throws DomainServiceException
     */
    public String toRecordObj(Long taskindex, Long objindex, Long destindex, String objid, String elementid,
            int objtype, int actiontype, String categorycode) throws DomainServiceException
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
        objectSyncRecord.setTaskindex(taskindex);
        objectSyncRecord.setObjectindex(objindex);
        objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
        objectSyncRecord.setObjecttype(objtype);// 对象类型编号：1-service，2-Category，3-Program
        if (!elementid.equals(""))
        {
            objectSyncRecord.setElementid(elementid);
            objectSyncRecord.setParentid(objid);// mapping的做法
            objectSyncRecord.setObjectid(objid);
        }
        else
        {
            objectSyncRecord.setObjectid(objid);
        }
        objectSyncRecord.setDestindex(destindex);
        Categorycdn category = new Categorycdn();
        category.setCategoryindex(objindex);
        List list = cmsCategoryDS.selectByCategoryindex(category);
        if (list != null && list.size() > 0)
        {
            category = (Categorycdn) list.get(0);
        }
        objectSyncRecord.setObjectcode(category.getCatagorycode());
        try
        {
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 插入同步任务
     * 
     * @param targindex
     *            网元index,contentmngxmlurl 内容相关对象的XML文件URL
     * @return result
     * @throws DomainServiceException
     */
    public String insertSyncTask(Long targetindex, String contentmngxmlurl, Long objindex, String objectid,
            int objtype, int actiontype, String categorycode, Long batchid, int status) throws DomainServiceException
    {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");

        Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setBatchid(batchid);
        cntsyncTask.setStatus(status);// 任务状态：0-初始 1-待执行 2-执行中 3-成功结束 4-失败结束
        cntsyncTask.setPriority(5);// 任务优先级：1-高 2-中 3-低
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(contentmngxmlurl);
        cntsyncTask.setDestindex(targetindex);
        cntsyncTask.setSource(1);
        cntsyncTask.setTaskindex(taskindex);
        cntsyncTask.setStarttime(dateformat.format(new Date()));
        cntsyncTask.setRetrytimes(3);
        try
        {
            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        toRecordObj(taskindex, objindex, targetindex, objectid, "", objtype, actiontype, categorycode);// 记录到发布日志表,2是栏目
        // 栏目
        // if (objtype == 2) {
        // CategoryPictureMap map = new CategoryPictureMap();
        // map.setCategoryindex(objindex);
        // List list = new ArrayList();
        //
        // list = categoryPictureMapDS.getCategoryPictureMapByCond(map);
        //
        // if (list != null && list.size() > 0) {
        // for (int k = 0; k < list.size(); k++) {
        // CategoryPictureMap cpmap = (CategoryPictureMap) list.get(k);
        // toRecordObj(taskindex, cpmap.getMapindex(), targetindex,
        // cpmap.getPictureid(), cpmap.getCategoryid(),
        // objtype, actiontype, "");// 记录到发布日志表,2是栏目
        // Long pindex = cpmap.getPictureindex();
        // Picture p = new Picture();
        // p.setPictureindex(pindex);
        // p = pictureDS.getPicture(p);
        // if (p != null) {
        // toRecordObj(taskindex, p.getPictureindex(),
        // targetindex, p.getPictureid(), "", objtype,
        // actiontype, p.getPicturecode());// 记录到发布日志表,2是栏目
        // }
        //
        // }
        // }
        // }
        return null;
    }

    
    
    // 平台发布方法,有用,只能发布到关联了的网元,针对一个栏目，一次只能发布一个平台
    public HashMap publishCategoryPlt(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        HashMap map = new HashMap();
        int actiontype = 0;
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int index = 1;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        Categorycdn cdn = getCmsCategory(cntPlatformSynclist.get(0).getObjectid());// 栏目
        Categorycdn parentcdn = getCmsCategory(cdn.getParentid());// 父栏目
        Map<String, List> objMap = new HashMap();
        ArrayList list = new ArrayList();
        cdn.setParentcode(parentcdn.getCatagorycode());// 文广code
        list.add(cdn);
        objMap.put("categoryList", list);

        if (cntPlatformSynclist != null && cntPlatformSynclist.size() > 0)
        {
            boolean flagtotal = false;
            String msg = "";
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {

                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                if (cntPlatformSync.getStatus() == 200 || cntPlatformSync.getStatus() == 300)
                {// 发布中和发布成功

                    map.put("msg", "1:平台发布数据状态不正确");
                    continue;
                }
                else
                {

                    CntTargetSync cntTargetSync = new CntTargetSync();
                    cntTargetSync.setObjectindex(cdn.getCategoryindex());
                    cntTargetSync.setRelateindex(cntPlatformSync.getSyncindex());
                    List cnttarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);// 得到所有网元

                    if (cnttarlist != null && cnttarlist.size() > 0)
                    {
                       
                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                .toString());
                        for (int count = 0; count < cnttarlist.size(); count++)
                        {
                            cntTargetSync = (CntTargetSync) cnttarlist.get(count);
                            List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                            if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                            {

                            }
                            else
                            {
                                msg = msg + " 发布数据不存在";

                                continue;
                            }
                            int statusbegin = cntTargetSync.getStatus();
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getStatus() != 0)
                            {
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                continue;
                            }

                            if (!parentcdn.getParentid().equals("0"))
                            {

                                CntTargetSync tarsync = new CntTargetSync();
                                tarsync.setObjectid(parentcdn.getCategoryid());
                                tarsync.setTargetindex(cntTargetSync.getTargetindex());
                                List parlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                                if (parlist != null && parlist.size() > 0)
                                {
                                    tarsync = (CntTargetSync) parlist.get(0);
                                    if ((tarsync.getStatus() != 300) && (tarsync.getStatus() != 500))
                                    {

                                        msg = msg + " 必须在目标系统 " + targetSystem.getTargetid() + "上对父节点进行新增同步";
                                        continue;
                                    }

                                }
                                else
                                {

                                    msg = msg + " 必须在目标系统 " + targetSystem.getTargetid() + "上对父节点进行新增同步";
                                    continue;
                                }
                            }
                            if (cntTargetSync.getStatus() == 0 || cntTargetSync.getStatus() == 400

                            || cntTargetSync.getStatus() == 500)
                            {// 0待发布，400发布失败(新增)，500修改发布失败

                                if (cntTargetSync.getStatus() == 0 || cntTargetSync.getStatus() == 400)
                                {// 网元发布数据
                                    actiontype = 1;
                                    cntTargetSync.setOperresult(10);// 新增同步中

                                }
                                else
                                {
                                    actiontype = 2;// update
                                    cntTargetSync.setOperresult(40);// 修改同步中
                                }
                                String xmladdress = "";
                                if (cntPlatformSync.getPlatform() == 2)
                                {// 3.0平台,1个平台生成1个xml
                                    xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                            targetSystem.getTargettype(), 2, actiontype);// 文广1是regist2是栏目

                                }
                                else
                                {
                                  //如果是2.0平台就每个网元生成一个不同的batchid
                                    batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                                    xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                            targetSystem.getTargettype(), 2, actiontype);// 文广1是regist2是栏目
                                }
                                int status = 1;
                                // bms和egp的情况且有别的网元的情况,status可为0或1
                                if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                                        && (cnttarlist.size() > 0))
                                {
                                    for (int ss = 0; ss < cnttarlist.size(); ss++)
                                    {
                                        CntTargetSync cs = (CntTargetSync) cnttarlist.get(ss);
                                        if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                                        {
                                            Targetsystem tsnew = new Targetsystem();
                                            tsnew.setTargetindex(cs.getTargetindex());
                                            List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                            if (target != null && target.size() > 0)
                                            {
                                                tsnew = (Targetsystem) target.get(0);
                                            }
                                            if ((tsnew.getTargettype() == 3))
                                            {// cdn
                                                status = 0;
                                            }
                                            else if (tsnew.getTargettype() == 1)
                                            {// bms
                                                status = 0;
                                            }

                                        }
                                    }
                                }
                                insertSyncTask(cntTargetSync.getTargetindex(), xmladdress, cdn.getCategoryindex(), cdn
                                        .getCategoryid(), 2, actiontype, cdn.getCatagorycode(), batchid, status);// 调用同步任务模块DS代码

                                cntTargetSync.setStatus(200);// 200发布中
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSync);
                                flagtotal = true;
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":操作成功";
                                CommonLogUtil.insertOperatorLog(
                                        cdn.getCategoryid() + ", " + targetSystem.getTargetid(), "log.categorysyn.mgt",
                                        CategoryConstant.OPERTYPE_CATEGORY_PUBLISH,
                                        CategoryConstant.OPERTYPE_CATEGORY_PUBLISH_INFO,
                                        CategoryConstant.RESOURCE_OPERATION_SUCCESS);

                            }
                            else
                            {

                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":发布数据状态不正确";
                                continue;
                            }

                        }

                        CntTargetSync finaltarget = new CntTargetSync();
                        finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                        List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                        if (finaltargetlist != null && finaltargetlist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[finaltargetlist.size()];
                            for (int ii = 0; ii < finaltargetlist.size(); ii++)
                            {
                                targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                            }
                            int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                            int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                            if (originalstatus != statussync)
                            {
                                cntPlatformSync.setStatus(statussync);
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }
                }
            }
            if (flagtotal == true)
            {
                msg = "0:" + msg;
                map.put("msg", msg);

            }
            else
            {
                msg = "1:" + msg;
                map.put("msg", msg);

            }
        }

        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = "失败数量：" + ifail;
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail; // "350098"
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publishCategory end...");
        return map;
    }

    // 平台取消发布方法,有用,只能发布到关联了的网元
    public HashMap cancelPublishCategoryPlt(List<CntPlatformSync> cntPlatformSynclist) throws Exception
    {
        log.debug("cancelpublishCategoryPlt start...");
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int actiontype = 0;
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int index = 1;
        Categorycdn cdn = getCmsCategory(cntPlatformSynclist.get(0).getObjectid());// 栏目
        Categorycdn cdntemp = new Categorycdn();
        cdntemp.setParentid(cdn.getCategoryid());
        List childctglist = cmsCategoryDS.selectByCategoryindex(cdntemp);

        if (cntPlatformSynclist != null && cntPlatformSynclist.size() > 0)
        {
            String msg = "";
            boolean flagbool = false;
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {

                Map<String, List> objMap = new HashMap();
                ArrayList list = new ArrayList();
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                if (cntPlatformSync.getStatus() == 300 || cntPlatformSync.getStatus() == 500)
                {// 300发布成功，500修改发布失败

                    long objindex = cntPlatformSync.getObjectindex();
                    String objid = cntPlatformSync.getObjectid();// 栏目id

                    CntTargetSync sync = new CntTargetSync();
                    sync.setRelateindex(cntPlatformSync.getSyncindex());
                    List targetsynclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                    int pritargettype = -1;
                    if (targetsynclist != null && targetsynclist.size() > 0)
                    {

                        for (int count = 0; count < targetsynclist.size(); count++)
                        {
                            // 找出优先级最高的网元

                            CntTargetSync cntTargetSync = (CntTargetSync) targetsynclist.get(count);
                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getTargettype() == 2)
                            {
                                pritargettype = 2;
                            }
                            else if ((targetSystem.getTargettype() == 1) && (pritargettype != 2))
                            {
                                pritargettype = 1;
                            }
                            else if ((targetSystem.getTargettype() != 1) && (pritargettype != 2)
                                    && (pritargettype != 1))
                            {
                                pritargettype = targetSystem.getTargettype();
                            }
                        }

                        boolean priflagbool = false;

                        for (int sed = 0; sed < targetsynclist.size(); sed++)
                        {
                            boolean flagele = false;
                            sync = (CntTargetSync) targetsynclist.get(sed);

                            int statussync = sync.getStatus();

                            long targetindex = sync.getTargetindex();

                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(sync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getTargettype() == pritargettype)
                            {

                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                    continue;
                                }
                                CntTargetSync sync2 = new CntTargetSync();
                                sync2.setTargetindex(targetindex);
                                sync2.setParentid(objid);
                                sync2.setObjecttype(28);// 频道关联栏目

                                List sync2list = cntTargetSyncDS.getCntTargetSyncByCond(sync2);
                                if (sync2list != null && sync2list.size() > 0)
                                {
                                    for (int num = 0; num < sync2list.size(); num++)
                                    {
                                        sync2 = (CntTargetSync) sync2list.get(num);
                                        int status = sync2.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与频道的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与频道关联关系已发布";
                                            break;
                                        }
                                    }

                                }
                                CntTargetSync syncparent = new CntTargetSync();
                                syncparent.setTargetindex(targetindex);
                                syncparent.setParentid(objid);
                                syncparent.setObjecttype(27);// 连续剧关联栏目
                                List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                if (syncparentlist != null && syncparentlist.size() > 0)
                                {
                                    for (int num = 0; num < syncparentlist.size(); num++)
                                    {
                                        syncparent = (CntTargetSync) syncparentlist.get(num);
                                        int status = syncparent.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与连续剧的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与连续剧关联关系已发布";
                                            break;
                                        }
                                    }

                                }

                                CntTargetSync syncprog = new CntTargetSync();
                                syncprog.setTargetindex(targetindex);
                                syncprog.setParentid(objid);
                                syncprog.setObjecttype(26);// 内容关联栏目
                                List syncproglist = cntTargetSyncDS.getCntTargetSyncByCond(syncprog);
                                if (syncproglist != null && syncproglist.size() > 0)
                                {
                                    for (int num = 0; num < syncproglist.size(); num++)
                                    {
                                        syncprog = (CntTargetSync) syncproglist.get(num);
                                        int status = syncprog.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与内容的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与内容关联关系已发布";
                                            break;
                                        }
                                    }

                                }

                                CntTargetSync syncpic = new CntTargetSync();
                                syncpic.setTargetindex(targetindex);
                                syncpic.setElementid(objid);
                                syncpic.setObjecttype(31);// 海报关联栏目
                                List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                if (syncpiclist != null && syncpiclist.size() > 0)
                                {
                                    for (int num = 0; num < syncpiclist.size(); num++)
                                    {
                                        syncpic = (CntTargetSync) syncpiclist.get(num);
                                        int status = syncpic.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与海报的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与海报关联关系已发布";
                                            break;
                                        }
                                    }

                                }

                                if (flagele == true)
                                {
                                    continue;
                                }

                                Categorycdn categoryCdn = new Categorycdn();
                                categoryCdn.setCategoryindex(objindex);
                                List ctglist = cmsCategoryDS.selectByCategoryindex(categoryCdn);
                                if (ctglist != null && ctglist.size() > 0)
                                {
                                    categoryCdn = (Categorycdn) ctglist.get(0);
                                    Categorycdn parentcdn = new Categorycdn();
                                    parentcdn.setCategoryid(categoryCdn.getParentid());
                                    List parentlist = cmsCategoryDS.getCategorycdnByCond(parentcdn);
                                    if (parentlist != null && parentlist.size() > 0)
                                    {
                                        parentcdn = (Categorycdn) parentlist.get(0);
                                    }
                                    categoryCdn.setParentcode(parentcdn.getCatagorycode());

                                }
                                else
                                {
                                    ifail++;
                                    operateInfo = new OperateInfo(String.valueOf(index++), categoryCdn.getCategoryid(),
                                            ResourceMgt.findDefaultText(FAIL), ResourceManager
                                                    .getResourceText("msg.category.notexist"));
                                    returnInfo.appendOperateInfo(operateInfo);
                                    continue;
                                }
                                list.add(categoryCdn);
                                objMap.put("categoryList", list);
                                // CntTargetSync cntTargetSync = new CntTargetSync();
                                // cntTargetSync.setObjectindex(objindex);
                                // cntTargetSync.setRelateindex(cntPlatformSync.getSyncindex());

                                // List cnttarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                                // if (cnttarlist != null && cnttarlist.size() > 0)
                                // {
                                // for (int count = 0; count < cnttarlist.size(); count++)
                                // {
                                // CntTargetSync cntTargetSync2 = (CntTargetSync) cnttarlist.get(count);
                                int countchild = 0;
                                if (childctglist != null && childctglist.size() > 0)
                                {
                                    for (int k = 0; k < childctglist.size(); k++)
                                    {
                                        CntTargetSync temp = new CntTargetSync();
                                        temp.setObjectid(((Categorycdn) childctglist.get(k)).getCategoryid());
                                        temp.setTargetindex(sync.getTargetindex());
                                        List listtar = cntTargetSyncDS.getCntTargetSyncByCond(temp);
                                        if (listtar != null && listtar.size() > 0)
                                        {
                                            temp = (CntTargetSync) listtar.get(0);
                                            if (temp.getStatus() == 300 || temp.getStatus() == 500
                                                    || temp.getStatus() == 200)
                                            {
                                                countchild++;

                                            }
                                        }
                                    }

                                }
                                if (countchild != 0)
                                {
                                    // ifail++;
                                    // operateInfo = new OperateInfo(String.valueOf(index++),
                                    // categoryCdn.getCategoryid(),
                                    // ResourceMgt.findDefaultText(FAIL), "必须先对子节点删除同步");
                                    // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":必须先对子节点取消发布";
                                    continue;
                                }

                                priflagbool = true;
                            }
                        }

                        // for循环结束
                        if (priflagbool == true)
                        {
                            Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey(
                                    "ucdn_task_batch_id").toString());
                            for (int kk = 0; kk < targetsynclist.size(); kk++)
                            {

                                boolean flagele = false;
                                sync = (CntTargetSync) targetsynclist.get(kk);
                                List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                                if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                                {

                                }
                                else
                                {
                                    msg = msg + " 发布数据不存在";

                                    continue;
                                }
                                int statussync = sync.getStatus();

                                long targetindex = sync.getTargetindex();

                                Targetsystem targetSystem = new Targetsystem();
                                targetSystem.setTargetindex(sync.getTargetindex());
                                List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                                if (targetlist != null && targetlist.size() > 0)
                                {
                                    targetSystem = (Targetsystem) targetlist.get(0);
                                }
                                if (targetSystem.getStatus() != 0)
                                {
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                    continue;
                                }
                                CntTargetSync sync2 = new CntTargetSync();
                                sync2.setTargetindex(targetindex);
                                sync2.setParentid(objid);
                                sync2.setObjecttype(28);// 频道关联栏目

                                List sync2list = cntTargetSyncDS.getCntTargetSyncByCond(sync2);
                                if (sync2list != null && sync2list.size() > 0)
                                {
                                    for (int num = 0; num < sync2list.size(); num++)
                                    {
                                        sync2 = (CntTargetSync) sync2list.get(num);
                                        int status = sync2.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与频道的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与频道关联关系已发布";
                                            continue;
                                        }
                                    }

                                }
                                CntTargetSync syncparent = new CntTargetSync();
                                syncparent.setTargetindex(targetindex);
                                syncparent.setParentid(objid);
                                syncparent.setObjecttype(27);// 连续剧关联栏目
                                List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                                if (syncparentlist != null && syncparentlist.size() > 0)
                                {
                                    for (int num = 0; num < syncparentlist.size(); num++)
                                    {
                                        syncparent = (CntTargetSync) syncparentlist.get(num);
                                        int status = syncparent.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与连续剧的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与连续剧关联关系已发布";
                                            continue;
                                        }
                                    }

                                }

                                CntTargetSync syncprog = new CntTargetSync();
                                syncprog.setTargetindex(targetindex);
                                syncprog.setParentid(objid);
                                syncprog.setObjecttype(26);// 内容关联栏目
                                List syncproglist = cntTargetSyncDS.getCntTargetSyncByCond(syncprog);
                                if (syncproglist != null && syncproglist.size() > 0)
                                {
                                    for (int num = 0; num < syncproglist.size(); num++)
                                    {
                                        syncprog = (CntTargetSync) syncproglist.get(num);
                                        int status = syncprog.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与内容的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与内容关联关系已发布";
                                            continue;
                                        }
                                    }

                                }

                                CntTargetSync syncpic = new CntTargetSync();
                                syncpic.setTargetindex(targetindex);
                                syncpic.setElementid(objid);
                                syncpic.setObjecttype(31);// 海报关联栏目
                                List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                                if (syncpiclist != null && syncpiclist.size() > 0)
                                {
                                    for (int num = 0; num < syncpiclist.size(); num++)
                                    {
                                        syncpic = (CntTargetSync) syncpiclist.get(num);
                                        int status = syncpic.getStatus();
                                        if (status == 300 || status == 500)
                                        {
                                            flagele = true;
                                            // ifail++;
                                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync
                                            // .getSyncindex().toString(), ResourceMgt.findDefaultText(FAIL),
                                            // "此栏目与海报的 mapping关系已发布不能取消同步");
                                            // returnInfo.appendOperateInfo(operateInfo);
                                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":与海报关联关系已发布";
                                            break;
                                        }
                                    }

                                }

                                if (flagele == true)
                                {
                                    continue;
                                }

                                Categorycdn categoryCdn = new Categorycdn();
                                categoryCdn.setCategoryindex(objindex);
                                List ctglist = cmsCategoryDS.selectByCategoryindex(categoryCdn);
                                if (ctglist != null && ctglist.size() > 0)
                                {
                                    categoryCdn = (Categorycdn) ctglist.get(0);
                                    Categorycdn parentcdn = new Categorycdn();
                                    parentcdn.setCategoryid(categoryCdn.getParentid());
                                    List parentlist = cmsCategoryDS.getCategorycdnByCond(parentcdn);
                                    if (parentlist != null && parentlist.size() > 0)
                                    {
                                        parentcdn = (Categorycdn) parentlist.get(0);
                                    }
                                    categoryCdn.setParentcode(parentcdn.getCatagorycode());

                                }
                                else
                                {
                                    ifail++;
                                    operateInfo = new OperateInfo(String.valueOf(index++), categoryCdn.getCategoryid(),
                                            ResourceMgt.findDefaultText(FAIL), ResourceManager
                                                    .getResourceText("msg.category.notexist"));
                                    returnInfo.appendOperateInfo(operateInfo);
                                    continue;
                                }
                                list.add(categoryCdn);
                                objMap.put("categoryList", list);
                                // CntTargetSync cntTargetSync = new CntTargetSync();
                                // cntTargetSync.setObjectindex(objindex);
                                // cntTargetSync.setRelateindex(cntPlatformSync.getSyncindex());

                                // List cnttarlist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                                // if (cnttarlist != null && cnttarlist.size() > 0)
                                // {
                                // for (int count = 0; count < cnttarlist.size(); count++)
                                // {
                                // CntTargetSync cntTargetSync2 = (CntTargetSync) cnttarlist.get(count);
                                int countchild = 0;
                                if (childctglist != null && childctglist.size() > 0)
                                {
                                    for (int k = 0; k < childctglist.size(); k++)
                                    {
                                        CntTargetSync temp = new CntTargetSync();
                                        temp.setObjectid(((Categorycdn) childctglist.get(k)).getCategoryid());
                                        temp.setTargetindex(sync.getTargetindex());
                                        List listtar = cntTargetSyncDS.getCntTargetSyncByCond(temp);
                                        if (listtar != null && listtar.size() > 0)
                                        {
                                            temp = (CntTargetSync) listtar.get(0);
                                            if (temp.getStatus() == 300 || temp.getStatus() == 500
                                                    || temp.getStatus() == 200)
                                            {
                                                countchild++;

                                            }
                                        }
                                    }

                                }
                                if (countchild != 0)
                                {
                                    // ifail++;
                                    // operateInfo = new OperateInfo(String.valueOf(index++),
                                    // categoryCdn.getCategoryid(),
                                    // ResourceMgt.findDefaultText(FAIL), "必须先对子节点删除同步");
                                    // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":必须先对子节点取消发布";
                                    continue;
                                }
                                else if (sync.getStatus() == 300 || sync.getStatus() == 500)
                                {// 300发布成功，500修改发布失败

                                    actiontype = 3;// delete
                                    sync.setOperresult(70);
                                    String xmladdress = "";
                                    if (cntPlatformSync.getPlatform() == 2)
                                    {// 3.0平台,1个平台生成1个xml
                                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                                targetSystem.getTargettype(), 2, actiontype);// 文广1是regist2是栏目

                                    }
                                    else
                                    {
                                      //如果是2.0平台就每个网元生成一个不同的batchid
                                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                                targetSystem.getTargettype(), 2, actiontype);// 文广1是regist2是栏目
                                    }
                                    int pristatus = 1;
                                    // bms和egp的情况且有别的网元的情况,status可为0或1
                                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                                            && (targetsynclist.size() > 0))
                                    {
                                        for (int count = 0; count < targetsynclist.size(); count++)
                                        {
                                            CntTargetSync cs = (CntTargetSync) targetsynclist.get(count);
                                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                                            {
                                                Targetsystem tsnew = new Targetsystem();
                                                tsnew.setTargetindex(cs.getTargetindex());
                                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                                if (target != null && target.size() > 0)
                                                {
                                                    tsnew = (Targetsystem) target.get(0);
                                                }
                                                if ((tsnew.getTargettype() == 2))
                                                {// cdn
                                                    pristatus = 0;
                                                }
                                                else if (tsnew.getTargettype() == 1)
                                                {// bms
                                                    pristatus = 0;
                                                }

                                            }
                                        }
                                    }
                                    insertSyncTask(sync.getTargetindex(), xmladdress, categoryCdn.getCategoryindex(),
                                            categoryCdn.getCategoryid(), 2, actiontype, categoryCdn.getCatagorycode(),
                                            batchid, pristatus);// 调用同步任务模块DS代码

                                    sync.setStatus(200);// 200发布中
                                    cntTargetSyncDS.updateCntTargetSync(sync);

                                    flagbool = true;
                                    msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";
                                    CommonLogUtil.insertOperatorLog(cdn.getCategoryid() + ", "
                                            + targetSystem.getTargetid(), "log.categorysyn.mgt",
                                            "log.category.unpublish", "log.category.unpublish.info",
                                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                                    // iSuccessed++;
                                    // operateInfo = new OperateInfo(String.valueOf(index++),
                                    // sync.getSyncindex().toString(),
                                    // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt
                                    // .findDefaultText(PUBLISH_SUCCESS));
                                    // returnInfo.appendOperateInfo(operateInfo);
                                }
                                // }
                                // }

                            }
                            CntTargetSync finaltarget = new CntTargetSync();
                            finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                            if (finaltargetlist != null && finaltargetlist.size() > 0)
                            {
                                int[] targetSyncStatus = new int[finaltargetlist.size()];
                                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                                {
                                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                                }
                                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                                int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                                if (originalstatus != statussync)
                                {
                                    cntPlatformSync.setStatus(statussync);
                                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                                }
                            }
                        }
                    }

                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            ResourceMgt.findDefaultText(FAIL), "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                if (flagbool == true)
                {
                    msg = "0:" + msg;
                    map.put("msg", msg);
                    // iSuccessed++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex().toString(),
                    // "成功", msg);
                    // returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    msg = "1:" + msg;
                    map.put("msg", msg);
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex().toString(),
                    // "失败", msg);
                    // returnInfo.appendOperateInfo(operateInfo);
                }
            }

        }
        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = "失败数量：" + ifail;
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail; // "350098"
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("cancelpublishCategoryPlt end...");
        return map;
    }

    // 网元发布方法，有用,所有这些网元都属于一个平台
    public HashMap publishCategoryTar(List<CntTargetSync> cntTargetSynclist) throws Exception
    {
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int successCount = 0;
        int totalCategoryindex = 0;
        int failCount = 0;
        int num = 1;
        int index = 1;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        List<Categorycdn> listCmsProgramtype = new ArrayList<Categorycdn>();
        Integer ifail = 0;
        Integer iSuccessed = 0;
        long objindex = 0;
        if (cntTargetSynclist != null && cntTargetSynclist.size() > 0)
        {
            objindex = cntTargetSynclist.get(0).getObjectindex();
        }

        Categorycdn categoryCdn = new Categorycdn();
        categoryCdn.setCategoryindex(objindex);
        categoryCdn = cmsCategoryDS.getCategorycdn(categoryCdn);
        Categorycdn parentcdn = getCmsCategory(categoryCdn.getParentid());// 父栏目
        Map<String, List> objMap = new HashMap();
        ArrayList list = new ArrayList();
        categoryCdn.setParentcode(parentcdn.getCatagorycode());
        list.add(categoryCdn);
        objMap.put("categoryList", list);
        if (cntTargetSynclist != null && cntTargetSynclist.size() > 0)
        {
            Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
            String xmladdress;
            int actiontype = 0;
            for (int i = 0; i < cntTargetSynclist.size(); i++)
            {
                CntTargetSync cntTargetSync = cntTargetSynclist.get(i);
                List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                CntPlatformSync cntPlatformSync = new CntPlatformSync();

                cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (pltlist != null && pltlist.size() > 0)
                {
                    cntPlatformSync = (CntPlatformSync) pltlist.get(0);// 获取平台发布信息
                }
                if (!parentcdn.getParentid().equals("0"))
                {
                    CntTargetSync tarsync = new CntTargetSync();
                    tarsync.setObjectid(parentcdn.getCategoryid());
                    tarsync.setTargetindex(cntTargetSync.getTargetindex());
                    List parlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (parlist != null && parlist.size() > 0)
                    {
                        tarsync = (CntTargetSync) parlist.get(0);
                        if (tarsync.getStatus() != 300 && tarsync.getStatus() != 500)
                        {

                            map.put("msg", "1:必须对父节点进行新增同步");
                            return map;
                        }
                    }
                    else
                    {
                        map.put("msg", "1:必须对父节点进行新增同步");
                        return map;
                    }
                }

                int status = cntTargetSync.getStatus();
                if (status == 0 || status == 400 || status == 300 || status == 500)
                {// 0待发布，400发布失败(新增)，500修改发布失败
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        map.put("msg", "1:目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确");
                        return map;
                    }

                    if (cntTargetSync.getStatus() == 0 || cntTargetSync.getStatus() == 400

                    )
                    {// 网元发布数据
                        actiontype = 1;
                        cntTargetSync.setOperresult(10);// 新增同步中
                    }
                    else
                    {
                        actiontype = 2;// update
                        cntTargetSync.setOperresult(40);// 修改同步中
                    }

                    long targetindex = cntTargetSync.getTargetindex();
                    CntPlatformSync cntPlatformSyncnew = new CntPlatformSync();
                    cntPlatformSyncnew.setSyncindex(cntTargetSync.getRelateindex());
                    List platlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSyncnew);
                    cntPlatformSyncnew = (CntPlatformSync) platlist.get(0);
                    if (cntPlatformSyncnew.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 2, actiontype);// 1是2.0，2是3.0
                    }
                    else
                    {
                        //如果是2.0平台就每个网元生成一个不同的batchid
                        batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 2, actiontype);// 1是2.0，2是3.0

                    }

                    int pristatus = 1;
                    // bms和egp的情况且有别的网元的情况,status可为0或1
                    if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                            && (cntTargetSynclist.size() > 0))
                    {
                        for (int ss = 0; ss < cntTargetSynclist.size(); ss++)
                        {
                            CntTargetSync cs = (CntTargetSync) cntTargetSynclist.get(ss);
                            if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                            {
                                Targetsystem tsnew = new Targetsystem();
                                tsnew.setTargetindex(cs.getTargetindex());
                                List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                if (target != null && target.size() > 0)
                                {
                                    tsnew = (Targetsystem) target.get(0);
                                }
                                if ((tsnew.getTargettype() == 3))
                                {// cdn
                                    pristatus = 0;
                                }
                                else if (tsnew.getTargettype() == 1)
                                {// bms
                                    pristatus = 0;
                                }

                            }
                        }
                    }
                    insertSyncTask(targetindex, xmladdress, categoryCdn.getCategoryindex(),
                            categoryCdn.getCategoryid(), 2, actiontype, categoryCdn.getCatagorycode(), batchid,
                            pristatus);// 调用同步任务模

                    // 块DS代码
                    cntTargetSync.setStatus(200);// 200发布中

                    cntTargetSyncDS.updateCntTargetSync(cntTargetSync);

                    CommonLogUtil.insertOperatorLog(cntTargetSync.getObjectid() + ", " + targetSystem.getTargetid(),
                            "log.categorysyn.mgt", CategoryConstant.OPERTYPE_CATEGORY_PUBLISH,
                            CategoryConstant.OPERTYPE_CATEGORY_PUBLISH_INFO,
                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                    map.put("msg", "0:操作成功");

                }
                else
                {

                    map.put("msg", "1:发布数据状态不正确");
                    continue;
                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }

            }

            totalCategoryindex++;
            num++;
        }
        if ((successCount) == totalCategoryindex)
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCategoryindex)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }
        return map;
    }

    // public String publishCategory(List<Categorycdn> categoryList, int
    // platform,
    // List targetlist) throws Exception {
    // Map<String, List> objMap = new HashMap();
    // ArrayList list = new ArrayList();
    // String xmladdress;
    // // 2.0平台
    //
    // for (int i = 0; i < categoryList.size(); i++) {
    //
    // Categorycdn categoryCdn = (Categorycdn) categoryList.get(i);
    // list.add(categoryCdn);
    // objMap.put("categoryList", list);
    // if (platform == 2) {// 3.0平台,1个平台生成1个xml
    // xmladdress = StandardImpFactory.getInstance().create(platform)
    // .createXmlFile(objMap, 2, 2, 1);// 文广1是regist2是栏目
    // } else {
    // xmladdress = StandardImpFactory.getInstance().create(platform)
    // .createXmlFile(objMap, 1, 2, 1);// 1是regist2是栏目
    //
    // }
    // // 针对不同栏目，不同网元，插多条同步任务，一条同步任务包括一个对象下所有mapping及对象
    // for (int j = 0; j < targetlist.size(); j++) {
    // Targetsystem targetsystem = (Targetsystem) targetlist.get(j);
    // insertSyncTask(targetsystem.getTargetindex(), xmladdress, Long
    // .parseLong(categoryCdn.getObjindex()), categoryCdn
    // .getCategoryid(), 2, 1);// 调用同步任务模块DS代码
    // }
    //
    // }
    // // 3.0平台
    //
    // return null;
    // }

    // 网元取消发布方法，有用,所有这些网元都属于一个平台
    public HashMap cancelpublishCategoryTar(List<CntTargetSync> cntTargetSynclist) throws Exception
    {
        HashMap map = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        int successCount = 0;
        int totalCategoryindex = 0;
        int failCount = 0;
        int num = 1;
        int index = 1;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        List<Categorycdn> listCmsProgramtype = new ArrayList<Categorycdn>();
        Integer ifail = 0;
        Integer iSuccessed = 0;
        Categorycdn cdn = getCmsCategory(cntTargetSynclist.get(0).getObjectid());// 栏目
        Categorycdn cdntemp = new Categorycdn();
        cdntemp.setParentid(cdn.getCategoryid());
        List childctglist = cmsCategoryDS.selectByCategoryindex(cdntemp);
        if (cntTargetSynclist != null && cntTargetSynclist.size() > 0)
        {
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setSyncindex(cntTargetSynclist.get(0).getRelateindex());
            List pltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (pltlist != null && pltlist.size() > 0)
            {
                cntPlatformSync = (CntPlatformSync) pltlist.get(0);// 获取平台发布信息
            }

            List tarsynclist = new ArrayList();
            String xmladdress;
            int actiontype = 0;
            for (int i = 0; i < cntTargetSynclist.size(); i++)
            {
                CntTargetSync cntTargetSync = cntTargetSynclist.get(i);
                List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    map.put("msg", "1:发布数据不存在");

                    return map;
                }
                Map<String, List> objMap = new HashMap();
                ArrayList list = new ArrayList();

                int status = cntTargetSync.getStatus();

                String objid = cntPlatformSync.getObjectid();// 栏目id
                boolean flagele = false;
                long targetindex = cntTargetSync.getTargetindex();
                CntTargetSync sync2 = new CntTargetSync();
                sync2.setTargetindex(targetindex);
                sync2.setParentid(objid);
                sync2.setObjecttype(28);// 频道关联栏目

                List sync2list = cntTargetSyncDS.getCntTargetSyncByCond(sync2);
                if (sync2list != null && sync2list.size() > 0)
                {
                    for (int kk = 0; kk < sync2list.size(); kk++)
                    {
                        sync2 = (CntTargetSync) sync2list.get(kk);
                        int statussync = sync2.getStatus();
                        if (statussync == 300 || statussync == 500)
                        {
                            flagele = true;
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex()
                            // .toString(), ResourceMgt.findDefaultText(FAIL), "此栏目与频道的 mapping关系已发布不能取消同步");
                            // returnInfo.appendOperateInfo(operateInfo);
                            map.put("msg", "1:与频道关联关系已发布");
                            continue;
                        }
                    }

                }
                CntTargetSync syncparent = new CntTargetSync();
                syncparent.setTargetindex(targetindex);
                syncparent.setParentid(objid);
                syncparent.setObjecttype(27);// 连续剧关联栏目
                List syncparentlist = cntTargetSyncDS.getCntTargetSyncByCond(syncparent);
                if (syncparentlist != null && syncparentlist.size() > 0)
                {
                    for (int kk = 0; kk < syncparentlist.size(); kk++)
                    {
                        syncparent = (CntTargetSync) syncparentlist.get(kk);
                        int statussync = syncparent.getStatus();
                        if (statussync == 300 || statussync == 500)
                        {
                            flagele = true;
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex()
                            // .toString(), ResourceMgt.findDefaultText(FAIL), "此栏目与连续剧的 mapping关系已发布不能取消同步");
                            // returnInfo.appendOperateInfo(operateInfo);
                            map.put("msg", "1:与连续剧关联关系已发布");
                            continue;
                        }
                    }

                }

                CntTargetSync syncprog = new CntTargetSync();
                syncprog.setTargetindex(targetindex);
                syncprog.setParentid(objid);
                syncprog.setObjecttype(26);// 内容关联栏目
                List syncproglist = cntTargetSyncDS.getCntTargetSyncByCond(syncprog);
                if (syncproglist != null && syncproglist.size() > 0)
                {
                    for (int kk = 0; kk < syncproglist.size(); kk++)
                    {
                        syncprog = (CntTargetSync) syncproglist.get(kk);
                        int statussync = syncprog.getStatus();
                        if (statussync == 300 || statussync == 500)
                        {
                            flagele = true;
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex()
                            // .toString(), ResourceMgt.findDefaultText(FAIL), "此栏目与内容的 mapping关系已发布不能取消同步");
                            // returnInfo.appendOperateInfo(operateInfo);
                            map.put("msg", "1:与内容关联关系已发布");
                            continue;
                        }
                    }

                }

                CntTargetSync syncpic = new CntTargetSync();
                syncpic.setTargetindex(targetindex);
                syncpic.setElementid(objid);
                syncpic.setObjecttype(31);// 海报关联栏目
                List syncpiclist = cntTargetSyncDS.getCntTargetSyncByCond(syncpic);
                if (syncpiclist != null && syncpiclist.size() > 0)
                {
                    for (int kk = 0; kk < syncpiclist.size(); kk++)
                    {
                        syncpic = (CntTargetSync) syncpiclist.get(kk);
                        int statussync = syncpic.getStatus();
                        if (statussync == 300 || statussync == 500)
                        {
                            flagele = true;
                            map.put("msg", "1:与海报关联关系已发布");
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getSyncindex()
                            // .toString(), ResourceMgt.findDefaultText(FAIL), "此栏目与海报的 mapping关系已发布不能取消同步");
                            // returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                    }

                }
                if (flagele == true)
                {
                    break;
                }
                int countchild = 0;
                if (childctglist != null && childctglist.size() > 0)
                {
                    for (int k = 0; k < childctglist.size(); k++)
                    {
                        CntTargetSync temp = new CntTargetSync();
                        temp.setObjectid(((Categorycdn) childctglist.get(k)).getCategoryid());
                        temp.setTargetindex(cntTargetSync.getTargetindex());
                        List listtar = cntTargetSyncDS.getCntTargetSyncByCond(temp);
                        if (listtar != null && listtar.size() > 0)
                        {
                            temp = (CntTargetSync) listtar.get(0);
                            if (temp.getStatus() == 300 || temp.getStatus() == 500 || temp.getStatus() == 200)
                            {
                                countchild++;

                            }
                        }
                    }

                }
                if (countchild != 0)
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++),
                    // cntPlatformSync.getSyncindex().toString(),
                    // ResourceMgt.findDefaultText(FAIL), "必须先对子节点删除同步");
                    // returnInfo.appendOperateInfo(operateInfo);
                    // continue;
                    map.put("msg", "1:必须先对子节点取消发布");
                    return map;
                }
                else if (status == 300 || status == 500)
                {// 300发布成功，500修改发布失败
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        map.put("msg", "1:目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确");
                        return map;
                    }
                    actiontype = 3;// delete
                    cntTargetSync.setOperresult(70);// 取消同步中

                    long objindex = cntTargetSync.getObjectindex();
                    Categorycdn categoryCdn = new Categorycdn();
                    categoryCdn.setCategoryindex(objindex);
                    categoryCdn = cmsCategoryDS.getCategorycdn(categoryCdn);
                    Categorycdn parentcdn = new Categorycdn();
                    parentcdn.setCategoryid(categoryCdn.getParentid());
                    parentcdn = cmsCategoryDS.getCategoryByCategoryId(parentcdn);
                    categoryCdn.setParentcode(parentcdn.getCatagorycode());
                    list.add(categoryCdn);
                    objMap.put("categoryList", list);

                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 2, actiontype);// 1是regist2是栏目
                    }
                    else
                    {
                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 2, actiontype);// 文广1是regist2是栏目

                    }
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    insertSyncTask(targetindex, xmladdress, categoryCdn.getCategoryindex(),
                            categoryCdn.getCategoryid(), 2, actiontype, categoryCdn.getCatagorycode(), batchid, 1);// 调用同步任务模块DS代码
                    cntTargetSync.setStatus(200);// 200发布中

                    cntTargetSyncDS.updateCntTargetSync(cntTargetSync);

                    // successCount++;
                    // listCmsProgramtype.add(getCmsCategory(categoryCdn
                    // .getCategoryid()));
                    // operateInfo = new OperateInfo(String.valueOf(index++),
                    // categoryCdn.getCategoryid(), ResourceMgt
                    // .findDefaultText(SUCCESS), ResourceMgt
                    // .findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    CommonLogUtil.insertOperatorLog(cdn.getCategoryid() + ", " + targetSystem.getTargetid(),
                            "log.categorysyn.mgt", "log.category.unpublish", "log.category.unpublish.info",
                            CategoryConstant.RESOURCE_OPERATION_SUCCESS);
                    map.put("msg", "0:操作成功");
                    // tarsynclist.add(200);
                    // }
                }
                else
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String.valueOf(index++),
                    // cntTargetSync.getObjectindex().toString(),
                    // ResourceMgt.findDefaultText(FAIL), "当前状态不能取消发布");
                    // returnInfo.appendOperateInfo(operateInfo);
                    // failCount++;
                    // continue;
                    map.put("msg", "1:发布数据状态不正确");
                }
            }

            CntTargetSync finaltarget = new CntTargetSync();
            finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
            List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

            if (finaltargetlist != null && finaltargetlist.size() > 0)
            {
                int[] targetSyncStatus = new int[finaltargetlist.size()];
                for (int ii = 0; ii < finaltargetlist.size(); ii++)
                {
                    targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                }
                int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                if (originalstatus != statussync)
                {
                    cntPlatformSync.setStatus(statussync);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
            }
            totalCategoryindex++;
            num++;
        }
        if ((successCount) == totalCategoryindex)
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCategoryindex)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }
        return map;
    }

    /**
     * 批量关联运营平台，多个对象关联多个运营平台
     * 
     * @param targetsystemList
     *            Targetsystem对象列表
     * @param cmsCategoryList
     *            栏目列表
     * @return String
     * @throws DomainServiceException
     *             ds异常
     */
    public String bindBatchProgramTargetsys(List<Targetsystem> targetsystemList, List<Categorycdn> cmsCategoryList)
            throws DomainServiceException
    {

        ResourceMgt.addDefaultResourceBundle(CategoryConstants.CMS_TARGETSYSTEM_RES_FILE_NAME);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCategoryindex = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsCategoryList.size(); i++) // 遍历要操作的内容主键，依次操作要关联的内容
        {

            String result = null;
            String operdesc = null;
            String proStatusStr = null;

            Categorycdn categorycdn = (Categorycdn) cmsCategoryList.get(i);
            if (categorycdn == null)
            {
                proStatusStr = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_PROGRAM_HAS_DELETED);
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), categorycdn.getCategoryid(), result.substring(2,
                        result.length()), proStatusStr);
                returnInfo.appendOperateInfo(operateInfo);
            }
            totalCategoryindex++;// 栏目
            boolean flag = false;
            String msg = "";
            for (int j = 0; j < targetsystemList.size(); j++)
            {
                operdesc = preparetopublish(targetsystemList.get(j), categorycdn.getCategoryindex(), categorycdn
                        .getCategoryid(), 2);// 添加到网元的数据，栏目

                if (operdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                {
                    flag = true;// 只要有一条成功就算成功
                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "关联成功";

                }
                else
                // 内容关联运营平台失败
                {
                    msg = msg + " 平台" + targetsystemList.get(j).getTargetid() + "已经被关联";

                }
                if (targetsystemList.get(j).getTargettype() == 2 || targetsystemList.get(j).getTargettype() == 0
                        || targetsystemList.get(j).getTargettype() == 10)
                {
                    CategoryPictureMap map = new CategoryPictureMap();
                    map.setCategoryindex(categorycdn.getCategoryindex());
                    List list = categoryPictureMapDS.getCategoryPictureMapByCond(map);// 栏目下存在海报,将海报一起关联平台
                    if (list != null && list.size() > 0)
                    {
                        for (int k = 0; k < list.size(); k++)
                        {
                            CategoryPictureMap cpmap = (CategoryPictureMap) list.get(k);
                            Long pindex = cpmap.getPictureindex();
                            preparetopublishForMap(targetsystemList.get(j), cpmap.getMapindex(), cpmap.getMappingid(),
                                    31, categorycdn.getCategoryid(), cpmap.getPictureid());// 海报的发布数据

                        }
                    }
                }

            }

            if (flag == true)// 只要有一条成功就算成功
            {
                successCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_SUCCESSFUL);
                operateInfo = new OperateInfo(String.valueOf(num), categorycdn.getCategoryid(), "成功", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            else
            {
                failCount++;
                result = ResourceMgt.findDefaultText(CategoryConstants.CMS_TARGETSYSTEM_MSG_OPER_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), categorycdn.getCategoryid(), "失败", msg);
                returnInfo.appendOperateInfo(operateInfo);
            }
            num++;
        }

        if ((successCount) == totalCategoryindex)
        {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCategoryindex)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量：" + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();
    }

    /** ***********************栏目关联海报发布********************* */
    public TableDataInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            categoryPictureMap.setCreateStarttime(DateUtil2.get14Time(categoryPictureMap.getCreateStarttime()));
            categoryPictureMap.setCreateEndtime(DateUtil2.get14Time(categoryPictureMap.getCreateEndtime()));
            categoryPictureMap.setOnlinetimeStartDate(DateUtil2.get14Time(categoryPictureMap.getOnlinetimeStartDate()));
            categoryPictureMap.setOnlinetimeEndDate(DateUtil2.get14Time(categoryPictureMap.getOnlinetimeEndDate()));
            categoryPictureMap.setCntofflinestarttime(DateUtil2.get14Time(categoryPictureMap.getCntofflinestarttime()));
            categoryPictureMap.setCntofflineendtime(DateUtil2.get14Time(categoryPictureMap.getCntofflineendtime()));

            if (categoryPictureMap.getPictureid() != null && !"".equals(categoryPictureMap.getPictureid()))
            {
                categoryPictureMap.setPictureid(EspecialCharMgt.conversion(categoryPictureMap.getPictureid()));
            }
            dataInfo = categoryPictureMapDS.pageInfoQuerycatPicMap(categoryPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }

    public TableDataInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");

            dataInfo = categoryPictureMapDS.pageInfoQuerypcatPicTargetMap(categoryPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    }

    public String batchPublishOrDelPublishcatPic(List<CategoryPictureMap> list, String operType, int type)
            throws Exception
    {
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;
        try
        {
            int success = 0;
            int fail = 0;
            String message = "";
            String resultStr = "";
            if (operType.equals("0")) // 批量发布
            {
                for (CategoryPictureMap categoryPictureMap : list)
                {
                    message = publishcatPicMapPlatformSyn(categoryPictureMap.getMapindex().toString(),
                            categoryPictureMap.getCategoryid(), categoryPictureMap.getPictureid(), type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else
            // 批量取消发布
            {
                for (CategoryPictureMap categoryPictureMap : list)
                {
                    message = delPublishcatPicMapPlatformSyn(categoryPictureMap.getMapindex().toString(),
                            categoryPictureMap.getCategoryid(), categoryPictureMap.getPictureid(), type, 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), categoryPictureMap.getMappingid(), resultStr
                                .substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }
        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();
    }

    public String publishcatPicMapPlatformSyn(String mapindex, String categoryid, String pictureid, int type,
            int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null;
        boolean sucessPub = false;
        StringBuffer resultPubStr = new StringBuffer();
        try
        {
            CategoryPictureMap categoryPictureMap = new CategoryPictureMap();
            categoryPictureMap.setMapindex(Long.parseLong(mapindex));
            categoryPictureMap.setCategoryid(categoryid);
            categoryPictureMap.setPictureid(pictureid);
            List<CategoryPictureMap> categoryPictureMapList = new ArrayList<CategoryPictureMap>();
            categoryPictureMapList = categoryPictureMapDS.getCategoryPictureMapByCond(categoryPictureMap);
            if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
            {
                return "1:操作失败,栏目关联海报记录不存在";
            }
            String initresult = "";
            initresult = initDataPic(categoryPictureMapList.get(0), "platform", type, 1);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
            cntPlatformSync.setObjecttype(31);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(categoryPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            catpicmap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(31);
            catpicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            CategoryPictureMap categoryPictureMapObj = new CategoryPictureMap();
            int platformType = 1;
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setObjectindex(categoryPictureMap.getMapindex());
            if (type == 1) // 3.0平台
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                catpicmap.put("pictureMap", categoryPictureMapList);
                int targettype;
                for (CategoryPictureMap categoryPictureMapTmp : categoryPictureMapList)
                {
                    targettype = categoryPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (categoryPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + ": " + "目标系统状态不正确"
                                + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        synType = 1;
                        boolean isxml = true;
                        boolean ispub = true;
                        cntTargetSync.setObjectindex(categoryPictureMapTmp.getCategoryindex());
                        cntTargetSync.setTargetindex(categoryPictureMapTmp.getTargetindex());
                        cntTargetSync.setObjecttype(2);
                        cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                        if (cntTargetSync == null
                                || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync
                                        .getStatus() != 500))
                        {
                            resultPubStr.append("该栏目没有发布到" + TARGETSYSTEM + categoryPictureMapTmp.getTargetid() + "  ");
                            ispub = false;
                        }
                        if (ispub)
                        {
                            int synTargetType = 1;
                            if (categoryPictureMapTmp.getStatus() == 500) // 修改发布
                            {
                                synTargetType = 2;
                                synType = 2;
                            }
                            if (targettype == 2
                                    && (categoryPictureMapTmp.getStatus() == 0
                                            || categoryPictureMapTmp.getStatus() == 400 || categoryPictureMapTmp
                                            .getStatus() == 500)) // EPG系统
                            {
                                String xml;
                                if (synTargetType == 1)
                                {
                                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(catpicmap,
                                            targettype, 10, 1); // 新增发布到EPG
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid()
                                                + ": " + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                else
                                {
                                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(catpicmap,
                                            targettype, 10, 2); // 修改发布到EPG
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid()
                                                + ": " + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                if (isxml)
                                {
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                            .toString();
                                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                    Long targetindex = categoryPictureMapTmp.getTargetindex();

                                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index");
                                    Long objectPicindex = categoryPictureMapTmp.getPictureindex();
                                    String objectid = categoryPictureMapTmp.getPictureid();
                                    String objectcode = categoryPictureMapTmp.getPicturecode();
                                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                            synType, objectcode, 1); // 插入对象日志发布表
                                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index");
                                    String objectidMapping = categoryPictureMapTmp.getMappingid();
                                    String parentid = categoryPictureMapTmp.getPictureid();
                                    String elementid = categoryPictureMapTmp.getCategoryid();
                                    Long objectindex = categoryPictureMapTmp.getMapindex();
                                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping,
                                            elementid, parentid, targetindex, synType);

                                    insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                    cntTargetSyncTemp.setSyncindex(categoryPictureMapTmp.getSyncindex());
                                    if (synTargetType == 1)
                                    {
                                        cntTargetSyncTemp.setOperresult(10);
                                    }
                                    else
                                    {
                                        cntTargetSyncTemp.setOperresult(40);
                                    }
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(categoryPictureMapTmp.getMappingid() + ","
                                            + categoryPictureMapTmp.getTargetid(),
                                            CommonLogConstant.MGTTYPE_CATEGORYSYN,
                                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH,
                                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub = true;
                                    resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + ": "
                                            + operSucess + "  ");
                                }
                            }
                        }
                    }
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                catpicmap.put("pictureMap", categoryPictureMapList);
                int targettype;
                for (CategoryPictureMap categoryPictureMapTmp : categoryPictureMapList)
                {
                    targettype = categoryPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (categoryPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + ": " + "目标系统状态不正确"
                                + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {

                        synType = 1;
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        boolean ispub = true;
                        boolean isxml = true;
                        cntTargetSync.setObjectindex(categoryPictureMapTmp.getCategoryindex());
                        cntTargetSync.setTargetindex(categoryPictureMapTmp.getTargetindex());
                        cntTargetSync.setObjecttype(2);
                        cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                        if (cntTargetSync == null
                                || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync
                                        .getStatus() != 500))
                        {
                            resultPubStr.append("该栏目没有发布到" + TARGETSYSTEM + ":" + categoryPictureMapTmp.getTargetname()
                                    + "  ");
                            ispub = false;
                        }
                        if (ispub)
                        {
                            int synTargetType = 1;
                            if (categoryPictureMapTmp.getStatus() == 500) // 修改发布
                            {
                                synTargetType = 2;
                                synType = 2;
                            }
                            if ((categoryPictureMapTmp.getStatus() == 0 || categoryPictureMapTmp.getStatus() == 400 || categoryPictureMapTmp
                                    .getStatus() == 500)) // 2.0平台网元
                            {
                                String xml;
                                if (synTargetType == 1)
                                {
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(catpicmap,
                                            targettype, 10, 1); // 新增发布
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid()
                                                + ": " + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                else
                                {
                                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(catpicmap,
                                            targettype, 10, 2); // 修改发布
                                    if (null == xml)
                                    {
                                        isxml = false;
                                        resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid()
                                                + ": " + "生成同步指令xml文件失败" + "  ");
                                    }
                                }
                                if (isxml)
                                {
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                            .toString();
                                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                    Long targetindex = categoryPictureMapTmp.getTargetindex();
                                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index"); // 获取taskindex
                                    Long objectPicindex = categoryPictureMapTmp.getPictureindex();
                                    String objectid = categoryPictureMapTmp.getPictureid();
                                    String objectcode = categoryPictureMapTmp.getPicturecode();
                                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                            synType, objectcode, 1); // 插入对象日志发布表
                                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                            "object_sync_record_index");
                                    String objectidMapping = categoryPictureMapTmp.getMappingid();
                                    String parentid = categoryPictureMapTmp.getPictureid();
                                    String elementid = categoryPictureMapTmp.getCategoryid();
                                    Long objectindex = categoryPictureMapTmp.getMapindex();
                                    String elementcode = categoryPictureMapTmp.getCatagorycode();
                                    String parentcode = objectcode;
                                    insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping,
                                            elementid, parentid, elementcode, parentcode, targetindex, synType);
                                    insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                    cntTargetSyncTemp.setSyncindex(categoryPictureMapTmp.getSyncindex());
                                    if (synTargetType == 1)
                                    {
                                        cntTargetSyncTemp.setOperresult(10);
                                    }
                                    else
                                    {
                                        cntTargetSyncTemp.setOperresult(40);
                                    }
                                    cntTargetSyncTemp.setStatus(200);
                                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                    CommonLogUtil.insertOperatorLog(categoryPictureMapTmp.getMappingid() + ","
                                            + categoryPictureMapTmp.getTargetid(),
                                            CommonLogConstant.MGTTYPE_CATEGORYSYN,
                                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH,
                                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_INFO,
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                    sucessPub = true;
                                    resultPubStr.append(PUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + ": "
                                            + operSucess + "  ");
                                }
                            }
                        }
                    }
                }
            }
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTemp.setStatus(200);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        rtnPubStr = sucessPub == false ? "1:" + resultPubStr.toString() : "0:" + resultPubStr.toString();
        return rtnPubStr;
    }

    public String delPublishcatPicMapPlatformSyn(String mapindex, String categoryid, String pictureid, int type,
            int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null;
        boolean sucessPub = false;
        StringBuffer resultPubStr = new StringBuffer();
        try
        {
            CategoryPictureMap categoryPictureMap = new CategoryPictureMap();
            categoryPictureMap.setMapindex(Long.parseLong(mapindex));
            categoryPictureMap.setCategoryid(categoryid);
            categoryPictureMap.setPictureid(pictureid);
            List<CategoryPictureMap> categoryPictureMapList = new ArrayList<CategoryPictureMap>();
            categoryPictureMapList = categoryPictureMapDS.getCategoryPictureMapByCond(categoryPictureMap);
            if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
            {
                return "1:操作失败,栏目关联海报记录不存在";
            }
            String initresult = "";
            initresult = initDataPic(categoryPictureMapList.get(0), "platform", type, 3);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
            cntPlatformSync.setObjecttype(31);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(categoryPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            catpicmap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(31);
            catpicmap.put("type", typeList);
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            CategoryPictureMap categoryPictureMapObj = new CategoryPictureMap();
            int platformType = 1;
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setObjectindex(categoryPictureMap.getMapindex());
            if (type == 1) // 3.0平台
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                catpicmap.put("pictureMap", categoryPictureMapList);
                for (CategoryPictureMap categoryPictureMapTmp : categoryPictureMapList)
                {

                    int targettype = categoryPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (categoryPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub + ": "
                                + "目标系统状态不正确" + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {

                        if (targettype == 2
                                && (categoryPictureMapTmp.getStatus() == 300 || categoryPictureMapTmp.getStatus() == 500)) // EPG系统
                        {
                            boolean isxml = true;
                            String xml;
                            xml = StandardImpFactory.getInstance().create(2)
                                    .createXmlFile(catpicmap, targettype, 10, 3); // 取消发布到EPG
                            if (null == xml)
                            {
                                isxml = false;
                                resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub
                                        + ": " + "生成同步指令xml文件失败" + "  ");
                            }
                            if (isxml)
                            {
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID)
                                        .toString();
                                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                                Long targetindex = categoryPictureMapTmp.getTargetindex();
                                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index");
                                Long objectPicindex = categoryPictureMapTmp.getPictureindex();
                                String objectid = categoryPictureMapTmp.getPictureid();
                                String objectcode = categoryPictureMapTmp.getPicturecode();
                                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex,
                                        synType, objectcode, 1); // 插入对象日志发布表
                                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                        "object_sync_record_index");
                                String objectidMapping = categoryPictureMapTmp.getMappingid();
                                String parentid = categoryPictureMapTmp.getPictureid();
                                String elementid = categoryPictureMapTmp.getCategoryid();
                                Long objectindex = categoryPictureMapTmp.getMapindex();
                                insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping,
                                        elementid, parentid, targetindex, synType);
                                insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务

                                cntTargetSyncTemp.setSyncindex(categoryPictureMapTmp.getSyncindex());
                                cntTargetSyncTemp.setOperresult(70);
                                cntTargetSyncTemp.setStatus(200);
                                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                                CommonLogUtil.insertOperatorLog(categoryPictureMapTmp.getMappingid() + ","
                                        + categoryPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CATEGORYSYN,
                                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL,
                                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL_INFO,
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                sucessPub = true;
                                resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub
                                        + ": " + operSucess + "  ");
                            }
                        }
                    }
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取关联目标网元发布记录失败";
                }
                catpicmap.put("pictureMap", categoryPictureMapList);
                int targettype;
                for (CategoryPictureMap categoryPictureMapTmp : categoryPictureMapList)
                {

                    targettype = categoryPictureMapTmp.getTargettype();
                    boolean isstatus = true;
                    if (categoryPictureMapTmp.getTargetstatus() != 0)
                    {
                        resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub + ": "
                                + "目标系统状态不正确" + "  ");
                        isstatus = false;
                    }
                    if (isstatus)
                    {
                        if (categoryPictureMapTmp.getStatus() == 300 || categoryPictureMapTmp.getStatus() == 500) // 2.0平台网元
                        {
                            String xml;
                            boolean isxml = true;
                            xml = StandardImpFactory.getInstance().create(1)
                                    .createXmlFile(catpicmap, targettype, 10, 3); // 新增发布
                            if (null == xml)
                            {
                                isxml = false;
                                resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub
                                        + ": " + "生成同步指令xml文件失败" + "  ");
                            }
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                            Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                            Long targetindex = categoryPictureMapTmp.getTargetindex();
                            Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                    "object_sync_record_index"); // 获取taskindex
                            Long objectPicindex = categoryPictureMapTmp.getPictureindex();
                            String objectid = categoryPictureMapTmp.getPictureid();
                            String objectcode = categoryPictureMapTmp.getPicturecode();
                            insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, targetindex, synType,
                                    objectcode, 1); // 插入对象日志发布表
                            Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                                    "object_sync_record_index");
                            String objectidMapping = categoryPictureMapTmp.getMappingid();
                            String parentid = categoryPictureMapTmp.getPictureid();
                            String elementid = categoryPictureMapTmp.getCategoryid();
                            Long objectindex = categoryPictureMapTmp.getMapindex();
                            String elementcode = categoryPictureMapTmp.getCatagorycode();
                            String parentcode = objectcode;
                            insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping,
                                    elementid, parentid, elementcode, parentcode, targetindex, synType);
                            insertSyncPicTask(index, xml, correlateid, targetindex); // 插入同步任务
                            cntTargetSyncTemp.setSyncindex(categoryPictureMapTmp.getSyncindex());
                            cntTargetSyncTemp.setOperresult(70);
                            cntTargetSyncTemp.setStatus(200);
                            cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                            CommonLogUtil.insertOperatorLog(categoryPictureMapTmp.getMappingid() + ","
                                    + categoryPictureMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_CATEGORYSYN,
                                    CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL,
                                    CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            sucessPub = true;
                            resultPubStr.append(DELPUBTARGETSYSTEM + categoryPictureMapTmp.getTargetid() + delPub
                                    + ": " + operSucess + "  ");
                        }
                    }
                }
            }
            if (sucessPub)
            {
                CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTemp.setStatus(200);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        rtnPubStr = sucessPub == false ? "1:" + resultPubStr.toString() : "0:" + resultPubStr.toString();
        return rtnPubStr;
    }

    public String publishcatPicMapTarget(String mapindex, String targetindex, int type) throws Exception
    {
        try
        {
            ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
            CategoryPictureMap categoryPictureMap = new CategoryPictureMap();
            categoryPictureMap.setMapindex(Long.parseLong(mapindex));
            categoryPictureMap.setTargetindex(Long.parseLong(targetindex));
            categoryPictureMap.setObjecttype(31);
            List<CategoryPictureMap> categoryPictureMapList = new ArrayList<CategoryPictureMap>();
            categoryPictureMapList = categoryPictureMapDS.getCategoryPictureMapByCond(categoryPictureMap);
            if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
            {
                return "1:操作失败,栏目关联海报记录不存在";
            }
            int synType = 1;
            String initresult = "";
            initresult = initDataPic(categoryPictureMapList.get(0), targetindex, 3, synType);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(categoryPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            catpicmap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(31);
            catpicmap.put("type", typeList);
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(categoryPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(31);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            CategoryPictureMap categoryPictureMapObj = new CategoryPictureMap();
            int platformType = 1;
            if (type == 1)
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (categoryPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";
                }
                int targettype = categoryPictureMapList.get(0).getTargettype();
                catpicmap.put("pictureMap", categoryPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(categoryPictureMapList.get(0).getCategoryindex());
                cntTargetSync.setTargetindex(categoryPictureMapList.get(0).getTargetindex());
                cntTargetSync.setObjecttype(2);
                cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                if (cntTargetSync == null
                        || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
                {
                    return "1:操作失败,该栏目没有发布到该网元";
                }
                int synTargetType = 1;
                if (categoryPictureMapList.get(0).getStatus() == 500) // 修改发布
                {
                    synTargetType = 2;
                    synType = 2;
                }
                if (targettype == 2) // EPG系统
                {
                    String xml;
                    if (synTargetType == 1)
                    {
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(catpicmap, targettype, 10, 1); // 新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                    }
                    else
                    {
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(catpicmap, targettype, 10, 2); // 修改发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                    }
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                    Long objectPicindex = categoryPictureMapList.get(0).getPictureindex();
                    String objectid = categoryPictureMapList.get(0).getPictureid();
                    String objectcode = categoryPictureMapList.get(0).getPicturecode();
                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex),
                            synType, objectcode, 1); // 插入对象日志发布表
                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                            "object_sync_record_index");
                    String objectidMapping = categoryPictureMapList.get(0).getMappingid();
                    String parentid = categoryPictureMapList.get(0).getPictureid();
                    String elementid = categoryPictureMapList.get(0).getCategoryid();
                    Long objectindex = categoryPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                            Long.valueOf(targetindex), synType);
                    insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                    cntTargetSyncTemp.setSyncindex(categoryPictureMapList.get(0).getSyncindex());
                    if (synTargetType == 1)
                    {
                        cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                        cntTargetSyncTemp.setOperresult(40);
                    }
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                    CommonLogUtil.insertOperatorLog(categoryPictureMapList.get(0).getMappingid() + ","
                            + categoryPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CATEGORYSYN,
                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH,
                            CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMapObj);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (categoryPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";
                }
                int targettype = categoryPictureMapList.get(0).getTargettype();
                catpicmap.put("pictureMap", categoryPictureMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(categoryPictureMapList.get(0).getCategoryindex());
                cntTargetSync.setTargetindex(categoryPictureMapList.get(0).getTargetindex());
                cntTargetSync.setObjecttype(2);
                cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
                if (cntTargetSync == null
                        || (cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
                {
                    return "1:操作失败,该栏目没有发布到该网元";
                }
                int synTargetType = 1;
                if (categoryPictureMapList.get(0).getStatus() == 500) // 修改发布
                {
                    synTargetType = 2;
                    synType = 2;
                }

                String xml;
                if (synTargetType == 1)
                {
                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(catpicmap, targettype, 10, 1); // 新增发布
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                }
                else
                {
                    xml = StandardImpFactory.getInstance().create(1).createXmlFile(catpicmap, targettype, 10, 2); // 修改发布
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                }
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                Long objectPicindex = categoryPictureMapList.get(0).getPictureindex();
                String objectid = categoryPictureMapList.get(0).getPictureid();
                String objectcode = categoryPictureMapList.get(0).getPicturecode();
                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex), synType,
                        objectcode, 1); // 插入对象日志发布表
                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                String objectidMapping = categoryPictureMapList.get(0).getMappingid();
                String parentid = categoryPictureMapList.get(0).getPictureid();
                String elementid = categoryPictureMapList.get(0).getCategoryid();
                Long objectindex = categoryPictureMapList.get(0).getMapindex();
                String elementcode = categoryPictureMapList.get(0).getCatagorycode();
                String parentcode = objectcode;
                insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                        elementcode, parentcode, Long.valueOf(targetindex), synType);
                insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                cntTargetSyncTemp.setSyncindex(categoryPictureMapList.get(0).getSyncindex());
                if (synTargetType == 1)
                {
                    cntTargetSyncTemp.setOperresult(10);
                }
                else
                {
                    cntTargetSyncTemp.setOperresult(40);
                }
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                CommonLogUtil.insertOperatorLog(categoryPictureMapList.get(0).getMappingid() + ","
                        + categoryPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CATEGORYSYN,
                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH,
                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }

            List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
            cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
            int[] targetSyncStatus = new int[cntTargetSyncList.size()];
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                targetSyncStatus[i] = cntTargetSyncList.get(i).getStatus();
            }
            int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
            if (cntPlatformSynctmp.getStatus() != status) // 平台状态与计算出的状态不一致需要更新
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(categoryPictureMap.getMapindex());
                cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(status);
                cntPlatformSyncTmp.setPlatform(platformType);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return result;
    }

    public String delPublishcatPicMapTarget(String mapindex, String targetindex, int type) throws Exception
    {
        try
        {
            ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
            CategoryPictureMap categoryPictureMap = new CategoryPictureMap();
            categoryPictureMap.setMapindex(Long.parseLong(mapindex));
            categoryPictureMap.setTargetindex(Long.parseLong(targetindex));
            categoryPictureMap.setObjecttype(31);
            List<CategoryPictureMap> categoryPictureMapList = new ArrayList<CategoryPictureMap>();
            categoryPictureMapList = categoryPictureMapDS.getCategoryPictureMapByCond(categoryPictureMap);
            if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
            {
                return "1:操作失败,栏目关联海报记录不存在";
            }
            String initresult = "";
            int synType = 3;
            initresult = initDataPic(categoryPictureMapList.get(0), targetindex, 3, synType);
            if (!ProgramSynConstants.SUCCESS.equals(initresult))
            {
                return initresult;
            }
            List<Picture> pictureList = new ArrayList<Picture>();
            Picture picture = new Picture();
            picture.setPictureindex(categoryPictureMapList.get(0).getPictureindex());
            pictureList = pictureDS.getPictureByCond(picture);
            List<UsysConfig> usysConfigList = null;
            for (Picture picturetmp : pictureList)
            {
                picturetmp.setPictureurl(picturetmp.getPictureurl());
            }
            catpicmap.put("picture", pictureList);
            List<Integer> typeList = new ArrayList();
            typeList.add(31);
            catpicmap.put("type", typeList);
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(categoryPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(31);
            CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();
            CategoryPictureMap categoryPictureMapObj = new CategoryPictureMap();
            int platformType = 1;
            if (type == 1)
            {
                platformType = 2;
                cntPlatformSync.setPlatform(2);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMap);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (categoryPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";
                }
                int targettype = categoryPictureMapList.get(0).getTargettype();
                catpicmap.put("pictureMap", categoryPictureMapList);
                if (targettype == 2) // EPG系统
                {
                    String xml;
                    xml = StandardImpFactory.getInstance().create(2).createXmlFile(catpicmap, targettype, 10, 3); // 新增发布到EPG
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }

                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                    Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                    Long objectPicindex = categoryPictureMapList.get(0).getPictureindex();
                    String objectid = categoryPictureMapList.get(0).getPictureid();
                    String objectcode = categoryPictureMapList.get(0).getPicturecode();
                    insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex),
                            synType, objectcode, 1); // 插入对象日志发布表
                    Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
                            "object_sync_record_index");
                    String objectidMapping = categoryPictureMapList.get(0).getMappingid();
                    String parentid = categoryPictureMapList.get(0).getPictureid();
                    String elementid = categoryPictureMapList.get(0).getCategoryid();
                    Long objectindex = categoryPictureMapList.get(0).getMapindex();
                    insertPicMappingRecord(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                            Long.valueOf(targetindex), synType);
                    insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务
                    cntTargetSyncTemp.setSyncindex(categoryPictureMapList.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                    CommonLogUtil.insertOperatorLog(categoryPictureMapList.get(0).getMappingid() + ","
                            + categoryPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL,
                            CommonLogConstant.OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
            }
            if (type == 2)
            {
                cntPlatformSync.setPlatform(1);
                cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                categoryPictureMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
                categoryPictureMapObj.setTargetindex(Long.parseLong(targetindex));
                categoryPictureMapList = categoryPictureMapDS.getCategorynormalPictureMapByCond(categoryPictureMap);
                if (categoryPictureMapList == null || categoryPictureMapList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (categoryPictureMapList.get(0).getTargetstatus() != 0)
                {
                    return "1:操作失败,目标系统状态不正确";
                }
                int targettype = categoryPictureMapList.get(0).getTargettype();
                catpicmap.put("pictureMap", categoryPictureMapList);

                String xml;
                xml = StandardImpFactory.getInstance().create(1).createXmlFile(catpicmap, targettype, 10, 3); // 取消发布
                if (null == xml)
                {
                    return "1:操作失败,生成同步指令xml文件失败";
                }
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); // 获取taskindex
                Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); // 获取taskindex
                Long objectPicindex = categoryPictureMapList.get(0).getPictureindex();
                String objectid = categoryPictureMapList.get(0).getPictureid();
                String objectcode = categoryPictureMapList.get(0).getPicturecode();
                insertObjectRecord(syncindex, index, objectPicindex, objectid, 10, Long.valueOf(targetindex), synType,
                        objectcode, 1); // 插入对象日志发布表
                Long syncindexMapping = (Long) this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                String objectidMapping = categoryPictureMapList.get(0).getMappingid();
                String parentid = categoryPictureMapList.get(0).getPictureid();
                String elementid = categoryPictureMapList.get(0).getCategoryid();
                Long objectindex = categoryPictureMapList.get(0).getMapindex();
                String elementcode = categoryPictureMapList.get(0).getCatagorycode();
                String parentcode = objectcode;
                insertPicMappingIPTV2Record(syncindexMapping, index, objectindex, objectidMapping, elementid, parentid,
                        elementcode, parentcode, Long.valueOf(targetindex), synType);
                insertSyncPicTask(index, xml, correlateid, Long.valueOf(targetindex)); // 插入同步任务

                cntTargetSyncTemp.setSyncindex(categoryPictureMapList.get(0).getSyncindex());
                cntTargetSyncTemp.setOperresult(70);
                cntTargetSyncTemp.setStatus(200);
                cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); // 修改网元发布状态
                CommonLogUtil.insertOperatorLog(categoryPictureMapList.get(0).getMappingid() + ","
                        + categoryPictureMapList.get(0).getTargetid(), CommonLogConstant.MGTTYPE_CATEGORYSYN,
                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL,
                        CommonLogConstant.OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }

            List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
            CntTargetSync cntTargetSyncObj = new CntTargetSync();
            cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
            cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
            int[] targetSyncStatus = new int[cntTargetSyncList.size()];
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                targetSyncStatus[i] = cntTargetSyncList.get(i).getStatus();
            }
            int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
            if (cntPlatformSynctmp.getStatus() != status) // 平台状态与计算出的状态不一致需要更新
            {
                CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                cntPlatformSyncTmp.setObjectindex(categoryPictureMap.getMapindex());
                cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                cntPlatformSyncTmp.setStatus(status);
                cntPlatformSyncTmp.setPlatform(platformType);
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return result;
    }

    public String initDataPic(CategoryPictureMap categoryPictureMap, String targetindex, int platfType, int synType)
            throws Exception
    {
        try
        {
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setObjectindex(categoryPictureMap.getMapindex());
            cntPlatformSync.setObjecttype(31);
            Categorycdn categorycdn = new Categorycdn();
            categorycdn.setCategoryindex(categoryPictureMap.getCategoryindex());
            categorycdn = cmsCategoryDS.getCategorycdn(categorycdn);
            if (categorycdn == null)
            {
                return "1:操作失败,栏目不存在";
            }
            Picture picture = new Picture();
            picture.setPictureindex(categoryPictureMap.getPictureindex());
            picture = pictureDS.getPicture(picture);
            if (picture == null)
            {
                return "1:操作失败,海报不存在";
            }
            if (platfType == 1 || platfType == 2) // 发布到3.0平台 或者2.0平台
            {
                CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
                if (platfType == 1)
                {
                    cntPlatformSync.setPlatform(2);
                    cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                    if (cntPlatformSynctmp == null)
                    {
                        return "1:操作失败,发布记录不存在";
                    }
                }
                if (platfType == 2)
                {
                    cntPlatformSync.setPlatform(1);
                    cntPlatformSynctmp = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                    if (cntPlatformSynctmp == null)
                    {
                        return "1:操作失败,发布记录不存在";
                    }
                }
                if (synType == 1) // 发布
                {
                    if (cntPlatformSynctmp.getStatus() != 0 && cntPlatformSynctmp.getStatus() != 400
                            && cntPlatformSynctmp.getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                else
                // 取消发布
                {
                    if (cntPlatformSynctmp.getStatus() != 300 && cntPlatformSynctmp.getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                if (platfType == 1)
                {
                    cntTargetSyncTemp.setTargettype(2); // 海报只发布到EPG
                }
                else
                {
                    cntTargetSyncTemp.setPlatform(1); // 海报只发布到2.0网元
                }
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null || cntTargetSyncList.size() == 0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType == 1)
                {
                    boolean targetstatus = false;
                    for (CntTargetSync cntTargetSync : cntTargetSyncList)
                    {
                        if (cntTargetSync.getStatus() == 0 || cntTargetSync.getStatus() == 400
                                || cntTargetSync.getStatus() == 500)
                        {
                            targetstatus = true;
                        }
                    }
                    if (!targetstatus)
                    {
                        return "1:操作失败,目标网元发布状态不正确";
                    }
                }
                else
                {
                    boolean targetstatus = false;
                    for (CntTargetSync cntTargetSync : cntTargetSyncList)
                    {
                        if (cntTargetSync.getStatus() == 300 || cntTargetSync.getStatus() == 500)
                        {
                            targetstatus = true;
                        }
                    }
                    if (!targetstatus)
                    {
                        return "1:操作失败,目标网元发布状态不正确";
                    }
                }
            }
            if (platfType == 3) // 发布到网元
            {
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSync = new CntTargetSync();
                cntTargetSync.setObjectindex(categoryPictureMap.getMapindex());
                cntTargetSync.setObjecttype(31);
                cntTargetSync.setTargetindex(Long.valueOf(targetindex));
                cntTargetSyncListTemp = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
                if (cntTargetSyncListTemp == null || cntTargetSyncListTemp.size() == 0)
                {
                    return "1:操作失败,发布记录不存在";
                }
                if (synType == 1) // 发布
                {
                    if (cntTargetSyncListTemp.get(0).getStatus() != 0
                            && cntTargetSyncListTemp.get(0).getStatus() != 400
                            && cntTargetSyncListTemp.get(0).getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
                else
                // 取消发布
                {
                    if (cntTargetSyncListTemp.get(0).getStatus() != 300
                            && cntTargetSyncListTemp.get(0).getStatus() != 500)
                    {
                        return "1:操作失败,发布状态不正确";
                    }
                }
            }

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return "success";
    }

    private void insertPicMappingRecord(Long syncindexMapping, Long index, Long objectindexMovie,
            String objectidMapping, String elementid, String parentid, Long targetindex, int synType)
    {
        ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncMappingRecord.setSyncindex(syncindexMapping);
            objectSyncMappingRecord.setTaskindex(index);
            objectSyncMappingRecord.setObjectindex(objectindexMovie);
            objectSyncMappingRecord.setObjectid(objectidMapping);
            objectSyncMappingRecord.setDestindex(targetindex);
            objectSyncMappingRecord.setObjecttype(31);
            objectSyncMappingRecord.setElementid(elementid);
            objectSyncMappingRecord.setParentid(parentid);
            objectSyncMappingRecord.setActiontype(synType);
            objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    private void insertObjectRecord(Long syncindex, Long taskindex, Long objectindex, String objectid, int objecttype,
            Long targetindex, int synType, String objectcode, int platType)
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setObjectcode(objectcode);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    private void insertSyncPicTask(Long taskindex, String xmlAddress, String correlateid, Long targetindex)
    {
        Long batchid = (Long) (this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
        CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setBatchid(batchid);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    private void insertPicMappingIPTV2Record(Long syncindexMapping, Long index, Long objectindexMovie,
            String objectidMapping, String elementid, String parentid, String elmentcode, String parentcode,
            Long targetindex, int synType)
    {
        ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncMappingRecord.setSyncindex(syncindexMapping);
            objectSyncMappingRecord.setTaskindex(index);
            objectSyncMappingRecord.setObjectindex(objectindexMovie);
            objectSyncMappingRecord.setObjectid(objectidMapping);
            objectSyncMappingRecord.setDestindex(targetindex);
            objectSyncMappingRecord.setObjecttype(31);
            objectSyncMappingRecord.setElementid(elementid);
            objectSyncMappingRecord.setParentid(parentid);
            objectSyncMappingRecord.setElementcode(elmentcode);
            objectSyncMappingRecord.setParentcode(parentcode);
            objectSyncMappingRecord.setActiontype(synType);
            objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }

    public TableDataInfo getCategoryCntMapList(CategorycdnMap categorycdnMap, int start, int pageSize) throws Exception
    {
        // TODO Auto-generated method stub
        return null;
    }

    public TableDataInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize)
            throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("query category from content information given starting...");
        categorycdnMap.setCategoryname(EspecialCharMgt.conversion(categorycdnMap.getCategoryname()));
        categorycdnMap.setCategoryid(EspecialCharMgt.conversion(categorycdnMap.getCategoryid()));
        categorycdnMap.setContentid(EspecialCharMgt.conversion(categorycdnMap.getContentid()));
        categorycdnMap.setNamecn(EspecialCharMgt.conversion(categorycdnMap.getNamecn()));
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mappingid");
        try
        {
            tableInfo = getCategoryCntMapQuery(categorycdnMap, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query category from content information given ends...");

        return tableInfo;
    }

    public TableDataInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize,
            PageUtilEntity puEntity) throws Exception
    {
        // TODO Auto-generated method stub
        log.debug("query category from content information given starting...");
        TableDataInfo tableInfo = null;
        try
        {// 本处根据不同的type查询不同的关联关系。
            tableInfo = cmsCategoryDS.getCategoryCntMapQuery(categorycdnMap, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            throw new DomainServiceException(dsEx);
        }
        log.debug("query category from content information given ends...");
        return tableInfo;
    }

    /** ***********************栏目关联海报发布 结束********************** */
}

package com.zte.cms.categorycdn.dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.cms.common.DbUtil;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CategorycdnDAO extends DynamicObjectBaseDao implements ICategorycdnDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCategorycdn(Categorycdn categorycdn) throws DAOException
    {
        log.debug("insert categorycdn starting...");
        super.insert("insertCategorycdn", categorycdn);
        log.debug("insert categorycdn end");
    }

    public void updateCategorycdn(Categorycdn categorycdn) throws DAOException
    {
        log.debug("update categorycdn by pk starting...");
        super.update("updateCategorycdn", categorycdn);
        log.debug("update categorycdn by pk end");
    }

    public void deleteCategorycdn(Categorycdn categorycdn) throws DAOException
    {
        log.debug("delete categorycdn by pk starting...");
        super.delete("deleteCategorycdn", categorycdn);
        log.debug("delete categorycdn by pk end");
    }

    public Categorycdn getCategorycdn(Categorycdn categorycdn) throws DAOException
    {
        log.debug("query categorycdn starting...");
        Categorycdn resultObj = (Categorycdn) super.queryForObject("getCategorycdn", categorycdn);
        log.debug("query categorycdn end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Categorycdn> getCategorycdnByCond(Categorycdn categorycdn) throws DAOException
    {
        log.debug("query categorycdn by condition starting...");
        List<Categorycdn> rList = (List<Categorycdn>) super.queryForList("queryCategorycdnListByCond", categorycdn);
        log.debug("query categorycdn by condition end");
        return rList;
    }
    public List<Categorycdn> selectByCategoryindex(Categorycdn categorycdn) throws DAOException
    {
        log.debug("query categorycdn by condition starting...");
        List<Categorycdn> rList = (List<Categorycdn>) super.queryForList("selectByCategoryindex", categorycdn);
        log.debug("query categorycdn by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize) throws DAOException
    {
        log.debug("page query categorycdn by condition starting...");
        PageInfo pageInfo = null;
        boolean flag = false;
//        if (categorycdn.getCategoryindex() == -1 || categorycdn.getCategoryindex() == 0)
//        {
//            categorycdn.setParentid("-1");
//            int totalCnt = ((Integer) super.queryForObject("queryCategorycdnListCntByCond", categorycdn)).intValue();
//            if (totalCnt > 0)
//            {
//                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
//
//                List<Categorycdn> rsList = (List<Categorycdn>) super.pageQuery("selectByCategoryindex", categorycdn,
//                        start, fetchSize);
//                for(int i=0;i<rsList.size();i++){
//                	Categorycdn cdn = (Categorycdn)rsList.get(i);
//                	if(cdn.getCategoryindex()==0){
//                		rsList.remove(i);//去掉根节点
//                		flag=true;
//                	}
//                }         
//                if(flag==true){
//                	 //totalCnt--;
//                }
//               
//                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
//
//            }
//            else
//            {
//                pageInfo = new PageInfo();
//            }
//
//        }
//        else
//        {
            int totalCnt = ((Integer) super.queryForObject("selectByCategoryindexSingleCnt", categorycdn)).intValue();
            if (totalCnt > 0)
            {
                int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
                List<Categorycdn> rsList = (List<Categorycdn>) super.pageQuery("selectByCategoryindexSingle",
                        categorycdn, start, fetchSize);
                pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
            }
            else
            {
                pageInfo = new PageInfo();
            }
       // }

        log.debug("page query categorycdn by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCategorycdnListByCond", "queryCategorycdnListCntByCond", categorycdn, start,
                pageSize, puEntity);
    }

    /**
     * 展示树
     */
    public List<Categorycdn> listMenuTreeByParentid(String typeid)
    {
        List<Categorycdn> list = null;
        try
        {
            list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategory", typeid);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }
    //全部展开
    public List<Categorycdn> listMenuTreeByParentidcategoryAll(String typeid)
    {
        List<Categorycdn> list = null;
        try
        {
            list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategoryAll", typeid);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }
    
    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex)
    {
        List<Categorycdn> list = null;
        try
        {
            if (parentindex.equals("0"))
            {
                list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategory", parentindex);
            }
            else
            {
                 list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategoryPub", parentindex);
             }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }
    
    /**
     * 展示树发布过的
     * obindex
     */
    public List<Categorycdn> listMenuTreeByParentidPub(String parentindex, String type, String obindex)
    {
        List<Categorycdn> list = null;
        DbUtil db = new DbUtil();
        try
        {
            if (parentindex.equals("0"))
            {
                list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategory", parentindex);
            }
            else
            {
                 list = super.getSqlMapClient().queryForList("listMenuTreeByParentidcategoryPub", parentindex);
               
                if(list!=null&&list.size()>0){
                    for(int i=0;i<list.size();i++){
                        Categorycdn cdn = (Categorycdn)list.get(i);
                        if(type.equals("27")){ // 连续剧
                            String sql = "select count(1) from category_series_map where seriesindex='"+obindex+"'" +
                            		" and categoryindex='"+cdn.getCategoryindex()+"'";
                            try
                            {
                                List listcount = db.getQuery(sql);
                                if(listcount!=null&&listcount.size()>0){
                                    HashMap map = (HashMap)listcount.get(0);
                                    String count = map.get("count(1)").toString();
                                    if(count.equals("0")){
                                        cdn.setIsmapexists(0);//可用
                                    }else{
                                        cdn.setIsmapexists(1);
                                    }
                                }
                                System.out.println(listcount);
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        if(type.equals("26")){ // 内容
                            String sql = "select count(1) from category_program_map where programindex='"+obindex+"'" +
                                    " and categoryindex='"+cdn.getCategoryindex()+"'";
                            try
                            {
                                List listcount = db.getQuery(sql);
                                if(listcount!=null&&listcount.size()>0){
                                    HashMap map = (HashMap)listcount.get(0);
                                    String count = map.get("count(1)").toString();
                                    if(count.equals("0")){
                                        cdn.setIsmapexists(0);//可用
                                    }else{
                                        cdn.setIsmapexists(1);
                                    }
                                }
                                System.out.println(listcount);
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        if(type.equals("28")){ // 频道
                            String sql = "select count(1) from category_channel_map where channelindex='"+obindex+"'" +
                                    " and categoryindex='"+cdn.getCategoryindex()+"'";
                            try
                            {
                                List listcount = db.getQuery(sql);
                                if(listcount!=null&&listcount.size()>0){
                                    HashMap map = (HashMap)listcount.get(0);
                                    String count = map.get("count(1)").toString();
                                    if(count.equals("0")){
                                        cdn.setIsmapexists(0);//可用
                                    }else{
                                        cdn.setIsmapexists(1);
                                    }
                                }
                                System.out.println(listcount);
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        if(type.equals("29")){ // 节目单
                            String sql = "select count(1) from category_schedual_map where schedualindex='"+obindex+"'" +
                                    " and categoryindex='"+cdn.getCategoryindex()+"'";
                            try
                            {
                                List listcount = db.getQuery(sql);
                                if(listcount!=null&&listcount.size()>0){
                                    HashMap map = (HashMap)listcount.get(0);
                                    String count = map.get("count(1)").toString();
                                    if(count.equals("0")){
                                        cdn.setIsmapexists(0);//可用
                                    }else{
                                        cdn.setIsmapexists(1);
                                    }
                                }
                                System.out.println(listcount);
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        if(type.equals("30")){ // 服务
                            String sql = "select count(1) from category_service_map where serviceindex='"+obindex+"'" +
                                    " and categoryindex='"+cdn.getCategoryindex()+"'";
                            try
                            {
                                List listcount = db.getQuery(sql);
                                if(listcount!=null&&listcount.size()>0){
                                    HashMap map = (HashMap)listcount.get(0);
                                    String count = map.get("count(1)").toString();
                                    if(count.equals("0")){
                                        cdn.setIsmapexists(0);//可用
                                    }else{
                                        cdn.setIsmapexists(1);
                                    }
                                }
                                System.out.println(listcount);
                            }
                            catch (Exception e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                }
                 

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public Categorycdn getCmsProgramtypeById(Long id)
    {
        return (Categorycdn) super.queryForObject("getCmsCategoryById", id);
    }

    // 判断是否存在于typeid相同的数据
    public Integer queryByCategoryid(Categorycdn categorycdn)
    {
        log.debug("query by condition starting...");
        List<Categorycdn> list = (List<Categorycdn>) super.queryForList("queryByCategoryid", categorycdn);
        log.debug("query by condition end...");
        return list.size();
    }

    // 判断在同一父节点下是否存在与typename相同的节点
    public Integer queryByCategoryname(Categorycdn categorycdn)
    {
        log.debug("query by condition starting...");
        List<Categorycdn> list = (List<Categorycdn>) super.queryForList("queryByCategoryname", categorycdn);
        log.debug("query by condition end...");
        return list.size();
    }

    /**
     * 判断是否有重复的typeid
     */
    public Categorycdn getCmsCategory(String categoryid)
    {
        return (Categorycdn) super.queryForObject("getCmsCategoryByCategoryid", categoryid);
    }

    public Integer insertCmsCategory(Categorycdn categorycdn) throws DAOException
    {
        log.debug("insert cmsProgramtype starting...");
        int i = 0;
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            
            categorycdn.setCreatetime(dateformat.format(new Date()));
            categorycdn.setStatus(0);
            super.insert("insertCategorycdn", categorycdn);
            i = 1;
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            i = 0;
        }

        log.debug("insert cmsProgramtype end");
        return i;
    }

    public List<Categorycdn> queryChildCmsCategory(Long id)
    {
        List list = (List) super.queryForList("queryChildCmsCategory", id);
        return list;
    }

    public Categorycdn queryByParentid(String typeid)
    {
        return (Categorycdn) super.queryForObject("queryByParentidCategory", typeid);
    }

    // 判断在同一节点下的同名产品分类信息是否拥有相同的typeindex
    public Long queryCategoryindexByCategoryname(Categorycdn categorycdn)
    {
        return (Long) super.queryForObject("queryCategoryindexByCategoryname", categorycdn);
    }

    public Categorycdn getCategoryByCategoryId(Categorycdn categorycdn) throws DAOException
    {
        
        log.debug("query getCategoryByCategoryId starting...");
        Categorycdn resultObj=null;
        resultObj   = (Categorycdn)super.queryForObject( "getCategoryByCategoryId",categorycdn);
        log.debug("query getCategoryByCategoryId end...");
        return resultObj;    
    }

    public PageInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
      //根据内容的不同，查询不同的表
        int cntType = categorycdnMap.getContenttype();
        if(cntType ==4) //连续剧
        {
            return super.indexPageQuery("queryCategoryFromSeriesInfoListByCond", "queryCategoryFromSeriesInfoListCntByCond", categorycdnMap,
                    start, pageSize, puEntity);
        }
        else if(cntType == 3)//Program连续剧
        {
            return super.indexPageQuery("queryCategoryFromProgInfoListByCond", "queryCategoryFromProgInfoListCntByCond", categorycdnMap,
                    start, pageSize, puEntity);
        }
        else if(cntType == 5)  //直播内容
        {
            return  super.indexPageQuery("queryCategoryFromChannelInfoListByCond", "queryCategoryFromChannelInfoListCntByCond", categorycdnMap,
                    start, pageSize, puEntity);
        }
        else
        {
            return null;
        }
    }
}
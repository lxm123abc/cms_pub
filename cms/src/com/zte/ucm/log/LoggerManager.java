package com.zte.ucm.log;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Hashtable;

public class LoggerManager
{
  static final LoggerOutter EMPTYOUTTER = new EmptyLoggerOutter();

  private static final HashSet LOGGERMODELS = new HashSet();

  private static final Hashtable LOGGEROUTTERS = new Hashtable(3);

  private static final Hashtable MODELLEVEL = new Hashtable(10);

  private static final Hashtable MODELOUTTERS = new Hashtable(10);

  public static void addModel(String paramString)
  {
    LOGGERMODELS.add(paramString);
  }

  public static void addModel(String[] paramArrayOfString)
  {
    if ((paramArrayOfString == null) || (paramArrayOfString.length == 0))
    {
      return;
    }
    for (int i = 0; i < paramArrayOfString.length; i++)
    {
      LOGGERMODELS.add(paramArrayOfString[i]);
    }
  }

  public static void removeModel(String paramString)
  {
    LOGGERMODELS.remove(paramString);
  }

  public static void clear()
  {
    LOGGERMODELS.clear();
  }

  public static boolean hasModel(String paramString)
  {
    return LOGGERMODELS.contains(paramString);
  }

  public static HashSet getModels()
  {
    return new HashSet(LOGGERMODELS);
  }

  /** @deprecated */
  public static String[] getDefaultModels()
  {
    Field[] arrayOfField = LoggerModel.class.getDeclaredFields();
    String[] arrayOfString = new String[arrayOfField.length];
    try
    {
      for (int i = 0; i < arrayOfField.length; i++)
      {
        arrayOfString[i] = ((String)arrayOfField[i].get(null));
      }
    }
    catch (Exception localException) {
      return new String[0];
    }
    return arrayOfString;
  }

  public static int getModelLevel(String paramString)
  {
    Integer localInteger = (Integer)MODELLEVEL.get(paramString);
    int i;
    if (localInteger == null)
    {
      i = 0;
    }
    else {
      i = localInteger.intValue();
    }
    return i;
  }

  public static void setModelLevel(String paramString, int paramInt)
  {
    MODELLEVEL.put(paramString, new Integer(paramInt & 0xF));
  }

  public static void addModelLevel(String paramString, int paramInt)
  {
    int i = getModelLevel(paramString);
    i = (i | paramInt) & 0xF;
    MODELLEVEL.put(paramString, new Integer(i));
  }

  public static LoggerOutter getModelLoggerOutter(String paramString)
  {
    if (!hasModel(paramString))
    {
      return EMPTYOUTTER;
    }
    LoggerOutter localLoggerOutter1 = (LoggerOutter)MODELOUTTERS.get(paramString);
    LoggerOutter localLoggerOutter2 = null;
    if (localLoggerOutter1 == null)
    {
      localLoggerOutter2 = EMPTYOUTTER;
    }
    else {
      localLoggerOutter2 = localLoggerOutter1;
    }
    return localLoggerOutter2;
  }

  public static void setModelLoggerOutter(String paramString1, String paramString2)
  {
    Object localObject = LOGGEROUTTERS.get(paramString2);
    if (localObject == null)
    {
      localObject = EMPTYOUTTER;
    }
    MODELOUTTERS.put(paramString1, localObject);
  }

  public static void modelValidate()
  {
    LoggerFactory.validate();
  }

  public static void main(String[] paramArrayOfString)
  {
    System.out.print("");
  }

  static
  {
    FileLoggerOutter localFileLoggerOutter = new FileLoggerOutter();
    LOGGEROUTTERS.put(localFileLoggerOutter.getId(), localFileLoggerOutter);

    String[] arrayOfString = getDefaultModels();
    addModel(arrayOfString);
    for (int i = 0; i < arrayOfString.length; i++)
    {
      setModelLevel(arrayOfString[i], 8);

      setModelLoggerOutter(arrayOfString[i], localFileLoggerOutter.getId());
    }
  }
}
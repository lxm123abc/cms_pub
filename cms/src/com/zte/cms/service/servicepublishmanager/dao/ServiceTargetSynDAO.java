
  package com.zte.cms.service.servicepublishmanager.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class ServiceTargetSynDAO extends DynamicObjectBaseDao implements IServiceTargetSynDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	

    public List<ServiceTargetSyn> getCntTargetsysSynByCond(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
        // TODO Auto-generated method stub
        return null;
    }

    public PageInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceTargetSyn by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceTargetSynListCntByCond",  serviceTargetSyn)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryServiceTargetSynListByCond" ,  serviceTargetSyn , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceTargetSyn by condition end");
        return pageInfo;
    }
//add by xiaoliangjun 2012.11.01
    public PageInfo pageInfoQueryAll(Service service, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceTargetSyn by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryAllServiceTargetSynListCntByCond",  service)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryAllServiceTargetSynListByCond" ,  service , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceTargetSyn by condition end");
        return pageInfo;
    }
    public PageInfo pageInfoQuery(ServiceTargetSyn serviceTargetSyn, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery( "queryProgramTargetSynListByCond", "queryCntTargetsysSynListCntByCond", serviceTargetSyn, start, pageSize, puEntity);
       
    }

    public void updateServiceTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
       
            log.debug("update serviceTargetSyn by pk starting...");
            super.update("updateServiceTargetSyn", serviceTargetSyn);
            log.debug("update serviceTargetSyn by pk end");

            
    }

    public ServiceTargetSyn getServiceTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DomainServiceException
    {
        log.debug("query serviceTargetSyn starting...");
        ServiceTargetSyn resultObj=null;
        resultObj   = (ServiceTargetSyn)super.queryForObject( "getServiceTargetSyn",serviceTargetSyn);
        log.debug("query serviceTargetSyn end...");
        return resultObj;
    }

    public List<ServiceTargetSyn> getServiceTargetSynByCond(ServiceTargetSyn serviceTargetSyn)
            throws DomainServiceException
    {
        log.debug("query serviceTargetSyn list starting...");
       List< ServiceTargetSyn> resultObjList =  (List< ServiceTargetSyn>)super.queryForList( "getServiceTargetSynList",serviceTargetSyn);
        log.debug("query serviceTargetSyn list end...");
        return resultObjList;
        }

    public PageInfo queryforSerProMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query queryforSerProMapTargetBing by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySerProMapTargetSynListCntByCond",  serviceTargetSyn)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "querySerProMapTargetSynListByCond" ,  serviceTargetSyn , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query queryforSerProMapTargetBing by condition end");
        return pageInfo;
    }

    public PageInfo queryforSerSerMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query queryforSerSerMapTargetBing by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySerSerMapTargetSynListCntByCond",  serviceTargetSyn)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "querySerSerMapTargetSynListByCond" ,  serviceTargetSyn , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query queryforSerSerMapTargetBing by condition end");
        return pageInfo;
    }

    public PageInfo queryforSerChaMapTargetBing(ServiceTargetSyn serviceTargetSyn, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query queryforSerChaMapTargetBing by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySerChaMapTargetSynListCntByCond",  serviceTargetSyn)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "querySerChaMapTargetSynListByCond" ,  serviceTargetSyn , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query queryforSerChaMapTargetBing by condition end");
        return pageInfo;
    }

    public ServiceTargetSyn getMappingTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
        log.debug("query getMappingTargetSyn starting...");
        ServiceTargetSyn resultObj=null;
        resultObj   = (ServiceTargetSyn)super.queryForObject( "getMappingTargetSyn",serviceTargetSyn);
        log.debug("query getMappingTargetSyn end...");
        return resultObj;
    }

    public ServiceTargetSyn getObjectTargetSyn(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
        log.debug("query getObjectTargetSyn starting...");
        ServiceTargetSyn resultObj=null;
        resultObj   = (ServiceTargetSyn)super.queryForObject( "getObjectTargetSyn",serviceTargetSyn);
        log.debug("query getObjectTargetSyn end...");
        return resultObj;
    }

    public List<ServiceTargetSyn> getAllServiceTargetSynById(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
        log.debug("query serviceTargetSyn list starting...");
        List< ServiceTargetSyn> resultObjList =  (List< ServiceTargetSyn>)super.queryForList( "getAllServiceTargetSynById",serviceTargetSyn);
         log.debug("query serviceTargetSyn list end...");
         return resultObjList;  
    }

    public List<ServiceTargetSyn> getSameParentTargetMappingSyn(ServiceTargetSyn serviceTargetSyn) throws DAOException
    {
        log.debug("query getSameParentTargetMappingSyn list starting...");
        List< ServiceTargetSyn> resultObjList =  (List< ServiceTargetSyn>)super.queryForList( "getSameParentTargetMappingSyn",serviceTargetSyn);
         log.debug("query getSameParentTargetMappingSyn list end...");
         return resultObjList; 
    }
} 
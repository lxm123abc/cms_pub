package com.zte.cms.EMBtask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsEmbtask extends DynamicBaseObject
{
    private java.lang.Long taskindex;
    private java.lang.String taskid;
    private java.lang.Integer sourcetype;
    private java.lang.Long sourceindex;
    private java.lang.Long contentindex;
    private java.lang.Long subcontentindex;
    private java.lang.Integer priority;
    private java.lang.Integer status;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer tasktype;
    private java.lang.Integer mdfivegenerateoption;
    private java.lang.Long templateindex;
    private java.lang.Integer srcfilegrouptype;
    private java.lang.String srcfilephyfilename;
    private java.lang.Integer srcfilestoragetype;
    private java.lang.String srcfilepath;
    private java.lang.Integer destfilegrouptype;
    private java.lang.String destfilephyfilename;
    private java.lang.Integer destfilestoragetype;
    private java.lang.String destfilepath;
    private java.lang.Long destfilesize;
    private java.lang.Long destfilelength;
    private java.lang.String resultdesc;
    private java.lang.String fileverifycode;
    private java.lang.String sourcefilename;
    private java.lang.String moviename;
    private java.lang.Integer worktype;
    private java.lang.Integer filetranstype;
    private java.lang.Integer filetype;
    private java.lang.String embRule;
    private java.lang.String drmUrl;
    private java.lang.String logaddress;
    private java.lang.String taskname;  
    private java.lang.String callbackUrl;      
    private java.lang.String resType;
    private java.lang.Long hlsTargetDuration;
    private java.lang.String hlsIndexFilename;    
    
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    private java.lang.String endtime1;
    private java.lang.String endtime2;    
    
    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.String getTaskid()
    {
        return taskid;
    }

    public void setTaskid(java.lang.String taskid)
    {
        this.taskid = taskid;
    }

    public java.lang.Integer getSourcetype()
    {
        return sourcetype;
    }

    public void setSourcetype(java.lang.Integer sourcetype)
    {
        this.sourcetype = sourcetype;
    }

    public java.lang.Long getSourceindex()
    {
        return sourceindex;
    }

    public void setSourceindex(java.lang.Long sourceindex)
    {
        this.sourceindex = sourceindex;
    }

    public java.lang.Long getContentindex()
    {
        return contentindex;
    }

    public void setContentindex(java.lang.Long contentindex)
    {
        this.contentindex = contentindex;
    }

    public java.lang.Long getSubcontentindex()
    {
        return subcontentindex;
    }

    public void setSubcontentindex(java.lang.Long subcontentindex)
    {
        this.subcontentindex = subcontentindex;
    }

    public java.lang.Integer getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.Integer priority)
    {
        this.priority = priority;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getTasktype()
    {
        return tasktype;
    }

    public void setTasktype(java.lang.Integer tasktype)
    {
        this.tasktype = tasktype;
    }

    public java.lang.Long getTemplateindex()
    {
        return templateindex;
    }

    public void setTemplateindex(java.lang.Long templateindex)
    {
        this.templateindex = templateindex;
    }

    public java.lang.Integer getSrcfilegrouptype()
    {
        return srcfilegrouptype;
    }

    public void setSrcfilegrouptype(java.lang.Integer srcfilegrouptype)
    {
        this.srcfilegrouptype = srcfilegrouptype;
    }

    public java.lang.String getSrcfilephyfilename()
    {
        return srcfilephyfilename;
    }

    public void setSrcfilephyfilename(java.lang.String srcfilephyfilename)
    {
        this.srcfilephyfilename = srcfilephyfilename;
    }

    public java.lang.Integer getSrcfilestoragetype()
    {
        return srcfilestoragetype;
    }

    public void setSrcfilestoragetype(java.lang.Integer srcfilestoragetype)
    {
        this.srcfilestoragetype = srcfilestoragetype;
    }

    public java.lang.String getSrcfilepath()
    {
        return srcfilepath;
    }

    public void setSrcfilepath(java.lang.String srcfilepath)
    {
        this.srcfilepath = srcfilepath;
    }

    public java.lang.Integer getDestfilegrouptype()
    {
        return destfilegrouptype;
    }

    public void setDestfilegrouptype(java.lang.Integer destfilegrouptype)
    {
        this.destfilegrouptype = destfilegrouptype;
    }

    public java.lang.String getDestfilephyfilename()
    {
        return destfilephyfilename;
    }

    public void setDestfilephyfilename(java.lang.String destfilephyfilename)
    {
        this.destfilephyfilename = destfilephyfilename;
    }

    public java.lang.Integer getDestfilestoragetype()
    {
        return destfilestoragetype;
    }

    public void setDestfilestoragetype(java.lang.Integer destfilestoragetype)
    {
        this.destfilestoragetype = destfilestoragetype;
    }

    public java.lang.String getDestfilepath()
    {
        return destfilepath;
    }

    public void setDestfilepath(java.lang.String destfilepath)
    {
        this.destfilepath = destfilepath;
    }

    public java.lang.Long getDestfilesize()
    {
        return destfilesize;
    }

    public void setDestfilesize(java.lang.Long destfilesize)
    {
        this.destfilesize = destfilesize;
    }

    public java.lang.Long getDestfilelength()
    {
        return destfilelength;
    }

    public void setDestfilelength(java.lang.Long destfilelength)
    {
        this.destfilelength = destfilelength;
    }

    public java.lang.String getResultdesc()
    {
        return resultdesc;
    }

    public void setResultdesc(java.lang.String resultdesc)
    {
        this.resultdesc = resultdesc;
    }

    public java.lang.String getFileverifycode()
    {
        return fileverifycode;
    }

    public void setFileverifycode(java.lang.String fileverifycode)
    {
        this.fileverifycode = fileverifycode;
    }

    public void initRelation()
    {
        this.addRelation("taskindex", "TASKINDEX");
        this.addRelation("taskid", "TASKID");
        this.addRelation("sourcetype", "SOURCETYPE");
        this.addRelation("sourceindex", "SOURCEINDEX");
        this.addRelation("contentindex", "CONTENTINDEX");
        this.addRelation("subcontentindex", "SUBCONTENTINDEX");
        this.addRelation("priority", "PRIORITY");
        this.addRelation("status", "STATUS");
        this.addRelation("starttime", "STARTTIME");
        this.addRelation("endtime", "ENDTIME");
        this.addRelation("tasktype", "TASKTYPE");
        this.addRelation("templateindex", "TEMPLATEINDEX");
        this.addRelation("srcfilegrouptype", "SRCFILEGROUPTYPE");
        this.addRelation("srcfilephyfilename", "SRCFILEPHYFILENAME");
        this.addRelation("srcfilestoragetype", "SRCFILESTORAGETYPE");
        this.addRelation("srcfilepath", "SRCFILEPATH");
        this.addRelation("destfilegrouptype", "DESTFILEGROUPTYPE");
        this.addRelation("destfilephyfilename", "DESTFILEPHYFILENAME");
        this.addRelation("destfilestoragetype", "DESTFILESTORAGETYPE");
        this.addRelation("destfilepath", "DESTFILEPATH");
        this.addRelation("destfilesize", "DESTFILESIZE");
        this.addRelation("destfilelength", "DESTFILELENGTH");
        this.addRelation("resultdesc", "RESULTDESC");
        this.addRelation("fileverifycode", "FILEVERIFYCODE");
        this.addRelation("sourcefilename", "SOURCEFILENAME");
        this.addRelation("moviename", "MOVIENAME");
        this.addRelation("worktype", "WORKTYPE");
        this.addRelation("filetranstype", "FILETRANSTYPE");
        this.addRelation("filetype", "FILETYPE");
        this.addRelation("embRule", "EMBRULE");
        this.addRelation("drmUrl", "DRMURL");
        this.addRelation("logaddress", "LOGADDRESS");
        this.addRelation("taskname", "TASKNAME");
        this.addRelation("callbackUrl", "CALLBACKURL");        
        this.addRelation("resType", "RESTYPE");
        this.addRelation("hlsTargetDuration", "HLSTARGETDURATION");
        this.addRelation("hlsIndexFilename", "HLSINDEXFILENAME");        
    }

    public java.lang.Integer getMdfivegenerateoption()
    {
        return mdfivegenerateoption;
    }

    public void setMdfivegenerateoption(java.lang.Integer mdfivegenerateoption)
    {
        this.mdfivegenerateoption = mdfivegenerateoption;
    }

    public java.lang.String getSourcefilename()
    {
        return sourcefilename;
    }

    public void setSourcefilename(java.lang.String sourcefilename)
    {
        this.sourcefilename = sourcefilename;
    }

	public java.lang.String getMoviename()
	{
		return moviename;
	}

	public void setMoviename(java.lang.String moviename)
	{
		this.moviename = moviename;
	}
	public java.lang.Integer getWorktype()
	{
		return worktype;
	}

	public void setWorktype(java.lang.Integer worktype)
	{
		this.worktype = worktype;
	}

	public java.lang.Integer getFiletranstype()
	{
		return filetranstype;
	}

	public void setFiletranstype(java.lang.Integer filetranstype)
	{
		this.filetranstype = filetranstype;
	}

	public java.lang.Integer getFiletype()
	{
		return filetype;
	}

	public void setFiletype(java.lang.Integer filetype)
	{
		this.filetype = filetype;
	}

	public java.lang.String getEmbRule()
	{
		return embRule;
	}

	public void setEmbRule(java.lang.String embRule)
	{
		this.embRule = embRule;
	}



	public java.lang.String getLogaddress()
	{
		return logaddress;
	}

	public void setLogaddress(java.lang.String logaddress)
	{
		this.logaddress = logaddress;
	}

	public java.lang.String getTaskname()
	{
		return taskname;
	}

	public void setTaskname(java.lang.String taskname)
	{
		this.taskname = taskname;
	}

	public java.lang.String getDrmUrl()
	{
		return drmUrl;
	}

	public void setDrmUrl(java.lang.String drmUrl)
	{
		this.drmUrl = drmUrl;
	}

	public java.lang.String getCallbackUrl()
	{
		return callbackUrl;
	}

	public void setCallbackUrl(java.lang.String callbackUrl)
	{
		this.callbackUrl = callbackUrl;
	}

	public java.lang.String getResType()
	{
		return resType;
	}

	public void setResType(java.lang.String resType)
	{
		this.resType = resType;
	}



	public java.lang.String getHlsIndexFilename()
	{
		return hlsIndexFilename;
	}

	public void setHlsIndexFilename(java.lang.String hlsIndexFilename)
	{
		this.hlsIndexFilename = hlsIndexFilename;
	}
	public java.lang.String getStarttime1()
	{
		return starttime1;
	}

	public void setStarttime1(java.lang.String starttime1)
	{
		this.starttime1 = starttime1;
	}

	public java.lang.String getStarttime2()
	{
		return starttime2;
	}

	public void setStarttime2(java.lang.String starttime2)
	{
		this.starttime2 = starttime2;
	}

	public java.lang.String getEndtime1()
	{
		return endtime1;
	}

	public void setEndtime1(java.lang.String endtime1)
	{
		this.endtime1 = endtime1;
	}

	public java.lang.String getEndtime2()
	{
		return endtime2;
	}

	public void setEndtime2(java.lang.String endtime2)
	{
		this.endtime2 = endtime2;
	}

	public java.lang.Long getHlsTargetDuration()
	{
		return hlsTargetDuration;
	}

	public void setHlsTargetDuration(java.lang.Long hlsTargetDuration)
	{
		this.hlsTargetDuration = hlsTargetDuration;
	}
}

package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ChannelWkfwhisDAO extends DynamicObjectBaseDao implements IChannelWkfwhisDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DAOException
    {
        log.debug("insert channelWkfwhis starting...");
        super.insert("insertChannelWkfwhis", channelWkfwhis);
        log.debug("insert channelWkfwhis end");
    }

    public void updateChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DAOException
    {
        log.debug("update channelWkfwhis by pk starting...");
        super.update("updateChannelWkfwhis", channelWkfwhis);
        log.debug("update channelWkfwhis by pk end");
    }

    public void deleteChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DAOException
    {
        log.debug("delete channelWkfwhis by pk starting...");
        super.delete("deleteChannelWkfwhis", channelWkfwhis);
        log.debug("delete channelWkfwhis by pk end");
    }

    public ChannelWkfwhis getChannelWkfwhis(ChannelWkfwhis channelWkfwhis) throws DAOException
    {
        log.debug("query channelWkfwhis starting...");
        ChannelWkfwhis resultObj = (ChannelWkfwhis) super.queryForObject("getChannelWkfwhis", channelWkfwhis);
        log.debug("query channelWkfwhis end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ChannelWkfwhis> getChannelWkfwhisByCond(ChannelWkfwhis channelWkfwhis) throws DAOException
    {
        log.debug("query channelWkfwhis by condition starting...");
        List<ChannelWkfwhis> rList = (List<ChannelWkfwhis>) super.queryForList("queryChannelWkfwhisListByCond",
                channelWkfwhis);
        log.debug("query channelWkfwhis by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ChannelWkfwhis channelWkfwhis, int start, int pageSize) throws DAOException
    {
        log.debug("page query channelWkfwhis by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryChannelWkfwhisListCntByCond", channelWkfwhis)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ChannelWkfwhis> rsList = (List<ChannelWkfwhis>) super.pageQuery("queryChannelWkfwhisListByCond",
                    channelWkfwhis, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query channelWkfwhis by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ChannelWkfwhis channelWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryChannelWkfwhisListByCond", "queryChannelWkfwhisListCntByCond",
                channelWkfwhis, start, pageSize, puEntity);
    }

}
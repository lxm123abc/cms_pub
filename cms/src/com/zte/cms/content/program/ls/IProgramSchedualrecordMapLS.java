package com.zte.cms.content.program.ls;

import java.util.List;

import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface IProgramSchedualrecordMapLS
{
    public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)
    throws Exception;
    
    public String insertProgramSchedualrecordMap(String schedualrecordIdList, String programindex);

    public String deleteProgramSchedualrecordMap(Long mapindex)throws Exception;
    
    public String publishPsrMap(Long mapindex)throws Exception;
    
    public String cancelPublishPsrMap(Long mapindex)throws Exception;
    
    public String batchDeletePsm(List<ProgramSchedualrecordMap> list) ;
    
    public String batchPublishOrDelPublish(List<ProgramSchedualrecordMap> list, String type);
}

package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ssb.framework.base.util.PageInfo;

import java.util.List;

public interface SettlementDataRptMapper {

    void deleteByPrimaryKey(SettlementDataRptVO record);

    void insert(SettlementDataRpt record);

    int selectCountById(SettlementDataRpt record);

    void update(SettlementDataRpt record);

    void updateIsConfirmById(SettlementDataRptVO record);

    SettlementDataRpt selectSettlementDataRptById(SettlementDataRptVO record);

    List<String> getProjectType();

    PageInfo selectSettlementDataRpt(SettlementDataRptVO settlementDataRptVO);

}
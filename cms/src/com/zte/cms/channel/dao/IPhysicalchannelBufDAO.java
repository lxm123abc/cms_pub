package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IPhysicalchannelBufDAO
{
     /**
	  * 新增PhysicalchannelBuf对象 
	  * 
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @throws DAOException dao异常
	  */	
     public void insertPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException;
     
     /**
	  * 根据主键更新PhysicalchannelBuf对象
	  * 
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @throws DAOException dao异常
	  */
     public void updatePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException;


     /**
	  * 根据主键删除PhysicalchannelBuf对象
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @throws DAOException dao异常
	  */
     public void deletePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException;
     
     
     /**
	  * 根据主键查询PhysicalchannelBuf对象
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @return 满足条件的PhysicalchannelBuf对象
	  * @throws DAOException dao异常
	  */
     public PhysicalchannelBuf getPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException;
     
     /**
	  * 根据条件查询PhysicalchannelBuf对象 
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象
	  * @return 满足条件的PhysicalchannelBuf对象集
	  * @throws DAOException dao异常
	  */
     public List<PhysicalchannelBuf> getPhysicalchannelBufByCond( PhysicalchannelBuf physicalchannelBuf )throws DAOException;

     /**
	  * 根据条件分页查询PhysicalchannelBuf对象，作为查询条件的参数
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询PhysicalchannelBuf对象，作为查询条件的参数
	  *
	  * @param physicalchannelBuf PhysicalchannelBuf对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
        
     public void deletePhysicalchannelBufByChId( PhysicalchannelBuf physicalchannelBuf )throws DAOException;
   
}
package com.zte.cms.impSyncTask.dao;

import java.util.List;

import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IImpCmsSynctaskDAO
{
     /**
   * 新增ImpCmsSynctask对象 
   * 
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */ 
     public void insertImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException;
     /**
   * 新增ImpCmsSynctask对象 
   * 
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */ 
     public void insertImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException;
     /**
   * 根据主键更新ImpCmsSynctask对象
   * 
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */
     public void updateImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException;
     /**
   * 根据主键更新ImpCmsSynctask对象
   * 
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */
     public void updateImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException;

     /**
   * 根据条件更新ImpCmsSynctask对象  
   *
   * @param impCmsSynctask ImpCmsSynctask更新条件
   * @throws DAOException dao异常
   */
     public void updateImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException;
     /**
   * 根据条件更新ImpCmsSynctask对象  
   *
   * @param impCmsSynctask ImpCmsSynctask更新条件
   * @throws DAOException dao异常
   */
     public void updateImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException;

     /**
   * 根据主键删除ImpCmsSynctask对象
   *
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */
     public void deleteImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException;	
     /**
   * 根据主键删除ImpCmsSynctask对象
   *
   * @param impCmsSynctask ImpCmsSynctask对象
   * @throws DAOException dao异常
   */
     public void deleteImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException;
     
     /**
   * 根据条件删除ImpCmsSynctask对象
   *
   * @param impCmsSynctask ImpCmsSynctask删除条件
   * @throws DAOException dao异常
   */ 
     public void deleteImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException;
     /**
   * 根据条件删除ImpCmsSynctask对象
   *
   * @param impCmsSynctask ImpCmsSynctask删除条件
   * @throws DAOException dao异常
   */ 
     public void deleteImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DAOException;
     
     /**
   * 根据主键查询ImpCmsSynctask对象
   *
   * @param impCmsSynctask ImpCmsSynctask对象
   * @return 满足条件的ImpCmsSynctask对象
   * @throws DAOException dao异常
   */
     public ImpCmsSynctask getImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DAOException;
     
     /**
   * 根据条件查询ImpCmsSynctask对象 
   *
   * @param impCmsSynctask ImpCmsSynctask对象
   * @return 满足条件的ImpCmsSynctask对象集
   * @throws DAOException dao异常
   */
     public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DAOException;


     public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask, PageUtilEntity puEntity )throws DAOException;

     /**
   * 根据条件分页查询ImpCmsSynctask对象，作为查询条件的参数
   *
   * @param impCmsSynctask ImpCmsSynctask对象 
   * @param start 起始行
   * @param pageSize 页面大小
   * @return  查询结果
   * @throws DAOException dao异常
   */
     public PageInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize)throws DAOException;
     
     /**
   * 根据条件分页查询ImpCmsSynctask对象，作为查询条件的参数
   *
   * @param impCmsSynctask ImpCmsSynctask对象 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DAOException dao异常
   */
     public PageInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
        
}
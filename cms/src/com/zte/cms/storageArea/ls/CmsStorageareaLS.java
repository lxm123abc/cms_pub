package com.zte.cms.storageArea.ls;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageArea.service.ICmsStorageareaDS;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.cms.storageManage.service.ICmsStoragemanageDS;
import com.zte.ismp.common.util.StrUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

public class CmsStorageareaLS implements ICmsStorageareaLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsStorageareaDS ds = null;
    private ICmsStoragemanageDS spaceds = null;
    private storageareaConstants constrangeSrc = new storageareaConstants();

    public void setSpaceds(ICmsStoragemanageDS spaceds)
    {
        this.spaceds = spaceds;
    }

    public void setDs(ICmsStorageareaDS ds)
    {
        this.ds = ds;
    }

    public String insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        int hasSameID = 0;
        long parentIndex = 0L; // 父节点Index
        parentIndex = cmsStoragearea.getNumbackup1();
        CmsStoragearea parentArea = new CmsStoragearea();
        parentArea.setAreaindex(parentIndex);
        // 得到其他的库区
        List<CmsStoragearea> otherAreas = getCmsStorageareaByCond(parentArea);

        // 判断是否存在相同的标识
        for (int i = 0; i < otherAreas.size(); i++)
        {
            if (otherAreas.get(i).getAreaid().equals(cmsStoragearea.getAreaid()))
            {
                hasSameID = 1;
            }
        }
        if (hasSameID == 1) // 存在相同的标识
        {
            return "14";
        }
        else
        {
            log.debug("insert cmsStoragearea starting...");
            try
            {
                ds.insertCmsStoragearea(cmsStoragearea);
            }
            catch (DomainServiceException dSEx)
            {
                log.error("ds exception:", dSEx);
                // TODO 根据实际应用，可以在此处添加异常国际化处理
                throw new DomainServiceException(dSEx);
            }
            log.debug("insert cmsStoragearea end");

            return "15";
        }

    }

    public String updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        // 首先判断已经使用的百分比是否大于想要修改的百分比，如果已经大于，则提示不能修改
        CmsStoragearea storageCheck = new CmsStoragearea();
        storageCheck.setAreaindex(cmsStoragearea.getAreaindex());
        storageCheck = ds.getCmsStoragearea(storageCheck);
        if (Double.parseDouble(storageCheck.getStoragepercentage()) > Double.parseDouble(cmsStoragearea
                .getMonitorthreshold()))
        {
            return "31";
        }

        log.debug("update cmsStoragearea by pk starting...");
        try
        {
            ds.updateCmsStoragearea(cmsStoragearea);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("update cmsStoragearea by pk end");

        // 写日志
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.storagearea.manage.mgt"); // 库区管理
        String optObject = String.valueOf(cmsStoragearea.getAreaindex());
        String optObjecttype = ResourceManager.getResourceText("log.update.area"); // 库区修改

        OperInfo loginUser = LoginMgt.getOperInfo("CMS");
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.detail.update.space"); // 操作员[opername]修改了库区[areaindex]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[areaindex\\]", String.valueOf(cmsStoragearea.getAreaindex()));
        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey("CMS");

        LogInfoMgt.doLog(loginfo);

        return "20";
    }

    public void updateCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException
    {
        log.debug("update cmsStorageareaList by pk starting...");
        if (null == cmsStorageareaList || cmsStorageareaList.size() == 0)
        {
            log.debug("there is no datas in cmsStorageareaList");
            return;
        }
        try
        {
            for (CmsStoragearea cmsStoragearea : cmsStorageareaList)
            {
                ds.updateCmsStoragearea(cmsStoragearea);
            }
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("update cmsStorageareaList by pk end");
    }

    public void removeCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("remove cmsStoragearea by pk starting...");
        try
        {
            ds.removeCmsStoragearea(cmsStoragearea);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("remove cmsStoragearea by pk end");
    }

    public void removeCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException
    {
        log.debug("remove cmsStorageareaList by pk starting...");
        if (null == cmsStorageareaList || cmsStorageareaList.size() == 0)
        {
            log.debug("there is no datas in cmsStorageareaList");
            return;
        }
        try
        {
            for (CmsStoragearea cmsStoragearea : cmsStorageareaList)
            {
                ds.removeCmsStorageareaList(cmsStorageareaList);
            }
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("remove cmsStorageareaList by pk end");
    }

    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("get cmsStoragearea by pk starting...");
        CmsStoragearea rsObj = null;
        try
        {
            rsObj = ds.getCmsStoragearea(cmsStoragearea);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsStorageareaList by pk end");
        return rsObj;
    }

    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DomainServiceException
    {
        log.debug("get cmsStoragearea by condition starting...");
        List<CmsStoragearea> rsList = null;
        try
        {
            rsList = ds.getCmsStorageareaByCond(cmsStoragearea);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get cmsStoragearea by condition end");
        return rsList;
    }

    // 查询该库区下已经绑定的空间
    public TableDataInfo pageInfoQueryofBindedSpace(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get binded space page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = spaceds.pageInfoQueryPriority(cmsStoragemanage, start, pageSize);
        }
        catch (DomainServiceException spacedsEx)
        {
            log.error("spaceds exception:", spacedsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(spacedsEx);
        }
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsStoragearea page info by condition starting...");
        TableDataInfo pageInfo = null;
        try
        {
            pageInfo = ds.pageInfoQuery(cmsStoragearea, start, pageSize);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }

        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsStoragearea page info by condition starting...");
        TableDataInfo pageInfo = null;
        try
        {
            pageInfo = ds.pageInfoQuery(cmsStoragearea, start, pageSize, puEntity);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }

        return pageInfo;
    }

    // 绑定空间时，修改空间的相应字段storageArea
    public Map updatebindedSpaceArea(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(storageareaConstants.CMS_STORAGEAREA_RES_FILE_NAME);
        Map map = new HashMap();
        String rtn = "";
        log.debug("update the storageArea of binded spaces  page info by condition starting...");
        // 首先判断此空间是否已经不存在或者被绑定
        CmsStoragemanage spaceifBinded = new CmsStoragemanage();
        spaceifBinded.setStorageindex(cmsStoragemanage.getStorageindex());
        spaceifBinded = spaceds.getCmsStoragemanage(spaceifBinded);
        if (spaceifBinded == null)
        {
            rtn = ResourceMgt.findDefaultText(storageareaConstants.CMS_STORAGEAREA_MSG_SPACE_DOESNOT_EXIST);
            map.put("rtn", rtn);
            map.put("cmsStoragemanage", null);
            return map;
        }
        if (!spaceifBinded.getStoragearea().equals("0"))
        {

            rtn = ResourceMgt.findDefaultText(storageareaConstants.CMS_STORAGEAREA_MSG_SPACE_HAS_BINDED);
            map.put("rtn", rtn);
            map.put("cmsStoragemanage", null);
            return map;
        }
        try
        {
            spaceds.updateCmsStoragemanage(cmsStoragemanage);
        }
        catch (DomainServiceException spacedsEx)
        {
            log.error("spaceds exception:", spacedsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(spacedsEx);
        }

        log.debug("update the storageArea of binded spaces  page info by condition ends" + "...");
        rtn = "16";
        map.put("cmsStoragemanage", cmsStoragemanage);
        map.put("rtn", rtn);

        // 写日志
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        // 操作对象类型
        String optType = ResourceManager.getResourceText("log.storagearea.manage.mgt"); // 库区管理
        String optObject = String.valueOf(cmsStoragemanage.getStorageindex());
        String optObjecttype = ResourceManager.getResourceText("log.bind.new.space"); // 绑定空间

        OperInfo loginUser = LoginMgt.getOperInfo("CMS");
        // 操作员编码
        String operId = loginUser.getOperid();
        // 操作员名称
        String operName = loginUser.getUserId();

        String optDetail = ResourceManager.getResourceText("log.detail.bind.new.space"); // 操作员[opername]绑定了空间[storageindex]到库区[areaindex]
        optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
        optDetail = optDetail.replaceAll("\\[storageindex\\]", String.valueOf(cmsStoragemanage.getStorageindex()));
        optDetail = optDetail.replaceAll("\\[areaindex\\]", cmsStoragemanage.getStoragearea());
        loginfo.setUserId(operId);
        loginfo.setOptType(optType);
        loginfo.setOptObject(optObject);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptDetail(optDetail);
        loginfo.setOptTime("");
        loginfo.setServicekey("CMS");

        LogInfoMgt.doLog(loginfo);

        return map;

    }

    // 空间的去绑定，首先判断绑定库区的类型，如果库区类型为5,（模板存储区），则不允许去绑定
    // 如果不是，则判断该空间是否为空，不为空，则不可去绑定，为空，则可以
    public String releasebindedSpace(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        ResourceMgt.addDefaultResourceBundle(storageareaConstants.CMS_STORAGEAREA_RES_FILE_NAME);
        // ResourceMgt.addDefaultResourceBundle(storageareaConstants.CMS_STORAGEAREA_RES_FILE_NAME);
        CmsStoragemanage ifspacereleased = null;
        ifspacereleased = spaceds.getCmsStoragemanage(cmsStoragemanage);
        if (null == ifspacereleased)
        {
            return ResourceMgt.findDefaultText(storageareaConstants.CMS_STORAGEAREA_MSG_SPACE_DOESNOT_EXIST);
        }
        if (!ifspacereleased.getStoragearea().equals(cmsStoragemanage.getStoragearea()))
        {
            return ResourceMgt.findDefaultText(storageareaConstants.CMS_STORAGEAREA_MSG_SPACE_RELEASEED);
        }
        log.debug("release the binded spaces starting...");
        CmsStoragemanage spaceInfo = spaceds.getCmsStoragemanage(cmsStoragemanage); // 得到该空间的信息
        // System.out.println(spaceInfo.getStoragearea());
        // 判断库区的类型
        if (spaceInfo.getStoragearea().equals("5"))
        {
            return "21";

        }
        
        String spaceID = spaceInfo.getStorageid(); // 得到该空间的标识，以拼接路径
        CmsStoragemanage parentIndex = new CmsStoragemanage(); // 定义一个设备
        parentIndex.setStorageindex(spaceInfo.getParentindex());
        CmsStoragemanage parentDeviceInfo = spaceds.getCmsStoragemanage(parentIndex);
        String deviceId = parentDeviceInfo.getStorageid();
        String path = "";

        path = GlobalConstants.getMountPoint() + deviceId + File.separator + spaceID; // 拼接空间路径

        // 判断空间下是否有文件或者文件夹
        File f = new File(path);
        if (!f.exists())
        {
            return "25";
        }
        // 如果该文件夹不为空，说明空间不为空

        if (f != null && f.list().length > 0)
        {
            return "17";
        }
        // 如果空间下没有文件或者文件夹，则取绑定
        else
        {
            String area = cmsStoragemanage.getStoragearea();
            cmsStoragemanage.setStoragearea("0");
            spaceds.updateCmsStoragemanage(cmsStoragemanage);

            log.debug("release the binded spaces finished successfully...");

            // 写日志
            AppLogInfoEntity loginfo = new AppLogInfoEntity();
            // 操作对象类型
            String optType = ResourceManager.getResourceText("log.storagearea.manage.mgt"); // 库区管理
            String optObject = String.valueOf(cmsStoragemanage.getStorageindex());
            String optObjecttype = ResourceManager.getResourceText("log.release.binded.space"); // 空间去绑定

            OperInfo loginUser = LoginMgt.getOperInfo("CMS");
            // 操作员编码
            String operId = loginUser.getOperid();
            // 操作员名称
            String operName = loginUser.getUserId();

            String optDetail = ResourceManager.getResourceText("log.optrelase.binded.space"); // 操作员[opername]去绑定了空间[storageindex]

            optDetail = optDetail.replaceAll("\\[opername\\]", StrUtil.filterDollarStr(operName));
            optDetail = optDetail.replaceAll("\\[storageindex\\]", String.valueOf(cmsStoragemanage.getStorageindex()));
            optDetail = optDetail.replaceAll("\\[areaindex\\]", area);
            loginfo.setUserId(operId);
            loginfo.setOptType(optType);
            loginfo.setOptObject(optObject);
            loginfo.setOptObjecttype(optObjecttype);
            loginfo.setOptDetail(optDetail);
            loginfo.setOptTime("");
            loginfo.setServicekey("CMS");

            LogInfoMgt.doLog(loginfo);

            return "30";
        }
    }

    // 给定库区的类型，获取设备的ID
    public String getDeviceID(String storageAreatype) throws DomainServiceException
    {
        String path = ""; // 将要返回的地址
        String spaceID = ""; // 保存最大空间的id
        String deviceID = ""; // 保存最大空间的id
        // 首先通过库区的类型得到绑定空间列表
        CmsStoragemanage areaTypeStorage = new CmsStoragemanage();
        areaTypeStorage.setStoragearea(storageAreatype);
        List<CmsStoragemanage> bindedSpaceList = spaceds.getCmsStoragemanageofbindedSpace(areaTypeStorage);
        // System.out.println("the length of bindedSpaceList" +bindedSpaceList.size() );

        // 取出符合条件的第一个空间
        CmsStoragemanage sutibleSpace = new CmsStoragemanage();
        if (bindedSpaceList.size() > 0)
        {
            sutibleSpace = bindedSpaceList.get(0);
        }
        else
        {
            return "1056020";
        }
        spaceID = sutibleSpace.getStorageid();
        // 定义一个新节点，保存父设备的信息，得到设备的id
        CmsStoragemanage parentDevice = new CmsStoragemanage();
        parentDevice.setStorageindex(sutibleSpace.getParentindex());
        parentDevice = spaceds.getCmsStoragemanage(parentDevice);
        deviceID = parentDevice.getStorageid();

        path = deviceID;
        return path;
    }

    // 给定库区的类型，首先通过空间的优先级进行排序，然后通过空间的容量进行排序，进行选择
    public String getAddress(String storageAreatype) throws DomainServiceException
    {
        String path = ""; // 将要返回的地址
        String spaceID = ""; // 保存最大空间的id
        String deviceID = ""; // 保存最大空间的id
        // 首先通过库区的类型得到绑定空间列表
        CmsStoragemanage areaTypeStorage = new CmsStoragemanage();
        areaTypeStorage.setStoragearea(storageAreatype);
        List<CmsStoragemanage> bindedSpaceList = spaceds.getCmsStoragemanageofbindedSpace(areaTypeStorage);
        // System.out.println("the length of bindedSpaceList" +bindedSpaceList.size() );

        // 取出符合条件的第一个空间
        CmsStoragemanage sutibleSpace = new CmsStoragemanage();
        if (bindedSpaceList.size() > 0)
        {
            sutibleSpace = bindedSpaceList.get(0);
        }
        else
        {
            return "1056020";
        }
        spaceID = sutibleSpace.getStorageid();
        // 定义一个新节点，保存父设备的信息，得到设备的id
        CmsStoragemanage parentDevice = new CmsStoragemanage();
        parentDevice.setStorageindex(sutibleSpace.getParentindex());
        parentDevice = spaceds.getCmsStoragemanage(parentDevice);
        deviceID = parentDevice.getStorageid();

        path = deviceID + File.separator + spaceID;
        return path;
    }

    // 为了配合收录和拆条获取在线区地址，重新写一个只返回空间标识的方法
    public String getSpaceID(String storageAreatype) throws DomainServiceException
    {
        String path = ""; // 将要返回的地址
        String spaceID = ""; // 保存最大空间的id
        // String deviceID = "" ; //
        // 首先通过库区的类型得到绑定空间列表
        CmsStoragemanage areaTypeStorage = new CmsStoragemanage();
        areaTypeStorage.setStoragearea(storageAreatype);
        List<CmsStoragemanage> bindedSpaceList = spaceds.getCmsStoragemanageofbindedSpace(areaTypeStorage);
        // System.out.println("the length of bindedSpaceList" +bindedSpaceList.size() );

        // 取出符合条件的第一个空间
        CmsStoragemanage sutibleSpace = new CmsStoragemanage();
        if (bindedSpaceList.size() > 0)
        {
            sutibleSpace = bindedSpaceList.get(0);
        }
        else
        {
            return "1056020";
        }
        spaceID = sutibleSpace.getStorageid();
        // 定义一个新节点，保存父设备的信息，得到设备的id
        // CmsStoragemanage parentDevice = new CmsStoragemanage();
        // parentDevice.setStorageindex(sutibleSpace.getParentindex());
        // parentDevice = spaceds.getCmsStoragemanage(parentDevice);
        // deviceID = parentDevice.getStorageid();

        path = spaceID;
        return path;
    }

    // 绑定之前对库区的检查，是否为全国文件交换区或者为CP上传交换区，如果为二者之一的话，检查是否已经有绑定关系
    public String bindCheckForTwoAreas(CmsStoragemanage cmsStoragemanage) throws DomainServiceException
    {
        List<CmsStoragemanage> list = spaceds.getCmsStoragemanageByCond(cmsStoragemanage);
        // 判断是否已经有一个绑定，
        if(list!=null&&list.size()>0){
        	 // 如果有一个的话，那么判断其类型是否为5
            if ((list.get(0).getStoragearea().equals("5")))
            {
                return "23";
            }
            else
            {
                return "24";
            }
        }
        return "24";
    }
}

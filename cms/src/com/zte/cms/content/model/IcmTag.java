package com.zte.cms.content.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmTag extends DynamicBaseObject
{
    private java.lang.Long tagindex;
    private java.lang.String tagname;
    private java.lang.Long tagparentid;

    public java.lang.Long getTagindex()
    {
        return tagindex;
    }

    public void setTagindex(java.lang.Long tagindex)
    {
        this.tagindex = tagindex;
    }

    public java.lang.String getTagname()
    {
        return tagname;
    }

    public void setTagname(java.lang.String tagname)
    {
        this.tagname = tagname;
    }

    public java.lang.Long getTagparentid()
    {
        return tagparentid;
    }

    public void setTagparentid(java.lang.Long tagparentid)
    {
        this.tagparentid = tagparentid;
    }

    public void initRelation()
    {
        this.addRelation("tagindex", "TAGINDEX");
        this.addRelation("tagname", "TAGNAME");
        this.addRelation("tagparentid", "TAGPARENTID");
    }
}

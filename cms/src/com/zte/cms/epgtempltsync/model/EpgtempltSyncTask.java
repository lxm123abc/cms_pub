package com.zte.cms.epgtempltsync.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class EpgtempltSyncTask extends DynamicBaseObject
{
    private java.lang.Long taskindex;
    private java.lang.String starttime;
    private java.lang.String endtime;
    private java.lang.Integer status;
    private java.lang.Integer priority;
    private java.lang.Integer resultcode;
    private java.lang.String resultdesc;
    private java.lang.String description;
    private java.lang.String correlateid;
    private java.lang.Long templtindex;
    private java.lang.Integer tasktype;
    private java.lang.Integer source;
    private java.lang.String resultfileurl;
    private java.lang.String cmdfileurl;
    private java.lang.Integer platform;
    
    public java.lang.Integer getSource() {
		return source;
	}
	public void setSource(java.lang.Integer source) {
		this.source = source;
	}

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.String getStarttime()
    {
        return starttime;
    }

    public void setStarttime(java.lang.String starttime)
    {
        this.starttime = starttime;
    }

    public java.lang.String getEndtime()
    {
        return endtime;
    }

    public void setEndtime(java.lang.String endtime)
    {
        this.endtime = endtime;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.Integer priority)
    {
        this.priority = priority;
    }

    public java.lang.Integer getResultcode()
    {
        return resultcode;
    }

    public void setResultcode(java.lang.Integer resultcode)
    {
        this.resultcode = resultcode;
    }

    public java.lang.String getResultdesc()
    {
        return resultdesc;
    }

    public void setResultdesc(java.lang.String resultdesc)
    {
        this.resultdesc = resultdesc;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getCorrelateid()
    {
        return correlateid;
    }

    public void setCorrelateid(java.lang.String correlateid)
    {
        this.correlateid = correlateid;
    }

    public java.lang.Long getTempltindex()
    {
        return templtindex;
    }

    public void setTempltindex(java.lang.Long templtindex)
    {
        this.templtindex = templtindex;
    }

    public java.lang.Integer getTasktype()
    {
        return tasktype;
    }

    public void setTasktype(java.lang.Integer tasktype)
    {
        this.tasktype = tasktype;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub

    }
	public java.lang.String getResultfileurl() {
		return resultfileurl;
	}
	public void setResultfileurl(java.lang.String resultfileurl) {
		this.resultfileurl = resultfileurl;
	}
	public java.lang.String getCmdfileurl() {
		return cmdfileurl;
	}
	public void setCmdfileurl(java.lang.String cmdfileurl) {
		this.cmdfileurl = cmdfileurl;
	}
	public java.lang.Integer getPlatform() {
		return platform;
	}
	public void setPlatform(java.lang.Integer platform) {
		this.platform = platform;
	}

}

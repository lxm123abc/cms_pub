package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;

import java.util.Date;
import java.util.List;

/**
 * ProjectName: cms-com.zte.cms.settlementDataRpt.dao
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 17:00 2019/5/7
 * @Description:
 */
public class SettlementDataRptMapperImpl extends DynamicObjectBaseDao implements SettlementDataRptMapper {

    private Log logger = SSBBus.getLog(SettlementDataRptMapperImpl.class);

    @Override
    public void deleteByPrimaryKey(SettlementDataRptVO record) {
        logger.info("delete SettlementDataRpt.deleteByPrimaryKey starting...");
        super.delete("deleteSettlementDataRptById", record);
        logger.info("delete SettlementDataRpt.deleteByPrimaryKey  end");
    }


    @Override
    public int selectCountById(SettlementDataRpt record) {
        return ((Integer) super.queryForObject("selectSettlementCountDataRptById", record)).intValue();
    }

    @Override
    public void insert(SettlementDataRpt record) {
        record.setIsconfirm("0");
        super.insert("insertSettlementDataRpt", record);
    }

    @Override
    public void update(SettlementDataRpt record) {
        super.update("updateSettlementData",record);
    }

    @Override
    public void updateIsConfirmById(SettlementDataRptVO record) {
        super.update("updateSettlementDataIsConfirmById",record);
    }

    @Override
    public SettlementDataRpt selectSettlementDataRptById(SettlementDataRptVO record) {
        logger.info("query selectByPrimaryKey starting...");
        SettlementDataRpt settlementDataRpt = (SettlementDataRpt) super.queryForObject("selectSettlementDataRptById",record);
        logger.info("query selectByPrimaryKey end");
        return settlementDataRpt;
    }

    @Override
    public List<String> getProjectType() {
        logger.info("query getProjectType starting...");
        List<String> typeList = (List<String>) super.queryForList("getSettlementDataRptProjectType",new SettlementDataRptVO());
        logger.info("query getProjectType end");
        return typeList;
    }

    @Override
    public PageInfo selectSettlementDataRpt(SettlementDataRptVO settlementDataRptVO) {
        logger.info("query selectPage starting...");
        int pageSize = settlementDataRptVO.getPageSize();
        int start = settlementDataRptVO.getStart();
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("selectSettlementDataRptCount", settlementDataRptVO)).intValue();
        if (totalCnt > 0) {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SettlementDataRpt> rsList = (List<SettlementDataRpt>) super.pageQuery("selectSettlementDataRpt",
                    settlementDataRptVO, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        } else {
            pageInfo = new PageInfo();
        }
        logger.info("query selectPage end");
        return pageInfo;
    }
}

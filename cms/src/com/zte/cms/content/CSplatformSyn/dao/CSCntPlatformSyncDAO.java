
  package com.zte.cms.content.CSplatformSyn.dao;

import java.util.List;

import com.zte.cms.content.CSplatformSyn.model.CSCntPlatformSync;
import com.zte.ssb.dynamicobj.DynamicBaseObject;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CSCntPlatformSyncDAO extends DynamicObjectBaseDao implements ICSCntPlatformSyncDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("insert cntPlatformSync starting...");
    	super.insert( "insertCntPlatformSync", cntPlatformSync );
    	log.debug("insert cntPlatformSync end");
    }
     
    public void updateCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("update cntPlatformSync by pk starting...");
       	super.update( "updateCntPlatformSync", cntPlatformSync );
       	log.debug("update cntPlatformSync by pk end");
    }


    public void deleteCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("delete cntPlatformSync by pk starting...");
       	super.delete( "deleteCntPlatformSync", cntPlatformSync );
       	log.debug("delete cntPlatformSync by pk end");
    }


    public CSCntPlatformSync getCntPlatformSync( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("query cntPlatformSync starting...");
    	CSCntPlatformSync resultObj = (CSCntPlatformSync)super.queryForObject( "getCntPlatformSync",cntPlatformSync);
       	log.debug("query cntPlatformSync end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<CSCntPlatformSync> getCntPlatformSyncByCond( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
    	log.debug("query cntPlatformSync by condition starting...");
    	List<CSCntPlatformSync> rList = (List<CSCntPlatformSync>)super.queryForList( "csqueryCntPlatformSyncListByCond",cntPlatformSync);
    	log.debug("query cntPlatformSync by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException
    {
    	log.debug("page query cntPlatformSync by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("csqueryCntPlatformSyncListCntByCond",  cntPlatformSync)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<CSCntPlatformSync> rsList = (List<CSCntPlatformSync>)super.pageQuery( "csqueryCntPlatformSyncListByCond" ,  cntPlatformSync , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query cntPlatformSync by condition end");
    	return pageInfo;
    }
	
	 public PageInfo pageInfoQueryCha(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException
	    {
	    	log.debug("page query cntPlatformSync by condition starting...");
	     	PageInfo pageInfo = null;
	    	int totalCnt = ((Integer) super.queryForObject("csqueryCntPlatformSyncListCntByCondCha",  cntPlatformSync)).intValue();
	    	if( totalCnt > 0 )
	    	{
	    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
	    		List<CSCntPlatformSync> rsList = (List<CSCntPlatformSync>)super.pageQuery( "csqueryCntPlatformSyncListByCondCha" ,  cntPlatformSync , start , fetchSize );
	    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
	    	}
	    	else
	    	{
	    		pageInfo = new PageInfo();
	    	}
	    	log.debug("page query cntPlatformSync by condition end");
	    	return pageInfo;
	    }
	    
	    public PageInfo pageInfoQuerySche(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException
	    {
	    	log.debug("page query cntPlatformSync by condition starting...");
	     	PageInfo pageInfo = null;
	    	int totalCnt = ((Integer) super.queryForObject("csqueryCntPlatformSyncListCntByCondSche",  cntPlatformSync)).intValue();
	    	if( totalCnt > 0 )
	    	{
	    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
	    		List<CSCntPlatformSync> rsList = (List<CSCntPlatformSync>)super.pageQuery( "csqueryCntPlatformSyncListByCondSche" ,  cntPlatformSync , start , fetchSize );
	    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
	    	}
	    	else
	    	{
	    		pageInfo = new PageInfo();
	    	}
	    	log.debug("page query cntPlatformSync by condition end");
	    	return pageInfo;
	    }
	    
	    public PageInfo pageInfoQueryCach(CSCntPlatformSync cntPlatformSync, int start, int pageSize)throws DAOException
	    {
	    	log.debug("page query cntPlatformSync by condition starting...");
	     	PageInfo pageInfo = null;
	    	int totalCnt = ((Integer) super.queryForObject("csqueryCntPlatformSyncListCntByCondCtgCha",  cntPlatformSync)).intValue();
	    	if( totalCnt > 0 )
	    	{
	    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
	    		List<CSCntPlatformSync> rsList = (List<CSCntPlatformSync>)super.pageQuery( "csqueryCntPlatformSyncListByCondCtgCha" ,  cntPlatformSync , start , fetchSize );
	    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
	    	}
	    	else
	    	{
	    		pageInfo = new PageInfo();
	    	}
	    	log.debug("page query cntPlatformSync by condition end");
	    	return pageInfo;
	    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(DynamicBaseObject cntPlatformSync, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "csqueryCntPlatformSyncListByCond", "queryCntPlatformSyncListCntByCond", cntPlatformSync, start, pageSize, puEntity);
    }

	public PageInfo pageInfoQuery(CSCntPlatformSync cntPlatformSync, int start,
			int pageSize, PageUtilEntity puEntity) throws DAOException
	{
		// TODO Auto-generated method stub
		return null;
	}
    
	public void deleteCntPlatformSyncByObjindex( CSCntPlatformSync cntPlatformSync )throws DAOException
    {
        log.debug("delete cntPlatformSync by conditions starting...");
        super.delete( "deleteCntPlatformSyncByObjindex", cntPlatformSync );
        log.debug("update cntPlatformSync by conditions end");
    }

    public CSCntPlatformSync getCntPlatformSync4Object(CSCntPlatformSync cntPlatformSync) throws DAOException
    {
        // TODO Auto-generated method stub
        log.debug("query cntPlatformSync4Object starting...");
        CSCntPlatformSync resultObj = (CSCntPlatformSync)super.queryForObject( "getCntPlatformSync4Object",cntPlatformSync);
        log.debug("query cntPlatformSync4Object end");
        return resultObj;
    }

    public int getCntPlatformSyncChildNumber(CSCntPlatformSync cntPlatformSync) throws DAOException
    {
        // TODO Auto-generated method stub
        return ((Integer) super.queryForObject("queryCntPlatformChildTargetsystemByCond",  cntPlatformSync)).intValue();
    }
} 
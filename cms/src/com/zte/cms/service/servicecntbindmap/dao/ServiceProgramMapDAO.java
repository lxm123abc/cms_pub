package com.zte.cms.service.servicecntbindmap.dao;

import java.util.List;

import com.zte.cms.service.servicecntbindmap.dao.IServiceProgramMapDAO;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceProgramMapDAO extends DynamicObjectBaseDao implements IServiceProgramMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException
    {
        log.debug("insert serviceProgramMap starting...");
        super.insert("insertServiceProgramMap", serviceProgramMap);
        log.debug("insert serviceProgramMap end");
    }

    public void updateServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException
    {
        log.debug("update serviceProgramMap by pk starting...");
        super.update("updateServiceProgramMap", serviceProgramMap);
        log.debug("update serviceProgramMap by pk end");
    }

    public void deleteServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException
    {
        log.debug("delete serviceProgramMap by pk starting...");
        super.delete("deleteServiceProgramMap", serviceProgramMap);
        log.debug("delete serviceProgramMap by pk end");
    }

    public ServiceProgramMap getServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DAOException
    {
        log.debug("query serviceProgramMap starting...");
        ServiceProgramMap resultObj = (ServiceProgramMap) super.queryForObject("getServiceProgramMap",
                serviceProgramMap);
        log.debug("query serviceProgramMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceProgramMap> getServiceProgramMapByCond(ServiceProgramMap serviceProgramMap) throws DAOException
    {
        log.debug("query serviceProgramMap by condition starting...");
        List<ServiceProgramMap> rList = (List<ServiceProgramMap>) super.queryForList(
                "queryServiceProgramMapListByCond", serviceProgramMap);
        log.debug("query serviceProgramMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceProgramMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceProgramMapListCntByCond", serviceProgramMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceProgramMap> rsList = (List<ServiceProgramMap>) super.pageQuery(
                    "queryServiceProgramMapListByCond", serviceProgramMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceProgramMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceProgramMapListByCond", "queryServiceProgramMapListCntByCond",
                serviceProgramMap, start, pageSize, puEntity);
    }

}
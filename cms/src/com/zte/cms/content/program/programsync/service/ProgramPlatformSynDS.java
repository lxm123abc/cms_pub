package com.zte.cms.content.program.programsync.service;

import java.util.List;

import com.zte.cms.content.program.programsync.dao.IProgramPlatformSynDAO;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class ProgramPlatformSynDS implements IProgramPlatformSynDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IProgramPlatformSynDAO dao = null;
	
	public void setDao(IProgramPlatformSynDAO dao)
	{
	    this.dao = dao;
	}

	
	public ProgramPlatformSyn getCntTargetsysSyn( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException
	{
		log.debug( "get programPlatformSyn by pk starting..." );
		ProgramPlatformSyn rsObj = null;
		try
		{
			rsObj = dao.getCntTargetsysSyn( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetsysSynList by pk end" );
		return rsObj;
	}

	public ProgramPlatformSyn getCntTargetsysBindSyn( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException
	{
		log.debug( "get programPlatformSyn by pk starting..." );
		ProgramPlatformSyn rsObj = null;
		try
		{
			rsObj = dao.getCntTargetsysBindSyn( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetsysSynList by pk end" );
		return rsObj;
	}	
	//按查询条件查询
	public int getCntTargetsysBindSynPub( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException
	{
		log.debug( "get getCntTargetsysBindSynPub by pk starting..." );
		int result = 0;
		try
		{
			result = dao.getCntTargetsysBindSynPub( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get getCntTargetsysBindSynPub by pk end" );
		return result;
	}
	

	
	
	public List<ProgramPlatformSyn> getCntTargetsysSynByCond( ProgramPlatformSyn programPlatformSyn )throws DomainServiceException
	{
		log.debug( "get programPlatformSyn by condition starting..." );
		List<ProgramPlatformSyn> rsList = null;
		try
		{
			rsList = dao.getCntTargetsysSynByCond( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programPlatformSyn by condition end" );
		return rsList;
	}
	

	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get programPlatformSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programPlatformSyn, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramPlatformSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programPlatformSyn page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get programPlatformSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programPlatformSyn, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramPlatformSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programPlatformSyn page info by condition end" );
		return tableInfo;
	}

	public ProgramPlatformSyn getProgramPlatformSyn3(ProgramPlatformSyn programPlatformSyn)
			throws DomainServiceException
	{
		log.debug( "get programPlatformSyn by pk starting..." );
		ProgramPlatformSyn rsObj = null;
		try
		{
			rsObj = dao.getProgramPlatformSyn3( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetsysSynList by pk end" );
		return rsObj;
	}

	public ProgramPlatformSyn getProgramPlatformSyn2(ProgramPlatformSyn programPlatformSyn)
			throws DomainServiceException
	{
		log.debug( "get programPlatformSyn by pk starting..." );
		ProgramPlatformSyn rsObj = null;
		try
		{
			rsObj = dao.getProgramPlatformSyn2( programPlatformSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetsysSynList by pk end" );
		return rsObj;
	}	
}

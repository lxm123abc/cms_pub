package com.zte.cms.storageManage.dao;

import java.util.List;

import com.zte.cms.storageManage.dao.ICmsDeviceWindiskidBindDAO;
import com.zte.cms.storageManage.model.CmsDeviceWindiskidBind;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsDeviceWindiskidBindDAO extends DynamicObjectBaseDao implements ICmsDeviceWindiskidBindDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException
    {
        log.debug("insert cmsDeviceWindiskidBind starting...");
        super.insert("insertCmsDeviceWindiskidBind", cmsDeviceWindiskidBind);
        log.debug("insert cmsDeviceWindiskidBind end");
    }

    public void updateCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException
    {
        log.debug("update cmsDeviceWindiskidBind by pk starting...");
        super.update("updateCmsDeviceWindiskidBind", cmsDeviceWindiskidBind);
        log.debug("update cmsDeviceWindiskidBind by pk end");
    }

    public void deleteCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException
    {
        log.debug("delete cmsDeviceWindiskidBind by pk starting...");
        super.delete("deleteCmsDeviceWindiskidBind", cmsDeviceWindiskidBind);
        log.debug("delete cmsDeviceWindiskidBind by pk end");
    }

    public CmsDeviceWindiskidBind getCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DAOException
    {
        log.debug("query cmsDeviceWindiskidBind starting...");
        CmsDeviceWindiskidBind resultObj = (CmsDeviceWindiskidBind) super.queryForObject("getCmsDeviceWindiskidBind",
                cmsDeviceWindiskidBind);
        log.debug("query cmsDeviceWindiskidBind end");
        return resultObj;
    }

    public List<CmsDeviceWindiskidBind> getCmsDeviceWindiskidBindByCond(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DAOException
    {
        log.debug("query cmsDeviceWindiskidBind by condition starting...");
        List<CmsDeviceWindiskidBind> rList = (List<CmsDeviceWindiskidBind>) super.queryForList(
                "queryCmsDeviceWindiskidBindListByCond", cmsDeviceWindiskidBind);
        log.debug("query cmsDeviceWindiskidBind by condition end");
        return rList;
    }

    public PageInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsDeviceWindiskidBind by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsDeviceWindiskidBindListCntByCond",
                cmsDeviceWindiskidBind)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsDeviceWindiskidBind> rsList = (List<CmsDeviceWindiskidBind>) super.pageQuery(
                    "queryCmsDeviceWindiskidBindListByCond", cmsDeviceWindiskidBind, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsDeviceWindiskidBind by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCmsDeviceWindiskidBindListByCond",
                "queryCmsDeviceWindiskidBindListCntByCond", cmsDeviceWindiskidBind, start, pageSize, puEntity);
    }

}
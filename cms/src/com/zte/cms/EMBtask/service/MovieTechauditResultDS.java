package com.zte.cms.EMBtask.service;

import java.util.List;
import com.zte.cms.EMBtask.model.MovieTechauditResult;
import com.zte.cms.EMBtask.dao.IMovieTechauditResultDAO;
import com.zte.cms.EMBtask.service.IMovieTechauditResultDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class MovieTechauditResultDS implements IMovieTechauditResultDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IMovieTechauditResultDAO dao = null;

    public void setDao(IMovieTechauditResultDAO dao)
    {
        this.dao = dao;
    }

    public void insertMovieTechauditResult(MovieTechauditResult movieTechauditResult) throws DomainServiceException
    {
        log.debug("insert movieTechauditResult starting...");
        try
        {
            dao.insertMovieTechauditResult(movieTechauditResult);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert movieTechauditResult end");
    }

    public void updateMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException
    {
        log.debug("update movieTechauditResult by condition starting...");
        try
        {
            dao.updateMovieTechauditResultByCond(movieTechauditResult);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update movieTechauditResult by condition end");
    }

    public void updateMovieTechauditResultListByCond(List<MovieTechauditResult> movieTechauditResultList)
            throws DomainServiceException
    {
        log.debug("update movieTechauditResultList by condition starting...");
        if (null == movieTechauditResultList || movieTechauditResultList.size() == 0)
        {
            log.debug("there is no datas in movieTechauditResultList");
            return;
        }
        try
        {
            for (MovieTechauditResult movieTechauditResult : movieTechauditResultList)
            {
                dao.updateMovieTechauditResultByCond(movieTechauditResult);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update movieTechauditResultList by condition end");
    }

    public void removeMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException
    {
        log.debug("remove movieTechauditResult by condition starting...");
        try
        {
            dao.deleteMovieTechauditResultByCond(movieTechauditResult);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove movieTechauditResult by condition end");
    }

    public void removeMovieTechauditResultListByCond(List<MovieTechauditResult> movieTechauditResultList)
            throws DomainServiceException
    {
        log.debug("remove movieTechauditResultList by condition starting...");
        if (null == movieTechauditResultList || movieTechauditResultList.size() == 0)
        {
            log.debug("there is no datas in movieTechauditResultList");
            return;
        }
        try
        {
            for (MovieTechauditResult movieTechauditResult : movieTechauditResultList)
            {
                dao.deleteMovieTechauditResultByCond(movieTechauditResult);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove movieTechauditResultList by condition end");
    }

    public List<MovieTechauditResult> getMovieTechauditResultByCond(MovieTechauditResult movieTechauditResult)
            throws DomainServiceException
    {
        log.debug("get movieTechauditResult by condition starting...");
        List<MovieTechauditResult> rsList = null;
        try
        {
            rsList = dao.getMovieTechauditResultByCond(movieTechauditResult);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get movieTechauditResult by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get movieTechauditResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(movieTechauditResult, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<MovieTechauditResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get movieTechauditResult page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(MovieTechauditResult movieTechauditResult, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get movieTechauditResult page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(movieTechauditResult, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<MovieTechauditResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get movieTechauditResult page info by condition end");
        return tableInfo;
    }
}

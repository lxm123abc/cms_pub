<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="syspara" class="com.zte.purviewext.manSysPara" scope="page" />
<jsp:useBean id="mangroup" class="com.zte.purviewext.ManGroup" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%  
    HashMap pmap = pwdrule.getPolicy(true);
    String prerequisite = (String)pmap.get(i18n.getMessage("purview.prerequisite"));
    String checkflag = (String)pmap.get(i18n.getMessage("purview.checkflag"));
    application.setAttribute("ISGROUP","1");
    try {
        String initPwd="";
        String grouplen = (String)session.getAttribute("GROUPIDLEN")==null?"0":(String)session.getAttribute("GROUPIDLEN");
        String isgroup = (String)application.getAttribute("ISGROUP")==null?"0":(String)application.getAttribute("ISGROUP");
        String operID = (String)request.getParameter("operID");
        HashMap map = null;
        ArrayList list = new ArrayList();
        HashMap ipMap = new HashMap();
        String flagString = "";
        boolean flag = true;
        int  serflag = 0;
        int  serindex = 0;
        if (operID == null){
            initPwd = pwdrule.getInitPassword(pmap);
            flagString = "add";
            if(session.getAttribute("YWYY_CODE_ATTRIBUTE")!=null){
              serindex= Integer.parseInt((String)session.getAttribute("YWYY_CODE_ATTRIBUTE"));
            }
            if(session.getAttribute("OPER_TYPE_ATTRIBUTE")!=null){
              serflag = Integer.parseInt((String)session.getAttribute("OPER_TYPE_ATTRIBUTE"));
            }
        }
        else {
            flagString = "update";
            flag = false;
            map = purview.getOperInformation(operID);
            serflag = Integer.parseInt((String)map.get("SERFLAG"));
            serindex= Integer.parseInt((String)map.get("SERINDEX"));
            list = (ArrayList)map.get("IPLIST");
        }
        String strOption = "";
        String grpOption = "";
        ArrayList spvet = new ArrayList();
        ArrayList grpvet = new ArrayList();
        //spvet = syspara.getXsmpSPInfo();
        for(int i=0; i<spvet.size(); i++) {
         HashMap  smap = (HashMap)spvet.get(i);
          int  sp = Integer.parseInt((String)smap.get("spindex"));
          if(serflag == 1 && sp == serindex)
              strOption = strOption + "<option value=" + (String)smap.get("spindex")+" selected >"+(String)smap.get("spname")+"</option>";
          else
             strOption = strOption + "<option value=" + (String)smap.get("spindex")+" >"+(String)smap.get("spname")+"</option>";
        }
        if("1".equals(isgroup)){
          //grpvet = mangroup.getGroupInfo();
          for(int i=0;i<grpvet.size();i++){
	          HashMap gmap = (HashMap)grpvet.get(i);
	          System.out.println(gmap.get("CORPID")+"/"+serindex);
	          if(serflag == 3 && Integer.parseInt((String)gmap.get("CORPID")) == serindex)
	              grpOption = grpOption + "<option value=" + (String)gmap.get("CORPID")+" selected >"+(String)gmap.get("CORPNAME")+"</option>";
	          else
	              grpOption = grpOption + "<option value=" + (String)gmap.get("CORPID")+" >"+(String)gmap.get("CORPNAME")+"</option>";          
          }
        }


%>
<html>
<head>
<title><%= flag ? i18n.getMessage("operInfo.add") : i18n.getMessage("operInfo.update") %><fmt:message key="operInfo.operator"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="JsFun.jsp" flush="" />
<script language="javascript">
   var grouplen = <%= grouplen %>;
   function initform(pform){
      var serflag = <%= serflag %>;
      if(serflag == '0'){
        pform.serflag[0].checked = true;
      }
      else if(serflag == '1'){
        pform.serflag[1].checked = true;
      }
      else{
        pform.serflag[2].checked = true;
      }
      
      onSerFlag();
   }
   function onpasswordfocus(){
   
     <%
       if(flag && initPwd.length()>0 && "1".equals(prerequisite)){
     %>
	      document.view.operPwd.value="<%= initPwd %>";
	      document.view.operConfirm.value="<%= initPwd %>";
	   <%}%>
   }
   function unLoad () {
      window.opener.focus();
   }

   function changeUseDays () {
      if (document.view.maxNoUseDays.checked) {
         document.view.noUseDays.disabled = false;
         document.view.noUseDays.focus();
      }
      else
         document.view.noUseDays.disabled = true;
   }
   
   function nextUpdPwdClicked(){
     <%
       String updateflag = (String)pmap.get(i18n.getMessage("purview.updateflag"));
       if("1".equals(prerequisite)){
         if("1".equals(updateflag)){
     %>
         document.view.nextUpdPwd.checked = true;
         return;
     <%}if("0".equals(updateflag)){%>
         document.view.nextUpdPwd.checked = false;
         return;
     <%}}%>
   }
   
   function changeWrongTimes () {
      
     <%
       if("1".equals(prerequisite)){
     %>
         document.view.maxWrongTimes.checked = true;
         return;
     <%}else{%>   
      if (document.view.maxWrongTimes.checked) {
         document.view.wrongTimes.disabled = false;
         document.view.wrongTimes.focus();
      }
      else
         document.view.wrongTimes.disabled = true;
     <%}%>
   }

   function changePasswordChange () {
     <%
       if("1".equals(prerequisite)){
     %>
         document.view.pwdMustChange1.checked = true;
         return;
     <%}else{%>     
      if (document.view.pwdMustChange1.checked) {
         document.view.passwordChange.disabled = false;
         document.view.passwordChange.focus();
      }
      else
         document.view.passwordChange.disabled = true;
     <%}%>
   }


   function changeUserDate () {
      if (document.view.useDate.checked) {
         document.view.endDate.disabled = false;
         document.view.endDate.focus();
      }
      else
         document.view.endDate.disabled = true;
   }


   function changePwdMustChange () {
     <%
       if("1".equals(prerequisite)){
     %>
       document.view.pwdMustChange.checked = false;
        return;
     <%}else{%>     
      if (document.view.pwdMustChange.checked){
         document.view.pwdMustChange1.disabled = true;
         document.view.passwordChange.disabled = true;
      }
      else{
         document.view.pwdMustChange1.disabled = false;
         document.view.passwordChange.disabled = false;
      }
     <%}%>
   }

   function changeLimit () {
      if (document.view.onLimit[0].checked) {
         document.view.sunday.disabled = true;
         document.view.monday.disabled = true;
         document.view.tuesday.disabled = true;
         document.view.wednesday.disabled = true;
         document.view.thursday.disabled = true;
         document.view.friday.disabled = true;
         document.view.saturday.disabled = true;
         document.view.beginTime.disabled = true;
         document.view.endTime.disabled = true;
      }
      else {
         document.view.sunday.disabled = false;
         document.view.monday.disabled = false;
         document.view.tuesday.disabled = false;
         document.view.wednesday.disabled = false;
         document.view.thursday.disabled = false;
         document.view.friday.disabled = false;
         document.view.saturday.disabled = false;
         document.view.beginTime.disabled = false;
         document.view.endTime.disabled = false;
         document.view.beginTime.focus();
      }
   }

   function changeIP () {
      if (document.view.noIP.checked) {
         document.view.ipAddress.disabled = true;
         document.view.addAllowIP.disabled = true;
         document.view.delAllowIP.disabled = true;
         document.view.addForbidIP.disabled = true;
         document.view.delForbidIP.disabled = true;
         document.view.allowedIP.disabled = true;
         document.view.forbidedIP.disabled = true;
      }
      else {
         document.view.ipAddress.disabled = false;
         document.view.addAllowIP.disabled = false;
         document.view.delAllowIP.disabled = false;
         document.view.addForbidIP.disabled = false;
         document.view.delForbidIP.disabled = false;
         document.view.allowedIP.disabled = false;
         document.view.forbidedIP.disabled = false;
         document.view.ipAddress.focus();
      }
   }



   function checkIP () {
      var tmp = document.view.ipAddress.value;
      var index = tmp.indexOf('.');
      var ip;
      for (i = 0; i < document.view.allowedIP.options.length; i++)
         if (tmp == document.view.allowedIP.options[i].value) {
            alert('<fmt:message key="operInfo.ipexists"/>');
            document.view.allowedIP.selectedIndex = i;
            document.view.ipAddress.focus();
            return false;
         }
      for (i = 0; i < document.view.forbidedIP.options.length; i++)
         if (tmp == document.view.forbidedIP.options[i].value) {
            alert('<fmt:message key="operInfo.ipexists"/>');
            document.view.forbidedIP.selectedIndex = i;
            document.view.ipAddress.focus();
            return false;
         }

      ip = tmp.substring(0,index);
      if (ip.length == 0 || isNaN(ip) || ip < 1 || ip > 255) {
         alert('<fmt:message key="operInfo.ipillegal"/>');
         document.view.ipAddress.focus();
         return false;
      }
 
      tmp = tmp.substring(index + 1,tmp.length);
      index = tmp.indexOf('.');
      ip = tmp.substring(0,index);
      if (ip.length == 0 || isNaN(ip) || ip < 0 || ip > 255) {
         alert('<fmt:message key="operInfo.ipillegal"/>');
         document.view.ipAddress.focus();
         return false;
      }
    
      tmp = tmp.substring(index + 1,tmp.length);
      index = tmp.indexOf('.');
      ip = tmp.substring(0,index);
      if (ip.length == 0 || isNaN(ip) || ip < 0 || ip > 255) {
         alert('<fmt:message key="operInfo.ipillegal"/>');
         document.view.ipAddress.focus();
         return false;
      }
      ip = tmp.substring(index + 1,tmp.length);
      if (ip.length == 0 || isNaN(ip) || ip < 0 || ip > 255) {
         alert('<fmt:message key="operInfo.ipillegal"/>');
         document.view.ipAddress.focus();
         return false;
      }
      return true;
   }


   function addIP (selectObject) {
      if (checkIP()) {
         allowIP = new Option(document.view.ipAddress.value,document.view.ipAddress.value,false,true);
         selectObject.options[selectObject.options.length] = allowIP;
      }
   }

   function delIP (selectObject) {
      if (selectObject.options.length == 0)
         return;
      if (selectObject.selectedIndex < 0) {
         alert('<fmt:message key="operInfo.ipfordelrequired"/>');
         return;
      }
      document.view.ipAddress.value = selectObject.options[selectObject.selectedIndex].value;
      selectObject.options[selectObject.selectedIndex] = null;
      document.view.ipAddress.focus();
   }

   
   function onSubmit () {

      var fm = document.view;
      var pwdhistory = new Array(<%= pwdrule.getHistoryPwd(operID,true)%>);
      fm.operName.value = trim(fm.operName.value);
      fm.operPwd.value = trim(fm.operPwd.value);
      fm.operConfirm.value = trim(fm.operConfirm.value);
      var allowIP = '';
      var noAllowIP = '';
      if (fm.operName.value == '') {
         alert('<fmt:message key="operInfo.opernamerequired"/>');
         fm.operName.focus();
         return;
      }
      if(strlength(fm.operName.value)>20){
         alert('<fmt:message key="operInfo.opernameexceed"/>');
         fm.operName.focus();
         return;
      }
      if (!CheckInputStr(fm.operName,'<fmt:message key="operDel.opername"/>')){
         fm.operName.focus();
         return ;
      }
      if(fm.operAllName.value !='' && strlength(fm.operAllName.value)>40){
         alert('<fmt:message key="operInfo.opernameexceed40"/>');
         fm.operAllName.focus();
         return;
      }
     if (!CheckInputStr(fm.operAllName,'<fmt:message key="operDel.operfullname"/>')){
         fm.operAllName.focus();
         return  ;
      }
      if(fm.operDescription.value !='' && strlength(fm.operDescription.value)>40){
         alert('<fmt:message key="operInfo.descexceed40"/>');
         fm.operDescription.focus();
         return;
      }
      if (!CheckInputStr(fm.operDescription,'<fmt:message key="operDel.description"/>')){
         fm.operDescription.focus();
         return  ;
      }
      if (fm.operPwd.value.length<<%= pmap.get(i18n.getMessage("purview.minlen"))%>) {
         alert('<fmt:message key="operInfo.pwdminimallength"/>'+<%= pmap.get(i18n.getMessage("purview.minlen"))%>);
         fm.operPwd.focus();
         return;
      }
      if (fm.operPwd.value.length><%= pmap.get(i18n.getMessage("purview.maxlen"))%>) {
         alert('<fmt:message key="operInfo.pwdmaximallength"/>'+<%= pmap.get(i18n.getMessage("purview.maxlen"))%>);
         fm.operPwd.focus();
         return;
      }      
      if (fm.operPwd.value != fm.operConfirm.value) {
         alert('<fmt:message key="operInfo.pwdinconsistent"/>');
         fm.operPwd.value = '';
         fm.operConfirm.value = '';
         fm.operPwd.focus();
         return;
      }
      <%
      if((!flag || initPwd.length()==0) && "1".equals(checkflag)){
	      if("0".equals((String)pmap.get(i18n.getMessage("purview.chrtype")))){
	      %>
	      if(!checkpwd0(fm.operPwd.value)){
	        alert('<fmt:message key="operInfo.pwdcharacterillegal"/>');
	      <%}else if("1".equals((String)pmap.get(i18n.getMessage("purview.chrtype")))){%>
	      if(!checkpwd1(fm.operPwd.value)){
	        alert('<fmt:message key="operInfo.pwdnumberonly"/>');  
	      <%}else if("2".equals((String)pmap.get(i18n.getMessage("purview.chrtype")))){%>
	      if(!checkpwd2(fm.operPwd.value)){ 
	        alert('<fmt:message key="operInfo.pwdletter"/>');
	      <%}else if("3".equals((String)pmap.get(i18n.getMessage("purview.chrtype")))){%>
	      if(!checkpwd3(fm.operPwd.value)){
	        alert('<fmt:message key="operInfo.pwdnumberandletter"/>');
	      <%}%>
         
         fm.operPwd.value = '';
         fm.operConfirm.value = '';
         fm.operPwd.focus();
         return;
      }
      <%}
      if("1".equals((String)pmap.get(i18n.getMessage("purview.createtype")))){
      %>
       if(fm.operPwd.value == fm.operName.value){
         alert('<fmt:message key="operInfo.passwordsameasname"/>');
         fm.operPwd.value = '';
         fm.operPwd.focus();
         return;
       }
      <%}%>
      
      if(fm.wrongTimes.disabled == false &&fm.wrongTimes.value.length==0){
        fm.wrongTimes.focus();
        return;
      }
      
      if(fm.wrongTimes.value > <%= pmap.get(i18n.getMessage("purview.maxwrglog")) %>){
         alert('<fmt:message key="operInfo.wrongtimestoobig"/>'+<%= pmap.get(i18n.getMessage("purview.maxwrglog")) %>);
         fm.wrongTimes.value = '';
         fm.wrongTimes.focus();
         return;
      }
      if(fm.passwordChange.disabled == false &&fm.passwordChange.value.length==0){
        fm.passwordChange.focus();
        return;
      }      
      if(fm.passwordChange.value > <%= pmap.get(i18n.getMessage("purview.pwdmaxday")) %>){
         alert('<fmt:message key="operInfo.passwordChangetoobig"/>'+<%= pmap.get(i18n.getMessage("purview.pwdmaxday")) %>);
         fm.passwordChange.value = '';
         fm.passwordChange.focus();  
         return;    
      }
      <%if(!flag){%>
        if(fm.operConfirm.value != '<%= (String)map.get("OPERPWD") %>'){
      <%}%>
	      for(var i=0; i<=pwdhistory.length; i++){
	         if(pwdhistory[i] ==fm.operPwd.value){
		         alert('<fmt:message key="operInfo.passwordinhistory"/>');
		         fm.operPwd.value = '';
		         fm.operConfirm.value = '';
		         fm.operPwd.focus();
		         return;
	         }      
	      }
      <%if(!flag){%>
        }
      <%}%>
	    if(fm.notify.value=='1' && (fm.phonenumber.value.length<7 ||!checkstring('0123456789',fm.phonenumber.value))){
	      
				alert('<fmt:message key="operInfo.phoneillegal"/>');
				fm.phonenumber.focus();
				return;
	    }
      if(fm.notify.value=='2' && !isEmail(trim(fm.email.value))){
				alert('<fmt:message key="operInfo.emailillegal"/>');
				fm.email.focus();
				return;
	    }
      if(fm.serflag[2].checked){
         var value = trim(fm.groupid.value);
         if(value==''){
            alert("<fmt:message key="operInfo.inputgrpid"/>");
            return;
         }
         if(isNaN(value)){
          alert('<fmt:message key="operInfo.grpidbenumeric"/>');
          fm.groupid.focus();
          return;
         }
         if(value.length < grouplen){
           alert('<fmt:message key="operInfo.grpidexceed15"/>'+grouplen + '<fmt:message key="operInfo.inputagain"/>');
           fm.groupid.focus();
           return ;
         }
      }
      if (! document.view.noIP.checked) {
         for (i = 0; i < document.view.allowedIP.options.length; i++)
            allowIP = allowIP + document.view.allowedIP.options[i].value + ';'
         document.view.allowIP.value = allowIP;
         for (i = 0; i < document.view.forbidedIP.options.length; i++)
            noAllowIP = noAllowIP + document.view.forbidedIP.options[i].value + ';'
         document.view.noAllowIP.value = noAllowIP;
      }

      fm.submit();
   }

   function onSerFlag(){
       var fm = document.view;
       if(fm.serflag[0].checked){
          document.all('id_sp').style.display="none";
          document.all('id_group').style.display="none";
       } else if(fm.serflag[1].checked){
          var len = fm.spindex.length;
          if(len==0){
             alert('<fmt:message key="operInfo.sprequired"/>');
             fm.serflag[0].checked = true;
             document.all('id_sp').style.display="none";
             document.all('id_group').style.display="none";
             return;
          }
          document.all('id_sp').style.display="block";
          document.all('id_group').style.display="none";
      }else if(fm.serflag[2].checked){
          document.all('id_sp').style.display="none";
          document.all('id_group').style.display="block";
      }
    }
		function checkstring(checkstr,userinput)
		{
		  var allValid = true;
		  for (i = 0;i<userinput.length;i++)
		  {
		    ch = userinput.charAt(i);
		    if(checkstr.indexOf(ch) == -1)
		    {
		      allValid = false;
		      break;
		    }
		  }
		
		  if (!allValid) return (false);
		  else return (true);
		}
		function checkpwd0(pwd){
		  var allValid = true;
		  for (i = 0;i<pwd.length;i++)
		  {

		    ch = pwd.charAt(i);
		    if((ch<='9' && ch >= '0') || (ch <='z' && ch >= 'a') ||(ch <='Z' && ch >='A')){ 
		    }
		    else{
		     allValid = false;
		    }
		  }
		  if (!allValid) return (false);
		  else return (true);
		}
		function checkpwd1(pwd){
		  var allValid = true;
		  for (i = 0;i<pwd.length;i++)
		  {

		    ch = pwd.charAt(i);
		    if((ch<='9' && ch >= '0')){ 
		    }
		    else{
		     allValid = false;
		    }
		  }
		  if (!allValid) return (false);
		  else return (true);
		}
		function checkpwd2(pwd){
		  var allValid = true;
		  for (i = 0;i<pwd.length;i++)
		  {
		    ch = pwd.charAt(i);
		    if((ch <='z' && ch >= 'a') ||(ch <='Z' && ch >='A')){ 
		    }
		    else{
		     allValid = false;
		    }
		  }
		  if (!allValid) return (false);
		  else return (true);
		}
		function checkpwd3(pwd){
		  var allValid = true;
		  for (i = 0;i<pwd.length;i++)
		  {
		    ch = pwd.charAt(i);
		    if(((ch<='9' && ch >= '0')||(ch <='z' && ch >= 'a') ||(ch <='Z' && ch >='A'))&&!checkpwd2(pwd) &&!checkpwd1(pwd) ){
		    }
		    else{
		     allValid = false;
		    }
		  }
		  if (!allValid) return (false);
		  else return (true);
		}				
		
		function isEmail(strEmail) {
     return (strEmail.search(/^\w+((-\w+)|(\.\w+))*\@\w+((\.|-)\w+)*\.\w+$/) != -1);
    }   
	
	function notify_onchange( thisform ){
		var notiy_value = thisform.notify.value;
		if(notiy_value == 0){
			document.getElementById('notifyContent').style.display="none";
			//thisform.phonenumber.style.display="none";
			//alert(document.getElementById('emaillabel'));
			//document.getElementById("email_").style.display="none";
			//thisform.email.style.display="none";
		}
		if(notiy_value == 1){
			document.getElementById('notifyContent').style.display="block";
			thisform.phonenumber.style.display="block";
			document.getElementById('emaillabel').style.display="none";
			document.getElementById('phonenumberlabel').style.display="block";
			thisform.email.style.display="none";
		}
		if(notiy_value == 2){
			document.getElementById('notifyContent').style.display="block";
			thisform.phonenumber.style.display="none";
			document.getElementById('emaillabel').style.display="block";
			document.getElementById('phonenumberlabel').style.display="none";
			thisform.email.style.display="block";
		}
	}

</script>
</head>
<body class="body-style1" onunload="javascript:unLoad()" onload="initform(document.forms[0])">
<form name="view" method="post" action="operInfoEnd.jsp">
<input type="hidden" name="flag" value="<%= flagString %>">
<input type="hidden" name="allowIP" value="">
<input type="hidden" name="noAllowIP" value="">
<%
if (! flag) {
%>
<input type="hidden" name="operID" value="<%= operID %>">
<%
}
%>
<table border="0" width="650" class="text-default">
  <tr>
    <td colspan="2">
      <table border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
       <tr class="tr-ring">
          <td width="105" height="22" align="right"><fmt:message key="operDel.opername"/></td>
          <td width="120" height="22"><input type="text" name="operName" value="<%= flag ? "" : (String)map.get("OPERNAME") %>" maxlength="20"  size ="15"></td>
          <td width="160" height="22"><input type="checkbox" name="nextUpdPwd" onclick="javascript:nextUpdPwdClicked()" <%= flag || ((String)map.get("NEXTUPDPWD")).equals("1") ? "checked" : "" %> ><fmt:message key="operInfo.changepwdwhenlogin"/></td>
          <td height="22"><input type="checkbox" name="maxWrongTimes" onclick="javascript:changeWrongTimes()" <%= ((!flag) && ((String)map.get("MAXWRGLOG")).equals("0")) ? "" : "checked" %>><fmt:message key="operInfo.continuouswronglogin"/></td>
<%
        if (!flag && (((String)map.get("MAXWRGLOG")).equals("0"))) {
%>
          <td height="22"><input type="text" name="wrongTimes" value="<%= pmap.get(i18n.getMessage("purview.maxwrglog"))%>" disabled size ="13"></td>
<%
        }
        else if(flag){
%>
          <td height="22"><input type="text" name="wrongTimes" value="<%= pmap.get(i18n.getMessage("purview.maxwrglog")) %>" size ="13"></td>
<%
        }else{
%>
          <td height="22"><input type="text" name="wrongTimes" value="<%= map.get("MAXWRGLOG") %>" size ="13"></td>
<%
				}
%>

        </tr>
        <tr class="tr-ring">
          <td height="22" align="right"><fmt:message key="operDel.operfullname"/></td>
          <td height="22"><input type="text" name="operAllName" value="<%= flag ? "" : (String)map.get("OPERALLNAME") %>" maxlength="40" size ="15"></td>
          <td height="22"><input type="checkbox" name="userLocked" <%= (! flag) && (((String)map.get("OPERSTATUS")).equals("1") || ((String)map.get("OPERSTATUS")).equals("4")) ? "checked" : "" %> <%= (! flag) && ((String)map.get("OPERSTATUS")).equals("10") ? "disabled" : "" %>><fmt:message key="operInfo.accountlocked"/></td>
          <td height="22"><input type="checkbox" name="pwdMustChange1" onclick="javascript:changePasswordChange()" <%= (!flag && (((String)map.get("PWDMUSTCHANGE")).equals("0"))) ? "" : "checked" %>><fmt:message key="operInfo.pwdexpiratioin"/></td>
<%
        if (!flag && (((String)map.get("PWDMUSTCHANGE")).equals("0"))) {
%>
          <td height="22"><input type="text" name="passwordChange" value="<%= pmap.get(i18n.getMessage("purview.pwdmaxday"))%>" disabled size ="13"></td>
<%
        }else if(flag) {
%>
          <td height="22"><input type="text" name="passwordChange" value="<%= pmap.get(i18n.getMessage("purview.pwdmaxday")) %>" size ="13"></td>
<%
        }else{
%>
          <td height="22"><input type="text" name="passwordChange" value="<%= map.get("PWDMUSTCHANGE") %>" size ="13"></td>
<%
				}
%>
        </tr>
        <tr class="tr-ring">
          <td height="22" align="right"><fmt:message key="operDel.description"/></td>
          <td height="22"><input type="text" name="operDescription" value="<%= flag ? "" : (String)map.get("OPERDESCRIPTION") %>" maxlength="40" size ="15"></td>
          <td height="22"><input type="checkbox" name="pwdMustChange" <%= (! flag) && ((String)map.get("PWDMUSTCHANGE")).equals("0") ? "checked" : "" %> onclick="javascript:changePwdMustChange()"><fmt:message key="operInfo.pwdneverexpired"/></td>
          <td height="22"><input type="checkbox" name="useDate" onclick="javascript:changeUserDate()" <%= flag || ((String)map.get("USEDATE")).equals("999") ? "" : "checked" %>><fmt:message key="operInfo.accountexpiration"/></td>
<%
        if (flag || ((String)map.get("USEDATE")).equals("999")) {
%>
          <td height="22"><input type="text" name="endDate" value="<%= (purview.nowString()).substring(0,11) %>" disabled size ="13"></td>
<%
        }
        else {
%>
          <td height="22"><input type="text" name="endDate" value="<%= (String)map.get("USEDATE") %>" size ="13"></td>
<%
        }
%>
        </tr>
        <tr class="tr-ring">
          <td height="22" align="right" ><fmt:message key="operInfo.password"/></td>
          <td height="22"><input type="<%= !flag ||( !"2".equals((String)pmap.get(i18n.getMessage("purview.createtype"))))?  "password":"text" %>" name="operPwd" value="<%= flag ? initPwd:(String)map.get("OPERPWD") %>" maxlength="<%= pmap.get(i18n.getMessage("purview.maxlen"))%>" size ="15" onchange="onpasswordfocus()"></td>
          <td height="22"><input type="checkbox" name="userForbid" <%= (! flag) && (((String)map.get("OPERSTATUS")).equals("3") || ((String)map.get("OPERSTATUS")).equals("4")) ? "checked" : "" %> <%= (! flag) && ((String)map.get("OPERSTATUS")).equals("10") ? "disabled" : "" %>><fmt:message key="operInfo.accounttempforbid"/></td>
          <td width="160" height="22"><input type="checkbox" name="maxNoUseDays" onclick="javascript:changeUseDays()" <%= flag || ((String)map.get("MAXNOUSEDAYS")).equals("0") ? "" : "checked" %>><fmt:message key="operInfo.accountinactive"/></td>
<%
        if (flag || ((String)map.get("MAXNOUSEDAYS")).equals("0")) {
%>
          <td width="75" height="22"><input type="text" name="noUseDays" value="90" disabled size ="13"></td>
<%
        }
        else {
%>
          <td width="75" height="22"><input type="text" name="noUseDays" value="<%= (String)map.get("MAXNOUSEDAYS") %>" size ="13"></td>
<%
        }
%>
        </tr>
        <tr class="tr-ring">
          <td height="22" align="right"><fmt:message key="operInfo.pwdconfirm"/></td>
          <td height="22"><input type="<%= !flag ||( !"2".equals((String)pmap.get(i18n.getMessage("purview.createtype"))))?  "password":"text" %>" name="operConfirm" value="<%= flag ? initPwd : (String)map.get("OPERPWD") %>" maxlength="<%= pmap.get(i18n.getMessage("purview.maxlen"))%>" size ="15" onchange="onpasswordfocus()"></td>
          <td height="22" align="left"><input type="checkbox" name="" disabled /><fmt:message key="operInfo.notify.mode"/></td>
          <td>
          <select name="notify" style="width:152px" onchange="notify_onchange(this.form);">
          <option value="0"><fmt:message key="operInfo.notnotify"/></option>
          <option value="1"<%= ( !flag && ( (String)map.get("PHONENUMBER")).trim().length()>0 )? "selected":"" %>><fmt:message key="operInfo.phonenumber"/></option>
          <option value="2"<%= ( !flag &&  ( (String)map.get("EMAIL")).trim().length()>0) ? "selected":"" %>><fmt:message key="operInfo.email"/></option>
          </select>
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr class="tr-ring" name="notifyContent" id="notifyContent">
          <td height="22" align="right"><span name="phonenumberlabel" id="phonenumberlabel" ><fmt:message key="operInfo.phonenumber"/></span> </td>
          <td height="22"><input type="text" name="phonenumber" value="<%= flag ? "" : (String)map.get("PHONENUMBER") %>" maxlength="40" size ="14"> </td>
          <td height="22" align="left"><span name="emaillabel" id="emaillabel" ><input type="checkbox" name="" disabled /><fmt:message key="operInfo.email"/></span> </td>
          <td height="22"><input type="text" name="email" value="<%= flag ? "" : (String)map.get("EMAIL") %>" maxlength="40" size ="19"> </td>
          <td>&nbsp;</td>
        </tr>
        <tr class="tr-ring">
          <td height="22" align="right"><fmt:message key="allotrights.managesystem"/></td>
          <td colspan=4>
           <select name="servicekey" style="width:152px">
           <%
             
            ArrayList serviceList = purview.getServiceList("all");
            String  optServiceList = "";
            for (int i = 0; i < serviceList.size(); i++) {
	            HashMap sm = (HashMap)serviceList.get(i);
	            if(map!=null && sm.get("SERVICEKEY").equals(map.get("SERVICEKEY"))){
	              optServiceList = optServiceList + "<option value=\"" +  (String)sm.get("SERVICEKEY") +"\" selected >" + (String)sm.get("DESCRIPTION") + "</option>";
	            }
	            else{
	              optServiceList = optServiceList + "<option value=\"" +  (String)sm.get("SERVICEKEY") +"\" >" + (String)sm.get("DESCRIPTION") + "</option>";
	            }
            }
            out.println(optServiceList);              
            
           %>
          </td>
        </tr>
        <tr class="tr-ring" >
		    <td align="right" ><fmt:message key="operInfo.operatortype"/></td>
            <td colspan=4>
              <table cellspacing="0" cellpadding="0" class="table-style5" border="0">
               <tr>
                <td width=100 align="center"> <input type="radio" checked name="serflag" value="0" onclick="onSerFlag()"><fmt:message key="operInfo.sysmanager"/></td>
                <td width=100 align="center"  <%= purview.showSpCategory()?  "" : "style=\"DISPLAY: none\""%>> <input type="radio"  name="serflag" value="1" onclick="onSerFlag()" ><fmt:message key="operInfo.spmanager"/></td>
                <td width=100 align="center"  <%= purview.showGrpCategory()?  "" : "style=\"DISPLAY: none\""%>> <input type="radio"  name="serflag" value="3" onclick="onSerFlag()" ><fmt:message key="operInfo.grpmanager"/></td>
               </tr>
              </table>
            </td>
		</tr>
		<tbody id="id_sp" <%= session.getAttribute("YWYY_CODE_ATTRIBUTE")!=null&&"2".equals((String)session.getAttribute("YWYY_CODE_ATTRIBUTE"))? "" : "style=\"DISPLAY: none\""%> >
        <tr class="tr-ring" >
		    <td  align="right"><fmt:message key="operInfo.choosesp"/></td>
            <td colspan=4 >&nbsp;&nbsp;
              <select name="spindex" style="width:150px" >
               <option value="-1"><fmt:message key="operInfo.spindex.reserve"/></option>
              <%  
                if(session.getAttribute("YWYY_CODE_ATTRIBUTE")==null || !"-1".equals(session.getAttribute("YWYY_CODE_ATTRIBUTE")))
                {
                 out.print(strOption); 
                }%>
              </select>
            </td>
		</tr>

		</tbody>
		<tbody id="id_group" <%= session.getAttribute("YWYY_CODE_ATTRIBUTE")!=null&&"3".equals((String)session.getAttribute("YWYY_CODE_ATTRIBUTE"))? "" : "style=\"DISPLAY: none\""%>>
		<tr class="tr-ring" >
		    <td align="right"><fmt:message key="operInfo.inputgrpcode"/></td>
            <td colspan=4>&nbsp;&nbsp;
              <select name="groupid">      
              <%  
                 out.print(grpOption); 
                %>
              </select>              
              </td>
            </td>
		</tr>
        </tbody>
      </table>
    </td>
  </tr>

  <tr>
    <td width="50%" valign="top">
      <table width="100%" border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
        <tr class="table-title1">
          <td height="24" colspan="2"><fmt:message key="operInfo.logintime"/></td>
        </tr>
<%
        String workDay = "";
		String beginTime = "";
		String endTime = "";
        if (flag || ((String)map.get("BEGINTIME")).equals("999")){
            workDay = "1111111";
			beginTime = "999";
		}
        else{
            workDay = (String)map.get("WORKDAY");
			beginTime = (String)map.get("BEGINTIME");
			endTime = (String)map.get("ENDTIME");
		}
%>
       <tr class="tr-ring">
          <td width="50%" height="22"><input type="radio" name="onLimit" value="0" <%= beginTime.equals("999") ? "checked" : "" %> onclick="javascript:changeLimit()"><fmt:message key="operInfo.loginanytime"/></td>
          <td width="50%" height="22"><input type="radio" name="onLimit" value="1" <%= beginTime.equals("999") ? "" : "checked" %> onclick="javascript:changeLimit()"><fmt:message key="operInfo.loginjustperiod"/></td>
        </tr>
        <tr class="tr-ring">
          <td height="22"><input type="checkbox" name="sunday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(0,1)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.sunday"/></td>
          <td height="22"><input type="checkbox" name="monday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(1,2)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.monday"/></td>
        </tr>
        <tr class="tr-ring">
          <td height="22"><input type="checkbox" name="tuesday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(2,3)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.tues"/></td>
          <td height="22"><input type="checkbox" name="wednesday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(3,4)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.wed"/></td>
        </tr>
        <tr  class="tr-ring">
          <td height="22"><input type="checkbox" name="thursday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(4,5)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.thurs"/></td>
          <td height="22"><input type="checkbox" name="friday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(5,6)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.fri"/></td>
        </tr>
        <tr  class="tr-ring">
          <td height="22"><input type="checkbox" name="saturday" <%= workDay.equals("1111111") ? "disabled" : "" %> <%= (workDay.substring(6,7)).equals("1") ? "checked" : "" %>><fmt:message key="operInfo.sat"/></td>
          <td height="22">&nbsp;</td>
        </tr>
        <tr align="center" class="tr-ring">
          <td height="22" colspan="2"><fmt:message key="operInfo.from"/>
            <input type="text" name="beginTime" value='<%= beginTime.equals("999") ? "08:00:00" : beginTime %>' disabled class="input-style2"><fmt:message key="operInfo.to"/><input type="text" name="endTime" value='<%= beginTime.equals("999") ? "18:00:00" : endTime %>' disabled class="input-style2"></td>
        </tr>
      </table>
	  <script language="javascript">
			changeLimit();
		</script>
    </td>
<%
        ArrayList allowIP = new ArrayList();
        ArrayList noAllowIP = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            ipMap = (HashMap)list.get(i);
            if (((String)ipMap.get("ALLOWFLAG")).equals("1"))
                allowIP.add((String)ipMap.get("IPADDR"));
            else
                noAllowIP.add((String)ipMap.get("IPADDR"));
        }
%>
    <td width="50%" valign="top">
      <table width="100%" border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
        <tr class="table-title1">
          <td height="24" colspan="2"><fmt:message key="operInfo.loginhost"/></td>
        </tr>
        <tr class="tr-ring"></tr>
          <td height="22" colspan="2"><input type="checkbox" name="noIP" onclick="javascript:changeIP()" <%= list.size() > 0 ? "" : "checked" %>><fmt:message key="operInfo.anyip"/></td>
        </tr>
        <tr class="tr-ring">
          <td height="22" colspan="2"><fmt:message key="operInfo.ipaddr"/>
            <input type="text" name="ipAddress" disabled class="input-style1"></td>
        </tr>
        <tr align="center" class="tr-ring">
          <td width="50%" height="22">
            <table width="100%" border="0" class="table-style5">
              <tr align="center">
                <td width="50%"><input type="button" name="addAllowIP" value="+" class="button-style3" onclick="javascript:addIP(document.view.allowedIP)"></td>
                <td width="50%"><input type="button" name="delAllowIP" value="-" class="button-style3" onclick="javascript:delIP(document.view.allowedIP)"></td>
              </tr>
            </table>          </td>
          <td width="50%" height="22">
            <table width="100%" border="0" class="table-style5">
              <tr align="center">
                <td width="50%"><input type="button" name="addForbidIP" value="+" class="button-style3" onclick="javascript:addIP(document.view.forbidedIP)"></td>
                <td width="50%"><input type="button" name="delForbidIP" value="-" class="button-style3" onclick="javascript:delIP(document.view.forbidedIP)"></td>
              </tr>
            </table>          </td>
        </tr>
        <tr  class="table-title1">
          <td height="22"><fmt:message key="operInfo.permitlogin"/></td>
          <td height="22"><fmt:message key="operInfo.forbitlogin"/></td>
        </tr>
        <tr  class="tr-ring">
          <td height="22">
            <select size="4" name="allowedIP" class="input-style1">
<%
        for (int i = 0; i < allowIP.size(); i++) {
%>
              <option value="<%= (String)allowIP.get(i) %>"><%= (String)allowIP.get(i) %></option>
<%
        }
%>
            </select>          </td>
          <td height="22">
            <select size="4" name="forbidedIP" class="input-style1">
<%
        for (int i = 0; i < noAllowIP.size(); i++) {
%>
              <option value="<%= (String)noAllowIP.get(i) %>"><%= (String)noAllowIP.get(i) %></option>
<%
        }
%>
            </select>          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr align="center">
    <td><input type="button" name="sure" value="<fmt:message key="operDel.yes"/>" class="button-style1" onclick="javascript:onSubmit()"></td>
    <td><input type="button" name="quit" value="<fmt:message key="operDel.cancel"/>" class="button-style1" onclick="javascript:window.close()"></td>
  </tr>
</table>
</form>
<script language="javascript">
   changePwdMustChange();
   changeIP();
   notify_onchange(document.forms[0]);
</script>
</body>
</html>
<%
    }
    catch (Exception e) {
    e.printStackTrace();
%>
<html>
<body>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
</body>
</html>
<%
    }
%>

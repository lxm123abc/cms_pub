package com.zte.cms.channel.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICategoryChannelMapLS
{

    public TableDataInfo pageInfo(CategoryChannelMap categoryChannelMap, int start, int pageSize);

    public String insertCategoryChannel(CategoryChannelMap categoryChannelMap);

    public String deleteCategoryChannel(CategoryChannelMap categoryChannelMap[]);

    public String publishCategoryChannel(
			List<CntPlatformSync> cntPlatformSynclist);

//    public String canclePubCategoryChannel(CategoryChannelMap categoryChannelMapList[]);
    public HashMap publishCategoryChannelTar(List<CntTargetSync> tarlist);
    public String cancelpublishCategoryChannel(
			List<CntPlatformSync> cntPlatformSynclist);
    public HashMap cancelpublishCategoryChannelTar(List<CntTargetSync> tarlist);
    public HashMap cancelpublishCategoryChannelSingle(List<CntPlatformSync> cntPlatformSynclist);
    public HashMap publishCategoryChannelSingle(List<CntPlatformSync> cntPlatformSynclist);

}
package com.zte.cms.settlementDataRpt.ls;

import com.zte.cms.settlementDataRpt.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

/**
 * @Author: Liangxiaomin
 * @Date Created in 14:52 2019/6/27
 * @Description:
 */
public interface OperatorsController {

    /**
     *  查询oper表
     * @param operInformation
     * @param arg1
     * @param arg2
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(OperInformation operInformation, int arg1, int arg2) throws Exception;

}

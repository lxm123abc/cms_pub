package com.zte.cms.channel.apply;

import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 
 * @author Administrator
 * 
 */
public interface IChannelApply
{

    /**
     * 工作流申请
     * 
     * @param channelIdx Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String apply(Long channelIdx) throws DomainServiceException;

    /**
     * 
     * @param channel CmsChannel
     * @param templateRoute String
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String apply(CmsChannel channel, String templateRoute, String priority)
            throws DomainServiceException;

    /**
     * 工作流审核
     * 
     * @param channel CmsChannel
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public abstract String audit(CmsChannel channel, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException;

    /**
     * 
     * @param channelWkfwhis ChannelWkfwhis
     * @return ChannelWkfwhis
     * @throws Exception Exception
     */
    public ChannelWkfwhis getChannelWkfwHis(ChannelWkfwhis channelWkfwhis) throws Exception;

}
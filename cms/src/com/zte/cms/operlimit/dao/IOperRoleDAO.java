package com.zte.cms.operlimit.dao;


import java.util.List;


import com.zte.cms.operlimit.model.OperRole;
import com.zte.cms.operlimit.model.OperatorLimit;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IOperRoleDAO
{
    
    public List getOperRole(OperRole operrole) ;
    public void insertOperRole(OperatorLimit OperRole) throws DAOException;
    public void deleteOperRole(OperatorLimit OperRole) throws DAOException;
    public List getFuncById(OperatorLimit OperRole);
    
    
}
package com.zte.cms.commonSync.object;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.framework.base.ISysCode;

public class CreateXmlFile4Channel extends AbstractCreateXmlFile4Object {
	private final static String CMSCASTMTYPE_CNTSYNCXML = "xmlsync";

	public String createXmlFile4WG18(Map<String, List> objectMap,
			int targetType, int syncType) throws DomainServiceException {
		// TODO Auto-generated method stub

		String action = null;
		//		if (0 != targetType) {
		//			return null;
		//		}
		// 获取发布Action类型
		if (1 == syncType) {
			action = "REGIST";
		} else if (2 == syncType) {
			action = "UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}
		
		Date date = new Date();
		String fileName = "channel_physicalchannel" +"_"+ "20wg" + "_" +action.toLowerCase().replace(" ", "")+"_"+ date.getTime()
				+ ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
				+ File.separator + getCurrentTime() + File.separator
				+ "channel" + File.separator;

		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		String fileFullName = absolutePath + fileName;

		

		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		List<CmsChannel> channellist = (List<CmsChannel>) objectMap
				.get("channellist");

		Element progobjectElement = null;
		Element progpertyElement = null;
		// 元素节点
		progobjectElement = objectsElement.addElement("Object");
		progobjectElement.addAttribute("ElementType", "Channel");
		progobjectElement.addAttribute("ID", channellist.get(0).getCpcontentid());
		progobjectElement.addAttribute("Code", channellist.get(0)
				.getCpcontentid());
		progobjectElement.addAttribute("Action", action);

		writeElement(progobjectElement, progpertyElement, "Name", channellist
				.get(0).getChannelname(), true);
		writeElement(progobjectElement, progpertyElement, "ChannelNumber",
				channellist.get(0).getChannelnumber(), false);
		writeElement(progobjectElement, progpertyElement, "CallSign",
				channellist.get(0).getCallsign(), true);
		writeElement(progobjectElement, progpertyElement, "TimeShift",
				channellist.get(0).getTimeshift(), true);
		writeElement(progobjectElement, progpertyElement, "StorageDuration",
				channellist.get(0).getStorageduration(), true);
		writeElement(progobjectElement, progpertyElement, "TimeShiftDuration",
				channellist.get(0).getTimeshiftduration(), true);

		writeElement(progobjectElement, progpertyElement, "Description",
				channellist.get(0).getDescription(), true);
		writeElement(progobjectElement, progpertyElement, "Country",
				channellist.get(0).getCountry(), true);
		writeElement(progobjectElement, progpertyElement, "State", channellist
				.get(0).getState(), true);
		writeElement(progobjectElement, progpertyElement, "City", channellist
				.get(0).getCity(), true);
		writeElement(progobjectElement, progpertyElement, "ZipCode",
				channellist.get(0).getZipcode(), false);
		writeElement(progobjectElement, progpertyElement, "Type", channellist
				.get(0).getChanneltype(), false);
		writeElement(progobjectElement, progpertyElement, "SubType",
				channellist.get(0).getSubtype(), false);
		writeElement(progobjectElement, progpertyElement, "Language",
				channellist.get(0).getLanguage(), true);
		writeElement(progobjectElement, progpertyElement, "Status",
				channellist.get(0).getIsvalid(), false);
		writeElement(progobjectElement, progpertyElement, "StartTime",
				channellist.get(0).getStarttime(), false);
		writeElement(progobjectElement, progpertyElement, "EndTime",
				channellist.get(0).getEndtime(), false);
		writeElement(progobjectElement, progpertyElement, "Macrovision",
				channellist.get(0).getMacrovision(), false);
		writeElement(progobjectElement, progpertyElement, "VideoType",
				channellist.get(0).getVideotype(), true);
		writeElement(progobjectElement, progpertyElement, "AudioType",
				channellist.get(0).getAudiotype(), true);
		writeElement(progobjectElement, progpertyElement, "StreamType",
				channellist.get(0).getStreamtype(), true);
		writeElement(progobjectElement, progpertyElement, "Bilingual",
				channellist.get(0).getBilingual(), false);
		writeElement(progobjectElement, progpertyElement, "URL", channellist
				.get(0).getWeburl(), true);
		List phylist = objectMap.get("physicalchannellist");
		if(phylist!=null&&phylist.size()>0){
			for(int i=0;i<phylist.size();i++){
				Physicalchannel thephysicalchannel = (Physicalchannel) phylist
				.get(i);
				progobjectElement = objectsElement.addElement("Object");
				progobjectElement.addAttribute("ElementType", "PhysicalChannel");
				progobjectElement.addAttribute("ID", thephysicalchannel.getCpcontentid());
				progobjectElement.addAttribute("Code", thephysicalchannel.getCpcontentid());
				progobjectElement.addAttribute("Action", action);

				writeElement(progobjectElement, progpertyElement, "ChannelID", channellist.get(0).getCpcontentid()
						, false);
				writeElement(progobjectElement, progpertyElement, "ChannelCode", channellist.get(0).getCpcontentid()
						, false);
				writeElement(progobjectElement, progpertyElement, "BitRateType", thephysicalchannel.getBitratetype()
						, false);
				writeElement(progobjectElement, progpertyElement, "MultiCastIP", thephysicalchannel.getMulticastip()
						, true);
				writeElement(progobjectElement, progpertyElement, "MultiCastPort", thephysicalchannel.getMulticastport()
						, false);
				
			}
		}
		// 把对象写入xml文件
		writeDom(dom, fileFullName);

		return retPath;
	}

	public String createXmlFile4IPTV30(Map<String, List> objectMap,
			int targetType, int syncType) {
		// TODO Auto-generated method stub
		CmsChannel channel = new CmsChannel();
		List list = objectMap.get("channellist");
		if (list != null && list.size() > 0) {
			channel = (CmsChannel) list.get(0);
		}
		String targetTypestr = "";
		
		if(targetType==1){
			targetTypestr="bms";
		}
		if(targetType==2){
			targetTypestr="epg";
		}
		if(targetType==3){
			targetTypestr="cdn";
		}
		Date date = new Date();
		String dir = getCurrentTime();
		String fileName = "";
		String synctype = "";
		if (1 == syncType || 2 == syncType) {
            synctype = "REGIST or UPDATE";
        } else if (3 == syncType) {
            synctype = "DELETE";
        } else {
            return null;
        }
		if(targetType==1||targetType==2){
			fileName = "channel_physicalchannel" +"_"+ targetTypestr + "_" +synctype.toLowerCase().replace(" ", "")+"_"+date.getTime()
		+ ".xml";	
		}else{
			fileName = "physicalchannel"  +"_"+ targetTypestr + "_" +synctype.toLowerCase().replace(" ", "")+"_"+ date.getTime()
			+ ".xml";
		}
		
		String tempFilePath = tempAreaPath + File.separator
				+ CMSCASTMTYPE_CNTSYNCXML + File.separator + dir
				+ File.separator + "channel" + File.separator;
		String mountAndTempfilePath = mountPoint + tempFilePath;
		String retPath = filterSlashStr(tempFilePath + fileName);
		File file = new File(mountAndTempfilePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String fileFullName = mountAndTempfilePath + fileName;
		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		Element objectElement = null;
		Element propertyElement = null;

		
		// CDN网元
		if (targetType != 3) {

			// 元素节点
			objectElement = objectsElement.addElement("Object");
			objectElement.addAttribute("Action", synctype);
			objectElement.addAttribute("ElementType", "Channel");
			objectElement.addAttribute("ContentID", channel.getChannelid());

			// 属性节点
			writeElement(objectElement,propertyElement,"Name",channel.getChannelname(),true);
			writeElement(objectElement,propertyElement,"ChannelNumber",channel.getChannelnumber(),false);
			writeElement(objectElement,propertyElement,"Type",channel.getChanneltype(),false);
			writeElement(objectElement,propertyElement,"Status",channel.getIsvalid(),false);
			writeElement(objectElement,propertyElement,"CallSign",channel.getCallsign(),false);
			writeElement(objectElement,propertyElement,"TimeShift",channel.getTimeshift(),false);
			writeElement(objectElement,propertyElement,"StartTime",channel.getStarttime(),false);
			writeElement(objectElement,propertyElement,"EndTime",channel.getEndtime(),false);
			writeElement(objectElement,propertyElement,"CPContentID",channel.getCpcontentid(),true);
			writeElement(objectElement,propertyElement,"StorageDuration",channel.getStorageduration(),false);			
			writeElement(objectElement,propertyElement,"TimeShiftDuration",channel.getTimeshiftduration(),false);
			writeElement(objectElement,propertyElement,"Description",channel.getDescription(),true);
			writeElement(objectElement,propertyElement,"Country",channel.getCountry(),true);
			writeElement(objectElement,propertyElement,"State",channel.getState(),true);
			writeElement(objectElement,propertyElement,"City",channel.getCity(),true);
			writeElement(objectElement,propertyElement,"ZipCode",channel.getZipcode(),true);
			writeElement(objectElement,propertyElement,"SubType",channel.getSubtype(),false);
			writeElement(objectElement,propertyElement,"Language",channel.getLanguage(),true);
			writeElement(objectElement,propertyElement,"Macrovision",channel.getMacrovision(),false);
			writeElement(objectElement,propertyElement,"VideoType",channel.getVideotype(),true);
			writeElement(objectElement,propertyElement,"AudioType",channel.getAudiotype(),true);
			writeElement(objectElement,propertyElement,"StreamType",channel.getStreamtype(),false);
			writeElement(objectElement,propertyElement,"Bilingual",channel.getBilingual(),false);
			writeElement(objectElement,propertyElement,"URL",channel.getWeburl(),true);
			writeElement(objectElement,propertyElement,"UniContentId",channel.getUnicontentid(),true);
		}
		// 得到频道下所有的物理频道
		List phylist = objectMap.get("physicalchannellist");
		if (phylist != null && phylist.size() > 0) {
			for (int i = 0; i < phylist.size(); i++) {
				Physicalchannel thephysicalchannel = (Physicalchannel) phylist
						.get(i);
				objectElement = objectsElement.addElement("Object");
				objectElement.addAttribute("ElementType", "PhysicalChannel");
				objectElement.addAttribute("PhysicalContentID",
						thephysicalchannel.getPhysicalchannelid());
				
				if (1 == syncType || 2 == syncType) {
					synctype = "REGIST or UPDATE";
				} else if (3 == syncType) {
					synctype = "DELETE";
				} else {
					return null;
				}
				objectElement.addAttribute("Action",
						synctype);
				// 属性节点
				writeElement(objectElement,propertyElement,"DestCastType",thephysicalchannel.getDestcasttype(),true);
				writeElement(objectElement,propertyElement,"SrcCastType",thephysicalchannel.getSrccasttype(),true);
				writeElement(objectElement,propertyElement,"ChannelID",thephysicalchannel.getChannelid(),false);
				writeElement(objectElement,propertyElement,"BitRateType",thephysicalchannel.getBitratetype(),false);
				writeElement(objectElement,propertyElement,"CPContentID",thephysicalchannel.getCpcontentid(),true);
				writeElement(objectElement,propertyElement,"MultiCastIP",thephysicalchannel.getMulticastip(),true);
				writeElement(objectElement,propertyElement,"MultiCastPort",thephysicalchannel.getMulticastport(),false);
				writeElement(objectElement,propertyElement,"UnicastUrl",thephysicalchannel.getUnicasturl(),true);
				writeElement(objectElement,propertyElement,"VideoType",thephysicalchannel.getVideotype(),true);
				writeElement(objectElement,propertyElement,"AudioType",thephysicalchannel.getAudiotype(),true);
				writeElement(objectElement,propertyElement,"Resolution",thephysicalchannel.getResolution(),false);
				writeElement(objectElement,propertyElement,"Video Profile",thephysicalchannel.getVideoprofile(),false);
				writeElement(objectElement,propertyElement,"System Layer",thephysicalchannel.getSystemlayer(),false);
				writeElement(objectElement,propertyElement,"Domain",thephysicalchannel.getDomain(),false);
				writeElement(objectElement,propertyElement,"Hotdegree",thephysicalchannel.getHotdegree(),false);
			}
		}

		writeDom(dom, fileFullName);

		return retPath;
	}

	private static String filterSlashStr(String str) {
		String rtnStr = "";
		if (!str.trim().equals("")) {
			if (str.indexOf("\\", 0) != -1) {
				while (str.length() > 0) {
					if (str.indexOf("\\", 0) > -1) {
						rtnStr += str.subSequence(0, str.indexOf("\\", 0));
						rtnStr += "/";
						str = str.substring(str.indexOf("\\", 0) + 1, str
								.length());
					} else {
						rtnStr += str;
						str = "";
					}
				}
			} else {

				rtnStr = str;
			}
		}
		return rtnStr;
	}

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
            throws DomainServiceException
    {
        String action = null;
        //      if (0 != targetType) {
        //          return null;
        //      }
        // 获取发布Action类型
        if (1 == syncType) {
            action = "REGIST";
        } else if (2 == syncType) {
            action = "UPDATE";
        } else if (3 == syncType) {
            action = "DELETE";
        } else {
            return null;
        }
        
        Date date = new Date();
        String fileName = "channel_physicalchannel" +"_"+ "20ys" + "_" +action.toLowerCase().replace(" ", "")+"_"+ date.getTime()
                + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "channel" + File.separator;

        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants
                .filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }

        String fileFullName = absolutePath + fileName;

        

        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        List<CmsChannel> channellist = (List<CmsChannel>) objectMap
                .get("channellist");

        Element progobjectElement = null;
        Element progpertyElement = null;
        // 元素节点
        progobjectElement = objectsElement.addElement("Object");
        progobjectElement.addAttribute("ElementType", "Channel");
        progobjectElement.addAttribute("ID", channellist.get(0).getCpcontentid());
        progobjectElement.addAttribute("Code", channellist.get(0)
                .getCpcontentid());
        progobjectElement.addAttribute("Action", action);

        writeElement(progobjectElement, progpertyElement, "Name", channellist
                .get(0).getChannelname(), true);
        writeElement(progobjectElement, progpertyElement, "ChannelNumber",
                channellist.get(0).getChannelnumber(), false);
        writeElement(progobjectElement, progpertyElement, "CallSign",
                channellist.get(0).getCallsign(), true);
        writeElement(progobjectElement, progpertyElement, "TimeShift",
                channellist.get(0).getTimeshift(), true);
        writeElement(progobjectElement, progpertyElement, "StorageDuration",
                channellist.get(0).getStorageduration(), true);
        writeElement(progobjectElement, progpertyElement, "TimeShiftDuration",
                channellist.get(0).getTimeshiftduration(), true);

        writeElement(progobjectElement, progpertyElement, "Description",
                channellist.get(0).getDescription(), true);
        writeElement(progobjectElement, progpertyElement, "Country",
                channellist.get(0).getCountry(), true);
        writeElement(progobjectElement, progpertyElement, "State", channellist
                .get(0).getState(), true);
        writeElement(progobjectElement, progpertyElement, "City", channellist
                .get(0).getCity(), true);
        writeElement(progobjectElement, progpertyElement, "ZipCode",
                channellist.get(0).getZipcode(), false);
        writeElement(progobjectElement, progpertyElement, "Type", channellist
                .get(0).getChanneltype(), false);
        writeElement(progobjectElement, progpertyElement, "SubType",
                channellist.get(0).getSubtype(), false);
        writeElement(progobjectElement, progpertyElement, "Language",
                channellist.get(0).getLanguage(), true);
        writeElement(progobjectElement, progpertyElement, "Status",
                channellist.get(0).getIsvalid(), false);
        writeElement(progobjectElement, progpertyElement, "StartTime",
                channellist.get(0).getStarttime(), false);
        writeElement(progobjectElement, progpertyElement, "EndTime",
                channellist.get(0).getEndtime(), false);
        writeElement(progobjectElement, progpertyElement, "Macrovision",
                channellist.get(0).getMacrovision(), false);
        writeElement(progobjectElement, progpertyElement, "VideoType",
                channellist.get(0).getVideotype(), true);
        writeElement(progobjectElement, progpertyElement, "AudioType",
                channellist.get(0).getAudiotype(), true);
        writeElement(progobjectElement, progpertyElement, "StreamType",
                channellist.get(0).getStreamtype(), true);
        writeElement(progobjectElement, progpertyElement, "Bilingual",
                channellist.get(0).getBilingual(), false);
    
        List phylist = objectMap.get("physicalchannellist");
        if(phylist!=null&&phylist.size()>0){
            for(int i=0;i<phylist.size();i++){
                Physicalchannel thephysicalchannel = (Physicalchannel) phylist
                .get(i);
                progobjectElement = objectsElement.addElement("Object");
                progobjectElement.addAttribute("ElementType", "PhysicalChannel");
                progobjectElement.addAttribute("ID", thephysicalchannel.getCpcontentid());
                progobjectElement.addAttribute("Code", thephysicalchannel.getCpcontentid());
                progobjectElement.addAttribute("Action", action);

                writeElement(progobjectElement, progpertyElement, "ChannelID", channellist.get(0).getCpcontentid()
                        , false);
                writeElement(progobjectElement, progpertyElement, "ChannelCode", channellist.get(0).getCpcontentid()
                        , false);
                writeElement(progobjectElement, progpertyElement, "BitRateType", thephysicalchannel.getBitratetype()
                        , false);
                writeElement(progobjectElement, progpertyElement, "MultiCastIP", thephysicalchannel.getMulticastip()
                        , true);
                writeElement(progobjectElement, progpertyElement, "MultiCastPort", thephysicalchannel.getMulticastport()
                        , false);
                
            }
        }
        // 把对象写入xml文件
        writeDom(dom, fileFullName);

        return retPath;
    }
}

package com.zte.cms.content.movie.service;

import java.util.List;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsMovieDS
{
    /**
     * 新增CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsMovie(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovie(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 批量更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieList(List<CmsMovie> cmsMovieList) throws DomainServiceException;

    /**
     * 根据条件更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsMovie对象
     * 
     * @param cmsMovie CmsMovie更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsMovieListByCond(List<CmsMovie> cmsMovieList) throws DomainServiceException;

    /**
     * 删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovie(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 批量删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieList(List<CmsMovie> cmsMovieList) throws DomainServiceException;

    /**
     * 根据条件删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsMovie对象
     * 
     * @param cmsMovie CmsMovie删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsMovieListByCond(List<CmsMovie> cmsMovieList) throws DomainServiceException;

    /**
     * 查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @return CmsMovie对象
     * @throws DomainServiceException ds异常
     */
    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 根据条件查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象
     * @return 满足条件的CmsMovie对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsMovie> getCmsMovieByCond(CmsMovie cmsMovie) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsMovie对象的数量
     * 
     * @param cmsMovie CmsMovie对象
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public int getCmsMovieCount(CmsMovie cmsMovie) throws DomainServiceException;
    /**
     * 根据条件分页查询CmsMovie对象
     * 
     * @param cmsMovie CmsMovie对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws DomainServiceException;
}
package com.zte.ucm.log;

public abstract interface LoggerModel
{
  public static final String MODEL_All = "All";
  public static final String MODEL_DATABASE = "Database";
  public static final String MODEL_THREAD = "Thread";
  public static final String MODEL_CONTROL = "Control";
  public static final String MODEL_INTERFACE = "Interface";
  public static final String MODEL_EXCEPTION = "Exception";
  public static final String MODEL_XML = "xml";
  public static final String MODEL_REPORT = "createReport";
  public static final String MODEL_TOPIC = "topic";
}
/**
 * QueryMediaInfoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public interface QueryMediaInfoService extends javax.xml.rpc.Service
{
    public java.lang.String getQueryMediaInfoServiceHttpPortAddress();

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort getQueryMediaInfoServiceHttpPort()
            throws javax.xml.rpc.ServiceException;

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoPort getQueryMediaInfoServiceHttpPort(java.net.URL portAddress)
            throws javax.xml.rpc.ServiceException;
}

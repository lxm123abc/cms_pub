package com.zte.cms.impSyncTask.dao;

import java.util.List;

import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IObjectCpcntRecordDAO
{
     /**
   * 新增ObjectCpcntRecord对象 
   * 
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */ 
     public void insertObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException;
     /**
   * 新增ObjectCpcntRecord对象 
   * 
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */ 
     public void insertObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException;
     /**
   * 根据主键更新ObjectCpcntRecord对象
   * 
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */
     public void updateObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException;
     /**
   * 根据主键更新ObjectCpcntRecord对象
   * 
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */
     public void updateObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException;

     /**
   * 根据条件更新ObjectCpcntRecord对象  
   *
   * @param objectCpcntRecord ObjectCpcntRecord更新条件
   * @throws DAOException dao异常
   */
     public void updateObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException;
     /**
   * 根据条件更新ObjectCpcntRecord对象  
   *
   * @param objectCpcntRecord ObjectCpcntRecord更新条件
   * @throws DAOException dao异常
   */
     public void updateObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException;

     /**
   * 根据主键删除ObjectCpcntRecord对象
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */
     public void deleteObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException;	
     /**
   * 根据主键删除ObjectCpcntRecord对象
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */
     public void deleteObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException;
     
     /**
   * 根据条件删除ObjectCpcntRecord对象
   *
   * @param objectCpcntRecord ObjectCpcntRecord删除条件
   * @throws DAOException dao异常
   */ 
     public void deleteObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException;
     /**
   * 根据条件删除ObjectCpcntRecord对象
   *
   * @param objectCpcntRecord ObjectCpcntRecord删除条件
   * @throws DAOException dao异常
   */ 
     public void deleteObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException;
     
     /**
   * 根据主键查询ObjectCpcntRecord对象
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @return 满足条件的ObjectCpcntRecord对象
   * @throws DAOException dao异常
   */
     public ObjectCpcntRecord getObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException;
     
     /**
   * 根据条件查询ObjectCpcntRecord对象 
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象
   * @return 满足条件的ObjectCpcntRecord对象集
   * @throws DAOException dao异常
   */
     public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException;


     public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord, PageUtilEntity puEntity )throws DAOException;

     /**
   * 根据条件分页查询ObjectCpcntRecord对象，作为查询条件的参数
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象 
   * @param start 起始行
   * @param pageSize 页面大小
   * @return  查询结果
   * @throws DAOException dao异常
   */
     public PageInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DAOException;
     
     /**
   * 根据条件分页查询ObjectCpcntRecord对象，作为查询条件的参数
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DAOException dao异常
   */
     public PageInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     
     /**
      * 根据条件分页查询ObjectCpcntRecord对象，作为查询条件的参数
      *
      * @param objectCpcntRecord ObjectCpcntRecord对象 
      * @param start 起始行
      * @param pageSize 页面大小
      * @return  查询结果
      * @throws DAOException dao异常
      */
        public PageInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DAOException;
        
}
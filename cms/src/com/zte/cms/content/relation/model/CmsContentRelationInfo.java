package com.zte.cms.content.relation.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsContentRelationInfo extends DynamicBaseObject
{

    private java.lang.String cgid;
    private java.lang.String contentid;
    private java.lang.String namecn;
    private java.lang.String sql;
    private java.lang.String sqlcount;
    private java.lang.String cpid;
    private java.lang.String operid;

    public java.lang.String getOperid()
    {
        return operid;
    }

    public void setOperid(java.lang.String operid)
    {
        this.operid = operid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getSql()
    {
        return sql;
    }

    public void setSql(java.lang.String sql)
    {
        this.sql = sql;
    }

    public java.lang.String getSqlcount()
    {
        return sqlcount;
    }

    public void setSqlcount(java.lang.String sqlcount)
    {
        this.sqlcount = sqlcount;
    }

    public java.lang.String getCgid()
    {
        return cgid;
    }

    public void setCgid(java.lang.String cgid)
    {
        this.cgid = cgid;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public void initRelation()
    {
        this.addRelation("cgid", "CGID");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("cpid", "CPID");
    }
}

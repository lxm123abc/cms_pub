package com.zte.cms.avtemplate.service;

import java.util.List;
import com.zte.cms.avtemplate.dao.ICmsAvtemplateinfoDAO;
import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.cms.avtemplate.service.ICmsAvtemplateinfoDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsAvtemplateinfoDS extends DynamicObjectBaseDS implements ICmsAvtemplateinfoDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsAvtemplateinfoDAO dao = null;

    public void setDao(ICmsAvtemplateinfoDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        log.debug("insert cmsAvtemplateinfo starting...");
        try
        {
            Long templateIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("template_system_seq");
            cmsAvtemplateinfo.setTemplateindex(templateIndex);
            dao.insertCmsAvtemplateinfo(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsAvtemplateinfo end");
    }

    public void updateCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        log.debug("update cmsAvtemplateinfo by pk starting...");
        try
        {
            dao.updateCmsAvtemplateinfo(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsAvtemplateinfo by pk end");
    }

    public void updateCmsAvtemplateinfoList(List<CmsAvtemplateinfo> cmsAvtemplateinfoList)
            throws DomainServiceException
    {
        log.debug("update cmsAvtemplateinfoList by pk starting...");
        if (null == cmsAvtemplateinfoList || cmsAvtemplateinfoList.size() == 0)
        {
            log.debug("there is no datas in cmsAvtemplateinfoList");
            return;
        }
        try
        {
            for (CmsAvtemplateinfo cmsAvtemplateinfo : cmsAvtemplateinfoList)
            {
                dao.updateCmsAvtemplateinfo(cmsAvtemplateinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsAvtemplateinfoList by pk end");
    }

    public void removeCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        log.debug("remove cmsAvtemplateinfo by pk starting...");
        try
        {
            dao.deleteCmsAvtemplateinfo(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsAvtemplateinfo by pk end");
    }

    public void removeCmsAvtemplateinfoList(List<CmsAvtemplateinfo> cmsAvtemplateinfoList)
            throws DomainServiceException
    {
        log.debug("remove cmsAvtemplateinfoList by pk starting...");
        if (null == cmsAvtemplateinfoList || cmsAvtemplateinfoList.size() == 0)
        {
            log.debug("there is no datas in cmsAvtemplateinfoList");
            return;
        }
        try
        {
            for (CmsAvtemplateinfo cmsAvtemplateinfo : cmsAvtemplateinfoList)
            {
                dao.deleteCmsAvtemplateinfo(cmsAvtemplateinfo);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsAvtemplateinfoList by pk end");
    }

    public CmsAvtemplateinfo getCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        log.debug("get cmsAvtemplateinfo by pk starting...");
        CmsAvtemplateinfo rsObj = null;
        try
        {
            rsObj = dao.getCmsAvtemplateinfo(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsAvtemplateinfoList by pk end");
        return rsObj;
    }

    public List<CmsAvtemplateinfo> getCmsAvtemplateinfoByCond(CmsAvtemplateinfo cmsAvtemplateinfo)
            throws DomainServiceException
    {
        log.debug("get cmsAvtemplateinfo by condition starting...");
        List<CmsAvtemplateinfo> rsList = null;
        try
        {
            rsList = dao.getCmsAvtemplateinfoByCond(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsAvtemplateinfo by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsAvtemplateinfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsAvtemplateinfo, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsAvtemplateinfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsAvtemplateinfo page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsAvtemplateinfo page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsAvtemplateinfo, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsAvtemplateinfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsAvtemplateinfo page info by condition end");
        return tableInfo;
    }

    public List<CmsAvtemplateinfo> getTemplateByName(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException
    {
        log.debug("get targetSystem by condition starting...");
        List<CmsAvtemplateinfo> rsList = null;
        try
        {
            rsList = dao.getTemplateByName(cmsAvtemplateinfo);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get targetSystem by condition end");
        return rsList;
    }
}

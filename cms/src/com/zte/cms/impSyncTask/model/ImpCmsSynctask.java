
  package com.zte.cms.impSyncTask.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ImpCmsSynctask extends DynamicBaseObject
{
	private java.lang.Long taskindex;
	private java.lang.String correlateid;
	private java.lang.String impcode;
	private java.lang.String cspid;
	private java.lang.String lspid;
	private java.lang.Long cpindex;
	private java.lang.String cpurl;
	private java.lang.Integer cptype;
	private java.lang.Integer cpsynctype;
	private java.lang.String fileurl;
	private java.lang.Long rulecode;
	private java.lang.String starttime;
	private java.lang.String endtime;
	private java.lang.Integer status;
	private java.lang.String localreqfileurl;
	private java.lang.String localrspfileurl;
	private java.lang.Long result;
	private java.lang.String description;
    private java.lang.String starttime1;
    private java.lang.String starttime2;
    private java.lang.String cpid;
    private java.lang.Long result_20;
    private java.lang.Long result_30;
    private java.lang.String replyfileurl_20;
    private java.lang.String replyfileurl_30;
    private java.lang.Integer retrytimes;
    private java.lang.Integer wg_reply;
    private java.lang.String endtime1;
    private java.lang.String endtime2;
    
    public java.lang.Long getTaskindex()
    {
        return taskindex;
    } 
         
    public void setTaskindex(java.lang.Long taskindex) 
    {
        this.taskindex = taskindex;
    }
    
    public java.lang.String getCorrelateid()
    {
        return correlateid;
    } 
         
    public void setCorrelateid(java.lang.String correlateid) 
    {
        this.correlateid = correlateid;
    }
    
    public java.lang.String getImpcode()
    {
        return impcode;
    } 
         
    public void setImpcode(java.lang.String impcode) 
    {
        this.impcode = impcode;
    }
    
    public java.lang.String getCspid()
    {
        return cspid;
    } 
         
    public void setCspid(java.lang.String cspid) 
    {
        this.cspid = cspid;
    }
    
    public java.lang.String getLspid()
    {
        return lspid;
    } 
         
    public void setLspid(java.lang.String lspid) 
    {
        this.lspid = lspid;
    }
    
    public java.lang.Long getCpindex()
    {
        return cpindex;
    } 
         
    public void setCpindex(java.lang.Long cpindex) 
    {
        this.cpindex = cpindex;
    }
    
    public java.lang.String getCpurl()
    {
        return cpurl;
    } 
         
    public void setCpurl(java.lang.String cpurl) 
    {
        this.cpurl = cpurl;
    }
    
    public java.lang.Integer getCptype()
    {
        return cptype;
    } 
         
    public void setCptype(java.lang.Integer cptype) 
    {
        this.cptype = cptype;
    }
    
    public java.lang.Integer getCpsynctype()
    {
        return cpsynctype;
    } 
         
    public void setCpsynctype(java.lang.Integer cpsynctype) 
    {
        this.cpsynctype = cpsynctype;
    }
    
    public java.lang.String getFileurl()
    {
        return fileurl;
    } 
         
    public void setFileurl(java.lang.String fileurl) 
    {
        this.fileurl = fileurl;
    }
    
    public java.lang.Long getRulecode()
    {
        return rulecode;
    } 
         
    public void setRulecode(java.lang.Long rulecode) 
    {
        this.rulecode = rulecode;
    }
    
    public java.lang.String getStarttime()
    {
        return starttime;
    } 
         
    public void setStarttime(java.lang.String starttime) 
    {
        this.starttime = starttime;
    }
    
    public java.lang.String getEndtime()
    {
        return endtime;
    } 
         
    public void setEndtime(java.lang.String endtime) 
    {
        this.endtime = endtime;
    }
    
    public java.lang.Integer getStatus()
    {
        return status;
    } 
         
    public void setStatus(java.lang.Integer status) 
    {
        this.status = status;
    }
    
    public java.lang.String getLocalreqfileurl()
    {
        return localreqfileurl;
    } 
         
    public void setLocalreqfileurl(java.lang.String localreqfileurl) 
    {
        this.localreqfileurl = localreqfileurl;
    }
    
    public java.lang.String getLocalrspfileurl()
    {
        return localrspfileurl;
    } 
         
    public void setLocalrspfileurl(java.lang.String localrspfileurl) 
    {
        this.localrspfileurl = localrspfileurl;
    }
    
    public java.lang.Long getResult()
    {
        return result;
    } 
         
    public void setResult(java.lang.Long result) 
    {
        this.result = result;
    }
    
    public java.lang.String getDescription()
    {
        return description;
    } 
         
    public void setDescription(java.lang.String description) 
    {
        this.description = description;
    }
    
    public java.lang.String getStarttime1()
    {
        return starttime1;
    }

    public void setStarttime1(java.lang.String starttime1)
    {
        this.starttime1 = starttime1;
    }

    public java.lang.String getStarttime2()
    {
        return starttime2;
    }

    public void setStarttime2(java.lang.String starttime2)
    {
        this.starttime2 = starttime2;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Long getResult_20()
    {
        return result_20;
    }

    public void setResult_20(java.lang.Long result_20)
    {
        this.result_20 = result_20;
    }

    public java.lang.Long getResult_30()
    {
        return result_30;
    }

    public void setResult_30(java.lang.Long result_30)
    {
        this.result_30 = result_30;
    }

    public java.lang.String getReplyfileurl_20()
    {
        return replyfileurl_20;
    }

    public void setReplyfileurl_20(java.lang.String replyfileurl_20)
    {
        this.replyfileurl_20 = replyfileurl_20;
    }

    public java.lang.String getReplyfileurl_30()
    {
        return replyfileurl_30;
    }

    public void setReplyfileurl_30(java.lang.String replyfileurl_30)
    {
        this.replyfileurl_30 = replyfileurl_30;
    }

    public java.lang.Integer getRetrytimes()
    {
        return retrytimes;
    }

    public void setRetrytimes(java.lang.Integer retrytimes)
    {
        this.retrytimes = retrytimes;
    }

    public java.lang.Integer getWg_reply()
    {
        return wg_reply;
    }

    public void setWg_reply(java.lang.Integer wg_reply)
    {
        this.wg_reply = wg_reply;
    }
    public java.lang.String getEndtime1()
    {
        return endtime1;
    }

    public void setEndtime1(java.lang.String endtime1)
    {
        this.endtime1 = endtime1;
    }
    public java.lang.String getEndtime2()
    {
        return endtime2;
    }

    public void setEndtime2(java.lang.String endtime2)
    {
        this.endtime2 = endtime2;
    }

    public void initRelation()
    {
        this.addRelation("taskindex","TASKINDEX");   
        this.addRelation("correlateid","CORRELATEID");   
        this.addRelation("impcode","IMPCODE");   
        this.addRelation("cspid","CSPID");   
        this.addRelation("lspid","LSPID");   
        this.addRelation("cpindex","CPINDEX");   
        this.addRelation("cpurl","CPURL");   
        this.addRelation("cptype","CPTYPE");   
        this.addRelation("cpsynctype","CPSYNCTYPE");   
        this.addRelation("fileurl","FILEURL");   
        this.addRelation("rulecode","RULECODE");   
        this.addRelation("starttime","STARTTIME");   
        this.addRelation("endtime","ENDTIME");   
        this.addRelation("status","STATUS");   
        this.addRelation("localreqfileurl","LOCALREQFILEURL");   
        this.addRelation("localrspfileurl","LOCALRSPFILEURL");   
        this.addRelation("result","RESULT");   
        this.addRelation("description","DESCRIPTION"); 
        this.addRelation("starttime1", "STARTTIME1");
        this.addRelation("starttime2", "STARTTIME2");
        this.addRelation("endtime1", "ENDTIME1");
        this.addRelation("endtime2", "ENDTIME2");

    } 
}

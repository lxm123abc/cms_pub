package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.dao.IServiceChannelMapDAO;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceChannelMapDS extends DynamicObjectBaseDS implements IServiceChannelMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceChannelMapDAO dao = null;

    public void setDao(IServiceChannelMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException
    {
        log.debug("insert serviceChannelMap starting...");
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_service_channel_map_index");
        serviceChannelMap.setMapindex(index);
        String id = index.toString();
        for (int i = id.length();i<30;i++){
            id = "0"+id;
        }
        id = "23"+id;
        serviceChannelMap.setMappingid(id);
        try
        {
            dao.insertServiceChannelMap(serviceChannelMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceChannelMap end");
    }

    public void updateServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException
    {
        log.debug("update serviceChannelMap by pk starting...");
        try
        {
            dao.updateServiceChannelMap(serviceChannelMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceChannelMap by pk end");
    }

    public void updateServiceChannelMapList(List<ServiceChannelMap> serviceChannelMapList)
            throws DomainServiceException
    {
        log.debug("update serviceChannelMapList by pk starting...");
        if (null == serviceChannelMapList || serviceChannelMapList.size() == 0)
        {
            log.debug("there is no datas in serviceChannelMapList");
            return;
        }
        try
        {
            for (ServiceChannelMap serviceChannelMap : serviceChannelMapList)
            {
                dao.updateServiceChannelMap(serviceChannelMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceChannelMapList by pk end");
    }

    public void removeServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException
    {
        log.debug("remove serviceChannelMap by pk starting...");
        try
        {
            dao.deleteServiceChannelMap(serviceChannelMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceChannelMap by pk end");
    }

    public void removeServiceChannelMapList(List<ServiceChannelMap> serviceChannelMapList)
            throws DomainServiceException
    {
        log.debug("remove serviceChannelMapList by pk starting...");
        if (null == serviceChannelMapList || serviceChannelMapList.size() == 0)
        {
            log.debug("there is no datas in serviceChannelMapList");
            return;
        }
        try
        {
            for (ServiceChannelMap serviceChannelMap : serviceChannelMapList)
            {
                dao.deleteServiceChannelMap(serviceChannelMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceChannelMapList by pk end");
    }

    public ServiceChannelMap getServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException
    {
        log.debug("get serviceChannelMap by pk starting...");
        ServiceChannelMap rsObj = null;
        try
        {
            rsObj = dao.getServiceChannelMap(serviceChannelMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceChannelMapList by pk end");
        return rsObj;
    }

    public List<ServiceChannelMap> getServiceChannelMapByCond(ServiceChannelMap serviceChannelMap)
            throws DomainServiceException
    {
        log.debug("get serviceChannelMap by condition starting...");
        List<ServiceChannelMap> rsList = null;
        try
        {
            rsList = dao.getServiceChannelMapByCond(serviceChannelMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceChannelMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceChannelMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceChannelMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceChannelMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceChannelMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceChannelMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceChannelMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceChannelMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceChannelMap page info by condition end");
        return tableInfo;
    }
}

 
  
  function strTrim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
}

function get19Time(time14)
{
	if(time14 != undefined && time14 != null && strTrim(time14) != "")
    {
     	   var  tmpTime = strTrim(time14);
     	   if(tmpTime.length == 14)
     	   {
     	   	   var year = tmpTime.substring(0,4);
     	   	   var month = tmpTime.substring(4,6);
     	   	   var day = tmpTime.substring(6,8);
     	   	   var hour = tmpTime.substring(8,10);
     	   	   var minutes = tmpTime.substring(10,12);
     	   	   var seconds = tmpTime.substring(12,14);
     	   	   return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
     	   }
     	   else
     	   {
     	   	 	return "";
     	   }
   	}
   	else
   	{
   		  return "";
   	}
}
  //验证是否为URL格式
function isURLFormat(input,input_msg){
  if(trim(input.value) != '' && input.value != null){
    var strRegex = /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/; 
    var exp=new RegExp(strRegex);
    var inputvalue = input.value.toLowerCase();
    var reg = inputvalue.match(exp);
    if(reg == null){
        document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.11"); //"URL格式不正确";
        document.getElementById(input.id).focus();
        return false;      
    }else{
        document.getElementById(input_msg).innerText = "";   
        return true;   
    }      
  }
}


  //设置label的提示信息
  function setLabelValue(labelId,labelmsg)
  {
  	 document.getElementById(labelId).innerText=labelmsg;
  }
 function dealErrResult(errorCode,rtn_expDesc)
  {				
				openMsg($res_entry("msg.sys.error"));
  }

  
     function BatchResult(result)
{
	
	var desc = getDesc(result);
	var succeed = desc.succeed;
	var fail    = desc.fail;

	//var descstr = "成功数量："+ succeed + "，失败数量："+fail 
	//	+"\n"+ desc.des;
	
	var wheight= 200 + (parseInt(succeed)+ parseInt(fail))* 14;
	var wwidth = 400;
	if(fail>0)
		wwidth = 550;
	
	if(wheight > 400)
		wheight = 400;

	openMsgBoxSize($res_entry("msg.iptv.channelManage.note.6")+ succeed+"\t," +$res_entry("msg.iptv.channelManage.note.7")+fail+"\n"+ desc.des,wwidth,wheight);
		_para.isLoad="true";
	init();
}
   	
  function getDesc(descstr)
 {
    var reg = /(\w+):(\w+):?(.*)/;
    var rtn = descstr.match(reg);
    var desc = {};
    desc.succeed = rtn[1];
    desc.fail    = rtn[2];
    
    var l = desc.succeed.length + desc.fail.length + 2;
    
    desc.des     = descstr.substring(l,descstr.length);
    return desc;
 }

 //校验异常字符
   function checkstring(checkstr,userinput)
    {

    var allValid = false;

    for (i = 0;i<userinput.length;i++) {

        ch = userinput.charAt(i);

        if(checkstr.indexOf(ch) >= 0) {

            allValid = true;

            break;
          }
      }
     return allValid;
   }
   
 function isNumb(ex){
    if (ex.match(/^(\d)*$/g) == null)
	{
        return false;
    }else{
        return true;
    }
}

//判断是不是超过10位的数字
 function isNumbbwt10(ex)
 {
 if (ex.match(/^(\d){0,10}$/g) == null)
    {
        return false;
    }
    else
    {
        return true;
    }
 }
//转换时间格式
 function getTime(str)
	{
		if (str.length > 6)
		{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
		}else{
			return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
		}
	}
	//
 function formatStarttime()
 {
 var time = getRiaCtrlValue('starttime')
	 return getTime(time);
 }

 function formatEndtime()
 {
 var time = getRiaCtrlValue('endtime')
	 return getTime(time);
 }

 // 校验开始时间和结束时间
	function checkStartEndTime(){
		if (document.getElementById("starttime") && document.getElementById("endtime"))
		{
			var starttime = document.getElementById("starttime").value;
			var endtime = document.getElementById("endtime").value;
			var t1 = starttime.replace(/\D/g,"");
			var t2 = endtime.replace(/\D/g,"");
			if (starttime.length > 0){
				var cur = curDateTime().replace(/\D/g,"");
				if (cur >= t1)
				{
					showErrorText("starttime",$res_entry("note.iptv.channelManage.13"));
					return false;
				}
			}
			if (endtime.length > 0){
				var cur = curDateTime().replace(/\D/g,"");
				if (cur >= t2)
				{
					showErrorText("endtime",$res_entry("note.iptv.channelManage.14"));
					return false;
				}
			}
			if (starttime.length > 0 && endtime.length > 0)
			{
				if (t1 >= t2)
				{
					showErrorText("starttime",$res_entry("note.iptv.channelManage.15"));
					return false;
				}
			}
			
		}
		return true;
	}

   //校验开始时间必须要小于结束时间
 function checkEndLargeThanStartTime()
 {
 if (document.getElementById("starttime") && document.getElementById("endtime"))
		{
			var starttime = document.getElementById("starttime").value;
			var endtime = document.getElementById("endtime").value;
			var t1 = starttime.replace(/\D/g,"");
			var t2 = endtime.replace(/\D/g,"");
			if (starttime.length > 0 && endtime.length > 0)
			{
				if (t1 >= t2)
				{
					showErrorText("starttime",$res_entry("note.iptv.channelManage.15"));
					return false;
				}
			}
			
		}
		return true;
 }

function showErrorText(id, text){
		$("label[class='validFont']").each(hidevalidFont);
		var obj = document.getElementById(id + "_msg");
		if (obj)
		{
			if (text != "")
			{
				obj.innerText = text;
			}else{
				obj.innerText = $res_entry("note.iptv.channelManage.16");
			}
		}
	}

	function hidevalidFont(i){
		if (i > 0)
		{
			this.innerText = "";
		}
	}

	function curDateTime(){
    var curDate = new Date();
    var DTAsString = "";
    var y = 0;
    var m = 0;
    var d = 0;
    var h = 0;
    var mi = 0;
    var s = 0;

    y = curDate.getYear();
    m = curDate.getMonth() + 1;
    d = curDate.getDate();

    if(y < 100){
        DTAsString += (1900+y);
    }else{
        DTAsString = y;
    }
    if(m < 10){
        DTAsString += "-0" + m;
    }else{
        DTAsString += "-" + m;
    }
    if(d < 10){
        DTAsString += "-0" +d;
    }else{
        DTAsString += "-" + d;
    }

    DTAsString += " ";
    h = curDate.getHours();
    mi = curDate.getMinutes();
    s = curDate.getSeconds();
    if (h < 10) {
        DTAsString += "0" + h;
    }else{
        DTAsString += h;
    }
    if (mi < 10) {
        DTAsString += ":0" + mi;
    }else{
        DTAsString += ":" + mi;
    }
    if (s < 10) {
        DTAsString += ":0" + s;
    }else{
        DTAsString += ":" + s;
    }
    return DTAsString;
}

 //校验是否为空      
function checkNull(inputStr)
 {

       return ( Trim(inputStr).length==0);
   }
   	  // 去除先后空格
   function strTrim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
}
//取得页面上去掉空格以后的输入框的值
function getHtmlCtrlTrimValue(id)
{
	return strTrim(document.getElementById(id).value);
}

//校验频道码率number(8,2)
 function validRate(ex)
 {
//校验正浮点数的
var reg1=/^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;　
//校验码率的长度的
var reg2=/^((\d{1,6}\.\d{0,2})||(\d{1,6}))$/;
var flag1 = reg1.test(ex);
var flag2 = reg2.test(ex);
var flag = (flag1&&flag2);
   return flag;
}
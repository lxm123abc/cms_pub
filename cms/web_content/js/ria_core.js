// @version 3.00.04

if (!this.JSON) {

// Create a JSON object only if one does not already exist. We create the
// object in a closure to avoid creating global variables.

    JSON = function () {

        function f(n) {
            // Format integers to have at least two digits.
            return n < 10 ? '0' + n : n;
        }

        Date.prototype.toJSON = function (key) {

            return this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z';
        };

        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapeable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap,
            indent,
            meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            },
            rep;


        function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

            escapeable.lastIndex = 0;
            return escapeable.test(string) ?
                '"' + string.replace(escapeable, function (a) {
                    var c = meta[a];
                    if (typeof c === 'string') {
                        return c;
                    }
                    return '\\u' + ('0000' +
                            (+(a.charCodeAt(0))).toString(16)).slice(-4);
                }) + '"' :
                '"' + string + '"';
        }


        function str(key, holder) {

// Produce a string from holder[key].

            var i,          // The loop counter.
                k,          // The member key.
                v,          // The member value.
                length,
                mind = gap,
                partial,
                value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

            if (value && typeof value === 'object' &&
                    typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

            if (typeof rep === 'function') {
                value = rep.call(holder, key, value);
            }

// What happens next depends on the value's type.

            switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

                return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

            case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

                if (!value) {
                    return 'null';
                }

// Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

// If the object has a dontEnum length property, we'll treat it as an array.

                if (typeof value.length === 'number' &&
                        !(value.propertyIsEnumerable('length'))) {

// The object is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                    v = partial.length === 0 ? '[]' :
                        gap ? '[\n' + gap +
                                partial.join(',\n' + gap) + '\n' +
                                    mind + ']' :
                              '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

// If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        k = rep[i];
                        if (typeof k === 'string') {
                            v = str(k, value, rep);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

// Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = str(k, value, rep);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

                v = partial.length === 0 ? '{}' :
                    gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                            mind + '}' : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
            }
        }

// Return the JSON object containing the stringify and parse methods.

        return {
            stringify: function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

                var i;
                gap = '';
                indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

                if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) {
                        indent += ' ';
                    }

// If the space parameter is a string, it will be used as the indent string.

                } else if (typeof space === 'string') {
                    indent = space;
                }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

                rep = replacer;
                if (replacer && typeof replacer !== 'function' &&
                        (typeof replacer !== 'object' ||
                         typeof replacer.length !== 'number')) {
                    throw new Error('JSON.stringify');
                }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

                return str('', {'': value});
            },


            parse: function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

                var j;

                function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                    var k, v, value = holder[key];
                    if (value && typeof value === 'object') {
                        for (k in value) {
                            if (Object.hasOwnProperty.call(value, k)) {
                                v = walk(value, k);
                                if (v !== undefined) {
                                    value[k] = v;
                                } else {
                                    delete value[k];
                                }
                            }
                        }
                    }
                    return reviver.call(holder, key, value);
                }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

                cx.lastIndex = 0;
                if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' + ('0000' +
                                (+(a.charCodeAt(0))).toString(16)).slice(-4);
                    });
                }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

                if (/^[\],:{}\s]*$/.
test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                    j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                    return typeof reviver === 'function' ?
                        walk({'': j}, '') : j;
                }

// If the text is not JSON parseable, then a SyntaxError is thrown.

                throw new SyntaxError('JSON.parse');
            }
        };
    }();
}
//\u6570\u636e\u7ed1\u5b9a\u5230\u63a7\u4ef6
function BindDataToCtrl(id, eleVal,ctrlext,datatype) {
    //\u901a\u8fc7id\u83b7\u53d6\u63a7\u4ef6\u5bf9\u8c61\u6570\u7ec4\u3002
    var obj = $("#" + id) ;

    //\u53ea\u80fd\u9488\u5bf9\u5176\u4e2d\u4e00\u4e2a\uff08id\u552f\u4e00\uff09
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d\u79f0
        var tagNam = getTageName(obj[0]);
            //input
        if ("input" == tagNam) {
            //\u6587\u672c\u6846
            
            if ("text" == obj[0].type) {
                if(datatype&&datatype=="NullString"){
                   obj[0].value = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].value = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }
            } else
                //\u5355\u9009\u6309\u94ae
            if ("radio" == obj[0].type) {
                BindDataToRadioCtrl(obj, eleVal);
            } else 
                //\u590d\u9009\u6309\u94ae
            if ("checkbox" == obj[0].type) {
                BindDataToRadioMultCtrl(obj, eleVal);
            } else 
            if ("hidden" == obj[0].type) {
                obj[0].value = eleVal == null ? "" : eleVal;
            } else
            {
                
                obj[0].value = eleVal;
            }
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            BindDataToSelectCtrl(obj, eleVal,ctrlext);
        }
        //\u6811\u578b\u6309\u94ae
        else if ("tree" == tagNam) {
            var tree = new Tree_Factory(eleVal, id);
            tree.initTree();
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
           SSB_RadioGroup.radioGroupObjs[id].setValue(eleVal);
        }
        //\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
        else if ("slider" == tagNam) {
           SliderCtrObj.setValueObjFromSlider(id,eleVal);
        }
        //\u81ea\u5b9a\u4e49colorbar\u63a7\u4ef6
        else if ("colorbar" == tagNam) {
           ColorBarCtrObj.setValueObjFromColorBar(id,eleVal);
        }
        //\u81ea\u5b9a\u4e49inputtext\u63a7\u4ef6
        else if ("inputtext" == tagNam) {
           ChangeableTextCtrObj.setValueObjFromChangeableText(id,eleVal);
        }
        //\u81ea\u5b9a\u4e49extextarea\u63a7\u4ef6
        else if ("extextarea" == tagNam) {
           SSB_EXTEXTAREA.setTextAreaValue(id,eleVal);
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            setCalendarValue(id, eleVal);
        }
        //table
        else if ("table" == tagNam) {
            setTableValue(id, eleVal);
        }
        //Label
        else if ("label" == tagNam) {
			obj[0] = document.getElementById(id); 
            if (!isIE()) {
                  if(datatype&&datatype=="NullString"){
                  obj[0].textContent = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].textContent = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }  
            } else {
                //obj[0].innerText = eleVal;
                if(datatype&&datatype=="NullString"){
                  obj[0].innerText = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].innerText = (eleVal == null||eleVal == "null") ? "" : eleVal;
                } 
            }
        }
        //ip
        else if("ip" == tagNam) {
            SSB_IP.ipObjs[id].setValue(eleVal);
        }
        //textArea
        else if("textarea" == tagNam) {
            if(datatype&&datatype=="NullString"){
                   obj[0].value = eleVal == null ? "null" : eleVal;
                }else{
                   obj[0].value = (eleVal == null||eleVal == "null") ? "" : eleVal;
                }
        }
        // add by 
        else if ("barchart" == tagNam) {
            setbarsValue(id,eleVal);
        }
         else if ("lineschart" == tagNam) {
            setlinesValue(id,eleVal);
        }
         else if ("piechart" == tagNam) {
            setpiesValue(id,eleVal);
        }
        //\u9ed8\u8ba4\u60c5\u51b5
        else
        {
            obj[0].value = eleVal;
        }
    }
};

//\u4ece\u63a7\u4ef6\u4e0a\u53d6\u503c
function GetDataFromCtrl(id) {
    //\u63a7\u4ef6\uff08\u6570\u7ec4\uff09
    var obj = $("#" + id) ;
    //id\u552f\u4e00\uff0c\u6240\u4ee5obj\u552f\u4e00
    if (obj.length == 1) {
        //\u6807\u7b7e\u540d
        var tagNam = getTageName(obj[0]);
            //\u6587\u672c\u6846
        if ("input" == tagNam) {
            if ("text" == obj[0].type) {
                return obj[0].value;
            } else if ("radio" == obj[0].type) {
                return GetDataFromRadioCtrl(obj);
            } else if ("checkbox" == obj[0].type) {
                return GetDataFromRadioMultCtrl(obj);
            } else if ("hidden" == obj[0].type) {
                return obj[0].value;
            } else if("file" == obj[0].type){
                __file_List.push(document.getElementById(id));
                return obj[0].value;
            }else{              
                return obj[0].value;
            } 
        }
        //\u4e0b\u62c9
        else if ("select" == tagNam) {
            return GetDataFromSelectCtrl(obj[0]);
        }
        //\u81ea\u5b9a\u4e49\u6811
        else if ("tree" == tagNam) {
            var tree = SSB_Tree.trees[obj.attr("id")];
            var ret = tree.getValue();
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u5355\u9009\u6309\u94ae\u7ec4
        else if ("radiogroup" == tagNam) {
            var ret = SSB_RadioGroup.radioGroupObjs[obj.attr("id")].getValue();
            return ret;
        }
        //\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
        else if ("slider" == tagNam) {
            var ret = SliderCtrObj.getValueObjFromSlider(obj.attr("id"));               
            return ret;
        }
        //colorbar
        else if ("colorbar" == tagNam) {

           var ret = ColorBarCtrObj.getValueObjFromColorBar(obj.attr("id"));
           return ret;
        }
        //inputtext
        else if ("inputtext" == tagNam) {
           var ret = ChangeableTextCtrObj.getValueObjFromChangeableText(obj.attr("id"));
           return ret;
        }
        //\u81ea\u5b9a\u4e49extextarear\u63a7\u4ef6
        else if ("extextarea" == tagNam) {
            var ret = SSB_EXTEXTAREA.getTextAreaValue(obj.attr("id"));              
            return ret;
        }
        //\u65e5\u5386
        else if ("calendar" == tagNam) {
            var ret = getCalendarValue(id);
            return ret;
        }
        //\u81ea\u5b9a\u4e49\u8868\u683c
        else if ("table" == tagNam) {
			var ret = getTable_objTrs(id);
            return ret;
        }
        //Label
        else if ("label" == tagNam) {
            if (!isIE()) {
                return obj[0].textContent;
            } else {
                return obj[0].innerText;
            }
        }else if("upload"==tagNam){
            return GetDataFromUploadCtrl(obj[0]);
        }
//ip
        else if("ip" == tagNam) {
            var ret = SSB_IP.ipObjs[obj.attr("id")].getValue();
            return ret; 
        }else{
            //\u9ed8\u8ba4
            return obj[0].value;
        }
    }
};


//\u53d6\u6807\u7b7e\u540d\u79f0\uff0c\u907f\u514did\u548cfirefox\u6709\u4e0d\u540c\u7684\u6807\u7b7e\u540d\u79f0
function getTageName(obj) {
    //\u6587\u672c\u6846
    if ("input" == (obj.tagName).toLowerCase()) {
        return "input";
    }
    //
    if ("textarea" == (obj.tagName).toLowerCase()) {
        return "textarea";
    }
    //\u4e0b\u62c9
    if ("select" == (obj.tagName).toLowerCase()) {
        return "select";
    }
    //\u81ea\u5b9a\u4e49\u6309\u94ae\u7ec4
    if ("radiogroup" == (obj.tagName).toLowerCase() || "z:radiogroup" == (obj.tagName).toLowerCase()) {
        return "radiogroup";
    }
    //\u81ea\u5b9a\u4e49Slider\u63a7\u4ef6
    if ("slider" == (obj.tagName).toLowerCase() || "z:slider" == (obj.tagName).toLowerCase()) {
        return "slider";
    }
    //\u81ea\u5b9a\u4e49colorbar\u63a7\u4ef6
    if ("colorbar" == (obj.tagName).toLowerCase() || "z:colorbar" == (obj.tagName).toLowerCase()) {
        return "colorbar";
    }
    //\u81ea\u5b9a\u4e49inputtext\u63a7\u4ef6
    if ("inputtext" == (obj.tagName).toLowerCase() || "z:inputtext" == (obj.tagName).toLowerCase()) {
        return "inputtext";
    }   
    //\u81ea\u5b9a\u4e49extextarea\u63a7\u4ef6
    if ("extextarea" == (obj.tagName).toLowerCase() || "z:extextarea" == (obj.tagName).toLowerCase()) {
        return "extextarea";
    }
    //\u81ea\u5b9a\u4e49\u65e5\u5386\u63a7\u4ef6
    if ("calendar" == (obj.tagName).toLowerCase() || "z:calendar" == (obj.tagName).toLowerCase()) {
        return "calendar";
    }
    //\u81ea\u5b9a\u4e49\u8868\u683c\u63a7\u4ef6
    if ("table" == (obj.tagName).toLowerCase() || "z:table" == (obj.tagName).toLowerCase()) {
        return "table";
    }
    //\u81ea\u5b9a\u4e49\u6811\u578b\u63a7\u4ef6
    if ("tree" == (obj.tagName).toLowerCase() || "z:tree" == (obj.tagName).toLowerCase()) {
        return "tree";
    }
    //label\u6807\u7b7e
    if ("label" == (obj.tagName).toLowerCase()) {
        return "label";
    }
    if("upload"==(obj.tagName).toLowerCase()||"z:upload"==(obj.tagName).toLowerCase()){
        return "upload";
    }
     //ip\u6807\u7b7e
    if("ip" == (obj.tagName).toLowerCase() || "z:ip" == (obj.tagName).toLowerCase()) {
        return "ip";
    }

    if ("barchart" == (obj.tagName).toLowerCase()) {
        return "barchart";
    }
    if ("lineschart" == (obj.tagName).toLowerCase()) {
        return "lineschart";
    }
    if ("piechart" == (obj.tagName).toLowerCase()) {
        return "piechart";
    }
    return "other";
};


//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioMultCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
    //\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
    //\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
   for (var i = 0; i < radioObjs.length && elementvalue !=null ; i++) {
       //\u5982\u679c\u662f\u5355\u4e2a\u503c\uff0c\u800c\u4e0d\u662f\u6570\u7ec4
       var tmpArr = new Array();
       if( elementvalue instanceof Array )
           tmpArr = elementvalue ;
       else 
           tmpArr[0] = elementvalue;
        for (var j = 0; tmpArr != null && j < tmpArr.length; j++) {
            var radioObj = radioObjs[i] ;
            var varV = tmpArr[j];
            //\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9                
            if (varV !=null && varV.toString() == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
        }
    }
};

//\u7ed1\u5b9a\u6570\u636e\u5230\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u6846
function BindDataToRadioCtrl(obj, elementvalue) {
    //\u9488\u5bf9\u5bf9\u8c61\u540d\u79f0\u5904\u7406
    var radioName = obj.attr("name");
    //\u5bf9\u8c61\u6570\u7ec4
    var radioObjs = document.getElementsByName(radioName);
    //\u5355\u4e2a\u9009\u4e2d\u6240\u6709\u7684\u4f20\u5165\u7684\u503c
    for (var i = 0; i < radioObjs.length; i++) {
       
            var radioObj = radioObjs[i] ;
            var varV = elementvalue;
            //\u5224\u65ad\u662f\u5426\u88ab\u9009\u62e9                    
            if (varV == radioObj.getAttribute("value")) {
                radioObj.checked = true;
            } else {
                radioObj.checked = false;
            }
       
    }
};
        
//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioMultCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn ;
    var j = 0 ;
    //\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    if ( radioObjs.length == 1 ) {
        rtn = "";
        var radioObj = radioObjs[0];
        if (radioObj.checked) {
            rtn = radioObj.getAttribute("value");
        }
    } else {
        rtn = new Array()   
        for (var i = 0; i < radioObjs.length; i++) {
            var radioObj = radioObjs[i] ;
            if (radioObj.checked) {
                rtn[j++] = radioObj.getAttribute("value");
            }
        }
    }
    return rtn;
};

//\u4ece\u5355\u9009\u6216\u590d\u9009\u6309\u94ae\u7684\u88ab\u9009\u4e2d\u7684\u503c\u3002
function GetDataFromRadioCtrl(obj) {
    //\u5355\u9009\u6309\u94ae\u548c\u590d\u9009\u6309\u94ae\u4ee5\u540d\u79f0\u5224\u65ad\u5176\u4e2a\u6570
    var radioName = obj.attr("name");
    var radioObjs = document.getElementsByName(radioName);
    var rtn = "";
    var j = 0 ;
    //\u5224\u65ad\u54ea\u4e9b\u5df2\u88ab\u9009\u4e2d
    for (var i = 0; i < radioObjs.length; i++) {
        var radioObj = radioObjs[i] ;
        if (radioObj.checked) {
            return radioObj.getAttribute("value");
        }
    }
    return rtn;
};

//\u7ed1\u5b9a\u503c\u5230\u4e0b\u62c9\u6846
function BindDataToSelectCtrl(obj, elementvalue,ctrlext ) {

    //\u6709\u91cd\u590d\u64cd\u4f5c\uff0c\u4f18\u5316\uff01
    var selectObj = document.getElementById(obj.attr("id"));
    var refObj = elementvalue ;

    if (undefined == ctrlext )
    {
        for (var i = 0; i < selectObj.length; i++) {
            //\u5224\u65ad\u88ab\u9009\u4e2d\u7684\u4e0b\u62c9\u5143\u7d20
            var optA = selectObj.options[i] ;
            if (!selectObj.getAttribute('multiple') && elementvalue == optA.value) {
                optA.selected = true;
            }
            else if(selectObj.getAttribute('multiple'))
            {
                for(var m = 0;m < elementvalue.length; m++)
                {
                    if(elementvalue[m] == optA.value)
                    {
                        optA.selected = true;
                    }
                }
            }
        }
    } else {
        var textName = ctrlext.text;
        var valueName = ctrlext.value;
        //\u5982\u679c\u6709\u503c
        if ( refObj )
        {
            //\u5982\u679c\u662f\u5bf9\u8c61
            if ( refObj instanceof(Object))
            { 
                 
                var textArr = refObj[textName] ;
                var valueArr = refObj[valueName];
                var defaultValue = refObj[ctrlext.defaultValue];
                        
                var k = 0;
                if ( selectObj )
                {
                    k = selectObj.length;
                }
                for (var j = 0; j < k; j++) {
                    selectObj.remove(0);
                }
                //\u5faa\u73af\u5224\u65ad
                for (var i = 0; i < textArr.length  && textArr.length  == valueArr.length ; i++) {
                    var optA = document.createElement("OPTION");
                    optA.text = textArr[i];
                    optA.value = valueArr[i];
                    selectObj.options.add(optA);
                    //\u5224\u65ad\u88ab\u9009\u62e9\u7684\u4e0b\u62c9\u5143\u7d20
                    if (!selectObj.getAttribute('multiple') && defaultValue == optA.value) {                                
                        optA.selected = true;                
                    }
                    else if(selectObj.getAttribute('multiple') && defaultValue && defaultValue.length && defaultValue.length > 0)
                    {
                        for(var n = 0;n < defaultValue.length; n++)
                        {
                            if(defaultValue[n] == optA.value)
                            {
                                optA.selected = true;
                            }
                        }               
                    }                  
                }
            }
            
        }
        
    }
};


//\u83b7\u53d6\u4e0b\u62c9\u7684\u9009\u4e2d\u503c
function GetDataFromSelectCtrl(obj) {
    var ret = new Array();
    var j = 0 ;
    if (!obj.getAttribute('multiple')) {
        return obj.value;
    }
    //\u4ece\u6240\u6709\u7684\u4e0b\u62c9\u5143\u7d20\u4e2d\u5224\u65ad\u88ab\u9009\u62e9\u7684\u5143\u7d20\u503c
    for (var i = 0; i < obj.length; i++) {
        var optA = obj.options[i] ;
        if (optA.selected) {
            ret[j++] = optA.value;
        }
    }
    return ret;
};

//\u5224\u65adie\u7c7b\u578b
function isIE() {
    return window.navigator.userAgent.indexOf("MSIE") != -1;
};
var __file_List=[];
//\u83b7\u53d6\u4e0a\u4f20\u6587\u4ef6\u63a7\u4ef6\u7684\u503c,\u53ea\u662f\u6587\u4ef6\u8def\u5f84
function GetDataFromUploadCtrl(obj){
    //wanghao
    __file_List = [];

    var values=[];
    var inputs=obj.getElementsByTagName('INPUT');
    for(var i=0;i<inputs.length;i++){
        if(inputs[i].type=='file')
        {   
                    
            __file_List.push(inputs[i]);                
        }

        values.push(inputs[i].value);
    }
    return values;
}


var CallService = function(sid, outParas, autoPara, obj, fun,mm,cacheurl,urlErrorEvent) {
    this._xmlhttp = new FactoryXMLHttpRequest();
    this.username = null;
    this.password = null;
    this.sid = sid;
    this.outParas = outParas;
    this.autoPara = autoPara;
    this.pageContext = pageContextCache;
    this._object = obj;
    this.onSuccess = fun;
    this.mm = mm;
    this.cacheurl = cacheurl;
    this.urlErrorEvent = urlErrorEvent;
};
var processBarHidden = function(serviceId,instance)
{
   if(instance._xmlhttp.readyState == 4){
     //tianyingquan add exception 2008-5-23
     httpExceptionCode(instance._xmlhttp.status); 
     //end 
     if(instance._xmlhttp.status == 12029||instance._xmlhttp.status == 408)
    {   
        if(serviceId)
        {
            var loadingobj = pageContextCache.getLoading(serviceId);
            var issyn = loadingobj.isSyn;
            var divid = loadingobj.divId;
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                hiddendDivImg(divid);
                return;
            }else if(issyn==true||issyn=="true")
            {
               removeObj();          
               return;
            }         
        }
    } 
     }
};

var CallService_call = function(action, url, data,isSyn) {
    var instance = this;
    var hiddensid = this.sid;
    if(isSyn && (isSyn=='true'||isSyn == true))
    {
        //\u540c\u6b65
        isSyn = false;
    }
    else
    {
        //\u5f02\u6b65
        isSyn = true;
    }
    this._xmlhttp.open(action, url, isSyn);
    this.openCallback(this._xmlhttp);
    this._xmlhttp.onreadystatechange = callbackallbrowser;
    function callbackallbrowser() {
    //\u670d\u52a1\u8d85\u65f6\u8bbe\u7f6e\u56fe\u7247\u9690\u85cf
    processBarHidden(hiddensid,instance);
        switch (instance._xmlhttp.readyState) {
            case 1:
                instance.loading();
                break;
            case 2:
                instance.loaded();
                break;
            case 3:
                instance.interactive();
                break;
            case 4:
                if (instance.complete) {
                    try {
                        instance.complete(instance._xmlhttp.status, instance._xmlhttp.statusText,
                                instance._xmlhttp.responseText, instance._xmlhttp.responseXML);
                        instance._xmlhttp.abort();
                        instance._xmlhttp = null ;        
                    }
                    catch (e) {
                      var tempException = new RIAException("",constantRia.RIA_EXCECOMPLETE,e.description);
                      commonException(constantRia.RIA_RIA,tempException);                   
                    }
                }
                break;
        }
    };
    try {
        this._xmlhttp.send(data);		
		
        //if(this._xmlhttp.onreadystatechange == null)  callbackallbrowser();
		//\u56de\u8c03\u51fd\u6570\u53ea\u6709\u5728FF\u4e2d\u4e14\u4e3a\u540c\u6b65\u65b9\u5f0f\u65f6\u624d\u4e0d\u4f1a\u8c03\u7528\uff0c\u6240\u4ee5\u9996\u5148\u5e94\u5f53\u5224\u65ad\u6d4f\u89c8\u5668\u4ee5\u53ca\u4f20\u9001\u65b9\u5f0f
		if(!isIE()){
			if(!isSyn && this._xmlhttp.status == 200 && this._xmlhttp.readyState == 4) {
				callbackallbrowser();
			}
			//if(!isSyn && this._xmlhttp.onreadystatechange == null) callbackallbrowser();			
		}
            //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
       var serviceRequest = pageContextCache.getRequest(this.sid);
       //\u8bf7\u6c42\u76f8\u5e94\u540e\u4e8b\u4ef6
       if(serviceRequest && serviceRequest.postRequest&&serviceRequest.postRequest.trim())
       {
            var requesteventafter = parseValueById(serviceRequest.postRequest);
            evalEvent(this.mm,requesteventafter);
       }
       
    }
    catch(e) {
           var tempException = new RIAException("",e.number,e.description);
           commonException(constantRia.RIA_RIA,tempException);    
    }
};

var CallService_get = function(url) {
    this.call("GET", url, null);
};

var CallService_put = function(url, mimetype, datalength, data) {
    this.openCallback = function(xmlhttp) {
        xmlhttp.setRequestHeader("Content-type", mimetype);
        xmlhttp.setRequestHeader("Content-Length", datalength);
    };
    this.call("PUT", url, data);
};

var CallService_delete = function (url) {
    this.call("DELETE", url, null);
};

var CallService_post = function(url, mimetype, datalength, data,isSyn) {
    var thisReference = this;
    this.userOpenCallback = this.openCallback;
    this.openCallback = function(xmlhttp) {
        //xmlhttp.setRequestHeader( "Content-type", mimetype);
        //xmlhttp.setRequestHeader( "Content-Length", datalength);
        thisReference.userOpenCallback(xmlhttp);
        thisReference.openCallback = thisReference.userOpenCallback;
    };
    this.call("POST", url, data,isSyn);
};

var CallService_openCallback = function (obj) {
};
var CallService_loading = function () {
};
var CallService_loaded = function () {
};
var CallService_interactive = function () {
};

//\u89e3\u6790\u6807\u7b7e\u5f02\u5e38
var RIA_ERROR_PARSETAG = 100;//\u89e3\u6790\u6807\u7b7e
var RIA_ERROR_PARSETAG_SERVICE = 101;//\u89e3\u6790\u670d\u52a1\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_BIND = 102;//\u89e3\u6790\u90a6\u5b9a\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_INIT = 103;//\u89e3\u6790\u521d\u59cb\u5316\u51fa\u9519
var RIA_ERROR_PARSETAG_REQUEST = 104;//\u89e3\u6790\u8bf7\u6c42\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_RESPONSE = 105;//\u89e3\u6790\u8fd4\u56de\u65f6\u51fa\u9519
var RIA_ERROR_PARSETAG_FORWORD = 106;//\u89e3\u6790\u8df3\u8f6c\u65f6\u51fa\u9519
//\u83b7\u53d6\u6d4f\u89c8\u5668\u51fa\u9519\uff0c\u5224\u65ad\u6d4f\u89c8\u5668\u662f\u5426\u652f\u6301
var RIA_ERROR_SUPPORT_BROWSER = 110;
//\u5c01\u88c5\u9875\u9762\u53c2\u6570\u5931\u8d25
var RIA_ERROR_ENCAPSULATION_URL = 111;
//HTTP\u54cd\u5e94\u5f02\u5e38
var RIA_ERROR_HTTPRES = 200;
var RIA_ERROR_HTTPRES_STATS = 201;
//\u4e1a\u52a1\u5904\u7406\u5f02\u5e38
var RIA_ERROR_OPERATION = 300;
//\u8c03\u7528\u670d\u52a1\u65f6callSid\u662f\u5fc5\u987b\u6307\u5b9a\u4e00\u4e2a\u7533\u660e\u7684\u670d\u52a1
var RIA_ERROR_CALLSID = 301;
var RIA_ERROR_GETCTRL = 302;
var RIA_FRAMEWORK_EXCEPTION = "RiaFrameWork";
//\u670d\u52a1\u56de\u8c03\u5f02\u5e38
var RIA_ERROR_DISPOSE = 401;//\u670d\u52a1\u5bb9\u5668\u5f02\u5e38

//\u7530\u5e94\u6743 2008-5-22 \u6dfb\u52a0\u65e5\u81f3\u5904\u7406\u7c7b
var RiaLog = function() {
};
var showMsgLevel_Ria ="default_ctrl";
RiaLog.prototype.info = function(description) {
};
RiaLog.prototype.debug = function(description){
};
RiaLog.prototype.warn = function(description){
};
RiaLog.prototype.error = function(description) {
  var code_domainService = new Array("20","21","22");
  var code_riaFramework = new Array("00","01","02","03","10","40","60","70");
  var pro_code =arguments[1];
  if(pro_code == "30"||pro_code == 30) alert(description);
  if(showMsgLevel_Ria == constantRia.SHOWDOMAIN_EXCEPTION && pro_code !=undefined){
    for(var i =0;i<code_domainService.length;i++) 
    if(pro_code ==code_domainService[i]) alert(description);
  }
  if(showMsgLevel_Ria == constantRia.SHOWRIA_FRAMEERROR && pro_code !=undefined){
    for(var i=0;i<code_riaFramework.length;i++)
    if(pro_code == code_riaFramework[i]) alert(description);
  }
};
RiaLog.prototype.trace = function(description){
};
RiaLog.prototype.fatal = function(description){
 //alert(description);
};

var logLevel;//\u8bbe\u7f6e\u65e5\u81f3\u7ea7\u522b
var LogFactory = {
    getLog:function() {
        var logjs;
        try {
            logjs = new Log4js.getLogger(RIA_FRAMEWORK_EXCEPTION);
            logLevel = logLevel || Log4js.Level.ALL;
            logjs.setLevel(logLevel);
            logjs.addAppender(new Log4js.ConsoleAppender(false));
            // \u5199\u65e5\u5fd7\u7684\u65b9\u5f0f\u4e0d\u5b89\u5168\uff0c\u6240\u4ee5\uff0c\u8fd8\u662f\u9009\u62e9\u63a7\u5236\u53f0\u6253\u5370
            // var filePath = "C:\\somefile.log";
            //logjs.addAppender(new Log4js.FileAppender(filePath));
        }
        catch(e) {
            logjs = new RiaLog();
        }
        return logjs;
    }
};
var log = new LogFactory.getLog();

//\u7530\u5e94\u6743 2008-5-22 \u6dfb\u52a0\u5f02\u5e38\u5904\u7406\u7c7b
//\u5b9a\u4e49\u5e38\u91cf
var constantRia = {
   EXCEPTION_TYPEHTTP     : "http_error",
   EX_TYPE_HTTPDESC       : $res_entry('ria.exception.service','\u670d\u52a1\u5668\u4e2d\u65ad'), 
   EXCEPTION_TYPENETWORK  : "nerwork_error",
   EX_TYPE_NETDESC        : $res_entry('ria.exception.network',"\u7f51\u7edc\u4e2d\u65ad"),
   RIA_EXCEPTIONTYPE      : $res_entry('ria.exception.exType'," \u5f02\u5e38\u7c7b\u578b:"),
   RIA_EXCEPTIONLINENUMBER: $res_entry('ria.exception.exLine'," \u5f02\u5e38\u884c\u53f7:"),
   RIA_EXCEPTIONCODE      : $res_entry('ria.exception.exCode'," \u5f02\u5e38\u4ee3\u7801:"),
   RIA_EXCEPTIONDESC      : $res_entry('ria.exception.exDesc'," \u5f02\u5e38\u63cf\u8ff0:"),
   RIA_FRAMEWORKEXCEPTION : $res_entry('ria.exception.exFrame',"\u6846\u67b6\u5f02\u5e38"),
   RIA_HTTPEXCEPTION      : $res_entry('ria.exception.exHttp',"HTTP\u5f02\u5e38"),
   RIA_USEREXCEPTION      : $res_entry('ria.exception.exUser',"\u7528\u6237\u5f02\u5e38"),
   RIA_EXCEBACKGROUND     : $res_entry('ria.exception.exBackGround',"\u540e\u53f0\u5f02\u5e38"),
   RIA_DEFAULTEXCEPTION   : $res_entry('ria.exception.exUndefined',"\u672a\u77e5\u5f02\u5e38"),
   RIA_EXCECOMPLETE       : $res_entry('ria.exception.exComp',"\u670d\u52a1\u5b8c\u6210\u65f6\u5f02\u5e38"),
   RIA_EXCE_CTRLDATA      : $res_entry('ria.exception.exUictrl',"UI\u63a7\u4ef6\u7ed1\u5b9a\u5f02\u5e38"),
   RIA_EXCE_ORGANIZE      : $res_entry('ria.exception.exData',"\u6846\u67b6\u7ec4\u7ec7\u6570\u636e\u5f02\u5e38"), 
   RIA_HTTP_INIT          : $res_entry('ria.exception.exHttpInstance',"http\u5bf9\u8c61\u521d\u59cb\u5316\u5f02\u5e38"),  
   RIA_RIA                : "ria",
   RIA_HTTP               : "http",
   RIA_USER               : "user",
   RIA_BACKGROUND         : "background",
   SHOWDOMAIN_EXCEPTION   : "SHOWDOMAIN_EXCEPTION",
   SHOWRIA_FRAMEERROR     : "SHOWRIA_FRAMEERROR"     
};
/*
if(!Object.prototype.extend) {
    Object.extend = function(destination, source) {
      for (property in source) {
        destination[property] = source[property];
      }
      return destination;
    };
    
    Object.prototype.extend = function(object) {
      return Object.extend.apply(this, [this, object]);
    };
};
//\u5f02\u5e38\u5904\u7406\u63a5\u53e3
var IRIAException = function(){
};
//\u63a5\u53e3\u65b9\u6cd5
IRIAException.prototye={
  doException:function(exType){
    return;
  }
};*/
function RIAException(locationExceptionID,exceptionCode,exceptionDESC,reservedFiled){
  this.locationExceptionID = locationExceptionID;
  this.exceptionCode = exceptionCode;
  this.exceptionDESC = exceptionDESC;
  this.reservedFiled = reservedFiled;
};
RIAException.prototype = new Error();//\u6846\u67b6\u5b9e\u73b0\u9519\u8bef\u5f02\u5e38
RIAException.prototype = {
    doException:function(exType,product_code){
      switch(exType){
        case constantRia.RIA_RIA : log.error(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_FRAMEWORKEXCEPTION+","+this.exceptionDESC);break;
        case constantRia.RIA_HTTP: alert(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_HTTPEXCEPTION+","+this.exceptionDESC);break;
        case constantRia.RIA_USER: log.error(constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_USEREXCEPTION+","+this.exceptionDESC); break;
        case constantRia.RIA_BACKGROUND:
            if(product_code == "30" ) log.error(this.exceptionDESC);
            else log.error(constantRia.RIA_EXCEPTIONLINENUMBER+this.locationExceptionID+"\r\n"+constantRia.RIA_EXCEPTIONDESC+constantRia.RIA_EXCEBACKGROUND+","+this.exceptionDESC,product_code); 
            break;
        default: log.error(constantRia.RIA_EXCEPTIONDESC+this.exceptionDESC);break;
      }
    }
};

var commonException = function(flag,exception){
      flag = flag || "";
     //\u5224\u65ad\u662f\u6d4b\u8bd5\u73af\u5883\u8fd8\u662f\u751f\u4ea7\u73af\u5883
     if(log instanceof RiaLog){//\u751f\u4ea7\u73af\u5883\u901a\u8fc7alert\u7684\u65b9\u5f0f\u6765\u5904\u7406
        var product_code = arguments[2];
        exception.doException(flag,product_code); 
     }//\u6d4b\u8bd5\u73af\u5883\u901a\u8fc7log4js\u7684\u5f39\u51fa\u63a7\u5236\u53f0\u7684\u65b9\u5f0f\u6765\u5199\u65e5\u81f3\u4fe1\u606f       
     else{log.error(constantRia.RIA_EXCEPTIONCODE+exception.exceptionCode+constantRia.RIA_EXCEPTIONDESC+exception.exceptionDESC);}
};
var httpExceptionCode = function(httpstatus){
   var flag;
    try{
       if(httpstatus == 500 ||httpstatus == 503||httpstatus == 504) {flag = constantRia.RIA_HTTP;throw new RIAException("",constantRia.EXCEPTION_TYPEHTTP,constantRia.EX_TYPE_HTTPDESC);}
       if(httpstatus == 12029 ){flag =constantRia.RIA_HTTP; throw new RIAException("",constantRia.EXCEPTION_TYPENETWORK,constantRia.EX_TYPE_HTTPDESC); }
    }catch(e){
       commonException(flag,e);
    }    
};

function IswhichBrower(){
 
};
//function HTTPException(){
//};
//HTTPException.prototype = new Error();//http\u5f02\u5e38\u5904\u7406\u7c7b

/////////////////////////////////////////////////////////////////////
//////////////////////////////\u6570\u636e\u6821\u9a8c///////////////////////////////
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u957f\u5ea6 \u6c49\u5b57\u7b97\uff12\u4e2a\u5b57\u8282*/
function strlen(str)
{   var i;
    var len;
    len = 0;
    if(str != undefined && str != null && str != ""){
        for (i=0;i<str.length;i++)
        {
            if (str.charCodeAt(i)>255) len+=2; else len++;
        }
        return len;
    }
};
/*\u68c0\u6d4b\u662f\u5426\u662f\u5b57\u7b26\u6216\u6570\u5b57*/
function ischar(str)
{
    var s2=/[^A-Za-z0-9]/;
   if(s2.exec(str))
        return false;
    else
        return true;
};
/* \u68c0\u6d4b\u5b57\u7b26\u4e32\u662f\u5426\u4e3a\u7a7a */
function isnull(str)
{
    if(str == null) return true;
    var i;
    for (i=0;i<str.length;i++)
    {
        if (str.charAt(i)!=' ') return false;
    }
    return true;
};
//\u5224\u65ad\u8f93\u5165\u662f\u5426\u4e3a\u5b57\u6bcd
function isLetter(str)
{
   var regex=/^([a-zA-Z])*$/;
   if(regex.test(str))
   {
      return true;
   }
   return false;
}
/*\u5224\u65ad\u662f\u5426\u8f93\u5165\u4e86\u4e2d\u6587\u7684\u5b57\u7b26*/
function isExsitChineseChar(str){
    if( typeof str =="undefined" || isnull(str) || str =="")
        return false;
   if(str.length!=strlen(str))
        return true;
   else
        return false;
};

//\u68c0\u6d4b\u8d27\u5e01
function ismoney(str)
{
    var pos1 = str.indexOf(".");
    var pos2 = str.lastIndexOf(".");
    if ((pos1 != pos2)||!isnumber(str)) {
        return false;
    }
    return true;
};
//\u68c0\u6d4b\u5e74(YYYY\u5e74)
function  isyear(str)
{
  if(!isnumber(str))
     return false;
   //if(str.length != 4 ) {
    //  return false;
    //}

    if((str<'1950')||(str>'2050'))
    {
        return false;
    }

   return true;

};

//\u68c0\u6d4b\u975e\u7a7a
function checklength(val,maxlen)
{
    var str = trim(document.forms[0].elements[val].value);
    if ( str == "" && maxlen==null ) {
        alert ($res_entry("ria.exception.notnull","\u6b64\u9879\u4e0d\u53ef\u4e3a\u7a7a"));
        document.forms[0].elements[val].focus();
        document.forms[0].elements[val].select();
        return false;
    }else if(str!="" && maxlen!=null){
        if (str.length>maxlen){
            return false;
        }
    }
    document.forms[0].elements[val].value = str;
    return true;
};
//\u9a8c\u8bc1\u6570\u5b57
function checknum(val)
{
   if(isNaN(document.forms[0].elements[val].value)) {
       alert ($res_entry("ria.exception.neednumber","\u8bf7\u8f93\u5165\u6570\u5b57"));
       document.forms[0].elements[val].focus();
       document.forms[0].elements[val].select();
       return false;
   }
   return true;
};
//\u53bb\u7a7a\u683c
function trim(str)
{
    var num = str.length;
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
};
//\u68c0\u6d4b\u7535\u8bdd\u53f7\u7801
function isphone(str)
{
    var number_chars = "()-1234567890";
    var i;
    for (i=0;i<str.length;i++)
    {
    if (number_chars.indexOf(str.charAt(i))==-1) return false;
    }
    return true;
};

function replaceAll(str){
    var i;
    for (i=0;i<str.length;i++)
    {/*
        if(str.charAt(i)=='&')
            str=str.replace('&','&amp;');
        if(str.charAt(i)=='<')
            str=str.replace('<','&lt;');
        if(str.charAt(i)=='>')
            str=str.replace('>','&gt;');*/
        if(str.charAt(i)=='\'')
            str=str.replace('\'','\\uff07');
        if(str.charAt(i)=='\"')
            str=str.replace('\"','\\uff02');
    }
            return str;
};
//\u68c0\u67e5\u8868\u5355\u6240\u6709\u5143\u7d20
function verifyAll(myform)
{
    var i;
    for (i=0;i<myform.elements.length;i++)
    {
        /*\u5c06\u534a\u89d2\u8f6c\u6362\u4e3a\u5168\u89d2*///comment by chris:\u4ec5\u4ec5\u662f\u5c06\u4e2d\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7\u6362\u6210\u82f1\u6587\u7684\u53cc\u5f15\u53f7\u548c\u5355\u5f15\u53f7
        if (myform.elements[i].type=="textarea"||myform.elements[i].type=="text")
        {
            myform.elements[i].value=replaceAll(myform.elements[i].value);
        }

        /* \u975e\u81ea\u5b9a\u4e49\u5c5e\u6027\u7684\u5143\u7d20\u4e0d\u4e88\u7406\u776c */
        if (myform.elements[i].chname+""=="undefined") continue;

        /* \u6821\u9a8c\u5f53\u524d\u5143\u7d20 */
        if (verifyInput(myform.elements[i])==false)
        {
            myform.elements[i].focus();
            myform.elements[i].select();
            return false;
        }
    }
    return true;
};
function isElementDefined(el_ctrl){
  if(el_ctrl == null || el_ctrl == undefined || el_ctrl == "") return false;
  else if(el_ctrl) return true;
};
/* \u68c0\u6d4b\u6307\u5b9a\u6587\u672c\u6846\u8f93\u5165\u662f\u5426\u5408\u6cd5 */
function verifyInput(input,datatype)
{
    var i;
    var error = false;
    var isnotnull=false;
    input.datatype = datatype;
    input.chname = input.name;
    var elementCtrl = input.id;
    var el_ctrl = document.getElementById(elementCtrl+RIA_MSG_LABLE);
    /* \u975e\u7a7a\u6821\u9a8c */
    
    if (isnull(input.value))
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText =$res_entry("ria.exception.isnull","\u4e0d\u80fd\u4e3a\u7a7a");
        else alert("\""+input.chname+"\""+$res_entry("ria.exception.isnull","\u4e0d\u80fd\u4e3a\u7a7a\uff01"));
        return false;
    }

    if(input.maxlen+""!="undefined" && strlen(input.value)>input.maxlen)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.toolonger","\u8d85\u8fc7\u9650\u5236\u957f\u5ea6!");
        else alert("\""+input.chname+"\""+$res_entry("ria.exception.toolonger","\u8d85\u8fc7\u9650\u5236\u957f\u5ea6!"));
        error = true;
        return false;
    }
    if(input.datatype+""!="undefined")
    {
     
     var type = input.datatype;
     var datasize = null;
     var size = null;
     if(type.indexOf("(") != -1)
     {
        type = input.datatype.substring(0, type.indexOf("("));
        
        datasize = input.datatype.substring(input.datatype.indexOf("(") + 1, input.datatype.indexOf(")"));
        size = datasize.split(",");
     }
    //\u4e0d\u533a\u5206\u5927\u5c0f\u5199
    type=type.toLowerCase();
    /* \u6570\u636e\u7c7b\u578b\u6821\u9a8c */
        switch(type)
        {
            case "posiint":
                 if(isnull(input.value)) error = true;
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57");
                          else alert("\""+input.chname+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57"));

                          error = true;
                     }
                     else{
                         if (isPositiveInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                              if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.positivenumber","\u503c\u5e94\u4e3a\u6b63\u6574\u6570");
                              else alert("\""+input.chname+"\""+$res_entry("ria.exception.positivenumber","\u503c\u5e94\u4e3a\u6b63\u6574\u6570"));
                              error = true;
                         }
                         else if(size+""!="undefined" && size!=null && strlen(input.value)>size)
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6");
                             else alert("\""+input.chname+"\""+$res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6"));
                             error = true
                         } 
                     }    
                  }
                  
                  break;
                  
            case "negaint":
                 if(isnull(input.value)) error = true; 
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57");
                          else alert("\""+input.chname+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57"));
                          error = true;
                     }
                     else{
                         if (isNegativeNumber(input.value)==false)//\u975e\u6570\u5b57
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.negative","\u503c\u5e94\u4e3a\u8d1f\u6574\u6570");
                             else alert("\""+input.chname+"\""+$res_entry("ria.exception.negative","\u503c\u5e94\u4e3a\u8d1f\u6574\u6570"));
                             error = true;
                         }
                         else if(size+""!="undefined" && size!=null&& strlen(input.value.substring(1))>size)
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6");
                             else alert("\""+input.chname+"\""+$res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6"));
                             error = true
                         } 
                     }    
                  }
                  
                  break;  
                  
             case "int":
                 if(isnull(input.value)) error = true; 
                 if(input.value!="")
                 {
                     if(isExsitChineseChar(input.value))
                     {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57");
                          else alert("\""+input.chname+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u6570\u5b57"));
                          error = true;
                     }
                     else{
                         if (isInteger(input.value)==false)//\u975e\u6570\u5b57
                         {
                             if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.integral","\u503c\u5e94\u4e3a\u6574\u6570");
                             else alert("\""+input.chname+"\""+$res_entry("ria.exception.integral","\u503c\u5e94\u4e3a\u6574\u6570"));
                             error = true;
                         }
                         else if(size+""!="undefined" && size!=null)
                         {
                            var reg1  = "^[0-9]+$";          
                            var regu2 = "^-[0-9]+$";
                            var re1 = new RegExp(reg1);
                            var re2 = new RegExp(regu2);                        
                            if (input.value.search(re2) != -1 && strlen(input.value.substring(1))>size)
                            {
                              if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6");
                              else alert("\""+input.chname+"\""+$res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6"));
                               error = true
                            }
                            else if(input.value.search(re1)!=-1 && strlen(input.value)>size)
                            {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6");
                                else alert("\""+input.chname+"\""+$res_entry("ria.exception.valuetoolonger","\u503c\u957f\u5ea6\u8d85\u8fc7\u9650\u5236\u957f\u5ea6"));
                                error = true
                            } 
                         } 
                     }    
                  }
                  
                  break;        
                      
            case "char": 
                 if(isnull(input.value)) error = true;
                 if (ischar(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.neednumberorchar","\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd");
                        else alert("\""+input.chname+"\""+$res_entry("ria.exception.neednumberorchar","\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd"));
                        error = true;
                    }
                    
                    break;
                    
            case "mobile":
                 if(isnull(input.value)) error = true; 
                 if(trim(input.value)!="")
                 {            
                    if(checkMobile(trim(input.value))==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.formaterror","\u683c\u5f0f\u4e0d\u6b63\u786e");
                        else alert("\""+input.chname+"\""+$res_entry("ria.exception.formaterror","\u683c\u5f0f\u4e0d\u6b63\u786e"));
                        error = true;
                    }
                 }      
                        
                 break;
                 
            case "postcode":
                if(isnull(input.value)) error = true;
                if(checkPostCode(trim(input.value))==false)
                {
                    if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.sixnumber","\u53ea\u80fd\u662f\u957f\u5ea6\u4e3a6\u4f4d\u7684\u6570\u5b57");
                    else alert("\""+input.chname+"\""+$res_entry("ria.exception.sixnumber","\u53ea\u80fd\u662f\u957f\u5ea6\u4e3a6\u4f4d\u7684\u6570\u5b57"));
                    error = true;
                }
                
                break;
                
            case "posidecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                if(isnull(input.value)) error = true;
                 input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkPositiveDecimal(input,el_ctrl)==false)
                  {
                      error=true;
                  }
                }  
                
               break;
               
           case "negadecimal"://\u53ea\u4e3a\u6b63\u5c0f\u6570
                if(isnull(input.value)) error = true;
                input.precision = datasize;
                if(trim(input.value)!="")
                {
                  if(checkNegativeDecimal(input,el_ctrl)==false)
                  {
                      error=true;
                  }
                }  
                
               break;    
            
           case "decimal"://\u4e3a\u6b63\u5c0f\u6570\uff0c\u4e5f\u53ef\u4ee5\u4e3a\u8d1f\u5c0f\u6570
               if(isnull(input.value)) error = true;
               input.precision = datasize;
               if(trim(input.value)!="")
               {
                 if(checkDecimal(input,el_ctrl)==false)
                 {
                     error=true;
                 }
               }  
                 break;

           case "numberchar":
              if(isnull(input.value)) error = true;
              if(input.value!="")
              {
                 if(isNumberOrChar(trim(input.value))==false)
                 {
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText =  $res_entry("ria.exception.neednumberorchar","\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd");
                     else alert("\""+input.chname+"\""+$res_entry("ria.exception.neednumberorchar","\u503c\u5e94\u8be5\u662f\u6570\u5b57\u6216\u5b57\u6bcd"));
                     error = true;
                 }
              }
              
                 break;
                 
            case "phone":
                if(isnull(input.value)) error = true;
                if(input.value!="")
                {
                   if((trim(input.value)).length<6){
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.moresixnumber","\u957f\u5ea6\u5fc5\u987b6\u4f4d\u4ee5\u4e0a");
                        else alert("\""+input.chname+"\""+$res_entry("ria.exception.moresixnumber","\u957f\u5ea6\u5fc5\u987b6\u4f4d\u4ee5\u4e0a"));
                        error = true;
                        break;
                    }
                   if (isphone(input.value)==false)
                       {
                          if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.mustlike","\u53ea\u53ef\u5305\u542b-\u3001()\u548c\u6570\u5b57");
                          else alert("\""+input.chname+"\""+$res_entry("ria.exception.mustlike","\u53ea\u53ef\u5305\u542b-\u3001()\u548c\u6570\u5b57"));
                          error = true;
                       }
                  }
                  
                  break;
                  
             case "money": 
                  if(isnull(input.value)) error = true;
                  if(ismoney(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.numbermustlike","\u53ea\u53ef\u5305\u542b\u4e00\u4e2a.\u548c\u6570\u5b57");
                        else alert("\""+input.chname+"\""+$res_entry("ria.exception.numbermustlike","\u53ea\u53ef\u5305\u542b\u4e00\u4e2a.\u548c\u6570\u5b57"));
                        error = true;
                    }
                    
                    break;
                    
             case "year":   
                   if(isnull(input.value)) error = true;
                   if(isyear(input.value)==false)
                    {
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.nearbetween","\u5e94\u8be5\u662f\u4e00\u4e2a\u5e74\u4efd\u8303\u56f4\u57281950\uff0d2050\u4e4b\u95f4");
                        else alert("\""+input.chname+"\""+$res_entry("ria.exception.nearbetween","\u5e94\u8be5\u662f\u4e00\u4e2a\u5e74\u4efd\u8303\u56f4\u57281950\uff0d2050\u4e4b\u95f4"));
                        error = true;
                    }
                    break;
                    
             case "cardid": 
                    if(isnull(input.value)) error = true;
                    if(input.value!="")
                    {
                        return validIdCard(input.value,input.chname,el_ctrl);
                    }
                    break;
                    
             case "email":  
                   if(isnull(input.value)) error = true;
                   if(input.value!="")
                    {
                        return emailCheck(input.value,input.chname,el_ctrl);
                    }
                    
                    break;
                    
             case "date":
                   if(isnull(input.value)) error = true;             
                   input.precision = datasize;
                   if(isExsitChineseChar(input.value))
                   {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f");
                       else alert("\""+input.chname+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01"));
                       error = true;
                   }
                   else
                       if(input.value != "")
                           return checkDate(input,el_ctrl);
                           //return true;
                  
                  break;  
             case "percent" :
                  if(isnull(input.value)) error = true;
                  if(isExsitChineseChar(input.value))
                  {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f");
                       else alert("\""+input.chname+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\u6216\u201c\uff0d\u201d\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\u65e5\u671f\uff01"));
                       error = true;                    
                  }else if(input.value != "")
                  {
                    if(checkPercent(input.value))
                    {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.needpercent","\u5e94\u8be5\u662f\u4e00\u4e2a\u767e\u5206\u6570");
                       else alert("\""+input.chname+"\""+$res_entry("ria.exception.needpercent","\u5e94\u8be5\u662f\u4e00\u4e2a\u767e\u5206\u6570"));
                       return false;
                    }
                    return true;
                  }else
                  {
                    return true;
                  }
                                  
            case "letter":
                  if(isnull(input.value)) error = true;
                  if(!isLetter(input.value))
                  {
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.needchar","\u8f93\u5165\u7684\u5e94\u8be5\u662f\u5b57\u6bcd\uff01");
                     else alert("\""+input.chname+"\""+$res_entry("ria.exception.needchar","\u8f93\u5165\u7684\u5e94\u8be5\u662f\u5b57\u6bcd\uff01"));
                     return false;
                  }
                  else{
                     return true;
                  }
            case "nonempty":
                 if (input.value == null||input.value ==""){
                     if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.notnull","\u4e0d\u80fd\u4e3a\u7a7a");
                     else alert("\""+input.chname+"\""+$res_entry("ria.exception.notnull","\u4e0d\u80fd\u4e3a\u7a7a"));
                     error =true;
                     
                 } 
                 break;                       
            /* \u5728\u8fd9\u91cc\u53ef\u4ee5\u6dfb\u52a0\u591a\u4e2a\u81ea\u5b9a\u4e49\u6570\u636e\u7c7b\u578b\u7684\u6821\u9a8c\u5224\u65ad */
            /*  case datatype1: ... ; break;        */
            /*  case datatype2: ... ; break;        */
            /*  ....................................*/
            default :           
                var regu =type;
                var re = new RegExp(regu);
                if (!re.test(input.value)) 
                {
                   error = true;                 
                }   
               break;
        }
    }
    /* \u6839\u636e\u6709\u65e0\u9519\u8bef\u8bbe\u7f6e\u6216\u53d6\u6d88\u8b66\u793a\u6807\u5fd7 */
    if (error)
    {
        return false;
    }
    else
    {
        return true;
    }
};
//\u9a8c\u8bc1percent
function checkPercent(regEx)
{
    var regu =/^-?([1-9]\d*|0)(\.\d+)?%$/;
    var re = new RegExp(regu);
    if (re.test(regEx)) 
    {
       return false;                 
    }
    else
    {
      return true;
    }
}
//\u5c4f\u853d\u952e\u76d8\u5feb\u6377\u952e
function checkKey()
{
    var srce = window.event.srcElement.type;
    var shield = false;
    //\u6587\u672c\u6846\u5185\u4e0d\u5c4f\u853d\u5feb\u6377\u952e
    if(window.event.keyCode == 8 && srce =="password") return;
    if(window.event.keyCode == 8 && srce =="file") return;
    if (window.event.keyCode == 8 && srce =="text") return;
    if (window.event.keyCode == 8 && srce =="textarea") return;

    //\u5c4f\u853d\u6587\u672c\u6846\u5185\u7684\u56de\u8f66
    if (window.event.keyCode == 13 && srce =="password"){
        event.keyCode=0;
        return false;
    }

    if (window.event.keyCode == 13 && srce =="file"){
        event.keyCode=0;
        return false;
    }
    if (window.event.keyCode == 13 && srce =="text"){
        event.keyCode=0;
        return false;
    }
    if (window.event.keyCode == 13 && srce =="textarea"){
        event.keyCode=0;
        return false;
    }

    if ((event.keyCode==8) || //\u5c4f\u853d\u9000\u683c\u5220\u9664\u952e
        (event.keyCode==116)|| //\u5c4f\u853d F5 \u5237\u65b0\u952e
        (event.ctrlKey && event.keyCode==82)){ //Ctrl + R
        event.keyCode=0;
        return false;
    }
    if ((window.event.altKey)&&
        ((window.event.keyCode==37)|| //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2190
        (window.event.keyCode==39))){ //\u5c4f\u853d Alt+ \u65b9\u5411\u952e \u2192
        alert("\u4e0d\u51c6\u4f60\u4f7f\u7528ALT+\u65b9\u5411\u952e\u524d\u8fdb\u6216\u540e\u9000\u7f51\u9875\uff01");
        return false;
    }
    if ((event.ctrlKey)&&(event.keyCode==78)) //\u5c4f\u853d Ctrl+n
        return false;
    if (window.event.srcElement.tagName == "A" && window.event.shiftKey)
        return false; //\u5c4f\u853d shift \u52a0\u9f20\u6807\u5de6\u952e\u65b0\u5f00\u4e00\u7f51\u9875
};
function compareDate(date1,date2){
    var value;
    if((date1==null||date1=="undefined")&&(date2==null||date2=="undefined"))
        value="d1=d2";
    else if((date1==null||date1=="undefined")&&(date2!=null&&date2!="undefined"))
         value="d1<d2";
    else if((date1!=null&&date1!="undefined")&&(date2==null||date2=="undefined"))
         value="d1>d2";
    else 
        value=date1.localeCompare(date2);
    if(value==0)
        return "d1=d2";
    else if (value>0)
        return "d1>d2";
    else
        return "d1<d2";
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u7684\u65e5\u671f\u662f\u5426\u7b26\u5408 yyyy-MM-dd
\u8f93\u5165\uff1a
    value\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDate(elem)
{
    var date=elem.value;
    var length=date.length;
    var year=date.substr(0,4);
    var month=date.substr(5,2);
    var day=date.substr(8,2);
    var var1=date.substr(4,1);
    var var2=date.substr(7,1);

    if(length == '10' && var1==var2 && (var1 == '-'||var1 == '/'||var1 == '.'))
    {
        if(isNumber(year) && isNumber(month) && isNumber(day))
        {
                    if(month>"12" || month< "01") {
                       alert("\""+elem.chname+"\""+$res_entry("ria.exception.monthbetween","\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4"));
                       return false;
                    }
                    if(day>getMaxDay(year,month) || day< "01") {
                       alert("\""+elem.chname+"\""+$res_entry("ria.exception.maxdateerror","\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408"));
                       return false;
                    }
                    
                    return true;
        }
        else {
                   alert("\""+elem.chname+"\""+$res_entry("ria.exception.dateformateerror","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57"));
                   return false;
              }
    }
    else {
         
          
             alert("\"" + elem.chname + "\""+$res_entry("ria.exception.dateformatelike","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD,YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f"));
             return false;
            
    }
};

function checkDate(elem,el_ctrl)
{
    var format=elem.precision;
    var date=elem.value;
    // sujingui  
    var start=/^('|")/;
    var end=/('|")$/;
    if(date!="undefined" && date != null && date != "")
    {       
        date=date.replace(start,"");
        date=date.replace(end,"");  
    }
    if(format!="undefined" && format != null && format != "")
    {
        format=format.replace(start,"");
        format=format.replace(end,"");  
    }
    //end
    var length=date.length;
    var year=date.substr(0,4);
    var month=date.substr(5,2);
    var day=date.substr(8,2);
    var var1=date.substr(4,1);
    var var2=date.substr(7,1);

         
    if(length == '10' && var1==var2 && (var1 == '-'||var1 == '/'||var1 == '.'))
    {
        if(isNumber(year) && isNumber(month) && isNumber(day))
        {
                    if(month>"12" || month< "01") {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.monthbetween","\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4");
                       else alert("\""+elem.chname+"\""+$res_entry("ria.exception.monthbetween","\u6708\u4efd\u5e94\u8be5\u572801\u548c12\u4e4b\u95f4"));
                       return false;
                    }
                    if(day>getMaxDay(year,month) || day< "01") {
                       if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.maxdateerror","\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408");
                       else alert("\""+elem.chname+"\""+$res_entry("ria.exception.maxdateerror","\u6708\u4efd\u548c\u65e5\u671f\u6700\u5927\u503c\u4e0d\u7b26\u5408"));
                       return false;
                    }
                    if(format!="undefined" && format != null && format != "")
                    {
                                  
                        if(format=="YYYY/MM/DD")
                        {
                           var reg=/^(\d{4})([/])(\d{2})([/])(\d{2})/;
                           if(!reg.test(date))
                           {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.dateformatelikesecond","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY/MM/DD");
                                else alert("\""+elem.chname+"\""+$res_entry("ria.exception.dateformatelikesecond","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY/MM/DD"));
                                return false;
                           }
                        }
                        else if(format=="YYYY-MM-DD")
                        {
                           var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
                           if(!reg.test(date))
                           {  
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.dateformatelikethird","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY-MM-DD");
                                else alert("\""+elem.chname+"\""+$res_entry("ria.exception.dateformatelikethird","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY-MM-DD"));
                                return false;
                           }
                        }
                        else if(format=="YYYY.MM.DD")
                        {
                           var reg=/^(\d{4})([.])(\d{2})([.])(\d{2})/;
                           if(!reg.test(date))
                           {
                                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.dateformatelikeforth","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY.MM.DD");
                                else alert("\""+elem.chname+"\""+$res_entry("ria.exception.dateformatelikeforth","\u65e5\u671f\u683c\u5f0f\u5fc5\u987b\u4e3aYYYY.MM.DD"));
                                return false;
                           }
                        }
                      }
                    return true;
        }
        else {
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.dateformateerror","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57\uff01");
                   else alert("\""+elem.chname+"\""+$res_entry("ria.exception.dateformateerror","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u5e74\u6708\u65e5\u5fc5\u987b\u4e3a\u6570\u5b57\uff01"));
                   return false;
              }
    }
    else {
         
             if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.dateformatelikefirth","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f");
             else alert("\"" + elem.chname + "\""+$res_entry("ria.exception.dateformatelikefirth","\u65f6\u95f4\u683c\u5f0f\u4e0d\u5408\u6cd5\uff0c\u8bf7\u68c0\u67e5\u65e5\u671f\u662f\u5426\u7b26\u5408YYYY/MM/DD YYYY-MM-DD\u6216YYYY.MM.DD\u683c\u5f0f"));
             return false;
            
    }
};
function getMaxDay(year,month) {
    if(month==4||month==6||month==9||month==11)
        return "30";
    if(month==2)
        if(year%4==0&&year%100!=0 || year%400==0)
            return "29";
        else
            return "28";
    return "31";
};

//\u8eab\u4efd\u8bc1\u4e2d\u7684\u7c4d\u8d2f\u4fe1\u606f
function IdCardArea(){
    var area={11:"\u5317\u4eac",12:"\u5929\u6d25",13:"\u6cb3\u5317",14:"\u5c71\u897f",15:"\u5185\u8499\u53e4",21:"\u8fbd\u5b81",22:"\u5409\u6797",23:"\u9ed1\u9f99\u6c5f",31:"\u4e0a\u6d77",32:"\u6c5f\u82cf",33:"\u6d59\u6c5f",34:"\u5b89\u5fbd",35:"\u798f\u5efa",36:"\u6c5f\u897f",37:"\u5c71\u4e1c",41:"\u6cb3\u5357",42:"\u6e56\u5317",43:"\u6e56\u5357",44:"\u5e7f\u4e1c",45:"\u5e7f\u897f",46:"\u6d77\u5357",50:"\u91cd\u5e86",51:"\u56db\u5ddd",52:"\u8d35\u5dde",53:"\u4e91\u5357",54:"\u897f\u85cf",61:"\u9655\u897f",62:"\u7518\u8083",63:"\u9752\u6d77",64:"\u5b81\u590f",65:"\u65b0\u7586",71:"\u53f0\u6e7e",81:"\u9999\u6e2f",82:"\u6fb3\u95e8",00:"\u56fd\u5916"};
    return area;
};

//\u8eab\u4efd\u8bc1\u7684\u9a8c\u8bc1      <!--{idcard}-->
function validIdCard(strVal,name,el_ctrl)
{
        //add by chris on 2006-03-20,\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad
        var areaArray=IdCardArea();

    var count = 0;
    var numArray = "0123456789";

    strVal =trim(strVal);
    var strSfzhm = strVal;

        if(isExsitChineseChar(strVal))
        {
           if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165");
           else alert("\""+name+"\""+$res_entry("ria.exception.inputkeyborderror","\u503c\u53ef\u80fd\u5b58\u5728\u5168\u89d2\u8f93\u5165\u6570\u5b57\uff0c\u8bf7\u5c06\u8f93\u5165\u6cd5\u8c03\u6574\u4e3a\u534a\u89d2\u518d\u91cd\u65b0\u8f93\u5165\uff01"));
        }

    if ((strSfzhm.length !=15 ) && (strSfzhm.length!=18))
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.needlength","\u5e94\u8f93\u516515\u4f4d\u621618\u4f4d");
        else alert("["+name+"]" +$res_entry("ria.exception.needlength","\u5e94\u8f93\u516515\u4f4d\u621618\u4f4d"));
        return false;
    }
    else
    {
        if (strSfzhm.length == 15)
        {
            for(var i=0;i<15;i++)
            {
                tmpChar = strSfzhm.charAt(i);
                if ( numArray.indexOf(tmpChar) == -1 )
                {
                    count = count+1;
                }
            }
            if(count!=0)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.15errors","15\u4f4d\u6570\u5b57\u8f93\u5165\u6709\u8bef!");
                else alert("["+name+"]"+$res_entry("ria.exception.15errors","15\u4f4d\u6570\u5b57\u8f93\u5165\u6709\u8bef!"));
                return false;
            }
        }
        else            //18\u4f4d\u7684SFZHM\u6821\u9a8c
        {
            for(i=0;i<17;i++)
            {
                tmpChar = strSfzhm.charAt(i);
                if(numArray.indexOf(tmpChar)==-1)
                {
                    if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.18inputnumbererrors","18\u4f4d\u65f6\u524d17\u6570\u5b57\u8f93\u5165\u6709\u8bef");
                    else alert("["+name+"]" + $res_entry("ria.exception.18inputnumbererrors","18\u4f4d\u65f6\u524d17\u6570\u5b57\u8f93\u5165\u6709\u8bef") );
                    return false;
                }
            }
            var lastchar = strSfzhm.charAt(17).toLowerCase();
            var denominator='abcdefghijklmnopqrstuvwxyz0123456789';
            if(denominator.indexOf(lastchar) == -1)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.18numberorchar","18\u4f4d\u5e94\u4e3a\u6570\u5b57\u6216\u5b57\u6bcd!");
                else alert("["+name+"]" + $res_entry("ria.exception.18numberorchar","18\u4f4d\u5e94\u4e3a\u6570\u5b57\u6216\u5b57\u6bcd!"));
                return false;
            }
        }
                    //\u589e\u52a0\u7c4d\u8d2f\u5224\u65ad\uff0cadd by chris on 2006-03-20
                     var sub=strVal.substr(0,2);
                     if((typeof areaArray[sub]) =="undefined"){
                        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.noprovince","\u524d\u4e24\u4f4d\u6570\u5b57\u5bf9\u5e94\u7684\u7c4d\u8d2f\u4e3a'\u4e0d\u660e\u7701\u4efd'");
                        else alert("["+name+"]" +$res_entry("ria.exception.noprovince","\u524d\u4e24\u4f4d\u6570\u5b57\u5bf9\u5e94\u7684\u7c4d\u8d2f\u4e3a'\u4e0d\u660e\u7701\u4efd'") );
                        return false;
                     }
    }
    return true;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u624b\u673a\u53f7\u7801\u662f\u5426\u6b63\u786e
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function checkMobile( s ){
    //sujingui
    var regu =/^[1][3|5][0-9]{9}$/;
    var re = new RegExp(regu);
    if (re.test(s)) {
      return true;
    }else{
      return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u90ae\u7f16\u683c\u5f0f\u662f\u5426\u6b63\u786e
*/
function checkPostCode(s){
   if(s!= null && s.length==6&&isNumber(s)){
      return true;
   }
   else
     return false;
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u6b63\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumber( s ){
    var regu = "^[0-9]+$";
    var re = new RegExp(regu);
    if (s!=null && s.search(re) != -1) {
       return true;
    } else {
       return false;
    }
};

function isInteger( s ){
    var regu1 = "^[0-9]+$";
    var regu2 = "^-[0-9]+$";
    var re1 = new RegExp(regu1);
    var re2 = new RegExp(regu2);

    if (s!=null && (s.search(re1) != -1 || s.search(re2) != -1)) {
       return true;
    } else {
       return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u7b26\u5408\u8d1f\u6574\u6570\u683c\u5f0f,\u4e0d\u53ef\u4ee5\u5728\u6574\u6570\u540e\u52a0.
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNegativeNumber( s ){
    var regu = "^-[0-9]+$";
    var re = new RegExp(regu);
    if (s!=null && s.search(re) != -1) {
       return true;
    } else {
       return false;
    }
};

/*
  \u7528\u9014:\u65e5\u671f\u8054\u60f3\u8f93\u5165\u6cd5
*/
function smartDay(obj){
  var year,month,day;
  var date=obj.value;
  if(date.length==8){
     if(isNumber(obj.value)){
        year=date.substr(0,4);
    month=date.substr(4,2);
    day=date.substr(6,2);
        obj.value=year+"-"+month+"-"+day;
    }
  }

};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u53ea\u7531\u82f1\u6587\u5b57\u6bcd\u548c\u6570\u5b57\u7ec4\u6210
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isNumberOrChar( s ){    //\u5224\u65ad\u662f\u5426\u662f\u6570\u5b57\u6216\u5b57\u6bcd
    var regu = "^[0-9a-zA-Z]+$";
    var re = new RegExp(regu);
    if (re.test(s)) {
      return true;
    }else{
      return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u53ef\u4ee5\u662f\u8d1f\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isDecimal( str ){
        if(isNumber(str)||isNegativeNumber(str)) return true;
    var re = /^[-]{0,1}(\d+)[\.]+(\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};

/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveDecimal( str ){
       if(isNumber(str)) return true;

    var re = /^(\d+)[\.](\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};
function isNegativeDecimal( str ){
       if(isNegativeNumber(str)) return true;

    var re = /^-(\d+)[\.](\d+)$/;
    if (re.test(str)) {
       //if(RegExp.$1==0&&RegExp.$2==0) return false;//add by chris,\u5224\u65ad\u5c0f\u6570\u662f\u5426\u53ef\u4ee50.0
       return true;
    } else {
       return false;
    }
};
/*
\u7528\u9014\uff1a\u68c0\u67e5\u8f93\u5165\u5b57\u7b26\u4e32\u662f\u5426\u662f\u5e26\u5c0f\u6570\u7684\u6570\u5b57\u683c\u5f0f,\u5fc5\u987b\u662f\u6b63\u6570
\u8f93\u5165\uff1a
    s\uff1a\u5b57\u7b26\u4e32
\u8fd4\u56de\uff1a
    \u5982\u679c\u901a\u8fc7\u9a8c\u8bc1\u8fd4\u56detrue,\u5426\u5219\u8fd4\u56defalse
*/
function isPositiveInteger( str ){
       if(isNumber(str)) return true;
    var re = /^(\d+)(\d+)$/;
    if (re.test(str)) {
       if(RegExp.$1==0&&RegExp.$2==0) return false;
       return true;
    } else {
       return false;
    }
};

function checkNegativeDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
                if(isNegativeDecimal(obj.value)==false){
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.negativeformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u8d1f\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u8d1f\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206");
                   else alert("\""+obj.chname+"\""+$res_entry("ria.exception.negativeformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u8d1f\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u8d1f\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206"));
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       if(numType.indexOf(",")!=-1)
                       {
                          numpric = Math.abs( strArr[1] );
                       }
                       else
                       { 
                          numpric=0;
                       }
                      
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function checkPositiveDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
                if(isPositiveDecimal(obj.value)==false){
                   if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.positiveformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u6b63\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6b63\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206");
                   else alert("\""+obj.chname+"\""+$res_entry("ria.exception.positiveformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u6b63\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6b63\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206"));
                   return false;
                }

               if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       //\u5f53\u6709\u4e24\u4e2a\u53c2\u6570\u9650\u5236\u65f6
                       if(numType.indexOf(",")!=-1)
                       {
                          numpric = Math.abs( strArr[1] );
                       }
                       else
                       {
                          numpric = 0;
                       }
                                  
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function checkDecimal(obj,el_ctrl){

    var strTemp;
    var numpric;
    var numLen;
    var strArr;
    try{
            if(isDecimal(obj.value)==false){
               if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.decimalformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206");  
               else alert("\""+obj.chname+"\""+$res_entry("ria.exception.decimalformate","\u8f93\u5165\u5fc5\u987b\u4e3a\u5c0f\u6570!\u6ce8\u610f\u683c\u5f0f\u5e94\u4e3a\u6574\u6570\uff0c\u5c0f\u6570\u70b9\u52a0\u4e0a\u5c0f\u6570\u90e8\u5206\uff01"));
               return false;
            }
            if(obj.precision){
                 var numType=obj.precision;
                   
               if(numType != null&& numType !=""){
                      // strTemp = numType.substr( numType.indexOf("(") + 1 ,numType.indexOf(")") - numType.indexOf("(") -1 );
                       strArr = numType.split(",");
                       numLen = Math.abs( strArr[0] );
                       if(numType.indexOf(",")!=-1)
                       {
                            numpric = Math.abs( strArr[1] );
                       }
                       else
                       {
                            numpric = 0;
                       }
                      
                      
                      return f_checkNumLenPrec(obj,numLen, numpric,el_ctrl);
                 }
            }
    }catch(e){
        alert("in checkDecimal = " + e);
    }
};

function f_checkNumLenPrec(obj, len, pric,el_ctrl){
    var numReg;
    var value = obj.value;
    var strValueTemp, strInt, strDec;
    //alert(value + "=====" + len + "====="+ pric);
    try{
    
        numReg =/[\-]/;
        strValueTemp = value.replace(numReg, "");
        strValueTemp = strValueTemp.replace(numReg, "");
        //\u6574\u6570
        if(pric==0){
            numReg =/[\.]/;
            //alert(numReg.test(value));
            if(numReg.test(value) == true){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.integral","\u503c\u5e94\u4e3a\u6574\u6570");                
                else alert("\""+obj.chname+"\""+$res_entry("ria.exception.integral","\u503c\u5e94\u4e3a\u6574\u6570"));
                return false;
            }
        }

        if(strValueTemp.indexOf(".") < 0 ){
            //alert("lennth==" + strValueTemp);
            if(strValueTemp.length > len){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.integertoolong","\u6574\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570");  
                else alert("\""+obj.chname+"\""+$res_entry("ria.exception.integernotlong","\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7")+ len +$res_entry("ria.exception.wei","\u4f4d"));
                return false;
            }

        }else{
            strInt = strValueTemp.substr( 0, strValueTemp.indexOf(".") );
            //alert("lennth==" + strInt);
            if(strInt.length > len ){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.integertoolong","\u6574\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570");
                else alert("\""+obj.chname+"\""+$res_entry("ria.exception.integernotlong","\u6574\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7")+ len + $res_entry("ria.exception.wei","\u4f4d"));
                return false;
            }

            strDec = strValueTemp.substr( (strValueTemp.indexOf(".")+1), strValueTemp.length );
            //alert("pric==" + strDec);
            if(strDec.length > pric){
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.decimaletoolong","\u5c0f\u6570\u4f4d\u8d85\u8fc7\u4f4d\u6570");
                else alert("\""+obj.chname+"\""+$res_entry("ria.exception.decimalenotlong","\u5c0f\u6570\u4f4d\u4e0d\u80fd\u8d85\u8fc7") +  pric +$res_entry("ria.exception.wei","\u4f4d"));
                return false;
            }
        }

        return true;
    }catch(e){
        alert("in f_checkNumLenPrec = " + e);
        return false;
    }
};


function emailCheck (emailStr,name,el_ctrl)
{
    var checkTLD=1;
    var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
    var emailPat=/^(.+)@(.+)$/;
    var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
    var validChars="\[^\\s" + specialChars + "\]";
    var quotedUser="(\"[^\"]*\")";
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom=validChars + '+';
    var word="(" + atom + "|" + quotedUser + ")";
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
    var matchArray=emailStr.match(emailPat);

    if (matchArray==null)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.invalidatechar","\u65e0\u6548\u5b57\u7b26\u5fc5\u987b\u542b\u6709'@'\u548c'.'");
        else alert("["+name+"]" +$res_entry("ria.exception.invalidatechar2","\u65e0\u6548,\u5fc5\u987b\u542b\u6709'@'\u548c'.'"));
        return false;
    }
    var user=matchArray[1];
    var domain=matchArray[2];

    for (i=0; i<user.length; i++)
    {
        if (user.charCodeAt(i)>127)
        {
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.invalidatechar3","\u542b\u6709\u975e\u6cd5\u5b57\u7b26");
            else alert("["+name+"]" + $res_entry("ria.exception.invalidatechar3","\u542b\u6709\u975e\u6cd5\u5b57\u7b26"));
            return false;
        }
    }
    for (i=0; i<domain.length; i++) {
        if (domain.charCodeAt(i)>127) {
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.invalidatechar3","\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01");
            else alert("["+name+"]" + $res_entry("ria.exception.invalidatechar3","\u542b\u6709\u975e\u6cd5\u5b57\u7b26\uff01") ) ;
            return false;
        }
    }

    if (user.match(userPat)==null)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.nameerror","\u540d\u79f0\u6709\u8bef");
        else alert("["+name+"]" + $res_entry("ria.exception.nameerror","\u540d\u79f0\u6709\u8bef"));
        return false;
    }

    var IPArray=domain.match(ipDomainPat);
    if (IPArray!=null)
    {
        for (var i=1;i<=4;i++)
        {
            if (IPArray[i]>255)
            {
                if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.nameerror","\u540d\u79f0\u6709\u8bef");
                else alert("["+name+"]" +  $res_entry("ria.exception.inputerror","\u8f93\u5165\u6709\u8bef"));
                return false;
            }
        }
        return true;
    }

    var atomPat=new RegExp("^" + atom + "$");
    var domArr=domain.split(".");
    var len=domArr.length;
    for (i=0;i<len;i++)
    {
        if (domArr[i].search(atomPat)==-1)
        { 
            if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.demainerror","\u57df\u540d\u6709\u8bef");
            else alert("["+name+"]" + $res_entry("ria.exception.demainerror2","\u4e2d\u7684\u57df\u540d\u6709\u8bef"));
            return false;
        }
    }

    if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.demainend","\u5fc5\u987b\u4ee5\u4e00\u4e2a\u6709\u7528\u57df\u540d\u7ed3\u675f");
        else alert("["+name+"]" + $res_entry("ria.exception.demainend","\u5fc5\u987b\u4ee5\u4e00\u4e2a\u6709\u7528\u57df\u540d\u7ed3\u675f"));
        return false;
    }

    if (len<2)
    {
        if(isElementDefined(el_ctrl)) el_ctrl.innerText = $res_entry("ria.exception.noinputhost","\u6ca1\u6709\u8f93\u5165\u4e3b\u673a\u5730\u5740");
        else alert("["+name+"]" + $res_entry("ria.exception.noinputhost","\u6ca1\u6709\u8f93\u5165\u4e3b\u673a\u5730\u5740"));
        return false;
    }
    return true;
} ;
window.addOnloadEvent = function(f) {
    // If already loaded, just invoke f() now.
    if (addOnloadEvent.loaded) f();
    // Otherwise, store it for later
    else addOnloadEvent.funcs.push(f);
};
// The array of functions to call when the document loads
addOnloadEvent.funcs = [];
// The functions have not been run yet.
addOnloadEvent.loaded = false;

// Run all registered functions in the order in which they were registered.
// It is safe to call addOnloadEvent.run() more than once: invocations after the
// first do nothing. It is safe for an initialization function to call
// addOnloadEvent() to register another function.
addOnloadEvent.run = function() {
    // If we've already run, do nothing
    if (addOnloadEvent.loaded) return;
    for(var i = 0; i < addOnloadEvent.funcs.length; i++) {
        try { addOnloadEvent.funcs[i](); }
        catch(e) { /* An exception in one function shouldn't stop the rest */ }
    }
    // Remember that we've already run once.
    addOnloadEvent.loaded = true;
    // But don't remember the functions themselves.
    delete addOnloadEvent.funcs;
    // And forget about this function too!
    delete addOnloadEvent.run;
};

// Register addOnloadEvent.run() as the onload event handler for the window
if (window.addEventListener)
    window.addEventListener("load", addOnloadEvent.run, false);
else if (window.attachEvent) window.attachEvent("onload", addOnloadEvent.run);
else window.onload = addOnloadEvent.run;
function $res_entry(){
  var argument_one = arguments[0];
  var argument_two = arguments[1];
  if(argument_two== 'undefine'||argument_two==null ||argument_two=="")
  return "no value";
  else return argument_two;
};


//----------------------- add by Daniel for event manager--------------begin
if(!Array.prototype.push){
    Array.prototype.push=function(elem){
         this[this.length]=elem;
    }
}
var  EventManager={
    _registry: null,
    Initialise: function(){
         if(this._registry==null){
             this._registry=[];
             //  Register the cleanup handler on page unload. 
             EventManager.Add(window,"unload" ,this.CleanUp);
         }
    },
    /* 
     * Registers an event and handler with the manager.
     *
     * @param  obj         Object handler will be attached to.
     * @param  type        Name of event handler responds to.
     * @param  fn          Handler function.
     * @param  useCapture  Use event capture. False by default.
     *                     If you don't understand this, ignore it.
     *
     * @return True if handler registered, else false.
     */ 
    Add: function(obj, type, fn, useCapture){
         this.Initialise();
         //  If a string was passed in, it's an id. 
         if(typeof obj=="string"){
            obj = document.getElementById(obj);
         }
         if(obj==null || fn==null){
            return  false ;
         }
         // Mozilla/W3C listeners? 
         if(obj.addEventListener){
            obj.addEventListener(type, fn, useCapture);
            this._registry.push({obj: obj, type: type, fn: fn, useCapture: useCapture});
            return  true ;
         }
         //  IE-style listeners? 
         if(obj.attachEvent && obj.attachEvent("on" + type,fn)){
            this._registry.push({obj: obj, type: type, fn: fn, useCapture: false });
            return true ;
         }
         return false ;
    },
    /* *
     * Cleans up all the registered event handlers.
     */ 
    CleanUp: function(){
         for(var i=0;i<EventManager._registry.length;i++){
             with(EventManager._registry[i]) {
                 // Mozilla/W3C listeners? 
                 if(obj.removeEventListener) {
                    obj.removeEventListener(type, fn, useCapture);
                 }
                 else if(obj.detachEvent){//  IE-style listeners? 
                    obj.detachEvent("on"+type,fn);
                 }
             }
         }
         //  Kill off the registry itself to get rid of the last remaining 
         //  references. 
         EventManager._registry = null ;
    }
}; 
//----------------------- add by Daniel for event manager--------------end

//constant
var RIA_BIND_TAG = "bind";
var RIA_S_TAG = "s";
var RIA_Z_TAG = "z:";
var RIA_INSTANCE_TAG = "instance";
var RIA_SERVICE_TAG = "service";
var RIA_SETVALUE_TAG = "setvalue";
var RIA_INCLUDE_TAG = "include";//\u65b0\u589einclude\u6807\u7b7e
var RIA_INIT_TAG = "init";
var RIA_REQUEST_TAG = "request";
var RIA_RESPONSE_TAG = "response";
var RIA_FORWARD_TAG = "forward";
var RIA_LOADING_TAG = "loading";
var RIA_ZREQUEST_TAG = "Z:REQUEST";
var RIA_ZRESPONSE_TAG = "Z:RESPONSE";
var RIA_ZFORWARD_TAG = "Z:FORWARD";
var RIA_ZLOADING_TAG = "Z:LOADING";
var RIA_DOT_TAG = ".";
//add include tags properties
var INCLUDE_ID_TAG = "id";
var INCLUDE_URL_TAG = "url";
//loadingtag class properties
var LOADING_ID_TAG ="id";
var LOADING_ISSYN_TAG ="isSyn";
var LOADING_DIVID_TAG = "divId";
var LOADING_IMG_TAG ="img";
var LOADING_DELAY_TAG ="delay";
var LOADING_WIDTH_TAG ="width";
var LOADING_HEIGHT_TAG ="height";

//Bindtag class properties
var BIND_ID_TAG = 'id';

var BIND_CTRL_TAG = 'ctrl';

var BIND_REF_TAG = 'ref';

var BIND_AUTOCHECK_TAG = 'autocheck';

var BIND_DATATYPE_TAG = 'datatype';

var BIND_VALIDATE_TAG = 'validate';

var BIND_CTRLEXT_TAG = 'ctrlext';
var BIND_CALCULATE_TAG = 'calculate';
//RequestTag class properties

var REQUEST_ID_TAG = 'id';

var REQUEST_PARA_TAG = 'para';

var REQUEST_PREREQUEST_TAG = 'prerequest';

var REQUEST_POSTREQUEST_TAG = 'postrequest';

//ResponseTag class properties

var RESPONSE_ID_TAG = 'id';

var RESPONSE_RTN_TAG = 'rtn';

var RESPONSE_PRERESPONSE_TAG = 'preResponse';

var RESPONSE_POSTRESPONSE_TAG = 'postResponse';

//InstanceTag class properties

var INSTANCE_OBJNAME_TAG = 'var';

var INSTANCE_OBJTYPE_TAG = 'type';

//ForwardTag class properties

var FORWARD_ID_TAG = 'id';

var FORWARD_TARGET_TAG = 'target';

var FORWARD_TEST_TAG = 'test';

var FORWARD_ACTION_TAG = 'action';

var FORWARD_ONFORWARD_TAG = 'onforward';

//ServiceTag class properties

var SERVICE_ID_TAG = 'id';

var SERVICE_URL_TAG = 'url';

var SERVICE_ISSYN_TAG = 'isSyn';

//add by Daniel
var SERVICE_Enctype_TAG = 'enctype';
//add by Daniel

var SERVICE_CACHE_TAG = 'cache';

var SERVICE_EXPIRES_TAG = 'expires';

var SERVICE_ONERROR_TAG = 'onError';

var SERVICE_REQUESTOBJ_TAG = 'reqeuestObj';

var SERVICE_RESPONSEOBJ_TAG = 'responseObj';

var SERVICE_FORWARDOBJ_TAG = 'forwardObj';

//InitTag class properties

var INIT_ID_TAG = 'id';

var INIT_CTRLID_TAG = 'ctrl';

var INIT_REF_TAG = 'ref';

var INIT_VALUE_TAG = 'value';
//\u6d88\u606f\u63d0\u793a
var RIA_MSG_LABLE = '_msg';
//\u53c2\u6570\u5b9e\u4f53
var _para = {};

//define loading class
var LoadingTag = function()
{
    this.id='';
    this.isSyn='';
    this.divId='';
    this.img='';
    this.delay='';
    this.width='';
    this.height='';

};

//define BindTag class

var BindTag = function ()
{

    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.autocheck = '';

    this.datatype = '';

    this.validate = '';

    this.ctrlext = '';
    this.calculate = ''; 
};

//define RequestTag class

var RequestTag = function ()
{

    this.id = '';

    this.para = '';

    this.preRequest = '';

    this.postRequest = '';
};

//define ResponseTag class

var ResponseTag = function ()
{

    this.id = "";

    this.rnt = '';

    this.preResponse = '';

    this.postResponse = '';

};

//define  InstanceTag class

var InstanceTag = function ()
{

    this.objName = '';

    this.objType = '';
};

//define ForwardTag class 

var ForwardTag = function ()
{

    this.id = '';

    this.target = '';

    this.test = '';

    this.action = '';

    this.onforward = '';

};

//define ServiceTag class

var ServiceTag = function ()
{
    this.id = '';

    this.method = '';

    this.url = '';
    
    this.isSyn = '';

    // add by Daniel begin 
    this.enctype = '' ;
    //add by Daniel end ;
     
    this.expires = '';
    
    this.cache = '';
     
    this.requestObj = '';

    this.responseObj = '';

    this.forwardObj = '';
    
    this.loadingObj ='';
    
    this.onError = "";
};

//define InitTag class 
var InitTag = function()
{
    this.id = '';

    this.ctrlid = '';

    this.ref = '';

    this.value = '';

};
//define pagecontextCache class
var IncludeTag = function(){
    this.id = '';
    this.url= '';
};
//define PageContextCache class
var PageContextCache = function () {
    PageContextCache.prototype.bindArr = new Array();

    PageContextCache.prototype.instanceArr = new Array();

    PageContextCache.prototype.serviceArr = new Array();

    PageContextCache.prototype.initArr = new Array();
    PageContextCache.prototype.include = new Array();   
};

//define PageContextCache class method getService

PageContextCache.prototype.getService = function(serviceId)
{
    if (serviceId == null || serviceId == "")
        log.info("errorCode is" + RIA_ERROR_CALLSID + " error description:Error encountered when retrieving the service id,please check if there is a corresponding service id declared.");
    var serviceTags = new ServiceTag();
    for (var i = 0; i < PageContextCache.prototype.serviceArr.length; i++) {
        if (PageContextCache.prototype.serviceArr[i].id == serviceId) {
            serviceTags = PageContextCache.prototype.serviceArr[i];
        }
    }
    return serviceTags;
};

//define PageContextCache class method getBind

PageContextCache.prototype.getBind = function(ctrlid)
{
    var bindtags = new BindTag();
    for (var i = 0; i < PageContextCache.prototype.bindArr.length; i++) {
        if (PageContextCache.prototype.bindArr[i].ctrlid == ctrlid) {
            bindtags = PageContextCache.prototype.bindArr[i];
        }
    }

    return bindtags;
};

//define PageContextCache class method getInstance

PageContextCache.prototype.getInstance = function(objName)
{
    var instanceTags = new InstanceTag();
    for (var i = 0; i < PageContextCache.prototype.instanceArr.length; i++) {
        if (PageContextCache.prototype.instanceArr[i].objName == objName) {
            instanceTags = PageContextCache.prototype.instanceArr[i];
        }
    }
    return instanceTags;
};

//define PageContextCache class method getRequest

PageContextCache.prototype.getRequest = function(serviceId)
{
    var requestTags = new RequestTag();
    requestTags = PageContextCache.prototype.getService(serviceId).requestObj;
    return requestTags;
};

//define PageContextCache class method getResponse

PageContextCache.prototype.getResponse = function(serviceId)
{
    var responseTags = new ResponseTag();
    responseTags = PageContextCache.prototype.getService(serviceId).responseObj;
    return responseTags;
};
//define PageContextCache class method getForward

PageContextCache.prototype.getForward = function(serviceId)
{
    var forwardTags = new Array();
    forwardTags = PageContextCache.prototype.getService(serviceId).forwardObj;
    return forwardTags;
};
//define PageContextCache class method getloading
PageContextCache.prototype.getLoading = function(serviceId)
{
    var loadingTags = new Array();
    loadingTags = PageContextCache.prototype.getService(serviceId).loadingObj;
    return loadingTags;
};
//define pageContextCache class menthod getUrl
PageContextCache.prototype.getUrl = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceUrl = serviceTag.url;
    return serviceUrl;
};

//define pageContextCache class menthod getIsSyn
PageContextCache.prototype.getIsSyn = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceIsSyn = serviceTag.isSyn;
    
    //\u8fd4\u56de\u662f\u5426\u5f02\u6b65
    return serviceIsSyn;
};

//\u83b7\u53d6\u670d\u52a1\u6807\u7b7e\u7c7b\u578b
PageContextCache.prototype.getEnctype = function(sid)
{
    var serviceTag = new ServiceTag();
    serviceTag = PageContextCache.prototype.getService(sid);
    var serviceEnctype = serviceTag.enctype;
    return serviceEnctype;
};

var newinputEvent = function(input) { 
   return function test(){
    input.value = document.getElementById(input.ctrlid).value;
    //add by tianyingquan 2008-8-28
    input.id = input.ctrlid;
    var verifybl = verifyInput(input,input.datatype);
    var ria_div_obj = document.getElementById(input.ctrlid+RIA_MSG_LABLE);
    if(verifybl &&ria_div_obj && ria_div_obj!=null && ria_div_obj!="") {
      var intext = ria_div_obj.innerText;
      if(intext!=""||intext!=null)  ria_div_obj.innerText ="";
    }
    //end 
   }
};
var getBindObjects = function(){
    var binds = getTagValue(RIA_BIND_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < binds.length; i++) {       
        //2008-6-27 modify by tianyingquan
        //var singleBinds = binds[i];     
        //for(var j =0;j<singleBinds.childNodes.length;j++){
        //if(singleBinds.childNodes[j].tagName == RIA_BIND_TAG){
        //var tmp1 = encapsBindTag(singleBinds.childNodes[j]);
        var tmp1 = encapsBindTag(binds[i])
        if (tmp1.autocheck || tmp1.autocheck == "true")
        {
            //datatype \u4e0d\u80fd\u4e3a\u7a7a
            if (tmp1.datatype != null && tmp1.datatype != "" && document.getElementById(tmp1.ctrlid)) {
                var input = {};
                input.datatype = tmp1.datatype;
                input.name = document.getElementById(tmp1.ctrlid).name;
                input.ctrlid =tmp1.ctrlid;
                if(isIEorFF()){
                  document.getElementById(tmp1.ctrlid).attachEvent("onblur",newinputEvent(input));
                }else{
                 document.getElementById(tmp1.ctrlid).addEventListener("blur",newinputEvent(input),false);
                }
            }

        }
          pageContextCache.bindArr.push(tmp1);
     //   }
     //  }
     //end
    }
};
var pageContextCache = new PageContextCache();
//\u5b9a\u4e49\u91cd\u590d\u63d0\u4ea4\u6309\u94ae\u7684\u7f13\u5b58
var cacheMultiUrl = new Array();
var riatags_init = function () {
    //-------includeTag------------------------
    var include = getTagValue(RIA_INCLUDE_TAG,"");
        for(var i = 0;i<include.length;i++){
           pageContextCache.include.push(encapsIncludeTag(include[i],i));
        }   
    //------bindTag----------------------------------
    //var bindtag = new BindTag();
    getBindObjects();
    
//-------instanceTag----------------------------------------
    var instances = getTagValue(RIA_INSTANCE_TAG, RIA_S_TAG);
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < instances.length; i++) {
        pageContextCache.instanceArr.push(encapsInstanceTag(instances[i]));
    }

    
//---------ServiceTag---------------------------------------
    var services = getTagValue(RIA_SERVICE_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < services.length; i++) {
        pageContextCache.serviceArr.push(encapsServiceTag(services[i]));
    }

//-------initTag----------------------------

    //var initTag = new InitTag();
    var inits = getTagValue(RIA_SETVALUE_TAG, "");
    
    //\u5c01\u88c5\u5230Array
    for (var i = 0; i < inits.length; i++) {
        pageContextCache.initArr.push(encapsInitTag(inits[i]));
    }
    
    //_para\u5bf9\u8c61
    creatParaBean();
    
    //\u521d\u59cb\u5316init\u6807\u7b7e
    setValues();

    return pageContextCache;
};

var encapsBindTag = function (node) {
    var newbindtag = new BindTag();
    newbindtag.id = node.getAttribute(BIND_ID_TAG);
    newbindtag.ctrlid = node.getAttribute(BIND_CTRL_TAG);
    newbindtag.ref = node.getAttribute(BIND_REF_TAG);
    newbindtag.autocheck = node.getAttribute(BIND_AUTOCHECK_TAG);
    newbindtag.datatype = node.getAttribute(BIND_DATATYPE_TAG);
    newbindtag.validate = node.getAttribute(BIND_VALIDATE_TAG);
    newbindtag.ctrlext = node.getAttribute(BIND_CTRLEXT_TAG);
    newbindtag.calculate = node.getAttribute(BIND_CALCULATE_TAG);
    return newbindtag;
};

var encapsInstanceTag = function (node) {
    var newinstanceTag = new InstanceTag();
    newinstanceTag.objName = node.getAttribute(INSTANCE_OBJNAME_TAG);
    newinstanceTag.objType = node.getAttribute(INSTANCE_OBJTYPE_TAG);
    return newinstanceTag;
};

var encapsInitTag = function (node)
{
    var newinitTag = new InitTag();
    newinitTag.id = node.getAttribute(INIT_ID_TAG);
    newinitTag.ctrlid = node.getAttribute(INIT_CTRLID_TAG);
    newinitTag.ref = node.getAttribute(INIT_REF_TAG);
    newinitTag.value = node.getAttribute(INIT_VALUE_TAG);
    return newinitTag;
};
//\u5c01\u88c5include tag
var encapsIncludeTag = function (node,count){
   try{
       var newincludeTag = new IncludeTag();
       newincludeTag.id  = node.getAttribute(INCLUDE_ID_TAG);
       newincludeTag.url = node.getAttribute(INCLUDE_URL_TAG);
       if(newincludeTag.url!=null ||newincludeTag.url!="") {createDivTag(node,newincludeTag.url,count);}
       /*
       document.body.onunload =function(){
          cleanDivObject(count);
       };*/
       return newincludeTag;
   }
   catch(e){
      alert($res_entry('ria.riatags.exinformation','\u9519\u8bef\u4fe1\u606f\u63cf\u8ff0===')+e.description);
   }
};
//\u83b7\u53d6html\u6587\u4ef6\u7684\u5b57\u7b26\u4e32
var getHtmlString = function (URL){
    var sync = true;
    var xmlhttp_include = new FactoryXMLHttpRequest();
    xmlhttp_include.open("GET",URL, !sync);
    var tempstring;
    function includeHtmlBack(){
        if(xmlhttp_include.readyState == 4){
              if(xmlhttp_include.status == 200){ 
                 var xmlString = xmlhttp_include.responseText;
                 xmlhttp_include = null;
                 tempstring = xmlString;
              }               
          }                 
        };  
        xmlhttp_include.send(null);
        if(!sync)xmlhttp_include.onreadystatechange = includeHtmlBack;
        else includeHtmlBack();
        if(tempstring) return tempstring;
};
//\u521b\u5efadiv\u5bf9\u8c61
var createDivTag = function (node,url,count){
     var newdiv = document.createElement("div");
     newdiv.id="divid"+count;
     var stringhtml = getHtmlString(url);
     newdiv.innerHTML =stringhtml;
     $(node).after(newdiv);
     //document.body.appendChild(newdiv);
     //newdiv.style.visibility = "hidden";
};
//\u6e05\u9664div\u5bf9\u8c61
var cleanDivObject = function (count){
    var tempdivid = 'divid'+count;
    if(document.getElementById(tempdivid)){ 
    var divobj = document.getElementById(tempdivid);
    if(divobj&&(divobj!=null||divobj!="")){document.body.removeChild(divobj);}
    if(divobj){
      alert(" div object is exit");
    }
 }
};

var encapsServiceTag = function (node)
{
    var newserviceTag = new ServiceTag();
    newserviceTag.id = node.getAttribute(SERVICE_ID_TAG);
    newserviceTag.url = node.getAttribute(SERVICE_URL_TAG);
    newserviceTag.isSyn = node.getAttribute(SERVICE_ISSYN_TAG);

    //add by Daniel begin
    newserviceTag.enctype = node.getAttribute(SERVICE_Enctype_TAG);
    //add by Daniel end

    newserviceTag.onError = node.getAttribute(SERVICE_ONERROR_TAG);
    //\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
    newserviceTag.cache = node.getAttribute(SERVICE_CACHE_TAG);
    newserviceTag.expires = node.getAttribute(SERVICE_EXPIRES_TAG);
    //\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f 
    if (isIEorFF())
    {
        var forwards = new Array();
        for (var i = 0; i < node.childNodes.length; i++) {
            //request\u5bf9\u8c61
            if (node.childNodes[i].tagName == RIA_REQUEST_TAG)
            {
                newserviceTag.requestObj = encapsRequestTag(node.childNodes[i]);
            }
            //response\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_RESPONSE_TAG)
            {
                newserviceTag.responseObj = encapsResponseTag(node.childNodes[i]);
            }
            //forward\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_FORWARD_TAG)
            {
                forwards.push(encapsForwardTag(node.childNodes[i]));
            }
            //loading\u5bf9\u8c61
            else if (node.childNodes[i].tagName == RIA_LOADING_TAG)
            {
                newserviceTag.loadingObj=encapsLoadingTag(node.childNodes[i]);
            }
        }
        newserviceTag.forwardObj = forwards;
    }

    else {
        var forwords = new Array();
        while (true) {
            //\u5224\u65ad\u662f\u5426\u5b58\u5728\u5bf9\u8c61
            if (node.childNodes[1] == undefined) {
                break;
            } else {
                node = node.childNodes[1];
                
                //request\u5bf9\u8c61
                if (node.tagName == RIA_ZREQUEST_TAG)
                {
                    newserviceTag.requestObj = encapsRequestTag(node);
                }
                //response\u5bf9\u8c61
                else if (node.tagName == RIA_ZRESPONSE_TAG)
                {
                    newserviceTag.responseObj = encapsResponseTag(node);
                }
                //forward\u5bf9\u8c61
                else if (node.tagName == RIA_ZFORWARD_TAG)
                {
                    forwords.push(encapsForwardTag(node));
                }
                //loading\u5bf9\u8c61
                else if (node.tagName == RIA_ZLOADING_TAG)
                {
                    newserviceTag.loadingObj=encapsLoadingTag(node);
                }
            }
        }
        newserviceTag.forwardObj = forwords;
    }

    return newserviceTag;
};

var encapsRequestTag = function (node) {
    var newrequestTag = new RequestTag();
    newrequestTag.id = node.getAttribute(REQUEST_ID_TAG);
    newrequestTag.para = node.getAttribute(REQUEST_PARA_TAG);
    newrequestTag.preRequest = node.getAttribute(REQUEST_PREREQUEST_TAG);
    newrequestTag.postRequest = node.getAttribute(REQUEST_POSTREQUEST_TAG);

    return newrequestTag;
};
var encapsLoadingTag = function (node){
    var newloadingTag = new LoadingTag();
    newloadingTag.id = node.getAttribute(LOADING_ID_TAG);
    newloadingTag.isSyn = node.getAttribute(LOADING_ISSYN_TAG);
    newloadingTag.img = node.getAttribute(LOADING_IMG_TAG);
    newloadingTag.delay = node.getAttribute(LOADING_DELAY_TAG);
    newloadingTag.divId = node.getAttribute(LOADING_DIVID_TAG);
    newloadingTag.height = node.getAttribute(LOADING_HEIGHT_TAG);
    newloadingTag.width = node.getAttribute(LOADING_WIDTH_TAG);
    return newloadingTag;
};

var encapsResponseTag = function (node) {
    var newresponseTag = new ResponseTag();
    newresponseTag.id = node.getAttribute(RESPONSE_ID_TAG);
    newresponseTag.rtn = node.getAttribute(RESPONSE_RTN_TAG);
    newresponseTag.preResponse = node.getAttribute(RESPONSE_PRERESPONSE_TAG);
    newresponseTag.postResponse = node.getAttribute(RESPONSE_POSTRESPONSE_TAG);

    return newresponseTag;
};

var encapsForwardTag = function (node) {
    var newforwardTag = new ForwardTag();
    newforwardTag.id = node.getAttribute(FORWARD_ID_TAG);
    newforwardTag.target = node.getAttribute(FORWARD_TARGET_TAG);
    newforwardTag.test = node.getAttribute(FORWARD_TEST_TAG);
    newforwardTag.action = node.getAttribute(FORWARD_ACTION_TAG);
    newforwardTag.onforward = node.getAttribute(FORWARD_ONFORWARD_TAG);

    return newforwardTag;
};

var getTagValue = function (node, str) {
    try {
        if (isIEorFF())
        {
            return getIETagValue(node + str);
        }
        else {
            return getFFTagVlaue(RIA_Z_TAG + node);
        }
    }
    catch(e) {
        var tempException = new RIAException("",$res_entry('ria.riatags.exOwnerctrl','\u4e0d\u540c\u6d4f\u89c8\u5668\u83b7\u53d6\u81ea\u5b9a\u4e49\u6807\u7b7e\u5f02\u5e38'),e.description);
        commonException("ria",tempException);    
        //log.info("errorcode is:" + RIA_ERROR_SUPPORT_BROWSER + "errordescription is:" + e.message);
    }
};

var getIETagValue = function (node) {

    if (document.getElementsByTagName(node)[0] == undefined)
    {
        return new Array();
    }
    var ieNode = document.getElementsByTagName(node)[0].children;
    if (node == RIA_SERVICE_TAG || node == RIA_SETVALUE_TAG ||node ==RIA_INCLUDE_TAG||node == RIA_BIND_TAG) {
        ieNode = document.getElementsByTagName(node);
    }
    return ieNode;
};

var getFFTagVlaue = function (node) {

    var length = document.getElementsByTagName(node).length;
    var foxNode = new Array(length);
    for (var i = 0; i < length; i++) {
        foxNode[i] = document.getElementsByTagName(node).item(i);
    }
    return foxNode;
};

var isIEorFF = function () {
    if (navigator.appName == "Microsoft Internet Explorer")
    {
        return true;
    }
    return false;
};

var setValues = function ()
{
    var initArr = pageContextCache.initArr;

    var initArr_index = initArr.length;
    if (initArr_index > 0)
    {
        for (var i = 0; i < initArr_index; i++)
        {
            var initTag = initArr[i];
            var init_ctrlid = initTag.ctrlid;
            var init_value = initTag.value;
            var init_ref = initTag.ref;
            if (init_ctrlid == null || init_ctrlid == undefined || init_ctrlid == "")
            {
                continue;
            }
            if (init_value)
            {
               BindDataToCtrl(init_ctrlid, init_value);
               // document.getElementById(init_ctrlid).value = init_value;
            }
            else if (init_ref)
            {
                var init_ref_index = init_ref.indexOf(RIA_DOT_TAG);
                var para_value = _para[init_ref.substring(init_ref_index + 1)];
                //document.getElementById(init_ctrlid).value = para_value == undefined ? "" : para_value;
                para_value = para_value == undefined ? "" : para_value;
                BindDataToCtrl(init_ctrlid, para_value);
            }
        }
    }
};

var getRequestParaUrl = function ()
{
    //\u83b7\u53d6url
    //var url = decodeURI(document.URL);
    var url = decodeURI(window.location.href);
    return url;

};

var paraValue = function ()
{
    var str = new String();
    str = getRequestParaUrl();
    var i = 0;
    if (str != "")
    {
        i = str.indexOf("?");
        i = i + 1;
        str = str.substring(i, str.length);
        var arr = str.split("&");
        return arr;
    }
};

var creatParaBean = function ()
{
    try {
        var value = paraValue();
        if (value != "")
        {
            for (var i = 0; i < value.length; i++)
            {
                var name = value[i].split("=");
                _para[name[0]] = name[1];
            }
        }
    } catch(e)
    {
          var tempException = new RIAException("",$res_entry('ria.riatags.exInstanceCtrl','\u521d\u59cb\u5316\u6807\u7b7e\u5f02\u5e38'),e.description);
          commonException("ria",tempException);
        //log.info("errorcode is " + RIA_ERROR_ENCAPSULATION_URL + "errordescription is" + e.message);
    }
};
//\u521d\u59cb\u5316\u6240\u6709\u6570\u636e
try{
  addOnloadEvent(riatags_init);
}catch(e){
     var tempException = new RIAException("",$res_entry('ria.riatags.exInstanceCtrlRia','Ria\u521d\u59cb\u5316\u6807\u7b7e\u5f02\u5e38'),e.description);
     commonException("ria",tempException);
}


var FactoryXMLHttpRequest = function () {   
    if( window.ActiveXObject) {
        var msxmls = new Array(
            'Msxml2.XMLHTTP.5.0',
            'Msxml2.XMLHTTP.4.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP');
        for (var i = 0; i < msxmls.length; i++) {
            try {
                return new ActiveXObject(msxmls[i]);
            } catch (e) {
            // modify by Daniel begin
            //   var tempException = new RIAException("",constantRia.RIA_HTTP_INIT,e.message);
            //   commonException(constantRia.RIA_HTTP,tempException);            
            // modify by Daniel end

            }
        }
    }
	if( window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    throw new Error($res_entry('ria.factory.instance','Could not instantiate factory'));
};
var OrganizeData = function(inParas) {
    this.inParas = inParas;
    this.pageContext = pageContextCache;
    //this.processorFactory = new ProcessorFactory();
};
OrganizeData.prototype.buildData = function(sid,$M)
{
    var jsonvalue = this.parseParas(sid,$M);
    if(jsonvalue === false) return false;
    var requestBySid = this.pageContext.getRequest(sid);
    var preRequestMethod = requestBySid.preRequest; 
    //\u4e8b\u4ef6\u8bf7\u6c42\u524d\u65b9\u6cd5
    if(requestBySid && preRequestMethod && preRequestMethod.trim())
    {
        var requestevent = parseValueById(preRequestMethod);
        evalEvent($M,requestevent);
    } 
    jsonvalue = JSON.stringify(jsonvalue);
    return jsonvalue;
};

OrganizeData.prototype.getInfoByCtrlId = function(ctrlId)
{

};
function isIncludeSymbol(requestPara,organizeData,$M)
{
    var inparasValue;
    var group;
    var typePara = typeof(requestPara);
   if(typePara =="string")
    {
        if (requestPara.indexOf("[") == -1 && requestPara.indexOf("]") == -1)
        {
            inparasValue = "[" + requestPara + "]";
        }
        if (requestPara.indexOf("[") != -1 && requestPara.indexOf("]") != -1)
        {
            inparasValue = requestPara;
        }
        group = organizeData.splitParaValue(inparasValue, organizeData, group,$M);  
        if(group === false) return false;
        return group;       
    }
    /*else if(requestPara instanceof Array)
    {
        var temp = new Array();
        for(var i=0;i<requestPara.length;i++){
          var requestM = requestPara[i];
          if(typeof(requestM)=='string'&&requestM.indexOf("$M.")!=-1) {
              var tempobjpara = organizeData.splitParaValue(requestM,organizeData,group,$M);
              if(tempobjpara === false) return false;
              requestPara[i] =tempobjpara[0] ;
          }
        }
        temp[0] = requestPara;
        return temp;    
    }*/
    else{
        var objArr = new Array();
        objArr[0] = requestPara;
        group = objArr;
        return group;       
    }
};

OrganizeData.prototype.parseParas = function(sid,$M)
{
    var group;
    var organizeData = this;
    if (organizeData.inParas != null && organizeData.inParas != "")
    {       
        group = isIncludeSymbol(organizeData.inParas,organizeData,$M);
        if (group === false) return false;
        return group;
    }
    else{
        var requestBySid = organizeData.pageContext.getRequest(sid);
        //\u83b7\u53d6request\u6807\u7b7e
        if (requestBySid != null && requestBySid != "" && (this.inParas == null || this.inParas == ""))
        {
            var requestPara = requestBySid.para;
            //\u83b7\u53d6request\u6807\u7b7e\u4e0b\u9762\u7684para\u53c2\u6570
            if (requestPara != null && requestPara != "")
            {
              group = isIncludeSymbol(requestPara,organizeData,$M);
              if(group === false) return false;
              return group;
            }else
            {
                return new Array();
            }
            
        }else
        {
            return new Array();
        }
         
    }
   
};

OrganizeData.prototype.invokeFunction = function(elementId)
{
    var result = {};
    try {
        result = GetDataFromCtrl(elementId);
    } catch(e)
    {
       var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
       commonException(constantRia.RIA_RIA,tempException);          
      return;
    }
    return result;
};
OrganizeData.prototype.baseTypeValue = function (requestPara){
    var arrobj = new Array();
    arrobj[0] = requestPara;
    para =  arrobj;
    return para;
};

OrganizeData.prototype.splitParaValue = function(requestPara, organizeData, group,$M)
{
    if(requestPara == null)
    {
        return new Array();
    }
    var group = new Object();
    var splitPara;
    var para;
    //call url\u7684\u65f6\u5019\u83b7\u53d6\u53c2\u6570\u7684\u7c7b\u578b\uff0c$m.obj.name 
    //"number," "string," "boolean," "object," "function," \u548c "undefined."
    var typePara = typeof(requestPara); 
    switch(typePara)
    {
        case "string" :
            var subPara; 
            if(requestPara.indexOf("[")!=-1&&requestPara.indexOf("]")!=-1)
            {
                var paraStart = requestPara.indexOf("[");
                var paraEnd = requestPara.indexOf("]");
                subPara = requestPara.substring(paraStart + 1, paraEnd);
                splitPara = subPara.split(",");                 
            }
            else
            {
                splitPara = requestPara.split(",");
            }
            break;
        case "object" ://\u5982\u679c\u662f\u6570\u7ec4\u5f62\u5f0f\u7684\u4e5f\u5f53\u4f5cobject\u6765\u5904\u7406
            return this.baseTypeValue(requestPara);
        case "number":
            return this.baseTypeValue(requestPara);
        case "boolean"://false\u30010\u3001null\u3001 NaN
            return this.baseTypeValue(requestPara);
        case "undefined":
            return new Array();                 
    }   
    para = new Array(splitPara.length);
    // modify by tianyingquan
    ///var stortBindValue = "";
    ///var f = 0;
    //end 
    //\u5206\u79bb\u591a\u4e2a\u53c2\u6570\u7684\u5f62\u5f0f 
    for (var i = 0; i < splitPara.length; i++)
    {
        var singlePara = splitPara[i];
        var j = singlePara.indexOf("$");
        //\u5982\u679c\u5e26\u6709$\u7b26\u53f7
         //\u5904\u7406\u5bf9\u8c61\u6a21\u578b\uff0c\u5982 $M.obj
        if (j != -1)
        {
            //\u5982\u679c\u6709\u70b9\u53f7
            if (singlePara.indexOf(".") != -1)
            {
                var requestModel = singlePara.substring(singlePara.indexOf(".") + 1, singlePara.length);
                if (requestModel != null && requestModel != "")
                {
                    var requestModelArr = requestModel.split(".");
                    var modelArrLength = requestModelArr.length;
                    requestModelArr[modelArrLength - 1] = new Object();
                    group = requestModelArr[modelArrLength - 1];
                    var bindObject = new Object();
                    //\u5faa\u73afbinds\u6807\u7b7e\u91cc\u9762\u7684ref\u5c5e\u6027\uff0c\u53d6\u51fa\u4e0erequestModel\u503c\u76f8\u540c\u7684\u503c                              
                    var bindTagArr = organizeData.pageContext.bindArr;
                    var d = 0;
                    //var arrAllBindRef = [];
                    //var arrAllBindCtrl = [];
                    //\u5c01\u88c5\u6240\u6709\u5b58\u5728requestModel \u4e2d\u7684bind\u6807\u7b7e
                    var bindTempArr = [];
                    for (var k = 0; k < bindTagArr.length; k++)
                    {
                        var bindRef = bindTagArr[k].ref;
                        var bindCtrl = bindTagArr[k].ctrlid;
                        var bindId = bindTagArr[k].id;
                        var bindAutoCheck = bindTagArr[k].autocheck;
                        var bindDatatype = bindTagArr[k].datatype;
                        var bindValidate = bindTagArr[k].validate;
                        var bindCtrlext = bindTagArr[k].ctrlext;
                        var regexp = new RegExp("^" + requestModel + "[.]");
                        if (regexp.test(bindRef) || bindRef == requestModel)
                        {
                            //1.\u628a\u5728modelRequest\u91cc\u9762\u7684\u503c\u53d6\u51fa\u6765\u653e\u5728\u4e00\u4e2a\u6570\u7ec4\u91cc\u9762
                            //arrAllBindRef[d] = bindRef.replace(requestModel+".","");
                            //arrAllBindCtrl[d] = bindCtrl;
                            var newBind = new BindTag();
                            newBind.ref = bindRef.replace(requestModel + ".", "");
                            newBind.ctrlid = bindCtrl;
                            newBind.datatype = bindDatatype;
                            newBind.autocheck = bindAutoCheck;
                            newBind.ctrlext = bindCtrlext;
                            newBind.id = bindId;
                            newBind.validate = bindValidate;
                            bindTempArr[d] = newBind;
                            d++;
                        }
                    }
                    //2.\u5728\u53d6\u51fa\u6570\u7ec4\u91cc\u9762\u67d0\u4e2a\u5177\u4f53\u7684\u503c\u8fdb\u884c\u5904\u7406
                    for (var q = 0; q < bindTempArr.length; q++)
                    {
                        var singleBindRefArr = (bindTempArr[q].ref).split(".");
                        var singleBindRefCtrl = bindTempArr[q].ctrlid;
                        if (singleBindRefCtrl != null && singleBindRefCtrl != "") {
                            if (singleBindRefArr.length >= 1 && bindTempArr[q].ref == requestModel)
                            {
                                var ctrlResult = "";
                                //ui\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728
                                if(document.getElementById(singleBindRefCtrl) != null) ctrlResult =this.invokeFunction(singleBindRefCtrl);
                                //\u5b9a\u4e49\u6587\u672c\u6846\u7684\u540d\u79f0                             
                                var bind_type = bindTempArr[q].datatype;
                                var setValue_obj = document.getElementById(singleBindRefCtrl+RIA_MSG_LABLE);                              
                                //\u5148\u6267\u884c\u6846\u67b6\u7684\u7c7b\u578b
                                if(bind_type!= null && bind_type!= ""&&document.getElementById(singleBindRefCtrl) != null&&bind_type!="NullString")
                                {
                                    var inputname = document.getElementById(singleBindRefCtrl).name;
                                    var ble = validateDataType(ctrlResult, bindTempArr[q], inputname,singleBindRefCtrl); 
                                    if (!ble)
                                    {
                                        try{
                                           document.getElementById(singleBindRefCtrl).focus();
                                           return false; 
                                        }catch(e){
                                           return false;
                                        }                                                                          
                                    }
                                    //add by tianyingquan2008-8-28 RIA_MSG_LABLE
                                    else if(setValue_obj && setValue_obj!=null && setValue_obj!=""){
                                       var innervalue = setValue_obj.innerText;
                                       if(innervalue!="" || innervalue!= null) setValue_obj.innerText ="";
                                    }                                     
                                    //end                                   
                                }
                                var validatefunction = bindTempArr[q].validate;
                                //\u540e\u6267\u884c\u7528\u6237\u81ea\u5df1\u7684\u6821\u9a8c\u51fd\u6570
                                if (validatefunction != null && validatefunction != "") {
                                   var blvli = eval(validatefunction);
                                   if(!blvli) return false; 
                                }                               
                                if(bind_type!=null && bind_type.indexOf("date")!=-1) bindObject = riaDate2UTC(bind_type,ctrlResult); 
                                else bindObject = ctrlResult;
                                break;
                            }
                           var rtnValue= this.getObject(bindObject, singleBindRefArr, 1, singleBindRefCtrl, bindTempArr[q]);
                           if(rtnValue === false) return false;
                        }
                    }
                    $M[requestModel] = bindObject;
                    para[i] = group = bindObject;
                }
            }
        }
     else 
        {
            para[i] = splitEvalFunction(singlePara);
        }
    }
    return     para;
};
function splitEvalFunction(singlePara){
    var evalparatype = typeof(singlePara);  
    if(evalparatype =='string')
    {
        var nn = new RegExp("\^[0-9]");
        if(nn.test(singlePara))
        {
            return singlePara;
        }else{
            try{
                var evalPara = eval(singlePara);
				//if(evalPara == undefined) //\u65e7\u7684\u5199\u6cd5\uff0c\u5f53singlePara=null\u65f6\u4f1a\u5f15\u8d77\u5224\u65ad\u9519\u8bef
                if(evalPara === undefined)
                {
                    return singlePara;
                }               
                else
                {
                    return evalPara;
                }
            }
            catch(e){
                return singlePara;
            }
        }       
    }
     else
    {
        var evalPara = eval(singlePara);                
        return evalPara;
    }
};
var validateDataType = function (ctrlResult, bindTemp, inputname,ctrlid)
{
    var datatype = bindTemp.datatype;
    var input = {};
    input.datatype = datatype;
    input.value = ctrlResult;
    input.name = inputname;
    input.id = ctrlid;
    return verifyInput(input,datatype);
};
function validateTypeOfDate(str)
{
  //var regexp = new RegExp(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])$/);
   var regexp = new RegExp(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2]\d|3[0-1])(\s?([0-1]?[0-9]|2[0-3]):([0-5]?[0-9]):([0-5]?[0-9]))?$/);
  if(regexp.test(str))
  {
      return true;
  }
  else
  {
      return false;
  } 
};
var  riaDate2UTC = function(format, time){
    var format = format.substring(6,format.length-2);
    if(time == null || time == '')
    {
        return null;
    }
    var utc = "";
    var yy = "-";
    var mm = "-";
    var date_format = new Array();
    var date_format1 = "";
    var date_format2 = "";
    var time_arr = new Array();
    var time1 = time;
    var time2 = "";
    if(format != null)
    {
        if(format.length > 10)
        {
            date_format = format.split(" ");
            date_format1 = date_format[0];
            date_format2 = date_format[1];
            
            time_arr = time.split(" ");
            time1 = time_arr[0];
            time2 = time_arr[1];
        }else{
            date_format1 = format;
            time1 = time;
        }
        var y_index = date_format1.indexOf("y");
        var y_endindex = date_format1.lastIndexOf("y");
        yy = date_format1.substring(y_endindex+1,y_endindex+2); 

        var m_index = date_format1.indexOf("m");
        var m_endindex = date_format1.lastIndexOf("m");
        mm = date_format1.substring(m_endindex+1, m_endindex+2);
    }
    var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
    var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);

    time1 = time1.replace(YM,',');
    time1 = time1.replace(MD,',');
    var tt = time1.split(",");
  
    if(time2.length < 1){
        var date = new Date(tt[0],parseInt(tt[1]-1),tt[2]);
        utc = date.getTime();
    }else{
        var HH = time2.split(":");
         var date = new Date(tt[0],parseInt(tt[1]-1),tt[2],HH[0],HH[1],HH[2]);
         utc = date.getTime();
    }
    return utc;
  };
OrganizeData.prototype.getObject = function(obj, propertyPath, level, ctrlid, bindtemp)
{
    if (level == propertyPath.length)
    {
        var ctrlResult ="";
        //\u63a7\u4ef6\u5728\u9875\u9762\u5b58\u5728\u7684\u60c5\u51b5
        if(document.getElementById(ctrlid)!=null) ctrlResult=this.invokeFunction(ctrlid);
        
        if(typeof(ctrlResult)=='string')
                ctrlResult=ctrlResult.trim();  
        var bind_type = bindtemp.datatype;
        var div_obj = document.getElementById(ctrlid+RIA_MSG_LABLE);                    
        //\u5148\u6267\u884c\u6846\u67b6\u7684\u6821\u9a8c\u7c7b\u578b
        if(bindtemp&&bind_type != null && bind_type != ""&& document.getElementById(ctrlid)!=null&&bind_type!="NullString")
        {
            var inputname = document.getElementById(ctrlid).name;           
            var bl = validateDataType(ctrlResult, bindtemp, inputname,ctrlid);
            if(!bl) 
            {   
                try{
                    document.getElementById(ctrlid).focus();
                    return false;               
                }catch(e){
                    return false;
                }
            }
            //add by tianyingquan 2008-8-28 RIA_MSG_LABLE
            else if(div_obj&& div_obj!=null && div_obj!=""){
               var ctrlE =div_obj.innerText; 
               if(ctrlE!=""||ctrlE!=null) div_obj.innerText ="";
            }       
            //end
            
            if(ctrlResult==null||ctrlResult=="")
            {
                obj[propertyPath[level - 1]] = null;
                return;
            }
            
            if(validateTypeOfDate(ctrlResult))
            {             
               //var year = parseInt(ctrlResult.substring(0,4));
               //var month = parseInt(ctrlResult.substring(5,7));
               //var day = parseInt(ctrlResult.substring(8,10));               
               //obj[propertyPath[level - 1]] = new Date(year,month,day).getTime();
               obj[propertyPath[level - 1]] = riaDate2UTC(bindtemp.datatype,ctrlResult);
               return ;
            }           
        }
        //\u540e\u6267\u884c\u7528\u6237\u81ea\u5b9a\u4e49\u7684\u6821\u9a8c\u51fd\u6570
        if (bindtemp&&bindtemp.validate != null && bindtemp.validate != "") {
          var vabl = eval(bindtemp.validate);
          if(!vabl) return false;
        }       
        obj[propertyPath[level - 1]] = ctrlResult;
        return;
    }
    if (!obj[propertyPath[level - 1]])
    {
        obj[propertyPath[level - 1]] = {};
    }
    var obj_bl = this.getObject(obj[propertyPath[level - 1]], propertyPath, level + 1, ctrlid, bindtemp);
    if(obj_bl === false) return false;
};

var SHOWDATA_OPRST_TAG = "opRst";
var SHOWDATA_RTN_TAG = "rtn";
var SHOWDATA_EXPMODEL_TAG = "expModel";
var SHOWDATA_DOT_TAG = ".";
var ERR_ERRORCODE ="excepCode";
var ERR_ERRORDESC = "excepDesc";
var ERR_LOCATIONID = "locationId";

var CODE_ERR_CONSOLE = "4";
var RIA_NOLOGIN_OPRST = "6";
var RIA_NOPOPE_OPRST = "7";
var CODE_ERR_SIZELIMITEXCEEDED="8";
 
String.prototype.trim = function()
{
    // \u7528\u6b63\u5219\u8868\u8fbe\u5f0f\u5c06\u524d\u540e\u7a7a\u683c
    // \u7528\u7a7a\u5b57\u7b26\u4e32\u66ff\u4ee3\u3002
    return this.replace(/(^\s*)|(\s*$)/g, "");
};
//\u4fee\u6539\u8bb0\u5f551 \u5f00\u59cb
//\u5904\u7406\u591a\u6b21\u63d0\u4ea4\u7f13\u5b58
function addCacheUrl(url)
{   
   var cacheUrl ="";
   //\u904d\u5386\u83b7\u53d6\u6240\u6709\u7684\u7f13\u5b58url 
   /*
    for(cacheUrl in cacheMultiUrl)    {   
        //\u5982\u679c\u5728\u7f13\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728\u4e86
        if(url == cacheUrl)
        {
            return false;
        }                               
    }
	*/
	//2009-11-30 wxr \u4e0a\u9762\u7684\u5faa\u73af\u6709\u95ee\u9898
	for(var i=0; i<cacheMultiUrl.length; i++) {
    	if(url == cacheMultiUrl[i]){
    		return false;
    	}
    }
    //\u5728\u7f13\u5b58\u4e2d\u4e0d\u5b58\u5728\u6dfb\u52a0url
    cacheMultiUrl.push(url);
    return true;
};
//\u79fb\u51fa\u5185\u5b58\u4e2durl\u5730\u5740
function removeCacheUrl(cacheurl)
{
    //\u904d\u5386\u591a\u6b21\u53d6\u5f97url
    for(var i=0;i<cacheMultiUrl.length;i++)
    {
        //\u5982\u679c\u5728\u5185\u5b58\u4e2d\u5df2\u7ecf\u5b58\u5728
        if(cacheMultiUrl[i]==cacheurl)
        {
            cacheMultiUrl.splice(i,1);
        }
    }
};
//\u4fee\u6539\u8bb0\u5f551 \u7ed3\u675f

var CallService_complete = function (status, statusText, responseText, responseHTML) {
    
    log.info("Call Service complete:status=" + status);
    log.info("Call Service complete:statusText=" + statusText);
    log.info("Call Service Complete:responseText=" + responseText);
    
    var outPara = this.outParas;
    var serviceId = this.sid;
    var autoPara = this.autoPara;
    var _object = this._object;
    var onSuccess = this.onSuccess;
    var urlerror = this.urlErrorEvent;
    
    //session time out
    if(status == 1000)
    {
        //var tempUrl = document.referrer;
        //tempUrl = tempUrl.substring(0,tempUrl.lastIndexOf("/"));
        //top.window.location.href=tempUrl + "/login.html";
		var href = document.location.href;
		var tempArray = href.replace("//", "/").split("/");	
        var tempUrl =  tempArray[0] + "//" + tempArray[1] + "/" + tempArray[2] + "/uiloader/forward.html";
        window.open(tempUrl, "_self");
        return;
    }
    // noauthori
    //wanghao add
    if(status == 403) {
        document.body.innerHTML = responseText;
        return;
     }
    //success
    if (status == 200)
    {       
        removeCacheUrl(this.cacheurl);
        //\u4fee\u6539\u8bb0\u5f552 \u5f00\u59cb
        //\u7528\u6237\u5b58\u5728\u56fe\u7247\u8fdb\u5ea6\u6761\u7684\u663e\u793a
        if(serviceId){

            var loadingobj = pageContextCache.getLoading(serviceId);
            //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
            if(loadingobj)
            {
                var issyn = loadingobj.isSyn;
                var delay = loadingobj.delay;               
                //\u4fee\u6539\u8bb0\u5f556 \u5f00\u59cb
                if(delay == ""||delay==null||delay==undefined) delay="500";             
                //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
                if(issyn==null||issyn==""||issyn=="false"||issyn==false)
                {
                    var divid = loadingobj.divId;
                    var numObj = new Number(delay);
                    //\u5ef6\u65f6\u529f\u80fd
                    if(delay)
                    {
                        window.setTimeout("hiddendDivImg('"+divid+"');",parseInt(delay));
                    }else//\u4e0d\u5ef6\u65f6
                    {
                        hiddendDivImg(divid);
                    }
                    
                }else if(issyn==true||issyn=="true")//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
                {
                    //\u5ef6\u65f6\u529f\u80fd
                    if(delay)
                    {
                        window.setTimeout("removeObj();",parseInt(delay));
                    }else//\u4e0d\u5ef6\u65f6
                    {
                        removeObj();
                    }                               
                }
                //\u4fee\u6539\u8bb0\u5f556 \u7ed3\u675f
            }           
        }
        //\u4fee\u6539\u8bb0\u5f552 \u7ed3\u675f
        
        if (!responseText)
        {
            alert($res_entry('ria.complete.data.responsetext','\u670d\u52a1\u7aef\u6ca1\u6709\u6570\u636e\u8fd4\u56de'));
            return;
        }
        var response;
        try{
          response = eval("(" + responseText + ")");  
        }catch(e){
          return;
        }
        //\u4ecejson\u4e2d\u63d0\u53d6\u6570\u636e
        var rtn_value = response[SHOWDATA_RTN_TAG]; 
        //\u4fee\u6539\u8bb0\u5f553 \u5f00\u59cb
        if(serviceId)
        {
            var servicetagid = pageContextCache.getService(serviceId);
            //\u662f\u5426\u9700\u8981cookies\u7f13\u5b58
            //\u4fee\u6539\u8bb0\u5f554 \u5f00\u59cb
            if(servicetagid.cache=="true"||servicetagid.cache==true)
            {
                var expiresvalue = servicetagid.expires;
                
                setCookie(getNameCookie(serviceId),JSON.stringify(rtn_value),expiresvalue);
            }
            //\u4fee\u6539\u8bb0\u5f554 \u7ed3\u675f
        }
        //\u4fee\u6539\u8bb0\u5f553 \u7ed3\u675f    
        //\u8fd4\u56de\u72b6\u6001
        var res_oprst = response[SHOWDATA_OPRST_TAG] + "";
        
        //var rtn_expDesc = response[SHOWDATA_EXPDESC_TAG];
        var res_expModel = response[SHOWDATA_EXPMODEL_TAG];
        switch (res_oprst) {
                case "0":
                    //\u8fd4\u56de\u4fe1\u606f\u663e\u793a
                    response_success(outPara,serviceId,autoPara,_object,onSuccess,rtn_value);                   
                    break;
                case RIA_NOLOGIN_OPRST:
                    //\u6ca1\u6709\u767b\u5f55
                    isLogin(rtn_value);
                    break;
                case RIA_NOPOPE_OPRST:
                    //\u6ca1\u6709\u6743\u9650
                    isPrivilege(rtn_value);
                    break;
                case CODE_ERR_SIZELIMITEXCEEDED:
                    //\u4e0a\u4f20\u8d85\u51fa\u6700\u5927\u503c
                    response_rtn_expDesc(res_oprst,"FileUploadException - "+res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                    log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                    break;
                case CODE_ERR_CONSOLE:
                     log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                     response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                     break; 
                default:
                    //\u8fd4\u56de\u9519\u8bef\u4fe1\u606f
                    log.info("Call Service complete:expcetion desc=" + JSON.stringify(res_expModel));
                    response_rtn_expDesc(res_oprst,res_expModel,serviceId,onSuccess,urlerror,res_oprst);
                    break;
                                    
        };
    }
};

var response_success = function (outPara,serviceId,autoPara,_object,onSuccess,rtn_value)
{

    //\u82e5\u8c03\u7528\u7684\u662fcallMethod
    if (onSuccess)
    {
        if((typeof _object) != "object")
        {
            _object = this;
        }
        onSuccess.call(_object, 0, rtn_value);
        return;
    }
    
    //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var responseTags = serviceId == null ? {} : pageContextCache.getResponse(serviceId);
    
    var resp_rtn = "";
    
    //\u5224\u65ad\u7528\u6237\u662f\u5426\u914d\u7f6eoutparas\u5c5e\u6027
    if (outPara)
    {
        resp_rtn = outPara;
    }//\u82e5\u6ca1\u914d\u7f6e\u5219\u76f4\u63a5\u89e3\u6790response
    else if(responseTags.rtn){
        resp_rtn = responseTags.rtn;
    }
    
    //\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
    
    //\u521b\u5efa\u5bf9\u8c61
    var $M = {};
    $M[rtn_string] = rtn_value;
    
    //\u670d\u52a1\u8bf7\u6c42\u524d\u54cd\u5e94\u7684\u4e8b\u4ef6
    if (responseTags.preResponse && responseTags.preResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var preResponse_event = parseValueById(responseTags.preResponse);
        evalEvent($M,preResponse_event);
    }
                
    //\u81ea\u52a8\u76f4\u63a5\u56de\u9009
    if (autoPara)
    {
        log.info("Call Service Complete:autoPara=" + autoPara);
        log.info("Call Service Complete:resp_rtn=" + resp_rtn);
        log.info("Call Service Complete:rtn_value=" + rtn_value);
        if(!document.getElementById(resp_rtn))
        {
           var respRtn = eval(resp_rtn);
           if(respRtn && resp_rtn &&resp_rtn.indexOf("$")!=-1){
			   setBindObject(resp_rtn,respRtn);
		   }
           else if(respRtn){
              respRtn.rtnValue = $M[rtn_string];
            }           
        }else{
            try{
                BindDataToCtrl(resp_rtn, $M[rtn_string]);
            }
            catch(e){
                var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                commonException(constantRia.RIA_RIA,tempException);
            }
        }
    }
                
    //\u5206\u6790\u914d\u7f6e\u8fd4\u56de\u503c\u4fe1\u606f
    else if (resp_rtn != null)
    {
        //\u8fd4\u56de\u503c\u914d\u7f6e\u82e5\u662f\u5e26$M\u7b26\u53f7
        if ("$" == resp_rtn.substring(0,1))
        {
            //\u5224\u65ad\u4f20\u8fc7\u6765\u7684\u6570\u636e\u662f\u4ec0\u4e48\u7c7b\u578b
            if ((typeof rtn_value) == "object")
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string],$M);

            } else if ((typeof rtn_value) == "string" || (typeof rtn_value) == "boolean" || (typeof rtn_value) =='number')
            {
                parseRtn(pageContextCache, resp_rtn, $M[rtn_string],$M);
            } else if ((typeof rtn_value) == "undefined")
            {
                alert($res_entry('ria.complete.data.undefined','undefined'));
            }
            //\u7c7b\u578b\u5224\u65ad\u7ed3\u675f.

        }//\u82e5\u662f\u63a7\u4ef6ID
        else {
            log.info("Call Service Complete:resp_rtn=" + resp_rtn);
            log.info("Call Service Complete:rtn_value=" + $M[rtn_string]);
            
            try{
                BindDataToCtrl(resp_rtn, $M[rtn_string]);
            }
            catch(e){
                var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                commonException(constantRia.RIA_RIA,tempException);         
            }
        }
    }
    
    //\u670d\u52a1\u8bf7\u6c42\u540e\u54cd\u5e94\u7684\u4e8b\u4ef6      
    if (responseTags.postResponse && responseTags.postResponse.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var postResponse_event = parseValueById(responseTags.postResponse);
        evalEvent($M,postResponse_event);
    }
    //forward\u9875\u9762\u8df3\u8f6c
    getForwards(serviceId,$M);
       
};
function setRiaCtrlvalue(ctrid,value,objvalue){
    if(ctrid!=null&&ctrid !=""&&(objvalue!=undefined||ctrid!=undefined)&&objvalue!=null){
      var bind_object = pageContextCache.getBind(ctrid);
        if(bind_object && bind_object !=null){
        var bind_ref = bind_object.ref;
        var bind_arr = bind_ref.split(".");
        var length_ref = bind_arr.length;
        if(length_ref ==1) return objvalue[bind_ref]=value;
        if(length_ref == 2) return objvalue[bind_arr[0]][bind_arr[1]]=value;
        else if(length_ref >=3){
           var tempvar ={};
           for(var i=0;i<length_ref;i++){
              var strProperty = bind_arr[i];
              tempvar = objvalue[strProperty];
           }
          tempvar = value;
        }
     }
    }
};
var evalEvent = function (m,para)
{ 
    try{
        eval("var $M = arguments[0];");
        eval(para);
    }catch(e){
        var tempException = new RIAException("",e.number,e.description);
        commonException(constantRia.RIA_USER,tempException);    
    }
};

var response_rtn_expDesc = function(errorCode,res_expModel,serviceId,onSuccess,urlerror,res_oprst)
{
    
    if (onSuccess)
    {       
        if((typeof _object) != "object")
        {
            _object = this;
        }
        onSuccess.call(_object, 1, res_expModel);
        return;
    }
    
    //\u670d\u52a1ID\u4fe1\u606f\u5bf9\u8c61
    var serviceTags = serviceId == null ? {} : pageContextCache.getService(serviceId);
    if (serviceTags.onError && serviceTags.onError.trim())
    {
        //\u89e3\u6790\u670d\u52a1\u54cd\u5e94\u4e8b\u4ef6
        var onError_event = parseValueById(serviceTags.onError);
        urlErrorHandel(errorCode,onError_event,res_expModel,res_oprst);
        return;

    }else if(urlerror!=null&&urlerror!=""&&urlerror){
       urlerror(res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
      /// var onError_callUrlevent = parseValueById(urlerror);
       ///urlErrorHandel(errorCode,onError_callUrlevent,res_expModel);
       return;
    }else if((serviceTags.onError==null||serviceTags.onError=="")&&res_expModel){
       var tempException = new RIAException(res_expModel[ERR_LOCATIONID],res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
       commonException(constantRia.RIA_BACKGROUND,tempException,res_oprst);
       //log.error("\u9519\u8bef\u4ee3\u7801:"+res_expModel[ERR_ERRORCODE]+"\u9519\u8bef\u63cf\u8ff0:"+res_expModel[ERR_ERRORDESC]);
       return;
    }

};
var urlErrorHandel = function(errorCode,onError_event,res_expModel,res_oprst){
   var error_code = new Array("3","4","00","01","02","03","10","20","21","22","30","40","60","70");
   for(var i=0;i<error_code.length;i++)
   if(res_oprst == error_code[i]){
      ssb_update_eval(onError_event,res_expModel[ERR_ERRORCODE],res_expModel[ERR_ERRORDESC]);
      return;
   }
   eval(onError_event);
};
var ssb_update_eval = function()
{
    var onError_event = arguments[0];
    if(onError_event.indexOf("(") != -1 && onError_event.indexOf(")") != -1)
    {
        var eventStr = onError_event.substring(onError_event.indexOf("("),onError_event.indexOf(")"));
        if(eventStr.indexOf("errorCode") != -1)
        {
             onError_event = onError_event.replace("errorCode","arguments[1]");
        }
        if(eventStr.indexOf("rtn_expDesc") != -1)
        {
             onError_event = onError_event.replace("rtn_expDesc","arguments[2]");
        }
    }
    
    eval(onError_event);
};

var parseRtn = function (pageContext, resp_rtn, rtn_values,tempobj)
{
    //\u5f97\u5230rtn\u503c
    var rtn_string = resp_rtn.substring(resp_rtn.indexOf(SHOWDATA_DOT_TAG) + 1);
    
    //\u904d\u5386\u6240\u6709bind
    for (var j = 0; j < pageContext.bindArr.length; j++)
    {
        var rtn_value = rtn_values;
        var typeValues = new Array();
        var bind_ref = pageContext.bindArr[j].ref;
        var ctrlid = pageContext.bindArr[j].ctrlid;
        var datatype = pageContext.bindArr[j].datatype;
        var ctrlext = pageContext.bindArr[j].ctrlext;
        var calculate = pageContext.bindArr[j].calculate;

        //\u6bd4\u8f83bind\u7684ref\u548crtn\u503c
        var bindre = new RegExp('^' + rtn_string);
        if (bindre.test(bind_ref))
        {
            var bind_refs = bind_ref.substring(rtn_string.length);
            
            var bind_ref_index = bind_refs.indexOf(SHOWDATA_DOT_TAG);
            var bind_ref_values = new Array();      
                
            var bind_array = /^\[/;
            var bind_arrays = /\]$/;
            if(bind_array.test(bind_refs))
            {
                var array_index = bind_refs.substring(bind_refs.indexOf("[")+1,bind_refs.indexOf("]"));
                rtn_value = rtn_values[parseInt(array_index)];
            }
            
            if (bind_ref_index != -1)
            {
                bind_ref_values = bind_refs.substring(bind_ref_index + 1).split(SHOWDATA_DOT_TAG);
                
                for (var n = 0; n < bind_ref_values.length; n++)
                {
                    //\u4fee\u6539\u8bb0\u5f557 \u5f00\u59cb
                    if(rtn_value == null) return;
                    //\u4fee\u6539\u8bb0\u5f557 \u7ed3\u675f
                    rtn_value = rtn_value[bind_ref_values[n]];
                    
                    //\u82e5rtn_value\u4e2d\u6ca1\u6709\u5c01\u88c5\u6b64\u5bf9\u8c61,\u5219\u76f4\u63a5\u8df3\u51fa\u6b64\u65b9\u6cd5
                    if (rtn_value == undefined)
                    {
                        //error\u65e5\u5fd7
                        //log.error("Call Service Complete:rtn_value1=" + rtn_value);
                        break;
                    }
                }
                log.info("Call Service Complete:ctrlid=" + ctrlid);
                log.info("Call Service Complete:rtn_value=" + rtn_value);
                try{
                    //rtn_value = rtnCalendarValue(datatype,rtn_value);
                    if(calculate != null&&calculate != "") rtn_value=this.rtnCalendarValue(calculate,rtn_value,tempobj);
                    BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext),datatype);
                }
                catch(e){
                      var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                      commonException("ria",tempException);
                }
                
            }else
            {
                if(bind_ref == rtn_string || (bind_array.test(bind_refs)&&bind_arrays.test(bind_refs)))
                {
                    log.info("Call Service Complete:ctrlid=" + ctrlid);
                    log.info("Call Service Complete:rtn_value=" + rtn_value);
                    try{
                        //rtn_value = rtnCalendarValue(datatype,rtn_value);
                        if(calculate != null&&calculate != "") rtn_value = this.rtnCalendarValue(calculate,rtn_value,tempobj);
                        BindDataToCtrl(ctrlid, rtn_value,isctrlext(ctrlext),datatype);
                    }
                    catch(e){
                      var tempException = new RIAException("",constantRia.RIA_EXCE_CTRLDATA,e.message);
                      commonException(constantRia.RIA_RIA,tempException);           
                    }
                }
            }

        }

    }
};

var isctrlext = function (ctrlext)
{
    var obj = {};
    if(ctrlext)
    {
        var ctrlArr = ctrlext.split(";");
        for(var i =0;i<ctrlArr.length;i++)
        {
            var cArr = ctrlArr[i].split(":");
            obj[cArr[0]] = cArr[1];
        }
        return obj;
    }
    else{
        return null;
    }
            
};

var isLogin = function(rtn_url)
{
    openPath(rtn_url);
};

var isPrivilege = function(rtn_url)
{
    openPath(rtn_url);
};

var openPath = function (rtn_url){
    window.open(rtn_url,"_self");
};

function splitStringDate(format,value){
  var year = parseInt(value.substring(0,4));
  var month =value.substring(4,6)-1;
  var day = parseInt(value.substring(6,8));
  var hour = parseInt(value.substring(8,10));
  var minute = parseInt(value.substring(10,12));
  var sec = parseInt(value.substring(12,14));
  if(value.length ==14){
      return new Date(year,month,day,hour,minute,sec);
  }else if (value.length == 8) return new Date(year,month,day);
};
var temp_rtnObject;
function getRiaCtrlValue(ctrlid,tempobj_clone){
  if(tempobj_clone!= undefined && ctrlid == null) {
    temp_rtnObject = tempobj_clone;
    return;
  }
  if(tempobj_clone == undefined && ctrlid != null){
     var bind_object = pageContextCache.getBind(ctrlid);
     if(bind_object && bind_object !=null){
        var bind_ref = bind_object.ref;
        var bind_arr = bind_ref.split(".");
        var length_ref = bind_arr.length;
        if(length_ref ==1) return temp_rtnObject[bind_ref];
        if(length_ref == 2) return temp_rtnObject[bind_arr[0]][bind_arr[1]] ;
        else if(length_ref >=3){
           var tempvar =temp_rtnObject;
           var strProperty;
           for(var i=0;i<length_ref;i++){
              strProperty= bind_arr[i];
              tempvar = tempvar[strProperty];
           }
          return tempvar;
        }
     }
  }  
};
/*
function riaClone(myObj){
  if(typeof(myObj) != 'object') return myObj;
  if(myObj == null) return myObj;
  
  var myNewObj = new Object();
  
  for(var i in myObj)
     myNewObj[i] = clone(myObj[i]);
  
  return myNewObj;
};*/
var ria_objClone=function(orgin){ 
    var obj={}; 
    if(typeof orgin=="object"){ 
        var cb=arguments.callee; 
        if(orgin instanceof Array){ 
            for(var i=0,obj=[],l=orgin.length;i<l;i++){ 
                obj.push(cb(orgin[i])); 
            } 
            return obj; 
        } 
        for(var i in orgin){ 
            obj[i]=cb(orgin[i]); 
        } 
        return obj; 
    } 
    return orgin; 
};
/*
Object.prototype.Clone = function(){
    var objClone;
    if ( this.constructor == Object ) objClone = new this.constructor(); 
    else objClone = new this.constructor(this.valueOf()); 
    for ( var key in this ){
        if ( objClone[key] != this[key] ){ 
            if ( typeof(this[key]) == 'object' ){ 
                 objClone[key] = this[key].Clone();
             }
            else{
                 objClone[key] = this[key];
             }
         }
     }
     objClone.toString = this.toString;
     objClone.valueOf = this.valueOf;
    return objClone; 
};*/

function rtnCalendarValue(datatype,value,tempobj)
  {
    
    if(datatype)
    {
        var type =  datatype.trim();
        var datatypes = type.indexOf("date");
        
        if(datatypes == -1)
        {
            var tempobj_clone = ria_objClone(tempobj);
            this.getRiaCtrlValue(null,tempobj_clone);
            var userMethod = datatype;
            var userValue = eval(userMethod+"()");          
            temp_rtnObject = null;
            return userValue;
        }
        if(value == null || value == "")
        {
            return "";
        }
        var format = "yyyy-MM-dd";
        if(type.indexOf("(") != -1)
        {
            format = type.substring(type.indexOf("(")+1,type.indexOf(")"));
        }
        


        // \u4fee\u6539\u8bb0\u5f555 \u5f00\u59cb
        format = format.toLowerCase();

        // add by Daniel  del from ' or " from "format" 
        var start=/^('|")/;
        var end=/('|")$/;

        if(format!="undefined" && format != null && format != "")
        {
            format=format.replace(start,"");
            format=format.replace(end,"");  
        }
        //------------------------------

        if(format.indexOf("hh24") != -1)
        {
            format = format.replace("hh24","hh");
        }
        // \u4fee\u6539\u8bb0\u5f555 \u7ed3\u675f
        
        if(typeof(value) =="string" && value!=null &&(value.length ==14||value.length ==8)) time_value=splitStringDate(format,value);
        else time_value = new Date(value);
    
        var yTime = rtn_replace(format,"yyyy",time_value.getFullYear()+"");
        
        var MTime = rtn_replace(yTime,"mm",numTimes(parseInt(time_value.getMonth()+1)+""));
        
        var dTime = rtn_replace(MTime,"dd",numTimes(time_value.getDate())+"");
        
        var hTime = rtn_replace(dTime,"hh",time_value.getHours()+"");
        
        var mTime = rtn_replace(hTime,"mi",time_value.getMinutes()+"");
        
        var sTime = rtn_replace(mTime,"ss",time_value.getSeconds()+"");
   
        return sTime;
    }
    return value;
   
  };

var numTimes = function (num)
{
    if(parseInt(num)<10)
    {
        return "0"+num;
    }
    return num;
};

var rtn_replace = function (format,str,value)
{
    if(format.indexOf(str) != -1)
    {
        if(value == 0) value = "00";
        else if(value.length == 1) value = "0"+value;       
        return  format.replace(str,value);
    }
    return format;
};

CallService.prototype.openCallback = CallService_openCallback;
CallService.prototype.loading = CallService_loading;
CallService.prototype.loaded = CallService_loaded;
CallService.prototype.interactive = CallService_interactive;
CallService.prototype.complete = CallService_complete;
CallService.prototype.get = CallService_get;
CallService.prototype.put = CallService_put;
CallService.prototype.del = CallService_delete;
CallService.prototype.post = CallService_post;

CallService.prototype.call = CallService_call;

CallService.prototype.response_success = response_success;
CallService.prototype.response_rtn_expDesc = response_rtn_expDesc;
CallService.prototype.parseRtn = parseRtn;
var DEFAULT_METHOD = "POST";
var DEFAULT_DOTSSM_TAG = ".ssm";
RIAControl = function(serviceId, inParas, outParas, autoPara, obj, fun,$M,cacheurl,urlErrorEvent) {
    this.sid = serviceId;
    this.inParas = inParas;
    this.outParas = outParas;
    this.pageContext = pageContextCache;
    this.organizedata = new OrganizeData(inParas);
    this.callService = new CallService(serviceId, outParas, autoPara, obj, fun,$M,cacheurl,urlErrorEvent);
};

RIAControl.prototype.formatGetUrl = function(url, paras) {
    return url;
};


RIAControl.prototype.callByUrl = function(url, methods, paras) {
    this.process(false, url, methods, paras);
};

RIAControl.prototype.callBySid = function(sid, methods, paras) {
    var url = this.pageContext.getUrl(sid);
    this.process(url, methods, paras);
};

RIAControl.prototype.process = function(url, methods, paras,isSyn) {
    var meth1 = methods || "post";
    var meth = meth1.toUpperCase();
    switch (meth) {
        case 'POST':
            this.callService.post(url, null, null, paras,isSyn);
            break;
        case 'GET':
            var furl = this.formatGetUrl(url, paras);
            this.callService.get(furl);
            break;
        case 'DELETE':
            this.callService.del(url);
            break;
        case 'PUT':
            this.callService.put(url, null, null, paras);
            break;
        default:
            break;
    }

};

RIAControl.prototype.processUpload = function(paraMap,url){
    var id =new Date().getTime();
    var createForm=function(){
        var oForm=document.createElement('FORM');
        oForm.id='form_'+id;
        oForm.action=url;
        oForm.method='POST';
        if(oForm.encoding){
            oForm.encoding = 'multipart/form-data';
        }
        else{
            oForm.enctype = 'multipart/form-data';
        }
        
        var jsonInput=document.createElement('INPUT');
        jsonInput.value=paraMap;
        jsonInput.name='_JasonParameters';
        jsonInput.type='hidden';
        oForm.appendChild(jsonInput);
        document.body.appendChild(oForm);
    };
    var createIFrame=function(){
        var oIFrame={};
        try{
            oIFrame = document.createElement("<IFRAME name=frame_"+id+" />");
        }catch(e){
            oIFrame=document.createElement("IFRAME");
            oIFrame.name='frame_'+ id;
        }
        oIFrame.name ='frame_'+ id;
        oIFrame.width=0;
        oIFrame.height=0;
        oIFrame.frameBorder=0;
        oIFrame.id='frame_'+id;
        document.body.appendChild(oIFrame);
        return oIFrame;
    };
    createForm();
    var oFrame=createIFrame();
    var oForm=document.getElementById('form_'+id);
    var thisCtrl=this;
    var uploadCallback=function(){
        var thisDocument=oFrame.contentDocument||oFrame.contentWindow.document;
        var html=thisDocument.body.innerHTML;
        if(html.charAt(0)=='{'){
            responseText=html;
        }else if(html.substring(0,5)=='<pre>'){
            responseText=html.substring(5,html.length-6);
        }
        else
            responseText=thisDocument.getElementById('result').value;
        try{
            thisCtrl.callService.complete(200,'success',responseText,'');
        }catch(e){
            throw $res_entry('ria.controls.processUpload.exception','\u4e0a\u4f20\u5f02\u5e38:')+e;
        }
        setTimeout(function(){try{
                                oForm.parentNode.removeChild(oForm);
                                oFrame.parentNode.removeChild(oFrame);
                                } catch(e){
                                    throw e;
                                }
                              }, 100);
    };
    
    //wanghao
    var oldParentNodes = [];
    var oldNodes = [];
    
    //wanghao
    var clonedNodes = [];
    for(var i=0;i<__file_List.length;i++){
        if(__file_List[i].type!='file'||__file_List[i].value=='') continue;
        var old=__file_List[i];
        var cloned=__file_List[i].cloneNode(true);
        
        //\u4fee\u6539\u8bb0\u5f552: wanghao
        cloned.onchange=old.onchange;
		cloned.onmousemove=old.onmousemove;
        old.parentNode.insertBefore(cloned,old);
        old.id=id+'_file_'+i;
        if(old.hideFocus != true)
            old.style.visibility = "hidden";
        
        oldNodes.push(old);
        clonedNodes.push(cloned);
        oldParentNodes.push(    old.parentNode);            
        oForm.appendChild(old);
        /*cloned.onchange=old.onchange;
        old.parentNode.insertBefore(cloned,old);
        old.id=id+'_file_'+i;
        old.style.display='none';
        oForm.appendChild(old);*/
        //\u4fee\u6539\u8bb0\u5f551: wanghao
        /*oldNodes.push(old);
        oldParentNodes.push(old.parentNode);            
        oForm.appendChild(old);*/
    }
    if(window.attachEvent){
        //oFrame.attachEvent('onload', uploadCallback);
        EventManager.Add(oFrame,'load',uploadCallback);
    }
    else{
        oFrame.addEventListener('load', uploadCallback, false);
    }
    oForm.setAttribute("target",oFrame.name);
    oForm.submit();
    
    /*for(var j=0;j<oldParentNodes.length;j++)
    {
        oldParentNodes[j].appendChild(oldNodes[j]);
    }*/
    //\u4fee\u6539\u8bb0\u5f552: wanghao
    for(var j=0;j<oldParentNodes.length;j++)
    {
        if(oldNodes[j].hideFocus == true)
        {
			if(oldParentNodes.length == 1) {
				oldNodes[j].id = clonedNodes[j].id;//2009-06-09 wxr\uff1a\u5355\u4e0a\u4f20\u4e0d\u6539\u53d8\u5176id
			}
            oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
            continue;
        }
        if(oldNodes[j].style.visibility == "hidden")
            oldNodes[j].style.visibility = "visible";
        oldParentNodes[j].replaceChild(oldNodes[j],clonedNodes[j]);
    }
    
    //\u4fee\u6539\u8bb0\u5f553: wanghao ------IE\u62a5\u9519\u95ee\u9898
    
    //wanghao8
    //__file_List = input_file;
    //document.body.removeChild(document.getElementById("form_"+id));
    //document.body.removeChild(document.getElementById("frame_"+id));
};
function cacheCookies(outParas,jsonvalue,sid)
{   
    var rtnserviec=pageContextCache.getService(sid);
    var rtnsrponse =rtnserviec.responseObj;
    var rtn =rtnsrponse.rtn;
    if(sid){
        if(outParas&&outParas.indexOf("$")!=-1)
        {
            parseRtn(pageContextCache,outParas,eval('('+jsonvalue+')'));
        }else if(!outParas)
        {
            if(rtn&&rtn.indexOf("$")!=-1)
            {
                parseRtn(pageContextCache,rtn,eval('('+jsonvalue+')'));
            }else
            {
                 BindDataToCtrl(rtn,eval('('+jsonvalue+')'));                   
            }
            
        }       
    }   
};
var SSB_MULTI_VALUE = {};
var SSB_INPARA_MULTI;
window.getMultiValue = function(arr){
    SSB_INPARA_MULTI = arr;
    window.callUrl("$/ssb/mutli/value/getMultiValue.ssm",SSB_INPARA_MULTI,"SSB_MULTI_VALUE","true");
    if(SSB_MULTI_VALUE.rtnValue){
       var multi_value = SSB_MULTI_VALUE.rtnValue;
       return multi_value;
    }  
};
window.callSidQueue = function(sidArr){
    var sidList = sidArr;
    for(var i=0;sidList!=null && i<sidList.length;i++){
        callSid(sidList[i]);
    }
};
window.callSid = function(sid, inParas, outParas) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
    var obj = getCacheCookie(getNameCookie(sid));
    //\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
    if(obj&&sid)
    {
        cacheCookies(outParas,obj,sid); 
        return;
    }
    var $M ={};
    var url = pageContextCache.getUrl(sid);
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        //\u663e\u793a\u8fdb\u5ea6\u6761
        var loadingobj = pageContextCache.getLoading(sid);
        //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
        if(loadingobj)
        {
            var issyn = loadingobj.isSyn;
            //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                displayDivImg(loadingobj);
            }else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
            {
				//\u540c\u4e00\u65f6\u95f4\u53ea\u5141\u8bb8\u663e\u793a\u4e00\u4e2a\u8fdb\u5ea6\u6761
				if(disableKeyWhenProcessing.isRuning == true) {
					//return false;
				}
				else {
					disableKeyWhenProcessing.isRuning = true;				
					divalert(loadingobj);
				}				
            }
        }
        var control;
        var paraMap;
        try{
             control= new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
             paraMap= control.organizedata.buildData(sid,$M);
             //wanghao modify
             if(paraMap === false) 
             {
				removeCacheUrl(url);
                hiddendDivImg(loadingobj.divId);
                removeObj();
                return;
             }
             
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException); 
        }       
        var isSyn = pageContextCache.getIsSyn(sid);
        log.info("call service: url=" + url);
        log.info("call service: input parameter=\n" + paraMap);

        //add by Daniel
        var enctype = pageContextCache.getEnctype(sid);

        if (  enctype=="multipart"&&__file_List.length&&__file_List.length>0 ) {
            control.processUpload(paraMap,url);
            return ;
        }
        
        if (  enctype=="form") {
            control.process(url, DEFAULT_METHOD, paraMap,isSyn);
            return;
        } 
        //add by Daniel
        
        if(__file_List.length&&__file_List.length>0){
            control.processUpload(paraMap,url);
            //\u4fee\u6539:wanghao
            //__file_List=[]; 
            return;
        }
        control.process(url, DEFAULT_METHOD, paraMap,isSyn);        
    }


};

//wanghao5
window.callSidUpload = function(groupId, sid, inParas, outParas)
{
    
    if(!file_group[groupId])
    {
        return;
    }
    else
    {
        __file_List = file_group[groupId];
    }
    
    callSid(sid, inParas, outParas);
};

window.callTSid = function(sid, inParas, outParas,jsonValue) {
    log.info("call service: service id=" + sid);    
    //log.info("call service: inParas=" + inParas);
    //log.info("call service: outParas=" + outParas);
    var obj = getCacheCookie(getNameCookie(sid));
    //\u662f\u5426\u5b58\u5728cookies\u7f13\u5b58
    if(obj&&sid)
    {
        cacheCookies(outParas,obj,sid); 
        return;
    }
    var $M ={};
    var url = pageContextCache.getUrl(sid);
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        //\u663e\u793a\u8fdb\u5ea6\u6761
        var loadingobj = pageContextCache.getLoading(sid);
        //\u5982\u679c\u7528\u6237\u9700\u8981\u663e\u793a\u8c03\u7528\u8fdb\u5ea6
        if(loadingobj)
        {
            var issyn = loadingobj.isSyn;
            //\u5f02\u6b65\u663e\u793a\u65b9\u5f0f
            if(issyn==null||issyn==""||issyn=="false"||issyn==false)
            {
                displayDivImg(loadingobj);
            }else if(issyn==true||issyn=='true')//\u540c\u6b65\u56fe\u7247\u663e\u793a\u65b9\u5f0f\uff0c\u9501\u5b9a\u5c4f\u5e55
            {
				//\u540c\u4e00\u65f6\u95f4\u53ea\u5141\u8bb8\u663e\u793a\u4e00\u4e2a\u8fdb\u5ea6\u6761
				if(disableKeyWhenProcessing.isRuning == true) {
					//return false;
				}
				else {
					disableKeyWhenProcessing.isRuning = true;
					divalert(loadingobj);
				}				
            }
        }       
         var control;
        try{
            control= new RIAControl(sid, inParas, outParas,null,null,null,$M,url);
            var temppara = control.organizedata.buildData(sid,$M);
            if(temppara === false) {
				removeCacheUrl(url);
				return;      
			}
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException);     
        }       
        
        var paraMap = jsonValue;
        var isSyn = pageContextCache.getIsSyn(sid);
        log.info("call service: url=" + url);
        log.info("call service: input parameter=\n" + paraMap);

        control.process(url, DEFAULT_METHOD, paraMap,isSyn);        
    }


};

window.setBindObject = function(resp_rtn,rtn_values)
{
    var pagecontext = pageContextCache;
    parseRtn(pagecontext,resp_rtn,rtn_values);
};
window.getBindObject = function(inParas)
{
    var $M ={};
    var control = new RIAControl(null, inParas, null,true,null,null,$M);
    var paraMaps;
    paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);
    return  paraMaps;   
};
window.callUrl = function(url, inParas, outParas,isSyn,urlErrorEvent) {
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        var $M ={};
         var control;
        var paraMaps;
        try{
            control= new RIAControl(null, inParas, outParas,true,null,null,$M,url,urlErrorEvent);   
            paraMaps = control.organizedata.splitParaValue(inParas,control.organizedata,paraMaps,$M);       
            if(paraMaps === false) {
				removeCacheUrl(url);
				return;
			}
        }catch(e){
            var tempException = new RIAException("",constantRia.RIA_EXCE_ORGANIZE,e.message);
            commonException(constantRia.RIA_RIA,tempException);         
        }
        var paraMap = JSON.stringify(paraMaps);
        log.info("call url: url=" + url);
        log.info("call url: input parameter=\n" + paraMap);
        control.process(url, DEFAULT_METHOD, paraMap,isSyn);
    }
};

window.callMethod = function(sidOrUrl, inParas, obj, fun)
{
   
    var index_url = sidOrUrl.indexOf(DEFAULT_DOTSSM_TAG);
    var url = "";
    if (index_url == -1)
    {
        url = pageContextCache.getUrl(sidOrUrl);
    }
    else
    {
        url = sidOrUrl;
    }
    var blean = addCacheUrl(url);
    //\u6ca1\u6709\u88ab\u91cd\u590d\u8c03\u7528
    if(blean)
    {
        var control = new RIAControl(null, null, null, null, obj, fun,url,url);
        var paraMap = JSON.stringify(inParas);
        control.process(url, DEFAULT_METHOD, paraMap);
    }
};
var _timer_names = new Array();

function callSidTimer(sid, timer)
{
   
   var name = setInterval("callSid('"+sid+"')",timer*1000);

   _timer_names[sid] = name;
};

function intTimerServ(sid)
{
   var name = _timer_names[sid];
   clearInterval(name);
};

function intCurrServ()
{
  throw true;
  
};

window.onerror=function()
{
  return true;
};

var getForwards = function (sid,m)
{
    eval("var $M = arguments[1];");
    var forwardTags = pageContextCache.getForward(sid);
    //alert(forwardTags.length);
    var forwardTags_length = forwardTags.length;
    if (forwardTags_length <= 0)
    {
        return;
    }
    
    //\u904d\u5386forward
    for (var i = 0; i < forwardTags_length; i++)
    {
        var forward_onforward = forwardTags[i].onforward;
        var forward_str = parseForwardValueById(forward_onforward);

        //\u5f97\u5230onforward\u4e2d\u7684\u7b97\u5f0f\u662f\u5426\u6210\u7acb
        var forward_s = eval(forward_str);
        if (forward_s)
        {
            var forward_action = parseToString_action(m,forwardTags[i].action);
            var forward_target = parseValueById(forwardTags[i].target);
            window.open(encodeURI(forward_action), forward_target);
            //return;       
        } else {
            //alert(forward_s);
            continue;
        }

    }

};

var parseForwardValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
        }
        else
        {
            id_value = id;
        }
        str = str.replace(strArr[i], id_value);
    }
    return str;
};

var parseValueById = function(str)
{
    var strArr = rtnValueById(str);
    for (var i = 0; i < strArr.length; i++)
    {
        var end = strArr[i].indexOf("}");
        var id = strArr[i].substring(2, end);
        var id_value = "";
        if (document.getElementById(id))
        {
            id_value = document.getElementById(id).value;
            str = str.replace(strArr[i], '"'+id_value+'"');
        }
        else
        {
            str = str.replace(strArr[i], id);
        }
        
    }
    return str;
};

var parseToString_action = function(m, _parsedStr)
{
    eval("var $M = arguments[0];");
    var parsedStr = parseForwardValueById(_parsedStr);
    var parseArr = parseLabel(parsedStr);
    var str = parsedStr;
    if (parseArr.length != 0)
    {
        for (var j = 0; j < parseArr.length; j++)
        {
            var rtn_str_value = eval(parseArr[j]);           
            var str = str.replace(parseArr[j], rtn_str_value);
        }
    }
    return str;
};

var parseLabel = function (str)
{
    var strArr = new Array();
    var str_index = str.indexOf("$M");
    if (str_index != -1)
    {
        strArr = parseArr(str, strArr);
    }

    return strArr;
};

var parseArr = function (str, strArr)
{
    var strArrs = str.split("");
    var parse_str = "";
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i - 1] == "$" && strArrs[i] == "M")
        {
            parse_str = str.substring(i);
            var strings = "$";
            var parse_strArrs = parse_str.split("");
            for (var j = 0; j < parse_strArrs.length; j++)
            {
                if ((/[^0-9a-zA-Z_\[\].]/g).test(parse_strArrs[j]))
                {
                    strArr.push(strings);
                    strings = "$";
                    var parse_string = parse_str.substring(j);
                    strArr = parseArr(parse_string, strArr);
                    return strArr;
                }
                else {
                    strings = strings + parse_strArrs[j];
                }
            }
            strArr.push(strings);
            return strArr;
        }
    }
    return strArr;
};


var rtnValueById = function (str)
{
    var strArr = new Array();
    var strArrs = str.split("");
    var parse_str = "";
    var TORF = false;
    for (var i = 0; i < strArrs.length; i++)
    {
        if (strArrs[i] == "$" && strArrs[i + 1] == "{")
        {
            TORF = true;
        }
        if (strArrs[i] == "}")
        {
            parse_str = parse_str + strArrs[i];
            strArr.push(parse_str);
            parse_str = "";
            TORF = false;
        }
        if (TORF)
        {
            parse_str = parse_str + strArrs[i];
        }
    }
    return strArr;
};
    function fixWindow()
    {
        var bgObj = document.getElementById('bgDiv');
        if(document.body.offsetWidth > document.body.scrollWidth)
            bgObj.style.width = document.body.offsetWidth + "px";
        else
            bgObj.style.width = document.body.scrollWidth + "px";
            
        if(document.body.scrollHeight > document.body.offsetHeight)
            bgObj.style.height = document.body.scrollHeight + "px";
        else
            bgObj.style.height = document.body.offsetHeight + "px";
        
    }
        
    function divalert(loadingObj)
    {
       var msgw,msgh,bordercolor;
	   $(window.document).bind("keydown", disableKeyWhenProcessing).bind("keyup", disableKeyWhenProcessing);
	   
       msgw=400;//\u63d0\u793a\u7a97\u53e3\u7684\u5bbd\u5ea6
       msgh=100;//\u63d0\u793a\u7a97\u53e3\u7684\u9ad8\u5ea6
       titleheight=25;//\u63d0\u793a\u7a97\u53e3\u6807\u9898\u9ad8\u5ea6
       bordercolor="#336699";//\u63d0\u793a\u7a97\u53e3\u7684\u8fb9\u6846\u989c\u8272
       titlecolor="#99CCFF";//\u63d0\u793a\u7a97\u53e3\u7684\u6807\u9898\u989c\u8272
       var sWidth,sHeight;
       
                
       //\u80cc\u666f\u5c42\uff08\u5927\u5c0f\u4e0e\u7a97\u53e3\u6709\u6548\u533a\u57df\u76f8\u540c\uff0c\u5373\u5f53\u5f39\u51fa\u5bf9\u8bdd\u6846\u65f6\uff0c\u80cc\u666f\u663e\u793a\u4e3a\u653e\u5c04\u72b6\u900f\u660e\u7070\u8272  
       var bgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09
       bgObj.setAttribute('id','bgDiv');
       bgObj.style.position="absolute";
       bgObj.style.top="0";
       //bgObj.style.background="#777";
       bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75";
       bgObj.style.opacity="0.6";
       bgObj.style.left="0";
       //bgObj.style.width=sWidth + "px";
       //bgObj.style.height=sHeight + "px";
       bgObj.style.zIndex = "10000";
       
       document.body.appendChild(bgObj);//\u5728body\u5185\u6dfb\u52a0\u8be5div\u5bf9\u8c61
        
       //wanghao
       fixWindow();
       window.onresize = fixWindow;
       
       var msgObj=document.createElement("div");//\u521b\u5efa\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
       msgObj.setAttribute("id","msgDiv");
       
       msgObj.style.background="white";
       msgObj.style.border="1px solid " + bordercolor;
       msgObj.style.position = "absolute";
       msgObj.style.left = "50%";
       msgObj.style.top = "50%";
       msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";
       
       msgObj.style.marginLeft = "-225px" ;
       msgObj.style.marginTop = -75+document.documentElement.scrollTop+"px";
       /*msgObj.style.width = msgw + "px";
       msgObj.style.height =msgh + "px";
       msgObj.style.textAlign = "center";
       msgObj.style.lineHeight ="25px";
       */
       msgObj.style.zIndex = "10001";
    
       var title=document.createElement("h4");//\u521b\u5efa\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
       title.setAttribute("id","msgTitle");
       title.setAttribute("align","right");
       title.style.filter="progid:DXImageTransform.Microsoft.Alpha(startX=20, startY=20, finishX=100, finishY=100,style=1,opacity=75,finishOpacity=100);";
       title.style.opacity="0.75";
       /*      title.style.margin="0";
       title.style.padding="3px";
       title.style.background=bordercolor;     
       title.style.border="1px solid " + bordercolor;
       title.style.height="18px";
       title.style.font="12px Verdana, Geneva, Arial, Helvetica, sans-serif";
       title.style.color="white";
       title.style.cursor="pointer";
       */
       title.innerHTML=$res_entry("ria.loading.title","\u6b63\u5728\u88c5\u8f7d\u4e2d\u8bf7\u7a0d\u5019......");
    
       var image = document.createElement("IMG");//\u5b9a\u4e49\u4e00\u4e2a\u80cc\u666f\u56fe\u7247
        var imgscr = "";//\u56fe\u7247\u8def\u5f84
        var width = ""; //\u56fe\u7247\u5bbd\u5ea6
        var height =""; //\u56fe\u7247\u9ad8\u5ea6
        //\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
        if(loadingObj)
        {
            imgscr = loadingObj.img;
            width = loadingObj.width;
            height = loadingObj.height;
        }
        
       //wanghao22
       image.setAttribute("id","msgImage");        
       image.setAttribute("src",imgscr);
       image.setAttribute("align","middle");
       image.setAttribute("width",width);
       image.setAttribute("height",height);
       
       //wanghao22
       /*var cancel =  document.createElement("div");//\u5b9a\u4e49\u4e00\u4e2a\u53d6\u6d88\u6309\u94ae
       cancel.setAttribute("id","msg_cancel");
       cancel.innerHTML = "<a onclick='removeObj()' href='javascript:void(0)'>Cancel</a>";
           */
       document.body.appendChild(msgObj);//\u5728body\u5185\u6dfb\u52a0\u63d0\u793a\u6846div\u5bf9\u8c61msgObj
       document.getElementById("msgDiv").appendChild(title);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u6807\u9898\u680f\u5bf9\u8c61title
       document.getElementById("msgDiv").appendChild(image);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u56fe\u7247\u5bf9\u8c61image
       //document.getElementById("msgDiv").appendChild(cancel);//\u5728\u63d0\u793a\u6846div\u4e2d\u6dfb\u52a0\u53d6\u6d88\u6309\u94aecancel
    }; 
	
	function disableKeyWhenProcessing(e)
	{	
		e.preventDefault();		
	}
	disableKeyWhenProcessing.isRuning = false;
	
    function removeObj()
    {
         //\u70b9\u51fb\u6807\u9898\u680f\u89e6\u53d1\u7684\u4e8b\u4ef6
         var msgObj=document.getElementById("msgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u5c42\uff09
         var title=document.getElementById("msgTitle");//\u83b7\u53d6\u4e00\u4e2ah4\u5bf9\u8c61\uff08\u63d0\u793a\u6846\u6807\u9898\u680f\uff09
         var bgObj=document.getElementById("bgDiv");//\u83b7\u53d6\u4e00\u4e2adiv\u5bf9\u8c61\uff08\u80cc\u666f\u5c42\uff09 
         
         //wanghao22
         window.onresize = null;
         
         //wanghao22
         if(bgObj)
            document.body.removeChild(bgObj);//\u5220\u9664\u80cc\u666f\u5c42Div
         if(msgObj && title)
            document.getElementById("msgDiv").removeChild(title);//\u5220\u9664\u63d0\u793a\u6846\u7684\u6807\u9898\u680f
         if(msgObj)
            document.body.removeChild(msgObj);//\u5220\u9664\u63d0\u793a\u6846\u5c42
		
		$(window.document).unbind("keydown", disableKeyWhenProcessing).unbind("keyup", disableKeyWhenProcessing);
		disableKeyWhenProcessing.isRuning = false;
    };

    function displayDivImg(loadingObj)
    {
        var imgscr = "";//\u56fe\u7247\u8def\u5f84
        var width = ""; //\u56fe\u7247\u5bbd\u5ea6
        var height =""; //\u56fe\u7247\u9ad8\u5ea6
        var divid = ''; //\u8bbe\u7f6e\u8981\u663e\u793a\u7684div
        //\u88c5\u8f7d\u56fe\u7247\u7684\u5bf9\u8c61
        if(loadingObj)
        {
            imgscr = loadingObj.img;
            width = loadingObj.width;
            height = loadingObj.height;
            divid = loadingObj.divId;
        }
        //\u8be5div\u662f\u5426\u5b58\u5728
        if(divid)
        {
           document.getElementById(divid).style.display="";  
           document.getElementById(divid).innerHTML="<img src='"+imgscr+"' width='"+width+"' height='"+height+"'/>";
        }
    };

    function hiddendDivImg(divid)
    {
      //\u8be5div\u662f\u5426\u5b58\u5728
      if(divid)
      {
         document.getElementById(divid).style.display="none";
      }
    };
    //\u8fd9\u662f\u6709\u8bbe\u5b9a\u8fc7\u671f\u65f6\u95f4\u7684\u4f7f\u7528\u793a\u4f8b\uff1a
    //s20\u662f\u4ee3\u886820\u79d2
    //h\u662f\u6307\u5c0f\u65f6\uff0c\u598212\u5c0f\u65f6\u5219\u662f\uff1ah12
    //d\u662f\u5929\u6570\uff0c30\u5929\u5219\uff1ad30  
    //\u5199cookies
    function setCookie(name,value,time){
        var strsec = getsec(time);
        document.cookie = name + "="+ encodeURIComponent (value) + ";expires=" + strsec;
    };
    function getNameCookie(sid)
    {
        var path = document.location.pathname.toString();
        path = path+":"+sid;
        return path;        
    };
    //\u83b7\u53d6\u7528\u4e8e\u8fc7\u671f\u7edf\u8ba1
    function getsec(strtime){
        //\u9a8c\u8bc1\u65f6\u95f4\u683c\u5f0fyyyy-mm-dd
        var timearr = strtime.split("-");
        //\u662f\u65f6\u95f4\u683c\u5f0f
        if(timearr.length==3)
        {
            var objdate =new Object();
            objdate.value=strtime;          
            var bl = isDate(objdate);
            if(bl==undefined)
            {
                bl = true;
            }           
            //\u662f\u6b63\u786e\u7684\u65f6\u95f4\u683c\u5f0f
            if(bl)
            {
                var yeararr = strtime.split("-");
                var year =yeararr[0];
                var month = yeararr[1];
                var day = yeararr[2];
                var utc=Date.UTC(year,month-1,day);
                var date = new Date(utc);
                return date.toGMTString();
            }           
        }else if(strtime)
        {
            //\u5982\u679c\u662f\u4e0b\u4e2a\u6708
            if(strtime.indexOf("DAY")!=-1)
            {
                var monthday =strtime.substring(3,strtime.length);
                var nowtime = new Date();
                var nowmonth = nowtime.getMonth();
                var nowyear = nowtime.getFullYear();
                if(nowmonth ==11)
                {
                    nowyear += 1;
                    nowmonth = "01";                    
                }
                var utc=Date.UTC(nowyear,nowmonth,monthday);
                var date = new Date(utc);
                return date.toGMTString();              
                
            }else//\u5982\u679c\u662f\u4e0b\u4e2a\u661f\u671f\u6216\u8005\u662f\u5f53\u5929\u7684\u67d0\u4e2a\u65f6\u523b
            {
                var weekday = new Date();
                var d = weekday.getDay();
                if(d==0)
                {
                    d=7;
                }               
                switch(strtime)
                {
                    case "MON":
                       return getDisDate(d,1);                      
                    case "TUE":
                       return getDisDate(d,2);
                    case "WED":
                       return getDisDate(d,3);
                    case "THU":
                       return getDisDate(d,4);
                    case "FRI":
                       return getDisDate(d,5);
                    case "SAT":
                       return getDisDate(d,6);
                    case "SUN":
                       return getDisDate(d,7);                                                                                      
                }               
            }
        }
    };
    function getDisDate(disDate,passweekday)
    {
        
        var numObj ;
        if(disDate>passweekday)
        {
            numObj = passweekday - disDate+7;
        }else if(disDate==passweekday)
        {
            numObj=7;
        }else if(disDate<passweekday)
        {
            numObj =passweekday - disDate;
        }       
        var utcm = numObj*60*60*24*1000;
        var cdate = new Date();
        var year = cdate.getYear();
        var month = cdate.getMonth();
        var day = cdate.getDate();

        var utc=Date.UTC(year,month,day);
        utc+=utcm;
        var date = new Date(utc);
        return date;        
    };
    //\u8bfb\u53d6cookies
    function getCacheCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        arr=document.cookie.match(reg);
        if(arr){ return decodeURIComponent(arr[2]);}
        else {return null;}
    };
    //\u5220\u9664\u4e00\u4e2acookies
    function iterativeCookie(name)
    {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval=getCacheCookie(name);
        if(cval!=null) {document.cookie= name + "="+cval+";expires="+exp.toGMTString();}            
    };
    //\u591a\u4e2asid\u7f13\u5b58\u7684\u5220\u9664
    function delCookie(name)
    {
        //\u5220\u9664\u6240\u6709\u7684
        //\u5982\u679c\u53ea\u6709\u4e00\u4e2asid
        if(!name)
        {
            var allservice = pageContextCache.serviceArr;
            for(var i=0;i<allservice.length;i++)
            {
                if(allservice[i].cache==true||allservice[i].cache=='true') iterativeCookie(allservice[i].id);
            }
            return;
        }
        if(name.indexOf(";")==-1)
        {
              iterativeCookie(getNameCookie(name));
              return;
        }else//\u5982\u679c\u6709\u591a\u4e2asid
        {
            var namearr=name.split(";");
            for(var i=0;i<namearr.length;i++)
            {
                if(namearr[i]) 
                {
                    iterativeCookie(getNameCookie(namearr[i]));
                }
            }
            return;
        }
    };

package com.zte.cms.content.service;

import java.util.List;

import com.zte.cms.content.model.IcmTag;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmTagDS
{
    /**
     * 新增IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void insertIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 更新IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 批量更新IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTagList(List<IcmTag> icmTagList) throws DomainServiceException;

    /**
     * 删除IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 批量删除IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTagList(List<IcmTag> icmTagList) throws DomainServiceException;

    /**
     * 查询IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @return IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public IcmTag getIcmTag(IcmTag icmTag) throws DomainServiceException;

    /**
     * 根据条件查询IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @return 满足条件的IcmTag对象集
     * @throws DomainServiceException ds异常
     */
    public List<IcmTag> getIcmTagByCond(IcmTag icmTag) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTag对象
     * 
     * @param icmTag IcmTag对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTag对象
     * 
     * @param icmTag IcmTag对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoUnclearQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTag对象
     * 
     * @param icmTag IcmTag对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 返回内容标签列表
     */
    public List<IcmTag> getIcmTagList() throws DomainServiceException;
}
package com.zte.cms.basicdata.service;

import java.util.List;
import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsExtractedvafileInfoDS
{
    /**
     * 新增CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException;

    /**
     * 更新CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException;

    /**
     * 批量更新CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsExtractedvafileInfoList(List<CmsExtractedvafileInfo> cmsExtractedvafileInfoList)
            throws DomainServiceException;

    /**
     * 删除CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException;

    /**
     * 批量删除CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsExtractedvafileInfoList(List<CmsExtractedvafileInfo> cmsExtractedvafileInfoList)
            throws DomainServiceException;

    /**
     * 查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @return CmsExtractedvafileInfo对象
     * @throws DomainServiceException ds异常
     */
    public CmsExtractedvafileInfo getCmsExtractedvafileInfo(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException;

    /**
     * 根据条件查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象
     * @return 满足条件的CmsExtractedvafileInfo对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsExtractedvafileInfo> getCmsExtractedvafileInfoByCond(CmsExtractedvafileInfo cmsExtractedvafileInfo)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsExtractedvafileInfo对象
     * 
     * @param cmsExtractedvafileInfo CmsExtractedvafileInfo对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsExtractedvafileInfo cmsExtractedvafileInfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
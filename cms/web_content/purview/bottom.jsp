<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<html>
<body bgcolor="#ffffff">
<form name="view">
  <input type="hidden" name="operID" value="">
<table>
  <tr>
    <td id="m1" style="display:none"><input type="button" name="memberRight" value="<fmt:message key="bottom.view"/>" onclick="javascript:detail()" disabled></td>
    <td id="b1" style="display:none"><input type="button" name="back2" value="<fmt:message key="bottom.back"/>" class="button-style1" onclick="javascript:goback()"></td>
  </tr>
</table>
</form>


<script language="javascript">
function detail() {
  var opid = document.view.operID.value;
  m1.style.display = "none";
  b1.style.display = "block";
  parent.allotFrame.document.view.operID.value = "";
  parent.allotFrame.document.view.operName.value = "";
  parent.allotFrame.document.view.submit();
  parent.operFrame.document.URL = "memberTreeRights.jsp?operID="+opid+"&memberID="+opid+"&operType=init";
}

function goback() {
  m1.style.display = "block";
  b1.style.display = "none";
  document.view.memberRight.disabled = true;
  parent.allotFrame.document.view.operID.value = "";
  parent.allotFrame.document.view.operName.value = "";
  parent.allotFrame.document.view.submit();
  parent.operFrame.document.URL = "operListRights.jsp";
}
</script>
</body>
</html>

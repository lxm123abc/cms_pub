package com.zte.cms.service.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.service.categoryservicemap.model.CategoryServiceMap;
import com.zte.cms.service.categoryservicemap.service.ICategoryServiceMapDS;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceContentBindDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceSchedualMap;
import com.zte.cms.service.servicecntbindmap.model.ServiceSeriesMap;
import com.zte.cms.service.servicecntbindmap.service.IServiceChannelMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceSchedualMapDS;
import com.zte.cms.service.servicecntbindmap.service.IServiceSeriesMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.terminal.ls.UhandsetConstant;

/**
 * 
 * @author shaoyan
 * 
 */
public class ServiceContentBindLS extends DynamicObjectBaseDS implements IServiceContentBindLS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    private IServiceDS serviceds = null; // 服务
    private ITargetsystemDS targetSystemDS = null; // 目标系统
    private ICntSyncTaskDS cntSyncTaskDS = null; // 同步任务表
    private ICntSyncRecordDS cntSyncRecordDS = null; // 分发记录表
    private IServiceContentBindDS serviceCntBindds = null; // 服务内容绑定
    private IServiceProgramMapDS serviceprogrammapds = null;
    private IServiceChannelMapDS servicechannelmapds = null;
    private ICmsChannelDS channelds = null;
    private IServiceSchedualMapDS serviceschedulemapds = null;
    private ICmsProgramDS cmsProgramDS = null;
    private ICmsScheduleDS cmsscheduleds = null;
    private ICmsSeriesDS cmsseriesds = null;
    private IServiceSeriesMapDS serviceseriesmapds = null;
    private ICategoryServiceMapDS categoryServiceMapds = null;
    private ICategorycdnDS cmsCategoryDS = null;

    private ICntPlatformSyncDS cntPlatformSyncds = null;
    private ICntTargetSyncDS cntTargetSyncds = null;

    public void setCntPlatformSyncds(ICntPlatformSyncDS cntPlatformSyncds)
    {
        this.cntPlatformSyncds = cntPlatformSyncds;
    }

    public void setCntTargetSyncds(ICntTargetSyncDS cntTargetSyncds)
    {
        this.cntTargetSyncds = cntTargetSyncds;
    }

    /** @param categoryServiceMapds
     *            ICategoryServiceMapDS */
    public void setCategoryServiceMapds(ICategoryServiceMapDS categoryServiceMapds)
    {
        this.categoryServiceMapds = categoryServiceMapds;
    }

    /**
     * 
     * @param cmsseriesds ICmsSeriesDS
     */
    public void setCmsseriesds(ICmsSeriesDS cmsseriesds)
    {
        this.cmsseriesds = cmsseriesds;
    }

    /**
     * 
     * @param serviceseriesmapds IServiceSeriesMapDS
     */
    public void setServiceseriesmapds(IServiceSeriesMapDS serviceseriesmapds)
    {
        this.serviceseriesmapds = serviceseriesmapds;
    }

    /**
     * 
     * @param serviceschedulemapds IServiceSchedualMapDS
     */
    public void setServiceschedulemapds(IServiceSchedualMapDS serviceschedulemapds)
    {
        this.serviceschedulemapds = serviceschedulemapds;
    }

    /**
     * 
     * @param cmsscheduleds ICmsScheduleDS
     */
    public void setCmsscheduleds(ICmsScheduleDS cmsscheduleds)
    {
        this.cmsscheduleds = cmsscheduleds;
    }

    /**
     * 
     * @param serviceds IServiceDS
     */
    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    /**
     * 
     * @param targetSystemDS ITargetsystemDS
     */
    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    /**
     * 
     * @param cntSyncTaskDS ICntSyncTaskDS
     */
    public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
    {
        this.cntSyncTaskDS = cntSyncTaskDS;
    }

    /**
     * 
     * @param cntSyncRecordDS ICntSyncRecordDS
     */
    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    /**
     * 
     * @param serviceCntBindds IServiceContentBindDS
     */
    public void setServiceCntBindds(IServiceContentBindDS serviceCntBindds)
    {
        this.serviceCntBindds = serviceCntBindds;
    }

 // 构造方法
    ServiceContentBindLS(){
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
    }
    /**
     * 查询和服务绑定的内容信息
     * 
     * @param 利用service类将查询条件传入后台
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize) throws Exception
    {
        //
        log.debug("query bind relationship between serviec and content starting...");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mapindex");

        try
        {
            tableInfo = pageInfoQueryCntBindService(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query bind relationship between serviec and content ends...");

        return tableInfo;
    }

    /**
     * 查询和服务绑定的内容信息
     * 
     * @param service 利用service类将查询条件传入后台serviceindex不变，
     *      内容类型由feetype传入，绑定关系状态由status传入， 内容的编码由corpmode传入 内容名称由cntrange传入
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryCntBindService(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception
    {
        log.debug("get service page info by condition starting...");
     // 先进行字符的转换
        service.setCntrange(EspecialCharMgt.conversion(service.getCntrange()));
        service.setCorpmode(EspecialCharMgt.conversion(service.getCorpmode()));
        TableDataInfo tableInfo = null;
        try
        {
            
             OperInfo operinfo = LoginMgt.getOperInfo("CMS");
             service.setOperid(operinfo.getOperid());
             tableInfo = serviceCntBindds.pageInfoQueryCntBindService(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 查询没有绑定的但是可以绑定的内容
     * 
     * @param 利用service类将查询条件传入后台
     * @param start
     * @param pageSize
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize) throws Exception
    {
        //
        log.debug("query bind relationship between serviec and content starting...");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("contentid");

        try
        {
            tableInfo = pageInfoQueryContentForServiceBind(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query bind relationship between serviec and content ends...");

        return tableInfo;
    }

    /**
     * 查询没有绑定的但是可以绑定的内容
     *      内容类型由feetype传入，绑定关系状态由status传入， 内容的编码由corpmode传入 内容名称由cntrange传入
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryContentForServiceBind(Service service, int start, int pageSize,
            PageUtilEntity puEntity) throws Exception
    {
        log.debug("get content for bind start...");
        service.setCntrange(EspecialCharMgt.conversion(service.getCntrange()));
        service.setCorpmode(EspecialCharMgt.conversion(service.getCorpmode()));
        TableDataInfo tableInfo = null;
        try
        {
             OperInfo operinfo = LoginMgt.getOperInfo("CMS");
             service.setOperid(operinfo.getOperid());
             tableInfo = serviceCntBindds.pageInfoQueryContentForServiceBind(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("get content for bind ends ");
        return tableInfo;
    }

    /** 批量绑定内容
     * 
     * @param cntIdxList
     *            ：选中内容的index数组
     * @param serviceIdx
     *            ：要绑定的服务的index
     * @param cntBindType
     *            ：绑定的内容类型 根据不同的类型，插入不同的map表
     * @return
     * @throws Exception */
    public String insertServiceBindMap(String cntIdxList[], Long serviceIdx, int cntBindType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 绑定失败的个数
        Integer iSuccessed = 0; // 绑定成功的个数
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        int index = 1;

        if (cntIdxList.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_NO_CONTENT_CHOSEN)); // 没有内容被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        Service service = new Service();
        service.setServiceindex(serviceIdx);
        try
        {
            service = serviceds.getService(service);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        if (null == service)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS)); // 服务不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }
        else if (service.getStatus() != 0) // 服务的状态应该为审核通过
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_STATUS_WRONG)); // 服务状态不正确
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        try
        {
            List<CntTargetSync> ComTargetSyncList = null; // 用于记录公共平台
            CntTargetSync getCntTargetSync = new CntTargetSync(); // 用于通过向其写入服务index和内容id，调用方法获取公共网元系统
            getCntTargetSync.setObjectindex(serviceIdx);
            // 判断内容的类型，根据不同的类型，查询不同的表
            if (cntBindType == 1)// 连续剧内容
            {
                for (int idxProg = 0; idxProg < cntIdxList.length; idxProg++)
                {
//                   本处开始修改
                    // 获取当前连续剧完整信息
                    CmsSeries cmsseries = new CmsSeries();
                    cmsseries.setSeriesindex(Long.valueOf(cntIdxList[idxProg]));
                    cmsseries = cmsseriesds.getCmsSeries(cmsseries);
                    // 判断连续剧是否存在
                    if (null == cmsseries)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cntIdxList[idxProg], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_NO_EXISTS)); // 内容不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 判断连续剧和服务是否有共同平台
                    getCntTargetSync.setElementid(cmsseries.getSeriesid());
                    ComTargetSyncList = cntTargetSyncds.getCommonTarget4ServiceAndSeries(getCntTargetSync);
                    // 通过后台读取对应的关联目标系统(暂不考虑)
//                    if (ComTargetSyncList.size() < 1)
//                    {
//                        ifail++;
//                        operateInfo = new OperateInfo(String.valueOf(index++),cmsseries.getSeriesid(), ResourceMgt
//                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
//                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_TARGET_NO_COMMON_EXISTS)); // 没有网元可以绑定
//                        returnInfo.appendOperateInfo(operateInfo);
//                        continue;
//                    }

//                  检测是否关联，
                    ServiceSeriesMap servSerMap = new ServiceSeriesMap();
                    servSerMap.setServiceindex(service.getServiceindex());
                    servSerMap.setSeriesindex(cmsseries.getSeriesindex());

                    int exitServSerMapNumber = serviceseriesmapds.getServiceSeriesMapByCond(servSerMap).size();
                    if (exitServSerMapNumber < 1)//如果服务与连续剧内容没有关联就添加
                    {
                        servSerMap.setServiceid(service.getServiceid());
                        servSerMap.setSeriesid(cmsseries.getSeriesid());
                        servSerMap.setStatus(0);
                        servSerMap.setCreatetime(dateFormat.format(new Date()));
                        serviceseriesmapds.insertServiceSeriesMap(servSerMap);
                    }
                    else//如果服务与连续剧内容已经关联就不添加，
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cmsseries.getSeriesid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_BIND_EXISTS)); // 服务与内容已经绑定
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    servSerMap = serviceseriesmapds.getServiceSeriesMapByCond(servSerMap).get(0);

                    // 根据当前服务与内容的共同平台开始循环添加绑定
                    for (int i = 0; i < ComTargetSyncList.size(); i++)
                    {

                        Long ComTargetindex = ComTargetSyncList.get(i).getTargetindex();
                        //检测主平台是否有相应信息，如果没有就添加，如果有就不添加
                        CntPlatformSync servSerPlatform = new CntPlatformSync();
                        servSerPlatform.setObjecttype(22);
                        servSerPlatform.setParentid(service.getServiceid());
                        servSerPlatform.setElementid(cmsseries.getSeriesid());
                        // 获取当前平台类型(本处使用的programTarget变量应该是seriesTarget更符合其代表的含义)
                        Targetsystem programTarget = new Targetsystem();
                        programTarget.setTargetindex(ComTargetindex);
                        programTarget = targetSystemDS.getTargetsystem(programTarget);
                        servSerPlatform.setPlatform(programTarget.getPlatform());
                        //检测当前平台对应的网元是否注销或已经关闭
                        if (programTarget.getStatus()!=0)
                        {
                            continue;
                        }                        
                        
                        int servSerPlatformSize = cntPlatformSyncds.getCntPlatformSyncByCond(servSerPlatform).size();
                        if (servSerPlatformSize < 1)
                        {
                            servSerPlatform.setObjectindex(servSerMap.getMapindex());
                            servSerPlatform.setObjectid(servSerMap.getMappingid());
                            servSerPlatform.setStatus(0);
                            cntPlatformSyncds.insertCntPlatformSync(servSerPlatform);
                        }

                        Long relateindex = cntPlatformSyncds.getCntPlatformSyncByCond(servSerPlatform).get(0)
                                .getSyncindex();

//                      向网元添加信息
                        CntTargetSync servSerTarget = new CntTargetSync();
                        servSerTarget.setObjecttype(22);
                        servSerTarget.setRelateindex(relateindex);
                        servSerTarget.setObjectindex(servSerMap.getMapindex());
                        servSerTarget.setObjectid(servSerMap.getMappingid());
                        servSerTarget.setTargetindex(ComTargetindex);

                        int servSerTargetSize = cntTargetSyncds.getCntTargetSyncByCond(servSerTarget).size();
                        if (servSerTargetSize < 1)
                        {
                            servSerTarget.setParentid(service.getServiceid());
                            servSerTarget.setElementid(cmsseries.getSeriesid());
                            servSerTarget.setStatus(0);
                            servSerTarget.setOperresult(0);
                            cntTargetSyncds.insertCntTargetSync(servSerTarget);
                        }

                    }
                    // 根据当前服务与内容的共同平台循环添加绑定结束

                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(service.getServiceid()+","+cmsseries.getSeriesid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_SERIES,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_SERIES_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                    // 返回信息
                    iSuccessed++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cmsseries.getSeriesid(), ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_SUCC)); // 内容绑定成功
                    returnInfo.appendOperateInfo(operateInfo);

                    // 结束当前连续剧绑定操作
                }
                // 结束绑定连续剧循环
            }
            else if (cntBindType == 2) // VOD内容
            {
                for (int idxProg = 0; idxProg < cntIdxList.length; idxProg++)
                {
                    CmsProgram cmsProgram = new CmsProgram();
                    cmsProgram.setProgramindex(Long.valueOf(cntIdxList[idxProg]));
                    cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
                    if (null == cmsProgram)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cntIdxList[idxProg], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_NO_EXISTS)); // 内容不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 判断连续剧和服务是否有共同平台
                    getCntTargetSync.setElementid(cmsProgram.getProgramid());
                    ComTargetSyncList = cntTargetSyncds.getCommonTarget4ServiceAndProgram(getCntTargetSync);
                    // 通过后台读取对应的关联目标系统(暂不考虑)
//                    if (ComTargetSyncList.size() < 1)
//                    {
//                        ifail++;
//                        operateInfo = new OperateInfo(String.valueOf(index++),cmsProgram.getProgramid(), ResourceMgt
//                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
//                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_TARGET_NO_COMMON_EXISTS)); // 没有网元可以绑定
//                        returnInfo.appendOperateInfo(operateInfo);
//                        continue;
//                    }
//                    检测是否关联，如果关联就不添加，如果没有就添加

                    ServiceProgramMap servProgMap = new ServiceProgramMap();
                    servProgMap.setServiceindex(service.getServiceindex());
                    servProgMap.setProgramindex(cmsProgram.getProgramindex());

                    int exitServProgMapNumber = serviceprogrammapds.getServiceProgramMapByCond(servProgMap).size();
                    if (exitServProgMapNumber < 1)
                    {
                        servProgMap.setServiceid(service.getServiceid());
                        servProgMap.setProgramid(cmsProgram.getProgramid());
                        servProgMap.setStatus(0);
                        servProgMap.setCreatetime(dateFormat.format(new Date()));
                        serviceprogrammapds.insertServiceProgramMap(servProgMap);
                    }
                    else//如果服务与点播内容已经关联就不添加，
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cmsProgram.getProgramid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_BIND_EXISTS)); // 服务与内容已经绑定
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    servProgMap = serviceprogrammapds.getServiceProgramMapByCond(servProgMap).get(0);
                    // 根据当前服务与内容的共同平台开始循环添加绑定
                    for (int i = 0; i < ComTargetSyncList.size(); i++)
                    {
                        Long ComTargetindex = ComTargetSyncList.get(i).getTargetindex();

//                    检测主平台是否有相应信息，如果没有就添加，如果有就不添加
                        CntPlatformSync servProgPlatform = new CntPlatformSync();
                        servProgPlatform.setObjecttype(21);
                        servProgPlatform.setParentid(service.getServiceid());
                        servProgPlatform.setElementid(cmsProgram.getProgramid());
                        // 获取当前平台类型
                        Targetsystem programTarget = new Targetsystem();
                        programTarget.setTargetindex(ComTargetindex);
                        programTarget = targetSystemDS.getTargetsystem(programTarget);
                        servProgPlatform.setPlatform(programTarget.getPlatform());
                        //检测当前平台对应的网元是否注销或已经关闭
                        if (programTarget.getStatus()!=0)
                        {
                            continue;
                        }   
                        
                        int servProgPlatformSize = cntPlatformSyncds.getCntPlatformSyncByCond(servProgPlatform).size();
                        if (servProgPlatformSize < 1)
                        {
                            servProgPlatform.setObjectindex(servProgMap.getMapindex());
                            servProgPlatform.setObjectid(servProgMap.getMappingid());
                            servProgPlatform.setStatus(0);
                            cntPlatformSyncds.insertCntPlatformSync(servProgPlatform);
                        }

                        Long relateindex = cntPlatformSyncds.getCntPlatformSyncByCond(servProgPlatform).get(0)
                                .getSyncindex();

//                    向网元添加信息
                        CntTargetSync servProgTarget = new CntTargetSync();
                        servProgTarget.setObjecttype(21);
                        servProgTarget.setRelateindex(relateindex);
                        servProgTarget.setObjectindex(servProgMap.getMapindex());
                        servProgTarget.setObjectid(servProgMap.getMappingid());
                        servProgTarget.setTargetindex(ComTargetindex);

                        int servProgTargetSize = cntTargetSyncds.getCntTargetSyncByCond(servProgTarget).size();
                        if (servProgTargetSize < 1)
                        {
                            servProgTarget.setParentid(service.getServiceid());
                            servProgTarget.setElementid(cmsProgram.getProgramid());
                            servProgTarget.setStatus(0);
                            servProgTarget.setOperresult(0);
                            cntTargetSyncds.insertCntTargetSync(servProgTarget);
                        }
                    }
                    // 根据当前服务与内容的共同平台循环添加绑定结束

                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(service.getServiceid()+","+cmsProgram.getProgramid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_PROGRAM,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_PROGRAM_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                    // 返回信息
                    iSuccessed++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cmsProgram.getProgramid(), ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_SUCC)); // 内容绑定成功
                    returnInfo.appendOperateInfo(operateInfo);

                }
            }
            else if (cntBindType == 3) // 直播内容
            {
                for (int idxProg = 0; idxProg < cntIdxList.length; idxProg++)
                {
                    CmsChannel cmsChannelBind = new CmsChannel();
                    cmsChannelBind.setChannelindex(Long.valueOf(cntIdxList[idxProg]));
                    cmsChannelBind = channelds.getCmsChannel(cmsChannelBind);
                    if (null == cmsChannelBind)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cntIdxList[idxProg], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_NO_EXISTS)); // 内容不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 判断直播频道和服务是否有共同平台
                    getCntTargetSync.setElementid(cmsChannelBind.getChannelid());
                    ComTargetSyncList = cntTargetSyncds.getCommonTarget4ServiceAndChannel(getCntTargetSync);

                    // 通过后台读取对应的关联目标系统(暂不考虑)
//                    if (ComTargetSyncList.size() < 1)
//                    {
//                        ifail++;
//                        operateInfo = new OperateInfo(String.valueOf(index++), cmsChannelBind.getChannelid(), ResourceMgt
//                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
//                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_TARGET_NO_COMMON_EXISTS)); // 没有网元可以绑定
//                        returnInfo.appendOperateInfo(operateInfo);
//                        continue;
//                    }

//                  检测是否关联，如果关联就不添加，如果没有就添加
                    ServiceChannelMap servChannMap = new ServiceChannelMap();
                    servChannMap.setServiceindex(service.getServiceindex());
                    servChannMap.setChannelindex(cmsChannelBind.getChannelindex());

                    int exitServChannelMapNumber = servicechannelmapds.getServiceChannelMapByCond(servChannMap).size();
                    if (exitServChannelMapNumber < 1)
                    {
                        servChannMap.setServiceid(service.getServiceid());
                        servChannMap.setChannelid(cmsChannelBind.getChannelid());
                        servChannMap.setStatus(0);
                        servChannMap.setCreatetime(dateFormat.format(new Date()));
                        servicechannelmapds.insertServiceChannelMap(servChannMap);
                    }
                    else//如果服务与直播内容已经关联就不添加，
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), cmsChannelBind.getChannelid(), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_CNT_BIND_EXISTS)); // 服务与内容已经绑定
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    servChannMap = servicechannelmapds.getServiceChannelMapByCond(servChannMap).get(0);

                    // 根据当前服务与内容的共同平台开始循环添加绑定
                    for (int i = 0; i < ComTargetSyncList.size(); i++)
                    {
                        Long ComTargetindex = ComTargetSyncList.get(i).getTargetindex();

//                      检测主平台是否有相应信息，如果没有就添加，如果有就不添加
                        CntPlatformSync servChannelPlatform = new CntPlatformSync();
                        servChannelPlatform.setObjecttype(23);
                        servChannelPlatform.setParentid(service.getServiceid());
                        servChannelPlatform.setElementid(cmsChannelBind.getChannelid());
                        // 获取当前平台类型
                        Targetsystem programTarget = new Targetsystem();
                        programTarget.setTargetindex(ComTargetindex);
                        programTarget = targetSystemDS.getTargetsystem(programTarget);
                        servChannelPlatform.setPlatform(programTarget.getPlatform());
                        //检测当前平台对应的网元是否注销或已经关闭
                        if (programTarget.getStatus()!=0)
                        {
                            continue;
                        }   

                        int servChannelPlatformSize = cntPlatformSyncds.getCntPlatformSyncByCond(servChannelPlatform)
                                .size();
                        if (servChannelPlatformSize < 1)
                        {
                            servChannelPlatform.setObjectindex(servChannMap.getMapindex());
                            servChannelPlatform.setObjectid(servChannMap.getMappingid());
                            servChannelPlatform.setStatus(0);
                            cntPlatformSyncds.insertCntPlatformSync(servChannelPlatform);
                        }

                        Long relateindex = cntPlatformSyncds.getCntPlatformSyncByCond(servChannelPlatform).get(0)
                                .getSyncindex();

//                   向网元添加信息
                        CntTargetSync servChannelTarget = new CntTargetSync();
                        servChannelTarget.setObjecttype(23);
                        servChannelTarget.setRelateindex(relateindex);
                        servChannelTarget.setObjectindex(servChannMap.getMapindex());
                        servChannelTarget.setObjectid(servChannMap.getMappingid());
                        servChannelTarget.setTargetindex(ComTargetindex);

                        int servProgTargetSize = cntTargetSyncds.getCntTargetSyncByCond(servChannelTarget).size();
                        if (servProgTargetSize < 1)
                        {
                            servChannelTarget.setParentid(service.getServiceid());
                            servChannelTarget.setElementid(servChannMap.getChannelid());
                            servChannelTarget.setStatus(0);
                            servChannelTarget.setOperresult(0);
                            cntTargetSyncds.insertCntTargetSync(servChannelTarget);
                        }
                    }
                    // 根据当前服务与内容的共同平台循环添加绑定结束

                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(service.getServiceid()+","+cmsChannelBind.getChannelid(), CommonLogConstant.MGTTYPE_SERVICE,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_CHANNEL,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_CHANNEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                    // 返回信息
                    iSuccessed++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cmsChannelBind.getChannelid(), ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_SUCC)); // 内容绑定成功
                    returnInfo.appendOperateInfo(operateInfo);

                }

            }
            else
            {
                operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                        .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WRONG_CNT_TYPE));
                returnInfo.appendOperateInfo(operateInfo);
                flag = GlobalConstants.FAIL; // "1"
                errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
                returnInfo.setFlag(flag);
                returnInfo.setReturnMessage(errorcode);
                return returnInfo.toString();
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        if (ifail == cntIdxList.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < cntIdxList.length)
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
            // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publish delColumn end...");
        return returnInfo.toString();

    }

    // 批量删除绑定关系
    /**
     * @mapIndexAll 确认删除map关系的所有mapindex拼接成的字符串
     * @param bindCntType 想要删除的绑定关系的内容的类型
     */
    public String removeServiceBindMapList(String mapIndexAll, int bindCntType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 删除失败的个数
        Integer iSuccessed = 0; // 删除成功的个数
        int index = 1;

        String mapIdxArray[] = mapIndexAll.split(":");
        if (mapIdxArray.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_NO_MAP_CHOSEN_TO_DEL)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        try
        {
            // 判断内容的类型
            if (bindCntType == 1) // 连续剧
            {
                for (int cntIdx = 0; cntIdx < mapIdxArray.length; cntIdx++)
                {
                    ServiceSeriesMap servSeriesMapDel = new ServiceSeriesMap();
                    servSeriesMapDel.setMapindex(Long.valueOf(mapIdxArray[cntIdx]));
                    try
                    {
                        servSeriesMapDel = serviceseriesmapds.getServiceSeriesMap(servSeriesMapDel);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.getMessage());
                    }

                    if (null == servSeriesMapDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 删除绑定关系表，先判断状态能否删除

                    Service currentService = new Service();// 当前服务
                    List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                    CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                    List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                    CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                    boolean isCanDel = true;// 发布状态是否允许删除

                    // 检测服务
                    currentService = new Service();
                    currentService.setServiceindex(servSeriesMapDel.getServiceindex());
                    currentService = serviceds.getService(currentService);
                    if (null == currentService)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS)); // 服务不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 检测总发布状态状态
                    currentCntPlatformSync.setObjecttype(22);
                    currentCntPlatformSync.setParentid(servSeriesMapDel.getServiceid());
                    currentCntPlatformSync.setElementid(servSeriesMapDel.getSeriesid());

                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    for (int i = 0; i < currentPlatformSyncList.size(); i++)
                    {
                        currentCntPlatformSync = currentPlatformSyncList.get(i);
                        int curPlatStatus = currentCntPlatformSync.getStatus();
                        if (curPlatStatus == 0 || curPlatStatus == 400)
                        {
                            // 检测网元发布状态
                            currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                            currentCntTargetSync.setObjecttype(22);

                            currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                            for (int j = 0; j < currentTargetSyncList.size(); j++)
                            {
                                currentCntTargetSync = currentTargetSyncList.get(j);
                                int curTargetStatus = currentCntTargetSync.getStatus();
                                if (curTargetStatus != 0 && curTargetStatus != 400)
                                {
                                    isCanDel = false;
                                    break;
                                }
                            }
                            if (!isCanDel)
                            {
                                break;
                            }
                        }
                        else
                        {
                            isCanDel = false;
                            break;
                        }
                    }

                    // 状态满足条件才可以删除
                    if (!isCanDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB)); // 状态必须为待发布或者取消发布成功才可以删除
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        // 开始删除

                        // 删除主发布平台mapping
                        currentCntPlatformSync = new CntPlatformSync();
                        currentCntPlatformSync.setObjecttype(22);
                        currentCntPlatformSync.setObjectindex(servSeriesMapDel.getMapindex());
                        currentCntPlatformSync.setObjectid(servSeriesMapDel.getMappingid());
                        currentCntPlatformSync.setParentid(servSeriesMapDel.getServiceid());
                        currentCntPlatformSync.setElementid(servSeriesMapDel.getSeriesid());

                        currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                        cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);

                        // 删除网元发布平台mapping
                        currentCntTargetSync = new CntTargetSync();
                        currentCntTargetSync.setObjecttype(22);
                        currentCntTargetSync.setObjectindex(servSeriesMapDel.getMapindex());
                        currentCntTargetSync.setObjectid(servSeriesMapDel.getMappingid());
                        currentCntTargetSync.setParentid(servSeriesMapDel.getServiceid());
                        currentCntTargetSync.setElementid(servSeriesMapDel.getSeriesid());

                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);

                        // 从服务绑定关系表中删除对应服务
                        serviceseriesmapds.removeServiceSeriesMap(servSeriesMapDel);

                        // 写日志
                        CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servSeriesMapDel.getSeriesid(),
                                CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_SERIES,
                                CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_SERIES_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), servSeriesMapDel.getSeriesid(),
                                ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_DEL_SUCC)); // 内容绑定删除成功

                        returnInfo.appendOperateInfo(operateInfo);
                    }
                }
                // 循环结束

            }
            else if (bindCntType == 2) // VOD
            {
                for (int cntIdx = 0; cntIdx < mapIdxArray.length; cntIdx++)
                {
                    ServiceProgramMap servPromMapDel = new ServiceProgramMap();
                    servPromMapDel.setMapindex(Long.valueOf(mapIdxArray[cntIdx]));
                    try
                    {
                        servPromMapDel = serviceprogrammapds.getServiceProgramMap(servPromMapDel);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.getMessage());
                    }

                    if (null == servPromMapDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    // 删除绑定关系表，先判断状态能否删除

                    Service currentService = new Service();// 当前服务
                    List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                    CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                    List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                    CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                    boolean isCanDel = true;// 发布状态是否允许删除

                    // 检测服务
                    currentService = new Service();
                    currentService.setServiceindex(servPromMapDel.getServiceindex());

                    currentService = serviceds.getService(currentService);
                    if (null == currentService)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 服务不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 检测总发布状态
                    currentCntPlatformSync.setObjecttype(21);
                    currentCntPlatformSync.setObjectindex(servPromMapDel.getMapindex());
                    currentCntPlatformSync.setElementid(servPromMapDel.getProgramid());

                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    for (int i = 0; i < currentPlatformSyncList.size(); i++)
                    {
                        currentCntPlatformSync = currentPlatformSyncList.get(i);
                        int curPlatStatus = currentCntPlatformSync.getStatus();
                        if (curPlatStatus == 0 || curPlatStatus == 400)
                        {
                            // 检测网元发布状态
                            currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                            currentCntTargetSync.setObjecttype(21);

                            currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                            for (int j = 0; j < currentTargetSyncList.size(); j++)
                            {
                                currentCntTargetSync = currentTargetSyncList.get(j);
                                int curTargetStatus = currentCntTargetSync.getStatus();
                                if (curTargetStatus != 0 && curTargetStatus != 400)
                                {
                                    isCanDel = false;
                                    break;
                                }
                            }
                            if (!isCanDel)
                            {
                                break;
                            }
                        }
                        else
                        {
                            isCanDel = false;
                            break;
                        }
                    }

                    // 状态满足条件才可以删除
                    if (!isCanDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB)); // 状态必须为待发布或者取消发布成功才可以删除
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        // 开始删除

                        // 删除主发布平台mapping
                        currentCntPlatformSync = new CntPlatformSync();
                        currentCntPlatformSync.setObjecttype(21);
                        currentCntPlatformSync.setObjectindex(servPromMapDel.getMapindex());
                        currentCntPlatformSync.setObjectid(servPromMapDel.getMappingid());
                        currentCntPlatformSync.setParentid(servPromMapDel.getServiceid());
                        currentCntPlatformSync.setElementid(servPromMapDel.getProgramid());

                        currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                        cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);

                        // 删除网元发布平台mapping
                        currentCntTargetSync = new CntTargetSync();
                        currentCntTargetSync.setObjecttype(21);
                        currentCntTargetSync.setObjectindex(servPromMapDel.getMapindex());
                        currentCntTargetSync.setObjectid(servPromMapDel.getMappingid());
                        currentCntTargetSync.setParentid(servPromMapDel.getServiceid());
                        currentCntTargetSync.setElementid(servPromMapDel.getProgramid());

                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);

                        // 从服务绑定关系表中删除对应服务
                        serviceprogrammapds.removeServiceProgramMap(servPromMapDel);
                        // 写日志
                        CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servPromMapDel.getProgramid(),
                                CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_PROGRAM,
                                CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_PROGRAM_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), servPromMapDel.getProgramid(),
                                ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_DEL_SUCC)); // 内容绑定删除成功

                        returnInfo.appendOperateInfo(operateInfo);
                    }
                }
                // 循环结束
            }
            else if (bindCntType == 3) // 直播
            {
                for (int cntIdx = 0; cntIdx < mapIdxArray.length; cntIdx++)
                {
                    ServiceChannelMap servChannMapDel = new ServiceChannelMap();
                    servChannMapDel.setMapindex(Long.valueOf(mapIdxArray[cntIdx]));
                    try
                    {
                        servChannMapDel = servicechannelmapds.getServiceChannelMap(servChannMapDel);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.getMessage());
                    }

                    if (null == servChannMapDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 删除绑定关系表，先判断状态能否删除

                    Service currentService = new Service();// 当前服务
                    List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                    CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                    List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                    CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                    boolean isCanDel = true;// 发布状态是否允许删除

                    // 检测服务
                    currentService = new Service();
                    currentService.setServiceindex(servChannMapDel.getServiceindex());
                    currentService = serviceds.getService(currentService);
                    if (null == currentService)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 服务不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    // 检测总发布状态状态
                    currentCntPlatformSync.setObjecttype(23);
                    currentCntPlatformSync.setParentid(servChannMapDel.getServiceid());
                    currentCntPlatformSync.setElementid(servChannMapDel.getChannelid());

                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    for (int i = 0; i < currentPlatformSyncList.size(); i++)
                    {
                        currentCntPlatformSync = currentPlatformSyncList.get(i);
                        int curPlatStatus = currentCntPlatformSync.getStatus();
                        if (curPlatStatus == 0 || curPlatStatus == 400)
                        {
                            // 检测网元发布状态
                            currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                            currentCntTargetSync.setObjecttype(23);

                            currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                            for (int j = 0; j < currentTargetSyncList.size(); j++)
                            {
                                currentCntTargetSync = currentTargetSyncList.get(j);
                                int curTargetStatus = currentCntTargetSync.getStatus();
                                if (curTargetStatus != 0 && curTargetStatus != 400)
                                {
                                    isCanDel = false;
                                    break;
                                }
                            }
                            if (!isCanDel)
                            {
                                break;
                            }
                        }
                        else
                        {
                            isCanDel = false;
                            break;
                        }
                    }

                    // 状态满足条件才可以删除
                    if (!isCanDel)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB)); // 状态必须为待发布或者取消发布成功才可以删除
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        // 开始删除
                        // 删除主发布平台mapping
                        currentCntPlatformSync = new CntPlatformSync();
                        currentCntPlatformSync.setObjecttype(23);
                        currentCntPlatformSync.setObjectindex(servChannMapDel.getMapindex());
                        currentCntPlatformSync.setObjectid(servChannMapDel.getMappingid());
                        currentCntPlatformSync.setParentid(servChannMapDel.getServiceid());
                        currentCntPlatformSync.setElementid(servChannMapDel.getChannelid());

                        currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                        cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);

                        // 删除网元发布平台mapping
                        currentCntTargetSync = new CntTargetSync();
                        currentCntTargetSync.setObjecttype(23);
                        currentCntTargetSync.setObjectindex(servChannMapDel.getMapindex());
                        currentCntTargetSync.setObjectid(servChannMapDel.getMappingid());
                        currentCntTargetSync.setParentid(servChannMapDel.getServiceid());
                        currentCntTargetSync.setElementid(servChannMapDel.getChannelid());

                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);

                        // 从服务绑定关系表中删除对应服务
                        servicechannelmapds.removeServiceChannelMap(servChannMapDel);

                        // 写日志
                        CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servChannMapDel.getChannelid(),
                                CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_CHANNEL,
                                CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_CHANNEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), servChannMapDel.getChannelid(),
                                ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_CNT_DEL_SUCC)); // 内容绑定删除成功

                        returnInfo.appendOperateInfo(operateInfo);
                    }
                }
                // 循环结束
            }
            else
            // 内容类型不为4种类型中的任何一种，报错
            {
                operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                        .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                        .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WRONG_CNT_TYPE)); // 没有绑定关系被选中
                returnInfo.appendOperateInfo(operateInfo);
                flag = GlobalConstants.FAIL; // "1"
                errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
                returnInfo.setFlag(flag);
                returnInfo.setReturnMessage(errorcode);
                return returnInfo.toString();
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        if (ifail == mapIdxArray.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < mapIdxArray.length)
        {
            flag = "0";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
            // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publish delColumn end...");
        return returnInfo.toString();

    }

    
 // 单个删除绑定关系
    /***
     * 
     */
    public String removeServiceBindMap(String mapIndex, int bindCntType) throws Exception{
     // 判断内容的类型
        try
        {
            if (bindCntType == 1) // 连续剧
            {
                ServiceSeriesMap servSeriesMapDel = new ServiceSeriesMap();
                servSeriesMapDel.setMapindex(Long.valueOf(Long.valueOf(mapIndex)));
                try
                {
                    servSeriesMapDel = serviceseriesmapds.getServiceSeriesMap(servSeriesMapDel);
                }
                catch (Exception e)
                {
                    throw new Exception(e.getMessage());
                }
                if (null == servSeriesMapDel)
                {
                    return  "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS);
                }
                
             // 删除绑定关系表，先判断状态能否删除
                Service currentService = new Service();// 当前服务
                List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                boolean isCanDel = true;// 发布状态是否允许删除
    
                // 检测服务
                currentService = new Service();
                currentService.setServiceindex(servSeriesMapDel.getServiceindex());
                currentService = serviceds.getService(currentService);
                if (null == currentService)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);
                }
                // 检测总发布状态状态
                currentCntPlatformSync.setObjecttype(22);
                currentCntPlatformSync.setParentid(servSeriesMapDel.getServiceid());
                currentCntPlatformSync.setElementid(servSeriesMapDel.getSeriesid());
    
                currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                for (int i = 0; i < currentPlatformSyncList.size(); i++)
                {
                    currentCntPlatformSync = currentPlatformSyncList.get(i);
                    int curPlatStatus = currentCntPlatformSync.getStatus();
                    if (curPlatStatus == 0 || curPlatStatus == 400)
                    {
                        // 检测网元发布状态
                        currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                        currentCntTargetSync.setObjecttype(22);
    
                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        for (int j = 0; j < currentTargetSyncList.size(); j++)
                        {
                            currentCntTargetSync = currentTargetSyncList.get(j);
                            int curTargetStatus = currentCntTargetSync.getStatus();
                            if (curTargetStatus != 0 && curTargetStatus != 400)
                            {
                                isCanDel = false;
                                break;
                            }
                        }
                        if (!isCanDel)
                        {
                            break;
                        }
                    }
                    else
                    {
                        isCanDel = false;
                        break;
                    }
                }
                if (!isCanDel)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB);
                }
                else
                {
                    // 开始删除
    
                    // 删除主发布平台mapping
                    currentCntPlatformSync = new CntPlatformSync();
                    currentCntPlatformSync.setObjecttype(22);
                    currentCntPlatformSync.setObjectindex(servSeriesMapDel.getMapindex());
                    currentCntPlatformSync.setObjectid(servSeriesMapDel.getMappingid());
                    currentCntPlatformSync.setParentid(servSeriesMapDel.getServiceid());
                    currentCntPlatformSync.setElementid(servSeriesMapDel.getSeriesid());
    
                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);
    
                    // 删除网元发布平台mapping
                    currentCntTargetSync = new CntTargetSync();
                    currentCntTargetSync.setObjecttype(22);
                    currentCntTargetSync.setObjectindex(servSeriesMapDel.getMapindex());
                    currentCntTargetSync.setObjectid(servSeriesMapDel.getMappingid());
                    currentCntTargetSync.setParentid(servSeriesMapDel.getServiceid());
                    currentCntTargetSync.setElementid(servSeriesMapDel.getSeriesid());
    
                    currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                    cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);
    
                    // 从服务绑定关系表中删除对应服务
                    serviceseriesmapds.removeServiceSeriesMap(servSeriesMapDel);
    
                    // 写日志
                    CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servSeriesMapDel.getSeriesid(),
                            CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_SERIES,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_SERIES_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
    
                    // 返回信息
                    return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_OPERATE_SUCCESS);
                }
            }
            else if(bindCntType == 2)// VOD
            {
                ServiceProgramMap servPromMapDel = new ServiceProgramMap();
                servPromMapDel.setMapindex(Long.valueOf(Long.valueOf(mapIndex)));
                try
                {
                    servPromMapDel = serviceprogrammapds.getServiceProgramMap(servPromMapDel);
                }
                catch (Exception e)
                {
                    throw new Exception(e.getMessage());
                }
    
                if (null == servPromMapDel)
                {
                    return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS);
                }
             // 删除绑定关系表，先判断状态能否删除
    
                Service currentService = new Service();// 当前服务
                List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                boolean isCanDel = true;// 发布状态是否允许删除
    
                // 检测服务
                currentService = new Service();
                currentService.setServiceindex(servPromMapDel.getServiceindex());
    
                currentService = serviceds.getService(currentService);
                if (null == currentService)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);
                }
             // 检测总发布状态
                currentCntPlatformSync.setObjecttype(21);
                currentCntPlatformSync.setObjectindex(servPromMapDel.getMapindex());
                currentCntPlatformSync.setElementid(servPromMapDel.getProgramid());
    
                currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                for (int i = 0; i < currentPlatformSyncList.size(); i++)
                {
                    currentCntPlatformSync = currentPlatformSyncList.get(i);
                    int curPlatStatus = currentCntPlatformSync.getStatus();
                    if (curPlatStatus == 0 || curPlatStatus == 400)
                    {
                        // 检测网元发布状态
                        currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                        currentCntTargetSync.setObjecttype(21);
    
                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        for (int j = 0; j < currentTargetSyncList.size(); j++)
                        {
                            currentCntTargetSync = currentTargetSyncList.get(j);
                            int curTargetStatus = currentCntTargetSync.getStatus();
                            if (curTargetStatus != 0 && curTargetStatus != 400)
                            {
                                isCanDel = false;
                                break;
                            }
                        }
                        if (!isCanDel)
                        {
                            break;
                        }
                    }
                    else
                    {
                        isCanDel = false;
                        break;
                    }
                }
                // 状态满足条件才可以删除
                if (!isCanDel)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB);
                }
                else
                {
                    // 开始删除
    
                    // 删除主发布平台mapping
                    currentCntPlatformSync = new CntPlatformSync();
                    currentCntPlatformSync.setObjecttype(21);
                    currentCntPlatformSync.setObjectindex(servPromMapDel.getMapindex());
                    currentCntPlatformSync.setObjectid(servPromMapDel.getMappingid());
                    currentCntPlatformSync.setParentid(servPromMapDel.getServiceid());
                    currentCntPlatformSync.setElementid(servPromMapDel.getProgramid());
    
                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);
    
                    // 删除网元发布平台mapping
                    currentCntTargetSync = new CntTargetSync();
                    currentCntTargetSync.setObjecttype(21);
                    currentCntTargetSync.setObjectindex(servPromMapDel.getMapindex());
                    currentCntTargetSync.setObjectid(servPromMapDel.getMappingid());
                    currentCntTargetSync.setParentid(servPromMapDel.getServiceid());
                    currentCntTargetSync.setElementid(servPromMapDel.getProgramid());
    
                    currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                    cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);
    
                    // 从服务绑定关系表中删除对应服务
                    serviceprogrammapds.removeServiceProgramMap(servPromMapDel);
                    // 写日志
                    CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servPromMapDel.getProgramid(),
                            CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_PROGRAM,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_PROGRAM_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
    
                    // 返回信息
                    return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_OPERATE_SUCCESS);
                }
                
            }
            else if(bindCntType == 3)// 直播
            {
                ServiceChannelMap servChannMapDel = new ServiceChannelMap();
                servChannMapDel.setMapindex(Long.valueOf(Long.valueOf(mapIndex)));
                try
                {
                    servChannMapDel = servicechannelmapds.getServiceChannelMap(servChannMapDel);
                }
                catch (Exception e)
                {
                    throw new Exception(e.getMessage());
                }
    
                if (null == servChannMapDel)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS);
                }
                
             // 删除绑定关系表，先判断状态能否删除
                Service currentService = new Service();// 当前服务
                List<CntPlatformSync> currentPlatformSyncList = null; // 当前发布平台列表
                CntPlatformSync currentCntPlatformSync = new CntPlatformSync();// 当前发布平台状态
                List<CntTargetSync> currentTargetSyncList = null; // 当前发布网元列表
                CntTargetSync currentCntTargetSync = new CntTargetSync();// 当前发布网元状态
                boolean isCanDel = true;// 发布状态是否允许删除
    
                // 检测服务
                currentService = new Service();
                currentService.setServiceindex(servChannMapDel.getServiceindex());
                currentService = serviceds.getService(currentService);
                if (null == currentService)
                {
                    return "1:"+ ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_4UPDATE_DOESNOT_EXIST);
                }
                
             // 检测总发布状态状态
                currentCntPlatformSync.setObjecttype(23);
                currentCntPlatformSync.setParentid(servChannMapDel.getServiceid());
                currentCntPlatformSync.setElementid(servChannMapDel.getChannelid());
    
                currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                for (int i = 0; i < currentPlatformSyncList.size(); i++)
                {
                    currentCntPlatformSync = currentPlatformSyncList.get(i);
                    int curPlatStatus = currentCntPlatformSync.getStatus();
                    if (curPlatStatus == 0 || curPlatStatus == 400)
                    {
                        // 检测网元发布状态
                        currentCntTargetSync.setRelateindex(currentCntPlatformSync.getSyncindex());
                        currentCntTargetSync.setObjecttype(23);
    
                        currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                        for (int j = 0; j < currentTargetSyncList.size(); j++)
                        {
                            currentCntTargetSync = currentTargetSyncList.get(j);
                            int curTargetStatus = currentCntTargetSync.getStatus();
                            if (curTargetStatus != 0 && curTargetStatus != 400)
                            {
                                isCanDel = false;
                                break;
                            }
                        }
                        if (!isCanDel)
                        {
                            break;
                        }
                    }
                    else
                    {
                        isCanDel = false;
                        break;
                    }
                }
                
             // 状态满足条件才可以删除
                if (!isCanDel)
                {
                    return "1:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB);
                }
                else
                {
                    // 开始删除
                    // 删除主发布平台mapping
                    currentCntPlatformSync = new CntPlatformSync();
                    currentCntPlatformSync.setObjecttype(23);
                    currentCntPlatformSync.setObjectindex(servChannMapDel.getMapindex());
                    currentCntPlatformSync.setObjectid(servChannMapDel.getMappingid());
                    currentCntPlatformSync.setParentid(servChannMapDel.getServiceid());
                    currentCntPlatformSync.setElementid(servChannMapDel.getChannelid());
    
                    currentPlatformSyncList = cntPlatformSyncds.getCntPlatformSyncByCond(currentCntPlatformSync);
                    cntPlatformSyncds.removeCntPlatformSyncList(currentPlatformSyncList);
    
                    // 删除网元发布平台mapping
                    currentCntTargetSync = new CntTargetSync();
                    currentCntTargetSync.setObjecttype(23);
                    currentCntTargetSync.setObjectindex(servChannMapDel.getMapindex());
                    currentCntTargetSync.setObjectid(servChannMapDel.getMappingid());
                    currentCntTargetSync.setParentid(servChannMapDel.getServiceid());
                    currentCntTargetSync.setElementid(servChannMapDel.getChannelid());
    
                    currentTargetSyncList = cntTargetSyncds.getCntTargetSyncByCond(currentCntTargetSync);
                    cntTargetSyncds.removeCntTargetSyncList(currentTargetSyncList);
    
                    // 从服务绑定关系表中删除对应服务
                    servicechannelmapds.removeServiceChannelMap(servChannMapDel);
    
                    // 写日志
                    CommonLogUtil.insertOperatorLog(currentService.getServiceid()+","+servChannMapDel.getChannelid(),
                            CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_CHANNEL,
                            CommonLogConstant.OPERTYPE_SERVICE_BIND_DEL_CHANNEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
    
                    // 返回信息
                    return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_OPERATE_SUCCESS);
                }
            }
            return "0:"+ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WRONG_CNT_TYPE);
        }
        catch (Exception e5)
        {
            // TODO: handle exception
            throw new Exception(e5.getMessage());
        }
    }
    
    /**
     * 发布服务和内容的绑定关系
     * 
     * @param servCntBindMapIndexAll 选中的想要发布的所有的绑定关系
     * @param bindCntType 绑定的内容类型
     * @return
     * @throws Exception
     */
    public String publishServiceBindMap(String servCntBindMapIndexAll, int bindCntType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int index = 0;

        String arrayMapIdx[] = servCntBindMapIndexAll.split(":");

        // 如果没有绑定关系被选中，写返回信息
        if (arrayMapIdx.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_NO_MAP_CHOSEN)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取目标系统的index
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }

        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取临时取地址以及同步XML文件FTP地址
        String tempAreaPath = getTempAreAdd();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 获取临时区失败
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        int mapSize = arrayMapIdx.length;

        try
        {
            if (bindCntType == 1) // 连续剧
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceSeriesMap servSeriMap = new ServiceSeriesMap();
                    servSeriMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servSeriMap = serviceseriesmapds.getServiceSeriesMap(servSeriMap);
                    if (null == servSeriMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servSeriMap.getStatus() != 0// 待发布
                                && servSeriMap.getStatus() != 30// 新增同步失败
                                && servSeriMap.getStatus() != 80// 取消同步成功
                                && servSeriMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(
                                    String.valueOf(index++),
                                    servSeriMap.getMapindex().toString(),
                                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                                    ResourceMgt
                                            .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                            if (servSeriMap.getStatus() == 0 || servSeriMap.getStatus() == 80)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlSeries(servSeriMap, tempAreaPath, "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);         
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servSeriMap.setStatus(10);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servSeriMap.getMapindex());
                                cntSyncRecord.setObjtype(22);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int cntSyncRecordbmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servSeriMap.getMapindex());
                                cntSyncRecordepg.setObjtype(22);
                                cntSyncRecordepg.setDesttype(2); // EPG

                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int cntSyncRecordepgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((cntSyncRecordbmsSize > 0) && (cntSyncRecordepgSize > 0))
                                {
                                    servSeriMap.setStatus(20); // 新增同步成功
                                }
                                else if ((0 == cntSyncRecordbmsSize) && (cntSyncRecordepgSize > 0))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlSeries(servSeriMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);                                
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSeriMap.setStatus(10); // 新增同步中

                                }
                                else if ((cntSyncRecordbmsSize > 0) && (cntSyncRecordepgSize == 0))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlSeries(servSeriMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSeriMap.setStatus(10); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlSeries(servSeriMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servSeriMap.setStatus(10);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servSeriMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servSeriMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceseriesmapds.updateServiceSeriesMap(servSeriMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else if (bindCntType == 2) // vod
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceProgramMap servProgMap = new ServiceProgramMap();
                    servProgMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servProgMap = serviceprogrammapds.getServiceProgramMap(servProgMap);
                    if (null == servProgMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servProgMap.getStatus() != 0// 待发布
                                && servProgMap.getStatus() != 30// 新增同步失败
                                && servProgMap.getStatus() != 80// 取消同步成功
                                && servProgMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(
                                    String.valueOf(index++),
                                    servProgMap.getMapindex().toString(),
                                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                                    ResourceMgt
                                            .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                            if (servProgMap.getStatus() == 0 || servProgMap.getStatus() == 80)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlProgram(servProgMap, tempAreaPath, "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servProgMap.setStatus(10);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servProgMap.getMapindex());
                                cntSyncRecord.setObjtype(21);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servProgMap.getMapindex());
                                cntSyncRecordepg.setObjtype(21);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();

                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((bmsSize > 0) && (epgSize > 0))
                                {
                                    servProgMap.setStatus(20); // 新增同步成功
                                }
                                else if ((null == cntSyncRecord) && (null != cntSyncRecordepg))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlProgram(servProgMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servProgMap.setStatus(10); // 新增同步中

                                }
                                else if ((bmsSize > 0) && (epgSize == 0))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlProgram(servProgMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servProgMap.setStatus(10); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlProgram(servProgMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servProgMap.setStatus(10);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servProgMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servProgMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceprogrammapds.updateServiceProgramMap(servProgMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else if (bindCntType == 3) // 直播
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceChannelMap servChannMap = new ServiceChannelMap();
                    servChannMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servChannMap = servicechannelmapds.getServiceChannelMap(servChannMap);
                    if (null == servChannMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servChannMap.getStatus() != 0// 待发布
                                && servChannMap.getStatus() != 30// 新增同步失败
                                && servChannMap.getStatus() != 80// 取消同步成功
                                && servChannMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(String.valueOf(index++), servChannMap.getMapindex()
                                    .toString(), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                            if (servChannMap.getStatus() == 0 || servChannMap.getStatus() == 80)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlProgram(servChannMap, tempAreaPath,
                                        "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);     
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servChannMap.setStatus(10);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servChannMap.getMapindex());
                                cntSyncRecord.setObjtype(23);
                                cntSyncRecord.setDesttype(1);

                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servChannMap.getMapindex());
                                cntSyncRecordepg.setObjtype(23);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((bmsSize > 0) && (epgSize > 0))
                                {
                                    servChannMap.setStatus(20); // 新增同步成功
                                }
                                else if ((bmsSize == 0) && (epgSize > 0))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlProgram(servChannMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servChannMap.setStatus(10); // 新增同步中

                                }
                                else if ((bmsSize > 0) && (epgSize == 0))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlProgram(servChannMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servChannMap.setStatus(10); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlProgram(servChannMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servChannMap.setStatus(10);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servChannMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servChannMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            servicechannelmapds.updateServiceChannelMap(servChannMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else
            // 节目
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceSchedualMap servSchedualMap = new ServiceSchedualMap();
                    servSchedualMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servSchedualMap = serviceschedulemapds.getServiceSchedualMap(servSchedualMap);
                    if (null == servSchedualMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servSchedualMap.getStatus() != 0// 待发布
                                && servSchedualMap.getStatus() != 30// 新增同步失败
                                && servSchedualMap.getStatus() != 80// 取消同步成功
                                && servSchedualMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(String.valueOf(index++), servSchedualMap.getMapindex()
                                    .toString(), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                            if (servSchedualMap.getStatus() == 0 || servSchedualMap.getStatus() == 80)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                        "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servSchedualMap.setStatus(10);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servSchedualMap.getMapindex());
                                cntSyncRecord.setObjtype(24);
                                cntSyncRecord.setDesttype(1);

                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servSchedualMap.getMapindex());
                                cntSyncRecordepg.setObjtype(24);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((bmsSize > 0) && (epgSize > 0))
                                {
                                    servSchedualMap.setStatus(20); // 新增同步成功
                                }
                                else if ((0 == epgSize) && (0 < epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSchedualMap.setStatus(10); // 新增同步中

                                }
                                else if ((0 < epgSize) && (0 == epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSchedualMap.setStatus(10); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
   
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servSchedualMap.setStatus(10);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servSchedualMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servSchedualMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceschedulemapds.updateServiceSchedualMap(servSchedualMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        if (ifail == arrayMapIdx.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < arrayMapIdx.length)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
            // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publishColumn end...");
        return returnInfo.toString();

    }

    /**
     * 取消绑定关系发布
     * 
     * @param servCntBindMapIndexAll
     * @param bindCntType
     * @return
     * @throws Exception
     */
    public String cancelPublishServiceBindMap(String servCntBindMapIndexAll, int bindCntType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // map取消发布失败个数
        Integer iSuccessed = 0;
        int index = 0;

        String arrayMapIdx[] = servCntBindMapIndexAll.split(":");

        // 如果没有绑定关系被选中，写返回信息
        if (arrayMapIdx.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_NO_MAP_CHOSEN)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取目标系统的index
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }

        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取临时取地址以及同步XML文件FTP地址
        String tempAreaPath = getTempAreAdd();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 获取临时区失败
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        int mapSize = arrayMapIdx.length;

        try
        {
            if (bindCntType == 1) // 连续剧
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceSeriesMap servSeriMap = new ServiceSeriesMap();
                    servSeriMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servSeriMap = serviceseriesmapds.getServiceSeriesMap(servSeriMap);
                    if (null == servSeriMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servSeriMap.getStatus() != 20// 发布成功
                                && servSeriMap.getStatus() != 30// 新增同步失败
                                && servSeriMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(
                                    String.valueOf(index++),
                                    servSeriMap.getMapindex().toString(),
                                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                                    ResourceMgt
                                            .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以取消发布
                        {
                            // 判断绑定关系状态，如果为新增同步成功,向分发任务表中插入两条记录
                            if (servSeriMap.getStatus() == 20)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlSeries(servSeriMap, tempAreaPath, "DELETE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
            
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
   
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);                             
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servSeriMap.setStatus(70);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servSeriMap.getMapindex());
                                cntSyncRecord.setObjtype(22);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> ntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = ntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servSeriMap.getMapindex());
                                cntSyncRecordepg.setObjtype(22);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((0 == bmsSize) && (0 == epgSize))
                                {
                                    servSeriMap.setStatus(80); // 取消发布成功
                                }
                                else if ((0 == bmsSize) && (0 < epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlSeries(servSeriMap, tempAreaPath, "DELETE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);

                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
            
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSeriMap.setStatus(70); // 新增同步中

                                }
                                else if ((0 < bmsSize) && (0 == epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlSeries(servSeriMap, tempAreaPath, "DELETE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);

                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
         
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSeriMap.setStatus(70); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlSeries(servSeriMap, tempAreaPath, "DELETE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);

                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);

                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servSeriMap.setStatus(70);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servSeriMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servSeriMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC)); // 取消发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceseriesmapds.updateServiceSeriesMap(servSeriMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else if (bindCntType == 2) // vod
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceProgramMap servProgMap = new ServiceProgramMap();
                    servProgMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servProgMap = serviceprogrammapds.getServiceProgramMap(servProgMap);
                    if (null == servProgMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servProgMap.getStatus() != 20// 待发布
                                && servProgMap.getStatus() != 30// 新增同步失败
                                && servProgMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(
                                    String.valueOf(index++),
                                    servProgMap.getMapindex().toString(),
                                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                                    ResourceMgt
                                            .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以取消发布
                        {
                            // 判断绑定关系状态，如果新增同步成功，向分发任务表中插入两条记录
                            if (servProgMap.getStatus() == 20)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlProgram(servProgMap, tempAreaPath, "DELETE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                               
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                           
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);
                               
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servProgMap.setStatus(70);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servProgMap.getMapindex());
                                cntSyncRecord.setObjtype(21);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servProgMap.getMapindex());
                                cntSyncRecordepg.setObjtype(21);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((0 == bmsSize) && (0 == epgSize))
                                {
                                    servProgMap.setStatus(80); // 新增同步成功
                                }
                                else if ((0 == bmsSize) && (0 < epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlProgram(servProgMap, tempAreaPath,
                                            "REGIST or UPDATE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
             
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                           
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servProgMap.setStatus(70); // 新增同步中

                                }
                                else if ((0 < bmsSize) && (0 == epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlProgram(servProgMap, tempAreaPath, "DELETE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                   
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                        
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servProgMap.setStatus(10); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlProgram(servProgMap, tempAreaPath,
                                            "DELETE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                   
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                        
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                   
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servProgMap.setStatus(70);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servProgMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servProgMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC)); // 取消发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceprogrammapds.updateServiceProgramMap(servProgMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else if (bindCntType == 3) // 直播
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceChannelMap servChannMap = new ServiceChannelMap();
                    servChannMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servChannMap = servicechannelmapds.getServiceChannelMap(servChannMap);
                    if (null == servChannMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servChannMap.getStatus() != 20// 待发布
                                && servChannMap.getStatus() != 30// 新增同步失败
                                && servChannMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(String.valueOf(index++), servChannMap.getMapindex()
                                    .toString(), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为新增同步成功,向分发任务表中插入两条记录
                            if (servChannMap.getStatus() == 20)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlProgram(servChannMap, tempAreaPath, "DELETE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);

                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                    
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                                
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);
                                
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servChannMap.setStatus(70);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servChannMap.getMapindex());
                                cntSyncRecord.setObjtype(23);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servChannMap.getMapindex());
                                cntSyncRecordepg.setObjtype(23);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((0 == bmsSize) && (0 == epgSize))
                                {
                                    servChannMap.setStatus(80); // 取消发布成功
                                }
                                else if ((0 == bmsSize) && (0 < epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlProgram(servChannMap, tempAreaPath, "DELETE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);

                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                                   
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servChannMap.setStatus(70); // 新增同步中

                                }
                                else if ((0 < bmsSize) && (0 == epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlProgram(servChannMap, tempAreaPath, "DELETE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                   
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servChannMap.setStatus(70); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlProgram(servChannMap, tempAreaPath,
                                            "DELETE"); // 发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                  
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                   
                                   
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                   
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servChannMap.setStatus(70);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servChannMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servChannMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC)); // 取消发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            servicechannelmapds.updateServiceChannelMap(servChannMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
            else
            // 节目
            {
                for (int i = 0; i < mapSize; i++)
                {
                    ServiceSchedualMap servSchedualMap = new ServiceSchedualMap();
                    servSchedualMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                    servSchedualMap = serviceschedulemapds.getServiceSchedualMap(servSchedualMap);
                    if (null == servSchedualMap)
                    {

                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        if (servSchedualMap.getStatus() != 20// 待发布
                                && servSchedualMap.getStatus() != 30// 新增同步失败
                                && servSchedualMap.getStatus() != 90)// 取消同步失败
                        {
                            ifail++;
                            operateInfo = new OperateInfo(String.valueOf(index++), servSchedualMap.getMapindex()
                                    .toString(), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                            returnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        // 状态正常，可以发布
                        {
                            // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                            if (servSchedualMap.getStatus() == 20)
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName = createServiceXmlSchedule(servSchedualMap, tempAreaPath, "DELETE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName);
                                
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                               
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName);
                               
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                servSchedualMap.setStatus(70);

                            }
                            else
                            {
                                // 查询分发记录表
                                CntSyncRecord cntSyncRecord = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecord.setObjindex(servSchedualMap.getMapindex());
                                cntSyncRecord.setObjtype(24);
                                cntSyncRecord.setDesttype(1);
                                List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecord);
                                int bmsSize = cntSyncRecordList.size();
                                // 是否存在EPG
                                CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                                // 是否存在BMS记录
                                cntSyncRecordepg.setObjindex(servSchedualMap.getMapindex());
                                cntSyncRecordepg.setObjtype(24);
                                cntSyncRecordepg.setDesttype(2); // EPG
                                List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                        .getCntSyncRecordByCond(cntSyncRecordepg);
                                int epgSize = cntSyncRecordepgList.size();
                                // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                                if ((0 == bmsSize) && (0 == epgSize))
                                {
                                    servSchedualMap.setStatus(80); // 取消发布成功
                                }
                                else if ((0 == bmsSize) && (0 < epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bms = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "DELETE"); // 取消发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                   
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bms);
                      
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSchedualMap.setStatus(70); // 新增同步中

                                }
                                else if ((0 < bmsSize) && (0 == epgSize))
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4epg = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "DELETE"); // 发布
                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
       
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4epg);
                                    
                                    cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    servSchedualMap.setStatus(70); // 新增同步中
                                }
                                else
                                {
                                    // 生成服务xml，返回生成文件路径
                                    String fileName4bmsEpg = createServiceXmlSchedule(servSchedualMap, tempAreaPath,
                                            "DELETE"); // 取消发布

                                    // 获取任务流水号和流水批次号
                                    String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                            .toString();
                                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                            "ucdn_task_correlate_id").toString();

                                    CntSyncTask cntsyncTask = new CntSyncTask();
                                    cntsyncTask.setStatus(1);
                                    cntsyncTask.setPriority(2);
                                    
                                    cntsyncTask.setCorrelateid(correlateid);
                                    cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                       
                                    cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                    // 插入EPG的任务
                                    correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                            .toString();

                                    CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                    cntsyncTaskEPG.setStatus(1);
                                    cntsyncTaskEPG.setPriority(2);
                                    
                                    cntsyncTaskEPG.setCorrelateid(correlateid);
                                    cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                    
                                    cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                    cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                    servSchedualMap.setStatus(70);// 新增同步中

                                }
                            }
                            // 写操作日志
                            CommonLogUtil.insertOperatorLog(servSchedualMap.getMapindex().toString(),
                                    CommonLogConstant.MGTTYPE_SERVICE,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL,
                                    CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            // 返回信息
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(servSchedualMap
                                    .getMapindex()), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC)); // 发布成功
                            returnInfo.appendOperateInfo(operateInfo);

                            serviceschedulemapds.updateServiceSchedualMap(servSchedualMap); // 更新绑定关系的状态
                        }
                    }
                }

            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        if (ifail == arrayMapIdx.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < arrayMapIdx.length)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail; // "350098"
            // //"成功数量："
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publishColumn end...");
        return returnInfo.toString();

    }

    // 得到同步Series的xml的ftp地址
    private static final String XMLADDR = "xmlsync"; // 所有的xml文件都放在临时区的该目录下

    private String createServiceXmlSeries(
            ServiceSeriesMap servSeriMap, 
            String tempAddr, 
            String actionType)throws Exception
    {
        log.debug("create service xml starting...");
        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint(); // CMS
        String fileName = date.getTime() + "_service.xml"; // 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date()); // 文件夹

        String tempFilePath = tempAddr + File.separator + XMLADDR + File.separator + fileFolder4xml + File.separator
                + "service" + File.separator; // xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName; // synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        objectElement = objectsElement.addElement("Mapping");

        objectElement.addAttribute("ParentType", "Service");
        objectElement.addAttribute("ParentID", servSeriMap.getServiceid());
        objectElement.addAttribute("ElementType", "Series");
        objectElement.addAttribute("ElementID", servSeriMap.getSeriesid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (null == servSeriMap.getSequence())
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(servSeriMap.getSequence().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");
        propertyElement.addText("");

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");
        propertyElement.addText("");

        // objectid = objtype+mapindex，objtype占两位，不足补0，mapindex占30位，不足补0
        String objectid = String.format("%02d", 22)
            + String.format("%030d", servSeriMap.getMapindex());
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ObjectID");
        propertyElement.addText(objectid);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);

    }

    // 得到同步VOD的xml的ftp地址
    private String createServiceXmlProgram(ServiceProgramMap servProgramMap, String tempAddr, String actionType)
            throws Exception
    {
        log.debug("create service xml starting...");
        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint();// CMS
        String fileName = date.getTime() + "_service.xml";// 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date()); // 文件夹

        String tempFilePath = tempAddr + File.separator + XMLADDR + File.separator + fileFolder4xml + File.separator
                + "service" + File.separator;// xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName;// synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        objectElement = objectsElement.addElement("Mapping");

        objectElement.addAttribute("ParentType", "Service");
        objectElement.addAttribute("ParentID", servProgramMap.getServiceid());
        objectElement.addAttribute("ElementType", "Program");
        objectElement.addAttribute("ElementID", servProgramMap.getProgramid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (null == servProgramMap.getSequence())
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(servProgramMap.getSequence().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");
        propertyElement.addText("");

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");
        propertyElement.addText("");

        // objectid = objtype+mapindex，objtype占两位，不足补0，mapindex占30位，不足补0
        String objectid = String.format("%02d", 21)
            + String.format("%030d", servProgramMap.getMapindex());
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ObjectID");
        propertyElement.addText(objectid);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);

    }

    // 得到同步Channel的xml的ftp地址
    private String createServiceXmlProgram(ServiceChannelMap servChannMap, String tempAddr, String actionType)
            throws Exception
    {
        log.debug("create service xml starting...");
        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint();// CMS
        String fileName = date.getTime() + "_service.xml"; // 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date()); // 文件夹

        String tempFilePath = tempAddr + File.separator + XMLADDR + File.separator + fileFolder4xml + File.separator
                + "service" + File.separator; // xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName; // synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        objectElement = objectsElement.addElement("Mapping");

        objectElement.addAttribute("ParentType", "Service");
        objectElement.addAttribute("ParentID", servChannMap.getServiceid());
        objectElement.addAttribute("ElementType", "Channel");
        objectElement.addAttribute("ElementID", servChannMap.getChannelid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (null == servChannMap.getSequence())
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(servChannMap.getSequence().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");
        propertyElement.addText("");

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");
        propertyElement.addText("");

        // objectid = objtype+mapindex，objtype占两位，不足补0，mapindex占30位，不足补0
        String objectid = String.format("%02d", 23)
            + String.format("%030d", servChannMap.getMapindex());
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ObjectID");
        propertyElement.addText(objectid);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);

    }

    // 得到同步Schedule的xml的ftp地址
    private String createServiceXmlSchedule(ServiceSchedualMap servScheduleMap, String tempAddr, String actionType)
            throws Exception
    {
        log.debug("create service xml starting...");
        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint();// CMS
        String fileName = date.getTime() + "_service.xml";// 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date()); // 文件夹

        String tempFilePath = tempAddr + File.separator + XMLADDR + File.separator + fileFolder4xml + File.separator
                + "service" + File.separator;// xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName;// synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        objectElement = objectsElement.addElement("Mapping");

        objectElement.addAttribute("ParentType", "Service");
        objectElement.addAttribute("ParentID", servScheduleMap.getServiceid());
        objectElement.addAttribute("ElementType", "Schedule");
        objectElement.addAttribute("ElementID", servScheduleMap.getSchedualid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (null == servScheduleMap.getSequence())
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(servScheduleMap.getSequence().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");
        propertyElement.addText("");

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");
        propertyElement.addText("");

        // objectid = objtype+mapindex，objtype占两位，不足补0，mapindex占30位，不足补0
        String objectid = String.format("%02d", 24)
            + String.format("%030d", servScheduleMap.getMapindex());
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ObjectID");
        propertyElement.addText(objectid);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);

    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {
                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 获取临时取的地址
    private String getTempAreAdd() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1");// 临时区
        return stroageareaPath;
    }

    /**
     * 通过给定的内容，查询出改内容绑定的服务
     * 
     * @param service Service 带入查询条件 status为内容类型，corpmode内容名称 cntrange 内容编码
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws Exception Exception
     */

    public TableDataInfo serviceContentMapQuery(Service service, int start, int pageSize) throws Exception
    {
        log.debug("query services from content information given starting...");
        service.setCntrange(EspecialCharMgt.conversion(service.getCntrange()));
        service.setCorpmode(EspecialCharMgt.conversion(service.getCorpmode()));
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mapindex");
        try
        {
            tableInfo = serviceContentMapQuery(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query services from content information given ends...");

        return tableInfo;
    }
    
    /**
     * 通过给定的内容，查询出改内容绑定的服务
     * 
     * @param service Service
     * @param start int
     * @param pageSize int
     * @param  puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo serviceContentMapQuery(Service service, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception
    {
        log.debug("query services from content information given starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = serviceCntBindds.serviceCntMapQuery(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            throw new DomainServiceException(dsEx);
        }
        log.debug("query services from content information given ends...");
        return tableInfo;
    }

    /**
     * 
     * @param serviceprogrammapds IServiceProgramMapDS
     */
    public void setServiceprogrammapds(IServiceProgramMapDS serviceprogrammapds)
    {
        this.serviceprogrammapds = serviceprogrammapds;
    }

    /**
     * 
     * @param cmsProgramDS ICmsProgramDS
     */
    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    /**
     * 
     * @param servicechannelmapds IServiceChannelMapDS
     */
    public void setServicechannelmapds(IServiceChannelMapDS servicechannelmapds)
    {
        this.servicechannelmapds = servicechannelmapds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @param cmsCategoryDS ICategorycdnDS
     */
    public void setCmsCategoryDS(ICategorycdnDS cmsCategoryDS)
    {
        this.cmsCategoryDS = cmsCategoryDS;
    }

    /** *****************************下面为服务和栏目的关联关系方法************************************************* */
    /** ****************************************************************************************************** */
    /**
     * 查询和服务关联的栏目信息
     * 
     * @param service Service利用service类将查询条件传入后台
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize) throws Exception
    {
        log.debug("query bind relationship between serviec and content starting...");
        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("mapindex");

        try
        {
            tableInfo = pageInfoQueryServiceCategoryMap(service, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query bind relationship between serviec and content ends...");

        return tableInfo;
    }

    /**
     * 查询和服务绑定的内容信息
     * 
     * @param service Service利用service类将查询条件传入后台
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws Exception Exception
     */
    public TableDataInfo pageInfoQueryServiceCategoryMap(Service service, int start, int pageSize,
            PageUtilEntity puEntity) throws Exception
    {
        log.debug("get service page info by condition starting...");
        TableDataInfo tableInfo = null;
        try
        {
            tableInfo = serviceCntBindds.pageInfoQueryServiceCategoryMap(service, start, pageSize, puEntity);
        }
        catch (Exception dsEx)
        {
            log.error("ds exception:", dsEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(dsEx);
        }
        log.debug("get service page info by condition end");
        return tableInfo;
    }

    /**
     * 新增服务和栏目的绑定关系
     * 
     * @param serviceIdx String
     * @param categoryIdx String
     * @return String
     * @throws Exception Exception
     */
    public String addServiceCategoryMap(String serviceIdx, String categoryIdx) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        // 判断服务是否存在
        Service serviceCheck = new Service();
        serviceCheck.setServiceindex(Long.valueOf(serviceIdx));
        serviceCheck = serviceds.getService(serviceCheck);
        if (null == serviceCheck)
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CATEGORY_MAP_SERV_NO_EXISTS);
        }
        else if (serviceCheck.getStatus() != 20 
                && serviceCheck.getStatus() != 50 
                && serviceCheck.getStatus() != 60) // 判断服务的状态是否发布成功,以上几个状态均代表发布成功
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_STATUS_NOT_PUBLISHED);
        }
        // 判断栏目是否存在
        Categorycdn categoryCheck = new Categorycdn();
        categoryCheck.setCategoryindex(Long.valueOf(categoryIdx));
        categoryCheck = cmsCategoryDS.getCategorycdn(categoryCheck);
        if (null == categoryCheck)
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CATEGORY_MAP_CATE_NO_EXISTS);
        }

        // 判断栏目的状态是否为发布
        if (categoryCheck.getStatus() != 20 && categoryCheck.getStatus() != 50 && categoryCheck.getStatus() != 60)
        {
            return ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CATE_STATUS_NOT_PUBLISHED);
        }

        CategoryServiceMap categoryService = new CategoryServiceMap();
        categoryService.setCategoryid(categoryCheck.getCategoryid());
        categoryService.setCategoryindex(categoryCheck.getCategoryindex());
        categoryService.setServiceid(serviceCheck.getServiceid());
        categoryService.setServiceindex(serviceCheck.getServiceindex());
        categoryService.setStatus(0); // 待发布

        categoryServiceMapds.insertCategoryServiceMap(categoryService);

        // 写操作日志
        CommonLogUtil.insertOperatorLog(categoryCheck.getCategoryid(), CommonLogConstant.MGTTYPE_SERVICE,
                CommonLogConstant.OPERTYPE_SERVICE_CATE_MAP, CommonLogConstant.OPERTYPE_SERVICE_CATE_MAP_INFO,
                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        // 返回操作结果
        return ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERV_CATE_MAP_SUCCESSFUL);
    }

    /**
     * 批量删除服务和栏目的关联关系
     * 
     * @param mapindexAll String 想要删除的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String delServiceCategoryMap(String mapIndexAll) throws Exception
    {
        log.debug("remove serivce and category map starts...");
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 删除失败的个数
        Integer iSuccessed = 0; // 删除成功的个数
        int index = 1;

        String mapIdxArray[] = mapIndexAll.split(":");
        if (mapIdxArray.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_NO_MAP_CHOSEN_TO_DEL)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }
        try
        {
            for (int cntIdx = 0; cntIdx < mapIdxArray.length; cntIdx++)
            {
                CategoryServiceMap categoryServiceMapDel = new CategoryServiceMap();
                categoryServiceMapDel.setMapindex(Long.valueOf(mapIdxArray[cntIdx]));
                categoryServiceMapDel = categoryServiceMapds.getCategoryServiceMap(categoryServiceMapDel);

                if (null == categoryServiceMapDel)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                            .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_BIND_MAP_NO_EXISTS)); // 绑定关系不存在
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                // 删除绑定关系表，先判断状态能否删除
                {
                    // 状态只有为待发布和取消发布成功时，才可以删除
                    if (categoryServiceMapDel.getStatus() != 0 && categoryServiceMapDel.getStatus() != 80)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), mapIdxArray[cntIdx], ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_MAP_PUB)); // 状态必须为待发布或者取消发布成功才可以删除
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        categoryServiceMapds.removeCategoryServiceMap(categoryServiceMapDel);
                        // 写日志
                        CommonLogUtil.insertOperatorLog(categoryServiceMapDel.getCategoryid(),
                                CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_CATE_MAP_DEL,
                                CommonLogConstant.OPERTYPE_SERVICE_CATE_MAP_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), categoryServiceMapDel.getCategoryid(),
                                ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                        .findDefaultText(ServiceConstants.RES_UCDN_SERV_CATE_MAP_DEL_SUCC)); // 服务栏目关联关系删除成功

                        returnInfo.appendOperateInfo(operateInfo);

                    }

                }

            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        // 返回结果
        if (ifail == mapIdxArray.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail; // "失败数量："
        }
        else if (ifail > 0 && ifail < mapIdxArray.length)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail;
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("remove serivce and category map ends...");
        return returnInfo.toString();
    }

    /**
     * 批量发布服务和栏目的关联关系
     * 
     * @param mapIndexAll String 想要发布的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String pubServiceCategoryMap(String mapIndexAll) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // 服务发布失败个数
        Integer iSuccessed = 0;
        int index = 0;

        String arrayMapIdx[] = mapIndexAll.split(":");

        // 如果没有绑定关系被选中，写返回信息
        if (arrayMapIdx.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_NO_MAP_CHOSEN)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取目标系统的index
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }

        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取临时取地址以及同步XML文件FTP地址
        String tempAreaPath = getTempAreAdd();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", ResourceMgt
                    .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                    .findDefaultText(ServiceConstants.RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 获取临时区失败
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        int mapSize = arrayMapIdx.length;
        try
        {
            for (int i = 0; i < mapSize; i++)
            {
                CategoryServiceMap categoryServiceMap = new CategoryServiceMap();
                categoryServiceMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                categoryServiceMap = categoryServiceMapds.getCategoryServiceMap(categoryServiceMap);
                if (null == categoryServiceMap)
                {

                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], ResourceMgt
                            .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), ResourceMgt
                            .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    if (categoryServiceMap.getStatus() != 0// 待发布
                            && categoryServiceMap.getStatus() != 30// 新增同步失败
                            && categoryServiceMap.getStatus() != 80// 取消同步成功
                            && categoryServiceMap.getStatus() != 90)// 取消同步失败
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), categoryServiceMap.getMapindex()
                                .toString(), ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL),
                                ResourceMgt
                                        .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    // 状态正常，可以发布
                    {
                        // 判断绑定关系状态，如果为待发布或者取消同步成功，直接发布,向分发任务表中插入两条记录
                        if (categoryServiceMap.getStatus() == 0 || categoryServiceMap.getStatus() == 80)
                        {
                            // 生成服务xml，返回生成文件路径
                            String fileName = createCategoryServiceXml(categoryServiceMap, tempAreaPath,
                                    "REGIST or UPDATE"); // 发布

                            // 获取任务流水号和流水批次号
                            String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                    .toString();
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTask = new CntSyncTask();
                            cntsyncTask.setStatus(1);
                            cntsyncTask.setPriority(2);
                            cntsyncTask.setCorrelateid(correlateid);
                            cntsyncTask.setContentmngxmlurl(fileName);
                            cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                            // 插入EPG的任务
                            correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                    .toString();

                            CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                            cntsyncTaskEPG.setStatus(1);
                            cntsyncTaskEPG.setPriority(2);
                            
                            cntsyncTaskEPG.setCorrelateid(correlateid);
                            cntsyncTaskEPG.setContentmngxmlurl(fileName);
                            
                            cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                            categoryServiceMap.setStatus(10);

                        }
                        else
                        {
                            // 查询分发记录表
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecord.setObjindex(categoryServiceMap.getMapindex());
                            cntSyncRecord.setObjtype(30);
                            cntSyncRecord.setDesttype(1);
                            List<CntSyncRecord> cntSyncRecordList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecord);
                            int cntSyncRecordbmsSize = cntSyncRecordList.size();
                            // 是否存在EPG
                            CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecordepg.setObjindex(categoryServiceMap.getMapindex());
                            cntSyncRecordepg.setObjtype(30);
                            cntSyncRecordepg.setDesttype(2); // EPG

                            List<CntSyncRecord> cntSyncRecordepgList = cntSyncRecordDS
                                    .getCntSyncRecordByCond(cntSyncRecordepg);
                            int cntSyncRecordepgSize = cntSyncRecordepgList.size();
                            // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                            if ((cntSyncRecordbmsSize > 0) && (cntSyncRecordepgSize > 0))
                            {
                                categoryServiceMap.setStatus(20); // 新增同步成功
                            }
                            else if ((0 == cntSyncRecordbmsSize) && (cntSyncRecordepgSize > 0))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bms = createCategoryServiceXml(categoryServiceMap, tempAreaPath,
                                        "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                               
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bms);
                                
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                categoryServiceMap.setStatus(10); // 新增同步中

                            }
                            else if ((cntSyncRecordbmsSize > 0) && (cntSyncRecordepgSize == 0))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4epg = createCategoryServiceXml(categoryServiceMap, tempAreaPath,
                                        "REGIST or UPDATE"); // 发布
                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                               
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4epg);
                                
                                cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                categoryServiceMap.setStatus(10); // 新增同步中
                            }
                            else
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bmsEpg = createCategoryServiceXml(categoryServiceMap, tempAreaPath,
                                        "REGIST or UPDATE"); // 发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                        .toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                                
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id")
                                        .toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                               
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                              
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                categoryServiceMap.setStatus(10);// 新增同步中

                            }
                        }
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(categoryServiceMap.getMapindex().toString(),
                                CommonLogConstant.MGTTYPE_SERVICE, CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH,
                                CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(categoryServiceMap
                                .getMapindex()), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                .findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_PUBLISH_SUCCESS)); // 发布成功
                        returnInfo.appendOperateInfo(operateInfo);

                        categoryServiceMapds.updateCategoryServiceMap(categoryServiceMap); // 更新绑定关系的状态
                    }
                }
            }

        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

        if (ifail == arrayMapIdx.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
        }
        else if (ifail > 0 && ifail < arrayMapIdx.length)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail;
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("publish service and category map  ends...");
        return returnInfo.toString();
    }

    // 服务和栏目绑定关系发布时的xml
    private String createCategoryServiceXml(CategoryServiceMap categoryServiceMap, String tempAddr, String actionType)
            throws Exception
    {
        log.debug("create xml of map between service and category starting...");
        Date date = new Date();
        String mountPoint = com.zte.cms.common.GlobalConstants.getMountPoint();// CMS
        String fileName = date.getTime() + "_service.xml";// 当前时间精确到毫秒作为文件名的一部分加上后缀

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileFolder4xml = dateFormat.format(new Date()); // 文件夹

        String tempFilePath = tempAddr + File.separator + XMLADDR + File.separator + fileFolder4xml + File.separator
                + "service" + File.separator;// xml的存放路径
        String mountAndTempfilePath = mountPoint + tempFilePath; // 绝对路径

        String retPath = tempFilePath + fileName;// synXMLFTPAdd:ftp路径
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument(); // 创建 xml文件
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Mappings");
        Element objectElement = null;
        Element propertyElement = null;

        objectElement = objectsElement.addElement("Mapping");

        objectElement.addAttribute("ParentType", "Category");
        objectElement.addAttribute("ParentID", categoryServiceMap.getCategoryid());
        objectElement.addAttribute("ElementType", "Service");
        objectElement.addAttribute("ElementID", categoryServiceMap.getServiceid());
        objectElement.addAttribute("Action", actionType);

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "Sequence");
        if (null == categoryServiceMap.getSequence())
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(categoryServiceMap.getSequence().toString());
        }

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidStart");
        propertyElement.addText("");

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ValidEnd");
        propertyElement.addText("");

        // objectid = objtype+mapindex，objtype占两位，不足补0，mapindex占30位，不足补0
        String objectid = String.format("%02d", 30)
                + String.format("%030d", categoryServiceMap.getMapindex());
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "ObjectID");
        propertyElement.addText(objectid);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("create service Xml exception:" + e);
            throw e;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("create service Xml exception:" + e);
                    throw e;
                }
            }
        }

        log.debug("createScheduleXml end");
        return filterSlashStr(retPath);

    }

    /**
     * 批量取消发布服务和栏目的关联关系
     * 
     * @param mapIndexAll String 想要发布的绑定关系的index组成的字符串，由","分隔
     * @return String
     * @throws Exception Exception
     */
    public String delPubServiceCategoryMap(String mapIndexAll) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        Integer ifail = 0; // map取消发布失败个数
        Integer iSuccessed = 0;
        int index = 0;

        String arrayMapIdx[] = mapIndexAll.split(":");

        // 如果没有绑定关系被选中，写返回信息
        if (arrayMapIdx.length == 0)
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RES_UCDN_SERV_CNT_BIND_PUB_NO_MAP_CHOSEN)); // 没有绑定关系被选中
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取目标系统的index
        Targetsystem targetSystembms = new Targetsystem();
        targetSystembms.setTargettype(1);
        List<Targetsystem> listtsystembms = targetSystemDS.getTargetsystemByCond(targetSystembms);
        if (listtsystembms != null)
        {
            targetSystembms = listtsystembms.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RESOURCE_UCDN_SERVICE_PUB_NO_BMS_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();

        }

        Targetsystem targetSystemepg = new Targetsystem();
        targetSystemepg.setTargettype(2);
        List<Targetsystem> listtsystemepg = targetSystemDS.getTargetsystemByCond(targetSystemepg);
        if (listtsystemepg != null)
        {
            targetSystemepg = listtsystemepg.get(0);
        }
        else
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RESOURCE_UCDN_SERVICE_PUB_NO_EPG_SYSTEM)); // 目标系统不存在
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        // 获取临时取地址以及同步XML文件FTP地址
        String tempAreaPath = getTempAreAdd();
        if (tempAreaPath == null || tempAreaPath.equals("1056020"))
        {
            operateInfo = new OperateInfo(String.valueOf(index++), "", 
                    ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                    ResourceMgt.findDefaultText(ServiceConstants.
                            RES_UCDN_PUB_GET_TEMP_AREA_FAILED)); // 获取临时区失败
            returnInfo.appendOperateInfo(operateInfo);
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
            returnInfo.setFlag(flag);
            returnInfo.setReturnMessage(errorcode);
            return returnInfo.toString();
        }

        int mapSize = arrayMapIdx.length;

        try
        {
            for (int i = 0; i < mapSize; i++)
            {
                CategoryServiceMap categoryServiceMap = new CategoryServiceMap();
                categoryServiceMap.setMapindex(Long.valueOf(arrayMapIdx[i]));
                categoryServiceMap = categoryServiceMapds.getCategoryServiceMap(categoryServiceMap);
                if (null == categoryServiceMap)
                {

                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), arrayMapIdx[i], 
                            ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                            ResourceMgt.findDefaultText(ServiceConstants.
                                    RES_UCDN_SERV_CNT_BIND_PUB_MAP_NO_EXISTS)); // 绑定关系不存在
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    if (categoryServiceMap.getStatus() != 20 // 发布成功
                            && categoryServiceMap.getStatus() != 30 // 新增同步失败
                            && categoryServiceMap.getStatus() != 90) // 取消同步失败
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), 
                                categoryServiceMap.getMapindex().toString(),
                                ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_FAIL), 
                                ResourceMgt.findDefaultText(ServiceConstants.
                                                RES_UCDN_SERV_CNT_BIND_PUB_MAP_STATUS_WRONG)); // 绑定关系当前状态不正确
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    // 状态正常，可以取消发布
                    {
                        // 判断绑定关系状态，如果为新增同步成功,向分发任务表中插入两条记录
                        if (categoryServiceMap.getStatus() == 20)
                        {
                            // 生成服务xml，返回生成文件路径
                            String fileName = createCategoryServiceXml(
                                    categoryServiceMap, 
                                    tempAreaPath, 
                                    "DELETE"); // 取消发布

                            // 获取任务流水号和流水批次号
                            String batchid = this.getPrimaryKeyGenerator().getPrimarykey(
                                    "ucdn_task_batch_id").toString();
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                    "ucdn_task_correlate_id").toString();

                            CntSyncTask cntsyncTask = new CntSyncTask();
                            cntsyncTask.setStatus(1);
                            cntsyncTask.setPriority(2);
                           
                            cntsyncTask.setCorrelateid(correlateid);
                            cntsyncTask.setContentmngxmlurl(fileName);
                           
                            cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                            // 插入EPG的任务
                            correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                    "ucdn_task_correlate_id").toString();

                            CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                            cntsyncTaskEPG.setStatus(1);
                            cntsyncTaskEPG.setPriority(2);
                           
                            cntsyncTaskEPG.setCorrelateid(correlateid);
                            cntsyncTaskEPG.setContentmngxmlurl(fileName);
                            
                            cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                            cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                            categoryServiceMap.setStatus(70);

                        }
                        else
                        {
                            // 查询分发记录表
                            CntSyncRecord cntSyncRecord = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecord.setObjindex(categoryServiceMap.getMapindex());
                            cntSyncRecord.setObjtype(30);
                            cntSyncRecord.setDesttype(1);
                            List<CntSyncRecord> ntSyncRecordList = 
                                cntSyncRecordDS.getCntSyncRecordByCond(cntSyncRecord);
                            int bmsSize = ntSyncRecordList.size();
                            // 是否存在EPG
                            CntSyncRecord cntSyncRecordepg = new CntSyncRecord();
                            // 是否存在BMS记录
                            cntSyncRecordepg.setObjindex(categoryServiceMap.getMapindex());
                            cntSyncRecordepg.setObjtype(30);
                            cntSyncRecordepg.setDesttype(2); // EPG
                            List<CntSyncRecord> cntSyncRecordepgList = 
                                cntSyncRecordDS.getCntSyncRecordByCond(cntSyncRecordepg);
                            int epgSize = cntSyncRecordepgList.size();
                            // 如果两个都在记录表里面，说明取消发布失败时两个都没有取消成功，则直接修改状态
                            if ((0 == bmsSize) && (0 == epgSize))
                            {
                                categoryServiceMap.setStatus(80); // 取消发布成功
                            }
                            else if ((0 == bmsSize) && (0 < epgSize))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bms = createCategoryServiceXml(
                                        categoryServiceMap, 
                                        tempAreaPath, 
                                        "DELETE"); // 取消发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_batch_id").toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                               
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bms);
                       
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                categoryServiceMap.setStatus(70); // 新增同步中

                            }
                            else if ((0 < bmsSize) && (0 == epgSize))
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4epg = createCategoryServiceXml(
                                        categoryServiceMap, 
                                        tempAreaPath, 
                                        "DELETE"); // 取消发布
                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_batch_id").toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4epg);
                              
                                cntsyncTask.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                categoryServiceMap.setStatus(70); // 新增同步中
                            }
                            else
                            {
                                // 生成服务xml，返回生成文件路径
                                String fileName4bmsEpg = createCategoryServiceXml(
                                        categoryServiceMap, 
                                        tempAreaPath, "" +
                                        		"DELETE"); // 取消发布

                                // 获取任务流水号和流水批次号
                                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_batch_id").toString();
                                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTask = new CntSyncTask();
                                cntsyncTask.setStatus(1);
                                cntsyncTask.setPriority(2);
                                
                                cntsyncTask.setCorrelateid(correlateid);
                                cntsyncTask.setContentmngxmlurl(fileName4bmsEpg);
                               
                                cntsyncTask.setDestindex(targetSystembms.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTask);

                                // 插入EPG的任务
                                correlateid = this.getPrimaryKeyGenerator().getPrimarykey(
                                        "ucdn_task_correlate_id").toString();

                                CntSyncTask cntsyncTaskEPG = new CntSyncTask();
                                cntsyncTaskEPG.setStatus(1);
                                cntsyncTaskEPG.setPriority(2);
                               
                                cntsyncTaskEPG.setCorrelateid(correlateid);
                                cntsyncTaskEPG.setContentmngxmlurl(fileName4bmsEpg);
                                
                                cntsyncTaskEPG.setDestindex(targetSystemepg.getTargetindex()); // 目标系统index
                                cntSyncTaskDS.insertCntSyncTask(cntsyncTaskEPG);

                                categoryServiceMap.setStatus(70);// 新增同步中

                            }
                        }
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(categoryServiceMap.getMapindex().toString(),
                                CommonLogConstant.MGTTYPE_SERVICE,
                                CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL,
                                CommonLogConstant.OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        // 返回信息
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), String
                                .valueOf(categoryServiceMap.getMapindex()), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPER_SUCCESS), ResourceMgt
                                .findDefaultText(ServiceConstants.RES_UCDN_SERV_CNT_BIND_PUB_DEL_SUCC)); // 取消发布成功
                        returnInfo.appendOperateInfo(operateInfo);

                        categoryServiceMapds.updateCategoryServiceMap(categoryServiceMap); // 更新绑定关系的状态
                    }
                }
            }

        }catch (Exception e)
        {
            throw new Exception(e.getMessage());

        }
        
        if (ifail == arrayMapIdx.length)
        {
            flag = GlobalConstants.FAIL; // "1"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ": " + ifail;
        }
        else if (ifail > 0 && ifail < arrayMapIdx.length)
        {
            flag = "2";
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + ": " + iSuccessed
                    + "  " + ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_FAILED_NUM) + ifail;
        }
        else
        {
            flag = GlobalConstants.SUCCESS; // "0"
            errorcode = ResourceMgt.findDefaultText(ServiceConstants.RES_UCDN_SERVICE_SUCC_NUM) + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);
        log.debug("cancel publish service and category map  ends...");
        return returnInfo.toString();
    }

}

package com.zte.cms.content.objectSynRecord.service;

import java.util.List;

import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.dao.IObjectSyncRecordDAO;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ObjectSyncRecordDS extends
com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements IObjectSyncRecordDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IObjectSyncRecordDAO dao = null;
	
	public void setDao(IObjectSyncRecordDAO dao)
	{
	    this.dao = dao;
	}

	public void insertObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug("insert objectSyncRecord starting...");
		try
		{
			Long syncindex;
			if (objectSyncRecord.getSyncindex() == null)
			{
				syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
				"object_sync_record_index");
				objectSyncRecord.setSyncindex(syncindex);
			}
			dao.insertObjectSyncRecord( objectSyncRecord );

			/*
			Long syncindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey(
			"object_sync_record_index");
			objectSyncRecord.setSyncindex(syncindex);
			dao.insertObjectSyncRecord( objectSyncRecord );
			*/
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert objectSyncRecord end");
	}
	
	public void updateObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug("update objectSyncRecord by pk starting...");
	    try
		{
			dao.updateObjectSyncRecord( objectSyncRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectSyncRecord by pk end");
	}

	public void updateObjectSyncRecordList( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException
	{
		log.debug("update objectSyncRecordList by pk starting...");
		if(null == objectSyncRecordList || objectSyncRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectSyncRecordList");
			return;
		}
	    try
		{
			for( ObjectSyncRecord objectSyncRecord : objectSyncRecordList )
			{
		    	dao.updateObjectSyncRecord( objectSyncRecord );
		    }
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectSyncRecordList by pk end");
	}



	public void removeObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "remove objectSyncRecord by pk starting..." );
		try
		{
			dao.deleteObjectSyncRecord( objectSyncRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectSyncRecord by pk end" );
	}

	public void removeObjectSyncRecordList ( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException
	{
		log.debug( "remove objectSyncRecordList by pk starting..." );
		if(null == objectSyncRecordList || objectSyncRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectSyncRecordList");
			return;
		}
		try
		{
			for( ObjectSyncRecord objectSyncRecord : objectSyncRecordList )
			{
				dao.deleteObjectSyncRecord( objectSyncRecord );
			}
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectSyncRecordList by pk end" );
	}



	public ObjectSyncRecord getObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "get objectSyncRecord by pk starting..." );
		ObjectSyncRecord rsObj = null;
		try
		{
			rsObj = dao.getObjectSyncRecord( objectSyncRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get objectSyncRecordList by pk end" );
		return rsObj;
	}

	public List<ObjectSyncRecord> getObjectSyncRecordByCond( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "get objectSyncRecord by condition starting..." );
		List<ObjectSyncRecord> rsList = null;
		try
		{
			rsList = dao.getObjectSyncRecordByCond( objectSyncRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get objectSyncRecord by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get objectSyncRecord page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(objectSyncRecord, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ObjectSyncRecord>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get objectSyncRecord page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get objectSyncRecord page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(objectSyncRecord, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ObjectSyncRecord>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get objectSyncRecord page info by condition end" );
        
		return tableInfo;
	}

    public List<ObjectSyncRecord> getObjectSyncRecordByTaskindex(ObjectSyncRecord objectSyncRecord)
            throws DomainServiceException
    {
        log.debug( "get objectSyncRecord by taskindex starting..." );
        List<ObjectSyncRecord> rsList = null;
        try
        {
            rsList = dao.getObjectSyncRecordByTaskindex( objectSyncRecord );
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        log.debug( "get objectSyncRecord by taskindex end" );
        return rsList;
    }

    public TableDataInfo getobjectSyncRecord(ObjectSyncRecord objectSyncRecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug( "get objectSyncRecord page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.getobjectSyncRecord(objectSyncRecord, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ObjectSyncRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get objectSyncRecord page info by condition end" );
        return tableInfo;
    }
}

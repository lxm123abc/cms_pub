package com.zte.cms.programcrmmap.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.ls.ProgramSynConstants;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.cms.content.program.programsync.service.IProgramTargetSynDS;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.cms.programcrmmap.model.ProgramCrmMapConstant;
import com.zte.cms.programcrmmap.service.IProgramCrmMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ismp.common.ResourceManager;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class ProgramcrmmapLS extends DynamicObjectBaseDS implements IProgramcrmmapLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CONTENT, getClass());

    private IProgramCrmMapDS programCrmMapDS;
    private ICmsProgramDS cmsProgramDS;
    private ICastrolemapDS castrolemapds;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ITargetsystemDS targetSystemDS;
    private IUsysConfigDS usysConfigDS = null;
    private ICmsStorageareaLS cmsStorageareaLS;
	private IObjectSyncRecordDS objectSyncRecordDS;
	private IProgramTargetSynDS programTargetSynDS;
	private ICntSyncTaskDS cntsyncTaskDS;

    public static final String PUBTARGETSYSTEM = "发布到目标系统";
    public static final String DELPUBTARGETSYSTEM = "从目标系统";
	public static final String TARGETSYSTEM = "目标系统";
    public static final String operSucess = "操作成功";
    public static final String delPub = "取消发布";
    private Map procastrolemap = new HashMap<String, List>(); // 生成同步XML需要的参数，包含内容对象 和 其子内容对象
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
	public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    private String result = "0:操作成功";    
    
    public Long syncindex = null;
	public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
	{
		this.cntsyncTaskDS = cntsyncTaskDS;
	}

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
	{
		this.usysConfigDS = usysConfigDS;
	}

	public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
	{
		this.cmsStorageareaLS = cmsStorageareaLS;
	}

	public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
	{
		this.objectSyncRecordDS = objectSyncRecordDS;
	}

	public void setProgramTargetSynDS(IProgramTargetSynDS programTargetSynDS)
	{
		this.programTargetSynDS = programTargetSynDS;
	}

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCastrolemapds(ICastrolemapDS castrolemapds)
    {
        this.castrolemapds = castrolemapds;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setProgramCrmMapDS(IProgramCrmMapDS programCrmMapDS)
    {
        this.programCrmMapDS = programCrmMapDS;
    }

    public String insertProgramcrmmap(Long[] castrolemapindexList, String programindex) throws DomainServiceException
    {
        log.debug("insertProgramcrmmap start ");

        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        Castrolemap castrolemap = new Castrolemap();
        Castrolemap castrolemapObj = null;
        OperateInfo operateInfo = null;
        String opindex = "";
        String castrolemapindex = null;
        String flag = "";
        CmsProgram cmsProgramObj = null;
        ProgramCrmMap programcrmmap = null;

        CmsProgram cmsProgram = new CmsProgram();
        cmsProgram.setProgramindex(Long.valueOf(programindex));
        cmsProgramObj = cmsProgramDS.getCmsProgram(cmsProgram);
        if (cmsProgramObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.not.exist"));//"该VOD内容不存在");
            return rtnInfo.toString();
        }
        // 判断VOD内容状态是否已发生改变，不为审核通过
        else if (cmsProgramObj.getStatus() != 0)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.cnt.status.changed"));//"该VOD内容的状态已发生改变");
            return rtnInfo.toString();
        }
        for (int i = 0; i < castrolemapindexList.length; i++)
        {
            try
            {

                opindex = (i + 1) + "";
                castrolemapindex = String.valueOf(castrolemapindexList[i]);

                castrolemap.setCastrolemapindex(castrolemapindexList[i]);
                castrolemapObj = castrolemapds.getCastrolemap(castrolemap);
                if (castrolemapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"), 
                    		ResourceMgt.findDefaultText("cast.role.not.exist"));////"操作失败"   "角色不存在"
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                // 判断VOD内容与角色是否已存在绑定关系
                programcrmmap = new ProgramCrmMap();
                programcrmmap.setProgramindex(cmsProgramObj.getProgramindex());
                programcrmmap.setCastrolemapindex(castrolemapObj.getCastrolemapindex());
                List<ProgramCrmMap> listpcrmmap = programCrmMapDS.getProgramCrmMapByCond(programcrmmap);
                if (listpcrmmap != null && listpcrmmap.size() > 0)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, castrolemapindex,ResourceMgt.findDefaultText("do.fail") , "该VOD内容与角色已存在绑定关系");
                    //"操作失败"   该VOD内容与角色已存在绑定关系
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("program_crm_map");
                    String mappingid = String.format("%02d", 34)+ String.format("%030d", mapindex);
                    programcrmmap.setMapindex(mapindex);
                    programcrmmap.setMappingid(mappingid);
                    programcrmmap.setProgramid(cmsProgramObj.getProgramid());
                    programcrmmap.setCastrolemapid(castrolemapObj.getCastrolemapid());
                    programcrmmap.setStatus(ProgramCrmMapConstant.PROGRAMCRMMAP_STATUS_WAITPUBLISH);

                    programCrmMapDS.insertProgramCrmMap(programcrmmap);
                    iSuccessed++;
                    operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.success"),ResourceMgt.findDefaultText("do.success"));//"操作成功", " 操作成功"
                    rtnInfo.appendOperateInfo(operateInfo);

                    CommonLogUtil.insertOperatorLog(mapindex.toString(), ProgramCrmMapConstant.MGTTYPE_PROGRAMCRMMAP,
                            ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_ADD,
                            ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_ADD_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    
                    CntTargetSync target = new CntTargetSync();
                    target.setObjecttype(3);
                    target.setObjectid(cmsProgramObj.getProgramid());
                    //运营平台存在
                    List<CntTargetSync> targetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                    if(targetSyncList !=null && targetSyncList.size()>0){
                        Targetsystem targetSystem = new Targetsystem();
                        for(CntTargetSync targetSync:targetSyncList){
                            targetSystem.setTargetindex(targetSync.getTargetindex());
                            targetSystem = targetSystemDS.getTargetsystem(targetSystem);
                            
                            target.setObjecttype(12);
                            target.setObjectid(castrolemapObj.getCastrolemapid());
                            target.setTargetindex(targetSystem.getTargetindex());
                            List<CntTargetSync> pcrmtargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                            //点播内容关联角色关联平台，前提是角色已关联该平台
                            if(pcrmtargetSyncList !=null && pcrmtargetSyncList.size()>0)
                            {
                                CntPlatformSync platform = new CntPlatformSync();
                                platform.setObjecttype(34);
                                platform.setObjectid(mappingid);
                                platform.setPlatform(targetSystem.getPlatform());
                                List<CntPlatformSync> pcrmPlatSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                                if(pcrmPlatSyncList !=null && pcrmPlatSyncList.size()>0)  
                                {
                                    syncindex = pcrmPlatSyncList.get(0).getSyncindex();
                                    platform = pcrmPlatSyncList.get(0);
                                    platform.setStatus(0);
                                    cntPlatformSyncDS.updateCntPlatformSync(platform);   
                                }else{
                                    insertPlatformSyncTask(34,mapindex,mappingid,castrolemapObj.getCastrolemapid(),cmsProgramObj.getProgramid(),
                                            targetSystem.getPlatform(),0);
                                    // 写操作日志
                                    CommonLogUtil.insertOperatorLog(mappingid + ", " + targetSystem.getTargetid(), ProgramCrmMapConstant.MGTTYPE_PROGRAMCRMMAP,
                                            ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD), ResourceManager.getResourceText(CntConstants.OPERTYPE_PLATFORM_PCRMMAP_BIND_ADD_INFO),
                                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                                }
                                
                                insertTargetSyncTask(syncindex,34,mapindex,mappingid,castrolemapObj.getCastrolemapid(),
                                        cmsProgramObj.getProgramid(),targetSystem.getTargetindex(),0,0);
                                // 写操作日志
                                CommonLogUtil.insertOperatorLog(mappingid + ", " + targetSystem.getTargetid(), ProgramCrmMapConstant.MGTTYPE_PROGRAMCRMMAP,
                                        ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_ADD), ResourceManager.getResourceText(CntConstants.OPERTYPE_TARGET_PCRMMAP_BIND_ADD_INFO),
                                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            }
                        }
                    }

                }
                
            }
            catch (Exception e)
            {

                ifail++;
                operateInfo = new OperateInfo(opindex, castrolemapindex, ResourceMgt.findDefaultText("do.fail"), ResourceMgt.findDefaultText("add.bind.relation.cod.with.role.failed"));//"新增VOD内容与角色的绑定关系失败"
                //"操作失败"
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (castrolemapindexList.length == 0 || ifail == castrolemapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + castrolemapindexList.length);
        }

        log.debug("insertProgramcrmmap end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public String batchDeleteList(Long[] mapindexList, String programindex) throws DomainServiceException
    {
        log.debug("batchDeleteList start ");

        ReturnInfo rtnInfo = new ReturnInfo();

        Integer ifail = 0;
        Integer iSuccessed = 0;
        OperateInfo operateInfo = null;
        String opindex = "";
        String mapindex = null;
        String flag = "";
        boolean isBreak = false;
        ProgramCrmMap programCrmMapObj = null;
        ProgramCrmMap programcrmmap = new ProgramCrmMap();

        CmsProgram cmsProgramObj = null;
        CmsProgram cmsProgram = new CmsProgram();
        cmsProgram.setProgramindex(Long.valueOf(programindex));
        cmsProgramObj = cmsProgramDS.getCmsProgram(cmsProgram);
        if (cmsProgramObj == null)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.not.exist"));//"该VOD内容不存在");
            return rtnInfo.toString();
        }
        // 判断VOD内容状态是否已发生改变，不为审核通过
        else if (cmsProgramObj.getStatus() != 0)
        {
            rtnInfo.setFlag("1");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.cnt.status.changed"));
            //"该VOD内容的状态已发生改变");
            return rtnInfo.toString();
        }

        for (int i = 0; i < mapindexList.length; i++)
        {
            try
            {

                opindex = (i + 1) + "";
                mapindex = String.valueOf(mapindexList[i]);

                programcrmmap.setMapindex(mapindexList[i]);
                programCrmMapObj = programCrmMapDS.getProgramCrmMap(programcrmmap);

                if (programCrmMapObj == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(opindex, mapindex,ResourceMgt.findDefaultText("do.fail") , ResourceMgt.findDefaultText("bind.relation.notexist"));//"操作失败"   绑定关系不存在
                    rtnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                
                CntPlatformSync platform = new CntPlatformSync();
                platform.setObjecttype(34);
                platform.setObjectid(programCrmMapObj.getMappingid());
                CntTargetSync target = new CntTargetSync();
                target.setObjecttype(34);
                target.setObjectid(programCrmMapObj.getMappingid());
                List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
                List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
                if(targetList!=null && targetList.size()>0){
                    for(CntTargetSync pcrmTarget:targetList){
                        int pcrmSyncStatus = pcrmTarget.getStatus();
                        if(pcrmSyncStatus==200||pcrmSyncStatus==300){
                            ifail++;
                            operateInfo = new OperateInfo(opindex, mapindex,ResourceMgt.findDefaultText("do.fail") , ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));
                            rtnInfo.appendOperateInfo(operateInfo);
                            isBreak = true;
                            break;
                        }
                    }
                    if(isBreak == true){
                        continue;
                    }
                    cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
                    cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
                }
                
                programCrmMapDS.removeProgramCrmMap(programCrmMapObj);
                iSuccessed++;
                operateInfo = new OperateInfo(opindex, mapindex, ResourceMgt.findDefaultText("do.success"),ResourceMgt.findDefaultText("do.success"));//"操作成功", " 操作成功"
                rtnInfo.appendOperateInfo(operateInfo);

                CommonLogUtil.insertOperatorLog(mapindex, ProgramCrmMapConstant.MGTTYPE_PROGRAMCRMMAP,
                        ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_DEL,
                        ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_DEL_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            }
            catch (Exception e)
            {
                ifail++;
                operateInfo = new OperateInfo(opindex, mapindex,ResourceMgt.findDefaultText("do.fail") ,
                		ResourceMgt.findDefaultText("bindrelation.vod.with.role.failed"));
                //"操作失败"   删除VOD内容与角色的绑定关系失败
                rtnInfo.appendOperateInfo(operateInfo);
                continue;
            }
        }
        if (mapindexList.length == 0 || ifail == mapindexList.length)
        {
            flag = "1"; // "1"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
        }
        else if (ifail > 0)
        {
            flag = "2"; // "2"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("blank.fail.count") + ifail);
        }
        else
        {
            flag = "0"; // "0"
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + mapindexList.length);
        }

        log.debug("batchDeleteList end ");
        rtnInfo.setFlag(flag);
        return rtnInfo.toString();
    }

    public ProgramCrmMap getProgramcrmmap(Long mapindex)
    {
        ProgramCrmMap programcrmmap = new ProgramCrmMap();
        programcrmmap.setMapindex(mapindex);
        try
        {
            programcrmmap = programCrmMapDS.getProgramCrmMap(programcrmmap);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in getProgramcrmmap method");
            return null;
        }
        return programcrmmap;
    }

    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            programCrmMap.setProgramid(EspecialCharMgt.conversion(programCrmMap.getProgramid()));
            programCrmMap.setCastrolemapid(EspecialCharMgt.conversion(programCrmMap.getCastrolemapid()));

            tableInfo = programCrmMapDS.pageInfoQuery(programCrmMap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;

    }

    public TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        TableDataInfo tableInfo = new TableDataInfo();
        try
        {
            log.debug("pageInfoQuery start");

            programCrmMap.setProgramid(EspecialCharMgt.conversion(programCrmMap.getProgramid()));
            programCrmMap.setCastrolemapid(EspecialCharMgt.conversion(programCrmMap.getCastrolemapid()));

            tableInfo = programCrmMapDS.pageInfoQuery(programCrmMap, start, pageSize, puEntity);
        }
        catch (DomainServiceException e)
        {
            log.error("Error occurred in pageInfoQuery method");
            throw new DomainServiceException(e);
        }

        log.debug("pageInfoQuery end");
        return tableInfo;
    }

    public String deleteProgarmcrmmap(Long mapindex, String progaramindex) throws DomainServiceException
    {
        log.debug("deleteProgramcrmmap start ");

        ReturnInfo rtnInfo = new ReturnInfo();

        ProgramCrmMap programCrmMapObj = null;
        ProgramCrmMap programcrmmap = new ProgramCrmMap();

        CmsProgram cmsProgramObj = null;
        CmsProgram cmsProgram = new CmsProgram();
        cmsProgram.setProgramindex(Long.valueOf(progaramindex));
        try
        {
            cmsProgramObj = cmsProgramDS.getCmsProgram(cmsProgram);
            if (cmsProgramObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.not.exist"));
                return rtnInfo.toString();
            }
            // 判断VOD内容状态是否已发生改变，不为审核通过
            else if (cmsProgramObj.getStatus() != 0)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("vod.cnt.status.changed"));//"该VOD内容的状态已发生改变");
                return rtnInfo.toString();
            }

            programcrmmap.setMapindex(mapindex);
            programCrmMapObj = programCrmMapDS.getProgramCrmMap(programcrmmap);
            if (programCrmMapObj == null)
            {
                rtnInfo.setFlag("1");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("bind.relation.notexist"));//"绑定关系不存在");
                return rtnInfo.toString();

            }
            
            CntPlatformSync platform = new CntPlatformSync();
            platform.setObjecttype(34);
            platform.setObjectid(programCrmMapObj.getMappingid());
            CntTargetSync target = new CntTargetSync();
            target.setObjecttype(34);
            target.setObjectid(programCrmMapObj.getMappingid());
            List<CntTargetSync> targetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            List<CntPlatformSync> platformList = cntPlatformSyncDS.getCntPlatformSyncByCond(platform);
            if(targetList!=null && targetList.size()>0){
                for(CntTargetSync pcrmTarget:targetList){
                    int pcrmSyncStatus = pcrmTarget.getStatus();
                    if(pcrmSyncStatus==200||pcrmSyncStatus==300){
                        rtnInfo.setFlag("1");
                        rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("syncstatus.wrong.cannot.delete"));
                        return rtnInfo.toString();
                    }
                }
                cntPlatformSyncDS.removeCntPlatformSynListByObjindex(platformList);
                cntTargetSyncDS.removeCntTargetSynListByObjindex(targetList);
            }

            programCrmMapDS.removeProgramCrmMap(programCrmMapObj);
            rtnInfo.setFlag("0");
            rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("do.success"));//"操作成功"

            CommonLogUtil.insertOperatorLog(String.valueOf(mapindex), ProgramCrmMapConstant.MGTTYPE_PROGRAMCRMMAP,
                    ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_DEL,
                    ProgramCrmMapConstant.OPERTYPE_PROGRAMCRMMAP_DEL_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);    
           
        }
        catch (Exception e)
        {
            log.error("Error occurred in deleteProgarmcrmmap method:" + e);
            throw new DomainServiceException(e);

        }
        log.debug("deleteProgramcrmmap end ");
        return rtnInfo.toString();
    }
    
    /**
     * 
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param platform 平台类型：1-2.0平台，2-3.0平台
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     */
    private void insertPlatformSyncTask(int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, int platform, int status) throws Exception
    {
        CntPlatformSync platformSync = new CntPlatformSync();
        try
        {
            platformSync.setObjecttype(objecttype);
            platformSync.setObjectindex(objectindex);
            platformSync.setObjectid(objectid);
            platformSync.setElementid(elementid);
            platformSync.setParentid(parentid);
            platformSync.setPlatform(platform);
            platformSync.setStatus(status);  
            syncindex = cntPlatformSyncDS.insertCntPlatformSyncRtn(platformSync);
        }
        catch (Exception e)
        {
            log.error("ProgramcrmmapLS insertPlatformSyncTask exception:" + e.getMessage());
        }

    }
    
    /**
     * 
     * @param relateindex 关联平台发布总状态的index
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param parentid 发布对象为mapping时必填,mapping的parentid
     * @param targetindex 目标系统的主键
     * @param status 发布总状态：0-待发布 200-发布中 300-发布成功 400-发布失败 500-修改发布失败 600-预下线
     * @param operresult 操作结果：0-待发布 10-新增同步中 20-新增同步成功 30-新增同步失败 40-修改同步中 50-修改同步成功 60-修改同步失败 70-取消同步中 80-取消同步成功 90-取消同步失败
     */
    private void insertTargetSyncTask(Long relateindex,int objecttype, Long objectindex, String objectid, String elementid,
            String parentid, Long targetindex, int status,int operresult) throws Exception
    {
        CntTargetSync targetSync = new CntTargetSync();
        try
        {
            targetSync.setRelateindex(relateindex);
            targetSync.setObjecttype(objecttype);
            targetSync.setObjectindex(objectindex);
            targetSync.setObjectid(objectid);
            targetSync.setElementid(elementid);
            targetSync.setParentid(parentid);
            targetSync.setTargetindex(targetindex);
            targetSync.setStatus(status);  
            targetSync.setOperresult(operresult);
            cntTargetSyncDS.insertCntTargetSync(targetSync);
        }
        catch (Exception e)
        {
            log.error("ProgramcrmmapLS insertTargetSyncTask exception:" + e.getMessage());
        }

    }

    /************************* publis manage begin*************/
    
    
    public TableDataInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            // 获得当前用户ID

            //programCrmMap.setTargetname("");

     
            // 格式化内容带有时间的字段
            programCrmMap.setCreateStarttime(DateUtil2.get14Time(programCrmMap.getCreateStarttime()));
            programCrmMap.setCreateEndtime(DateUtil2.get14Time(programCrmMap.getCreateEndtime()));
            programCrmMap.setOnlinetimeStartDate(DateUtil2.get14Time(programCrmMap.getOnlinetimeStartDate()));
            programCrmMap.setOnlinetimeEndDate(DateUtil2.get14Time(programCrmMap.getOnlinetimeEndDate()));
            programCrmMap.setCntofflinestarttime(DateUtil2.get14Time(programCrmMap.getCntofflinestarttime()));
            programCrmMap.setCntofflineendtime(DateUtil2.get14Time(programCrmMap.getCntofflineendtime()));
            

            if(programCrmMap.getCastrolemapid()!=null&&!"".equals(programCrmMap.getCastrolemapid())){
            	programCrmMap.setCastrolemapid(EspecialCharMgt.conversion(programCrmMap.getCastrolemapid()));
            }           

            dataInfo = programCrmMapDS.pageInfoQueryproCrmMap(programCrmMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
       
    
    public TableDataInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            // 获得当前用户ID

            //programCrmMap.setTargetname("");
            // 格式化内容带有时间的字段
            programCrmMap.setCreateStarttime(DateUtil2.get14Time(programCrmMap.getCreateStarttime()));
            programCrmMap.setCreateEndtime(DateUtil2.get14Time(programCrmMap.getCreateEndtime()));
            programCrmMap.setOnlinetimeStartDate(DateUtil2.get14Time(programCrmMap.getOnlinetimeStartDate()));
            programCrmMap.setOnlinetimeEndDate(DateUtil2.get14Time(programCrmMap.getOnlinetimeEndDate()));
            programCrmMap.setCntofflinestarttime(DateUtil2.get14Time(programCrmMap.getCntofflinestarttime()));
            programCrmMap.setCntofflineendtime(DateUtil2.get14Time(programCrmMap.getCntofflineendtime()));
            

            if(programCrmMap.getCastrolemapid()!=null&&!"".equals(programCrmMap.getCastrolemapid())){
            	programCrmMap.setCastrolemapid(EspecialCharMgt.conversion(programCrmMap.getCastrolemapid()));
            }           

            dataInfo = programCrmMapDS.pageInfoQueryproCrmTargetMap(programCrmMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }    
        
    //批量发布或取消发布点播内容关联角色
    public String batchPublishOrDelPublishproCrm(List<ProgramCrmMap> list,String operType, int type) throws Exception
    {    
        log.debug("batchPublishProgram starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        int num = 1;       
        try
        {
            int success = 0;
            int fail = 0;

            String message = "";
            String resultStr = "";
    
            if (operType.equals("0"))  //批量发布
            {
                for (ProgramCrmMap programCrmMap : list)
                {
                    message = publishproCrmMapPlatformSyn(programCrmMap.getMapindex().toString(), programCrmMap.getProgramid(),programCrmMap.getCastrolemapid(),type, 1);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), programCrmMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), programCrmMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
            else  //批量取消发布
            {
                for (ProgramCrmMap programCrmMap : list)
                {
                    message = delPublishproCrmMapPlatformSyn(programCrmMap.getMapindex().toString(), programCrmMap.getProgramid(),programCrmMap.getCastrolemapid(),type , 3);
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), programCrmMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else
                    {
                        fail++;
                        resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), programCrmMap.getMappingid(), 
                        		resultStr.substring(2, resultStr.length()), message.substring(2, message.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    num++;
                }
            }
         
            if (success == list.size())
            {
                returnInfo.setReturnMessage("成功数量：" + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage("失败数量：" + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage("成功数量：" + success + "  失败数量：" + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
            returnInfo.setFlag("1");
        }

        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();        
    }
      
    //点播内容关联角色针对平台发布
    public String publishproCrmMapPlatformSyn(String mapindex, String programid,String castrolemapid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();
		
    	try
    	{       
    		ProgramCrmMap programCrmMap = new ProgramCrmMap();
    		programCrmMap.setMapindex(Long.parseLong(mapindex));

    		programCrmMap.setProgramid(programid);
    		programCrmMap.setCastrolemapid(castrolemapid);
    		
			List<ProgramCrmMap> programCrmMapList = new ArrayList<ProgramCrmMap>();

			programCrmMapList =programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
			if (programCrmMapList == null ||programCrmMapList.size()==0)
			{
				return "1:操作失败,点播内容关联角色记录不存在";
			}
			
    		String initresult="";
    		initresult =initData(programCrmMapList.get(0),"platform",type,1);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(34);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            ProgramCrmMap programCrmMappObj=new ProgramCrmMap();
            int  platformType = 1;
    	    
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programCrmMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		
    		Castrolemap castrolemap = new Castrolemap();
    		castrolemap.setCastrolemapindex(programCrmMapList.get(0).getCastrolemapindex());
    		castrolemap=castrolemapds.getCastrolemap(castrolemap);
    		
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(programCrmMap.getMapindex());

    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programCrmMappObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMappObj);
    	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	procastrolemap.put("programcastroleMap", programCrmMapList);
    	    	
    	    	int targettype = 0;
    			for (ProgramCrmMap programCrmMapTmp:programCrmMapList )
    			{
    			    targettype = programCrmMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (programCrmMapTmp.getTargetstatus()!=0)
    				{
                   	    resultPubStr.append(PUBTARGETSYSTEM
                 	    +programCrmMapTmp.getTargetid()+": "+"目标系统状态不正确"
                        +"  ");
                   	    isstatus = false;
    				}
    				if (isstatus)
    				{
    	            CntTargetSync cntTargetSync = new CntTargetSync();    		
    	            synType = 1;
                    boolean isxml = true;
    				boolean ispub = true;
    				cntTargetSync.setObjectindex(programCrmMapTmp.getProgramindex());
    				cntTargetSync.setTargetindex(programCrmMapTmp.getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				//点播内容在该平台已发布
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
               	    		 +programCrmMapTmp.getTargetid()
                               +"  ");    	
                	    ispub = false;
                	}
    				if (ispub)
    				{
        	            CntTargetSync cntTargetSynctemp = new CntTargetSync();    		

        	            cntTargetSynctemp.setObjectindex(programCrmMapTmp.getCastrolemapindex());
        	            cntTargetSynctemp.setTargetindex(programCrmMapTmp.getTargetindex());
        	            cntTargetSynctemp.setObjecttype(12);
        	            cntTargetSynctemp = cntTargetSyncDS.getCntTargetSync(cntTargetSynctemp);
        				//栏目在该平台已发布
        				if (cntTargetSynctemp == null||(cntTargetSynctemp != null && cntTargetSynctemp.getStatus() != 300 && cntTargetSynctemp.getStatus() != 500))
        				{
                    	    resultPubStr.append("该角色没有发布到"+TARGETSYSTEM
                   	    		 +programCrmMapTmp.getTargetid()
                                   +"  ");    	
                    	    ispub = false;
                    	}    					
    				}
    				if (ispub)
    				{
                        int synTargetType =1;
                        if (programCrmMapTmp.getStatus()== 500) //修改发布    
                        {
                    		synTargetType =2;
            	            synType = 2;
                        }
                       	if (targettype ==2&&(programCrmMapTmp.getStatus()==0||programCrmMapTmp.getStatus()==400
                       			||programCrmMapTmp.getStatus()== 500))  //EPG系统
                       	{
                       		String xml;
                       		if (synTargetType==1)
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 1); //新增发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    +programCrmMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                    +"  ");
                                }
                       		}
                       		else
                       		{
                                xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 2); //修改发布到EPG
                                if (null == xml)
                                {
                                	isxml = false;
                               	    resultPubStr.append(PUBTARGETSYSTEM
                             	    +programCrmMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                    +"  ");
                                }
                       		}
                       		if (isxml)
                       		{
                            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                            Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                            Long targetindex =programCrmMapTmp.getTargetindex();
 
                            //Mapping 对象日志记录
                            Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                            String objectidMapping = programCrmMapTmp.getMappingid();
                            String parentid = programCrmMapTmp.getProgramid();
                            String  elementid = programCrmMapTmp.getCastrolemapid();
                            Long objectindex = programCrmMapTmp.getMapindex();
                            insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                            insertSyncTask(index,xml,correlateid,targetindex); //插入同步任务

                            cntTargetSyncTemp.setSyncindex(programCrmMapTmp.getSyncindex());
                            if (synTargetType ==1)
                            {
                            	cntTargetSyncTemp.setOperresult(10);
                            }
                            else
                            {
                            	cntTargetSyncTemp.setOperresult(40);
                            }                            
                            cntTargetSyncTemp.setStatus(200);
                            cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                CommonLogUtil.insertOperatorLog(programCrmMapTmp.getMappingid()+","
                                +programCrmMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                    CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                            sucessPub=true;
                                resultPubStr.append(PUBTARGETSYSTEM
                        	    		+programCrmMapTmp.getTargetid()+": "
                                        +operSucess+"  "); 
                       		}
                           }
                       }                       		
    			   } 
    		   }
    		   }
    	       
    	       if (type == 2)
    	       {
          	       cntPlatformSync.setPlatform(1);
       			   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
       			   programCrmMappObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
       			   programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMappObj);
       	    	   if (programCrmMapList == null || programCrmMapList.size() == 0)
       	    	   {
                       return "1:操作失败,获取目标网元发布记录失败";
       	    	   }
       	    	   procastrolemap.put("programcastroleMap", programCrmMapList);
       	    	   int targettype = 0;
         			for (ProgramCrmMap programCrmMapTmp:programCrmMapList )
           			{
         			    targettype = programCrmMapTmp.getTargettype();
        				boolean isstatus = true;
        				if (programCrmMapTmp.getTargetstatus()!=0)
        				{
                       	    resultPubStr.append(PUBTARGETSYSTEM
                     	    +programCrmMapTmp.getTargetid()+": "+"目标系统状态不正确"
                            +"  ");
                       	    isstatus = false;
        				}
        				if (isstatus)
        				{
        	            synType = 1;
                        boolean isxml = true;
           	            CntTargetSync cntTargetSync = new CntTargetSync();    		
           				boolean ispub = true;
           				cntTargetSync.setObjectindex(programCrmMapTmp.getProgramindex());
           				cntTargetSync.setTargetindex(programCrmMapTmp.getTargetindex());
        				cntTargetSync.setObjecttype(3);

           				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
           				//点播内容在该平台已发布
           				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
           				{
                       	    resultPubStr.append("该点播内容没有发布到"+TARGETSYSTEM
                      	    		 +programCrmMapTmp.getTargetid()
                                      +"  ");    	
                       	    ispub = false;
                       	}
           				if (ispub)
           				{
            	            CntTargetSync cntTargetSynctemp = new CntTargetSync();    		

            	            cntTargetSynctemp.setObjectindex(programCrmMapTmp.getCastrolemapindex());
            	            cntTargetSynctemp.setTargetindex(programCrmMapTmp.getTargetindex());
            	            cntTargetSynctemp.setObjecttype(12);

            	            cntTargetSynctemp = cntTargetSyncDS.getCntTargetSync(cntTargetSynctemp);
               				//栏目在该平台已发布
               				if (cntTargetSynctemp == null||(cntTargetSynctemp != null && cntTargetSynctemp.getStatus() != 300 && cntTargetSynctemp.getStatus() != 500))
               				{
                           	    resultPubStr.append("该角色没有发布到"+TARGETSYSTEM
                          	    		 +programCrmMapTmp.getTargetid()
                                          +"  ");    	
                           	    ispub = false;
                           	}
           					
           				}
           				if (ispub)
           				{
                               int synTargetType =1;
                               if (programCrmMapTmp.getStatus()== 500) //修改发布    
                               {
                           		synTargetType =2;
                	            synType = 2;

                               }
                              	if ((programCrmMapTmp.getStatus()==0||programCrmMapTmp.getStatus()==400
                              			||programCrmMapTmp.getStatus()== 500))  //2.0平台网元
                              	{
                              		String xml;
                              		if (synTargetType==1)
                              		{
                                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 1); //新增发布
                                        if (null == xml)
                                        {
                                        	isxml = false;
                                       	    resultPubStr.append(PUBTARGETSYSTEM
                                     	    +programCrmMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                            +"  ");
                                        }
                              		}
                              		else
                              		{
                                         xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 2); //修改发布
                                         if (null == xml)
                                         {
                                         	isxml = false;
                                        	resultPubStr.append(PUBTARGETSYSTEM
                                      	    +programCrmMapTmp.getTargetid()+": "+"生成同步指令xml文件失败"
                                             +"  ");
                                         }
                              		}
                              		if (isxml)
                              		{
                                   String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                                   Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                                   Long targetindex =programCrmMapTmp.getTargetindex();
        
                                   //Mapping 对象日志记录
                                   Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                                   String objectidMapping = programCrmMapTmp.getMappingid();
                                   String parentid = programCrmMapTmp.getProgramid();
                                   String  elementid = programCrmMapTmp.getCastrolemapid();
                                   Long objectindex = programCrmMapTmp.getMapindex();
                                   String elementcode = castrolemap.getCastrolemapcode();
                                   String parentcode = cmsProgram.getCpcontentid();
                                   insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                                   insertSyncTask(index,xml,correlateid,targetindex); //插入同步任务

                                   cntTargetSyncTemp.setSyncindex(programCrmMapTmp.getSyncindex());
                                   if (synTargetType ==1)
                                   {
                                   	cntTargetSyncTemp.setOperresult(10);
                                   }
                                   else
                                   {
                                   	cntTargetSyncTemp.setOperresult(40);
                                   }                            
                                   cntTargetSyncTemp.setStatus(200);
                                   cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                                        CommonLogUtil.insertOperatorLog(programCrmMapTmp.getMappingid()+","
                                                +programCrmMapTmp.getTargetid(), CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                           CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_INFO,
                                           CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                                   sucessPub=true;
                                        resultPubStr.append(PUBTARGETSYSTEM
                                        +programCrmMapTmp.getTargetid()+": "
                                        +operSucess+"  "); 
                              		}
                                 }
                             }                          		
           				} 
    	       }
    	       }
               // 更新平台发布状态       
    	       if (sucessPub)
    	       {
                   CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
                   cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTemp.setStatus(200);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
    	       }
    	    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();

    	return rtnPubStr;   
    	}   
       
    //点播内容关联角色针对平台取消发布
    public String delPublishproCrmMapPlatformSyn(String mapindex, String programid,String castrolemapid,int type,int synType) throws Exception
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        String rtnPubStr = null; 
		boolean sucessPub =false ;
        StringBuffer resultPubStr = new StringBuffer();		
    	try
    	{       
    		ProgramCrmMap programCrmMap = new ProgramCrmMap();
    		programCrmMap.setMapindex(Long.parseLong(mapindex));

    		programCrmMap.setProgramid(programid);
    		programCrmMap.setCastrolemapid(castrolemapid);
    		
			List<ProgramCrmMap> programCrmMapList = new ArrayList<ProgramCrmMap>();

			programCrmMapList =programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
			if (programCrmMapList == null ||programCrmMapList.size()==0)
			{
				return "1:操作失败,点播内容关联角色记录不存在";
			}
			
    		String initresult="";
    		initresult =initData(programCrmMapList.get(0),"platform",type,3);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }    		
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(Long.parseLong(mapindex));
			cntPlatformSync.setObjecttype(34);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            ProgramCrmMap programCrmMappObj=new ProgramCrmMap();
            int  platformType = 1;
    	    
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programCrmMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		
    		Castrolemap castrolemap = new Castrolemap();
    		castrolemap.setCastrolemapindex(programCrmMapList.get(0).getCastrolemapindex());
    		castrolemap=castrolemapds.getCastrolemap(castrolemap);
            
			CntTargetSync cntTargetSyncObj = new CntTargetSync();
			cntTargetSyncObj.setObjectindex(programCrmMap.getMapindex());

    	    if (type == 1) //3.0平台
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programCrmMappObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMappObj);
    	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	procastrolemap.put("programcastroleMap", programCrmMapList);
    	    	int targettype = 0;
    			for (ProgramCrmMap programCrmMapTmp:programCrmMapList )
    			{
    			    targettype = programCrmMapTmp.getTargettype();
    				boolean isstatus = true;
    				if (programCrmMapTmp.getTargetstatus()!=0)
    				{
                	    resultPubStr.append(DELPUBTARGETSYSTEM
                      	        +programCrmMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                                +"  ");
                	    isstatus = false;
    				}
    				if (isstatus)
    				{
    				boolean isxml = true;                    
                   	if (targettype==2&&(programCrmMapTmp.getStatus()==300||programCrmMapTmp.getStatus()==500))  //EPG系统
                   	{
                   		String xml;
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 3); //新增发布到EPG
                        if (null == xml)
                        {
                            isxml = false;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                  	        +programCrmMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                            +"  ");                               
                    	} 
                        if (isxml)
                        {
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long targetindex =programCrmMapTmp.getTargetindex();

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programCrmMapTmp.getMappingid();
                        String parentid = programCrmMapTmp.getProgramid();
                        String  elementid = programCrmMapTmp.getCastrolemapid();
                        Long objectindex = programCrmMapTmp.getMapindex();
                        insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,targetindex,synType);
                        insertSyncTask(index,xml,correlateid,targetindex); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(programCrmMapTmp.getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);                       
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                            CommonLogUtil.insertOperatorLog(programCrmMapTmp.getMappingid()+","+programCrmMapTmp.getTargetid(), 
                            		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                        sucessPub=true;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                    	    +programCrmMapTmp.getTargetid()+delPub+": "
                            +operSucess+"  ");  
                        }
                   }                       		
    			} 
    	    }
    	   }
 	       if (type == 2)
 	       {
       	       cntPlatformSync.setPlatform(1);
    		   cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    		   programCrmMappObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    		   programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMappObj);
	    	   if (programCrmMapList == null || programCrmMapList.size() == 0)
	    	   {
                return "1:操作失败,获取目标网元发布记录失败";
	    	   }
    	       procastrolemap.put("programcastroleMap", programCrmMapList);
    	       int targettype = 0;
     			for (ProgramCrmMap programCrmMapTmp:programCrmMapList )
    			{
                    targettype = programCrmMapTmp.getTargettype();

    				boolean isstatus = true;
    				if (programCrmMapTmp.getTargetstatus()!=0)
    				{
                	    resultPubStr.append(DELPUBTARGETSYSTEM
                      	        +programCrmMapTmp.getTargetid()+delPub+": "+"目标系统状态不正确"
                                +"  ");
                	    isstatus = false;
    				}
    				if (isstatus)
    				{
     				boolean isxml = true;
                   	if ((programCrmMapTmp.getStatus()==300||programCrmMapTmp.getStatus()==500))  //2.0平台网元
                   	{
                   		String xml;
                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 3); //新增发布
                        if (null == xml)
                        {
                            isxml = false;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                  	        +programCrmMapTmp.getTargetid()+delPub+": "+"生成同步指令xml文件失败"
                            +"  ");                               
                    	} 
                        if (isxml)
                        {
                        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                        Long targetindex =programCrmMapTmp.getTargetindex();

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programCrmMapTmp.getMappingid();
                        String parentid = programCrmMapTmp.getProgramid();
                        String  elementid = programCrmMapTmp.getCastrolemapid();
                        Long objectindex = programCrmMapTmp.getMapindex();
                        String elementcode = castrolemap.getCastrolemapcode();
                        String parentcode = cmsProgram.getCpcontentid();
                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,targetindex,synType);
                        insertSyncTask(index,xml,correlateid,targetindex); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(programCrmMapTmp.getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                            CommonLogUtil.insertOperatorLog(programCrmMapTmp.getMappingid()+","+programCrmMapTmp.getTargetid(), 
                            		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                    
                        sucessPub=true;
                    	    resultPubStr.append(DELPUBTARGETSYSTEM
                    	    +programCrmMapTmp.getTargetid()+delPub+": "
                            +operSucess+"  "); 
                      }
                     }
                }                          			 
 	        }    
 	       }    	       
           // 更新平台发布状态       
 	       if (sucessPub)
 	       {
           CntPlatformSync cntPlatformSyncTemp = new CntPlatformSync();
           cntPlatformSyncTemp.setSyncindex(cntPlatformSynctmp.getSyncindex());
           cntPlatformSyncTemp.setStatus(200);
           cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTemp);
 	       }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	
		rtnPubStr = sucessPub==false ? "1:"+resultPubStr.toString():"0:"+resultPubStr.toString();

    	return rtnPubStr;   
    	}   
         
    
    //针对网元发布
    public String publishproCrmProTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		ProgramCrmMap programCrmMap = new ProgramCrmMap();
    		programCrmMap.setMapindex(Long.parseLong(mapindex));

    		programCrmMap.setTargetindex(Long.parseLong(targetindex));
    		programCrmMap.setObjecttype(34);
			List<ProgramCrmMap> programCrmMapList = new ArrayList<ProgramCrmMap>();

			programCrmMapList =programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
			if (programCrmMapList == null ||programCrmMapList.size()==0)
			{
				return "1:操作失败,点播内容关联角色记录不存在";
			}
    		int synType = 1;
            String initresult="";

    		initresult =initData(programCrmMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programCrmMap.getMapindex());
			cntPlatformSync.setObjecttype(34);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            ProgramCrmMap programCrmMapObj=new ProgramCrmMap();
            int  platformType = 1;
            
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programCrmMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		
    		Castrolemap castrolemap = new Castrolemap();
    		castrolemap.setCastrolemapindex(programCrmMapList.get(0).getCastrolemapindex());
    		castrolemap=castrolemapds.getCastrolemap(castrolemap);
    		
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programCrmMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programCrmMapObj.setTargetindex(Long.parseLong(targetindex));
    			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMapObj);
    	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (programCrmMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
    	    	int targettype = programCrmMapList.get(0).getTargettype();
     	       procastrolemap.put("programcastroleMap", programCrmMapList);
                CntTargetSync cntTargetSync = new CntTargetSync();
				
				cntTargetSync.setObjectindex(programCrmMapList.get(0).getProgramindex());
				cntTargetSync.setTargetindex(programCrmMapList.get(0).getTargetindex());
				cntTargetSync.setObjecttype(3);
				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
				//点播内容在该平台已发布
				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
				{
                    return "1:操作失败,该点播内容没有发布到该网元";

            	}
	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();

	            cntTargetSyncTmp.setObjectindex(programCrmMapList.get(0).getCastrolemapindex());
	            cntTargetSyncTmp.setTargetindex(programCrmMapList.get(0).getTargetindex());
	            cntTargetSyncTmp.setObjecttype(12);
	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
				//栏目在该平台已发布
				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
				{
                    return "1:操作失败,该角色没有发布到该网元";

            	}
				
                int synTargetType =1;
                if (programCrmMapList.get(0).getStatus()== 500) //修改发布    
                {
            		synTargetType =2;
            		synType = 2;

                }
               	if (targettype ==2)  //EPG系统
               	{
               		String xml;
               		if (synTargetType==1)
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 1); //新增发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}
               		else
               		{
                        xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 2); //修改发布到EPG
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
               		}    					

                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = programCrmMapList.get(0).getMappingid();
                    String parentid = programCrmMapList.get(0).getProgramid();
                    String  elementid = programCrmMapList.get(0).getCastrolemapid();
                    Long objectindex = programCrmMapList.get(0).getMapindex();

                    insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex)); //插入同步任务

                    cntTargetSyncTemp.setSyncindex(programCrmMapList.get(0).getSyncindex());
                    if (synTargetType ==1)
                    {
                    	cntTargetSyncTemp.setOperresult(10);
                    }
                    else
                    {
                    	cntTargetSyncTemp.setOperresult(40);
                    }                            
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programCrmMapList.get(0).getMappingid()+","+programCrmMapList.get(0).getTargetid(), 
                    		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			programCrmMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			programCrmMapObj.setTargetindex(Long.parseLong(targetindex));

        			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMapObj);
        	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	if (programCrmMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
                    int targettype = programCrmMapList.get(0).getTargettype();

          	        procastrolemap.put("programcastroleMap", programCrmMapList);
                    CntTargetSync cntTargetSync = new CntTargetSync();

    				cntTargetSync.setObjectindex(programCrmMapList.get(0).getProgramindex());
    				cntTargetSync.setTargetindex(programCrmMapList.get(0).getTargetindex());
    				cntTargetSync.setObjecttype(3);
    				cntTargetSync = cntTargetSyncDS.getCntTargetSync(cntTargetSync);
    				//点播内容在该平台已发布
    				if (cntTargetSync == null||(cntTargetSync != null && cntTargetSync.getStatus() != 300 && cntTargetSync.getStatus() != 500))
    				{
                        return "1:操作失败,该点播内容没有发布到该网元";

                	}
    	            CntTargetSync cntTargetSyncTmp = new CntTargetSync();

    	            cntTargetSyncTmp.setObjectindex(programCrmMapList.get(0).getCastrolemapindex());
    	            cntTargetSyncTmp.setTargetindex(programCrmMapList.get(0).getTargetindex());
    	            cntTargetSyncTmp.setObjecttype(12);
    	            cntTargetSyncTmp = cntTargetSyncDS.getCntTargetSync(cntTargetSyncTmp);
    				//栏目在该平台已发布
    				if (cntTargetSyncTmp == null||(cntTargetSyncTmp != null && cntTargetSyncTmp.getStatus() != 300 && cntTargetSyncTmp.getStatus() != 500))
    				{
                        return "1:操作失败,该角色没有发布到该网元";

                	}
    				
                    int synTargetType =1;
                    if (programCrmMapList.get(0).getStatus()== 500) //修改发布    
                    {
                		synTargetType =2;
        	            synType = 2;

                    }

                   		String xml;
               			if (synTargetType ==1)
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 1); //新增发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }               			}
               			else
               			{
                            xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 2); //修改发布
                            if (null == xml)
                            {
                                return "1:操作失败,生成同步指令xml文件失败";
                            }
               			}

                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programCrmMapList.get(0).getMappingid();
                        String parentid = programCrmMapList.get(0).getProgramid();
                        String  elementid = programCrmMapList.get(0).getCastrolemapid();
                        Long objectindex = programCrmMapList.get(0).getMapindex();
                        String elementcode = castrolemap.getCastrolemapcode();
                        String parentcode = cmsProgram.getCpcontentid();
                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcode,parentcode,Long.valueOf(targetindex),synType);
                        insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex)); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(programCrmMapList.get(0).getSyncindex());
                        if (synTargetType ==1)
                        {
                        	cntTargetSyncTemp.setOperresult(10);

                        }
                        else
                        {
                        	cntTargetSyncTemp.setOperresult(40);
                        }                            
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(programCrmMapList.get(0).getMappingid()+","+programCrmMapList.get(0).getTargetid(), 
                        		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     

                }

               // 更新平台发布状态         
   			List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();

    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(programCrmMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	

    	return result; 
    }
        
    
    //针对网元取消发布
    public String delPublishproCrmProTarget(String mapindex,  String targetindex,int type) throws Exception
    {    	
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
    	try
    	{      
    		ProgramCrmMap programCrmMap = new ProgramCrmMap();
    		programCrmMap.setMapindex(Long.parseLong(mapindex));

    		programCrmMap.setTargetindex(Long.parseLong(targetindex));
    		programCrmMap.setObjecttype(34);
			List<ProgramCrmMap> programCrmMapList = new ArrayList<ProgramCrmMap>();

			programCrmMapList =programCrmMapDS.getProgramCrmMapByCond(programCrmMap);
			if (programCrmMapList == null ||programCrmMapList.size()==0)
			{
				return "1:操作失败,点播内容关联角色记录不存在";
			}
            String initresult="";
             int synType = 3;

    		initresult =initData(programCrmMapList.get(0),targetindex,3,synType);
    	    if(!ProgramSynConstants.SUCCESS.equals(initresult))
    	    {
    	        return initresult;
    	    }
        	CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programCrmMap.getMapindex());
			cntPlatformSync.setObjecttype(34);
    		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();
    		
            CntTargetSync cntTargetSyncTemp = new CntTargetSync();

            ProgramCrmMap programCrmMapObj=new ProgramCrmMap();
            int  platformType = 1;
            
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programCrmMapList.get(0).getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		
    		Castrolemap castrolemap = new Castrolemap();
    		castrolemap.setCastrolemapindex(programCrmMapList.get(0).getCastrolemapindex());
    		castrolemap=castrolemapds.getCastrolemap(castrolemap);
            
    	    if (type == 1)
    	    {
    	    	platformType = 2;
       			cntPlatformSync.setPlatform(2);
    			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
    			programCrmMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    			programCrmMapObj.setTargetindex(Long.parseLong(targetindex));
    			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMapObj);
    	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
    	    	{
                    return "1:操作失败,获取目标网元发布记录失败";
    	    	}
    	    	if (programCrmMapList.get(0).getTargetstatus()!=0)
    	    	{
                    return "1:操作失败,目标系统状态不正确";
    	    	}
     	        procastrolemap.put("programcastroleMap", programCrmMapList);

     	        int targettype = programCrmMapList.get(0).getTargettype();
               	if (targettype ==2)  //EPG系统
               	{
                    String xml = StandardImpFactory.getInstance().create(2).createXmlFile(procastrolemap, targettype, 34, 3); // 取消发布到EPG
                    if (null == xml)
                    {
                        return "1:操作失败,生成同步指令xml文件失败";
                    }
                    String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                    //Mapping 对象日志记录
                    Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                    String objectidMapping = programCrmMapList.get(0).getMappingid();
                    String parentid = programCrmMapList.get(0).getProgramid();
                    String  elementid = programCrmMapList.get(0).getCastrolemapid();
                    Long objectindex = programCrmMapList.get(0).getMapindex();
                    insertObjectMappingRecord( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,Long.valueOf(targetindex),synType);
                    insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex)); //插入同步任务

                    cntTargetSyncTemp.setSyncindex(programCrmMapList.get(0).getSyncindex());
                    cntTargetSyncTemp.setOperresult(70);
                    cntTargetSyncTemp.setStatus(200);
                    cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                    CommonLogUtil.insertOperatorLog(programCrmMapList.get(0).getMappingid()+","+programCrmMapList.get(0).getTargetid(), 
                    		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                            CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL_INFO,
                            CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     
                   }
               	}
    	       if (type == 2)
    	       {
          			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			programCrmMapObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
        			programCrmMapObj.setTargetindex(Long.parseLong(targetindex));

        			programCrmMapList = programCrmMapDS.getProgramnormalCrmMapSynByCond(programCrmMapObj);
        	    	if (programCrmMapList == null || programCrmMapList.size() == 0)
        	    	{
                        return "1:操作失败,获取目标网元发布记录失败";
        	    	}
        	    	
        	    	if (programCrmMapList.get(0).getTargetstatus()!=0)
        	    	{
                        return "1:操作失败,目标系统状态不正确";
        	    	}
          	        procastrolemap.put("programcastroleMap", programCrmMapList);
                    int targettype = programCrmMapList.get(0).getTargettype();

                   		String xml;

                        xml = StandardImpFactory.getInstance().create(1).createXmlFile(procastrolemap, targettype, 34, 3); //修改发布
                        if (null == xml)
                        {
                            return "1:操作失败,生成同步指令xml文件失败";
                        }
                        String correlateidBMS = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                        Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex

                        //Mapping 对象日志记录
                        Long syncindexMapping = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                        String objectidMapping = programCrmMapList.get(0).getMappingid();
                        String parentid = programCrmMapList.get(0).getProgramid();
                        String  elementid = programCrmMapList.get(0).getCastrolemapid();
                        Long objectindex = programCrmMapList.get(0).getMapindex();
                        String elementcoe = castrolemap.getCastrolemapcode();
                        String parentcode = cmsProgram.getCpcontentid();
                        insertObjectMappingIPTV2Record( syncindexMapping, index,objectindex,objectidMapping, elementid, parentid,elementcoe,parentcode,Long.valueOf(targetindex),synType);
                        insertSyncTask(index,xml,correlateidBMS,Long.valueOf(targetindex)); //插入同步任务

                        cntTargetSyncTemp.setSyncindex(programCrmMapList.get(0).getSyncindex());
                        cntTargetSyncTemp.setOperresult(70);
                        cntTargetSyncTemp.setStatus(200);
                        cntTargetSyncDS.updateCntTargetSync(cntTargetSyncTemp); //修改网元发布状态
                        CommonLogUtil.insertOperatorLog(programCrmMapList.get(0).getMappingid()+","+programCrmMapList.get(0).getTargetid(), 
                        		CommonLogConstant.MGTTYPE_VODTARGETSYN,
                                CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL, CommonLogConstant.OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL_INFO,
                                CommonLogConstant.RESOURCE_OPERATION_SUCCESS);                     

                }

               // 更新平台发布状态         
   			List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();

    	       CntTargetSync cntTargetSyncObj = new CntTargetSync();
    	       cntTargetSyncObj.setRelateindex(cntPlatformSynctmp.getSyncindex());
    	       cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSyncObj);
               int[] targetSyncStatus = new int[cntTargetSyncList.size()];
               for(int i = 0;i<cntTargetSyncList.size() ;i++)
               {
               	targetSyncStatus[i] =cntTargetSyncList.get(i).getStatus();
               }
               
               int status = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);
               if (cntPlatformSynctmp.getStatus() != status) //平台状态与计算出的状态不一致需要更新
               {
                   CntPlatformSync cntPlatformSyncTmp = new CntPlatformSync();
                   cntPlatformSyncTmp.setObjectindex(programCrmMap.getMapindex());
                   cntPlatformSyncTmp.setSyncindex(cntPlatformSynctmp.getSyncindex());
                   cntPlatformSyncTmp.setStatus(status);
                   cntPlatformSyncTmp.setPlatform(platformType);
                   cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSyncTmp);
               }    
    	}
        catch (Exception e)
        {
            throw e;
        }
    	

    	return result; 
    }
        
        
    
    
    
    public String initData(ProgramCrmMap programCrmMap,String targetindex,int platfType,int synType)throws Exception
    {
    	try
    	{
    		CntPlatformSync cntPlatformSync = new CntPlatformSync();
			cntPlatformSync.setObjectindex(programCrmMap.getMapindex());
			cntPlatformSync.setObjecttype(34);
    		CmsProgram cmsProgram = new CmsProgram();
    		cmsProgram.setProgramindex(programCrmMap.getProgramindex());
    		cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram); 
    		if (cmsProgram == null)
    		{
                return "1:操作失败,内容不存在";
    		}
    		
    		Castrolemap castrolemap = new Castrolemap();
    		castrolemap.setCastrolemapindex(programCrmMap.getCastrolemapindex());
    		castrolemap=castrolemapds.getCastrolemap(castrolemap);
    		if (castrolemap == null)
    		{
                return "1:操作失败,角色不存在";
    		}
    		
    		if(platfType == 1 ||platfType == 2) //发布到3.0平台 或者2.0平台
    		{
        		CntPlatformSync cntPlatformSynctmp = new CntPlatformSync();

        		if (platfType == 1)
        		{
        			cntPlatformSync.setPlatform(2);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}

        		if (platfType == 2)
        		{
        			cntPlatformSync.setPlatform(1);
        			cntPlatformSynctmp =cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
        			if (cntPlatformSynctmp==null)
        			{
                        return "1:操作失败,发布记录不存在";
        			}
        		}
        		
    			if (synType == 1) //发布
    			{
    				//状态为待发布、发布失败、修改发布失败才能发布
    				if(cntPlatformSynctmp.getStatus()!=0&&cntPlatformSynctmp.getStatus()!=400&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}	
    			}
    			else  //取消发布
    			{   
    				//状态为发布成功、修改发布失败才能取消发布
    				if(cntPlatformSynctmp.getStatus()!=300&&cntPlatformSynctmp.getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    			
                /**
                 * 获取关联目标网元及状态
                 * */
                List<CntTargetSync> cntTargetSyncList = new ArrayList<CntTargetSync>();
                CntTargetSync cntTargetSyncTemp = new CntTargetSync();
                cntTargetSyncTemp.setRelateindex(cntPlatformSynctmp.getSyncindex());
                
                cntTargetSyncList = cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSyncTemp);
                if (cntTargetSyncList == null||cntTargetSyncList.size()==0)
                {
                    return "1:操作失败,获取目标网元发布记录失败";
                }
                if (synType ==1)
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==0||cntTargetSync.getStatus()==400||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";

                	}
                }
                else
                {
                	boolean targetstatus = false;
                	for (CntTargetSync cntTargetSync:cntTargetSyncList)
                	{
                		if (cntTargetSync.getStatus()==300||cntTargetSync.getStatus()==500)
                		{
                			targetstatus = true;
                		}
                	}
                	if (!targetstatus)
                	{
                        return "1:操作失败,目标网元发布状态不正确";

                	}
                }                
    		}

    		if (platfType == 3) //发布到网元
    		{
                List<CntTargetSync> cntTargetSyncListTemp = new ArrayList<CntTargetSync>();
    			CntTargetSync cntTargetSync = new CntTargetSync();
    			cntTargetSync.setObjectindex(programCrmMap.getMapindex());
    			cntTargetSync.setObjecttype(34);
    			cntTargetSync.setTargetindex(Long.valueOf(targetindex));
    			cntTargetSyncListTemp =cntTargetSyncDS.getcntSynnormaltargetListBycond(cntTargetSync);
    			if (cntTargetSyncListTemp==null ||cntTargetSyncListTemp.size()==0)
    			{
                    return "1:操作失败,发布记录不存";
    			}
    			
    			if (synType == 1) //发布
    			{
    				//状态为待发布、发布失败、修改发布失败才能发布
    				if(cntTargetSyncListTemp.get(0).getStatus()!=0&&cntTargetSyncListTemp.get(0).getStatus()!=400&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    			else  //取消发布
    			{   
    				//状态为发布成功、修改发布失败才能取消发布
    				if(cntTargetSyncListTemp.get(0).getStatus()!=300&&cntTargetSyncListTemp.get(0).getStatus()!=500)
    				{
                        return "1:操作失败,发布记录状态不正确";
    				}
    			}
    		}
            List<UsysConfig> usysConfigList = null;
            UsysConfig usysConfig = new UsysConfig();
            usysConfig = usysConfigDS.getUsysConfigByCfgkey(FTPADDRESS);
            if (null == usysConfig || null == usysConfig.getCfgvalue()
                || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return "1:操作失败,获取同步内容实体文件FTP地址失败";
            } 
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
    	}
        catch (Exception e)
        {
            throw e;
        }
    	
    	return "success";
    }
    
    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex)
    {
        Long batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
    	CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setBatchid(batchid);
        	cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
    
    private void insertObjectMappingRecord(Long syncindexMapping, Long index,Long objectindex, String objectidMapping, String elementid, String parentid,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindex);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(34);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    } 
    
    private void insertObjectMappingIPTV2Record(Long syncindexMapping, Long index,Long objectindex, String objectidMapping, 
    		String elementid, String parentid,String elementcode,String parentcode,Long targetindex,int synType)
    {
    	ObjectSyncRecord objectSyncMappingRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
        	objectSyncMappingRecord.setSyncindex(syncindexMapping);
        	objectSyncMappingRecord.setTaskindex(index);
        	objectSyncMappingRecord.setObjectindex(objectindex);
        	objectSyncMappingRecord.setObjectid(objectidMapping);
        	objectSyncMappingRecord.setDestindex(targetindex);
        	objectSyncMappingRecord.setObjecttype(34);
        	objectSyncMappingRecord.setElementid(elementid);
        	objectSyncMappingRecord.setParentid(parentid);
        	objectSyncMappingRecord.setElementcode(elementcode);
        	objectSyncMappingRecord.setParentcode(parentcode);
        	objectSyncMappingRecord.setActiontype(synType);
        	objectSyncMappingRecord.setStarttime(dateformat.format(new Date()));
        	objectSyncRecordDS.insertObjectSyncRecord(objectSyncMappingRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }     
    
     
    /************************* publis manage end*************/
}

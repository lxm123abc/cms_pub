package com.zte.cms.content.objectSynRecord.ls;

import java.util.List;

import com.zte.cms.common.DateUtil2;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ObjectSyncRecordLS implements IObjectSyncRecordLS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    private IObjectSyncRecordDS ds = null;
    
  public void setDs(IObjectSyncRecordDS ds)
  {
      this.ds = ds;
  }

	public void insertObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug("insert objectSyncRecord starting...");
		try
		{
			ds.insertObjectSyncRecord( objectSyncRecord );
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug("insert objectSyncRecord end");
	}
	
	public void updateObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug("update objectSyncRecord by pk starting...");
	    try
		{
			ds.updateObjectSyncRecord( objectSyncRecord );
		}
		catch(Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug("update objectSyncRecord by pk end");
	}

	public void updateObjectSyncRecordList( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException
	{
		log.debug("update objectSyncRecordList by pk starting...");
		if(null == objectSyncRecordList || objectSyncRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectSyncRecordList");
			return;
		}
	    try
		{
			for( ObjectSyncRecord objectSyncRecord : objectSyncRecordList )
			{
		    	ds.updateObjectSyncRecord( objectSyncRecord );
		    }
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug("update objectSyncRecordList by pk end");
	}

	public void removeObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "remove objectSyncRecord by pk starting..." );
		try
		{
			ds.removeObjectSyncRecord(objectSyncRecord);
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug( "remove objectSyncRecord by pk end" );
	}

	public void removeObjectSyncRecordList ( List<ObjectSyncRecord> objectSyncRecordList )throws DomainServiceException
	{
		log.debug( "remove objectSyncRecordList by pk starting..." );
		if(null == objectSyncRecordList || objectSyncRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectSyncRecordList");
			return;
		}
		try
		{
			for( ObjectSyncRecord objectSyncRecord : objectSyncRecordList )
			{
				ds.removeObjectSyncRecord(objectSyncRecord);

			}
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug( "remove objectSyncRecordList by pk end" );
	}



	public ObjectSyncRecord getObjectSyncRecord( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "get objectSyncRecord by pk starting..." );
		ObjectSyncRecord rsObj = null;
		try
		{
			rsObj = ds.getObjectSyncRecord( objectSyncRecord );
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug( "get objectSyncRecordList by pk end" );
		return rsObj;
	}

	public List<ObjectSyncRecord> getObjectSyncRecordByCond( ObjectSyncRecord objectSyncRecord )throws DomainServiceException
	{
		log.debug( "get objectSyncRecord by condition starting..." );
		List<ObjectSyncRecord> rsList = null;
		try
		{
			rsList = ds.getObjectSyncRecordByCond( objectSyncRecord );
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug( "get objectSyncRecord by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize)throws DomainServiceException
	{

	    log.debug( "get objectSyncRecord page info by condition starting..." );
	    
	    //进行时间格式的转换,将时间中的空格 冒号 等去掉
        objectSyncRecord.setStarttime1(DateUtil2.get14Time(objectSyncRecord.getStarttime1()));
        objectSyncRecord.setStarttime2(DateUtil2.get14Time(objectSyncRecord.getStarttime2()));
        
        objectSyncRecord.setEndtime1(DateUtil2.get14Time(objectSyncRecord.getEndtime1()));
        objectSyncRecord.setEndtime2(DateUtil2.get14Time(objectSyncRecord.getEndtime2()));
        //对特殊字符的处理
        if(objectSyncRecord.getObjectid()!=null && !"".equals(objectSyncRecord.getObjectid())){
            objectSyncRecord.setObjectid(EspecialCharMgt.conversion(objectSyncRecord.getObjectid()));
        }
        
	    TableDataInfo tableInfo = null;
	    objectSyncRecord.setOrderCond("taskindex desc, starttime desc");	    
		try
		{
		   
		   // System.out.println("@@@@@@@@@@@@@@@@before@@@@@@@@@@@@");
		    tableInfo = ds.pageInfoQuery(objectSyncRecord, start, pageSize);
            //System.out.println("@@@@@@@@@@@@@@@@after@@@@@@@@@@@@");
		 
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectSyncRecord objectSyncRecord, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get objectSyncRecord page info by condition starting..." );
		TableDataInfo tableInfo = null;
		try
		{		 
		    tableInfo = ds.pageInfoQuery(objectSyncRecord, start, pageSize, puEntity);
			 //       pageInfoQuery(objectSyncRecord, start, pageSize, puEntity);
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
	
		log.debug( "get objectSyncRecord page info by condition end" );
		return tableInfo;
	}

    public TableDataInfo getobjectSyncRecord(ObjectSyncRecord objectSyncRecord, int start, int pageSize)
            throws DomainServiceException
    {


        log.debug( "get objectSyncRecord page info by condition starting..." );
            
      //进行时间格式的转换,将时间中的空格 冒号 等去掉
        objectSyncRecord.setStarttime1(DateUtil2.get14Time(objectSyncRecord.getStarttime1()));
        objectSyncRecord.setStarttime2(DateUtil2.get14Time(objectSyncRecord.getStarttime2()));
        
        objectSyncRecord.setEndtime1(DateUtil2.get14Time(objectSyncRecord.getEndtime1()));
        objectSyncRecord.setEndtime2(DateUtil2.get14Time(objectSyncRecord.getEndtime2()));
        
        TableDataInfo tableInfo = null;
        objectSyncRecord.setOrderCond("taskindex desc, starttime desc");        
        try
        {
           
           // System.out.println("@@@@@@@@@@@@@@@@before@@@@@@@@@@@@");
            tableInfo = ds.getobjectSyncRecord(objectSyncRecord, start, pageSize);
            //System.out.println("@@@@@@@@@@@@@@@@after@@@@@@@@@@@@");
         
        }
        catch( Exception dsEx )
        {
            log.error( "ds exception:", dsEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( dsEx );
        }
        return tableInfo;
    
    }
}

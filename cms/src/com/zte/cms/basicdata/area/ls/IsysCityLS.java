package com.zte.cms.basicdata.area.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceCodeParser;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.sys.model.Tree;
import com.zte.umap.sys.model.UsysCity;
import com.zte.umap.sys.model.UsysProvince;
import com.zte.umap.sys.service.IUsysCityDS;
import com.zte.umap.sys.service.IUsysProvinceDS;

public class IsysCityLS implements IIsysCityLS
{
    private Log log = SSBBus.getLog(getClass());
    private IUsysCityDS ds = null;
    private IUsysProvinceDS usysProvinceDS = null;
    // 内容同步任务DS

    private static final String INITIAL = "10";// 初始
    private static final String SYNCHRONIZING = "21";// 同步中
    private static final String SYNCHRONIZED = "40";// 已同步
    private static final String ADDSYNCHRONIZEDERROR = "41";// 新增同步失败
    private static final String DELSYNCHRONIZEDERROR = "42";// 删除同步失败

    private ResourceCodeParser parser = new ResourceCodeParser(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);

    public void setUsysProvinceDS(IUsysProvinceDS usysProvinceDS)
    {
        this.usysProvinceDS = usysProvinceDS;
    }

    public void setDs(IUsysCityDS ds)
    {
        this.ds = ds;
    }

    public Tree insertUsysCityInIptvTree(String provid, UsysCity usysCity) throws DomainServiceException
    {
        log.debug("insert UsysCityInIptvTree starting...");
        Tree citytree = new Tree();
        UsysProvince usysProvince = new UsysProvince();
        usysProvince.setProvid(provid);
        usysProvince.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysProvince> usysProvList;
        try
        {
            usysProvList = usysProvinceDS.getUsysProvinceByCond(usysProvince);
            if (usysProvList != null && !usysProvList.isEmpty())
            {
                UsysProvince usysProvincenew = usysProvList.get(0);
                usysCity.setProvindex(usysProvincenew.getProvindex());
                usysCity.setProvid(provid);
                usysCity.setCitydesc(INITIAL);// 10:初始
                ds.insertUsysCity(usysCity);
                usysCity = this.getUsysCityById(usysCity);// 获得主键
                citytree = new Tree();
                citytree.setMenuid(usysCity.getCityindex());
                citytree.setMenuname(usysCity.getCityname());
                citytree.setParentid(usysCity.getProvindex());
                citytree.setOperresult(new Integer(1150000)); // 增加成功
                citytree.setFlag(2);
                citytree.setEnabled("1");
                // 操作成功增加日志操作
                CommonLogUtil.insertOperatorLog(usysCity.getCityindex() + "", AreaConstant.MGTTYPE_AREA,
                        AreaConstant.OPERTYPE_AREA_CiTY_ADD, AreaConstant.OPERTYPE_AREA_CiTY_ADD_INFO,
                        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                citytree.setOperresult(new Integer(1150001)); // 该省份不存在或已被删除
            }
        }
        catch (DomainServiceException e)
        {
            log.error("during insert UsysCity errors ");
            throw new DomainServiceException(e);
        }
        log.debug("insert UsysCityInIptvTree ending...");
        return citytree;
    }


    public Tree updateUsysCityInIptvTree(UsysCity usysCity) throws DomainServiceException
    {
        log.debug("update usysCityInIptvTree  starting...");
        Tree citytree = new Tree();
        UsysCity cityinfo = new UsysCity();
        cityinfo.setCityid(usysCity.getCityid());
        try
        {
            cityinfo = this.getUsysCityById(cityinfo);
            usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            if (cityinfo == null)
            {
                citytree.setOperresult(new Integer(1150002)); // 该城市不存在
            }
            else
            {
                UsysProvince usysprovince = new UsysProvince();
                usysprovince.setProvindex(usysCity.getProvindex());
                UsysProvince usysProvince = usysProvinceDS.getUsysProvince(usysprovince);
                if (usysProvince == null)
                {
                    citytree.setOperresult(new Integer(1150001));// 省份不存在或已被删除
                }
                else
                {
                    if (SYNCHRONIZED.equals(usysProvince.getProvdesc())
                            || DELSYNCHRONIZEDERROR.equals(usysProvince.getProvdesc()))
                    {
                        if (INITIAL.equals(cityinfo.getCitydesc())
                                || ADDSYNCHRONIZEDERROR.equals(cityinfo.getCitydesc()))
                        {
                            usysCity.setCityindex(cityinfo.getCityindex());
                            usysCity.setProvindex(cityinfo.getProvindex());
                            ds.updateUsysCity(usysCity);
                            citytree.setMenuid(usysCity.getCityindex());
                            citytree.setMenuname(usysCity.getCityname());
                            citytree.setParentid(cityinfo.getProvindex());
                            citytree.setOperresult(new Integer(1150000)); // 修改成功
                            citytree.setFlag(2);
                            citytree.setEnabled("1");

                            // 操作成功增加日志操作
                            CommonLogUtil.insertOperatorLog(usysCity.getCityindex() + "", AreaConstant.MGTTYPE_AREA,
                                    AreaConstant.OPERTYPE_AREA_CiTY_MOD, AreaConstant.OPERTYPE_AREA_CiTY_MOD_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        else
                        {
                            citytree.setOperresult(new Integer(1150004));// 城市的状态不正确，城市不能修改
                        }
                    }
                    else
                    {
                        citytree.setOperresult(new Integer(1150003));// 省份的状态不正确，城市不能修改
                    }
                }
            }
        }
        catch (DomainServiceException e)
        {

            log.error("during update UsysCity errors ");
            throw new DomainServiceException(e);
        }
        log.debug("update usysCityInIptvTree  ending...");
        return citytree;
    }

    public Tree removeUsysCityFromIptvTree(UsysCity usysCity) throws DomainServiceException
    {
        log.debug("remove usysCityFromIptvTree  starting...");
        Tree citytree = new Tree();
        usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        UsysCity cityinfo = new UsysCity();
        cityinfo.setCityid(usysCity.getCityid());
        try
        {
            cityinfo = this.getUsysCityById(cityinfo);
            if (cityinfo == null)
            {
                citytree.setOperresult(new Integer(1150002)); // 该城市不存在
            }
            else
            {
                UsysProvince usysprovince = new UsysProvince();
                usysprovince.setProvindex(usysCity.getProvindex());
                UsysProvince usysProvince = usysProvinceDS.getUsysProvince(usysprovince);
                if (usysProvince == null)
                {
                    citytree.setOperresult(new Integer(1150001));// 省份不存在或已被删除
                }
                else
                {
                    if (SYNCHRONIZED.equals(usysProvince.getProvdesc())
                            || DELSYNCHRONIZEDERROR.equals(usysProvince.getProvdesc()))
                    {
                        if (INITIAL.equals(cityinfo.getCitydesc()))
                        {
                            usysCity.setCityindex(cityinfo.getCityindex());
                            ds.removeUsysCity(usysCity);
                            citytree.setMenuid(usysCity.getCityindex());
                            citytree.setMenuname(cityinfo.getCityname());
                            citytree.setParentid(cityinfo.getProvindex());
                            citytree.setOperresult(new Integer(1150000)); // 删除成功
                            // 操作成功增加日志操作
                            CommonLogUtil.insertOperatorLog(usysCity.getCityindex() + "", AreaConstant.MGTTYPE_AREA,
                                    AreaConstant.OPERTYPE_AREA_CiTY_DEL, AreaConstant.OPERTYPE_AREA_CiTY_DEL_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        }
                        else
                        {
                            citytree.setOperresult(new Integer(1150006));// 城市的状态不正确，城市不能删除
                        }
                    }
                    else
                    {
                        citytree.setOperresult(new Integer(1150005));// 省份的状态不正确，城市不能删除
                    }
                }
            }
        }
        catch (DomainServiceException e)
        {
            log.error("during remove UsysCity errors ");
            throw new DomainServiceException(e);
        }
        log.debug("remove usysCityFromIptvTree  ending...");
        return citytree;
    }

    // 根据主键查询
    public UsysCity getUsysCity(UsysCity usysCity) throws DomainServiceException
    {
        log.debug("get usysCity  starting...");
        UsysCity usyscity = null;
        usyscity = ds.getUsysCity(usysCity);
        log.debug("get getUsysCity  ending...");
        return usyscity;

    }

    // 根据cityid查询
    public UsysCity getUsysCityById(UsysCity usysCity) throws DomainServiceException
    {
        log.debug("get usysCity  by id starting...");
        UsysCity city = null;
        UsysCity tem = new UsysCity();
        tem.setCityid(usysCity.getCityid());
        tem.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysCity> list = ds.getUsysCityByCond(tem);
        if (list != null && !list.isEmpty())
        {
            city = list.get(0);
        }
        log.debug("get usysCity by id end...");
        return city;

    }

    public List<UsysCity> getUsysCityByCond(UsysCity usysCity) throws DomainServiceException
    {
        log.debug("getUsysCityByCond  starting...");
        usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysCity> listUyscity = null;
        listUyscity = ds.getUsysCityByCond(usysCity);
        log.debug("getUsysCityByCond  ending...");
        return listUyscity;
    }

    public List<Tree> getUsysCityInIptvTree(Tree tree) throws DomainServiceException
    {
        log.debug("getUsysCityInIptvTree  starting...");
        List<Tree> rsList = null;
        tree.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        rsList = ds.getUsysCityInIptvTree(tree);
        log.debug("getUsysCityInIptvTree  ending...");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public HashMap<String, Object> getSysCity(String selectedValue, Boolean needSelect) throws DomainServiceException
    {

        log.debug("getSysCity  starting...");
        HashMap<String, Object> map = new HashMap<String, Object>();
        UsysCity usysCity = new UsysCity();
        usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        if (needSelect)
        {

            //textList.add("--请选择--");
        	textList.add(ResourceMgt.findDefaultText("log.please.choice"));
            valueList.add("");
        }
        List<UsysCity> list = (List<UsysCity>) ds.getUsysCityByCond(usysCity);
        for (UsysCity item : list)
        {
            textList.add(item.getCityname());
            valueList.add(item.getCityid());
        }
        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);
        log.debug("getSysCity  ending...");
        return map;

    }

    public HashMap<String, Object> getSysCityByProvid(String provid, String selectedValue, Boolean needSelect)
            throws DomainServiceException
    {

        log.debug("getSysCity  starting...");
        HashMap<String, Object> map = new HashMap<String, Object>();

        UsysCity usysCity = new UsysCity();
        usysCity.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        if (needSelect)
        {

            textList.add("--请选择--");
            valueList.add("");
        }

        // 获取所有的城市
        if (provid == null)
        {
            List<UsysCity> list = (List<UsysCity>) ds.getUsysCityByCond(usysCity);
            for (UsysCity item : list)
            {
                textList.add(item.getCityname());
                valueList.add(item.getCityid());
            }

        }

        // 获取省份下对应的城市
        if (provid != null && !"".equals(provid))
        {
            usysCity.setProvid(provid);
            List<UsysCity> list = (List<UsysCity>) ds.getUsysCityByCond(usysCity);
            for (UsysCity item : list)
            {
                textList.add(item.getCityname());
                valueList.add(item.getCityid());
            }
        }

        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);
        log.debug("getSysCity  ending...");
        return map;
    }

    public String getUsysCityByName(UsysCity usyscity) throws DomainServiceException
    {
        log.debug("getUsysCityByName start ");
        UsysCity rsObj = null;
        UsysCity temp = new UsysCity();
        temp.setProvid(usyscity.getProvid());
        temp.setCityname(usyscity.getCityname());
        temp.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        List<UsysCity> provlist = ds.getUsysCityByCond(temp);
        if (provlist.size() == 0)
        {
            return "0";
        }
        if (provlist.size() == 1)
        {
            rsObj = provlist.get(0);
            if (rsObj.getCityid().equals(usyscity.getCityid()))
            {
                return "0";
            }
        }

        log.debug("getUsysCityByName end");
        return "1";
    }

}

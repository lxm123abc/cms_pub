package com.zte.cms.content.wkfw.ls;

import java.sql.SQLException;
import java.util.List;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.jwf.interfaces.business.workflow.model.ProcessBase;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmContentWkfw
{
    /**
     * 我待处理的工作流查询方法
     * 
     * @param condition
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getMyNeedHandleWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 我已经处理的工作流历史查询方法
     * 
     * @param condition
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getMyHaveHandleWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 我申请过的工作流历史查询方法
     * 
     * @param condition
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getMyStartWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 我待处理的工作流数量查询方法
     * 
     * @param condition
     * @return
     * @throws DomainServiceException
     */
    public long getMyNeedHandleWorkflowItemListCount(IcmContentWkfwCondition condition) throws DomainServiceException;

    /**
     * 获取操作员待处理的工作流数量查询方法
     * 
     * @param condition
     * @return
     * @throws DomainServiceException
     */
    public long getOperatorNeedHandleWorkflowItemListCount(IcmContentWkfwCondition condition)
            throws DomainServiceException;

    /**
     * 内容审核历史工单查询方法
     * 
     * @param processId
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getWorkflowTrack(String processId);

    /**
     * 内容审核历史工单无数据时提示方法
     * 
     * @param processId
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo getErroeWorkflowTrack(String processId);

    public ProcessBase getProcessInstance(String processId);
}

package com.zte.cms.picture.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServicePictureMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.String mappingid;
    private java.lang.Long serviceindex;
    private java.lang.String serviceid;
    private java.lang.Long pictureindex;
    private java.lang.String pictureid;
    private java.lang.Integer maptype;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    }

    public void setServiceindex(java.lang.Long serviceindex)
    {
        this.serviceindex = serviceindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.Long getPictureindex()
    {
        return pictureindex;
    }

    public void setPictureindex(java.lang.Long pictureindex)
    {
        this.pictureindex = pictureindex;
    }

    public java.lang.String getPictureid()
    {
        return pictureid;
    }

    public void setPictureid(java.lang.String pictureid)
    {
        this.pictureid = pictureid;
    }

    public java.lang.Integer getMaptype()
    {
        return maptype;
    }

    public void setMaptype(java.lang.Integer maptype)
    {
        this.maptype = maptype;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("mappingid", "MAPPINGID");
        this.addRelation("serviceindex", "SERVICEINDEX");
        this.addRelation("serviceid", "SERVICEID");
        this.addRelation("pictureindex", "PICTUREINDEX");
        this.addRelation("pictureid", "PICTUREID");
        this.addRelation("maptype", "MAPTYPE");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
    }
}

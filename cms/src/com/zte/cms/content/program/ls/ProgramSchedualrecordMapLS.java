package com.zte.cms.content.program.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.model.ProgramSchedualrecordMap;
import com.zte.cms.content.program.service.ICmsProgramDS;
import com.zte.cms.content.program.service.IProgramSchedualrecordMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class ProgramSchedualrecordMapLS extends DynamicObjectBaseDS implements IProgramSchedualrecordMapLS
{

    public static final String BATCHID = "ucdn_task_batch_id";// 任务批次号
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    public final static String CNTSYNCXML = "xmlsync";
    public final static String PROGRAM_DIR = "program";

    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String ONLINE_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败

    public static final String FTPADDRESS = "cms.cntsyn.ftpaddress";

    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private IProgramSchedualrecordMapDS programSchedualrecordMapDS;
    private ICmsSchedulerecordDS cmsSchedulerecordDS;
    private ICmsProgramDS cmsProgramDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ITargetsystemDS targetSystemDS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IBatchObjectRecordDS batchObjectDS;

    public TableDataInfo pageInfoQuery(ProgramSchedualrecordMap programSchedualrecordMap, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try
        {
            dataInfo = programSchedualrecordMapDS.pageInfoQuery(programSchedualrecordMap, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        log.debug("pageInfoQuery end");
        return dataInfo;
    }

    public String insertProgramSchedualrecordMap(String schedualrecordIdList, String programindex)
    {
        log.debug("insertProgramSchedualrecordMap starting...");
        ReturnInfo rtnInfo = new ReturnInfo();
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int index = 0;
        OperateInfo operateInfo = null;
        schedualrecordIdList = schedualrecordIdList.substring(0, schedualrecordIdList.length() - 1);
        String[] arrayList = schedualrecordIdList.split(",");
        try
        {
            CmsProgram cmsProgram = new CmsProgram();
            cmsProgram.setProgramindex(Long.valueOf(programindex));
            cmsProgram = cmsProgramDS.getCmsProgram(cmsProgram);
            if (cmsProgram == null)
            {
                rtnInfo.setFlag("1");
                //rtnInfo.setReturnMessage("内容不存在");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cnt.cntnot.exist"));
                return rtnInfo.toString();
            }
            else if (cmsProgram.getStatus() != 20 && cmsProgram.getStatus() != 50 && cmsProgram.getStatus() != 60)
            {
                rtnInfo.setFlag("1");
                //rtnInfo.setReturnMessage("内容状态不正确");
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("cnt.cntstatus.wrong"));
                return rtnInfo.toString();
            }

            for (String schedualrecordId : arrayList)
            {
                index++;
                try
                {
                    CmsSchedulerecord cmsSchedulerecord = new CmsSchedulerecord();
                    cmsSchedulerecord.setSchedulerecordid(schedualrecordId);

                    List<CmsSchedulerecord> list = cmsSchedulerecordDS.getCmsSchedulerecordByCond(cmsSchedulerecord);
                    if (list != null && list.size() > 0)
                    {
                        cmsSchedulerecord = list.get(0);
                    }
                    else
                    {
                        ifail++;//收录计划不存在
                        operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId), ResourceMgt.findDefaultText("do.fail"),
                        		ResourceMgt.findDefaultText("record.plan.notexist"));
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }

                    if (cmsSchedulerecord == null)
                    {
                        ifail++;//收录计划不存在
                        operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId), ResourceMgt.findDefaultText("do.fail"),
                        		ResourceMgt.findDefaultText("record.plan.notexist"));
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else if (cmsSchedulerecord.getStatus() != 20 && cmsSchedulerecord.getStatus() != 50
                            && cmsSchedulerecord.getStatus() != 60)
                    {
                        ifail++;//收录计划状态不正确
                        operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId), ResourceMgt.findDefaultText("do.fail"),
                        		ResourceMgt.findDefaultText("record.plan.status.wrong"));
                        rtnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        ProgramSchedualrecordMap programSchedualrecordMap = new ProgramSchedualrecordMap();
                        programSchedualrecordMap.setProgramid(cmsProgram.getProgramid());
                        programSchedualrecordMap.setProgramindex(cmsProgram.getProgramindex());
                        programSchedualrecordMap.setSchrecordid(cmsSchedulerecord.getSchedulerecordid());
                        programSchedualrecordMap.setSchrecordindex(cmsSchedulerecord.getSchedulerecordindex());
                        programSchedualrecordMap.setMaptype(33);
  
                        List pslist = programSchedualrecordMapDS
                                .getProgramSchedualrecordMapByCond(programSchedualrecordMap);
                        if (pslist != null && pslist.size() > 0)
                        {
                            ifail++;//内容已和收录计划关联
                            operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId),
                            		ResourceMgt.findDefaultText("do.fail"), ResourceMgt.findDefaultText("cnt.connected.with.record"));
                            rtnInfo.appendOperateInfo(operateInfo);
                            continue;
                        }
                        else
                        {
                            programSchedualrecordMap.setStatus(0);
                            programSchedualrecordMapDS.insertProgramSchedualrecordMap(programSchedualrecordMap);
                            iSuccessed++;
                            operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId),
                            		ResourceMgt.findDefaultText("cnt.dosuccess"), ResourceMgt.findDefaultText("cnt.dosuccess"));
                            rtnInfo.appendOperateInfo(operateInfo);
                            
                            
                            CommonLogUtil.insertOperatorLog(programSchedualrecordMap.getMapindex().toString(), CommonLogConstant.MGTTYPE_PROGRAM,
                                    CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_ADD, CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_ADD_INFO,
                                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                            continue;
                        }
                    }
                }
                catch (Exception e)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index), String.valueOf(schedualrecordId), ResourceMgt.findDefaultText("do.fail"),
                    		ResourceMgt.findDefaultText("do.fail"));
                    rtnInfo.appendOperateInfo(operateInfo);
                    e.printStackTrace();
                    continue;
                }
            }

            if (iSuccessed == arrayList.length)
            {
                rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed);
                rtnInfo.setFlag("0");
            }
            else
            {
                if (ifail == arrayList.length)
                {
                    rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + ifail);
                    rtnInfo.setFlag("1");
                }
                else
                {
                    rtnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + iSuccessed + ResourceMgt.findDefaultText("fail.count") + ifail);
                    rtnInfo.setFlag("2");
                }
            }
            
            
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        log.debug("insertProgramSchedualrecordMap end");
        return rtnInfo.toString();
    }

    public String deleteProgramSchedualrecordMap(Long mapindex) throws Exception
    {
        log.debug("deleteProgramSchedualrecordMap  starting...");
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");
        try
        {
            ProgramSchedualrecordMap psrMap = new ProgramSchedualrecordMap();
            psrMap.setMapindex(mapindex);
            psrMap = programSchedualrecordMapDS.getProgramSchedualrecordMap(psrMap);
            if (psrMap == null)
            {//1:操作失败,关联的录制计划不存在
                return ResourceMgt.findDefaultText("do.fail.record.plan.notexist");
            }
            else if (psrMap.getStatus() != 0 && psrMap.getStatus() != 30 && psrMap.getStatus() != 80)
            {//1:操作失败,关联的录制计划状态不正确
                return ResourceMgt.findDefaultText("do.fail.record.plan.status.wrong");
            }
            else
            {
                programSchedualrecordMapDS.removeProgramSchedualrecordMap(psrMap);
            }
            
            
            CommonLogUtil.insertOperatorLog(psrMap.getMapindex().toString(), CommonLogConstant.MGTTYPE_PROGRAM,
                    CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_DEL, CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_DEL_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        log.debug("deleteProgramSchedualrecordMap end");
        return result;
    }

    /**
     * 批量删除内容
     */
    public String batchDeletePsm(List<ProgramSchedualrecordMap> list)
    {
        log.debug("batchDeletePsm starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            int success = 0;
            int fail = 0;
            int index = 1;

            String message = "";
            String result = "";

            for (ProgramSchedualrecordMap psm : list)
            {
                message = deleteProgramSchedualrecordMap(psm.getMapindex());
                if (message.substring(0, 1).equals("0"))
                {
                    success++;
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                }
                else
                {
                    fail++;
                    result = message.substring(2);
                    message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                }

                operateInfo = new OperateInfo(String.valueOf(index++), psm.getSchrecordid(), message, result);
                returnInfo.appendOperateInfo(operateInfo);

            }

            if (success == list.size())
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }

        log.debug("batchDeletePsm end");
        return returnInfo.toString();
    }

    /**
     * 根据type批量发布或取消发布 type:0-发布 1-取消发布
     */
    public String batchPublishOrDelPublish(List<ProgramSchedualrecordMap> list, String type)
    {
        log.debug("batchPublishOrDelPublish starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            int success = 0;
            int fail = 0;
            int index = 1;

            String message = "";
            String result = "";
            if (type.equals("0"))
            {
                for (ProgramSchedualrecordMap psm : list)
                {
                    message = publishPsrMap(psm.getMapindex());
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        fail++;
                        result = message.substring(2);
                        message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);
                    }

                    operateInfo = new OperateInfo(String.valueOf(index++), psm.getSchrecordid(), message, result);
                    returnInfo.appendOperateInfo(operateInfo);

                }
            }
            else
            {
                for (ProgramSchedualrecordMap psm : list)
                {
                    message = cancelPublishPsrMap(psm.getMapindex());
                    ;
                    if (message.substring(0, 1).equals("0"))
                    {
                        success++;
                        message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        fail++;
                        result = message.substring(2);
                        message = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);

                    }

                    operateInfo = new OperateInfo(String.valueOf(index++), psm.getSchrecordid(), message, result);
                    returnInfo.appendOperateInfo(operateInfo);

                }
            }

            if (success == list.size())
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
                returnInfo.setFlag("0");
            }
            else
            {
                if (fail == list.size())
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("1");
                }
                else
                {
                    returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count")+ success + ResourceMgt.findDefaultText("fail.count") + fail);
                    returnInfo.setFlag("2");
                }
            }

        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }

        log.debug("batchPublishOrDelPublish end");
        return returnInfo.toString();
    }

    public String publishPsrMap(Long mapindex) throws Exception
    {
        log.debug("publishPsrMap  starting...");
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");

        try
        {
            ProgramSchedualrecordMap psrMap = new ProgramSchedualrecordMap();
            psrMap.setMapindex(mapindex);
            psrMap = programSchedualrecordMapDS.getProgramSchedualrecordMap(psrMap);
            if (psrMap == null)
            {//1:操作失败,关联的录制计划不存在
                return ResourceMgt.findDefaultText("do.fail.record.plan.notexist");
            }
            else if (psrMap.getStatus() != 0 && psrMap.getStatus() != 30 && psrMap.getStatus() != 80)
            {//1:操作失败,关联的录制计划状态不正确
                return ResourceMgt.findDefaultText("do.fail.record.plan.status.wrong");
            }

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return ResourceMgt.findDefaultText("get.temporaryadress.failed");
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return ResourceMgt.findDefaultText("dest.adress.notfound.checkbind");
                }

                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();// 获取任务批次号
                String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

                String epgDestIndex = getTargetSystem(2);
                if (epgDestIndex == null)
                {
                    //return "1:获取EGP系统失败";
                	return ResourceMgt.findDefaultText("get.egp.failed");
                }

                String EPG_XMLAddress = writeXML(psrMap, 1, desPath); // 生成发布EPG的XML文件，并获取XML存放路径

                // 内容向EPG系统发布 插入同步任务 并且生成内容和其子内容的批处理对象信息
                insertCntSyncTask(batchid, correlateidEPG, psrMap.getMapindex(), EPG_XMLAddress, 33, 1, 2,
                        epgDestIndex, 1, 1, 1);
                inserBatchObjectRecord(batchid, psrMap.getMapindex(), 33, 2, epgDestIndex);

                psrMap.setStatus(10);
                programSchedualrecordMapDS.updateProgramSchedualrecordMap(psrMap);
            }
            
            CommonLogUtil.insertOperatorLog(psrMap.getMapindex().toString(), CommonLogConstant.MGTTYPE_PROGRAM,
                    CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_PUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_PUBLISH_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        log.debug("publishPsrMap end");
        return result;
    }

    public String cancelPublishPsrMap(Long mapindex) throws Exception
    {
        log.debug("cancelPublishPsrMap  starting...");
        //String result = "0:操作成功";
        String result = ResourceMgt.findDefaultText("cnt.do.success");

        try
        {
            ProgramSchedualrecordMap psrMap = new ProgramSchedualrecordMap();
            psrMap.setMapindex(mapindex);
            psrMap = programSchedualrecordMapDS.getProgramSchedualrecordMap(psrMap);
            if (psrMap == null)
            {//1:操作失败,关联的录制计划不存在
                return ResourceMgt.findDefaultText("do.fail.record.plan.notexist");
            }
            else if (psrMap.getStatus() != 20 && psrMap.getStatus() != 90)
            {//1:操作失败,关联的录制计划状态不正确
                return ResourceMgt.findDefaultText("do.fail.record.plan.status.wrong");
            }

            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return ResourceMgt.findDefaultText("get.temporaryadress.failed");
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return ResourceMgt.findDefaultText("dest.adress.notfound.checkbind");
                }

                String batchid = this.getPrimaryKeyGenerator().getPrimarykey(BATCHID).toString();// 获取任务批次号
                String correlateidEPG = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();

                String epgDestIndex = getTargetSystem(2);
                if (epgDestIndex == null)
                {
                    //return "1:获取EGP系统失败";          
                	return ResourceMgt.findDefaultText("get.egp.failed");
                }

                String EPG_XMLAddress = writeXML(psrMap, 3, desPath); // 生成发布EPG的XML文件，并获取XML存放路径

                // 内容向EPG系统发布 插入同步任务 并且生成内容和其子内容的批处理对象信息
                insertCntSyncTask(batchid, correlateidEPG, psrMap.getMapindex(), EPG_XMLAddress, 33, 3, 2,
                        epgDestIndex, 1, 1, 1);
                inserBatchObjectRecord(batchid, psrMap.getMapindex(), 33, 2, epgDestIndex);

                psrMap.setStatus(70);
                programSchedualrecordMapDS.updateProgramSchedualrecordMap(psrMap);
            }
            
            CommonLogUtil.insertOperatorLog(psrMap.getMapindex().toString(), CommonLogConstant.MGTTYPE_PROGRAM,
                    CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_DELPUBLISH, CommonLogConstant.OPERTYPE_PROGRAM_SCHEDUALRECORD_DELPUBLISH_INFO,
                    CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        log.debug("cancelPublishPsrMap end");
        return result;
    }

    /**
     * 
     * @param batchid 任务批次号
     * @param objindex 同步对象index
     * @param objtype 同步对象类型：若干object
     * @param desttype 目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex 目标系统index
     */
    private void inserBatchObjectRecord(String batchid, Long objindex, int objtype, int desttype, String destindex)
            throws Exception
    {
        BatchObjectRecord batchObjectRecord = new BatchObjectRecord();
        try
        {
            batchObjectRecord.setBatchid(batchid);
            batchObjectRecord.setObjindex(objindex);
            batchObjectRecord.setObjtype(objtype);
            batchObjectRecord.setDesttype(desttype);
            batchObjectRecord.setDestindex(Long.valueOf(destindex));

            batchObjectDS.insertBatchObjectRecord(batchObjectRecord);
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
    }

    /**
     * 
     * @param batchid 任务批次号
     * @param correlateid 任务流水号
     * @param programIndex 同步对象index
     * @param xmlAddress 内容相关对象的XML文件URL
     * @param objecttype 同步对象类型：若干object，若干map关系
     * @param taskType 同步类型：1 REGIST，2 UPDATE，3 DELETE
     * @param desttype 目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex 目标系统index
     * @param batchsingleflag 本批任务是否针对单个对象：0否，1是
     * @param tasksingleflag 当前任务是否针对单个对象：0否，1是
     */
    private void insertCntSyncTask(String batchid, String correlateid, Long programIndex, String xmlAddress,
            int objecttype, int taskType, int desttype, String destindex, int batchsingleflag, int tasksingleflag,
            int status) throws Exception
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        try
        {
            cntSyncTask.setStatus(status);
            cntSyncTask.setPriority(2);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
      
            cntSyncTask.setDestindex(Long.valueOf(destindex));
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }

    }

    private String writeXML(ProgramSchedualrecordMap psrMap, int syncType, String tempAreaPath) throws Exception
    {
        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = date.getTime() + "_program_schedualrecord_map.xml";

        // 创建XML文件
        String synctype = "";
        String tempFilePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + dir + File.separator
                + PROGRAM_DIR + File.separator;
        String mountAndTempfilePath = GlobalConstants.getMountPoint() + tempFilePath;
        String retPath = filterSlashStr(tempFilePath + fileName);
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        // 获取发布Action类型
        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }

        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");

        Element objectElement = null;
        Element propertyElement = null;
        String objectId = getObjectId(psrMap.getMapindex().toString(), psrMap.getMaptype());
        objectElement = mappingElement.addElement("Mapping");
        objectElement.addAttribute("ParentType", "Program");
        objectElement.addAttribute("ParentID", psrMap.getProgramid());
        objectElement.addAttribute("ObjectID", objectId);
        objectElement.addAttribute("ElementType", "ScheduleRecord");
        objectElement.addAttribute("ElementID", psrMap.getSchrecordid());
        objectElement.addAttribute("Action", synctype);

        writeElement(objectElement, propertyElement, "Type", "", false);
        writeElement(objectElement, propertyElement, "Sequence", "", false);
        writeElement(objectElement, propertyElement, "ValidStart", "", false);
        writeElement(objectElement, propertyElement, "ValidEnd", "", false);

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("exception:" + e);
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    log.error("IO exception:" + e);
                }
            }
        }

        return retPath;
    }

    /**
     * 根据mapping对象index生成ObjectId
     * 
     * @param index mapping对象index
     * @return ObjectId
     */
    private String getObjectId(String index, int type)
    {
        String objectId = "";
        objectId = String.format("%02d", type) + String.format("%030d", Integer.valueOf(index));
        return objectId;

    }

    /**
     * 写入节点属性
     * 
     * @param objectElement 父节点
     * @param propertyElement 子节点
     * @param key 节点名称
     * @param value 节点值
     */
    private void writeElement(Element objectElement, Element propertyElement, String key, Object value, Boolean convert)
    {
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", key);
        if (value == null)
        {
            propertyElement.addText("");
        }
        else
        {
            if (convert)
                propertyElement.addCDATA(String.valueOf(value));
            else
                propertyElement.addText(String.valueOf(value));
        }
    }

    /**
     * 根据要发布的系统类型或者对应的系统index
     * 
     * @param type 系统类型
     * @return 发布系统index
     */
    private String getTargetSystem(int type) throws Exception
    {
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(type);
        List<Targetsystem> listtsystem;
        String systemIndex = null;
        try
        {
            listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem != null && listtsystem.size() > 0)
            {
                targetSystem = listtsystem.get(0);
                systemIndex = targetSystem.getTargetindex().toString();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            log.error("ProgramSchedualrecordMapLS exception:" + e.getMessage());
        }
        return systemIndex;
    }

    // 获取时间 如"20111002"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    public void setProgramSchedualrecordMapDS(IProgramSchedualrecordMapDS programSchedualrecordMapDS)
    {
        this.programSchedualrecordMapDS = programSchedualrecordMapDS;
    }

    public void setCmsSchedulerecordDS(ICmsSchedulerecordDS cmsSchedulerecordDS)
    {
        this.cmsSchedulerecordDS = cmsSchedulerecordDS;
    }

    public void setCmsProgramDS(ICmsProgramDS cmsProgramDS)
    {
        this.cmsProgramDS = cmsProgramDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setBatchObjectDS(IBatchObjectRecordDS batchObjectDS)
    {
        this.batchObjectDS = batchObjectDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }
}

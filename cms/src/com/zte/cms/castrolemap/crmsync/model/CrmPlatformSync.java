package com.zte.cms.castrolemap.crmsync.model;

public class CrmPlatformSync
{
    private java.lang.Long syncindex;
    private java.lang.Integer objecttype;
    private java.lang.Long objectindex;
    private java.lang.String objectid;
    private java.lang.String elementid;
    private java.lang.String parentid;
    private java.lang.Integer platform;
    private java.lang.Integer status;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;
    private java.lang.Integer isfiledelete;
    private java.lang.Long reserve01;
    private java.lang.String reserve02;
    
    private java.lang.String onlinetimeStartDate;
    private java.lang.String onlinetimeEndDate;
    private java.lang.String cntofflinestarttime;
    private java.lang.String cntofflineendtime;
    private java.lang.String createStarttime;
    private java.lang.String createEndtime;
    
    private java.lang.Long castrolemapindex;
    private java.lang.String castrolemapid;
    private java.lang.String castid;
    private java.lang.String castrole;
    private java.lang.String createtime;
    
    private java.lang.String typePlatform;
    private java.lang.String statuses;
    private java.lang.String targetids;
    private java.lang.String castname;
    
    
    public java.lang.String getCastname()
    {
        return castname;
    }
    public void setCastname(java.lang.String castname)
    {
        this.castname = castname;
    }
    public java.lang.String getOnlinetimeStartDate()
    {
        return onlinetimeStartDate;
    }
    public void setOnlinetimeStartDate(java.lang.String onlinetimeStartDate)
    {
        this.onlinetimeStartDate = onlinetimeStartDate;
    }
    public java.lang.String getOnlinetimeEndDate()
    {
        return onlinetimeEndDate;
    }
    public void setOnlinetimeEndDate(java.lang.String onlinetimeEndDate)
    {
        this.onlinetimeEndDate = onlinetimeEndDate;
    }
    public java.lang.String getCntofflinestarttime()
    {
        return cntofflinestarttime;
    }
    public void setCntofflinestarttime(java.lang.String cntofflinestarttime)
    {
        this.cntofflinestarttime = cntofflinestarttime;
    }
    public java.lang.String getCntofflineendtime()
    {
        return cntofflineendtime;
    }
    public void setCntofflineendtime(java.lang.String cntofflineendtime)
    {
        this.cntofflineendtime = cntofflineendtime;
    }
    public java.lang.String getCreateStarttime()
    {
        return createStarttime;
    }
    public void setCreateStarttime(java.lang.String createStarttime)
    {
        this.createStarttime = createStarttime;
    }
    public java.lang.String getCreateEndtime()
    {
        return createEndtime;
    }
    public void setCreateEndtime(java.lang.String createEndtime)
    {
        this.createEndtime = createEndtime;
    }
    
    public java.lang.Long getSyncindex()
    {
        return syncindex;
    }
    public void setSyncindex(java.lang.Long syncindex)
    {
        this.syncindex = syncindex;
    }
    public java.lang.Integer getObjecttype()
    {
        return objecttype;
    }
    public void setObjecttype(java.lang.Integer objecttype)
    {
        this.objecttype = objecttype;
    }
    public java.lang.Long getObjectindex()
    {
        return objectindex;
    }
    public void setObjectindex(java.lang.Long objectindex)
    {
        this.objectindex = objectindex;
    }
    public java.lang.String getObjectid()
    {
        return objectid;
    }
    public void setObjectid(java.lang.String objectid)
    {
        this.objectid = objectid;
    }
    public java.lang.String getElementid()
    {
        return elementid;
    }
    public void setElementid(java.lang.String elementid)
    {
        this.elementid = elementid;
    }
    public java.lang.String getParentid()
    {
        return parentid;
    }
    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }
    public java.lang.Integer getPlatform()
    {
        return platform;
    }
    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }
    public java.lang.Integer getStatus()
    {
        return status;
    }
    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }
    public java.lang.String getPublishtime()
    {
        return publishtime;
    }
    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }
    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }
    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }
    public java.lang.Integer getIsfiledelete()
    {
        return isfiledelete;
    }
    public void setIsfiledelete(java.lang.Integer isfiledelete)
    {
        this.isfiledelete = isfiledelete;
    }
    public java.lang.Long getReserve01()
    {
        return reserve01;
    }
    public void setReserve01(java.lang.Long reserve01)
    {
        this.reserve01 = reserve01;
    }
    public java.lang.String getReserve02()
    {
        return reserve02;
    }
    public void setReserve02(java.lang.String reserve02)
    {
        this.reserve02 = reserve02;
    }
   
    public java.lang.Long getCastrolemapindex()
    {
        return castrolemapindex;
    }
    public void setCastrolemapindex(java.lang.Long castrolemapindex)
    {
        this.castrolemapindex = castrolemapindex;
    }
    public java.lang.String getCastrolemapid()
    {
        return castrolemapid;
    }
    public void setCastrolemapid(java.lang.String castrolemapid)
    {
        this.castrolemapid = castrolemapid;
    }
    public java.lang.String getCastrole()
    {
        return castrole;
    }
    public java.lang.String getCreatetime()
    {
        return createtime;
    }
    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }
    public java.lang.String getTypePlatform()
    {
        return typePlatform;
    }
    public void setTypePlatform(java.lang.String typePlatform)
    {
        this.typePlatform = typePlatform;
    }
    public java.lang.String getStatuses()
    {
        return statuses;
    }
    public void setStatuses(java.lang.String statuses)
    {
        this.statuses = statuses;
    }
    public java.lang.String getTargetids()
    {
        return targetids;
    }
    public void setTargetids(java.lang.String targetids)
    {
        this.targetids = targetids;
    }
    public java.lang.String getCastid()
    {
        return castid;
    }
    public void setCastid(java.lang.String castid)
    {
        this.castid = castid;
    }
    public void setCastrole(java.lang.String castrole)
    {
        this.castrole = castrole;
    }
}

package com.zte.cms.basicdata.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsProgramtype extends DynamicBaseObject
{
    private java.lang.Long typeindex;
    private java.lang.String typeid;
    private java.lang.Integer clslevel;
    private java.lang.String typename;
    private java.lang.String typedesc;
    private java.lang.String parentid;
    private String publishtime;
    private Long publishstatus;
    private Long childnum;
    private String cpid;
    private String cpcnshortname;
    private Long mapindex;

    public String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public String getCpid()
    {
        return cpid;
    }

    public void setCpid(String cpid)
    {
        this.cpid = cpid;
    }

    public Long getChildnum()
    {
        return childnum;
    }

    public void setChildnum(Long childnum)
    {
        this.childnum = childnum;
    }

    public String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(String publishtime)
    {
        this.publishtime = publishtime;
    }

    public Long getPublishstatus()
    {
        return publishstatus;
    }

    public void setPublishstatus(Long publishstatus)
    {
        this.publishstatus = publishstatus;
    }

    public java.lang.Long getTypeindex()
    {
        return typeindex;
    }

    public void setTypeindex(java.lang.Long dex)
    {
        this.typeindex = dex;
    }

    public java.lang.String getTypeid()
    {
        return typeid;
    }

    public void setTypeid(java.lang.String typeid)
    {
        this.typeid = typeid;
    }

    public java.lang.Integer getClslevel()
    {
        return clslevel;
    }

    public void setClslevel(java.lang.Integer clslevel)
    {
        this.clslevel = clslevel;
    }

    public java.lang.String getTypename()
    {
        return typename;
    }

    public void setTypename(java.lang.String typename)
    {
        this.typename = typename;
    }

    public java.lang.String getTypedesc()
    {
        return typedesc;
    }

    public void setTypedesc(java.lang.String typedesc)
    {
        this.typedesc = typedesc;
    }

    public java.lang.String getParentid()
    {
        return parentid;
    }

    public void setParentid(java.lang.String parentid)
    {
        this.parentid = parentid;
    }

    public void initRelation()
    {
        this.addRelation("typeindex", "TYPEINDEX");
        this.addRelation("typeid", "TYPEID");
        this.addRelation("clslevel", "CLSLEVEL");
        this.addRelation("typename", "TYPENAME");
        this.addRelation("typedesc", "TYPEDESC");
        this.addRelation("parentid", "PARENTID");
    }
}

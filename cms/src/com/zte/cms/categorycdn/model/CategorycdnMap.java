package com.zte.cms.categorycdn.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CategorycdnMap extends DynamicBaseObject
{
    private java.lang.String categoryid;
    private java.lang.String categoryname;
    private java.lang.String catagorycode;
    private java.lang.String contentid;
    private java.lang.String namecn;
    private java.lang.Integer contenttype;
    private java.lang.String cpid;
    private java.lang.String cpcontentid;
    private java.lang.String cpcnshortname;
    private java.lang.String createtime; 
    private java.lang.String mappingid;


    public void initRelation()
    {
        // TODO Auto-generated method stub
        this.addRelation("categoryid", "CATEGORYID");
        this.addRelation("categoryname", "CATEGORYNAME");
        this.addRelation("catagorycode", "CATAGORYCODE");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("contenttype", "CONTENTTYPE");
        this.addRelation("cpid", "CPID");
        this.addRelation("cpcnshortname", "CPCNSHORTNAME");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("mappingid", "MAPPINGID");

    }

    public java.lang.String getCategoryid()
    {
        return categoryid;
    }

    public void setCategoryid(java.lang.String categoryid)
    {
        this.categoryid = categoryid;
    }

    public java.lang.String getCategoryname()
    {
        return categoryname;
    }

    public void setCategoryname(java.lang.String categoryname)
    {
        this.categoryname = categoryname;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public Integer getContenttype()
    {
        return contenttype;
    }

    public void setContenttype(Integer contenttype)
    {
        this.contenttype = contenttype;
    }

    public java.lang.String getCatagorycode()
    {
        return catagorycode;
    }

    public void setCatagorycode(java.lang.String catagorycode)
    {
        this.catagorycode = catagorycode;
    }

    public java.lang.String getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.String cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public java.lang.String getMappingid()
    {
        return mappingid;
    }

    public void setMappingid(java.lang.String mappingid)
    {
        this.mappingid = mappingid;
    }

}

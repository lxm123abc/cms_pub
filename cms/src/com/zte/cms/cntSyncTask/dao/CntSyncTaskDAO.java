package com.zte.cms.cntSyncTask.dao;

import java.util.List;

import com.zte.cms.cntSyncTask.dao.ICntSyncTaskDAO;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CntSyncTaskDAO extends DynamicObjectBaseDao implements ICntSyncTaskDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCntSyncTask(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("insert cntSyncTask starting...");
        super.insert("insertCntSyncTask", cntSyncTask);
        log.debug("insert cntSyncTask end");
    }

    public void updateCntSyncTask(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("update cntSyncTask by pk starting...");
        super.update("updateCntSyncTask", cntSyncTask);
        log.debug("update cntSyncTask by pk end");
    }

    public void updateCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("update cntSyncTask by conditions starting...");
        super.update("updateCntSyncTaskByCond", cntSyncTask);
        log.debug("update cntSyncTask by conditions end");
    }

    public void deleteCntSyncTask(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("delete cntSyncTask by pk starting...");
        super.delete("deleteCntSyncHisTask", cntSyncTask);
        log.debug("delete cntSyncTask by pk end");
    }

    public void deleteCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("delete cntSyncTask by conditions starting...");
        super.delete("deleteCntSyncTaskByCond", cntSyncTask);
        log.debug("update cntSyncTask by conditions end");
    }

    public CntSyncTask getCntSyncTask(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("query cntSyncTask starting...");
        CntSyncTask resultObj = (CntSyncTask) super.queryForObject("getCntSyncTask", cntSyncTask);
        log.debug("query cntSyncTask end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CntSyncTask> getCntSyncTaskByCond(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("query cntSyncTask by condition starting...");
        List<CntSyncTask> rList = (List<CntSyncTask>) super.queryForList("queryCntSyncTaskListByCond", cntSyncTask);
        log.debug("query cntSyncTask by condition end");
        return rList;
    }

    public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("query cntSyncTask starting...");
        CntSyncTask resultObj = (CntSyncTask) super.queryForObject("getCntSynchorTaskListByCond", cntSyncTask);
        log.debug("query cntSyncTask end");
        return resultObj;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize) throws DAOException
    {
        log.debug("page query cntSyncTask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCntSyncTaskListCntByCond", cntSyncTask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CntSyncTask> rsList = (List<CntSyncTask>) super.pageQuery("queryCntSyncTaskListByCond", cntSyncTask,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cntSyncTask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCntSyncTaskListByCond", "queryCntSyncTaskListCntByCond", cntSyncTask, start,
                pageSize, puEntity);
    }
    
    public PageInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, PageUtilEntity puEntity)
    throws DAOException
    {
        return super.indexPageQuery("queryCntSyncHorTaskListByCond", "queryCntSyncHorTaskListCntByCond", cntSyncTask, start,
                pageSize, puEntity);	
    }
    public List<CntSyncTask> getCntSynhisListBybatchid(CntSyncTask cntSyncTask) throws DAOException
    {
        log.debug("query cntSyncTask starting...");
        @SuppressWarnings("unchecked")
        List<CntSyncTask> resultObj = (List<CntSyncTask>) super.queryForList("getCntSynhisListBybatchid", cntSyncTask);
        log.debug("query cntSyncTask end");
        return resultObj;
    }
}
package com.zte.cms.operCnttypeMap.service;

import java.util.List;
import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.cms.operCnttypeMap.dao.IOperCnttypemapDAO;
import com.zte.cms.operCnttypeMap.service.IOperCnttypemapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class OperCnttypemapDS implements IOperCnttypemapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IOperCnttypemapDAO dao = null;

    public void setDao(IOperCnttypemapDAO dao)
    {
        this.dao = dao;
    }

    public void insertOperCnttypemap(OperCnttypemap operCnttypemap) throws DomainServiceException
    {
        log.debug("insert operCnttypemap starting...");
        try
        {
            dao.insertOperCnttypemap(operCnttypemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert operCnttypemap end");
    }

    public void updateOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException
    {
        log.debug("update operCnttypemap by condition starting...");
        try
        {
            dao.updateOperCnttypemapByCond(operCnttypemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update operCnttypemap by condition end");
    }

    public void updateOperCnttypemapListByCond(List<OperCnttypemap> operCnttypemapList) throws DomainServiceException
    {
        log.debug("update operCnttypemapList by condition starting...");
        if (null == operCnttypemapList || operCnttypemapList.size() == 0)
        {
            log.debug("there is no datas in operCnttypemapList");
            return;
        }
        try
        {
            for (OperCnttypemap operCnttypemap : operCnttypemapList)
            {
                dao.updateOperCnttypemapByCond(operCnttypemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update operCnttypemapList by condition end");
    }

    public void removeOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException
    {
        log.debug("remove operCnttypemap by condition starting...");
        try
        {
            dao.deleteOperCnttypemapByCond(operCnttypemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove operCnttypemap by condition end");
    }

    public void removeOperCnttypemapListByCond(List<OperCnttypemap> operCnttypemapList) throws DomainServiceException
    {
        log.debug("remove operCnttypemapList by condition starting...");
        if (null == operCnttypemapList || operCnttypemapList.size() == 0)
        {
            log.debug("there is no datas in operCnttypemapList");
            return;
        }
        try
        {
            for (OperCnttypemap operCnttypemap : operCnttypemapList)
            {
                dao.deleteOperCnttypemapByCond(operCnttypemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove operCnttypemapList by condition end");
    }

    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DomainServiceException
    {
        log.debug("get operCnttypemap by condition starting...");
        List<OperCnttypemap> rsList = null;
        try
        {
            rsList = dao.getOperCnttypemapByCond(operCnttypemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get operCnttypemap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get operCnttypemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(operCnttypemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<OperCnttypemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get operCnttypemap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get operCnttypemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(operCnttypemap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<OperCnttypemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get operCnttypemap page info by condition end");
        return tableInfo;
    }
}

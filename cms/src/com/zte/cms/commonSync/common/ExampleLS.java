package com.zte.cms.commonSync.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleLS
{
    public String publishProgram(List<String> programList, int platform)throws Exception
    {
        Map<String, List> objMap = new HashMap();
        objMap.put("programList", programList);
        String wg18xml = StandardImpFactory.getInstance().create(platform).createXmlFile(objMap, 0, 3, 1);
        insertSyncTask();// 调用同步任务模块DS代码

        String bmsxml = StandardImpFactory.getInstance().create(platform).createXmlFile(objMap, 1, 3, 1);
        insertSyncTask();// 调用同步任务模块DS代码
        String epgxml = StandardImpFactory.getInstance().create(platform).createXmlFile(objMap, 2, 3, 1);
        insertSyncTask();// 调用同步任务模块DS代码
        String cdnxml = StandardImpFactory.getInstance().create(platform).createXmlFile(objMap, 3, 3, 1);
        insertSyncTask();// 调用同步任务模块DS代码
        return null;
    }

    private void insertSyncTask()
    {
        // .....
    }
}

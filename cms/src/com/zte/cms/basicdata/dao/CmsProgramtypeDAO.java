package com.zte.cms.basicdata.dao;

import java.sql.SQLException;
import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsProgramtypeDAO extends DynamicObjectBaseDao implements ICmsProgramtypeDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public Integer insertCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("insert cmsProgramtype starting...");
        int i = 0;
        try
        {
            super.insert("programtypeInsert", cmsProgramtype);
            i = 1;
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            i = 0;
        }

        log.debug("insert cmsProgramtype end");
        return i;
    }

    public void insertCmsProgramtypeList(List<CmsProgramtype> cmsProgramtypeList) throws DAOException
    {
        log.debug("insert cmsProgramtypeList starting...");
        if (null != cmsProgramtypeList)
        {
            super.insertBatch("insertCmsProgramtype", cmsProgramtypeList);
        }
        log.debug("insert cmsProgramtypeList end");
    }

    public Integer updateCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("update cmsProgramtype by pk starting...");
        int i = 0;
        try
        {
            super.update("updateCmsProgramtype", cmsProgramtype);
            i = 1;
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            i = 0;
        }
        log.debug("update cmsProgramtype by pk end");
        return i;
    }

    public void updateCmsProgramtypeList(List<CmsProgramtype> cmsProgramtypeList) throws DAOException
    {
        log.debug("update cmsProgramtypeList by pk starting...");
        super.updateBatch("updateCmsProgramtype", cmsProgramtypeList);
        log.debug("update cmsProgramtypeList by pk end");
    }

    public void updateCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("update cmsProgramtype by conditions starting...");
        super.update("updateCmsProgramtypeByCond", cmsProgramtype);
        log.debug("update cmsProgramtype by conditions end");
    }

    public void updateCmsProgramtypeListByCond(List<CmsProgramtype> cmsProgramtypeList) throws DAOException
    {
        log.debug("update cmsProgramtypeList by conditions starting...");
        super.updateBatch("updateCmsProgramtypeByCond", cmsProgramtypeList);
        log.debug("update cmsProgramtypeList by conditions end");
    }

    public Integer deleteCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("delete cmsProgramtype by pk starting...");
        int i = 0;
        try
        {
            super.delete("deleteCmsProgramtype", cmsProgramtype);
            i = 1;
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            i = 0;
        }
        log.debug("delete cmsProgramtype by pk end");
        return i;
    }

    public void deleteCmsProgramtypeList(List<CmsProgramtype> cmsProgramtypeList) throws DAOException
    {
        log.debug("delete cmsProgramtypeList by pk starting...");
        super.deleteBatch("deleteProgramtype", cmsProgramtypeList);
        log.debug("delete cmsProgramtypeList by pk end");
    }

    public void deleteCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("delete cmsProgramtype by conditions starting...");
        super.delete("deleteCmsProgramtypeByCond", cmsProgramtype);
        log.debug("delete cmsProgramtype by conditions end");
    }

    public void deleteCmsProgramtypeListByCond(List<CmsProgramtype> cmsProgramtypeList) throws DAOException
    {
        log.debug("delete cmsProgramtypeList by conditions starting...");
        super.deleteBatch("deleteCmsProgramtypeByCond", cmsProgramtypeList);
        log.debug("delete cmsProgramtypeList by conditions end");
    }

    public CmsProgramtype getCmsProgramtype(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("query cmsProgramtype starting...");
        CmsProgramtype resultObj = (CmsProgramtype) super.queryForObject("getCmsProgramtype", cmsProgramtype);
        log.debug("query cmsProgramtype end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype) throws DAOException
    {
        log.debug("query cmsProgramtype by condition starting...");
        List<CmsProgramtype> rList = (List<CmsProgramtype>) super.queryForList("queryCmsProgramtypeListByCond",
                cmsProgramtype);
        log.debug("query cmsProgramtype by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype, PageUtilEntity puEntity)
            throws DAOException
    {
        log.debug("query cmsProgramtype by condition starting...");
        List<CmsProgramtype> rList = (List<CmsProgramtype>) super.queryForList("queryCmsProgramtypeListByCond",
                cmsProgramtype, puEntity);
        log.debug("query cmsProgramtype by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramtype cmsProgramtype, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramtype by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypeListCntByCond", cmsProgramtype)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> rsList = (List<CmsProgramtype>) super.pageQuery("getProgramtypeByCond",
                    cmsProgramtype, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    public PageInfo queryParentProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramtype by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCountById", cmsProgramtype)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> rsList = (List<CmsProgramtype>) super.pageQuery("queryParentProgramtype",
                    cmsProgramtype, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    public PageInfo queryCmsProgramtypeById(CmsProgramtype cmsProgramtype, int start, int pageSize) throws DAOException
    {
        log.debug("queryCmsProgramtypeById by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = 1;
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> rsList = (List<CmsProgramtype>) super.pageQuery("queryCmsProgramtypeById",
                    cmsProgramtype, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramtype cmsProgramtype, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        log.debug("page query cmsProgramtype by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypeListCntByCond", cmsProgramtype)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> rsList = (List<CmsProgramtype>) super.pageQuery("selectByTypeindex", cmsProgramtype,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    public CmsProgramtype getCmsProgramtypeById(Long id)
    {
        return (CmsProgramtype) super.queryForObject("getCmsProgramtypeById", id);
    }

    public List<CmsProgramtype> queryForMenuTree(CmsProgramtype cmsProgramtype)
    {
        return (List<CmsProgramtype>) super.queryForList("queryForTree", cmsProgramtype);
    }

    public Integer deleteProgramtype(Long id) throws DAOException
    {
        int i = 0;
        log.debug("delete cmsProgramtype by conditions starting...");
        try
        {
            super.delete("deleteProgramtype", id);
            i = 1;
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
            i = 0;
        }
        return i;
    }

    public List<CmsProgramtype> queryChildCmsProgramtype(CmsProgramtype programtype)
    {
        List list = (List) super.queryForList("queryChildCmsProgramtype", programtype);
        return list;
    }

    public PageInfo getProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize)
    {
        log.debug("queryCmsProgramtypeById by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCountById", cmsProgramtype)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> list = (List<CmsProgramtype>) super.queryForList("queryChildCmsProgramtype",
                    cmsProgramtype.getTypeindex());
            pageInfo = new PageInfo(start, totalCnt, fetchSize, list);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    public PageInfo selectByTypeindex(Long typeindex, int start, int pageSize)
    {
        log.debug("queryCmsProgramtypeById by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypeListCntByCond", typeindex)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtype> list = (List<CmsProgramtype>) super.queryForList("selectByTypeindex", typeindex);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, list);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtype by condition end");
        return pageInfo;
    }

    // 判断是否存在于typeid相同的数据
    public Integer queryByTypeid(CmsProgramtype cmsProgramtype)
    {
        log.debug("query by condition starting...");
        List<CmsProgramtype> list = (List<CmsProgramtype>) super.queryForList("queryByTypeid", cmsProgramtype);
        log.debug("query by condition end...");
        return list.size();
    }

    // 判断在同一父节点下是否存在与typename相同的节点
    public Integer queryByTypename(CmsProgramtype cmsProgramtype)
    {
        log.debug("query by condition starting...");
        List<CmsProgramtype> list = (List<CmsProgramtype>) super.queryForList("queryByTypename", cmsProgramtype);
        log.debug("query by condition end...");
        return list.size();
    }

    // 判断在同一节点下的同名产品分类信息是否拥有相同的typeindex
    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype)
    {
        return (Long) super.queryForObject("queryTypeindexByTypename", cmsProgramtype);
    }

    public CmsProgramtype queryByParentid(String typeid)
    {
        return (CmsProgramtype) super.queryForObject("queryByParentid", typeid);
    }

    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype)
    {
        return (List<CmsProgramtype>) super.queryForList("listCmsprogramtypeByParentid", cmsProgramtype);
    }

    /**
     * 判断是否有重复的typeid
     */
    public CmsProgramtype getCmsProgramtype(String typeid)
    {
        return (CmsProgramtype) super.queryForObject("getCmsProgramtypeByTypeid", typeid);
    }

    /**
     * 判断节目分类信息下是不是有子信息被使用，如果存在就不能取消发布
     * 
     * @param typeid
     * @return
     */
    public Integer isHasChild(String typeid)
    {
        return (Integer) super.queryForObject("isHasChild", typeid);
    }

    /**
     * 展示树
     */
    public List<CmsProgramtype> listMenuTreeByParentid(String typeid)
    {
        List<CmsProgramtype> list = null;
        try
        {
            list = super.getSqlMapClient().queryForList("listMenuTreeByParentid", typeid);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public CmsProgramtype getCmsProgramtype(Long typeindex)
    {
        return (CmsProgramtype) super.queryForObject("getCmsprogramtype", typeindex);
    }

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex)
    {
        return (CmsProgramtype) super.queryForObject("getCmsprogramtypeByTypeindex", typeindex);
    }

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype)
    {
        return (CmsProgramtype) super.queryForObject("getProgramtypeByCond", cmsProgramtype);
    }
}
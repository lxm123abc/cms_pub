package com.zte.cms.channel.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ScheduleWkfwhis extends DynamicBaseObject
{
    private java.lang.Long historyindex;
    private java.lang.String historydate;
    private java.lang.Long workflowindex;
    private java.lang.Long taskindex;
    private java.lang.Long nodeid;
    private java.lang.String nodename;
    private java.lang.String workflowstarttime;
    private java.lang.String workflowendtime;
    private java.lang.String opopinion;
    private java.lang.String opername;
    private java.lang.Long templateindex;
    private java.lang.String handleevent;
    private java.lang.Long scheduleindex;
    private java.lang.String scheduleid;
    private java.lang.String programname;
    private java.lang.String lastupdatetime;
    
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.Integer workflow;
    private java.lang.Integer workflowlife;
    private java.lang.String effecttime;
    private java.lang.String createtime;
 
    private java.lang.Long cpindex;
    private java.lang.String startdate;
    private java.lang.String starttime;
    private java.lang.String duration;
    private java.lang.Integer isvalid;
    
    private java.lang.Integer platform;
    private java.lang.String servicekey;
    private java.lang.String channelid;
    private java.lang.Long reservecode1;
    
    
    
    public java.lang.Long getReservecode1()
    {
        return reservecode1;
    }

    public void setReservecode1(java.lang.Long reservecode1)
    {
        this.reservecode1 = reservecode1;
    }
    public java.lang.String getLastupdatetime() {
		return lastupdatetime;
	}

	public void setLastupdatetime(java.lang.String lastupdatetime) {
		this.lastupdatetime = lastupdatetime;
	}

	public java.lang.String getChannelid() {
		return channelid;
	}

	public void setChannelid(java.lang.String channelid) {
		this.channelid = channelid;
	}

	public java.lang.String getStartdate() {
		return startdate;
	}

	public void setStartdate(java.lang.String startdate) {
		this.startdate = startdate;
	}

	

	public java.lang.String getStarttime() {
		return starttime;
	}

	public void setStarttime(java.lang.String starttime) {
		this.starttime = starttime;
	}

	public java.lang.String getDuration() {
		return duration;
	}

	public void setDuration(java.lang.String duration) {
		this.duration = duration;
	}

	public java.lang.Integer getIsvalid() {
		return isvalid;
	}

	public void setIsvalid(java.lang.Integer isvalid) {
		this.isvalid = isvalid;
	}

	public java.lang.Integer getPlatform() {
		return platform;
	}

	public void setPlatform(java.lang.Integer platform) {
		this.platform = platform;
	}

	public java.lang.String getServicekey() {
		return servicekey;
	}

	public void setServicekey(java.lang.String servicekey) {
		this.servicekey = servicekey;
	}

	public java.lang.Long getHistoryindex()
    {
        return historyindex;
    }

    public void setHistoryindex(java.lang.Long historyindex)
    {
        this.historyindex = historyindex;
    }

    public java.lang.String getHistorydate()
    {
        return historydate;
    }

    public void setHistorydate(java.lang.String historydate)
    {
        this.historydate = historydate;
    }

    public java.lang.Long getWorkflowindex()
    {
        return workflowindex;
    }

    public void setWorkflowindex(java.lang.Long workflowindex)
    {
        this.workflowindex = workflowindex;
    }

    public java.lang.Long getTaskindex()
    {
        return taskindex;
    }

    public void setTaskindex(java.lang.Long taskindex)
    {
        this.taskindex = taskindex;
    }

    public java.lang.Long getNodeid()
    {
        return nodeid;
    }

    public void setNodeid(java.lang.Long nodeid)
    {
        this.nodeid = nodeid;
    }

    public java.lang.String getNodename()
    {
        return nodename;
    }

    public void setNodename(java.lang.String nodename)
    {
        this.nodename = nodename;
    }

    public java.lang.String getWorkflowstarttime()
    {
        return workflowstarttime;
    }

    public void setWorkflowstarttime(java.lang.String workflowstarttime)
    {
        this.workflowstarttime = workflowstarttime;
    }

    public java.lang.String getWorkflowendtime()
    {
        return workflowendtime;
    }

    public void setWorkflowendtime(java.lang.String workflowendtime)
    {
        this.workflowendtime = workflowendtime;
    }

    public java.lang.String getOpopinion()
    {
        return opopinion;
    }

    public void setOpopinion(java.lang.String opopinion)
    {
        this.opopinion = opopinion;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public java.lang.Long getTemplateindex()
    {
        return templateindex;
    }

    public void setTemplateindex(java.lang.Long templateindex)
    {
        this.templateindex = templateindex;
    }

    public java.lang.String getHandleevent()
    {
        return handleevent;
    }

    public void setHandleevent(java.lang.String handleevent)
    {
        this.handleevent = handleevent;
    }

    

    public java.lang.Long getScheduleindex()
    {
        return scheduleindex;
    }

    public void setScheduleindex(java.lang.Long scheduleindex)
    {
        this.scheduleindex = scheduleindex;
    }

    public java.lang.String getScheduleid()
    {
        return scheduleid;
    }

    public void setScheduleid(java.lang.String scheduleid)
    {
        this.scheduleid = scheduleid;
    }

   

    public java.lang.String getProgramname() {
		return programname;
	}

	public void setProgramname(java.lang.String programname) {
		this.programname = programname;
	}

	

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getWorkflow()
    {
        return workflow;
    }

    public void setWorkflow(java.lang.Integer workflow)
    {
        this.workflow = workflow;
    }

    public java.lang.Integer getWorkflowlife()
    {
        return workflowlife;
    }

    public void setWorkflowlife(java.lang.Integer workflowlife)
    {
        this.workflowlife = workflowlife;
    }

    public java.lang.String getEffecttime()
    {
        return effecttime;
    }

    public void setEffecttime(java.lang.String effecttime)
    {
        this.effecttime = effecttime;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.Long getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Long cpindex)
    {
        this.cpindex = cpindex;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub

    }

}

package com.zte.cms.service.apply;

import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonValid;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class ServiceApplyValid extends CommonValid
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected IServiceDS serviceds = null;

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    @Override
    public AppLogInfoEntity getLogInfo(Long index)
    {
        AppLogInfoEntity loginfo = new AppLogInfoEntity();
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        Service service = new Service();
        service.setServiceindex(index);
        try
        {
            service = serviceds.getService(service);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        String optObjecttype = ServiceConstants.getWorkflowTypeName(ServiceConstants.WKFWTYPECODE_SERVICE_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(String.valueOf(index));

        String info = ResourceMgt.findDefaultText("ucdn.service.log.wkfw.cntdistribute.add.valid");
        if (null != info)
        {
            info = info.replace("[servicename]", service.getServicename());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void operate(Long index)
    {
        // 修改状态
        try
        {
            Service service = new Service();
            service.setServiceindex(index);
            service = serviceds.getService(service);

            service.setWorkflow(0); // 未启动
            service.setStatus(0); // 审核通过后，状态修改成待发布
            serviceds.updateService(service);

        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }
}

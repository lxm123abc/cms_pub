package com.zte.cms.cast.castsync.dao;

import com.zte.cms.cast.castsync.model.CastTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICastTargetSyncDAO
{
    /**
     * 根据条件分页查询CastTargetSync对象 
     *
     * @param castTargetSync CastTargetSync对象，作为查询条件的参数 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return  查询结果
     * @throws DomainServiceException ds异常
     */
    public PageInfo pageInfoQuery(CastTargetSync castTargetSync, int start, int pageSize)throws DAOException;

}

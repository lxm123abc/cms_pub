    var syscodeValue = {};
	var returnTarget;
	
	function replacespecial(elementobj,exp){
       var keycode=event.keyCode;                 
       var keychar=String.fromCharCode(keycode);
    //alert('ASCII='+keycode+'\nKeyChar='+keychar+'\regular expression ='+exp);
       if(keycode!=37&&keycode!=39)
       elementobj.value=elementobj.value.replace(exp,'');
    }
    
	function doEncrypttag(){
		if (document.getElementsByName('encrypttag')[0].checked){
		    $("#dynencrypttag").val("0");
			encrypt.style.display = "none";
		}else{
			encrypt.style.display = "";
			$("#dynencrypttag").val("1");
		}
	}

	function doSnapexist(){
		if (document.getElementsByName('snapexist')[0].checked){
			snapexistdiv.style.display = "none";
			$("#snapbegintime").val("");
			$("#snapendtime").val("");
			$("#issnapexist").val("0");
			_hide();
		}else{
			snapexistdiv.style.display = "";
			$("#issnapexist").val("1");
		} 
	}

	function doSendtime(){
		if (document.getElementsByName('sendtime')[0].checked){
			//document.getElementsByName('uploadtime')[0].style.display = "none";
			document.getElementById('s_uptime').style.display = "none";

		}else{
			//document.getElementsByName('uploadtime')[0].style.display = "";
			document.getElementById('s_uptime').style.display = "";
			document.getElementsByName('uploadtime')[0].value = curDateTime();
		}
	}

	function doSendType(){
		if (document.getElementsByName('sendType')[0].checked){
			ftpdiv.style.display = "";
			uploaddiv.style.display = "none";
			$("#addfilepath").val("");
			$("#addfilename").val("");
			$("#uploadtype").val("1");
			//document.getElementById('').value='1';
		}else{
			ftpdiv.style.display = "none";
			uploaddiv.style.display = "";
			$("#ip").val("");
			$("#port").val("");
			$("#useraccount").val("");
			$("#password").val("");
			$("#filepath").val("");
			$("#filename").val("");
			$("#uploadtype").val("3");
			//document.getElementById('uploadtype').value='3';
		} 
	}

	function doEditFile(){
		if (document.getElementsByName('isEditFile')[0].checked){
			uptypetr.style.display = "none";
			ftpdiv.style.display = "none";
			uploaddiv.style.display = "none";
		}else{
			uptypetr.style.display = "";
			doSendType();
		}
	}

	function showErrorText(id, text){
		$("label[class='validFont']").each(hidevalidFont);
		var obj = document.getElementById(id + "_msg");
		if (obj)
		{
			if (text != "")
			{
				obj.innerText = text;
			}else{
				obj.innerText = $res_entry("msg.cpspmanager.check.1","");
			}
		}
	}

	function hidevalidFont(i){
		if (i >= 0)
		{
			this.innerText = "";
		}
	}

	// 校验内容基本信息：内容名称，内容描述，计费价格，分成比例
	function checkBasicInfo(){
		if (document.getElementById("namecn"))
		{
			var namecn = document.getElementById("namecn").value;
			if (utf8_strlen2(namecn) > 128)
			{
				showErrorText("namecn",$res_entry("label.page.content.name.extralong","")); return false;
			}
		}
		if (document.getElementById("firstletter"))
		{
			var namecn = document.getElementById("firstletter").value;
			if (utf8_strlen2(namecn) > 200)
			{
				showErrorText("firstletter",$res_entry("label.page.content.firstletter.extralong","")); return false;
			}
		}
		if (document.getElementById("cntDesc"))
		{
			var cntDesc = document.getElementById("cntDesc").value;
			if (utf8_strlen2(cntDesc) > 1024)
			{
				showErrorText("cntDesc",$res_entry("label.page.content.desc.extralong","")); return false; 
			}
			if (utf8_strlen2(cntDesc) > 1024)
			{
				showErrorText("cntDesc",$res_entry("label.page.content.desc.extralong","")); return false; 
			}
		}
		return true;
	}

	// 校验加密信息
	function checkEncryptInfo(){
		if (document.getElementsByName('encrypttag'))
		{
			var encrypttag = document.getElementsByName('encrypttag')[0].checked;
			if(!encrypttag){
				var encratio = document.getElementById("encratio").value;
				if (encratio == "")
				{
					showErrorText("encratio","");
					return false;
				}
				if (encratio < 0 || encratio > 100)
				{
					showErrorText("encratio",$res_entry("label.page.content.encratio.illegal",""));  
					return false;
				}
			}
			return true;
		}
	}

	// 校验片花信息
	function checkSnapInfo(){
		if (document.getElementsByName('snapexist'))
		{
			var snapexist = document.getElementsByName('snapexist')[0].checked;
			if (!snapexist)
			{
				var snapbegintime = $("#snapbegintime").val();
				var snapendtime = $("#snapendtime").val();
				if (snapbegintime == "")
				{
					showErrorText("snapbegintime","");
					return false;
				}
				if (snapendtime == "")
				{
					showErrorText("snapendtime","");
					return false;
				}
				var t1 = snapbegintime.replace(/\D/g,"");
				var t2 = snapendtime.replace(/\D/g,"");
				if (snapendtime != "" && snapbegintime != "")
				{
					if (t1 >= t2)
					{
						showErrorText("snapendtime",$res_entry("label.page.content.snapendtime.starttimeex","")); 
						return false;
					}
				}
			}else{
			$("#snapbegintime").val("");
			$("#snapendtime").val("");
			}
			return true;
		}
	}

	// 校验ftp信息
	function checkFTPInfo(){
		var sendType = document.getElementsByName('sendType')[0].checked;
		if (!sendType)
		{
			var addfilepath = document.getElementById("addfilepath").value;
			if (addfilepath == "")
			{
				showErrorText("addfilepath","");
				return false;
			}
			if (document.getElementById("addfilename"))
			{
				var addfilename = document.getElementById("addfilename").value;
				if (addfilename == "")
				{
					showErrorText("addfilename","");
					return false;
				}
				if (addfilename.indexOf('.') ==-1)
				{
				    showErrorText("addfilename",$res_entry("label.iptv.content.filesuffix",""));
					return false;
				}
				if (utf8_strlen2(addfilename) > 60)
				{
					showErrorText("addfilename",$res_entry("label.page.content.addfilename.extralong","")); return false;  
				}
			}
			return true;
		}else{
			var ip = $("#ip").val();
			var port = $("#port").val();
			var useraccount = $("#useraccount").val();
			var password = $("#password").val();
			var filepath = $("#filepath").val();
			if (ip == "") {
				showErrorText("ip",""); return false;
			}
			if (!isIp(ip))
			{
				showErrorText("ip",$res_entry("label.page.content.format.error","")); return false;  
			}
			if (port == "") {
				showErrorText("port",""); return false;
			}
			if (port <0 || port > 65535)
			{
				showErrorText("port",$res_entry("label.page.content.port.notinrange","")); return false;  
			}
			if (useraccount == "") {
				showErrorText("useraccount",""); return false;
			}
			if (password == "") {
				showErrorText("password",""); return false;
			}
			if (filepath == "") {
				showErrorText("filepath",""); return false;
			}
			if (document.getElementById("filename"))
			{
				var filename = $("#filename").val();
				if (filename == "") {
					showErrorText("filename",""); return false;
				}
				if (filename.indexOf('.') ==-1)
				{
				    showErrorText("filename",$res_entry("label.iptv.content.filesuffix",""));
					return false;
				}
				if (utf8_strlen2(filename) > 60)
				{
					showErrorText("",$res_entry("label.page.content.filename.extralong","")); return false; 
				}
			}
			return true;
		}
	}

	// 校验生效时间和失效filename时间
	function checkStartEndTime(){
		if (document.getElementById("starttime") && document.getElementById("endtime"))
		{
			var starttime = document.getElementById("starttime").value;
			var endtime = document.getElementById("endtime").value;
			var t1 = starttime.replace(/\D/g,"");
			var t2 = endtime.replace(/\D/g,"");
			if (starttime.length > 0){
				var cur = curDateTime().replace(/\D/g,"");
				/*if (cur >= t1)
				{
					showErrorText("starttime",$res_entry("label.page.content.starttime.lessthancurrent","")); 
					return false;
				}*/
			}else{
			showErrorText("starttime",$res_entry("msg.iptv.product.shouldnot.be.null","")); 
			return false;
			}
			if (endtime.length > 0){
				var cur = curDateTime().replace(/\D/g,"");
				if (cur >= t2)
				{
					showErrorText("endtime",$res_entry("label.page.content.endtime.lessthancurrent",""));  
					return false;
				}
			}else{
			showErrorText("endtime",$res_entry("msg.iptv.product.shouldnot.be.null","")); 
			return false;
			}
			if (starttime.length > 0 && endtime.length > 0)
			{
				if (t1 >= t2)
				{
					showErrorText("starttime",$res_entry("label.page.content.starttime.extracurrent",""));
					return false;
				}
			}			
		}
		return true;
	}

	// 弹出上载目录选择页面
	function popdir()
	{
		var cpindex = document.getElementById("cpindex").value;
		var name = "../cntcpdirectory/popconSystem.html?cpindex=" + cpindex;
		var feature = "dialogWidth:300px,dialogHeight:1000px;resizable=no;location=no;status:no";
		var obj = window.showModalDialog(name, "", feature);
		if(typeof(obj) != "undefined"){
		$("#dirindex").val(obj.dirindex);
		$("#dirname").val(obj.dirname); }  
	}

	// 弹出内容提供商选择页面
	function popcpsp(){
		var page = "../../umap/cpsp/popCpsp.html";
		var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
		var rtnobj = window.showModalDialog(page, "", sFeatures);
		if(typeof(rtnobj) != "undefined"){
		$("#cpcnshortname").val(rtnobj.cpcnshortname);
		$("#cpindex").val(rtnobj.cpindex);
		$("#cpid").val(rtnobj.cpid);
		$("#dirname").val("");
		$("#dirindex").val("");}
	}

	// 弹出人物选择页面
	function doSelectActor(page, index, name){
		var sFeatures = "dialogHeight: " + 600 + "px,dialogWidth: " + 800 + "px,scrollbars=yes";
		var array = new Array();
		//array = window.showModalDialog(page, "", sFeatures);
		page = page + "?index=" + index + "&name=" + name;
		returnTarget = window.open(page, "", sFeatures);
	}
		
	function show(array, index, name)
	{
        returnTarget.close();
		var rtnname = "";
		var rtnindex = "";
		for (i = 0; i < array.length; i++)
		{
			rtnname += array[i].actorname + ",";
			rtnindex += array[i].actorindex + ",";
		}
		var _id = "#" + name;
		$(_id).val(rtnname);
		_id = "#" + index;
		$(_id).val(rtnindex);
	}

    function getFeetype() {
        var value = getRiaCtrlValue('feetype');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_content_feetype", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
    }

    function getCountry(){
        var value = getRiaCtrlValue('country');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_actorcountrytype", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
    }
    
    function getLimitlevel() {
        var value = getRiaCtrlValue('contentlevel');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["uptl_iptv_content_contentlevel", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
    }

    function getAlgorithm(){
        var value = getRiaCtrlValue('algorithm');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_content_algorithm", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
    }

    function getSchema(){
        var value = getRiaCtrlValue('schema');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_content_schema", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
    }

	function getSonglang(){
        var value = getRiaCtrlValue('singlanguage');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_cnt_singerlang", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
	}

	function getSongtype(){
        var value = getRiaCtrlValue('songtype');
        syscodeValue = {};
        callUrl('sysCode/getSysCodeValueByPK.ssm', '["ucm_cnt_kalaoktype", ' + value + ']', 'syscodeValue', true);
        return syscodeValue.rtnValue;
	}

	function formatTime1(){
        var time = getRiaCtrlValue('effectivedate');
        return getTime(time);
    }

    function formatTime2(){
        var time = getRiaCtrlValue('expirydate');
        return getTime(time);
    }

    function formatSnapTime1(){
        var time = getRiaCtrlValue('snapbegintime');
        return getTime(time);
    }

    function formatSnapTime2(){
        var time = getRiaCtrlValue('snapendtime');
        return getTime(time);
    }


	function getTime(str)
	{
		if (str.length > 6)
		{
			return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2);
		}else{
			return str.substr(0,2) + ":" + str.substr(2,2) + ":" + str.substr(4,2);
		}
	}

	function getPwd(){
		var pwd = getRiaCtrlValue('password');
		return pwd.replace(/\w/g,"*");
	}

	function isIp(str){
		//var re = /^([1-2]{1}[0-9]{1}[0-9]{1}|[1-9]{1}|[1-9]{1}[0-9]{1})\.([1-2]{1}[0-9]{1}[0-9]{1}|[1-9]{1}|[1-9]{1}[0-9]{1})\.([1-2]{1}[0-9]{1}[0-9]{1}|[1-9]{1}|[1-9]{1}[0-9]{1})\.([1-2]{1}[0-9]{1}[0-9]{1}|[1-9]{1}|[1-9]{1}[0-9]{1})$/;
		var re = /^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/;
        return re.test(str);
	}

	function getFtpInfo(str){
        str = str.replace(/\/\//g,"/");
        var user_pwd = str.split("@")[0].replace("ftp:/","")
        var port_path_name = str.split("@")[1].split(":")[1];
        var _path = port_path_name.substring(port_path_name.indexOf('/'),port_path_name.lastIndexOf('/'));
		var rtnObj = new Object();
        rtnObj.ip = str.split("@")[1].split(":")[0];
        rtnObj.port = str.split("@")[1].split(":")[1].split("/")[0];
        rtnObj.user = user_pwd.split(":")[0];
        rtnObj.pwd = user_pwd.split(":")[1];
        rtnObj.filepath = _path;
        rtnObj.filename = port_path_name.substr(port_path_name.lastIndexOf('/') + 1);
        //alert(rtnObj.ip + "\n" + rtnObj.port + "\n" + rtnObj.user + "\n" + rtnObj.pwd + "\n" + rtnObj.filename + "\n" + rtnObj.filepath);
		return rtnObj;
	}

    function getDynencryptTag(){
        var dynencrypttag = getRiaCtrlValue('dynencrypttag');
		if (dynencrypttag > 0)
		{
			return $res_entry("label.page.content.true","");
		}else{
			return $res_entry("label.page.content.false","");
		}
    }

    function getIsChkey(){
        var ischkey = getRiaCtrlValue('ischkey');
		if (ischkey > 0)
		{
			return $res_entry("label.page.content.true","");
		}else{
			return $res_entry("label.page.content.false","");
		}
    }

    function getIsEncau(){
        var isencau = getRiaCtrlValue('isencau');
		if (isencau > 0)
		{
			return $res_entry("label.page.content.true","");
		}else{
			return $res_entry("label.page.content.false","");
		}
    }

    function showResult(errorcode, des) {
        if (typeof(des) != "undefined") {
            errorcode = errorcode + ":" + $res_entry("label.page.content.syserror","");  
        }
        openMsg(errorcode, null, false);
        doSerach();
    }
    function setLabelValue(labelId,labelmsg)
    {
  	 document.getElementById(labelId).innerText=labelmsg;
    }

function curDateTime(){
    var curDate = new Date();
    var DTAsString = "";
    var y = 0;
    var m = 0;
    var d = 0;
    var h = 0;
    var mi = 0;
    var s = 0;

    y = curDate.getYear();
    m = curDate.getMonth() + 1;
    d = curDate.getDate();

    if(y < 100){
        DTAsString += (1900+y);
    }else{
        DTAsString = y;
    }
    if(m < 10){
        DTAsString += "-0" + m;
    }else{
        DTAsString += "-" + m;
    }
    if(d < 10){
        DTAsString += "-0" +d;
    }else{
        DTAsString += "-" + d;
    }

    DTAsString += " ";
    h = curDate.getHours();
    mi = curDate.getMinutes();
    s = curDate.getSeconds();
    if (h < 10) {
        DTAsString += "0" + h;
    }else{
        DTAsString += h;
    }
    if (mi < 10) {
        DTAsString += ":0" + mi;
    }else{
        DTAsString += ":" + mi;
    }
    if (s < 10) {
        DTAsString += ":0" + s;
    }else{
        DTAsString += ":" + s;
    }
    return DTAsString;
}



/**//***********************************
* 简单时间控件： version 1.0
* 作者：李禄燊 
* 时间：2007-10-31
* 
* 使用说明：
* 首先把本控件包含到页面 
* <script src="XXX/setTime.js" type="text/javascript"></script>
* 控件调用函数：_SetTime(field)
* 例如 <input name="time" type="text"   onclick="_SetTime(this)"/>
*
************************************/
var _str_ = "";
document.writeln("<div id=\"_contents\" style=\"padding:3px; background-color:#E3E3E3; font-size: 12px; border: 1px solid #666666;  position:absolute; left:?px; top:?px; width:?px; height:?px; z-index:1; visibility:hidden\">");
_str_ += $res_entry("label.iptv.content.hour","")+"<select name=\"_hour\" style=\"width:38px\">";
for (h = 0; h <= 9; h++) {
    _str_ += "<option value=\"0" + h + "\">0" + h + "</option>";
}
for (h = 10; h <= 23; h++) {
    _str_ += "<option value=\"" + h + "\">" + h + "</option>";
}
_str_ += "</select> "+$res_entry("label.iptv.content.min","")+"<select name=\"_minute\" style=\"width:38px\">";
for (m = 0; m <= 9; m++) {
    _str_ += "<option value=\"0" + m + "\">0" + m + "</option>";
}
for (m = 10; m <= 59; m++) {
    _str_ += "<option value=\"" + m + "\">" + m + "</option>";
}
_str_ += "</select> "+$res_entry("label.iptv.content.sec","")+"<select name=\"_second\" style=\"width:38px\">";
for (s = 0; s <= 9; s++) {
    _str_ += "<option value=\"0" + s + "\">0" + s + "</option>";
}
for (s = 10; s <= 59; s++) {
    _str_ += "<option value=\"" + s + "\">" + s + "</option>";
}
_str_ += "</select><input name=\"queding\" type=\"button\" onclick=\"_select()\" value=\""+$res_entry("button.ok#value","")+"\" style=\"font-size:12px\" />&nbsp;<input name=\"quxiao\" type=\"button\" onclick=\"_hide()\" value=\""+$res_entry("button.app.user.mgt.cancel#value","")+"\" style=\"font-size:12px\" /></div>";
document.writeln(_str_);
var _fieldname;
function _SetTime(tt) {
    _fieldname = tt;
    var ttop = tt.offsetTop;    //TT控件的定位点高
    var thei = tt.clientHeight;    //TT控件本身的高
    var tleft = tt.offsetLeft;    //TT控件的定位点宽
    while (tt = tt.offsetParent) {
        ttop += tt.offsetTop;
        tleft += tt.offsetLeft;
    }
    document.all._contents.style.top = ttop + thei + 4;
    document.all._contents.style.left = tleft;
    document.all._contents.style.visibility = "visible";
}
function _select() {
    _fieldname.value = document.all._hour.value + ":" + document.all._minute.value + ":" + document.all._second.value;
    document.all._contents.style.visibility = "hidden";
}
function _hide(){
    document.all._contents.style.visibility = "hidden";
}

    //得到字符串的长度（汉字按3个字符计）
    function strlength(str){
        var l = str.length;
        var n = l;
        for(var i = 0; i < l; i++){
    		    if(str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255){
    		    	n += 2;
    		    }
    	  }
        return n;
    }
    
   function isSafe(str,reg){
        return  !reg.test(str);   
   }

       function getLens(obj)
    {
	   return obj.replace(/[^\x00-\xff]/gi,'zch').length;
    }

     function chgWidth(obj) 
    {
	  var v=obj.options[obj.selectedIndex].text;
	  var width=6.5*getLens(v);
	  width = (width<130 ? 130 : width);
	  obj.style.width=width;
	  select();
    }

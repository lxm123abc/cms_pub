package com.zte.cms.content.batchUpload.service;

import java.util.List;
import com.zte.cms.content.batchUpload.model.IcmExcelimportFail;
import com.zte.cms.content.batchUpload.dao.IIcmExcelimportFailDAO;
import com.zte.cms.content.batchUpload.service.IIcmExcelimportFailDS;
import com.zte.cms.content.model.CmsSplittask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmExcelimportFailDS implements IIcmExcelimportFailDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IIcmExcelimportFailDAO dao = null;

    public void setDao(IIcmExcelimportFailDAO dao)
    {
        this.dao = dao;
    }

    public void insertIcmExcelimportFail(IcmExcelimportFail icmExcelimportFail) throws DomainServiceException
    {
        log.debug("insert icmExcelimportFail starting...");
        try
        {
            dao.insertIcmExcelimportFail(icmExcelimportFail);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert icmExcelimportFail end");
    }

    public void removeIcmExcelimportFail(IcmExcelimportFail icmExcelimportFail) throws DomainServiceException
    {
        log.debug("remove deleteIcmExcelimportFailByCondstarting...");
        try
        {
            dao.deleteIcmExcelimportFailByCond(icmExcelimportFail);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove deleteIcmExcelimportFailByCond end");
    }

    public List<IcmExcelimportFail> getIcmExcelimportFailByCond(IcmExcelimportFail icmExcelimportFail)
            throws DomainServiceException
    {
        log.debug("get icmExcelimportFail by condition starting...");
        List<IcmExcelimportFail> rsList = null;
        try
        {
            rsList = dao.getIcmExcelimportFailByCond(icmExcelimportFail);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get icmExcelimportFail by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get icmExcelimportFail page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmExcelimportFail, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmExcelimportFail>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmExcelimportFail page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get icmExcelimportFail page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(icmExcelimportFail, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<IcmExcelimportFail>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get icmExcelimportFail page info by condition end");
        return tableInfo;
    }
}

//获取控件的值
function getHtmlCtrlTrimValue(id) {
    return strTrim(document.getElementById(id).value);
}

//验证空值
function htmlElemValueIsEmpty(elemValue) {
    return elemValue == "" || elemValue == null
}

//去空格
function strTrim(str) {
    var num = str.length;
    var i = 0;
    for (i = 0; i < num; i++) {
        if (str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length;
    for (i = num - 1; i > -1; i--) {
        if (str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i + 1);
    return str;
}

//提示信息并获得焦点
function setMsgCtrlTextAndFocus(id, text) {
    var ctrlObj = document.getElementById(id);
    msgId = id + "_msg";
    var msgObj = document.getElementById(msgId);
    msgObj.innerText = text;
    ctrlObj.focus();
}

//提示信息
function setMsgCtrlText(id, text) {
    var ctrlObj = document.getElementById(id);
    msgId = id + "_msg";
    var msgObj = document.getElementById(msgId);
    msgObj.innerText = text;
}

//验证email格式
function ismailFormat(input, input_msg) {

    if (trim(input.value) != '' && input.value != null) {
        var exp = new RegExp("[a-z0-9-_]+@[a-z0-9-_]+[\.]{1}(com|net|bta|cn|org|edu|mil|tv|hk)", "gi");
        var reg = input.value.match(exp);
        if (reg == null) {
            document.getElementById(input_msg).innerText = $res_entry("msg.cpspmanager.check.10");
            document.getElementById(input.id).focus();
            return false;
        } else {
            document.getElementById(input_msg).innerText = "";
            return true;
        }
    }
}

//去空格
function trim(input) {
    return input.replace(/(^\s*)|(\s*$)/g, "");
}

//验证URL格式
function isURLFormat(input, input_msg) {
    if (trim(input.value) != '' && input.value != null) {
        var strRegex = /^(http|HTTP):\/\/[\w-]+\.[\w-]+[\/=\?%\-&_~`@[\]\?+!]*([^<>\"\"])*$/;
        var exp = new RegExp(strRegex);
        var inputvalue = input.value.toLowerCase().trim();
        var reg = inputvalue.match(exp);
        if (reg == null) {
            document.getElementById(input_msg).innerText = "URL格式不正确,输入格式为 http://ss.cn";
            document.getElementById(input.id).focus();
            return false;
        } else {
            document.getElementById(input_msg).innerText = "";
            return true;
        }
    }
}

//修改CP时获取联系人信息
function getcontacts() {

    var contacts = [];
    var contactname = document.getElementsByName("contactname");
    var mobile = document.getElementsByName("mobile");
    var email = document.getElementsByName("email");
    var contactindex = document.getElementsByName("contactindex");
    var cpindex = document.getElementById("cpindex").value;

    for (var i = 0; i < email.length; i++) {

        var contact = new Object();
        contact["contactname"] = contactname[i].value;
        contact["mobile"] = mobile[i].value;
        contact["email"] = email[i].value;
        if (cpindex != "" && cpindex != null) {
            contact["cpindex"] = cpindex;
        }
        if (contactindex[i] != null && contactindex != "") {
            contact["contactindex"] = contactindex[i].value;
        }
        contacts.push(contact);
    }
    return contacts;

}

//新增CP时获取联系人信息
function getcontact() {

    var contacts = [];
    var contactname = document.getElementsByName("contactname");
    var mobile = document.getElementsByName("mobile");
    var email = document.getElementsByName("email");

    for (var i = 0; i < email.length; i++) {

        var contact = new Object();
        contact["contactname"] = contactname[i].value;
        contact["mobile"] = mobile[i].value;
        contact["email"] = email[i].value;
        contact["servicekey"] = "CMS";
        contacts.push(contact);
    }
    return contacts;

}

//验证CPID是否选中
function validCPID() {

    if (document.getElementById("cpid").selectedIndex == 0) {
        setMsgCtrlTextAndFocus('cpid', '请选择');
        return false;
    } else {
        return true;
    }
}

//在做提交前清空所有的提示信息
function clearAllMsgLabel() {
    $("label[@id$='_msg']").html("");
}


/***
 特殊字符效验
 **/
function isSafe(str, reg) {
    return !reg.test(str);
}

//检验电话号码  数字、字母、-
function validMobile(str) {
    if (str.length > 20 || str.length < 7) {
        return false;
    }
    else {
        var regex = /^[0-9]*$/;
        return regex.test(str);
    }
}

function valid() {
    var error = true;
    clearAllMsgLabel();
    //验证合作方名称
    var cpcnshortname = getHtmlCtrlTrimValue("cpcnshortname");
    document.getElementById("cpcnshortname").value = cpcnshortname;
    if (htmlElemValueIsEmpty(cpcnshortname)) {
        setMsgCtrlTextAndFocus('cpcnshortname', '此项不能为空');
        return "fail";
    }

    if (!isSafe(cpcnshortname, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
        setMsgCtrlText("cpcnshortname", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
        return "fail";
    }


    error = IsMaxLength(document.getElementById('cpcnshortname'), 'cpcnshortname_msg', 40);
    if (error == false) {
        return "fail";
    }

    if (cpcreditlevel.selectedIndex == 0) {
        setMsgCtrlTextAndFocus('cpcreditlevel', '请选择');
        return "fail";
    }
    //验证是否选择业务类型
    var cpposition = getHtmlCtrlTrimValue("cpposition");
    if (htmlElemValueIsEmpty(cpposition)) {
        setMsgCtrlTextAndFocus('cpposition', '请选择');
        return "fail";
    }

    //验证公司地址
    var corpaddr = getHtmlCtrlTrimValue("corpaddr");
    document.getElementById("corpaddr").value = corpaddr;
//	if(htmlElemValueIsEmpty(corpaddr))
//	{
//		setMsgCtrlTextAndFocus('corpaddr','此项不能为空');
//		return "fail";
//	}
    if (corpaddr != null && corpaddr != "") {

        if (!isSafe(corpaddr, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
            setMsgCtrlText("corpaddr", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
            return "fail";
        }

        error = IsMaxLength(document.getElementById('corpaddr'), 'corpaddr_msg', 255);
        if (error == false) {
            return "fail";
        }
    }

    //验证公司网址
    var customersrvurl = getHtmlCtrlTrimValue("customersrvurl");
    document.getElementById("customersrvurl").value = customersrvurl;
//	if(htmlElemValueIsEmpty(customersrvurl))
//	{
//		setMsgCtrlTextAndFocus('customersrvurl','此项不能为空');
//		return "fail";
//	}

    if (customersrvurl != null && customersrvurl != "") {
        if (!isSafe(customersrvurl, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
            setMsgCtrlText("customersrvurl", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
            return "fail";
        }

        var input = document.getElementById("customersrvurl");
        if (!isURLFormat(input, "customersrvurl_msg")) //验证URL格式
        {
            return "fail";
        }

        error = IsMaxLength(document.getElementById('customersrvurl'), 'customersrvurl_msg', 255);
        if (error == false) {
            return "fail";
        }

    }
    //营业执照号
    var licensecode = trim(document.getElementById("licensecode").value);
    var patt_code = /^[a-zA-Z0-9]{6,}$/;
    if (licensecode != null && licensecode != "") {
        if (!patt_code.test(licensecode)) {
            document.getElementById("licensecode_msg").innerHTML = "此项格式不正确，由字母、数字组成，且长度至少为6位";
            return "fail";
        }
        error = IsMaxLength(document.getElementById('licensecode'), 'licensecode_msg', 40);
        if (error == false) {
            return "fail";
        }
    }

    //主页
    var homepageurl = trim(document.getElementById("homepageurl").value);
    if (homepageurl != null && homepageurl != "") {
        if (!isSafe(homepageurl, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
            setMsgCtrlText("homepageurl", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
            return "fail";
        }

        var input = document.getElementById("homepageurl");
        if (!isURLFormat(input, "homepageurl_msg")) //验证URL格式
        {
            return "fail";
        }
    }
    error = IsMaxLength(document.getElementById('homepageurl'), 'homepageurl_msg', 255);
    if (error == false) {
        return "fail";
    }

    //到期时间
    var cpenddate = document.getElementById("cpenddateTime").value;
    $("#cpenddate").val(cpenddate.replace(/\D/g, ""));


    //描述
    var description = document.getElementById("description").value;
    if (!isSafe(description, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
        setMsgCtrlText("description", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
        return "fail";
    }
    //验证联系人
    var contactname = getHtmlCtrlTrimValue("contactname");
    document.getElementById("contactname").value = contactname;
    if (htmlElemValueIsEmpty(contactname)) {
        setMsgCtrlTextAndFocus('contactname', '此项不能为空');
        return "fail";
    }
    if (!isSafe(contactname, new RegExp('[!\'\"@#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
        setMsgCtrlText("contactname", $res_entry("msg.cms.content.42053", "") + '!@#$%^&*;|?()\[\]\{\}\<\>\'\"');
        return "fail";
    }

    error = IsMaxLength(document.getElementById('contactname'), 'contactname_msg', 60);
    if (error == false) {
        return "fail";
    }

    //验证联系号码
    var mobile = getHtmlCtrlTrimValue("mobile");
    document.getElementById("mobile").value = mobile;
    if (htmlElemValueIsEmpty(mobile)) {
        setMsgCtrlTextAndFocus('mobile', '此项不能为空');
        return "fail";
    }


    if (!validMobile(mobile)) {
        setMsgCtrlTextAndFocus('mobile', '请输入7到20位数字');
        return "fail";
    }


    error = IsMaxLength(document.getElementById('mobile'), 'mobile_msg', 20);
    if (error == false) {
        return "fail";
    }

    //验证邮箱
    var email = getHtmlCtrlTrimValue("email");
    document.getElementById("email").value = email;
    if (htmlElemValueIsEmpty(email)) {
        setMsgCtrlTextAndFocus('email', '此项不能为空');
        return "fail";
    }
    if (!isSafe(email, new RegExp('[!\'\"#$%^&*;|?()\\[\\]\\{\\}\\<\\>]', 'g'))) {
        setMsgCtrlText("email", $res_entry("msg.cms.content.42053", "") + '!#$%^&*;|?()\[\]\{\}\<\>\'\"');
        return "fail";
    }

    var emailObj = document.getElementById("email");
    error = ismailFormat(emailObj, 'email_msg');
    if (!error) {
        return "fail";
    }

    error = IsMaxLength(document.getElementById('email'), 'email_msg', 120);
    if (error == false) {
        return "fail";
    }

    //节目类型
    var rtnValue=checkedConType();
    if(rtnValue==false){
        return "fail";
    }


    /*
    //验证FTP信息
    var cpsharedkey=getHtmlCtrlTrimValue("cpsharedkey");
    document.getElementById("cpsharedkey").value=cpsharedkey;
    if(htmlElemValueIsEmpty(cpsharedkey))
    {
        setMsgCtrlTextAndFocus('cpsharedkey','此项不能为空');
        return "fail";
    }
    */

    if (thisPageIsManager==0){
        if (getHtmlCtrlTrimValue("blstartdt").length == 0 || getHtmlCtrlTrimValue("blenddt").length == 0
        || getHtmlCtrlTrimValue("opstartdt").length == 0 || getHtmlCtrlTrimValue("openddt").length == 0){
            alert("营业执照和开户许可证开始结束时间为必填!");
            return "fail";
        }
    }

    //验证资质文件
    if (!daysBetween("blstartdt", "blenddt", "营业执照") || !daysBetween("opstartdt", "openddt", "开户许可证")
        || !daysBetween("poistartdt", "poienddt", "网络视听许可") || !daysBetween("oqstartdt", "oqenddt", "其他资质变更函")) {
        return "fail";
    }

}

/**
 * 判断两个天数
 */
function daysBetween(start, end, msg) {
    start = getHtmlCtrlTrimValue(start);
    end = getHtmlCtrlTrimValue(end);
    // 判断开始时间是否小于结束时间
    if (start.length > 0 && end.length > 0) {
        if (Date.parse(start.replace("-", "/")) > Date.parse(end.replace("-", "/"))) {
            alert(msg + " 结束时间不能小于开始时间!");
            return false;
        } else {
            return true;
        }
    } else if (start.length > 0 || end.length > 0) {
        alert(msg + " 输入开始(或结束)时间,另一项必输");
        return false;
    }
    return true;
}

//获取业务类型
function getCpposition() {
    var cpposition = "";
    document.getElementById("cpposition").value = "";
    var chk = document.getElementsByName("chk");
    var num = 0;
    for (var i = 0; i < chk.length; i++) {
        if (chk[i].checked) {
            if (num == 0) {
                cpposition = chk[i].value;
            } else {
                cpposition = cpposition + "|" + chk[i].value;
            }
            num++;
        }
    }
    document.getElementById("cpposition").value = cpposition;
}

//显示或隐藏修改按钮
function query_modify(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if ((status == 1 || status == 41) && modRight) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}

function query_modifySyn(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if (status == 40 && modRight) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}

//显示或隐藏删除按钮
function query_del(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if (status == 1 && delRight) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}

//显示或隐藏 查看审核牌照方按钮
function query_bind(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if (status != 4 && infoRight && status != 21) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}

//显示或隐藏新增同步按钮
function query_synchro(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if ((status == 1 || status == 41) && synRight) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}


//显示或隐藏删除同步按钮
function query_delSynchro(i) {
    if (i > 0) {
        var status = data[i - 1].status;
        if ((status == 40 || status == 42) && delSynRight) {
            this.style.display = "";
        }
        else {
            this.style.display = "none";
        }
    }
}


function dynChgWidth(id) {
    var selectObj = document.getElementById(id);
    if (selectObj != undefined && selectObj != null) {
        var v = selectObj.options[selectObj.selectedIndex].text;
        var width = getCharWidth(v);
        width = (width < 130 ? 130 : width);
        selectObj.style.width = width;
    }
}

function getCharWidth(str) {
    var width = 0;
    var type2 = "\'";
    var type3 = "fijltI! |[]\\:;,./";
    var type4 = "r`()-{}\"";
    var type5 = "vxyz^*";
    var type6 = "ckJ";
    var type7 = "abdeghnopqsu0123456789AFLTVXYZ~#$_+=>?";
    var type8 = "BEKPS&";
    var type9 = "wCDGHMNOQRU";
    var type11 = "mW%";
    var type12 = "@";
    for (i = 0; i < str.length; i++) {
        var value = str.charCodeAt(i);
        if (value < 0x080) {
            if (type2.indexOf(str.charAt(i)) != -1) {
                width += 2;
            }
            else if (type3.indexOf(str.charAt(i)) != -1) {
                width += 3;
            }
            else if (type4.indexOf(str.charAt(i)) != -1) {
                width += 4;
            }
            else if (type5.indexOf(str.charAt(i)) != -1) {
                width += 5;
            }
            else if (type6.indexOf(str.charAt(i)) != -1) {
                width += 6;
            }
            else if (type7.indexOf(str.charAt(i)) != -1) {
                width += 7;
            }
            else if (type8.indexOf(str.charAt(i)) != -1) {
                width += 8;
            }
            else if (type9.indexOf(str.charAt(i)) != -1) {
                width += 9;
            }
            else if (type11.indexOf(str.charAt(i)) != -1) {
                width += 11;
            }
            else if (type12.indexOf(str.charAt(i)) != -1) {
                width += 12;
            }
            else {
                width += 12;
            }
        }
        else {
            width += 12;
        }
    }
    width += 28;
    return width;
}

function IsMaxLength(input, input_msg, len) {

    if (utf8_strlen2(input.value) > len) {
        document.getElementById(input_msg).innerText = "此项长度不能超过" + len + "个字符(一个汉字占3个字符)";//"不能超过"+len+ "个字符";
        document.getElementById(input.id).focus();
        return false;
    } else {
        document.getElementById(input_msg).innerText = "";
        return true;
    }
}


var rowI;//当前行序号
function clickRow(theTR) {
    rowI = theTR.rowIndex;
    infoTable.rows(rowI).cells(0).firstChild.click();
}


var infoTable;
var tableId;

function ctrlSelectCheckbox(tabId) {
    infoTable = getTableById(tabId);
    selectedCnt = 0;
    data = new Object();
    infoTable.currentRow = 0;
    var trs = infoTable.rows;
    for (var i = 1; i < trs.length; i++) {
        var tr = trs[i];
        $(tr).click(function () {
            clickRow(this);
        });
    }
}


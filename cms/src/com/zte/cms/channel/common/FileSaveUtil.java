package com.zte.cms.channel.common;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * <p>
 * Title:
 * </p>
 * <p>
 * Description: 为了http上传文件方式用，该类只实现了文件流的写入
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class FileSaveUtil
{
    InputStream myStream;

    private boolean isEnableCover = true;

    /**
     * 
     * @param value boolean
     */
    public void setIsEnableCover(boolean value)
    {
        isEnableCover = value;
    }

    /**
     * 
     * @param inputStream InputStream
     */
    public FileSaveUtil(InputStream inputStream)
    {
        this.myStream = inputStream;
    }

    /**
     * 
     */
    public FileSaveUtil()
    {
    }

    /**
     * 
     * @param path String
     * @param content String
     * @return boolean
     */
    public boolean saveAs(String path, String content)
    {
        try
        {
            File myFile = new File(path);
            if ((!isEnableCover) && myFile.exists())
            {
                return false;
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(myFile));
            out.write(content);
            out.flush();
            out.close();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    /**
     * 
     * @return String
     */
    public String getStringFile()
    {
        int bytesread = 0;
        String s = "";
        byte[] mybyte = new byte[2048];
        try
        {
            while ((bytesread = myStream.read(mybyte, 0, 2048)) != -1)
            {
                s = s + new String(mybyte);
                mybyte = new byte[2048];
            }

            myStream.close();
        }
        catch (IOException ex)
        {
            int kkk = 0;
        }
        return s;

    }

    /**
     * 
     * @param file String
     * @return String
     */
    public String getFileContent(String file)
    {
        try
        {
            String s = "";
            File myFile = new File(file);
            BufferedReader in = new BufferedReader(new FileReader(myFile));
            String a_s = in.readLine();
            while (a_s != null)
            {
                s = s + a_s + "\r\n";
                a_s = in.readLine();
            }
            in.close();
            return s;
        }
        catch (Exception ex)
        {
            return " ";
        }
    }

    /**
     * 
     * @param path String
     * @return boolean
     */
    public boolean saveAs(String path)
    {
        if (myStream == null)
        {
            return false;
        }
        try
        {
            File myFile = new File(path);
            // decide if the file can be recovered!
            if ((!isEnableCover) && myFile.exists())
            {
                return false;
            }
            OutputStream outputStream = new FileOutputStream(myFile);
            int bytesread = 0;
            byte[] mybyte = new byte[2048];
            while ((bytesread = myStream.read(mybyte, 0, 2048)) != -1)
            {
                outputStream.write(mybyte, 0, bytesread);
            }
            outputStream.close();
            myStream.close();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    /**
     * 
     * @param filename String
     * @return String
     */
    public static synchronized String getFileExt(String filename)
    {
        int i = filename.trim().lastIndexOf(".");
        String fileExt = "";
        if (i >= 0)
        {
            fileExt = filename.substring(i + 1, filename.length());
        }
        return fileExt.toLowerCase();

    }

    /**
     * 
     * @param filename String
     * @return String
     */
    public static String getFileName(String filename)
    {
        int i = filename.lastIndexOf(".");
        String fileName = "";
        if (i > 0)
        {
            fileName = filename.substring(0, i);
        }
        return fileName;
    }

    /**
     * 
     * @param path String
     * @return boolean
     */
    public static synchronized boolean deleteFile(String path)
    {
        File myFile = new File(path);
        if (myFile.exists())
        {
            if (myFile.delete())
            {
                boolean bRes = true;
                return bRes;
            }
            else
            {
                boolean bRes = false;
                return bRes;
            }
        }
        return true;
    }

    /**
     * 节目时长(HH24MISS) M 6
     * 
     * @param starttime
     * @param endtime
     * @return
     * @throws ParseException
     */
    public static String getDuration(String starttime, String time) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        // 计算时间差
        Date sTime = null;
        sTime = sdf.parse(starttime);
        Long l = (Long.parseLong(time) * 1000 + sTime.getTime());
        Date t = new Date(l);
        return sdf.format(t);
    }

    public static boolean isShortDate(String str)
    {
        Date d = new Date(Integer.parseInt(str.substring(0, 4)), Integer.parseInt(str.substring(4, 6)) - 1, Integer
                .parseInt(str.substring(6, 8)));
        return (d.getYear() == Integer.parseInt(str.substring(0, 4)))
                && (d.getMonth() + 1) == Integer.parseInt(str.substring(4, 6))
                && d.getDate() == Integer.parseInt(str.substring(6, 8));
    }

    public static boolean checkstring(String checkstr, String userinput)
    {

        boolean allValid = false;
        for (int i = 0; i < userinput.length(); i++)
        {

            char ch = userinput.charAt(i);

            if (checkstr.indexOf(ch) >= 0)
            {

                allValid = true;

                break;
            }
        }
        return allValid;
    }

    /*
     * 数字验证
     */
    public static boolean checkint(String num)
    {
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(num);
        return m.matches();
    }

    /**
     * 字符长度验证
     * 
     * @param content
     * @param len
     * @param language
     * @return
     */
    public static boolean exlen(String content, int len, String language)
    {
        boolean result = false;
        int temp = 0;
        if ("en".equals(language))
        {
            for (int i = 0; i < content.length(); i++)
            {
                int value = content.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 3;
                }
                else
                {
                    temp += 4;
                }
            }
        }
        else
        {
            for (int i = 0; i < content.length(); i++)
            {
                int value = content.codePointAt(i);
                if (value < 0x080)
                {
                    temp += 1;
                }
                else if (value < 0x0800)
                {
                    temp += 2;
                }
                else if (value < 0x010000)
                {
                    temp += 2;
                }
                else
                {
                    temp += 4;
                }
            }
        }

        if (temp > len)
        {
            result = true;
        }
        return result;
    }
}

package com.zte.cms.commonSync.mappings;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.content.targetSyn.model.CntTargetSync;

public class CreateXmlFile4Series_Castrolemap_Map extends AbstractCreateXmlFile4Object
{

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        if (0 != targetType)
        {
        return null;
        }
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
    
        Date date = new Date();
        String fileName = "series_castrolemap_20wg"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"series"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
     
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");
        List<CntTargetSync> mapList = objectMap.get("seriesmapping");
        if (null == mapList || 0 == mapList.size())
        {
            return null;
        }
        CntTargetSync mapping = mapList.get(0);
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");    
        
        mapObjectElement.addAttribute("ID", mapping.getObjectid());
        mapObjectElement.addAttribute("Action", action);
        mapObjectElement.addAttribute("ParentType", "Series");
        mapObjectElement.addAttribute("ElementType", "CastRoleMap");
        mapObjectElement.addAttribute("ParentID", mapping.getCancelpubtime());
        mapObjectElement.addAttribute("ElementID", mapping.getReserve02());
        mapObjectElement.addAttribute("ParentCode", mapping.getCancelpubtime());
        mapObjectElement.addAttribute("ElementCode", mapping.getReserve02());
        
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", mapping.getReserve01().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);       
        
        writeDom(dom,fileFullName);      
        return retPath;       
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        if (1 == syncType || 2 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
          
        }        
        Date date = new Date();
        String targetTypeName=null;
        if(targetType==2){
            targetTypeName =  "epg";
        }else{
            return null;
        }
        
        String fileName = "series_castrolemap"+"_"+targetTypeName+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"series"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
     
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");
        List<CntTargetSync> mapList = objectMap.get("seriesmapping");
        if (null == mapList || 0 == mapList.size())
        {
            return null;
        }
        CntTargetSync mapping = mapList.get(0);
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");    
        
        mapObjectElement.addAttribute("ObjectID", mapping.getObjectid());
        mapObjectElement.addAttribute("Action", action);
        mapObjectElement.addAttribute("ParentType", "Series");
        mapObjectElement.addAttribute("ElementType", "CastRoleMap");
        mapObjectElement.addAttribute("ParentID", mapping.getParentid());
        mapObjectElement.addAttribute("ElementID", mapping.getElementid());

        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", mapping.getReserve01().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);       
        
        writeDom(dom,fileFullName);      
        return retPath;
    }

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {
        return null;
    }

}

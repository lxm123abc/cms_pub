package com.zte.cms.content.wkfw.ls;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.zte.cms.common.DbUtil;
import com.zte.cms.content.wkfw.common.IcmContentWkfwConstants;
import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;
import com.zte.cms.content.wkfw.service.IIcmContentWkfwProcessResultDS;
import com.zte.cms.content.wkfw.service.IIcmContentWkfwTaskHisResultDS;
import com.zte.cms.content.wkfw.service.IIcmContentWkfwTaskResultDS;
import com.zte.cms.cpsp.service.ICmsLpopNoauditDS;
import com.zte.jwf.interfaces.business.workflow.model.ProcessBase;
import com.zte.jwf.workflowDefine.business.mainFrame.model.SysUser;
import com.zte.jwf.workflowDefine.business.templateManage.model.WfwWorkflow;
import com.zte.jwf.workflowEngine.business.FlowEngineCore.model.WfwActivity;
import com.zte.jwf.workflowEngine.business.FlowEngineCore.model.WfwFlowprocess;
import com.zte.jwf.workflowEngine.business.messageProcess.service.WorkFlowProvider;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;

public class IcmContentWkfw implements IIcmContentWkfw
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    // 工作流待处理查询DS
    private IIcmContentWkfwTaskResultDS icmContentWkfwTaskResultDS;
    // 工作流已审核处理查询DS
    private IIcmContentWkfwTaskHisResultDS icmContentWkfwTaskHisResultDS;
    // 工作流我申请的查询DS
    private IIcmContentWkfwProcessResultDS icmContentWkfwProcessResultDS;
    // CP免审DS
    private ICmsLpopNoauditDS icmsLpopNoauditDS;
    // CP信息DS
    private IUcpBasicDS ucpBasicDS;
    // 工作流回调类
    private String beanid;
    // 工作流成功回调类
    private String callbackbean;
    // 工作流模板
    private String templateCode;
    private Vector<String> singleList = new Vector<String>();
    private Vector<String> batchList = new Vector<String>();

    public synchronized int batchAuditSyn(String taskId, String handEvent, String handResult)
            throws DomainServiceException, SQLException
    {
        WorkflowService wkflService = new WorkflowService();
        wkflService.completeTask(taskId, handResult, handEvent);
        return 0;
    }

    public TableDataInfo getMyNeedHandleWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("getMyNeedHandleWorkflow method of IcmContentWkfw class begin ....");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = loginUser.getUserId();
        condition.setExecutorId(executorId);
        // 获得当前用户ID
        OperInfo operInfo = LoginMgt.getOperInfo(null);

        // 获得当前用户ID
        condition.setOperid(Long.parseLong(operInfo.getOperid()));
        
        // 先进行时间格式的转换
        String starttime = condition.getProcessStartTimeStart();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            // System.out.println(starttime1);
            condition.setProcessStartTimeStart(starttime1);
        }
        String starttime2 = condition.getProcessStartTimeEnd();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            // System.out.println(starttime3);
            condition.setProcessStartTimeEnd(starttime3);
        }

        condition.setTaskStatus("N");
        // 查询待二审数量
        // 加载日志
        ResourceMgt.addDefaultResourceBundle("log_icmcontent_resource");
        String audit = null;
        if (condition.getNodeName().equals("1"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.oneaudit");
        }
        if (condition.getNodeName().equals("2"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.twoaudit");
        }
        if (condition.getNodeName().equals("3"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.threeaudit");
        }
        condition.setNodeName(audit);
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        log.debug("getMyNeedHandleWorkflow method of IcmContentWkfw class end ...");
        if (null != condition.getCpid() && !condition.getCpid().equals(""))
        {
            String cpid = EspecialCharMgt.conversion(condition.getCpid());
            condition.setCpid(cpid);
        }
        if (null != condition.getCpcnshortname() && !condition.getCpcnshortname().equals(""))
        {
            String Cpcnshortname = EspecialCharMgt.conversion(condition.getCpcnshortname());
            condition.setCpcnshortname(Cpcnshortname);
        }
        if (null != condition.getContentid() && !condition.getContentid().equals(""))
        {
            String Contentid = EspecialCharMgt.conversion(condition.getContentid());
            condition.setContentid(Contentid);
        }
        if (null != condition.getNamecn() && !condition.getNamecn().equals(""))
        {
            String Namecn = EspecialCharMgt.conversion(condition.getNamecn());
            condition.setNamecn(Namecn);
        }
        return icmContentWkfwTaskResultDS.pageInfoQuery(condition, start, pageSize, puEntity);
    }

    public TableDataInfo getMyHaveHandleWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("getMyHaveHandleWorkflow method of IcmContentWkfw class begin ...");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = null;
        if (loginUser != null)
        {
            executorId = loginUser.getUserId();
        }
        condition.setExecutorId(executorId);
        condition.setAcceptStatus("Y");
        condition.setTaskStatus("Y");
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        log.debug("getMyHaveHandleWorkflow method of IcmContentWkfw class end ...");
        // 先进行时间格式的转换
        String starttime = condition.getProcessStartTimeStart();
        if (!starttime.equals(""))
        {
            String starttime1 = starttime.substring(0, 4) + starttime.substring(5, 7) + starttime.substring(8, 10)
                    + starttime.substring(11, 13) + starttime.substring(14, 16) + starttime.substring(17, 19);
            // System.out.println(starttime1);
            condition.setProcessStartTimeStart(starttime1);
        }
        String starttime2 = condition.getProcessStartTimeEnd();
        if (!starttime2.equals(""))
        {
            String starttime3 = starttime2.substring(0, 4) + starttime2.substring(5, 7) + starttime2.substring(8, 10)
                    + starttime2.substring(11, 13) + starttime2.substring(14, 16) + starttime2.substring(17, 19);
            // System.out.println(starttime3);
            condition.setProcessStartTimeEnd(starttime3);
        }
        if (null != condition.getCpid() && !condition.getCpid().equals(""))
        {
            String cpid = EspecialCharMgt.conversion(condition.getCpid());
            condition.setCpid(cpid);
        }
        if (null != condition.getCpcnshortname() && !condition.getCpcnshortname().equals(""))
        {
            String Cpcnshortname = EspecialCharMgt.conversion(condition.getCpcnshortname());
            condition.setCpcnshortname(Cpcnshortname);
        }
        if (null != condition.getContentid() && !condition.getContentid().equals(""))
        {
            String Contentid = EspecialCharMgt.conversion(condition.getContentid());
            condition.setContentid(Contentid);
        }
        if (null != condition.getNamecn() && !condition.getNamecn().equals(""))
        {
            String Namecn = EspecialCharMgt.conversion(condition.getNamecn());
            condition.setNamecn(Namecn);
        }
        return icmContentWkfwTaskHisResultDS.pageInfoQuery(condition, start, pageSize, puEntity);
    }

    public TableDataInfo getMyStartWorkflow(IcmContentWkfwCondition condition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("getMyStartWorkflow method of IcmContentWkfw class begin ...");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String creator = null;
        if (loginUser != null)
        {
            creator = loginUser.getUserId();
        }
        condition.setCreator(creator);
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        log.debug("getMyStartWorkflow method of IcmContentWkfw class end ...");
        return icmContentWkfwProcessResultDS.pageInfoQuery(condition, start, pageSize, puEntity);
    }

    private int countContent(String nodename, long cpindex)
    {
        DbUtil db = new DbUtil();
        String sql = "select count(distinct w.process_id) num from wfw_task w "
                + "left join icm_content_business b on b.businessindex=w.pbo_id "
                + "left join icm_content_basicinfo p on p.contentindex = b.contentindex "
                + "left join ucp_basic c on b.cpindex = c.cpindex and c.cptype=1 " + "where w.node_name='" + nodename
                + "' and w.status='N' and b.cpindex=" + cpindex;

        try
        {
            List result = db.getQuery(sql);
            if (result.size() > 0)
            {
                HashMap map = (HashMap) result.get(0);
                return Integer.parseInt((String) map.get("num"));
            }

        }
        catch (Exception e)
        {

            e.printStackTrace();
        }
        return 0;
    }

    public void setIcmContentWkfwTaskResultDS(IIcmContentWkfwTaskResultDS icmContentWkfwTaskResultDS)
    {
        this.icmContentWkfwTaskResultDS = icmContentWkfwTaskResultDS;
    }

    public void setIcmContentWkfwTaskHisResultDS(IIcmContentWkfwTaskHisResultDS icmContentWkfwTaskHisResultDS)
    {
        this.icmContentWkfwTaskHisResultDS = icmContentWkfwTaskHisResultDS;
    }

    public void setIcmContentWkfwProcessResultDS(IIcmContentWkfwProcessResultDS icmContentWkfwProcessResultDS)
    {
        this.icmContentWkfwProcessResultDS = icmContentWkfwProcessResultDS;
    }

    public void setBeanid(String beanid)
    {
        this.beanid = beanid;
    }

    public void setTemplateCode(String templateCode)
    {
        this.templateCode = templateCode;
    }

    public long getMyNeedHandleWorkflowItemListCount(IcmContentWkfwCondition condition) throws DomainServiceException
    {
        log.debug("getMyNeedHandleWorkflowItemListCount method of IcmContentWkfw class begin ...");
        // 获得当前用户ID
        UserInfo loginUser = LoginMgt.getUserInfo();
        String executorId = loginUser.getUserId();
        condition.setExecutorId(executorId);

        // 获得当前用户ID
        OperInfo operInfo = LoginMgt.getOperInfo(null);

        // 获得当前用户ID
        condition.setOperid(Long.parseLong(operInfo.getOperid()));

        condition.setTaskStatus("N");
        // 查询待二审数量
        // 加载日志
        ResourceMgt.addDefaultResourceBundle("log_icmcontent_resource");
        String audit = null;
        if (condition.getNodeName().equals("1"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.oneaudit");
        }
        if (condition.getNodeName().equals("2"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.twoaudit");
        }
        if (condition.getNodeName().equals("3"))
        {
            audit = ResourceMgt.findDefaultText("icmcontent.workflownode.threeaudit");
        }
        condition.setNodeName(audit);
        log.debug("getMyNeedHandleWorkflowItemListCount method of IcmContentWkfw class end ...");
        return icmContentWkfwTaskResultDS.getMyNeedHandleWorkflowItemListCount(condition);
    }


    public void setIcmsLpopNoauditDS(ICmsLpopNoauditDS icmsLpopNoauditDS)
    {
        this.icmsLpopNoauditDS = icmsLpopNoauditDS;
    }

    public ICmsLpopNoauditDS getIcmsLpopNoauditDS()
    {
        return icmsLpopNoauditDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    public void setCallbackbean(String callbackbean)
    {
        this.callbackbean = callbackbean;
    }

    public long getOperatorNeedHandleWorkflowItemListCount(IcmContentWkfwCondition condition)
            throws DomainServiceException
    {
        log.debug("getOperatorNeedHandleWorkflowItemListCount method of IcmContentWkfw class begin ...");
        long count = 0;
        String audit = null;
        ResourceMgt.addDefaultResourceBundle("log_icmcontent_resource");
        // 查询待一审数量
        condition.setNodeName("1");
        
        condition.setTaskStatus("N");
        audit = ResourceMgt.findDefaultText("icmcontent.workflownode.oneaudit");
        condition.setNodeName(audit);
        count = count + icmContentWkfwTaskResultDS.getMyNeedHandleWorkflowItemListCount(condition);

        // 查询待二审数量
        condition.setNodeName("2");
        // 操作员权限判断
        condition.setTaskStatus("N");
        audit = ResourceMgt.findDefaultText("icmcontent.workflownode.twoaudit");
        condition.setNodeName(audit);
        count = count + icmContentWkfwTaskResultDS.getMyNeedHandleWorkflowItemListCount(condition);
        // 查询待三审数量
        condition.setNodeName("3");
        condition.setTaskStatus("N");
        audit = ResourceMgt.findDefaultText("icmcontent.workflownode.threeaudit");
        condition.setNodeName(audit);
        count = count + icmContentWkfwTaskResultDS.getMyNeedHandleWorkflowItemListCount(condition);
        
        log.debug("getOperatorNeedHandleWorkflowItemListCount method of IcmContentWkfw class end ...");
        return count;
    }

    public ProcessBase getProcessInstance(String processId)
    {
        Object[] obj = WorkFlowProvider.getProcessInstance(processId);
        if (null != obj)
        {

            WfwFlowprocess process = (WfwFlowprocess) obj[0];
            WfwWorkflow workflow = (WfwWorkflow) obj[1];
            SysUser user = (SysUser) obj[2];

            ProcessBase processBase = new ProcessBase();

            processBase.setProcessId(process.getProcessId());
            processBase.setSeqNo(process.getSeqNo());
            processBase.setProcessName(process.getProcessName());
            processBase.setStatus(process.getStatus());
            processBase.setStartTime(process.getStartTime());
            processBase.setCreator(process.getCreator());
            processBase.setEndTime(process.getEndTime());
            processBase.setPboId(process.getPboId());
            processBase.setPboSpec(process.getPboSpec());
            processBase.setPboName(process.getPboName());
            processBase.setPboRight(process.getPboRight());
            processBase.setRemark(process.getRemark());
            processBase.setParentProcessId(process.getTeamid());
            processBase.setSystemCode(process.getSystemCode());
            processBase.setTemplateName(workflow.getTemplateName());
            if (user != null)
            {
                processBase.setUserName(user.getUserName());
            }

            return processBase;
        }
        else
        {
            return null;
        }
    }

    public TableDataInfo getWorkflowTrack(String processId)
    {
        TableDataInfo tableInfo = new TableDataInfo();
        WorkflowService wkfwService = new WorkflowService();
        List rstList = new ArrayList();
        List list = wkfwService.getWfwActivityInstanceList(processId);

        ProcessBase process = getProcessInstance(processId);

        if (null != process)
        {
            WfwActivity activity = new WfwActivity();
            activity.setTaskName(ResourceMgt.findDefaultText("nodename.apply"));

            activity.setTaskId(BigInteger.valueOf(0L));
            activity.setUserId(process.getCreator());
            activity.setUserName(process.getUserName());
            activity.setStartDate(process.getStartTime());
            activity.setFinishDate(process.getStartTime());
            activity.setProcessId(BigInteger.valueOf(Long.valueOf(processId).longValue()));
            activity.setHandleEvent(ResourceMgt.findDefaultText("handleevent.apply"));

            rstList.add(0, activity);

            for (int i = list.size() - 1; i >= 0; --i)
            {
                rstList.add(list.get(i));
            }

            tableInfo.setData(rstList);
            tableInfo.setTotalCount(rstList.size());
            return tableInfo;
        }
        else
        {
            return getErroeWorkflowTrack(processId);
        }
    }

    public TableDataInfo getErroeWorkflowTrack(String processId)
    {
        TableDataInfo tableInfo = new TableDataInfo();
        List ExceptionList = new ArrayList();
        tableInfo.setData(ExceptionList);
        tableInfo.setTotalCount(ExceptionList.size());
        return tableInfo;
    }
}

package com.zte.ucm.log;

public class EmptyLoggerOutter extends LoggerOutter
{
  public String getId()
  {
    return EmptyLoggerOutter.class.getName();
  }

  public void print(int paramInt)
  {
  }

  public void print(String paramString)
  {
  }

  public void print(Object paramObject)
  {
  }
}
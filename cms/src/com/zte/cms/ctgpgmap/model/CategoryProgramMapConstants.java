package com.zte.cms.ctgpgmap.model;

public class CategoryProgramMapConstants {
    
	public static final String MGTTYPE_CTGPGMAP = "log.ctgpg.mgt";
	public static final String OPERTYPE_CTGPG_ADD = "log.ctgpg.mgt.add";// 新增
	public static final String OPERTYPE_CTGPG_ADD_INFO = "log.ctgpg.mgt.add.info";// 新增
	
	public static final String OPERTYPE_CTGPG_DELETE = "log.ctgpg.mgt.delete";// 删除
	public static final String OPERTYPE_CTGPG_DELETE_INFO = "log.ctgpg.mgt.delete.info";// 删除
	
	public static final String OPERTYPE_CTGPG_PUBLISH = "log.ctgpg.mgt.publish";// 发布
	public static final String OPERTYPE_CTGPG_PUBLISH_INFO = "log.ctgpg.mgt.publish.info";// 发布
	
	public static final String OPERTYPE_CTGPG_UNPUBLISH = "log.ctgpg.mgt.unpublish";// 取消发布
	public static final String OPERTYPE_CTGPG_UNPUBLISH_INFO = "log.ctgpg.mgt.unpublish.info";// 取消发布

	public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
	public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
}

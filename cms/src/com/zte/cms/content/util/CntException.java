package com.zte.cms.content.util;

@SuppressWarnings("serial")
public class CntException extends Exception
{
    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getMeaning()
    {
        return meaning;
    }

    public void setMeaning(String meaning)
    {
        this.meaning = meaning;
    }

    public CntException(String code, String meaning)
    {
        this.code = code;
        this.meaning = meaning;
    }

    private String code;
    private String meaning;
}

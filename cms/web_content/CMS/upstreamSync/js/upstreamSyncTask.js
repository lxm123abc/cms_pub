// 查询
function search()
{
	clearInputMsg('taskindex_msg');
	clearInputMsg('starttime_msg');
	clearInputMsg('endtime_msg');
	
	if( checkindexInput('taskindex', 'taskindex_msg')==false)
	{
		return;
	}		
	
	// 开始时间
	var error = true;
    error = validSearchTime('starttime1','starttime2','starttime_msg');
	if(error == false)
	{
	    return;
	}
	//查询时对于结束时间段的判断
    var error3 = true;
    error3 = validSearchTime('endtime1','endtime2','endtime_msg');
    if(error3 == false)
	{
	    return;
	}
		
	//判断开始时间的最小值是否小于结束时间的最大值
	error4 = validBeginAndEndTime('starttime1','endtime2','endtime_msg',"开始","结束");
	if(error4 == false)
	{
	    return;
	}
	
	searchTableData('searchSync','$M.synctask','zteTable'); 
	
}

function checkindexInput(input,input_msg)
{
	var inputValue = document.getElementById(input).value;      	
   	if(inputValue == "")
	    {
		    $("#"+input_msg).text("");
		    return true;
 	}
 	else
 	{
 	    var regExp = new RegExp("^[1-9]+[0-9]*$");  
 	    if(regExp.test(inputValue))
        {
            return true;
        }   
       else
       {
       $("#"+input_msg).text("请输入大于0的整数");
           return false;
       }	    
 	}
}

// 工单管理页面--xml文件预览链接，文件不存在时不显示该链接
var data={};
function setDataTable(table,obj)
{
	data = getTableById(obj).data;	
	noDataTrips(table,obj);	
	$("a[name='lnkRequest']").each(query_Reqxml);  
	$("a[name='lnkResponse']").each(query_Rspxml);  
	
	$("a[name='lnkSyncTask']").each(query_taskView); 
	
	
	$("a[name='lnkDetail']").each(query_detail);  
	$("a[name='lnkObject']").each(query_objectddetail);
	
	$("a[name='lnkviewxml']").each(query_ReqTaskxml);  
	$("a[name='lnkviewresultxml']").each(query_RspTaskxml); 
}
//上游下发对象管理页面
function setDataTable_obj(table,obj)
{
	noDataTrips(table,obj);	
	$("a[name='lnkSyncObjTask']").each(query_ObjectTask); 
	$("a[name='lnktaskDetail']").each(query_ObjectTaskDetail); 	
}

//工单页面，查看同步任务
function setDataTable_task(table,obj)
{
	data = getTableById(obj).data;	
	noDataTrips(table,obj);	
	$("a[name='lnkviewxml_sync']").each(query_syncTaskReqxml); 
	$("a[name='lnkviewresultxml_sync']").each(query_syncTaskRspxml); 	
}

//对象页面，查看同步任务
function setDataTable_obj_task(table,obj)
{
	data = getTableById(obj).data;	
	noDataTrips(table,obj);	
	$("a[name='lnkviewxml_sync_obj']").each(query_syncTaskReqxml_obj); 
	$("a[name='lnkviewresultxml_sync_obj']").each(query_syncTaskRspxml_obj);  	
}
//工单页面查看同步任务请求xml
function query_syncTaskReqxml(i)
{
	if(i>0)
	{
		var req = data[i-1].contentmngxmlurl;	
		if(req == null || !taskViewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//工单页面查看同步任务应答xml
function query_syncTaskRspxml(i)
{
	if(i>0)
	{
		var rsp = data[i-1].resultfileurl;
		if(rsp == null || !taskViewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//对象页面查看同步任务请求xml
function query_syncTaskReqxml_obj(i)
{
	if(i>0)
	{
		var req = data[i-1].contentmngxmlurl;	
		if(req == null || !objectsynctaskviewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
	
}
//对象页面查看同步任务应答xml
function query_syncTaskRspxml_obj(i)
{
	if(i>0)
	{
		var rsp = data[i-1].resultfileurl;
		if(rsp == null || !objectsynctaskviewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
	
}

//对象管理页面-同步任务查询展示权限
function query_ObjectTask(i)
{
	if(i>0)
	{
		if(!objectsynctaskviewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
	
}


//对象管理页面-查看任务权限展示
function query_ObjectTaskDetail(i)
{
	if(i>0)
	{
		if(!objecttaskviewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}



function query_ReqTaskxml(i)
{
	if(i>0)
	{
		var req = data[i-1].contentmngxmlurl;
		if(req == null || !taskViewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//设置浏览应答xml权限
function query_RspTaskxml(i)
{
	if(i>0)
	{
		var rsp = data[i-1].resultfileurl;
		if(rsp == null || !taskViewRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}

function query_taskView(i)
{
	if(i>0)
	{
		var status = data[i-1].status;	
		if(status == null || !taskViewRight)
		{			
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}

function query_Reqxml(i)
{
	if(i>0)
	{
		var req = data[i-1].localreqfileurl;	
		if(req == null || !reqDetailRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}
//设置浏览应答xml权限
function query_Rspxml(i)
{
	if(i>0)
	{
		var rsp = data[i-1].localrspfileurl;
		//alert("rsp"+rsp);
		if(rsp == null || !rspDetailRight)
		{
		    this.style.display = "none";
			return;
	    }
        this.style.display = "";
	}
}

//设置查看详情权限
function query_detail(i)
{
	if(i>0)
	{
		if(detailRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}
//设置查看对象日志权限
function query_objectddetail(i)
{
	if(i>0)
	{
		if(objectDetailRight)
		{
				 this.style.display = "";
		}else
		{
				 this.style.display = "none";
		}
	}
}

//任务状态转化
function getstatus(keyvalue)
{
	keyvalue['status'] = taskstatusMap[keyvalue['status']];
	return keyvalue;
}

//结果码转换
function getresult(keyvalue)
{
	keyvalue['result'] = taskresultMap[keyvalue['result']];
	return keyvalue;
}

//开始时间格式转换    
function formatstarttime(keyvalue) 
{
	if(keyvalue['starttime'] == null)
    {
        keyvalue['starttime'] = $res_entry("no.time.data");
    }
    else
    {
    	keyvalue['starttime'] = getTime(keyvalue['starttime']); 
    }  
    return keyvalue;
}

//结束时间格式转换    
function formatendtime(keyvalue) 
{
    if(keyvalue['endtime'] == null)
    {
        keyvalue['endtime'] = $res_entry("no.time.data");
    }
    else
    {
        keyvalue['endtime'] = getTime(keyvalue['endtime']);    
    }        
    return keyvalue;
}

// 清空
function clearData()
{
    clearSelect('status');
    
    clearInputText('taskindex');
    clearInputText('cpid');
    clearInputText('cspid');		    
    clearInputText('starttime1');
    clearInputText('starttime2');   
    clearInputText('endtime1');
    clearInputText('endtime2');
    clearInputText('description');
    clearInputText('correlateid');
    
    clearInputMsg('taskindex_msg');
    clearInputMsg('starttime_msg');
    clearInputMsg('endtime_msg');  
    clearInputMsg('cpid_msg'); 
}

// 查看链接
function viewTask(obj)
{
	if(detailRight){
		savePageStatus('zteTable');
		var taskIndex = obj.parentNode.childNodes[0].value;	
	    location.href = "upstreamSyncDetail.html?taskIndex="+taskIndex;
	}else{
		  openMsg("1:对不起,您没有上游下发工单查看权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;    
	}
}

// 查看对象日志链接
function viewTaskObject(obj)
{
	if(objectDetailRight){	
		savePageStatus('zteTable');		
		var taskindex = obj.parentNode.childNodes[0].value;
	    location.href = "object_cpcnt_record_detail.html?isLoad=false&taskindex="+taskindex;
	}else{
	     openMsg("1:对不起,您没有上游下发工单查看对象日志权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;    		
	}

}

//上游下发工单查看同步任务链接
function viewSyncTask(obj)
{
	if(taskViewRight){	
		savePageStatus('zteTable');		
		var taskindex = obj.parentNode.childNodes[0].value;
	    location.href = "upstreamSyncTaskView.html?isLoad=false&taskindex="+taskindex;
	}else{
	     openMsg("1:对不起,您没有上游下发工单查看同步任务权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;    		
	}

}

//上游同步对象查看同步任务链接
function viewSyncObjTask(obj)
{
	if(objectsynctaskviewRight){	
		savePageStatus('zteTable');		
		var taskindex = obj.parentNode.childNodes[0].value;
	    location.href = "upstreamSyncTaskView_object.html?isLoad=false&taskindex="+taskindex;
	}else{
	     openMsg("1:对不起,您没有上游下发对象查看同步任务权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;    		
	}

}

// 查看和查看对象日志页面的‘返回’按钮，_para.rtnFlag=1时返回到对象管理页面，否则返回到工单管理页面
function toTaskMainPage()
{
	if(typeof(_para.rtnFlag) != "undefined" && _para.rtnFlag == '1')
	{
		location.href = "objectMain.html";
	}
	else
	{
		location.href = "upstreamSyncMain.html";
	}	

}

//返回到任务
function toObjTaskMainPage()
{
	location.href = "objectMain.html";
}

// 查看对象日志页面--对象类型转换
function getObjType(keyvalue)
{
	keyvalue['objecttype'] = objTypeMap[keyvalue['objecttype']];
	return keyvalue;
}

// 查看对象日志页面--操作类型转换
function getActType(keyvalue)
{
	keyvalue['actiontype'] = actionMap[keyvalue['actiontype']];
	return keyvalue;
}

// 查看对象日志页面--操作结果转换
function getResCode(keyvalue)
{
	keyvalue['resultcode'] = resultMap[keyvalue['resultcode']];
	return keyvalue;
}

// 查看对象日志页面--查询
function searchObj()
{
	clearInputMsg('starttime_msg');
	
	// 开始时间
	var error = true;
    error = validSearchTime('starttime1','starttime2','starttime_msg');
	if(error == false)
	{
	    return;
	}
	
	searchTableData('searchSync','$M.synctask','zteTable'); 
}

// 查看对象日志页面--清空
function clearObjData()
{
    clearSelect('objecttype');	
    clearSelect('actiontype');
    
    clearInputText('objectcode');
    clearInputText('parentcode');		    
    clearInputText('starttime1');
    clearInputText('starttime2');   
    
    clearInputMsg('starttime_msg');
}

// 请求文件预览
function previewFile(obj)
{
	if(reqDetailRight){
		savePageStatus('zteTable');		
		var configUrl = getFTPUrl();		
		var url = obj.parentNode.childNodes[3].value;
		if(url != null && url != "")
		{
			var sURL = configUrl + "/" + url;
			var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
		    	            (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
		    window.open(sURL, "", sFeatures);
		}
		else
		{
			alert("该文件不存在");
		}	
		
	}else{
		  openMsg("1:对不起,您没有上游下发工单查看请求xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;   		
	}

}

// 应答文件预览
function previewRspFile(obj)
{
	if(rspDetailRight){
	savePageStatus('zteTable');
	
	var sURL = obj.parentNode.childNodes[4].value;
	if(sURL != null && sURL != "")
	{
		var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                        (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;

        window.open(sURL, "", sFeatures);
	}
	else
	{
		alert("该文件不存在");
	}
}else{
	  openMsg("1:对不起,您没有上游下发工单查看应答xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
		return;   		
}
}

// 查看页面的请求文件预览
function previewFileDetail()
{
	var configUrl = getFTPUrl();
	var sURL = configUrl + "/"  + xmlfileurl; 
    var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
    	            (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;

    window.open(sURL, "", sFeatures);
}

//查看页面的应答文件预览
function previewRspFileDetail()
{
	var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                    (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;

    window.open(xmlfileurlRsp, "", sFeatures);
}

// 获取请求文件地址
var urlObj = {};
var usysconfig={};
function getFTPUrl()   
{
	usysconfig["cfgkey"]="cms.cntsyn.ftpaddress";
	usysconfig["servicekey"]="CMS";
	callUrl('usysConfigFacade/getUsysConfig.ssm',usysconfig,'urlObj',true);
	return urlObj.rtnValue.cfgvalue;  
}

// 根据流水号获取任务编号
function getTaskindex(keyvalue)
{
	keyvalue['correlateid'] = taskMap[keyvalue['correlateid']];
	return keyvalue;
}

// 对象管理页面--查询
function searchAllObj()
{
	clearInputMsg('correlateid_msg');
	clearInputMsg('starttime_msg');
	
	// 检查输入的任务编号
	var taskindex = document.getElementById("taskindex").value;
	if(taskindex != null && taskindex != "")
    {
    	if( checkindexInput('taskindex', 'correlateid_msg')==false)
    	{
    		return;
    	}
    }
	// 开始时间
	var error = true;
    error = validSearchTime('starttime1','starttime2','starttime_msg');
	if(error == false)
	{
	    return;
	}
	
	searchTableData('searchSync','$M.synctask','zteTable'); 
}

// 对象管理页面--清空
function clearAllObjData()
{
	clearSelect('objecttype');	
    clearSelect('actiontype');
    clearSelect('resultcode');
    
    clearInputText('taskindex');
    clearInputText('objectcode');
    clearInputText('parentcode');		    
    clearInputText('starttime1');
    clearInputText('starttime2');   
    
    clearInputMsg('correlateid_msg');
	clearInputMsg('starttime_msg');
}

// 对象管理页面--查看任务
function viewObjTask(obj)
{
	if (getPurview("statistic.cptask.monitor.object.view") != "1") 
    {
	    openMsg("1:"+'对不起,您没有上游下发对象查看任务权限','./uiloader/forward.html',true);  //跳转到登录页面
		return;
    }
	savePageStatus('zteTable');		
	var taskindex = obj.parentNode.childNodes[0].value;
	//var taskindex = taskMap[correlateid];
	//alert(correlateid);
	//alert(taskindex);
    // 链接进入公用的工单详细信息页面，返回时仍然返回到对象管理页面（用rtnFlag=1标识）
	location.href = "upstreamSyncDetail_object.html?taskIndex="+taskindex+"&rtnFlag=1";
}

//增加权限
var detailRight = false;//查看权限
var objectDetailRight = false;//对象日志查看权限
var taskViewRight = false; //同步任务查看权限
var reqDetailRight = false;//预览请求xml权限
var rspDetailRight = false;	//预览应答xml权限

function showPurview()
{

	if(getPurview("statistic.cptask.monitor.task") !=1)
	{

            openMsg("1:对不起,您没有上游下发工单访问权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	if(getPurview("statistic.cptask.monitor.task.view") ==1)detailRight = true;//查看权限

	if(getPurview("statistic.cptask.monitor.task.objects") ==1)objectDetailRight = true;//对象日志查看权限
	
	if(getPurview("statistic.cptask.monitor.task.taskview") ==1)taskViewRight = true;//同步任务查看查看权限

	if(getPurview("statistic.cptask.monitor.task.reqview") ==1)reqDetailRight = true;//预览请求xml权限
	
	if(getPurview("statistic.cptask.monitor.task.rspview") ==1)rspDetailRight = true;	//预览应答xml权限
	
	
//	alert("detailRight "+detailRight);
//    alert("objectDetailRight "+objectDetailRight);
//	alert("reqDetailRight "+reqDetailRight);
//	alert("rspDetailRight "+rspDetailRight);

}
var objectsynctaskviewRight = false; //同步任务查看对象监控页面查看权限
var objecttaskviewRight = false; //对象监控页面查看权限

function showObjPurview()
{
	if (getPurview("statistic.cptask.monitor.object") != "1") 
    {
	    openMsg("1:"+'对不起,您没有上游下发对象访问权限','./uiloader/forward.html',true);  //跳转到登录页面
		return;
    }
	
	if(getPurview("statistic.cptask.monitor.object.taskview") ==1) objectsynctaskviewRight = true;//同步任务查看对象监控页面查看权限
	
	if(getPurview("statistic.cptask.monitor.object.view") ==1) objecttaskviewRight = true; //对象监控页面查看权限
	
}

//任务监控页面--任务状态转化
function getstatus(keyvalue)
{
    keyvalue['status'] = taskstatusMap[keyvalue['status']];
	return keyvalue;
}

// 任务监控页面--平台类型转换
function getplatform(keyvalue)
{
    keyvalue['platform'] = platformMap[keyvalue['platform']];
	return keyvalue;
}
//目标系统类型转化
function getTargettype(keyvalue) 
{
     keyvalue['targettype'] = targettypeMap[keyvalue['targettype']];
     return keyvalue;
}
//任务监控页面--工单来源类型转换
function getsource(keyvalue)	
{  
	keyvalue['source'] = sourceMap[keyvalue['source']];
	return keyvalue;
}  
//开始时间格式转换
function formatSDate(keyvalue)
{
    if(keyvalue['starttime']==null)
    {
       keyvalue['starttime']= $res_entry("no.time.data");
    }
    else
    {
    	 keyvalue['starttime'] = translateStrToDate(keyvalue['starttime']); 
    }     
    return keyvalue;
}

// 结束时间格式转换
function formatEDate(keyvalue)
{
    if(keyvalue['endtime']==null)
    {
      keyvalue['endtime']=  $res_entry("no.time.data");
    }
    else
    {
      keyvalue['endtime'] = translateStrToDate(keyvalue['endtime']);    
    }
        
    return keyvalue;
}
//任务监控页面--预览请求xml文件 
var urlObj = {};
function view(taskindex)
{
	if(!taskViewRight)
	{

            openMsg("1:对不起,您没有查看请求xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	 callUrl('cntSyncTaskLSFacade/getPreviewXMLPath.ssm',taskindex,'urlObj',true);
	 var configUrl = urlObj.rtnValue;
	 
	 if(configUrl !=null)
	 {		
		 var URL = configUrl; 
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(URL, "", sFeatures);
	 }
	 else
	 {
		 alert("该文件不存在");		 
	 }	
}
//任务监控页面--预览应答xml文件
function viewResult(resultfileurl)
{
	if(!taskViewRight)
	{

            openMsg("1:对不起,您没有查看应答xml权限",'./uiloader/forward.html',true);  //跳转到登录页面
			return;           
	}	
	 if( resultfileurl != '' && resultfileurl != 'null' && resultfileurl != "undefined")
	 {
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(resultfileurl, "", sFeatures);
	 } 
	 else
	 {
		 alert("该文件不存在");			 
	 }
}

// 同步任务--预览请求xml文件 
var urlObj = {};
function viewsync(taskindex)
{
	 callUrl('cntSyncTaskLSFacade/getPreviewXMLPath.ssm',taskindex,'urlObj',true);
	 var configUrl = urlObj.rtnValue;
	 
	 if(configUrl !=null)
	 {		
		 var URL = configUrl; 
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(URL, "", sFeatures);
	 }
	 else
	 {
		 alert("该文件不存在");		 
	 }	
}

// 同步任务--预览应答xml文件
function viewResultsync(resultfileurl)
{
	 if( resultfileurl != '' && resultfileurl != 'null' && resultfileurl != "undefined")
	 {
		 var sFeatures = "height=500,width=900,toolbar=no,menubar=no,resizeable=no,location=no,status=no,scrollbars=yes,top="+ 
                         (screen.height - 600)/2 + ",left=" + (screen.width - 720)/2;
         window.open(resultfileurl, "", sFeatures);
	 } 
	 else
	 {
		 alert("该文件不存在");			 
	 }
}




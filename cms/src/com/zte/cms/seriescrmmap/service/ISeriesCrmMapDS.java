package com.zte.cms.seriescrmmap.service;

import java.util.List;

import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ISeriesCrmMapDS
{
    /**
     * 新增SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public String insertSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 批量更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesCrmMapList(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException;

    /**
     * 根据条件更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 根据条件批量更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesCrmMapListByCond(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException;

    /**
     * 删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 批量删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesCrmMapList(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException;

    /**
     * 根据条件删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 根据条件批量删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesCrmMapListByCond(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException;

    /**
     * 查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @return SeriesCrmMap对象
     * @throws DomainServiceException ds异常
     */
    public SeriesCrmMap getSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 根据条件查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @return 满足条件的SeriesCrmMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<SeriesCrmMap> getSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
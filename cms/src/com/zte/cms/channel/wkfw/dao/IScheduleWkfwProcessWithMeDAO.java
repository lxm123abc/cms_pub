package com.zte.cms.channel.wkfw.dao;

import java.util.List;


import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwProcessWithMe;

import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IScheduleWkfwProcessWithMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ScheduleWkfwProcessWithMe> queryScheduleWkfwProcessWithMeListByCond(
            ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int queryScheduleWkfwProcessWithMeListCntByCond(
            ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition, int start,
            int pageSize) throws DAOException;

    public abstract PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwProcessWithMeCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DAOException;

}
package com.zte.cms.content.series.ls;

import java.util.List;

import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsSeriesLS
{
    /**
     * 连续剧内容新增
     * 
     * @param cmsSeries 连续剧对象
     * @return 新增操作结果
     */
    public String insertCmsSeries(CmsSeries cmsSeries) throws DomainServiceException;

    /**
     * 根据模糊条件查询并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据模糊条件查询连续剧平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据seriesid和platform查询连续剧网元发布数据
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize)
    throws DomainServiceException;
    
    /**
     * 根据连续index获取连续剧内容对象
     * 
     * @param seriesindex 连续剧Index
     * @return 连续剧对象
     */
    public CmsSeries getCmsSeriesByIndex(Long seriesindex) throws DomainServiceException;

    /**
     * 修改连续剧内容
     * 
     * @param cmsSeries 连续剧内容对象
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String updateCmsSeries(CmsSeries cmsSeries) throws Exception;

    /**
     * 根据Index删除连续剧内容
     * 
     * @param seriesindex 连续剧内容索引值
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String removeCmsSeries(Long seriesindex) throws DomainServiceException;
    
    /**
     * 连续剧关联平台
     * @param targetsystemList 目标系统列表
     * @param cmsSeriesList   连续剧列表
     * @return
     * @throws DomainServiceException
     */
    public String bindSeriesTargetsys(
            List<Targetsystem> targetsystemList,
            List<CmsSeries> cmsSeriesList) throws DomainServiceException;
    
    public String bind2Platform(Targetsystem target, CntPlatformSync cntPlatformSync) throws DomainServiceException;
    
    /**
     * 发布单个连续剧
     * 
     * @param series连续剧
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String publishCmsSeries(CmsSeries series) throws Exception;
    
    /**
     * 连续剧内容取消发布服务
     * 
     * @param series连续剧
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String cancelPublishSeries(CmsSeries series) throws Exception;
   
    /**
     * 连续剧内容网元取消发布服务
     * @param seriesTargetSync
     * @return
     * @throws Exception
     */
    public String cancelPublishSerie2Target(CntTargetSync seriesTargetSync) throws Exception;

    /**
     * 批量发布连续剧内容
     * 
     * @param indexLists 连续剧内容id列表
     * @return 批量操作结果提示
     * @throws Exception
     */
    public String batchPublishSeries(List<CmsSeries> seriesList) throws Exception;
    
    /**
     * 发布连续剧内容到网元
     * @param seriesTargetSync 网元发布数据
     * @return 操作结果提示
     * @throws Exception
     */
    public String publishSerie2Target(CntTargetSync seriesTargetSync) throws Exception;

    /**
     * 批量取消发布连续剧内容
     * 
     * @param indexLists 连续剧内容id列表
     * @return 批量操作结果提示
     * @throws Exception
     */
    public String batchCancelPublishSeries(List<CmsSeries> seriesList) throws Exception;

    /**
     * 预下线连续剧内容
     * 
     * @param seriesindex 连续剧内容主键
     * @return 操作结果
     * @throws DomainServiceException ls异常
     */
    public String updateSeriesForPreOff(long seriesindex) throws DomainServiceException;
    
    /**
     * 根据模糊条件查询栏目连续剧关联关系平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws Exception;
    /**
     * 根据模糊条件查询栏目连续剧关联关系网元发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws Exception;
    /**
     * 根据模糊条件查询连续剧角色关联关系平台发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws Exception;
    /**
     * 根据模糊条件查询连续剧角色关联关系发布数据并在z-table中显示
     * 
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public TableDataInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws Exception;
    
    /**
     * 批量删除连续剧关联平台
     * @param targetsystemList 目标系统列表
     * @param cmsSeriesList   连续剧列表
     * @return
     * @throws DomainServiceException
     */
    public String removeSeriesTargetsysBindList(
            List<Targetsystem> targetsystemList,
            List<CmsSeries> cmsSeriesList) throws DomainServiceException;
    

    /**
     * 删除连续剧关联平台
     * @param targetsystemList 目标系统列表
     * @param cmsSeriesList   连续剧列表
     * @return
     * @throws DomainServiceException
     */
    public String removeSeriesTargetsysBind(
            Targetsystem targetsystem,
            CmsSeries cmsSeries) throws DomainServiceException;
}

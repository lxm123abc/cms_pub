<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<html>
<head>
<title><fmt:message key="operListLog.operlog"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   // ???��???����??���?����o�����騦����??������
   function viewOper (operID,operName) {
      parent.logFrame.document.view.operID.value = operID;
      parent.logFrame.document.view.operName.value = operName;
   }
</script>
</head>
<body class="body-style1">
<%
    try {
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        purview.setServiceKey(serviceKey);        
        String operID = (String)session.getAttribute("OPERID");
        ArrayList list = new ArrayList();
        HashMap map = new HashMap();
        if (operID != null)
            list = purview.sortChildOperators(operID,"");
        if (list == null)
            list = new ArrayList();
%>
<table border="1" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr class="table-title1">
    <td width="90" height="24"><fmt:message key="operInfo.loginname"/></td>
  </tr>
<%
        for (int i = 0; i < list.size(); i++) {
            map = (HashMap)list.get(i);
%>
  <tr ondblclick="javascript:viewOper('<%= (String)map.get("OPERID")%>','<%= (String)map.get("OPERNAME") %>')">
    <td height="22"><%= (String)map.get("OPERNAME") %>&nbsp;</td>
  </tr>
<%
        }
%>
</table>
</form>
<%
    }
    catch (Exception e) {
%>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
<%
    }
%>
</table>
</body>
</html>

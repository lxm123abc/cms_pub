package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsLpopNoauditDAO
{
    /**
     * 新增CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DAOException dao异常
     */
    public void insertCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据主键更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DAOException dao异常
     */
    public void updateCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据条件更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据主键删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DAOException dao异常
     */
    public void deleteCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据条件删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据主键查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return 满足条件的CmsLpopNoaudit对象
     * @throws DAOException dao异常
     */
    public CmsLpopNoaudit getCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据条件查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return 满足条件的CmsLpopNoaudit对象集
     * @throws DAOException dao异常
     */
    public List<CmsLpopNoaudit> getCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DAOException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象，作为查询条件的参数
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象，作为查询条件的参数
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象，作为查询条件的参数
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return 满足条件的CmsLpopNoaudit对象集
     * @throws DAOException dao异常
     */
    public List<UcpCmsLpopNoaudit> getCmsLpopNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit) throws DAOException;

    /**
     * 查询CP信息 以table展示
     */
    public PageInfo pageInfoQueryForOPTable(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize)
            throws DAOException;

}
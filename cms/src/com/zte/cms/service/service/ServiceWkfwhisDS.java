package com.zte.cms.service.service;

import java.util.List;

import com.zte.cms.service.dao.IServiceWkfwhisDAO;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceWkfwhisDS extends DynamicObjectBaseDS implements IServiceWkfwhisDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceWkfwhisDAO dao = null;

    public void setDao(IServiceWkfwhisDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException
    {
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("ucdn_service_wkfw_his_index");
        serviceWkfwhis.setHistoryindex(index);
        String historydate = ServiceConstants.getCurrentTimeString();
        serviceWkfwhis.setHistorydate(historydate);
        log.debug("insert serviceWkfwhis starting...");
        try
        {
            dao.insertServiceWkfwhis(serviceWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceWkfwhis end");
    }

    public void updateServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException
    {
        log.debug("update serviceWkfwhis by pk starting...");
        try
        {
            dao.updateServiceWkfwhis(serviceWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceWkfwhis by pk end");
    }

    public void updateServiceWkfwhisList(List<ServiceWkfwhis> serviceWkfwhisList) throws DomainServiceException
    {
        log.debug("update serviceWkfwhisList by pk starting...");
        if (null == serviceWkfwhisList || serviceWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in serviceWkfwhisList");
            return;
        }
        try
        {
            for (ServiceWkfwhis serviceWkfwhis : serviceWkfwhisList)
            {
                dao.updateServiceWkfwhis(serviceWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceWkfwhisList by pk end");
    }

    public void removeServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException
    {
        log.debug("remove serviceWkfwhis by pk starting...");
        try
        {
            dao.deleteServiceWkfwhis(serviceWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceWkfwhis by pk end");
    }

    public void removeServiceWkfwhisList(List<ServiceWkfwhis> serviceWkfwhisList) throws DomainServiceException
    {
        log.debug("remove serviceWkfwhisList by pk starting...");
        if (null == serviceWkfwhisList || serviceWkfwhisList.size() == 0)
        {
            log.debug("there is no datas in serviceWkfwhisList");
            return;
        }
        try
        {
            for (ServiceWkfwhis serviceWkfwhis : serviceWkfwhisList)
            {
                dao.deleteServiceWkfwhis(serviceWkfwhis);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceWkfwhisList by pk end");
    }

    public ServiceWkfwhis getServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException
    {
        log.debug("get serviceWkfwhis by pk starting...");
        ServiceWkfwhis rsObj = null;
        try
        {
            rsObj = dao.getServiceWkfwhis(serviceWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceWkfwhisList by pk end");
        return rsObj;
    }

    public List<ServiceWkfwhis> getServiceWkfwhisByCond(ServiceWkfwhis serviceWkfwhis) throws DomainServiceException
    {
        log.debug("get serviceWkfwhis by condition starting...");
        List<ServiceWkfwhis> rsList = null;
        try
        {
            rsList = dao.getServiceWkfwhisByCond(serviceWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceWkfwhis by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceWkfwhis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceWkfwhis page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get serviceWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceWkfwhis, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwhis>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceWkfwhis page info by condition end");
        return tableInfo;
    }
}

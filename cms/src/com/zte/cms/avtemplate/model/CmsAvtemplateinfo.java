package com.zte.cms.avtemplate.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

/**
 * 模版
 * 
 * @author Administrator
 * 
 */
public class CmsAvtemplateinfo extends DynamicBaseObject
{
    private java.lang.Long templateindex;
    private java.lang.String templatename;
    private java.lang.Integer templatetype;
    private java.lang.Integer grouptype;
    private java.lang.Integer servicetype;
    private java.lang.Integer framewidth;
    private java.lang.Integer framehight;
    private java.lang.Integer framerate;
    private java.lang.Integer videobitrate;
    private java.lang.String videotype;
    private java.lang.Integer audiosamplerate;
    private java.lang.Integer audiobitrate;
    private java.lang.String audiotype;
    private java.lang.String videoformat;
    private java.lang.String description;

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Long getTemplateindex()
    {
        return templateindex;
    }

    public void setTemplateindex(java.lang.Long templateindex)
    {
        this.templateindex = templateindex;
    }

    public java.lang.String getTemplatename()
    {
        return templatename;
    }

    public void setTemplatename(java.lang.String templatename)
    {
        this.templatename = templatename;
    }

    public java.lang.Integer getTemplatetype()
    {
        return templatetype;
    }

    public void setTemplatetype(java.lang.Integer templatetype)
    {
        this.templatetype = templatetype;
    }

    public java.lang.Integer getGrouptype()
    {
        return grouptype;
    }

    public void setGrouptype(java.lang.Integer grouptype)
    {
        this.grouptype = grouptype;
    }

    public java.lang.Integer getServicetype()
    {
        return servicetype;
    }

    public void setServicetype(java.lang.Integer servicetype)
    {
        this.servicetype = servicetype;
    }

    public java.lang.Integer getFramewidth()
    {
        return framewidth;
    }

    public void setFramewidth(java.lang.Integer framewidth)
    {
        this.framewidth = framewidth;
    }

    public java.lang.Integer getFramehight()
    {
        return framehight;
    }

    public void setFramehight(java.lang.Integer framehight)
    {
        this.framehight = framehight;
    }

    public java.lang.Integer getFramerate()
    {
        return framerate;
    }

    public void setFramerate(java.lang.Integer framerate)
    {
        this.framerate = framerate;
    }

    public java.lang.Integer getVideobitrate()
    {
        return videobitrate;
    }

    public void setVideobitrate(java.lang.Integer videobitrate)
    {
        this.videobitrate = videobitrate;
    }

    public java.lang.String getVideotype()
    {
        return videotype;
    }

    public void setVideotype(java.lang.String videotype)
    {
        this.videotype = videotype;
    }

    public java.lang.Integer getAudiosamplerate()
    {
        return audiosamplerate;
    }

    public void setAudiosamplerate(java.lang.Integer audiosamplerate)
    {
        this.audiosamplerate = audiosamplerate;
    }

    public java.lang.Integer getAudiobitrate()
    {
        return audiobitrate;
    }

    public void setAudiobitrate(java.lang.Integer audiobitrate)
    {
        this.audiobitrate = audiobitrate;
    }

    public java.lang.String getAudiotype()
    {
        return audiotype;
    }

    public void setAudiotype(java.lang.String audiotype)
    {
        this.audiotype = audiotype;
    }

    public java.lang.String getVideoformat()
    {
        return videoformat;
    }

    public void setVideoformat(java.lang.String videoformat)
    {
        this.videoformat = videoformat;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub
        this.addRelation("templatename", "TEMPLATENAME");
        this.addRelation("templatetype", "TEMPLATETYPE");
        this.addRelation("grouptype", "GROUPTYPE");
        this.addRelation("framewidth", "FRAMEWIDTH");
        this.addRelation("framehight", "FRAMEHIGHT");
        this.addRelation("framerate", "FRAMERATE");
        this.addRelation("videobitrate", "VIDEOBITRATE");
        this.addRelation("videotype", "VIDEOTYPE");
        this.addRelation("audiosamplerate", "AUDIOSAMPLERATE");
        this.addRelation("audiobitrate", "AUDIOBITRATE");
        this.addRelation("audiotype", "AUDIOTYPE");
        this.addRelation("servicetype", "SERVICETYPE");
        this.addRelation("videoformat", "VIDEOFORMAT");
        this.addRelation("templateindex", "TEMPLATEINDEX");
        this.addRelation("description", "DESCRIPTION");
    }

}

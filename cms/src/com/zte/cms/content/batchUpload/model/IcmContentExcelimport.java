package com.zte.cms.content.batchUpload.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class IcmContentExcelimport extends DynamicBaseObject
{
    private java.lang.Long importindex;
    private java.lang.Long contentindex;
    private java.lang.String contentid;
    private java.lang.String cpid;
    private java.lang.String fileid;
    private java.lang.String namecn;
    private java.lang.String keywords;
    private java.lang.String playtime;
    private java.lang.String introduction;
    private java.lang.String premieredate;
    private java.lang.String effectivedate;
    private java.lang.String expirydate;
    private java.lang.String offlinetime;
    private java.lang.String deletetime;
    private java.lang.String recommendedstar;
    private java.lang.String contenttags;
    private java.lang.Integer contentlevel;
    private java.lang.String licenseorgan;
    private java.lang.String licensecode;
    private java.lang.String provid;
    private java.lang.String cityid;
    private java.lang.Integer feetype;
    private java.lang.String copyrightcn;
    private java.lang.String copyrightexpiration;
    private java.lang.Integer copyrightproperty;
    private java.lang.Integer exclusivevalidity;
    private java.lang.Integer platform;
    private java.lang.Integer authorizationway;
    private java.lang.String copyrightcode;
    private java.lang.String importlisence;
    private java.lang.String releaselisence;
    private java.lang.String actor;
    private java.lang.String director;
    private java.lang.String nameen;
    private java.lang.String releasedate;
    private java.lang.String contentversion;
    private java.lang.String country;
    private java.lang.String publisher;
    private java.lang.String productcorp;
    private java.lang.String languages;
    private java.lang.String subtitlelanguage;
    private java.lang.String dubbinglanguage;
    private java.lang.String producer;
    private java.lang.String opername;
    private java.lang.Integer excelrownum;

    public java.lang.Long getImportindex()
    {
        return importindex;
    }

    public void setImportindex(java.lang.Long importindex)
    {
        this.importindex = importindex;
    }

    public java.lang.Long getContentindex()
    {
        return contentindex;
    }

    public void setContentindex(java.lang.Long contentindex)
    {
        this.contentindex = contentindex;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getFileid()
    {
        return fileid;
    }

    public void setFileid(java.lang.String fileid)
    {
        this.fileid = fileid;
    }

    public java.lang.String getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.String namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.String getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.String keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.String getPlaytime()
    {
        return playtime;
    }

    public void setPlaytime(java.lang.String playtime)
    {
        this.playtime = playtime;
    }

    public java.lang.String getIntroduction()
    {
        return introduction;
    }

    public void setIntroduction(java.lang.String introduction)
    {
        this.introduction = introduction;
    }

    public java.lang.String getPremieredate()
    {
        return premieredate;
    }

    public void setPremieredate(java.lang.String premieredate)
    {
        this.premieredate = premieredate;
    }

    public java.lang.String getEffectivedate()
    {
        return effectivedate;
    }

    public void setEffectivedate(java.lang.String effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public java.lang.String getExpirydate()
    {
        return expirydate;
    }

    public void setExpirydate(java.lang.String expirydate)
    {
        this.expirydate = expirydate;
    }

    public java.lang.String getOfflinetime()
    {
        return offlinetime;
    }

    public void setOfflinetime(java.lang.String offlinetime)
    {
        this.offlinetime = offlinetime;
    }

    public java.lang.String getDeletetime()
    {
        return deletetime;
    }

    public void setDeletetime(java.lang.String deletetime)
    {
        this.deletetime = deletetime;
    }

    public java.lang.String getRecommendedstar()
    {
        return recommendedstar;
    }

    public void setRecommendedstar(java.lang.String recommendedstar)
    {
        this.recommendedstar = recommendedstar;
    }

    public java.lang.String getContenttags()
    {
        return contenttags;
    }

    public void setContenttags(java.lang.String contenttags)
    {
        this.contenttags = contenttags;
    }

    public java.lang.Integer getContentlevel()
    {
        return contentlevel;
    }

    public void setContentlevel(java.lang.Integer contentlevel)
    {
        this.contentlevel = contentlevel;
    }

    public java.lang.String getLicenseorgan()
    {
        return licenseorgan;
    }

    public void setLicenseorgan(java.lang.String licenseorgan)
    {
        this.licenseorgan = licenseorgan;
    }

    public java.lang.String getLicensecode()
    {
        return licensecode;
    }

    public void setLicensecode(java.lang.String licensecode)
    {
        this.licensecode = licensecode;
    }

    public java.lang.String getProvid()
    {
        return provid;
    }

    public void setProvid(java.lang.String provid)
    {
        this.provid = provid;
    }

    public java.lang.String getCityid()
    {
        return cityid;
    }

    public void setCityid(java.lang.String cityid)
    {
        this.cityid = cityid;
    }

    public java.lang.Integer getFeetype()
    {
        return feetype;
    }

    public void setFeetype(java.lang.Integer feetype)
    {
        this.feetype = feetype;
    }

    public java.lang.String getCopyrightcn()
    {
        return copyrightcn;
    }

    public void setCopyrightcn(java.lang.String copyrightcn)
    {
        this.copyrightcn = copyrightcn;
    }

    public java.lang.String getCopyrightexpiration()
    {
        return copyrightexpiration;
    }

    public void setCopyrightexpiration(java.lang.String copyrightexpiration)
    {
        this.copyrightexpiration = copyrightexpiration;
    }

    public java.lang.Integer getCopyrightproperty()
    {
        return copyrightproperty;
    }

    public void setCopyrightproperty(java.lang.Integer copyrightproperty)
    {
        this.copyrightproperty = copyrightproperty;
    }

    public java.lang.Integer getExclusivevalidity()
    {
        return exclusivevalidity;
    }

    public void setExclusivevalidity(java.lang.Integer exclusivevalidity)
    {
        this.exclusivevalidity = exclusivevalidity;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.Integer getAuthorizationway()
    {
        return authorizationway;
    }

    public void setAuthorizationway(java.lang.Integer authorizationway)
    {
        this.authorizationway = authorizationway;
    }

    public java.lang.String getCopyrightcode()
    {
        return copyrightcode;
    }

    public void setCopyrightcode(java.lang.String copyrightcode)
    {
        this.copyrightcode = copyrightcode;
    }

    public java.lang.String getImportlisence()
    {
        return importlisence;
    }

    public void setImportlisence(java.lang.String importlisence)
    {
        this.importlisence = importlisence;
    }

    public java.lang.String getReleaselisence()
    {
        return releaselisence;
    }

    public void setReleaselisence(java.lang.String releaselisence)
    {
        this.releaselisence = releaselisence;
    }

    public java.lang.String getActor()
    {
        return actor;
    }

    public void setActor(java.lang.String actor)
    {
        this.actor = actor;
    }

    public java.lang.String getDirector()
    {
        return director;
    }

    public void setDirector(java.lang.String director)
    {
        this.director = director;
    }

    public java.lang.String getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.String nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.String getReleasedate()
    {
        return releasedate;
    }

    public void setReleasedate(java.lang.String releasedate)
    {
        this.releasedate = releasedate;
    }

    public java.lang.String getContentversion()
    {
        return contentversion;
    }

    public void setContentversion(java.lang.String contentversion)
    {
        this.contentversion = contentversion;
    }

    public java.lang.String getCountry()
    {
        return country;
    }

    public void setCountry(java.lang.String country)
    {
        this.country = country;
    }

    public java.lang.String getPublisher()
    {
        return publisher;
    }

    public void setPublisher(java.lang.String publisher)
    {
        this.publisher = publisher;
    }

    public java.lang.String getProductcorp()
    {
        return productcorp;
    }

    public void setProductcorp(java.lang.String productcorp)
    {
        this.productcorp = productcorp;
    }

    public java.lang.String getLanguages()
    {
        return languages;
    }

    public void setLanguages(java.lang.String languages)
    {
        this.languages = languages;
    }

    public java.lang.String getSubtitlelanguage()
    {
        return subtitlelanguage;
    }

    public void setSubtitlelanguage(java.lang.String subtitlelanguage)
    {
        this.subtitlelanguage = subtitlelanguage;
    }

    public java.lang.String getDubbinglanguage()
    {
        return dubbinglanguage;
    }

    public void setDubbinglanguage(java.lang.String dubbinglanguage)
    {
        this.dubbinglanguage = dubbinglanguage;
    }

    public java.lang.String getProducer()
    {
        return producer;
    }

    public void setProducer(java.lang.String producer)
    {
        this.producer = producer;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public java.lang.Integer getExcelrownum()
    {
        return excelrownum;
    }

    public void setExcelrownum(java.lang.Integer excelrownum)
    {
        this.excelrownum = excelrownum;
    }

    public void initRelation()
    {
        this.addRelation("importindex", "IMPORTINDEX");
        this.addRelation("contentindex", "CONTENTINDEX");
        this.addRelation("contentid", "CONTENTID");
        this.addRelation("cpid", "CPID");
        this.addRelation("fileid", "FILEID");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("playtime", "PLAYTIME");
        this.addRelation("introduction", "INTRODUCTION");
        this.addRelation("premieredate", "PREMIEREDATE");
        this.addRelation("effectivedate", "EFFECTIVEDATE");
        this.addRelation("expirydate", "EXPIRYDATE");
        this.addRelation("offlinetime", "OFFLINETIME");
        this.addRelation("deletetime", "DELETETIME");
        this.addRelation("recommendedstar", "RECOMMENDEDSTAR");
        this.addRelation("contenttags", "CONTENTTAGS");
        this.addRelation("contentlevel", "CONTENTLEVEL");
        this.addRelation("licenseorgan", "LICENSEORGAN");
        this.addRelation("licensecode", "LICENSECODE");
        this.addRelation("provid", "PROVID");
        this.addRelation("cityid", "CITYID");
        this.addRelation("feetype", "FEETYPE");
        this.addRelation("copyrightcn", "COPYRIGHTCN");
        this.addRelation("copyrightexpiration", "COPYRIGHTEXPIRATION");
        this.addRelation("copyrightproperty", "COPYRIGHTPROPERTY");
        this.addRelation("exclusivevalidity", "EXCLUSIVEVALIDITY");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("authorizationway", "AUTHORIZATIONWAY");
        this.addRelation("copyrightcode", "COPYRIGHTCODE");
        this.addRelation("importlisence", "IMPORTLISENCE");
        this.addRelation("releaselisence", "RELEASELISENCE");
        this.addRelation("actor", "ACTOR");
        this.addRelation("director", "DIRECTOR");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("releasedate", "RELEASEDATE");
        this.addRelation("contentversion", "CONTENTVERSION");
        this.addRelation("country", "COUNTRY");
        this.addRelation("publisher", "PUBLISHER");
        this.addRelation("productcorp", "PRODUCTCORP");
        this.addRelation("languages", "LANGUAGES");
        this.addRelation("subtitlelanguage", "SUBTITLELANGUAGE");
        this.addRelation("dubbinglanguage", "DUBBINGLANGUAGE");
        this.addRelation("producer", "PRODUCER");
        this.addRelation("opername", "OPERNAME");
        this.addRelation("excelrownum", "EXCELROWNUM");
    }
}

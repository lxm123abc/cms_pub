package com.zte.ismp.common;

public class ExcelHead
{

    /** 表头名称 * */
    String headName;
    /** 表头字段名称，即导出Excel中表格表头名称 * */
    String headField;

    public ExcelHead(String headField, String headName)
    {
        this.headField = headField;
        this.headName = headName;
    }

    public String getHeadName()
    {
        return headName;
    }

    public void setHeadName(String headName)
    {
        this.headName = headName;
    }

    public String getHeadField()
    {
        return headField;
    }

    public void setHeadField(String headField)
    {
        this.headField = headField;
    }

}

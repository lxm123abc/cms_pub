<%@ page import="java.lang.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%
    try {
        String serviceKey = (String)session.getAttribute("SERVICEKEY");
        purview.setServiceKey(serviceKey); 
        String operID = (String)request.getParameter("operID");
        HashMap map = purview.getOperInformation(operID);
%>
<html>
<head>
<title><fmt:message key="operDel.deleteoper"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript">
   // 1?��??��?��??���?��??D��1?��?����??
   function unLoad () {
      window.opener.focus();
   }

   // ��3??����??
   function onSubmit () {
      document.view.submit();
   }
</script>
</head>
<body class="body-style1" onunload="javascript:unLoad()">
<form name="view" method="post" action="operDelEnd.jsp">
<input type="hidden" name="operID" value="<%= operID %>">
<table border="1" width="300" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style5">
  <tr>
    <td width="33%" height="22" align="right" class="table-title"><fmt:message key="operDel.opername"/>:</td>
    <td height="22" widht="67%"><%= (String)map.get("OPERNAME") %>&nbsp;</td>
  </tr>
  <tr>
    <td width="33%" height="22" align="right" class="table-title"><fmt:message key="operDel.operfullname"/>:</td>
    <td height="22" widht="67%"><%= (String)map.get("OPERALLNAME") %>&nbsp;</td>
  </tr>
  <tr>
    <td width="33%" height="22" align="right" class="table-title"><fmt:message key="operDel.description"/>:</td>
    <td height="22" widht="67%"><%= (String)map.get("OPERDESCRIPTION") %>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%" border="0" class="table-style5">
        <tr align="center">
          <td height="22"><input type="button" name="sure" value="<fmt:message key="operDel.yes"/>" class="button-style1" onclick="javascript:onSubmit()"></td>
          <td height="22"><input type="button" name="quit" value="<fmt:message key="operDel.cancel"/>" class="button-style1" onclick="window.close()"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html>
<%
    }
    catch (Exception e) {
%>
<html>
<body>
<table border="1" width="100%" bordercolorlight="#77BEEE" bordercolordark="#ffffff" cellspacing="0" cellpadding="0" class="table-style1">
  <tr>
    <td colspan="2"><fmt:message key="allotrights.erroroccured"/><%= e.toString() %></td>
  </tr>
</table>
</body>
</html>
<%
    }
%>

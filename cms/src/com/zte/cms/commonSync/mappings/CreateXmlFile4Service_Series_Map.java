package com.zte.cms.commonSync.mappings;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;

public class CreateXmlFile4Service_Series_Map extends AbstractCreateXmlFile4Object
{

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {

        String action = null;
        if (0 != targetType)
        {
        return null;
        }
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
    
        Date date = new Date();
        String fileName = "service_series_20wg"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
     
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");
        List<ServiceTargetSyn> serviceprogrammapList = objectMap.get("servicemapping");
        if (null == serviceprogrammapList || 0 == serviceprogrammapList.size())
        {
            return null;
        }
        ServiceTargetSyn mapping = serviceprogrammapList.get(0);
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");    
        
        mapObjectElement.addAttribute("ID", mapping.getObjectid());
        mapObjectElement.addAttribute("Action", action);
        mapObjectElement.addAttribute("ParentType", "Package");
        mapObjectElement.addAttribute("ElementType", "Series");
        mapObjectElement.addAttribute("ParentID", mapping.getServicecode());
        mapObjectElement.addAttribute("ElementID", mapping.getCpcontentid());
        mapObjectElement.addAttribute("ParentCode", mapping.getServicecode());
        mapObjectElement.addAttribute("ElementCode", mapping.getCpcontentid());
        
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", mapping.getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);       
        
        writeDom(dom,fileFullName);      
        return retPath;

    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {

        String action = null;
        if (1 == syncType || 2 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }        
        Date date = new Date();
        String targetTypeName=null;
        if(targetType==1){
            targetTypeName = "bms";
        }else{
            targetTypeName = "epg";
        }
        String fileName = "service_series"+"_"+targetTypeName+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
     
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");
        List<ServiceTargetSyn> serviceprogrammapList = objectMap.get("servicemapping");
        if (null == serviceprogrammapList || 0 == serviceprogrammapList.size())
        {
            return null;
        }
        ServiceTargetSyn mapping = serviceprogrammapList.get(0);
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");    
        
        mapObjectElement.addAttribute("ObjectID", mapping.getObjectid());
        mapObjectElement.addAttribute("Action", action);
        mapObjectElement.addAttribute("ParentType", "Service");
        mapObjectElement.addAttribute("ElementType", "Series");
        mapObjectElement.addAttribute("ParentID", mapping.getParentid());
        mapObjectElement.addAttribute("ElementID", mapping.getElementid());

        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", mapping.getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);       
        
        writeDom(dom,fileFullName);      
        return retPath;
 
    }

    @Override
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {


        String action = null;
        if (10 != targetType)
        {
        return null;
        }
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
    
        Date date = new Date();
        String fileName = "service_series_20ys"+"_"+action.toLowerCase().replace(" ", "")+"_"+date.getTime() + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML + File.separator + getCurrentTime()+ File.separator+"service"+File.separator ;
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = absolutePath + fileName;
     
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element mappingElement = adiElement.addElement("Mappings");
        List<ServiceTargetSyn> serviceprogrammapList = objectMap.get("servicemapping");
        if (null == serviceprogrammapList || 0 == serviceprogrammapList.size())
        {
            return null;
        }
        ServiceTargetSyn mapping = serviceprogrammapList.get(0);
        Element mapObjectElement = null;
        Element mapPertyElement = null;
        mapObjectElement = mappingElement.addElement("Mapping");    
        
        mapObjectElement.addAttribute("ID", mapping.getObjectid());
        mapObjectElement.addAttribute("Action", action);
        mapObjectElement.addAttribute("ParentType", "Package");
        mapObjectElement.addAttribute("ElementType", "Series");
        mapObjectElement.addAttribute("ParentID", mapping.getServicecode());
        mapObjectElement.addAttribute("ElementID", mapping.getCpcontentid());
        mapObjectElement.addAttribute("ParentCode", mapping.getServicecode());
        mapObjectElement.addAttribute("ElementCode", mapping.getCpcontentid());
        
        writeElement(mapObjectElement, mapPertyElement, "Type", "", false);
        writeElement(mapObjectElement, mapPertyElement, "Sequence", mapping.getSequence().toString(), false);
        writeElement(mapObjectElement, mapPertyElement, "ValidStart", "", false);
        writeElement(mapObjectElement, mapPertyElement, "ValidEnd", "", false);       
        
        writeDom(dom,fileFullName);      
        return retPath;

    
    }

}

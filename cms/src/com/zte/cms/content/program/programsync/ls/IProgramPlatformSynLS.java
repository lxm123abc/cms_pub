package com.zte.cms.content.program.programsync.ls;

import java.util.List;
import com.zte.cms.content.program.model.CmsProgram;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface IProgramPlatformSynLS
{
    /**
     * 根据条件分页查询CntTargetsysSyn对象
     * @param ProgramPlatformSyn cntTargetsysSyn对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize) throws Exception;
  
    //点播内容发布到平台
    public String publishProgramPlatformSyn(String programindex, int platform,int synType) throws Exception;
   
    //点播内容批量发布或取消发布
    public String batchPublishOrDelPublish(List<CmsProgram> list,String operType, int type) throws Exception;
    
    //点播内容发布到网元
    public String publishProgramTarget(String programindex, String targetindex,int type,int modifyType) throws Exception;
   
    //预下线
    public String preOfflineProgram(String programindex,int type);

    //点播内容取消发布到网元
    public String delPublishProgramPlatformSyn(String programindex, int type,int synType) throws Exception;

    //点播内容取消发布到网元
    public String delPublishProgramTarget(String programindex, String targetindex,int type) throws Exception;
    
    /**
     * 根据查询ProgramPictureMap对象
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws Exception;
 
    /**
     * 根据查询ProgramPictureMap对象
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize) throws Exception;   
   
    /**
     * 点播内容针对工作流修改发布
     * @param String programindex对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String modifyProgramPlatformSyn(String programindex) throws Exception;    
    
    /**
     * 点播内容海报针对平台发布
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String publishproPicMapPlatformSyn(String mapindex, String programid,String pictureid,int type,int synType) throws Exception;
    /**
     * 点播内容海报针对平台取消发布
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String delPublishproPicMapPlatformSyn(String mapindex, String programid,String pictureid,int type,int synType) throws Exception;
    
    /**
     * 点播内容海报针对网元发布
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String publishproPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;
    
    /**
     * 点播内容海报针对网元取消发布
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String delPublishproPicMapTarget(String mapindex,  String targetindex,int type) throws Exception;
    
    /**
     * 点播内容海报批量发布或取消发布
     * @param ProgramPictureMap programPictureMap对象，作为查询条件的参数
     * @return 查询结果
     * @throws Exception
     */
    public String batchPublishOrDelPublishproPic(List<ProgramPictureMap> list,String operType, int type) throws Exception;
}

package com.zte.cms.cntSyncRecord.ls;

import com.zte.cms.cntSyncRecord.model.CntSyncRecord;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICntSyncRecordLS {
	/**
	 * 显示所有的信息
	 * @param cntSyncRecord
	 * @param start
	 * @param pageSize
	 * @return TableDataInfo
	 */
	public TableDataInfo pageInfoQuery(CntSyncRecord cntSyncRecord, int start, int pageSize);
	
	
	/**
	 * 根据对象查询
	 * @param cntSyncRecord
	 * @return CntSyncRecord
	 */
	public CntSyncRecord getCntSyncRecord(CntSyncRecord cntSyncRecord);
	

}

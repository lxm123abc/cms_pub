package com.zte.cms.avtemplate.service;

import java.util.List;

import com.zte.cms.avtemplate.model.CmsAvtemplateinfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsAvtemplateinfoDS
{
    /**
     * 新增CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;

    /**
     * 更新CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;

    /**
     * 批量更新CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsAvtemplateinfoList(List<CmsAvtemplateinfo> cmsAvtemplateinfoList)
            throws DomainServiceException;

    /**
     * 删除CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;

    /**
     * 批量删除CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsAvtemplateinfoList(List<CmsAvtemplateinfo> cmsAvtemplateinfoList)
            throws DomainServiceException;

    /**
     * 查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @return CmsAvtemplateinfo对象
     * @throws DomainServiceException ds异常
     */
    public CmsAvtemplateinfo getCmsAvtemplateinfo(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;

    /**
     * 根据条件查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象
     * @return 满足条件的CmsAvtemplateinfo对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsAvtemplateinfo> getCmsAvtemplateinfoByCond(CmsAvtemplateinfo cmsAvtemplateinfo)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsAvtemplateinfo对象
     * 
     * @param cmsAvtemplateinfo CmsAvtemplateinfo对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsAvtemplateinfo cmsAvtemplateinfo, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public List<CmsAvtemplateinfo> getTemplateByName(CmsAvtemplateinfo cmsAvtemplateinfo) throws DomainServiceException;
}

package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceChannelMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IServiceChannelMapDS
{
    /**
     * 新增ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException;

    /**
     * 更新ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException;

    /**
     * 批量更新ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateServiceChannelMapList(List<ServiceChannelMap> serviceChannelMapList)
            throws DomainServiceException;

    /**
     * 删除ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException;

    /**
     * 批量删除ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeServiceChannelMapList(List<ServiceChannelMap> serviceChannelMapList)
            throws DomainServiceException;

    /**
     * 查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @return ServiceChannelMap对象
     * @throws DomainServiceException ds异常
     */
    public ServiceChannelMap getServiceChannelMap(ServiceChannelMap serviceChannelMap) throws DomainServiceException;

    /**
     * 根据条件查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象
     * @return 满足条件的ServiceChannelMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<ServiceChannelMap> getServiceChannelMapByCond(ServiceChannelMap serviceChannelMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询ServiceChannelMap对象
     * 
     * @param serviceChannelMap ServiceChannelMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(ServiceChannelMap serviceChannelMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

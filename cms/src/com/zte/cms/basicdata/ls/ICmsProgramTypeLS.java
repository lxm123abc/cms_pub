package com.zte.cms.basicdata.ls;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;

public interface ICmsProgramTypeLS
{

    public CmsProgramtype getCmsProgramtypeById(Long id);

    public TableDataInfo queryCmsProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize);

    public HashMap updateProgramtype(CmsProgramtype cmsProgramtype);

    public HashMap insertProgramtype(CmsProgramtype cmsProgramtype);

    public Integer queryByTypeid(CmsProgramtype cmsProgramtype);

    public Integer queryByTypename(CmsProgramtype cmsProgramtype);

    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype);

    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype);

    public CmsProgramtype getCmsProgramtype(String typeid);

    public Integer isHasChild(String typeid);

    public List<CmsProgramtype> listMenuTreeByParentid(String typeid);

    public CmsProgramtype getCmsprogramtype(Long typeindex);

    public String insertCP(CmsProgramtypecpMap cmsProgramtypecpMap) throws Exception;

    public String getTypeid(Long typeindex) throws Exception;

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex);

    public List getFirstRootProgramtype() throws Exception;

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype);

    public UcpBasic getCPnameByCPid(UcpBasic basic) throws DomainServiceException;

    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype);
    
    /**
     * 操作员关联对象时查询节目分类信息
     * **/
    public List<CmsProgramtype> getCmsProgramtypeForOperByCond(CmsProgramtype cmsProgramtype,String operid);

    public HashMap getProgramtypeMap(CmsProgramtype cmsProgramtype);
    
    /**
     * 删除节目分类信息
     * @param id long 节目分类index 
     * @return String 删除结果
     * **/
    public String removeProgramtype(Long id);

}
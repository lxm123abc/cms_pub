package com.zte.ismp.systemmanage.ls;

public interface ISystemManage
{

    public String getAdconfigxmlContent() throws Exception;

    public String modAdconfigxml(String[] key, String[] value) throws Exception;

}

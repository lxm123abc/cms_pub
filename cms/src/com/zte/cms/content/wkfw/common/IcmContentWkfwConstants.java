package com.zte.cms.content.wkfw.common;

public class IcmContentWkfwConstants
{
    public static final String wkfwObjectName = "CNT";

    public static final String pboType = "CNT_A";
    /**
     * 操作成功
     */
    public static final String Success = "0";
    /**
     * 操作失败
     */
    public static final String Fail = "1";
    /*******************************************************************************************************************
     * 内容同步审核错误码
     * 
     ******************************************************************************************************************/
    public static final String errorCode = "110";
    /*******************************************************************************************************************
     * 内容同步审核错误码
     * 
     ******************************************************************************************************************/
    public static final String contentStausCode = "117";

    /*******************************************************************************************************************
     * 内容同步审核数据库异常
     * 
     ******************************************************************************************************************/
    public static final String DBErroe = "119";
    /*******************************************************************************************************************
     * 内容同步审核工作流异常
     * 
     ******************************************************************************************************************/
    public static final String WorkFlowErroe = "112";
    /**
     * 工作流已经启动
     */
    public static final String wkfw_has_start = "-1";
    /**
     * 工作流拒绝状态
     */
    public static final int wkfw_audit_fail = -1;
    /**
     * 工作流开始状态
     */
    public static final int wkfw_start_status = 0;
    /**
     * 工作流申请状态
     */
    public static final int wkfw_apply_status = 1;
    /**
     * 内容状态：初始
     */
    public static final int content_status_start = 10;
    /**
     * 内容状态：待一审
     */
    public static final int content_status_toaudit = 11;
    /**
     * 内容状态：一审中
     */
    public static final int content_status_onaudit = 12;
    /**
     * 内容状态：二审中
     */
    public static final int content_status_twoaudit = 14;
    /**
     * 内容状态：三审中
     */
    public static final int content_status_threeaudit = 16;
    /**
     * 内容状态：审核中
     */
    public static final int content_status_audit = 17;
    /**
     * 内容状态：待上线
     */
    public static final int content_status_waitup = 20;
    /**
     * 牌照方免审
     */
    public static final int lp_no_audit = 2;
    /**
     * 运营商免审
     */
    public static final int op_no_audit = 3;

    /**
     * 待一审
     */
    public static final int cms_oneaudit_count = 2;

    /**
     * 统计待二审
     */
    public static final int cms_twoaudit_count = 3;
    /**
     * 统计待三审
     */
    public static final int cms_threeaudit_count = 4;

    /**
     * 统计审核不通过
     */
    public static final int cms_auditnotpass_count = 6;

    /**
     * 系统管理员默认返回
     */
    public static final int sys_wfw_count = 0;
}

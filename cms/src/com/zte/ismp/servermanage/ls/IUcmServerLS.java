package com.zte.ismp.servermanage.ls;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.umap.ucms.model.UcmServer;

/**
 * <p>
 * 文件名称: IUcmServerLS.java
 * </p>
 * <p>
 * 文件描述: 服务器管理接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2010
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供服务器的增、删、改、查接口
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2010年08月10日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 */
public interface IUcmServerLS
{

    /**
     * <p>
     * 新增一条服务器记录
     * </p>
     * 
     * @param ucmServer 服务器实体
     * @return 返回操作结果码
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public String insertUcmServer(UcmServer ucmServer) throws Exception;

    /**
     * <p>
     * 分页查询服务器记录
     * </p>
     * 
     * @param ucmServer 服务器信息实体
     * @param start 开始行数
     * @param pageSize 每页行数
     * @return 返回分页记录
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public TableDataInfo pageInfoQuery(UcmServer ucmServer, int start, int pageSize) throws Exception;

    /**
     * <p>
     * 分页查询服务器记录
     * </p>
     * 
     * @param ucmServer 服务器信息实体
     * @param start 开始行数
     * @param pageSize 每页行数
     * @param puEntity 表格中需要排序的字段实体
     * @return 返回分页记录
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public TableDataInfo pageInfoQuery(UcmServer ucmServer, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * <p>
     * 删除一条服务器记录
     * </p>
     * 
     * @param index 服务器主键值
     * @return 返回操作结果码，类型String
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public String deleteUcmServer(Long serverindex) throws Exception;

    /**
     * <p>
     * 根据服务器主键值，查询服务器
     * </p>
     * 
     * @param index 服务器记录主键值
     * @return 服务器信息实体
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public UcmServer getUcmServer(Long serverindex) throws Exception;

    /**
     * <p>
     * 更新一条服务器记录
     * </p>
     * 
     * @param ucmServer 服务器实体
     * @return 成功返回true,失败返回false
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表ucm_server
     * @see com.zte.umap.ucms.model.UcmServer
     */
    public String updateUcmServer(UcmServer ucmServer) throws Exception;

}

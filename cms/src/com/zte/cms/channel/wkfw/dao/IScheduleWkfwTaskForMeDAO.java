package com.zte.cms.channel.wkfw.dao;

import java.util.List;


import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwTaskForMe;

import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IScheduleWkfwTaskForMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ScheduleWkfwTaskForMe> queryScheduleWkfwTaskForMeListByCond(
            ScheduleWkfwCondition scheduleWkfwTaskForMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int queryscheduleWkfwTaskForMeListCntByCond(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize)
            throws DAOException;

    public abstract PageInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
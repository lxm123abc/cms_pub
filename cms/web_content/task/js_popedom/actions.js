function chkall(input1,input2)
{
    var objForm = document.forms["form1"];
    var objLen = objForm.length;
    for (var iCount = 0; iCount < objLen; iCount++)
    {
        if (input2.checked == true)
        {
            if (objForm.elements[iCount].type == "checkbox")
            {
                objForm.elements[iCount].checked = true;
            }
        }
        else
        {
            if (objForm.elements[iCount].type == "checkbox")
            {
                objForm.elements[iCount].checked = false;
            }
        }
    }
}
function turnToPage(){
   var toPage = document.forms["form1"].toPage.value;

   document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?pageNo="+toPage;
   document.forms["form1"].submit();
 }
function turnToPage_previous(){
	var curpage = <%=request.getAttribute("currentPageNo")%>
        var toPage = curpage--
	document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?pageNo="+toPage;
       document.forms["form1"].submit();
}
function turnToPage_next(){
	var curpage = <%=request.getAttribute("currentPageNo")%>
        var toPage = curpage++
	document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?pageNo="+toPage;
       document.forms["form1"].submit();
}
function turnToPage_index(){
	var toPage = 1
	document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?pageNo="+toPage;
        document.forms["form1"].submit();
}
function turnToPage_last(){
	var toPage = document.forms["form1"].pageNoCount.value
	document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?pageNo="+toPage;
       document.forms["form1"].submit();
}
function autorefresh(){
  document.forms["form1"].action = "<%=request.getContextPath()%>/TaskAction.do?refresh=yes;
  document.forms["form1"].submit();
}


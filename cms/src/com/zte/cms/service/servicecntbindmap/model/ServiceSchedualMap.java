package com.zte.cms.service.servicecntbindmap.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ServiceSchedualMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.Long serviceindex;
    private java.lang.String serviceid;
    private java.lang.Long schedualindex;
    private java.lang.String schedualid;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String cancelpubtime;

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("serviceindex", "SERVICEINDEX");
        this.addRelation("serviceid", "SERVICEID");
        this.addRelation("schedualindex", "SCHEDULEINDEX");
        this.addRelation("schedualid", "SCHEDULEID");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getServiceindex()
    {
        return serviceindex;
    }

    public void setServiceindex(java.lang.Long serviceindex)
    {
        this.serviceindex = serviceindex;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.Long getSchedualindex()
    {
        return schedualindex;
    }

    public void setSchedualindex(java.lang.Long schedualindex)
    {
        this.schedualindex = schedualindex;
    }

    public java.lang.String getSchedualid()
    {
        return schedualid;
    }

    public void setSchedualid(java.lang.String schedualid)
    {
        this.schedualid = schedualid;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

}

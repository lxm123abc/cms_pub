package com.zte.cms.egptemplt.ls;

import com.zte.cms.egptemplt.model.Epgtemplt;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;

/**
 * 
 * 
 * @author Administrator
 * 
 */
public interface IEpgtempltLS
{
    /**
     * 
     * @param epgtemplt Epgtemplt
     * @return Epgtemplt
     * @throws DomainServiceException DomainServiceException
     */
    public abstract Epgtemplt getEpgtemplt(Epgtemplt epgtemplt) throws DomainServiceException;

    /**
     * 
     * @param epgtemplt Epgtemplt
     * @param start int
     * @param pageSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 
     * @param epgtemplt Epgtemplt
     * @param start int
     * @param pageSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public abstract TableDataInfo pageInfoQuery(Epgtemplt epgtemplt, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    public abstract TableDataInfo pageInfoQueryforTask(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize)
    throws DomainServiceException;
    
    /**
     * 
     * @param epgtemplt Epgtemplt
     * @param paths String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String newTemplate(Epgtemplt epgtemplt, String[] paths) throws DomainServiceException;

    /**
     * 
     * @param epgtemplt Epgtemplt
     * @param paths String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String renewTemplate(Epgtemplt epgtemplt, String[] paths) throws DomainServiceException;

    /**
     * 
     * @param templateindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String removeTemplate(Long templateindex) throws DomainServiceException;

    /**
     * 
     * @param templateindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkExist(Long templateindex) throws DomainServiceException;

    /**
     * 
     * @param filename String
     * @return String
     * @throws Exception Exception
     */
    public String downloadFromNAS(String filename) throws Exception;

    /**
     * 
     * @param templateindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String canceldeployTemplate(Long templateindex) throws DomainServiceException;
    
    /**
     * 
     * @param templateindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String deployTemplate(Long templateindex) throws DomainServiceException;

    /**
     * 
     * @param templateindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String deployTemplate20(Long templateindex,int typenum) throws DomainServiceException;
    
    /**
     * 
     * @param templtIndex String
     * @return String
     * @throws Exception Exception
     */
    public String templtPublishBatch(String templtIndex) throws Exception;

    /**
     * 
     * @param templtIndex String
     * @return String
     * @throws Exception Exception
     */
    public String templtCancelPublishBatch(String templtIndex) throws Exception;
    public String repeatdeploy(String taskindex) throws Exception;
    
    public TableDataInfo getUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize) throws Exception;
    
    public TableDataInfo getUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize, 
            PageUtilEntity puEntity)throws Exception;
    
    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail( UpstreamEPGTaskDetail upstreamEPGTask )throws Exception; 
}
package com.zte.cms.channel.apply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.ChannelWkfwhis;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.service.IChannelWkfwhisDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.wkfw.ls.IChannelWkfwCheck;
import com.zte.cms.common.ResourceManager;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 
 * @author Administrator
 * 
 */
public class ChannelApply extends WorkflowBase implements IChannelApply
{
    protected IChannelWkfwCheck channelWkfwCheckLS = null;
    protected IChannelWkfwhisDS channelWkfwhisDS;
    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

    private Log log = SSBBus.getLog(getClass());
    private ICmsChannelDS channelds = null;
    private IPhysicalchannelDS physicalchannelDS = null;
    private IUsysConfigDS usysConfigDS = null;
    
    private String callbackProcess;

    
    public String getCallbackProcess()
    {
        return callbackProcess;
    }

    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    // 构造方法
    ChannelApply()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }
    
    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    /**
     * 
     * @return IPhysicalchannelDS
     */
    public IPhysicalchannelDS getPhysicalchannelDS()
    {
        return physicalchannelDS;
    }

    /**
     * 
     * @param physicalchannelDS IPhysicalchannelDS
     */
    public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS)
    {
        this.physicalchannelDS = physicalchannelDS;
    }

    /**
     * 
     * @return ICmsChannelDS
     */
    public ICmsChannelDS getChannelds()
    {
        return channelds;
    }

    /**
     * 
     * @param channelds ICmsChannelDS
     */
    public void setChannelds(ICmsChannelDS channelds)
    {
        this.channelds = channelds;
    }

    /**
     * 
     * @return IChannelWkfwCheck
     */
    public IChannelWkfwCheck getChannelWkfwCheckLS()
    {
        return channelWkfwCheckLS;
    }

    /**
     * 
     * @param channelWkfwCheckLS IChannelWkfwCheck
     */
    public void setChannelWkfwCheckLS(IChannelWkfwCheck channelWkfwCheckLS)
    {
        this.channelWkfwCheckLS = channelWkfwCheckLS;
    }

    /**
     * 
     * @return IChannelWkfwhisDS
     */
    public IChannelWkfwhisDS getChannelWkfwhisDS()
    {
        return channelWkfwhisDS;
    }

    /**
     * 
     * @param channelWkfwhisDS IChannelWkfwhisDS
     */
    public void setChannelWkfwhisDS(IChannelWkfwhisDS channelWkfwhisDS)
    {
        this.channelWkfwhisDS = channelWkfwhisDS;
    }

    /**
     * @param channelWkfwhis ChannelWkfwhis
     * @return ChannelWkfwhis
     * @throws Exception Exception
     */
    // 查询历史表
    public ChannelWkfwhis getChannelWkfwHis(ChannelWkfwhis channelWkfwhis) throws Exception
    {
        log.debug("get ChannelWkfwHis by workflwoindex and Channelindex and nodeid starting....");
        List<ChannelWkfwhis> rsList = null;
        try
        {
            rsList = channelWkfwhisDS.getChannelWkfwhisByCond(channelWkfwhis);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());

        }

        if (rsList != null && rsList.size() > 0)
        {
            log.debug("get channelWkfwHis by workflwoindex and channelindex and nodeid ends");
            return rsList.get(0);
        }
        return null;
    }

    /**
     * @param channelIdx Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String apply(Long channelIdx) throws DomainServiceException
    {

        CmsChannel ch = new CmsChannel();
        ch.setChannelindex(channelIdx);
        ch = channelds.getCmsChannel(ch);
        if (ch != null)
        {
            Physicalchannel ph = new Physicalchannel();
            ph.setChannelid(ch.getChannelid());
            List list = physicalchannelDS.listPhysicalchannelByChannelid(ph);
            if (list != null && list.size() > 0)
            {

            }
            else
            {
                return ResourceMgt.findDefaultText("channel.shouldhave.matterchannel.canaudit");//"1:频道下须有物理频道才能送审";
            }
            String checkResult = channelWkfwCheckLS.checkChannelWkfw(channelIdx);
            if (checkResult.charAt(0) != '0')
            {
                return checkResult;
            }
         // 查看是否免审
            int flag = 0;
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            usysConfig.setCfgkey("cms.channel.noaudit.switch");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                flag = Integer.parseInt(usysConfig.getCfgvalue());
            }           
            if(flag==0){
                    // 免审
                ch.setStatus(0);       // 审核通过的
                ch.setWorkflow(0);
                ch.setWorkflowlife(0);
                channelds.updateCmsChannel(ch);
                for(int kk =0;kk<list.size();kk++){
                    Physicalchannel phy = (Physicalchannel)list.get(kk);
                    phy.setStatus(0);
                    phy.setWorkflow(0);
                    phy.setWorkflowlife(0);
                    physicalchannelDS.updatePhysicalchannel(phy);
                }
                return ResourceMgt.findDefaultText("this.channel.noaudit.set");
                
            }
            CmsChannel channel = new CmsChannel();
            channel.setChannelindex(channelIdx);
            channel = channelds.getCmsChannel(channel);
            
            return apply(channel, "1", null);
        }
        else
        {
            return ResourceMgt.findDefaultText("this.channel.not.exist");//"1:此频道已不存在";
        }
    }

    /**
     * @param channel CmsChannel
     * @param templateRoute String
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String apply(CmsChannel channel, String templateRoute, String priority) throws DomainServiceException
    {

        channel.setStatus(ChannelConstants.WKFW_STATUS_AUDITING); // 120是审核中
        channel.setWorkflow(1); // 新增
        channel.setWorkflowlife(0);

        // 插入正式表，判断是否已经存在
        try
        {
            channelds.updateCmsChannel(channel);
            Physicalchannel physicalchannel = new Physicalchannel();
            physicalchannel.setChannelid(channel.getChannelid());
            List list = physicalchannelDS.listPhysicalchannelByChannelid(physicalchannel);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Physicalchannel physical = (Physicalchannel) list.get(i);
                    physical.setStatus(channel.getStatus()); // 状态改成审核失败
                    physical.setWorkflow(1); // 启动
                    physical.setWorkflowlife(0); // 未启动
                    physical.setEffecttime(format.format(new Date())); // 将生效时间字段设为当前时间
                    physicalchannelDS.updatePhysicalchannel(physical);

                }
            }
        }
        catch (DomainServiceException e)
        {
            throw new DomainServiceException(e.getMessage());
        }
        // 发起工作流参数
        String pboName = channel.getChannelname() + "[" + channel.getChannelid() + "]";
        String pboId = String.valueOf(channel.getChannelindex());
        String pboType = "CMS_CHANNEL_A"; // ChannelConstants.getWorkflowType(channel.getWorkflow());
        String processName = pboType + ":" + pboName;

        String arg6 = "step1," + templateRoute; // 工作流模板路由选择

        String callbackBean = "";
        if (null != this.getCallbackProcess())
        {
            callbackBean = "callbackProcess," + this.getCallbackProcess();
            arg6 = arg6 + "|" + callbackBean; // 回调bean

        }

        WorkflowService wkflService = new WorkflowService();
        String tempcode = this.getTemplateCode();
        int flag = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType, "",
                arg6, priority);
        if (flag <= 0)
        {
            throw new DomainServiceException();
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_ADD_APPLY_SUCCESS);
        // 频道新增审核提交成功
    }

    /**
     * @param channel CmsChannel
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String audit(CmsChannel channel, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException
    {

        String checkResult = channelWkfwCheckLS.checkChannelWkfwAudit(channel.getChannelindex());
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }

        // 修改正式表
        if (channel != null && channel.getChannelid() != null)
        {
            channelds.updateCmsChannel(channel);
        }

        // 工作流处理
        WorkflowService wkflService = new WorkflowService();
        wkflService.completeTask(taskId, handleMsg, handleEvent);
        
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_ADD_AUDIT_SUCCESS);
        
//        if (workflowStatusCode.equals("1"))
//        {// 审核
//            return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_ADD_AUDIT_SUCCESS);
//            // 频道新增审核成功
//        }
//        else
//        {
//            return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_WKFW_ADD_AUDIT_SUCCESS);
//            // 频道新增审核成功
//        }
    }

}
package com.zte.cms.content.batchUpload.service;

import java.util.List;
import com.zte.cms.content.batchUpload.model.IcmContentExcelimport;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmContentExcelimportDS
{
    /**
     * 新增IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @throws DomainServiceException ds异常
     */
    public void insertIcmContentExcelimport(IcmContentExcelimport icmContentExcelimport) throws DomainServiceException;

    /**
     * 根据条件查询IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象
     * @return 满足条件的IcmContentExcelimport对象集
     * @throws DomainServiceException ds异常
     */
    public List<IcmContentExcelimport> getIcmContentExcelimportByCond(IcmContentExcelimport icmContentExcelimport)
            throws DomainServiceException;

    /**
     * 根据条件分页查询IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询IcmContentExcelimport对象
     * 
     * @param icmContentExcelimport IcmContentExcelimport对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmContentExcelimport icmContentExcelimport, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}
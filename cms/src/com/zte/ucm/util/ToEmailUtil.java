package com.zte.ucm.util;


public class ToEmailUtil implements Runnable{

    private  String toMail;
    private  String title;
    private  String content;
    private  String emailInfo;


    /**
     *
     * @param toMail    收件人的邮箱地址，用“,”连接
     * @param title     邮件标题
     * @param content   邮件的内容
     * @param emailInfo 发件人的信息
     */
    public ToEmailUtil(String toMail, String title, String content, String emailInfo){
        this.toMail = toMail;
        this.title = title;
        this.content = content;
        this.emailInfo = emailInfo;
    }


    /**
     *
     * 邮件线程实现，把所有的邮件都放进来，不影响页面的返回，让邮件在后台发送
     * 由于发送频率可能会有限制，故设置为每发5封休息5秒
     */
    @Override
    public void run() {
        String[] toMails =toMail.split(",");
        for(int i=0;i<toMails.length;i++){
            if(i % 5 == 0&& i != 0){
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            EmailServiceUtil.send(title, content, toMails[i], emailInfo);
        }
    }



    public static void main(String[] args) {
        System.out.println("测试邮件线程");
        ToEmailUtil toEmailUtil = new ToEmailUtil("18301494080@139.com,505941855@qq.com,","test","超出,License个数预设{10},当前{",
                "smtp.qq.com;25;505941855@qq.com;thlmcuwclnticade;505941855@qq.com");
        Thread thread = new Thread(toEmailUtil);
        thread.start();
        for(int i = 0 ;i<20;i++){
            System.out.println("---------------"+ i);
        }
    }
}

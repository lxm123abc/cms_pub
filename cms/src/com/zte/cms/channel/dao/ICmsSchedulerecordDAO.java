package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsSchedulerecordDAO
{
    /**
     * 新增CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DAOException dao异常
     */
    public void insertCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据主键更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DAOException dao异常
     */
    public void updateCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据条件更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据主键删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DAOException dao异常
     */
    public void deleteCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据条件删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据主键查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return 满足条件的CmsSchedulerecord对象
     * @throws DAOException dao异常
     */
    public CmsSchedulerecord getCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据条件查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return 满足条件的CmsSchedulerecord对象集
     * @throws DAOException dao异常
     */
    public List<CmsSchedulerecord> getCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

    /**
     * 根据条件分页查询CmsSchedulerecord对象，作为查询条件的参数
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsSchedulerecord对象，作为查询条件的参数
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;
    //根据频道查询录制计划
    
    public PageInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize) throws DAOException;
    
    /**
     * 根据条件查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return 满足条件的CmsSchedulerecord对象集
     * @throws DAOException dao异常
     */
    public List<CmsSchedulerecord> getCmsSchedulerecordByID(CmsSchedulerecord cmsSchedulerecord) throws DAOException;

}
package com.zte.ucm.updateconf;

import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;
import com.zte.ucm.util.FileUtil;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class RecoverConfigFiles
{
  private static Logger log = LoggerFactory.getLogger(RecoverConfigFiles.class);

  public void exe() throws Exception
  {
    String str1 = BackupConfigFiles.bakupdir;
    Map localMap = BackupConfigFiles.fileMap;
    log.info("Upgrade failure, start to recover the config files...");

    Set localSet = localMap.keySet();
    Iterator localIterator = localSet.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      String str3 = (String)localMap.get(str2);
      FileUtil localFileUtil = new FileUtil();
      boolean bool = localFileUtil.copyFile(str1 + str3, str2);
      if (bool)
        log.info("Recover success:" + str1 + str3 + "-->" + str2);
      else {
        log.error("Recover failure:" + str1 + str3 + "-->" + str2);
      }
    }
    log.info("Recover completed success!");
  }
}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.IServicePictureMapDAO;
import com.zte.cms.picture.model.ServicePictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServicePictureMapDAO extends DynamicObjectBaseDao implements IServicePictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException
    {
        log.debug("insert servicePictureMap starting...");
        super.insert("insertServicePictureMap", servicePictureMap);
        log.debug("insert servicePictureMap end");
    }

    public void updateServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException
    {
        log.debug("update servicePictureMap by pk starting...");
        super.update("updateServicePictureMap", servicePictureMap);
        log.debug("update servicePictureMap by pk end");
    }

    public void deleteServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException
    {
        log.debug("delete servicePictureMap by pk starting...");
        super.delete("deleteServicePictureMap", servicePictureMap);
        log.debug("delete servicePictureMap by pk end");
    }

    public ServicePictureMap getServicePictureMap(ServicePictureMap servicePictureMap) throws DAOException
    {
        log.debug("query servicePictureMap starting...");
        ServicePictureMap resultObj = (ServicePictureMap) super.queryForObject("getServicePictureMap",
                servicePictureMap);
        log.debug("query servicePictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServicePictureMap> getServicePictureMapByCond(ServicePictureMap servicePictureMap) throws DAOException
    {
        log.debug("query servicePictureMap by condition starting...");
        List<ServicePictureMap> rList = (List<ServicePictureMap>) super.queryForList(
                "queryServicePictureMapListByCond", servicePictureMap);
        log.debug("query servicePictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query servicePictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServicePictureMapListCntByCond", servicePictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServicePictureMap> rsList = (List<ServicePictureMap>) super.pageQuery(
                    "queryServicePictureMapListByCond", servicePictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query servicePictureMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServicePictureMap servicePictureMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServicePictureMapListByCond", "queryServicePictureMapListCntByCond",
                servicePictureMap, start, pageSize, puEntity);
    }

}
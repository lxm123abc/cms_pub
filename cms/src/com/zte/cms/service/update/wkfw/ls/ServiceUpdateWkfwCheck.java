package com.zte.cms.service.update.wkfw.ls;

import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

public class ServiceUpdateWkfwCheck implements IServiceUpdateWkfwCheck
{

    private IServiceDS serviceds = null;
    private IServiceBufDS serviceBufds = null;

    public void setServiceBufds(IServiceBufDS serviceBufds)
    {
        this.serviceBufds = serviceBufds;
    }

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    // 构造方法
    ServiceUpdateWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
    }

    public String checkServiceUpdateWkfw(Long serviceindex) throws DomainServiceException
    {
        Service service = new Service();
        service.setServiceindex(serviceindex);
        service = serviceds.getService(service);

        if (service == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_NO_EXISTS); // 服务不存在
        }
        // 状态不为待发布0，新增同步成功20 修改同步成功50 修改同步失败60 取消同步成功80
        if ((service.getStatus() != 0) && (service.getStatus() != 20) && (service.getStatus() != 50)
                && (service.getStatus() != 60) && (service.getStatus() != 80))
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_STATUS_WRONG); // 服务状态不正确
        }
        // 已经发起工作流
        if (service.getWorkflow() != 0)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_IN_ALREADY);// 已发起工作流未结束
        }
        if (service.getWorkflowlife() != 0)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);// 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    public String checkServiceUpdateWkfwAudit(Long serviceIndex) throws DomainServiceException
    {
        ServiceBuf servicebuf = new ServiceBuf();
        servicebuf.setServiceindex(serviceIndex);
        servicebuf = serviceBufds.getServiceBuf(servicebuf);

        if (servicebuf == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_BUF_NO_EXISTS);// buf表中数据不存在
        }
        if (servicebuf.getWorkflow() != 2)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_NOT_IN_WKFW_OR_WRONG_WKFW); // 服务不在工作流中或所处工作流不正确
        }
        if (servicebuf.getWorkflowlife() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);// 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

package com.zte.cms.cpContractManagement.dao;

import com.zte.cms.cpContractManagement.model.ContractEmailSendLog;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;

import java.util.List;

public interface IContractMapDao {

    //查询
    PageInfo pageInfoQuery(ContractMap contractMap, int start, int pageSize) throws DAOException;

    //添加合同
    void insertContractMap(ContractMap contractMap);

    //修改合同
    void updateByContractId(ContractMap contractMap);

    //查询以有合同
    Integer queryContractByContractId(ContractMap contractMap);

    //查询以有合同
    void deleteConstractByIds(String[] ids);

    //查询合同ById
    ContractMap selectContractById(Integer id) throws DAOException;


    //根据id更改合同信息
    ContractMap updateContractById(ContractMap contractMap)throws DAOException;

    //查询附件byId
    List<ContractMap> getAttachListById(ContractMap contractMap)throws DAOException;

    //查询附件地址by附件id
    ContractMap getAttachInfoById(String attachid)throws DAOException;

    //通过合同号，查询对应的合同
    ContractMap getContractMapByContractMap(ContractMap contractMap) throws DAOException;


    /**
     *  通过过期天数查询数据
     *
     * @param day
     * @return
     * @throws DAOException
     */
    List<ContractMap> queryWillOver(int day) throws DAOException;
    Integer queryContractSendEmailLogByOidAndGrade(ContractEmailSendLog contractEmailSendLog) throws DAOException;
    void insertContractSendEmailLog(ContractEmailSendLog record);
}

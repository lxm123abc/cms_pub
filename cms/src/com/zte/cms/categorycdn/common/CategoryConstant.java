package com.zte.cms.categorycdn.common;
/**
 * 日志常量类
 * @author Administrator
 *
 */
public class CategoryConstant {
	public static final String MGTTYPE_CATEGORY = "log.category.manage";
	public static final String OPERTYPE_CATEGORY_ADD = "log.category.add";
	public static final String OPERTYPE_CATEGORY_ADD_INFO = "log.category.add.info";
	public static final String OPERTYPE_CATEGORY_UPDATE = "log.category.update";
	public static final String OPERTYPE_CATEGORY_UPDATE_INFO = "log.category.update.info";
	public static final String OPERTYPE_CATEGORY_DELETE = "log.category.delete";
	public static final String OPERTYPE_CATEGORY_DELETE_INFO = "log.category.delete.info";
	public static final String OPERTYPE_CATEGORY_PUBLISH = "log.category.publish";
	public static final String OPERTYPE_CATEGORY_PUBLISH_INFO = "log.category.publish.info";
	public static final String OPERTYPE_CATEGORY_UNPUBLISH = "log.category.unpublish";
	public static final String OPERTYPE_CATEGORY_UNPUBLISH_INFO = "log.category.unpublish.info";	
	public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
	public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
}

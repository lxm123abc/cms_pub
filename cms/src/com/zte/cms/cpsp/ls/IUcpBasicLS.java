package com.zte.cms.cpsp.ls;

import java.util.HashMap;
import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.cms.cpsp.model.CmsCpbind;
import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.model.UcpContact;

public interface IUcpBasicLS {

    /**
     * 申请CP
     *
     * @param ucpContactList 联系人信息集合
     * @param ucpBasic       CP基本信息
     * @return 操作结果
     */
    public String saveUcpBasic(String uploadResFileCtrl, List<UcpContact> ucpContactList, UcpBasicModel ucpBasic,
                               List<CmsProgramtypecpMap> programtypecpMap) throws Exception;

    /**
     * 修改CP信息
     *
     * @param ucpContactList 联系人信息集合
     * @param model          CP基本信息
     * @return 操作结果
     */
    public String updateUcpBasic(String uploadResFileCtrl, List<UcpContact> ucpContactList, UcpBasicModel model,
                                 List<CmsProgramtypecpMap> programtypecpMap) throws Exception;


    /**
     * 获取省份Map
     *
     * @return 省份Map
     */
    public HashMap getUsysProvinceMap();

    /**
     * 根据cpindex删除CP
     *
     * @param cpindex CP主键
     * @return 操作结果
     * @throws Exception
     */
    public String deleteUcpBasic(String cpindex) throws Exception;

    /**
     * CP绑定审核牌照方
     *
     * @param cmsCpbindList Cp绑定审核牌照方关联对象
     * @param cpid          需要绑定的CPID
     * @return 操作结果
     * @throws Exception
     */
    public String saveCmsCpbindByCond(List<CmsCpbind> cmsCpbindList, String cpid) throws Exception;

    /**
     * 通过FTP infoindex 验证FTP是否有CP绑定
     *
     * @param infoindex
     * @return 验证结果
     */
    public String validFtp(String infoindex);

    /**
     * CP同步
     *
     * @param cpindex CP主键
     * @param opertype 同步类型
     * @return 操作结果
     */
    // public String saveSyncTack(String cpindex,String opertype);

    /**
     * 从会话中获取CP操作员ID
     *
     * @return 返回CP操作员ID
     * @throws Exception
     */
    public String getCPIDFromSession() throws Exception;

    /**
     * 查询CP信息
     *
     * @param ucpBasic 查询条件对象
     * @param arg1     开始页数
     * @param arg2     条数
     * @return CP信息
     */
    public TableDataInfo pageInfoQuery(UcpBasic ucpBasic, int arg1, int arg2);

    /**
     * 通过CPID和类型得到CP或者牌照方
     *
     * @param cpid
     * @param cptype 类型
     * @return CP或者牌照方
     */
    public UcpBasic getUcpBasicByCPIDAndCptype(String cpid, int cptype);

    /**
     * 获取cp关联的操作员
     */
    public String validOper(String cpindex);

    /**
     * 暂停和恢复功能
     */
    public String pauseAndRework(String cpindex, String opertype);

    /**
     * 根据CPID查询该CP支持的内容类型
     */
    public HashMap getProgramTypeByCPID(String cpid, Boolean needSelect);

    /**
     * 查询CP可以绑定的牌照方,要求为和cp区域相同的或是区域为"全国"的牌照方
     */
    public TableDataInfo pageInfoQueryByProv(UcpBasic ucpBasic, int arg1, int arg2) throws Exception;

    /**
     * 查询CP的CPID和牌照方的CPID相同的CP信息
     *
     * @param ucpBasic 查询条件对象
     * @param start    开始页数
     * @param pageSize 条数
     * @return CP信息
     * @throws Exception
     */
    public TableDataInfo queryUcpbasicBySameCPID(UcpBasic ucpBasic, int start, int pageSize) throws Exception;

    @SuppressWarnings("unchecked")
    public HashMap getUsysCpBash(String cpid) throws DomainServiceException;


    UcpBasicModel getUcpBasic(UcpBasicModel model);

    /**
     *  删除PC绑定文件名 实际update字段
     * @param name
     * @param cpindex
     * @return
     */
    UcpBasicModel deleteUcpBasicFile(String name, String cpindex);

    /**
     *  获取当前登录人是pc还是管理员
     * @return
     */
    String getOperIsPcOrmanager();

    /**
     *  首页校验当前登录人(cp)是否登记必须资质
     * @return
     */
    String checkQualificationDoc();

    /**
     *  修改资质字段By index
     * @param uploadResFileCtrl
     * @param ucpBasic
     * @return
     * @throws DAOException
     * @throws DomainServiceException
     */
    String indexUpdate(String uploadResFileCtrl,UcpBasicModel ucpBasic) throws DAOException, DomainServiceException;
}

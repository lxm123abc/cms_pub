package com.zte.cms.operlimit.dao;

import java.util.List;


import com.zte.cms.operlimit.model.OperRole;
import com.zte.cms.operlimit.model.OperatorLimit;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class OperRoleDAO extends DynamicObjectBaseDao implements IOperRoleDAO {

	private Log log = SSBBus.getLog(getClass());

	@Override
	public List getOperRole(OperRole operrole) {
        
        List<OperRole> rsList = (List<OperRole>) super.queryForList("getOperRole", operrole);


		return rsList;

	}

	@Override
	public void insertOperRole(OperatorLimit OperRole) throws DAOException {
		log.debug("insert operRole starting...");

		super.insert("insertOperRole", OperRole);
		log.debug("insert operRole end");

	}
    @Override
	public void deleteOperRole(OperatorLimit OperRole) throws DAOException {
		log.debug("delete OperRole by pk starting...");
        super.delete("deleteOperRole", OperRole);
        log.debug("delete OperRole by pk end");
		
	}
    
    public List getFuncById(OperatorLimit OperRole){

    	List<OperatorLimit> rsList = (List) super.queryForList("getFuncById", OperRole);
		return rsList;
    }

}
package com.zte.cms.content.batchUpload.dao;

import java.util.List;

import com.zte.cms.content.batchUpload.dao.IIcmExcelimportFailDAO;
import com.zte.cms.content.batchUpload.model.IcmExcelimportFail;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmExcelimportFailDAO extends DynamicObjectBaseDao implements IIcmExcelimportFailDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmExcelimportFail(IcmExcelimportFail icmExcelimportFail) throws DAOException
    {
        log.debug("insert icmExcelimportFail starting...");
        super.insert("insertIcmExcelimportFail", icmExcelimportFail);
        log.debug("insert icmExcelimportFail end");
    }

    public void deleteIcmExcelimportFailByCond(IcmExcelimportFail icmExcelimportFail) throws DAOException
    {
        log.debug("delete deleteIcmExcelimportFailByCond starting...");
        super.delete("deleteIcmExcelimportFailByCond", icmExcelimportFail);
        log.debug("delete deleteIcmExcelimportFailByCond end");
    }

    @SuppressWarnings("unchecked")
    public List<IcmExcelimportFail> getIcmExcelimportFailByCond(IcmExcelimportFail icmExcelimportFail)
            throws DAOException
    {
        log.debug("query icmExcelimportFail by condition starting...");
        List<IcmExcelimportFail> rList = (List<IcmExcelimportFail>) super.queryForList(
                "queryIcmExcelimportFailListByCond", icmExcelimportFail);
        log.debug("query icmExcelimportFail by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmExcelimportFail by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmExcelimportFailListCntByCond", icmExcelimportFail))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmExcelimportFail> rsList = (List<IcmExcelimportFail>) super.pageQuery(
                    "queryIcmExcelimportFailListByCond", icmExcelimportFail, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmExcelimportFail by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmExcelimportFail icmExcelimportFail, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryIcmExcelimportFailListByCond", "queryIcmExcelimportFailListCntByCond",
                icmExcelimportFail, start, pageSize, puEntity);
    }

}
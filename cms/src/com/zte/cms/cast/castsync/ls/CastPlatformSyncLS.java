package com.zte.cms.cast.castsync.ls;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.cms.cast.castsync.service.ICastPlatformSyncDS;
import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.cast.model.CastcdnConstant;
import com.zte.cms.cast.service.ICastcdnDS;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.CntConstants;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.content.util.CntUtils;
import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.model.CastPictureMap;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.service.ICastPictureMapDS;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CastPlatformSyncLS extends DynamicObjectBaseDS implements ICastPlatformSyncLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private ICastPlatformSyncDS castPlatformSyncDS;
    private ICastrolemapDS castrolemapDS;
    private ICastcdnDS castds;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private ITargetsystemDS targetSystemDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private ICntSyncTaskDS cntsyncTaskDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private ICastPictureMapDS castPictureMapDS;
    private IPictureDS pictureDS;
    
    private Map map = new HashMap<String, List>();
    public final static String CNTSYNCXML = "xmlsync";
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String MEDIA_ID = "2"; // 在线区ID
    public static final String TEMPAREA_ERRORSIGN = "1056020"; // 获取临时区目录失败
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    
    public void setCastds(ICastcdnDS castds)
    {
        this.castds = castds;
    }

    public void setPictureDS(IPictureDS pictureDS)
    {
        this.pictureDS = pictureDS;
    }

    public void setCastPictureMapDS(ICastPictureMapDS castPictureMapDS)
    {
        this.castPictureMapDS = castPictureMapDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }
    
    public void setCastrolemapDS(ICastrolemapDS castrolemapDS)
    {
        this.castrolemapDS = castrolemapDS;
    }

    public String batchPublishCastToPlatform(List<Castcdn> castList, int platform ,int syncType) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCastindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < castList.size(); i++)  
            {
                totalCastindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
                
                Castcdn castObj = new Castcdn();
                castObj.setCastindex(castList.get(i).getCastindex());
                castObj = castds.getCastcdn(castObj);
                if (castObj == null ){
                   proStatusStr = ResourceMgt.findDefaultText("cast.character.notexist");
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), castList.get(i).getCastid(), 
                           result.substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }else{
                    operdesc = publishCastToPlatform(castList.get(i).getCastindex(),platform,syncType);  
                    if (operdesc.substring(0, 1).equals("0"))  
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), castObj.getCastid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), castObj.getCastid(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("CastPlatformSyncLS exception:" + e.getMessage());
        }
        if ((successCount) == totalCastindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCastindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
        
    }
    
    /**
     * 人物发布到平台
     *
     * @param castindex 人物index 
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCastToPlatform(Long castindex, int platform ,int syncType) throws Exception
    {
        log.debug("publishCastToPlatform start ");
        List<Castcdn> castList = new ArrayList<Castcdn>();
        Castcdn cast = new Castcdn();
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        Long batchid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id");
        
        try{
            cast.setCastindex(castindex);
            cast = castds.getCastcdn(cast);
            if(cast == null){
                return "1:人物不存在";
            }else{
                castList.add(cast);
                map.put("castList", castList);
            }
            
            Long relateindex = null;
            CntPlatformSync castPlatform = new CntPlatformSync();
            castPlatform.setObjecttype(11);
            castPlatform.setObjectid(cast.getCastid());
            castPlatform.setPlatform(platform);
            List<CntPlatformSync> castPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(castPlatform);
            if(castPlatformList != null && castPlatformList.size() > 0){
                int castSyncStatus = castPlatformList.get(0).getStatus();
                relateindex = castPlatformList.get(0).getSyncindex();
                if(syncType == 1){ //新增同步
                    //0：待发布，400：发布失败
                    if(castSyncStatus != 0 && castSyncStatus!= 400){
                        return "1:人物发布状态不正确";
                    }
                }else{
                    if(castSyncStatus != 300 && castSyncStatus!= 500 && castSyncStatus!= 400 ){
                        return "1:人物发布状态不正确";
                    }
                }
            }else
            {
                return "1:发布记录不存在";
            }
            
            if(syncType ==3){
                CntTargetSync othTarget = new CntTargetSync();
                CastPictureMap castPictureMap = new CastPictureMap();
                castPictureMap.setCastindex(castindex);
                List<CastPictureMap> castPictureMapList = castPictureMapDS.getCastPictureMapByCond(castPictureMap);
                if(castPictureMapList != null && castPictureMapList.size()>0)
                {
                    for(CastPictureMap map:castPictureMapList){
                        castPlatform.setObjecttype(41);
                        castPlatform.setObjectid(map.getMappingid());
                        castPlatform.setPlatform(platform);
                        List<CntPlatformSync> cpPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(castPlatform);
                        if(cpPlatformList != null && cpPlatformList.size() > 0 )
                        {
                            othTarget.setRelateindex(cpPlatformList.get(0).getSyncindex());
                            List<CntTargetSync> otherTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                            if(otherTargetList != null && otherTargetList.size() > 0 )
                            {
                                for(CntTargetSync otherTargetSync:otherTargetList){
                                    if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                        return "1:该人物关联的有已发布的海报，请先取消发布海报";
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
                Castrolemap castrolemap = new Castrolemap();
                castrolemap.setCastindex(castindex);
                List<Castrolemap> castrolemapList = castrolemapDS.getCastrolemapByCond(castrolemap);
                if(castrolemapList != null && castrolemapList.size()>0){
                    for(Castrolemap crm:castrolemapList){
                        castPlatform.setObjecttype(12);
                        castPlatform.setObjectid(crm.getCastrolemapid());
                        castPlatform.setPlatform(platform);
                        List<CntPlatformSync> crmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(castPlatform);
                        if(crmPlatformList != null && crmPlatformList.size() > 0 )
                        {
                            othTarget.setRelateindex(crmPlatformList.get(0).getSyncindex());
                            List<CntTargetSync> crmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(othTarget);
                            if(crmTargetList != null && crmTargetList.size() > 0 )
                            {
                                for(CntTargetSync otherTargetSync:crmTargetList)
                                {
                                    if(otherTargetSync.getStatus() == 300 || otherTargetSync.getStatus()== 500 || otherTargetSync.getStatus()== 200){
                                        return "1:该人物关联的有已发布的角色，请先取消发布角色";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
            
            CntTargetSync target = new CntTargetSync();
            target.setRelateindex(relateindex);
            List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            int[] castTargetStatus = new int[castTargetList.size()];
            if(castTargetList != null && castTargetList.size() > 0 )
            {
                for(CntTargetSync castTargetSync:castTargetList)
                { 
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                    String wg18xml = "";
                    String castxml = "";
                    int operresult = 0;
                    int targetStatus = 0;
                    Targetsystem tagertsystem = new Targetsystem();
                    tagertsystem.setTargetindex(castTargetSync.getTargetindex());
                    tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
                    if(tagertsystem.getStatus()==1 || tagertsystem.getStatus()==2)
                    {
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"状态为暂停或者已注销  ");
                        continue;
                    }
                    
                    if((syncType == 1 && castTargetSync.getStatus() == 0) || (syncType == 1 && castTargetSync.getStatus() == 400))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 11, 1);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());
                        }else if(platform ==2) //3.0
                        {
                            castxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 11, 1);
                            if(castxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castxml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());
                        }
                        
                        //更新网元状态
                        operresult = 10;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        castTargetSync.setStatus(targetStatus);
                        castTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(castTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(cast.getCastid(), CastcdnConstant.MGTTYPE_CAST_SYNC,CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH,
                                CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                        
                    }else if(syncType == 1 && castTargetSync.getStatus() == 500)
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 11, 2);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());

                        }else if(platform ==2) //3.0
                        {
                            castxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 11, 2);
                            if(castxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castxml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());
                        }
                        
                        //更新网元状态
                        operresult = 40;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        castTargetSync.setStatus(targetStatus);
                        castTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(castTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(cast.getCastid(), CastcdnConstant.MGTTYPE_CAST_SYNC,CastcdnConstant.OPERTYPE_CAST_PUBLISH,
                                CastcdnConstant.OPERTYPE_CAST_PUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                    else if((syncType == 3 && castTargetSync.getStatus() == 300) || (syncType == 3 && castTargetSync.getStatus() == 500))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 11, 3);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());

                        }else if(platform ==2) //3.0
                        {
                            castxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 11, 3);
                            if(castxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castxml,correlateid,castTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castindex,cast.getCastid(),cast.getCastcode(),11,
                                    "","","","",syncType,castTargetSync.getTargetindex());
                        }
                        
                        //更新网元状态
                        operresult = 70;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        castTargetSync.setStatus(targetStatus);
                        castTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(castTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(cast.getCastid(), CastcdnConstant.MGTTYPE_CAST_SYNC,CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH,
                                CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                    else if(syncType == 3 && castTargetSync.getStatus() == 400)
                    {
                        //更新网元状态
                        castTargetSync.setStatus(0);
                        castTargetSync.setOperresult(80);
                        cntTargetSyncDS.updateCntTargetSync(castTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(cast.getCastid(), CastcdnConstant.MGTTYPE_CAST_SYNC,CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH,
                                CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                }
                for(int i = 0; i< castTargetList.size(); i++){
                    castTargetStatus[i] = castTargetList.get(i).getStatus();
                }
                //更新平台状态
                int platformStatus = StatusTranslator.getPlatformSyncStatus(castTargetStatus);
                castPlatform = castPlatformList.get(0);
                castPlatform.setStatus(platformStatus);
                cntPlatformSyncDS.updateCntPlatformSync(castPlatform);
            }
            
        }
        catch (Exception e)
        {
            log.error("CastPlatformSyncLS exception:" + e.getMessage());
            return "1:操作失败";
        }
        
        log.debug("publishCastToPlatform end ");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }
    
    public TableDataInfo pageInfoQuery(CastPlatformSync castPlatformSync, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
     
            // 格式化内容带有时间的字段
            castPlatformSync.setCreateStarttime(DateUtil2.get14Time(castPlatformSync.getCreateStarttime()));
            castPlatformSync.setCreateEndtime(DateUtil2.get14Time(castPlatformSync.getCreateEndtime()));
            castPlatformSync.setOnlinetimeStartDate(DateUtil2.get14Time(castPlatformSync.getOnlinetimeStartDate()));
            castPlatformSync.setOnlinetimeEndDate(DateUtil2.get14Time(castPlatformSync.getOnlinetimeEndDate()));
            castPlatformSync.setCntofflinestarttime(DateUtil2.get14Time(castPlatformSync.getCntofflinestarttime()));
            castPlatformSync.setCntofflineendtime(DateUtil2.get14Time(castPlatformSync.getCntofflineendtime()));
            
            if(castPlatformSync.getCastid()!=null&&!"".equals(castPlatformSync.getCastid())){
                castPlatformSync.setCastid(EspecialCharMgt.conversion(castPlatformSync.getCastid()));
            }
            if(castPlatformSync.getCastname()!=null&&!"".equals(castPlatformSync.getCastname())){
                castPlatformSync.setCastname(EspecialCharMgt.conversion(castPlatformSync.getCastname()));
            }
            dataInfo = castPlatformSyncDS.pageInfoQuery(castPlatformSync, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return dataInfo;
    }
    
    public TableDataInfo pageInfoQueryCastPicMap(CastPictureMap castPictureMap, int start, int pageSize) throws Exception
    {
        TableDataInfo dataInfo = null;
        try
        {
            log.debug("pageInfoQuery starting...");
            
            castPictureMap.setCreateStarttime(DateUtil2.get14Time(castPictureMap.getCreateStarttime()));
            castPictureMap.setCreateEndtime(DateUtil2.get14Time(castPictureMap.getCreateEndtime()));
            castPictureMap.setOnlinetimeStartDate(DateUtil2.get14Time(castPictureMap.getOnlinetimeStartDate()));
            castPictureMap.setOnlinetimeEndDate(DateUtil2.get14Time(castPictureMap.getOnlinetimeEndDate()));
            castPictureMap.setCntofflinestarttime(DateUtil2.get14Time(castPictureMap.getCntofflinestarttime()));
            castPictureMap.setCntofflineendtime(DateUtil2.get14Time(castPictureMap.getCntofflineendtime()));
            
            if(castPictureMap.getParentid()!=null&&!"".equals(castPictureMap.getParentid())){
                castPictureMap.setParentid(EspecialCharMgt.conversion(castPictureMap.getParentid()));
            }           
                       
            dataInfo = castPictureMapDS.pageInfoQueryCastPicMap(castPictureMap, start, pageSize);
        }
        catch (Exception e)
        {
            throw e;
        }
        return dataInfo;
    } 
    
    public String batchPublishCastPicMapToPlatform(List<CastPictureMap> castPictureMapList, int platform ,int syncType) throws Exception
    {
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalCastindex = 0;
        int failCount = 0;
        int num = 1;
        
        try{
            for(int i = 0; i < castPictureMapList.size(); i++)  //遍历要操作的人物海报主键，依次操作要关联的内容
            {
                totalCastindex ++;
                String result = null;
                String operdesc = null;
                String proStatusStr = null;
              
                CastPictureMap cpmObj = new CastPictureMap();
                cpmObj.setMapindex(castPictureMapList.get(i).getMapindex());
                cpmObj = castPictureMapDS.getCastPictureMap(cpmObj);
                if (cpmObj == null ){
                   proStatusStr = "关联关系不存在";
                   failCount++;
                   result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                   operateInfo = new OperateInfo(String.valueOf(num), castPictureMapList.get(i).getMapindex().toString(), 
                           result.substring(2, result.length()), proStatusStr);
                   returnInfo.appendOperateInfo(operateInfo);  
                }else{
                    operdesc = publishCastPicMapToPlatform(cpmObj.getMapindex(),platform,syncType);  
                    if (operdesc.substring(0, 1).equals("0"))  
                    {
                        successCount++;
                        result = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        operateInfo = new OperateInfo(String.valueOf(num), cpmObj.getMapindex().toString(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                    }
                    else  //内容关联运营平台失败
                    {
                        failCount++;
                        result = CntUtils.getResourceText(CntConstants.CNT_FAIL);
                        operateInfo = new OperateInfo(String.valueOf(num), cpmObj.getMapindex().toString(), 
                                result.substring(2, result.length()), operdesc.substring(2, operdesc.length()));
                        returnInfo.appendOperateInfo(operateInfo);
                            
                    }
                }
                num++;
            }
        }catch(Exception e){
            log.error("CastPlatformSyncLS exception:" + e.getMessage());
        }
        if ((successCount) == totalCastindex)
        {
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount);
            returnInfo.setFlag("0");
        }
        else
        {
            if ((failCount) == totalCastindex)
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + successCount + 
                        ResourceMgt.findDefaultText("fail.count") + failCount);
                returnInfo.setFlag("2");
            }
        }

        return returnInfo.toString();   
        
    }

    public String publishCastPicMapToPlatform(Long mapindex,int platform,int syncType) throws Exception
    {
        log.debug("publishCastPicMapToPlatform start ");
        List<Castcdn> castList = new ArrayList<Castcdn>();
        List<Picture> pictureList = new ArrayList<Picture>();
        List<CastPictureMap> castPictureMapList = new ArrayList<CastPictureMap>();
        List<Integer> typeList = new ArrayList();
        CastPictureMap castPictureMap = new CastPictureMap();
        Castcdn cast = new Castcdn();
        Picture picture = new Picture();
        StringBuffer resultStr = new StringBuffer();
        boolean sucess = false;
        String rtnStr; 
        Long batchid = (Long)this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id");
        
        try{
            castPictureMap.setMapindex(mapindex);
            castPictureMap = castPictureMapDS.getCastPictureMap(castPictureMap);
            if(castPictureMap == null){
                return "1:关联关系不存在";
            }
            
            Long relateindex = null;
            CntTargetSync target = new CntTargetSync();
            CntPlatformSync cpmPlatform = new CntPlatformSync();
            cpmPlatform.setObjecttype(41);
            cpmPlatform.setObjectid(castPictureMap.getMappingid());
            cpmPlatform.setPlatform(platform);
            List<CntPlatformSync> cpmPlatformList = cntPlatformSyncDS.getCntPlatformSyncByCond(cpmPlatform);
            if(cpmPlatformList != null && cpmPlatformList.size() > 0){
                int cpmSyncStatus = cpmPlatformList.get(0).getStatus();
                relateindex = cpmPlatformList.get(0).getSyncindex();
                if(syncType == 1){ //新增同步
                    //0：待发布，400：发布失败
                    if(cpmSyncStatus != 0 && cpmSyncStatus!= 400){
                        return "1:关联关系发布状态不正确";
                    }
                }else{
                    if(cpmSyncStatus != 300 && cpmSyncStatus!= 500 && cpmSyncStatus!= 400){
                        return "1:关联关系发布状态不正确";
                    }
                }
            }else
            {
                return "1:发布记录不存在";
            }
            
            target.setRelateindex(relateindex);
            List<CntTargetSync> cpmTargetList = cntTargetSyncDS.getCntTargetSyncByCond(target);
            if(cpmTargetList != null && cpmTargetList.size() > 0 )
            {
                for(CntTargetSync cpmTargetSync:cpmTargetList){
                    Long cpmtargetindex = cpmTargetSync.getTargetindex();
                    CntTargetSync casttarget = new CntTargetSync();
                    casttarget.setObjecttype(11);
                    casttarget.setObjectid(castPictureMap.getCastid());
                    casttarget.setTargetindex(cpmtargetindex);
                    List<CntTargetSync> castTargetList = cntTargetSyncDS.getCntTargetSyncByCond(casttarget);
                    if(castTargetList != null && castTargetList.size() > 0 )
                    {
                        int castSyncStatus = castTargetList.get(0).getStatus();
                        if(castSyncStatus!=300&&castSyncStatus!=500){
                            return "1:人物发布状态不正确";
                        }
                    }
                }
                    
            }
            
            String desPath = cmsStorageareaLS.getAddress(TEMPAREA_ID);
            if (null == desPath || (TEMPAREA_ERRORSIGN).equals(desPath)) // 获取临时区地址失败
            {
                return "1:操作失败,获取临时区地址失败";
            }
            else
            {
                File makePath = new File(GlobalConstants.getMountPoint() + desPath + File.separator);
                if (!makePath.exists())
                {// 找不到目标地址，请检查存储区绑定
                    return "1:操作失败,找不到临时区目标地址，请检查存储区绑定";
                }             
            }
            
            cast.setCastindex(castPictureMap.getCastindex());
            cast = castds.getCastcdn(cast);
            picture.setPictureindex(castPictureMap.getPictureindex());
            picture = pictureDS.getPicture(picture);
            castList.add(cast);
            pictureList.add(picture);
            castPictureMapList.add(castPictureMap);
            typeList.add(41);
            map.put("type", typeList);
            map.put("castList", castList);
            map.put("pictureList", pictureList);
            map.put("castPictureMapList", castPictureMapList);
            
            int[] cpmTargetStatus = new int[cpmTargetList.size()];
            if(cpmTargetList != null && cpmTargetList.size() > 0 )
            {
                for(CntTargetSync cpmTargetSync:cpmTargetList)
                { 
                    String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                    Long index = (Long)this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task"); //获取taskindex
                    Long syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                    String wg18xml = "";
                    String castxml = "";
                    int operresult = 0;
                    int targetStatus = 0;
                    Targetsystem tagertsystem = new Targetsystem();
                    tagertsystem.setTargetindex(cpmTargetSync.getTargetindex());
                    tagertsystem = targetSystemDS.getTargetsystem(tagertsystem);
                    if(tagertsystem.getStatus()==1 || tagertsystem.getStatus()==2)
                    {
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"状态为暂停或者已注销  ");
                        continue;
                    }
                    
                    if((syncType == 1 && cpmTargetSync.getStatus() == 0) || (syncType == 1 && cpmTargetSync.getStatus() == 400))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 10, 1);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,cpmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castPictureMap.getMapindex(),castPictureMap.getMappingid(),"",41,
                                    cast.getCastid(),cast.getCastcode(),picture.getPictureid(),picture.getPicturecode(),syncType,cpmTargetSync.getTargetindex());
                            syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                            insertObjectRecord( syncindex,index,picture.getPictureindex(),picture.getPictureid(),picture.getPicturecode(),10,
                                    "","","","",syncType,cpmTargetSync.getTargetindex());
                        }else if(platform ==2) //3.0
                        {
                            castxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 10, 1);
                            if(castxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castxml,correlateid,cpmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castPictureMap.getMapindex(),castPictureMap.getMappingid(),"",41,
                                    cast.getCastid(),"",picture.getPictureid(),"",syncType,cpmTargetSync.getTargetindex());
                            syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                            insertObjectRecord( syncindex,index,picture.getPictureindex(),picture.getPictureid(),picture.getPicturecode(),10,
                                    "","","","",syncType,cpmTargetSync.getTargetindex());
                        }
                        
                        //更新网元状态
                        operresult = 10;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        cpmTargetSync.setStatus(targetStatus);
                        cpmTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(cpmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(cast.getCastid(), CastcdnConstant.MGTTYPE_CAST_SYNC,CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH,
                                CastcdnConstant.OPERTYPE_CAST_CANCELPUBLISH_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                        
                    }
                    else if((syncType == 3 && cpmTargetSync.getStatus() == 300) || (syncType == 1 && cpmTargetSync.getStatus() == 500))
                    {
                        if(platform ==1) //2.0
                        { 
                            wg18xml = StandardImpFactory.getInstance().create(1).createXmlFile(map, tagertsystem.getTargettype(), 10, 3);
                            if(wg18xml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,wg18xml,correlateid,cpmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castPictureMap.getMapindex(),castPictureMap.getMappingid(),"",41,
                                    cast.getCastid(),cast.getCastcode(),picture.getPictureid(),picture.getPicturecode(),syncType,cpmTargetSync.getTargetindex());
                            syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                            insertObjectRecord( syncindex,index,picture.getPictureindex(),picture.getPictureid(),picture.getPicturecode(),10,
                                    "","","","",syncType,cpmTargetSync.getTargetindex());
                        }else if(platform ==2) //3.0
                        {
                            castxml = StandardImpFactory.getInstance().create(2).createXmlFile(map, tagertsystem.getTargettype(), 10, 3);
                            if(castxml == null)
                            {
                                resultStr.append("目标系统"+tagertsystem.getTargetid()+"生成同步xml文件失败  ");
                                continue;
                            }
                            insertSyncTask(index,castxml,correlateid,cpmTargetSync.getTargetindex(),batchid);  // 插入同步任务
                            insertObjectRecord( syncindex,index,castPictureMap.getMapindex(),castPictureMap.getMappingid(),"",41,
                                    cast.getCastid(),"",picture.getPictureid(),"",syncType,cpmTargetSync.getTargetindex());
                            syncindex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index");
                            insertObjectRecord( syncindex,index,picture.getPictureindex(),picture.getPictureid(),picture.getPicturecode(),10,
                                    "","","","",syncType,cpmTargetSync.getTargetindex());
                        }
                        
                        //更新网元状态
                        operresult = 70;
                        targetStatus = StatusTranslator.getTargetSyncStatus(operresult);
                        cpmTargetSync.setStatus(targetStatus);
                        cpmTargetSync.setOperresult(operresult);
                        cntTargetSyncDS.updateCntTargetSync(cpmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(mapindex.toString(), CastcdnConstant.MGTTYPE_CAST_SYNC,
                                PictureConstants.OPERTYPE_PICTURE_PUB, PictureConstants.OPERTYPE_PICTURE_PUB_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                    else if(syncType == 3 && cpmTargetSync.getStatus() == 400)
                    {
                        //更新网元状态
                        cpmTargetSync.setStatus(0);
                        cpmTargetSync.setOperresult(80);
                        cntTargetSyncDS.updateCntTargetSync(cpmTargetSync);
                        
                        CommonLogUtil.insertOperatorLog(mapindex.toString(), CastcdnConstant.MGTTYPE_CAST_SYNC,
                                PictureConstants.OPERTYPE_PICTURE_PUB, PictureConstants.OPERTYPE_PICTURE_PUB_INFO,CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
                        
                        resultStr.append("目标系统"+tagertsystem.getTargetid()+"操作成功"+"  ");
                        sucess = true;
                    }
                }
                for(int i = 0; i< cpmTargetList.size(); i++){
                    cpmTargetStatus[i] = cpmTargetList.get(i).getStatus();
                }
                //更新平台状态
                int platformStatus = StatusTranslator.getPlatformSyncStatus(cpmTargetStatus);
                cpmPlatform = cpmPlatformList.get(0);
                cpmPlatform.setStatus(platformStatus);
                cntPlatformSyncDS.updateCntPlatformSync(cpmPlatform);
            }
            
        }
        catch (Exception e)
        {
            log.error("publishCastPicMapToPlatform exception:" + e.getMessage());
            return "1:操作失败";
        }
        
        log.debug("publishCastPicMapToPlatform end ");
        rtnStr = sucess==false ? "1:"+resultStr.toString():"0:"+resultStr.toString();
        return rtnStr;
    }
    
    /**
     * 
     * @param taskindex 同步任务index
     * @param xmlAddress 同步xml文件地址
     * @param correlateid 流水号
     * @param targetindex 目标系统index
     */
    private void insertSyncTask( Long taskindex,  String xmlAddress, String correlateid ,Long targetindex,Long batchid)
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            cntSyncTask.setTaskindex(taskindex);
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setRetrytimes(3);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
            cntSyncTask.setDestindex(Long.valueOf(targetindex));
            cntSyncTask.setSource(1);
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            cntSyncTask.setBatchid(batchid);
            cntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    }
 
    /**
     * 
     * @param syncindex 对象发布记录index
     * @param taskindex 同步任务index
     * @param objectindex 对象index
     * @param objectid 对象编码
     * @param objectcode 发布到2.0平台时对象的文广code
     * @param objecttype 对象类型：1-Service，2-Category，3-Program，
     * @param elementid 对象为mapping时必填，mapping的elementid
     * @param elementcode 文广elementcode，对象为mapping时且发布到2.0平台时必填
     * @param parentid 对象为mapping时必填，mapping的parentid
     * @param parentcode  文广parentcode，对象为mapping时且发布到2.0平台时必填
     * @param synType 同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @param targetindex 目标系统index
     * @param platType 平台类型： 1:3.0平台，2:3.0平台
     */
    private void insertObjectRecord( Long syncindex, Long taskindex,Long objectindex,String objectid, String objectcode,
            int objecttype,String elementid,String elementcode,String parentid,String parentcode,int synType, Long targetindex )
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            objectSyncRecord.setSyncindex(syncindex);
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(objectindex);
            objectSyncRecord.setObjectid(objectid);
            objectSyncRecord.setObjectcode(objectcode);
            objectSyncRecord.setObjecttype(objecttype);
            objectSyncRecord.setElementid(elementid);
            objectSyncRecord.setElementcode(elementcode);
            objectSyncRecord.setParentid(parentid);
            objectSyncRecord.setParentcode(parentcode);
            objectSyncRecord.setActiontype(synType);
            objectSyncRecord.setStarttime(dateformat.format(new Date()));
            objectSyncRecord.setDestindex(targetindex);
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
    } 
    
    public void setLog(Logger log)
    {
        this.log = log;
    }

    public void setCastPlatformSyncDS(ICastPlatformSyncDS castPlatformSyncDS)
    {
        this.castPlatformSyncDS = castPlatformSyncDS;
    }

}

package com.zte.cms.channel.wkfw.ls;

import com.zte.cms.channel.ls.ChannelConstants;

import com.zte.cms.channel.model.CmsSchedule;

import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.common.ResourceManager;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleWkfwCheck implements IScheduleWkfwCheck
{

    private ICmsScheduleDS scheduleds = null;

    // 构造方法
    ScheduleWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }

    public ICmsScheduleDS getScheduleds()
    {
        return scheduleds;
    }
    public void setScheduleds(ICmsScheduleDS scheduleds)
    {
        this.scheduleds = scheduleds;
    }

    /**
     * @param channelindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkScheduleWkfw(Long scheduleindex) throws DomainServiceException
    {
        CmsSchedule schedule = new CmsSchedule();
        schedule.setScheduleindex(scheduleindex);
        schedule = scheduleds.getCmsSchedule(schedule);

        if (schedule == null)
        {
            return "1:" + ResourceManager.getResourceText("msg.schedul.notexist"); // 不存在
        }
        // 状态不为带审核或者审核失败或者待审核,且没有发起工作流
        if ((schedule.getStatus() != 110) && (schedule.getStatus() != 130))
        {
            return "1:" + ResourceManager.getResourceText("msg.schedulaudit.statuswrong"); // 状态不正确
        }
           
        // 已经发起工作流
        if (schedule.getWorkflow() != null)
        {
            if (schedule.getWorkflow() != 0)
            {
                return "1:" + ResourceManager.getResourceText("msg.schedulaudit.wkfwinaudit");
                // 策略已发起工作流未结束
            }
        }
        if (schedule.getWorkflowlife() != null)
        {
            if (schedule.getWorkflowlife() != 0)
            {
                return "1:" + ResourceManager.getResourceText("msg.schedulaudit.wkfwstatuswrong");
                // 工作流流程状态不正确
            }
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    /**
     * @param channelIndex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     * 
     */
    public String checkScheduleWkfwAudit(Long scheduleIndex) throws DomainServiceException
    {
        CmsSchedule schedule = new CmsSchedule();
        schedule.setScheduleindex(scheduleIndex);
        schedule = scheduleds.getCmsSchedule(schedule);

        if (schedule == null)
        {
            return "1:" + ResourceManager.getResourceText("msg.schedul.notexist"); // 不存在
            // 服务不存在
        }

        // 120：审核中
        if (schedule.getStatus() != 120)
        {
            return "1:" + ResourceManager.getResourceText("msg.schedulaudit.statuswrong"); // 状态不正确
        }
        if (schedule.getWorkflow() != 1)
        {
            return "1:" + ResourceManager.getResourceText("msg.schedulaudit.wkfwnotinorstatuswrong");
            // 服务不在工作流中或所处工作流不正确
        }
        if (schedule.getWorkflowlife() != 1)
        {
            return "1:" + ResourceManager.getResourceText("msg.schedulaudit.wkfwstatuswrong");
            // 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

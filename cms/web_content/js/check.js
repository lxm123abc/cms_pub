function is_invalid(str) {
	if (str == null || str == "") return true;
	if (strTrim(str) == "") return true;
	return false;
}

function checkLen(obj,maxChars)  
{
    var len = getStrLen(obj.value);
    if (len > maxChars)
    {
        obj.value = getStrValueByMax(obj,maxChars);
    }
    var curr = maxChars - getStrLen(obj.value);
    document.getElementById(obj.id+"_msg").innerHTML = curr.toString(); 
}
function getStrValueByMax(obj,maxChars)
{
    var str = obj.value;
    var len = 0;
    for(i=0;i<str.length;i++)
    {
        value = str.charCodeAt(i);
        if( value < 0x080) 
        { 
            len += 1; 
        } 
        else if( value < 0x0800) 
        { 
            len += 2; 
        } 
        else if(value < 0x010000)
        { 
            len += 3; 
        }
		else
		{
		    len +=4;
		}
        if(len > maxChars)
        {
           return str.substring(0,i);
        }
    }
}
function getStrLen(str)
{
    len = 0;
    for(i=0;i<str.length;i++)
    {
        value = str.charCodeAt(i);
        if( value < 0x080) 
        { 
            len += 1; 
        } 
        else if( value < 0x0800) 
        { 
            len += 2; 
        } 
        else if(value < 0x010000)
        { 
            len += 3; 
        }
		else
		{
		    len +=4;
		}
    }
    return len;
}
function strlen(str,maxlen)
{
    len = 0;
    for(i=0;i<str.length;i++)
    {
        value = str.charCodeAt(i);
        if( value < 0x080) 
        { 
            len += 1; 
        } 
        else if( value < 0x0800) 
        { 
            len += 2; 
        } 
        else if(value < 0x010000)
        { 
            len += 3; 
        }
		else
		{
		    len +=4;
		}
    }
    if(len>maxlen)
    {
        return false;
    }
    return true;
}
function is_number(str){
    if (str == null || str == "") return true;
	var chr = str.charAt(0);
	if (chr == "0") return false;
	if (chr == "+" || chr == "-") str = str.substring(1);
	var maxLen = 500;
	if (arguments.length > 1) maxLen = arguments[1];
	return (str.length <= maxLen && is_numcode(str));
}

function strTrim(str){
	str = str + " ";
    var first = 0;
    var last = str.length - 1;
    while (str.charAt(first) == " ") { first++; }
    while (str.charAt(last) == " ") { last--; }
	if (first > last) return "";
    return str.substring(first, last + 1);
}

function is_numcode(str){
    if (str == null || str == "") return true;
    var myReg = /^[0-9]+$/;
    return myReg.test(str);
}

function checkip(str){
	
	ip='(25[0-5]|2[0-4]\\d|1\\d\\d|\\d\\d|\\d)';   
	ipx=ip+'\\.';   
	isIPaddress=new   RegExp('^'+ipx+ipx+ipx+ip+'$');   
	
	if(isIPaddress.test(str)){
		return true;
	}
	else{
		return false;
        }
}


function isValidateURL(sCheck,showalert){
  var regexp = /^http:\/\//; 
  var result = sCheck.match(regexp); 
  if(!result){
    if(showalert)
      alert("\u8bf7\u8f93\u5165\u6b63\u786e\u7684\u8d85\u94fe\u63a5\uff08http://\uff09\uff01");
	return false;
  }
  return true;
}


function checkLength(str,length)
{
	var tempStr=trim(str);
    tempStr = tempStr.replace(/[^\x00-\xff]/g,"***");
    if (tempStr.length > length ) return true;
	return false;
}


function checkString(str)
{
    var reg = /^(\w|[\u4E00-\u9FA5])+$/;
   	if(arr=str.match(reg))
   	{
      		return true;
   	}
   	else
   	{
      		return false;
   	}
}

function trim(input){
    return input.replace(/(^\s*)|(\s*$)/g, "");
}

function checkstring(checkstr,userinput) {
    var allValid = false;
    for (i = 0;i<userinput.length;i++) {
        ch = userinput.charAt(i);
        if(checkstr.indexOf(ch) >= 0) {
            allValid = true;
            break;
    }
  }
  return allValid;
}
function checkImg(img)
{
    post = img.lastIndexOf('.');
    imgtype = img.substr(post+1);
    imgtype = imgtype.toLowerCase();
    if(imgtype=='jpg'||imgtype=='gif'||imgtype=='png')
    {
        return true;
    }
    
    return false;
  
}	
function isChinaOrNumbOrLett( s )
{


var regu = "^[0-9a-zA-Z\u4e00-\u9fa5]+$";   

var re = new RegExp(regu);

if (re.test(s)) {

return true;

}else{

return false;

}

}
 

package com.zte.cms.basicdata.ls;

import com.zte.cms.basicdata.model.CmsExtractedvafileInfo;

public interface ICmsMediaInfoLS
{

    public String extractMediaInfo(String fileindex, String sourcetype);

    public CmsExtractedvafileInfo getVAInfo(String fileindex);

    public Object getMovieInfo(String fileindex, String sourcetype);

    public Object getMovieBufInfo(String fileindex, String sourcetype);
}
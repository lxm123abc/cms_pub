
  package com.zte.cms.content.program.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class ProgramSchedualrecordMap extends DynamicBaseObject
{
	private java.lang.Long mapindex;
	private java.lang.Long programindex;
	private java.lang.String programid;
	private java.lang.Long schrecordindex;
	private java.lang.String schrecordid;
	private java.lang.Integer maptype;
	private java.lang.Integer sequence;
	private java.lang.Integer status;
	private java.lang.String createtime;
	private java.lang.String publishtime;
	private java.lang.String cancelpubtime;
    
    public java.lang.Long getMapindex()
    {
        return mapindex;
    } 
         
    public void setMapindex(java.lang.Long mapindex) 
    {
        this.mapindex = mapindex;
    }
    
    public java.lang.Long getProgramindex()
    {
        return programindex;
    } 
         
    public void setProgramindex(java.lang.Long programindex) 
    {
        this.programindex = programindex;
    }
    
    public java.lang.String getProgramid()
    {
        return programid;
    } 
         
    public void setProgramid(java.lang.String programid) 
    {
        this.programid = programid;
    }
    
    public java.lang.Long getSchrecordindex()
    {
        return schrecordindex;
    } 
         
    public void setSchrecordindex(java.lang.Long schrecordindex) 
    {
        this.schrecordindex = schrecordindex;
    }
    
    public java.lang.String getSchrecordid()
    {
        return schrecordid;
    } 
         
    public void setSchrecordid(java.lang.String schrecordid) 
    {
        this.schrecordid = schrecordid;
    }
    
    public java.lang.Integer getMaptype()
    {
        return maptype;
    } 
         
    public void setMaptype(java.lang.Integer maptype) 
    {
        this.maptype = maptype;
    }
    
    public java.lang.Integer getSequence()
    {
        return sequence;
    } 
         
    public void setSequence(java.lang.Integer sequence) 
    {
        this.sequence = sequence;
    }
    
    public java.lang.Integer getStatus()
    {
        return status;
    } 
         
    public void setStatus(java.lang.Integer status) 
    {
        this.status = status;
    }
    
    public java.lang.String getCreatetime()
    {
        return createtime;
    } 
         
    public void setCreatetime(java.lang.String createtime) 
    {
        this.createtime = createtime;
    }
    
    public java.lang.String getPublishtime()
    {
        return publishtime;
    } 
         
    public void setPublishtime(java.lang.String publishtime) 
    {
        this.publishtime = publishtime;
    }
    
    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    } 
         
    public void setCancelpubtime(java.lang.String cancelpubtime) 
    {
        this.cancelpubtime = cancelpubtime;
    }
    


    public void initRelation()
    {
        this.addRelation("mapindex","MAPINDEX");   
        this.addRelation("programindex","PROGRAMINDEX");   
        this.addRelation("programid","PROGRAMID");   
        this.addRelation("schrecordindex","SCHRECORDINDEX");   
        this.addRelation("schrecordid","SCHRECORDID");   
        this.addRelation("maptype","MAPTYPE");   
        this.addRelation("sequence","SEQUENCE");   
        this.addRelation("status","STATUS");   
        this.addRelation("createtime","CREATETIME");   
        this.addRelation("publishtime","PUBLISHTIME");   
        this.addRelation("cancelpubtime","CANCELPUBTIME");   
    } 
}

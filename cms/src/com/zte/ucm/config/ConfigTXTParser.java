package com.zte.ucm.config;

import com.zte.ucm.Constants;
import com.zte.ucm.Resultlog;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class ConfigTXTParser
{
  Logger log = LoggerFactory.getLogger(ConfigParser.class);

  public Config getConfig(String paramString1, String paramString2) throws Exception {
    String str = "";
    Config localConfig = new Config();
    Resultlog localResultlog = new Resultlog();
    File localFile = new File(Constants.CONFIGFILEPATH + Constants.CONFIGFILENAME);
    try
    {
      SAXReader localSAXReader = new SAXReader();
      Document localDocument = localSAXReader.read(localFile);

      localConfig.setTag(paramString1);
      List localList = localDocument.selectNodes("/configs/config[tag='" + paramString1.trim() + "']");

      if ((localList != null) && (localList.size() > 0)) {
        Element localElement1 = (Element)localList.get(0);

        Iterator localIterator = localElement1.elementIterator("file");
        Element localElement2;
        while (localIterator.hasNext()) {
          localElement2 = (Element)localIterator.next();
          localConfig.setFile(localElement2.getTextTrim());
        }

        localIterator = localElement1.elementIterator("key");
        while (localIterator.hasNext()) {
          localElement2 = (Element)localIterator.next();
          localConfig.setKey(localElement2.getTextTrim());
        }

        localIterator = localElement1.elementIterator("value");
        while (localIterator.hasNext()) {
          localElement2 = (Element)localIterator.next();
          localConfig.setValue(localElement2.getTextTrim().replaceAll("\\$\\{value\\}", Matcher.quoteReplacement(localConfig.getKey() + "=" + paramString2)));
        }
      } else {
        this.log.error("Operation ERROR:Cannot find the config, please check the input param[" + paramString1 + "]");
        str = str + "1|Operation ERROR:Cannot find the config, please check the input param[" + paramString1 + "]" + "\n";
        localResultlog.appendLog(str);
      }

      this.log.info("Config Info:[Tag:" + localConfig.getTag() + " File:" + localConfig.getFile() + " Prevalue:" + localConfig.getKey() + "  Value:" + localConfig.getValue() + "]");
    }
    catch (DocumentException localDocumentException)
    {
      this.log.error("Operation ERROR:ConfigParser getConfig error");
      str = str + "1|Operation ERROR:" + paramString1 + "|" + "ConfigParser getConfig error" + "\n";
      localResultlog.appendLog(str);

      throw localDocumentException;
    }
    return localConfig;
  }
}

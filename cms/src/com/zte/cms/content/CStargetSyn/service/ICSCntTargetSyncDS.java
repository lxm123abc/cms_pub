package com.zte.cms.content.CStargetSyn.service;

import java.util.List;
import com.zte.cms.content.CStargetSyn.model.CSCntTargetSync;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICSCntTargetSyncDS
{
  

	 /**
	  * 根据条件分页查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntTargetSync对象 
	  *
	  * @param cntTargetSync CntTargetSync对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(CSCntTargetSync cntTargetSync, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
  
     
 	 public TableDataInfo pageInfoQueryCha(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException;
 	 public TableDataInfo pageInfoQuerySche(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException;
 	 public TableDataInfo pageInfoQueryCach(CSCntTargetSync cntTargetSync, int start, int pageSize)throws DomainServiceException;

}

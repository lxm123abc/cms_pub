package com.zte.cms.basicdata.area.ls;

public class AreaConstant
{

    // mgttype 管理类型 ftp管理

    public static final String MGTTYPE_AREA = "log.area.mgt";

    /**
     * opertype 操作类型（String)
     */

    public static final String OPERTYPE_AREA_PROVINCE_ADD = "log.area.province.add";// 省份新增
    public static final String OPERTYPE_AREA_PROVINCE_MOD = "log.area.province.modify";// 省份修改
    public static final String OPERTYPE_AREA_PROVINCE_DEL = "log.area.province.delete";// 省份删除
    public static final String OPERTYPE_AREA_PROVINCE_NOTEXIST = "log.area..province.notexist";// 省份不存在或已经删除
    public static final String OPERTYPE_AREA_PROVINCE_SYNCHRONIZED_MIDDEL = "log.area.province.synchronized.middle";// 省份状态已处于同步中

    public static final String OPERTYPE_AREA_PROVINCE_SYNCHRONIZED_ADD = "log.area.province.synchronized.add";// 省份新增同步
    public static final String OPERTYPE_AREA_PROVINCE_SYNCHRONIZED_DELETE = "log.area.province.synchronized.delete";// 省份删除同步

    public static final String OPERTYPE_AREA_PROVINCE_SYNCHRONIZED_ADD_INFO = "log.area.province.synchronized.add.info";
    public static final String OPERTYPE_AREA_PROVINCE_SYNCHRONIZED_DELETE_INFO = "log.area.province.synchronized.delete.info";

    public static final String OPERTYPE_AREA_CiTY_ADD = "log.area.city.add";// 城市新增
    public static final String OPERTYPE_AREA_CiTY_MOD = "log.area.city.modify";// 城市修改
    public static final String OPERTYPE_AREA_CiTY_DEL = "log.area.city.delete";// 城市删除
    public static final String OPERTYPE_AREA_CiTY_NOTEXIST = "log.area.city.notexist";// 城市不存在或已经删除
    public static final String OPERTYPE_AREA_CiTY_SYNCHRONIZED_MIDDEL = "log.area.city.synchronized.middle";// 城市状态已处于同步中

    public static final String OPERTYPE_AREA_CITY_SYNCHRONIZED__ADD = "log.area.city.synchronized.add";// 城市新增同步
    public static final String OPERTYPE_AREA_CITY_SYNCHRONIZED_DELETE = "log.area.city.synchronized.delete";// 城市删除同步

    public static final String OPERTYPE_AREA_CITY_SYNCHRONIZED__ADD_INFO = "log.area.city.synchronized.add.info";
    public static final String OPERTYPE_AREA_CITY_SYNCHRONIZED_DELETE_INFO = "log.area.city.synchronized.delete.info";

    /**
     * opertypeinfo 操作类型（String)
     */

    public static final String OPERTYPE_AREA_PROVINCE_ADD_INFO = "log.area.province.add.info";
    public static final String OPERTYPE_AREA_PROVINCE_MOD_INFO = "log.area.province.modify.info";
    public static final String OPERTYPE_AREA_PROVINCE_DEL_INFO = "log.area.province.delete.info";

    public static final String OPERTYPE_AREA_CiTY_ADD_INFO = "log.area.city.add.info";
    public static final String OPERTYPE_AREA_CiTY_MOD_INFO = "log.area.city.modify.info";
    public static final String OPERTYPE_AREA_CiTY_DEL_INFO = "log.area.city.delete.info";

}

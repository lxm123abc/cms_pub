package com.zte.cms.settlementDataRpt.dao;

import com.zte.cms.settlementDataRpt.model.SettlementEntDownFile;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;

/**
 * @Author: Liangxiaomin
 * @Date Created in 15:19 2019/6/28
 * @Description:
 */
public class SettlementEntDownFileMapperImpl extends DynamicObjectBaseDao implements SettlementEntDownFileMapper {
    @Override
    public void insertSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) {
        super.insert("insertSettlementEntDownFile", settlementEntDownFile);
    }

    @Override
    public void updateSettlementEntDownFile(SettlementEntDownFile settlementEntDownFile) {
        super.update("updateSettlementEntDownFileBySpid",settlementEntDownFile);
    }

    @Override
    public SettlementEntDownFile selectSettlementEntDownFileBySpid(SettlementEntDownFile settlementEntDownFile) {
        settlementEntDownFile = (SettlementEntDownFile) super.queryForObject("selectSettlementEntDownFileBySpid",settlementEntDownFile);
        return settlementEntDownFile;
    }

    @Override
    public Integer selectSettlementEntDownFileCountBySpid(SettlementEntDownFile settlementEntDownFile) {
        return ((Integer) super.queryForObject("selectSettlementEntDownFileCountBySpid", settlementEntDownFile));
    }
}

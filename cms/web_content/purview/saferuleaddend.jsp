<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ page import="java.util.*"%>
<%@ include file="JavaFun.jsp" %>
<jsp:useBean id="pwdrule" class="com.zte.purviewext.PwdRule" scope="page"/>
<%

      String flag = request.getParameter("flag");
	    if("2".equals(flag)){

        HashMap map = new HashMap();
        map.put("levelindex",request.getParameter("levelindex"));
        map.put("minlen",request.getParameter("minlen"));
        map.put("maxlen",request.getParameter("maxlen"));
        map.put("chrtype",request.getParameter("chrtype"));
        map.put("createtype",request.getParameter("createtype"));
        map.put("defaultpwd",request.getParameter("defaultpwd"));
        map.put("updateflag",request.getParameter("updateflag")); 
        map.put("maxwrglog",request.getParameter("maxwrglog"));
        map.put("pwdmaxday",request.getParameter("pwdmaxday"));
        map.put("hintflag",request.getParameter("hintflag"));
        map.put("usednum",request.getParameter("usednum"));
        map.put("prerequisite",request.getParameter("prerequisite"));
        map.put("description",transferString(request.getParameter("description")));
        map.put("checkflag",request.getParameter("checkflag"));
	      pwdrule.addSafeRule(map);
	      
	    }
	    else if("3".equals(flag)){ 
	    
        HashMap map = new HashMap();
        map.put("targetindex",request.getParameter("targetindex"));
        map.put("levelindex",request.getParameter("levelindex"));
        map.put("minlen",request.getParameter("minlen"));
        map.put("maxlen",request.getParameter("maxlen"));
        map.put("chrtype",request.getParameter("chrtype"));
        map.put("createtype",request.getParameter("createtype"));
        map.put("defaultpwd",request.getParameter("defaultpwd"));
        map.put("updateflag",request.getParameter("updateflag")); 
        map.put("maxwrglog",request.getParameter("maxwrglog"));
        map.put("pwdmaxday",request.getParameter("pwdmaxday"));
        map.put("hintflag",request.getParameter("hintflag"));
        map.put("usednum",request.getParameter("usednum"));
        map.put("checkflag",request.getParameter("checkflag"));
        map.put("prerequisite",request.getParameter("prerequisite"));
        map.put("description",transferString(request.getParameter("description")));
        pwdrule.updateSafeRule(map);

	    } 
      response.sendRedirect("saferuleset.jsp");
%>
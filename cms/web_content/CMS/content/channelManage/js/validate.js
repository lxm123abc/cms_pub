
function Trim(inputStr)
{
    return inputStr.replace(/(^\s*)|(\s*$)/g,"");
}


function checkNull(inputStr)
{
    return ( Trim(inputStr).length==0)
}


function checkLen(inputStr,min,max)
{
    var tempStr=Trim(inputStr);
    if((tempStr.length<min)||(tempStr.length>max))
    {
        return false;
    }
    return true;
}

 

function IsDigit(cCheck)
{
    return (('0'<=cCheck) && (cCheck<='9'));
}


function checkNum(sCheck){
  var regexp= /^\d+$/; 
  return sCheck.match(regexp); 
}

function isZeroStart(sCheck){
  var regexp= /^0/; 
  return sCheck.match(regexp); 
}


function checkIP(inputStr)
{
    var regIP=/\b((25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)\.){3}(25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)\b/;
    return inputStr.match(regIP); 
}


function checkDomain(inputStr)
{
    var regDomain=/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/;
    return inputStr.match(regDomain); 
}


function checkUrl(inputStr)
{
    //var regUrl=/^(http:[/][/]|www.)(|[a-z]|[A-Z]|[0-9]|[/.]|[~]|[:])*$/;
    var regUrl=/^(http:[/][/]|www.)([a-z]|[A-Z]|[0-9]|[/.]|[~]|[:]|[\?]|[=])*$/;
    return inputStr.match(regUrl); 
}

function IsAlpha(cCheck)
{
    return ((('a'<=cCheck) && (cCheck<='z')) || (('A'<=cCheck) && (cCheck<='Z')))
}


function IsaNull(cCheck)
{
    return(cCheck != " ")
}


function checkMobileCC(inputStr)
{
    var Pattern=/^13[0-9]{9}$/; 
    var Pattern1=/^8613[0-9]{9}$/; 

	if(Pattern.test(Trim(inputStr))==false && Pattern1.test(Trim(inputStr))==false)
	{
		return false;
	}
	else
	{
		return true;
	}

}


function checkMobile(inputStr)
{
    var Pattern=/^13[0-9]{9}$/; 

	if(Pattern.test(Trim(inputStr))==false)
	{
		return false;
	}
	else
	{
		return true;
	}

}


function checkPhone(inputStr)
{
    var regPhone=/^[0-9]+\-[0-9]+$/;
    return (regPhone.test(Trim(inputStr)));
}


function checkCN(inputStr)
{
    var cnPattern=/[\u4E00-\u9FA5]/;
    return (cnPattern.test(Trim(inputStr)) || Trim(inputStr).length == 0);
}


function checkMoney(inputStr)
{
    var regPhone=/^[0-9]+\.[0-9]{0,2}$/;
    if (regPhone.test(Trim(inputStr))) return true;
    if (checkNum(Trim(inputStr))) return true;
    return false;
}


 function checkEmail(inputStr)
 {
     emailerr=0;
     var email=new String(inputStr);
     for (i=0; i<email.length; i++)
    {
         if ((email.charAt(i) == "@") & (email.length > 5))
            {
                 emailerr=emailerr+1;
             }
     }
     if (emailerr != 1)
     {
           return false;
     }
     return true;
 }


 function checkDate(year,month,day)
 {
     var yStr=new String(year);
     var mStr=new String(month);
     var dStr=new String(day);
     if((yStr.length!=4)||(!checkLen(month,1,2))||(!checkLen(day,1,2)))
     {
            return false;
     }
     for(i=0;i<yStr.length;i++)
     {
         cCheck = yStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                     return false;
             }
     }
     for(i=0;i<mStr.length;i++)
     {
         cCheck = mStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                      return false;
             }
     }
     for(i=0;i<dStr.length;i++)
     {
         cCheck = dStr.charAt(i);
         if (!(IsDigit(cCheck)))
            {
                          return false;
             }
     }
     return true;
}




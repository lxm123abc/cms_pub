package com.zte.cms.service.apply;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.service.IServiceWkfwhisDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;

public class ServiceApplyCallback extends CommonCallback
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    protected IServiceDS serviceds = null;
    protected IServiceWkfwhisDS serviceWkfwhisDS;

    protected Service service;
    protected ServiceWkfwhis serviceWkfwhis;

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    public void setServiceWkfwhisDS(IServiceWkfwhisDS serviceWkfwhisDS)
    {
        this.serviceWkfwhisDS = serviceWkfwhisDS;
    }

    @Override
    public AppLogInfoEntity getLogInfo()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
        AppLogInfoEntity loginfo = new AppLogInfoEntity();

        String optObjecttype = ServiceConstants.getWorkflowTypeName(ServiceConstants.WKFWTYPECODE_SERVICE_ADD);
        loginfo.setOptObjecttype(optObjecttype);
        loginfo.setOptObject(wkflItem.getPboIndex().toString());

        String info;
        String logKey = "";
        if (wkflItem.getNodeId().longValue() == 1)
        {
            logKey = "ucdn.service.log.wkfw.cntdistribute.add.apply";
        }
        else
        {
            logKey += "ucdn.service.log.wkfw.cntdistribute.add.audit";
        }
        info = ResourceMgt.findDefaultText(logKey);
        if (null != info)
        {
            info = info.replace("[opername]", wkflItem.getExcutorName());
            info = info.replace("[servicename]", service.getServicename());
            info = info.replace("[nodename]", wkflItem.getNodeName());
            info = info.replace("[handleevent]", wkflItem.getHandleEvent());
        }
        loginfo.setOptDetail(info);
        loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
        return loginfo;
    }

    @Override
    public void initData()
    {
        Long serviceindex = wkflItem.getPboIndex();
        service = new Service();
        service.setServiceindex(serviceindex);

        // 查询service对象
        try
        {
            service = serviceds.getService(service);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }

        // strategy对象处理，修改正式表状态
        service.setWorkflowlife(wkflItem.getPboState());

        // 工作流对象处理，设置生效时间
        wkflItem.setEffectiveTime("0");

        // 创建工作流历史对象
        serviceWkfwhis = service.newStrategyWkfwhis();
        serviceWkfwhis.setWorkflowindex(Long.valueOf(wkflItem.getFlowInstId()));
        serviceWkfwhis.setTaskindex(Long.valueOf(wkflItem.getTaskId()));
        serviceWkfwhis.setNodeid(wkflItem.getNodeId().longValue());
        serviceWkfwhis.setNodename(wkflItem.getNodeName());
        serviceWkfwhis.setOpername(wkflItem.getExcutorId());
        serviceWkfwhis.setOpopinion(wkflItem.getComments());
        serviceWkfwhis.setTemplateindex(wkflItem.getTemplateSeqNo().longValue());
        serviceWkfwhis.setHandleevent(wkflItem.getHandleEvent());

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        if (wkflItem.getStartTime() != null)
        {
            String workflowstarttime = dateformat.format(wkflItem.getStartTime());
            serviceWkfwhis.setWorkflowstarttime(workflowstarttime);
        }
        if (wkflItem.getFinishTime() != null)
        {
            String workflowendtime = dateformat.format(wkflItem.getFinishTime());
            serviceWkfwhis.setWorkflowendtime(workflowendtime);
        }
    }

    @Override
    public void operate()
    {
        try
        {
            if (wkflItem.getPboState().intValue() == -1)
            {
                service.setStatus(130); // 状态改成审核失败
                service.setWorkflow(0); // 未启动
                service.setWorkflowlife(0); // 未启动
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                service.setEffecttime(format.format(new Date()));// 将生效时间字段设为当前时间
                serviceds.updateService(service);
            }
            else
            {
                serviceds.updateService(service);
            }
            serviceWkfwhisDS.insertServiceWkfwhis(serviceWkfwhis);
        }
        catch (DomainServiceException dsEx)
        {
            log.error("domain exception:" + dsEx);
            throw new RuntimeException(dsEx);
        }
    }

}

package com.zte.cms.cntSyncRecord.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CntSyncRecord extends DynamicBaseObject
{
    private java.lang.Long recordindex;
    private java.lang.Integer objtype;
    private java.lang.Long objindex;
    private java.lang.Integer desttype;
    private java.lang.Long destindex;
    private java.lang.Integer changeflag;
    private java.lang.String recordtime;
    private java.lang.String description;
    
	
	private java.lang.String startTime;
	private java.lang.String endTime;
	
	//
	private String objindexStr ;
	
	public String getObjindexStr() {
		return objindexStr;
	}

	public void setObjindexStr(String objindexStr) {
		this.objindexStr = objindexStr;
	}
	
	

    public java.lang.String getStartTime() {
		return startTime;
	}

	public void setStartTime(java.lang.String startTime) {
		this.startTime = startTime;
	}

	public java.lang.String getEndTime() {
		return endTime;
	}

	public void setEndTime(java.lang.String endTime) {
		this.endTime = endTime;
	}

	public java.lang.Long getRecordindex()
    {
        return recordindex;
    }

    public void setRecordindex(java.lang.Long recordindex)
    {
        this.recordindex = recordindex;
    }

    public java.lang.Integer getObjtype()
    {
        return objtype;
    }

    public void setObjtype(java.lang.Integer objtype)
    {
        this.objtype = objtype;
    }

    public java.lang.Long getObjindex()
    {
        return objindex;
    }

    public void setObjindex(java.lang.Long objindex)
    {
        this.objindex = objindex;
    }

    public java.lang.Integer getDesttype()
    {
        return desttype;
    }

    public void setDesttype(java.lang.Integer desttype)
    {
        this.desttype = desttype;
    }

    public java.lang.Long getDestindex()
    {
        return destindex;
    }

    public void setDestindex(java.lang.Long destindex)
    {
        this.destindex = destindex;
    }

    public java.lang.Integer getChangeflag()
    {
        return changeflag;
    }

    public void setChangeflag(java.lang.Integer changeflag)
    {
        this.changeflag = changeflag;
    }

    public java.lang.String getRecordtime()
    {
        return recordtime;
    }

    public void setRecordtime(java.lang.String recordtime)
    {
        this.recordtime = recordtime;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    @Override
    public void initRelation()
    {
        // TODO Auto-generated method stub

    }

	


}

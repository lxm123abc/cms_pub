package com.zte.cms.server.service;

import java.util.List;

import com.zte.cms.server.model.CmsServer;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsServerDS
{
    /**
     * 新增CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsServer(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 新增CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException;

    /**
     * 更新CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsServer(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 批量更新CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException;

    /**
     * 根据条件更新CmsServer对象
     * 
     * @param cmsServer CmsServer更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsServerByCond(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsServer对象
     * 
     * @param cmsServer CmsServer更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsServerListByCond(List<CmsServer> cmsServerList) throws DomainServiceException;

    /**
     * 删除CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsServer(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 批量删除CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsServerList(List<CmsServer> cmsServerList) throws DomainServiceException;

    /**
     * 根据条件删除CmsServer对象
     * 
     * @param cmsServer CmsServer删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsServerByCond(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsServer对象
     * 
     * @param cmsServer CmsServer删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsServerListByCond(List<CmsServer> cmsServerList) throws DomainServiceException;

    /**
     * 查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @return CmsServer对象
     * @throws DomainServiceException ds异常
     */
    public CmsServer getCmsServer(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 根据条件查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象
     * @return 满足条件的CmsServer对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsServer> getCmsServerByCond(CmsServer cmsServer) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsServer对象
     * 
     * @param cmsServer CmsServer对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsServer cmsServer, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
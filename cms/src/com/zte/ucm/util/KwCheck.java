package com.zte.ucm.util;

public class KwCheck
{

    /**
     * 处理xss攻击问题
     * @param parma 需要处理的字符串
     * @return
     */
    public static String checkXSS(String parma)
    {
        if(parma==null||parma.contains("script")){
            parma="";
        }
        return parma;
    }
    
    /**
     * 处理sv.exec攻击问题
     * @param parma 需要处理的字符串
     * @return
     */
    public static String checkExec(String parma)
    {
        if(parma==null||parma.contains("exec")){
            parma="";
        }
        return parma;
    }
    
    /**
     * 处理String为null问题
     * @param parma 需要处理的字符串
     * @return
     */
    public static String checkNull(Object parma)
    {
        if(parma==null){
            parma="";
        }
        return parma.toString();
    }
    
    /**
     * 处理sql注入的问题
     * @param value.toString() 需要处理的字符串
     * @return
     */
    public static String checkSql(Object parma)
    {
        if(parma==null||parma.toString().contains("select")){
            parma="";
        }
        return parma.toString();
    }
}

package com.zte.cms.picture.service;

import java.util.List;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICategoryPictureMapDS
{
    /**
     * 新增CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException;

    /**
     * 更新CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException;

    /**
     * 批量更新CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateCategoryPictureMapList(List<CategoryPictureMap> categoryPictureMapList)
            throws DomainServiceException;

    /**
     * 删除CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DomainServiceException;

    /**
     * 批量删除CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeCategoryPictureMapList(List<CategoryPictureMap> categoryPictureMapList)
            throws DomainServiceException;

    /**
     * 查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return CategoryPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public CategoryPictureMap getCategoryPictureMap(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException;

    /**
     * 根据条件查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return 满足条件的CategoryPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategoryPictureMap> getCategoryPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    
  /***************************海报发布管理*************************/
   
    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据条件分页查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize)
            throws DomainServiceException; 
    
    
    /**
     * 根据条件查询CategoryPictureMap对象
     * 
     * @param categoryPictureMap CategoryPictureMap对象
     * @return 满足条件的CategoryPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<CategoryPictureMap> getCategorynormalPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DomainServiceException;    
    
    /***************************海报发布管理*************************/    
}

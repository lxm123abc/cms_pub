package com.zte.cms.channel.wkfw.dao;

import java.util.List;

import com.zte.cms.channel.wkfw.model.ChannelWkfwCondition;
import com.zte.cms.channel.wkfw.model.ChannelWkfwProcessWithMe;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class ChannelWkfwProcessWithMeDAO extends DynamicObjectBaseDao implements IChannelWkfwProcessWithMeDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#queryStrategyWkfwProcessWithMeListByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public List<ChannelWkfwProcessWithMe> queryChannelWkfwProcessWithMeListByCond(
            ChannelWkfwCondition channelWkfwProcessWithMeCondition) throws DAOException
    {
        log.debug("query serviceWkfwProcessWithMeList by condition starting...");
        List<ChannelWkfwProcessWithMe> rList = (List<ChannelWkfwProcessWithMe>) super.queryForList(
                "queryChannelWkfwProcessWithMeListByCond", channelWkfwProcessWithMeCondition);
        log.debug("query serviceWkfwProcessWithMeList by condition end");
        return rList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#queryStrategyWkfwProcessWithMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    @SuppressWarnings("unchecked")
    public int queryChannelWkfwProcessWithMeListCntByCond(ChannelWkfwCondition channelWkfwProcessWithMeCondition)
            throws DAOException
    {
        log.debug("query ChannelWkfwProcessWithMeList count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryChannelWkfwProcessWithMeListCntByCond",
                channelWkfwProcessWithMeCondition)).intValue();
        log.debug("query ChannelWkfwProcessWithMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query ChannelWkfwProcessWithMeList by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryChannelWkfwProcessWithMeListCntByCond",
                channelWkfwProcessWithMeCondition)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ChannelWkfwProcessWithMe> rsList = (List<ChannelWkfwProcessWithMe>) super.pageQuery(
                    "queryChannelWkfwProcessWithMeListByCond", channelWkfwProcessWithMeCondition, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query strategyWkfwProcessWithMeList by condition end");
        return pageInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.dao.IStrategyWkfwProcessWithMeDAO#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public PageInfo pageInfoQuery(ChannelWkfwCondition channelWkfwProcessWithMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryChannelWkfwProcessWithMeListByCond",
                "queryChannelWkfwProcessWithMeListCntByCond", channelWkfwProcessWithMeCondition, start, pageSize,
                puEntity);
    }
}
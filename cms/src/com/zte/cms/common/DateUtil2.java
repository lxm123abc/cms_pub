package com.zte.cms.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.util.Util;

/**
 * @author liuxiaojing
 * @version ISMP-IPTV2.04.01 introduction: 日期格式转换及计算方法 modified by daibo
 */

public class DateUtil2
{
    private static Log log = SSBBus.getLog(DateUtil2.class);

    /**
     * 将日期如(2008-11-26 15:00:23) 转换为14位的字符串 (20081126150023)
     * 
     * @param time19
     * @return
     */
    public static String get14Time(String time19)
    {
        if (Util.isStrEmpty(time19))
        {
            return "";
        }
        else if (time19.length() == 14)
        {
            return time19;
        }
        else if (time19.length() == 10)
        {
            time19 = time19 + " 00:00:00";
        }
        else if (time19.length() != 19)
        {
            return time19;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            Date date = sdf.parse(time19);
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            log.error("get14Time" + e);
            return "";
        }
    }

    /**
     * 将日期如(2008.11.26 15:00:23) 转换为14位的字符串 (20081126150023)
     * 
     * @param time19
     * @return
     */
    public static String TransTo14Time(String time19)
    {
        if (Util.isStrEmpty(time19))
        {
            return "";
        }
        else if (time19.length() == 14)
        {
            return time19;
        }
        else if (time19.length() != 19)
        {
            return time19;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            Date date = sdf.parse(time19);
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            log.error("get14Time" + e);
            return "";
        }
    }

    /**
     * 将日期如 (20081126150023)转换为19位的字符串 (2008-11-26 15:00:23)
     * 
     * @param time19
     * @return
     */
    public static String get19Time(String time14)
    {
        if (Util.isStrEmpty(time14))
        {
            return "";
        }
        else if (time14.length() == 19)
        {
            return time14;
        }
        else if (time14.length() != 14)
        {
            return time14;
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            Date date = sdf.parse(time14);
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            log.error("get19Time" + e);
            return "";
        }
    }

    /**
     * 取得yyyyMMddHHmmss日期中的YYYYMMDD
     * 
     * @param source
     * @return
     */
    public static String getDate(String source)
    {
        if (isValidDate(source))
        {
            String tmp = source.substring(0, 8);
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
            Date date;
            try
            {
                date = sdfDate.parse(tmp);
                String endDate = sdfDate.format(date);
                return endDate;
            }
            catch (ParseException e)
            {
                log.error("getDate" + e);
                return "";
            }
        }
        else
        {
            return "";
        }

    }

    /**
     * 取得yyyyMMddHHmmss日期中的HH24MISS
     * 
     * @param source
     * @return
     */

    public static String getTime(String source)
    {
        if (isValidDate(source))
        {
            String tmp = source.substring(8, 14);
            SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");

            Date time;
            try
            {
                time = sdfTime.parse(tmp);
                String endTime = sdfTime.format(time);
                return endTime;
            }
            catch (ParseException e)
            {
                log.error("getTime................" + e);
                return "";
            }
        }
        else
        {
            return "";
        }

    }

    public static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    /**
     * 节目时长(HH24MISS) M 6
     * 
     * @param starttime
     * @param endtime
     * @return
     */
    public static String getDuration(String starttime, String endtime)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String duration = "";
        String shour = "";
        String smin = "";
        String ss = "";
        try
        {
            // 计算时间差
            Date sTime = sdf.parse(starttime);
            Date eTime = sdf.parse(endtime);
            Long l = (eTime.getTime() - sTime.getTime());
            Long day = l / (24 * 60 * 60 * 1000);
            Long hour = l / (60 * 60 * 1000) - day * 24;
            Long min = (l / (60 * 1000)) - day * 24 * 60 - hour * 60;
            Long s = (l / 1000) - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60;
            // 因为页面上已经控制时间长度为12小时，所有时间差，day=0；
            switch ((hour.toString()).length())
            {
                case 0:
                    shour = "00";
                    break;
                case 1:
                    shour = "0" + hour.toString();
                    break;
                case 2:
                    shour = hour.toString();
                    break;
                default:
                    shour = hour.toString();
                    break;
            }
            switch ((min.toString()).length())
            {
                case 0:
                    smin = "00";
                    break;
                case 1:
                    smin = "0".concat(min.toString());
                    break;
                case 2:
                    smin = min.toString();
                    break;
                default:
                    smin = min.toString();
                    break;
            }
            switch ((s.toString()).length())
            {
                case 0:
                    ss = "00";
                    break;
                case 1:
                    ss = "0".concat(s.toString());
                    break;
                case 2:
                    ss = s.toString();
                    break;
                default:
                    ss = s.toString();
                    break;
            }
            duration = shour.concat(smin).concat(ss);
            return duration;
        }

        catch (ParseException e)
        {
            log.error("getDuration++++++++++++++===" + e);
            return "";
        }

    }

    /**
     * 判断一个字符串是不是yyyyMMddHHmmss 格式
     * 
     * @param s
     */
    public static boolean isValidDate(String s)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            sdf.parse(s);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public static String getPreTime(Date time, int hours)
    {

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        // Date sourceDate = null;
        Date resultDate = null;
        try
        {
            // sourceDate = simpleFormat.parse(time);
            long sourceMillTime = time.getTime();
            long resultMillTime = sourceMillTime + hours * 60 * 60 * 1000;
            resultDate = new Date(resultMillTime);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return simpleFormat.format(resultDate);
    }

}

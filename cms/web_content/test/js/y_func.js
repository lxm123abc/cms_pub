
String.prototype.endWith = function(str) {
	if (str == null || str == "" || this.length == 0) return false;
	var strs = str.split(";");
	for (var i=0; i<strs.length; i++) {
		if ((this.length >= strs[i].length) && (this.substr(this.length - strs[i].length) == strs[i])) 
			return true;
	}
	return false;
}

String.prototype.startWith = function(str) {
	if (str == null || str == "" || this.length == 0) return false;
	var strs = str.split(";");
	for (var i=0; i<strs.length; i++) {
		if ((this.length >= strs[i].length) && (this.substr(0, strs[i].length) == strs[i])) return true;
	}
	return false;
}

function is_invalid(str) {
	if (str == null || str == "") return true;
	if ($.trim(str) == "") return true;
	return false;
}
	
function is_numcode(str) {
	if (str == null || str == "") return true;
	var myReg = /^(0|([1-9]\d*))$/;
	return myReg.test(str);
}

function is_english(str) {
	if (str == null || str == "") return true;
	var myReg = /^([a-zA-Z]*)$/;
	return myReg.test(str);
}

function getLens(str) {
	return str.replace(/[^\x00-\xff]/gi, 'zch').length;
}

function getZchLens(maxchar) {
	return Math.floor(maxchar / 3);
}

function getDT(fmt) {
	var d = new Date();
	if (fmt == "utc") return d.getTime();
	var sy,sm,sd,sh,smi,ss,ms;
	sy = d.getYear(); if (sy < 1000) sy = "" + (1900 + sy);
	sm = d.getMonth()+1; if (sm < 10) sm = "0" + sm;
	sd = d.getDate(); if (sd < 10) sd = "0" + sd;
	if (fmt == 'yyyy-mm-dd') return sy+"-"+sm+"-"+sd;
	if (fmt == 'yyyymmdd') return sy+""+sm+""+sd;
	sh = d.getHours(); if (sh < 10) sh = "0" + sh;
	smi = d.getMinutes(); if (smi < 10) smi = "0" + smi;
	ss = d.getSeconds(); if (ss < 10) ss = "0" + ss;
	if (fmt == 'yyyy-mm-dd hh:mm:ss') return sy+"-"+sm+"-"+sd+" "+sh+":"+smi+":"+ss;
	if (fmt == 'yyyymmddhhmmss') return sy+""+sm+""+sd+""+sh+""+smi+""+ss;
	ms = d.getMilliseconds(); if (ms < 10) {ms = "00" + ms;} else if (ms < 100) {ms = "0" + ms;}
	return sy+""+sm+""+sd+""+sh+""+smi+""+ss +""+ms;
}

function isNull(obj) {
	if (typeof(obj) == "undefined") return true;
	if (typeof(obj) == "object") return obj == null;
	return false;
}

function isEmpty(obj) {
	if (typeof(obj) == "undefined") return true;
	if (typeof(obj) == "object") return obj == null || JSON.stringify(obj)=="{}" || JSON.stringify(obj)=="[]";
	if (typeof(obj) == "string") return obj.length==0;
	return false;
}

function nvl(str, ishtml) {
	if (str == undefined || str.length == 0) return "";
	if (ishtml) {
		try {
			str = str.replace(/\'/g, "&#39;");
		}
		catch (ex) {}
	}
	return str;
}

function bol(obj) {
	if (isEmpty(obj)) return false;
	return obj;
}

function getTypeSymbol(ctype, cstatus) {
	var str = "O";
	if (cstatus != undefined) {
		str = (cstatus == "0" || cstatus.toUpperCase() == "N") ? "*" : "";
		str = str + ctype.charAt(0).toUpperCase();
		return str;
	}
	if (ctype == "java.lang.Boolean" || ctype == "boolean") return "B"; 
	if (ctype == "java.lang.Integer" || ctype == "int") return "I"; 
	if (ctype == "java.lang.Long" || ctype == "long") return "L"; 
	if (ctype == "java.lang.Double" || ctype == "double") return "D"; 
	if (ctype == "java.lang.String") return "S";
	if (ctype == "java.util.Date") return "T"; 
	return str;
}

function getTypeDefVal(ctype, cdefault) {
	var str = "";
	if (cdefault != undefined) {
		str = (cdefault == null) ? "" : cdefault.replace(/DEFAULT|\s|\'*/g, '');
		if (str.indexOf("convert") != -1 || str.indexOf("to_char") != -1) str = getDT("yyyymmddhhmmss");
		return str;
	}
	if (ctype == "java.lang.Boolean" || ctype == "boolean") return "true"; 
	if (ctype == "java.lang.Integer" || ctype == "int") return "0"; 
	if (ctype == "java.lang.Long" || ctype == "long") return "0"; 
	if (ctype == "java.lang.Double" || ctype == "double") return "0.0"; 
	if (ctype == "java.lang.String") return "";
	if (ctype == "java.util.Date") return getDT("utc");
	return str;
}

function getTypeExpress(ptype) {
	var str = "S";
	if (ptype.charAt(0) == '&' || ptype.charAt(0) == '[' || ptype == "java.util.List") return "O";
	if (ptype.startWith("com.;net.;org.")) return "O";
	return str;
}

function getObjectArray(obj, dataval) {
	if (obj instanceof Array) {
		if (obj.length == 0) return obj;
		if (typeof(obj[0]) != "object") {
			for (var i=0; i<obj.length; i++) {
				var p = {}; p["rtnval"] = obj[i]; obj[i] = p;
			}
			return obj;
		}
		return obj;
	}
	if (typeof(obj) != "object") {
		var p = {}; p["rtnval"] = obj; obj = p;
	}
	if (obj[dataval] != undefined) {
		return obj[dataval];
	}
	var arr = []; arr[0] = obj; obj = arr; 
	return obj;
}

function getJsonVal(str) {
	var obj = str;
	if (obj == undefined) return "";
	var myReg = /^((\[|\{).*(\]|\}))$/;
	if (myReg.test(str)) {
		try {obj = eval("(" + str + ")");} catch(ex) {}
	}
	return obj;
}

function createJsonStr(obj, ishtml, nullval) {
	var str = "";
	if (obj == undefined) { // null also equal defined
		return str;
	}
	else if (typeof(obj) != "object") {
		str = obj;
	}
	else if (obj instanceof Array) {
		str = JSON.stringify(obj);
	}
	else if (obj.length != undefined) {
		var objs = [];
		for (var i=0; i<obj.length; i++)  objs[i] = obj[i];
		str = JSON.stringify(objs);
	}
	else if (nullval != undefined && nullval.length > 0) {
		obj[nullval] = undefined;
		str = JSON.stringify(obj);
	}
	else {
		str = JSON.stringify(obj);
	}
	if (ishtml) {
		try {
			str = str.replace(/\'/g, "&#39;");
		}
		catch (ex) {}
	}
	return str;
}
package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.model.IcmSourcefile;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IIcmSourcefileDAO
{
    /**
     * 新增IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DAOException dao异常
     */
    public void insertIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据主键更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DAOException dao异常
     */
    public void updateIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据条件更新IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile更新条件
     * @throws DAOException dao异常
     */
    public void updateIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据主键删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @throws DAOException dao异常
     */
    public void deleteIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据条件删除IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile删除条件
     * @throws DAOException dao异常
     */
    public void deleteIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据主键查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @return 满足条件的IcmSourcefile对象
     * @throws DAOException dao异常
     */
    public IcmSourcefile getIcmSourcefile(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据条件查询IcmSourcefile对象
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @return 满足条件的IcmSourcefile对象集
     * @throws DAOException dao异常
     */
    public List<IcmSourcefile> getIcmSourcefileByCond(IcmSourcefile icmSourcefile) throws DAOException;

    /**
     * 根据条件分页查询IcmSourcefile对象，作为查询条件的参数
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询IcmSourcefile对象，作为查询条件的参数
     * 
     * @param icmSourcefile IcmSourcefile对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(IcmSourcefile icmSourcefile, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
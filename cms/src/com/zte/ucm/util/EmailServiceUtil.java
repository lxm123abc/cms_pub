package com.zte.ucm.util;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;



import org.apache.commons.lang.StringUtils;

import com.zte.ismp.common.ConfigUtil;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

import net.sourceforge.jtds.jdbc.ColInfo;


public class EmailServiceUtil{

    // 日志
    private static Log logger = SSBBus.getLog(EmailServiceUtil.class);

    private MimeMessage mimeMsg; // MIME邮件对象

    private Session session; // 邮件会话对象

    private Properties props; // 系统属性

    private boolean needAuth = false; // smtp是否需要认证

    private String username = ""; // smtp认证用户名和密码

    private String password = "";

    private Multipart mp; // Multipart对象,邮件内容,标题,附件等内容均添加到其中后再生成

    public EmailServiceUtil(){};

    public EmailServiceUtil(String smtp,String port) {
        setSmtpHost(smtp);
        setSmtpPort(port);
        createMimeMessage();
    }

    public void setSmtpHost(String hostName) {
        logger.info("class EmailServiceUtil set system value：mail.smtp.host = " + hostName);
        if (props == null)
            props = System.getProperties(); // 获得系统属性对象
        props.put("mail.smtp.host", hostName); // 设置SMTP主机
    }

    public void setSmtpPort(String port) {
        logger.info("class EmailServiceUtil set system value：mail.smtp.port = " + port);
        if (props == null)
            props = System.getProperties(); // 获得系统属性对象
        props.put("mail.smtp.port", port);  // 设置SMTP端口
    }

    public boolean createMimeMessage() {
        try {
            logger.info("class EmailServiceUtil get email session object");
            session = Session.getDefaultInstance(props, null); // 获得邮件会话对象
            session.setDebug(true);
        } catch (Exception e) {
            logger.error("class EmailServiceUtil get email session object error " + e);
            return false;
        }
        logger.info("class EmailServiceUtil create MIME email object");
        try {
            mimeMsg = new MimeMessage(session); // 创建MIME邮件对象
            mp = new MimeMultipart(); // mp 一个multipart对象
            // Multipart is a container that holds multiple body parts.
            return true;
        } catch (Exception e) {
            logger.error("class EmailServiceUtil create MIME email object error " + e);
            return false;
        }
    }

    public void setNeedAuth(boolean need) {
        logger.info("class EmailServiceUtil set smtp valid code：mail.smtp.auth = " + need);
        if (props == null)
            props = System.getProperties();
        if (need) {
            props.put("mail.smtp.auth", "true");
        } else {
            props.put("mail.smtp.auth", "false");
        }
    }

    public void setNamePass(String name, String pass) {
        logger.info("class EmailServiceUtil get user name and user password");
        username = name;
        password = pass;
    }

    public boolean setSubject(String mailSubject) {
        logger.info("class EmailServiceUtil set email theme");
        try {
            mimeMsg.setSubject(MimeUtility.encodeText(mailSubject, "UTF-8", "B"));
            return true;
        } catch (Exception e) {
            logger.error("class EmailServiceUtil set email theme error");
            return false;
        }
    }

    public boolean setBody(String mailBody) {
        try {
            logger.info("class EmailServiceUtil set email body part");
            BodyPart bp = new MimeBodyPart();
            // 转换成中文格式
            bp.setContent("<meta http-equiv=Content-Type content=text/html; charset=utf-8>" + mailBody,
                    "text/html;charset=utf-8");
            mp.addBodyPart(bp);
            return true;
        } catch (Exception e) {
            logger.error("class EmailServiceUtil set email body part error " + e);
            return false;
        }
    }

    //附件添加
    public boolean setAffix(String affix, String affixName)
    {
        try {
            logger.info("class EmailServiceUtil set email affix body part");
            BodyPart bp = new MimeBodyPart();
            DataSource source = new FileDataSource(affix);
            // 添加附件的内容
            bp.setDataHandler(new DataHandler(source));
            if("".equals(affixName))
            {
                affixName = source.getName();
            }
            sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
            bp.setFileName("=?GBK?B?" + enc.encode(affixName.getBytes()) + "?=");
            mp.addBodyPart(bp);
            return true;
        } catch (Exception e) {
            logger.error("class EmailServiceUtil set email affix body part error " + e);
            return false;
        }
    }

    public boolean setFrom(String nick,String from) {
        logger.info("class EmailServiceUtil  set send email address");
        try {
            if(null == nick || nick.length() == 0){
                mimeMsg.setFrom(new InternetAddress(from)); // 设置发信人
            } else {
                mimeMsg.setFrom(new InternetAddress(MimeUtility.encodeText(nick, "UTF-8", "B")+" <"+from+">")); // 设置发信人
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean setTo(String to) {
        logger.info("class EmailServiceUtil set recipients email address");
        if (to == null)
            return false;
        try {
            mimeMsg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean setCopyTo(String copyto) {
        if (copyto == null)
            return false;
        try {
            mimeMsg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(copyto));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean sendout() {
        try {
            mimeMsg.setContent(mp);
            mimeMsg.saveChanges();
            logger.info("class EmailServiceUtil sending email......");
            props.put("mail.smtp.ssl.enable", true);
            props.put("mail.smtp.starttls.enable", false);
            props.put("mail.debug", "true");
            props.put("mail.transport.protocol", "smtp");// 连接协议
            props.put("mail.smtp.host", (String) props.get("mail.smtp.host"));// 主机名
            props.put("mail.smtp.localhost", "localhost");// 添加对应本机IP
            props.put("mail.smtp.port", Integer.parseInt(String.valueOf(props.get("mail.smtp.port"))));// 端口号

            Session mailSession = Session.getInstance(props);
            Transport transport = mailSession.getTransport();
            transport.connect(username, password);
            transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.TO));
            // transport.send(mimeMsg);
            logger.info("class EmailServiceUtil send email success");
            transport.close();
            return true;
        } catch (Exception e) {
            logger.error("class EmailServiceUtil send email fail e:"+e);
            logger.error("class EmailServiceUtil send email fail getStackTrace:" + e.getStackTrace());
            logger.error("class EmailServiceUtil send email fail getMessage:" + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @name:send
     * @description:
     * @param title 主题
     * @param content 内容
     * @param tomail 收件人
     * @param affix 附件路径
     * @param affixName 附件名称
     * @exception
     * @since  1.0.0
     */
    public static boolean send(String title, String content, String tomail, String affix, String affixName,String emailSmtpInfo)
    {
        String[] mailSmtpInfo = emailSmtpInfo.split(";");
        String smtphost = mailSmtpInfo[0];
        String smtpport =mailSmtpInfo[1];
        String frommail = mailSmtpInfo[2];
        String frommailpwd = mailSmtpInfo[3];
        String nick = mailSmtpInfo[4];

        if(null == smtphost || smtphost.length() < 6){
            logger.error("--- please set smtphost ------");
            return false;
        }
        if(null != frommail && frommail.length() > 0 && null != frommailpwd && frommailpwd.length() > 0)
        {
            if(null == smtpport || smtpport.length() == 0){
                smtpport = "25";
            }
            EmailServiceUtil themail = new EmailServiceUtil(smtphost,smtpport);
            themail.setNeedAuth(true);
            themail.setSubject(title);
            themail.setBody(content);
            if(!"".equals(affix) && null != affix)
            {
                themail.setAffix(affix, affixName);
            }
            logger.info(" set to mail == " + tomail);
            themail.setTo(tomail);

            themail.setFrom(nick, frommail);
            themail.setNamePass(frommail, frommailpwd);
            return themail.sendout();
        }
        logger.info("--- please set smtphost,frommail,frommailpwd ------");
        return false;
    }
    /**
     * @name:send
     * @description:
     * @param title 主题
     * @param content 内容
     * @param tomail 收件人
     * @param emailSmtpInfo 邮箱服务器信息
     * @exception
     * @since  1.0.0
     */
    public static boolean send(String title, String content, String tomail,String emailSmtpInfo) {
        //System.out.println("发送邮件给："+tomail+"，标题是："+title+"，内容是："+content+",接口是："+emailSmtpInfo);
        return send(title, content, tomail, "", "",emailSmtpInfo);
    }

    public static void main(String[] args)
    {
        //smtp.139.com;465;18301494080@139.com;Ll7057287;18301494080@139.com
        //smtp.qq.com;465;505941855@qq.com;thlmcuwclnticade;505941855@qq.com
        EmailServiceUtil em = new EmailServiceUtil();
        em.send("", "", "505941855@qq.com,",
                "smtp.qq.com;25;505941855@qq.com;thlmcuwclnticade;505941855@qq.com");


    }


}

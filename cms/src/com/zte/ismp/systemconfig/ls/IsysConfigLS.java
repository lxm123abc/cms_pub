package com.zte.ismp.systemconfig.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ismp.common.util.DbUtil;
import com.zte.ismp.systemconfig.common.LogCommon;
import com.zte.jwf.workflowDefine.access.cache.WfwActivetynodeCache;
import com.zte.jwf.workflowDefine.business.templateAttrMaintain.model.WfwActivetynode;
import com.zte.jwf.workflowDefine.business.templateAttrMaintain.model.WfwEmailnode;
import com.zte.jwf.workflowDefine.business.templateAttrMaintain.service.intf.WfwActivetynodeDomainService;
import com.zte.jwf.workflowDefine.business.templateAttrMaintain.service.intf.WfwEmailnodeDomainService;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class IsysConfigLS implements IIsysConfigLS
{

    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IUsysConfigDS ds = null;

    public void setDs(IUsysConfigDS ds)
    {
        this.ds = ds;
    }

    public String updateUsysConfig(UsysConfig usysConfig) throws DomainServiceException
    {
        log.debug("update usysConfig by pk starting...");
        UsysConfig config = new UsysConfig();
        config.setCfgkey(usysConfig.getCfgkey());
        try
        {
            config = ds.getUsysConfig(config);
            if (config == null)
            {
                return "1:" + ResourceManager.getResourceText("msg.info.sys.config.3400202");
            }
            ds.updateUsysConfig(usysConfig);

            // 判断是否为工作流通知配置参数
            if ("cms.cntaudit1.autoalarm".equals(usysConfig.getCfgkey()))
            {// 一审
                this.setAutoalarm(1, usysConfig.getCfgvalue());
            }
            if ("cms.cntaudit2.autoalarm".equals(usysConfig.getCfgkey()))
            {// 二审
                this.setAutoalarm(2, usysConfig.getCfgvalue());
            }
            if ("cms.cntaudit3.autoalarm".equals(usysConfig.getCfgkey()))
            {// 三审
                this.setAutoalarm(3, usysConfig.getCfgvalue());
            }
        }
        catch (DomainServiceException e)
        {
            // 记录业务日志
            LogCommon.addAppLog(usysConfig.getCfgkey(), 2, 6, usysConfig.getCfgdesc(), false);
            e.printStackTrace();
            throw e;
        }
        LogCommon.addAppLog(usysConfig.getCfgkey(), 2, 6, usysConfig.getCfgdesc(), true);
        log.debug("update usysConfig by pk end");
        return "0:" + ResourceManager.getResourceText("msg.info.sys.config.3400201");
    }

    public UsysConfig getUsysConfig(UsysConfig usysConfig) throws DomainServiceException
    {
        log.debug("get usysConfig by pk starting...");
        UsysConfig rsObj = null;
        rsObj = ds.getUsysConfig(usysConfig);
        log.debug("get usysConfigList by pk end");
        return rsObj;
    }

    public List<UsysConfig> getUsysConfigByCond(UsysConfig usysConfig) throws DomainServiceException
    {
        log.debug("get usysConfig by condition starting...");
        List<UsysConfig> rsList = null;
        rsList = ds.getUsysConfigByCond(usysConfig);
        log.debug("get usysConfig by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(UsysConfig usysConfig, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get usysConfig page info by condition starting...");
        TableDataInfo tableInfo = new TableDataInfo();
        usysConfig.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        tableInfo = ds.pageInfoQuery(usysConfig, start, pageSize);
        log.debug("get usysConfig page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(UsysConfig usysConfig, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get usysConfig page info by condition starting...");
        usysConfig.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo = ds.pageInfoQuery(usysConfig, start, pageSize, puEntity);
        log.debug("get usysConfig page info by condition end");
        return tableInfo;
    }

    public String getUsysConfigByCfgkey(String cfgkey) throws DomainServiceException
    {
        log.debug("get getUsysConfigByCfgkey by pk starting...");
        UsysConfig rsObj = null;
        UsysConfig usysConfig = new UsysConfig();
        usysConfig.setCfgkey(cfgkey);
        usysConfig.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        rsObj = ds.getUsysConfig(usysConfig);
        if (rsObj == null)
        {
            log.error("cfgkey=" + cfgkey + " ,Object is null");
            return null;
        }
        log.debug("get getUsysConfigByCfgkey by pk end");
        return rsObj.getCfgvalue();
    }

    /**
     * <p>
     * 根据系统参数配置邮件自动提醒功能
     * </p>
     * 
     * @param auditLevel 审核级别 1：一审；2：二审；3：三审 sendMail 是否邮件通知 Y:是；N:否
     * @throws DomainServiceException 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @since ZXIN10 CMSV2.01.01
     */
    @SuppressWarnings("unchecked")
    private void setAutoalarm(int auditLevel, String sendMail) throws DomainServiceException
    {// 设置工作流通知配置 内容新增和修改模板
        StringBuffer sqlAdd = new StringBuffer();
        StringBuffer sqlMod = new StringBuffer();
        String nodeidAdd = "";
        String nodeidMod = "";
        if (1 == auditLevel)
        {// 一审
            nodeidAdd = "7";
            nodeidMod = "5";
        }
        else if (2 == auditLevel)
        {// 二审
            nodeidAdd = "12";
            nodeidMod = "9";
        }
        else
        {// 三审
            nodeidAdd = "16";
            nodeidMod = "13";
        }
        // 内容新增模板
        sqlAdd.append("update zxjwf.wfw_activetynode n set n.sendmail='");
        sqlAdd.append(sendMail);
        sqlAdd.append("' where n.node_id=");
        sqlAdd.append(nodeidAdd);
        sqlAdd.append(" and n.seq_no=1318691534921");
        // 内容修改模板
        sqlMod.append("update zxjwf.wfw_activetynode n set n.sendmail='");
        sqlMod.append(sendMail);
        sqlMod.append("' where n.node_id=");
        sqlMod.append(nodeidMod);
        sqlMod.append(" and n.seq_no=1319100709046");
        DbUtil dbUtil = new DbUtil();
        List<String> sqlList = new ArrayList<String>();
        sqlList.add(sqlAdd.toString());
        sqlList.add(sqlMod.toString());
        try
        {
            dbUtil.excute(sqlList);
            WfwActivetynodeCache.wfwActivetynodeCache.clear();
            WfwActivetynodeDomainService wfwActivetynodeDomainService = (WfwActivetynodeDomainService)SSBBus.findDomainService("wfwActivetynodeDomainService");
            List<WfwActivetynode> wfwActivetynodelist = wfwActivetynodeDomainService.getWfwActivetynodesForCache();
            WfwActivetynode wfwActivetynode;
            String pkey;
            for(Iterator<WfwActivetynode> i$ = wfwActivetynodelist.iterator(); i$.hasNext(); WfwActivetynodeCache.wfwActivetynodeCache.put(pkey, wfwActivetynode))
            {
                wfwActivetynode = (WfwActivetynode)i$.next();
                pkey = (new StringBuilder()).append(wfwActivetynode.getNodeId().toString()).append(wfwActivetynode.getSeqNo().toString()).toString();
                log.debug("nodeid=" + wfwActivetynode.getNodeId().toString() + ", seq_no=" + wfwActivetynode.getSeqNo().toString() + ", sendmail=" + wfwActivetynode.getSendmail());
            }    
        }
        catch (Exception e)
        {
            throw new DomainServiceException(e);
        }
    }

}

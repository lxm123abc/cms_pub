package com.zte.cms.programcrmmap.model;

public class ProgramCrmMapConstant
{
    // mgttype 管理类型 VOD内容与角色管理

    public static final String MGTTYPE_PROGRAMCRMMAP = "log.programcrmmap.mgt";

    public final static int PROGRAMCRMMAP_STATUS_WAITPUBLISH = 0;// 待发布
    public final static int PROGRAMCRMMAP_STATUS_ADDSYNCHRONIZING = 10;// 新增同步中
    public final static int PROGRAMCRMMAP_STATUS_ADDSYNCHRONIZED_SUCCESS = 20;// 新增同步成功
    public final static int PROGRAMCRMMAP_STATUS_ADDSYNCHRONIZED_FAIL = 30;// 新增同步失败
    public final static int PROGRAMCRMMAP_STATUS_UPDATESYNCHRONIZING = 40;// 修改同步中
    public final static int PROGRAMCRMMAP_STATUS_UPDATESYNCHRONIZED_SUCCESS = 50;// 修改同步成功
    public final static int PROGRAMCRMMAP_STATUS_UPDATESYNCHRONIZED_FAIL = 60;// 修改同步失败
    public final static int PROGRAMCRMMAP_STATUS_CANCELSYNCHRONIZING = 70;// 取消同步中
    public final static int PROGRAMCRMMAP_STATUS_CANCELSYNCHRONIZED_SUCCESS = 80;// 取消同步成功
    public final static int PROGRAMCRMMAP_STATUS_CANCELSYNCHRONIZED_FAIL = 90;// 取消同步失败

    /**
     * opertype 操作类型（String)
     */
    public static final String OPERTYPE_PROGRAMCRMMAP_ADD = "log.programcrmmap.add";// 新增VOD内容绑定角色
    public static final String OPERTYPE_PROGRAMCRMMAP_DEL = "log.programcrmmap.delete";// 删除VOD内容绑定角色
    public static final String OPERTYPE_PROGRAMCRMMAP_PUBLISH = "log.programcrmmap.publish";
    public static final String OPERTYPE_PROGRAMCRMMAP_CANCELPUBLISH = "log.programcrmmap.cancelpublish";
    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_PROGRAMCRMMAP_ADD_INFO = "log.programcrmmap.add.info";
    public static final String OPERTYPE_PROGRAMCRMMAP_DEL_INFO = "log.programcrmmap.delete.info";
    public static final String OPERTYPE_PROGRAMCRMMAP_PUBLISH_INFO = "log.programcrmmap.publish.info";
    public static final String OPERTYPE_PROGRAMCRMMAP_CANCELPUBLISH__INFO = "log.programcrmmap.cancelpublish.info";

}

package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.dao.IPictureDAO;
import com.zte.cms.picture.model.Picture;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class PictureDAO extends DynamicObjectBaseDao implements IPictureDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertPicture(Picture picture) throws DAOException
    {
        log.debug("insert picture starting...");
        super.insert("insertPicture", picture);
        log.debug("insert picture end");
    }

    public void updatePicture(Picture picture) throws DAOException
    {
        log.debug("update picture by pk starting...");
        super.update("updatePicture", picture);
        log.debug("update picture by pk end");
    }

    public void deletePicture(Picture picture) throws DAOException
    {
        log.debug("delete picture by pk starting...");
        super.delete("deletePicture", picture);
        log.debug("delete picture by pk end");
    }

    public Picture getPicture(Picture picture) throws DAOException
    {
        log.debug("query picture starting...");
        Picture resultObj = (Picture) super.queryForObject("getPicture", picture);
        log.debug("query picture end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByCond(Picture picture) throws DAOException
    {
        log.debug("query picture by condition starting...");
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByCond", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    // 关联对象类型:1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByServiceIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByServiceIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByCatagoryIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByCatagoryIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByProgramIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByProgramIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureBySeriesIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListBySeriesIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByChannelIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByChannelIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    public List<Picture> queryChannelPictureListByChnanelindex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryChannelPictureListByChnanelindex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByCastIndex(Long index) throws DAOException
    {
        log.debug("query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(index);
        List<Picture> rList = (List<Picture>) super.queryForList("queryPictureListByCastIndex", picture);
        log.debug("query picture by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryService(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByServiceIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByServiceIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryCatagory(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByCatagoryIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByCatagoryIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryProgram(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByProgramIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByProgramIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerySeries(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntBySeriesIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListBySeriesIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryChannel(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByChannelIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByChannelIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryCast(Long relativeIndex, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByCastIndex", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByCastIndex", picture, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Picture picture, int start, int pageSize) throws DAOException
    {
        log.debug("page query picture by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryPictureListCntByCond", picture)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<Picture> rsList = (List<Picture>) super.pageQuery("queryPictureListByCond", picture, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query picture by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(Picture picture, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryPictureListByCond", "queryPictureListCntByCond", picture, start, pageSize,
                puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        log.debug("page query picture by Type starting...");
        Picture picture = new Picture();
        picture.setObjindex(relativeIndex);
        PageInfo pageInfo = null;
        switch (objType.intValue())
        {
            case PictureConstants.SERVIEC:
                pageInfo = super.indexPageQuery("queryPictureListByServiceIndex", "queryPictureListCntByServiceIndex",
                        picture, start, pageSize, puEntity);
                break;
            case PictureConstants.CATAGORY:
                pageInfo = super.indexPageQuery("queryPictureListByCatagoryIndex",
                        "queryPictureListCntByCatagoryIndex", picture, start, pageSize, puEntity);
                break;
            case PictureConstants.PROGRAM:
                pageInfo = super.indexPageQuery("queryPictureListByProgramIndex", "queryPictureListCntByProgramIndex",
                        picture, start, pageSize, puEntity);
                break;
            case PictureConstants.SERIES:
                pageInfo = super.indexPageQuery("queryPictureListBySeriesIndex", "queryPictureListCntBySeriesIndex",
                        picture, start, pageSize, puEntity);
                break;
            case PictureConstants.CHANNEL:
                pageInfo = super.indexPageQuery("queryPictureListByChannelIndex", "queryPictureListCntByChannelIndex",
                        picture, start, pageSize, puEntity);
                break;
            case PictureConstants.CAST:
                pageInfo = super.indexPageQuery("queryPictureListByCastIndex", "queryPictureListCntByCastIndex",
                        picture, start, pageSize, puEntity);
                break;
            default:
                pageInfo = new PageInfo();
                break;
        }

        log.debug("page query picture by Type end");
        return pageInfo;
    }
}
package com.zte.cms.castrolemap.crmsync.dao;

import java.util.List;

import com.zte.cms.castrolemap.crmsync.model.CrmPlatformSync;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CrmPlatformSyncDAO extends DynamicObjectBaseDao implements ICrmPlatformSyncDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    
    public PageInfo pageInfoQuery(CrmPlatformSync crmPlatformSync, int start, int pageSize)throws DAOException
    {
        log.debug("page query crmPlatformSync by condition starting...");
        PageInfo pageInfo = null;
        if (crmPlatformSync.getTypePlatform().equals("iptv3") )
        {
            int totalCnt = ((Integer) super.queryForObject("queryCrmPlatfSynListCntByCond",  crmPlatformSync)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCrmPlatfSynListByCond" ,  crmPlatformSync , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }
            
        }
        if (crmPlatformSync.getTypePlatform().equals("iptv2") )
        {

            int totalCnt = ((Integer) super.queryForObject("queryCrmPlatfSyn2ListCntByCond",  crmPlatformSync)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCrmPlatfSyn2ListByCond" ,  crmPlatformSync , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }
            
        }

        log.debug("page query crmPlatformSync by condition end");
        return pageInfo;
    }
    
    
    public CrmPlatformSync getCrmPlatformSync( CrmPlatformSync crmPlatformSync )throws DAOException
    {
        log.debug("query crmPlatformSync starting...");
        CrmPlatformSync resultObj = (CrmPlatformSync)super.queryForObject( "getCrmPlatformSyn",crmPlatformSync);
        log.debug("query crmPlatformSync end");
        return resultObj;
    }

}

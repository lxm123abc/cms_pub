package com.zte.cms.ftptask.ls;

import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.model.CmsFtptaskAndHis;
import com.zte.cms.ftptask.service.ICmsFtptaskDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;

public class CmsFtptaskLS implements ICmsFtptaskLS
{
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CPSP, getClass());
    private ICmsFtptaskDS cmsFtptaskDS;

    public TableDataInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskAndHis, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("pageCmsFtptaskHisInfoQuery starting...");
        TableDataInfo dataInfo = null;
        try
        {
            if (cmsFtptaskAndHis.getFileid() != null && !cmsFtptaskAndHis.getFileid().equals(""))
            {
                String fileid = EspecialCharMgt.conversion(cmsFtptaskAndHis.getFileid());
                cmsFtptaskAndHis.setFileid(fileid);
            }

            cmsFtptaskAndHis.setStartDate1(DateUtil2.get14Time(cmsFtptaskAndHis.getStartDate1()));
            cmsFtptaskAndHis.setStartDate2(DateUtil2.get14Time(cmsFtptaskAndHis.getStartDate2()));
            cmsFtptaskAndHis.setEndDate1(DateUtil2.get14Time(cmsFtptaskAndHis.getEndDate1()));
            cmsFtptaskAndHis.setEndDate2(DateUtil2.get14Time(cmsFtptaskAndHis.getEndDate2()));

            dataInfo = cmsFtptaskDS.pageCmsFtptaskHisInfoQuery(cmsFtptaskAndHis, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("CmsFtptaskLS exception:" + e.getMessage());
        }
        log.debug("pageCmsFtptaskHisInfoQuery end");
        return dataInfo;
    }

    public String restartFtptask(Long taskindex) throws DomainServiceException
    {
        log.debug("restartFtptask starting...");
        String result = ResourceMgt.findDefaultText("oper.success.with.0");//"0:操作成功"
        CmsFtptask cmsFtptaskHis = null;
        try
        {
            cmsFtptaskHis = new CmsFtptask();
            cmsFtptaskHis.setTaskindex(taskindex);
            cmsFtptaskHis = cmsFtptaskDS.getCmsFtptaskHis(cmsFtptaskHis);

            if (cmsFtptaskHis == null || cmsFtptaskHis.getStatus() != 3)
            {
                return ResourceMgt.findDefaultText("oper.fail.movetask.status.wrong");//"1:操作失败,迁移任务状态不正确"
            }

            CmsFtptask cmsFtptask = new CmsFtptask();
            cmsFtptask.setContentindex(cmsFtptaskHis.getContentindex());
            cmsFtptask.setTasktype(cmsFtptaskHis.getTasktype());
            cmsFtptask.setUploadtype(cmsFtptaskHis.getUploadtype());
            cmsFtptask.setFileindex(cmsFtptaskHis.getFileindex());
            cmsFtptask.setSrcaddress(cmsFtptaskHis.getSrcaddress());
            cmsFtptask.setDestaddress(cmsFtptaskHis.getDestaddress());
            cmsFtptask.setStatus(0);
            cmsFtptask.setSrcfilehandle(0); // 源文件不处理
            cmsFtptask.setFiletype(cmsFtptaskHis.getFiletype());
            if (cmsFtptaskHis.getTasktype() == 3)
                cmsFtptask.setBatchid(cmsFtptaskHis.getBatchid());

            cmsFtptaskDS.insertCmsFtptask(cmsFtptask);
            cmsFtptaskDS.removeCmsFtptaskHis(cmsFtptaskHis);

        }
        catch (Exception e)
        {
            log.error("CmsFtptaskLS exception:" + e.getMessage());
        }
        log.debug("restartFtptask end");
        return result;
    }

    public void setCmsFtptaskDS(ICmsFtptaskDS cmsFtptaskDS)
    {
        this.cmsFtptaskDS = cmsFtptaskDS;
    }
}

package com.zte.cms.notice.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.zte.cms.common.UpLoadResult;
import com.zte.cms.egptemplt.service.IEpgtempltDS;
import com.zte.ismp.systemconfig.ls.IIsysConfigLS;
import com.zte.ismp.systemconfig.ls.IsysConfigLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;

public class FtpAttachment {

    //文件上传的缓冲区大小
    private static final int BUF_SIZE = 1024 * 1024;
    private Log log = SSBBus.getLog(getClass());
    private static String path = "/home/zxin10/CmsNoticeAttachment/";

    /**
     * @param newfilename 上传文件的新文件名
     * @return
     * @throws IOException
     * @throws DomainServiceException
     */
    public static void upload2FTP(String newfilename) throws DomainServiceException, IOException {
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        //通过系统配置项获取目标FTP服务器的ip port user password
        IIsysConfigLS configLs = (IIsysConfigLS) SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.noticeAttachment.ftpaddress");
        String user = null;
        String password = null;
        String host = null;
        int port = 21;
        FTPClient ftpConn = new FTPClient();
        InputStream in = null;
        try {
            ftpaddress = ftpaddress.substring(ftpaddress.lastIndexOf("/") + 1);
            String[] parm = ftpaddress.split("@");
            if (parm.length == 1) {
                throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
            } else {
                String[] userInfo = parm[0].split(":");
                String[] address = parm[1].split(":");
                if (userInfo.length == 1 || address.length == 1) {
                    throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
                } else {
                    user = userInfo[0];
                    password = userInfo[1];
                    host = address[0];
                    port = Integer.parseInt(address[1].trim());
                }
            }
            //流方式上传文件到FTP服务器
            if (fileList != null && fileList.size() > 0) {
                //获取文件上传控件上传的文件流，并将其转化为InputStream
                Iterator<FileItem> iter = fileList.iterator();
                FileItem tmp = (FileItem) iter.next();
                in = tmp.getInputStream();
                //将文件流上传到FTP服务器上
                ftpConn.connect(host, port);
                ftpConn.login(user, password);
                //判断FTP连接是否成功，失败则抛出异常
                int reply = ftpConn.getReplyCode();
                if (!FTPReply.isPositiveCompletion(reply)) {
                    ftpConn.logout();
                    ftpConn.disconnect();
                    throw new Exception("FTP connect refused");
                }
                //判断目录是否存在，如果不存在则创建
                if (!isDirExist(ftpConn)) {
                    ftpConn.makeDirectory(path);
                }
                ftpConn.changeWorkingDirectory(path);
                ftpConn.setFileType(FTP.BINARY_FILE_TYPE);
                ftpConn.enterLocalPassiveMode();
                ftpConn.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
                ftpConn.setBufferSize(BUF_SIZE);
                String targetPath = path + newfilename;
                ftpConn.storeFile(targetPath, in);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
            if (ftpConn.isConnected()) {
                ftpConn.logout();
                ftpConn.disconnect();
            }
        }
    }

    /**
     * @param newfilename 上传文件的新文件名
     * @return
     * @throws IOException
     * @throws DomainServiceException
     */
    public static void upload2FTP(FileItem tmp, String newfilename) throws DomainServiceException, IOException {
        //通过系统配置项获取目标FTP服务器的ip port user password
        IIsysConfigLS configLs = (IIsysConfigLS) SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.noticeAttachment.ftpaddress");
        String user = null;
        String password = null;
        String host = null;
        int port = 21;
        FTPClient ftpConn = new FTPClient();
        InputStream in = null;
        try {
            ftpaddress = ftpaddress.substring(ftpaddress.lastIndexOf("/") + 1);
            String[] parm = ftpaddress.split("@");
            if (parm.length == 1) {
                throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
            } else {
                String[] userInfo = parm[0].split(":");
                String[] address = parm[1].split(":");
                if (userInfo.length == 1 || address.length == 1) {
                    throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
                } else {
                    user = userInfo[0];
                    password = userInfo[1];
                    host = address[0];
                    port = Integer.parseInt(address[1].trim());
                }
            }
            in = tmp.getInputStream();
            //将文件流上传到FTP服务器上
            ftpConn.connect(host, port);
            ftpConn.login(user, password);
            //判断FTP连接是否成功，失败则抛出异常
            int reply = ftpConn.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpConn.logout();
                ftpConn.disconnect();
                throw new Exception("FTP connect refused");
            }
            //判断目录是否存在，如果不存在则创建
            if (!isDirExist(ftpConn)) {
                ftpConn.makeDirectory(path);
            }
            ftpConn.changeWorkingDirectory(path);
            ftpConn.setFileType(FTP.BINARY_FILE_TYPE);
            ftpConn.enterLocalPassiveMode();
            ftpConn.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
            ftpConn.setBufferSize(BUF_SIZE);
            String targetPath = path + newfilename;
            ftpConn.storeFile(targetPath, in);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
            if (ftpConn.isConnected()) {
                ftpConn.logout();
                ftpConn.disconnect();
            }
        }
    }

    public static void deleteFile(String filename) throws IOException, DomainServiceException {
        //获取FTP信息
        IIsysConfigLS configLs = (IIsysConfigLS) SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.noticeAttachment.ftpaddress");
        String user = null;
        String password = null;
        String host = null;
        int port = 21;
        FTPClient ftpConn = new FTPClient();
        try {
            ftpaddress = ftpaddress.substring(ftpaddress.lastIndexOf("/") + 1);
            String[] parm = ftpaddress.split("@");
            if (parm.length == 1) {
                throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
            } else {
                String[] userInfo = parm[0].split(":");
                String[] address = parm[1].split(":");
                if (userInfo.length == 1 || address.length == 1) {
                    throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
                } else {
                    user = userInfo[0];
                    password = userInfo[1];
                    host = address[0];
                    port = Integer.parseInt(address[1].trim());
                }
            }

            //连接FTP服务器并删除文件
            String targetPath = path + filename;
            ftpConn.connect(host, port);
            ftpConn.login(user, password);
            int reply = ftpConn.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpConn.logout();
                ftpConn.disconnect();
                throw new Exception("FTP connect refused");
            }
            ftpConn.dele(targetPath);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ftpConn.isConnected()) {
                ftpConn.logout();
                ftpConn.disconnect();
            }
        }
    }

    //利用异常判断FTP服务器中目录是否存在
    public static boolean isDirExist(FTPClient ftpConn) {
        try {
            ftpConn.changeWorkingDirectory("/home/zxin10/");
            FTPFile[] files = ftpConn.listFiles();
            if (files != null && files.length > 0) {
                for (FTPFile file : files) {
                    if (file.isDirectory() && file.getName().equals("CmsNoticeAttachment")) {
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void upload2FTP(String newfilename, InputStream in) throws DomainServiceException, IOException {
        //通过系统配置项获取目标FTP服务器的ip port user password
        IIsysConfigLS configLs = (IIsysConfigLS) SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.noticeAttachment.ftpaddress");
        String user = null;
        String password = null;
        String host = null;
        int port = 21;
        FTPClient ftpConn = new FTPClient();
        try {
            ftpaddress = ftpaddress.substring(ftpaddress.lastIndexOf("/") + 1);
            String[] parm = ftpaddress.split("@");
            if (parm.length == 1) {
                throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
            } else {
                String[] userInfo = parm[0].split(":");
                String[] address = parm[1].split(":");
                if (userInfo.length == 1 || address.length == 1) {
                    throw new Exception("Your ftpaddress setted in SysConfig is incorrect");
                } else {
                    user = userInfo[0];
                    password = userInfo[1];
                    host = address[0];
                    port = Integer.parseInt(address[1].trim());
                }
            }
            //将文件流上传到FTP服务器上
            ftpConn.connect(host, port);
            ftpConn.login(user, password);
            //判断FTP连接是否成功，失败则抛出异常
            int reply = ftpConn.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpConn.logout();
                ftpConn.disconnect();
                throw new Exception("FTP connect refused");
            }
            //判断目录是否存在，如果不存在则创建
            if (!isDirExist(ftpConn)) {
                ftpConn.makeDirectory(path);
            }
            ftpConn.changeWorkingDirectory(path);
            ftpConn.setFileType(FTP.BINARY_FILE_TYPE);
            ftpConn.enterLocalPassiveMode();
            ftpConn.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
            ftpConn.setBufferSize(BUF_SIZE);
            String targetPath = path + newfilename;
            ftpConn.storeFile(targetPath, in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
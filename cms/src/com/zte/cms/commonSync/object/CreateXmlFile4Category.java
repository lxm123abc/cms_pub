package com.zte.cms.commonSync.object;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CreateXmlFile4Category extends AbstractCreateXmlFile4Object {
	

	public String createXmlFile4WG18(Map<String, List> objectMap,
			int targetType, int syncType) throws DomainServiceException {
		String action = null;
//		if (0 != targetType) {
//			return null;
//		}
		// 获取发布Action类型
		if (1 == syncType) {
			action = "REGIST";
		} else if (2 == syncType) {
			action = "UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}
		
		Date date = new Date();
		String fileName = "category_20wg_" + action.toLowerCase().trim().replace(" ", "") + "_" + date.getTime()
				+ ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
				+ File.separator + getCurrentTime() + File.separator
				+ "category" + File.separator ;

		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		String fileFullName = absolutePath + fileName;

		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");
		List<Categorycdn> categoryList = (List<Categorycdn>) objectMap
				.get("categoryList");
		boolean isRoot = false;

		if (categoryList.get(0).getParentid().equals("-1")) {// 如果是一级节点，就把它的parentid值设置成-1
			isRoot = true;
		}
		Element progobjectElement = null;
		Element progpertyElement = null;
		// 元素节点
		progobjectElement = objectsElement.addElement("Object");
		progobjectElement.addAttribute("ElementType", "Category");
		progobjectElement.addAttribute("ID", categoryList.get(0)
				.getCatagorycode());
		progobjectElement.addAttribute("Code", categoryList.get(0)
				.getCatagorycode());
		progobjectElement.addAttribute("Action", action);
		if(isRoot==true){
			progobjectElement.addAttribute("ParentCode", "0");
		}else{
			progobjectElement.addAttribute("ParentCode", categoryList.get(0).getParentcode());
		}
		
		writeElement(progobjectElement, progpertyElement, "Name", categoryList
				.get(0).getCategoryname(), true);
		writeElement(progobjectElement, progpertyElement, "Sequence",
				categoryList.get(0).getSequence(), false);
		if(isRoot==true){
			writeElement(progobjectElement, progpertyElement, "ParentID",
					"0", false);
			
		}else{
			writeElement(progobjectElement, progpertyElement, "ParentID",
				categoryList.get(0).getParentcode(), false);
			
		}
	
		writeElement(progobjectElement, progpertyElement, "Status",
				categoryList.get(0).getActivestatus(), false);
		writeElement(progobjectElement, progpertyElement, "Description",
				categoryList.get(0).getDescription(), true);

		// 把对象写入xml文件
		writeDom(dom, fileFullName);

		return retPath;
	}

	public String createXmlFile4IPTV30(Map<String, List> objectMap,
			int targetType, int syncType) throws DomainServiceException {
		String action = null;
		// 获取发布Action类型
		if (1 == syncType || 2 == syncType) {
			action = "REGIST or UPDATE";
		} else if (3 == syncType) {
			action = "DELETE";
		} else {
			return null;
		}
		String targetTypestr = "";
		
		if(targetType==1){
			targetTypestr="bms";
		}
		if(targetType==2){
			targetTypestr="epg";
		}
		if(targetType==3){
			targetTypestr="cdn";
		}
		
		Date date = new Date();
		String fileName = "category_" + targetTypestr + "_" + action.toLowerCase().trim().replace(" ", "") + "_" + date.getTime()
				+ ".xml";
		String relativePath = tempAreaPath + File.separator + CNTSYNCXML
		+ File.separator + getCurrentTime() + File.separator
		+ "category" + File.separator ;
		String absolutePath = mountPoint + relativePath;
		String retPath = GlobalConstants
				.filterSlashStr(relativePath + fileName);
		File file = new File(absolutePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		String fileFullName = absolutePath + fileName;

		Document dom = DocumentHelper.createDocument();
		Element adiElement = dom.addElement("ADI");
		Element objectsElement = adiElement.addElement("Objects");

		List<Categorycdn> categoryList = (List<Categorycdn>) objectMap
				.get("categoryList");
		for (int i = 0; i < categoryList.size(); i++) {
			
			boolean isRoot = false;

			if (categoryList.get(i).getParentid().equals("-1")) {// 如果是一级节点，就把它的parentid值设置成-1
				isRoot = true;
			}
			
			Element progobjectElement = null;
			Element progpertyElement = null;
			// 元素节点
			progobjectElement = objectsElement.addElement("Object");
			progobjectElement.addAttribute("ElementType", "Category");

			progobjectElement.addAttribute("Action", action);
			if(isRoot==true){
				writeElement(progobjectElement, progpertyElement, "ParentID",
						"0", false);
			}else{
				writeElement(progobjectElement, progpertyElement, "ParentID",
					categoryList.get(i).getParentid(), false);
			}
			writeElement(progobjectElement, progpertyElement, "Name",
					categoryList.get(i).getCategoryname(), true);
			writeElement(progobjectElement, progpertyElement, "Sequence",
					categoryList.get(i).getSequence(), false);
			writeElement(progobjectElement, progpertyElement, "CategoryID",
					categoryList.get(i).getCategoryid(), false);
			writeElement(progobjectElement, progpertyElement, "Status",
					categoryList.get(i).getActivestatus(), false);
			writeElement(progobjectElement, progpertyElement, "Description",
					categoryList.get(i).getDescription(), true);
		}

		// 把对象写入xml文件
		writeDom(dom, fileFullName);

		return retPath;
	}
	
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        if (1 == syncType) {
            action = "REGIST";
        } else if (2 == syncType) {
            action = "UPDATE";
        } else if (3 == syncType) {
            action = "DELETE";
        } else {
            return null;
        }
        
        Date date = new Date();
        String fileName = "category_20ys_" + action.toLowerCase().trim().replace(" ", "") + "_" + date.getTime()
                + ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "category" + File.separator ;

        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants
                .filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }

        String fileFullName = absolutePath + fileName;

        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        List<Categorycdn> categoryList = (List<Categorycdn>) objectMap
                .get("categoryList");
        boolean isRoot = false;

        if (categoryList.get(0).getParentid().equals("-1")) {// 如果是一级节点，就把它的parentid值设置成-1
            isRoot = true;
        }
        Element progobjectElement = null;
        Element progpertyElement = null;
        // 元素节点
        progobjectElement = objectsElement.addElement("Object");
        progobjectElement.addAttribute("ElementType", "Category");
        progobjectElement.addAttribute("ID", categoryList.get(0)
                .getCatagorycode());
        progobjectElement.addAttribute("Code", categoryList.get(0)
                .getCatagorycode());
        progobjectElement.addAttribute("Action", action);
        if(isRoot==true){
            progobjectElement.addAttribute("ParentCode", "0");
        }else{
            progobjectElement.addAttribute("ParentCode", categoryList.get(0).getParentcode());
        }
        
        writeElement(progobjectElement, progpertyElement, "Name", categoryList
                .get(0).getCategoryname(), true);
        writeElement(progobjectElement, progpertyElement, "Sequence",
                categoryList.get(0).getSequence(), false);
        if(isRoot==true){
            writeElement(progobjectElement, progpertyElement, "ParentID",
                    "0", false);
            
        }else{
            writeElement(progobjectElement, progpertyElement, "ParentID",
                categoryList.get(0).getParentcode(), false);
            
        }
    
        writeElement(progobjectElement, progpertyElement, "Status",
                categoryList.get(0).getActivestatus(), false);
        writeElement(progobjectElement, progpertyElement, "Description",
                categoryList.get(0).getDescription(), true);

        // 把对象写入xml文件
        writeDom(dom, fileFullName);

        return retPath;
    }
	 

}

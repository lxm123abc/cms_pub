package com.zte.cms.programcrmmap.ls;

import java.util.List;
import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramcrmmapLS
{

    /**
     * 删除节目与角色的关联关系
     * 
     * @param programCrmMap
     * @return
     * @throws Exception
     */
    String batchDeleteList(Long[] mapindexList, String programindex) throws DomainServiceException;

    /**
     * 获取单个节目与角色的关联关系
     * 
     * @param mapindex 主键
     * @return
     * @throws Exception
     */
    ProgramCrmMap getProgramcrmmap(Long mapindex);

    /**
     * 新增节目与角色的关联关系
     * 
     * @param castcdn
     * @return
     * @throws Exception
     */
    String insertProgramcrmmap(Long[] castrolemapindexList, String programindex) throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象
     * 
     * @param programCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询ProgramCrmMap对象 支持排序
     * 
     * @param programCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    TableDataInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

     public String deleteProgarmcrmmap(Long mapindex, String programindex) throws DomainServiceException;

     /************************ publish manage*********************/
     /**
      * 根据条件分页查询ProgramCrmMap对象 支持排序
      * 
      * @param programCrmMap对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */
     public TableDataInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws Exception;

     
     /**
      * 根据条件分页查询ProgramCrmMap对象 支持排序
      * 
      * @param programCrmMap对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */
     public TableDataInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws Exception;

     /**
      * 点播内容关联角色发布
      * 
      * @param mapindex 主键对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */     
     public String publishproCrmMapPlatformSyn(String mapindex, String programid,String castrolemapid,int type,int synType) throws Exception;

     /**
      * 点播内容关联角色取消发布
      * 
      * @param mapindex 主键对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */     
     public String delPublishproCrmMapPlatformSyn(String mapindex, String programid,String castrolemapid,int type,int synType) throws Exception;
       
     /**
      * 点播内容关联角色针对网元发布
      * 
      * @param mapindex 主键对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */ 
     public String publishproCrmProTarget(String mapindex,  String targetindex,int type) throws Exception;

     /**
      * 点播内容关联角色针对网元取消发布
      * 
      * @param mapindex 主键对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */ 
     public String delPublishproCrmProTarget(String mapindex,  String targetindex,int type) throws Exception;

     /**
      * 点批量发布或取消发布点播内容关联角色
      * 
      * @param mapindex 主键对象
      * @param start 起始行
      * @param pageSize 页面大小
      * @return 查询结果
      * @throws Exception
      */      
     public String batchPublishOrDelPublishproCrm(List<ProgramCrmMap> list,String operType, int type) throws Exception;
     
     /************************ publish manage*********************/
}
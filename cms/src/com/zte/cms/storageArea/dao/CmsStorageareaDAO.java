package com.zte.cms.storageArea.dao;

import java.util.List;

import com.zte.cms.storageArea.dao.ICmsStorageareaDAO;
import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsStorageareaDAO extends DynamicObjectBaseDao implements ICmsStorageareaDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException
    {
        log.debug("insert cmsStoragearea starting...");
        super.insert("insertCmsStoragearea", cmsStoragearea);
        log.debug("insert cmsStoragearea end");
    }

    public void updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException
    {
        log.debug("update cmsStoragearea by pk starting...");
        super.update("updateCmsStoragearea", cmsStoragearea);
        log.debug("update cmsStoragearea by pk end");
    }

    public void deleteCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException
    {
        log.debug("delete cmsStoragearea by pk starting...");
        super.delete("deleteCmsStoragearea", cmsStoragearea);
        log.debug("delete cmsStoragearea by pk end");
    }

    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DAOException
    {
        log.debug("query cmsStoragearea starting...");
        CmsStoragearea resultObj = (CmsStoragearea) super.queryForObject("getCmsStoragearea", cmsStoragearea);
        log.debug("query cmsStoragearea end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DAOException
    {
        log.debug("query cmsStoragearea by condition starting...");
        List<CmsStoragearea> rList = (List<CmsStoragearea>) super.queryForList("queryCmsStorageareaListByCond",
                cmsStoragearea);
        log.debug("query cmsStoragearea by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsStoragearea by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsStorageareaListCntByCond", cmsStoragearea)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsStoragearea> rsList = (List<CmsStoragearea>) super.pageQuery("queryCmsStorageareaListByCond",
                    cmsStoragearea, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsStoragearea by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsStorageareaListByCond", "queryCmsStorageareaListCntByCond",
                cmsStoragearea, start, pageSize, puEntity);
    }

}
package com.zte.cms.common;

import java.io.File;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.servicecontainer.business.util.ServiceConsts;

public class UpLoadFile
{
    public static final String FILESEPARATOR = System.getProperty("file.separator");
    /**
     * 日志生成器
     */
    private Log log = SSBBus.getLog(getClass());

    /**
     * 获取http方式上传文件，并将其移动到指定文件夹
     * 
     * @param newfilename 文件的新名字 sizeLimit 文件大小限制 uploadPath 上载路径
     * 
     * @return UpLoadResult 上载结果
     */
    public static UpLoadResult singleFileUplord(String newfilename, Long sizeLimit, String targetPath)
    {
        UpLoadResult result = new UpLoadResult();
        RIAContext context = RIAContext.getCurrentInstance();
        List<FileItem> fileList = (List<FileItem>) context.getRequest().getAttribute(ServiceConsts.FILEITEM_LIST);
        if (fileList != null && fileList.size() > 0)
        {
            Iterator<FileItem> iter = fileList.iterator();
            File filePath = new File(targetPath);

            // 如果文件夹不存在，则新建文件夹
            if (!filePath.exists())
            {
                filePath.mkdirs();
            }
            String fileAbsPath = filePath.getAbsolutePath();
            try
            {
                FileItem tmp = (FileItem) iter.next();
                // 判断大小
                if (tmp.getSize() > sizeLimit)
                {
                    result.setFlag(-1);
                    result.setResult(false);
                    return result;
                }
                else
                {
                    result.setFlag(0);
                }
                File targetFile = new File(fileAbsPath, newfilename);
                if(targetFile.exists())
                {
                    targetFile.delete();  
                }
                tmp.write(targetFile);
                String md5 = MD5.getFileMD5String(targetFile);
                result.setMd5(md5);
                result.setPosterFilePath(fileAbsPath + File.separator + newfilename);
                result.setFileSize(tmp.getSize());
                result.setResult(true);
            }
            catch (Exception e)
            {
                // 删除失败后删除本地临时文件
                removeAfterUpload(fileAbsPath, newfilename);
                e.printStackTrace();
                result.setResult(false);
            }
        }
        else
        {
            result.setFlag(-2);
            result.setResult(false);
        }
        return result;
    }

    /**
     * 取得临时名称
     * 
     * @param filename
     * @return
     */
    public static String gettempName(String filename)
    {
        String postfix = "";
        postfix = filename.substring(filename.lastIndexOf(".")).toLowerCase();
        String tmp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String name = tmp + postfix;
        return name;
    }

    /**
     * 上载失败后或者成功以后，删除临时文件
     * 
     * @param epgframename
     */
    private static void removeAfterUpload(String filePath, String filename)
    {
        System.out.println("delete file for uploading finish... " + "posterfilename=========" + filename);
        // 删除文件
        File posterFile = new File(filePath + FILESEPARATOR + filename);

        if (posterFile.exists())
        {
            posterFile.delete();
        }
    }

    /**
     * 删除文件
     * 
     * @param epgframename
     */
    public static void removeFile(String filename)
    {
        // 删除文件
        File posterFile = new File(filename);
        if (posterFile.exists())
        {
            posterFile.delete();
        }
    }

}

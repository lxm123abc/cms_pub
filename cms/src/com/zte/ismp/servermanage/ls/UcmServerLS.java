package com.zte.ismp.servermanage.ls;

import java.util.List;

import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ismp.servermanage.common.ServerUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.ucms.model.UcmServer;
import com.zte.umap.ucms.service.IUcmServerDS;

/**
 * <p>
 * 文件名称: UcmServerLS.java
 * </p>
 * <p>
 * 文件描述: 服务器管理类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2010
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要完成服务器的增、删、改、查功能
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2010年08月10日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 */
public class UcmServerLS implements IUcmServerLS
{
    private IUcmServerDS ucmServerDS = null;
    private IUcpBasicDS ucpBasicDS = null;
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void setUcmServerDS(IUcmServerDS ucmServerDS)
    {
        this.ucmServerDS = ucmServerDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    /**
     * 新增服务器前的校验
     */
    public String checkBeforeInsert(UcmServer ucmServer) throws Exception
    {
        // TODO 根据实际业务需要，做插入前校验
        log.debug("UcmServerLS: checkBeforeInsert method starts");
        String resultCode = "0420100";// 新增服务器校验成功(默认)
        UcmServer tempUcmServer = new UcmServer();
        try
        {
            // 判断当前系统是否已存在一个资源下载服务器
            tempUcmServer.setServertype(GlobalConstants.SERVER_TYPE_DOWNLOAD);
            tempUcmServer.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            log.debug("UcmServerDS: getUcmServerByCond starts");
            List<UcmServer> list = ucmServerDS.getUcmServerByCond(tempUcmServer);
            log.debug("UcmServerDS: getUcmServerByCond ends");
            if (list != null && list.size() >= 1)
            {
                resultCode = "0420102";// 当前系统只能添加一个资源服务器
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in checkBeforeInsert method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("UcmServerLS: checkBeforeInsert method ends and resultCode=" + resultCode);
        return resultCode;
    }

    /**
     * 插入一条服务器记录 实现流程： 1、做插入前校验 1.1、校验成功，插入一条服务器记录及操作日志，返回操作结果信息 1.2、校验失败，返回校验失败信息
     */
    public String insertUcmServer(UcmServer ucmServer) throws Exception
    {
        log.debug("UcmServerLS: insertUcmServer method starts");
        ReturnInfo rtnInfo = new ReturnInfo(GlobalConstants.SUCCESS, "0420101");// 新增服务器操作成功（默认）
        // 插入前校验
        // String resultCode = this.checkBeforeInsert(ucmServer);
        String resultCode = "0420100";
        if (!resultCode.equals("0420100"))
        {// 新增服务器校验失败
            rtnInfo.setFlag(GlobalConstants.FAIL);
            rtnInfo.setReturnMessage(resultCode);
            return rtnInfo.toString();
        }
        else
        {// 新增服务器校验成功
            // TODO 根据实际业务，设置服务器信息
            ucmServer.setServertype(GlobalConstants.SERVER_TYPE_DOWNLOAD);
            ucmServer.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            try
            {
                log.debug("UcmServerDS: insertUcmServer starts");
                ucmServerDS.insertUcmServer(ucmServer);
                log.debug("UcmServerDS: insertUcmServer ends");
                // 添加业务操作日志
                String optObjectType = ResourceManager.getResourceText("log.server.optobjecttype.add");// 新增服务器
                String optObject = String.valueOf(ucmServer.getServerindex());
                String optDetail = ResourceManager.getResourceText("log.server.optdetail.add");
                optDetail = optDetail.replaceAll("\\[servername\\]", ucmServer.getServername());
                ServerUtil.addLog(optObjectType, optObject, optDetail);
            }
            catch (Exception e)
            {
                log.error("Error occurred in insert method", e);
                throw new Exception(ResourceManager.getResourceText(e));
            }
        }
        log.debug("UcmServerLS: insertUcmServer method ends and resultCode = " + rtnInfo.toString());
        return rtnInfo.toString();
    }

    /**
     * 分页查询
     */
    public TableDataInfo pageInfoQuery(UcmServer ucmServer, int start, int pageSize) throws DomainServiceException
    {
        log.debug("UcmServerLS: pageInfoQuery method starts");
        TableDataInfo serverPageInfo = null;
        try
        {
            ucmServer.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            serverPageInfo = ucmServerDS.pageInfoQuery(ucmServer, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("Error occurred in pageInfoQuery method", e);
        }
        log.debug("UcmServerLS: pageInfoQuery method ends");
        return serverPageInfo;
    }

    /**
     * 分页查询
     */
    public TableDataInfo pageInfoQuery(UcmServer ucmServer, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("UcmServerLS: pageInfoQuery method with PageUtilEntity starts");
        TableDataInfo serverPageInfo = null;
        try
        {
            ucmServer.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            serverPageInfo = ucmServerDS.pageInfoQuery(ucmServer, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            log.error("Error occurred in pageInfoQuery method", e);
        }
        log.debug("UcmServerLS: pageInfoQuery method with PageUtilEntity ends");
        return serverPageInfo;
    }

    /**
     * 删除一条服务器记录 实现流程： 1、调用服务层删除方法删除服务器信息
     */
    public String deleteUcmServer(Long serverindex) throws Exception
    {
        log.debug("UcmServerLS: deleteUcmServer by pk starts");
        ReturnInfo rtnInfo = new ReturnInfo(GlobalConstants.SUCCESS, "0420301");// 删除服务器操作成功（默认）
        try
        {
            // 删除前校验
            // 判断当前服务器是否存在
            log.debug("UcmServerDS: getUcmServerByIndex starts and serverindex=" + serverindex);
            UcmServer ucmServer = ucmServerDS.getUcmServerByIndex(serverindex);
            log.debug("UcmServerDS: getUcmServer by PK end");
            if (ucmServer == null)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage("0420302");// 当前服务器已经被删除
                return rtnInfo.toString();
            }
            // 判断服务器是已否被SP使用
            UcpBasic ucpBasic = new UcpBasic();
            ucpBasic.setCntsvrindex(serverindex);
            log.debug("UcpBasicDS: getUcpByCond starts");
            List<UcpBasic> ucpBasicList = ucpBasicDS.getUcpBasicByCond(ucpBasic);
            log.debug("UcpBasicDS: getUcpBasicByCond ends");
            if (ucpBasicList != null && ucpBasicList.size() > 0)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage("0420303");// 当前服务器已经被SP使用，不能删除
                return rtnInfo.toString();
            }
            log.debug("UcmServerDS: removeUcmServer starts");
            ucmServerDS.removeUcmServer(ucmServer);
            log.debug("UcmServerDS: removeUcmServer end");
            // 添加业务操作日志
            String optObjectType = ResourceManager.getResourceText("log.server.optdetail.del");// 删除服务器
            String optObject = String.valueOf(ucmServer.getServerindex());
            String optDetail = ResourceManager.getResourceText("log.server.optdetail.del");
            optDetail = optDetail.replaceAll("\\[servername\\]", ucmServer.getServername());
            ServerUtil.addLog(optObjectType, optObject, optDetail);
        }
        catch (Exception e)
        {
            log.debug("Error occurred in deleteUcmServer method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("UcmServerLS: deleteUcmServer by index ends and resultCode = " + rtnInfo.getReturnMessage());
        return rtnInfo.toString();
    }

    /**
     * 更新服务器信息 实现流程： 1、调用服务层更新方法，更新服务器信息
     */
    public String updateUcmServer(UcmServer ucmServer) throws Exception
    {
        log.debug("UcmServerLS: updateUcmServer method starts");
        ReturnInfo rtnInfo = new ReturnInfo(GlobalConstants.SUCCESS, "0420201");// 更新操作成功（默认）
        try
        {
            // 更新前校验
            UcmServer oldUcmServer = new UcmServer();
            log.debug("UcmServerDS: getUcmServerByIndex starts and serverindex = " + ucmServer.getServerindex());
            oldUcmServer = ucmServerDS.getUcmServerByIndex(ucmServer.getServerindex());
            log.debug("UcmServerDS: getUcmServerByIndex ends");
            if (oldUcmServer == null)
            {
                rtnInfo.setFlag(GlobalConstants.FAIL);
                rtnInfo.setReturnMessage("0420202");// 当前修改服务器不存在
                return rtnInfo.toString();
            }
            log.debug("UcmServerDS: updateUcmServer starts");
            ucmServerDS.updateUcmServer(ucmServer);
            log.debug("UcmServerDS: updateUcmServer ends");
            // 添加业务操作日志
            String optObjectType = ResourceManager.getResourceText("log.server.optobjecttype.mod");// 修改服务器
            String optObject = String.valueOf(ucmServer.getServerindex());
            String optDetail = ResourceManager.getResourceText("log.server.optdetail.mod");
            optDetail = optDetail.replaceAll("\\[servername\\]", ucmServer.getServername());
            ServerUtil.addLog(optObjectType, optObject, optDetail);
        }
        catch (Exception e)
        {
            log.debug("Error occurred in updateUcmServer method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("UcmServerLS: updateUcmServer method ends");
        return rtnInfo.toString();
    }

    /**
     * 获得一条服务器记录
     */
    public UcmServer getUcmServer(Long serverindex) throws Exception
    {
        log.debug("UcmServerLS: getUcmServer by index starts and serverindex = " + serverindex);
        UcmServer ucmServer = null;
        try
        {
            log.debug("UcmServerDS: getUcmServerByIndex starts and serverindex = " + serverindex);
            ucmServer = ucmServerDS.getUcmServerByIndex(serverindex);
            log.debug("UcmServerDS: getUcmServer by index ends");
            if (ucmServer == null)
            {
                throw new Exception("0422202");// 查找的服务器不存在
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in getUcmServer method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("UcmServerLS: getUcmServer by index ends");
        return ucmServer;
    }
}

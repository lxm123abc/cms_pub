package com.zte.cms.channel.wkfw.service;

import java.util.List;


import com.zte.cms.channel.wkfw.dao.IScheduleWkfwTaskForMeDAO;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;
import com.zte.cms.channel.wkfw.model.ScheduleWkfwTaskForMe;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ScheduleWkfwTaskForMeDS implements IScheduleWkfwTaskForMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IScheduleWkfwTaskForMeDAO scheduleWkfwTaskForMedao = null;    

    public IScheduleWkfwTaskForMeDAO getScheduleWkfwTaskForMedao()
    {
        return scheduleWkfwTaskForMedao;
    }

    public void setScheduleWkfwTaskForMedao(IScheduleWkfwTaskForMeDAO scheduleWkfwTaskForMedao)
    {
        this.scheduleWkfwTaskForMedao = scheduleWkfwTaskForMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#queryStrategyWkfwTaskForMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryScheduleWkfwTaskForMeListCntByCond(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        log.debug("query ScheduleWkfwTaskForMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(scheduleWkfwTaskForMeCondition);
            totalCnt = scheduleWkfwTaskForMedao.queryscheduleWkfwTaskForMeListCntByCond(scheduleWkfwTaskForMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query ScheduleWkfwTaskForMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get ScheduleWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(scheduleWkfwTaskForMeCondition);
            pageInfo = scheduleWkfwTaskForMedao.pageInfoQuery(scheduleWkfwTaskForMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get ScheduleWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get ScheduleWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(scheduleWkfwTaskForMeCondition);
            pageInfo = scheduleWkfwTaskForMedao.pageInfoQuery(scheduleWkfwTaskForMeCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ScheduleWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
    {
        scheduleWkfwTaskForMeCondition.setScheduleid(EspecialCharMgt.conversion(scheduleWkfwTaskForMeCondition
                .getScheduleid()));

        scheduleWkfwTaskForMeCondition.setProgramname(EspecialCharMgt.conversion(scheduleWkfwTaskForMeCondition
                .getProgramname()));
    }
}

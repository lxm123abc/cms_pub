package com.zte.cms.impSyncTask.service;

import java.util.List;
import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.cms.impSyncTask.dao.IObjectCpcntRecordDAO;
import com.zte.cms.impSyncTask.service.IObjectCpcntRecordDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ObjectCpcntRecordDS implements IObjectCpcntRecordDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IObjectCpcntRecordDAO dao = null;
	
	public void setDao(IObjectCpcntRecordDAO dao)
	{
	    this.dao = dao;
	}

	public void insertObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug("insert objectCpcntRecord starting...");
		try
		{
			dao.insertObjectCpcntRecord( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert objectCpcntRecord end");
	}
	
	public void insertObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException
	{
		log.debug("insert objectCpcntRecordList by pk starting...");
		if(null == objectCpcntRecordList || objectCpcntRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectCpcntRecordList");
			return;
		}
	    try
		{
			dao.insertObjectCpcntRecordList( objectCpcntRecordList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("insert objectCpcntRecordList by pk end");
	}
	
	public void updateObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug("update objectCpcntRecord by pk starting...");
	    try
		{
			dao.updateObjectCpcntRecord( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectCpcntRecord by pk end");
	}

	public void updateObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException
	{
		log.debug("update objectCpcntRecordList by pk starting...");
		if(null == objectCpcntRecordList || objectCpcntRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectCpcntRecordList");
			return;
		}
	    try
		{
			dao.updateObjectCpcntRecordList( objectCpcntRecordList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectCpcntRecordList by pk end");
	}

	public void updateObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug("update objectCpcntRecord by condition starting...");
		try
		{
			dao.updateObjectCpcntRecordByCond( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectCpcntRecord by condition end");
	}

	public void updateObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException
	{
		log.debug("update objectCpcntRecordList by condition starting...");
		if(null == objectCpcntRecordList || objectCpcntRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectCpcntRecordList");
			return;
		}
		try
		{
			dao.updateObjectCpcntRecordListByCond( objectCpcntRecordList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug("update objectCpcntRecordList by condition end");
	}

	public void removeObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug( "remove objectCpcntRecord by pk starting..." );
		try
		{
			dao.deleteObjectCpcntRecord( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectCpcntRecord by pk end" );
	}

	public void removeObjectCpcntRecordList ( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException
	{
		log.debug( "remove objectCpcntRecordList by pk starting..." );
		if(null == objectCpcntRecordList || objectCpcntRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectCpcntRecordList");
			return;
		}
		try
		{
			dao.deleteObjectCpcntRecordList( objectCpcntRecordList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectCpcntRecordList by pk end" );
	}

	public void removeObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug( "remove objectCpcntRecord by condition starting..." );
		try
		{
			dao.deleteObjectCpcntRecordByCond( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectCpcntRecord by condition end" );
	}

	public void removeObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DomainServiceException
	{
		log.debug( "remove objectCpcntRecordList by condition starting..." );
		if(null == objectCpcntRecordList || objectCpcntRecordList.size() == 0 )
		{
			log.debug("there is no datas in objectCpcntRecordList");
			return;
		}
		try
		{
			dao.deleteObjectCpcntRecordListByCond( objectCpcntRecordList );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "remove objectCpcntRecordList by condition end" );
	}

	public ObjectCpcntRecord getObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug( "get objectCpcntRecord by pk starting..." );
		ObjectCpcntRecord rsObj = null;
		try
		{
			rsObj = dao.getObjectCpcntRecord( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get objectCpcntRecordList by pk end" );
		return rsObj;
	}

	public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DomainServiceException
	{
		log.debug( "get objectCpcntRecord by condition starting..." );
		List<ObjectCpcntRecord> rsList = null;
		try
		{
			rsList = dao.getObjectCpcntRecordByCond( objectCpcntRecord );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get objectCpcntRecord by condition end" );
		return rsList;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get objectCpcntRecord page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(objectCpcntRecord, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ObjectCpcntRecord>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get objectCpcntRecord page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get objectCpcntRecord page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(objectCpcntRecord, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ObjectCpcntRecord>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get objectCpcntRecord page info by condition end" );
		return tableInfo;
	}

    public TableDataInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug( "get objectCpcntRecord page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.getObjectCpcntRecord(objectCpcntRecord, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ObjectCpcntRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get objectCpcntRecord page info by condition end" );
        return tableInfo;
    }
}

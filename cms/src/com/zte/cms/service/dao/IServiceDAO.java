package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.service.model.Service;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceDAO
{
    /**
     * 新增Service对象
     * 
     * @param service Service对象
     * @throws DAOException dao异常
     */
    public void insertService(Service service) throws DAOException;

    /**
     * 根据主键更新Service对象
     * 
     * @param service Service对象
     * @throws DAOException dao异常
     */
    public void updateService(Service service) throws DAOException;

    /**
     * 根据主键删除Service对象
     * 
     * @param service Service对象
     * @throws DAOException dao异常
     */
    public void deleteService(Service service) throws DAOException;

    /**
     * 根据主键查询Service对象
     * 
     * @param service Service对象
     * @return 满足条件的Service对象
     * @throws DAOException dao异常
     */
    public Service getService(Service service) throws DAOException;
    public Service getServiceById(Service service) throws DAOException;

    /**
     * 根据条件查询Service对象
     * 
     * @param service Service对象
     * @return 满足条件的Service对象集
     * @throws DAOException dao异常
     */
    public List<Service> getServiceByCond(Service service) throws DAOException;

    /**
     * 根据条件分页查询Service对象，作为查询条件的参数
     * 
     * @param service Service对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Service service, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询Service对象，作为查询条件的参数
     * 
     * @param service Service对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 查询用来发布的服务
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 取消发布的查询
     * 
     * @param service
     * @param start
     * @param pageSize
     * @param puEntity
     * @return
     * @throws DAOException
     */
    public PageInfo pageInfoQueryDelPublish(Service service, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据objectindex和targetindex查询服务是否有内容处于发布中状态
     * @param cntTargetSync
     * @return
     * @throws DAOException
     */
    public int countCntContentPublish(CntTargetSync cntTargetSync)throws DAOException;     
    
}
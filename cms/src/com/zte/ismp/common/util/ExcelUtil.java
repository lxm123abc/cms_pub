package com.zte.ismp.common.util;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;

/**
 * <p>
 * 文件名称: ExcelUtil.java
 * </p>
 * <p>
 * 文件描述: 对excel文件一些操作
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2005-2008
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要:
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2008-3-18
 * <p>
 * 修改记录:
 * </p>
 * 
 * <pre>
 *      修改日期:
 *      版 本 号:
 *      修 改 人:
 *      修改内容:
 * </pre>
 * 
 * @author 张大海
 * @version
 */

public class ExcelUtil
{

    public static int EXCEL_MAX_ROWS = 30000;// excel文件最大的行数
    Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * 分割Vector为Vector数组，对于Vector长度较大的时候使用
     * 
     * @param v 需要分割的Vector
     * @param maxSize 分割后每个Vector中的size长度
     * @return 分割后的Vector[]
     */
    public Vector[] splitVector(Vector v, int maxSize)
    {
        if (v == null || v.size() == 0 || v.size() <= maxSize)
        {
            return new Vector[] { v };
        }
        int count = v.size() / maxSize + 1;
        Vector[] retV = new Vector[count];
        Vector tempV = new Vector();
        for (int i = 0; i < count; i++)
        {
            tempV = new Vector();
            for (int j = i * maxSize; j < v.size() && j < (i + 1) * maxSize; j++)
            {
                tempV.add(v.get(j));
            }
            retV[i] = tempV;
        }
        return retV;
    }

    /**
     * 模糊匹配字符串
     * 
     * @param res 需要匹配的字符串
     * @param pattern 匹配的字符串样式，可以带*或者?查询
     * @return 是否匹配
     */
    public boolean isMatcher(String res, String pattern)
    {
        pattern = pattern.replace('.', '#');
        pattern = pattern.replaceAll("#", "\\\\.");
        pattern = pattern.replace('*', '#');
        pattern = pattern.replaceAll("#", ".*");
        pattern = pattern.replace('?', '#');
        pattern = pattern.replaceAll("#", ".?");
        pattern = "^" + pattern + "$";

        Pattern p = Pattern.compile(pattern);
        return p.matcher(res).matches();
    }

    /**
     * 删除指定目录下匹配的文件
     * 
     * @param filename 文件名称
     * @param filepath 文件路径
     * @return 删除结果，是否成功
     */
    public boolean delFile(String filename, String filepath)
    {
        boolean result = true;
        if (!new File(filepath).isDirectory())
        {
            return result;
        }
        if (filename.indexOf("*") == -1 && filename.indexOf("?") == -1)// 没有通配符
        {
            if (new File(filepath + filename).isFile())
            {
                new File(filepath + filename).delete();
            }
        }
        else
        {
            File file = new File(filepath);
            String[] filelist = file.list();
            for (int i = 0; i < filelist.length; i++)
            {
                if (isMatcher(filelist[i], filename))
                {
                    result = new File(filepath + filelist[i]).delete();
                    if (result = false)
                    {
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 创建excel文件，自动的对大数据的结果分为多个文件
     * 
     * @param data Vector 需要创建excel表格的信息集合，Vector中包含HashMap
     * @param filename 文件名称
     * @param filepath 文件路径
     * @param title 表格的标题的信息（包含标题的顺序，如果为空，为没有顺序要求）
     * @param sheetName String
     * @param applicationResourceMap HashMap
     * @return Vector 每条Vector是个HashMap，其中HashMap的key：pathname生成的文件路径，items文件中记录的条数
     * @throws Exception Exception
     */
    public Vector createExcel(Vector data, String filename, String filepath, Vector title, String sheetName,
            HashMap applicationResourceMap) throws Exception
    {
        Vector retV = new Vector();
        HashMap map = new HashMap();
        if (data.size() < EXCEL_MAX_ROWS)
        {
            createSingleExcel(data, filename, filepath, title, sheetName, applicationResourceMap);
            map = new HashMap();
            map.put("filename", filename);
            map.put("filepath", filepath);
            map.put("items", String.valueOf(data.size()));
            retV.add(map);
            return retV;
        }
        Vector[] vList = splitVector(data, EXCEL_MAX_ROWS);
        filename = filename.substring(0, filename.lastIndexOf("."));
        for (int i = 0; i < vList.length; i++)
        {
            createSingleExcel((Vector) vList[i], filename + "_" + i + ".xls", filepath, title, sheetName,
                    applicationResourceMap);
            map = new HashMap();
            map.put("filename", filename + "_" + i + ".xls");
            map.put("filepath", filepath);
            map.put("items", String.valueOf(((Vector) vList[i]).size()));
            retV.add(map);
        }
        return retV;
    }

    /**
     * 向指定的excel文件追加数据，如果指定的文件不存在，创建excel文件
     * 
     * @param data 需要追加的数据
     * @param filename 文件名称
     * @param filepath 文件路径
     * @param title 表格的标题的信息（包含标题的顺序，如果为空，为没有顺序要求）
     * @param sheetName String
     * @param applicationResourceMap HashMap
     * @throws Exception Exception
     */
    public void createOrAddSingleExcel(Vector data, String filename, String filepath, Vector title, String sheetName,
            HashMap applicationResourceMap) throws Exception
    {
        if (data.size() > EXCEL_MAX_ROWS + 1)
        {
            return;
        }
        if (!new File(filepath + filename).isFile())
        {
            if (!new File(filepath).isDirectory())
            {
                new File(filepath).mkdirs();
            }
            createSingleExcel(data, filename, filepath, title, sheetName, applicationResourceMap);
            return;
        }
        addExcel(data, filename, filepath, title, sheetName);
    }

    /**
     * 批量 向指定的excel文件追加数据，如果指定的文件不存在，创建excel文件
     * 
     * @param fileInfo 每一行为一个需要创建或者需要追加的文件信息
     * @param applicationResourceMap HashMap
     * @throws Exception Exception
     */
    public void createOrAddExcel(Vector fileInfo, HashMap applicationResourceMap) throws Exception
    {
        if (fileInfo == null || fileInfo.size() == 0)
        {
            return;
        }
        for (int i = 0; i < fileInfo.size(); i++)
        {
            HashMap fileInfoMap = (HashMap) fileInfo.get(i);

            Vector data = (Vector) fileInfoMap.get("data");
            String filename = (String) fileInfoMap.get("filename");
            String filepath = (String) fileInfoMap.get("filepath");
            Vector title = (Vector) fileInfoMap.get("title");
            String sheetName = (String) fileInfoMap.get("sheetname");

            createOrAddSingleExcel(data, filename, filepath, title, sheetName, applicationResourceMap);
        }
    }

    /**
     * 追加excel文件数据
     * 
     * @param data 需要追加的数据
     * @param filename 文件名称
     * @param filepath 文件路径
     * @param title 表格的标题的信息（包含标题的顺序，如果为空，为没有顺序要求）
     * @param sheetName 表单的名称，为空就取默认的Sheet1
     * @throws Exception Exception
     */
    private void addExcel(Vector data, String filename, String filepath, Vector title, String sheetName)
            throws Exception
    {
        HashMap map;
        Label label;
        WritableWorkbook book = null;

        try
        {
            if (data == null || data.size() == 0 || data.size() > EXCEL_MAX_ROWS + 1
                    || !new File(filepath + filename).isFile())
            {
                return;
            }
            if (sheetName == null || sheetName.length() == 0)
            {
                sheetName = "Sheet1";
            }
            if (title == null || title.size() == 0)
            {
                title = new Vector();
                for (int i = 0; i < data.size(); i++)
                {
                    map = (HashMap) data.get(i);
                    Iterator iterator = map.keySet().iterator();
                    while (iterator.hasNext())
                    {
                        String temp = (String) iterator.next();
                        if (!title.contains(temp))
                        {
                            title.add(temp);
                        }
                    }
                }
            }

            Workbook wb = Workbook.getWorkbook(new File(filepath + filename));
            book = Workbook.createWorkbook(new File(filepath + filename), wb);
            WritableSheet sheet = book.getSheet(sheetName);
            int rows = sheet.getRows();
            if (data.size() + rows > EXCEL_MAX_ROWS + 1)
            {
                return;
            }

            for (int i = 0; i < data.size(); i++)
            {
                map = (HashMap) data.get(i);
                for (int j = 0; j < title.size(); j++)
                {
                    label = new Label(j, (i + rows), (String) map.get((String) title.get(j)));
                    sheet.addCell(label);
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            if (book != null)
            {
                book.write();
                book.close();
            }
        }
    }

    /**
     * 创建单个excel文件
     * 
     * @param data 需要创建excel表格的信息集合，Vector中包含HashMap
     * @param filename 文件名称
     * @param filepath 文件路径
     * @param title 表格的标题的信息（包含标题的顺序，如果为空，为没有顺序要求）
     * @param sheetName 表单的名称，为空就取默认的Sheet1
     * @param applicationResourceMap HashMap
     * @throws Exception Exception
     */
    private void createSingleExcel(Vector data, String filename, String filepath, Vector title, String sheetName,
            HashMap applicationResourceMap) throws Exception
    {
        HashMap map;
        Label label;
        WritableWorkbook book = null;

        try
        {
            if (sheetName == null || sheetName.length() == 0)
            {
                sheetName = "Sheet1";
            }

            // 如果不存在指定的路径，就生成路径
            if (!new File(filepath).isDirectory())
            {
                new File(filepath).mkdirs();
            }
            book = Workbook.createWorkbook(new File(filepath + filename));
            WritableSheet sheet = book.createSheet(sheetName, 0);
            if (title == null || title.size() == 0)
            {
                title = new Vector();
                for (int i = 0; i < data.size(); i++)
                {
                    map = (HashMap) data.get(i);
                    Iterator iterator = map.keySet().iterator();
                    while (iterator.hasNext())
                    {
                        String temp = (String) iterator.next();
                        if (!title.contains(temp))
                        {
                            title.add(temp);
                        }
                    }
                }
            }

            for (int i = 0; i < title.size(); i++)
            {
                if (applicationResourceMap == null)
                {
                    label = new Label(i, 0, (String) title.get(i));
                }
                else
                {
                    label = new Label(i, 0, (String) applicationResourceMap.get((String) title.get(i)));
                }
                sheet.addCell(label);
            }
            for (int i = 0; i < data.size(); i++)
            {
                map = (HashMap) data.get(i);
                for (int j = 0; j < title.size(); j++)
                {
                    label = new Label(j, (i + 1), (String) map.get((String) title.get(j)));
                    sheet.addCell(label);
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            if (book != null)
            {
                book.write();
                book.close();
            }
        }
    }

    /**
     * 创建单个excel文件(简化版)
     * 
     * @param data 需要创建excel表格的信息集合，Vector中包含HashMap
     * @param filename 文件名称
     * @param filepath 文件路径
     * @param sheetName 表单的名称，为空就取默认的Sheet1
     * @throws Exception Exception
     */
    public boolean createExcel(Vector data, String filename, String filepath, String sheetName) throws Exception
    {
        Vector row;
        Label label;
        WritableWorkbook book = null;

        try
        {
            if (sheetName == null || sheetName.length() == 0)
            {
                sheetName = "Sheet1";
            }

            // 如果不存在指定的路径，就生成路径
            if (!new File(filepath).isDirectory())
            {
                new File(filepath).mkdirs();
            }
            book = Workbook.createWorkbook(new File(filepath + filename));

            WritableSheet sheet = book.createSheet(sheetName, 0);

            for (int i = 0; i < data.size(); i++)
            {
                row = (Vector) data.get(i);
                for (int j = 0; j < row.size(); j++)
                {
                    label = new Label(j, i, (String) row.get(j));
                    sheet.addCell(label);
                }
            }
            return true;
        }
        catch (Exception e)
        {
            logger.error(e);
            return false;
        }
        finally
        {
            if (book != null)
            {
                book.write();
                book.close();
            }
        }
    }

}

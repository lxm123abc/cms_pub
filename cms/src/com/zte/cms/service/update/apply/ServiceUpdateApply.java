package com.zte.cms.service.update.apply;

import net.sf.cglib.beans.BeanCopier;

import com.zte.cms.common.BeanPropertyConvert;
import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.model.Service;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.cms.service.service.IServiceDS;
import com.zte.cms.service.update.wkfw.ls.IServiceUpdateWkfwCheck;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;

public class ServiceUpdateApply extends WorkflowBase implements IServiceUpdateApply
{

    private Log log = SSBBus.getLog(getClass());
    private IServiceDS serviceds = null;
    private IServiceBufDS serviceBufds = null;
    protected IServiceUpdateWkfwCheck serviceUpdateWkfwCheckLS = null;
    private String callbackProcess;
    public String getCallbackProcess()
    {
        return callbackProcess;
    }
    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    public IServiceDS getServiceds()
    {
        return serviceds;
    }

    public void setServiceds(IServiceDS serviceds)
    {
        this.serviceds = serviceds;
    }

    public void setServiceUpdateWkfwCheckLS(IServiceUpdateWkfwCheck serviceUpdateWkfwCheckLS)
    {
        this.serviceUpdateWkfwCheckLS = serviceUpdateWkfwCheckLS;
    }

    // 构造方法
    ServiceUpdateApply()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
    }

    public String apply(Service service) throws DomainServiceException
    {
        String checkResult = serviceUpdateWkfwCheckLS.checkServiceUpdateWkfw(service.getServiceindex());
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }
        return apply(service, null);
    }

    public String apply(Service service, String priority) throws DomainServiceException
    {
        log.debug("workflow for service update starting...");
        Service serviceBasic = new Service();
        serviceBasic.setServiceindex(service.getServiceindex());
        serviceBasic = serviceds.getService(serviceBasic);

        int basicStatus = serviceBasic.getStatus(); // 在修改状态之前得到基本表的状态

        serviceBasic.setStatus(120); // 审核中
        serviceBasic.setWorkflow(2); // 1：新增，2：修改
        serviceBasic.setWorkflowlife(0);
        serviceds.updateService(serviceBasic);

        // 把修改后的数据写入buf表
        ServiceBuf serviceDBBuf = new ServiceBuf();
        BeanCopier basicObjDBCopy = BeanCopier.create(Service.class, ServiceBuf.class, true);
        basicObjDBCopy.copy(serviceBasic, serviceDBBuf, new BeanPropertyConvert());

        serviceBufds.insertServiceBuf(serviceDBBuf); // 不需要index，直接写进去

        ServiceBuf servicebuf = new ServiceBuf();
        BeanCopier basicCopy = BeanCopier.create(Service.class, ServiceBuf.class, false);
        basicCopy.copy(service, servicebuf, null);
        servicebuf.setWorkflow(Integer.valueOf(2));
        servicebuf.setStatus(basicStatus); // 把基本表的状态存在buf表中
        serviceBufds.updateServiceBuf(servicebuf);

        // 发起工作流参数
        String pboName = servicebuf.getServicename() + "[" + servicebuf.getServiceid() + "]";
        String pboId = String.valueOf(service.getServiceindex());
        String pboType = ServiceConstants.getWorkflowType(serviceBasic.getWorkflow());
        String processName = pboType + ":" + pboName;

        String callbackBean = "";
        if (null != this.getCallbackProcess())
        {
            callbackBean = "callbackProcess," + this.getCallbackProcess();

        }

        WorkflowService wkflService = new WorkflowService();
        int flag = wkflService.startProcessInstance(this.getTemplateCode(), processName, pboId, pboName, pboType, "",
                callbackBean, priority);
        if (flag <= 0)
        {
            throw new DomainServiceException();
        }

        log.debug("workflow for service update ends...");
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_UDPATE_APPLY_SUCCESS);
    }

    public String audit(ServiceBuf serviceBuf, String taskId, String handleMsg, String handleEvent,
            String workflowStatusCode) throws DomainServiceException
    {

        String checkResult = serviceUpdateWkfwCheckLS.checkServiceUpdateWkfwAudit(serviceBuf.getServiceindex());
        if (checkResult.charAt(0) != '0')
        {
            return checkResult;
        }
        // 审核时，有可能在页面做了修改，因此需要更新
        serviceBufds.updateServiceBuf(serviceBuf);

        // 工作流处理
        WorkflowService wkflService = new WorkflowService();
        UserInfo loginUser = LoginMgt.getUserInfo();
        
        wkflService.completeTask(taskId, handleMsg, handleEvent);
        
        if (workflowStatusCode.equals("1"))
        {// 审核
            return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_UPDATE_AUDIT_SUCCESS);
        }
        else
        {
            return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_WKFW_UPDATE_AUDIT_SUCCESS);
        }
    }

    public void setServiceBufds(IServiceBufDS serviceBufds)
    {
        this.serviceBufds = serviceBufds;
    }

}
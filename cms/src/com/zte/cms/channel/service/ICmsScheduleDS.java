package com.zte.cms.channel.service;

import java.util.List;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsScheduleDS
{
    /**
     * 新增CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 批量更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsScheduleList(List<CmsSchedule> cmsScheduleList) throws DomainServiceException;

    /**
     * 根据条件更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsScheduleListByCond(List<CmsSchedule> cmsScheduleList) throws DomainServiceException;

    /**
     * 删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 批量删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsScheduleList(List<CmsSchedule> cmsScheduleList) throws DomainServiceException;

    /**
     * 根据条件删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsScheduleListByCond(List<CmsSchedule> cmsScheduleList) throws DomainServiceException;

    /**
     * 查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return CmsSchedule对象
     * @throws DomainServiceException ds异常
     */
    public CmsSchedule getCmsSchedule(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 根据条件查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return 满足条件的CmsSchedule对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSchedule> getCmsScheduleByCond(CmsSchedule cmsSchedule) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryForRecrod(CmsSchedule cmsSchedule, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 查询同一个频道下时间是否有重复
     * 
     * @param icmChannelSchedule
     * @return
     */
    public List<CmsSchedule> isTimeRepeated(CmsSchedule cmsSchedule) throws DomainServiceException;;
    
    /**
     * 根据条件查询CmsSchedule对象
     * 
     * @param cmsSchedule CmsSchedule对象
     * @return 满足条件的CmsSchedule对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSchedule> getCmsScheduleByID(CmsSchedule cmsSchedule) throws DomainServiceException;
}
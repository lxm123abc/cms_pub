package com.zte.cms.content.program.programsync.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IProgramPlatformSynDAO
{

     /**
	  * 根据主键查询CntTargetsysSyn对象
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象
	  * @throws DAOException dao异常
	  */
     public ProgramPlatformSyn getCntTargetsysSyn( ProgramPlatformSyn programPlatformSyn )throws DAOException;
 
     /**
	  * 根据主键查询CntTargetsysSyn对象
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象
	  * @throws DAOException dao异常
	  */
     public ProgramPlatformSyn getCntTargetsysBindSyn( ProgramPlatformSyn programPlatformSyn )throws DAOException;
          
     /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
     public List<ProgramPlatformSyn> getCntTargetsysSynByCond( ProgramPlatformSyn programPlatformSyn )throws DAOException;

     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param programPlatformSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramPlatformSyn programPlatformSyn, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     /**
      * 按条件查询
      * @param programPlatformSyn
      * @return
      * @throws DAOException
      */
     public int getCntTargetsysBindSynPub( ProgramPlatformSyn programPlatformSyn )throws DAOException;
     
     /**
      * 按条件查询
      * @param programPlatformSyn
      * @return
      * @throws DAOException
      */
     public ProgramPlatformSyn getProgramPlatformSyn3( ProgramPlatformSyn cntPlatformSyn )throws DAOException;
     
     /**
      * 按条件查询
      * @param programPlatformSyn
      * @return
      * @throws DAOException
      */
     public ProgramPlatformSyn getProgramPlatformSyn2( ProgramPlatformSyn cntPlatformSyn )throws DAOException;

}
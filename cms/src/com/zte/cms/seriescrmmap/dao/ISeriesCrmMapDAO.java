package com.zte.cms.seriescrmmap.dao;

import java.util.List;

import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ISeriesCrmMapDAO
{
    /**
     * 新增SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DAOException dao异常
     */
    public void insertSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据主键更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DAOException dao异常
     */
    public void updateSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据条件更新SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap更新条件
     * @throws DAOException dao异常
     */
    public void updateSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据主键删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @throws DAOException dao异常
     */
    public void deleteSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据条件删除SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap删除条件
     * @throws DAOException dao异常
     */
    public void deleteSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据主键查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @return 满足条件的SeriesCrmMap对象
     * @throws DAOException dao异常
     */
    public SeriesCrmMap getSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据条件查询SeriesCrmMap对象
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @return 满足条件的SeriesCrmMap对象集
     * @throws DAOException dao异常
     */
    public List<SeriesCrmMap> getSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException;

    /**
     * 根据条件分页查询SeriesCrmMap对象，作为查询条件的参数
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询SeriesCrmMap对象，作为查询条件的参数
     * 
     * @param seriesCrmMap SeriesCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
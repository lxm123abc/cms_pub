package com.zte.cms.service.service;

import java.util.List;
import com.zte.cms.service.model.ServiceBuf;
import com.zte.cms.service.dao.IServiceBufDAO;
import com.zte.cms.service.service.IServiceBufDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceBufDS implements IServiceBufDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceBufDAO dao = null;

    public void setDao(IServiceBufDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException
    {
        log.debug("insert serviceBuf starting...");
        try
        {
            dao.insertServiceBuf(serviceBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceBuf end");
    }

    public void updateServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException
    {
        log.debug("update serviceBuf by pk starting...");
        try
        {
            dao.updateServiceBuf(serviceBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceBuf by pk end");
    }

    public void updateServiceBufList(List<ServiceBuf> serviceBufList) throws DomainServiceException
    {
        log.debug("update serviceBufList by pk starting...");
        if (null == serviceBufList || serviceBufList.size() == 0)
        {
            log.debug("there is no datas in serviceBufList");
            return;
        }
        try
        {
            for (ServiceBuf serviceBuf : serviceBufList)
            {
                dao.updateServiceBuf(serviceBuf);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceBufList by pk end");
    }

    public void removeServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException
    {
        log.debug("remove serviceBuf by pk starting...");
        try
        {
            dao.deleteServiceBuf(serviceBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceBuf by pk end");
    }

    public void removeServiceBufList(List<ServiceBuf> serviceBufList) throws DomainServiceException
    {
        log.debug("remove serviceBufList by pk starting...");
        if (null == serviceBufList || serviceBufList.size() == 0)
        {
            log.debug("there is no datas in serviceBufList");
            return;
        }
        try
        {
            for (ServiceBuf serviceBuf : serviceBufList)
            {
                dao.deleteServiceBuf(serviceBuf);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceBufList by pk end");
    }

    public ServiceBuf getServiceBuf(ServiceBuf serviceBuf) throws DomainServiceException
    {
        log.debug("get serviceBuf by pk starting...");
        ServiceBuf rsObj = null;
        try
        {
            rsObj = dao.getServiceBuf(serviceBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceBufList by pk end");
        return rsObj;
    }

    public List<ServiceBuf> getServiceBufByCond(ServiceBuf serviceBuf) throws DomainServiceException
    {
        log.debug("get serviceBuf by condition starting...");
        List<ServiceBuf> rsList = null;
        try
        {
            rsList = dao.getServiceBufByCond(serviceBuf);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceBuf by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get serviceBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceBuf, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceBuf page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceBuf serviceBuf, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get serviceBuf page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceBuf, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceBuf>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceBuf page info by condition end");
        return tableInfo;
    }
}

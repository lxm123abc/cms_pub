/**
 * QueryMediaInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zte.cms.basicdata.mediainfo;

public class QueryMediaInfoResponse implements java.io.Serializable
{
    /* 状态：0表示成功、非0表示失败 */
    private int status;

    /* 状态的补充描述 */
    private java.lang.String description;

    /* 视音频格式信息 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormat AVFormat;

    /* 文件信息 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfo fileInfo;

    /* 扩展属性 */
    private com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute[] extendAttribute;

    public QueryMediaInfoResponse()
    {
    }

    public QueryMediaInfoResponse(int status, java.lang.String description,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormat AVFormat,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfo fileInfo,
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute[] extendAttribute)
    {
        this.status = status;
        this.description = description;
        this.AVFormat = AVFormat;
        this.fileInfo = fileInfo;
        this.extendAttribute = extendAttribute;
    }

    /**
     * Gets the status value for this QueryMediaInfoResponse.
     * 
     * @return status * 状态：0表示成功、非0表示失败
     */
    public int getStatus()
    {
        return status;
    }

    /**
     * Sets the status value for this QueryMediaInfoResponse.
     * 
     * @param status * 状态：0表示成功、非0表示失败
     */
    public void setStatus(int status)
    {
        this.status = status;
    }

    /**
     * Gets the description value for this QueryMediaInfoResponse.
     * 
     * @return description * 状态的补充描述
     */
    public java.lang.String getDescription()
    {
        return description;
    }

    /**
     * Sets the description value for this QueryMediaInfoResponse.
     * 
     * @param description * 状态的补充描述
     */
    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    /**
     * Gets the AVFormat value for this QueryMediaInfoResponse.
     * 
     * @return AVFormat * 视音频格式信息
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormat getAVFormat()
    {
        return AVFormat;
    }

    /**
     * Sets the AVFormat value for this QueryMediaInfoResponse.
     * 
     * @param AVFormat * 视音频格式信息
     */
    public void setAVFormat(com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseAVFormat AVFormat)
    {
        this.AVFormat = AVFormat;
    }

    /**
     * Gets the fileInfo value for this QueryMediaInfoResponse.
     * 
     * @return fileInfo * 文件信息
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfo getFileInfo()
    {
        return fileInfo;
    }

    /**
     * Sets the fileInfo value for this QueryMediaInfoResponse.
     * 
     * @param fileInfo * 文件信息
     */
    public void setFileInfo(com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseFileInfo fileInfo)
    {
        this.fileInfo = fileInfo;
    }

    /**
     * Gets the extendAttribute value for this QueryMediaInfoResponse.
     * 
     * @return extendAttribute * 扩展属性
     */
    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute[] getExtendAttribute()
    {
        return extendAttribute;
    }

    /**
     * Sets the extendAttribute value for this QueryMediaInfoResponse.
     * 
     * @param extendAttribute * 扩展属性
     */
    public void setExtendAttribute(
            com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute[] extendAttribute)
    {
        this.extendAttribute = extendAttribute;
    }

    public com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute getExtendAttribute(int i)
    {
        return this.extendAttribute[i];
    }

    public void setExtendAttribute(int i, com.zte.cms.basicdata.mediainfo.QueryMediaInfoResponseExtendAttribute _value)
    {
        this.extendAttribute[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj)
    {
        if (!(obj instanceof QueryMediaInfoResponse))
            return false;
        QueryMediaInfoResponse other = (QueryMediaInfoResponse) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null)
        {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && this.status == other.getStatus()
                && ((this.description == null && other.getDescription() == null) || (this.description != null && this.description
                        .equals(other.getDescription())))
                && ((this.AVFormat == null && other.getAVFormat() == null) || (this.AVFormat != null && this.AVFormat
                        .equals(other.getAVFormat())))
                && ((this.fileInfo == null && other.getFileInfo() == null) || (this.fileInfo != null && this.fileInfo
                        .equals(other.getFileInfo())))
                && ((this.extendAttribute == null && other.getExtendAttribute() == null) || (this.extendAttribute != null && java.util.Arrays
                        .equals(this.extendAttribute, other.getExtendAttribute())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode()
    {
        if (__hashCodeCalc)
        {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getStatus();
        if (getDescription() != null)
        {
            _hashCode += getDescription().hashCode();
        }
        if (getAVFormat() != null)
        {
            _hashCode += getAVFormat().hashCode();
        }
        if (getFileInfo() != null)
        {
            _hashCode += getFileInfo().hashCode();
        }
        if (getExtendAttribute() != null)
        {
            for (int i = 0; i < java.lang.reflect.Array.getLength(getExtendAttribute()); i++)
            {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtendAttribute(), i);
                if (obj != null && !obj.getClass().isArray())
                {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            QueryMediaInfoResponse.class, true);

    static
    {
        typeDesc.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">QueryMediaInfoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "AVFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>QueryMediaInfoResponse>AVFormat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "FileInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>QueryMediaInfoResponse>FileInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendAttribute");
        elemField.setXmlName(new javax.xml.namespace.QName("NewMediaCMS", "ExtendAttribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("NewMediaCMS", ">>QueryMediaInfoResponse>ExtendAttribute"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
    {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}

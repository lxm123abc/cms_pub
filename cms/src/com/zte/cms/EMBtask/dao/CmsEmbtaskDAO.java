package com.zte.cms.EMBtask.dao;

import java.util.List;

import com.zte.cms.EMBtask.dao.ICmsEmbtaskDAO;
import com.zte.cms.EMBtask.model.CmsEmbtask;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsEmbtaskDAO extends DynamicObjectBaseDao implements ICmsEmbtaskDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException
    {
        log.debug("insert cmsEmbtask starting...");
        super.insert("insertCmsEmbtask", cmsEmbtask);
        log.debug("insert cmsEmbtask end");
    }

    public void updateCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException
    {
        log.debug("update cmsEmbtask by pk starting...");
        super.update("updateCmsEmbtask", cmsEmbtask);
        log.debug("update cmsEmbtask by pk end");
    }

    public void deleteCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException
    {
        log.debug("delete cmsEmbtask by pk starting...");
        super.delete("deleteCmsEmbtask", cmsEmbtask);
        log.debug("delete cmsEmbtask by pk end");
    }

    public CmsEmbtask getCmsEmbtask(CmsEmbtask cmsEmbtask) throws DAOException
    {
        log.debug("query cmsEmbtask starting...");
        CmsEmbtask resultObj = (CmsEmbtask) super.queryForObject("getCmsEmbtask", cmsEmbtask);
        log.debug("query cmsEmbtask end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsEmbtask> getCmsEmbtaskByCond(CmsEmbtask cmsEmbtask) throws DAOException
    {
        log.debug("query cmsEmbtask by condition starting...");
        List<CmsEmbtask> rList = (List<CmsEmbtask>) super.queryForList("queryCmsEmbtaskListByCond", cmsEmbtask);
        log.debug("query cmsEmbtask by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsEmbtask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsEmbtaskListCntByCond", cmsEmbtask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsEmbtask> rsList = (List<CmsEmbtask>) super.pageQuery("queryCmsEmbtaskListByCond", cmsEmbtask,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsEmbtask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsEmbtask by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsEmbtaskListCntByCond", cmsEmbtask)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsEmbtask> rsList = (List<CmsEmbtask>) super.pageQuery("queryCmsEmbtaskListByCondcolumn", cmsEmbtask,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsEmbtask by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerySub(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsEmbtaskListByCondcolumn", "queryCmsEmbtaskListCntByCondcolumn",
                cmsEmbtask, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryEncryption(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsEncryptaskListByCondcolumn", "queryCmsEncryptaskListCntByCondcolumn",
                cmsEmbtask, start, pageSize, puEntity);
    }    
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsEmbtask cmsEmbtask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsEmbtaskListByCond", "queryCmsEmbtaskListCntByCond", cmsEmbtask, start,
                pageSize, puEntity);
    }

}
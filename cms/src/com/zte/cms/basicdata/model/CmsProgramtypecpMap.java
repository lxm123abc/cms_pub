package com.zte.cms.basicdata.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsProgramtypecpMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.String typeid;
    private java.lang.String cpid;

    private String typename;
    private String cpcnshortname;
    private String status;
    private Long typeindex;
    private String publishstatus;
    private Long clslevel;
    private String parentid;
    private String typedesc;
    private String operid;

    public String getOperid()
    {
        return operid;
    }

    public void setOperid(String operid)
    {
        this.operid = operid;
    }

    public String getTypedesc()
    {
        return typedesc;
    }

    public void setTypedesc(String typedesc)
    {
        this.typedesc = typedesc;
    }

    public String getParentid()
    {
        return parentid;
    }

    public void setParentid(String parentid)
    {
        this.parentid = parentid;
    }

    public Long getClslevel()
    {
        return clslevel;
    }

    public void setClslevel(Long clslevel)
    {
        this.clslevel = clslevel;
    }

    public String getPublishstatus()
    {
        return publishstatus;
    }

    public void setPublishstatus(String publishstatus)
    {
        this.publishstatus = publishstatus;
    }

    public Long getTypeindex()
    {
        return typeindex;
    }

    public void setTypeindex(Long typeindex)
    {
        this.typeindex = typeindex;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    public String getTypename()
    {
        return typename;
    }

    public void setTypename(String typename)
    {
        this.typename = typename;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.String getTypeid()
    {
        return typeid;
    }

    public void setTypeid(java.lang.String typeid)
    {
        this.typeid = typeid;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("typeid", "TYPEID");
        this.addRelation("cpid", "CPID");
    }
}

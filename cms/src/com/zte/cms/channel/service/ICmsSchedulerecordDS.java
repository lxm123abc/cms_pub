package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsSchedulerecordDS
{
    /**
     * 新增CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 批量更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSchedulerecordList(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException;

    /**
     * 根据条件更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsSchedulerecordListByCond(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException;

    /**
     * 删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 批量删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSchedulerecordList(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException;

    /**
     * 根据条件删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsSchedulerecordListByCond(List<CmsSchedulerecord> cmsSchedulerecordList)
            throws DomainServiceException;

    /**
     * 查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return CmsSchedulerecord对象
     * @throws DomainServiceException ds异常
     */
    public CmsSchedulerecord getCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DomainServiceException;

    /**
     * 根据条件查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return 满足条件的CmsSchedulerecord对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSchedulerecord> getCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    public TableDataInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据条件查询CmsSchedulerecord对象
     * 
     * @param cmsSchedulerecord CmsSchedulerecord对象
     * @return 满足条件的CmsSchedulerecord对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsSchedulerecord> getCmsSchedulerecordByID(CmsSchedulerecord cmsSchedulerecord)
            throws DomainServiceException;
}
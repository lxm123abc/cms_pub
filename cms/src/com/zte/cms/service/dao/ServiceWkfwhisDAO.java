package com.zte.cms.service.dao;

import java.util.List;

import com.zte.cms.service.dao.IServiceWkfwhisDAO;
import com.zte.cms.service.model.ServiceWkfwhis;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ServiceWkfwhisDAO extends DynamicObjectBaseDao implements IServiceWkfwhisDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException
    {
        log.debug("insert serviceWkfwhis starting...");
        super.insert("insertServiceWkfwhis", serviceWkfwhis);
        log.debug("insert serviceWkfwhis end");
    }

    public void updateServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException
    {
        log.debug("update serviceWkfwhis by pk starting...");
        super.update("updateServiceWkfwhis", serviceWkfwhis);
        log.debug("update serviceWkfwhis by pk end");
    }

    public void deleteServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException
    {
        log.debug("delete serviceWkfwhis by pk starting...");
        super.delete("deleteServiceWkfwhis", serviceWkfwhis);
        log.debug("delete serviceWkfwhis by pk end");
    }

    public ServiceWkfwhis getServiceWkfwhis(ServiceWkfwhis serviceWkfwhis) throws DAOException
    {
        log.debug("query serviceWkfwhis starting...");
        ServiceWkfwhis resultObj = (ServiceWkfwhis) super.queryForObject("getServiceWkfwhis", serviceWkfwhis);
        log.debug("query serviceWkfwhis end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceWkfwhis> getServiceWkfwhisByCond(ServiceWkfwhis serviceWkfwhis) throws DAOException
    {
        log.debug("query serviceWkfwhis by condition starting...");
        List<ServiceWkfwhis> rList = (List<ServiceWkfwhis>) super.queryForList("queryServiceWkfwhisListByCond",
                serviceWkfwhis);
        log.debug("query serviceWkfwhis by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize) throws DAOException
    {
        log.debug("page query serviceWkfwhis by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryServiceWkfwhisListCntByCond", serviceWkfwhis)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<ServiceWkfwhis> rsList = (List<ServiceWkfwhis>) super.pageQuery("queryServiceWkfwhisListByCond",
                    serviceWkfwhis, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query serviceWkfwhis by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ServiceWkfwhis serviceWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryServiceWkfwhisListByCond", "queryServiceWkfwhisListCntByCond",
                serviceWkfwhis, start, pageSize, puEntity);
    }

}
package com.zte.cms.basicdata.ftp.ls;

import com.zte.cms.basicdata.ftp.model.IcmFtpinfo;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * <p>
 * 文件名称: IIcmFtpinfoDS.java
 * </p>
 * <p>
 * 文件描述: ftp管理接口
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2010
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 主要提供ftp的增、删、改、查接口
 * </p>
 */
public interface IIcmFtpinfoLS
{

    /**
     * <p>
     * 新增一条ftp记录
     * </p>
     * 
     * @param icmFtpinfo ftp实体
     * @return 返回操作结果码
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */
    public String insertIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws Exception;

    /**
     * <p>
     * 分页查询ftp记录
     * </p>
     * 
     * @param icmFtpinfo ftp实体
     * @param start 开始行数
     * @param pageSize 每页行数
     * @return 返回分页记录
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */

    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize) throws Exception;

    /**
     * <p>
     * 分页查询ftp记录
     * </p>
     * 
     * @param icmFtpinfo ftp实体
     * @param start 开始行数
     * @param pageSize 每页行数
     * @param puEntity 表格中需要排序的字段实体
     * @return 返回分页记录
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */
    public TableDataInfo pageInfoQuery(IcmFtpinfo icmFtpinfo, int start, int pageSize, PageUtilEntity puEntity)
            throws Exception;

    /**
     * <p>
     * 删除一条ftp记录
     * </p>
     * 
     * @param index ftp主键值
     * @return 返回操作结果，类型String
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */
    public String deleteIcmFtpinfo(Long infoindex) throws DomainServiceException;

    /**
     * <p>
     * 根据ftp主键值，查询ftp
     * </p>
     * 
     * @param infoindex ftp记录主键值
     * @return IcmFtpinfo ftp信息实体
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */
    public IcmFtpinfo getIcmFtpinfo(Long infoindex) throws DomainServiceException;

    /**
     * <p>
     * 更新一条ftp记录
     * </p>
     * 
     * @param infoindex ftp记录主键值
     * @return 返回操作结果，类型String
     * @throws Exception 抛出异常，同时异常会被输出Log4J日志中，ERROR级别
     * @see 表icm_ftpinfo
     * @see com.zte.cms.basicdata.ftp.model.IcmFtpinfo
     */
    public String updateIcmFtpinfo(IcmFtpinfo icmFtpinfo) throws DomainServiceException;

    /**
     * 获取临时区目录
     * 
     * @return 临时区目录
     * @throws DomainServiceException
     */
    public String getStroageareaPath() throws DomainServiceException;

    /**
     * 获取临时区绑定的空间对象
     * 
     * @return CmsStoragemanage 对象
     * @throws DomainServiceException
     */
    public CmsStoragemanage getStroagmanage(String storageAreatype) throws DomainServiceException;

    /**
     * 修改之前查询服务器上该目录下是否存在子目录或子文件
     */
    public String isExistFiles(Long infoindex) throws DomainServiceException;

    /*******************************************************************************************************************
     * 公告栏上显示FTP信息
     ******************************************************************************************************************/
    public IcmFtpinfo getIcmFtpinfoForBulletin() throws DomainServiceException;

}

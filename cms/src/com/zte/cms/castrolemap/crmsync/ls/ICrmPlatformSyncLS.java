package com.zte.cms.castrolemap.crmsync.ls;

import java.util.List;

import com.zte.cms.castrolemap.crmsync.model.CrmPlatformSync;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface ICrmPlatformSyncLS
{
    /**
     * 根据条件分页查询CrmPlatformSync对象
     * @param crmPlatformSync CrmPlatformSync对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQuery(CrmPlatformSync crmPlatformSync, int start, int pageSize) throws Exception;

    
    /**
     * 角色发布到平台
     *
     * @param castrolemapindex 角色index 
     * @param platform 平台类型： 1:2.0平台，2:3.0平台
     * @param syncType  同步类型：1-REGIST，2-UPDATE，3-DELETE
     * @return  操作结果
     * @throws Exception ds异常
     */
    public String publishCrmToPlatform(Long castrolemapindex, int platform ,int syncType) throws Exception;
    
    public String batchPublishCrmToPlatform(List<Castrolemap> castList, int platform ,int syncType) throws Exception;
}

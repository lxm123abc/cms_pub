package com.zte.cms.cntSyncTask.ls;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.ResourceManager;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.service.servicepublishmanager.model.ServiceTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 内容同步任务监控的管理，包括同步内容的查询，以及查看详情操作
 * 
 * @author Chenhu
 * 
 */

public class CntSyncTaskLS extends DynamicObjectBaseDS implements ICntSyncTaskLS
{   //日志
	private Log log = SSBBus.getLog(getClass());
	private ICntSyncTaskDS cntSyncTaskDS = null;
	private IUsysConfigDS usysConfigDS;
	
	private IObjectSyncRecordDS  objectSyncRecordDS = null;//需要新增
	
    /**
     * 
     * @return ICntSyncTaskDS
     */
	public ICntSyncTaskDS getCntSyncTaskDS()
	{
		return cntSyncTaskDS;
	}

    /**
     * 
     * @param cntSyncTaskDS ICntSyncTaskDS
     */
	public void setCntSyncTaskDS(ICntSyncTaskDS cntSyncTaskDS)
	{
		this.cntSyncTaskDS = cntSyncTaskDS;
	}

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    /**
     * 查询服务列表 参数为三个，调用四个参数的方法
     * 
     * @param cntSyncTask CntSyncTask 查询的条件对象
     * @param start int起始行
     * @param pageSize  int每页显示行数
     * @return TableDataInfo返回查询的列表
     * @throws Exception Exception
     */ 
    public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize)throws Exception
	{
		log.debug( "query cntSyncTasklist starting..." );
		
		//进行时间格式的转换,将时间中的空格 冒号 等去掉
		String starttime1 = cntSyncTask.getStarttime1();
		if(null != starttime1 && !starttime1.equals(""))
		{
		starttime1 = starttime1.trim().replace(" ", "").replace(":", "").replace("-", "");
		cntSyncTask.setStarttime1(starttime1);
		}
		String starttime2 = cntSyncTask.getStarttime2();
		if(null != starttime2 && !starttime2.equals(""))
        {
		starttime2 = starttime2.trim().replace(" ", "").replace(":", "").replace("-", "");
		cntSyncTask.setStarttime2(starttime2);
        }
		String endtime1 = cntSyncTask.getEndtime1();
		if(null != endtime1 && !endtime1.equals(""))
        {
		endtime1 = endtime1.trim().replace(" ", "").replace(":", "").replace("-", "");
		cntSyncTask.setEndtime1(endtime1);
        }
		String endtime2 = cntSyncTask.getEndtime2();
		if(null != endtime2 && !endtime2.equals(""))
        {
		endtime2 = endtime2.trim().replace(" ", "").replace(":", "").replace("-", "");
		cntSyncTask.setEndtime2(endtime2);
        }

		//特殊字符处理
		if(cntSyncTask.getResultdesc()!=null && !"".equals(cntSyncTask.getResultdesc()))
		{
		    cntSyncTask.setResultdesc(EspecialCharMgt.conversion(cntSyncTask.getResultdesc()));
        }
		TableDataInfo tableInfo = null;
		PageUtilEntity puEntity = new PageUtilEntity();
		puEntity.setIsAlwaysSearchSize(true);
		puEntity.setIsAsc(false);
		puEntity.setOrderByColumn("taskindex");
		
		try
        {
            tableInfo = pageInfoQuerycntsync(cntSyncTask, start, pageSize, puEntity);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        log.debug("query cntSyncTask list ends");

        return tableInfo;
	}
   


	
    /**
     * 查询服务列表 参数为四个参数
     * 
     * @param cntSyncTask CntSyncTask传入对象
     * @param start int起始行
     * @param pageSize int每页显示行数
     * @param puEntity PageUtilEntity默认排序
     * @return TableDataInfo查询得到的列表
     * @throws Exception Exception
     */
	public TableDataInfo pageInfoQuerycntsync(CntSyncTask cntSyncTask, int start, int pageSize, 
			PageUtilEntity puEntity)throws Exception
	{
		log.debug( "get cntSyncTask page info by condition starting..." );
		
		TableDataInfo tableInfo = null;
		try
		{
			tableInfo = cntSyncTaskDS.pageInfoQuerycntsync(cntSyncTask, start, pageSize, puEntity);
		}
		catch( Exception dsEx )
		{
			log.error( "ds exception:", dsEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( dsEx );
		}
		log.debug( "get cntSyncTask page info by condition end" );
		return tableInfo;
	}

    /**
     * @param cntSyncTask CntSyncTask
     * @return CntSyncTask
     * @throws Exception Exception
     */
    public CntSyncTask getCntSynchorTask(CntSyncTask cntSyncTask) throws Exception
    {

    	CntSyncTask rsObj = null;
        try
        {
            rsObj = cntSyncTaskDS.getCntSynchorTask(cntSyncTask);
        }
        catch (Exception dsEx)
        {
            throw new Exception(dsEx.getMessage());
        }
        log.debug("get cntSyncTaskList by pk end");
        return rsObj;
    }

    public String getPreviewXMLPath(Long taskindex) throws Exception
    {

        log.debug("getPreviewXMLPath  starting...");

        CntSyncTask cntSyncTask = new CntSyncTask();
        UsysConfig usysConfig = new UsysConfig();
        String previewPath = "";
        try
        {
            usysConfig.setCfgkey("cms.cntsyn.ftpaddress");
            List<UsysConfig> usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);

            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);

                cntSyncTask.setTaskindex(taskindex);
                cntSyncTask = cntSyncTaskDS.getCntSynchorTask(cntSyncTask);
                if (cntSyncTask == null)
                {
     
                    throw new Exception(ResourceMgt.findDefaultText("subcnt.notexist"));
                }
                else
                {
                    previewPath = usysConfig.getCfgvalue() + cntSyncTask.getContentmngxmlurl();
                }
            }
            else
            {// "272201"：未配置 系统参数WINDOWS共享访问点
                //throw new Exception("未配置系统参数WINDOWS共享访问点");
                throw new Exception(ResourceMgt.findDefaultText("parameter.notset.windows.sharepoint"));
            }
        }
        catch (Exception e)
        {
            log.error("cntSyncTask:" + e.getMessage());
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("getPreviewXMLPath end");
        return previewPath;
    }
    /**
     * 
     * 重新操作函数
     * 
     * */
    public String insertRerun(Long taskindex)
    {
        // TODO Auto-generated method stub
        log.debug("rerun start");
        
        try{
            //根据taskindex在任务历史表中查找该条任务//采用union查找
            CntSyncTask cntsynctask = new CntSyncTask();
            cntsynctask.setTaskindex(taskindex);
            CntSyncTask cntSyncTasktemp = null;
           
            cntSyncTasktemp = cntSyncTaskDS.getCntSynchorTask(cntsynctask);
           
            if(cntSyncTasktemp == null){
                return "1:任务已被删除或者不存在";
            }
            if(cntSyncTasktemp.getStatus()!=4){
                return "1:任务不是失败状态，不能重新执行";
            }
            
          //获取历史表中batchid相同的内容
            CntSyncTask sameBatchidTask =  new CntSyncTask();
            sameBatchidTask.setBatchid(cntSyncTasktemp.getBatchid());
            List<CntSyncTask> sameBatchidTaskList = cntSyncTaskDS.getCntSynhisListBybatchid(sameBatchidTask);
            int sameBatchidSize = sameBatchidTaskList.size();
            int unsuccessedsize = 0;
            for(int i = 0; i< sameBatchidSize; i++){
                CntSyncTask task =  sameBatchidTaskList.get(i);
                unsuccessedsize = task.getStatus() != 3? unsuccessedsize+1 : unsuccessedsize;//不成功+1
            }
          //根据taskindex找出所有的日志对象
        ObjectSyncRecord objectsyncrecord = new ObjectSyncRecord();
        objectsyncrecord.setTaskindex(taskindex);
        
        List<ObjectSyncRecord> objectsyncrecordList = null;
        
        objectsyncrecordList = objectSyncRecordDS.getObjectSyncRecordByTaskindex(objectsyncrecord);//多份日志也只取一份
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
        if(objectsyncrecordList != null && objectsyncrecordList.size()!=0){//有对象日志则插入对象日志表，无则不用操作
            for(int i=0; i<objectsyncrecordList.size(); i++){
                
                Long syncIndex = (Long)this.getPrimaryKeyGenerator().getPrimarykey("object_sync_record_index"); 
                objectsyncrecordList.get(i).setSyncindex(syncIndex);
                objectsyncrecordList.get(i).setStarttime(dateformat.format(new Date()));
                objectsyncrecordList.get(i).setEndtime(null);
                objectsyncrecordList.get(i).setResultcode(null);
                objectsyncrecordList.get(i).setResultdesc(null);
                
                objectSyncRecordDS.insertObjectSyncRecord(objectsyncrecordList.get(i));//插入对象日志操作
            }
        }
        if(unsuccessedsize == 1){//仅有一个网元的失败
        //删除任务历史表中的该任务 deleteCntSyncHisTask
         cntSyncTaskDS.removeCntSyncTask(cntsynctask);
        //把任务插到同步任务表中
        cntSyncTasktemp.setStatus(1);
        
        cntSyncTasktemp.setStarttime(dateformat.format(new Date()));
        cntSyncTasktemp.setEndtime(null);
        cntSyncTasktemp.setResultcode(null);
        cntSyncTasktemp.setResultdesc(null);
        cntSyncTasktemp.setResultfileurl(null);
        
        cntSyncTaskDS.insertCntSyncTask(cntSyncTasktemp);
        }else{//不成功的网元不止一个
            for(int i=0; i<sameBatchidSize; i++ ){
                CntSyncTask task =  sameBatchidTaskList.get(i);
                if(task.getStatus() != 3 && String.valueOf(task.getTaskindex()).equals(String.valueOf(taskindex))){//状态置为1
                    //删除任务历史表中的该任务 deleteCntSyncHisTask
                    cntSyncTaskDS.removeCntSyncTask(task);
                   //把任务插到同步任务表中
                    task.setStatus(1);
                    task.setStarttime(dateformat.format(new Date()));
                    task.setEndtime(null);
                    task.setResultcode(null);
                    task.setResultdesc(null);
                    task.setResultfileurl(null);
                   cntSyncTaskDS.insertCntSyncTask(task);
                    
                }else if(task.getStatus() != 3){//状态置为0
                  //删除任务历史表中的该任务 deleteCntSyncHisTask
                   cntSyncTaskDS.removeCntSyncTask(task); 
                   task.setResultdesc(null);
                       task.setStarttime(dateformat.format(new Date()));
                   cntSyncTaskDS.insertCntSyncTask(task);
                }else continue;
                
            }
        }
        }
        
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "1:数据库操作出错";
        }
        
        
        log.debug("rerun end");
     // 写操作日志
        CommonLogUtil.insertOperatorLog(String.valueOf(taskindex), CommonLogConstant.MGTTYPE_TASKSYN,
        CommonLogConstant.OPERTYPE_TASK_RERUN,       
        CommonLogConstant.OPERTYPE_TASK_RERUN_INFO,
        CommonLogConstant.RESOURCE_OPERATION_SUCCESS);
        return "0:操作成功";
    }

    /**
     * 
     * 批量重新执行操作函数，调用重新执行函数，一个任务一个任务执行
     * 
     * */
    public String batchRerun(List<CntSyncTask> cntSyncTaskList)
    {
        log.debug("batch rerun start...");
        Integer ifail = 0;          //重新发布发布失败个数
        Integer iSuccessed = 0;     //重新发布成功个数
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int rstindex = 1;
        String retInfo = "";
        int size = cntSyncTaskList.size();
 
        for(CntSyncTask cntsynctask:cntSyncTaskList)
        {
            retInfo = insertRerun(cntsynctask.getTaskindex());
            if(retInfo.substring(0, 1).equals("0")){
                
                iSuccessed++;
                String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_SUCCESS);            
                operateInfo = new OperateInfo(String.valueOf(rstindex++),cntsynctask.getTaskindex().toString(), 
                resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                returnInfo.appendOperateInfo(operateInfo);
             }else{
                 ifail++;
                 String resultStr = ResourceMgt.findDefaultText(CommonLogConstant.RESOURCE_OPERATION_FAIL);            
                 operateInfo = new OperateInfo(String.valueOf(rstindex++), cntsynctask.getTaskindex().toString(), 
                 resultStr.substring(2, resultStr.length()), retInfo.substring(2));
                 returnInfo.appendOperateInfo(operateInfo);            
             } 
            
        }
        if (iSuccessed ==size)
        {
            returnInfo.setReturnMessage("成功数量：" + iSuccessed);
            returnInfo.setFlag("0");
        }
        else
        {
            if (ifail == size)
            {
                returnInfo.setReturnMessage("失败数量：" + ifail);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setReturnMessage("成功数量：" + iSuccessed + "  失败数量：" +ifail);
                returnInfo.setFlag("2");
            }
        }
        log.debug("batch rerun end...");
        return returnInfo.toString();
    }

    /*
     * 查询同一批次号任务方法！
     * 
     */
    public TableDataInfo getSameBatchidTask(CntSyncTask cntSyncTask, int start, int pageSize) throws Exception
    {
        log.debug("serch the same batchid task start...");
        TableDataInfo tableInfo = null;
        
        CntSyncTask cntSyncTasktemp = cntSyncTaskDS.getCntSynchorTask(cntSyncTask);
 
      //获取历史表中batchid相同的内容
        CntSyncTask sameBatchidTask =  new CntSyncTask();
        sameBatchidTask.setBatchid(cntSyncTasktemp.getBatchid());       
        
        try
        {
            tableInfo = pageInfoQuerycntsync(sameBatchidTask, start, pageSize);
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        
          
        log.debug("serch the same batchid task end...");
                
        return tableInfo;
        
    }
    
}
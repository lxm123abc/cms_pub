package com.zte.ucm.chkconf;
import com.zte.ucm.Resultlog;
import com.zte.ucm.config.Config;
import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.File;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlConfigReader
{
  Logger log = LoggerFactory.getLogger(XmlConfigReader.class);

  public void readXMLConfig(Config paramConfig) throws Exception {
    String str1 = "";
    File localFile = new File(paramConfig.getFile());
    Resultlog localResultlog = new Resultlog();
    if (!localFile.exists())
    {
      this.log.warning("config file[" + paramConfig.getFile() + "] is not exist!!!");

      paramConfig.setCode(null);

      return;
    }
    try
    {
      SAXReader localSAXReader = new SAXReader();
      // 对dom4j的SAXReader进行设置，不去下载外部dtd文件来对xml进行验证
      localSAXReader.setValidation(false);
      localSAXReader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
      Document localDocument = localSAXReader.read(localFile);
      String str2 = localDocument.getXMLEncoding();
      List localList = localDocument.selectNodes(paramConfig.getXpath());

      if ((localList != null) && (localList.size() > 0))
      {
        String str3 = paramConfig.getType().trim();
        String str4 = "";
        Element localElement;
        if (("0".equals(str3)) || ("".equals(str3)))
        {
          localElement = (Element)localList.get(0);
          paramConfig.setValue(localElement.getText());
          this.log.info("Operation success:config file[" + paramConfig.getFile() + "] node[" + paramConfig.getXpath() + "] value[" + paramConfig.getValue() + "]");
        }
        else if ("1".equals(str3))
        {
          localElement = (Element)localList.get(0);
          Attribute localAttribute = localElement.attribute(paramConfig.getAttrname());
          if (localAttribute != null)
          {
            paramConfig.setValue(localAttribute.getValue());
            this.log.info("Operation success:config file[" + paramConfig.getFile() + "] node[" + paramConfig.getXpath() + "] attribute[" + paramConfig.getAttrname() + "] value[" + paramConfig.getValue() + "]");
          }
          else
          {
            this.log.error("Operation ERROR:Cannot find the attribute name[" + paramConfig.getAttrname() + "]");
            str1 = str1 + "1|Operation ERROR:Cannot find the attribute name[" + paramConfig.getAttrname() + "]" + "\n";
            localResultlog.appendLog(str1);
          }
        }

      }
      else
      {
         this.log.warning("Operation WARN:Cannot find the config, please check the xpath[" + paramConfig.getXpath() + "] of the item[" + paramConfig.getTag() + "] in config file[" + paramConfig.getFile() + "]");

         paramConfig.setCode(null);
      }

    }
    catch (DocumentException localDocumentException)
    {
       this.log.error("Operation ERROR:ConfigParser getConfig error");
       str1 = str1 + "Operation ERROR:ConfigParser getConfig error\n";
       localResultlog.appendLog(str1);

       throw localDocumentException;
    }
  }
}
/**
 * 当table没有数据时，提示用户 "对不起，没有符合条件的记录"
 * @param obj       服务返回的数据对象
 * @param tableId   table的id
 * @return
 */
function noDataTrips(obj, tableId)
{
    
    //分页数据返回的是PageDataInfo类型，其data属性为数据数组
    if((obj.data == undefined && obj.length==0)||obj.data.length == 0)
    {
        //设置所关联的表格ID
        var ztableid = tableId;
        //显示出来的表格
        var displayTB = getTableById(ztableid).table;
        //显示出来的表格的列数
        var colNum = displayTB.rows[0].cells.length;
        //因为没有数据，新增一空白提示行
        var tipRow = displayTB.insertRow(-1);
        //新建提示单元，设置其属性以及要显示的文本
        var tipTd = document.createElement("td");
        tipTd.style.cssText = "color:red; text-align:center; font-size:12px";
        tipTd.setAttribute("colSpan",colNum);
        tipTd.insertBefore(document.createTextNode($res_entry('msg.note.notListforCondition')),null);
        //将提示单元加入行
        tipRow.insertBefore(tipTd,null);
    }
    
}

package com.zte.cms.channel.wkfw.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;

import com.zte.cms.channel.wkfw.model.ScheduleWkfwCondition;

import com.zte.cms.channel.wkfw.service.IScheduleWkfwProcessWithMeDS;
import com.zte.cms.channel.wkfw.service.IScheduleWkfwTaskForMeDS;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

import com.zte.ssb.framework.base.util.PageUtilEntity;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleWkfwQueryLS implements IScheduleWkfwQueryLS
{
    protected IScheduleWkfwTaskForMeDS scheduleWkfwTaskForMeDS = null;
    protected IScheduleWkfwProcessWithMeDS scheduleWkfwProcessWithMeDS = null;

   

    public IScheduleWkfwTaskForMeDS getScheduleWkfwTaskForMeDS()
    {
        return scheduleWkfwTaskForMeDS;
    }

    public void setScheduleWkfwTaskForMeDS(IScheduleWkfwTaskForMeDS scheduleWkfwTaskForMeDS)
    {
        this.scheduleWkfwTaskForMeDS = scheduleWkfwTaskForMeDS;
    }

    public IScheduleWkfwProcessWithMeDS getScheduleWkfwProcessWithMeDS()
    {
        return scheduleWkfwProcessWithMeDS;
    }

    public void setScheduleWkfwProcessWithMeDS(IScheduleWkfwProcessWithMeDS scheduleWkfwProcessWithMeDS)
    {
        this.scheduleWkfwProcessWithMeDS = scheduleWkfwProcessWithMeDS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMeListCnt(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public int queryScheduleWkfwTaskForMeListCnt(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == scheduleWkfwTaskForMeCondition.getPboType()
                || ("").equals(scheduleWkfwTaskForMeCondition.getPboType()))
        {
            scheduleWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_SCHEDULE_ALL);
        }
        scheduleWkfwTaskForMeCondition.setExecutorId(ChannelConstants.getUserid());
        scheduleWkfwTaskForMeCondition.setTaskStatus("N");
        return scheduleWkfwTaskForMeDS.queryScheduleWkfwTaskForMeListCntByCond(scheduleWkfwTaskForMeCondition);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition, int, int)
     */
    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryScheduleWkfwTaskForMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int rangeStart,
            int fetchSize) throws DomainServiceException
    {

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        tableInfo = queryScheduleWkfwTaskForMe(scheduleWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryScheduleWkfwTaskForMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition, int rangeStart,
            int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == scheduleWkfwTaskForMeCondition.getPboType()
                || ("").equals(scheduleWkfwTaskForMeCondition.getPboType()))
        {
            scheduleWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_SCHEDULE_ALL);
        }
        scheduleWkfwTaskForMeCondition.setExecutorId(ChannelConstants.getUserid());
        scheduleWkfwTaskForMeCondition.setTaskStatus("N");

        TableDataInfo tableInfo = null;
        tableInfo = scheduleWkfwTaskForMeDS
                .pageInfoQuery(scheduleWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @return int
     * @throws DomainServiceException DomainServiceException
     */
    public int queryScheduleWkfwProcessWithMeListCnt(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == scheduleWkfwTaskForMeCondition.getPboType()
                || ("").equals(scheduleWkfwTaskForMeCondition.getPboType()))
        {
            scheduleWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_SCHEDULE_ALL);
        }
        scheduleWkfwTaskForMeCondition.setOpername(ChannelConstants.getUserid());
        scheduleWkfwTaskForMeCondition.setUserid(ChannelConstants.getUserid());
        return scheduleWkfwProcessWithMeDS.queryScheduleWkfwProcessWithMeListCntByCond(scheduleWkfwTaskForMeCondition);
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart rangeStart
     * @param fetchSize int
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryScheduleWkfwProcessWithMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException
    {
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);

        puEntity.setOrderByColumn("processStartTime");
        return queryScheduleWkfwProcessWithMe(scheduleWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    /**
     * @param channelWkfwTaskForMeCondition ChannelWkfwCondition
     * @param rangeStart int
     * @param fetchSize int
     * @param puEntity PageUtilEntity
     * @return TableDataInfo
     * @throws DomainServiceException DomainServiceException
     */
    public TableDataInfo queryScheduleWkfwProcessWithMe(ScheduleWkfwCondition scheduleWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == scheduleWkfwTaskForMeCondition.getPboType()
                || ("").equals(scheduleWkfwTaskForMeCondition.getPboType()))
        {
            scheduleWkfwTaskForMeCondition.setPboType(ChannelConstants.WKFWTYPE_SCHEDULE_ALL);
        }
        scheduleWkfwTaskForMeCondition.setOpername(ChannelConstants.getUserid());
        scheduleWkfwTaskForMeCondition.setUserid(ChannelConstants.getUserid());
        return scheduleWkfwProcessWithMeDS.pageInfoQuery(scheduleWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    /**
     * @param selectedValue String
     * @param needSelect Boolean
     * @return HashMap
     * 
     */
    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect)
    {
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);

        if (needSelect)
        {
            textList.add(ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_CHANNEL_OPTION_PLEASESELECT));
            valueList.add("CMS_SCHEDULE_%");
        }

        textList.add(ResourceMgt.findDefaultText(ChannelConstants.RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD));
        valueList.add(ChannelConstants.WKFWTYPE_SCHEDULE_ADD);

        // textList.add(ResourceMgt.findDefaultText(StrategyConstant.RES_CMS_STRA_WKFWTYPENAME_STRATEGY_DROPOUT));
        // valueList.add(StrategyConstant.WKFWTYPE_STRATEGY_DROPOUT);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);
        return map;
    }

    /**
     * @return HashMap
     */
    public HashMap<String, String> getWorkflowTypeMap()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ChannelConstants.WKFWTYPE_SCHEDULE_ADD, ResourceMgt
                .findDefaultText(ChannelConstants.RES_CMS_STRA_WKFWTYPENAME_CHANNEL_ADD));

        return map;
    }
}

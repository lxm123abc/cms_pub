package com.zte.cms.storageArea.service;

import java.util.List;
import com.zte.cms.storageArea.model.CmsStoragearea;
import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStorageareaDS
{
    /**
     * 新增CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 更新CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 批量更新CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException;

    /**
     * 删除CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 批量删除CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsStorageareaList(List<CmsStoragearea> cmsStorageareaList) throws DomainServiceException;

    /**
     * 查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return CmsStoragearea对象
     * @throws DomainServiceException ds异常
     */
    public CmsStoragearea getCmsStoragearea(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 根据条件查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象
     * @return 满足条件的CmsStoragearea对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsStoragearea> getCmsStorageareaByCond(CmsStoragearea cmsStoragearea) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsStoragearea对象
     * 
     * @param cmsStoragearea CmsStoragearea对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsStoragearea cmsStoragearea, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

}
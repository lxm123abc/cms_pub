package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.ICmsScheduleDAO;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsScheduleDAO extends DynamicObjectBaseDao implements ICmsScheduleDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsSchedule(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("insert cmsSchedule starting...");
        super.insert("insertCmsSchedule", cmsSchedule);
        log.debug("insert cmsSchedule end");
    }

    public void updateCmsSchedule(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("update cmsSchedule by pk starting...");
        super.update("updateCmsSchedule", cmsSchedule);
        log.debug("update cmsSchedule by pk end");
    }

    public void updateCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("update cmsSchedule by conditions starting...");
        super.update("updateCmsScheduleByCond", cmsSchedule);
        log.debug("update cmsSchedule by conditions end");
    }

    public void deleteCmsSchedule(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("delete cmsSchedule by pk starting...");
        super.delete("deleteCmsSchedule", cmsSchedule);
        log.debug("delete cmsSchedule by pk end");
    }

    public void deleteCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("delete cmsSchedule by conditions starting...");
        super.delete("deleteCmsScheduleByCond", cmsSchedule);
        log.debug("update cmsSchedule by conditions end");
    }

    public CmsSchedule getCmsSchedule(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("query cmsSchedule starting...");
        CmsSchedule resultObj = (CmsSchedule) super.queryForObject("getCmsSchedule", cmsSchedule);
        log.debug("query cmsSchedule end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsSchedule> getCmsScheduleByCond(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("query cmsSchedule by condition starting...");
        List<CmsSchedule> rList = (List<CmsSchedule>) super.queryForList("queryCmsScheduleListByCond", cmsSchedule);
        log.debug("query cmsSchedule by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsSchedule by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsScheduleListCntByCond", cmsSchedule)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSchedule> rsList = (List<CmsSchedule>) super.pageQuery("queryCmsScheduleListByCond", cmsSchedule,
                    start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsSchedule by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryForRecord(CmsSchedule cmsSchedule, int start, int pageSize) throws DAOException
    {
        log.debug("pageInfoQueryForRecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsScheduleForRecordListCntByCond", cmsSchedule))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSchedule> rsList = (List<CmsSchedule>) super.pageQuery("queryCmsScheduleForRecordListByCond",
                    cmsSchedule, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("pageInfoQueryForRecord by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSchedule cmsSchedule, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsScheduleListByCond", "queryCmsScheduleListCntByCond", cmsSchedule, start,
                pageSize, puEntity);
    }

    /**
     * 查询同一个频道下时间是否有重复
     * 
     * @param icmChannelSchedule
     * @return
     */
    public List<CmsSchedule> isTimeRepeated(CmsSchedule cmsSchedule) throws DAOException
    {
        List<CmsSchedule> list = (List<CmsSchedule>) super.queryForList("queryCmsScheduleListByCond", cmsSchedule);
        return list;

    }

    @Override
    public List<CmsSchedule> getCmsScheduleByID(CmsSchedule cmsSchedule) throws DAOException
    {
        log.debug("query cmsSchedule by scheduleid starting...");
        List<CmsSchedule> rList = (List<CmsSchedule>) super.queryForList("queryCmsScheduleListByID", cmsSchedule);
        log.debug("query cmsSchedule by scheduleid end");
        return rList;
    }
}
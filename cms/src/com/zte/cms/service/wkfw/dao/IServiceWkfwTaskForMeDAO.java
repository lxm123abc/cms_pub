package com.zte.cms.service.wkfw.dao;

import java.util.List;

import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwTaskForMe;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IServiceWkfwTaskForMeDAO
{

    @SuppressWarnings("unchecked")
    public abstract List<ServiceWkfwTaskForMe> queryServiceWkfwTaskForMeListByCond(
            ServiceWkfwCondition serviceWkfwTaskForMeCondition) throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract int queryserviceWkfwTaskForMeListCntByCond(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DAOException;

    @SuppressWarnings("unchecked")
    public abstract PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize)
            throws DAOException;

    public abstract PageInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
package com.zte.cms.content.dao;

import java.util.List;

import com.zte.cms.content.dao.IIcmTransfileDAO;
import com.zte.cms.content.model.IcmTransfile;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class IcmTransfileDAO extends DynamicObjectBaseDao implements IIcmTransfileDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertIcmTransfile(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("insert icmTransfile starting...");
        super.insert("insertIcmTransfile", icmTransfile);
        log.debug("insert icmTransfile end");
    }

    public void updateIcmTransfile(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("update icmTransfile by pk starting...");
        super.update("updateIcmTransfile", icmTransfile);
        log.debug("update icmTransfile by pk end");
    }

    public void updateIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("update icmTransfile by conditions starting...");
        super.update("updateIcmTransfileByCond", icmTransfile);
        log.debug("update icmTransfile by conditions end");
    }

    public void deleteIcmTransfile(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("delete icmTransfile by pk starting...");
        super.delete("deleteIcmTransfile", icmTransfile);
        log.debug("delete icmTransfile by pk end");
    }

    public void deleteIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("delete icmTransfile by conditions starting...");
        super.delete("deleteIcmTransfileByCond", icmTransfile);
        log.debug("update icmTransfile by conditions end");
    }

    public IcmTransfile getIcmTransfile(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("query icmTransfile starting...");
        IcmTransfile resultObj = (IcmTransfile) super.queryForObject("getIcmTransfile", icmTransfile);
        log.debug("query icmTransfile end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<IcmTransfile> getIcmTransfileByCond(IcmTransfile icmTransfile) throws DAOException
    {
        log.debug("query icmTransfile by condition starting...");
        List<IcmTransfile> rList = (List<IcmTransfile>) super.queryForList("queryIcmTransfileListByCond", icmTransfile);
        log.debug("query icmTransfile by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize) throws DAOException
    {
        log.debug("page query icmTransfile by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryIcmTransfileListCntByCond", icmTransfile)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<IcmTransfile> rsList = (List<IcmTransfile>) super.pageQuery("queryIcmTransfileListByCond",
                    icmTransfile, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query icmTransfile by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryIcmTransfileListByCond", "queryIcmTransfileListCntByCond", icmTransfile,
                start, pageSize, puEntity);
    }

}
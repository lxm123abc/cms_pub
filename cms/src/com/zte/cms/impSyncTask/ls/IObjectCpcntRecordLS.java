package com.zte.cms.impSyncTask.ls;

import java.util.HashMap;

import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IObjectCpcntRecordLS
{
	 /**
   * 根据条件分页查询ObjectCpcntRecord对象 
   *
   * @param objectCpcntRecord ObjectCpcntRecord对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws Exception 异常
   */
     public TableDataInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws Exception;
     
     /**
   * 查询出所有的任务编号和流水号，放入Map中
   * 
   * @return HashMap<String, String>
   * @throws Exception 异常
   */
     public HashMap<String, String> getTaskMap() throws Exception;
     
     /**
      * 根据taskindex分页查询ObjectCpcntRecord对象 
      *
      * @param objectCpcntRecord ObjectCpcntRecord对象，作为查询条件的参数 
      * @param start 起始行
      * @param pageSize 页面大小
      * @param puEntity 排序空置参数@see PageUtilEntity
      * @return  查询结果
      * @throws DomainServiceException ds异常
      */
     public TableDataInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws Exception;
     
}
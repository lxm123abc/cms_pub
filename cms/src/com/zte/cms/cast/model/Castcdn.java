package com.zte.cms.cast.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Castcdn extends DynamicBaseObject
{
    private java.lang.Long castindex;
    private java.lang.String castid;
    private java.lang.String castname;
    private java.lang.String persondisplayname;
    private java.lang.String personsortname;
    private java.lang.String personsearchname;
    private java.lang.String firstname;
    private java.lang.String middlename;
    private java.lang.String lastname;
    private java.lang.Integer sex;
    private java.lang.String birthday;
    private java.lang.String hometown;
    private java.lang.String education;
    private java.lang.Integer height;
    private java.lang.Integer weight;
    private java.lang.Integer bloodgroup;
    private java.lang.Integer marriage;
    private java.lang.String favorite;
    private java.lang.String webpage;
    private java.lang.String description;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String modtime;
    private java.lang.String publishtime;
    private java.lang.String castcode;
    private java.lang.String cancelpubtime;

    private java.lang.String statusList;// 自定义属性,用于保存多个状态的值

    public java.lang.String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(java.lang.String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public java.lang.String getStatusList()
    {
        return statusList;
    }

    public void setStatusList(java.lang.String statusList)
    {
        this.statusList = statusList;
    }

    public java.lang.Long getCastindex()
    {
        return castindex;
    }

    public void setCastindex(java.lang.Long castindex)
    {
        this.castindex = castindex;
    }

    public java.lang.String getCastid()
    {
        return castid;
    }

    public void setCastid(java.lang.String castid)
    {
        this.castid = castid;
    }

    public java.lang.String getCastname()
    {
        return castname;
    }

    public void setCastname(java.lang.String castname)
    {
        this.castname = castname;
    }

    public java.lang.String getPersondisplayname()
    {
        return persondisplayname;
    }

    public void setPersondisplayname(java.lang.String persondisplayname)
    {
        this.persondisplayname = persondisplayname;
    }

    public java.lang.String getPersonsortname()
    {
        return personsortname;
    }

    public void setPersonsortname(java.lang.String personsortname)
    {
        this.personsortname = personsortname;
    }

    public java.lang.String getPersonsearchname()
    {
        return personsearchname;
    }

    public void setPersonsearchname(java.lang.String personsearchname)
    {
        this.personsearchname = personsearchname;
    }

    public java.lang.String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(java.lang.String firstname)
    {
        this.firstname = firstname;
    }

    public java.lang.String getMiddlename()
    {
        return middlename;
    }

    public void setMiddlename(java.lang.String middlename)
    {
        this.middlename = middlename;
    }

    public java.lang.String getLastname()
    {
        return lastname;
    }

    public void setLastname(java.lang.String lastname)
    {
        this.lastname = lastname;
    }

    public java.lang.Integer getSex()
    {
        return sex;
    }

    public void setSex(java.lang.Integer sex)
    {
        this.sex = sex;
    }

    public java.lang.String getBirthday()
    {
        return birthday;
    }

    public void setBirthday(java.lang.String birthday)
    {
        this.birthday = birthday;
    }

    public java.lang.String getHometown()
    {
        return hometown;
    }

    public void setHometown(java.lang.String hometown)
    {
        this.hometown = hometown;
    }

    public java.lang.String getEducation()
    {
        return education;
    }

    public void setEducation(java.lang.String education)
    {
        this.education = education;
    }

    public java.lang.Integer getHeight()
    {
        return height;
    }

    public void setHeight(java.lang.Integer height)
    {
        this.height = height;
    }

    public java.lang.Integer getWeight()
    {
        return weight;
    }

    public void setWeight(java.lang.Integer weight)
    {
        this.weight = weight;
    }

    public java.lang.Integer getBloodgroup()
    {
        return bloodgroup;
    }

    public void setBloodgroup(java.lang.Integer bloodgroup)
    {
        this.bloodgroup = bloodgroup;
    }

    public java.lang.Integer getMarriage()
    {
        return marriage;
    }

    public void setMarriage(java.lang.Integer marriage)
    {
        this.marriage = marriage;
    }

    public java.lang.String getFavorite()
    {
        return favorite;
    }

    public void setFavorite(java.lang.String favorite)
    {
        this.favorite = favorite;
    }

    public java.lang.String getWebpage()
    {
        return webpage;
    }

    public void setWebpage(java.lang.String webpage)
    {
        this.webpage = webpage;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getModtime()
    {
        return modtime;
    }

    public void setModtime(java.lang.String modtime)
    {
        this.modtime = modtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public java.lang.String getCastcode()
    {
        return castcode;
    }

    public void setCastcode(java.lang.String castcode)
    {
        this.castcode = castcode;
    }

    public void initRelation()
    {
        this.addRelation("castindex", "CASTINDEX");
        this.addRelation("castid", "CASTID");
        this.addRelation("castname", "CASTNAME");
        this.addRelation("persondisplayname", "PERSONDISPLAYNAME");
        this.addRelation("personsortname", "PERSONSORTNAME");
        this.addRelation("personsearchname", "PERSONSEARCHNAME");
        this.addRelation("firstname", "FIRSTNAME");
        this.addRelation("middlename", "MIDDLENAME");
        this.addRelation("lastname", "LASTNAME");
        this.addRelation("sex", "SEX");
        this.addRelation("birthday", "BIRTHDAY");
        this.addRelation("hometown", "HOMETOWN");
        this.addRelation("education", "EDUCATION");
        this.addRelation("height", "HEIGHT");
        this.addRelation("weight", "WEIGHT");
        this.addRelation("bloodgroup", "BLOODGROUP");
        this.addRelation("marriage", "MARRIAGE");
        this.addRelation("favorite", "FAVORITE");
        this.addRelation("webpage", "WEBPAGE");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("modtime", "MODTIME");
        this.addRelation("publishtime", "PUBLISHTIME");
        this.addRelation("castcode", "CASTCODE");
        this.addRelation("cancelpubtime", "CANCELPUBTIME");

    }
}

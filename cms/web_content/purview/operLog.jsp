<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
    <meta http-equiv="Content-Type" content="text/html; charset=gbk">
<%@ include file="JavaFun.jsp" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="purview" class="com.zte.zxywpub.Purview" scope="page" />
<jsp:useBean id="i18n" class="com.zte.zxywpub.I18n" scope="page" />

<% if(i18n.isEnLocale()){%>
<fmt:setLocale value="en_US" scope="session" />
<%} else if(i18n.isSimplifiedChinese()){%>
<fmt:setLocale value="zh_CN" scope="session" />
<%} else {%>
<fmt:setLocale value="zh_TW" scope="session" />
<%}%>

<%

    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
    String serviceKey = (String)session.getAttribute("SERVICEKEY");
    purview.setServiceKey(serviceKey);     
    String sysTime = "";
    String operID = (String)session.getAttribute("OPERID");
    String operName = (String)session.getAttribute("OPERNAME");
    Hashtable purviewList = session.getAttribute("PURVIEW") == null ? new Hashtable() : (Hashtable)session.getAttribute("PURVIEW");
    String isgroup = (String)application.getAttribute("ISGROUP")==null?"0":(String)application.getAttribute("ISGROUP");
    String ismanualsvc = (String)application.getAttribute("ISMANUALSVC")==null?"0":(String)application.getAttribute("ISMANUALSVC");
    //operflag��o 1������?������D��( 0: ?�̨�31������?�� 1: SP1������?�� 2:?����?1������?�� 3��o?a?��?��?�̨�31������?��)
    String operflag = request.getParameter("operflag") == null ? "0" : ((String)request.getParameter("operflag")).trim();
    String   opertype = "";
    String   startday = "";
    String   endday = "";
    String   opcode = "";
    String   opmode = "";
    String   scp = "";
    String   operator ="";
    try {
        ArrayList arraylist = null;
        ArrayList opcodelist = null;
        HashMap   map  = new HashMap();
        String    errmsg = "";
        String    queryType = "";
        int       queryMode = 0;
        boolean   flag =true;

       //if (purviewList.get("6-5") == null) {
        //  errmsg = i18n.getMessage("checkRingLog.cl.forbidden");
        //  flag = false;
     //  }
       if (operID  == null){
          errmsg = i18n.getMessage("checkRingLog.cl.loginfirst");
          flag = false;
       }
       if(flag){

          String   op = request.getParameter("op") == null ? "" : ((String)request.getParameter("op")).trim();
          boolean  checkflag = request.getParameter("checkTime") != null ? true : false;
          operator = request.getParameter("operator") == null ? "" : transferString((String)request.getParameter("operator"));
          if(operator.equals(""))
             operator = operID;
          if(op.equals("search")){
             opertype = request.getParameter("opertype") == null ? "" : ((String)request.getParameter("opertype")).trim();
             startday = request.getParameter("startday") == null ? "" : ((String)request.getParameter("startday")).trim();
             endday = request.getParameter("endday") == null ? "" : ((String)request.getParameter("endday")).trim();
             map.put("operator",operator);
             map.put("opertype",opertype);
             map.put("starttime",startday);
             map.put("endtime",endday);
             map.put("operator",operator);
             map.put("operflag",operflag);
             map.put("manualflag",ismanualsvc);
             map.put("groupflag",isgroup);
             arraylist = purview.getOperLog(map);
          }
          map = new HashMap();
          map.put("operflag",operflag);
          map.put("manualflag",ismanualsvc);
          map.put("groupflag",isgroup);
          opcodelist = purview.getOperType(map);
          int  records = 10;
          int thepage = 0 ;
          int pagecount = 0;
          int size=0;
          if(arraylist==null)
             size =-1;
          else
            size = arraylist.size();
          pagecount = size/records;
          if(size%records>0)
             pagecount = pagecount + 1;
          if(pagecount==0)
             pagecount = 1;
          //2��?��??2������?����??����D����2������?��
          ArrayList operList = null;
          operList = purview.sortChildOperators(operID,"0");
          HashMap paraMap = null;
          if(!opertype.equals(""))
             paraMap = purview.getOperTypePara(Integer.parseInt(opertype));
%>

<html>
<head>
<title><fmt:message key="operLog.operlogquery"/></title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body class="body-style1" onload="loadPage();" >


<form name="inputForm" method="post" action="operLog.jsp">
<input type="hidden" name="operflag" value="<%= operflag %>">
<input type="hidden" name="pagecount" value="<%= pagecount %>">
<input type="hidden" name="thepage" value="<%= thepage+1 %>">
<input type="hidden" name="op" value="">
  <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0" class="table-style2" >
   <tr >
          <td height="50"  align="center" class="text-title"><fmt:message key="operLog.operlogquery"/></td>
   </tr>
   <tr>
   <td align="center">
     <table border="0" cellspacing="0" cellpadding="0"  class="table-style2" width="98%">
     <tr>
     <td width="40%" height=30>
        <fmt:message key="operInfo.operator"/>
        <select name="operator" style="width:120px" class="input-style0" >
        <%
         for (int i = 0; i < operList.size(); i++) {
            map = (HashMap)operList.get(i);
            out.println("<option value=" + (String)map.get("OPERID") + " >" + (String)map.get("OPERNAME") + "</option>");
        }
        %>
        </select>
     </td>
	        <td width="40%" height=30> <fmt:message key="operLog.opertype"/>&nbsp;
              <select name="opertype" style="width:150px" class="input-style0">
                <option value=""><fmt:message key="operLog.alltype"/></option>
      <%
	     for(int j=0; j<opcodelist.size();j++){
	       map = (HashMap)opcodelist.get(j);
	       out.println("<option value="+(String)map.get("opertype") + " >" + (String)map.get("opertypename") + "</option>");
	     }
	  %>
              </select>
            </td>
      </tr>
     <tr>
     <td width="40%" height=30>
      <fmt:message key="operLog.starttime"/>&nbsp;
      <input type="text" name="startday" id="t1_butt"  value="<%= startday %>" maxlength="10" class="input-style0" style="width:120px">
      <button onfocus = "blur()" target = "startday" id = "t1_butt" pattern = "date" pos = "bottom_left">v</button>
     </td>
	 <td width="60%" >
      <fmt:message key="operLog.endtime"/>&nbsp;
      <input type="text" name="endday"  id="t2_butt" value="<%= endday %>" maxlength="10" class="input-style0" style="width:150px">
      <button onfocus = "blur()" target = "endday" id = "t2_butt" pattern = "date" pos = "bottom_left">v</button>
      &nbsp;<img border="0" src="button/search.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:searchInfo()">
     </td>
     </tr>
     </table>
  </td>
  </tr>
  <tr>
  <td>
   <%
        int  record = 0;
        for (int i = 0; i < pagecount; i++) {
          String pageid = "page_"+Integer.toString(i+1);
  %>

     <table width="100%" border="0" cellspacing="1" cellpadding="1" align="center"  class="table-style2" id="<%= pageid %>" style="display:none" >
         <tr class="table-title1">
           <td height="30" width="60">
              <div align="center"><fmt:message key="operLog.occurtime"/></div>
          </td>
          <td height="30" width="70">
              <div align="center"><fmt:message key="operLog.opertype"/></div>
          </td>
          <td height="30" width="40">
              <div align="center"><fmt:message key="operLog.operresult"/></div>
          </td>
          <td height="30" width="60" >
              <div align="center"><%= opertype.equals("")?i18n.getMessage("operLog.operone"):(String)paraMap.get("para1name") %></div>
          </td>
          <td height="30" width="60">
              <div align="center"><%= opertype.equals("")?i18n.getMessage("operLog.opertwo"):(String)paraMap.get("para2name") %></div>
           <td height="30" width="60">
              <div align="center"><%= opertype.equals("")?i18n.getMessage("operLog.operthree"):(String)paraMap.get("para3name") %></div>
          </td>
		   <td height="30" width="60">
              <div align="center"><%= opertype.equals("")?i18n.getMessage("operLog.operfour"):(String)paraMap.get("para4name") %></div>
          </td>
          <td height="30" width="60">
              <div align="center"><fmt:message key="operLog.comment"/></div>
          </td>
        </tr>

 <%
   			if(size==0){
			%>
			<tr><td class="table-style2" align="center" colspan="10"><fmt:message key="operLog.noqualifiedrecord"/></td>
			</tr>
			<%
			}else if(size>0){
			if(i==(pagecount-1))
               record = size - (pagecount-1)*records;
            else
               record = records;
            for(int j=0;j<record;j++){
                String strcolor= j % 2 == 0 ? "#FFFFFF" :  "E6ECFF" ;
                String para1 = map.get("para1") == null ? "":(String)map.get("para1");
                String para2 = map.get("para1") == null ? "":(String)map.get("para2");
                String para3 = map.get("para1") == null ? "":(String)map.get("para3");
                String para4 = map.get("para1") == null ? "":(String)map.get("para4");
                map = (HashMap)arraylist.get(i*records + j);
                out.print("<tr bgcolor=" +strcolor + " height=23 >");
                out.print("<td align=center  >"+(String)map.get("opertime")+"</td>");
                out.print("<td align=center>"+(String)map.get("opertypename")+"</td>");
                out.print("<td align=center>"+(String)map.get("result")+"</td>");
                out.print("<td >"+para1+"</td>");
                out.print("<td >"+para2+"</td>");
                out.print("<td >"+para3+"</td>");
                out.print("<td >"+para4+"</td>");
                out.print("<td >"+(String)map.get("desc")+"</td>");
                out.print("</td></tr>");
      }
 %>
        <tr>
        <td width="100%" colspan="8">
          <table border="0" cellspacing="1" cellpadding="1" align="right" class="table-style2">
            <tr>
              <td class="table-style2"><fmt:message key="operLog.total"/>&nbsp;<%= arraylist.size() %>&nbsp;<fmt:message key="operLog.record"/>&nbsp;<%= pagecount %>&nbsp;<fmt:message key="operLog.page"/>&nbsp;&nbsp;<fmt:message key="operLog.nowpage"/>&nbsp;<%= i+1 %>&nbsp;<fmt:message key="operLog.page"/></td>
              <td><img src="button/firstpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:firstPage()"></td>
              <td><img src="button/prepage.gif"<%= i == 0 ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(-1)\"" %>></td>
              <td><img src="button/nextpage.gif" <%= i * records + records >= arraylist.size() ? "" : " onmouseover=\"this.style.cursor='hand'\" onclick=\"javascript:toPage(1)\"" %>></td>
              <td><img src="button/endpage.gif" onmouseover="this.style.cursor='hand'" onclick="javascript:endPage()"></td>
            </tr>
          </table>
        </td>
      </tr>
   </table>
<%}

        }
%>
      </td>
  </tr>
</table>
</form>
<%
        }
        else {
             if(operID == null){
              %>
              <script language="javascript">
                    alert( "<fmt:message key="allotrights.loginfirst"/>");
                    document.URL = '../enter.jsp';
              </script>
              <%
                      }
                      else{
               %>
              <script language="javascript">
                   alert( "<fmt:message key="allotrights.forbidden"/>");
              </script>
              <%

              }
         }
    }
    catch(Exception e) {
       e.printStackTrace();
%>
<form name="errorForm" method="post" action="../error.jsp">
<input type="hidden" name="historyURL" value="purview/operLog.jsp">
</form>
<script language="javascript">
   document.errorForm.submit();
</script>
<%
    }
%>
<script language = javascript src = "MyCalendar.js"></script>
</body>
<jsp:include page="JsFun.jsp" flush="" />
<script language="javascript">

   function loadPage(){
     var sStartDate = getMonthPriorDate(2);
     var fm = document.forms[0];
     firstPage();
     var sTmp = "<%=  operator  %>";
     if(sTmp!='')
         document.forms[0].operator.value = sTmp;
     var opertype = "<%= opertype %>";
     fm.opertype.value = opertype;
     var  startday = "<%= startday %>";
     if(startday=='')
        startday = sStartDate;
     fm.startday.value = startday;
     var  endday = "<%= endday %>";
     if(endday=='')
        endday = getCurrentDate();
      fm.endday.value = endday;
   }

   function searchInfo () {
      var fm = document.forms[0];
      if (! checkInfo())
         return;
      fm.op.value = 'search';
      fm.submit();
   }

   function offpage(num){
  	var obj  = eval("page_" + num);
  	obj.style.display="none";
  }

  function onpage(num){
      var obj  = eval("page_" + num);
  	  obj.style.display="block";
  	  document.forms[0].thepage.value = num;
  }

  function firstPage(){
	if(parseInt(document.forms[0].pagecount.value)==0)
	   return;
	var thePage = document.forms[0].thepage.value;
	offpage(thePage);
	onpage(1);
	return true;
  }

  function toPage(value){
	var thePage = parseInt(document.forms[0].thepage.value);
	var pageCount = parseInt(document.forms[0].pagecount.value);
	var index = thePage+value;
	if(index > pageCount || index<0)
	   return;
	if(index!=thePage){
	   offpage(thePage);
	   onpage(index);
     }
	return true;
  }

  function endPage(){
	var thePage = document.forms[0].thepage.value;
	var pageCount = parseInt(document.forms[0].pagecount.value);
	offpage(thePage);
	onpage(pageCount)
	return true;
  }

  function checkInfo () {

      var fm = document.forms[0];
      // ?��2��?e��?����??
      if (fm.operator.selectedIndex == -1 ) {
         alert('<fmt:message key="operLog.chooseoperator"/>');
         return false;
      }

      var value = trim(fm.startday.value);
      if(value==''){
          alert('<fmt:message key="operLog.inputstarttime"/>');
          fm.startday.focus();
          return false;
      }

      if (! checkDate2(value)) {
           alert('<fmt:message key="operLog.inputstarttime"/>');
           fm.startday.focus();
           return false;
      }

      var sStartDate = getMonthPriorDate(2);
      var tmp1 = sStartDate.substring(0,4) + sStartDate.substring(5,7) + sStartDate.substring(8,10);
      var tmp2 = value.substring(0,4) + value.substring(5,7) + value.substring(8,10);
      if(tmp1 > tmp2 )
      {
         alert('<fmt:message key="operLog.dateafter"/>' + sStartDate + '<fmt:message key="operLog.inputagain"/>' );
         fm.startday.focus();
         return false;
      }
      value = trim(fm.endday.value);
      if(value==''){
           alert('<fmt:message key="operLog.inputEndtime"/>');
           fm.endday.focus();
           return false;
       }
       if (! checkDate2(value)) {
           alert('<fmt:message key="operLog.inputEndtime"/>');
           fm.endday.focus();
           return false;
       }
       if(!checktrue2(value)){
           alert('<fmt:message key="operLog.endtimeillegal"/>');
           fm.endday.focus();
           return false;
       }
       if (! compareDate2(fm.startday.value,fm.endday.value)) {
            alert('<fmt:message key="operLog.starttimeillegal"/>');
            fm.endday.focus();
            return false;
       }

       return true;
   }


</script>
</html>

package com.zte.cms.programcrmmap.dao;

import java.util.List;

import com.zte.cms.programcrmmap.model.ProgramCrmMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IProgramCrmMapDAO
{
    /**
     * 新增ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DAOException dao异常
     */
    public void insertProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据主键更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DAOException dao异常
     */
    public void updateProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据条件更新ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap更新条件
     * @throws DAOException dao异常
     */
    public void updateProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据主键删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @throws DAOException dao异常
     */
    public void deleteProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据条件删除ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap删除条件
     * @throws DAOException dao异常
     */
    public void deleteProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据主键查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @return 满足条件的ProgramCrmMap对象
     * @throws DAOException dao异常
     */
    public ProgramCrmMap getProgramCrmMap(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据条件查询ProgramCrmMap对象
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @return 满足条件的ProgramCrmMap对象集
     * @throws DAOException dao异常
     */
    public List<ProgramCrmMap> getProgramCrmMapByCond(ProgramCrmMap programCrmMap) throws DAOException;

    /**
     * 根据条件分页查询ProgramCrmMap对象，作为查询条件的参数
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ProgramCrmMap对象，作为查询条件的参数
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(ProgramCrmMap programCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /***********************publish mangage begin*******************/
    /**
     * 根据条件分页查询ProgramCrmMap对象，作为查询条件的参数
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryproCrmMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询ProgramCrmMap对象，作为查询条件的参数
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryproCrmTargetMap(ProgramCrmMap programCrmMap, int start, int pageSize) throws DAOException;
    /**
     * 根据条件分页查询ProgramCrmMap对象，作为查询条件的参数
     * 
     * @param programCrmMap ProgramCrmMap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public List<ProgramCrmMap> getProgramnormalCrmMapSynByCond(ProgramCrmMap programCrmMap) throws DAOException;
    
    
    /***********************publish mangage end*******************/
}
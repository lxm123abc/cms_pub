package com.zte.cms.settlementDataRpt.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

import java.io.Serializable;

/**
 * @Author: Liangxiaomin
 * @Date Created in 14:53 2019/6/27
 * @Description:
 */
public class OperInformation  extends DynamicBaseObject implements Serializable {

    private Long operid;
    private String opername;
    private String operallname;

    private int start;
    private int pageSize;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public void initRelation() {
        this.addRelation("operid", "OPERID");
        this.addRelation("opername", "OPERNAME");
        this.addRelation("operallname", "OPERALLNAME");
    }

    public Long getOperid() {
        return operid;
    }

    public void setOperid(Long operid) {
        this.operid = operid;
    }

    public String getOpername() {
        return opername;
    }

    public void setOpername(String opername) {
        this.opername = opername;
    }

    public String getOperallname() {
        return operallname;
    }

    public void setOperallname(String operallname) {
        this.operallname = operallname;
    }

}

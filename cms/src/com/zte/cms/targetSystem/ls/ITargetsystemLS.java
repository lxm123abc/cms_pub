package com.zte.cms.targetSystem.ls;


import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 目标系统的LS层
 * 
 * @author sanya
 * 
 */
public interface ITargetsystemLS
{

    /**
     * 新增Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return String
     * @throws DomainServiceException ds异常
     */
    public String insertTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 更新Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return String
     * @throws DomainServiceException ds异常
     */
    public String updateTargetsystem(Targetsystem targetsystem) throws DomainServiceException;
    
    /**
     * 删除目标系统
     * @param targetIndex 目标系统的索引
     * @return String
     * @throws DomainServiceException 抛出异常信息
     */
    public String deleteTargetsystem(String targetIndex)throws DomainServiceException;

    /**
     * 查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象
     * @return Targetsystem对象
     * @throws DomainServiceException ds异常
     */
    public Targetsystem getTargetsystem(Targetsystem targetsystem) throws DomainServiceException;

    /**
     * 根据条件分页查询Targetsystem对象
     * 
     * @param targetsystem Targetsystem对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     */
    public TableDataInfo pageInfoQuery(Targetsystem targetsystem, int start, int pageSize);
    public TableDataInfo popForPub(Targetsystem targetsystem, int start, int pageSize);

    /**
     * 根据targetIndex找出Targetsystem对象，并修改其状态
     * 
     * @param targetsystem Targetsystem对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DomainServiceException 
     */
    public String changeStatus (String targetIndex, int status) throws DomainServiceException;

    /**
     * 查询服务已经绑定的目标平台
     * @param serviceIndex
     * @return
     * @throws Exception
     */
    public TableDataInfo getServiceBindTargetList (Long serviceIndex, int start, int pageSize)  throws Exception;
}

package com.zte.cms.service.wkfw.service;

import java.util.List;

import com.zte.cms.service.wkfw.dao.IServiceWkfwProcessWithMeDAO;
import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwProcessWithMe;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ServiceWkfwProcessWithMeDS implements IServiceWkfwProcessWithMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceWkfwProcessWithMeDAO serviceWkfwProcessWithMedao = null;

    public void setServiceWkfwProcessWithMedao(IServiceWkfwProcessWithMeDAO serviceWkfwProcessWithMedao)
    {
        this.serviceWkfwProcessWithMedao = serviceWkfwProcessWithMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#queryStrategyWkfwProcessWithMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryServiceWkfwProcessWithMeListCntByCond(ServiceWkfwCondition serviceWkfwProcessWithMeCondition)
            throws DomainServiceException
    {
        log.debug("query serviceWkfwProcessWithMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(serviceWkfwProcessWithMeCondition);
            totalCnt = serviceWkfwProcessWithMedao
                    .queryServiceWkfwProcessWithMeListCntByCond(serviceWkfwProcessWithMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query serviceWkfwProcessWithMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(serviceWkfwProcessWithMeCondition);
            pageInfo = serviceWkfwProcessWithMedao.pageInfoQuery(serviceWkfwProcessWithMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwProcessWithMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwProcessWithMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceWkfwProcessWithMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(serviceWkfwProcessWithMeCondition);
            pageInfo = serviceWkfwProcessWithMedao.pageInfoQuery(serviceWkfwProcessWithMeCondition, start, pageSize,
                    puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwProcessWithMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwProcessWithMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ServiceWkfwCondition serviceWkfwProcessWithMeCondition)
    {
        serviceWkfwProcessWithMeCondition.setServiceid(EspecialCharMgt.conversion(serviceWkfwProcessWithMeCondition
                .getServiceid()));

        serviceWkfwProcessWithMeCondition.setServicename(EspecialCharMgt.conversion(serviceWkfwProcessWithMeCondition
                .getServicename()));
    }
}

package com.zte.ucm.log;

import com.zte.ucm.Constants;
import com.zte.ucm.util.DateUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class FileLoggerOutter extends LoggerOutter
{
  private PrintStream out;

  public FileLoggerOutter()
  {
    this.out = System.out;
    String str1 = Constants.LOGPATH;

    File localFile1 = new File(str1);
    if ((!localFile1.isFile()) || (!localFile1.exists()))
    {
      localFile1.mkdir();
    }
    String str2 = str1 + "/log_" + DateUtil.getDate("yyyyMMdd") + ".log";
    try
    {
      File localFile2 = new File(str2);
      if (!localFile2.exists())
      {
        if (!localFile2.createNewFile())
        {
          System.out.println("FileLoggerOutter: log file create failed:" + str2);

          return;
        }
      }

      this.out = new PrintStream(new FileOutputStream(str2, true), true);
    }
    catch (Exception localException) {
      System.out.println("FileLoggerOutter: create log file failed:" + str2);

      System.out.println("FileLoggerOutter: exception is :" + localException);
    }
  }

  public String getId()
  {
    return FileLoggerOutter.class.getName();
  }

  public void print(int paramInt)
  {
    System.out.print(paramInt);
    this.out.print(paramInt);
  }

  public void print(String paramString)
  {
    System.out.print(paramString);
    this.out.print(paramString);
  }

  public void print(Object paramObject)
  {
    System.out.print(paramObject);
    this.out.print(paramObject);
  }
}
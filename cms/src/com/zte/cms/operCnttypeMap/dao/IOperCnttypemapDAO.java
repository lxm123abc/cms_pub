package com.zte.cms.operCnttypeMap.dao;

import java.util.List;

import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IOperCnttypemapDAO
{
    /**
     * 新增OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @throws DAOException dao异常
     */
    public void insertOperCnttypemap(OperCnttypemap operCnttypemap) throws DAOException;

    /**
     * 根据条件更新OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap更新条件
     * @throws DAOException dao异常
     */
    public void updateOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException;

    /**
     * 根据条件删除OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap删除条件
     * @throws DAOException dao异常
     */
    public void deleteOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException;

    /**
     * 根据条件查询OperCnttypemap对象
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @return 满足条件的OperCnttypemap对象集
     * @throws DAOException dao异常
     */
    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap) throws DAOException;

    /**
     * 根据条件分页查询OperCnttypemap对象，作为查询条件的参数
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询OperCnttypemap对象，作为查询条件的参数
     * 
     * @param operCnttypemap OperCnttypemap对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(OperCnttypemap operCnttypemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
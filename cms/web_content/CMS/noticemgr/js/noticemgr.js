//进行页面校验
function checkPropertyByRea(propertyname,isneed,type,length)
{   
	var tempStr=trim(document.getElementById(propertyname).value);
    if ( isneed && (tempStr=="")) 
    {
        document.getElementById(propertyname + "_msg").style.display="";
        document.getElementById(propertyname + "_msg").innerText= $res_entry("msg.check.noempty","");
        document.getElementById(propertyname).focus();
        return false;
    }
    if ( typeof(type) != 'undefined' && type!=null && type!="") 
    {
        var rex;
        var text;
        var flag = true;
        if(type == "special")
        {
        	//!@#$%^&*;|?()[]{}<>'"
        	var pattern = /[!@#$%^&;\[\]\{\}\(\)*|:\\"\<\>\/?']/;
        	//var pattern = /[*|:\\"\<\>\/?]/;
        	if(pattern.test(tempStr))
        	{	
        		flag = false;
            	text = $res_entry("msg.check.special","")+" !@#$%^& [] {} ()' * | : < > ? /  \\  \"";
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        else if(type == "allstring")
        {
        	var len=utf8_strlen2(tempStr);
        	if(len > length)
        	{
            	flag = false;
            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
            }
        } 
        else if(type == "string")
        {
        	var pattern = /\{|\}|\||(\/>)|(\]\]>)/;
        	if(pattern.test(tempStr))
        	{	
        		flag = false;
            	text = $res_entry("msg.check.special","")+"|  {  }  ]]>  \/>"
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        else if(type == "number")
        {
            rex=new RegExp("^((0?)|[1-9][0-9]{0,"+(length-1)+"})$");
            flag = rex.test(tempStr);
            text=$res_entry("msg.check.number.prefix","")+length+$res_entry("msg.check.number.suffix","");
        }
        else if(type == "code")
        {
            rex=new RegExp("^[0-9]{0,"+length+"}$");
            flag = rex.test(tempStr);
            text=$res_entry("msg.check.number.prefix","")+length+$res_entry("msg.check.code.suffix","");
        }
        else if(type == "rtsp")
        {
            var pattern = /^(rtsp|RTSP):\/\/([\w-]+\.)+[\w-]+(:\d+)?\/([^\\\/:\*\?\"<>\|]+\/)*[\w-\?#&]+(\.[\w-\?#&]+)?$/;
        	if(!pattern.test(tempStr))
        	{	
        		flag = false;
            	text = $res_entry("msg.check.format","")
        	}
        	else
        	{
	        	var len=utf8_strlen2(tempStr);
	        	if(len > length)
	        	{
	            	flag = false;
	            	text = $res_entry("msg.check.string.prefix","")+length+$res_entry("msg.check.string.suffix","");
	            }
            }
        }
        
        if (!flag && tempStr != "") 
        {
            document.getElementById(propertyname + "_msg").style.display="";
            document.getElementById(propertyname + "_msg").innerText = text;
            document.getElementById(propertyname).focus();
            return false;
        }
    }
    document.getElementById(propertyname + "_msg").innerText="";
    document.getElementById(propertyname + "_msg").style.display="none";
    return true;
}
// @version 3.00.04

Function.prototype.bindAsEventListener = function(object) {
	function $A(arr){
		var a=[];
		for(var i=0;i<arr.length;i++){
			a.push(arr[i]);
		}
		return a;
	};
  	var __method = this;
	var args = $A(arguments), 
	object = args.shift();
  	return function(event) {
    	return __method.apply(object, [event || window.event].concat(args));
  	};
};

function AUTO_init(){
	var atags=document.getElementsByTagName(_ZAUTO_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_AUTO_TAG);	
	}
	var autos={};
	for(var i=0;i<atags.length;i++){
		autos=new AutoCompleteTag(atags[i].getAttribute("id"));
		autos.init();
	}
};

addOnloadEvent(AUTO_init);

function AutoCompleteTag(id){
	var options={};
	var autoLabel=document.getElementById(id);
	options.matchAnywhere=autoLabel.getAttribute("matchAnywhere")||'true';
	options.ignoreCase=autoLabel.getAttribute("ignoreCase")||'false';
	options.queryId=autoLabel.getAttribute("queryId");
	options.count=autoLabel.getAttribute("count")||10;
	var params=autoLabel.getAttribute("params");
	var url=autoLabel.getAttribute("url");
	var ctlId=autoLabel.getAttribute("ctrlId");
	return new SSB_AutoComplete(id,ctlId,url,options,params);
};

function SSB_AutoComplete(labelId,anId,url,options,params){
	this.labelId=labelId;
    this.id          = anId;
    var browser	   = navigator.userAgent.toLowerCase();
    this.isIE        = browser.indexOf("msie") != -1;
    this.isOpera     = browser.indexOf("opera")!= -1;
    this.textInput   = document.getElementById(this.id);
    this.suggestions = [];
    this.setOptions(options);
    this.initAjax(url);
	this.setParams(params);
	this.nonReturn='';
};

SSB_AutoComplete.prototype = {

	initAjax: function(url) {
		this.url=url;
   },

	setParams:function(params){
		if(!params){//\u5982\u679c\u6ca1\u6709\u8bbe\u7f6e\u53c2\u6570\u5c31\u628a\u5f53\u524dinput\u7684\u503c\u52a0\u8fdb\u6765
			params='ctrl:'+this.id;
		}
		this.params=[];
		var param_re=new RegExp("(ctrl|value):([^,]+)", "i");
		var a=[];
		var tmp=params;
		while((a=param_re.exec(tmp))!=null){
			if(a[1].toUpperCase()=='CTRL'){
				this.params.push(document.getElementById(a[2]));
			}else if(a[1].toUpperCase()=='VALUE'){
				this.params.push(a[2]);
			}
			tmp=RegExp.rightContext;
		}
	},

	setOptions: function(options) {
		function extend(obj1,obj2){
			for(var property in obj1){
				if(obj2[property]){
					obj1[property]=obj2[property];
				}
			}
		};
		this.options = {
	     	//\u81ea\u52a8\u5b8c\u6210\u4e0b\u62c9\u83dc\u5355DIV\u5143\u7d20\u7684css class
			suggestDivClassName	: 'auto_list',
		 	//\u5f53\u9f20\u6807\u79bb\u5f00\u9009\u9879\u65f6\u7684css class
			mouseoutClassName	: 'auto_item_mouseout',
		 	//\u5f53\u9f20\u6807\u79fb\u52a8\u5230\u9009\u9879\u65f6\u7684css class
			mouseoverClassName	: 'auto_item_mouseover',
		 	//\u88ab\u5339\u914d\u7684\u6587\u672c\u7684css class
			matchClassName     	: 'auto_item_match',
		 	//\u662f\u5426\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
			matchAnywhere      	: 'true',
		 	//\u662f\u5426\u533a\u5206\u5927\u5c0f\u5199
			ignoreCase			: 'false',
		 	//\u4e0b\u62c9\u83dc\u5355\u4e2d\u6761\u76ee\u4e2a\u6570
			count              	: 10,
		 	//\u67e5\u8be2\u8bed\u53e5ID
			queryId				: ""
		};

	  extend(this.options,options);
   },

   	init: function() {
      	this.textInput.setAttribute("autocomplete","off");
		
      	var keyEventHandler = new TextSuggestKeyHandler(this);

      	this.createSuggestionsDiv();

	  	SSB_AutoComplete.autoObjs[this.labelId]=this;
   	},

	getValue:function(){
		return document.getElementById(this.id).value;
	},

   	handleTextInput: function() {
     	var previousRequest    = this.lastRequestString;
     	this.lastRequestString = this.textInput.value;
     	if ( this.lastRequestString == "" )
        	this.hideSuggestions();
     	else if ( this.lastRequestString != previousRequest ) {
     		if(this.nonReturn!=''&&this.lastRequestString.indexOf(this.nonReturn)!=-1)
     			return;
     		else
        		this.sendRequestForSuggestions();
     	}else if(this.suggestions.length!=0){
     		this.showSuggestions();
     	}
   	},

	moveSelectionUp: function() {
		if ( this.selectedIndex > 0 ) {
			this.updateSelection(this.selectedIndex - 1);
		}
	},

	moveSelectionDown: function() {
		if ( this.selectedIndex < (this.suggestions.length - 1)  ) {
			this.updateSelection(this.selectedIndex + 1);
		}
	},

	updateSelection: function(n) {
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoutClassName;
		}
		this.selectedIndex = n;
		var span = document.getElementById( this.id + "_" + this.selectedIndex );
		if ( span ){
			span.className=this.options.mouseoverClassName;
		}
   	},

	sendRequestForSuggestions: function() {
		if ( this.handlingRequest ) {
			this.pendingRequest = true;
			return;
		}
		this.handlingRequest = true;
		this.callRIAService();
	},

	getParams:function(callParms){
		for(var i=0;i<this.params.length;i++){
			if('string'==typeof this.params[i]){
				callParms.push(this.params[i]);
			}else if('object'==typeof this.params[i]){
				//\u5982\u679c\u8fd9\u4e2aHTML\u5bf9\u8c61\u662f\u5f53\u524d\u81ea\u52a8\u5b8c\u6210input\u5bf9\u8c61
				if(this.params[i]==this.textInput){
					var value=this.lastRequestString;
					//\u5982\u679c\u5339\u914d\u4efb\u610f\u4f4d\u7f6e
					if(this.options.matchAnywhere=='true'){
						value='%'+value+'%';
					}//\u5982\u679c\u5339\u914d\u5f00\u59cb\u4f4d\u7f6e
					else{
						value=value+'%';	
					}
					if(this.options.ignoreCase=='true'){
						value=value.toUpperCase();
					}
					callParms.push(value);	
				//\u5982\u679c\u662f\u5176\u4ed6HTML\u5bf9\u8c61
				}else{//\u8fd9\u91cc\u5e94\u8be5\u7528RIA\u6846\u67b6\u7684processor\u5f97\u5230\u503c
					callParms.push(this.params[i].value);	
				}
			}
		}
	},

   	callRIAService: function() {
    	var callParms = [];
		this.getParams(callParms);
		var param={};//\u53c2\u6570\u5bf9\u8c61
		param.count=this.options.count;
		//param.count=10;//\u4e0d\u80fd\u5c06\u56de\u663e\u6761\u6570\u5199\u6b7b
		param.ignoreCase=this.options.ignoreCase;
		param.paras=callParms;
		param.queryId=this.options.queryId;

		callMethod(this.url||SSB_AutoComplete.URL,[param],this,this.setValue);
	},
	
	setValue:function(s,data){
		this.suggestions=data;
		this.nonReturn=data.length==0?this.lastRequestString:'';
		this.processSuggestions();
	},
	
	processSuggestions:function(){
		if ( this.suggestions.length==0 ) {
			this.suggestionsDiv.innerHTML='';
         	this.hideSuggestions();
      	}
      	else {
        	this.updateSuggestionsDiv();
         	this.showSuggestions();
         	this.updateSelection(0);
      	}

      	this.handlingRequest = false;

      	if ( this.pendingRequest ) {
         	this.pendingRequest    = false;
         	this.lastRequestString = this.textInput.value;
         	this.sendRequestForSuggestions();
      	}
	},
	
   	setInputFromSelection: function() {
     	var suggestion  = this.suggestions[ this.selectedIndex ];
     	if(!suggestion||this.suggestionsDiv.style.display=='none')return;
	 	this.textInput.value = suggestion;
		this.lastRequestString = suggestion;
     	this.hideSuggestions();
   	},

   	showSuggestions: function() {
      	var divStyle = this.suggestionsDiv.style;
      	if ( divStyle.display == 'block' )
         	return;
      	this.positionSuggestionsDiv();
      	divStyle.display = 'block';
		if(window.navigator.userAgent.indexOf("MSIE") == -1)
			divStyle.overflow='auto';
   	},

	positionSuggestionsDiv: function() {
		var selectedPosX = 0;
        var selectedPosY = 0;
		var divStyle = this.suggestionsDiv.style;
        var theElement = this.textInput;
        if (!theElement) return;
        var theElemHeight = theElement.offsetHeight;
        var theElemWidth = theElement.offsetWidth;
        while(theElement != null){
          selectedPosX += theElement.offsetLeft;
          selectedPosY += theElement.offsetTop;
          theElement = theElement.offsetParent;
        }
		divStyle.width = theElemWidth;
        divStyle.left = selectedPosX;
       	divStyle.top = selectedPosY + theElemHeight;
   },

   	hideSuggestions: function() {
      	this.suggestionsDiv.style.display = 'none';
      	var iframe = document.getElementById("iframe_id_autocomplete");
      	iframe.style.display = "none";
   	},

   	createSuggestionsDiv: function() {
		//sujingui
		//\u5728FF\u4e2d\u4f1a\u6321\u4f4f\u540e\u9762\u63a7\u4ef6\uff1a\u6d4b\u8bd5\u9875\u9762\u4e3aRiaDemo\u7684\u7b80\u5355\u8868\u683c
		
		var iframe = document.createElement("iframe");
		iframe.style.cssText = "position:absolute;z-index:7;width:expression(this.nextSibling.offsetWidth);height:expression(this.nextSibling.offsetHeight);top:expression(this.nextSibling.offsetTop);left:expression(this.nextSibling.offsetLeft);";
		iframe.frameborder = 0;
		iframe.setAttribute('id',"iframe_id_autocomplete");
		iframe.setAttribute('frameBorder',0);
		
		//
      	this.suggestionsDiv = document.createElement("div");
      	this.suggestionsDiv.className = this.options.suggestDivClassName;

      	var divStyle = this.suggestionsDiv.style;
      	divStyle.zIndex   = 8;
      	divStyle.display  = "none";

		    //this.textInput.parentNode.appendChild(iframe);
      	//this.textInput.parentNode.appendChild(this.suggestionsDiv);
      	
      	document.body.appendChild(iframe);
      	document.body.appendChild(this.suggestionsDiv);
   	},

   	updateSuggestionsDiv: function() {
      	this.suggestionsDiv.innerHTML = "";
      	var suggestLines = this.createSuggestionSpans();
      	for ( var i = 0 ; i < suggestLines.length ; i++ )
         	this.suggestionsDiv.appendChild(suggestLines[i]);
   	},

   	createSuggestionSpans: function() {
      	var regExpFlags = "";
      	if ( this.options.ignoreCase == 'true')
         	regExpFlags = 'i';
     	var startRegExp = "^";
      	if ( this.options.matchAnywhere == 'true')
         	startRegExp = '';

      	var regExp  = new RegExp( startRegExp + this.lastRequestString, regExpFlags );

      	var suggestionSpans = [];
      	for ( var i = 0 ; i < this.suggestions.length ; i++ )
         	suggestionSpans.push( this.createSuggestionSpan( i, regExp ) );

      	return suggestionSpans;
	},

   	createSuggestionSpan: function( n, regExp ) {
      	var suggestion = this.suggestions[n];

      	var suggestionSpan = document.createElement("span");
      	suggestionSpan.className 	 = this.options.mouseoutClassName;
      	suggestionSpan.style.display = 'block';
      	suggestionSpan.id            = this.id + "_" + n;
      	suggestionSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	suggestionSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);

	  	var suggestionText={};

	  	suggestionText=suggestion;

      	var textValues=this.splitTextValues(suggestionText ,
                                             this.lastRequestString.length,
                                             regExp );

      	var textMatchSpan = document.createElement("span");
      	textMatchSpan.id            = this.id + "_match_" + n;
      	textMatchSpan.className     = this.options.matchClassName;
      	textMatchSpan.onmouseover   = this.mouseoverHandler.bindAsEventListener(this);
      	textMatchSpan.onclick       = this.itemClickHandler.bindAsEventListener(this);
      	
      	textMatchSpan.appendChild( document.createTextNode(textValues.mid) );
      	
      	suggestionSpan.appendChild( document.createTextNode( textValues.start ) );
      	suggestionSpan.appendChild( textMatchSpan );
      	suggestionSpan.appendChild( document.createTextNode( textValues.end ) );
      	
      	return suggestionSpan;
   	},
   	
   	mouseoverHandler: function(e) {
		e=e||window.event;
   	   	var src = e.srcElement||e.target;
   	   	var index = parseInt(src.id.substring(src.id.lastIndexOf('_')+1));
   	   	this.updateSelection(index);
   	},
   	
   	itemClickHandler: function(e) {
   	   	this.mouseoverHandler(e);
   	   	if ( this.suggestionsDiv.style.display == 'block' )
   	   	   	this.setInputFromSelection();
   	   	this.hideSuggestions();
   	   	this.textInput.focus();
   	},
   	
   	splitTextValues: function( text, len, regExp ) {
   	   	var startPos  = text.search(regExp);
   	   	var matchText = text.substring( startPos, startPos + len );
   	   	var startText = startPos == 0 ? "" : text.substring(0, startPos);
   	   	var endText   = text.substring( startPos + len );
   	   	return { start: startText, mid: matchText, end: endText };
   	},
   	
   	getElementContent: function(element) {
   	   	return element.firstChild.data;
   	}
};

SSB_AutoComplete.autoObjs={};

function TextSuggestKeyHandler(autoObj){
   	this.autoObj = autoObj;
   	this.input   = this.autoObj.textInput;
   	this.addKeyHandling();
};
SSB_AutoComplete.getKeyCode=function(e){
	e=e||window.event;
	return e.keyCode||e.which;
};



//change by Daniel for attachevent memory leak -------------begin.

function getEvent(){     //\u036c\u02b1\ufffd\ufffd\ufffd\ufffdie\ufffd\ufffdff\ufffd\ufffd\u0434\ufffd\ufffd
         if(document.all)    return window.event;        
         func=getEvent.caller;            
         while(func!=null){    
             var arg0=func.arguments[0];
             if(arg0){
                 if((arg0.constructor==Event || arg0.constructor ==MouseEvent)
                     || (typeof(arg0)=="object" && arg0.preventDefault && arg0.stopPropagation)){    
                    return arg0;
               }
            }
             func=func.caller;
         }
        return null;
        }
         
function keyupHandler(obj) {
   	   	var upArrow   = 38;
   	   	var downArrow = 40;
   	   	var enterKey  = 13;
   	   	var escKey	  = 27;
   	   	if ( obj.input.value.length == 0 && !obj.isOpera ){
   	      	obj.autoObj.hideSuggestions();
   	      	return;
   	   	}
   	   	switch(getEvent().keyCode){
	   	   	case upArrow:	obj.autoObj.moveSelectionUp();
	   	   	   			 	setTimeout( function(){}, 1 );
	   	   				 	break;
	   	   	case downArrow:	obj.autoObj.moveSelectionDown();
	   	   					break;
	   	   	case enterKey:	if(obj.autoObj.suggestionsDiv.style.display=='none')
	   	   						obj.autoObj.handleTextInput();
	   	   					else
	   	   						obj.autoObj.setInputFromSelection();
	   	   					break;
	   	   	case escKey:	obj.autoObj.hideSuggestions();
	   	   					break;
	   	   	default:obj.autoObj.handleTextInput();
	   	}
   	};
   	
   function onblurHandler(obj) {
   	   	if(obj.autoObj.suggestionsDiv.style.display == 'none')
   	   	   	return;
   	   	var event=getEvent();
		var toElement=event.explicitOriginalTarget||event.toElement;
		if(!toElement) return;

		var autolist=obj.autoObj.suggestionsDiv;
		do{
			if(toElement==autolist)
				return;
			toElement=toElement.parentNode;
		}while(toElement);
		autolist.style.display="none";
   	} ;
   	
   		
   	var leakKeyUp = function( obj ){ 
			return function(){
				keyupHandler(obj);
			}
	};
	
	var leakOnblur = function( obj ){ 
			return function(){
				onblurHandler(obj);
			}
	};
	
	
//change by Daniel for attachevent memory leak -------------end.
TextSuggestKeyHandler.prototype = {

   	addKeyHandling: function() {
   	   	//this.input.onkeydown = this.preventDefault.bindAsEventListener(this);
   	   	//this.input.onkeyup   = this.keyupHandler.bindAsEventListener(this);
   	   	//this.input.onblur    = this.onblurHandler.bindAsEventListener(this);
   	   	//this.input.onfocusout = this.onblurHandler.bindAsEventListener(this);
   	   	EventManager.Add(this.input,'keyup',leakKeyUp(this));
   	    EventManager.Add(this.input,'keydown',this.preventDefault);
        EventManager.Add(this.input,'blur',leakOnblur(this));        
        EventManager.Add(this.input,'focusout',leakOnblur(this));
        
   	   	if ( this.isOpera )
   	   	    EventManager.Add(this.input,'keyup',leakKeyUp(this));
   	   	   	//this.input.onkeypress = this.keyupHandler.bindAsEventListener(this);
   	},
	preventDefault:function(e){
		var enterKey=13;
		var escKey=27;
		var key=getEvent().keyCode;
		if(key==enterKey||key==escKey){
			if(e.preventDefault){
				e.preventDefault();
			}else{
				e.returnValue=false;
			}
		}
	}
	/*
   	keyupHandler: function(e) {
   	   	var upArrow   = 38;
   	   	var downArrow = 40;
   	   	var enterKey  = 13;
   	   	var escKey	  = 27;
   	   	if ( this.input.value.length == 0 && !this.isOpera ){
   	      	this.autoObj.hideSuggestions();
   	      	return;
   	   	}
   	   	switch(SSB_AutoComplete.getKeyCode(e)){
	   	   	case upArrow:	this.autoObj.moveSelectionUp();
	   	   	   			 	setTimeout( function(){}, 1 );
	   	   				 	break;
	   	   	case downArrow:	this.autoObj.moveSelectionDown();
	   	   					break;
	   	   	case enterKey:	if(this.autoObj.suggestionsDiv.style.display=='none')
	   	   						this.autoObj.handleTextInput();
	   	   					else
	   	   						this.autoObj.setInputFromSelection();
	   	   					break;
	   	   	case escKey:	this.autoObj.hideSuggestions();
	   	   					break;
	   	   	default:this.autoObj.handleTextInput();
	   	}
   	},

   	onblurHandler: function(oEvent) {
   	   	if(this.autoObj.suggestionsDiv.style.display == 'none')
   	   	   	return;
   	   	var event=window.event||oEvent;
		var toElement=event.explicitOriginalTarget||event.toElement;
		if(!toElement) return;

		var autolist=this.autoObj.suggestionsDiv;
		do{
			if(toElement==autolist)
				return;
			toElement=toElement.parentNode;
		}while(toElement);
		autolist.style.display="none";
   	}
   	*/
};

SSB_AutoComplete.URL="autocomplete/getInitValueArr.ssm";

var _ZAUTO_TAG='z:autocomplete';
var _AUTO_TAG='autocomplete';
	function RIADownload(url,array)
	{
		var oForm=document.createElement('FORM');
		oForm.id='form_riadownload';
		oForm.action=url;
		oForm.method='POST';		
    	
		for(var i =0 ;i<array.length;i++)
		{
			var jsonInput=document.createElement('INPUT');
	    	jsonInput.value=array[i];
	    	jsonInput.name='downloadfileProp';
	    	jsonInput.type='hidden';
	    	oForm.appendChild(jsonInput);	    	
		}
		oForm.style.display = "none";
    	document.body.appendChild(oForm);
		
		var createIFrame=function(){
			var oIFrame={};
			try{
				oIFrame = document.createElement("<IFRAME name=frame_riadownload />");
			}catch(e){
				oIFrame=document.createElement("IFRAME");
				oIFrame.name='frame_riadownload';
			}
			oIFrame.name ='frame_riadownload';
			oIFrame.width=0;
			oIFrame.height=0;
			oIFrame.frameBorder=0;
			oIFrame.id='frame_riadownload';
			document.body.appendChild(oIFrame);
			return oIFrame;
		};
		var oFrame=createIFrame();
		//var oForm=document.getElementById('form_riadownload');
		var downloadCallback=function(){
			var thisDocument=oFrame.contentDocument||oFrame.contentWindow.document;
			var html=thisDocument.body.innerHTML;
			if(html=="undefined"||html==null||html.trim()=="")
			{}
			else
				alert(html);
			
			setTimeout(function(){try{
									oForm.parentNode.removeChild(oForm);
									oFrame.parentNode.removeChild(oFrame);
									} catch(e){
										throw e;
									}
								  }, 100);
		};
			if(window.attachEvent){
		    	//oFrame.attachEvent('onload', uploadCallback);
		    	EventManager.Add(oFrame,'load',downloadCallback);
		    }
		    else{
		        oFrame.addEventListener('load', downloadCallback, false);
		    }
		    oForm.setAttribute("target",oFrame.name);
    		oForm.submit();
	}
var _EXTEXTAREA="EXTEXTAREA";
var _ZEXTEXTAREA="Z:EXTEXTAREA";

//\u6846\u67b6\u521d\u59cb\u5316\u5165\u53e3\u51fd\u6570
function exTextArea_Init(){

	var exTaTags=document.getElementsByTagName(_EXTEXTAREA); //ie
	if(!exTaTags||exTaTags.length==0)	{
		exTaTags=document.getElementsByTagName(_ZEXTEXTAREA);	//ff
	}
	
	if(objIsExisted(exTaTags)) {
		for(var i=0;i<exTaTags.length;i++){
			var exta = new SSB_EXTEXTAREA(exTaTags[i]);
			exta.init();
		}
	}
}
addOnloadEvent(exTextArea_Init);

var EXTEXTAREA_DEFAULT_ID = "msgArea";
var EXTEXTAREA_DEFAULT_MAXLENGTH = 200;
var EXTEXTAREA_DEFAULT_MSGSTATION = "bottom";
var EXTEXTAREA_DEFAULT_COLS = 80;
var EXTEXTAREA_DEFAULT_ROWS = 3;
var EXTEXTAREA_DEFAULT_MSG = $res_entry('ui.control.textarea.msg',"\u60a8\u8fd8\u53ef\u4ee5\u8f93\u5165{#maxlength}\u4e2a\u5b57\u7b26");//\u4f1a\u505a\u591a\u8bed\u5316\u5904\u7406
function SSB_EXTEXTAREA(exTaTags)
{
	this.id = EXTEXTAREA_DEFAULT_ID;
	this.maxlength = EXTEXTAREA_DEFAULT_MAXLENGTH;
	this.msgstation = EXTEXTAREA_DEFAULT_MSGSTATION;
	this.cols = EXTEXTAREA_DEFAULT_COLS;
	this.rows = EXTEXTAREA_DEFAULT_ROWS;
	this.msg = EXTEXTAREA_DEFAULT_MSG;
	
	(function(){
		
		//\u8bfb\u53d6\u7528\u6237\u8bbe\u7f6e\u76845\u4e2a\u57fa\u672c\u5c5e\u6027
		if(valueIsNotNull($(exTaTags).attr("id"))) this.id = $(exTaTags).attr("id");
		if(valueIsNotNull($(exTaTags).attr("msgstation"))) this.msgstation = $(exTaTags).attr("msgstation");
		if(valueIsNotNull($(exTaTags).attr("cols"))) this.cols = $(exTaTags).attr("cols");
		if(valueIsNotNull($(exTaTags).attr("rows"))) this.rows = $(exTaTags).attr("rows");
		if(valueIsNotNull($(exTaTags).attr("msg"))) this.msg = $(exTaTags).attr("msg");
		
		//\u6839\u636e\u9700\u6c42\uff0cmaxlength\u53ef\u4ee5\u6765\u81ea\u6570\u503c\uff0c \u4e5f\u53ef\u4ee5\u6765\u81ea\u7528\u6237\u51fd\u6570
		var maxchar = $(exTaTags).attr("maxlength");
		try{
			if(valueIsNotNull(maxchar))
			{
				var methods = maxchar.match(/(.*)/gi);
				if(methods){
					this.maxlength = parseInt(eval(maxchar));
				}
				else if(isNaN(maxchar))
					this.maxlength = maxchar;
			}
		}
		catch(err){	}//\u8fd9\u91cc\u4e0d\u9700\u8981\u505a\u4efb\u4f55\u5904\u7406\uff0c\u5df2\u7ecf\u9ed8\u8ba4\u8d4b\u503c
		
		this.otherprops = ssbUserPropFromHTMLTag(exTaTags,"id, maxlength, msgstation, cols, rows, msg");
	}).apply(this);
	
	this.parent = exTaTags.parentNode;
}

SSB_EXTEXTAREA.prototype =
{
	init:function(){
			//textarea
			var ssb_TextArea = document.createElement("TEXTAREA");
			$(ssb_TextArea).attr("maxlength", this.maxlength).attr("cols", this.cols).attr("rows", this.rows).attr("id", "ssb_"+this.id).appendTo(this.parent);
			for(var i=0; i<this.otherprops.length; i++){
				ssb_TextArea.setAttribute(this.otherprops[i].name, this.otherprops[i].value);
			}
			$(ssb_TextArea).bind("keyup",this.charLeft).bind("keydown",this.charLeft).bind("paste",this.charLeft).bind("mouseover",this.charLeft);
			//tipdiv
			var tipHtml = ("<div id='tip"+ this.id.replace("ssb_","") +"' class='remain'>" + this.msg + "</div>").replace(/{#maxlength}/,"<span>"+ this.maxlength +"</span>");
			
			var csswidth = function() { return isIE()? 6:4};
			if(this.msgstation == "bottom"){
				$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
			else{
				if(this.msgstation == "both"){
					$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
				}
				$(ssb_TextArea).before($(tipHtml).width($(ssb_TextArea).width()+csswidth()).css("margin-top","2px"));
				$(ssb_TextArea).css("margin-top","-2px");
			}
			
			ssb_TextArea.taGetLength = this.taGetLength;
			ssb_TextArea.setCaret = this.setCaret;
			ssb_TextArea.insertAtCaret = this.insertAtCaret;
			ssb_TextArea.updateTipsInfo = this.updateTipsInfo;
			
			if(isIE()){
				ssb_TextArea.attachEvent("onclick",this.setCaret);//\u9488\u5bf9\u7c98\u8d34
			}
			
	},
	//\u83b7\u53d6\u8f93\u5165\u957f\u5ea6
	taGetLength:function(txt){
			var multiple = 2;
			var chineseLength = 0;
			if(typeof(customChineseLength) !='undefined' && customChineseLength.constructor == Function)
				multiple = customChineseLength();
			var cArr = txt.match(/[^\x00-\xff]/ig);
			if(cArr != null)
				chineseLength = multiple * cArr.length - cArr.length;
			
			return txt.length + chineseLength;
	},
	//\u8f93\u5165\u4e8b\u4ef6\u54cd\u5e94
	charLeft: function(event){
		
		var event = event || window.event;
		var target = event.target || event.srcElement;  //  \u83b7\u5f97\u4e8b\u4ef6\u6e90
		if(target.tagName != "TEXTAREA") return;
		var maxlength = $(target).attr("maxlength");
		var pasteText = "";
		if (event.type == "paste" && window.clipboardData)	pasteText = window.clipboardData.getData("text");
		
		var content = $(target).val();
		var text = content + pasteText;
		
		var remain= maxlength - target.taGetLength(text);
		
		if(remain < 0){
				//\u7c98\u8d34\u5904\u7406
				if (event.type == "paste") {
					if(isIE()){
						var curpos = target.curpos;
	
						var len = maxlength - target.taGetLength(content);
						pasteText = pasteText.substr(0,len);
						for(var i=len-1; i>=0; i--){
							if(target.taGetLength(pasteText) <= len) break;
							pasteText = pasteText.substr(0,i);
						}
						
						if(pasteText.length == 0) return false;
						target.insertAtCaret(target,pasteText);
						remain = maxlength - target.taGetLength(target.value);
						return false;
					}
					else{
						
					}
				}
				else{
					//\u8f93\u5165\u5904\u7406
					while(remain < 0){
						text = text.substr(0,text.length-1);
						remain= maxlength - target.taGetLength(text);
					}
					$(target).val(text);
				}
			target.updateTipsInfo(target.id, remain);
		}
		target.setCaret(target);
		target.updateTipsInfo(target.id, remain);
	},
	
	updateTipsInfo: function(id, remain){
		//\u91c7\u7528id\u7684\u65b9\u5f0f\uff0c\u662f\u4e3a\u4e86\u907f\u514d\u591a\u4e2a\u6587\u672c\u57df\u662f\u5144\u5f1f\u8282\u70b9\u65f6\uff0c\u51fa\u73b0\u66f4\u65b0\u4fe1\u606f\u6709\u8bef\u7684\u60c5\u51b5
		$(this).siblings("div[id='tip"+ id.replace("ssb_","") +"']").each(function(){
			$(this).children("span").text(remain);
		});
	},
	
	setCaret: function(textObj){
		if (textObj.createTextRange) {
            textObj.caretPos = document.selection.createRange().duplicate();
        }
	},
	
	insertAtCaret: function(textObj, textFeildValue){
        if (document.all) {
            if (textObj.createTextRange && textObj.caretPos) {
                var caretPos = textObj.caretPos;
                caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == '' ? textFeildValue + '' : textFeildValue;
            }
            else {
                textObj.value = textFeildValue;
            }
        }
        else {
            if (textObj.setSelectionRange) {
                var rangeStart = textObj.selectionStart;
                var rangeEnd = textObj.selectionEnd;
                var tempStr1 = textObj.value.substring(0, rangeStart);
                var tempStr2 = textObj.value.substring(rangeEnd);
                textObj.value = tempStr1 + textFeildValue + tempStr2;
            }
            else {
            }
        }
    }
}

SSB_EXTEXTAREA.setTextAreaValue = function(taid, value)
{
	if(value != null && value != "null") {
		$("#ssb_"+taid).attr("value",value);
		try{
			$("#ssb_"+taid).keyup();
		}
		catch(e){}
	}		
}
SSB_EXTEXTAREA.getTextAreaValue = function(taid)
{
	return $("#ssb_"+taid).attr("value");
}
/*
 * @param(object) domobj		: \u4e00\u4e2aDOM\u5bf9\u8c61
 * @param(string) exception		: \u6392\u9664\u7684\u4e0d\u60f3\u8981\u7684\u5c5e\u6027
 * @return(array) rtnArray		: \u8fd4\u56de\u6570\u7ec4\uff0c\u5176\u5143\u7d20\u4e3a(name:propertyname, value:propertyvalue)\u5f62\u5f0f
 */
function ssbUserPropFromHTMLTag(domobj, exception)
{
	var rtnArray = new Array();
	var nameValue = function(name, value) {
		this.name = name;
		this.value = value;
	}
	
	if(!isIE()) {
	    if (window.HTMLElement) {
	        HTMLElement.prototype.__defineSetter__("outerHTML", function(sHTML) {
	            var r = this.ownerDocument.createRange();
	            r.setStartBefore(this);
	            var df = r.createContextualFragment(sHTML);
	            this.parentNode.replaceChild(df, this);
	            return sHTML;
	        });
	
	        HTMLElement.prototype.__defineGetter__("outerHTML", function() {
	            var attr;
	            var attrs = this.attributes;
	            var str = "<" + this.tagName.toLowerCase();
	            for (var i = 0; i < attrs.length; i++) {
	                attr = attrs[i];
	                if (attr.specified)
	                    str += " " + attr.name + '="' + attr.value + '"';
	            }
	            if (!this.canHaveChildren)
	                return str + ">";
	            return str + ">" + this.innerHTML + "</" + this.tagName.toLowerCase() + ">";
	        });
	
	        HTMLElement.prototype.__defineGetter__("canHaveChildren", function() {
	            switch (this.tagName.toLowerCase()) {
	                case "area":
	                case "base":
	                case "basefont":
	                case "col":
	                case "frame":
	                case "hr":
	                case "img":
	                case "br":
	                case "input":
	                case "isindex":
	                case "link":
	                case "meta":
	                case "param":
                    return false;
	            }
	            return true;
	        });
	    }
	}
	
	//\u5c5e\u6027\uff0c\u5305\u62ec\u4e8b\u4ef6\u7684\u8bfb\u53d6
	var attrs = domobj.outerHTML.match(/\s+\w+=/g); 
	var hasException = valueIsNotNull(exception);
	for(var i=0;i<attrs.length;i++)	{
		attrs[i] = $.trim(attrs[i]);
		attrs[i] = attrs[i].substring(0,attrs[i].length-1);
		if(hasException)
			if(exception.indexOf(attrs[i]) != -1) continue;
		
		rtnArray.push(new nameValue(attrs[i], domobj.getAttribute(attrs[i])));
	}
	//\u5224\u65ad\u662f\u5426\u6709disabled\u5c5e\u6027
	for(var i=0; i<domobj.attributes.length; i++){
		if(domobj.attributes[i].nodeName == "disabled" && domobj.attributes[i].nodeValue)
			rtnArray.push(new nameValue("disabled", "disabled"));
	}
	//\u8003\u8651\u5230\u6709\u901a\u7528\u7684\u53ef\u80fd\uff0c\u591a\u5224\u65ad\u4e00\u4e2aselected\u5c5e\u6027
	for(var i=0; i<domobj.attributes.length; i++){
		if(domobj.attributes[i].nodeName == "selected" && domobj.attributes[i].nodeValue)
			rtnArray.push(new nameValue("selected", "selected"));
	}
	
	return rtnArray;
}
//\u5224\u65ad\u5bf9\u8c61\u662f\u5426\u5b58\u5728\u5e76\u4e0d\u4e3a\u7a7a
function objIsExisted(obj)
{
	var rtn = true;
	if(typeof(obj) == "undefined" || obj == null || obj == "null") rtn = false;
	
	return rtn;
}

//\u5224\u65ad\u5c5e\u6027\u503c\u4e0d\u4e3a\u7a7a
function valueIsNotNull(value)
{
	var rtn = true;
	if(typeof(value) == "undefined" || value == null || value == "null" || $.trim(value).length == 0) rtn = false;
	
	return rtn;
}

function getSSBTextAreaById(id)
{
	var taid = "ssb_" + id;
	var taobj = document.getElementById(taid);
	
	//\u8bfb\u53d6\u6700\u5927\u957f\u5ea6
	taobj.getMaxLength = function(){
		return $(this).attr("maxlength");
	};
	//\u8bbe\u7f6e\u6700\u5927\u957f\u5ea6\u5e76\u66f4\u65b0\u63d0\u793a
	taobj.setMaxLength = function(maxlength){
		$(this).attr("maxlength", maxlength);
		var taid = this.id.replace("ssb_","");
		this.updateTipsInfo(taid, maxlength);
	};
	//\u8bfb\u53d6\u663e\u793a\u4f4d\u7f6e
	taobj.getMsgStation = function(){
		return $(this).attr("msgstation");
	};
	//\u66f4\u65b0\u663e\u793a\u4f4d\u7f6e
	taobj.setMsgStation = function(msgstation){
		var oldstation = $(this).attr("msgstation");
		if(oldstation == msgstation) return;
		
		var tipHtml = ("<div id='tip"+ this.id.replace("ssb_","") +"' class='remain'>" + this.msg + "</div>").replace(/{#maxlength}/,"<span>"+ this.maxlength +"</span>");
		var csswidth = function() { return isIE()? 6:4};
		
		if(msgstation == "bottom"){
			if(oldstation == "both"){
				var removeNode = $(this).siblings("div[id='tip"+ id +"']").get(0);
				removeNode.parentElement.removeChild(removeNode);
			}
			else{
				$(this).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
		}
		else{
			if(this.msgstation == "both"){
				$(ssb_TextArea).after($(tipHtml).width($(ssb_TextArea).width()+csswidth()));
			}
			$(ssb_TextArea).before($(tipHtml).width($(ssb_TextArea).width()+csswidth()).css("margin-top","2px"));
			$(ssb_TextArea).css("margin-top","-2px");
		}
	}
	
	return taobj;
}

//\u7528\u6237\u5bf9\u4e8e\u6c49\u5b57\u957f\u5ea6\u7684\u5904\u7406\u65b9\u5f0f
/*
function customChineseLength(){ return 3;}
*/
var _IP = "IP";
var _ZIP = "Z:IP";
SSB_IP.ipObjs={};

/*
 * \u81ea\u5df1\u52a0\u8f7d\uff0c\u89e3\u6790\u9875\u9762\u4e2d\u6240\u6709\u7684\u6307\u5b9a\u6807\u7b7e 
 */
function Ips_init() {
	/* \u53d6\u5230\u6240\u6709\u7684ip\u6807\u7b7e\uff0c\u5f97\u5230\u6570\u7ec4 */
	var tags = document.getElementsByTagName(_IP);
	if(!tags || tags.length == 0) {
		tags = document.getElementsByTagName(_ZIP);
	}
	if(tags && tags.length > 0) {
		var ip = {};
		/* \u904d\u5386\u4e0a\u8ff0\u6570\u7ec4\uff0c\u5bf9\u6bcf\u4e00\u4e2a\u6570\u7ec4\u5143\u7d20\u751f\u6210\u4e00\u4e2aip\u63a7\u4ef6\u5bf9\u8c61\uff0c\u8c03\u7528\u5176\u521d\u59cb\u5316\u65b9\u6cd5 */
		for(var i = 0; i < tags.length; i++) {
			var id = tags[i].getAttribute("id");
			var value = tags[i].getAttribute("value");
			var disabled = tags[i].getAttribute("disabled");
			ipObj = new SSB_IP(id);
			
			if(disabled)
				ipObj.setDisabled(disabled);
				
			ipObj.init();
			ipObj.setValue(value);
		}
	}
}
addOnloadEvent(Ips_init);

/*
 * IP\u5730\u5740\u63a7\u4ef6\u7c7b
 */
function SSB_IP(id)
{
    this.id = id;
    this.elem = document.getElementById(this.id);
    this.disabled = false;
}

SSB_IP.prototype={
	/* \u521d\u59cb\u5316\u65b9\u6cd5 */
	init:function() {
	  var str = "<table class=\"ip\" ><tbody><tr>";
	  
	  if(this.disabled == false)
	  {
	    str += "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_1\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 1, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 1, event)\"   onkeydown=\"OnIPFldKeyDown('" + this.id + "', 1, event)\" onchange=\"OnIPFldChange('" + this.id + "', 1, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_2\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 2, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 2, event)\"   onkeydown=\"OnIPFldKeyDown('" + this.id + "', 2, event)\" onchange=\"OnIPFldChange('" + this.id + "', 2, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_3\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 3, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 3, event)\"   onkeydown=\"OnIPFldKeyDown('" + this.id + "', 3, event)\" onchange=\"OnIPFldChange('" + this.id + "', 3, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\" size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_4\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 4, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 4, event)\"   onkeydown=\"OnIPFldKeyDown('" + this.id + "', 4, event)\" onchange=\"OnIPFldChange('" + this.id + "', 4, event)\" ></td>";
	   }
	   else if(this.disabled == "true" || this.disabled == true)
	   {
	   	str += "<td class=\"ip_fld\"><input type=\"text\" disabled=true size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_1\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 1, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 1, event)\"   onkeydown=\"OnIPFldKeyDown('" + this.id + "', 1, event)\" onchange=\"OnIPFldChange('" + this.id + "', 1, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\"  disabled=true size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_2\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 2, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 2, event)\"  onkeydown=\"OnIPFldKeyDown('" + this.id + "', 2, event)\" onchange=\"OnIPFldChange('" + this.id + "', 2, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\"  disabled=true size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_3\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 3, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 3, event)\"  onkeydown=\"OnIPFldKeyDown('" + this.id + "', 3, event)\" onchange=\"OnIPFldChange('" + this.id + "', 3, event)\" ></td>" +
	           "<td class=\"ip_dot\">.</td>" +
	           "<td class=\"ip_fld\"><input type=\"text\"  disabled=true size=\"3\" maxlength=\"3\" id=\"" + this.id + "_ipFld_4\"" +
	           " onselect=\"OnIPFldSelect('" + this.id + "', 4, event)\"  onfocus=\"OnIPFldFocus('" + this.id + "', 4, event)\"  onkeydown=\"OnIPFldKeyDown('" + this.id + "', 4, event)\" onchange=\"OnIPFldChange('" + this.id + "', 4, event)\" ></td>";
	   			   
	   }
	   
	    str += "</tr></tbody></table>";
	    this.elem.innerHTML = str;
	    
	    
	    SSB_IP.ipObjs[this.id] = this;
	},
	/* \u8bbe\u7f6eip\u63a7\u4ef6\u4e0d\u80fd\u8f93\u5165\u503c */
	setDisabled:function(disabled){
		this.disabled = disabled;
	},
	/* \u83b7\u53d6 IP \u503c\uff0cIP \u503c\u65e0\u6548\uff0c\u5219\u8fd4\u56de\u96f6\u957f\u5ea6\u5b57\u7b26\u4e32 */
	getValue:function() {
		var fld_1 = document.getElementById(this.id + "_ipFld_1").value;
	    var fld_2 = document.getElementById(this.id + "_ipFld_2").value;
	    var fld_3 = document.getElementById(this.id + "_ipFld_3").value;
	    var fld_4 = document.getElementById(this.id + "_ipFld_4").value;
	    
	    if (IsByte(fld_1) && IsByte(fld_2) & IsByte(fld_3) & IsByte(fld_4))
	    {
	        return fld_1 + "." + fld_2 + "." + fld_3 + "." + fld_4;
	    }
	    else
	    {
	        return "";
	    }
	},
	
	/* \u8bbe\u7f6e IP \u503c  \u6210\u529f\u8fd4\u56de true  \u5931\u8d25\u8fd4\u56de false\uff0c\u4e0d\u6539\u53d8\u539f\u8bbe\u7f6e\u503c */
	setValue:function(initValue) {
	
		if (initValue==null || typeof(initValue)=="undefined")
			return false;
	
		var ipFldArr = initValue.split(".");
	    if (ipFldArr.length != 4)
	    {
	        return false;
	    }
	    else if (!IsByte(ipFldArr[0]) || !IsByte(ipFldArr[1]) || !IsByte(ipFldArr[2]) || !IsByte(ipFldArr[3]))
	    {
	        return false;
	    }
	    
	    document.getElementById(this.id + "_ipFld_1").value = ipFldArr[0];
	    document.getElementById(this.id + "_ipFld_2").value = ipFldArr[1];
	    document.getElementById(this.id + "_ipFld_3").value = ipFldArr[2];
	    document.getElementById(this.id + "_ipFld_4").value = ipFldArr[3];
	   
	    return true;
	}

};


/* 
 * \u7981\u7528IP\u63a7\u4ef6
 */
SSB_IP.DisabledIP = function(id,index){
	if(typeof index =="number" && index >0 && index <5)
	{
		var disabledIP =  document.getElementById(id + "_ipFld_"+parseInt(index));
		disabledIP.disabled = true;
	}
	else
	{
		document.getElementById(id + "_ipFld_1").disabled = true;
	    document.getElementById(id + "_ipFld_2").disabled = true;
	    document.getElementById(id + "_ipFld_3").disabled = true;
	    document.getElementById(id + "_ipFld_4").disabled = true;
	
	}
}


/* 
 * \u4f7fIP\u63a7\u4ef6\u542f\u7528
 */
SSB_IP.EnabledIP = function(id,index){
	if(typeof index =="number" && index >0 && index <5)
	{
		var disabledIP =  document.getElementById(id + "_ipFld_"+parseInt(index));
		disabledIP.disabled = false;
	}
	else
	{
		document.getElementById(id + "_ipFld_1").disabled = false;
	    document.getElementById(id + "_ipFld_2").disabled = false;
	    document.getElementById(id + "_ipFld_3").disabled = false;
	    document.getElementById(id + "_ipFld_4").disabled = false;
	}
}


/* 
 * \u786e\u4fdd\u8f93\u5165\u7684\u6570\u503c\u8303\u56f4
 */
function OnIPFldChange(id, curFldIndex, oEvent)
{
    var ipFldElem = document.getElementById(id + "_ipFld_" + curFldIndex);
    var ipFldValue = ipFldElem.value;
    if(ipFldValue == "") {
    	//
    }
    else if (!IsByte(ipFldValue))
    {
      ipFldElem.value = 255;
    }
}


var SSB_IP_Selected = [];

function OnIPFldSelect(id, curFldIndex, oEvent)
{
	SSB_IP_Selected[id + "_ipFld_" + curFldIndex] = false;
	

}

function OnIPFldFocus(id, curFldIndex, oEvent)
{
	SSB_IP_Selected[id + "_ipFld_" + curFldIndex] = true;
	

}



/*
 * \u53ea\u5141\u8bb8\u8f93\u5165\u6570\u5b57\uff0c\u5220\u9664\u952e\uff0c\u5de6\u53f3\u952e\uff0c\u70b9\u53f7\u952e
 */
function OnIPFldKeyDown(id, curFldIndex, oEvent)
{
	var ipFldElem = document.getElementById(id + "_ipFld_" + curFldIndex);
    var ipFldValue = ipFldElem.value;
	
	if(window.event) {
		
	   if (event.keyCode == 37)
	    {
	        //\u6309\u4e0b\u5411\u5de6\u952e
	        this.PrevIPFld(id, curFldIndex);
	        event.returnValue = false;
	    }
	    else if (event.keyCode==39 || (event.keyCode==190 && !event.shiftKey) || event.keyCode==9)
	    {
	        //\u6309\u4e0b\u5411\u53f3\u952e\u6216\u53e5\u53f7\u952e
	        this.NextIPFld(id, curFldIndex);
	        event.returnValue = false;
	    }
	    else if(event.keyCode == 8 || event.keyCode == 46) {
	    	// \u9000\u683c,\u5220\u9664
	    }
	    else if((event.keyCode >= 48 && event.keyCode <= 57 && !event.shiftKey)
	    	|| (event.keyCode >= 96 && event.keyCode <= 105 && !event.shiftKey)) 
	    {
	    	// \u6570\u5b57
	    	if(parseInt(ipFldValue) == 25 
	    		&& ((event.keyCode >= 54 && event.keyCode <= 57)
	    			|| (event.keyCode >= 102 && event.keyCode <= 105))) 
	    	{
	    		event.returnValue = false;
	    	}
	    	else if(parseInt(ipFldValue) > 25 && SSB_IP_Selected[id])
	    	{
	    		event.returnValue = false;
	    	}
			else if(parseInt(ipFldValue) == 0) {
				ipFldElem.value = "";
			}
	    }
	    else 
	    {
	    	//\u975e\u6570\u5b57\uff0c\u7981\u6b62\u8f93\u5165
	    	event.returnValue = false;	    	
	    }	    

	}
	else { 
		
		if (oEvent.keyCode == 37)
	    {	
	        //\u6309\u4e0b\u5411\u5de6\u952e
	        this.PrevIPFld(id, curFldIndex);
	        oEvent.preventDefault();
	    }
	    else if (oEvent.keyCode==39 || (oEvent.keyCode==190 && !oEvent.shiftKey) || oEvent.keyCode==9)
	    {
	        //\u6309\u4e0b\u5411\u53f3\u952e\u6216\u53e5\u53f7\u952e
	        this.NextIPFld(id, curFldIndex);
	        oEvent.preventDefault();
	    }
	    else if(oEvent.keyCode == 8 || oEvent.keyCode == 46) {
	    	// \u9000\u683c,\u5220\u9664
	    }
	    else if((oEvent.keyCode >= 48 && oEvent.keyCode <= 57 && !oEvent.shiftKey) 
	    	|| (oEvent.keyCode >= 96 && oEvent.keyCode <= 105 && !oEvent.shiftKey))
	    {
	    	
	    	// \u6570\u5b57
	    	if(parseInt(ipFldValue) == 25 
	    		&& ((oEvent.keyCode >= 54 && oEvent.keyCode <= 57)
	    			|| (oEvent.keyCode >= 102 && oEvent.keyCode <= 105))) 
	    	{
	    		oEvent.preventDefault();
	    	}
	    	else if(parseInt(ipFldValue) > 25 && SSB_IP_Selected[id])
	    	{
	    		oEvent.preventDefault();
	    	}
	    	else if(parseInt(ipFldValue) == 0) {
				ipFldElem.value = "";
			}
	    }
	    else 
	    {
	    	oEvent.preventDefault();
	    }
	    
	}
}



/*
 * \u4e0b\u4e00\u4e2a\u5b57\u6bb5\u683c
 */
function PrevIPFld(id, curFldIndex)
{
    if (curFldIndex > 1)
    {
        document.getElementById(id + "_ipFld_" + (--curFldIndex)).select();
    }
}

/*
 * \u4e0a\u4e00\u4e2a\u5b57\u6bb5\u683c
 */
function NextIPFld(id, curFldIndex)
{
    if (curFldIndex < 4)
    {
        document.getElementById(id + "_ipFld_" + (++curFldIndex)).select();
    }
}

/*
 * \u68c0\u67e5\u8303\u56f4\u662f\u5426\u57280-255\u4e4b\u95f4
 */
function IsByte(str)
{
    if (str.replace(/[0-9]/gi, "") == "")
    {
        if (parseInt(str)>=0 && parseInt(str)<=255)
        {
            return true;
        }
        return false;
    }
    return false;
}
function NAVI_init(){
	var atags=document.getElementsByTagName(_ZNAVI_TAG);
	if(!atags||atags.length==0){
		atags=document.getElementsByTagName(_NAVI_TAG);	
	}
	var navis={};
	for(var i=0;i<atags.length;i++){
		navis=new NaviTag(atags[i].getAttribute("id"));
		navis.init();
	}
};

addOnloadEvent(NAVI_init);


function NaviTag(id){
	var labelObj=document.getElementById(id);
	var menuId=labelObj.getAttribute("menuId");
	var text=labelObj.getAttribute("text");
	var url=labelObj.getAttribute("url");
	var _ria_location = labelObj.getAttribute("caption");
	if(_ria_location==null||_ria_location=='null'||_ria_location==undefined||_ria_location=='undefined'){_ria_location=$res_entry("ui.control.navi","\u5f53\u524d\u4f4d\u7f6e\uff1a");}
	var re = /caption_text/g; 
	SSB_Navi.STARTHTML=SSB_Navi.STARTHTML.replace(re,_ria_location);
	return new SSB_Navi(menuId,text,url,id);
};

function SSB_Navi(menuId,text,url,labelId){
	this.menuId=menuId;
	this.setText(text);
	this.url=url;
	this.setObject(labelId);
	this.menuPath=[];
};
SSB_Navi.prototype={
	setText:function(text){
		if(text instanceof Array){
			this.text=text;
		}else if(text instanceof String ||'string'==typeof text){
			this.text=text.split(',');
		}else 
			this.text=[];
	},
	setObject:function(id){
		this.labelObj=document.getElementById(id);
		this.labelId=id;
	},
	init:function(){
		SSB_Navi.naviObjs[this.labelId]=this;
		this.sendAjaxRequest();
	},
	sendAjaxRequest:function(){
		callMethod(this.url||SSB_Navi.URL,[this.menuId],this,this.setValue);
	},
	setValue:function(s,pathStr){
		this.handleResponse(pathStr);
		this.concatPath();
		this.getMenuPath();
	},
	handleResponse:function(pathStr){
		if(null!=pathStr&&""!=pathStr){
			this.menuPath=pathStr.split(',');
		}
	},
	concatPath:function(){
		this.menuPath=this.menuPath.concat(this.text);
	},
	getMenuPath:function(){
		var html=[];
		html.push(SSB_Navi.STARTHTML);
		var length=this.menuPath.length-1;
		if(length>=0){
			var i=0;
			for(i=0;i<length;i++){
				html.push(this.menuPath[i]);
				html.push(SSB_Navi.SPANHTML);
			}
			html.push(this.menuPath[i]);
		}
		html.push(SSB_Navi.ENDHTML);
		this.displayPath(html);
	},
	displayPath:function(html){
		this.labelObj.innerHTML=html.join("");
	}
};

SSB_Navi.naviObjs={};

SSB_Navi.URL="naviService/getPathString.ssm";

SSB_Navi.STARTHTML="<TABLE class='tableNavi'><TBODY><TR><TD noWrap class='tdNavi'><DIV class=div_subtitle>caption_text";
SSB_Navi.SPANHTML="<SPAN class=arrow_subtitle>&gt;</SPAN>";
SSB_Navi.ENDHTML="</DIV></TD><TD noWrap class='tdEndNavi'>&nbsp;</TD></TR></TBODY></TABLE>";

var _NAVI_TAG="NAVI";
var _ZNAVI_TAG="Z:NAVI";
function Radios_init(){
	var rtags=document.getElementsByTagName(_RADIOGROUP);
	if(!rtags||rtags.length==0){
		rtags=document.getElementsByTagName(_ZRADIOGROUP);	
	}
	var radios={};
	for(var i=0;i<rtags.length;i++){
		radios=new RadioGroupTag(rtags[i].getAttribute("id"));
		radios.init();
	}
};
addOnloadEvent(Radios_init);
				//
				//radiogroup\u7684\u6807\u7b7e\u7c7b
				//
function RadioGroupTag(id){
	var label=document.getElementById(id);
	var value=label.getAttribute("value");
	var text=label.getAttribute("text");
	var checkedvalue=label.getAttribute("checkedvalue");
	var onclick=label.getAttribute("onclickfun");
	return new SSB_RadioGroup(value,text,checkedvalue,id,onclick);
};

				//
				//\u5355\u9009\u6309\u94ae\u7ec4
				//
function SSB_RadioGroup(value,text,checkedValue,id,onclick){
	function setValueArr(value){
		if(!value)return [];
		if(value instanceof Array){
			this.valueArr=value;
		}else if(value instanceof String||"string"==typeof value){
			this.valueArr=value.split(",");
		}
	};
	function setTextArr(text){
		if(!text)return [];
		if(text instanceof Array){
			this.textArr=text;
		}else if(text instanceof String||"string"==typeof text){
			this.textArr=text.split(",");
		}
	};
	setValueArr.call(this,value);
	setTextArr.call(this,text);
	this.onclick=onclick;
	this.checkedvalue=checkedValue||"";
	this.groupObj=document.getElementById(id);
	this.group_id=id;
};
SSB_RadioGroup.prototype={
	init:function(){
		var html=[];
		var checkedvalue=this.checkedvalue;
		var value={};
		for(var i=0;i<this.valueArr.length||i<this.textArr.length;i++){
			var radio=[];
			value=this.valueArr[i]||"";
			radio.push("<INPUT");
			radio.push("name=radio_"+this.group_id);
			radio.push("type=radio");
			radio.push("value="+value);
			radio.push("onclick="+this.onclick);
			if(checkedvalue==value){
				radio.push("checked=true");
			}
			radio.push(">");
			radio.push(this.textArr[i]||"");
			html.push(radio.join(" "));
		}
		this.groupObj.innerHTML=html.join(" ");
		SSB_RadioGroup.radioGroupObjs[this.group_id]=this;
	},
	getValue:function(){
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].checked){
				return radios[i].getAttribute("value");
			}
		}
	},
	setValue:function(checkedValue){
		if(checkedValue)
			this.checkedvalue=checkedValue;
		var radios=this.groupObj.getElementsByTagName('INPUT');
		for(var i=0;i<radios.length;i++){
			if(radios[i].value==checkedValue+""){
				radios[i].checked=true;
				return;
			}
		}
	}
};

SSB_RadioGroup.radioGroupObjs={};

var _RADIOGROUP="RADIOGROUP";
var _ZRADIOGROUP="Z:RADIOGROUP";
function SSB_SingleUpload(ctrlId){
	this.ctrlId=ctrlId;
	this.labelObj=document.getElementById(ctrlId)||{};
	var that=this;
	this.labelObj.doUpload=function(paraMap,url,contrlObj){
		that.doUpload(paraMap,url,contrlObj);
	};
	this.text=$res_entry('ui.control.upload.browser','\u6d4f\u89c8...');
	this.desc="";
	this.maxfiles=-1;
	this.count=0;
	this.onaddfile=null;
	
	//wanghao5
	this.groupId = "";
};

//wanghao5
var file_group = {};
file_group["noId"]=[];
var uploadLocale = {};
uploadLocale.rtnValue=$res_entry('ui.control.upload.delete', '');

SSB_SingleUpload.prototype={
	initCtrl:function( ){
		var html=[];
		var ctrlId=this.ctrlId;
		//wanghao4
		html.push('<div class="fileinputs" style="overflow:hidden;" id="span_'+ctrlId+'"><div  class="fakefile"  id="div_'
				+ctrlId+'"><input id="fake_'+this.ctrlId+'" readonly class="singleText"  title="'+this.desc+
				'"/><input id="fakebt_'+this.ctrlId+'" type="button" class="singleButton button" value="'+
				this.text+'"/></div></div>');
		this.labelObj.innerHTML=html.join(' ');
	},
	singleFileWidthAdjust: function(){
		var singleFile = document.getElementById("upload_"+this.ctrlId);
		var inputInfo = this.getCtronlInfo("fake_"+this.ctrlId);
		var buttonInfo = this.getCtronlInfo("fakebt_"+this.ctrlId);
		
		var spancontainer = document.getElementById("span_"+this.ctrlId);
		var width = buttonInfo.x - inputInfo.x + buttonInfo.width+4;
		//callUrl('$/ssb/uiloader/loginMgt/getLocaleInfo.ssm',null,'uploadLocale','true');//\u5982\u679c\u7528\u6237\u4e0d\u4f7f\u7528UILoader\u4e0d\u914d\u7f6e\u5176\u76f8\u5173Bean\u7684\u8bdd\u672c\u5904\u4f1a\u62a5\u9519\uff0c\u91c7\u7528\u5176\u5b83\u65b9\u5f0f\u5904\u7406
		
		if(buttonInfo.width != 0)
		{
		//$(spancontainer).css("width", buttonInfo.x+ buttonInfo.width+5);
			$(singleFile).css("width", width).attr("xwidth", width);
			UploadGlobalFlag = false;
		}
		else
		{
			UploadGlobalFlag = true;
		}
	},
	getCtronlInfo: function(id){
		
		var control = document.getElementById(id);
		
		var controlInfo = {};		
		controlInfo.x = 0;
		controlInfo.y = 0;
		controlInfo.width = control.offsetWidth;
		controlInfo.height = control.offsetHeight;
		
		while(control!=null && control!=document.body)
		{
			controlInfo.x += control.offsetLeft + control.clientLeft;
			controlInfo.y += control.offsetTop + control.clientTop;
			control = control.offsetParent;
		}
		
		return controlInfo;
	},
	singleMouseMoveEvent: function() {
		var ctrlId=this.ctrlId;
		var that = this;
		document.getElementById("span_"+ctrlId).onmousemove = function(){
			var buttonInfo = that.getCtronlInfo("fakebt_"+ctrlId);
			if(UploadGlobalFlag)
			{
				that.singleFileWidthAdjust();
				that.initSingleFakePosition(ctrlId);
				UploadGlobalFlag = false;
			}
			else {
				that.singleFileWidthAdjust();
				that.initSingleFakePosition(ctrlId);
			}

			var mouseposition = SSB_Upload.onmousemove();
			
			var fakebtInfo = eval("fakebtInfos."+ctrlId);
			var filebtInfo = eval("filebtInfos."+ctrlId);
			var filebt = document.getElementById("upload_"+ctrlId);
			var xwidth = $(filebt).attr("xwidth");
			var ajustx = 2;
			if(uploadLocale.rtnValue.indexOf("Delete") != -1) {
				ajustx = 13;
			}
			$(filebt).css("marginLeft", 0);
			
			if(mouseposition.x >= fakebtInfo.x-2 && mouseposition.x <= fakebtInfo.x+fakebtInfo.width+ajustx) {
				var offset = xwidth-(fakebtInfo.x+fakebtInfo.width - mouseposition.x) + 20;
				$(filebt).css("width", offset);
			}
			else {
				$(filebt).css("marginLeft", -2000);
			}
		}
	},
	initSingleFakePosition: function(upid){
		var tempobj = document.getElementById("fakebt_"+upid);	
		var fakebtInfo = {};		
		fakebtInfo.x = 0;
		fakebtInfo.y = 0;
		fakebtInfo.width = tempobj.scrollWidth;
		fakebtInfo.height = tempobj.scrollHeight;
		
		while(tempobj!=null && tempobj!=document.body)
		{
			fakebtInfo.x += tempobj.offsetLeft + tempobj.clientLeft;
			fakebtInfo.y += tempobj.offsetTop + tempobj.clientTop;
			tempobj = tempobj.offsetParent
		}
		
		var filebt = document.getElementById("upload_"+upid);
		var filebtInfo = {};
		filebtInfo.x = 0;
		filebtInfo.y = 0;
		filebtInfo.width = filebt.scrollWidth;
		filebtInfo.height = filebt.scrollHeight;
		
		while(filebt!=null && filebt!=document.body)
		{
			filebtInfo.x += filebt.offsetLeft + filebt.clientLeft;
			filebtInfo.y += filebt.offsetTop + filebt.clientTop;
			filebt = filebt.offsetParent
		}
		
		eval("fakebtInfos."+upid+"=fakebtInfo");
		eval("filebtInfos."+upid+"=filebtInfo");
	},
	addCtrl:function( ){
		var oInput=this.getFileCtrl();
		//wanghao
		__file_List = [];
		
		//wanghao5
		if(this.groupId == "")
			file_group["noId"].push(oInput);
		else
		{
			if(!file_group[this.groupId])
				file_group[this.groupId] = [];
			
			file_group[this.groupId].push(oInput);

		}	
		
		//\u4fee\u6539\u8bb0\u5f55 \u5f20\u5b8f\u4f1f
		__file_List.push(oInput);
		//wanghao4
		oInput.className='single_input';
		var tr = document.getElementById('div_'+this.ctrlId);
	    tr.parentNode.insertBefore(oInput,tr);
		
		//2009-06-09 wxr
		this.singleFileWidthAdjust();
		this.singleMouseMoveEvent();
		this.initSingleFakePosition(this.ctrlId);
	},
	getFileCtrl:function( ){
		var oInput=document.createElement('input');
		var that=this;
		oInput.id = "upload_"+this.ctrlId;
		oInput.name = "upload_"+this.ctrlId;
		oInput.type = "file";
		//wanghao4
		oInput.hideFocus = true;
		//oInput.size = 1;
		oInput['onchange']=function(event){
			that.addUpload(event||window.event);
		};
		return oInput;
	},
	init:function( ){
		this.initCtrl( );
		this.addCtrl( );
		SSB_Upload.uploads[this.ctrlId]=this;
	},
	addUpload:function(event){
		var ctrlId=this.ctrlId;
		function setValue(value){
			document.getElementById('fake_'+ctrlId).value=value;
		};
		var onaddfile=this.onaddfile;
		var ok=false;
		var obj=event.srcElement||event.target;
		if(onaddfile==''||!onaddfile){
			setValue(obj.value);
			return;
		}
		if(onaddfile.indexOf('this') != -1){
			var o = document.getElementById(ctrlId);
			ok = parseFunction(onaddfile,o);			
		}		
		else{
		ok=eval(onaddfile);
		}
		if(ok){
			setValue(obj.value);
			return;
		}else{
			obj.parentNode.removeChild(obj);
			this.addCtrl();
		}
	},
	delUpload:function(index){

	},
	setText:function(text){
		this.text=text;
	},
	setDesc:function(desc){
		this.desc=desc||this.desc;
	},
	setMaxFiles:function(){

	},
	setOnaddfile:function(onaddfile){
		this.onaddfile=onaddfile;
	},
	
	//wanghao5
	setGroupId:function(groupId){
		this.groupId=groupId || this.groupId;
	},
	clean:function(){
		this.init();
	}
};

var _imgMap={};
	_imgMap["css"] = _imgMap["txt"] = _imgMap["js"] = "text";
	_imgMap["html"] = _imgMap["htm"] = _imgMap["shtml"] = _imgMap["hta"] = "ie";
	_imgMap["xml"] = _imgMap["xsl"] = "xml";
	_imgMap["pdf"] = "pdf";
	_imgMap["mdb"] = "msaccess";
	_imgMap["doc"] = "msword";
	_imgMap["xls"] = "msexcel";
	_imgMap["ppt"] = "mspowerpoint";
	_imgMap["exe"] = _imgMap["dll"] = _imgMap["sys"] = _imgMap["bat"] = "system";
	_imgMap["zip"] = _imgMap["rar"] = _imgMap["gzip"] = _imgMap["jar"] = _imgMap["lzh"] = "archive";
	_imgMap["ini"] = _imgMap["inf"] = "conf";
	_imgMap["csv"] = "csv";
	_imgMap["psd"] = _imgMap["jpg"] = _imgMap["gif"] = "image";
	_imgMap["ra"] = _imgMap["rm"] = _imgMap["rmvb"] = _imgMap["mp3"] = _imgMap["wma"] = _imgMap["avi"] = "media";
	
function SSB_MultiUpload(ctrlId){
	SSB_SingleUpload.call(this,ctrlId);
	this.url='';
	this.uploading=false;
	this.count=0;  //\u4e0a\u4f20\u6587\u4ef6\u5217\u8868\u957f\u5ea6
	this.currentInput={};
};
SSB_MultiUpload.prototype=new SSB_SingleUpload();
SSB_MultiUpload.prototype.constructor=SSB_MultiUpload;
SSB_MultiUpload.prototype.iconMap=_imgMap;

SSB_MultiUpload.prototype.initCtrl=function(){
	var ctrlId=this.ctrlId;//2009-06-09 width:90%
	var a= '<span hidefocus="" id="btn_'+ctrlId+'"><input id="multibtn_'+ ctrlId +'" class="button" type="button" value="'+this.text+'"/>&nbsp;'+
				'<div class="font_notice addfile_notice" style="display:inline;" id="notice_'+ctrlId+'">'+this.desc+'</div>' +
			'</span>'+				
		   '<table id="tab_'+ctrlId+'" class="filelist" >' +
				'<thead><tr class="filelist_title"><th class="path">'+$res_entry("ui.control.upload.filepath","\u6587\u4ef6\u8def\u5f84")+'</th><th class="operate">'+$res_entry("ui.control.upload.operate","\u64cd\u4f5c")+'</th></tr>' +
				'</thead>' +
				'<tbody id="upload_list_'+ctrlId+'">' +
					'<tr id="no_upload_'+ctrlId+'" class="nofile_notice">' +
						'<td colSpan=2>'+$res_entry("ui.control.upload.unaddfile","\u5c1a\u672a\u6dfb\u52a0\u6587\u4ef6...")+'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>';
	this.labelObj.innerHTML=a;
};

var multiUpSerial = 0;
//wanghao4
SSB_MultiUpload.prototype.getFileCtrl=function(){
		var oInput=document.createElement('input');
		var that=this;
		//oInput.id = "upload_"+this.ctrlId+this.count;
		//oInput.name = "upload_"+this.ctrlId+this.count;
		oInput.id = "upload_"+this.ctrlId+multiUpSerial;
		oInput.name = "upload_"+this.ctrlId+multiUpSerial;
		multiUpSerial++;
		
		oInput.type = "file";
		oInput.hideFocus = true;
		oInput.size = 1;
		oInput['onchange']=function(event){
			that.addUpload(event||window.event);
		};
		return oInput;
}

SSB_MultiUpload.prototype.addCtrl=function(){
	var oInput=this.getFileCtrl();
	oInput.className='multi_input';
	this.currentInput=oInput;
	document.getElementById('btn_'+this.ctrlId).appendChild(oInput);
		
	//\u521d\u59cbfile\u63a7\u4ef6
	$(oInput).css("left", 0).css("margin-left",0).css("width",0);
	
	var buttonInfo = this.getCtronlInfo("multibtn_"+this.ctrlId);
	var fileInfo = this.getCtronlInfo(oInput.id);
	
	var offset = buttonInfo.x-fileInfo.x-10;
	
	$(oInput).attr("xwidth", offset).css("margin-left",offset);
	
	this.initMultiFakePositon(this.ctrlId, multiUpSerial-1);
	this.multiMouseMoveEvent();
};

SSB_MultiUpload.prototype.initMultiFakePositon = function(upid, count){
		var tempobj = document.getElementById("multibtn_"+upid);	
		var fakebtInfo = {};
		fakebtInfo.x = 0;
		fakebtInfo.y = 0;
		fakebtInfo.width = tempobj.offsetWidth;
		fakebtInfo.height = tempobj.offsetHeight;
		
		while(tempobj!=null && tempobj!=document.body)	{
			fakebtInfo.x += tempobj.offsetLeft + tempobj.clientLeft;
			fakebtInfo.y += tempobj.offsetTop + tempobj.clientTop;
			tempobj = tempobj.offsetParent;
		}

		var filebt = document.getElementById("upload_"+upid+count);
		var filebtInfo = {};
		filebtInfo.x = 0;
		filebtInfo.y = 0;
		filebtInfo.width = filebt.offsetWidth;
		filebtInfo.height = filebt.offsetHeight;
		
		while(filebt!=null && filebt!=document.body)
		{
			filebtInfo.x += filebt.offsetLeft + filebt.clientLeft;
			filebtInfo.y += filebt.offsetTop + filebt.clientTop;
			filebt = filebt.offsetParent
		}
		
		eval("multifakebtInfos."+upid+"=fakebtInfo");
		eval("multifilebtInfos."+upid+count+"=filebtInfo");
};

SSB_MultiUpload.prototype.multiMouseMoveEvent = function(){
	var ctrlId=(this.currentInput.id).split("_")[1];
	var upid = this.ctrlId;
	var adjustValue = this.adjustValue;
	
	document.getElementById('btn_'+this.ctrlId).onmousemove = function(){
		
		var mouseposition = SSB_Upload.onmousemove();

		var fakebtInfo = eval("multifakebtInfos."+upid);
		var filebtInfo = eval("multifilebtInfos."+ctrlId);

		var filebt = document.getElementById("upload_"+ctrlId);
		var xwidth = $(filebt).attr("xwidth");
		if(mouseposition.x >= fakebtInfo.x-2 && mouseposition.x <= fakebtInfo.x+fakebtInfo.width-2) {
			var offset = parseInt(xwidth) + (mouseposition.x - filebtInfo.x - filebtInfo.width) +20;
			$(filebt).css("margin-left", offset);
		}
		else{
			$(filebt).css("margin-left", parseInt(xwidth));
		}
	}
};

SSB_MultiUpload.prototype.addUpload=function(event){
	var oInput=event.srcElement||event.target;
	var filePath=oInput.value;
	var that=this;
	if(this.url.indexOf(filePath)>0){
		alert($res_entry("ui.control.upload.doublefile","\u5f53\u524d\u76ee\u5f55\u4e0b\u5df2\u6709\u76f8\u540c\u6587\u4ef6\uff0c\u65e0\u987b\u91cd\u590d\u4e0a\u4f20"));
		this.delRepeatInput(oInput);
		this.addCtrl();
		return;
	}
	if(this.maxfiles!=-1&&this.count>=this.maxfiles){
		alert($res_entry("ui.control.upload.toomuchfile1","\u6700\u591a\u4e0a\u4f20")+this.maxfiles+$res_entry("ui.control.upload.toomuchfile2","\u4e2a\u6587\u4ef6\u3002"));
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	var ok=true;
	var onaddfile=this.onaddfile;
	if(onaddfile){
		try{
			ok=eval(onaddfile);
		}catch(e){
			this.delUpload(oInput);
			this.addCtrl();
			throw e;
		}
	}
	if(!ok){
		this.delUpload(oInput);
		this.addCtrl();
		return;
	}
	this.url += "|"+filePath;
	var sIcon='unknown';
	var ext=filePath.substring(1+filePath.lastIndexOf("."),filePath.length);
	sIcon=this.iconMap[ext]?this.iconMap[ext]:sIcon;
	var oFileList = document.getElementById('upload_list_'+this.ctrlId);
	var oTr = document.createElement('tr');
	oFileList.appendChild(oTr);
	var oTd = document.createElement('td');
	oTr.appendChild(oTd);
	var oIcon = document.createElement('div');
	oIcon.className='file_icon icon_'+sIcon;
	var oStr=document.createTextNode(filePath);
	oTd.appendChild(oIcon);
	oTd.appendChild(oStr);
	oTd=document.createElement('td');
	oTr.appendChild(oTd);
	var oA=document.createElement('a');
	oA.className="option_del";
	oA.onclick=function(event){
		event=event||window.event;
		var a=event.srcElement||event.target;
		that.delUpload(a['oInput']);
	};
	oA.href="#";
	oTd.appendChild(oA);
	oIcon=document.createElement('div');
	oIcon.className='file_icon icon_del';

	oStr=document.createTextNode($res_entry('ui.control.upload.delete','\u5220\u9664'));
	oA.appendChild(oIcon);
	oA.appendChild(oStr);

	document.getElementById('no_upload_'+this.ctrlId).style.display='none';
	//\u4e92\u76f8\u4fdd\u5b58\u5f15\u7528
	oIcon['oInput']=oInput;
	oA['oInput']=oInput;
	oInput['oTr']=oTr;
	oInput.style.display='none';
	this.count++;			//\u6587\u4ef6\u589e\u52a01\u4e2a
	this.addCtrl();
};
SSB_MultiUpload.prototype.delUpload=function(oInput){
	if(this.uploading){
		alert($res_entry("ui.control.upload.uploading","\u6587\u4ef6\u4e0a\u4f20\u4e2d\uff0c\u8bf7\u7b49\u5f85"));
		return;
	}
	var oTr=oInput['oTr'];
	this.url = this.url.replace(oInput.value,"");
	this.url = this.url.replace("||","");
	oInput.parentNode.removeChild(oInput);
	if(oTr){
		oTr.parentNode.removeChild(oTr);
		this.count--;		//\u6587\u4ef6\u51cf\u5c11\u4e00\u4e2a
	}
	if(this.count==0){
		document.getElementById("no_upload_"+this.ctrlId).style.display='';
	}
};
//\u5220\u9664\u91cd\u590d\u7684 input \u6807\u7b7e\u3002
SSB_MultiUpload.prototype.delRepeatInput=function(oInput){
	oInput.parentNode.removeChild(oInput);
};
SSB_MultiUpload.prototype.setMaxFiles=function(maxfiles){
	this.maxfiles=isNaN(maxfiles)||maxfiles<1?-1:maxfiles;
};
SSB_MultiUpload.prototype.clean=function(){
	this.init();
	this.count=0;
	this.url='';
	this.currentInput={};
};

function UploadTag(id){
	var tagObj=document.getElementById(id);
	var type=tagObj.getAttribute('type');
	var upload={};
	switch(type){
		case 'single':upload= new SSB_SingleUpload(id);break;
		case 'multi':upload= new SSB_MultiUpload(id);break;
		default:upload= new SSB_SingleUpload(id);
	}
	upload.setText(tagObj.getAttribute("text"));
	upload.setDesc(tagObj.getAttribute("desc"));
	upload.setMaxFiles(parseInt(tagObj.getAttribute("maxfiles")));
	upload.setOnaddfile(tagObj.getAttribute("onaddfile"));
	
	//wanghao5
	upload.setGroupId(tagObj.getAttribute("groupId"));
	return upload;
};

var _ZUPLOAD_TAG="z:upload";
var _UPLOAD_TAG="upload";
var _ZTABLE_CLUMN ="z:column";
var _TABLE_CLUMN ="column";

//wanghao8
/*
function upload_init_add(){
	var script = document.createElement ("script");         
	script.type="text/javascript"; 
	script.text="addOnloadEvent(Upload_init);"; 	
	document.body.appendChild(script);
}
addOnloadEvent(upload_init_add);
*/
var input_file = [];

function Upload_init(){
	//wanghao8
	var inputs = document.getElementsByTagName("input");
	for(var ii = 0;ii<inputs.length;ii++)
	{
		if(inputs[ii].type.toUpperCase() == "FILE")
		{
			input_file.push(inputs[ii]);
			__file_List.push(inputs[ii]);
			if(typeof(inputs[ii].groupId)=="undefined" || inputs[ii].groupId==null || trim(inputs[ii].groupId)=="")
			{
				file_group["noId"].push(inputs[ii]);
			}
			else
			{
				if(!file_group[inputs[ii].groupId])
					file_group[inputs[ii].groupId] = [];
				
				file_group[inputs[ii].groupId].push(inputs[ii]);
	
			}
		}
		
	}
	
	var atags=document.getElementsByTagName(_ZUPLOAD_TAG);
	if(!atags||atags.length==0)
	{
		atags=document.getElementsByTagName(_UPLOAD_TAG);	
	}
	var upload={};
	for(var i=0;i<atags.length;i++)
	{
		var tagType = atags[i].parentNode;
		var ptn = tagType.tagName.toUpperCase();
		//\u5982\u679c\u63a7\u4ef6\u4e0d\u5728\u8868\u683c\u63a7\u4ef6\u4e2d\u65f6
		if(ptn != _ZTABLE_CLUMN && ptn != _TABLE_CLUMN)
		{
			upload=new UploadTag(atags[i].getAttribute("id"));
		    upload.init();
		}		
	}
};
SSB_Upload={};
SSB_Upload.uploads={};
addOnloadEvent(Upload_init);

//2009-06-09 wxr \u5355\u4e0a\u4f20\u5b58\u50a8\u63a7\u4ef6\u4f4d\u7f6e\u53d8\u91cf
var fakebtInfos = {};
var filebtInfos = {};

//2009-06-09 wxr \u591a\u4e0a\u4f20\u5b58\u50a8\u63a7\u4ef6\u4f4d\u7f6e\u53d8\u91cf
var multifakebtInfos = {};
var multifilebtInfos = {};

//2009-06-09 wxr \u4e0a\u4f20\u53d6\u9f20\u6807\u5f53\u524d\u4f4d\u7f6e\u65b9\u6cd5
SSB_Upload.onmousemove =  function(ev){
    ev = ev || window.event;

	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

//\u83b7\u53d6\u4e0a\u4f20\u6587\u4ef6\u4fe1\u606f\u7684\u7edf\u4e00\u65b9\u6cd5
function getUploadedFilesInfo(uploadId)
{	
	//uploadId \u4e3a\u4e0a\u4f20\u63a7\u4ef6id
	var fileInfo = {};
	var uploadType = $("#"+uploadId).attr("type");
	
	if(uploadType == "multi") {
		//\u591a\u6587\u4ef6\u4e0a\u4f20\uff0c\u8fd4\u56de\u6587\u4ef6\u4e2a\u6570\u548c\u6587\u4ef6\u540d\u6570\u7ec4
		fileInfo.fileNames = [];
		var divs = $("#upload_list_"+uploadId).find("tr td:first-child").each(function(index) {
			if(index > 0) {			
				fileInfo.fileNames.push($(this).text());
			}		
		});
		
		fileInfo.fileCount = fileInfo.fileNames.length;
	}
	else if(uploadType == "single"){
		//\u5355\u6587\u4ef6\u4e0a\u4f20\uff0c\u76f4\u63a5\u8fd4\u56de\u6587\u4ef6\u540d
		fileInfo = $("#div_"+uploadId).find(".singleText").val();
	}	
	
	return fileInfo;
}

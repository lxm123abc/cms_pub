package com.zte.cms.service.servicecntbindmap.service;

import java.util.List;
import com.zte.cms.service.servicecntbindmap.model.ServiceProgramMap;
import com.zte.cms.service.servicecntbindmap.dao.IServiceProgramMapDAO;
import com.zte.cms.service.servicecntbindmap.service.IServiceProgramMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ServiceProgramMapDS extends DynamicObjectBaseDS implements IServiceProgramMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceProgramMapDAO dao = null;

    public void setDao(IServiceProgramMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException
    {
        log.debug("insert serviceProgramMap starting...");
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_service_program_map_index");
        serviceProgramMap.setMapindex(index);
        //mappingid =mapping对象编码（两位）+mapping对象index（30位，不足30前面补零）
        String id = index.toString();
        for (int i = id.length();i<30;i++){
            id = "0"+id;
        }
        id = "21"+id;
        serviceProgramMap.setMappingid(id);
        try
        {
            dao.insertServiceProgramMap(serviceProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert serviceProgramMap end");
    }

    public void updateServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException
    {
        log.debug("update serviceProgramMap by pk starting...");
        try
        {
            dao.updateServiceProgramMap(serviceProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceProgramMap by pk end");
    }

    public void updateServiceProgramMapList(List<ServiceProgramMap> serviceProgramMapList)
            throws DomainServiceException
    {
        log.debug("update serviceProgramMapList by pk starting...");
        if (null == serviceProgramMapList || serviceProgramMapList.size() == 0)
        {
            log.debug("there is no datas in serviceProgramMapList");
            return;
        }
        try
        {
            for (ServiceProgramMap serviceProgramMap : serviceProgramMapList)
            {
                dao.updateServiceProgramMap(serviceProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update serviceProgramMapList by pk end");
    }

    public void removeServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException
    {
        log.debug("remove serviceProgramMap by pk starting...");
        try
        {
            dao.deleteServiceProgramMap(serviceProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceProgramMap by pk end");
    }

    public void removeServiceProgramMapList(List<ServiceProgramMap> serviceProgramMapList)
            throws DomainServiceException
    {
        log.debug("remove serviceProgramMapList by pk starting...");
        if (null == serviceProgramMapList || serviceProgramMapList.size() == 0)
        {
            log.debug("there is no datas in serviceProgramMapList");
            return;
        }
        try
        {
            for (ServiceProgramMap serviceProgramMap : serviceProgramMapList)
            {
                dao.deleteServiceProgramMap(serviceProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove serviceProgramMapList by pk end");
    }

    public ServiceProgramMap getServiceProgramMap(ServiceProgramMap serviceProgramMap) throws DomainServiceException
    {
        log.debug("get serviceProgramMap by pk starting...");
        ServiceProgramMap rsObj = null;
        try
        {
            rsObj = dao.getServiceProgramMap(serviceProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceProgramMapList by pk end");
        return rsObj;
    }

    public List<ServiceProgramMap> getServiceProgramMapByCond(ServiceProgramMap serviceProgramMap)
            throws DomainServiceException
    {
        log.debug("get serviceProgramMap by condition starting...");
        List<ServiceProgramMap> rsList = null;
        try
        {
            rsList = dao.getServiceProgramMapByCond(serviceProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get serviceProgramMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceProgramMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceProgramMap serviceProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(serviceProgramMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceProgramMap page info by condition end");
        return tableInfo;
    }
}

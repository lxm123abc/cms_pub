package com.zte.cms.channel.update.apply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.cms.channel.model.ScheduleWkfwhis;
import com.zte.cms.channel.service.ICmsScheduleBufDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.ICmsSchedulerecordDS;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.service.ls.ServiceConstants;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.CommonCallback;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import net.sf.cglib.beans.BeanCopier;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleUpdateApplyCallback extends CommonCallback {
	// 日志
	protected ICmsScheduleDS scheduleds = null;
	protected ICmsScheduleBufDS scheduleBufds = null;
	protected IScheduleWkfwhisDS scheduleWkfwhisDS;

	protected CmsSchedule scheduleBasic;
	protected CmsScheduleBuf scheduleBuf;
	protected ScheduleWkfwhis scheduleWkfwhis;
	private Log log = SSBBus.getLog(getClass());
	private IPhysicalchannelDS physicalchannelDS;
	

    /**
	 * 
	 * @return IPhysicalchannelDS
	 */
	public IPhysicalchannelDS getPhysicalchannelDS() {
		return physicalchannelDS;
	}

	/**
	 * 
	 * @param physicalchannelDS IPhysicalchannelDS
	 */
	public void setPhysicalchannelDS(IPhysicalchannelDS physicalchannelDS) {
		this.physicalchannelDS = physicalchannelDS;
	}

	public ICmsScheduleDS getScheduleds() {
		return scheduleds;
	}

	public void setScheduleds(ICmsScheduleDS scheduleds) {
		this.scheduleds = scheduleds;
	}

	public ICmsScheduleBufDS getScheduleBufds() {
		return scheduleBufds;
	}

	public void setScheduleBufds(ICmsScheduleBufDS scheduleBufds) {
		this.scheduleBufds = scheduleBufds;
	}

	public IScheduleWkfwhisDS getScheduleWkfwhisDS() {
		return scheduleWkfwhisDS;
	}

	public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS) {
		this.scheduleWkfwhisDS = scheduleWkfwhisDS;
	}

	public CmsSchedule getScheduleBasic() {
		return scheduleBasic;
	}

	public void setScheduleBasic(CmsSchedule scheduleBasic) {
		this.scheduleBasic = scheduleBasic;
	}

	public CmsScheduleBuf getScheduleBuf() {
		return scheduleBuf;
	}

	public void setScheduleBuf(CmsScheduleBuf scheduleBuf) {
		this.scheduleBuf = scheduleBuf;
	}

	public ScheduleWkfwhis getScheduleWkfwhis() {
		return scheduleWkfwhis;
	}

	public void setScheduleWkfwhis(ScheduleWkfwhis scheduleWkfwhis) {
		this.scheduleWkfwhis = scheduleWkfwhis;
	}

	@Override
	public AppLogInfoEntity getLogInfo() {
		ResourceMgt
				.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
		AppLogInfoEntity loginfo = new AppLogInfoEntity();

		String optObjecttype = ChannelConstants
				.getWorkflowTypeName(ChannelConstants.WKFWTYPE_CHANNEL_UPDATE);
		loginfo.setOptObjecttype(optObjecttype);
		loginfo.setOptObject(wkflItem.getPboIndex().toString());

		String info;
		String logKey = "";
		if (wkflItem.getNodeId().longValue() == 1) {
			logKey = "ucdn.schedule.log.wkfw.cntdistribute.update.apply";
		} else {
			logKey = "ucdn.schedule.log.wkfw.cntdistribute.update.audit";
		}
		info = ResourceMgt.findDefaultText(logKey);
		if (null != info) {
			info = info.replace("[opername]", wkflItem.getExcutorName());
			info = info
					.replace("[programname]", scheduleBasic.getProgramname());
			info = info.replace("[nodename]", wkflItem.getNodeName());
			info = info.replace("[handleevent]", wkflItem.getHandleEvent());
		}
		loginfo.setOptDetail(info);
		loginfo.setServicekey(GlobalConstants.IPTV_SERVICE_KEY);
		return loginfo;
	}

	@Override
	public void initData() {
		Long scheduleindex = wkflItem.getPboIndex();
		scheduleBuf = new CmsScheduleBuf();
		scheduleBuf.setScheduleindex(scheduleindex);

		// 查询service对象
		try {
			scheduleBuf = scheduleBufds.getCmsScheduleBuf(scheduleBuf);
			scheduleBuf.setWorkflowlife(wkflItem.getPboState());

			scheduleBasic = new CmsSchedule();
			scheduleBasic.setScheduleindex(scheduleindex);
			scheduleBasic = scheduleds.getCmsSchedule(scheduleBasic);
			scheduleBasic.setWorkflowlife(wkflItem.getPboState());

			if (scheduleBuf.getEffecttime() == null
					|| "".equals(scheduleBuf.getEffecttime())) {
				wkflItem.setEffectiveTime("0");
			} else {
				wkflItem.setEffectiveTime("0");
			}
			wkflItem.setPboId(scheduleBuf.getChannelid());

			// 创建工作流历史表
			scheduleWkfwhis = new ScheduleWkfwhis();
			BeanCopier copier = BeanCopier.create(CmsScheduleBuf.class,
					ScheduleWkfwhis.class, false);
			copier.copy(scheduleBuf, scheduleWkfwhis, null);

			scheduleWkfwhis.setWorkflowindex(Long.valueOf(wkflItem
					.getFlowInstId()));
			scheduleWkfwhis.setTaskindex(new Long(wkflItem.getTaskId()));
			scheduleWkfwhis.setNodeid(Long.valueOf(wkflItem.getNodeId()
					.longValue()));
			scheduleWkfwhis.setNodename(wkflItem.getNodeName());
			scheduleWkfwhis.setOpername(wkflItem.getExcutorId());
			scheduleWkfwhis.setTemplateindex(Long.valueOf(wkflItem
					.getTemplateSeqNo().longValue()));
			scheduleWkfwhis.setOpopinion(wkflItem.getComments());
			scheduleWkfwhis.setHandleevent(wkflItem.getHandleEvent());

			SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
			if (wkflItem.getStartTime() != null) {
				String workflowstarttime = dateformat.format(wkflItem
						.getStartTime());
				scheduleWkfwhis.setWorkflowstarttime(workflowstarttime);
			}
			if (wkflItem.getFinishTime() != null) {
				String workflowendtime = dateformat.format(wkflItem
						.getFinishTime());
				scheduleWkfwhis.setWorkflowendtime(workflowendtime);
			}

		} catch (DomainServiceException dsEx) {
			log.error("domain exception:" + dsEx);
			throw new RuntimeException(dsEx);
		}

	}

	@Override
	public void operate() {
		try {
			if (wkflItem.getPboState().intValue() == -1) // 审核不通过，根据原来的状态修改审核不通过的状态
			{
				int oldStatus = scheduleBuf.getStatus();
				if (oldStatus == 50) {
					scheduleBasic.setStatus(20);
				} else {
					scheduleBasic.setStatus(oldStatus);
				}

				scheduleBufds.removeCmsScheduleBuf(scheduleBuf);
				scheduleBasic.setWorkflow(0); // 否则会总是在审核中
				scheduleBasic.setWorkflowlife(0); // 未启动
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
				scheduleBasic.setEffecttime(format.format(new Date()));
				// 将生效时间字段设为当前时间

			} else {
				scheduleBufds.updateCmsScheduleBuf(scheduleBuf);
			}
			scheduleds.updateCmsSchedule(scheduleBasic);
			

			scheduleWkfwhisDS.insertScheduleWkfwhis(scheduleWkfwhis);
		} catch (DomainServiceException dsEx) {
			log.error("domain exception:" + dsEx);
			throw new RuntimeException(dsEx);
		}
	}

}

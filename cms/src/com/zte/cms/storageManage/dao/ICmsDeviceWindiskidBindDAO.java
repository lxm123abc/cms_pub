package com.zte.cms.storageManage.dao;

import java.util.List;

import com.zte.cms.storageManage.model.CmsDeviceWindiskidBind;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsDeviceWindiskidBindDAO
{
    /**
     * 新增CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DAOException dao异常
     */
    public void insertCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException;

    /**
     * 根据主键更新CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DAOException dao异常
     */
    public void updateCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException;

    /**
     * 根据主键删除CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @throws DAOException dao异常
     */
    public void deleteCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind) throws DAOException;

    /**
     * 根据主键查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @return 满足条件的CmsDeviceWindiskidBind对象
     * @throws DAOException dao异常
     */
    public CmsDeviceWindiskidBind getCmsDeviceWindiskidBind(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DAOException;

    /**
     * 根据条件查询CmsDeviceWindiskidBind对象
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @return 满足条件的CmsDeviceWindiskidBind对象集
     * @throws DAOException dao异常
     */
    public List<CmsDeviceWindiskidBind> getCmsDeviceWindiskidBindByCond(CmsDeviceWindiskidBind cmsDeviceWindiskidBind)
            throws DAOException;

    /**
     * 根据条件分页查询CmsDeviceWindiskidBind对象，作为查询条件的参数
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询CmsDeviceWindiskidBind对象，作为查询条件的参数
     * 
     * @param cmsDeviceWindiskidBind CmsDeviceWindiskidBind对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsDeviceWindiskidBind cmsDeviceWindiskidBind, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException;

}
package com.zte.ucm.config;

import com.zte.ucm.log.Logger;
import com.zte.ucm.log.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XMLParser
{
  private static Logger log = LoggerFactory.getLogger(XMLParser.class);

  public List<ConfigItem> parserXml(String paramString)
  {
    ArrayList localArrayList = new ArrayList();

    File localFile = new File(paramString);
    SAXReader localSAXReader = new SAXReader();
    try
    {
      Document localDocument = localSAXReader.read(localFile);
      List localList = localDocument.selectNodes("/config_info_desc/congfig_item_list/config_item");

      if ((localList != null) && (localList.size() > 0))
      {
        for (int i = 0; i < localList.size(); i++)
        {
          Element localElement = (Element)localList.get(i);
          String str1 = ((Element)localElement.elements("item_name").get(0)).getTextTrim();
          String str2 = ((Element)localElement.elements("item_value").get(0)).getTextTrim();
          String str3 = ((Element)localElement.elements("item_code").get(0)).getTextTrim();

          ConfigItem localConfigItem = new ConfigItem();
          localConfigItem.setItem_name(str1);
          localConfigItem.setItem_current_value(str2);
          localConfigItem.setItem_code(str3);

          localArrayList.add(localConfigItem);
        }
      }
      log.info("Parse config_apply_items.xml successful");
    }
    catch (DocumentException localDocumentException)
    {
      localDocumentException.printStackTrace();
    }
    return localArrayList;
  }
}
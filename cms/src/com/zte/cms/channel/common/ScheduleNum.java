package com.zte.cms.channel.common;

import com.zte.cms.channel.model.CmsSchedule;

public class ScheduleNum
{
    public ScheduleNum(int num, CmsSchedule cmsSchedule)
    {
        this.num = num;
        this.cmsSchedule = cmsSchedule;
    }

    public int num;
    public CmsSchedule cmsSchedule;

    public int getNum()
    {
        return num;
    }

    public void setNum(int num)
    {
        this.num = num;
    }

    public CmsSchedule getCmsSchedule()
    {
        return cmsSchedule;
    }

    public void setCmsSchedule(CmsSchedule cmsSchedule)
    {
        this.cmsSchedule = cmsSchedule;
    }

}

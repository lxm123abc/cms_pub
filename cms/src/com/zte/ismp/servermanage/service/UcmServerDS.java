package com.zte.ismp.servermanage.service;

import java.util.List;

import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.umap.ucms.dao.IUcmServerDAO;
import com.zte.umap.ucms.model.UcmServer;

public class UcmServerDS implements IUcmServerDS
{
    private IUcmServerDAO ucmServerDao = null;
    private Log log = SSBBus.getLog(getClass());

    public List<UcmServer> getResServer() throws Exception
    {
        log.debug("UcmServerLS: getResServer starts");
        try
        {
            UcmServer ucmServer = new UcmServer();
            ucmServer.setServertype(GlobalConstants.SERVER_TYPE_DOWNLOAD);
            ucmServer.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
            List<UcmServer> list = ucmServerDao.getUcmServerByCond(ucmServer);
            log.debug("UcmServer: getResServer ends");
            if (list != null && list.size() > 0)
            {
                return list;
            }
            else
            {
                throw new Exception("0422202");// 所查找的服务器不存在
            }
        }
        catch (Exception e)
        {
            log.debug("Error occurred in getResServer method", e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
    }

    public void setUcmServerDao(IUcmServerDAO ucmServerDao)
    {
        this.ucmServerDao = ucmServerDao;
    }
}

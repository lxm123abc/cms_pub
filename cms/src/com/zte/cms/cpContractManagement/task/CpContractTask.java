package com.zte.cms.cpContractManagement.task;

import com.zte.cms.common.DbUtil;
import com.zte.cms.cpContractManagement.dao.IContractMapDao;
import com.zte.cms.cpContractManagement.model.ContractEmailSendLog;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.ismp.common.ConfigUtil;
import com.zte.ismp.systemconfig.ls.IIsysConfigLS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ucm.util.EmailServiceUtil;
import com.zte.ucm.util.KwCheck;
import com.zte.ucm.util.ToEmailUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: Liangxiaomin
 * @Date Created in 12:25 2019/6/24
 * @Description:
 */
public class CpContractTask {

    //email地址
    private static String emailInfo = StringUtils.trimToEmpty(ConfigUtil.get("email_smtp_info"));

    //管理员电话
    private static String contractPhone = StringUtils.trimToEmpty(ConfigUtil.get("contract_Phone"));

    private static IContractMapDao dao;


    public void execute() throws Exception {

        long start = System.currentTimeMillis();
        System.out.println(" ********** 定时任务 - 监测即将到期合同 - 开始执行 - 时间:" + DateUtils.convertDataToString_fit(new Date(), DateUtils.fit_a));

        if (dao == null) {
            dao = (IContractMapDao) SSBBus.findDomainService("ContractMapDao");
        }

        //获取字典表配置定时时间参数(天)
        IIsysConfigLS configLs = (IIsysConfigLS) SSBBus.findDomainService("ismpSysConfigLS");
        String configSendDates = configLs.getUsysConfigByCfgkey("cms.CpContractTask.taskSendDates");
        if (StringUtils.isEmpty(configSendDates)) {
            System.out.println(" ********** 定时任务 - 监测到期合同 - 错误 - 未配置时间");
            return;
        }
        String[] split = configSendDates.split("\\|");
        for (int i = 0; i < split.length; i++) {
            int grade = i + 1;
            int day = Integer.parseInt(split[i]);
            /**
             *  1. 全查除只剩day天数据
             *  2. 根据查询出的id和等级去日志表查询
             */
            List<ContractMap> contractMaps = dao.queryWillOver(day);
            for (ContractMap contractMap : contractMaps) {
                ContractEmailSendLog contractEmailSendLog = new ContractEmailSendLog();
                contractEmailSendLog.setOid(contractMap.getId() + "");
                contractEmailSendLog.setGrade(grade);
                Integer count = dao.queryContractSendEmailLogByOidAndGrade(contractEmailSendLog);
                if (count == null || count == 0) {
                    //拼接发送Email的信息
                    String content = null;
                    String title = null;
                    String companyName = contractMap.getCpName();
                    //获取该CPCODE配置的Email地址
                    String email = UserInfoUtils.getEmailByCpCode(contractMap.getCpCode());

                    if (StringUtils.isEmpty(email)) {
                        System.out.println(" ********** 定时任务 - 监测到期合同 - 错误 - 原因 - " + contractMap.getCpCode() + "未配置email地址");
                        return;
                    }
                    if (day == 0) {
                        content = companyName +
                                ":贵司与四川移动合作协议(" +
                                DateUtils.convertDataToString_fit(contractMap.getBeginDate(), DateUtils.fit_yyyy_MM_DD) + "-" +
                                DateUtils.convertDataToString_fit(contractMap.getEndDate(), DateUtils.fit_yyyy_MM_DD) +
                                ")已到期，请注意续签合同，同时请注意版权的有效期的续签。<br />" +
                                "如有疑问请联络移动:聂毅,联系电话:" + contractPhone;
                        title = "四川移动电视业务合作伙伴管理平台|合同到期通知";
                    } else {
                        content = companyName +
                                ":贵司与四川移动合作协议(" +
                                DateUtils.convertDataToString_fit(contractMap.getBeginDate(), DateUtils.fit_yyyy_MM_DD) + "-" +
                                DateUtils.convertDataToString_fit(contractMap.getEndDate(), DateUtils.fit_yyyy_MM_DD) +
                                ")还有" + day + " 天到期，请注意续签合同，同时请注意版权的有效期的续签。<br />" +
                                "如有疑问请联络移动:聂毅,联系电话:" + contractPhone;
                        title = "四川移动电视业务合作伙伴管理平台|合同到期预警";
                    }
                    //发送邮件，在线程中发送邮件，解决一次性发送多封邮件导致的数据库连接中断
                    ToEmailUtil toEmailUtil = new ToEmailUtil(email, title, content, emailInfo);
                    Thread thread = new Thread(toEmailUtil);
                    thread.start();
                    String superEmail = UserInfoUtils.getEmailByCpId("super");
                    if (StringUtils.isNotEmpty(superEmail)) {
                        content = "公司:" + companyName + ",与四川移动合作协议已到期，请注意.";
                        ToEmailUtil toEmailUtil2 = new ToEmailUtil(superEmail, title, content, emailInfo);
                        Thread thread2 = new Thread(toEmailUtil2);
                        thread2.start();
                    } else {
                        System.out.println(" ********** 定时任务 - 监测到期合同 - 管理员未配置Email");
                    }
                    contractEmailSendLog.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    dao.insertContractSendEmailLog(contractEmailSendLog);
                }
            }
        }

        long end = System.currentTimeMillis();

        System.out.println(" ********** 定时任务 - 监测到期合同 - 完毕 - 用时 - " + (end - start));

    }


}

package com.zte.cms.service.wkfw.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

/**
 * 工作流相关查询条件
 * 
 * @author 唐秀华 tang.xiuhua@zte.com.cn
 */

public class ServiceWkfwCondition extends DynamicBaseObject
{
    /**
     * 进程创建者
     */
    private java.lang.String creator;
    /**
     * 进程启动时间开始时间
     */
    private java.util.Date processStartTimeStart;
    /**
     * 进程启动时间结束时间
     */
    private java.util.Date processStartTimeEnd;
    /**
     * 工作流类型
     */
    private java.lang.String pboType;
    /**
     * 工作流任务优先级
     */
    private java.lang.String priority;
    /**
     * 参与者ID
     */
    private java.lang.String executorId;
    /**
     * 工作流流程状态编码
     */
    private java.lang.String nodeStatusCode;
    /**
     * 任务状态
     */
    private java.lang.String taskStatus;
    /**
     * 进程状态
     */
    private java.lang.String processStatus;
    /**
     * 接受状态
     */
    private java.lang.String acceptStatus;

    /**
     * 服务编码
     */
    private java.lang.String serviceid;
    /**
     * 服务名称
     */
    private java.lang.String servicename;

    /**
     * 创建的操作员userid
     */
    private java.lang.String owner;

    /**
     * 策略历史表字段：操作策略工作流的操作员userid（比如工作流发起者或者任务执行者）
     */
    private java.lang.String opername;

    /**
     * 当前操作员用户名
     */
    private java.lang.String userid;

    public java.lang.String getCreator()
    {
        return creator;
    }

    public void setCreator(java.lang.String creator)
    {
        this.creator = creator;
    }

    public java.util.Date getProcessStartTimeStart()
    {
        return processStartTimeStart;
    }

    public void setProcessStartTimeStart(java.util.Date processStartTimeStart)
    {
        this.processStartTimeStart = processStartTimeStart;
    }

    public java.util.Date getProcessStartTimeEnd()
    {
        return processStartTimeEnd;
    }

    public void setProcessStartTimeEnd(java.util.Date processStartTimeEnd)
    {
        this.processStartTimeEnd = processStartTimeEnd;
    }

    public java.lang.String getPboType()
    {
        return pboType;
    }

    public void setPboType(java.lang.String pboType)
    {
        this.pboType = pboType;
    }

    public java.lang.String getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.String priority)
    {
        this.priority = priority;
    }

    public java.lang.String getExecutorId()
    {
        return executorId;
    }

    public void setExecutorId(java.lang.String executorId)
    {
        this.executorId = executorId;
    }

    public java.lang.String getNodeStatusCode()
    {
        return nodeStatusCode;
    }

    public void setNodeStatusCode(java.lang.String nodeStatusCode)
    {
        this.nodeStatusCode = nodeStatusCode;
    }

    public java.lang.String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(java.lang.String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public java.lang.String getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(java.lang.String processStatus)
    {
        this.processStatus = processStatus;
    }

    public java.lang.String getAcceptStatus()
    {
        return acceptStatus;
    }

    public void setAcceptStatus(java.lang.String acceptStatus)
    {
        this.acceptStatus = acceptStatus;
    }

    public java.lang.String getServiceid()
    {
        return serviceid;
    }

    public void setServiceid(java.lang.String serviceid)
    {
        this.serviceid = serviceid;
    }

    public java.lang.String getServicename()
    {
        return servicename;
    }

    public void setServicename(java.lang.String servicename)
    {
        this.servicename = servicename;
    }

    public java.lang.String getOwner()
    {
        return owner;
    }

    public void setOwner(java.lang.String owner)
    {
        this.owner = owner;
    }

    public java.lang.String getOpername()
    {
        return opername;
    }

    public void setOpername(java.lang.String opername)
    {
        this.opername = opername;
    }

    public java.lang.String getUserid()
    {
        return userid;
    }

    public void setUserid(java.lang.String userid)
    {
        this.userid = userid;
    }

    @Override
    /**
     * 设置变量跟表字段的映射关系，支持页面列表排序
     * 
     */
    public void initRelation()
    {
        this.addRelation("processId", "process_id");
        this.addRelation("processName", "process_name");
        this.addRelation("creator", "creator");
        this.addRelation("processStartTime", "process_start_time");
        this.addRelation("pboId", "pbo_id");
        this.addRelation("pboName", "pbo_name");
        this.addRelation("pboType", "pbo_type");
        this.addRelation("priority", "priority");
        this.addRelation("taskId", "task_id");
        this.addRelation("taskStartTime", "task_start_time");
        this.addRelation("executorId", "executor_id");
        this.addRelation("nodeStatus", "node_status");
        this.addRelation("nodeStatusCode", "node_status_code");
        this.addRelation("taskStatus", "task_status");
        this.addRelation("processStatus", "process_status");
        this.addRelation("nodeName", "node_name");
        this.addRelation("acceptStatus", "accept_status");

        this.addRelation("serviceid", "serviceid");
        this.addRelation("servicename", "servicename");

        this.addRelation("owner", "owner");
        this.addRelation("opername", "opername");

        this.addRelation("userid", "userid");
    }

}

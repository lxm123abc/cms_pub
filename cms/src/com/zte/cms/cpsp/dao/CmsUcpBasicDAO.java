package com.zte.cms.cpsp.dao;

import java.util.List;

import com.zte.cms.cppr.model.CmsCppr;
import com.zte.cms.cppr.model.CmsUcpBasic;
import com.zte.cms.cpsp.model.UcpBasicModel;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.cpsp.common.model.UcpBasic;
import org.apache.commons.lang.StringUtils;

public class CmsUcpBasicDAO extends DynamicObjectBaseDao implements ICmsUcpBasicDAO {
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public PageInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize) throws DAOException {
        log.debug("page query ucpBasic by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("pageQureyCmsUcpBasicCnt", ucpBasic)).intValue();
        if (totalCnt > 0) {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<UcpBasic> rsList = (List<UcpBasic>) super.pageQuery("pageQureyCmsUcpBasic",
                    ucpBasic, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        } else {
            pageInfo = new PageInfo();
        }
        log.debug("page query ucpBasic by condition end");
        return pageInfo;
    }

    public PageInfo pageInfoQuery(UcpBasic ucpBasic, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException {
        return super.indexPageQuery("pageQureyCmsUcpBasic", "pageQureyCmsUcpBasicCnt", ucpBasic, start, pageSize, puEntity);
    }

    @SuppressWarnings("unchecked")
    public List<CmsUcpBasic> getCmsUcpBasicListByServiceKey(CmsUcpBasic cmsUcpBasic) throws DAOException {
        log.debug("query ucpBasic by serviceKey starting...");
        List<CmsUcpBasic> rList = (List<CmsUcpBasic>) super.queryForList("queryCmsUcpBasicListByServiceKey", cmsUcpBasic);
        log.debug("query ucpBasic by serviceKey end");
        return rList;
    }

    public void deleteCmsOperPower(CmsCppr cmsCppr) throws DAOException {
        log.debug("deletec msCppr starting...");
        super.delete("deleteCmsOperPower", cmsCppr);
        log.debug("deletec msCppr end");
    }

    @Override
    public void insertUcpBasic(UcpBasicModel model) throws DAOException {
        this.log.debug("insert ucpBasic starting...");
        model.setDefaultValues();
        super.insert("insertCMSUcpBasicData", model);
        this.log.debug("insert ucpBasic end");
    }

    @Override
    public UcpBasicModel getUcpBasicByIndex(UcpBasicModel model) {
        return (UcpBasicModel) super.queryForObject("getUcpBasic_CMS", model);
    }

    @Override
    public void deleteUcpBasicFile(UcpBasicModel model) {
        super.update("updateUcpBasicByQualificationsurl_CMS", model);
    }

    public void updateUcpBasic(UcpBasicModel model) throws DAOException {
        super.update("updateUcpBasic_CMS", model);
    }

    @Override
    public List<UcpBasicModel> checkQualificationDoc(String operid) {
        return  (List<UcpBasicModel>) super.queryForList("selectQualificationDocByOperid", operid);
    }
}
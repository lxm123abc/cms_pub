package com.zte.cms.castrolemap.crmsync.service;

import java.util.List;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.cms.castrolemap.crmsync.dao.ICrmPlatformSyncDAO;
import com.zte.cms.castrolemap.crmsync.model.CrmPlatformSync;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CrmPlatformSyncDS implements ICrmPlatformSyncDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    
    private ICrmPlatformSyncDAO dao = null;
    
    public void setDao(ICrmPlatformSyncDAO dao)
    {
        this.dao = dao;
    }
    
    public TableDataInfo pageInfoQuery( CrmPlatformSync crmPlatformSync, int start, int pageSize)throws DomainServiceException
    {
        log.debug( "get crmPlatformSync page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(crmPlatformSync, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CastPlatformSync>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get crmPlatformSync page info by condition end" );
        return tableInfo;
    }
}

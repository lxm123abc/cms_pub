package com.zte.cms.content.series.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.DateUtil2;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.ObjectType;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.series.common.SeriesConfig;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.cms.content.series.service.ICmsSeriesDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.ctgsrmap.model.CategorySeriesMap;
import com.zte.cms.ctgsrmap.service.ICategorySeriesMapDS;
import com.zte.cms.picture.ls.IPictureLS;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.cms.picture.service.ISeriesPictureMapDS;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.cms.seriespromap.model.SeriesProgramMap;
import com.zte.cms.seriespromap.service.ISeriesProgramMapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

public class CmsSeriesLS extends DynamicObjectBaseDS implements ICmsSeriesLS
{

    private Log log = SSBBus.getLog(getClass());
    private ICmsSeriesDS cmSeriesDS;
    private ICntSyncTaskDS ntsyncTaskDS;
    private ICmsStorageareaLS cmsStorageareaLS;
    private IUsysConfigDS usysConfigDS;
    private IUcpBasicDS ucpBasicDS;
    private ICntSyncRecordDS cntSyncRecordDS;
    private ITargetsystemDS targetSystemDS;
    private ISeriesCrmMapDS seriesCrmMapDS;
    private ISeriesPictureMapDS seriesPictureMapDS;
    private ISeriesProgramMapDS seriesProgramMapDS;
    private ICategorySeriesMapDS categorySeriesMapDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private ICntTargetSyncDS cntTargetSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    public final static String SERIES_DIR = "series";
    public final static String CNTSYNCXML = "xmlsync";
    public static final String TEMPAREA_ID = "1"; // 临时区ID
    public static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    public static final String BATCHID = "ucdn_task_batch_id";// 任务批次号
    public static final String CORRELATEID = "ucdn_task_correlate_id";// 任务流水号
    public static final int SERIES_OBJECTTYPE = 4;
    // Action 类型
    public static final int TASKTYPE_REGIST = 1;
    public static final int TASKTYPE_UPDATE = 2;
    public static final int TASKTYPE_DELETE = 3;

    // 目标系统
    public static final int DESTTYPE_BMS = 1;
    public static final int DESTTYPE_EPG = 2;
    public static final int DESTTYPE_CDN = 3;

    // 预下线状态
    private static final int PRE_OFFLINE_STATUS = 65;

    private String convertDateTimeFromPage(String pageDateTime)
    {
        String convertedValue = pageDateTime;
        if (!convertedValue.equals(""))
        {
            String year = convertedValue.substring(0, 4);
            String month = convertedValue.substring(5, 7);
            String day = convertedValue.substring(8, 10);

            String hour = convertedValue.substring(11, 13);
            String minute = convertedValue.substring(14, 16);
            String second = convertedValue.substring(17, 19);
            convertedValue = year + month + day + hour + minute + second;

        }
        return convertedValue;
    }

    private String convertDateFromPage(String pageDate)
    {
        String convertedDate = pageDate;
        if (!convertedDate.equals(""))
        {
            String year = convertedDate.substring(0, 4);
            String month = convertedDate.substring(5, 7);
            String day = convertedDate.substring(8, 10);
            convertedDate = year + month + day;
        }
        return convertedDate;
    }

    public String updateSeriesForPreOff(long seriesindex) throws DomainServiceException
    {
        log.debug("updateSeriesForPreOff starting...");
        try
        {
            CmsSeries cmsSeries = new CmsSeries();
            cmsSeries.setSeriesindex(seriesindex);
            cmsSeries = cmSeriesDS.getCmsSeries(cmsSeries);
            if (cmsSeries == null || cmsSeries.getSeriesindex() == null || cmsSeries.getSeriesindex() < 1)
            {
                return "100";
            }

            SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
            seriesPictureMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS
                    .getSeriesPictureMapByCond(seriesPictureMap);
            if (seriesPictureMapList != null && seriesPictureMapList.size() > 0)
            {
                return "300";// 有海报不能退
            }

            SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
            seriesCrmMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<SeriesCrmMap> seriesCrmMapList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);
            if (seriesCrmMapList.size() > 0)
            {
                return "200";// 关联演员不能进行取消发布
            }

            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<SeriesProgramMap> seriesProMap = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
            if (seriesProMap != null && seriesProMap.size() > 0)
            {
                return "202";// 关联的单集不能取消发布
            }

            CategorySeriesMap categorySeriesMap = new CategorySeriesMap();
            categorySeriesMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<CategorySeriesMap> cgrSerMapList = categorySeriesMapDS.getCategorySeriesMapByCond(categorySeriesMap);

            if (cgrSerMapList != null && cgrSerMapList.size() > 0)
            {
                return "203";// 关联了栏目不能取消发布
            }

            int status = cmsSeries.getStatus();
            if (status == 20 || status == 50)
            {
                cmsSeries.setStatus(PRE_OFFLINE_STATUS);
                cmSeriesDS.updateCmsSeries(cmsSeries);
                CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid(), SeriesConfig.SERIES_MGT,
                        SeriesConfig.SERIES_PREOFFLINE, SeriesConfig.SERIES_PREOFFLINE_INFO,
                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                return "101";
            }
        }
        catch (DomainServiceException e)
        {
            log.error("updateSeriesForPreOff exception:" + e);
            throw e;
        }
        log.debug("updateSeriesForPreOff end");
        return "0";
    }

    public String insertCmsSeries(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("insertCmsSeries starting...");
        try
        {

            // CmsSeries cpcontentidSeries = new CmsSeries();
            //
            // cpcontentidSeries.setCpcontentid(cmsSeries.getCpcontentid());
            // List<CmsSeries> seriesList = cmSeriesDS
            // .getCmsSeriesByCond(cpcontentidSeries);
            // if (seriesList != null && seriesList.size() > 0) {
            // return "101";
            // }

            // 内容生效时间
            String effectivedate = cmsSeries.getEffectivedate();
            effectivedate = convertDateTimeFromPage(effectivedate);
            cmsSeries.setEffectivedate(effectivedate);

            // 内容失效时间
            String expirydate = cmsSeries.getExpirydate();
            expirydate = convertDateTimeFromPage(expirydate);
            cmsSeries.setExpirydate(expirydate);

            // 牌照方自定义内容下线时间
            String llicensofflinetime = cmsSeries.getLlicensofflinetime();
            llicensofflinetime = convertDateTimeFromPage(llicensofflinetime);
            cmsSeries.setLlicensofflinetime(llicensofflinetime);

            // 内容删除时间
            String deletetime = cmsSeries.getDeletetime();
            deletetime = convertDateTimeFromPage(deletetime);
            cmsSeries.setDeletetime(deletetime);

            // 首播日期
            String orgairdate = cmsSeries.getOrgairdate();
            orgairdate = convertDateFromPage(orgairdate);
            cmsSeries.setOrgairdate(orgairdate);

            // 有效开始时间
            String licensingstart = cmsSeries.getLicensingstart();
            licensingstart = convertDateTimeFromPage(licensingstart);
            cmsSeries.setLicensingstart(licensingstart);

            // 有效结束时间
            String licensingend = cmsSeries.getLicensingend();
            licensingend = convertDateTimeFromPage(licensingend);
            cmsSeries.setLicensingend(licensingend);

            // TODO 内容提交者
            // TODO 内容创建操作员名称

            // TODO 判断CP是否删除
            // TODO 判断牌照方是否删除
            // TODO 判断名称是否一致
            cmsSeries.setStatus(0);
            String cpid = cmsSeries.getCpid();
            String licenseid = cmsSeries.getLicenseid();
            if ((null != cpid) && (!"".equals(cpid)))
            { // 插入连续剧的时候判断cp有没有被删除，如果被删除则不能执行插入操作
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(cpid);
                ucpBasic.setCptype(1);
                List list = ucpBasicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() != 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == 4)
                    {
                        return "102"; // 表示cp已被删除
                    }
                    if (status == 2)
                    {
                        return "103"; // 表示cp已被删除
                    }
                }
            }
            // 判断牌照方是否存在
            if ((null != licenseid) && (!"".equals(licenseid)))
            { // 插入连续剧的时候判断牌照方有没有被删除，如果被删除则不能执行插入操作
                UcpBasic ucpBasic = new UcpBasic();
                ucpBasic.setCpid(licenseid);
                ucpBasic.setCptype(2);
                List list = ucpBasicDS.getUcpBasicExactByCond(ucpBasic);
                if (null != list && list.size() != 0)
                {
                    ucpBasic = (UcpBasic) list.get(0);
                    int status = ucpBasic.getStatus();
                    if (status == 4)
                    {
                        return "104"; // 表示牌照方被删除
                    }
                    if (status == 2)
                    {
                        return "105"; // 表示cp已被删除
                    }
                }
            }
            cmSeriesDS.insertCmsSeries(cmsSeries);

            CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid(), SeriesConfig.SERIES_MGT, SeriesConfig.SERIES_ADD,
                    SeriesConfig.SERIES_ADD_INFO, SeriesConfig.RESOURCE_OPERATION_SUCCESS);

            // TODO 插入操作日志
            // throw new DomainServiceException();

        }
        catch (Exception e)
        {
            log.error("insertCmsSeries exception :" + e);
            throw new DomainServiceException(e);
        }
        log.debug("insertCmsSeries end");
        return "0";
    }

    public TableDataInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("pageInfoQueryForPage starting...");
        TableDataInfo info = null;
        try
        {
            String namecn = cmsSeries.getNamecn();
            namecn = EspecialCharMgt.conversion(namecn);
            cmsSeries.setNamecn(namecn);
            
            String cpid = cmsSeries.getCpid();
            cpid = EspecialCharMgt.conversion(cpid);
            cmsSeries.setCpid(cpid);
            
            String cpcnshortname = cmsSeries.getCpcnshortname();
            cpcnshortname = EspecialCharMgt.conversion(cpcnshortname);
            cmsSeries.setCpcnshortname(cpcnshortname);

            String seriesid = cmsSeries.getSeriesid();
            seriesid=EspecialCharMgt.conversion(seriesid);
            cmsSeries.setSeriesid(seriesid);

            String createStartDate = convertDateTimeFromPage(cmsSeries.getCreateStarttime());
            cmsSeries.setCreateStarttime(createStartDate);

            String createEndDate = convertDateTimeFromPage(cmsSeries.getCreateEndtime());
            cmsSeries.setCreateEndtime(createEndDate);
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsSeries.setOperid(operinfo.getOperid());
            info = cmSeriesDS.pageInfoQueryForPage(cmsSeries, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQueryForPage exception:" + e);
            throw e;
        }
        log.debug("pageInfoQueryForPage end");
        return info;
    }
    
    public TableDataInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize)
    throws DomainServiceException
    {
        log.debug("pageInfoQueryPlatformSync starting...");
        TableDataInfo info = null;
        try
        {
            String namecn = cmsSeries.getNamecn();
            namecn = EspecialCharMgt.conversion(namecn);
            cmsSeries.setNamecn(namecn);
            
            String cpid = cmsSeries.getCpid();
            cpid = EspecialCharMgt.conversion(cpid);
            cmsSeries.setCpid(cpid);
            
            String cpcnshortname = cmsSeries.getCpcnshortname();
            cpcnshortname = EspecialCharMgt.conversion(cpcnshortname);
            cmsSeries.setCpcnshortname(cpcnshortname);

            String seriesid = cmsSeries.getSeriesid();
            seriesid=EspecialCharMgt.conversion(seriesid);
            cmsSeries.setSeriesid(seriesid);

            String createStartDate = convertDateTimeFromPage(cmsSeries.getCreateStarttime());
            cmsSeries.setCreateStarttime(createStartDate);

            String createEndDate = convertDateTimeFromPage(cmsSeries.getCreateEndtime());
            cmsSeries.setCreateEndtime(createEndDate);
            
            String publishStarttime = convertDateTimeFromPage(cmsSeries.getPublishStarttime());
            cmsSeries.setPublishStarttime(publishStarttime);

            String publishEndtime = convertDateTimeFromPage(cmsSeries.getPublishEndtime());
            cmsSeries.setPublishEndtime(publishEndtime);
            
            String cancelpubStarttime = convertDateTimeFromPage(cmsSeries.getCancelpubStarttime());
            cmsSeries.setCancelpubStarttime(cancelpubStarttime);

            String cancelpubEndtime = convertDateTimeFromPage(cmsSeries.getCancelpubEndtime());
            cmsSeries.setCancelpubEndtime(cancelpubEndtime);
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsSeries.setOperid(operinfo.getOperid());
            info = cmSeriesDS.pageInfoQueryPlatformSync(cmsSeries, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQueryPlatformSync exception:" + e);
            throw e;
        }
        log.debug("pageInfoQueryPlatformSync end");
        return info;
    }

    public TableDataInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize)
    throws DomainServiceException
    {
        log.debug("pageInfoQueryTargetSync starting...");
        TableDataInfo info = null;
        try
        {
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsSeries.setOperid(operinfo.getOperid());
            info = cmSeriesDS.pageInfoQueryTargetSync(cmsSeries, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfoQueryTargetSync exception:" + e);
            throw e;
        }
        log.debug("pageInfoQueryTargetSync end");
        return info;
    }
    
    public CmsSeries getCmsSeriesByIndex(Long seriesindex) throws DomainServiceException
    {
        log.debug("getCmsSeriesByIndex starting...");
        CmsSeries cmsSeries = new CmsSeries();
        try
        {
            cmsSeries.setSeriesindex(seriesindex);
            cmsSeries = cmSeriesDS.getCmsSeries(cmsSeries);
        }
        catch (DomainServiceException e)
        {
            log.error("getCmsSeriesByIndex exception:" + e);
            throw e;
        }
        log.debug("getCmsSeriesByIndex end");
        return cmsSeries;
    }

    /**
     * 修改连续剧内容
     * 
     * @param cmsSeries 连续剧内容对象
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String updateCmsSeries(CmsSeries cmsSeries) throws Exception
    {
        log.debug("updateCmsSeries starting...");
        try
        {
            CmsSeries indexSeries = new CmsSeries();
            indexSeries.setSeriesindex(cmsSeries.getSeriesindex());
            indexSeries = cmSeriesDS.getCmsSeries(indexSeries);
            if (indexSeries == null || indexSeries.getSeriesid() == null || indexSeries.getSeriesid().equals(""))
            {
                return "100";
            }
            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setSeriesindex(cmsSeries.getSeriesindex());
            int maxSequnce = seriesProgramMapDS.getMaxSequenceBySeriesindex(seriesProgramMap);
            if (maxSequnce > 0)
            {
                if ((cmsSeries.getVolumncount() == null) || (cmsSeries.getVolumncount() < maxSequnce))
                {
                    return "101";// 总集数不能小于其打包单集最大集序号
                }
            }

            String effectivedate = cmsSeries.getEffectivedate();
            effectivedate = convertDateTimeFromPage(effectivedate);
            cmsSeries.setEffectivedate(effectivedate);

            // 内容失效时间
            String expirydate = cmsSeries.getExpirydate();
            expirydate = convertDateTimeFromPage(expirydate);
            cmsSeries.setExpirydate(expirydate);

            // 牌照方自定义内容下线时间
            String llicensofflinetime = cmsSeries.getLlicensofflinetime();
            llicensofflinetime = convertDateTimeFromPage(llicensofflinetime);
            cmsSeries.setLlicensofflinetime(llicensofflinetime);

            // 内容删除时间
            String deletetime = cmsSeries.getDeletetime();
            deletetime = convertDateTimeFromPage(deletetime);
            cmsSeries.setDeletetime(deletetime);

            // 首播日期
            String orgairdate = cmsSeries.getOrgairdate();
            orgairdate = convertDateFromPage(orgairdate);
            cmsSeries.setOrgairdate(orgairdate);

            // 有效开始时间
            String licensingstart = cmsSeries.getLicensingstart();
            licensingstart = convertDateTimeFromPage(licensingstart);
            cmsSeries.setLicensingstart(licensingstart);

            // 有效结束时间
            String licensingend = cmsSeries.getLicensingend();
            licensingend = convertDateTimeFromPage(licensingend);
            cmsSeries.setLicensingend(licensingend);

            String lastUpdateDateTime = getCurrentDateTime();
            cmsSeries.setLastupdatetime(lastUpdateDateTime);
            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
            cntTargetSync.setObjectid(indexSeries.getSeriesid());
            cntTargetSync.setObjectindex(indexSeries.getSeriesindex());
            List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            
            if (null == cntTargetSyncList || 0 == cntTargetSyncList.size())
            {
                cmSeriesDS.updateCmsSeries(cmsSeries);
                CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid(), SeriesConfig.SERIES_MGT,
                        SeriesConfig.SERIES_MODIFY, SeriesConfig.SERIES_MODIFY_INFO,
                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);

            }
            else 
            {
                for (CntTargetSync seriesSynStatus:cntTargetSyncList)
                {
                    if (seriesSynStatus.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                    {
                        return "102";
                    }
                }
                
                Long batchid = null;
                int targettype = findFirTargettype(cntTargetSyncList,1);
                if(targettype != 99)                             
                    batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                
                cmSeriesDS.updateCmsSeries(cmsSeries);//先更新数据，保证如果做修改发布的话，修改发布的是最新的数据。
                for (CntTargetSync seriesTargetSyn:cntTargetSyncList)
                {
                    if (seriesTargetSyn.getStatus() == StatusTranslator.STATUS_PUBLISHED || seriesTargetSyn.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                    {
                        settargettype(seriesTargetSyn,targettype,"platfrom");
                        seriesTargetSyn.setReserve01(batchid);
                        updatePublishSerie2Target(seriesTargetSyn);
                    }
                }
                
                CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid(), SeriesConfig.SERIES_MGT,
                        SeriesConfig.SERIES_MODIFY, SeriesConfig.SERIES_MODIFY_INFO,
                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
            }

        }
        catch (Exception e)
        {
            log.error("updateCmsSeries exception:" + e);
            throw e;
        }

        log.debug("updateCmsSeries end");

        return "0";
    }

    /**
     * 根据Index删除连续剧内容
     * 
     * @param seriesindex 连续剧内容索引值得
     * @return 操作结果
     * @throws DomainServiceException
     */
    public String removeCmsSeries(Long seriesindex) throws DomainServiceException
    {
        log.debug("removeCmsSeries starting...");
        try
        {
            CmsSeries cmsSeries = new CmsSeries();
            cmsSeries.setSeriesindex(seriesindex);
            cmsSeries = cmSeriesDS.getCmsSeries(cmsSeries);
            if (cmsSeries == null || cmsSeries.getSeriesid() == null || cmsSeries.getSeriesid().equals(""))
            {
                return "100";
            }
            
            CntTargetSync targetSyncCond  = new CntTargetSync();
            targetSyncCond.setObjectid(cmsSeries.getSeriesid());
            targetSyncCond.setObjecttype(ObjectType.SERIES_TYPE);
            List<CntTargetSync> targetSynList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyncCond);
            if (null != targetSynList && targetSynList.size() > 0)
            {
                for (CntTargetSync seriesTargetSync:targetSynList )
                {
                    if (seriesTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING || seriesTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || seriesTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                    {
                        return "101";//连续已发布不能进行删除
                    }
                }
                
                for (CntTargetSync seriesTargetSync:targetSynList )
                {
                    Targetsystem targetsystem = new Targetsystem();
                    targetsystem.setTargetindex(seriesTargetSync.getTargetindex());
                    removeSeriesTargetsysBind(targetsystem, cmsSeries);
                }
            }
            
            //删除连续剧的海报及其关联
            SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
            seriesPictureMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);
            List<String> picIndexList = new ArrayList();
            for (SeriesPictureMap seriesPicMap:seriesPictureMapList )
            {
                picIndexList.add(seriesPicMap.getPictureindex().toString());
            }
            IPictureLS pictureLS = (IPictureLS) SSBBus.findDomainService("pictureLS");
            try
            {
                pictureLS.batchRemovePicMap(picIndexList, ObjectType.SERIES_TYPE, cmsSeries.getSeriesindex());
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            //删除连续剧与角色的关联
            SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
            seriesCrmMap.setSeriesindex(cmsSeries.getSeriesindex());
            seriesCrmMapDS.removeSeriesCrmMapByCond(seriesCrmMap);
            
            //删除连续剧与单集的关联
            SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
            seriesProgramMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<SeriesProgramMap> seriesProMap = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);
            seriesProgramMapDS.removeSeriesProgramMapList(seriesProMap);
            
            //删除连续剧与栏目的关联
            CategorySeriesMap categorySeriesMap = new CategorySeriesMap();
            categorySeriesMap.setSeriesindex(cmsSeries.getSeriesindex());
            List<CategorySeriesMap> cgrSerMapList = categorySeriesMapDS.getCategorySeriesMapByCond(categorySeriesMap);
            categorySeriesMapDS.removeCategorySeriesMapList(cgrSerMapList);
            
            cmSeriesDS.removeCmsSeries(cmsSeries);
            CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid(), SeriesConfig.SERIES_MGT,
                    SeriesConfig.SERIES_DELETE, SeriesConfig.SERIES_DELETE_INFO,
                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
        }
        catch (DomainServiceException e)
        {
            log.error("removeCmsSeries exception:" + e);
            throw e;
        }
        log.debug("removeCmsSeries end");
        return "0";
    }

    public String cancelPublishSeries(CmsSeries series) throws Exception
    {
        log.debug("cancelPublishSeries starting...");
        
        String result = "1:" +ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL);
       
        try
        {

            CntPlatformSync seriesPlatformSync = new CntPlatformSync();
            seriesPlatformSync.setObjectid(series.getSeriesid());
            seriesPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
            seriesPlatformSync.setPlatform(series.getPlatform());
            List<CntPlatformSync> seriesPlatformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(seriesPlatformSync);
            if ((seriesPlatformSyncList == null) || seriesPlatformSyncList.size() <= 0)
            {
                result = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_DATA_NOT_EXIST);
                
            }
            int status = seriesPlatformSyncList.get(0).getStatus();
            if (status != StatusTranslator.STATUS_PUBLISHED && status != StatusTranslator.STATUS_MODIFYPUBFAILED)
            {
                result = "1:" +ResourceMgt.findDefaultText(SeriesConfig.SERIES_STATUS_NON_PUBLISHED);
            }
            else 
            {//发布状态为待发布、发布失败、修改发布失败                        
                CntTargetSync seriesTargetSync = new CntTargetSync();
                seriesTargetSync.setRelateindex(seriesPlatformSyncList.get(0).getSyncindex());
                seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
                List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
                boolean isAllTargetSuccess = false;
                StringBuffer allresult = new StringBuffer();
                if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
                {
                    
                    String cancelstr = getEPGCanCancelPub(seriesTargetSyncList);
                    if(cancelstr.substring(0,1).equals("1")){
                        return cancelstr;
                    }
                    
                    Long batchid = null;
                    int targettype = findFirTargettype(seriesTargetSyncList,2);
                    if(targettype != 99)  
                        batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")); 
                    
                    for (CntTargetSync targetSync : seriesTargetSyncList)
                    {   
                        Targetsystem target = new Targetsystem();
                        target.setTargetindex(targetSync.getTargetindex());
                        target = targetSystemDS.getTargetsystem(target);
                        
                        if (targetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                        {
                            CntTargetSync mappingTargetSync = new CntTargetSync();                             
                            mappingTargetSync.setTargetindex(targetSync.getTargetindex());
                            mappingTargetSync.setElementid(series.getSeriesid());
                            mappingTargetSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
                            List<CntTargetSync> seriesCategoryTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                            if (null != seriesCategoryTargetSyncList && seriesCategoryTargetSyncList.size() > 0)
                            {
                                boolean isSeriesCategoryPulished = false;
                                for(CntTargetSync scTargetSync:seriesCategoryTargetSyncList )
                                {
                                    if (scTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                    {
                                        isSeriesCategoryPulished = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与栏目"+scTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                        break;
                                    }
                                }
                                if (isSeriesCategoryPulished)
                                {
                                    continue;
                                }
                            }
                            
                            mappingTargetSync.setParentid(series.getSeriesid());
                            mappingTargetSync.setElementid(null);
                            mappingTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                            List<CntTargetSync> seriesCrmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                            if (null != seriesCrmTargetSyncList && seriesCrmTargetSyncList.size() > 0)
                            {
                                boolean isseriesCrmPulished = false;
                                for(CntTargetSync scrmTargetSync:seriesCrmTargetSyncList )
                                {
                                    if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                    {
                                        isseriesCrmPulished = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与角色"+scrmTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧");
                                        break;
                                    }
                                }
                                if (isseriesCrmPulished)
                                {
                                    continue;
                                }
                            }
                            
                            mappingTargetSync.setParentid(series.getSeriesid());
                            mappingTargetSync.setElementid(null);
                            mappingTargetSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                            List<CntTargetSync> seriesProgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                            if (null != seriesProgTargetSyncList && seriesProgTargetSyncList.size() > 0)
                            {
                                boolean isSeriesProgPulished = false;
                                for(CntTargetSync spTargetSync:seriesProgTargetSyncList )
                                {
                                    if (spTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                    {
                                        isSeriesProgPulished = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与单集"+spTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧");
                                        break;
                                    }
                                }
                                if (isSeriesProgPulished)
                                {
                                    continue;
                                }
                            }
                            
                            mappingTargetSync.setParentid(null);
                            mappingTargetSync.setElementid(series.getSeriesid());
                            mappingTargetSync.setObjecttype(ObjectType.SERIESPICTUREMAP_TYPE);                                
                            List<CntTargetSync> seriesPIcTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                            if (null != seriesPIcTargetSyncList && seriesPIcTargetSyncList.size() > 0)
                            {
                                boolean isSeriesPicPulished = false;
                                for(CntTargetSync sPicTargetSync:seriesPIcTargetSyncList )
                                {
                                    if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                    {
                                        isSeriesPicPulished = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与海报"+sPicTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                        break;
                                    }
                                }
                                if (isSeriesPicPulished)
                                {
                                    continue;
                                }
                            }
                            
                            mappingTargetSync.setParentid(null);
                            mappingTargetSync.setElementid(series.getSeriesid());
                            mappingTargetSync.setObjecttype(ObjectType.SERVICESERIESMAP_TYPE);                                
                            List<CntTargetSync> seriesServiceTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                            if (null != seriesServiceTargetSyncList && seriesServiceTargetSyncList.size() > 0)
                            {
                                boolean isSeriesServicePulished = false;
                                for(CntTargetSync ssTargetSync:seriesServiceTargetSyncList )
                                {
                                    if (ssTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                    {
                                        isSeriesServicePulished = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与服务"+ssTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                        break;
                                    }
                                }
                                if (isSeriesServicePulished)
                                {
                                    continue;
                                }
                            }
                            
                            settargettype(targetSync,targettype,"platfrom");
                            targetSync.setReserve01(batchid);
                            
                            String rtnSynResult= this.cancelPublishSerie2Target(targetSync);
                            if (rtnSynResult.substring(0, 1).equals("0"))
                            {
                                isAllTargetSuccess = true;
                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:"+ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS));
                            }else
                            {
                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布"+rtnSynResult.substring(1));
                            }
                            
                        }else
                        {
                            allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:连续剧未发布,不能取消发布");
                        }
                    }
                }else
                {
                    allresult.append("发布数据已不存在");
                }
                
                if (isAllTargetSuccess)
                {
                    CommonLogUtil.insertOperatorLog(series.getSeriesid(), SeriesConfig.SERIES_MGT,
                            SeriesConfig.SERIES_CANCEL_PUBLISH, SeriesConfig.SERIES_CANCEL_PUBLISH_INFO,
                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                    result = "0:" + allresult.toString();
                }else
                {
                    
                    result = "1:" +allresult.toString();
                }
            }
        }
        catch (Exception e)
        {
            log.error("cancelPublishSeries exception:" + e);
            throw e;
        }
        
        log.debug("cancelPublishSeries end");
        
        return result;
    }
    
    private String getEPGCanCancelPub(List<CntTargetSync> seriesTargetSyncList) throws Exception
    {
        for (CntTargetSync targetSync : seriesTargetSyncList)
        {   
            Targetsystem target = new Targetsystem();
            target.setTargetindex(targetSync.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            //只有存在EPG网元且能够取消发布的时候才去判断是否有mapping已发布
            if (target.getTargettype() == 2)
            {
                if (targetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                {
                    CntTargetSync mappingTargetSync = new CntTargetSync();                             
                    mappingTargetSync.setTargetindex(targetSync.getTargetindex());
                    mappingTargetSync.setElementid(targetSync.getObjectid());
                    mappingTargetSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
                    List<CntTargetSync> seriesCategoryTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                    if (null != seriesCategoryTargetSyncList && seriesCategoryTargetSyncList.size() > 0)
                    {
                        for(CntTargetSync scTargetSync:seriesCategoryTargetSyncList )
                        {
                            if (scTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与栏目"+scTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";
                            }
                            if (scTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与栏目"+scTargetSync.getParentid() +"的关联发布中,不允许取消发布连续剧";
                            }
                            if (scTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与栏目"+scTargetSync.getParentid() +"的关联修改发布失败,不允许取消发布连续剧";
                            }
                        }
                    }
                    
                    mappingTargetSync.setParentid(targetSync.getObjectid());
                    mappingTargetSync.setElementid(null);
                    mappingTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                    List<CntTargetSync> seriesCrmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                    if (null != seriesCrmTargetSyncList && seriesCrmTargetSyncList.size() > 0)
                    {
                        for(CntTargetSync scrmTargetSync:seriesCrmTargetSyncList )
                        {
                            if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与角色"+scrmTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧";
                            }
                            if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与角色"+scrmTargetSync.getParentid() +"的关联发布中,不允许取消发布连续剧";
                            }
                            if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与角色"+scrmTargetSync.getParentid() +"的关联修改发布失败,不允许取消发布连续剧";
                            }
                        }
                    }
                    
                    mappingTargetSync.setParentid(targetSync.getObjectid());
                    mappingTargetSync.setElementid(null);
                    mappingTargetSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                    List<CntTargetSync> seriesProgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                    if (null != seriesProgTargetSyncList && seriesProgTargetSyncList.size() > 0)
                    {
                        for(CntTargetSync spTargetSync:seriesProgTargetSyncList )
                        {
                            if (spTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与单集"+spTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧";
                            }
                            if (spTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与单集"+spTargetSync.getParentid() +"的关联发布中,不允许取消发布连续剧";
                            }
                            if (spTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与单集"+spTargetSync.getParentid() +"的关联修改发布失败,不允许取消发布连续剧";
                            }
                        }
                    }
                    
                    mappingTargetSync.setParentid(null);
                    mappingTargetSync.setElementid(targetSync.getObjectid());
                    mappingTargetSync.setObjecttype(ObjectType.SERIESPICTUREMAP_TYPE);                                
                    List<CntTargetSync> seriesPIcTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                    if (null != seriesPIcTargetSyncList && seriesPIcTargetSyncList.size() > 0)
                    {
                        for(CntTargetSync sPicTargetSync:seriesPIcTargetSyncList )
                        {
                            if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与海报"+sPicTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";
                            }
                            if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与海报"+sPicTargetSync.getParentid() +"的关联发布中,不允许取消发布连续剧";
                            }
                            if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与海报"+sPicTargetSync.getParentid() +"的关联修改发布失败,不允许取消发布连续剧";
                            }
                        }
                    }
                    
                    mappingTargetSync.setParentid(null);
                    mappingTargetSync.setElementid(targetSync.getObjectid());
                    mappingTargetSync.setObjecttype(ObjectType.SERVICESERIESMAP_TYPE);                                
                    List<CntTargetSync> seriesServiceTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                    if (null != seriesServiceTargetSyncList && seriesServiceTargetSyncList.size() > 0)
                    {
                        for(CntTargetSync ssTargetSync:seriesServiceTargetSyncList )
                        {
                            if (ssTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与服务"+ssTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";
                            }
                            if (ssTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHING)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与服务"+ssTargetSync.getParentid() +"的关联发布中,不允许取消发布连续剧";
                            }
                            if (ssTargetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                            {
                                return "1:从目标系统"+target.getTargetid()+"取消发布:与服务"+ssTargetSync.getParentid() +"的关联修改发布失败,不允许取消发布连续剧";
                            }
                        }
                    }
                }
            }
        }
        
        return "0";
    }

    public String publishCmsSeries(CmsSeries series) throws Exception
    {
        log.debug("publishCmsSeries starting...");
        
        String result = "1:" +ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL);
        try
        {

            CntPlatformSync seriesPlatformSync = new CntPlatformSync();
            seriesPlatformSync.setObjectid(series.getSeriesid());
            seriesPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
            seriesPlatformSync.setPlatform(series.getPlatform());
            List<CntPlatformSync> seriesPlatformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(seriesPlatformSync);
            if ((seriesPlatformSyncList == null) || seriesPlatformSyncList.size() <= 0)
            {
                result = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_DATA_NOT_EXIST);
                
            }
            int status = seriesPlatformSyncList.get(0).getStatus();
            if (status != StatusTranslator.STATUS_TOPUBLISH && status != StatusTranslator.STATUS_PUBFAILED && status != StatusTranslator.STATUS_MODIFYPUBFAILED)
            {
                result = "1:" +ResourceMgt.findDefaultText(SeriesConfig.SERIES_STATUS_PUBLISHED);
            }
            else 
            {//发布状态为待发布、发布失败、修改发布失败                        
                CntTargetSync seriesTargetSync = new CntTargetSync();
                seriesTargetSync.setRelateindex(seriesPlatformSyncList.get(0).getSyncindex());
                seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
                List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
                boolean isAllTargetSuccess = false;
                StringBuffer allresult = new StringBuffer();
                if (null != seriesTargetSyncList&&seriesTargetSyncList.size() > 0)
                {
                    Long batchid = null;
                    int targettype = findFirTargettype(seriesTargetSyncList,0);
                    if(targettype != 99)  
                        batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                    
                    for (CntTargetSync targetSync : seriesTargetSyncList)
                    {
                        Targetsystem target = new Targetsystem();
                        target.setTargetindex(targetSync.getTargetindex());
                        target = targetSystemDS.getTargetsystem(target);
                        
                        if (targetSync.getStatus() == StatusTranslator.STATUS_TOPUBLISH || targetSync.getStatus() == StatusTranslator.STATUS_PUBFAILED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                        {
                            settargettype(targetSync,targettype,"platfrom");
                            targetSync.setReserve01(batchid);
                            String rtnSynResult= this.publishSerie2Target(targetSync);
                            if (rtnSynResult.substring(0, 1).equals("0"))
                            {
                                isAllTargetSuccess = true;
                                allresult.append(" 发布到目标系统"+target.getTargetid()+":"+ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS));
                            }else
                            {
                                allresult.append(" 发布到目标系统"+target.getTargetid()+rtnSynResult.substring(1));
                            }
                            
                        }else
                        {
                            allresult.append(" 发布到目标系统"+target.getTargetid()+":连续剧已发布,不能再发布");
                        }
                    }
                }else
                {
                    allresult.append("发布数据已不存在");
                }
                
                if (isAllTargetSuccess)
                {
                    CommonLogUtil.insertOperatorLog(series.getSeriesid(), SeriesConfig.SERIES_MGT,
                            SeriesConfig.SERIES_PUBLISH, SeriesConfig.SERIES_PUBLISH_INFO,
                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                    result = "0:" + allresult.toString();
                }else
                {
                    
                    result = "1:" +allresult.toString();
                }
            }
        }
        catch (Exception e)
        {
            log.error("publishCmsSeries exception:" + e);
            throw e;
        }
        
        log.debug("publishCmsSeries end");
        
        return result;
    }

    /**
     * 批量取消发布连续剧内容
     * 
     * @param indexLists 连续剧内容id列表
     * @return 批量操作结果提示
     * @throws Exception
     */
    public String batchCancelPublishSeries(List<CmsSeries> seriesList) throws Exception
    {
        log.debug("batchCancelPublishSeries starting...");
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int success = 0;
        int fail = 0;
        int index = 1;
        int exception = 0;
        try
        {
            for (CmsSeries series : seriesList)
            {
                try
                {
                    CntPlatformSync seriesPlatformSync = new CntPlatformSync();
                    seriesPlatformSync.setObjectid(series.getSeriesid());
                    seriesPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
                    seriesPlatformSync.setPlatform(series.getPlatform());
                    List<CntPlatformSync> seriesPlatformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(seriesPlatformSync);
                    if ((seriesPlatformSyncList == null) || seriesPlatformSyncList.size() <= 0)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), ResourceMgt.findDefaultText(SeriesConfig.SERIES_DATA_NOT_EXIST));
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    int status = seriesPlatformSyncList.get(0).getStatus();
                    if (status != StatusTranslator.STATUS_PUBLISHED && status != StatusTranslator.STATUS_MODIFYPUBFAILED)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), ResourceMgt.findDefaultText(SeriesConfig.SERIES_STATUS_NON_PUBLISHED));
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    else 
                    {//发布状态为发布成功、修改发布失败                        
                        CntTargetSync seriesTargetSync = new CntTargetSync();
                        seriesTargetSync.setRelateindex(seriesPlatformSyncList.get(0).getSyncindex());
                        seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
                        List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
                        boolean isAllTargetSuccess = false;
                        StringBuffer allresult = new StringBuffer();
                        if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
                        {
                            
                            String cancelstr = getEPGCanCancelPub(seriesTargetSyncList);
                            if(cancelstr.substring(0,1).equals("1")){
                                cancelstr = cancelstr.substring(2);
                                operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), cancelstr);
                                returnInfo.appendOperateInfo(operateInfo);
                                fail++;
                                continue;
                            }
                            
                            Long batchid = null;
                            int targettype = findFirTargettype(seriesTargetSyncList,2);
                            if(targettype != 99)  
                                batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                            
                            for (CntTargetSync targetSync : seriesTargetSyncList)
                            {
                                Targetsystem target = new Targetsystem();
                                target.setTargetindex(targetSync.getTargetindex());
                                target = targetSystemDS.getTargetsystem(target);
                                if (targetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                                {   
                                    
                                    
                                    CntTargetSync mappingTargetSync = new CntTargetSync();                             
                                    mappingTargetSync.setTargetindex(targetSync.getTargetindex());
                                    mappingTargetSync.setElementid(series.getSeriesid());
                                    mappingTargetSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
                                    List<CntTargetSync> seriesCategoryTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                                    if (null != seriesCategoryTargetSyncList && seriesCategoryTargetSyncList.size() > 0)
                                    {
                                        boolean isSeriesCategoryPulished = false;
                                        for(CntTargetSync scTargetSync:seriesCategoryTargetSyncList )
                                        {
                                            if (scTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                            {
                                                isSeriesCategoryPulished = true;
                                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与栏目"+scTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                                break;
                                            }
                                        }
                                        if (isSeriesCategoryPulished)
                                        {
                                            continue;
                                        }
                                    }
                                    
                                    mappingTargetSync.setParentid(series.getSeriesid());
                                    mappingTargetSync.setElementid(null);
                                    mappingTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                                    List<CntTargetSync> seriesCrmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                                    if (null != seriesCrmTargetSyncList && seriesCrmTargetSyncList.size() > 0)
                                    {
                                        boolean isseriesCrmPulished = false;
                                        for(CntTargetSync scrmTargetSync:seriesCrmTargetSyncList )
                                        {
                                            if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                            {
                                                isseriesCrmPulished = true;
                                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与角色"+scrmTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧");
                                                break;
                                            }
                                        }
                                        if (isseriesCrmPulished)
                                        {
                                            continue;
                                        }
                                    }
                                    
                                    mappingTargetSync.setParentid(series.getSeriesid());
                                    mappingTargetSync.setElementid(null);
                                    mappingTargetSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                                    List<CntTargetSync> seriesProgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                                    if (null != seriesProgTargetSyncList && seriesProgTargetSyncList.size() > 0)
                                    {
                                        boolean isSeriesProgPulished = false;
                                        for(CntTargetSync spTargetSync:seriesProgTargetSyncList )
                                        {
                                            if (spTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                            {
                                                isSeriesProgPulished = true;
                                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与单集"+spTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧");
                                                break;
                                            }
                                        }
                                        if (isSeriesProgPulished)
                                        {
                                            continue;
                                        }
                                    }
                                    
                                    mappingTargetSync.setParentid(null);
                                    mappingTargetSync.setElementid(series.getSeriesid());
                                    mappingTargetSync.setObjecttype(ObjectType.SERIESPICTUREMAP_TYPE);                                
                                    List<CntTargetSync> seriesPIcTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                                    if (null != seriesPIcTargetSyncList && seriesPIcTargetSyncList.size() > 0)
                                    {
                                        boolean isSeriesPicPulished = false;
                                        for(CntTargetSync sPicTargetSync:seriesPIcTargetSyncList )
                                        {
                                            if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                            {
                                                isSeriesPicPulished = true;
                                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与海报"+sPicTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                                break;
                                            }
                                        }
                                        if (isSeriesPicPulished)
                                        {
                                            continue;
                                        }
                                    }
                                    
                                    mappingTargetSync.setParentid(null);
                                    mappingTargetSync.setElementid(series.getSeriesid());
                                    mappingTargetSync.setObjecttype(ObjectType.SERVICESERIESMAP_TYPE);                                
                                    List<CntTargetSync> seriesServiceTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
                                    if (null != seriesServiceTargetSyncList && seriesServiceTargetSyncList.size() > 0)
                                    {
                                        boolean isSeriesServicePulished = false;
                                        for(CntTargetSync ssTargetSync:seriesServiceTargetSyncList )
                                        {
                                            if (ssTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                                            {
                                                isSeriesServicePulished = true;
                                                allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:与服务"+ssTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧");
                                                break;
                                            }
                                        }
                                        if (isSeriesServicePulished)
                                        {
                                            continue;
                                        }
                                    }
                                    
                                    settargettype(targetSync,targettype,"platfrom");
                                    targetSync.setReserve01(batchid);
                                    
                                    String result= this.cancelPublishSerie2Target(targetSync);
                                    if (result.substring(0, 1).equals("0"))
                                    {
                                        isAllTargetSuccess = true;
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:"+ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS));
                                    }else
                                    {
                                        allresult.append(" 从目标系统"+target.getTargetid()+"取消发布"+result.substring(1));
                                    }
                                }
                                else
                                {
                                    allresult.append(" 从目标系统"+target.getTargetid()+"取消发布:连续剧未发布,不能取消发布");
                                }
                            }
                        }
                        else
                        {
                            allresult.append("发布数据已不存在");
                        }
                        if (isAllTargetSuccess)
                        {
                            operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS), allresult.toString());
                            returnInfo.appendOperateInfo(operateInfo);
                            success++;
                        }else
                        {
                            operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), allresult.toString());
                            returnInfo.appendOperateInfo(operateInfo);
                            fail++;
                        }
                    }
                    
                    CommonLogUtil.insertOperatorLog(series.getSeriesid(), SeriesConfig.SERIES_MGT,
                                SeriesConfig.SERIES_CANCEL_PUBLISH, SeriesConfig.SERIES_CANCEL_PUBLISH_INFO,
                                SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                }
                catch (Exception e)
                {//"发布操作失败", "操作数据库异常"
                    log.error("batchCancelPublishSeries operate db exception:" + e);
                    operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS), ResourceMgt.findDefaultText(SeriesConfig.SERIES_DBNORMAL));
                    exception++;
                }
            }
        }
        catch (Exception e)
        {
            log.error("batchCancelPublishSeries exception:" + e);
            throw e;
        }
        if (success + exception == seriesList.size())
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
        }
        else if (fail + exception == seriesList.size())
        {
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count") + fail);
        }
        else
        {
            returnInfo.setFlag("2");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("fail.count")+ fail);
        }
        log.debug("batchCancelPublishSeries end");
        return returnInfo.toString();
    }

    /**
     * 批量发布连续剧内容
     * 
     * @param indexLists 连续剧内容id列表
     * @return 批量操作结果提示
     * @throws Exception
     */
    public String batchPublishSeries(List<CmsSeries> seriesList) throws Exception
    {
        
        log.debug("batchPublishSeries starting...");
        ResourceMgt
        .addDefaultResourceBundle(SeriesConfig.CMS_LOG_RESOURCE_FILE);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int success = 0;
        int fail = 0;
        int index = 1;
        int exception = 0;
        try
        {
            for (CmsSeries series : seriesList)
            {
                try
                {
                    CntPlatformSync seriesPlatformSync = new CntPlatformSync();
                    seriesPlatformSync.setObjectid(series.getSeriesid());
                    seriesPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
                    seriesPlatformSync.setPlatform(series.getPlatform());
                    List<CntPlatformSync> seriesPlatformSyncList = cntPlatformSyncDS.getCntPlatformSyncByCond(seriesPlatformSync);
                    if ((seriesPlatformSyncList == null) || seriesPlatformSyncList.size() <= 0)
                    {
                    	operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), ResourceMgt.findDefaultText(SeriesConfig.SERIES_DATA_NOT_EXIST));
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    int status = seriesPlatformSyncList.get(0).getStatus();
                    if (status != StatusTranslator.STATUS_TOPUBLISH && status != StatusTranslator.STATUS_PUBFAILED && status != StatusTranslator.STATUS_MODIFYPUBFAILED)
                    {
                        operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), ResourceMgt.findDefaultText(SeriesConfig.SERIES_STATUS_PUBLISHED));
                        returnInfo.appendOperateInfo(operateInfo);
                        fail++;
                        continue;
                    }
                    else 
                    {//发布状态为待发布、发布失败、修改发布失败                        
                        CntTargetSync seriesTargetSync = new CntTargetSync();
                        seriesTargetSync.setRelateindex(seriesPlatformSyncList.get(0).getSyncindex());
                        seriesTargetSync.setObjecttype(ObjectType.SERIES_TYPE);
                        List<CntTargetSync> seriesTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(seriesTargetSync);
                        boolean isAllTargetSuccess = false;
                        StringBuffer allresult = new StringBuffer();
                        if (null != seriesTargetSyncList && seriesTargetSyncList.size() > 0)
                        {
                            Long batchid = null;
                            
                            int targettype = findFirTargettype(seriesTargetSyncList,0);
                            if(targettype != 99)  
                                batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                            
                            for (CntTargetSync targetSync : seriesTargetSyncList)
                            {
                                Targetsystem target = new Targetsystem();
                                
                                target.setTargetindex(targetSync.getTargetindex());
                                target = targetSystemDS.getTargetsystem(target);
                                if (targetSync.getStatus() == StatusTranslator.STATUS_TOPUBLISH || targetSync.getStatus() == StatusTranslator.STATUS_PUBFAILED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
                                {
                                    settargettype(targetSync,targettype,"platfrom");
                                    targetSync.setReserve01(batchid);
                                    
                                    String result= this.publishSerie2Target(targetSync);
                                    if (result.substring(0, 1).equals("0"))
                                    {
                                        isAllTargetSuccess = true;
                                        allresult.append(" 发布到目标系统"+target.getTargetid()+":"+ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS));
                                    }else
                                    {
                                        allresult.append(" 发布到目标系统"+target.getTargetid()+result.substring(1));
                                    }
                                    
                                }else
                                {
                                    allresult.append(" 发布到目标系统"+target.getTargetid()+":连续剧已发布,不能再发布");
                                }
                            }
                        }
                        else
                        {
                            allresult.append("发布数据已不存在");
                        }
                        if (isAllTargetSuccess)
                        {
                            operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS), allresult.toString());
                            returnInfo.appendOperateInfo(operateInfo);
                            success++;
                        }else
                        {
                            operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL), allresult.toString());
                            returnInfo.appendOperateInfo(operateInfo);
                            fail++;
                        }
                    }
                    
                    CommonLogUtil.insertOperatorLog(series.getSeriesid(), SeriesConfig.SERIES_MGT,
                                SeriesConfig.SERIES_PUBLISH, SeriesConfig.SERIES_PUBLISH_INFO,
                                SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                }
                catch (Exception e)
                {//"发布操作失败", "操作数据库异常"
                    log.error("batchPublishSeries operate db exception:" + e);
                    operateInfo = new OperateInfo(String.valueOf(index++), series.getSeriesid(), ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS), ResourceMgt.findDefaultText(SeriesConfig.SERIES_DBNORMAL));
                    exception++;
                }

            }
        }
        catch (Exception e)
        {
            log.error("batchPublishSeries exception:" + e);
            throw e;
        }
        
        if (success + exception == seriesList.size())
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success);
        }
        else if (fail + exception == seriesList.size())
        {
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("fail.count")+ fail);
        }
        else
        {
            returnInfo.setFlag("2");
            returnInfo.setReturnMessage(ResourceMgt.findDefaultText("success.count") + success + ResourceMgt.findDefaultText("fail.count") + fail);
        }
        log.debug("batchPublishSeries end");
        return returnInfo.toString();
    }
    
    public String publishSerie2Target(CntTargetSync seriesTargetSync) throws Exception
    {
        String str = seriesTargetSync.getReserve02();
        int targettype = 99;
        String pubType = "target";
        if(str != null && !str.equals("")){
            String[] Str = str.split(","); 
            pubType = Str[0];
            targettype = Integer.valueOf(Str[1]);
        }        
        Long batchid = seriesTargetSync.getReserve01();
        
        seriesTargetSync.setReserve02("");
        seriesTargetSync.setReserve01(null);
        
        CntTargetSync targetSync = cntTargetSyncDS.getCntTargetSync(seriesTargetSync);
        String result = "0:操作成功";
        if (null == targetSync)
        {
            return "1:发布数据已不存在";//发布数据已不存在
        }
        if (targetSync.getStatus() == StatusTranslator.STATUS_TOPUBLISH || targetSync.getStatus() == StatusTranslator.STATUS_PUBFAILED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
        {
            
            CmsSeries series = new CmsSeries();
            series.setSeriesid(targetSync.getObjectid());
            List<CmsSeries>  seriesList = cmSeriesDS.getCmsSeriesByCond(series);
            if (null == seriesList)
            {
                return "1:连续剧已不存在";//连续剧已不存在
            }
            Map<String, List> objMap = new HashMap();
            objMap.put("seriesList", seriesList);
            
            //查询角色类型和名称
            CmsSeries cmsSeries = new CmsSeries();
            cmsSeries.setSeriesid(seriesList.get(0).getSeriesid());
            List<CmsSeries> castnameTypeList = cmSeriesDS.getSeriesCastTypeName(cmsSeries);
            if(castnameTypeList.size() != 0){
            objMap.put("castnameTypeList", castnameTypeList);
            }
            
            Targetsystem target = new Targetsystem();
            target.setTargetindex(targetSync.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            if (null == target)
            {
                return "1:目标系统已不存在";//目标系统已不存在
            }else if(0 != target.getStatus())
            {
                return "1:目标系统状态不正常";//目标系统状态不正常
            }
            String xmlfilepathname = null;
            if (targetSync.getStatus() == StatusTranslator.STATUS_TOPUBLISH || targetSync.getStatus() == StatusTranslator.STATUS_PUBFAILED)
            {
                xmlfilepathname = StandardImpFactory.getInstance().create(target.getPlatform()).createXmlFile(objMap,target.getTargettype(), ObjectType.SERIES_TYPE, 1);
                if (null == xmlfilepathname)
                {
                    return "1:生成同步指令xml文件失败";//生成同步指令xml文件失败
                }
                
                seriesTargetSync.setSyncindex(targetSync.getSyncindex());
                seriesTargetSync.setOperresult(StatusTranslator.OPER_STATUS_ADDPUBLISHING);//新增同步中
                seriesTargetSync.setStatus(StatusTranslator.getTargetSyncStatus(StatusTranslator.OPER_STATUS_ADDPUBLISHING));
                cntTargetSyncDS.updateCntTargetSync(seriesTargetSync);//更新网元发布数据表状态
                CntTargetSync targetSyncCond  = new CntTargetSync();
                targetSyncCond.setRelateindex(targetSync.getRelateindex());
                List<CntTargetSync> targetSynList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyncCond);
                int[] statusArray = new int[targetSynList.size()];
                for (int i=0; i<targetSynList.size(); i++)
                {
                    statusArray[i] = targetSynList.get(i).getStatus();
                }
                
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(targetSync.getRelateindex());
                cntPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
                cntPlatformSync.setObjectid(targetSync.getObjectid());
                cntPlatformSync.setPlatform(target.getPlatform());
                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(statusArray));//更新平台发布数据表状态
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                
                CntSyncTask cntSyncTask = new CntSyncTask();
                Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                cntSyncTask.setTaskindex(taskindex);
                if(targettype == target.getTargettype() || pubType.equals("target")){
                    cntSyncTask.setStatus(1);//待执行
                }
                else{
                    cntSyncTask.setStatus(0);//初始
                }
                if(pubType.equals("target")){
                   batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                }
                cntSyncTask.setBatchid(batchid); //填入批次号
                cntSyncTask.setPriority(5);//优先级：中
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(xmlfilepathname);
                cntSyncTask.setRetrytimes(3);//重试三次
                cntSyncTask.setDestindex(Long.valueOf(targetSync.getTargetindex()));
                cntSyncTask.setSource(1);//门户发布
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
                cntSyncTask.setStarttime(dateformat.format(new Date()));

                ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
                objectSyncRecord.setTaskindex(taskindex);
                objectSyncRecord.setObjectindex(targetSync.getObjectindex());
                objectSyncRecord.setObjectid(seriesList.get(0).getSeriesid());
                objectSyncRecord.setObjectcode(seriesList.get(0).getCpcontentid());
                objectSyncRecord.setObjecttype(targetSync.getObjecttype());
                objectSyncRecord.setActiontype(1);//REGIST
                objectSyncRecord.setDestindex(targetSync.getTargetindex());
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
                ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
            }else
            {
                xmlfilepathname = StandardImpFactory.getInstance().create(target.getPlatform()).createXmlFile(objMap,target.getTargettype(), ObjectType.SERIES_TYPE, 2);
                if (null == xmlfilepathname)
                {
                    return "1:生成同步指令xml文件失败";//生成同步指令xml文件失败
                }
                
                seriesTargetSync.setSyncindex(targetSync.getSyncindex());
                seriesTargetSync.setOperresult(StatusTranslator.OPER_STATUS_MODIFYPUBLISHING);//修改同步中
                seriesTargetSync.setStatus(StatusTranslator.getTargetSyncStatus(StatusTranslator.OPER_STATUS_MODIFYPUBLISHING));
                cntTargetSyncDS.updateCntTargetSync(seriesTargetSync);//更新网元发布数据表状态
                CntTargetSync targetSyncCond  = new CntTargetSync();
                targetSyncCond.setRelateindex(targetSync.getRelateindex());
                List<CntTargetSync> targetSynList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyncCond);
                int[] statusArray = new int[targetSynList.size()];
                for (int i=0; i<targetSynList.size(); i++)
                {
                    statusArray[i] = targetSynList.get(i).getStatus();
                }
                
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setSyncindex(targetSync.getRelateindex());
                cntPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
                cntPlatformSync.setObjectid(targetSync.getObjectid());
                cntPlatformSync.setPlatform(target.getPlatform());
                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(statusArray));//更新平台发布数据表状态
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                
                CntSyncTask cntSyncTask = new CntSyncTask();
                Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
                String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
                cntSyncTask.setTaskindex(taskindex);
                
                if(targettype == target.getTargettype() || pubType.equals("target")){
                    cntSyncTask.setStatus(1);//待执行
                }
                else{
                    cntSyncTask.setStatus(0);//初始
                }
                if(pubType.equals("target")){
                   batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
                }
                cntSyncTask.setBatchid(batchid); //填入批次号
                
                cntSyncTask.setPriority(5);//优先级：中
                cntSyncTask.setCorrelateid(correlateid);
                cntSyncTask.setContentmngxmlurl(xmlfilepathname);
                cntSyncTask.setRetrytimes(3);//重试三次
                cntSyncTask.setDestindex(Long.valueOf(targetSync.getTargetindex()));
                cntSyncTask.setSource(1);//门户发布
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
                cntSyncTask.setStarttime(dateformat.format(new Date()));
                
                ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
                objectSyncRecord.setTaskindex(taskindex);
                objectSyncRecord.setObjectindex(targetSync.getObjectindex());
                objectSyncRecord.setObjectid(seriesList.get(0).getSeriesid());
                objectSyncRecord.setObjectcode(seriesList.get(0).getCpcontentid());
                objectSyncRecord.setObjecttype(targetSync.getObjecttype());
                objectSyncRecord.setActiontype(2);//UPDATE
                objectSyncRecord.setDestindex(targetSync.getTargetindex());
                objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
                ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
            }
            
            CommonLogUtil.insertOperatorLog(seriesTargetSync.getObjectid()+","+target.getTargetid(), SeriesConfig.SERIES_MGT,
                    SeriesConfig.SERIES_PUBLISH, SeriesConfig.SERIES_PUBLISH_INFO,
                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
        }
        else
        {
            return  "1:此状态下不能做发布操作";//此状态下不能做发布操作
        }
        return result;
    }
    
    public String updatePublishSerie2Target(CntTargetSync seriesTargetSync) throws Exception
    {
        String str = seriesTargetSync.getReserve02();
        int targettype = 99;
        String pubType = "target";
        if(str != null && !str.equals("")){
            String[] Str = str.split(","); 
            pubType = Str[0];
            targettype = Integer.valueOf(Str[1]);
        }        
        Long batchid = seriesTargetSync.getReserve01();
        
        seriesTargetSync.setReserve02("");
        seriesTargetSync.setReserve01(null);
        
        CntTargetSync targetSync = cntTargetSyncDS.getCntTargetSync(seriesTargetSync);
        String result = "0:操作成功";
        if (null == targetSync)
        {
            return "1:发布数据已不存在";//发布数据已不存在
        }
        if (targetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
        {
            CmsSeries series = new CmsSeries();
            series.setSeriesid(targetSync.getObjectid());
            List<CmsSeries>  seriesList = cmSeriesDS.getCmsSeriesByCond(series);
            if (null == seriesList)
            {
                return "1:连续剧已不存在";//连续剧已不存在
            }
            Map<String, List> objMap = new HashMap();
            objMap.put("seriesList", seriesList);
            
          //查询角色类型和名称
            CmsSeries cmsSeries = new CmsSeries();
            cmsSeries.setSeriesid(seriesList.get(0).getSeriesid());
            List<CmsSeries> castnameTypeList = cmSeriesDS.getSeriesCastTypeName(cmsSeries);
            if(castnameTypeList.size() != 0){
            objMap.put("castnameTypeList", castnameTypeList);
            }
            
            Targetsystem target = new Targetsystem();
            target.setTargetindex(targetSync.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            if (null == target)
            {
                return "1:目标系统已不存在";//目标系统已不存在
            }else if(0 != target.getStatus())
            {
                return "1:目标系统状态不正常";//目标系统状态不正常
            }
            String xmlfilepathname = null;
            xmlfilepathname = StandardImpFactory.getInstance().create(target.getPlatform()).createXmlFile(objMap,target.getTargettype(), ObjectType.SERIES_TYPE, 2);
            if (null == xmlfilepathname)
            {
                return "1:生成同步指令xml文件失败";//生成同步指令xml文件失败
            }
            
            seriesTargetSync.setSyncindex(targetSync.getSyncindex());
            seriesTargetSync.setOperresult(StatusTranslator.OPER_STATUS_MODIFYPUBLISHING);//修改同步中
            seriesTargetSync.setStatus(StatusTranslator.getTargetSyncStatus(StatusTranslator.OPER_STATUS_MODIFYPUBLISHING));
            cntTargetSyncDS.updateCntTargetSync(seriesTargetSync);//更新网元发布数据表状态
            CntTargetSync targetSyncCond  = new CntTargetSync();
            targetSyncCond.setRelateindex(targetSync.getRelateindex());
            List<CntTargetSync> targetSynList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyncCond);
            int[] statusArray = new int[targetSynList.size()];
            for (int i=0; i<targetSynList.size(); i++)
            {
                statusArray[i] = targetSynList.get(i).getStatus();
            }
            
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setSyncindex(targetSync.getRelateindex());
            cntPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
            cntPlatformSync.setObjectid(targetSync.getObjectid());
            cntPlatformSync.setPlatform(target.getPlatform());
            cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(statusArray));//更新平台发布数据表状态
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
            
            CntSyncTask cntSyncTask = new CntSyncTask();
            Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
            cntSyncTask.setTaskindex(taskindex);
           
            
            if(targettype == target.getTargettype()){
                cntSyncTask.setStatus(1);//待执行
            }
            else{
                cntSyncTask.setStatus(0);//初始
            }
            if(target.getTargettype()!=1 && target.getTargettype()!=2 && target.getTargettype()!=3){
               batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
               cntSyncTask.setStatus(1);//待执行
            }
            cntSyncTask.setBatchid(batchid); //填入批次号
            cntSyncTask.setPriority(5);//优先级：中
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlfilepathname);
            cntSyncTask.setRetrytimes(3);//重试三次
            cntSyncTask.setDestindex(Long.valueOf(targetSync.getTargetindex()));
            cntSyncTask.setSource(1);//门户发布
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            
            ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(targetSync.getObjectindex());
            objectSyncRecord.setObjectid(seriesList.get(0).getSeriesid());
            objectSyncRecord.setObjectcode(seriesList.get(0).getCpcontentid());
            objectSyncRecord.setObjecttype(targetSync.getObjecttype());
            objectSyncRecord.setActiontype(2);//UPDATE
            objectSyncRecord.setDestindex(targetSync.getTargetindex());
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
            
            CommonLogUtil.insertOperatorLog(seriesTargetSync.getObjectid()+","+target.getTargetid(), SeriesConfig.SERIES_MGT,
                    SeriesConfig.SERIES_PUBLISH, SeriesConfig.SERIES_PUBLISH_INFO,
                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
        }
        else
        {
            return  "1:此状态下不能做发布操作";//此状态下不能做发布操作
        }
        return result;
    }
    
    public String cancelPublishSerie2Target(CntTargetSync seriesTargetSync) throws Exception
    {
        String str = seriesTargetSync.getReserve02();
        int targettype = 99;
        String pubType = "target";
        if(str != null && !str.equals("")){
            String[] Str = str.split(","); 
            pubType = Str[0];
            targettype = Integer.valueOf(Str[1]);
        }        
       Long batchid = seriesTargetSync.getReserve01();
       seriesTargetSync.setReserve02("");
       seriesTargetSync.setReserve01(null);

        CntTargetSync targetSync = cntTargetSyncDS.getCntTargetSync(seriesTargetSync);
        String result = "0:操作成功";
        if (null == targetSync)
        {
            return "1:发布数据已不存在";//发布数据已不存在
        }
        
        if (targetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED || targetSync.getStatus() == StatusTranslator.STATUS_MODIFYPUBFAILED)
        {
            
            CmsSeries series = new CmsSeries();
            series.setSeriesid(targetSync.getObjectid());
            List<CmsSeries>  seriesList = cmSeriesDS.getCmsSeriesByCond(series);
            if (null == seriesList)
            {
                return "1:连续剧已不存在";//连续剧已不存在
            }
            Map<String, List> objMap = new HashMap();
            objMap.put("seriesList", seriesList);
            Targetsystem target = new Targetsystem();
            target.setTargetindex(targetSync.getTargetindex());
            target = targetSystemDS.getTargetsystem(target);
            if (null == target)
            {
                return "1:目标系统已不存在";//目标系统已不存在
            }else if(0 != target.getStatus())
            {
                return "1:目标系统状态不正常";//目标系统状态不正常
            }
            
          //添加mapping发布判断
            CntTargetSync mappingTargetSync = new CntTargetSync();                             
            mappingTargetSync.setTargetindex(targetSync.getTargetindex());
            mappingTargetSync.setElementid(series.getSeriesid());
            mappingTargetSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
            List<CntTargetSync> seriesCategoryTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
            if (null != seriesCategoryTargetSyncList && seriesCategoryTargetSyncList.size() > 0)
            {
                for(CntTargetSync scTargetSync:seriesCategoryTargetSyncList )
                {
                    if (scTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                    {
                        
                        return "1:与栏目"+scTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";

                    }
                }
            }
            
            mappingTargetSync.setParentid(series.getSeriesid());
            mappingTargetSync.setElementid(null);
            mappingTargetSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
            List<CntTargetSync> seriesCrmTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
            if (null != seriesCrmTargetSyncList && seriesCrmTargetSyncList.size() > 0)
            {
                for(CntTargetSync scrmTargetSync:seriesCrmTargetSyncList )
                {
                    if (scrmTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                    {
                        return "1:与角色"+scrmTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧";
                    }
                }
            }
            
            mappingTargetSync.setParentid(series.getSeriesid());
            mappingTargetSync.setElementid(null);
            mappingTargetSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
            List<CntTargetSync> seriesProgTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
            if (null != seriesProgTargetSyncList && seriesProgTargetSyncList.size() > 0)
            {
                for(CntTargetSync spTargetSync:seriesProgTargetSyncList )
                {
                    if (spTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                    {
                        return "1:与单集"+spTargetSync.getElementid() +"的关联已经发布,不允许取消发布连续剧";
                    }
                }
            }
            
            mappingTargetSync.setParentid(null);
            mappingTargetSync.setElementid(series.getSeriesid());
            mappingTargetSync.setObjecttype(ObjectType.SERIESPICTUREMAP_TYPE);                                
            List<CntTargetSync> seriesPIcTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
            if (null != seriesPIcTargetSyncList && seriesPIcTargetSyncList.size() > 0)
            {
                for(CntTargetSync sPicTargetSync:seriesPIcTargetSyncList )
                {
                    if (sPicTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                    {
                        return "1:与海报"+sPicTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";
                    }
                }
            }
            
            mappingTargetSync.setParentid(null);
            mappingTargetSync.setElementid(series.getSeriesid());
            mappingTargetSync.setObjecttype(ObjectType.SERVICESERIESMAP_TYPE);                                
            List<CntTargetSync> seriesServiceTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(mappingTargetSync);
            if (null != seriesServiceTargetSyncList && seriesServiceTargetSyncList.size() > 0)
            {
                for(CntTargetSync ssTargetSync:seriesServiceTargetSyncList )
                {
                    if (ssTargetSync.getStatus() == StatusTranslator.STATUS_PUBLISHED)
                    {
                        return "1:与服务"+ssTargetSync.getParentid() +"的关联已经发布,不允许取消发布连续剧";
                    }
                }
            }
            
            String xmlfilepathname = null;
            xmlfilepathname = StandardImpFactory.getInstance().create(target.getPlatform()).createXmlFile(objMap,target.getTargettype(), ObjectType.SERIES_TYPE, 3);
            if (null == xmlfilepathname)
            {
                return "1:生成同步指令xml文件失败";//生成同步指令xml文件失败
            }
            
            seriesTargetSync.setSyncindex(targetSync.getSyncindex());
            seriesTargetSync.setOperresult(StatusTranslator.OPER_STATUS_DELPUBLISHING);//取消同步中
            seriesTargetSync.setStatus(StatusTranslator.getTargetSyncStatus(StatusTranslator.OPER_STATUS_DELPUBLISHING));
            cntTargetSyncDS.updateCntTargetSync(seriesTargetSync);//更新网元发布数据表状态
            CntTargetSync targetSyncCond  = new CntTargetSync();
            targetSyncCond.setRelateindex(targetSync.getRelateindex());
            List<CntTargetSync> targetSynList = cntTargetSyncDS.getCntTargetSyncByCond(targetSyncCond);
            int[] statusArray = new int[targetSynList.size()];
            for (int i=0; i<targetSynList.size(); i++)
            {
                statusArray[i] = targetSynList.get(i).getStatus();
            }
            
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setSyncindex(targetSync.getRelateindex());
            cntPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
            cntPlatformSync.setObjectid(targetSync.getObjectid());
            cntPlatformSync.setPlatform(target.getPlatform());
            cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(statusArray));//更新平台发布数据表状态
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
            
            CntSyncTask cntSyncTask = new CntSyncTask();
            Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
            String correlateid = this.getPrimaryKeyGenerator().getPrimarykey(CORRELATEID).toString();
            cntSyncTask.setTaskindex(taskindex);
            
            if(targettype == target.getTargettype() || pubType.equals("target")){
                cntSyncTask.setStatus(1);//待执行
            }
            else{
                cntSyncTask.setStatus(0);//初始
            }
            if(pubType.equals("target")){
               batchid = (Long)(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id"));
            }
            cntSyncTask.setBatchid(batchid); //填入批次号
            //cntSyncTask.setStatus(1);//待执行
            
            cntSyncTask.setPriority(5);//优先级：中
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlfilepathname);
            cntSyncTask.setRetrytimes(3);//重试三次
            cntSyncTask.setDestindex(Long.valueOf(targetSync.getTargetindex()));
            cntSyncTask.setSource(1);//门户发布
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            
            ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();
            objectSyncRecord.setTaskindex(taskindex);
            objectSyncRecord.setObjectindex(targetSync.getObjectindex());
            objectSyncRecord.setObjectid(seriesList.get(0).getSeriesid());
            objectSyncRecord.setObjectcode(seriesList.get(0).getCpcontentid());
            objectSyncRecord.setObjecttype(targetSync.getObjecttype());
            objectSyncRecord.setActiontype(3);//DELETE
            objectSyncRecord.setDestindex(targetSync.getTargetindex());
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
            ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
            
            CommonLogUtil.insertOperatorLog(seriesTargetSync.getObjectid()+","+target.getTargetid(), SeriesConfig.SERIES_MGT,
                    SeriesConfig.SERIES_CANCEL_PUBLISH, SeriesConfig.SERIES_CANCEL_PUBLISH_INFO,
                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
        }
        else
        {
            return  "1:此状态下不能做取消发布操作";//此状态下不能做取消发布操作
        }
        return result;
    }

    /**
     * 
     * @param batchid 任务批次号
     * @param correlateid 任务流水号
     * @param programIndex 同步对象index
     * @param xmlAddress 内容相关对象的XML文件URL
     * @param objecttype 同步对象类型：若干object，若干map关系
     * @param taskType 同步类型：1 REGIST，2 UPDATE，3 DELETE
     * @param desttype 目标系统类型：1 BMS，2 EPG，3 CDN
     * @param destindex 目标系统index
     * @param batchsingleflag 本批任务是否针对单个对象：0否，1是
     * @param tasksingleflag 当前任务是否针对单个对象：0否，1是
     */
    private void insertCntSyncTask(String batchid, String correlateid, Long seriesIndex, String xmlAddress,
            int objecttype, int taskType, int desttype, String destindex, int batchsingleflag, int tasksingleflag)
    {
        CntSyncTask cntSyncTask = new CntSyncTask();
        try
        {
            cntSyncTask.setStatus(1);
            cntSyncTask.setPriority(5);
            cntSyncTask.setCorrelateid(correlateid);
            cntSyncTask.setContentmngxmlurl(xmlAddress);
       
            cntSyncTask.setDestindex(Long.valueOf(destindex));
            
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
            cntSyncTask.setStarttime(dateformat.format(new Date()));
            
            ntsyncTaskDS.insertCntSyncTask(cntSyncTask);
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }

    }

    private String createXml(List<CmsSeries> seriesList, int syncType, String mountPoint, String tempAreaPath,
            String ftpAddress) throws Exception
    {
        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = "";
        fileName = date.getTime() + "_series.xml";
        // 创建XML文件
        String synctype = "";
        String tempFilePath = mountPoint + tempAreaPath + File.separator + CNTSYNCXML + File.separator + dir
                + File.separator + SERIES_DIR + File.separator;
        String retPath = filterSlashStr(tempAreaPath + File.separator + CNTSYNCXML + File.separator + dir
                + File.separator + SERIES_DIR + File.separator + fileName);
        File file = new File(tempFilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }

        String fileFullName = tempFilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        // 获取发布Action类型
        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }
        for (CmsSeries seriesDemo : seriesList)
        {
            Element objectElement = null;
            Element seriesElement = null;
            // 元素节点
            objectElement = objectsElement.addElement("Object");
            objectElement.addAttribute("ElementType", "Series");
            objectElement.addAttribute("ContentID", seriesDemo.getSeriesid());
            objectElement.addAttribute("CPContentID", seriesDemo.getCpcontentid());
            objectElement.addAttribute("Action", synctype);
            String unicontentid = seriesDemo.getUnicontentid();
            if (unicontentid == null)
            {
                unicontentid = "";
            }
            objectElement.addAttribute("UniContentId", unicontentid);
            writeElement(objectElement, seriesElement, "Name", seriesDemo.getNamecn());// 插入连续剧名字
            writeElement(objectElement, seriesElement, "OrderNumber", seriesDemo.getOrdernumber());// 插入订购编号
            writeElement(objectElement, seriesElement, "OriginalName", seriesDemo.getOriginalnamecn()); // 插入原名
            writeElement(objectElement, seriesElement, "SortName", seriesDemo.getSortname()); // 插入排序名称
            writeElement(objectElement, seriesElement, "SearchName", seriesDemo.getSearchname()); // 插入索引名称
            writeElement(objectElement, seriesElement, "OrgAirDate", seriesDemo.getOrgairdate()); // 插入首播日期yyyyMMdd
            writeElement(objectElement, seriesElement, "LicensingWindowStart", seriesDemo.getLicensingstart()); // 有效订购开始时间(YYYYMMDDHH24MiSS)
            writeElement(objectElement, seriesElement, "LicensingWindowEnd", seriesDemo.getLicensingend()); // 有效订购结束时间(YYYYMMDDHH24MiSS)
            //writeElement(objectElement, seriesElement, "CPContentID", seriesDemo.getCpcontentid());// CP 内部对于该对象的唯一标识
            writeElement(objectElement, seriesElement, "DisplayAsNew", seriesDemo.getNewcomedays());// 新到天数
            writeElement(objectElement, seriesElement, "DisplayAsLastChance", seriesDemo.getRemainingdays()); // 剩余天数
            writeElement(objectElement, seriesElement, "Macrovision", seriesDemo.getMacrovision());// 拷贝保护标志(0/1)
            writeElement(objectElement, seriesElement, "Price", seriesDemo.getPricetaxin()); // 含税定价
            writeElement(objectElement, seriesElement, "VolumnCount", seriesDemo.getVolumncount()); // 总集数
            writeElement(objectElement, seriesElement, "Status", seriesDemo.getIsvalid()); // 生效失效
            writeElement(objectElement, seriesElement, "Description", seriesDemo.getDesccn()); // 描述信息

            writeElement(objectElement, seriesElement, "ContentProvider", seriesDemo.getCpid());// 内容提供商
            writeElement(objectElement, seriesElement, "KeyWords", seriesDemo.getKeywords()); // 关键字
            writeElement(objectElement, seriesElement, "Tags", seriesDemo.getContenttags()); // 标签
            writeElement(objectElement, seriesElement, "ViewPoint", seriesDemo.getViewpoint());// 看点
            writeElement(objectElement, seriesElement, "StarLevel", seriesDemo.getRecommendstart());// 推荐星级
            writeElement(objectElement, seriesElement, "Rating", seriesDemo.getRating());// 限制类别
            writeElement(objectElement, seriesElement, "Awards", seriesDemo.getAwards());// 所含奖项
            writeElement(objectElement, seriesElement, "Reserve1", seriesDemo.getReserve1());// 保留字段
            writeElement(objectElement, seriesElement, "Reserve2", seriesDemo.getReserve1()); // 保留字段
            // writeElement(objectElement, seriesElement, "UniContentId",
            // seriesDemo.getUnicontentid()); // 关联内容唯一标识
            XMLWriter writer = null;
            try
            {
                writer = new XMLWriter(new FileOutputStream(fileFullName));
                writer.write(dom);
            }
            catch (Exception e)
            {
                log.error("exception:" + e);
            }
            finally
            {
                if (writer != null)
                {
                    try
                    {
                        writer.close();
                    }
                    catch (IOException e)
                    {
                        log.error("IO exception:" + e);
                    }
                }
            }
        }
        return retPath;
    }

    private void writeElement(Element objectElement, Element propertyElement, String key, Object value)
    {
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", key);
        if (value == null)
        {
            propertyElement.addText("");
        }
        else
        {
            propertyElement.addText(String.valueOf(value));
        }
    }

    // 将字符中的"\"转换成"/"
    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    // 获取时间 如"20111006"
    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    public ICmsStorageareaLS getCmsStorageareaLS()
    {
        return cmsStorageareaLS;
    }

    public void setCmsStorageareaLS(ICmsStorageareaLS cmsStorageareaLS)
    {
        this.cmsStorageareaLS = cmsStorageareaLS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }
    
    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }
    
    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    // 取出同步XML文件FTP地址
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig || null == usysConfig.getCfgvalue() || (usysConfig.getCfgvalue().trim().equals("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }

    // 获取挂载点
    private String getMountPoint()
    {
        String moutPoint = GlobalConstants.getMountPoint();
        return moutPoint;
    }

    /**
     * 根据要发布的系统类型或者对应的系统index
     * 
     * @param type 系统类型
     * @return 发布系统index
     */
    private String getTargetSystem(int type)
    {
        Targetsystem targetSystem = new Targetsystem();
        targetSystem.setTargettype(type);
        List<Targetsystem> listtsystem;
        String systemIndex = null;
        try
        {
            listtsystem = targetSystemDS.getTargetsystemByCond(targetSystem);
            if (listtsystem != null && listtsystem.size() > 0)
            {
                targetSystem = listtsystem.get(0);
                systemIndex = targetSystem.getTargetindex().toString();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            log.error("cmsProgramLS exception:" + e.getMessage());
        }
        return systemIndex;
    }

    private String getCurrentDateTime()
    {
        Date date = new Date();
        SimpleDateFormat formate = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateValue = formate.format(date);
        return dateValue;
    }

    public ISeriesProgramMapDS getSeriesProgramMapDS()
    {
        return seriesProgramMapDS;
    }

    public void setSeriesProgramMapDS(ISeriesProgramMapDS seriesProgramMapDS)
    {
        this.seriesProgramMapDS = seriesProgramMapDS;
    }

    public ICategorySeriesMapDS getCategorySeriesMapDS()
    {
        return categorySeriesMapDS;
    }

    public void setCategorySeriesMapDS(ICategorySeriesMapDS categorySeriesMapDS)
    {
        this.categorySeriesMapDS = categorySeriesMapDS;
    }

    public ISeriesPictureMapDS getSeriesPictureMapDS()
    {
        return seriesPictureMapDS;
    }

    public void setSeriesPictureMapDS(ISeriesPictureMapDS seriesPictureMapDS)
    {
        this.seriesPictureMapDS = seriesPictureMapDS;
    }

    public ISeriesCrmMapDS getSeriesCrmMapDS()
    {
        return seriesCrmMapDS;
    }

    public void setSeriesCrmMapDS(ISeriesCrmMapDS seriesCrmMapDS)
    {
        this.seriesCrmMapDS = seriesCrmMapDS;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public IUcpBasicDS getUcpBasicDS()
    {
        return ucpBasicDS;
    }

    public void setUcpBasicDS(IUcpBasicDS ucpBasicDS)
    {
        this.ucpBasicDS = ucpBasicDS;
    }

    public ICntSyncTaskDS getNtsyncTaskDS()
    {
        return ntsyncTaskDS;
    }

    public void setNtsyncTaskDS(ICntSyncTaskDS ntsyncTaskDS)
    {
        this.ntsyncTaskDS = ntsyncTaskDS;
    }

    public ICmsSeriesDS getCmSeriesDS()
    {
        return cmSeriesDS;
    }

    public void setCmSeriesDS(ICmsSeriesDS cmSeriesDS)
    {
        this.cmSeriesDS = cmSeriesDS;
    }

    public String bindSeriesTargetsys(List<Targetsystem> targetsystemList, List<CmsSeries> cmsSeriesList)
            throws DomainServiceException
    {
        ResourceMgt
        .addDefaultResourceBundle(SeriesConfig.CMS_LOG_RESOURCE_FILE);
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int totalSeriesindex = 0;
        int failCount = 0;
        int num = 1;
        for (int i = 0; i < cmsSeriesList.size(); i++) // 遍历要操作的连续剧主键，依次操作要关联的连续剧
        {
            boolean isBindSuccess = false;
            totalSeriesindex++;
            String operResult = null;
            String operdesc = null;
            String bindResult = "";
        
            CmsSeries series = (CmsSeries) cmsSeriesList.get(i);
            if (series == null) {
                failCount++;
                bindResult = ResourceMgt
                        .findDefaultText(SeriesConfig.CMS_BINDTARGET_MSG_HAS_DELETED);
                operResult = ResourceMgt
                        .findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL);
                operateInfo = new OperateInfo(String.valueOf(num), series
                        .getSeriesid(), operResult.substring(2, operResult.length()),
                        bindResult);
                returnInfo.appendOperateInfo(operateInfo);
                num++;
                continue;
            }
            for (int j = 0; j < targetsystemList.size(); j++) {
                CntPlatformSync seriesCntPlatformSync = new CntPlatformSync();
                seriesCntPlatformSync.setObjecttype(ObjectType.SERIES_TYPE);
                seriesCntPlatformSync.setObjectindex(series.getSeriesindex());
                seriesCntPlatformSync.setObjectid(series.getSeriesid());               
                operdesc = bind2Platform(targetsystemList.get(j),seriesCntPlatformSync);// 添加到网元的数据
                bindResult = bindResult + operdesc.substring(2)+" ";
                if (operdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                {
                    isBindSuccess = true;
                    CommonLogUtil.insertOperatorLog(
                            series.getSeriesid() + ", " + targetsystemList.get(j).getTargetid(),
                            SeriesConfig.SERIES_MGT,
                            SeriesConfig.SERIES_BINDTARGET,
                            SeriesConfig.SERIES_BINDTARGET_INFO,
                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                } 
                
                //连续剧的海报关联平台
                if (targetsystemList.get(j).getTargettype().intValue() == 0 || targetsystemList.get(j).getTargettype().intValue() == 2 || targetsystemList.get(j).getTargettype().intValue() == 10)//0-2.0文广网元，10-2.0央视网元，2-EPG 海报只关联2.0平台和EPG
                {
                    SeriesPictureMap seriesPictureMap = new SeriesPictureMap();
                    seriesPictureMap.setSeriesindex(series.getSeriesindex());
                    List<SeriesPictureMap> seriesPictureMapList = seriesPictureMapDS.getSeriesPictureMapByCond(seriesPictureMap);// 连续剧下存在海报,将海报一起关联平台
                    if (seriesPictureMapList != null && seriesPictureMapList.size() > 0) {
                        for (int k = 0; k < seriesPictureMapList.size(); k++) {
                            SeriesPictureMap spmap = seriesPictureMapList.get(k);
                            CntPlatformSync spmapCntPlatformSync = new CntPlatformSync();
                            spmapCntPlatformSync.setObjecttype(ObjectType.SERIESPICTUREMAP_TYPE);
                            spmapCntPlatformSync.setObjectindex(spmap.getMapindex());
                            spmapCntPlatformSync.setObjectid(spmap.getMappingid());
                            spmapCntPlatformSync.setElementid(spmap.getSeriesid());
                            spmapCntPlatformSync.setParentid(spmap.getPictureid());
                            String seriesPictureMapOperdesc = bind2Platform(targetsystemList.get(j), spmapCntPlatformSync);// 海报的发布数据
                            if (seriesPictureMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                            {
                                CommonLogUtil
                                .insertOperatorLog(
                                        spmap.getMappingid() + ", " + targetsystemList.get(j).getTargetid(),
                                        SeriesConfig.SERIES_MGT,
                                        SeriesConfig.SERIESPIC_BINDTARGET,
                                        SeriesConfig.SERIESPIC_BINDTARGET_INFO,
                                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                            }
                        }
                    }
                }
                
                
                //连续剧的栏目关联平台
                CategorySeriesMap categorySeriesMap = new CategorySeriesMap();
                categorySeriesMap.setSeriesindex(series.getSeriesindex());
                List<CategorySeriesMap> categorySeriesMapList = categorySeriesMapDS.getCategorySeriesMapByCond(categorySeriesMap);//连续剧的栏目,将与栏目的mapping一起关联平台
                if (categorySeriesMapList != null && categorySeriesMapList.size() > 0) {
                    for (int k = 0; k < categorySeriesMapList.size(); k++) {
                        CategorySeriesMap csmap = categorySeriesMapList.get(k);
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        cntTargetSync.setObjecttype(ObjectType.CATEGORY_TYPE);
                        cntTargetSync.setObjectindex(csmap.getCategoryindex());
                        cntTargetSync.setObjectid(csmap.getCategoryid());
                        cntTargetSync.setTargetindex(targetsystemList.get(j).getTargetindex());
                        List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                        if (null == cntTargetSyncList || cntTargetSyncList.size() <= 0)
                        {
                            continue;
                        }
                        CntPlatformSync csmapCntPlatformSync = new CntPlatformSync();
                        csmapCntPlatformSync.setObjecttype(ObjectType.CATEGORYSERIESMAP_TYPE);
                        csmapCntPlatformSync.setObjectindex(csmap.getMapindex());
                        csmapCntPlatformSync.setObjectid(csmap.getMappingid());
                        csmapCntPlatformSync.setElementid(csmap.getSeriesid());
                        csmapCntPlatformSync.setParentid(csmap.getCategoryid());
                        String categorySeriesMapOperdesc =  bind2Platform(targetsystemList.get(j), csmapCntPlatformSync);// 栏目的发布数据
                        if (categorySeriesMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                        {
                            CommonLogUtil
                            .insertOperatorLog(
                                    csmap.getMappingid() + ", " + targetsystemList.get(j).getTargetid(),
                                    SeriesConfig.SERIES_MGT,
                                    SeriesConfig.SERIESCATE_BINDTARGET,
                                    SeriesConfig.SERIESCATE_BINDTARGET_INFO,
                                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                        }
                    }
                }
                
                //连续剧的角色关联平台
                if (targetsystemList.get(j).getTargettype().intValue() == 0 || targetsystemList.get(j).getTargettype().intValue() == 2)//0-2.0平台，2-EPG 角色只关联2.0平台和EPG
                {
                    SeriesCrmMap seriesCrmMap = new SeriesCrmMap();
                    seriesCrmMap.setSeriesindex(series.getSeriesindex());
                    List<SeriesCrmMap> seriesCrmMapList = seriesCrmMapDS.getSeriesCrmMapByCond(seriesCrmMap);// 连续剧的角色,将与角色的mapping一起关联平台
                    if (seriesCrmMapList != null && seriesCrmMapList.size() > 0) {
                        for (int k = 0; k < seriesCrmMapList.size(); k++) {
                            SeriesCrmMap scmap = seriesCrmMapList.get(k);
                            CntTargetSync cntTargetSync = new CntTargetSync();
                            cntTargetSync.setObjecttype(ObjectType.CASTROLEMAP_TYPE);
                            cntTargetSync.setObjectindex(scmap.getCastrolemapindex());
                            cntTargetSync.setObjectid(scmap.getCastrolemapid());
                            cntTargetSync.setTargetindex(targetsystemList.get(j).getTargetindex());
                            List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                            if (null == cntTargetSyncList || cntTargetSyncList.size() <= 0)
                            {
                                continue;
                            }
                            CntPlatformSync scmapCntPlatformSync = new CntPlatformSync();
                            scmapCntPlatformSync.setObjecttype(ObjectType.SERIESCASTROLEMAPMAP_TYPE);
                            scmapCntPlatformSync.setObjectindex(scmap.getMapindex());
                            scmapCntPlatformSync.setObjectid(scmap.getMappingid());
                            scmapCntPlatformSync.setElementid(scmap.getCastrolemapid());
                            scmapCntPlatformSync.setParentid(scmap.getSeriesid());
                            String seriesCrmMapOperdesc = bind2Platform(targetsystemList.get(j), scmapCntPlatformSync);// 角色的发布数据
                            if (seriesCrmMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                            {
                                CommonLogUtil
                                .insertOperatorLog(
                                        scmap.getMappingid() + ", " + targetsystemList.get(j).getTargetid(),
                                        SeriesConfig.SERIES_MGT,
                                        SeriesConfig.SERIESCRM_BINDTARGET,
                                        SeriesConfig.SERIESCRM_BINDTARGET_INFO,
                                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                            }
                        }
                    }
                } 
                
                //连续剧与单集的mapping关联平台
                SeriesProgramMap seriesProgramMap = new SeriesProgramMap();
                seriesProgramMap.setSeriesindex(series.getSeriesindex());
                List<SeriesProgramMap> seriesProgramMapList = seriesProgramMapDS.getSeriesProgramMapByCond(seriesProgramMap);// 连续剧的单集,将与单集的mapping一起关联平台
                if (seriesProgramMapList != null && seriesProgramMapList.size() > 0) {
                    for (int k = 0; k < seriesProgramMapList.size(); k++) {
                        SeriesProgramMap spmap = seriesProgramMapList.get(k);
                        CntTargetSync cntTargetSync = new CntTargetSync();
                        cntTargetSync.setObjecttype(ObjectType.PROGRAM_TYPE);
                        cntTargetSync.setObjectindex(spmap.getProgramindex());
                        cntTargetSync.setObjectid(spmap.getProgramid());
                        cntTargetSync.setTargetindex(targetsystemList.get(j).getTargetindex());
                        List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                        if (null == cntTargetSyncList || cntTargetSyncList.size() <= 0)
                        {
                            continue;
                        }
                        CntPlatformSync spmapCntPlatformSync = new CntPlatformSync();
                        spmapCntPlatformSync.setObjecttype(ObjectType.SERIESPROGRAMMAP_TYPE);
                        spmapCntPlatformSync.setObjectindex(spmap.getMapindex());
                        spmapCntPlatformSync.setObjectid(spmap.getMappingid());
                        spmapCntPlatformSync.setElementid(spmap.getProgramid());
                        spmapCntPlatformSync.setParentid(spmap.getSeriesid());
                        String seriesProgramMapOperdesc = bind2Platform(targetsystemList.get(j), spmapCntPlatformSync);// 单集的发布数据
                        if (seriesProgramMapOperdesc.substring(0, 1).equals("0")) // 内容关联运营平台成功
                        {
                            CommonLogUtil
                            .insertOperatorLog(
                                    spmap.getMappingid() + ", " + targetsystemList.get(j).getTargetid(),
                                    SeriesConfig.SERIES_MGT,
                                    SeriesConfig.SERIESPROG_BINDTARGET,
                                    SeriesConfig.SERIESPROG_BINDTARGET_INFO,
                                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                        }
                    }
                }
            }
            
            if (isBindSuccess)
            {
                successCount++;
                operResult = ResourceMgt
                .findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                failCount++;
                operResult = ResourceMgt
                        .findDefaultText(SeriesConfig.RESOURCE_OPERATION_FAIL);
            }
            
            operateInfo = new OperateInfo(String.valueOf(num),
                    series.getSeriesid(), operResult.substring(2,
                            operResult.length()), bindResult);
            returnInfo.appendOperateInfo(operateInfo);
            num++;
        }
        
        if ((successCount) == totalSeriesindex) {
            returnInfo.setReturnMessage("成功数量：" + successCount);
            returnInfo.setFlag("0");
        } else {
            if ((failCount) == totalSeriesindex) {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            } else {
                returnInfo.setReturnMessage("成功数量：" + successCount + "  失败数量："
                        + failCount);
                returnInfo.setFlag("2");
            }
        }
        
        return returnInfo.toString();

    }
    
    /**
     * 关联发布平台方法，生成平台发布数据
     * @param Targetsystem target 要关联的平台中的一个网元
     * @param CntPlatformSync cntPlatformSync 要生成的平台发布数据
     */
    public String bind2Platform(Targetsystem target, CntPlatformSync cntPlatformSync) throws DomainServiceException
    {
        String operdesc = null;
        List list = new ArrayList();
        Long pltsyncindex = 0l;
        cntPlatformSync.setPlatform(target.getPlatform());
        try {
            list = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
            if (list != null && list.size() > 0) {
                cntPlatformSync = (CntPlatformSync) list.get(0);
                pltsyncindex = cntPlatformSync.getSyncindex();
            } else {
                cntPlatformSync.setStatus(0);// 发布总状态：0-待发布 
                pltsyncindex = (Long) this.getPrimaryKeyGenerator()
                        .getPrimarykey("cnt_platform_sync_index");

                cntPlatformSync.setSyncindex(pltsyncindex);
                // 调用ds,dao层插入数据库方法
                try {
                    cntPlatformSyncDS.insertCntPlatformSync(cntPlatformSync);// 插入到数据库中
                } catch (DomainServiceException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (DomainServiceException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        
        CntTargetSync cntTargetSync = new CntTargetSync();
        cntTargetSync.setRelateindex(cntPlatformSync.getSyncindex());
        cntTargetSync.setObjecttype(cntPlatformSync.getObjecttype());
        cntTargetSync.setObjectindex(cntPlatformSync.getObjectindex());
        cntTargetSync.setObjectid(cntPlatformSync.getObjectid());
        if (null != cntPlatformSync.getElementid())
        {
            cntTargetSync.setElementid(cntPlatformSync.getElementid());
        }
        if (null != cntPlatformSync.getParentid())
        {
            cntTargetSync.setParentid(cntPlatformSync.getParentid());
        }
        operdesc = bind2Target(target,cntTargetSync);
        if (operdesc.substring(0, 1).equals("0"))
        {
            cntPlatformSync.setStatus(0);//有新增关联的网元,平台发布数据状态更新为待发布
            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
        }
        return operdesc;
    }

    /** 关联发布网元方法，生成网元发布数据
     * 
     * @param Targetsystem
     *            target 要关联网元
     * @param CntTargetSync
     *            cntTargetSync 要生成的网元发布数据
     * @return */
    public String bind2Target(Targetsystem target, CntTargetSync cntTargetSync)
    {
        ResourceMgt.addDefaultResourceBundle(SeriesConfig.CMS_LOG_RESOURCE_FILE);
        StringBuffer resultStr = new StringBuffer();
        String rtnStr;
        boolean sucess = false;                  
        List list = new ArrayList();
        cntTargetSync.setTargetindex(target.getTargetindex());
        try
        {
            list = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (list != null && list.size() > 0)
            {
                resultStr.append(ResourceMgt.findDefaultText(SeriesConfig.CMS_BINDTARGET_MSG) + target.getTargetid()
                        + ResourceMgt.findDefaultText(SeriesConfig.CMS_BINDTARGET_MSG_HAS_BINDED) + "  ");

            }
            else
            {
                cntTargetSync.setStatus(0);
                cntTargetSync.setOperresult(0);//操作结果：0-待发布
                cntTargetSyncDS.insertCntTargetSync(cntTargetSync);
                resultStr.append(ResourceMgt.findDefaultText(SeriesConfig.CMS_BINDTARGET_MSG) + target.getTargetid()
                        + ResourceMgt.findDefaultText(SeriesConfig.RESOURCE_OPERATION_SUCCESS) + "  ");
                sucess = true;
            }
        }
        catch (DomainServiceException e1)
        {
            e1.printStackTrace();
        }

        rtnStr = sucess == false ? "1:" + resultStr.toString() : "0:" + resultStr.toString();
        return rtnStr;
    }
    public TableDataInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQueryCatSerMapPlatform  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo(); 
        // 格式化内容带有时间的字段
        cmsSeries.setCreateStarttime(DateUtil2.get14Time(cmsSeries.getCreateStarttime()));
        cmsSeries.setCreateEndtime(DateUtil2.get14Time(cmsSeries.getCreateEndtime()));
        
        cmsSeries.setPublishStarttime(DateUtil2.get14Time(cmsSeries.getPublishStarttime()));
        cmsSeries.setPublishEndtime(DateUtil2.get14Time(cmsSeries.getPublishEndtime()));
        
        cmsSeries.setCancelpubStarttime(DateUtil2.get14Time(cmsSeries.getCancelpubStarttime()));
        cmsSeries.setCancelpubEndtime(DateUtil2.get14Time(cmsSeries.getCancelpubEndtime()));
   
        //对特殊字符的处理
        if(cmsSeries.getCategoryid()!=null && !"".equals(cmsSeries.getCategoryid())){
            cmsSeries.setCategoryid(EspecialCharMgt.conversion(cmsSeries.getCategoryid()));
        }
        if(cmsSeries.getCategoryname()!=null && !"".equals(cmsSeries.getCategoryname())){
            cmsSeries.setCategoryname(EspecialCharMgt.conversion(cmsSeries.getCategoryname()));
        }       
        cmsSeries.setOrderCond("objectid desc");        
         try {
            dataInfo = cmSeriesDS.pageInfoQueryCatSerMapPlatform(cmsSeries, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQueryCatSerMapPlatform  end.................................");
        return dataInfo;        
    }
    public TableDataInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQueryCatSerMapTarget  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        cmsSeries.setOrderCond("objectid desc");   
        try {
            dataInfo = cmSeriesDS.pageInfoQueryCatSerMapTarget(cmsSeries, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQueryCatSerMapTarget  end.................................");
        return dataInfo;      
    }
    public TableDataInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuerySerCastMapPlatform  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        // 格式化内容带有时间的字段
        cmsSeries.setCreateStarttime(DateUtil2.get14Time(cmsSeries.getCreateStarttime()));
        cmsSeries.setCreateEndtime(DateUtil2.get14Time(cmsSeries.getCreateEndtime()));
        
        cmsSeries.setPublishStarttime(DateUtil2.get14Time(cmsSeries.getPublishStarttime()));
        cmsSeries.setPublishEndtime(DateUtil2.get14Time(cmsSeries.getPublishEndtime()));
        
        cmsSeries.setCancelpubStarttime(DateUtil2.get14Time(cmsSeries.getCancelpubStarttime()));
        cmsSeries.setCancelpubEndtime(DateUtil2.get14Time(cmsSeries.getCancelpubEndtime()));
   
        //对特殊字符的处理
        if(cmsSeries.getCastrolemapid()!=null && !"".equals(cmsSeries.getCastrolemapid())){
            cmsSeries.setCastrolemapid(EspecialCharMgt.conversion(cmsSeries.getCastrolemapid()));
        }
        if(cmsSeries.getCastrole()!=null && !"".equals(cmsSeries.getCastrole())){
            cmsSeries.setCastrole(EspecialCharMgt.conversion(cmsSeries.getCastrole()));
        }      
        cmsSeries.setOrderCond("objectid desc");       
         try {
            dataInfo = cmSeriesDS.pageInfoQuerySerCastMapPlatform(cmsSeries, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuerySerCastMapPlatform  end.................................");
        return dataInfo; 

    }

    public TableDataInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize) throws Exception
    {
        log.debug("pageInfoQuerySerCastMapTarget  starting.................................");
        TableDataInfo dataInfo = new TableDataInfo();
        cmsSeries.setOrderCond("objectid desc");   
        try {
            dataInfo = cmSeriesDS.pageInfoQuerySerCastMapTarget(cmsSeries, start, pageSize);
        } catch (Exception e) {
            log.error(e);
        }
        log.debug("pageInfoQuerySerCastMapTarget  end.................................");
        return dataInfo;   
        
    }

    public String removeSeriesTargetsysBindList(List<Targetsystem> targetsystemList, List<CmsSeries> cmsSeriesList)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        try{
            int index = 0;
            int successNumber = 0;
            int failNumber = 0;
            String failReason = "";
            String successTarget = "";
    
            ReturnInfo returnInfo = new ReturnInfo();
            String flag = "";
            String returnMessage = "";
            OperateInfo operateInfo = null;
    
            CmsSeries cmsSeries = new CmsSeries();
            Targetsystem targetsystem = new Targetsystem();
    
            for (int i = 0; i < cmsSeriesList.size(); i++)
            {
                index++;
                cmsSeries = cmsSeriesList.get(i);
                cmsSeries = cmSeriesDS.getCmsSeries(cmsSeries);
                if (cmsSeries == null)
                {
                    failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_NOT_EXIST);
                    failNumber++;
                    operateInfo = new OperateInfo(String.valueOf(index), cmsSeries.getSeriesid(),
                            SeriesConfig.SERIES_BIND_TARGET_DEL_FAILED, failReason);
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
    
                boolean isDelTarget = false;
                // 循环读取目标系统
                for (int j = 0; j < targetsystemList.size(); j++)
                {
                    targetsystem = targetsystemList.get(j);
                    targetsystem = targetSystemDS.getTargetsystem(targetsystem);
                    if (targetsystem == null)
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.TARGET_NOT_EXIST);
                        continue;
                    }
    
                    // 检测关联关系是否存在
                    CntTargetSync cntTargetSync = new CntTargetSync();
                    cntTargetSync.setObjecttype(4);
                    cntTargetSync.setObjectindex(cmsSeries.getSeriesindex());
                    cntTargetSync.setObjectid(cmsSeries.getSeriesid());
                    cntTargetSync.setTargetindex(targetsystem.getTargetindex());
    
                    List<CntTargetSync> cntTargetSynclist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
                    if (cntTargetSynclist.size() < 1)
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_TARGET_NOT_EXIST);
                        continue;
                    }
                    // 检测电视剧与网元的发布状态是否为发布中
                    cntTargetSync = cntTargetSynclist.get(0);
                    int cntTargetSyncStatus = cntTargetSync.getStatus();
                    if ((cntTargetSyncStatus == 200) || (cntTargetSyncStatus == 300) || (cntTargetSyncStatus == 500))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_IN_TARGETSYSTEM);
                        continue;
                    }
    
                    // //关联mapping检测
                    // 是否在此网元上关联了点播内容
                    if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESPROGRAMMAP_TYPE, 1))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                        continue;
                    }
                    // 是否在此网元上关联了海报
                    if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESCASTROLEMAPMAP_TYPE, 1))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                        continue;
                    }
                    // 是否在此网元上关联了角色
                    if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESPICTUREMAP_TYPE, 1))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                        continue;
                    }
                    // 是否有服务在此网元上关联了本电视剧
                    if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.CATEGORYSERIESMAP_TYPE, 2))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_BY_OTHER);
                        continue;
                    }
                    // 是否有栏目在此网元上关联了本电视剧
                    if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERVICESERIESMAP_TYPE, 2))
                    {
                        failReason = ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_BY_OTHER);
                        continue;
                    }
    
                    // //删除关联mapping
                    // 删除此网元上关联的点播内容
                    delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESPROGRAMMAP_TYPE, 1);
                    // 删除此网元上关联的海报
                    delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESCASTROLEMAPMAP_TYPE, 1);
                    // 删除此网元上关联的角色
                    delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESPICTUREMAP_TYPE, 1);
                    // 删除此网元上与服务的打包关系
                    delSeriesBindtype(targetsystem, cmsSeries, ObjectType.CATEGORYSERIESMAP_TYPE, 2);
                    // 删除此网元上与栏目的打包关系
                    delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERVICESERIESMAP_TYPE, 2);
                    // //删除电视剧与平台的关联关系
                    cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                    
                  //检测是否需要删除主平台关系-------------------------------------------------
                    CntTargetSync childCntTargetSync = new CntTargetSync();
                    childCntTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                    childCntTargetSync.setObjecttype(cntTargetSync.getObjecttype());
                    childCntTargetSync.setObjectindex(cntTargetSync.getObjectindex());
                    childCntTargetSync.setObjectid(cntTargetSync.getObjectid());
                    List<CntTargetSync> childTargetSyncList  = cntTargetSyncDS.getCntTargetSyncByCond(childCntTargetSync);
                    
                    if ( childTargetSyncList.size()<1)
                    {
                        //删除电视剧与主平台的关联关系-------------------------------------------------
                        CntPlatformSync cntPlatformSync = new CntPlatformSync() ;
                        cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                        cntPlatformSync.setObjecttype(4);
                        cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                        // 写操作日志
                        CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                                SeriesConfig.SERIES_BIND_PALTFORM_DEL, SeriesConfig.SERIES_BIND_PALTFORM_DEL_INF,
                                SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                    }
                    else
                    {
                        // 获取平台发布信息
                        CntPlatformSync cntPlatformSync = new CntPlatformSync();
                        cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                        cntPlatformSync.setPlatform(targetsystem.getPlatform());
                        cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                        cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);// 此处需要注意
                        // 更改主平台关联发布状态
                        int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                        for (int n = 0; n < childTargetSyncList.size(); n++)
                        {
                            childTargetSyncStatus[n] = childTargetSyncList.get(n).getStatus();
                        }
                        cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);            
                    }
                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                            SeriesConfig.SERIES_BIND_TARGET_DEL, SeriesConfig.SERIES_BIND_TARGET_DEL_INF,
                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                    isDelTarget = true;
                    successTarget = successTarget + " " + targetsystem.getTargetname();
                }
                // 循环读取目标系统结束
                if (isDelTarget)
                {
                    successNumber++;
                    operateInfo = new OperateInfo(String.valueOf(index), cmsSeries.getSeriesid(),
                            SeriesConfig.SERIES_BIND_TARGET_DEL_SUCCESS, SeriesConfig.SERIES_BIND_TARGET_DEL
                                    + successTarget);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    failNumber++;
                    operateInfo = new OperateInfo(String.valueOf(index), cmsSeries.getSeriesid(),
                            SeriesConfig.SERIES_BIND_TARGET_DEL_FAILED, SeriesConfig.SERIES_BIND_NO_TARGET_CAN_DEL);
                    returnInfo.appendOperateInfo(operateInfo);
                }
            }
    
            // 判断最终添加结果
            if (successNumber > 0)
            {
                flag = "0";
            }
            else
            {
                flag = "1";
            }
            returnInfo.setFlag(flag);
            return returnInfo.toString();
        }
        catch (DomainServiceException e)
        {
            log.error("removeSeriesTargetsysBindList exception:" + e);
            throw e;
        }
    }

    public String removeSeriesTargetsysBind(Targetsystem targetsystem, CmsSeries cmsSeries)
            throws DomainServiceException
    {
        // TODO Auto-generated method stub
        try
        {
            String rtnString = null;// 返回信息
    
            // 检测电视剧是否存在
            cmsSeries = cmSeriesDS.getCmsSeriesById(cmsSeries);
            if (cmsSeries == null)
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_NOT_EXIST);
                return rtnString;
            }
            // 检测网元是否存在
            targetsystem = targetSystemDS.getTargetsystem(targetsystem);
            if (targetsystem == null)
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.TARGET_NOT_EXIST);
                return rtnString;
            }
            // 检测电视剧与网元是否存在关联关系
            CntTargetSync cntTargetSync = new CntTargetSync();
            cntTargetSync.setObjecttype(4);
            cntTargetSync.setObjectindex(cmsSeries.getSeriesindex());
            cntTargetSync.setObjectid(cmsSeries.getSeriesid());
            cntTargetSync.setTargetindex(targetsystem.getTargetindex());
    
            List<CntTargetSync> cntTargetSynclist = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
            if (cntTargetSynclist.size() < 1)
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_TARGET_NOT_EXIST);
                return rtnString;
            }
    
            // 检测电视剧与网元的发布状态是否为发布中
            cntTargetSync = cntTargetSynclist.get(0);
            int cntTargetSyncStatus = cntTargetSync.getStatus();
            if ((cntTargetSyncStatus == 200) || (cntTargetSyncStatus == 300) || (cntTargetSyncStatus == 500))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_IN_TARGETSYSTEM);
                return rtnString;
            }
    
            // //关联mapping检测
            // 是否在此网元上关联了点播内容
            if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESPROGRAMMAP_TYPE, 1))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                return rtnString;
            }
            // 是否在此网元上关联了海报
            if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESPICTUREMAP_TYPE, 1))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                return rtnString;
            }
            // 是否在此网元上关联了角色
            if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERIESCASTROLEMAPMAP_TYPE, 1))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_CONTENT_PUBLISHED);
                return rtnString;
            }
            // 是否有服务在此网元上关联了本电视剧
            if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.SERVICESERIESMAP_TYPE, 2))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_BY_SERVICE);
                return rtnString;
            }
            // 是否有栏目在此网元上关联了本电视剧
            if (isSeriesBindPulish(targetsystem, cmsSeries, ObjectType.CATEGORYSERIESMAP_TYPE, 2))
            {
                rtnString = "1:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_PUBLISHED_BY_CATEGORY);
                return rtnString;
            }
    
            // //删除关联mapping
            // 删除此网元上关联的点播内容
            delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESPROGRAMMAP_TYPE, 1);
            // 删除此网元上关联的海报
            delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESPICTUREMAP_TYPE, 1);
            // 删除此网元上关联的角色
            delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERIESCASTROLEMAPMAP_TYPE, 1);
            // 删除此网元上与服务的打包关系
            delSeriesBindtype(targetsystem, cmsSeries, ObjectType.SERVICESERIESMAP_TYPE, 2);
            // 删除此网元上与栏目的打包关系
            delSeriesBindtype(targetsystem, cmsSeries, ObjectType.CATEGORYSERIESMAP_TYPE, 2);
            // //删除电视剧与平台网元的关联关系
            cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
            
    //        rtnString = "0:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_BIND_TARGET_DEL) + "  " + targetsystem.getTargetname();
            rtnString = "0:" + ResourceMgt.findDefaultText(SeriesConfig.SERIES_OPERATE_SUCCEED);
            //检测是否需要删除主平台关系
            CntTargetSync childCntTargetSync = new CntTargetSync();
            childCntTargetSync.setRelateindex(cntTargetSync.getRelateindex());
            childCntTargetSync.setObjecttype(cntTargetSync.getObjecttype());
            childCntTargetSync.setObjectindex(cntTargetSync.getObjectindex());
            childCntTargetSync.setObjectid(cntTargetSync.getObjectid());
            List<CntTargetSync> childTargetSyncList  = cntTargetSyncDS.getCntTargetSyncByCond(childCntTargetSync);
            
            if ( childTargetSyncList.size()<1)
            {
                //删除电视剧与主平台的关联关系
                CntPlatformSync cntPlatformSync = new CntPlatformSync() ;
                cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                cntPlatformSync.setObjecttype(4);
                cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                // 写操作日志
                CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                        SeriesConfig.SERIES_BIND_PALTFORM_DEL, SeriesConfig.SERIES_BIND_PALTFORM_DEL_INF,
                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
            }
            else
            {
                // 获取平台发布信息
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                cntPlatformSync.setPlatform(targetsystem.getPlatform());
                cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);
                // 更改主平台关联发布状态
                int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                for (int j = 0; j < childTargetSyncList.size(); j++)
                {
                    childTargetSyncStatus[j] = childTargetSyncList.get(j).getStatus();
                }
                cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);            
            }
            
            // 写操作日志
            CommonLogUtil.insertOperatorLog(cmsSeries.getSeriesid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                    SeriesConfig.SERIES_BIND_TARGET_DEL, SeriesConfig.SERIES_BIND_TARGET_DEL_INF,
                    SeriesConfig.RESOURCE_OPERATION_SUCCESS);
            
            return rtnString;            
        }
        catch (DomainServiceException e)
        {
            log.error("removeSeriesTargetsysBind exception:" + e);
            throw e;
        }
    }

    /** 检测电视剧和其他对象的关联关系mapping是否在本网元上有处于发布状态的。
     * 
     * @param targetsystem
     *            网元平台Targetindex
     * @param cmsSeries
     *            电视剧Seriesid
     * @param objecttype
     *            关联内容的类型编号
     * @param mapType
     *            1:电视剧在服务中为parentid ，2:电视剧在服务中为elementid ，其他：直接返回false
     * @return true:处于发布状态 ，false：没有处于发布状态
     * @throws DomainServiceException */
    private boolean isSeriesBindPulish(Targetsystem targetsystem, CmsSeries cmsSeries, int objecttype, int mapType)
            throws DomainServiceException
    {
        CntTargetSync cntTargetSync = new CntTargetSync();
        cntTargetSync.setObjecttype(objecttype);
        cntTargetSync.setTargetindex(targetsystem.getTargetindex());

        if (mapType == 1)
        {
            cntTargetSync.setParentid(cmsSeries.getSeriesid());
        }
        else if (mapType == 2)
        {
            cntTargetSync.setElementid(cmsSeries.getSeriesid());
        }
        else
        {
            return false;
        }

        List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);
        int cntTargetSyncStatus = 0;
        if (cntTargetSyncList.size() > 0)
        {
            // 是否在此网元上与栏目的关联处于发布中
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                cntTargetSync = cntTargetSyncList.get(0);
                cntTargetSyncStatus = cntTargetSync.getStatus();
                if ((cntTargetSyncStatus == 200) || (cntTargetSyncStatus == 300) || (cntTargetSyncStatus == 500))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /** 直接删除网元电视剧在对应平台且为父元素的mapping关系，同时更新平台关联关系。
     * 
     * @param targetsystem
     *            网元平台Targetindex
     * @param cmsSeries
     *            电视剧Seriesid
     * @param objecttype
     *            关联内容的类型编号
     * @param mapType
     *            1:电视剧在服务中为parentid ，2:电视剧在服务中为elementid ，其他：直接返回false
     * @return true:删除成功 ，false：没有删除或删除失败
     * @throws DomainServiceException */
    private boolean delSeriesBindtype(Targetsystem targetsystem, CmsSeries cmsSeries, int objecttype, int mapType)
            throws DomainServiceException
    {
        // 获取对应的网元关联
        CntTargetSync cntTargetSync = new CntTargetSync();
        cntTargetSync.setObjecttype(objecttype);
        cntTargetSync.setTargetindex(targetsystem.getTargetindex());

        if (mapType == 1)
        {
            cntTargetSync.setParentid(cmsSeries.getSeriesid());
        }
        else if (mapType == 2)
        {
            cntTargetSync.setElementid(cmsSeries.getSeriesid());
        }
        else
        {
            return false;
        }

        List<CntTargetSync> cntTargetSyncList = cntTargetSyncDS.getCntTargetSyncByCond(cntTargetSync);

        // 如果有mapping需要删除。
        if (cntTargetSyncList.size() > 0)
        {
            for (int i = 0; i < cntTargetSyncList.size(); i++)
            {
                cntTargetSync = cntTargetSyncList.get(i);
                cntTargetSyncDS.removeCntTargetSync(cntTargetSync);
                // 写操作日志
                CommonLogUtil.insertOperatorLog(cntTargetSync.getObjectid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                        SeriesConfig.SERIES_BIND_CONTENT_DEL_TARGET, SeriesConfig.SERIES_BIND_CONTENT_DEL_TARGET_INF,
                        SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                
                // 获取平台发布信息
                CntPlatformSync cntPlatformSync = new CntPlatformSync();
                cntPlatformSync.setObjecttype(cntTargetSync.getObjecttype());
                cntPlatformSync.setPlatform(targetsystem.getPlatform());
                cntPlatformSync.setSyncindex(cntTargetSync.getRelateindex());
                cntPlatformSync = cntPlatformSyncDS.getCntPlatformSync(cntPlatformSync);// 此处需要注意

                // 查询子网元发布信息
                CntTargetSync childTargetSync = new CntTargetSync();
                childTargetSync.setObjectid(cntTargetSync.getObjectid());
                childTargetSync.setRelateindex(cntTargetSync.getRelateindex());
                childTargetSync.setParentid(cntTargetSync.getParentid());
                childTargetSync.setElementid(cntTargetSync.getElementid());

                // 获取平台的子网元发布信息列表
                List<CntTargetSync> childTargetSyncList = cntTargetSyncDS
                        .getcntSynnormaltargetListBycond(childTargetSync);

                if (childTargetSyncList.size() < 1)
                {
                    // 删除主平台关联发布信息
                    cntPlatformSyncDS.removeCntPlatformSync(cntPlatformSync);
                    // 写操作日志
                    CommonLogUtil.insertOperatorLog(cntTargetSync.getObjectid()+","+targetsystem.getTargetid(), SeriesConfig.SERIES_MGT,
                            SeriesConfig.SERIES_BIND_CONTENT_DEL_PALTFORM, SeriesConfig.SERIES_BIND_CONTENT_DEL_PALTFORM_INF,
                            SeriesConfig.RESOURCE_OPERATION_SUCCESS);
                    // 更新电视剧与其他对象的mapping是否有关联平台发布，没有就删除它们之间的mapping关系（暂不实现）
                    
                }
                else
                {
                    // 更改主平台关联发布状态
                    int[] childTargetSyncStatus = new int[childTargetSyncList.size()];
                    for (int j = 0; j < childTargetSyncList.size(); j++)
                    {
                        childTargetSyncStatus[j] = childTargetSyncList.get(j).getStatus();
                    }
                    cntPlatformSync.setStatus(StatusTranslator.getPlatformSyncStatus(childTargetSyncStatus));
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }
            }
        }
        return true;
    }
    private int findFirTargettype(List<CntTargetSync> seriesTargetSyncList,int optype ) throws DomainServiceException{
        int type=99;
        for (CntTargetSync targetSync : seriesTargetSyncList){
            int status = targetSync.getStatus();
            if(optype == 0){//0表示新增
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 0 || status == 400 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 0 || status == 400 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 0 || status == 400 || status == 500))type =2;
            }else if(optype ==1){//1表示修改
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 3 && (status == 300 || status == 500))type =3;
                if(targettype == 1 && (type==99 || type==2) && (status == 300 || status == 500))type =1;
                if(targettype == 2 && type ==99 && (status == 300 || status == 500))type =2;
            }else{//2表示取消（删除）
                Targetsystem target = new Targetsystem();
                target.setTargetindex(targetSync.getTargetindex());
                target = targetSystemDS.getTargetsystem(target);                        
                int targettype = target.getTargettype();
                if(targettype == 2 && (status == 300 || status == 500))type =2;
                if(targettype == 1 && (type==99 || type==3) && (status == 300 || status == 500))type =1;
                if(targettype == 3 && type ==99 && (status == 300 || status == 500))type =3;
            }
        }
        return type;
    }
    private void settargettype(CntTargetSync targetSync,int targettype,String optype){
        if(targettype == 3)  targetSync.setReserve02(optype+",3");
        if(targettype == 1)  targetSync.setReserve02(optype+",1");
        if(targettype == 2)  targetSync.setReserve02(optype+",2");
        
    }

}

<%@page language="java" session="true" contentType="text/html"%>

<html>
	<head>
		<LINK href="inc/style.css" rel=STYLESHEET type=text/css>
		<title>Service Menu</title>
		<base target="main">
	</head>
<%
	String myServname = request.getHeader("host");
	String opername = (String) session.getAttribute("OPERNAME");
	if (opername == null) {
	    System.out.println("[menu.jsp] Not found OPERNAME in session.");
%>
	<body background="images/left2.gif" leftmargin="0" topmargin="0"
		marginwidth="0" marginheight="0">
		<table border="0" cellPadding="0" cellSpacing="0" width="100%"
			background="images/left1.gif">
			<tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<td height="26">
					<a href="login.jsp" style="COLOR: #606060"> Login</a>
				</td>
			</tr>
			<tr>
		</table>
	</body>
<%
	} else {
	    System.out.println("[menu.jsp] OPERNAME in session is:" + opername);
%>
	<body background="images/left2.gif" leftmargin="0" topmargin="0"
		marginwidth="0" marginheight="0"
		onunload="window.open('exit.jsp','up','width=2,height=1')">
		<table border="0" cellPadding="0" cellSpacing="0" width="100%"
			background="images/left1.gif">
			<tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Logout" height="26">
					<A href="logout.jsp" style="COLOR: #606060" target="_top">Logout</A>
				</TD>
			</tr>

			<tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Pool Monitor" height="26">
					<A href="monitor/poolmon.jsp?cmd=99" style="COLOR: #606060">ZXDB Pool Monitor</A>
				</TD>
			</tr>
			
			<tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="DB Pool Data" height="26">
					<A href="monitor/ftpmonitor.jsp?cmd=99" style="COLOR: #606060">FTP Pool Monitor</A>
				</TD>
			</tr>
			
			<tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="DB Pool Data" height="26">
					<A href="monitor/r01monitor.jsp?cmd=99" style="COLOR: #606060">R01 Monitor</A>
				</TD>
			</tr>
			
			<!-- tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Socket Pool" height="26">
					<A href="monitor/sockmon.jsp?cmd=99" style="COLOR: #606060">Socket Pool</A>
				</TD>
			</tr -->

			<!--  tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Test Database" height="26">
					<A href="monitor/dbtest.jsp" style="COLOR: #606060">Test
						Database</A>
				</TD>
			</tr -->

			<!-- tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Test Database" height="26">
					<A href="monitor/showversion.jsp" style="COLOR: #606060">Show
						Version</A>
				</TD>
			</tr-->
			
			<!-- tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="Change Password" height="26">
					<A href="monitor/admpass.jsp" style="COLOR: #606060">Change
						Password</A>
				</TD>
			</tr -->
			
			<!-- tr>
				<td height="26" width="30">
					<div align="center">
						<img src="images/dot.gif" width="9" height="9" border="0">
					</div>
				</td>
				<TD title="chanage zxlogin passwd" height="26">
					<A href="monitor/chzxloginpwd.jsp" style="COLOR: #606060">chanage
						zxlogin passwd</A>
				</TD>
			</tr -->
			
		</table>
	</body>
<%
	}
%>
</html>

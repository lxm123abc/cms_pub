package com.zte.cms.content.series.service;

import java.util.List;

import com.zte.cms.common.Generator;
import com.zte.cms.common.ObjectType;
import com.zte.cms.content.series.common.SeriesConfig;
import com.zte.cms.content.series.dao.ICmsSeriesDAO;
import com.zte.cms.content.series.model.CmsSeries;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsSeriesDS extends DynamicObjectBaseDS implements ICmsSeriesDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsSeriesDAO dao = null;

    public void setDao(ICmsSeriesDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsSeries(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("insert cmsSeries starting...");
        try
        {
            Long seriesindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_series_index");
            String seriesid = Generator.getContentId(Long.parseLong(cmsSeries.getCpid()), ObjectType.SERI);
            cmsSeries.setSeriesindex(seriesindex);
            cmsSeries.setSeriesid(seriesid);
            cmsSeries.setCpcontentid(seriesid);
            dao.insertCmsSeries(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsSeries end");
    }

    public void updateCmsSeries(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("update cmsSeries by pk starting...");
        try
        {
            dao.updateCmsSeries(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSeries by pk end");
    }

    public void updateCmsSeriesList(List<CmsSeries> cmsSeriesList) throws DomainServiceException
    {
        log.debug("update cmsSeriesList by pk starting...");
        if (null == cmsSeriesList || cmsSeriesList.size() == 0)
        {
            log.debug("there is no datas in cmsSeriesList");
            return;
        }
        try
        {
            for (CmsSeries cmsSeries : cmsSeriesList)
            {
                dao.updateCmsSeries(cmsSeries);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsSeriesList by pk end");
    }

    public void removeCmsSeries(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("remove cmsSeries by pk starting...");
        try
        {
            dao.deleteCmsSeries(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSeries by pk end");
    }

    public void removeCmsSeriesList(List<CmsSeries> cmsSeriesList) throws DomainServiceException
    {
        log.debug("remove cmsSeriesList by pk starting...");
        if (null == cmsSeriesList || cmsSeriesList.size() == 0)
        {
            log.debug("there is no datas in cmsSeriesList");
            return;
        }
        try
        {
            for (CmsSeries cmsSeries : cmsSeriesList)
            {
                dao.deleteCmsSeries(cmsSeries);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsSeriesList by pk end");
    }

    public CmsSeries getCmsSeries(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("get cmsSeries by pk starting...");
        CmsSeries rsObj = null;
        try
        {
            rsObj = dao.getCmsSeries(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSeriesList by pk end");
        return rsObj;
    }

    public List<CmsSeries> getCmsSeriesByCond(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("get cmsSeries by condition starting...");
        List<CmsSeries> rsList = null;
        try
        {
            rsList = dao.getCmsSeriesByCond(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsSeries by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsSeries page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSeries page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsSeries cmsSeries, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get cmsSeries page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsSeries, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSeries page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryForPage(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsSeries page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryForPage(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSeries page info by condition end");
        return tableInfo;
    }

    public CmsSeries getCmsSeriesById(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("getCmsSeriesById starting...");
        CmsSeries rsObj = null;
        try
        {
            rsObj = dao.getCmsSeriesById(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("getCmsSeriesById end");
        return rsObj;
    }
    
    public TableDataInfo pageInfoQueryPlatformSync(CmsSeries cmsSeries, int start, int pageSize)
    throws DomainServiceException
    {
        log.debug("get cmsSeriesPlatformSync info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryPlatformSync(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsSeriesPlatformSync info by condition end");
        return tableInfo;
    }

    public TableDataInfo pageInfoQueryCatSerMapPlatform(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {

        log.debug("get pageInfoQueryCatSerMapPlatform info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryCatSerMapPlatform(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get pageInfoQueryCatSerMapPlatform info by condition end");
        return tableInfo;

    }

    public TableDataInfo pageInfoQueryCatSerMapTarget(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get pageInfoQueryCatSerMapTarget info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryCatSerMapTarget(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get pageInfoQueryCatSerMapTarget info by condition end");
        return tableInfo;    
    }

    public TableDataInfo pageInfoQuerySerCastMapPlatform(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get pageInfoQuerySerCastMapPlatform info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySerCastMapPlatform(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get pageInfoQuerySerCastMapPlatform info by condition end");
        return tableInfo;  
    }

    public TableDataInfo pageInfoQuerySerCastMapTarget(CmsSeries cmsSeries, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get pageInfoQuerySerCastMapTarget info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuerySerCastMapTarget(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get pageInfoQuerySerCastMapTarget info by condition end");
        return tableInfo;  
    }
    
    public TableDataInfo pageInfoQueryTargetSync(CmsSeries cmsSeries, int start, int pageSize)
    throws DomainServiceException
    {
        log.debug("get pageInfoQueryTargetSync info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryTargetSync(cmsSeries, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsSeries>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get pageInfoQueryTargetSync info by condition end");
        return tableInfo;
    }

    public List<CmsSeries> getSeriesCastTypeName(CmsSeries cmsSeries) throws DomainServiceException
    {
        log.debug("query roletype and castname starting...");
        List<CmsSeries> rsList = null;
        try
        {
            rsList = dao.getSeriesCastTypeName(cmsSeries);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query roletype and castname  end");
        return rsList;
    }
}

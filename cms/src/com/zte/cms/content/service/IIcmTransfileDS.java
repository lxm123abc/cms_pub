package com.zte.cms.content.service;

import java.util.List;
import com.zte.cms.content.model.IcmTransfile;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IIcmTransfileDS
{
    /**
     * 新增IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public void insertIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 批量更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTransfileList(List<IcmTransfile> icmTransfileList) throws DomainServiceException;

    /**
     * 根据条件更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 根据条件批量更新IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateIcmTransfileListByCond(List<IcmTransfile> icmTransfileList) throws DomainServiceException;

    /**
     * 删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 批量删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTransfileList(List<IcmTransfile> icmTransfileList) throws DomainServiceException;

    /**
     * 根据条件删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 根据条件批量删除IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTransfileListByCond(List<IcmTransfile> icmTransfileList) throws DomainServiceException;

    /**
     * 查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @return IcmTransfile对象
     * @throws DomainServiceException ds异常
     */
    public IcmTransfile getIcmTransfile(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 根据条件查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象
     * @return 满足条件的IcmTransfile对象集
     * @throws DomainServiceException ds异常
     */
    public List<IcmTransfile> getIcmTransfileByCond(IcmTransfile icmTransfile) throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询IcmTransfile对象
     * 
     * @param icmTransfile IcmTransfile对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTransfile icmTransfile, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;
}
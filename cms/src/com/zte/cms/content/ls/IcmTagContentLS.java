package com.zte.cms.content.ls;

import java.util.List;

import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.common.TagLogConstant;
import com.zte.cms.content.model.IcmTag;
import com.zte.cms.content.model.IcmTagContent;
import com.zte.cms.content.service.IIcmTagContentDS;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;


public class IcmTagContentLS implements IIcmTagContentLS
{

    private IIcmTagContentDS cmTagContentDS;
    private IIcmTagLS cmTagLS;
    private Log log = SSBBus.getLog(getClass());
    private static final String MES_HAS_DEL_RELATION = "1:此内容标签关联已经被删除";
    private static final String MES_TAG_HAS_DEL = "该内容标签已经被删除";
    private static final String OPER_SUCCESS = "0:操作成功";

    public void setCmTagLS(IIcmTagLS cmTagLS)
    {
        this.cmTagLS = cmTagLS;
    }

    public void setCmTagContentDS(IIcmTagContentDS cmTagContentDS)
    {
        this.cmTagContentDS = cmTagContentDS;
    }

    /**
     * 删除内容关联
     * 
     * @param tagindex 内容标签主键
     * @param contentid 内容编码
     * @return 操作返回值
     */
    public String removeRelationByTindexAndConid(Long tagindex, String contentid) throws DomainServiceException
    {
        log.debug("removeRelationByTindexAndConid starting...");
        String message = "";
        try
        {
            IcmTagContent icmTagContent = new IcmTagContent();
            icmTagContent.setTagindex(tagindex);
            icmTagContent.setContentid(contentid);
            List<IcmTagContent> icmTagContentList = cmTagContentDS.getIcmTagContentByCond(icmTagContent);
            // 判断关联是否已经被删除
            if (icmTagContentList != null && icmTagContentList.size() > 0)
            {
                cmTagContentDS.removeIcmTagContent(icmTagContentList.get(0));
                message = OPER_SUCCESS;
                CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                        TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL, TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL_INFO,
                        TagLogConstant.RESOURCE_OPERATION_SUCCESS);

            }
            else
            {
                message = MES_HAS_DEL_RELATION;
            }

        }
        catch (DomainServiceException e)
        {
            log.error("removeRelationByTindexAndConid exception :" + e);
            throw e;
        }
        log.debug("removeRelationByTindexAndConid end");
        return message;

    }

    /**
     * 
     * 新增内容关联
     * 
     * @param tagindex 关联index
     * @param contentids 内容ID数组
     * @return 操作返回值
     */
    public String insertNewContentByIndexAndConID(Long tagindex, String[] contentids) throws DomainServiceException
    {
        log.debug("insert New Content By Index And ConID starting...");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        int successCount = 0;
        int failCount = 0;
        int exceptionCount = 0;
        try
        {
            IcmTag icmTag = new IcmTag();
            icmTag.setTagindex(tagindex);
            icmTag = cmTagLS.getIcmTag(icmTag);
            // 判断内容标签是否已经被删除
            if (icmTag == null)
            {
                returnInfo.setFlag("1");
                returnInfo.setReturnMessage(MES_TAG_HAS_DEL);
                return returnInfo.toString();
            }
            else
            {
                IcmTagContent icmTagCont = null;
                List<IcmTagContent> tagContentList = null;
                // 循环插入关联关系到内容标签---内容关联表中。
                for (int i = 0; i < contentids.length; i++)
                {
                    try
                    {
                        icmTagCont = new IcmTagContent();
                        icmTagCont.setTagindex(tagindex);
                        icmTagCont.setContentid(contentids[i]);
                        // 根据条件查询内容关联是否已经存在
                        tagContentList = cmTagContentDS.getIcmTagContentByCond(icmTagCont);
                        if (tagContentList != null && tagContentList.size() > 0)
                        {
                            operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "关联内容标签失败", "该关联已经存在");
                            returnInfo.appendOperateInfo(operateInfo);
                            failCount++;
                        }
                        else
                        {
                            cmTagContentDS.insertIcmTagContent(icmTagCont);
                            operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "关联内容标签成功", "操作成功");
                            returnInfo.appendOperateInfo(operateInfo);
                            successCount++;
                        }
                    }
                    catch (DomainServiceException e)
                    {
                        // 插入数据库异常则继续循环。
                        log.error("insertNewContentByIndexAndConID error while inserting exception:" + e);
                        operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "关联内容标签异常", "插入数据库异常");
                        exceptionCount++;
                        continue;
                    }
                }

            }
        }
        catch (Exception e)
        {
            log.error("insert New Content By Index And ConID exception:" + e);
            returnInfo = new ReturnInfo();
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage("系统错误！");
        }
        // returnInfo.setReturnMessage("关联内容 成功" + successCount + " 失败 "
        // + failCount);
        // 判断是全部成功，全部失败还是部分成功。 0：全部成功；1：全部失败；2：部分成功。
        if ((successCount + exceptionCount) == contentids.length)
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage("成功数量：" + successCount);
            CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                    TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_ADD, TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_ADD_INFO,
                    TagLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        else
        {
            if ((failCount + exceptionCount) == contentids.length)
            {
                returnInfo.setReturnMessage("失败数量：" + failCount);
                returnInfo.setFlag("1");
            }
            else
            {
                returnInfo.setFlag("2");
                returnInfo.setReturnMessage("成功数量：" + successCount + " 失败数量：" + failCount);
                CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                        TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_ADD, TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_ADD_INFO,
                        TagLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
        }
        log.error("insert New Content By Index And ConID end");
        return returnInfo.toString();
    }

    /**
     * 批量删除内容关联记录
     * 
     * @param tagindex 内容标签主键
     * @param contentids 内容ID数组
     * @return 操作返回值
     */
    public String removeBatchRelationByTagindexAndConIDs(Long tagindex, String[] contentids)
            throws DomainServiceException
    {
        log.debug("remove Batch Relation By Tagindex And ConIDs startind...");
        int successCount = 0;
        int failCount = 0;
        int exception = 0;
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        try
        {
            IcmTag icmTag = new IcmTag();
            icmTag.setTagindex(tagindex);
            icmTag = cmTagLS.getIcmTag(icmTag);
            // 判断内容标签是否已经被删除
            if (icmTag == null)
            {
                returnInfo.setFlag("1");
                returnInfo.setReturnMessage(MES_TAG_HAS_DEL);
                return returnInfo.toString();
            }
            else
            {
                List<IcmTagContent> tagContentList = null;
                IcmTagContent icmTagContent = null;
                for (int i = 0; i < contentids.length; i++)
                {
                    try
                    {
                        icmTagContent = new IcmTagContent();
                        icmTagContent.setTagindex(tagindex);
                        icmTagContent.setContentid(contentids[i]);
                        // 根据条件查询关联关系。
                        tagContentList = cmTagContentDS.getIcmTagContentByCond(icmTagContent);
                        if (tagContentList == null || tagContentList.size() < 1)
                        {
                            operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "删除关联失败", "该关联已经被删除");
                            returnInfo.appendOperateInfo(operateInfo);
                            failCount++;
                        }
                        else
                        {
                            cmTagContentDS.removeRelationByTagindexAndConIDs(tagindex, contentids[i]);
                            operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "删除关联成功", "操作成功");
                            returnInfo.appendOperateInfo(operateInfo);
                            successCount++;

                        }
                    }
                    catch (DomainServiceException e)
                    {
                        // 删除数据库数据异常则继续循环
                        log.error("removeBatchRelationByTagindexAndConIDs while deleting exception:" + e);
                        operateInfo = new OperateInfo(String.valueOf(i + 1), contentids[i], "删除关联异常", "删除数据异常");
                        returnInfo.appendOperateInfo(operateInfo);
                        exception++;
                        continue;
                    }

                }
            }
        }
        catch (Exception e)
        {
            log.error("remove Batch Relation By Tagindex And ConIDs exception:" + e);
            returnInfo = new ReturnInfo();
            returnInfo.setFlag("1");
            returnInfo.setReturnMessage("系统错误");
            return returnInfo.toString();
        }
        // returnInfo.setReturnMessage("删除内容标签关联 成功" + successCount + " 失败 "
        // + failCount);
        // 判断操作时全部成功，全部失败还是部分成功。 0：全部成功；1：全部失败；2：部分成功。
        if ((successCount + exception) == contentids.length)
        {
            returnInfo.setFlag("0");
            returnInfo.setReturnMessage("成功数量：" + successCount);
            CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                    TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL, TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL_INFO,
                    TagLogConstant.RESOURCE_OPERATION_SUCCESS);
        }
        else
        {
            if ((failCount + exception) == contentids.length)
            {
                returnInfo.setFlag("1");
                returnInfo.setReturnMessage("失败数量：" + failCount);
            }
            else
            {
                returnInfo.setFlag("2");
                returnInfo.setReturnMessage("成功数量：" + successCount + " 失败数量：" + failCount);
                CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                        TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL, TagLogConstant.OPERTYPE_LOG_TAG_CONTENT_DEL_INFO,
                        TagLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
        }

        log.debug("remove Batch Relation By Tagindex And ConIDs end");
        return returnInfo.toString();
    }
}

package com.zte.cms.content.program.programsync.dao;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IProgramTargetSynDAO
{ 
    /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param programTargetSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
    public ProgramTargetSyn getProgramTargetSyn( ProgramTargetSyn programTargetSyn )throws DAOException;	
     /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param programTargetSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
     public List<ProgramTargetSyn> getProgramTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException;

     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param programTargetSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询CntTargetsysSyn对象，作为查询条件的参数
	  *
	  * @param programTargetSyn CntTargetsysSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     /**
	  * 根据条件分页查询programTargetSyn对象，作为查询条件的参数
	  *
	  * @param programTargetSyn programTargetSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV3( ProgramTargetSyn programTargetSyn )throws DAOException;

     /**
	  * 根据条件分页查询programTargetSyn对象，作为查询条件的参数
	  *
	  * @param programTargetSyn programTargetSyn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV2( ProgramTargetSyn programTargetSyn )throws DAOException;
     /**
	  * 根据条件查询CntTargetsysSyn对象 
	  *
	  * @param programTargetSyn CntTargetsysSyn对象
	  * @return 满足条件的CntTargetsysSyn对象集
	  * @throws DAOException dao异常
	  */
     public List<ProgramTargetSyn> getProTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DAOException;
     
     public PageInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DAOException;
}
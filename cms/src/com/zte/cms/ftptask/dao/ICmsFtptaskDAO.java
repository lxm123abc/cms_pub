package com.zte.cms.ftptask.dao;

import java.util.List;

import com.zte.cms.ftptask.model.CmsFtptask;
import com.zte.cms.ftptask.model.CmsFtptaskAndHis;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICmsFtptaskDAO
{
    /**
     * 新增CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void insertCmsFtptask(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 新增CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void insertCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException;

    /**
     * 根据主键更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void updateCmsFtptask(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 根据主键更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void updateCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException;

    /**
     * 根据条件更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 根据条件更新CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask更新条件
     * @throws DAOException dao异常
     */
    public void updateCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DAOException;

    /**
     * 根据主键删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void deleteCmsFtptask(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 根据主键删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @throws DAOException dao异常
     */
    public void deleteCmsFtptaskList(List<CmsFtptask> cmsFtptaskList) throws DAOException;

    /**
     * 根据条件删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 根据条件删除CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask删除条件
     * @throws DAOException dao异常
     */
    public void deleteCmsFtptaskListByCond(List<CmsFtptask> cmsFtptaskList) throws DAOException;

    /**
     * 根据主键查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @return 满足条件的CmsFtptask对象
     * @throws DAOException dao异常
     */
    public CmsFtptask getCmsFtptask(CmsFtptask cmsFtptask) throws DAOException;

    /**
     * 根据条件查询CmsFtptask对象
     * 
     * @param cmsFtptask CmsFtptask对象
     * @return 满足条件的CmsFtptask对象集
     * @throws DAOException dao异常
     */
    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask) throws DAOException;

    public List<CmsFtptask> getCmsFtptaskByCond(CmsFtptask cmsFtptask, PageUtilEntity puEntity) throws DAOException;

    /**
     * 根据条件分页查询CmsFtptask对象，作为查询条件的参数
     * 
     * @param cmsFtptask CmsFtptask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsFtptask对象，作为查询条件的参数
     * 
     * @param cmsFtptask CmsFtptask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsFtptask cmsFtptask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public void deleteCmsFtptaskHis(CmsFtptask cmsFtptask) throws DAOException;

    public CmsFtptask getCmsFtptaskHis(CmsFtptask cmsFtptask) throws DAOException;

    public PageInfo pageCmsFtptaskHisInfoQuery(CmsFtptaskAndHis cmsFtptaskHis, int start, int pageSize)
            throws DAOException;

}
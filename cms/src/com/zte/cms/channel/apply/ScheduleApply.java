package com.zte.cms.channel.apply;

import java.text.SimpleDateFormat;

import java.util.List;

import com.zte.cms.channel.ls.ChannelConstants;

import com.zte.cms.channel.model.CmsSchedule;

import com.zte.cms.channel.model.ScheduleWkfwhis;

import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.channel.service.IScheduleWkfwhisDS;
import com.zte.cms.channel.wkfw.ls.IScheduleWkfwCheck;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.wkfw.WorkflowBase;
import com.zte.umap.common.wkfw.WorkflowService;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleApply extends WorkflowBase implements IScheduleApply {
    protected IScheduleWkfwCheck scheduleWkfwCheckLS = null;
    protected IScheduleWkfwhisDS scheduleWkfwhisDS;
    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
    private IUsysConfigDS usysConfigDS = null;
    private Log log = SSBBus.getLog(getClass());
    private ICmsScheduleDS scheduleds = null;
    private String callbackProcess;
    
    public String getCallbackProcess()
    {
        return callbackProcess;
    }

    public void setCallbackProcess(String callbackProcess)
    {
        this.callbackProcess = callbackProcess;
    }

    // 构造方法
	ScheduleApply() {
		ResourceMgt
				.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }   
	
	public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }


    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }


    public IScheduleWkfwCheck getScheduleWkfwCheckLS() {
        return scheduleWkfwCheckLS;
    }

	public void setScheduleWkfwCheckLS(IScheduleWkfwCheck scheduleWkfwCheckLS) {
        this.scheduleWkfwCheckLS = scheduleWkfwCheckLS;
    }

	public IScheduleWkfwhisDS getScheduleWkfwhisDS() {
        return scheduleWkfwhisDS;
    }

	public void setScheduleWkfwhisDS(IScheduleWkfwhisDS scheduleWkfwhisDS) {
        this.scheduleWkfwhisDS = scheduleWkfwhisDS;
    }

	public ICmsScheduleDS getScheduleds() {
        return scheduleds;
    }

	public void setScheduleds(ICmsScheduleDS scheduleds) {
        this.scheduleds = scheduleds;
    }

    /**
     * @param ScheduleWkfwhis ScheduleWkfwhis
     * @return ScheduleWkfwhis
     * @throws Exception Exception
     */
    // 查询历史表
	public ScheduleWkfwhis getScheduleWkfwHis(ScheduleWkfwhis scheduleWkfwhis)
			throws Exception {
		log
				.debug("get ScheduleWkfwhis by workflwoindex and scheduleindex and nodeid starting....");
        List<ScheduleWkfwhis> rsList = null;
		try {
			rsList = scheduleWkfwhisDS
					.getScheduleWkfwhisByCond(scheduleWkfwhis);
		} catch (Exception e) {
            throw new Exception(e.getMessage());

        }

		if (rsList != null && rsList.size() > 0) {
			log
					.debug("get scheduleWkfwHis by workflwoindex and scheduleindex and nodeid ends");
            return rsList.get(0);
        }
        return null;
    }

    /**
     * @param scheduleIdx Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
	public String apply(Long scheduleIdx) throws DomainServiceException {

        CmsSchedule ch = new CmsSchedule();
        ch.setScheduleindex(scheduleIdx);
        ch = scheduleds.getCmsSchedule(ch);
		if (ch != null) {
		        
		    String checkResult = scheduleWkfwCheckLS
					.checkScheduleWkfw(scheduleIdx);
			if (checkResult.charAt(0) != '0') {
                return checkResult;
            }
			// 查看是否免审
            int flag = 0;
            UsysConfig usysConfig = new UsysConfig();
            List<UsysConfig> usysConfigList = null;
            usysConfig.setCfgkey("cms.schedule.noaudit.switch");
            usysConfigList = usysConfigDS.getUsysConfigByCond(usysConfig);
            if (null != usysConfigList && usysConfigList.size() > 0)
            {
                usysConfig = usysConfigList.get(0);
                flag = Integer.parseInt(usysConfig.getCfgvalue());
            }           
            if(flag==0){
                    // 免审
                ch.setStatus(0);       // 审核通过的
                ch.setWorkflow(0);
                ch.setWorkflowlife(0);
                scheduleds.updateCmsSchedule(ch);
                return ResourceMgt.findDefaultText("this.schedule.noaudit.set");
            }
            
            CmsSchedule schedule = new CmsSchedule();
            schedule.setScheduleindex(scheduleIdx);
            schedule = scheduleds.getCmsSchedule(schedule);
            return apply(schedule, "1", null);
		} else {
            return ResourceMgt.findDefaultText("this.direct.programe.not.exist");//"1:此直播节目已不存在"
        }
    }

    /**
     * @param channel CmsChannel
     * @param templateRoute String
     * @param priority String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
	public String apply(CmsSchedule schedule, String templateRoute,
			String priority) throws DomainServiceException {

        schedule.setStatus(ChannelConstants.WKFW_STATUS_AUDITING); // 120是审核中
        schedule.setWorkflow(1); // 新增
        schedule.setWorkflowlife(0);

        // 插入正式表，判断是否已经存在
		try {
            scheduleds.updateCmsSchedule(schedule);           
		} catch (DomainServiceException e) {
            throw new DomainServiceException(e.getMessage());
        }
        // 发起工作流参数
		String pboName = schedule.getProgramname() + "["
				+ schedule.getScheduleid() + "]";
        String pboId = String.valueOf(schedule.getScheduleindex());
        String pboType = "CMS_SCHEDULE_A"; // ChannelConstants.getWorkflowType(channel.getWorkflow());
        String processName = pboType + ":" + pboName;

        String arg6 = "step1," + templateRoute; // 工作流模板路由选择

        String callbackBean = "";
		if (null != this.getCallbackProcess()) {
            callbackBean = "callbackProcess," + this.getCallbackProcess();
            arg6 = arg6 + "|" + callbackBean; // 回调bean

        }

        WorkflowService wkflService = new WorkflowService();
        String tempcode = this.getTemplateCode();
		int flag = wkflService.startProcessInstance(this.getTemplateCode(),
				processName, pboId, pboName, pboType, "", arg6, priority);
		if (flag <= 0) {
            throw new DomainServiceException();
        }
		return ResourceMgt.findDefaultText("programe.add.audit.push.success");//return "0:节目新增审核提交成功"
        // 频道新增审核提交成功
    }

    /**
     * @param Schedule CmsSchedule
     * @param taskId String
     * @param handleMsg String
     * @param handleEvent String
     * @param workflowStatusCode String
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
	public String audit(CmsSchedule schedule, String taskId, String handleMsg,
			String handleEvent, String workflowStatusCode)
			throws DomainServiceException {

		String checkResult = scheduleWkfwCheckLS
				.checkScheduleWkfwAudit(schedule.getScheduleindex());
		if (checkResult.charAt(0) != '0') {
            return checkResult;
        }

        // 修改正式表
		if (schedule != null && schedule.getScheduleindex() != null) {
            scheduleds.updateCmsSchedule(schedule);
        }

        // 工作流处理
        WorkflowService wkflService = new WorkflowService();
        wkflService.completeTask(taskId, handleMsg, handleEvent);
		
        return ResourceMgt.findDefaultText("programe.add.audit.success");//"0:节目新增审核成功" 
       
		//        if (workflowStatusCode.equals("1"))
		//        {// 审核
		//            return "0:节目新增审核成功" ;
		//            // 频道新增审核成功
		//        }
		//        else
		//        {
		//            return "0:节目新增审核成功";
		//            // 频道新增审核成功
		//        }
    }

}
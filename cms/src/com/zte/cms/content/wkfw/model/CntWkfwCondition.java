package com.zte.cms.content.wkfw.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

/**
 * <p>
 * 文件名称: CntWkfwCondition.java
 * </p>
 * <p>
 * 文件描述: 内容审核查询条件类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2010-2020
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: 表示内容审核查询条件对象
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 完成日期：2011年02月26日
 * </p>
 * <p>
 * 修改记录1:
 * </p>
 * 
 * <pre>
 *    修改日期：
 *    版 本 号：
 *    修 改 人：
 *    修改内容：
 * </pre>
 * 
 * @version ZXIN10 CMSV2.01.01
 * @author 李梅
 */

public class CntWkfwCondition extends DynamicBaseObject
{
    /**
     * 进程创建者
     */
    private java.lang.String creator;
    /**
     * 进程启动时间开始时间
     */
    private java.util.Date processStartTimeStart;
    /**
     * 进程启动时间结束时间
     */
    private java.util.Date processStartTimeEnd;
    /**
     * 工作流类型
     */
    private java.lang.String pboType;
    /**
     * 工作流任务优先级
     */
    private java.lang.String priority;
    /**
     * 参与者ID
     */
    private java.lang.String executorId;
    /**
     * 单据状态代码
     */
    private java.lang.String nodeStatusCode;
    /**
     * 任务状态
     */
    private java.lang.String taskStatus;
    /**
     * 进程状态
     */
    private java.lang.String processStatus;
    /**
     * 接受状态
     */
    private java.lang.String acceptStatus;
    /**
     * 审核节点名称
     */
    private java.lang.String nodeName;
    /**
     * 权限控制：当前操作员有权操作的工作流节点，为逗号隔开的字符串
     */
    private java.lang.String purviewNodeName;
    /**
     * 权限控制：当前操作员有权操作的工作流类型，为逗号隔开的字符串
     */
    private java.lang.String purviewWorkflow;

    private java.lang.String taskids;

    /**
     * 当前登陆的操作员id
     */
    private Long operid;

    /**
     * 当前登陆的操作员id
     */
    private String opername;

    /**
     * 当前登陆的操作员所属权限组
     */
    private String opergrpid;

    /**
     * 内容编码
     */
    private java.lang.String contentid;
    /**
     * 内容名称
     */
    private java.lang.String contentname;

    /**
     * CPID
     */
    private java.lang.String cpid;
    /**
     * CP名称
     */
    private java.lang.String cpcnshortname;
    
    /**
     * 当前时查询几审的flag：1-一审，2-二审，3-三审
     */
    private java.lang.Integer aduitflag;
    

    public java.lang.Integer getAduitflag()
    {
        return aduitflag;
    }

    public void setAduitflag(java.lang.Integer aduitflag)
    {
        this.aduitflag = aduitflag;
    }

    public Long getOperid()
    {
        return operid;
    }

    public void setOperid(Long operid)
    {
        this.operid = operid;
    }

    public String getOpername()
    {
        return opername;
    }

    public void setOpername(String opername)
    {
        this.opername = opername;
    }

    public String getOpergrpid()
    {
        return opergrpid;
    }

    public void setOpergrpid(String opergrpid)
    {
        this.opergrpid = opergrpid;
    }

    public java.lang.String getCreator()
    {
        return creator;
    }

    public void setCreator(java.lang.String creator)
    {
        this.creator = creator;
    }

    public java.util.Date getProcessStartTimeStart()
    {
        return processStartTimeStart;
    }

    public void setProcessStartTimeStart(java.util.Date processStartTimeStart)
    {
        this.processStartTimeStart = processStartTimeStart;
    }

    public java.util.Date getProcessStartTimeEnd()
    {
        return processStartTimeEnd;
    }

    public void setProcessStartTimeEnd(java.util.Date processStartTimeEnd)
    {
        this.processStartTimeEnd = processStartTimeEnd;
    }

    public java.lang.String getPboType()
    {
        return pboType;
    }

    public void setPboType(java.lang.String pboType)
    {
        this.pboType = pboType;
    }

    public java.lang.String getPriority()
    {
        return priority;
    }

    public void setPriority(java.lang.String priority)
    {
        this.priority = priority;
    }

    public java.lang.String getExecutorId()
    {
        return executorId;
    }

    public void setExecutorId(java.lang.String executorId)
    {
        this.executorId = executorId;
    }

    public java.lang.String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(java.lang.String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public java.lang.String getProcessStatus()
    {
        return processStatus;
    }

    public void setProcessStatus(java.lang.String processStatus)
    {
        this.processStatus = processStatus;
    }

    public java.lang.String getAcceptStatus()
    {
        return acceptStatus;
    }

    public void setAcceptStatus(java.lang.String acceptStatus)
    {
        this.acceptStatus = acceptStatus;
    }

    public java.lang.String getNodeStatusCode()
    {
        return nodeStatusCode;
    }

    public void setNodeStatusCode(java.lang.String nodeStatusCode)
    {
        this.nodeStatusCode = nodeStatusCode;
    }

    public java.lang.String getNodeName()
    {
        return nodeName;
    }

    public void setNodeName(java.lang.String nodeName)
    {
        this.nodeName = nodeName;
    }

    public java.lang.String getPurviewNodeName()
    {
        return purviewNodeName;
    }

    public void setPurviewNodeName(java.lang.String purviewNodeName)
    {
        this.purviewNodeName = purviewNodeName;
    }

    public java.lang.String getPurviewWorkflow()
    {
        return purviewWorkflow;
    }

    public void setPurviewWorkflow(java.lang.String purviewWorkflow)
    {
        this.purviewWorkflow = purviewWorkflow;
    }

    public java.lang.String getTaskids()
    {
        return taskids;
    }

    public void setTaskids(java.lang.String taskids)
    {
        this.taskids = taskids;
    }

    public java.lang.String getContentid()
    {
        return contentid;
    }

    public void setContentid(java.lang.String contentid)
    {
        this.contentid = contentid;
    }

    public java.lang.String getContentname()
    {
        return contentname;
    }

    public void setContentname(java.lang.String contentname)
    {
        this.contentname = contentname;
    }

    public java.lang.String getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.String cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.String getCpcnshortname()
    {
        return cpcnshortname;
    }

    public void setCpcnshortname(java.lang.String cpcnshortname)
    {
        this.cpcnshortname = cpcnshortname;
    }

    @Override
    /**
     * <p>
     * 设置变量跟表字段的映射关系，支持页面列表排序
     * </p>
     * 
     * @since ZXCMS
     */
    public void initRelation()
    {
        this.addRelation("processId", "process_id");
        this.addRelation("processName", "process_name");
        this.addRelation("creator", "creator");
        this.addRelation("pboId", "pbo_id");
        this.addRelation("pboName", "pbo_name");
        this.addRelation("pboType", "pbo_type");
        this.addRelation("priority", "priority");
        this.addRelation("taskId", "task_id");
        this.addRelation("taskStartTime", "task_start_time");
        this.addRelation("executorId", "executor_id");
        this.addRelation("nodeStatus", "node_status");
        this.addRelation("nodeStatusCode", "node_status_code");
        this.addRelation("taskStatus", "task_status");
        this.addRelation("processStatus", "process_status");
        this.addRelation("nodeName", "node_name");
        this.addRelation("acceptStatus", "accept_status");
        this.addRelation("processStartTime", "process_start_time");
        this.addRelation("contentid", "programid");
        this.addRelation("contentname", "namecn");
        this.addRelation("cpid", "cpid");
        this.addRelation("cpcnshortname", "cpcnshortname");
    }
}

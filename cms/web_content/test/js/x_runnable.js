
var taskeditcallback;
var taskarg0 = {};
var taskarg1 = {};
var taskarg2 = {};
var taskarg3 = {};
var taskarg4 = {};
var taskarg5 = {};
var taskarg6 = {};
var taskarg7 = {};
var taskarg8 = {};
var taskarg9 = {};
var taskresult = {};

function beginTask(pclass) {
	clearQueryHTML();
	buildQueryHTMLByTask(pclass);
	buildTaskButton(pclass);
	taskeditcallback = undefined;
}

function getTaskFromQueryCond(pclass) {
	var obj = {};
	obj.ptask = $("#queryBody").find("input[id='ptask']").val();
	obj.pclass = pclass;
	obj.pexpress = $("#queryBody").find("input[id='pexpress']").val();
	var args = {};
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		if (isEmpty(val) || is_numcode(val)) return false;
		args.temptemp = getJsonVal(pval);
		eval("args." + val + "=args.temptemp");
	});
	if (args.temptemp != undefined) args.temptemp = undefined;
	obj.pparam = args;
	return obj;
}

function getTaskTrObj(obj) {
	var trobj = {};
	$(obj).find("label[id^='lbl']").each(function(){
		var tid = "p" + $(this).attr("id").substring(3);
		trobj.temptemp = getJsonVal($(this).text());
		eval("trobj." + tid + "=trobj.temptemp");
	});
	if (trobj.temptemp != undefined) trobj.temptemp = undefined;
	return trobj;
}

function getTaskTrObjs() {
	var trobjs = [];
	var i = 0;
	$("#xtable").contents().find("input[id='trchk']").each(function(){
		var trobj = {};
		$(this).parent().parent().find("label[id^='lbl']").each(function(){
			var tid = "p" + $(this).attr("id").substring(3);
			trobj.temptemp = getJsonVal($(this).text());
			eval("trobj." + tid + "=trobj.temptemp");
		});
		if (trobj.temptemp != undefined) trobj.temptemp = undefined;
		trobjs[i++] = trobj;
	});
	return trobjs;
}

function doTaskTrNo() {
	$("#xtable").contents().find("table tr").each(function(i){
		$(this).find("td").eq(0).text(i-1);
	});
}

function doTaskInsert(pclass) {
	var obj = getTaskFromQueryCond(pclass);
	var isTaskInsert = false;
	if (!isTaskInsert) {
		$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
			$(this).parent().parent().after(buildTableTRHTMLByTask(obj));
			isTaskInsert = true;
		});
	}
	if (!isTaskInsert) {
		$("#xtable").contents().find("table tr:last").each(function(){
			$(this).after(buildTableTRHTMLByTask(obj));
			isTaskInsert = true;
		});
	}
	if (!isTaskInsert) {
		buildTableHTMLByTask(new Array(obj));
	}
	doTaskTrNo();
}

function doTaskEditCallback(pclass) {
	if (taskeditcallback == undefined) {
		alert("对象已经失效，请重新选择待修改项");
		return;
	}
	var obj = getTaskFromQueryCond(pclass);
	$(taskeditcallback).find("label[id^='lbl']").each(function(){
		var tid = "p" + $(this).attr("id").substring(3);
		$(this).text(createJsonStr(obj[tid]));
	});
}

function doTaskEdit() {
	taskeditcallback = undefined;
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		taskeditcallback = $(this).parent().parent();
		return false;
	});
	if (taskeditcallback == undefined) {
		return;
	}
	var trobj = getTaskTrObj(taskeditcallback);
	clearQueryHTML();
	buildQueryHTMLByTask(trobj.pclass, trobj.ptask, createJsonStr(trobj.pexpress));
	buildTaskButton(trobj.pclass, true);
	var args = trobj.pparam;
	for (var arg in args) {
		newQueryTask(true, true, false, arg, createJsonStr(args[arg]));
	}
}

function doTaskGC() {
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		$(this).parent().parent().remove();
	});
	doTaskTrNo();
}

function doTaskCopy() {
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		var trobj = getTaskTrObj($(this).parent().parent());
		$("#xtable").contents().find("table tr:last").after(buildTableTRHTMLByTask(trobj));
	});
	doTaskTrNo();
}

function doTaskCall(pid, ptask, pparam) {
	var params = "";
	var obj = {};
	for (var i=0; i<10; i++) {
		obj = pparam["arg" + i];
		if (typeof(obj) == "undefined") break;
		if (typeof(obj) == "string" && obj == "[]") obj = [];
		if (typeof(obj) == "string" && obj == "nvl") obj = null;
		eval("taskarg" + i + "=obj");
		params += ",taskarg" + i;
	}
	params = params.substr(1);
	taskresult = {};
	callSid(pid, isEmpty(params) ? null : '[' + params + ']', 'taskresult');
	buildQueryHTMLByTaskOut(pid, ptask, params, getDT(), "running", "0");
}

function doTaskRun() {
	var trobjs = getTaskTrObjs();
	var trobj = {};
	var str = "";
	for (var i=0; i<trobjs.length; i++) {
		str += "<?xml:namespace prefix=z ns=\"http://ria.zte.com.cn\"/>";
		str += "<z:service id='task" + i + "' url='" + trobjs[i].ptask + "' isSyn='false'>";
		str += "<z:response postResponse='doTaskResponse(" + i + ")'/>";
		str += "</z:service>";
	}
	document.getElementById("divhidden").innerHTML = str;

	var srvArr = pageContextCache.serviceArr;
	for (var i = 0; i < srvArr.length; i++) {
        if (srvArr[i].id.startWith("task")) {
			srvArr.splice(i, 1);
        }
    }
    var services = getTagValue(RIA_SERVICE_TAG, "");
    for (var i = 0; i < services.length; i++) {
        pageContextCache.serviceArr.push(encapsServiceTag(services[i]));
    }
	
	clearQueryHTML();
	buildQueryHTMLByTaskOut("", "task", "params", "start", "end", "consume");
	for (var i=0; i<trobjs.length; i++) {
		trobj = trobjs[i];
		if (trobj.pclass == "thread") {
			doTaskCall("task"+i, trobj.ptask, trobj.pparam);
		}
	}
	buildTaskOutButton();
}

function doTaskResponse(pid) {
	var tr = $("#queryBody").find("tr").eq(pid+1);
	var s = tr.find("td").eq(3).text();
	var t = getDT();
	tr.find("td").eq(4).text(t);
	tr.find("td").eq(5).text(t - s);
}

function clearTaskChart() {
	$("#divchart").html("");
	$("#divchart").removeClass();
}

function doTaskChart() {
	$("#divchart").addClass("jgtable {type:'lc', size:'520x370', lines:[['1','0','0']], axis_step:1," +
		" bg:'000000', bg_type:'gradient', bg_trasparency:90, grid:true, grid_x:4," +
		" grid_y:4, grid_line:2, grid_blank:2, colors:['5131C9'], filltop:''}");
	var str = "";
	str += "<table style=\"display:none\">";
	str += "<thead><tr><th></th><th class=\"serie\">consume curve</th></tr></thead>";
	str += "<tbody>";
	$("#queryBody").find("tr").each(function(){
		var tds = $(this).find("td");
		var cx = tds.eq(0).text();
		if (cx.startWith("task")) {
			str += "<tr><th class=\"serie\">" + cx + "</th>";
			str += "<td>" + tds.eq(5).text() + "</td></tr>";
		}
	});
	str += "</tbody>";
	str += "</table>";
	$("#divchart").html(str);
	$("#divchart").jgtable();
}

function saveRunnableObject() {
	var trobjs = getTaskTrObjs();
	$("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val(createJsonStr(trobjs));
}

function readRunnableObject() {
	var trobjs = getJsonVal($("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val());
	buildTableHTMLByTask(trobjs);
	doTaskTrNo();
}
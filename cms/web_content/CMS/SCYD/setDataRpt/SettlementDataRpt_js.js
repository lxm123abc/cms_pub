//上传excel
function importData() {
    //解析附件信息

    var filepath = document.getElementById("fake_uploadAttachment").value;
    if (filepath.length == 0) {
        alert("请选择要上传的excel文件");
        return;
    }
    if (confirm("是否确认上传文件?")) {
        callSid('excelImportMethod');
    }
}

function successInmport(msg) {
    alert(msg.data);
    reloadPage();
}

function successMsg(msg) {
    alert(msg.data);
    searchTableData('getSettlementDataList', '$M.SettlementDataRptVO', 'settlementDataTable');
}

function exportData() {
    callSid('excelExportMethod');
}

function deleteByIds() {
    var idList = getTable_Td_CheckedValue('settlementDataTable', '1');
    if (idList.length == 0){
        alert("至少选择一条");
        return;
    }
    if (confirm("确认删除?")){
        callSid('deleteSettlementDataRptByIds',idList);
        setTimeout(reloadPage,1000);
    }
}
function toOtherPage(url) {
    savePageStatus('settlementDataTable','rptStatus');
    document.location.href=url;
}
function reloadPage() {
    var cTime=document.getElementById("projectType");
    cTime.options.length=0;
    callSid('getProjectType');    //获取项目类型下拉框数据
    searchTableData('getSettlementDataList', '$M.SettlementDataRptVO', 'settlementDataTable');
}


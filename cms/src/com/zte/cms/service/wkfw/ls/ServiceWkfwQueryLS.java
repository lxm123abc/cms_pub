package com.zte.cms.service.wkfw.ls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zte.cms.service.ls.ServiceConstants;
import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.service.IServiceWkfwProcessWithMeDS;
import com.zte.cms.service.wkfw.service.IServiceWkfwTaskForMeDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

public class ServiceWkfwQueryLS implements IServiceWkfwQueryLS
{

    protected IServiceWkfwTaskForMeDS serviceWkfwTaskForMeDS = null;
    protected IServiceWkfwProcessWithMeDS serviceWkfwProcessWithMeDS = null;

    public void setServiceWkfwTaskForMeDS(IServiceWkfwTaskForMeDS serviceWkfwTaskForMeDS)
    {
        this.serviceWkfwTaskForMeDS = serviceWkfwTaskForMeDS;
    }

    public void setServiceWkfwProcessWithMeDS(IServiceWkfwProcessWithMeDS serviceWkfwProcessWithMeDS)
    {
        this.serviceWkfwProcessWithMeDS = serviceWkfwProcessWithMeDS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMeListCnt(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryServiceWkfwTaskForMeListCnt(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == serviceWkfwTaskForMeCondition.getPboType()
                || ("").equals(serviceWkfwTaskForMeCondition.getPboType()))
        {
            serviceWkfwTaskForMeCondition.setPboType(ServiceConstants.WKFWTYPE_SERVICE_ALL);
        }
        serviceWkfwTaskForMeCondition.setExecutorId(ServiceConstants.getUserid());
        serviceWkfwTaskForMeCondition.setTaskStatus("N");
        return serviceWkfwTaskForMeDS.queryServiceWkfwTaskForMeListCntByCond(serviceWkfwTaskForMeCondition);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS
     *      #queryStrategyWkfwTaskForMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition, int, int)
     */
    public TableDataInfo queryServiceWkfwTaskForMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int rangeStart,
            int fetchSize) throws DomainServiceException
    {

        TableDataInfo tableInfo = null;
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);
        puEntity.setOrderByColumn("processStartTime");
        tableInfo = queryServiceWkfwTaskForMe(serviceWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS#queryStrategyWkfwTaskForMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public TableDataInfo queryServiceWkfwTaskForMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int rangeStart,
            int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == serviceWkfwTaskForMeCondition.getPboType()
                || ("").equals(serviceWkfwTaskForMeCondition.getPboType()))
        {
            serviceWkfwTaskForMeCondition.setPboType(ServiceConstants.WKFWTYPE_SERVICE_ALL);
        }
        serviceWkfwTaskForMeCondition.setExecutorId(ServiceConstants.getUserid());
        serviceWkfwTaskForMeCondition.setTaskStatus("N");

        TableDataInfo tableInfo = null;
        tableInfo = serviceWkfwTaskForMeDS
                .pageInfoQuery(serviceWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS#queryStrategyWkfwProcessWithMeListCnt(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryServiceWkfwProcessWithMeListCnt(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        if (null == serviceWkfwTaskForMeCondition.getPboType()
                || ("").equals(serviceWkfwTaskForMeCondition.getPboType()))
        {
            serviceWkfwTaskForMeCondition.setPboType(ServiceConstants.WKFWTYPE_SERVICE_ALL);
        }
        serviceWkfwTaskForMeCondition.setOpername(ServiceConstants.getUserid());
        serviceWkfwTaskForMeCondition.setUserid(ServiceConstants.getUserid());
        return serviceWkfwProcessWithMeDS.queryServiceWkfwProcessWithMeListCntByCond(serviceWkfwTaskForMeCondition);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS#queryStrategyWkfwProcessWithMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    public TableDataInfo queryServiceWkfwProcessWithMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize) throws DomainServiceException
    {
        PageUtilEntity puEntity = new PageUtilEntity();
        puEntity.setIsAlwaysSearchSize(true);
        puEntity.setIsAsc(false);

        puEntity.setOrderByColumn("processStartTime");
        return queryServiceWkfwProcessWithMe(serviceWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.ls.IStrategyWkfwQueryLS#queryStrategyWkfwProcessWithMe(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    public TableDataInfo queryServiceWkfwProcessWithMe(ServiceWkfwCondition serviceWkfwTaskForMeCondition,
            int rangeStart, int fetchSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        if (null == serviceWkfwTaskForMeCondition.getPboType()
                || ("").equals(serviceWkfwTaskForMeCondition.getPboType()))
        {
            serviceWkfwTaskForMeCondition.setPboType(ServiceConstants.WKFWTYPE_SERVICE_ALL);
        }
        serviceWkfwTaskForMeCondition.setOpername(ServiceConstants.getUserid());
        serviceWkfwTaskForMeCondition.setUserid(ServiceConstants.getUserid());
        return serviceWkfwProcessWithMeDS.pageInfoQuery(serviceWkfwTaskForMeCondition, rangeStart, fetchSize, puEntity);
    }

    public HashMap<String, Object> getWorkflowTypeList(String selectedValue, Boolean needSelect)
    {
        List<String> textList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();

        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);

        if (needSelect)
        {
            textList.add(ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_SERVICE_OPTION_PLEASESELECT));
            valueList.add("CMS_SERVICE_%");
        }

        textList.add(ResourceMgt.findDefaultText(ServiceConstants.RES_CMS_STRA_WKFWTYPENAME_SERVICE_ADD));
        valueList.add(ServiceConstants.WKFWTYPE_SERVICE_ADD);

        // textList.add(ResourceMgt.findDefaultText(StrategyConstant.RES_CMS_STRA_WKFWTYPENAME_STRATEGY_DROPOUT));
        // valueList.add(StrategyConstant.WKFWTYPE_STRATEGY_DROPOUT);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("codeText", textList);
        map.put("codeValue", valueList);
        map.put("defaultValue", selectedValue);
        return map;
    }

    public HashMap<String, String> getWorkflowTypeMap()
    {
        ResourceMgt.addDefaultResourceBundle(ServiceConstants.RESOURCE_UCDN_SERVICE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ServiceConstants.WKFWTYPE_SERVICE_ADD, ResourceMgt
                .findDefaultText(ServiceConstants.RES_CMS_STRA_WKFWTYPENAME_SERVICE_ADD));
        // map.put(StrategyConstant.WKFWTYPE_STRATEGY_DROPOUT, ResourceMgt
        // .findDefaultText(StrategyConstant.RES_CMS_STRA_WKFWTYPENAME_STRATEGY_DROPOUT));
        return map;
    }
}

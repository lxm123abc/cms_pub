package com.zte.cms.content.movie.dao;

import java.util.List;

import com.zte.cms.content.movie.dao.ICmsMovieDAO;
import com.zte.cms.content.movie.model.CmsMovie;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsMovieDAO extends DynamicObjectBaseDao implements ICmsMovieDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsMovie(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("insert cmsMovie starting...");
        super.insert("insertCmsMovie", cmsMovie);
        log.debug("insert cmsMovie end");
    }

    public void updateCmsMovie(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("update cmsMovie by pk starting...");
        super.update("updateCmsMovie", cmsMovie);
        log.debug("update cmsMovie by pk end");
    }

    public void updateCmsMovieByCond(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("update cmsMovie by conditions starting...");
        super.update("updateCmsMovieByCond", cmsMovie);
        log.debug("update cmsMovie by conditions end");
    }

    public void deleteCmsMovie(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("delete cmsMovie by pk starting...");
        super.delete("deleteCmsMovie", cmsMovie);
        log.debug("delete cmsMovie by pk end");
    }

    public void deleteCmsMovieByCond(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("delete cmsMovie by conditions starting...");
        super.delete("deleteCmsMovieByCond", cmsMovie);
        log.debug("update cmsMovie by conditions end");
    }

    public CmsMovie getCmsMovie(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("query cmsMovie starting...");
        CmsMovie resultObj = (CmsMovie) super.queryForObject("getCmsMovie", cmsMovie);
        log.debug("query cmsMovie end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsMovie> getCmsMovieByCond(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("query cmsMovie by condition starting...");
        List<CmsMovie> rList = (List<CmsMovie>) super.queryForList("queryCmsMovieListByCond", cmsMovie);
        log.debug("query cmsMovie by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsMovie by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsMovieListCntByCond", cmsMovie)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsMovie> rsList = (List<CmsMovie>) super.pageQuery("queryCmsMovieListByCond", cmsMovie, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsMovie by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQueryEncryption(CmsMovie cmsMovie, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsMovie by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryEncryptionCmsMovieCntByCond", cmsMovie)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsMovie> rsList = (List<CmsMovie>) super.pageQuery("queryEncryptionCmsMovieListByCond", cmsMovie, start,
                    fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsMovie by condition end");
        return pageInfo;
    }    
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsMovie cmsMovie, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsMovieListByCond", "queryCmsMovieListCntByCond", cmsMovie, start, pageSize,
                puEntity);
    }

    @SuppressWarnings("unchecked")
    public int getCmsMovieCount(CmsMovie cmsMovie) throws DAOException
    {
        log.debug("query cmsMovie count by condition starting...");
        int totalCnt = ((Integer) super.queryForObject("queryCmsMovieListCntByCond", cmsMovie)).intValue();
        log.debug("query cmsMovie count by condition end");
        return totalCnt;
    }

}
package com.zte.cms.castrolemap.service;

import java.util.List;
import com.zte.cms.castrolemap.dao.ICastrolemapDAO;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.castrolemap.service.ICastrolemapDS;
import com.zte.cms.common.Generator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CastrolemapDS extends DynamicObjectBaseDS implements ICastrolemapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICastrolemapDAO dao = null;

    public void setDao(ICastrolemapDAO dao)
    {
        this.dao = dao;
    }

    public String insertCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("insert castrolemap starting...");
        Long castrolemapindex = null;
        String castrolemapid = null;
        try
        {
            castrolemapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("castrolemap_index");
            castrolemapid = Generator.getContentId(Long.valueOf(000000), "CARM");
            castrolemap.setCastrolemapindex(castrolemapindex);
            castrolemap.setCastrolemapid(castrolemapid);
            castrolemap.setCastrolemapcode(castrolemapid);

            dao.insertCastrolemap(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert castrolemap end");
        return castrolemapid;
    }

    public void updateCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("update castrolemap by pk starting...");
        try
        {
            dao.updateCastrolemap(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castrolemap by pk end");
    }

    public void updateCastrolemapList(List<Castrolemap> castrolemapList) throws DomainServiceException
    {
        log.debug("update castrolemapList by pk starting...");
        if (null == castrolemapList || castrolemapList.size() == 0)
        {
            log.debug("there is no datas in castrolemapList");
            return;
        }
        try
        {
            for (Castrolemap castrolemap : castrolemapList)
            {
                dao.updateCastrolemap(castrolemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castrolemapList by pk end");
    }

    public void updateCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("update castrolemap by condition starting...");
        try
        {
            dao.updateCastrolemapByCond(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castrolemap by condition end");
    }

    public void updateCastrolemapListByCond(List<Castrolemap> castrolemapList) throws DomainServiceException
    {
        log.debug("update castrolemapList by condition starting...");
        if (null == castrolemapList || castrolemapList.size() == 0)
        {
            log.debug("there is no datas in castrolemapList");
            return;
        }
        try
        {
            for (Castrolemap castrolemap : castrolemapList)
            {
                dao.updateCastrolemapByCond(castrolemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update castrolemapList by condition end");
    }

    public void removeCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("remove castrolemap by pk starting...");
        try
        {
            dao.deleteCastrolemap(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castrolemap by pk end");
    }

    public void removeCastrolemapList(List<Castrolemap> castrolemapList) throws DomainServiceException
    {
        log.debug("remove castrolemapList by pk starting...");
        if (null == castrolemapList || castrolemapList.size() == 0)
        {
            log.debug("there is no datas in castrolemapList");
            return;
        }
        try
        {
            for (Castrolemap castrolemap : castrolemapList)
            {
                dao.deleteCastrolemap(castrolemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castrolemapList by pk end");
    }

    public void removeCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("remove castrolemap by condition starting...");
        try
        {
            dao.deleteCastrolemapByCond(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castrolemap by condition end");
    }

    public void removeCastrolemapListByCond(List<Castrolemap> castrolemapList) throws DomainServiceException
    {
        log.debug("remove castrolemapList by condition starting...");
        if (null == castrolemapList || castrolemapList.size() == 0)
        {
            log.debug("there is no datas in castrolemapList");
            return;
        }
        try
        {
            for (Castrolemap castrolemap : castrolemapList)
            {
                dao.deleteCastrolemapByCond(castrolemap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove castrolemapList by condition end");
    }

    public Castrolemap getCastrolemap(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("get castrolemap by pk starting...");
        Castrolemap rsObj = null;
        try
        {
            rsObj = dao.getCastrolemap(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castrolemapList by pk end");
        return rsObj;
    }

    public List<Castrolemap> getCastrolemapByCond(Castrolemap castrolemap) throws DomainServiceException
    {
        log.debug("get castrolemap by condition starting...");
        List<Castrolemap> rsList = null;
        try
        {
            rsList = dao.getCastrolemapByCond(castrolemap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get castrolemap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get castrolemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castrolemap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Castrolemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castrolemap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Castrolemap castrolemap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get castrolemap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(castrolemap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Castrolemap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get castrolemap page info by condition end");
        return tableInfo;
    }
}

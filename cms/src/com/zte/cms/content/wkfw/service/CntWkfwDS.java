package com.zte.cms.content.wkfw.service;

import java.util.List;

import com.zte.cms.content.wkfw.dao.ICntWkfwDAO;
import com.zte.cms.content.wkfw.model.CntWkfwhis;
import com.zte.cms.content.wkfw.model.CntWkfwCondition;
import com.zte.cms.content.wkfw.model.CntWkfwTaskResult;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class CntWkfwDS implements ICntWkfwDS
{

    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICntWkfwDAO dao = null;

    @SuppressWarnings("unchecked")
    public TableDataInfo getMyNeedHandleWorkflowItemList(CntWkfwCondition cntorderWkfwCondition, int start,
            int pageSize, PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get myNeedHandleWorkflowItemList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.getMyNeedHandleWorkflowItemList(cntorderWkfwCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CntWkfwTaskResult>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get myNeedHandleWorkflowItemList page info by condition end");
        return tableInfo;
    }

    public long getMyNeedHandleWorkflowItemListCnt(CntWkfwCondition cntorderWkfwCondition)
            throws DomainServiceException
    {
        log.debug("get myNeedHandleWorkflowItemListCnt info by  condition starting...");
        long result = 0;
        try
        {
            result = dao.getMyNeedHandleWorkflowItemListCnt(cntorderWkfwCondition);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.debug("get myNeedHandleWorkflowItemListCnt info by condition end");
        return result;
    }

    public void setDao(ICntWkfwDAO dao)
    {
        this.dao = dao;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwhis, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        List<CntWkfwhis> cmsProgramWkfwhisResult = null;
        log.debug("get cmsProgramWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(cntWkfwhis, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        cmsProgramWkfwhisResult = (List<CntWkfwhis>) pageInfo.getResult();
        if (null != cmsProgramWkfwhisResult)
        {
            for (int i = 0; i < cmsProgramWkfwhisResult.size(); i++)
            {
                String cpname = cmsProgramWkfwhisResult.get(i).getCpcnshortname();
                String programid = cmsProgramWkfwhisResult.get(i).getProgramid();
                if (null == cpname || ("".equals(cpname)))
                {
                    cmsProgramWkfwhisResult.get(i).setCpcnshortname("该CP已被删除");
                    cmsProgramWkfwhisResult.get(i).setCpid("该CP已被删除");
                }
                if (null == programid || ("".equals(programid)))
                {
                    cmsProgramWkfwhisResult.get(i).setProgramid("该内容已被删除");
                    cmsProgramWkfwhisResult.get(i).setNamecn("该内容已被删除");
                }
            }
        }
        tableInfo.setData(cmsProgramWkfwhisResult);
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramWkfwhis page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CntWkfwhis cntWkfwhis, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get cmsProgramWkfwhis page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {

            pageInfo = dao.pageInfoQuery(cntWkfwhis, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsProgramWkfwhis page info by condition end");
        return tableInfo;
    }

    public long getNopassWorkflowCnt(CntWkfwhis cntWkfwis) throws DomainServiceException
    {
        log.debug("get myNeedHandleWorkflowItemListCnt info by  condition starting...");
        long result = 0;
        try
        {
            result = dao.getNopassWorkflowCnt(cntWkfwis);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.debug("get myNeedHandleWorkflowItemListCnt info by condition end");
        return result;
    }

    /**
     * 审核信息查看
     * 
     * @param cntWkfwhis
     * @return
     * @throws DomainServiceException
     */
    public CntWkfwhis getCmsProgramWkfwhis(CntWkfwhis cntWkfwhis) throws DomainServiceException
    {
        log.debug("getCmsProgramWkfwhisstarting...");
        CntWkfwhis objcms = null;
        try
        {
            objcms = dao.getCmsProgramWkfwhis(cntWkfwhis);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }

        log.debug("getCmsProgramWkfwhis condition end");
        return objcms;
    }
}

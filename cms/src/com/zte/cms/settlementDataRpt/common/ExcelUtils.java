package com.zte.cms.settlementDataRpt.common;

import com.zte.ssb.web.multipart.MultipartFile;
import org.apache.commons.fileupload.FileItem;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ProjectName: miaosha-com.miaoshaDemo.utils
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 15:22 2019/5/6
 * @Description:
 */
public class ExcelUtils {

    public static XSSFWorkbook wb = null;
    public static XSSFSheet sheet = null;

    public static final String ALIGN_CENTER = "center";
    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_RIGHT = "right";

    public static File getHSSFWorkbook(
            List<?> list,
            List<List<Map<String, Object>>> listMesh,
            String[] line, String[] lineEn,
            String fileName) throws IllegalAccessException, IOException {

        int startTableRow = 5;

        /**
         * 工作表列名
         */
        if (line.length != lineEn.length) {
            return null;
        }


        /******************* 我是分割线 **************************************/
        // 定义单元格报头
        wb = new XSSFWorkbook();

        // 创建单元格样式
        XSSFCellStyle cellStyleTitle = wb.createCellStyle();
        cellStyleTitle.setWrapText(true);
        // 设置单元格字体
        XSSFFont fontTitle = wb.createFont();
        fontTitle.setBold(true);
        fontTitle.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
        fontTitle.setFontHeight((short) 200);
        cellStyleTitle.setFont(fontTitle);
        XSSFCellStyle cellStyle = wb.createCellStyle();
        // 指定当单元格内容显示不下时自动换行
        cellStyle.setWrapText(true);
        // 设置单元格字体
        XSSFFont font = wb.createFont();
        font.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
        font.setFontHeight((short) 200);
        cellStyle.setFont(font);


        /**************** wo是分割线 *****************************************/

        sheet = wb.createSheet();
        // 设置默认行高，表示2个字符的高度，必须先设置列宽然后设置行高，不然列宽没有效果
        sheet.setDefaultRowHeight((short) (1 * 256));
        // 设置默认列宽
        sheet.setDefaultColumnWidth(22);
        for (int i = 0; i < listMesh.size(); i++) {
            List<Map<String, Object>> rowMesh = listMesh.get(i);
            XSSFRow row1 = sheet.createRow(i);
            for (int j = 0; j < rowMesh.size(); j++) {
                Map<String, Object> cellMesh = rowMesh.get(j);
                String param = String.valueOf(cellMesh.get(SettlementDataRptConstants.EXCEL_PARAM));
                if (param == null) {
                    param = "";
                }
                int startCol = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_STARTCOL) + "");
                int endCol = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_ENDCOL) + "");
                int startRow = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_STARTROW) + "");
                int endRow = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_ENDROW) + "");
                String align = String.valueOf(cellMesh.get(SettlementDataRptConstants.EXCEL_ALIGN));
                // 创建第row行
                meshRow(param, startCol, endCol, startRow, endRow, row1, j, align);
            }
        }
        /**
         * 创建标题行
         */
        XSSFRow row1 = sheet.createRow(startTableRow - 1);
        /**
         * 标题列
         */
        XSSFCell cell1 = null;
        for (int i = 0; i < line.length; i++) {
            cell1 = row1.createCell(i);
            XSSFCellStyle cellStyle2 = wb.createCellStyle();
            cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            // 设置单元格字体
            XSSFFont fonts = wb.createFont();
            fonts.setColor(HSSFColor.BLUE.index2);
            fonts.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
            fonts.setFontHeight((short) 250);
            cellStyle2.setFont(fonts);
            cell1.setCellStyle(cellStyle2);
            cell1.setCellValue(new XSSFRichTextString(line[i]));
        }
        /******************** 我是分割线 *********************************/
        /**
         * 定义表格第二行
         */
        XSSFRow row = null;
        XSSFCell cell = null;


        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = objectToMap(list.get(i));
            /**
             * 从i+2行开始，因为我们之前的表的标题和列的标题已经占用了两行
             */
            // 创建行
            row = sheet.createRow(i + startTableRow);
            // 设置每行的高度
            row.setHeight((short) (1 * 256));
            cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            for (int j = 0; j < lineEn.length; j++) {
                // 创建单元格
                cell = row.createCell(j);
                // 设置单元格样式
                cell.setCellStyle(cellStyle);
                // 设置单元格内容
                String cellValue = String.valueOf(map.get(lineEn[j]));
                if (cellValue == null) {
                    cellValue = "";
                }
                String entry = "null";
                if (entry.equalsIgnoreCase(cellValue)) {
                    cellValue = "";
                }
                cell.setCellValue(new XSSFRichTextString(cellValue));
            }
        }
        OutputStream fileOut = null;
        try {
            //设置文件名
            File file = new File(fileName);
            fileOut = new FileOutputStream(file);
            wb.write(fileOut);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (fileOut!=null){
                fileOut.close();
            }
        }

        return null;
    }


    /**
     * 任务管理 审批记录 导出保存到本地
     */
    public static File getHSSFWorkbook2(List<?> list, List<List<Map<String, Object>>> listMesh, String[] line,
                                        String[] lineEn, String fileName, Map<String, String> funcMap) throws IOException, IllegalAccessException {

        int startTableRow = 5;

        /**
         * 工作表列名
         */
        if (line.length != lineEn.length) {
            throw new RuntimeException("Message:列名长度和列名英文长度不一致");
        }
        /**

         /******************* 我是分割线 **************************************/
        // 定义单元格报头
        wb = new XSSFWorkbook();

        // 创建单元格样式
        XSSFCellStyle cellStyleTitle = wb.createCellStyle();
        cellStyleTitle.setWrapText(true);
        // 设置单元格字体
        XSSFFont fontTitle = wb.createFont();
        fontTitle.setBold(true);
        fontTitle.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
        fontTitle.setFontHeight((short) 200);
        cellStyleTitle.setFont(fontTitle);
        XSSFCellStyle cellStyle = wb.createCellStyle();
        // 指定当单元格内容显示不下时自动换行
        cellStyle.setWrapText(true);
        // 设置单元格字体
        XSSFFont font = wb.createFont();
        font.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
        font.setFontHeight((short) 200);
        cellStyle.setFont(font);


        /**************** wo是分割线 *****************************************/

        sheet = wb.createSheet();
        // 设置默认行高，表示2个字符的高度，必须先设置列宽然后设置行高，不然列宽没有效果
        sheet.setDefaultRowHeight((short) (1 * 256));
        // 设置默认列宽
        sheet.setDefaultColumnWidth(37);
        for (int i = 0; i < listMesh.size(); i++) {
            List<Map<String, Object>> rowMesh = listMesh.get(i);
            XSSFRow row1 = sheet.createRow(i);
            for (int j = 0; j < rowMesh.size(); j++) {
                Map<String, Object> cellMesh = rowMesh.get(j);
                String param = String.valueOf(cellMesh.get(SettlementDataRptConstants.EXCEL_PARAM));
                if (param == null) {
                    param = "";
                }
                int startCol = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_STARTCOL) + "");
                int endCol = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_ENDCOL) + "");
                int startRow = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_STARTROW) + "");
                int endRow = Integer.parseInt(cellMesh.get(SettlementDataRptConstants.EXCEL_ENDROW) + "");
                String align = String.valueOf(cellMesh.get(SettlementDataRptConstants.EXCEL_ALIGN));
                // 创建第row行
                meshRow(param, startCol, endCol, startRow, endRow, row1, j, align);
            }
        }
        /**
         * 创建标题行
         */
        XSSFRow row1 = sheet.createRow(startTableRow - 1);
        /**
         * 标题列
         */
        XSSFCell cell1 = null;
        for (int i = 0; i < line.length; i++) {
            cell1 = row1.createCell(i);
            XSSFCellStyle cellStyle2 = wb.createCellStyle();
            cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            // 设置单元格字体
            XSSFFont fonts = wb.createFont();
            fonts.setColor(HSSFColor.BLUE.index2);
            fonts.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
            fonts.setFontHeight((short) 250);
            cellStyle2.setFont(fonts);
            cell1.setCellStyle(cellStyle2);
            cell1.setCellValue(new XSSFRichTextString(line[i]));
        }
        /******************** 我是分割线 *********************************/
        /**
         * 定义表格第二行
         */
        XSSFRow row = null;
        XSSFCell cell = null;


        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = objectToMap(list.get(i));
            /**
             * 从i+2行开始，因为我们之前的表的标题和列的标题已经占用了两行
             */
            // 创建行
            row = sheet.createRow(i + startTableRow);
            // 设置每行的高度
            row.setHeight((short) (1 * 256));
            cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            for (int j = 0; j < lineEn.length; j++) {
                // 创建单元格
                cell = row.createCell(j);
                // 设置单元格样式
                cell.setCellStyle(cellStyle);
                // 设置单元格内容
                String cellValue = String.valueOf(map.get(lineEn[j]));
                if (cellValue == null) {
                    cellValue = "";
                }
                String entry = "null";
                if (entry.equalsIgnoreCase(cellValue)) {
                    cellValue = "";
                }
                if (lineEn[j].equals(SettlementDataRptConstants.Excel_STEP1) || lineEn[j].equals(SettlementDataRptConstants.Excel_STEP2)) {
                    cellValue = funcMap.get(cellValue) == null ? "" : funcMap.get(cellValue);
                }
                if (lineEn[j].equals(SettlementDataRptConstants.Excel_AUDIT_STATUS)) {
                    cellValue = cellValue.equals("0")?"通过":"驳回";
                }
                cell.setCellValue(new XSSFRichTextString(cellValue));
            }
        }
        OutputStream fileOut = null;
        try {
            //设置文件名
            File file = new File(fileName);
            fileOut = new FileOutputStream(file);
            wb.write(fileOut);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (fileOut!=null){
                fileOut.close();
            }
        }

        return null;
    }

    /**
     * 文件输出
     *
     * @param workbook 填充好的workbook
     * @param path     存放的位置
     * @author LiQuanhui
     * @date 2017年11月24日 下午5:26:23
     */
    public static boolean outFile(XSSFWorkbook workbook, String path) {
        File tempFile = new File(path);
        if (!tempFile.getParentFile().exists()) {
            tempFile.getParentFile().mkdirs();
        }
        if (!tempFile.exists()) {
            try {
                tempFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //导出到文件
        try {
            OutputStream outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void meshRow(String param, int startCol, int endCol, int startRow, int endRow, XSSFRow row, int index, String align) {

        XSSFCell cell2 = row.createCell(startCol);

        cell2.setCellType(XSSFCell.CELL_TYPE_BLANK);
        cell2.setCellValue(new XSSFRichTextString(param));
        if (startCol != endCol) {
            // 指定合并区域
            sheet.addMergedRegion(new CellRangeAddress(startRow, endRow, (short) startCol, (short) endCol));
        }
        XSSFCellStyle cellStyle = wb.createCellStyle();
        // 指定单元格垂直居中对齐
        cellStyle.setWrapText(true);
        if (ALIGN_CENTER.equals(align)) {
            cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        } else if (ALIGN_RIGHT.equals(align)) {
            cellStyle.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        } else if (ALIGN_LEFT.equals(align)) {
            cellStyle.setAlignment(XSSFCellStyle.ALIGN_LEFT);
        }
        // 设置单元格字体
        XSSFFont font = wb.createFont();
        font.setColor(HSSFColor.BLUE.index);
        font.setFontName(SettlementDataRptConstants.EXCEL_FONT_NAME);
        font.setFontHeight((short) 250);
        cellStyle.setFont(font);
        cell2.setCellStyle(cellStyle);
    }

    //发送响应流方法
    public static void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Map<String, Object> objectToMap(Object obj) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<String, Object>(10);
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            if (fieldName != null && fieldName.equals(SettlementDataRptConstants.RPTDT)) {
                map.put(fieldName, DateUtils.convertDataToString_fit((Date) value, DateUtils.fit_yyyyMM));
                continue;
            }
            map.put(fieldName, value);
        }
        return map;
    }


}
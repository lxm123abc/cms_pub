package com.zte.cms.channel.service;

import java.util.List;

import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.Physicalchannel;
import com.zte.cms.channel.dao.IPhysicalchannelDAO;
import com.zte.cms.channel.service.IPhysicalchannelDS;
import com.zte.cms.common.Generator;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class PhysicalchannelDS extends DynamicObjectBaseDS implements IPhysicalchannelDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    public String physicalChannelId = "";
    private IPhysicalchannelDAO dao = null;

    public void setDao(IPhysicalchannelDAO dao)
    {
        this.dao = dao;
    }

    public void insertPhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("insert physicalchannel starting...");
        try
        {
            Long index = (Long) getPrimaryKeyGenerator().getPrimarykey("physicalchannel");
            physicalchannel.setPhysicalchannelindex(index);
            physicalChannelId = Generator.getPhysicalContentId("PHCH");
            physicalchannel.setPhysicalchannelid(physicalChannelId);
            
            //String cpcontentid = Generator.getWGCodeByPhysicalContentId(physicalChannelId);
            physicalchannel.setCpcontentid(physicalChannelId);
            dao.insertPhysicalchannel(physicalchannel);
        }
        catch (Exception daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert physicalchannel end");
    }

    public void updatePhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("update physicalchannel by pk starting...");
        try
        {
            dao.updatePhysicalchannel(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update physicalchannel by pk end");
    }

    public void updatePhysicalchannelList(List<Physicalchannel> physicalchannelList) throws DomainServiceException
    {
        log.debug("update physicalchannelList by pk starting...");
        if (null == physicalchannelList || physicalchannelList.size() == 0)
        {
            log.debug("there is no datas in physicalchannelList");
            return;
        }
        try
        {
            for (Physicalchannel physicalchannel : physicalchannelList)
            {
                dao.updatePhysicalchannel(physicalchannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update physicalchannelList by pk end");
    }

    public void updatePhysicalchannelByCond(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("update physicalchannel by condition starting...");
        try
        {
            dao.updatePhysicalchannelByCond(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update physicalchannel by condition end");
    }

    public void updatePhysicalchannelListByCond(List<Physicalchannel> physicalchannelList)
            throws DomainServiceException
    {
        log.debug("update physicalchannelList by condition starting...");
        if (null == physicalchannelList || physicalchannelList.size() == 0)
        {
            log.debug("there is no datas in physicalchannelList");
            return;
        }
        try
        {
            for (Physicalchannel physicalchannel : physicalchannelList)
            {
                dao.updatePhysicalchannelByCond(physicalchannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update physicalchannelList by condition end");
    }

    public void removePhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("remove physicalchannel by pk starting...");
        try
        {
            dao.deletePhysicalchannel(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove physicalchannel by pk end");
    }

    public void removePhysicalchannelList(List<Physicalchannel> physicalchannelList) throws DomainServiceException
    {
        log.debug("remove physicalchannelList by pk starting...");
        if (null == physicalchannelList || physicalchannelList.size() == 0)
        {
            log.debug("there is no datas in physicalchannelList");
            return;
        }
        try
        {
            for (Physicalchannel physicalchannel : physicalchannelList)
            {
                dao.deletePhysicalchannel(physicalchannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove physicalchannelList by pk end");
    }

    public void removePhysicalchannelByCond(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("remove physicalchannel by condition starting...");
        try
        {
            dao.deletePhysicalchannelByCond(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove physicalchannel by condition end");
    }

    public void removePhysicalchannelListByCond(List<Physicalchannel> physicalchannelList)
            throws DomainServiceException
    {
        log.debug("remove physicalchannelList by condition starting...");
        if (null == physicalchannelList || physicalchannelList.size() == 0)
        {
            log.debug("there is no datas in physicalchannelList");
            return;
        }
        try
        {
            for (Physicalchannel physicalchannel : physicalchannelList)
            {
                dao.deletePhysicalchannelByCond(physicalchannel);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove physicalchannelList by condition end");
    }

    public Physicalchannel getPhysicalchannel(Physicalchannel physicalchannel) throws DomainServiceException
    {
        log.debug("get physicalchannel by pk starting...");
        Physicalchannel rsObj = null;
        try
        {
            rsObj = dao.getPhysicalchannel(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get physicalchannelList by pk end");
        return rsObj;
    }

    public List<Physicalchannel> getPhysicalchannelByCond(Physicalchannel physicalchannel)
            throws DomainServiceException
    {
        log.debug("get physicalchannel by condition starting...");
        List<Physicalchannel> rsList = null;
        try
        {
            rsList = dao.getPhysicalchannelByCond(physicalchannel);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get physicalchannel by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Physicalchannel physicalchannel, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get physicalchannel page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(physicalchannel, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Physicalchannel>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get physicalchannel page info by condition end");
        return tableInfo;
    }

    public List listPhysicalchannelByChannelid(Physicalchannel physicalchannel)
    {
        List list = dao.listPhysicalchannelByChannelid(physicalchannel);
        return list;
    }
    //查询新增节目单时选择的物理频道
    public TableDataInfo pageInfoQueryschedule(CmsSchedule cmsSchedule, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get physicalchannel page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryschedule(cmsSchedule, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Physicalchannel>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get physicalchannel page info by condition end");
        return tableInfo;
    }

}

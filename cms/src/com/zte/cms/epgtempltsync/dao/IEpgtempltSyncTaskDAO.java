package com.zte.cms.epgtempltsync.dao;

import java.util.List;

import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface IEpgtempltSyncTaskDAO
{
    /**
     * 新增EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DAOException dao异常
     */
    public void insertEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException;

    /**
     * 根据主键更新EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DAOException dao异常
     */
    public void updateEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException;

    /**
     * 根据主键删除EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DAOException dao异常
     */
    public void deleteEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException;

    /**
     * 根据主键查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @return 满足条件的EpgtempltSyncTask对象
     * @throws DAOException dao异常
     */
    public EpgtempltSyncTask getEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DAOException;

    /**
     * 根据条件查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @return 满足条件的EpgtempltSyncTask对象集
     * @throws DAOException dao异常
     */
    public List<EpgtempltSyncTask> getEpgtempltSyncTaskByCond(EpgtempltSyncTask epgtempltSyncTask) throws DAOException;

    /**
     * 根据条件分页查询EpgtempltSyncTask对象，作为查询条件的参数
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询EpgtempltSyncTask对象，作为查询条件的参数
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    /**
     * 根据条件分页查询UpstreamEPGTask对象，作为查询条件的参数
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail(UpstreamEPGTaskDetail rsObj) throws DAOException;
}
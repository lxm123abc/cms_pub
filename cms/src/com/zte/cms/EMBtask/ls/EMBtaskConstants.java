package com.zte.cms.EMBtask.ls;

import com.zte.umap.common.ResourceMgt;

public class EMBtaskConstants
{

    public static final String CMS_EMBTASK_RES_FILE_NAME = "cms_EMBtask_resource";// 资源文件名
    public static final String CMS_EMBTASK_MSG_SAME_TEMPLETE_EXITS = "cms.embtask.same.templete.exits";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_STATUS_NOT_RIGHT = "cms.embtask.msg.subcontent.status.not.right";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_PATH_NOT_RIGHT = "cms.embtask.msg.subcontent.path.not.right";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_GROUPTYPE_DEESNOT_MATCH_AVGROUPTYPE = "cms.embtask.msg.subcontent.grouptype.doesnot.match.avgrouptype";
    public static final String CMS_EMBTASK_MSG_OPER_SUCCESSFUL = "cms.embtask.msg.oper.successful";
    public static final String CMS_EMBTASK_MSG_OPER_FAIL = "cms.embtask.msg.oper.fail";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_NO_TEMPLATE = "cms.embtask.msg.subcontent.no.template";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_STATUS_DOESNOT_EXIT = "cms.embtask.msg.subcontent.status.doesnot.exit";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_GROUPTYPE_DOESNOT_EXIT = "cms.embtask.msg.subcontent.grouptype.doesnot.exit";
    public static final String CMS_EMBTASK_MSG_TEMPLATE_GROUPTYPE_DOESNOT_EXIT = "cms.embtask.msg.template.grouptype.doesnot.exit";
    public static final String CMS_EMBTASK_MSG_DUPLICATED_TASK = "cms.embtask.msg.duplicated.task";
    public static final String CMS_EMBTASK_MSG_CANTNOT_GET_MAKEAREA_ADDRESS = "cms.embtask.msg.cannot.get.makearea.address";
    public static final String CMS_EMBTASK_MSG_CANTNOT_GET_STORAGEAREA_ADDRESS = "cms.embtask.msg.cannot.get.storagearea.address";
    public static final String CMS_EMBTASK_MSG_SOURCEFILE_DOESNOT_EXIST = "cms.embtask.msg.sourcefile.does.not.exist";
    public static final String CMS_EMBTASK_MSG_CMSMOVIE_DOESNOT_EXIST = "cms.embtask.msg.cmsmovie.does.not.exist";
    public static final String CMS_EMBTASK_MSG_TEMPLATE_DOESNOT_EXIST = "cms.embtask.msg.template.does.not.exist";
    public static final String CMS_EMBTASK_MSG_SAME_TRANSFILE_EXISTS = "cms.embtask.msg.same.transfile.exists.already";
    public static final String CMS_EMBTASK_MSG_SAME_TECHAUDITTASK_EXISTS = "cms.embtask.msg.same.techautittask.exists.already";
    public static final String CMS_EMBTASK_MSG_SOURCEFILE_PATH_WRONG_FORMAT = "cms.embtask.msg.wrong.sourcefile.path.format";
    public static final String CMS_EMBTASK_MSG_SOURCEFILE_WRONG_POSTFIX = "cms.embtask.msg.wrong.sourcefile.wrong.postfix";
    public static final String CMS_EMBTASK_MSG_CANTNOT_GET_ENCRYPTLOGAREA_ADDRESS = "cms.embtask.msg.cannot.get.encryptlogarea.address";
    public static final String CMS_EMBTASK_MSG_CNT_HASNOT_SUBCONTENT = "cms.embtask.msg.program.hasnot.subcontent";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_ENCRYPTION_STATUS_DOESNOT_CORRECT = "cms.embtask.msg.subcontent.encryption.status.doesnot.correct";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_CMSMOVIE_DOESNOT_EXIST = "cms.embtask.msg.subcontent.cmsmovie.does.not.exist";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_CMSMOVIE_ENCRYPTION_STATUS_DOESNOT_CORRECT = "cms.embtask.msg.cmsmovie.encryption.status.doesnot.correct";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_CANTNOT_GET_MAKEAREA_ADDRESS = "cms.embtask.msg.encryption.cannot.get.makearea.address";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_CANTNOT_GET_ENCRYPTLOGAREA_ADDRESS = "cms.embtask.msg.encryption.cannot.get.encryptlogarea.address";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_SOURCEFILE_WRONG_POSTFIX = "cms.embtask.msg.encryption.wrong.sourcefile.wrong.postfix";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_CNT_HASNOT_SUBCONTENT = "cms.embtask.msg.encryption.program.hasnot.subcontent";
    public static final String CMS_EMBTASK_MSG_SUBCONTENT_STATUS_DOESNOT_CORRECT = "cms.embtask.msg.subcontent.status.doesnot.correct";
    public static final String CMS_EMBTASK_MSG_ENCRP_SUBCONTENT_STATUS_DOESNOT_CORRECT = "cms.embtask.msg.encry.subcontent.status.doesnot.correct";

    public static String setSourceFile()
    {
        ResourceMgt.addDefaultResourceBundle(CMS_EMBTASK_RES_FILE_NAME);
        return null;
    }
}

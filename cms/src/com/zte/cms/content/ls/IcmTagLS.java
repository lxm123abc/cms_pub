package com.zte.cms.content.ls;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.common.TagLogConstant;
import com.zte.cms.content.model.IcmTag;
import com.zte.cms.content.model.IcmTagContent;
import com.zte.cms.content.service.IIcmTagContentDS;
import com.zte.cms.content.service.IIcmTagDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class IcmTagLS implements IIcmTagLS
{

    private IIcmTagDS cmtagDS;
    private IIcmTagContentDS cmTagContentDS;
    private Log log = SSBBus.getLog(getClass());
    private static final String MES_EXISTS_FOR_OPENMSG = "1:该内容标签名称已经存在";
    private static final String MES_HAS_DEL_FOR_OPENMSG = "1:该内容标签已经被删除";
    private static final String MES_ERROR = "系统错误";
    private static final String MOD = "mod";
    private static final String ADD = "add";
    private static final String OPER_SUCCESS = "0:操作成功";
    private static final String MES_OTHER_CPID_MAKE_REL = "1:删除失败，该标签下存在其他CP的内容标签关联关系";
    private static final int NEW_TAG_RELATION_LIMIT = 100;
    private static final String MES_RALATION_MAX_LIMIT = "1:新增标签，关联的内容不能超过100条";
    private static final String MES_CNT_TAG_NO_CHOSEN = "1:没有标签被选中";
    private static final String MES_CNT_TAG_NO_EXISTS = "1:选中的标签已经被删除";

    public void setCmTagContentDS(IIcmTagContentDS cmTagContentDS)
    {
        this.cmTagContentDS = cmTagContentDS;
    }

    public void setCmtagDS(IIcmTagDS cmtagDS)
    {
        this.cmtagDS = cmtagDS;
    }

    public IcmTag getIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("com.zte.cms.content.ls IcmTagLS getIcmTag starting...");
        IcmTag rtnIcmTag = null;
        try
        {
            rtnIcmTag = cmtagDS.getIcmTag(icmTag);
        }
        catch (DomainServiceException e)
        {
            log.error("com.zte.cms.content.ls IcmTagLS getIcmTag exception:" + e);
            throw e;
        }
        log.debug("com.zte.cms.content.ls IcmTagLS getIcmTag end");
        return rtnIcmTag;
    }

    /**
     * 根据条件分页查询IcmTag对象
     * 
     * @param icmTag IcmTag对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(IcmTag icmTag, int start, int pageSize) throws DomainServiceException
    {
        log.debug("pageInfoQuery starting...");
        TableDataInfo info = null;
        try
        {
            String converTagNameValue = EspecialCharMgt.conversion(icmTag.getTagname());
            icmTag.setTagname(converTagNameValue);
            info = cmtagDS.pageInfoUnclearQuery(icmTag, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.debug("pageInfoQuery end");
            throw e;
        }

        return info;
    }

    /**
     * 删除IcmTag对象
     * 
     * @param icmTag IcmTag对象
     * @throws DomainServiceException ds异常
     */
    public void removeIcmTag(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("remove IcmTag starting...");
        try
        {
            //cmTagContentDS.removeTagContentByTagindex(icmTag.getTagindex());
            cmtagDS.removeIcmTag(icmTag);

        }
        catch (DomainServiceException e)
        {
            log.error("remove IcmTag exception:" + e);
            throw e;
        }
        log.debug("remove IcmTag end");
    }

    /**
     * 修改内容标签对象
     * 
     * @param icmTag IcmTag 对象
     * @return 操作返回值
     */
    public String updateIcmTagByObj(IcmTag icmTag) throws DomainServiceException
    {
        log.debug("update IcmTag By Obj starting...");
        String message = "";
        try
        {
            IcmTag nameIcmTag = new IcmTag();
            // 根据内容标签名称查询数据库
            nameIcmTag.setTagname(icmTag.getTagname());
            List<IcmTag> icmTagList = cmtagDS.getIcmTagByCond(nameIcmTag);
            if (icmTagList != null && icmTagList.size() > 0)
            {
                nameIcmTag = icmTagList.get(0);
                // 如果根据名称查询的内容标签与待修改的内容标签的主键不一致，说明修改的内容标签
                // 名字与别的内容标签重名。
                if (nameIcmTag.getTagindex().longValue() != icmTag.getTagindex().longValue())
                {
                    return MES_EXISTS_FOR_OPENMSG;
                }
            }

            IcmTag indexIcmTag = new IcmTag();
            indexIcmTag.setTagindex(icmTag.getTagindex());
            indexIcmTag = cmtagDS.getIcmTag(indexIcmTag);
            // 判断待修改的内容标签是否存在
            if (indexIcmTag == null)
            {
                return MES_HAS_DEL_FOR_OPENMSG;
            }
            // 修改内容标签
            cmtagDS.updateIcmTag(icmTag);
            message = OPER_SUCCESS;
        }
        catch (DomainServiceException e)
        {
            log.error("update IcmTag By Obj exception:" + e);
            throw e;
        }
        CommonLogUtil.insertOperatorLog(String.valueOf(icmTag.getTagindex()), TagLogConstant.MGTTYPE_TAG_CONTENT,
                TagLogConstant.OPERTYPE_LOG_TAG_MODIFY, TagLogConstant.OPERTYPE_LOG_TAG_MODIFY_INFO,
                TagLogConstant.RESOURCE_OPERATION_SUCCESS);
        log.debug("update IcmTag By end");
        return message;
    }

    public List<IcmTag> getIcmTagList() throws DomainServiceException
    {
        log.debug("get IcmTag List staring...");
        List<IcmTag> icmTagList = null;
        try
        {
            icmTagList = cmtagDS.getIcmTagList();
        }
        catch (DomainServiceException e)
        {
            log.error("get IcmTag List exception:" + e);
            throw e;
        }
        log.debug("get IcmTag List en");
        return icmTagList;
    }

    /**
     * 新增标签
     * 
     * @param icmTag IcmTag对象
     * @param contentIndex 内容index数组
     * @return 操作返回值
     */
    public String insertIcmTag(IcmTag icmTag, String[] contentids) throws DomainServiceException
    {
        log.debug("com.zte.cms.content.ls IcmTagLS insertIcmTag startind...");
        String message = "";
        List<String> returnList = new ArrayList<String>();
        if (contentids.length > NEW_TAG_RELATION_LIMIT)
        {
            message = MES_RALATION_MAX_LIMIT;

            return message;
        }
        try
        {
            // 判断内容标签是否重名
            List<IcmTag> icmTagListByNameList = cmtagDS.getIcmTagByCond(icmTag);
            if (icmTagListByNameList == null || icmTagListByNameList.size() == 0)
            {
                // 内容标签树只有两级，根节点已经规定为0，所以所有由客户端插入到数据库中的节点的
                // parentid都是为1。
                icmTag.setTagparentid(new Long(1));
                // 插入内容标签到数据库中
                cmtagDS.insertIcmTag(icmTag);
                message = OPER_SUCCESS;
                IcmTagContent icmTagContent = null;
                // 对已选中的内容ID进行循环插入到标签---内容关联表中
                for (int i = 0; i < contentids.length; i++)
                {
                    icmTagContent = new IcmTagContent();
                    icmTagContent.setContentid(contentids[i]);
                    icmTagContent.setContentid(contentids[i]);
                    icmTagContent.setTagindex(icmTag.getTagindex());
                    cmTagContentDS.insertIcmTagContent(icmTagContent);
                }

            }
            else
            {
                message = MES_EXISTS_FOR_OPENMSG;
                return message;
            }

        }
        catch (DomainServiceException e)
        {
            log.error("com.zte.cms.content.ls IcmTagLS insertIcmTag exception:" + e);
            throw e;
        }
        CommonLogUtil.insertOperatorLog(String.valueOf(icmTag.getTagindex()), TagLogConstant.MGTTYPE_TAG_CONTENT,
                TagLogConstant.OPERTYPE_LOG_TAG_ADD, TagLogConstant.OPERTYPE_LOG_TAG_ADD_INFO,
                TagLogConstant.RESOURCE_OPERATION_SUCCESS);
        log.debug("com.zte.cms.content.ls IcmTagLS insertIcmTag end");
        return message;
    }

    /**
     * 
     * 根据index删除标签
     * 
     * @param tagindex 标签主键
     * @return 操作返回值
     */
    public String removeTagByIndex(Long tagindex) throws DomainServiceException
    {
        log.debug("remove Tag By Index staring...");
        String message = "";
        IcmTag icmTag = null;
        try
        {
            icmTag = new IcmTag();
            icmTag.setTagindex(tagindex);
            icmTag = cmtagDS.getIcmTag(icmTag);
            if (icmTag == null)
            {
                message = MES_HAS_DEL_FOR_OPENMSG;
            }
            else
            {
                
                this.removeIcmTag(icmTag);
                message = OPER_SUCCESS;
                CommonLogUtil.insertOperatorLog(String.valueOf(tagindex), TagLogConstant.MGTTYPE_TAG_CONTENT,
                        TagLogConstant.OPERTYPE_LOG_TAG_DEL, TagLogConstant.OPERTYPE_LOG_TAG_DEL_INFO,
                        TagLogConstant.RESOURCE_OPERATION_SUCCESS);
            }
        }
        catch (DomainServiceException e)
        {
            log.error("remove Tag By Index exception:" + e);
            throw e;
        }
        return message;
    }

    /**
     * 根据请求模式分发修改，新增函数
     * 
     * @param icmTag 内容标签对象
     * @param contentid 内容ID数组，
     * @param mode 提交选择关键字 add/mod
     * @return 操作返回值
     */
    public String applyMode(IcmTag icmTag, String[] contentid, String mode) throws DomainServiceException
    {
        log.debug("applyMode starting...");
        String reValue = "";
        try
        {
            if (mode.equals(ADD))
            {
                reValue = this.insertIcmTag(icmTag, contentid);
            }
            else if (mode.equals(MOD))
            {
                reValue = this.updateIcmTagByObj(icmTag);
            }
            else
            {
                reValue = MES_ERROR;
            }
        }
        catch (DomainServiceException e)
        {
            log.error("applyMode exception:" + e);
            throw e;
        }
        log.debug("applyMode end");
        return reValue;
    }

    /**
     * 由标签的index得到标签的名称
     * 
     * @param indexAll，表签index由“：”拼接而成的字符串
     * @return
     * @throws Exception
     */
    public String getName(String indexAll) throws Exception
    {
        String nameTag = "";
        String indexStr[] = indexAll.split(":");
        if (indexStr.length == 0)
        {
            return MES_CNT_TAG_NO_CHOSEN;
        }
        for (int i = 0; i < indexStr.length; i++)
        {
            IcmTag icmtag = new IcmTag();
            icmtag.setTagindex(Long.valueOf(indexStr[i]));
            icmtag = cmtagDS.getIcmTag(icmtag);
            if (null == icmtag)
            {
                return MES_CNT_TAG_NO_EXISTS;
            }
            if (i == 0)
            {
                nameTag = icmtag.getTagname();
            }
            else
            {
                nameTag = nameTag + "," + icmtag.getTagname();
            }
        }
        return nameTag;

    }
}

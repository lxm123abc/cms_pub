package com.zte.ismp.log;

import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.umap.ssb.logmanager.business.model.AppLogInfoEntity;
import com.zte.umap.ssb.logmanager.business.service.LogInfoMgt;

/**
 * 
 * <p>
 * Title:
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class OperatorLog
{
    /**
     * 
     * @param objectId 操作对象编码
     * @param OperType 操作类型
     * @param optObjectType 操作对象类型
     * @param logDesc 操作详细描述
     */
    public static void insertOperatorLog(String objectId, String OperType, String optObjectType, String logDesc)
    {
        AppLogInfoEntity log = new AppLogInfoEntity();
        log.setOptObject(objectId);
        log.setOptType(OperType);
        log.setOptObjecttype(optObjectType);
        log.setOptDetail(logDesc);
        log.setServicekey(GlobalConstants.SYSTEM_SERVICE_KEY);
        LogInfoMgt.doLog(log);
    }
}

package com.zte.cms.notice.service;

import java.util.List;

import com.zte.cms.notice.model.Noticemap;
import com.zte.purview.access.model.OperInformation;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface INoticemapService
{

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap nastrolemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap noticemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap nastrolemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap noticemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryReplay(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap nastrolemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    /**
     * 根据条件分页查询Noticemap对象
     * 
     * @param Noticemap noticemap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryView(Noticemap noticemap, int start, int pageSize, PageUtilEntity puEntity) throws DomainServiceException;

    public Noticemap getNoticemap(Noticemap noticemap) throws DomainServiceException;
    
    public Noticemap getNoticeLettermap(Noticemap noticemap) throws DomainServiceException;

    public String insertNoticemap(Noticemap noticemap) throws DomainServiceException;

    /**
     * 增加消息与CP对应关系CMS_MESSAGE_CP
     * @param noticemap
     * @return
     * @throws DomainServiceException
     */
    public String insertCmsMessageCp(Integer messageid, String receiverid, Integer messagetype) throws DomainServiceException;

    public String insertCmsMessageView(String messageid, String cpid,String operatorid) throws DomainServiceException;
    
    public void insertCmsMessageForward(int messageid, int oldMessageid) throws DomainServiceException;

    public void deleteNoticeMessageCp(Noticemap noticemap) throws DomainServiceException;

    public void deleteNoticeCmsMessageView(Noticemap noticemap) throws DomainServiceException;

    public void deleteNoticeCmsReplay(Noticemap noticemap) throws DomainServiceException;

    public void deleteNoticemap(Noticemap noticemap) throws DomainServiceException;
    
    public void revokeNoticemap(Noticemap noticemap) throws DomainServiceException;
    
    public void updateNoticemap(Noticemap noticemap) throws DomainServiceException;

    public TableDataInfo notificationInfoQuery(Noticemap noticemap, int start, int pageSize) throws DomainServiceException;

    public int getNumOfUnView(Noticemap noticemap);

    public Noticemap getNotificationDetail(Noticemap noticemap) throws DomainServiceException;
    
    public Noticemap getCmsOperatorPowerInfo(Noticemap noticemap) throws DomainServiceException;

    public String insertReplay(Noticemap noticemap) throws DomainServiceException;

    public void updateView(Noticemap noticemap) throws DomainServiceException;
    
    public void updateReceiver(Noticemap noticemap) throws DomainServiceException;

    public void updateReplay(Noticemap noticemap) throws DomainServiceException;

    public void setStatusRead(Noticemap noticemap) throws DomainServiceException;
    
    /**
     * 根据cp跟新cms_message表：viewcpnum
     */
    public void setNoticeViewcpnum(Noticemap noticemap) throws DomainServiceException;

    public void deleteReplay(Noticemap noticemap) throws DomainServiceException;
    
    public void deleteAttach(Noticemap noticemap) throws DomainServiceException;
    
    public void updateAttach(Noticemap noticemap) throws DomainServiceException;
    
    /**
     * 查询操作员列表 用于管理员和cp操作员分别发送私信
     * 管理员可以发送私信给所有操作员（包括非cp操作员）
     * cp操作员只能发送私信给管理员
     * @param start
     * @param pageSize
     * @return
     * @throws DomainServiceException
     */
    public TableDataInfo operInfoQuery(OperInformation operInfo,int start, int pageSize) throws DomainServiceException;
    
    public TableDataInfo adminInfoQuery(OperInformation operInfo,int start, int pageSize) throws DomainServiceException;

    public void updateNoticeReplay(Noticemap noticemap) throws DomainServiceException;
	
	public List<Noticemap> getNotificationDetails(Noticemap noticemap)throws DomainServiceException;

}
    var processId = {};
    var eventMap = {};
    
    function initWkfw() {
        callSid('getTask',_para.taskId,'$M.task');
    }
    
    function dealTaskInfo() {
        var td = document.getElementById('tdHandle');
        var routType = document.getElementById('txtRoutType').value;
        var routEvent = document.getElementById('txtRoutEvent').value;
        if(routEvent != "") {
            if(routType == 'M') { 
                var routEvents = routEvent.split(";");
                for(var i = 0; i < routEvents.length; i ++) {
                    var checkBox = document.createElement("<input name='checkbox' />");
                    checkBox.setAttribute("type", "checkbox");
                    checkBox.checked = true;
                    checkBox.setAttribute("id", "id" + i);
                    checkBox.setAttribute("value", "Y");
                    var label = document.createElement("label");
                    label.setAttribute("id", "labelId" + i);
                    if (eventMap[routEvents[i]] != undefined) {
                        label.innerText = eventMap[routEvents[i]];
                    } else {
                        label.innerText = routEvents[i];
                    }
                    td.appendChild(checkBox);
                    td.appendChild(label);
                }
            } else if(routType == 'S') {
                var routEvents = routEvent.split(";");
                for(var i = 0; i < routEvents.length; i++) {
                    var radioButton = document.createElement("<input name='radioButton' " + (i == 0 ? "checked" : "") + " />");
                    radioButton.setAttribute("type", "radio");
                    radioButton.setAttribute("id", "id" + i);
                    radioButton.setAttribute("value", routEvents[i]);
                       
                    var label = document.createElement("label");
                    label.setAttribute("id", 'labelId' + i);
                    if (eventMap[routEvents[i]] != undefined) {
                        label.innerText = eventMap[routEvents[i]];
                    } else {
                        label.innerText = routEvents[i];
                    }
                    td.appendChild(radioButton);
                    td.appendChild(label);
                }
            }
        } else {
            document.getElementById("trHandle").style.display = "none";
        }
        
        processId = document.getElementById('txtProcessId').value;
        searchTableData('getHistory',processId,'tblHistoryList');
    }
    
    function getHandleEvent() {
        var handleEvent;
        var routType = document.getElementById('txtRoutType').value;
        if(routType == 'M') {
            var o = document.getElementsByName('checkbox');
            for(var i = 0; i < o.length; i ++) {
                if(o[i].checked == true) {
                    handleEvent = handleEvent + ';' + o[i].value;
                }
            }
        } else if(routType == 'S') {   
            var o = document.getElementsByName('radioButton');
            for(var i = 0; i < o.length; i ++) {
                if(o[i].checked == true) {
                    handleEvent = o[i].value;
                    break;
                }
            }
        }
        return handleEvent;
    }
    
    function noDataDisplay(obj, tableId) {
        if(obj.length == 0) {
            document.getElementById(tableId).style.display = "none";
        }
    }
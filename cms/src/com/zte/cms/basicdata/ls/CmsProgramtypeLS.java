package com.zte.cms.basicdata.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.basicdata.common.CmsProgramtypeConstant;
import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.cms.basicdata.service.ICmsProgramtypeDS;
import com.zte.cms.basicdata.service.ICmsProgramtypecpMapDS;
import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.operCnttypeMap.model.OperCnttypemap;
import com.zte.cms.operCnttypeMap.service.IOperCnttypemapDS;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.cpsp.common.model.UcpBasic;
import com.zte.umap.cpsp.common.service.IUcpBasicDS;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class CmsProgramtypeLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements ICmsProgramTypeLS
{

    private ICmsProgramtypeDS cmsProgramtypeDS;
    private ICmsProgramtypecpMapDS cmsProgramtypecpMapDS;
    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private static final String RESOURCE_ASSESSRULE_cancelnotforCondition = "cmsprogramtype.canclepublish.notforCondition"; // 节目分类信息取消发布不符合条件
    private static final String RESOURCE_ASSESSRULE_IsnotExist = "cmsprogramtype.reason.isnotExist";// 节目分类信息不存在
    private static final String Fail = "cmsprogramtype.fail";
    private static final String Success = "cmsprogramtype.success";
    private static final String CANCLE_PUBLISH_SUCCESS = "cmsprogramtype.canclepublish.success";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";
    private DbUtil db = new DbUtil();
    private IUsysConfigDS usysConfigDS = null;
    private final static String CMSPROGRAMTYPE_CNTSYNCXML = "cntsyncxml";
    private final static String CMSPROGRAMTYPE_PROGRAMTYPE = "programtype";
    private IUcpBasicDS basicDS;

    private IOperCnttypemapDS operCnttypemapDS;
    public void setOperCnttypemapDS(IOperCnttypemapDS operCnttypemapDS)
    {
        this.operCnttypemapDS = operCnttypemapDS;
    }

    /**
     * 按条件显示树
     */
    public List<CmsProgramtype> listMenuTreeByParentid(String typeid)
    {
        log.debug("listMenuTreeByParentid start...");
        List<CmsProgramtype> listCmsprogramtype = null;
        listCmsprogramtype = cmsProgramtypeDS.listMenuTreeByParentid(typeid);
        log.debug("listMenuTreeByParentid end");
        return listCmsprogramtype;
    }

    /**
     * 按照id查询cmsprogramtype信息
     */
    public CmsProgramtype getCmsProgramtypeById(Long id)
    {
        log.debug("getCmsProgramtypeById start...");
        CmsProgramtype cmsProgramtype = null;
        try
        {
            cmsProgramtype = cmsProgramtypeDS.getCmsProgramtypeById(id);
        }
        catch (RuntimeException e)
        {
            log.error("getCmsProgramtypeById error");
        }
        log.debug("getCmsProgramtypeById end");
        return cmsProgramtype;
    }

    public CmsProgramtype getCmsprogramtype(Long typeindex)
    {
        return cmsProgramtypeDS.getCmsProgramtype(typeindex);
    }

    public CmsProgramtype getCmsprogramtypeByTypeindex(Long typeindex)
    {
        return cmsProgramtypeDS.getCmsprogramtypeByTypeindex(typeindex);
    }

    public String getTypeid(Long typeindex) throws Exception
    {
        String sql = "select typeid from cms_programtype where typeindex=" + typeindex;
        List list = new DbUtil().getQuery(sql);
        String typeid = "";
        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            HashMap map = (HashMap) iterator.next();
            typeid = (String) map.get("typeid");
        }
        return typeid;
    }

    /**
     * 列表
     */
    public TableDataInfo queryCmsProgramtype(CmsProgramtype cmsProgramtype, int start, int pageSize)
    {
        log.debug("queryCmsProgramtype start...");
        TableDataInfo dataInfo = null;
        try
        {
            if (null != cmsProgramtype.getParentid() && cmsProgramtype.getParentid().equals(""))
            {
                cmsProgramtype.setParentid(null);
            }
            if (null != cmsProgramtype.getTypeid() && !"".equals(cmsProgramtype.getTypeid()))
            {
                cmsProgramtype.setTypeid(EspecialCharMgt.conversion(cmsProgramtype.getTypeid()));
            }
            if (null != cmsProgramtype.getTypename() && !"".equals(cmsProgramtype.getTypename()))
            {
                cmsProgramtype.setTypename(EspecialCharMgt.conversion(cmsProgramtype.getTypename()));
            }
            dataInfo = cmsProgramtypeDS.queryCmsProgramtype(cmsProgramtype, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("queryCmsProgramtype error");
        }
        log.debug("queryCmsProgramtype end");
        return dataInfo;
    }
    
    /**
     * 产品分类信息删除
     */
    public String removeProgramtype(Long id)
    {
        log.debug("deleteProgramtype start...");
        CmsProgramtype cmsProgramtype = getCmsprogramtypeByTypeindex(id);
        HashMap map = new HashMap();
        StringBuffer sb = new StringBuffer();
        String sql = "";
        String Sql="";
        List list = null;
        List sList=null;
        String delSql = "";
        Boolean isRight = false;
        try
        {
            if (cmsProgramtype != null)
            {
                if (cmsProgramtype.getPublishstatus() != 10)
                {// 判断状态
                  
                            sql = "select * from cms_programtypecp_map ptcp where ptcp.typeid='"+cmsProgramtype.getTypeid()+"'";   
                            Sql = "select * from oper_cnttypemap ptop where  ptop.typeid='"+cmsProgramtype.getTypeid()+"'";         
                            list = db.getQuery(sql);
                            sList = db.getQuery(Sql);
                            if (list!=null&&list.size()>0)
                            {
                              //return "1:节目与其他CP或操作员有关联关系，不能删除";
                            	return ResourceMgt.findDefaultText("basic.delete.cannot.programerelatecp");
                            }else if(sList!=null&&sList.size()>0){
                              //return "1:节目与其他CP或操作员有关联关系，不能删除";
                                return ResourceMgt.findDefaultText("basic.delete.cannot.programerelatecp");
                            }                          
                            else
                            {// 没有任何CP关联信息，直接删除
                                cmsProgramtypeDS.deleteProgramtype(id);
                                CommonLogUtil.insertOperatorLog(cmsProgramtype.getTypeid(),
                                        CmsProgramtypeConstant.MGTTYPE_CMSPROGRAMTYPE,
                                        CmsProgramtypeConstant.OPERTYPE_CMSPROGRAMTYPE_DEL,
                                        CmsProgramtypeConstant.OPERTYPE_CMSPROGRAMTYPE_DEL_INFO,
                                        CmsProgramtypeConstant.RESOURCE_OPERATION_SUCCESS);
                                //return "0:操作成功";                       
                                return ResourceMgt.findDefaultText("basic.do.success");
                            }
                }
                else
                {
                    //return "1:当前状态不能删除";
                	return ResourceMgt.findDefaultText("basic.delet.cannotforstatus");
                }
            }
            else
            {
                //return "1:节目分类信息不存在";
            	  return ResourceMgt.findDefaultText("basic.programclass.notexist");
            }
        }
        catch (Exception e)
        {
            log.error(e);
            //return "1:系统异常";
            return ResourceMgt.findDefaultText("basicls.system.abnormal");
        }
    }

    /**
     * 产品分类信息修改
     */
    public HashMap updateProgramtype(CmsProgramtype cmsProgramtype)
    {
        log.debug("programtypeUpdate start...");
        HashMap map = new HashMap();
        map.put("cmsprogramtype", cmsProgramtype);
        String rtnStr = "";
        CmsProgramtype programtype = new CmsProgramtype();
        programtype.setTypeindex(cmsProgramtype.getTypeindex());
        programtype = getProgramtypeByCond(programtype);
        if (programtype == null)
        {// 对象不存在
            rtnStr = "1";// 对象不存在
            map.put("result", rtnStr);
            return map;
        }
       /** if (programtype.getPublishstatus() != 10 && programtype.getPublishstatus() != 41)
        {
            rtnStr = "1:当前状态不能修改";// 状态不正确
            map.put("result", rtnStr);
            return map;
        }
        IcmContentBasicinfo icmContentBasicinfo = new IcmContentBasicinfo();
        icmContentBasicinfo.setContenttags(cmsProgramtype.getTypeid());
        if (basicinfoLS.getIcmContentBasicinfoByCond(icmContentBasicinfo))
        {
            rtnStr = "1:节目分类信息已被内容使用，不能修改";
            map.put("result", rtnStr);
            return map;
        }**/

        cmsProgramtypeDS.programtypeUpdate(cmsProgramtype);

        CommonLogUtil.insertOperatorLog(cmsProgramtype.getTypeid(), CmsProgramtypeConstant.MGTTYPE_CMSPROGRAMTYPE,
                CmsProgramtypeConstant.OPERTYPE_CMSPROGRAMTYPE_UPD,
                CmsProgramtypeConstant.OPERTYPE_CMSPROGRAMTYPE_UPD_INFO,
                CmsProgramtypeConstant.RESOURCE_OPERATION_SUCCESS);
        // rtnStr = "0:操作成功";
        rtnStr = ResourceMgt.findDefaultText("basic.do.success");
        map.put("result", rtnStr);
        return map;
    }

    /**
     * 产品分类信息添加
     */
    public HashMap insertProgramtype(CmsProgramtype cmsProgramtype)
    {
        log.debug("programtypeUpdate start...");
        HashMap map = new HashMap();
        StringBuffer sb = new StringBuffer();
        try
        {
            // cmsProgramtype.setParentid("0");
            cmsProgramtype.setClslevel(2);
            cmsProgramtype.setPublishstatus(Long.parseLong(0+""));
            cmsProgramtypeDS.programtypeInsert(cmsProgramtype);
          //sb.append("0:操作成功");
            sb.append(ResourceMgt.findDefaultText("basic.do.success"));
        }
        catch (Exception e)
        {
            log.error(e);
            sb.append(ResourceMgt.findDefaultText("basic.do.failed"));
        }
        map.put("result", sb.toString());
        map.put("cmsprogramtype", cmsProgramtype);
        log.debug("programtypeUpdate end");
        return map;
    }

    /**
     * 按照typeid查询 用于添加时判断是否有相同的typeid
     */
    public Integer queryByTypeid(CmsProgramtype cmsProgramtype)
    {
        log.debug("deleteProgramtype start...");
        Integer isExists = 0;
        isExists = cmsProgramtypeDS.queryByTypeid(cmsProgramtype);
        log.debug("deleteProgramtype end");
        return isExists;
    }

    /**
     * 按照typeid和typename进行查询 用于修改和添加时判断在同一节点写是否拥有同名的节目分类信息
     */
    public Integer queryByTypename(CmsProgramtype cmsProgramtype)
    {
        log.debug("queryByTypename start...");
        Integer count = 0;
        count = cmsProgramtypeDS.queryByTypename(cmsProgramtype);
        log.debug("queryByTypename end");
        return count;
    }

    /**
     * 判断同一节点下是否有同名的节目分类信息
     */
    public Long queryTypeindexByTypename(CmsProgramtype cmsProgramtype)
    {
        log.debug("queryTypeindexByTypename start...");
        Long typeindex = null;
        typeindex = cmsProgramtypeDS.queryTypeindexByTypename(cmsProgramtype);
        log.debug("queryTypeindexByTypename end");
        return typeindex;
    }

    /**
     * 在节目分类信息（取消）发布时，如果节点下有子节点，要将子节点一起（取消）发布
     */
    public List<CmsProgramtype> listCmsprogramtypeByParentid(CmsProgramtype cmsProgramtype)
    {
        log.debug("listCmsprogramtypeByParentid start...");
        List<CmsProgramtype> listCmsprogramtype = null;
        listCmsprogramtype = cmsProgramtypeDS.listCmsprogramtypeByParentid(cmsProgramtype);
        log.debug("listCmsprogramtypeByParentid end");
        return listCmsprogramtype;
    }

    /**
     * 按照typeid查询，得到cmsprogramtype
     */
    public CmsProgramtype getCmsProgramtype(String typeid)
    {
        log.debug("getCmsProgramtype start...");
        CmsProgramtype cmsProgramtype = null;
        cmsProgramtype = cmsProgramtypeDS.getCmsProgramtype(typeid);
        log.debug("getCmsProgramtype end");
        return cmsProgramtype;
    }

    /**
     * 判断该节点下又不是还有子节点被内容使用中，如果在使用中就不能取消发布
     * 



#log.please.choice=--请选择--
return ResourceMgt.findDefaultText("log.please.choice");
     */
    public Integer isHasChild(String typeid)
    {
        log.debug("isHasChild start...");
        Integer isHasChild = 0;
        isHasChild = cmsProgramtypeDS.isHasChild(typeid);
        log.debug("isHasChild end");
        return isHasChild;
    }

    // 新增CP关联
    public String insertCP(CmsProgramtypecpMap cmsProgramtypecpMap) throws Exception
    {
        StringBuffer str = new StringBuffer();
        Long index = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_programtypecp_map");

        // 首先查看所选的CP已经关联
        String cpSql = "select cpid,typeid from cms_programtypecp_map where typeid='" + cmsProgramtypecpMap.getTypeid()
                + "' and cpid='" + cmsProgramtypecpMap.getCpid() + "'";
        List list = new DbUtil().getQuery(cpSql);// 查看节目关联的所有CP
        String typeid = "";
        String cpid = "";
        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            HashMap map = (HashMap) iterator.next();
            cpid = (String) map.get("cpid");
            typeid = (String) map.get("typeid");
        }

        if (cpid.equals(""))
        {
            UcpBasic basic = new UcpBasic();
            basic.setCpid(cmsProgramtypecpMap.getCpid());
            basic.setCptype(1);
            List listCP = basicDS.getUcpBasicByCond(basic);
            if (listCP != null && listCP.size() > 0)
            {
                basic = (UcpBasic) listCP.get(0);
            }
            String sql = "insert into cms_programtypecp_map(mapindex,typeid,cpid) values(" + index + ",'"
                    + cmsProgramtypecpMap.getTypeid() + "'," + "'" + basic.getCpid() + "')";
            db.excute(sql);
            str.append("");
        }
        else
        {
            if (!cpid.equals(cmsProgramtypecpMap.getCpid()) && !typeid.equals(cmsProgramtypecpMap.getTypeid()))
            {
                UcpBasic basic = new UcpBasic();
                basic.setCpid(cpid);
                basic.setCptype(1);
                List listCP = basicDS.getUcpBasicByCond(basic);
                if (listCP != null && listCP.size() > 0)
                {
                    basic = (UcpBasic) listCP.get(0);
                }
                String sql = "insert into cms_programtypecp_map(mapindex,typeid,cpid) values(" + index + ",'"
                        + cmsProgramtypecpMap.getTypeid() + "'," + "'" + basic.getCpid() + "')";
                db.excute(sql);
                str.append("");
            }
            else
            {
                //str.append("所选的CP已关联该节目");
            	str.append(ResourceMgt.findDefaultText("basicls.cp.related.programe"));
            }
        }
        return str.toString();
    }

    private String getTempAreAdd() throws Exception
    {
        ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
        String stroageareaPath = storageareaLs.getAddress("1");// 临时区
        return stroageareaPath;
    }

    // 取出同步XML文件FTP地址
    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }
    
    public List<CmsProgramtype> getCmsProgramtypeByCond(CmsProgramtype cmsProgramtype)
    {
        log.debug("getCmsProgramtypeByCond starting.....");
        List<CmsProgramtype> list = null;
        try
        {
            list = cmsProgramtypeDS.getCmsProgramtypeByCond(cmsProgramtype);
        }
        catch (Exception e)
        {
            log.error("createScheduleXml exception:" + e);
        }

        log.debug("getCmsProgramtypeByCond end");
        return list;
    }
   
    /**
     * 操作员关联对象时查询节目分类信息
     * **/
    public List<CmsProgramtype> getCmsProgramtypeForOperByCond(CmsProgramtype cmsProgramtype,String creatorid)
    {
        log.debug("getCmsProgramtypeByCond starting.....");
        List<CmsProgramtype> list = new ArrayList<CmsProgramtype>();
        List<OperCnttypemap> operCntMaplist = null;
        try
        {
            //creatorid是0说明该操作员是草机操作员创建的超级操作员如super、guest；1表示是超级操作员（super）创建的一级操作员。
            if(creatorid.equals("0")||creatorid.equals("1")){
                list = cmsProgramtypeDS.getCmsProgramtypeByCond(cmsProgramtype);
            }else{
                OperCnttypemap operCnttypemap=new OperCnttypemap();
                operCnttypemap.setOperid(Long.parseLong(creatorid));
                operCntMaplist=operCnttypemapDS.getOperCnttypemapByCond(operCnttypemap);
                if(operCntMaplist!=null&&operCntMaplist.size()>0){
                    for(OperCnttypemap ocmap:operCntMaplist){
                        list.add(cmsProgramtypeDS.getCmsProgramtype(ocmap.getTypeid()));
                    }
                }
            }            
        }
        catch (Exception e)
        {
            log.error("createScheduleXml exception:" + e);
        }

        log.debug("getCmsProgramtypeByCond end");
        return list;
    }

    // 得到第一个根节点
    public List getFirstRootProgramtype() throws Exception
    {
        List list = new ArrayList();
        String sql = "select * from cms_programtype where typeindex=(select min(typeindex) from cms_programtype where parentid='0')";
        list = db.getQuery(sql);
        return list;
    }

    public CmsProgramtype getProgramtypeByCond(CmsProgramtype cmsProgramtype)
    {
        CmsProgramtype programtype = new CmsProgramtype();
        try
        {
            programtype = cmsProgramtypeDS.getProgramtypeByCond(cmsProgramtype);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
        return programtype;
    }

    public UcpBasic getCPnameByCPid(UcpBasic basic) throws DomainServiceException
    {
        UcpBasic ucpbasicRtn = new UcpBasic();
        List list = basicDS.getUcpBasicByCond(basic);
        if (list != null && list.size() > 0)
        {
            ucpbasicRtn = (UcpBasic) list.get(0);
        }
        ucpbasicRtn = (UcpBasic) list.get(0);
        return ucpbasicRtn;
    }

    public HashMap getProgramtypeMap(CmsProgramtype cmsProgramtype)
    {
        log.debug("getCmsProgramtype starting...");
        ResourceMgt.addDefaultResourceBundle("cms_log_resource");
        HashMap<String, Object> map = new HashMap<String, Object>();
        List<String> optionList = new ArrayList<String>();
        List<String> valueList = new ArrayList<String>();
        try
        {
            //optionList.add("--请选择--");
            optionList.add(ResourceMgt.findDefaultText("log.please.choice"));
            valueList.add("");
            List<CmsProgramtype> list = cmsProgramtypeDS.getCmsProgramtypeByCond(cmsProgramtype);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                { // 循环省份集合
                    optionList.add(list.get(i).getTypename());
                    valueList.add(list.get(i).getTypeid());
                }
            }

            map.put("codeText", optionList);
            map.put("codeValue", valueList);
            map.put("defaultValue", "");

        }
        catch (Exception e)
        {

            log.error("CmsProgramtypeLS exception:" + e.getMessage());
        }
        log.debug("getCmsProgramtype end");
        return map;
    }

    public ICmsProgramtypeDS getCmsProgramtypeDS()
    {
        return cmsProgramtypeDS;
    }

    public void setCmsProgramtypeDS(ICmsProgramtypeDS cmsProgramtypeDS)
    {
        this.cmsProgramtypeDS = cmsProgramtypeDS;
    }


    public ICmsProgramtypecpMapDS getCmsProgramtypecpMapDS()
    {
        return cmsProgramtypecpMapDS;
    }

    public void setCmsProgramtypecpMapDS(ICmsProgramtypecpMapDS cmsProgramtypecpMapDS)
    {
        this.cmsProgramtypecpMapDS = cmsProgramtypecpMapDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public IUcpBasicDS getBasicDS()
    {
        return basicDS;
    }

    public void setBasicDS(IUcpBasicDS basicDS)
    {
        this.basicDS = basicDS;
    }
}

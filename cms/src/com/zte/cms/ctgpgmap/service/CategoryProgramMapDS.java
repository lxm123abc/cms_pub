package com.zte.cms.ctgpgmap.service;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.cms.ctgpgmap.dao.ICategoryProgramMapDAO;
import com.zte.cms.ctgpgmap.model.CategoryProgramMap;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CategoryProgramMapDS extends DynamicObjectBaseDS implements ICategoryProgramMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICategoryProgramMapDAO dao = null;

    public void setDao(ICategoryProgramMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException
    {
        log.debug("insert categoryProgramMap starting...");
        try
        {
            if(categoryProgramMap.getMapindex() == null){
                Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("category_program_map_index");
                String mappingid = String.format("%02d", 26)+ String.format("%030d", mapindex);
                categoryProgramMap.setMapindex(mapindex);
                categoryProgramMap.setMappingid(mappingid);
            }
            dao.insertCategoryProgramMap(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert categoryProgramMap end");
    }

    public void updateCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException
    {
        log.debug("update categoryProgramMap by pk starting...");
        try
        {
            dao.updateCategoryProgramMap(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categoryProgramMap by pk end");
    }

    public void updateCategoryProgramMapList(List<CategoryProgramMap> categoryProgramMapList)
            throws DomainServiceException
    {
        log.debug("update categoryProgramMapList by pk starting...");
        if (null == categoryProgramMapList || categoryProgramMapList.size() == 0)
        {
            log.debug("there is no datas in categoryProgramMapList");
            return;
        }
        try
        {
            for (CategoryProgramMap categoryProgramMap : categoryProgramMapList)
            {
                dao.updateCategoryProgramMap(categoryProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update categoryProgramMapList by pk end");
    }

    public void removeCategoryProgramMap(CategoryProgramMap categoryProgramMap) throws DomainServiceException
    {
        log.debug("remove categoryProgramMap by pk starting...");
        try
        {
            dao.deleteCategoryProgramMap(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categoryProgramMap by pk end");
    }

    public void removeCategoryProgramMapList(List<CategoryProgramMap> categoryProgramMapList)
            throws DomainServiceException
    {
        log.debug("remove categoryProgramMapList by pk starting...");
        if (null == categoryProgramMapList || categoryProgramMapList.size() == 0)
        {
            log.debug("there is no datas in categoryProgramMapList");
            return;
        }
        try
        {
            for (CategoryProgramMap categoryProgramMap : categoryProgramMapList)
            {
                dao.deleteCategoryProgramMap(categoryProgramMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove categoryProgramMapList by pk end");
    }

    public CategoryProgramMap getCategoryProgramMap(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException
    {
        log.debug("get categoryProgramMap by pk starting...");
        CategoryProgramMap rsObj = null;
        try
        {
            rsObj = dao.getCategoryProgramMap(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryProgramMapList by pk end");
        return rsObj;
    }

    public List<CategoryProgramMap> getCategoryProgramMapByCond(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException
    {
        log.debug("get categoryProgramMap by condition starting...");
        List<CategoryProgramMap> rsList = null;
        try
        {
            rsList = dao.getCategoryProgramMapByCond(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryProgramMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get categoryProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categoryProgramMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryProgramMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CategoryProgramMap categoryProgramMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get categoryProgramMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(categoryProgramMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CategoryProgramMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get categoryProgramMap page info by condition end");
        return tableInfo;
    }
    /****** publish management *************/

    @SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQueryctprMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get programPlatformSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQueryctprMap(categoryProgramMap, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramPlatformSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programPlatformSyn page info by condition end" );
		return tableInfo;
	}   
    
    @SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQueryctprTargetMap(CategoryProgramMap categoryProgramMap, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get programPlatformSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQueryctprTargetMap(categoryProgramMap, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramPlatformSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programPlatformSyn page info by condition end" );
		return tableInfo;
	} 

    public List<CategoryProgramMap> getnormaltargetSynListByCond(CategoryProgramMap categoryProgramMap)
            throws DomainServiceException
    {
        log.debug("get categoryProgramMap by condition starting...");
        List<CategoryProgramMap> rsList = null;
        try
        {
            rsList = dao.getnormaltargetSynListByCond(categoryProgramMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get categoryProgramMap by condition end");
        return rsList;
    }
    /****** publish management end ********/
}

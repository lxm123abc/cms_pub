package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CategoryChannelMapDAO extends DynamicObjectBaseDao implements
		ICategoryChannelMapDAO {
	// 日志
	private Log log = SSBBus.getLog(getClass());

	public void insertCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DAOException {
		log.debug("insert categoryChannelMap starting...");
		super.insert("insertCategoryChannelMap", categoryChannelMap);
		log.debug("insert categoryChannelMap end");
	}

	public void updateCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DAOException {
		log.debug("update categoryChannelMap by pk starting...");
		super.update("updateCategoryChannelMap", categoryChannelMap);
		log.debug("update categoryChannelMap by pk end");
	}

	public void updateCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap) throws DAOException {
		log.debug("update categoryChannelMap by conditions starting...");
		super.update("updateCategoryChannelMapByCond", categoryChannelMap);
		log.debug("update categoryChannelMap by conditions end");
	}

	public void deleteCategoryChannelMap(CategoryChannelMap categoryChannelMap)
			throws DAOException {
		log.debug("delete categoryChannelMap by pk starting...");
		super.delete("deleteCategoryChannelMap", categoryChannelMap);
		log.debug("delete categoryChannelMap by pk end");
	}

	public void deleteCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap) throws DAOException {
		log.debug("delete categoryChannelMap by conditions starting...");
		super.delete("deleteCategoryChannelMapByCond", categoryChannelMap);
		log.debug("update categoryChannelMap by conditions end");
	}

	public CategoryChannelMap getCategoryChannelMap(
			CategoryChannelMap categoryChannelMap) throws DAOException {
		log.debug("query categoryChannelMap starting...");
		CategoryChannelMap resultObj = (CategoryChannelMap) super
				.queryForObject("getCategoryChannelMap", categoryChannelMap);
		log.debug("query categoryChannelMap end");
		return resultObj;
	}

	@SuppressWarnings("unchecked")
	public List<CategoryChannelMap> getCategoryChannelMapByCond(
			CategoryChannelMap categoryChannelMap) throws DAOException {
		log.debug("query categoryChannelMap by condition starting...");
		List<CategoryChannelMap> rList = (List<CategoryChannelMap>) super
				.queryForList("queryCategoryChannelMapListByCond",
						categoryChannelMap);
		log.debug("query categoryChannelMap by condition end");
		return rList;
	}

	public List<CategoryChannelMap> getCategoryChannelMapByCond2(
			CategoryChannelMap categoryChannelMap) throws DAOException {
		log.debug("query categoryChannelMap2 by condition starting...");
		List<CategoryChannelMap> rList = (List<CategoryChannelMap>) super
				.queryForList("queryCategoryChannelMapListByCond2",
						categoryChannelMap);
		log.debug("query categoryChannelMap2 by condition end");
		return rList;
	}

	@SuppressWarnings("unchecked")
	public PageInfo pageInfoQuery(CategoryChannelMap categoryChannelMap,
			int start, int pageSize) throws DAOException {
		log.debug("page query categoryChannelMap by condition starting...");
		PageInfo pageInfo = null;
		int totalCnt = ((Integer) super.queryForObject("pageInfoCnt",
				categoryChannelMap)).intValue();
		if (totalCnt > 0) {
			int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start)
					: pageSize;
			List<CategoryChannelMap> rsList = (List<CategoryChannelMap>) super
					.pageQuery("pageInfo", categoryChannelMap, start, fetchSize);
			pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
		} else {
			pageInfo = new PageInfo();
		}
		log.debug("page query categoryChannelMap by condition end");
		return pageInfo;
	}

}
package com.zte.cms.targetSystem.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class Targetsystem extends DynamicBaseObject
{
    private java.lang.Long targetindex;
    private java.lang.String targetid;
    private java.lang.String targetname;
    private java.lang.Integer status;
    private java.lang.Integer platform;
    private java.lang.Integer targettype;
    private java.lang.String changetime;
    private java.lang.String description;
    private java.lang.String msgurl;
    private java.lang.String msgurl1;
    private java.lang.String msgurl2;
    private java.lang.String msgurl3;
    private java.lang.String msgurl4;
    private java.lang.Integer reserve01;
    private java.lang.String reserve02;
    private java.lang.String objtype;
    private java.lang.String platforms;
    private java.lang.String targetindexes;
    private java.lang.String targettypes;
    
    
    public java.lang.String getTargettypes() {
		return targettypes;
	}

	public void setTargettypes(java.lang.String targettypes) {
		this.targettypes = targettypes;
	}

	public java.lang.String getPlatforms() {
		return platforms;
	}

	public void setPlatforms(java.lang.String platforms) {
		this.platforms = platforms;
	}

	

	public java.lang.String getTargetindexes() {
		return targetindexes;
	}

	public void setTargetindexes(java.lang.String targetindexes) {
		this.targetindexes = targetindexes;
	}

	public java.lang.String getObjtype() {
		return objtype;
	}

	public void setObjtype(java.lang.String objtype) {
		this.objtype = objtype;
	}

	public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.Integer getReserve01()
    {
        return reserve01;
    }

    public void setReserve01(java.lang.Integer reserve01)
    {
        this.reserve01 = reserve01;
    }

    public java.lang.String getReserve02()
    {
        return reserve02;
    }

    public void setReserve02(java.lang.String reserve02)
    {
        this.reserve02 = reserve02;
    }

    public java.lang.Long getTargetindex()
    {
        return targetindex;
    }

    public void setTargetindex(java.lang.Long targetindex)
    {
        this.targetindex = targetindex;
    }

    public java.lang.String getTargetid()
    {
        return targetid;
    }

    public void setTargetid(java.lang.String targetid)
    {
        this.targetid = targetid;
    }

    public java.lang.String getTargetname()
    {
        return targetname;
    }

    public void setTargetname(java.lang.String targetname)
    {
        this.targetname = targetname;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.Integer getTargettype()
    {
        return targettype;
    }

    public void setTargettype(java.lang.Integer targettype)
    {
        this.targettype = targettype;
    }

    public java.lang.String getChangetime()
    {
        return changetime;
    }

    public void setChangetime(java.lang.String changetime)
    {
        this.changetime = changetime;
    }

    public java.lang.String getDescription()
    {
        return description;
    }

    public void setDescription(java.lang.String description)
    {
        this.description = description;
    }

    public java.lang.String getMsgurl()
    {
        return msgurl;
    }

    public void setMsgurl(java.lang.String msgurl)
    {
        this.msgurl = msgurl;
    }

    public java.lang.String getMsgurl1()
    {
        return msgurl1;
    }

    public void setMsgurl1(java.lang.String msgurl1)
    {
        this.msgurl1 = msgurl1;
    }

    public java.lang.String getMsgurl2()
    {
        return msgurl2;
    }

    public void setMsgurl2(java.lang.String msgurl2)
    {
        this.msgurl2 = msgurl2;
    }

    public java.lang.String getMsgurl3()
    {
        return msgurl3;
    }

    public void setMsgurl3(java.lang.String msgurl3)
    {
        this.msgurl3 = msgurl3;
    }

    public java.lang.String getMsgurl4()
    {
        return msgurl4;
    }

    public void setMsgurl4(java.lang.String msgurl4)
    {
        this.msgurl4 = msgurl4;
    }

 

    public void initRelation()
    {
        this.addRelation("targetindex", "TARGETINDEX");
        this.addRelation("targetid", "TARGETID");
        this.addRelation("targetname", "TARGETNAME");
        this.addRelation("status", "STATUS");
        this.addRelation("targettype", "TARGETTYPE");
        this.addRelation("changetime", "CHANGETIME");
        this.addRelation("description", "DESCRIPTION");
        this.addRelation("msgurl", "MSGURL");
        this.addRelation("msgurl1", "MSGURL1");
        this.addRelation("msgurl2", "MSGURL2");
        this.addRelation("msgurl3", "MSGURL3");
        this.addRelation("msgurl4", "MSGURL4");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("reserve01", "RESERVE01");
        this.addRelation("reserve02", "RESERVE02");
    }
}

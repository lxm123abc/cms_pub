package com.zte.cms.picture.dao;

import java.util.List;

import com.zte.cms.picture.dao.ICategoryPictureMapDAO;
import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CategoryPictureMapDAO extends DynamicObjectBaseDao implements ICategoryPictureMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException
    {
        log.debug("insert categoryPictureMap starting...");
        super.insert("insertCategoryPictureMap", categoryPictureMap);
        log.debug("insert categoryPictureMap end");
    }

    public void updateCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException
    {
        log.debug("update categoryPictureMap by pk starting...");
        super.update("updateCategoryPictureMap", categoryPictureMap);
        log.debug("update categoryPictureMap by pk end");
    }

    public void deleteCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException
    {
        log.debug("delete categoryPictureMap by pk starting...");
        super.delete("deleteCategoryPictureMap", categoryPictureMap);
        log.debug("delete categoryPictureMap by pk end");
    }

    public CategoryPictureMap getCategoryPictureMap(CategoryPictureMap categoryPictureMap) throws DAOException
    {
        log.debug("query categoryPictureMap starting...");
        CategoryPictureMap resultObj = (CategoryPictureMap) super.queryForObject("getCategoryPictureMap",
                categoryPictureMap);
        log.debug("query categoryPictureMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CategoryPictureMap> getCategoryPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DAOException
    {
        log.debug("query categoryPictureMap by condition starting...");
        List<CategoryPictureMap> rList = (List<CategoryPictureMap>) super.queryForList(
                "queryCategoryPictureMapListByCond", categoryPictureMap);
        log.debug("query categoryPictureMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query categoryPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCategoryPictureMapListCntByCond", categoryPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CategoryPictureMap> rsList = (List<CategoryPictureMap>) super.pageQuery(
                    "queryCategoryPictureMapListByCond", categoryPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query categoryPictureMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CategoryPictureMap categoryPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCategoryPictureMapListByCond", "queryCategoryPictureMapListCntByCond",
                categoryPictureMap, start, pageSize, puEntity);
    }

    /*********************************海报发布管理*********************/
   
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerycatPicMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query categoryPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCatgrPictureMapSynListCntByCond", categoryPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CategoryPictureMap> rsList = (List<CategoryPictureMap>) super.pageQuery(
                    "queryCatgrPictureMapSynListByCond", categoryPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query categoryPictureMap by condition end");
        return pageInfo;
    }
    
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuerypcatPicTargetMap(CategoryPictureMap categoryPictureMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query categoryPictureMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCatgrPictureTargetMapSynListCntByCond", categoryPictureMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CategoryPictureMap> rsList = (List<CategoryPictureMap>) super.pageQuery(
                    "queryCatgrPictureTargetMapSynListByCond", categoryPictureMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query categoryPictureMap by condition end");
        return pageInfo;
    }   
    
    @SuppressWarnings("unchecked")
    public List<CategoryPictureMap> getCategorynormalPictureMapByCond(CategoryPictureMap categoryPictureMap)
            throws DAOException
    {
        log.debug("query categoryPictureMap by condition starting...");
        List<CategoryPictureMap> rList = (List<CategoryPictureMap>) super.queryForList(
                "getCatgrPictureTargetMapSynListByCond", categoryPictureMap);
        log.debug("query categoryPictureMap by condition end");
        return rList;
    }    
    
    
    
    /*********************************海报发布管理*********************/
}
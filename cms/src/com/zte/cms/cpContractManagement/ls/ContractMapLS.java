package com.zte.cms.cpContractManagement.ls;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.cpContractManagement.model.ContractMap;
import com.zte.cms.cpContractManagement.service.IContractMapService;
import com.zte.ismp.systemconfig.ls.IIsysConfigLS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ucm.util.PermissionsUtil;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import com.zte.umap.common.model.OperInfo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ContractMapLS extends DynamicObjectBaseDS implements IContractMapLS {

    private Log log = SSBBus.getLog(getClass());

    private IContractMapService contractMapServiceImpl;
    public void setContractMapServiceImpl(IContractMapService contractMapServiceImpl) {
        this.contractMapServiceImpl = contractMapServiceImpl;
    }

    /**
     * 查询数据，进行了权限控制，管理员可以查询所有的，CP只能查询自己的数据
     * @param contractMap
     * @param arg1
     * @param arg2
     * @return
     * @throws DAOException
     */

    @Override
    public TableDataInfo pageInfoQuery(ContractMap contractMap, int arg1, int arg2) throws DomainServiceException {

        log.info("ContractMapLS  pageInfoQuery start ");

        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        String operid = loginUser.getOperid();
        String sql = "select cpid from zxdbm_umap.cms_operator_power where operatorid=" + operid;
        DbUtil db = new DbUtil();
        List rtnlist = null;
        TableDataInfo dataInfo = null;
        try {
            rtnlist = db.getQuery(sql);
            //如果操作员绑定了cp传入cp，否则传入cp为"1"
            if (rtnlist.size() > 0 && !"1".equals(operid))
            {
                Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(0);
                contractMap.setCpCode(tmpMap.get("cpid").toString());
                dataInfo = contractMapServiceImpl.pageInfoQuery(contractMap,arg1,arg2);
            }
            else
            {
                //管理员进入无需验证CpCode
                dataInfo = contractMapServiceImpl.pageInfoQuery(contractMap,arg1,arg2);
            }
        } catch (Exception e) {
            log.error("Error ContractMapLS in pageInfoQuery method:", e);
            e.printStackTrace();
        }
        log.info("ContractMapLS  pageInfoQuery end ");
        return dataInfo;
    }


    /**
     * 删除合同数据
     * @param ids
     * @return
     * @throws DAOException
     */
    @Override
    public String deleteConstractByIds(String[] ids) throws DomainServiceException {
        log.info("ContractMapLS  deleteConstractByIds start ");
        String result = null;
        if(ids==null && ids.length == 0){
            return "合同选择不能为空，请选择您想要删除的合同！";
        }
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String userId = loginUser.getOperid();
        if(!PermissionsUtil.isAdministrator(userId)){
            return "您没有删除合同权限，请联系管理员！";
        }
        result = contractMapServiceImpl.deleteConstractByIds(ids);
        log.info("ContractMapLS  deleteConstractByIds end ");
        return result;
    }


    /**
     * 根据id查询合同信息,不能修改合同信息或者合同名不能为空
     * @param id
     * @return
     * @throws IOException
     */
    @Override
    public ContractMap selectContractById(Integer id) throws DomainServiceException {
        log.info("ContractMapLS  selectContractById start ");
        ContractMap contractMap = null;
        if(id ==null || "".equals(id)){
            contractMap.setMessage("无效数据，请刷新页面重新尝试！");
            return contractMap;
        }
         contractMap = contractMapServiceImpl.selectContractById(id);
        log.info("ContractMapLS  selectContractById end ");
        return contractMap;
    }

    /**
     * 根据id修改合同信息
     * @param contractMap
     * @return
     * @throws IOException
     */
    @Override
    public ContractMap updateContractById(ContractMap contractMap,String uploadResFileCtrl) throws DomainServiceException {
        log.info("ContractMapLS  updateContractById start ");
        ContractMap result = null;
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        if (!PermissionsUtil.isAdministrator(operId)) {
            ContractMap errorResult = new ContractMap();
            errorResult.setMessage("仅管理员有权限!");
            return errorResult;
        }

        result = contractMapServiceImpl.updateContractById(contractMap);
        log.info("ContractMapLS  updateContractById end ");
        return result;
    }

    /**
     * 根据id获取对应合同的附件
     * @param id
     * @return
     * @throws DomainServiceException
     */
    @Override
    public List<ContractMap> getAttachListById(Integer id) throws DomainServiceException {
        log.info("ContractMapLS  getAttachListById end ");
        ContractMap contractMap = new ContractMap();
        contractMap.setId(id);
        List<ContractMap> list = null;
        list = contractMapServiceImpl.getAttachListById(contractMap);
        log.info("ContractMapLS  getAttachListById end ");
        return list;
    }

    /**
     * 根据ID删除合同
     * @param attachid
     * @return
     * @throws DomainServiceException
     */
    @Override
    public String deleteContractAttach(String attachid) throws DomainServiceException {
        log.info("ContractMapLS  deleteContractAttach end ");
        String message = null;
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        if (!PermissionsUtil.isAdministrator(operId)) {
            message = "仅管理员有权限!";
            return message;
        }
        message = contractMapServiceImpl.deleteContractAttach(attachid);
        log.info("ContractMapLS  deleteContractAttach end ");

        return message;
    }

    /**
     * 获取公告的模板（合同）
     * @param id
     * @return
     * @throws DomainServiceException
     */
    @Override
    public String getContractTemplate(String id) throws DomainServiceException {
        IIsysConfigLS configLs = (IIsysConfigLS)SSBBus.findDomainService("ismpSysConfigLS");
        String ftpaddress = configLs.getUsysConfigByCfgkey("cms.AnnouncementTemplate.contract");
        return ftpaddress;
    }

    /**
     * 增加合同，根据并获得发送邮件的模板
     * @param contractMap
     * @param description
     * @param uploadResFileCtrl
     * @return
     * @throws DomainServiceException
     */
    @Override
    public String insertContract(ContractMap contractMap,String description,String uploadResFileCtrl) throws DomainServiceException {
        log.info("ContractMapLS  insertContract start ");
        String result = null;
        //获取当前登录用户对象
        OperInfo loginUser = LoginMgt.getOperInfo(GlobalConstants.IPTV_SERVICE_KEY);
        // 操作员编码
        String operId = loginUser.getOperid();
        if (!PermissionsUtil.isAdministrator(operId)) {
            result = "300";
            return result;
        }

        result = contractMapServiceImpl.insertContract(contractMap,description);

        log.info("ContractMapLS  insertContract end ");
        return result;
    }


}

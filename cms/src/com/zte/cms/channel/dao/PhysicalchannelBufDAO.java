
  package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.IPhysicalchannelBufDAO;
import com.zte.cms.channel.model.PhysicalchannelBuf;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class PhysicalchannelBufDAO extends DynamicObjectBaseDao implements IPhysicalchannelBufDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("insert physicalchannelBuf starting...");
    	super.insert( "insertPhysicalchannelBuf", physicalchannelBuf );
    	log.debug("insert physicalchannelBuf end");
    }
     
    public void updatePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("update physicalchannelBuf by pk starting...");
       	super.update( "updatePhysicalchannelBuf", physicalchannelBuf );
       	log.debug("update physicalchannelBuf by pk end");
    }


    public void deletePhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("delete physicalchannelBuf by pk starting...");
       	super.delete( "deletePhysicalchannelBuf", physicalchannelBuf );
       	log.debug("delete physicalchannelBuf by pk end");
    }
    
    public void deletePhysicalchannelBufByChId( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("delete deletePhysicalchannelBufByChId by pk starting...");
       	super.delete( "deletePhysicalchannelBufByChId", physicalchannelBuf );
       	log.debug("delete deletePhysicalchannelBufByChId by pk end");
    }


    public PhysicalchannelBuf getPhysicalchannelBuf( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("query physicalchannelBuf starting...");
       	PhysicalchannelBuf resultObj = (PhysicalchannelBuf)super.queryForObject( "getPhysicalchannelBuf",physicalchannelBuf);
       	log.debug("query physicalchannelBuf end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<PhysicalchannelBuf> getPhysicalchannelBufByCond( PhysicalchannelBuf physicalchannelBuf )throws DAOException
    {
    	log.debug("query physicalchannelBuf by condition starting...");
    	List<PhysicalchannelBuf> rList = (List<PhysicalchannelBuf>)super.queryForList( "queryPhysicalchannelBufListByCond",physicalchannelBuf);
    	log.debug("query physicalchannelBuf by condition end");
    	return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize)throws DAOException
    {
    	log.debug("page query physicalchannelBuf by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryPhysicalchannelBufListCntByCond",  physicalchannelBuf)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<PhysicalchannelBuf> rsList = (List<PhysicalchannelBuf>)super.pageQuery( "queryPhysicalchannelBufListByCond" ,  physicalchannelBuf , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query physicalchannelBuf by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(PhysicalchannelBuf physicalchannelBuf, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryPhysicalchannelBufListByCond", "queryPhysicalchannelBufListCntByCond", physicalchannelBuf, start, pageSize, puEntity);
    }
    
} 
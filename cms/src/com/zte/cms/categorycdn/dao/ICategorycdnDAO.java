package com.zte.cms.categorycdn.dao;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtype;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.model.CategorycdnMap;
import com.zte.cms.service.model.Service;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public interface ICategorycdnDAO
{
     /**
	  * 新增Categorycdn对象 
	  * 
	  * @param categorycdn Categorycdn对象
	  * @throws DAOException dao异常
	  */	
     public void insertCategorycdn( Categorycdn categorycdn )throws DAOException;
     
     /**
	  * 根据主键更新Categorycdn对象
	  * 
	  * @param categorycdn Categorycdn对象
	  * @throws DAOException dao异常
	  */
     public void updateCategorycdn( Categorycdn categorycdn )throws DAOException;


     /**
	  * 根据主键删除Categorycdn对象
	  *
	  * @param categorycdn Categorycdn对象
	  * @throws DAOException dao异常
	  */
     public void deleteCategorycdn( Categorycdn categorycdn )throws DAOException;
     
     
     /**
	  * 根据主键查询Categorycdn对象
	  *
	  * @param categorycdn Categorycdn对象
	  * @return 满足条件的Categorycdn对象
	  * @throws DAOException dao异常
	  */
     public Categorycdn getCategorycdn( Categorycdn categorycdn )throws DAOException;
     
     /**
	  * 根据条件查询Categorycdn对象 
	  *
	  * @param categorycdn Categorycdn对象
	  * @return 满足条件的Categorycdn对象集
	  * @throws DAOException dao异常
	  */
     public List<Categorycdn> getCategorycdnByCond( Categorycdn categorycdn )throws DAOException;

     /**
	  * 根据条件分页查询Categorycdn对象，作为查询条件的参数
	  *
	  * @param categorycdn Categorycdn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize)throws DAOException;
     
     /**
	  * 根据条件分页查询Categorycdn对象，作为查询条件的参数
	  *
	  * @param categorycdn Categorycdn对象 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DAOException dao异常
	  */
     public PageInfo pageInfoQuery(Categorycdn categorycdn, int start, int pageSize,PageUtilEntity puEntity)throws DAOException;
     public List<Categorycdn> listMenuTreeByParentid(String typeid);
     public Categorycdn getCmsProgramtypeById(Long id);
     public Integer queryByCategoryid(Categorycdn categorycdn);
     public Integer queryByCategoryname(Categorycdn categorycdn);
     public Categorycdn getCmsCategory(String categoryid);
     public Integer insertCmsCategory( Categorycdn categorycdn )throws DAOException;
     public List<Categorycdn> queryChildCmsCategory(Long id);
     public Categorycdn queryByParentid(String typeid);
     public Long queryCategoryindexByCategoryname(Categorycdn categorycdn);
     public List<Categorycdn> listMenuTreeByParentidPub(String parentindex);
     public List<Categorycdn> listMenuTreeByParentidPub(String parentindex, String type, String obindex);
     public List<Categorycdn> selectByCategoryindex(Categorycdn categorycdn) throws DAOException;
     
     public Categorycdn getCategoryByCategoryId(Categorycdn categorycdn)throws DAOException;
   //全部展开
     public List<Categorycdn> listMenuTreeByParentidcategoryAll(String typeid);

     public PageInfo getCategoryCntMapQuery(CategorycdnMap categorycdnMap, int start, int pageSize, PageUtilEntity puEntity)
             throws DAOException;
}
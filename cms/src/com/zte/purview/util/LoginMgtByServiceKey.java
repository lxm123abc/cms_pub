package com.zte.purview.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zte.ismp.common.ConfigUtil;
import com.zte.ismp.common.constant.GlobalConstants;
import com.zte.ismp.common.util.DbUtil;
import com.zte.purview.business.service.IOperInformationDS;
import com.zte.purviewext.Manager;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.util.ApplicationGlobalResource;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.ssb.ui.uiloader.MenuConstants;
import com.zte.ssb.ui.uiloader.access.Authorization;
import com.zte.ssb.ui.uiloader.model.DepartmentInfoBean;
import com.zte.ssb.ui.uiloader.model.Menu;
import com.zte.ssb.ui.uiloader.model.UserInfo;
import com.zte.ssb.ui.uiloader.ria.uiloader.ILoginMgt;
import com.zte.ssb.ui.uiloader.ria.uiloader.LoginMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.zxywpub.ZXYWException;
import com.zte.zxywpub.login;

public class LoginMgtByServiceKey implements ILoginMgt
{
    static final String USER_ID = "sessioninfo.userid";
    static final String USER_NAME = "sessioninfo.username";
    static final String LOGIN_USERINFO = "loginuserinfo";
    static final String WEB_LOCAL_LANGUAGE = "web_local_language";
    static final String LOGININFO_ISHTTPSLOGIN = "loginInfo.isHttpsLogin";
    static final String SESSIONINFO_USERID = "sessioninfo.userId";
    static final String SESSIONINFO_CURRENT_ENTERPRISE_NO = "sessioninfo.currentEnterpriseNo";
    static final String SESSIONINFO_CURRENT_SYSTEM_NO = "sessioninfo.currentSystemNo";
    static final String SESSION_MENU_ID = "sessionmenuid";
    static final String SESSION_PAGECODE = "sessionpagecode";
    static final String MENU_URL = "menuUrl";
    static final String MENU_THIRD_ORDER_ID = "menuThirdOrderId";
    static final String MENU_PARENT_ORDER_ID = "menuParentOrderId";
    static final String MENU_ROOT_ORDER_ID = "menuRootOrderId";

    static final String VIND_PASSWORD = "vindpassword";

    ApplicationGlobalResource appresource = ApplicationGlobalResource.getInstance();
    Log log = SSBBus.getLog(LoginMgt.class);

    private IOperInformationDS operInformationDS;

    public void setOperInformationDS(IOperInformationDS operInformationDS)
    {
        this.operInformationDS = operInformationDS;
    }

    /**
     * 获取session中operid
     * 
     * @return
     */
    public String getOperid()
    {
        String operid = "";
        try
        {
            RIAContext context = RIAContext.getCurrentInstance();
            ISession session = context.getSession();
            HttpSession httpSession = session.getHttpSession();

            operid = (String) httpSession.getAttribute("OPERID");
        }
        catch (Exception e)
        {
            log.error("LoginInfo exception :", e);
        }
        return operid;
    }

    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    public UserInfo getUserInfo()
    {

        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        String userName = (String) session.getSessionAttribute(appresource.getValueByKey(USER_NAME));
        UserInfo userInfo = (UserInfo) session.getSessionAttribute(LOGIN_USERINFO);
        if (null == userInfo)
        {
            userInfo = new UserInfo();
        }
        log.debug(userInfo.getUserName());
        return userInfo;
    }

    /**
     * 获取当前操作员的ID与名称
     * 
     * @return Map<String,String>
     * @throws Exception
     */
    public Map<String, String> getOper() throws Exception
    {
        log.debug("RightInfo: getOperId starts");
        OperInfo operInfo = null;
        Map<String, String> map = new HashMap<String, String>();
        String operId = null;
        String operName = null;
        try
        {
            operInfo = com.zte.umap.common.LoginMgt.getOperInfo(GlobalConstants.SYSTEM_SERVICE_KEY);
            operId = operInfo.getOperid();
            operName = operInfo.getUserId();
            map.put("operid", operId);
            map.put("opername", operName);
        }
        catch (Exception e)
        {
            log.error("Error occurred while getting operator ID", e);
            throw e;
        }
        log.debug("RightInfo: getOperId ends");
        return map;
    }

    /**
     * 语言切换
     * 
     * @param locale 要切换到哪种语言信息
     */
    public void languageChange(String locale)
    {
        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        httpSession.setAttribute(WEB_LOCAL_LANGUAGE, locale);
        log.debug(httpSession.getAttribute(WEB_LOCAL_LANGUAGE));
    }

    /**
     * 获取locale信息
     * 
     * @param
     * 
     */
    public String getLocaleInfo()
    {
        RIAContext context = RIAContext.getCurrentInstance();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        log.debug(httpSession.getAttribute(WEB_LOCAL_LANGUAGE));
        return (String) httpSession.getAttribute(WEB_LOCAL_LANGUAGE);
    }

    /**
     * 登录操作
     * 
     * @param userinfo 用户信息
     * @return 表示登录结果的字符串
     */
    public String login(UserInfo userinfo)
    {
        log.debug("begin logining...... userId=[" + userinfo.getUserId() + "]");
        RIAContext context = RIAContext.getCurrentInstance();
        HttpServletRequest request = context.getRequest();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();
        // 每次登录都清空权限数据
        Authorization.clearInitData(httpSession);
        String userId = userinfo.getUserId();
        String password = userinfo.getPassword();
        String emptyStr = "";
        String returnCode = "0000";
        String userName = "";
        String enterpriseNo = "00";
        String systemNo = "SYS10000";
        String serviceKey = appresource.getValueByKey("wsmpGenral.serviceKey");
        if (serviceKey == null || "".equals(serviceKey))
        {
            serviceKey = "riaservice";
        }
        log.debug("serviceKey=" + serviceKey);
        login logsystem = new login();
        boolean bool = false;

        boolean flag = false;

        // 缘由：为了使super用户在操作员管理页面不能查询到vind用户，vind用户仅供现场安装和升级时，工程人员修改系统相关配置使用
        // vind密码配置在app_config.xml文件中
        // 故有以下代码，以保证vind用户能正常登陆系统
        if (GlobalConstants.SYS_USER_VIND.equals(userId))
        {
            flag = true;
            try
            {
                bool = checkVind(password);
            }
            catch (Exception e)
            {
                log.error("", e);
                returnCode = "902"; // 用户不存在或密码有误
            }
            if (bool)
            {
                returnCode = "0000";
            }
            else
            {
                returnCode = "902"; // 用户不存在或密码有误
            }
        }
        else
        {
            try
            {
                String ip = request.getRemoteAddr();
                bool = logsystem.checkOper(userId, password, ip, serviceKey);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            returnCode = new Integer(logsystem.geterrorCode()).toString();
            log.debug("returnCode=" + returnCode);

            if (bool)
            {
                if (!userId.equals("super"))
                {
                    flag = operInformationDS.IsServiceKeyById(userId, serviceKey);
                }
                else
                {
                    flag = true;
                }
            }
        }

        // 登陆成功
        if (bool && flag)
        {

            userinfo.setPassword(emptyStr);
            userinfo.setUserName(userName);
            httpSession.setAttribute(LOGIN_USERINFO, userinfo);
            httpSession.setAttribute(appresource.getValueByKey(USER_ID), userId);
            httpSession.setAttribute(appresource.getValueByKey(SESSIONINFO_USERID), userId);
            httpSession.setAttribute(appresource.getValueByKey(USER_NAME), userName);
            httpSession.setAttribute(appresource.getValueByKey(SESSIONINFO_CURRENT_ENTERPRISE_NO), enterpriseNo);
            httpSession.setAttribute(appresource.getValueByKey(SESSIONINFO_CURRENT_SYSTEM_NO), systemNo);
            // jam设置
            httpSession.setAttribute(appresource.getValueByKey("sessioninfo.userId"), userId);
            httpSession.setAttribute(appresource.getValueByKey("sessioninfo.userName"), userName);
            httpSession.setAttribute(appresource.getValueByKey("sessioninfo.currentEnterpriseNo"), "00");
            httpSession.setAttribute(appresource.getValueByKey("sessioninfo.currentSystemNo"), "SYS10000");
            // jam设置完毕

            // wsmap设置
            Manager manager = new Manager();
            httpSession.setAttribute("SERVICEKEY", serviceKey); // 业务键
            httpSession.setAttribute("OPERID", logsystem.getoperid() + "");// 操作员id
            httpSession.setAttribute("OPERNAME", userName); // 操作员名
            try
            {
                httpSession.setAttribute("PURVIEW", manager.getPurview(logsystem.getoperid() + "", serviceKey));// 权限列表
            }
            catch (ZXYWException e)
            {
                e.printStackTrace();
            }
            Authorization.authoriaztionMenu(httpSession, userId);
            navigateUrl(httpSession);
        }

        return "0";
    }

    /**
     * 登录操作
     * 
     * @param userinfo 用户信息;vaildCode 验证码
     * @return 表示登录结果的字符串
     */
    public String login(UserInfo userinfo, String vaildCode)
    {
        if (vaildCode == null || "".equals(vaildCode.trim()))
        {
            return ILoginMgt.VALIDATE_FAILED_CODE;
        }

        HttpSession httpSession = RIAContext.getCurrentInstance().getSession().getHttpSession();

        String validCodeSession = (String) httpSession.getAttribute("validCode");
        if (validCodeSession == "" || validCodeSession == null)
        {
            // 验证码失效,请重新登录
            return "50000";
        }

        if (vaildCode.equals(validCodeSession))
        {
            return this.login(userinfo);
        }

        return ILoginMgt.VALIDATE_FAILED_CODE;
    }

    /**
     * 登录操作
     * 
     * @param userinfo 用户信息;
     * @return 表示登录结果的字符串
     */
    public String loginCert(UserInfo userinfo)
    {
        return "";
    }

    /**
     * 注销操作
     */
    public void logout()
    {
        RIAContext context = RIAContext.getCurrentInstance();
        HttpServletRequest request = context.getRequest();
        ISession session = context.getSession();
        HttpSession httpSession = session.getHttpSession();

        // 调用wsmp中注销方法
        String userId = (String) httpSession.getAttribute(appresource.getValueByKey("sessioninfo.userId"));
        String ip = request.getRemoteAddr();
        String serviceKey = (String) httpSession.getAttribute("SERVICEKEY");
        try
        {
            login logsystem = new login();
            logsystem.logOff(userId, ip, serviceKey);
        }
        catch (ZXYWException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // 清空权限数据
        Authorization.clearInitData(httpSession);
        httpSession.removeAttribute(LOGIN_USERINFO);
        httpSession.removeAttribute(appresource.getValueByKey(USER_ID));
        httpSession.removeAttribute(appresource.getValueByKey(SESSIONINFO_USERID));
        // gam用户移出
        httpSession.removeAttribute(appresource.getValueByKey("sessioninfo.userId"));
        httpSession.removeAttribute(appresource.getValueByKey("sessioninfo.userName"));
        httpSession.removeAttribute(appresource.getValueByKey("sessioninfo.currentEnterpriseNo"));
        httpSession.removeAttribute(appresource.getValueByKey("sessioninfo.currentSystemNo"));
        httpSession.invalidate();
        // jam remove over
        log.debug("logout success !");
    }

    /**
     * 获取用户的部门信息
     * 
     * @param userinfo 用户信息;
     * @return
     */
    public ArrayList<DepartmentInfoBean> getUserDepartmentInfo(UserInfo userinfo)
    {
        return new ArrayList<DepartmentInfoBean>();
    }

    private void navigateUrl(HttpSession httpSession)
    {
        String menuId = (String) httpSession.getAttribute(SESSION_MENU_ID);
        String pageCode = (String) httpSession.getAttribute(SESSION_PAGECODE);
        Vector vector = (Vector) httpSession.getAttribute(MenuConstants.WEB_KEY_MENU_LIST);
        String menuUrl = "";
        String menuThirdOrderId = "";
        String menuParentOrderId = "";
        String menuRootOrderId = "";
        int fint = 0;
        int sint = 0;
        int tint = 0;
        if (null != vector && vector.size() > 0)
        {
            for (Iterator v = vector.iterator(); v.hasNext();)
            {
                Menu root_menu = (Menu) v.next();
                Vector vectorChild = root_menu.getMenuList();
                for (Iterator v1 = vectorChild.iterator(); v1.hasNext();)
                {
                    Menu sub_menu = (Menu) v1.next();
                    Vector vectorSub = sub_menu.getMenuList();
                    for (Iterator v2 = vectorSub.iterator(); v2.hasNext();)
                    {
                        Menu menu = (Menu) v2.next();
                        if (menu != null && menu.getId().equals(menuId))
                        {
                            menuUrl = menu.getUri();
                            menuThirdOrderId = menu.getOrderId();
                            tint = Integer.parseInt(menuThirdOrderId) - 1;
                            menuThirdOrderId = String.valueOf(tint);

                            Menu parent = menu.getParent();
                            menuParentOrderId = parent.getOrderId();
                            sint = Integer.parseInt(menuParentOrderId) - 1;
                            menuParentOrderId = String.valueOf(sint);

                            Menu rootParent = parent.getParent();
                            menuRootOrderId = rootParent.getOrderId();
                            fint = Integer.parseInt(menuRootOrderId);
                            menuRootOrderId = String.valueOf(fint);
                        }
                    }
                }
            }

            httpSession.setAttribute(MENU_URL, menuUrl);
            httpSession.setAttribute(MENU_THIRD_ORDER_ID, menuThirdOrderId);
            httpSession.setAttribute(MENU_PARENT_ORDER_ID, menuParentOrderId);
            httpSession.setAttribute(MENU_ROOT_ORDER_ID, menuRootOrderId);
        }
    }

    /**
     * 校验vind用户密码
     * 
     * @param password
     * @return
     * @throws Exception
     */
    private boolean checkVind(String password) throws Exception
    {
        boolean bool = false;
        String configPassword = null;
        try
        {
            if (password == null || "".equals(password.trim()))
            {
                throw new Exception("vind, input password is null");
            }
            configPassword = ConfigUtil.getConfigValue(VIND_PASSWORD);
            bool = password.equals(configPassword);
        }
        catch (Exception e)
        {
            throw e;
        }
        return bool;
    }

}

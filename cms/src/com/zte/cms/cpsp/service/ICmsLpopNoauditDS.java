package com.zte.cms.cpsp.service;

import java.util.List;

import com.zte.cms.cpsp.model.CmsLpopNoaudit;
import com.zte.cms.cpsp.model.UcpCmsLpopNoaudit;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsLpopNoauditDS
{
    /**
     * 新增CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public void insertCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 批量更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public void updateCmsLpopNoauditList(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 根据条件更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 根据条件批量更新CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit更新条件
     * @throws DomainServiceException ds异常
     */
    public void updateCmsLpopNoauditListByCond(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 批量删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public void removeCmsLpopNoauditList(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 根据条件删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 根据条件批量删除CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit删除条件
     * @throws DomainServiceException ds异常
     */
    public void removeCmsLpopNoauditListByCond(List<CmsLpopNoaudit> cmsLpopNoauditList) throws DomainServiceException;

    /**
     * 查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return CmsLpopNoaudit对象
     * @throws DomainServiceException ds异常
     */
    public CmsLpopNoaudit getCmsLpopNoaudit(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 根据条件查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return 满足条件的CmsLpopNoaudit对象集
     * @throws DomainServiceException ds异常
     */
    public List<CmsLpopNoaudit> getCmsLpopNoauditByCond(CmsLpopNoaudit cmsLpopNoaudit) throws DomainServiceException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(CmsLpopNoaudit cmsLpopNoaudit, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException;

    /**
     * 根据条件分页查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 根据条件查询CmsLpopNoaudit对象
     * 
     * @param cmsLpopNoaudit CmsLpopNoaudit对象
     * @return 满足条件的CmsLpopNoaudit对象集
     * @throws DomainServiceException ds异常
     */
    public List<UcpCmsLpopNoaudit> getCmsLpopNoauditList(UcpCmsLpopNoaudit ucpCmsLpopNoaudit)
            throws DomainServiceException;

    /**
     * 查询CP信息 以table展示
     */
    public TableDataInfo pageInfoQueryForOPTable(UcpCmsLpopNoaudit ucpcmsLpopNoaudit, int start, int pageSize)
            throws DAOException;
}
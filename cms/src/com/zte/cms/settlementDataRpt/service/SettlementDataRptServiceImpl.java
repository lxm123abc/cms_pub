package com.zte.cms.settlementDataRpt.service;

import com.zte.cms.common.DbUtil;
import com.zte.cms.common.MD5;
import com.zte.cms.notice.util.FtpAttachment;
import com.zte.cms.settlementDataRpt.common.DateUtils;
import com.zte.cms.settlementDataRpt.common.ExcelUtils;
import com.zte.cms.settlementDataRpt.common.SettlementDataRptConstants;
import com.zte.cms.settlementDataRpt.common.UserInfoUtils;
import com.zte.cms.settlementDataRpt.dao.SettlementDataRptMapper;
import com.zte.cms.settlementDataRpt.error.EmunBusinessError;
import com.zte.cms.settlementDataRpt.model.SettlementDataRpt;
import com.zte.cms.settlementDataRpt.model.SettlementDataRptVO;
import com.zte.ismp.common.ConfigUtil;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.web.multipart.MultipartFile;
import com.zte.ucm.util.EmailServiceUtil;
import com.zte.ucm.util.ToEmailUtil;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.*;

/**
 * ProjectName: cms_demo-com.zte.cms_demo.settlementDataRpt.service
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 11:36 2019/5/6
 * @Description:
 */
public class SettlementDataRptServiceImpl extends DynamicObjectBaseDS implements SettlementDataRptService {

    private Log logger = SSBBus.getLog(SettlementDataRptServiceImpl.class);

    private static List<String> projectTypeList = new ArrayList<String>();

    //email地址
    private static String emailInfo = StringUtils.trimToEmpty(ConfigUtil.get("email_smtp_info"));

    //管理员电话
    private static String contractPhone = StringUtils.trimToEmpty(ConfigUtil.get("contract_Phone"));

    private static Integer perjectTypeUpdateLogo = 0;

    private SettlementDataRptMapper settlementDataRptMapper = null;

    public void setDao(SettlementDataRptMapper settlementDataRptMapper) {
        this.settlementDataRptMapper = settlementDataRptMapper;
    }

    private SettlementDataRptVO getVoById(String id) {
        SettlementDataRptVO settlementDataRptVO = new SettlementDataRptVO();
        settlementDataRptVO.setId(id);
        return settlementDataRptVO;
    }

    @Override
    public TableDataInfo pageInfoQuery(SettlementDataRptVO settlementDataRptVO) {
        PageInfo pageInfo = null;
        try {
            pageInfo = settlementDataRptMapper.selectSettlementDataRpt(settlementDataRptVO);
        } catch (Exception e) {
            pageInfo = new PageInfo();
            e.printStackTrace();
        }
        List<SettlementDataRpt> list = (List<SettlementDataRpt>) pageInfo.getResult();
        List<SettlementDataRptVO> voList = new ArrayList<SettlementDataRptVO>();
        for (SettlementDataRpt settlementDataRpt : list) {
            SettlementDataRptVO resultVO = new SettlementDataRptVO();
            try {
                PropertyUtils.copyProperties(resultVO, settlementDataRpt);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            if (resultVO.getRptDt() != null) {
                resultVO.setRptDtStr(DateUtils.convertDataToString_fit(resultVO.getRptDt(), DateUtils.fit_yyyy_MM));
            }
            voList.add(resultVO);
        }
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setData(voList);
        tableDataInfo.setTotalCount((int) pageInfo.getTotalCount());
        return tableDataInfo;
    }

    @Override
    public String importRpt(MultipartFile file) throws Exception {
        //获得文件名
        String originalFilename = file.getOriginalFilename();
        //获取后缀
        String suffix = originalFilename.substring(originalFilename.indexOf("."));

        /**
         *   开始解析 ----------------------------------------
         */
        Workbook work = null;
        if (SettlementDataRptConstants.SUFFIX_XLS.equals(suffix)) {
            work = new HSSFWorkbook(file.getInputStream());
        } else if (SettlementDataRptConstants.SUFFIX_XLSX.equals(suffix)) {
            work = new XSSFWorkbook(file.getInputStream());
        } else {
            return EmunBusinessError.FILE_FORMAT_ERROR.getErrorMsg();
        }

        Sheet sheet = work.getSheetAt(0);
        //    获取Excel表格第二行 的列个数
        int coloumNum = sheet.getRow(1).getPhysicalNumberOfCells();
        //    表示数据量,除了第 一,二,末 行以外的行数
        int lastRowNum = sheet.getLastRowNum() - 2;
        org.apache.poi.ss.usermodel.Row row0 = sheet.getRow(1);
        String[] herds = {SettlementDataRptConstants.SPID_CN, SettlementDataRptConstants.SPNAME_CN, SettlementDataRptConstants.SETTLEMENTAMOUNT_CN,
                SettlementDataRptConstants.TAXRATE_CN, SettlementDataRptConstants.TAXNOTINCLUDED_CN, SettlementDataRptConstants.TAXAMOUNT_CN,
                SettlementDataRptConstants.PROJECTTYPE_CN, SettlementDataRptConstants.RPTDT_CN};
        for (int i = 0; i < coloumNum; i++) {
            //    获取首行单列的名称
            String cellName = row0.getCell(i).getStringCellValue();
            if (cellName != null) {
                //    判断Excel表格的第 i 位和给定的第 i 位是否相同
                if (!cellName.trim().contains(herds[i])) {
                    //    只要不对应,则返回错误信息
                    return "第" + (i + 1) + "列应为" + herds[i];
                }
            } else {
                return EmunBusinessError.FILE_CONTENT_FORMAT_ERROR.getErrorMsg();
            }
        }
        //用于收集错误信息最后返回给前台
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        int j = 0;
        List<SettlementDataRpt> list = new ArrayList<SettlementDataRpt>();
        for (i = 1; i < sheet.getLastRowNum() - 1; i++) {
            SettlementDataRpt settlementDataRpt = new SettlementDataRpt();
            try {
                //    从第二行开始遍历
                org.apache.poi.ss.usermodel.Row row = sheet.getRow(i + 1);
                if (row == null) {
                    return "500";
                }
                if (row.getCell(0) == null) {
                    stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据SP账号不能为空;\n");
                }
                j = 0;
                if (row.getCell(0).getStringCellValue().equals("总计")) {
                    continue;
                }
                if (row.getCell(0).getStringCellValue() != null && "".equals(row.getCell(0).getStringCellValue())) {
                    stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据SP账号不能为空;\n");
                } else {
                    //    判断有无该CP
                    if (!checkCPExist(row.getCell(0).getStringCellValue())) {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据SP账号不存在;\n");
                    }
                }
                //    获取该行的用户名称
                String spId = row.getCell(j++).getStringCellValue();
                settlementDataRpt.setSpId(spId);
                //    获取到该行第二个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String spName = row.getCell(j - 1).getStringCellValue() + "";
                    if (spName != null && !"".equals(spName)) {
                        settlementDataRpt.setSpName(spName);
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  SP公司名称;\n");
                    }
                }
                //    获取到该行第三个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String settlementAmount = row.getCell(j - 1).getStringCellValue() + "";
                    if (settlementAmount != null && !"".equals(settlementAmount)) {
                        settlementDataRpt.setSettlementAmount(new BigDecimal(settlementAmount));
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  结算金额;\n");
                    }
                }
                //    获取到该行第四个参数
                if (row.getCell(j++) != null) {
                    String taxRate = null;
                    try {
                        taxRate = row.getCell(j - 1).getStringCellValue() + "";
                    } catch (Exception e) {
                        taxRate = row.getCell(j - 1).getNumericCellValue() + "";
                    }
                    if (taxRate != null && !"".equals(taxRate)) {
                        settlementDataRpt.setTaxRate(new BigDecimal(taxRate));
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  税率;\n");
                    }
                }
                //    获取到该行第五个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String taxNotIncluded = row.getCell(j - 1).getStringCellValue() + "";
                    if (taxNotIncluded != null && !"".equals(taxNotIncluded)) {
                        settlementDataRpt.setTaxNotIncluded(new BigDecimal(taxNotIncluded));
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  不含税额;\n");
                    }
                }
                //    获取到该行第六个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String taxAmount = row.getCell(j - 1).getStringCellValue() + "";
                    if (taxAmount != null && !"".equals(taxAmount)) {
                        settlementDataRpt.setTaxAmount(new BigDecimal(taxAmount));
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  税额;\n");
                    }
                }
                //    获取到该行第七个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String projectType = row.getCell(j - 1).getStringCellValue() + "";
                    if (projectType != null && !"".equals(projectType)) {
                        settlementDataRpt.setProjectType(projectType);
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  项目;\n");
                    }
                }
                //    获取到该行第八个参数
                if (row.getCell(j++) != null) {
                    row.getCell(j - 1).setCellType(Cell.CELL_TYPE_STRING);
                    String rptDt = row.getCell(j - 1).getStringCellValue() + "";
                    if (rptDt != null && !"".equals(rptDt)) {
                        settlementDataRpt.setRptDt(DateUtils.convert_fit_yyyyMM(rptDt));
                    } else {
                        stringBuffer.append("&nbsp&nbsp&nbsp&nbsp第" + (i + 2) + "行数据缺少  账单日期;\n");
                    }
                }
                String id = MD5.getMD5String(settlementDataRpt.getSpId() + "_" + settlementDataRpt.getProjectType() + "_" + settlementDataRpt.getRptDt());
                settlementDataRpt.setId(id);
                list.add(settlementDataRpt);
            } catch (Exception e) {
                e.printStackTrace();
                stringBuffer.append("数据导入出现异常;\n&nbsp&nbsp&nbsp&nbsp请检查第" + (i + 2) + "行" + herds[j - 1] + "数据格式是否正确;\n");
            }
        }

        if (stringBuffer.length() == 0) {
            setGetLogo(1);
            int lg = 0;
            for (SettlementDataRpt settlementDataRpt : list) {
                int cnt = settlementDataRptMapper.selectCountById(settlementDataRpt);
                if (cnt == 0) {
                    settlementDataRptMapper.insert(settlementDataRpt);
                    lg++;
                    sendEmail(settlementDataRpt.getRptDt(),settlementDataRpt.getSpId());
                } else {
                    settlementDataRptMapper.update(settlementDataRpt);
                }
            }
            stringBuffer.append("&nbsp&nbsp&nbsp&nbsp导入成功,新增:" + lg + "条,修改:" + (list.size() - lg) + "条;");
        }
        return stringBuffer.toString();
    }

    private boolean checkCPExist(String spName) {

        //查询该id用户的对象判断是否为pc
        StringBuffer sb = new StringBuffer();
        sb.append("select * from zxinsys.OPER_INFORMATION where opername='");
        sb.append(spName);
        sb.append("'");
        DbUtil dbutil = new DbUtil();
        List reslist = null;
        try {
            reslist = dbutil.getQuery(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (reslist == null || reslist.size() == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public SettlementDataRptVO exportRpt(SettlementDataRptVO settlementDataRptVo, String userId) {
        try {
            //获取数据
            //TableDataInfo tableDataInfo = settlementDataRptService.pageInfoQuery(settlementDataRptVO, settlementDataRptVO.getStart(), settlementDataRptVO.getPageSize());
            PageInfo pageInfo = settlementDataRptMapper.selectSettlementDataRpt(settlementDataRptVo);

            List<SettlementDataRpt> list = (List<SettlementDataRpt>) pageInfo.getResult();

            List<List<Map<String, Object>>> listMesh = new ArrayList<List<Map<String, Object>>>();
            List<Map<String, Object>> rowMesh = new ArrayList<Map<String, Object>>();

            /**
             * 指定下载名
             */
            String fileName = SettlementDataRptConstants.Excel_RPT_FILE_NAME + "_" + userId + "_导出报表" + SettlementDataRptConstants.SUFFIX_XLSX;

            //第一行合并A1-J1
            Map<String, Object> cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, fileName);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 7);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_CENTER);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第二行第一个合并A2-C2
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出人员：" + userId);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 5);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);

            //第二行第二个合并H2-J2
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出日期：" + DateUtils.convertDataToString_fit(new Date(), DateUtils.fit_a));
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 7);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 1);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第三行合并A3-M3
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "导出记录：" + list.size() + "条");
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 7);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 2);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 2);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            //第四行合并A4-M4
            rowMesh = new ArrayList<Map<String, Object>>();
            cellMesh = new HashMap<String, Object>(6);
            cellMesh.put(SettlementDataRptConstants.EXCEL_PARAM, "");
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTCOL, 0);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDCOL, 14);
            cellMesh.put(SettlementDataRptConstants.EXCEL_STARTROW, 3);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ENDROW, 3);
            cellMesh.put(SettlementDataRptConstants.EXCEL_ALIGN, ExcelUtils.ALIGN_LEFT);
            rowMesh.add(cellMesh);
            listMesh.add(rowMesh);

            String[] lineName = {SettlementDataRptConstants.SPID_CN, SettlementDataRptConstants.SPNAME_CN, SettlementDataRptConstants.SETTLEMENTAMOUNT_CN,
                    SettlementDataRptConstants.TAXRATE_CN, SettlementDataRptConstants.TAXNOTINCLUDED_CN, SettlementDataRptConstants.TAXAMOUNT_CN,
                    SettlementDataRptConstants.PROJECTTYPE_CN, SettlementDataRptConstants.RPTDT_CN};
            String[] lineEnName = {SettlementDataRptConstants.SPID, SettlementDataRptConstants.SPNAME, SettlementDataRptConstants.SETTLEMENTAMOUNT,
                    SettlementDataRptConstants.TAXRATE, SettlementDataRptConstants.TAXNOTINCLUDED, SettlementDataRptConstants.TAXAMOUNT,
                    SettlementDataRptConstants.PROJECTTYPE, SettlementDataRptConstants.RPTDT};

            File file = ExcelUtils.getHSSFWorkbook(list, listMesh, lineName, lineEnName, fileName);;
            if (file == null) {
                return null;
            }
            String path = file.getAbsolutePath();
            Long attachid = (Long) this.getPrimaryKeyGenerator().getPrimarykey("attachid_index");
            FileInputStream stream = new FileInputStream(path);
            String suffix = path.substring(path.lastIndexOf("."));
            String newName = ("excel_"+attachid + "") + suffix;
            FtpAttachment.upload2FTP(newName, stream);
            file.delete();
            settlementDataRptVo.setExportNewName(newName);
            settlementDataRptVo.setExportFileName(fileName);
            return settlementDataRptVo;
        }catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DomainServiceException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> getProjectType() {
        if (perjectTypeUpdateLogo == 1 || projectTypeList.size() == 0) {
            loadProjectType();
        }
        return projectTypeList;
    }

    @Override
    public String insertSettlementDataRpt(SettlementDataRptVO vo) throws Exception {
        if (!checkCPExist(vo.getSpId())) {
            return "CP账号不存在";
        }
        String id = MD5.getMD5String(vo.getSpId() + "_" + vo.getProjectType() + "_" + vo.getRptDt());
        vo.setId(id);
        settlementDataRptMapper.insert(convertVoToRpt(vo));
        sendEmail(vo.getRptDt(), vo.getSpId());
        return "OK";
    }

    @Override
    public String deleteByIds(String[] ids) {
        try {
            for (String id : ids) {
                SettlementDataRptVO settlementDataRptVO = this.getVoById(id);
                settlementDataRptMapper.deleteByPrimaryKey(settlementDataRptVO);
            }
            setGetLogo(1);
            return "删除成功!";
        } catch (Exception e) {
            e.printStackTrace();
            return "失败";
        }
    }

    private void setGetLogo(int i) {
        this.perjectTypeUpdateLogo = i;
    }

    @Override
    public SettlementDataRpt seletcOneById(String id) {
        SettlementDataRptVO voById = this.getVoById(id);
        int Cnt = settlementDataRptMapper.selectCountById(voById);
        SettlementDataRpt settlementDataRpt;
        if (Cnt != 0) {
            settlementDataRpt = settlementDataRptMapper.selectSettlementDataRptById(voById);
        } else {
            settlementDataRpt = new SettlementDataRptVO();
        }
        return settlementDataRpt;
    }

    @Override
    public void updateById(SettlementDataRptVO vo) {
        settlementDataRptMapper.update(vo);
    }

    @Override
    public void updateIsConfirmById(String id) {
        SettlementDataRptVO voById = getVoById(id);
        voById.setIsconfirm("1");
        settlementDataRptMapper.updateIsConfirmById(voById);
    }


    /**
     * 获取项目类型
     *
     * @return
     */
    private void loadProjectType() {
        projectTypeList = settlementDataRptMapper.getProjectType();
        setGetLogo(0);
    }

    private SettlementDataRpt convertVoToRpt(SettlementDataRptVO vo) {
        SettlementDataRpt settlementDataRpt = new SettlementDataRpt();
        try {
            PropertyUtils.copyProperties(settlementDataRpt, vo);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return settlementDataRpt;
    }


    private void sendEmail(Date rptDt, String spId) throws Exception {
        // TODO 发送邮件
        String content = null;
        String title = null;
        String convertRptDt = DateUtils.convertDataToString_fit(rptDt, DateUtils.fit_yyyy_MM);
        String[] split = convertRptDt.split("-");

        content = "各位合作伙伴好，" + split[0] + "年" + split[1] + "月结算数据已上传四川移动电视业务合作伙伴管理平台系统，请登录查看，并请确认结算数据。" +
                "<br />如果对结算数据有疑问请联络移动:聂毅，联系电话:" + contractPhone + "。";
        title = "四川移动电视业务合作伙伴管理平台|结算数据通知";

        //获取该CPCODE配置的Email地址
        String email = UserInfoUtils.getEmailByCpId(spId);

        //发送邮件，在线程中发送邮件，解决一次性发送多封邮件导致的数据库连接中断
        ToEmailUtil toEmailUtil = new ToEmailUtil(email,title,content,emailInfo);
        Thread thread = new Thread(toEmailUtil);
        thread.start();
    }
}

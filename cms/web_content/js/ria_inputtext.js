// @version 3.00.04

ChangeableTextCtrObj._CHANGEABLETEXT = "INPUTTEXT";
ChangeableTextCtrObj._Z_CHANGEABLETEXT = "Z:INPUTTEXT";
ChangeableTextCtrObj.BOOLEANEventListener = false;
ChangeableTextCtrObj.changeableTextArray = [];

function ChangeableTextCtrObj(id) {
	this.id = id;
	this.preClass = "";
	this.postClass = "";
	this.changeType = "input";
	this.value = "";
	this.width = "";
};

ChangeableTextCtrObj.prototype={
	//\u6253\u5f00\u9875\u9762\u65f6\u521d\u59cb\u5316\u521b\u5efadiv\u6807\u7b7e\uff0c\u4e0d\u63d0\u4f9b\u4fee\u6539
	createChangeableText:function() {
		var obj = {
			id:this.id,
			preClass:this.preClass,
			postClass:this.postClass,
			changeType:this.changeType,
			value:this.value,
			width:this.width
		};
		ChangeableTextCtrObj.changeElemType("", obj, "div");
	},
		
	initialize:function() {
		try {
			this.createChangeableText();
		} catch (e)	{
		
		}
	},
	
	//\u8bbe\u7f6evalue\u503c\uff0cChangeableTextCtrObj\u5bf9\u8c61\u8c03\u7528
	setValue:function(value) {
		/*
		if ("select" == this.changeType) {
			this.value = JSON.parse(value);
		} else {
			this.value = value;
		}
		*/
		this.value = value;
		this.initialize();
		return true;
		//ChangeableTextCtrObj.changeElemType("", this, "div");
	},
	
	//\u83b7\u53d6value\u503c\uff0cChangeableTextCtrObj\u5bf9\u8c61\u8c03\u7528
	getValue:function() {
		var _id = "inner_id" + this.id;
		return ChangeableTextCtrObj.getValueObjFromChangeableText(this.id);
	}
};

ChangeableTextCtrObj.initChangeableText = function () {
	var changeableTextTags = document.getElementsByTagName(ChangeableTextCtrObj._CHANGEABLETEXT);
	if(!changeableTextTags || changeableTextTags.length == 0) {
		changeableTextTags = document.getElementsByTagName(ChangeableTextCtrObj._Z_CHANGEABLETEXT);	
	}
	if(!changeableTextTags || changeableTextTags.length == 0)
		return;
	var changeableText={};
	for (var i=0; i<changeableTextTags.length; i++) {
		//\u6839\u636eid\u521b\u5efa\u6807\u7b7e\u5bf9\u8c61
		changeableText = new ChangeableTextCtrObj.getChangeableTextTag(changeableTextTags[i].getAttribute("id"));
		//\u5c06\u6807\u7b7e\u5bf9\u8c61\u4fdd\u5b58\u5230\u5168\u5c40\u53d8\u91cf\u4e2d\uff0c\u53ea\u80fd\u7528\u4e8e\u9759\u6001\u9875\u9762\u4e2d\uff0c\u5bf9\u52a8\u6001\u521b\u5efa\u7684\u6807\u7b7e\u65e0\u6548
		ChangeableTextCtrObj.changeableTextArray[changeableTextTags[i].getAttribute("id")] = changeableText ;
		//\u5c06\u5bf9\u8c61\u751f\u6210\u5b9e\u4f53\u6807\u7b7e
		if (changeableText) {
			changeableText.initialize();
		}
	}
};

//\u901a\u8fc7\u89e3\u6790\u914d\u7f6e\u6807\u7b7e\u6784\u5efa\u5bf9\u8c61\u5e76\u521d\u59cb\u5316\uff0c\u7136\u540e\u5bf9\u8c61\u8c03\u7528createChangeableText()\u65b9\u6cd5\u751f\u6210\u53ef\u89c1\u6807\u7b7e
ChangeableTextCtrObj.getChangeableTextTag = function (id) {
	var tagObj = document.getElementById(id);
	var _changeableText={};
	_changeableText = new ChangeableTextCtrObj(id);

	if (tagObj.getAttribute("preclass")) {	
		_changeableText.preClass = tagObj.getAttribute("preclass");
	} 
	if (tagObj.getAttribute("postclass")) {	
		_changeableText.postClass = tagObj.getAttribute("postclass");
	}	
		
	if (tagObj.getAttribute("type"))	
		_changeableText.changeType = tagObj.getAttribute("type");
	
	var _value = tagObj.getAttribute("value");
	
	if (_value) {
		if ("select" == _changeableText.changeType) {
			try {
				/*
				var $$testVar = '[["Test-CC","Test-DD"],["valueCC","valueDD"],0]';
				_value = "$$$StestVar";
				*/
				var pattern = /^\$+/;
				if (pattern.test(_value)) {
					var cutValue = _value.substr(1);
					var valueStr = eval(cutValue);
					_changeableText.value = JSON.parse(valueStr);
				} else {
					_changeableText.value = JSON.parse(_value);
				}
			} catch (e) {
				//alert("[" + _value + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
				return;
			}	
		} else {
			_changeableText.value = _value;
		}	
	}
	if (tagObj.getAttribute("width"))	
		_changeableText.width = tagObj.getAttribute("width");
	
	return _changeableText;
};

//\u5224\u65ad\u6d4f\u89c8\u5668\u7c7b\u578b
ChangeableTextCtrObj.getAgentType = function () {
	if(navigator.userAgent.indexOf("MSIE")>0) {
		return 0; //IE
	} else if(isFirefox=navigator.userAgent.indexOf("Firefox")>0) {
		return 1; //Firefox
	} else {
		return 2; //other
	}
};

//toElemType\u4e3a\u8981\u6539\u53d8\u6210\u7684\u6807\u7b7e\u7c7b\u578b\uff0c\u8c03\u7528\u6b64\u65b9\u6cd5\u5c06\u914d\u7f6e\u6807\u7b7e\u6539\u53d8\u4e3atoElemType\u578b\u6807\u7b7e
ChangeableTextCtrObj.changeElemType = function (elementID, obj, toElemType) {
	//\u6839\u636etoElemType\u8c03\u7528\u76f8\u5e94\u7684\u5904\u7406\u51fd\u6570
	ChangeableTextCtrObj.changeTypeMap[toElemType](elementID, obj);
	}; 

//\u5411id\u4e3aelementID\u7684\u5143\u7d20\u52a0\u5165\u76d1\u542c\u52a8\u4f5caction\uff0c\u8c03\u7528processFun\u51fd\u6570\u8fdb\u884c\u5bf9\u5143\u7d20\u5904\u7406
ChangeableTextCtrObj.addActionToElem = function (elementID, action, toElemType, obj) {
		if(action == "keydown") {
			EventManager.Add(elementID, action, function (e) { ChangeableTextCtrObj.keyDownAction(e, elementID, obj, toElemType) });
		} else {
			EventManager.Add(elementID, action, function () { ChangeableTextCtrObj.changeElemType(elementID, obj, toElemType) });
		}
};

//\u6309\u952e\u54cd\u5e94\u51fd\u6570
ChangeableTextCtrObj.keyDownAction = function (e, elementID, obj, toElemType) {
	e = e || window.event;
	currKey = e.keyCode || e.which;
	if(currKey == 13) {  
		//\u5982\u679c\u4e3a\u56de\u8f66\uff0c\u5219\u6839\u636etoElemType\u6539\u53d8\u6807\u7b7e\u7c7b\u578b
		//elementID.focus();
		elementID.blur();
		//ChangeableTextCtrObj.changeElemType(elementID, obj, toElemType);
	} 
};

//\u6539\u53d8\u4e3adiv\u6807\u7b7e\u4ee5\u5c55\u793a\u503c
ChangeableTextCtrObj.changeToDiv = function (elementID, obj) {
	//\u751f\u6210Div\u6807\u7b7e\u4e4b\u524d\u83b7\u53d6\u7528\u6237\u6539\u53d8\u540e\u7684\u503c\uff0celementID\u975e\u7a7a\u8868\u793a\u4e0d\u662f\u521d\u59cb\u5316\u65f6\u751f\u6210div\u7684\u5904\u7406
	//var obj = ChangeableTextCtrObj.changeableTextArray[id];
	if (elementID) {
		if(obj.changeType == "select") {
			obj.value[2] = elementID.selectedIndex;
		} else if (obj.changeType == "input") {
			obj.value = elementID.value
		}
		var methodReturnFlag = true;
		//\u5931\u53bb\u7126\u70b9\u5904\u7406\u51fd\u6570\u8c03\u7528\uff0c\u5148\u8c03\u7528map\u4e2d\u7684\u51fd\u6570
		var methodArr = ChangeableTextCtrObj.userMethodMap[obj.id];
		if(methodArr) {
			for ( var i = 0; i < methodArr.length; i++) {
				methodReturnFlag = methodArr[i](obj.value);
				if (methodReturnFlag == false) {
					return;
				}
			}
		}
		//\u518d\u8c03\u7528userMethodsArray\u4e2d\u7684\u51fd\u6570
		for ( var i = 0; i < ChangeableTextCtrObj.userMethodsArray.length; i++) {
			ChangeableTextCtrObj.userMethodsArray[i](obj.value);
			if (methodReturnFlag == false) {
					return;
			}
		}
		
	}
	var _tmpDivID = "changeable" + obj.id;
	var extendValue = JSON.stringify(obj.value);
	//var html = '<div id="' + _tmpDivID + '" valueext=\'' + extendValue +'\' ></div>';		
	var html = '<div id="' + _tmpDivID + '" valueext=\'' + extendValue +'\' extType="' + obj.changeType + '" ></div>';		
		$("#" + obj.id).html(html);
		var _divID = document.getElementById(_tmpDivID);
		if(obj.changeType == "select") {
			var jsValue = obj.value;
			if (jsValue) {
			//\u521d\u59cb\u5316\u6216\u8bbe\u7f6evalue\u624d\u8fdb\u884c\u5408\u6cd5\u6027\u68c0\u9a8c
				if (!elementID) {
					if (jsValue.length != 3) {
						//alert("[" + obj.value + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
						return;
					} 
					var textLength = jsValue[0].length; //value\u7b2c\u4e00\u7ef4\u957f\u5ea6
					var valueLength = jsValue[1].length; //value\u7b2c\u4e8c\u7ef4\u957f\u5ea6
					var indexSelected = jsValue[2]; //\u9009\u4e2d\u7684\u9009\u9879\u4e0b\u6807\uff0c\u4e0b\u6807\u4ece\u96f6\u5f00\u59cb
					var pattern = /^\d+$/;
					//\u5224\u65ad\u7b2c\u4e09\u5217\u662f\u5426\u4e3a\u975e\u8d1f\u6574\u6570
					//\u5224\u65ad\u7b2c\u4e00\u3001\u7b2c\u4e8c\u7ef4\u957f\u5ea6\u662f\u5426\u76f8\u7b49
					//\u5224\u65ad\u9009\u4e2d\u7684\u4e0b\u6807\u662f\u5426\u8d85\u51fa\u7ef4\u6570\u957f\u5ea6
					if (!pattern.test(indexSelected) || textLength != valueLength || indexSelected > textLength - 1) {
						//alert("[" + obj.value + "]" + $res_entry("ria.exception.parasError", "Value is not correct."));
						return;
					}
				}
				if (null === jsValue[0][jsValue[2]]) {
					$("#" + _tmpDivID).html("");
				} else {
					$("#" + _tmpDivID).html("" + jsValue[0][jsValue[2]]);
				}
			} else {
				$("#" + _tmpDivID).html("");
			}	
		} else if (obj.changeType == "input") {
			if (null === obj.value) {
				$("#" + _tmpDivID).html( "");
			} else {
				$("#" + _tmpDivID).html( "" + obj.value);
			}
		} else {
			//alert("[" + obj.changeType + "]" + $res_entry("ria.exception.typeNotSupport", "Type in lable is not suported now."));
			return;
		}
		var _preclass = "";
		if (obj.preClass) {
			_preclass =  "ui-changeableText-pre " + obj.preClass;
		} else {
			_preclass =  "ui-changeableText-pre";
		}
		//\u8bbe\u7f6e\u6837\u5f0f
		_divID.setAttribute("className", _preclass);
		_divID.setAttribute("class", _preclass);
		if (obj.width) {
			try {
				if(isNaN(obj.width)) {
					_divID.style.width = obj.width;
				} else {
					_divID.style.width = obj.width + "px";
				}
			} catch(e) {
					//_divID.style.width = "150px";
			}
		}
		ChangeableTextCtrObj.addActionToElem(_divID, "dblclick", obj.changeType, obj);
};

//\u6539\u53d8\u4e3ainput text
ChangeableTextCtrObj.changeToInput = function (elementID, obj) {
	//var obj = ChangeableTextCtrObj.changeableTextArray[zLableID];
	var _tmpInputID = "input" + obj.id;
	var html = '<input id="' + _tmpInputID + '" type="text"/>';
	$("#" + obj.id).html(html);
	var changedInputID = document.getElementById(_tmpInputID);

	//\u8bbe\u7f6e\u5bbd\u5ea6
	//changedInputID.setAttribute("size", obj.width);
	if (null === obj.value) {
		changedInputID.setAttribute("value", "");
	} else {
		changedInputID.setAttribute("value", obj.value);
	}
	//\u8bbe\u7f6eclass
	var _postClass = "";
		if (obj.postClass) {
			_postClass =  "ui-changeableText-post " + obj.postClass;
		} else {
			_postClass =  "ui-changeableText-post";
		}
		changedInputID.setAttribute("className", _postClass);
		changedInputID.setAttribute("class", _postClass);		
	if (obj.width) {
		try {
			if(isNaN(obj.width)) {
				changedInputID.style.width = obj.width;
			} else {
				changedInputID.style.width = obj.width + "px";
			}
		} catch(e) {
				//_outdiv.style.width = "146px";
		}
	}
	ChangeableTextCtrObj.addActionToElem(changedInputID, "keydown", "div", obj);
	ChangeableTextCtrObj.addActionToElem(changedInputID, "blur", "div", obj);
	changedInputID.focus();
	changedInputID.select();
};

//\u6539\u53d8\u4e3aselect
ChangeableTextCtrObj.changeToSelect = function (elementID, obj) {
	
	var _tmpSelectID = "select" + obj.id;
	var extendValue = JSON.stringify(obj.value);
	var html = '<select id="' + _tmpSelectID + '" eventFlag="0" valueext=\'' + extendValue + '\' ></select>';
	
	var jsValue = obj.value;
	
	$("#" + obj.id).html(html);
	var changedSelectID = document.getElementById(_tmpSelectID);
	//\u8bbe\u7f6eclass		
	var _postClass = "";
	if (obj.postClass) {
		_postClass =  "ui-changeableText-post " + obj.postClass;
	} else {
		_postClass =  "ui-changeableText-post";
	}
	changedSelectID.setAttribute("className", _postClass);
	changedSelectID.setAttribute("class", _postClass);
	if (obj.width) {
		try {
			if(isNaN(obj.width)) {
				changedSelectID.style.width = obj.width;
			} else {
				changedSelectID.style.width = obj.width + "px";
			}
		} catch(e) {
				//changedSelectID.style.width = "146px";
		}
	}
	var dimensionText = jsValue[0]; //value\u5c5e\u6027\u7684\u6570\u7ec4
	var dimensionValue = jsValue[1]; //\u663e\u793a\u7684text\u503c\u7684\u6570\u7ec4
	var indexSelected = jsValue[2]; //\u9009\u4e2d\u7684\u9009\u9879\u4e0b\u6807\uff0c\u4e0b\u6807\u4ece\u96f6\u5f00\u59cb
	var textLength = dimensionText.length;
	for (var i =0; i < textLength; i++) {
		if (null === dimensionText[i]) {
			dimensionText[i] = "";
		}
		changedSelectID.options.add(new Option(dimensionText[i], dimensionValue[i])); 
	}
	changedSelectID.focus();
	changedSelectID.options[indexSelected].selected = true;
	ChangeableTextCtrObj.addActionToElem(changedSelectID, "blur", "div", obj);
	ChangeableTextCtrObj.addActionToElem(changedSelectID, "keydown", "div", obj);
	
};

//\u5916\u90e8\u63a5\u53e3\uff0c\u8bbe\u7f6evalue
ChangeableTextCtrObj.setValueObjFromChangeableText = function (id, elementVal){
	var textObj = ChangeableTextCtrObj.getChangeableTextTag(id);
	if (textObj) {
		/*
		if ("select" == textObj.changeType) {
				textObj.value = JSON.parse(elementVal);
		} else {
			textObj.value = elementVal;
		}
		*/
		textObj.value = elementVal;
		textObj.initialize();
		return true;
	}
	return false;
};

//\u5916\u90e8\u63a5\u53e3\uff0c\u83b7\u53d6\u5f53\u524dvalue\uff0c\u5982\u679c\u7c7b\u578b\u4e3aselect\uff0c\u8fd4\u56deJSON\u5bf9\u8c61\uff0c\u5426\u5219\u8fd4\u56de\u5b57\u7b26\u4e32
ChangeableTextCtrObj.getValueObjFromChangeableText = function (id){
	var _divID = "changeable" + id;
	var _textID = "input" + id;
	var _selectID = "select" + id;
	
	if (document.getElementById(_divID)) {
		//\u9700\u8fd4\u56deJSON\u5bf9\u8c61
		var domObjDiv = document.getElementById(_divID);
		var divType = domObjDiv.getAttribute("extType");
		var valueString = domObjDiv.getAttribute("valueext");
		if (valueString) {
			if ("select" == divType) {
				var jsValue = JSON.parse(valueString);
				return jsValue[1][jsValue[2]];
			} else {
				return JSON.parse(valueString);
			}	
		}
	} else if (document.getElementById(_textID)) {
		return document.getElementById(_textID).value;
	} else if (document.getElementById(_selectID)) {
		var domObjSelect = document.getElementById(_selectID);
		var indexSelected = domObjSelect.selectedIndex;
		var JSONvalue = JSON.parse(domObjSelect.getAttribute("valueext"));
		JSONvalue[2] = indexSelected;
		return JSONvalue;
	} else {
		return null;
	}

};



//\u7528\u4e8e\u5b58\u50a8\u8f6c\u6362\u76ee\u6807\u7c7b\u578b\u7684map\u5bf9\u8c61\uff0ckey\u4e3a\u76ee\u6807\u7c7b\u578b\uff0cvalue\u4e3a\u5904\u7406\u51fd\u6570
ChangeableTextCtrObj.changeTypeMap = {};

//\u5411changeTypeMap\u6dfb\u52a0\u5bf9\u5e94\u5904\u7406\u51fd\u6570
ChangeableTextCtrObj.addChangeType = function (type, processFun) {
	ChangeableTextCtrObj.changeTypeMap[type] = processFun;
};

//\u6dfb\u52a0\u9700\u8981\u8f6c\u6362\u7684\u6807\u7b7e\u7c7b\u578b\u53ca\u5bf9\u5e94\u7684\u5904\u7406\u51fd\u6570\uff0c\u7b2c\u4e00\u4e2a\u53c2\u6570\u4e3a\u8f6c\u6362\u4e3a\u90a3\u79cd\u6807\u7b7e\uff0c\u7b2c\u4e8c\u4e2a\u53c2\u6570\u4e3a\u8f6c\u6362\u51fd\u6570\u540d
ChangeableTextCtrObj.addChangeType ("input", ChangeableTextCtrObj.changeToInput);
ChangeableTextCtrObj.addChangeType ("select", ChangeableTextCtrObj.changeToSelect);
ChangeableTextCtrObj.addChangeType ("div", ChangeableTextCtrObj.changeToDiv);


//\u7528\u6237\u65b9\u6cd5Map\u5bf9\u8c61\uff0c\u6839\u636eid\u5b58\u50a8\u6240\u6709\u65b9\u6cd5
ChangeableTextCtrObj.userMethodMap = {};
//\u7528\u6237\u65b9\u6cd5Map\u5bf9\u8c61\uff0c\u5b58\u50a8\u6240\u6709id\u90fd\u5fc5\u987b\u6267\u884c\u7684\u901a\u7528\u65b9\u6cd5
ChangeableTextCtrObj.userMethodsArray = [];

//\u5411userMethodMap\u6dfb\u52a0\u5bf9\u5e94\u5904\u7406\u51fd\u6570\uff0c\u5982\u679cid\u4e3a\u7a7a\uff0c\u5219\u5411userMethodsArray\u4e2d\u6dfb\u52a0\u65b9\u6cd5
ChangeableTextCtrObj.addUserMethod = function (processFun, id) {
	if (id) {
		if(ChangeableTextCtrObj.userMethodMap[id]) {
			ChangeableTextCtrObj.userMethodMap[id].push(processFun);
		} else {
			ChangeableTextCtrObj.userMethodMap[id] = new Array();
			ChangeableTextCtrObj.userMethodMap[id].push(processFun);	
		}
	} else {
		ChangeableTextCtrObj.userMethodsArray.push(processFun);
	}
		
};


addOnloadEvent(ChangeableTextCtrObj.initChangeableText);

  
   /**
   * 为日历控件赋值,后台传回来一个UTC时间。
   * @author 张宏伟
   * @param {Object} targetid
   * @param value UTC时间
   */
  
  
  function setCalendarValue(id, value)
  {
  	
  	var obj = document.getElementById(id);
	var targetId = obj.getAttribute("target");
	var obj1 = document.getElementById(targetId);

	var uDate = new Date();
	var offset = uDate.getTimezoneOffset();   
    var nd = new Date(value);
    var format = canlendarObj.formate;		
	var yy = "-";
	var mm = "-";
	if(format != null)
	{
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		
		yy = format.substring(y_endindex+1,y_endindex+2);
	
		//
	    var m_index = format.indexOf("m");
		var m_endindex = format.lastIndexOf("m");	
		mm = format.substring(m_endindex+1, m_endindex+2);
	}
	
	var time = nd.getFullYear() + yy + parseInt(nd.getMonth()+1) + mm + nd.getDate();
	obj1.value = time;
  };
 

 
 /**
   * 取日历控件值，转换成UTC时间回传到后台
   * @author 张宏伟
   * @param {Object} targetid
   */   	
  function getCalendarValue(id)
  {
  	var obj = document.getElementById(id);
	var targetId = obj.getAttribute("target");
	var obj1 = document.getElementById(targetId);
    var time = obj1.value;
    
    var format = canlendarObj.formate;		
	var yy = "-";
	var mm = "-";
	if(format != null)
	{
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		
		yy = format.substring(y_endindex+1,y_endindex+2);
	
		//
	    var m_index = format.indexOf("m");
		var m_endindex = format.lastIndexOf("m");	
		mm = format.substring(m_endindex+1, m_endindex+2);
	}
	var YM = time.substring(time.indexOf(yy),time.indexOf(yy)+1);
	var MD = time.substring(time.indexOf(mm),time.indexOf(mm)+1);
	time = time.replace(YM,',');
	time = time.replace(MD,',');
    var tt = time.split(",");
    var utc = Date.UTC(tt[0],parseInt(tt[1]-1),tt[2]);

	return utc;
  };
  
   var canlendarObj = new Object();//控件的模型数组
  
/**
 * 初始化日历控件
 * 
 * @copyright
 * @author 张宏伟
 */
function Calendar_init()
{
    var tagNames ;
	
	if(isIE())
	{
		tagNames = document.getElementsByTagName('Calendar'); 
	}else{
		tagNames = document.getElementsByTagName('z:Calendar'); 
	}

     for(var i = 0; i < tagNames.length; i++)
     {
         var date_formate = tagNames[i].getAttribute('formate'); //获取日期格式
         var mid = 	tagNames[i].getAttribute('id');
         
			if(date_formate != null && date_formate != "")
			{
				 canlendarObj.formate = date_formate; 
			}

		
		
	     var inputTarget = tagNames[i].getAttribute('target');	//获取内存输入框的ID
	     
		 var inputText = tagNames[i].getAttribute('value');//获取默认值
		 
		 if(inputText == null)
		 {
		 	var html='<INPUT type="text" id="'+inputTarget+'" name="'+inputTarget+'"  readonly= true  value="" size="20">&nbsp;';
		 }
	     else
		 {
		 	var html='<INPUT type="text" id="'+inputTarget+'" name="'+inputTarget+'"  readonly= true  value="'+inputText+'" size="20">&nbsp;';
		 }
		 
      	html+='<img border="0" src="../images/calendar.gif" width="16" height="15"  onClick="fPopCalendar(\''+inputTarget+'\',\''+inputTarget+'\',\''+date_formate+'\');return false"> ';
       
     	 tagNames[i].innerHTML=html; 

     }
};


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

var gdCtrl = new Object();
var goSelectTag = new Array();
var gcGray   = "#808080";
var gcToggle = "#FB8664";
var gcEmpty = '';
var gcBG = "#e5e6ec";
var previousObject = null;
var gdCurDate = new Date();
var giYear = gdCurDate.getFullYear();
var giMonth = gdCurDate.getMonth()+1;
var giDay = gdCurDate.getDate();
var curYear = gdCurDate.getFullYear();
var curMonth = gdCurDate.getMonth()+1;
var curDay = gdCurDate.getDate();
curMonth= (curMonth.toString().length==1)?'0'+curMonth.toString():curMonth.toString();
curDay= (curDay.toString().length==1)?'0'+curDay.toString():curDay.toString();
var giHh = gdCurDate.getHours();
var giMm = gdCurDate.getMinutes();
var giSs = gdCurDate.getSeconds();
var giTime = giHh+':'+giMm+':'+giSs;
var Hid = true;
/**
 * 
 * @param {Object} num
 */
function fCheckIsNumber(num)
{
	var i,j,strTemp;
	strTemp="0123456789";
	if (num.length==0)
	{
		return false;
	}
	for (i=0;i<num.length;i++)
	{
		j=strTemp.indexOf(num.charAt(i)); 
		if (j==-1)
		{
			return false;
		}
	}
	return true;
};
/**
 * 
 * @param {Object} str
 */
function LTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1)
	{
		 var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
		{
			j++;
		}
		s = s.substring(j, i);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function RTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	 if (whitespace.indexOf(s.charAt(s.length-1)) != -1)
	{
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
		{
			 i--;
		}
		s = s.substring(0, i+1);
	}
	return s;
};
/**
 * 
 * @param {Object} str
 */
function Trim(str) 
{
	return RTrim(LTrim(str));
};
/**
 * 
 */
function fCheckDateTime()
{
	var inputValue=document.all.dateStart.value;
	inputValue=Trim(inputValue);
	var len=inputValue.length;
	if (len!=10)
	{
		if(len!=16)
		{
			if (len!=19)
			{
				alert('输入日期不合法,\n请按yyyy-MM-dd格式输入!');
				return;
			}
		}
	}
	var iDate= inputValue.substring(0,10);
	var iTime= inputValue.substring(10,len);
	if (fCheckDate(iDate)== false)
	{
		alert('输入日期不合法,\n请按yyyy-MM-dd格式输入!');
		return;
	}
	if (len!=10)
	{
		if (fCheckTime(iTime)== false)
		{
			alert('输入日期不合法,\n请按yyyy-MM-dd hh:mm:ss格式输入!');
			return;
		}
	}
};
/**
 * 
 * @param {Object} inputdate
 */
function fCheckDate(inputdate)
{
	inputdate=inputdate.split('-');
	var year=inputdate[0];
	var month=inputdate[1];
	var day=inputdate[2];
	var Date1 = new Date(year,month-1,day); 
	if (Date1.getMonth()+1!=month||Date1.getDate()!=day||Date1.getFullYear()!=year||year.length!=4)
	{
		return false;
	}
	else
	{
		if (month.indexOf('0')==0) 
		{
			month=month.substr(1,month.length); 
		}
		if (day.indexOf('0')==0) 
		{
			day=day.substr(1,day.length); 
		}
		if ((month==4 || month==6 || month==9 || month==11) && (day>30))
		{
			return false; 
		}
		if (month==2) 
		{
			if (LeapYear(year)) 
			{
				if (day>29 || day<1)
				{
					return false;
				}
			}
			else
			{
				if (day>28 || day<1)
				{
					return false; 
				}
			}
		}
	}
	return true;
};
function fCheckTime(inputTime)
{
	inputTime=inputTime.split(':')
	if(inputTime.length!=2)
	{
		if(inputTime.length!=3)
		{
			return false;
		}
	}
	var hh=Trim(inputTime[0])
	if (isNaN(hh))
	{
		return false;
	}
	if (hh.indexOf('0')==0) 
	{
		hh=hh.substr(1,hh.length); 
	}
	if ((hh<0) || (hh>23)) 
	{
		return false;
	}
	var mi=Trim(inputTime[1]);
	if (isNaN(mi))
	{
		return false;
	}
	if (mi.indexOf('0')==0) 
	{
		mi=mi.substr(1,mi.length); 
	}
	if((mi<0) || (mi>59)) 
	{
		return false;
	}
	if(inputTime.length==3)
	{
		var ss=Trim(inputTime[2]);
		if (isNaN(ss))
		{
			return false;
		}
		if (ss.indexOf('0')==0) 
		{
			ss=ss.substr(1,ss.length); 
		}
		if ((ss<0) || (ss>59)) 
		{
			return false;
		}
	}
	return true
};
function LeapYear(intYear)
{
	if (intYear % 100 == 0)
	{
		if (intYear % 400 == 0)
		{
			return true;
		}
	}
	else
	{
		if ((intYear % 4) == 0) 
		{
			return true; 
		}
	}
	return false;
};
function fSetDateTime(iYear,iMonth,iDay)
{
	giHh=document.all.dpHour.value;
	giMm=document.all.dpMinute.value;
	giSs=document.all.dpSecon.value;
	giHh=(giHh.length==1)?'0'+giHh:giHh;
	giMm=(giMm.length==1)?'0'+giMm:giMm;
	giSs=(giSs.length==1)?'0'+giSs:giSs;
	giTime=giHh+':'+giMm+':'+giSs;
	VicPopCal.style.visibility = "hidden";
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{
		iMonth=(iMonth.toString().length==1)?'0'+iMonth.toString():iMonth;
		iDay=(iDay.toString().length==1)?'0'+iDay.toString():iDay;
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
		else
		{
			gdCtrl.innerText = iYear+"-"+iMonth+"-"+iDay + " " + giTime;
		}
	}
//	for (i in goSelectTag)
//	{
//		goSelectTag[i].style.visibility = "visible";
//	}
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function getNowDate()
{
	var nn = new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var monstr1;
	var datestr1
	if(mon1<10)
	{
		monstr1="0"+mon1;
	}
	else
	{
		monstr1=""+mon1;
	}
	if(date1<10)
	{
		datestr1="0"+date1;
	}
	else
	{
		datestr1=""+date1;
	}
	year1+"-"+monstr1+"-"+datestr1;
};
function getlastweekDate()
{
	var nn=new Date();
	year1=nn.getYear();
	mon1=nn.getMonth()+1;
	date1=nn.getDate();
	var mm=new Date(year1,mon1-1,date1);
	var tmp1=new Date(2000,1,1);
	var tmp2=new Date(2000,1,15);
	var ne=tmp2-tmp1;
	var mm2=new Date();
	mm2.setTime(mm.getTime()-ne);
	year2=mm2.getYear();
	mon2=mm2.getMonth()+1;
	date2=mm2.getDate();
	if(mon2<10)
	{
		monstr2="0"+mon2;
	}
	else
	{
		monstr2=""+mon2;
	}
	if(date2<10) 
	{
		datestr2="0"+date2;
	}
	else
	{
		datestr2=""+date2;
	}
	return year2+"-"+monstr2+"-"+datestr2;
};
function fSetDate(iYear, iMonth, iDay)
{
	
	var VicPopCal = document.getElementById("VicPopCal");
	//
	var format = canlendarObj.formate;
	var mm = "-";	
	var yy = "-";
	if(format != null)
	{
		var y_index = format.indexOf("y");
		var y_endindex = format.lastIndexOf("y");
		
		yy = format.substring(y_endindex+1,y_endindex+2);
	
		//
	    var m_index = format.indexOf("m");
		var m_endindex = format.lastIndexOf("m");	
		mm = format.substring(m_endindex+1, m_endindex+2);
	}

	//

	VicPopCal.style.visibility = "hidden";
	if ((iYear == 0) && (iMonth == 0) && (iDay == 0))
	{
		gdCtrl.value = "";
		var curDate = new Date();
		giYear = curDate.getFullYear();
		giMonth = curDate.getMonth()+1;
		giDay = curDate.getDate();
	}
	else
	{
		iMonth=(iMonth.toString().length==1)?'0'+iMonth.toString():iMonth;
		iDay=(iDay.toString().length==1)?'0'+iDay.toString():iDay;
		
		//处理日期格式 
	
		if(gdCtrl.tagName == "INPUT")
		{
			gdCtrl.value = iYear+yy+iMonth+mm+iDay;
		}
		else
		{
			gdCtrl.innerText = iYear+yy+iMonth+mm+iDay;
		}
	}
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	goSelectTag.length = 0;
	window.returnValue=gdCtrl.value;
};
function HiddenDiv()
{
	var i;
	document.getElementById('VicPopCal').style.visibility = "hidden";
	for (i in goSelectTag)
	{
	//	goSelectTag[i].style.visibility = "visible";
	}
	goSelectTag.length = 0;
};

/**
 * 获得选择值
 * @param {Object} aCell
 */
function fSetSelected(aCell)
{
	
	Hid = true;
	var iOffset = 0;
	giYear = parseInt(document.getElementById("tbSelYear").value);
	giMonth = parseInt(document.getElementById("tbSelMonth").value);
	var iYear = giYear;
	var iMonth = giMonth;
	aCell.style.bgColor = gcBG;
    
	var nodeobj="";
    
	//edit by chris on 2007-10-09
    if(!isIE()){
	
	  nodeobj=aCell.childNodes;

		var iDay = parseInt(nodeobj[0].textContent);//innerText
		if (nodeobj[0].color==gcGray)
		{
			iOffset = (nodeobj[0].getAttribute("Victor")<10)?-1:1;//zhanghongwei 2007-10-11
		}
		iMonth += iOffset;
		if (iMonth<1)
		{
			iYear--;
			iMonth = 12;
		}
		else if (iMonth>12)
		{
			iYear++;
			iMonth = 1;
		}

	}
	else{
      nodeobj=aCell.children["cellText"];
      // alert(nodeobj.Victor);
		with (nodeobj)
		{
	
			var iDay = parseInt(innerText);
			if (color==gcGray)
			{
				iOffset = (Victor<10)?-1:1;				
			}
			iMonth += iOffset;
			if (iMonth<1)
			{
				iYear--;
				iMonth = 12;
			}
			else if (iMonth>12)
			{
				iYear++;
				iMonth = 1;
			}
		}
	}


	fSetDate(iYear, iMonth, iDay);
};
function Point(iX, iY)
{
	this.x = iX;
	this.y = iY;
};
function fBuildCal(iYear, iMonth)
{
	var aMonth=new Array();
	for(i=1;i<7;i++)
	{
		aMonth[i]=new Array(i);
	}
	var dCalDate=new Date(iYear, iMonth-1, 1);
	var iDayOfFirst=dCalDate.getDay();
	var iDaysInMonth=new Date(iYear, iMonth, 0).getDate();
	var iOffsetLast=new Date(iYear, iMonth-1, 0).getDate()-iDayOfFirst+1;
	var iDate = 1;
	var iNext = 1;
	for (d = 0; d < 7; d++)
	{
		aMonth[1][d] = (d<iDayOfFirst)?-(iOffsetLast+d):iDate++;
	}
	for (w = 2; w < 7; w++)
	{
		for (d = 0; d < 7; d++)
		{
			aMonth[w][d] = (iDate<=iDaysInMonth)?iDate++:-(iNext++);
		}
	}
	return aMonth;
};
function fDrawCal(iYear, iMonth, iCellHeight, sDateTextSize)
{
	var divHtml = '';
	var WeekDay = new Array("日","一","二","三","四","五","六");
	var styleTD = " bgcolor='"+gcBG+"' bordercolor='"+gcBG+"' valign='middle' align='center' height='"+iCellHeight+"' style='font:bold arial "+sDateTextSize+";"; 
	var hmsStyleTD = " bgcolor='white' bordercolor='"+gcBG+"' valign='middle' align='center' height='"+iCellHeight+"' style='font:bold arial "+sDateTextSize+";"; 
	var txtHmsStyle="style=WIDTH:15px;COLOR:black;BORDER-TOP-STYLE:none;BORDER-RIGHT-STYLE:none;BORDER-LEFT-STYLE:none;HEIGHT:100%;BACKGROUND-COLOR:white;BORDER-BOTTOM-STYLE:none;background-color:Transparent;"; 
	divHtml = divHtml + "<tr>";
	for(i=0; i<7; i++)
	{
		divHtml = divHtml + "<td "+styleTD+"color:#990099' >"+ WeekDay[i] + "</td>";
	}
	divHtml = divHtml + "<tr>";
	for (w = 0; w < 6; w++)
	{
		divHtml = divHtml +  "<tr>";
		for (d = 0; d < 7; d++)
		{
			divHtml = divHtml + "<td id=calCell "+styleTD+"cursor:pointer;' onMouseOver='this.bgColor=gcToggle' onMouseOut='this.bgColor=gcBG' onclick='fSetSelected(this)'>";
			divHtml = divHtml + "<font id=cellText name=cellText Victor='Liming Weng'> </font>";
			divHtml = divHtml + "</td>";
		}
	}
	return divHtml;
};
var onfocusobj =null;
function selectHour(obj)
{
	onfocusobj =obj;
	obj.select();
};
function addTime()
{
	if(onfocusobj==undefined)
	{
		 onfocusobj =document.all('dpHour');
	}
	intTe=parseInt(onfocusobj.value);
	if(onfocusobj.id == 'dpHour') 
	{
		 if(intTe==23) 
		{
			intTe=0;   
		}
		else
		{
			intTe = intTe +1;   
		}
	}
	else
	{
		if(intTe==59)
		{
			intTe=0;
		}
		else
		{
			intTe = intTe +1;
		}
	}
	onfocusobj.value =intTe ;
};
function jianTime()
{
	 if(onfocusobj==null)
	{
		onfocusobj =document.all('dpHour');   
	}
	intTe=parseInt(onfocusobj.value);  
	 if(intTe>1)
	{
		 intTe =intTe- 1;
	}
	else
	{
		 if(onfocusobj.id == 'dpHour')
		{
			intTe = 23;
		}
		else
		{
			intTe = 59; 
		}
	}
	onfocusobj.value = intTe;
};
function NotHid()
{
	Hid = false;
};
function fUpdateCal(iYear, iMonth)
{
	Hid = false
	myMonth = fBuildCal(iYear, iMonth);
	 var i = 0;
	 var nn = new Date();
	 var cYear = nn.getFullYear();	
	
	 var cDay = nn.getDate();
	 var cMonth = nn.getMonth()+1;
	 var Marked = false;
	 var Started = false;
	
	for (w = 0; w < 6; w++)
	{
		for (d = 0; d < 7; d++)
		{
		
		
			if(isIE()){
				cellText[(7*w)+d].Victor = i++;
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].color = gcGray;
					//alert(cellText[(7*w)+d]);
					
					cellText[(7*w)+d].innerText = -myMonth[w+1][d];
					
				}
				else
				{
					cellText[(7*w)+d].color = ((d==0)||(d==6))?"blue":"black";
					cellText[(7*w)+d].innerText = myMonth[w+1][d];
				}
				if (cellText[(7*w)+d].innerText == 1) 
				{
					Started = true;
				}
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if (cDay != giDay && parseInt(giDay) == cellText[(7*w)+d].innerText && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
					Marked = true; color = "red"; 
					cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: buttonface'>" + cellText[(7*w)+d].innerText + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{
					color = "red"; cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: yellow'>" + cellText[(7*w)+d].innerText + "</div>";
				}
			
			}
			else{  //edit by chris on 2007-10-09,on firefox
				
				cellText=document.getElementsByName("cellText"); 
				cellText[(7*w)+d].setAttribute("Victor",i++);
				
				if (myMonth[w+1][d]<0) 
				{
					cellText[(7*w)+d].color = gcGray;
					

					cellText[(7*w)+d].textContent = -myMonth[w+1][d]; 					
					
				}
				else
				{
					cellText[(7*w)+d].color = ((d==0)||(d==6))?"blue":"black";
					cellText[(7*w)+d].textContent = myMonth[w+1][d];
				}
				if (cellText[(7*w)+d].textContent == 1) 
				{
					Started = true;
				}
				
				giDay=(giDay.toString().indexOf('0')==0)?giDay.toString().substring(1,2):giDay;
				giMonth=(giMonth.toString().indexOf('0')==0)?giMonth.toString().substring(1,2):giMonth;
				
				if (cDay != giDay && parseInt(giDay) == cellText[(7*w)+d].textContent && parseInt(giMonth) == iMonth &&  giYear == iYear && !Marked && Started) 
				{
				    Marked = true; color = "red"; 
				    cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: buttonface'>" + cellText[(7*w)+d].textContent + "</div>";
				}
				else if (cYear ==iYear  && parseInt(cMonth) ==parseInt(iMonth)  && myMonth[w+1][d] == parseInt(cDay)  && Started) 
				{ 
					color = "red"; cellText[(7*w)+d].innerHTML = "<div style='WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: yellow'>" + cellText[(7*w)+d].textContent + "</div>";
				}
			}
		}
	}
};
function fSetYearMon(iYear, iMon)
{
	//alert(document.all.tbSelMonth.length);
	if(isIE())	document.all.tbSelMonth.options[iMon-1].selected = true;
	else  document.getElementById("tbSelMonth").options[iMon-1].selected = true;
	
	for (i = 0; i < document.all.tbSelYear.length; i++)
	{
		if (document.all.tbSelYear.options[i].value == iYear)
		{
			document.all.tbSelYear.options[i].selected = true;
		}
	}
	fUpdateCal(iYear, iMon);
};
function fPrevMonth()
{
	Hid = false;
	
	if(isIE())
	{
		var iMon = document.all.tbSelMonth.value;
		var iYear = document.all.tbSelYear.value;
		
	}	
	else{
		var iMon =  document.getElementById("tbSelMonth").value;
		var iYear = document.getElementById("tbSelYear").value;
		
	} 
	
	
	if (--iMon<1) 
	{
			iMon = 12;
			iYear--;
	}
	fSetYearMon(iYear, iMon);
};
function fNextMonth()
{
	Hid = false;
	
	var iMon =  document.getElementById("tbSelMonth").value;//zhanghongwei
    var iYear = document.getElementById("tbSelYear").value;//zhanghongwei
	if (++iMon>12) 
	{
			iMon = 1;
			iYear++;
	}
	fSetYearMon(iYear, iMon);
};
function fToggleTags()
{
	
	with (document.getElementsByTagName("select"))//
	{
		    var temp=null;
			for (i=0; i<length; i++)
		{
				temp=item(i).getAttribute("Victor");//edit by chris on 2007-10-09

					if ((temp!="Won")&&fTagInBound(item(i)))
			{
							item(i).style.visibility = "hidden";
							goSelectTag[goSelectTag.length] = item(i);
			}
		}
	}
};
function fTagInBound(aTag)
{
	
	with (VicPopCal.style)
	{
			var l = parseInt(left);
			var t = parseInt(top);
			var r = l+parseInt(width);
			var b = t+parseInt(height);
			var ptLT = fGetXY(aTag);
			return !((ptLT.x>r)||(ptLT.x+aTag.offsetWidth<l)||(ptLT.y>b)||(ptLT.y+aTag.offsetHeight<t));
	}
};
function fGetXY(aTag)
{

	var oTmp = aTag;
	
	var pt = new Point(0,0);
	do 
	{
			pt.x += oTmp.offsetLeft;
			pt.y += oTmp.offsetTop;
			oTmp = oTmp.offsetParent;
	}
	while(oTmp.tagName!="BODY");
	
	return pt;
};
divHtml = '';
function HidChange(boVis)
{
	Hid = boVis;
};
function fPopCalendar(popCtrl, dateCtrl, formate)
{
	popCtrl =document.getElementById(popCtrl);
	//alert(popCtrl);

	var strDate = null;
		
	strDate=Trim(strDate)
	if(strDate.length>10)
	{
		giYear = strDate.substring(0,4);
		giMonth = strDate.substring(5,7);
		giDay= strDate.substring(8,10);
		var iTime=strDate.substring(10,strDate.Length) ;
		iTime=Trim(iTime).split(':');
		giHh=iTime[0];
		giMm=iTime[1];
		giSs=iTime[2];
	}
	else
	{
		var curDate = new Date();
		giHh = curDate.getHours();
		giMm = curDate.getMinutes();
		giSs = curDate.getSeconds();
	}
	var c = document.getElementById('VicPopCal');
	if(c == null)
	{
		var divHtml = '';
		//var divCal = document.createElement("<Div ALIGN=CENTER id='VicPopCal' onmouseover='HidChange(false)' onmouseout='HidChange(true)' style='OVERFLOW:visibile;POSITION:absolute;VISIBILITY:hidden;border:1px ridge;z-index:100;'></Div>");
		//document.body.insertBefore(divCal);

		var divCal = document.createElement("div");
		divCal.id="VicPopCal";

		divCal.style.cssText="ALIGN:center;POSITION:absolute;VISIBILITY:hidden;border:1px ridge;z-index:100;";
		document.body.insertBefore(divCal,null);

		//divCal.style.align="center";
		//divCal.style.overflow="visibile";
		//divCal.style.position="absolute";
		//divCal.style.visibility="hidden";
		//divCal.style.border="1px";
		//divCal.style.z-index="100";

		//divCal.style="ALIGN:center;OVERFLOW:visibile;POSITION:absolute;VISIBILITY:hidden;border:1px ridge;z-index:100;";
		
		//divCal.onmouseover=HidChange;
		//divCal.onmouseout=HidChange;

		divHtml = divHtml + "<table border='0' width=100% bgcolor='#cccccc'>";
		divHtml = divHtml + "<TR>";
		divHtml = divHtml + "<td valign='middle' align='center'><input type='button' css='button' name='PrevMonth' value='<' style='height:20;width:10%;FONT:bold' onClick='fPrevMonth()'>";
		divHtml = divHtml + "&nbsp;<SELECT id='tbSelYear' name='tbSelYear'onChange='fUpdateCal(tbSelYear.value, tbSelMonth.value)' Victor='Won' style='height:20;width:36%;'>";
		for(i=1900;i<=2050;i++)
		{
				divHtml = divHtml + "<OPTION value='"+i+"'>"+i+"年</OPTION>";
		}
		divHtml = divHtml + "</SELECT>";
		divHtml = divHtml + "&nbsp;<select id='tbSelMonth' name='tbSelMonth' onChange='fUpdateCal(tbSelYear.value, tbSelMonth.value)' Victor='Won' style='height:20;width:33%;'>";
		for (i=0; i<12; i++)
		{
				divHtml = divHtml + "<option value='"+(i+1)+"'>"+gMonths[i]+"</option>";
		}
		divHtml = divHtml + "</SELECT>";
		divHtml = divHtml + "&nbsp;<input type='button' css='button' name='PrevMonth' value='>' style='height:20;width:10%;FONT:bold' onclick='fNextMonth()'>";
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</TR><TR>";
		divHtml = divHtml + "<td align='center'>";
		divHtml = divHtml + "<DIV style='background-color:#3366CC'><table width='100%' border='0'>";
		divHtml = divHtml + fDrawCal(giYear, giMonth, 20, '12');
		divHtml = divHtml + "</table></DIV>";
		divHtml = divHtml + "</td>";
		divHtml = divHtml + "</TR><TR><TD align='center'>";
		divHtml = divHtml + "<TABLE width='100%' bgcolor='#cccccc'><TR><TD align='center'>";
		divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(0,0,0)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>清空</B>";
		divHtml = divHtml + "     ";
		divHtml = divHtml + "<B style='cursor:pointer;FONT:bold' onclick='fSetDate(curYear,curMonth,curDay)' onMouseOver='this.style.color=gcToggle' onMouseOut='this.style.color=0'>今天:"+curYear+"-"+curMonth+"-"+curDay+"</B>";
		divHtml = divHtml + "</td></tr></table>";
		divHtml = divHtml + "</TD></TR>";
		divHtml = divHtml + "</TABLE>";
		document.getElementById("VicPopCal").innerHTML = divHtml;
	}
	if (popCtrl == previousObject)
	{
		var VicPopCal = document.getElementById('VicPopCal');
			if (VicPopCal.style.visibility == "visible")
		{
					HiddenDiv();
					return true;
		}
	}
	
	previousObject = popCtrl;
	
	gdCtrl = document.getElementById(dateCtrl);
	fInitialDate(strDate);
	//alert("ok1");
	fSetYearMon(giYear, giMonth); 
	//alert("ok2");

	var point = fGetXY(popCtrl);
	
	var VicPopCal = document.getElementById('VicPopCal');
	
	with (VicPopCal.style) 
	{
			left = point.x+popCtrl.offsetWidth-190;
			top  = point.y+popCtrl.offsetHeight;
			width = VicPopCal.offsetWidth;
			width = 190;
			//height = VicPopCal.offsetHeight;
		//	fToggleTags(point); 	//comment by chris on 2007-10-09,这个函数什么功能，没太明白
			visibility = 'visible';
	}
};
function fSetHhMmSs()
{
	document.all.dpHour.value=giHh;
	document.all.dpMinute.value=giMm;
	if (giSs!=null && giSs!='')
	{
		document.all.dpSecon.value=giSs;
	}
	else
	{
		document.all.dpSecon.value='0';
	}
};
function fInitialDate(strDate)
{
	if( strDate == null || strDate.length != 10 )
	{
		return false;
	}
	var sYear  = strDate.substring(0,4);
	var sMonth = strDate.substring(5,7);
	var sDay   = strDate.substring(8,10);
	if( sMonth.charAt(0) == '0' ) { sMonth = sMonth.substring(1,2); }
	if( sDay.charAt(0)   == '0' ) { sDay   = sDay.substring(1,2);   }
	var nYear  = parseInt(sYear );
	var nMonth = parseInt(sMonth);
	var nDay   = parseInt(sDay  );
	if ( isNaN(nYear ) )	return false;
	if ( isNaN(nMonth) )	return false;
	if ( isNaN(nDay  ) )	return false;
	var arrMon = new Array(12);
	arrMon[ 0] = 31;	arrMon[ 1] = nYear % 4 == 0 ? 29:28;
	arrMon[ 2] = 31;	arrMon[ 3] = 30;
	arrMon[ 4] = 31;	arrMon[ 5] = 30;
	arrMon[ 6] = 31;	arrMon[ 7] = 31;
	arrMon[ 8] = 30;	arrMon[ 9] = 31;
	arrMon[10] = 30;	arrMon[11] = 31;
	if ( nYear  < 1900 || nYear > 2050 )	return false;
	if ( nMonth < 1 || nMonth > 12 )				return false;
	if ( nDay < 1 || nDay > arrMon[nMonth - 1] )	return false;
	giYear  = nYear;
	giMonth = nMonth;
	giDay   = nDay;
	return true;
};
var gMonths = new Array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");
function maskN()
{
	var masKey = 78;
	if(event.keyCode==masKey&&event.ctrlKey)
	{
		 event.returnValue=0;
		 window.event.returnValue=0;
	}
};
function isIE()
{
	browser_name = navigator.appName;
	if (browser_name == "Microsoft Internet Explorer") 
	{
			return true;
	}
	return false;
};
function isIE2()
{
	if(document.all)
	{
			return true;
	}
	return false;
};
function isNC()
{
	browser_name = navigator.appName;
	if(browser_name == "Netscape")
	{
			return true;
	}
	return false;
};
function isNC2()
{
	if(document.layers)
	{
			return true;
	}
	return false;
};
function version()
{
	 return parseFloat(navigator.appVersion);
};
var LastTabObjectName="";
var FirstTabObjectName="";
function handleEnter()
{
};
function myKeyEvent(e)
{
	var hotkey=13;
	var rightKey = 39;
	var leftKey = 41;
	if (isNC())
	{
			if(e.which==hotkey)
		{
					e.which=9;
					return true;
		}
	}
	else if (isIE())
	{
		if (event.keyCode==hotkey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						if(event.srcElement.type!="textarea")
				{
								event.keyCode=9;
				}
						return true;
			}
		}
		if (event.keyCode==rightKey)
		{
				if(LastTabObjectName.length>0 && !event.shiftKey && event.srcElement.name==LastTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+FirstTabObjectName+".focus()");
						return false;
			}
				else if(FirstTabObjectName.length>0 && event.shiftKey && event.srcElement.name==FirstTabObjectName)
			{
						event.keyCode=0;
						eval("document.all."+LastTabObjectName+".focus()");
						return false;
			}
				else
			{
						event.keyCode=9;
						return true;
			}
		}
	}
};
handleEnter();
function closeCalendarWin()
{

	try
	{
		var closeSelectDateWindow= Hid;
		if(previousObject == null)
		{
				return;
		}
		if(closeSelectDateWindow == true)
		{
				HiddenDiv();
		}
	}
	catch(e)
	{
			return;
	}
};
function dateCompare(fromDate,toDate,promptInfo)
{
	var calendar1Value = fromDate.value;
	var calendar2Value = toDate.value;
	if(calendar1Value!="" && calendar2Value!="")
	{
			if(calendar1Value > calendar2Value)
		{
				   alert(promptInfo);
					return false;
		}
	}
	return true;			
};
addOnloadEvent(Calendar_init);
package com.zte.cms.content.relation.service;

import java.util.List;

import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.cms.content.relation.dao.ICmsContentRelationDAO;
import com.zte.cms.content.relation.service.ICmsContentRelationDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class CmsContentRelationDS extends DynamicObjectBaseDS implements ICmsContentRelationDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ICmsContentRelationDAO dao = null;

    public void setDao(ICmsContentRelationDAO dao)
    {
        this.dao = dao;
    }

    public void insertCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException
    {
        log.debug("insert cmsContentRelation starting...");
        try
        {

            if (cmsContentRelation.getCgindex() == null)
            {
                Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_content_relation");
                cmsContentRelation.setCgindex(mapindex);
            }
            dao.insertCmsContentRelation(cmsContentRelation);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsContentRelation end");
    }

    public void insertCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException
    {
        log.debug("insert cmsContentRelationList by pk starting...");
        if (null == cmsContentRelationList || cmsContentRelationList.size() == 0)
        {
            log.debug("there is no datas in cmsContentRelationList");
            return;
        }
        try
        {
            for (CmsContentRelation cmsContentRelation : cmsContentRelationList)
            {

                if (cmsContentRelation.getCgindex() == null)
                {
                    Long mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_content_relation");
                    cmsContentRelation.setCgindex(mapindex);
                }
                dao.insertCmsContentRelation(cmsContentRelation);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert cmsContentRelationList by pk end");
    }

    public void updateCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException
    {
        log.debug("update cmsContentRelation by pk starting...");
        try
        {
            dao.updateCmsContentRelation(cmsContentRelation);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsContentRelation by pk end");
    }

    public void updateCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException
    {
        log.debug("update cmsContentRelationList by pk starting...");
        if (null == cmsContentRelationList || cmsContentRelationList.size() == 0)
        {
            log.debug("there is no datas in cmsContentRelationList");
            return;
        }
        try
        {
            for (CmsContentRelation cmsContentRelation : cmsContentRelationList)
            {
                dao.updateCmsContentRelation(cmsContentRelation);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update cmsContentRelationList by pk end");
    }

    public void removeCmsContentRelation(CmsContentRelation cmsContentRelation) throws DomainServiceException
    {
        log.debug("remove cmsContentRelation by pk starting...");
        try
        {
            dao.deleteCmsContentRelation(cmsContentRelation);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsContentRelation by pk end");
    }

    public void removeCmsContentRelationList(List<CmsContentRelation> cmsContentRelationList)
            throws DomainServiceException
    {
        log.debug("remove cmsContentRelationList by pk starting...");
        if (null == cmsContentRelationList || cmsContentRelationList.size() == 0)
        {
            log.debug("there is no datas in cmsContentRelationList");
            return;
        }
        try
        {
            for (CmsContentRelation cmsContentRelation : cmsContentRelationList)
            {
                dao.deleteCmsContentRelation(cmsContentRelation);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove cmsContentRelationList by pk end");
    }

    public CmsContentRelation getCmsContentRelation(CmsContentRelation cmsContentRelation)
            throws DomainServiceException
    {
        log.debug("get cmsContentRelation by pk starting...");
        CmsContentRelation rsObj = null;
        try
        {
            rsObj = dao.getCmsContentRelation(cmsContentRelation);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsContentRelationList by pk end");
        return rsObj;
    }

    public List<CmsContentRelation> getCmsContentRelationByCond(CmsContentRelation cmsContentRelation)
            throws DomainServiceException
    {
        log.debug("get cmsContentRelation by condition starting...");
        List<CmsContentRelation> rsList = null;
        try
        {
            rsList = dao.getCmsContentRelationByCond(cmsContentRelation);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get cmsContentRelation by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsContentRelation page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsContentRelation, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsContentRelation>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsContentRelation page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(CmsContentRelation cmsContentRelation, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get cmsContentRelation page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(cmsContentRelation, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsContentRelation>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsContentRelation page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery2(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get cmsContentRelation page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery2(cmsContentRelation, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsContentRelation>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get cmsContentRelation page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get UnSelected Content page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.queryUnSelectedContent(info, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<CmsContentRelationInfo>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get UnSelected Content page info by condition end");
        return tableInfo;
    }

    public TableDataInfo pageInfoQuery(String sql, String sqlcount, int start, int pageSize)
            throws DomainServiceException
    {
        PageInfo pageInfo = null;

        try
        {
            pageInfo = dao.pageInfoQuery(sql, sqlcount, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            daoEx.printStackTrace();
            throw new DomainServiceException(daoEx);
        }

        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());

        return tableInfo;
    }

    public List<CmsContentRelation> querySelectedContentList(CmsContentRelation cmsContentRelation)
            throws DomainServiceException
    {
        log.debug("get cmsContentRelation by condition starting...");
        List<CmsContentRelation> rsList = null;
        try
        {
            rsList = dao.querySelectedContentList(cmsContentRelation);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error(e);
        }
        log.debug("get cmsContentRelation by condition end...");
        return rsList;
    }

    public List<CmsContentRelationInfo> pageInfoQueryList(String sql, String sqlcount) throws DomainServiceException
    {
        log.debug("get pageInfoQueryList by condition starting...");
        List<CmsContentRelationInfo> rsList = null;
        try
        {
            rsList = dao.pageInfoQueryList(sql, sqlcount);
        }
        catch (DAOException e)
        {
            // TODO Auto-generated catch block
            log.error(e);
        }
        log.debug("get pageInfoQueryList by condition end...");
        return rsList;
    }
}

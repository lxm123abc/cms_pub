package com.zte.cms.basicdata.dao;

import java.util.List;

import com.zte.cms.basicdata.model.CmsProgramtypecpMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CmsProgramtypecpMapDAO extends DynamicObjectBaseDao implements ICmsProgramtypecpMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public CmsProgramtypecpMap getCmsProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap) throws DAOException
    {
        log.debug("query cmsProgramtypecpMap starting...");
        CmsProgramtypecpMap resultObj = (CmsProgramtypecpMap) super.queryForObject("getCmsProgramtypecpMap",
                cmsProgramtypecpMap);
        log.debug("query cmsProgramtypecpMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapByCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DAOException
    {
        log.debug("query cmsProgramtypecpMap by condition starting...");
        List<CmsProgramtypecpMap> rList = (List<CmsProgramtypecpMap>) super.queryForList(
                "queryCmsProgramtypecpMapListByCond", cmsProgramtypecpMap);
        log.debug("query cmsProgramtypecpMap by condition end");
        return rList;
    }

    public List<CmsProgramtypecpMap> getCmsProgramtypecpMapCond(CmsProgramtypecpMap cmsProgramtypecpMap)
            throws DAOException
    {
        log.debug("query getCmsProgramtypecpMapCond by condition starting...");
        List<CmsProgramtypecpMap> rList = (List<CmsProgramtypecpMap>) super.queryForList("getProgramtypecpMapByCond",
                cmsProgramtypecpMap);
        log.debug("query getCmsProgramtypecpMapCond by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsProgramtypecpMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypecpMapListCntByCond", cmsProgramtypecpMap))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtypecpMap> rsList = (List<CmsProgramtypecpMap>) super.pageQuery(
                    "queryCmsProgramtypecpMapListByCond", cmsProgramtypecpMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtypecpMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DAOException
    {
        return super.indexPageQuery("queryCmsProgramtypecpMapListByCond", "queryCmsProgramtypecpMapListCntByCond",
                cmsProgramtypecpMap, start, pageSize, puEntity);
    }

    // 按照typeid查询cp信息(参数typeindex)
    public PageInfo queryCPByTypid(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsProgramtypecpMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCPByTypidCount", cmsProgramtypecpMap)).intValue();
        if (totalCnt > 0)
        {

            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsProgramtypecpMap> rsList = (List<CmsProgramtypecpMap>) super.pageQuery("queryCPByTypid",

            cmsProgramtypecpMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);

        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtypecpMap by condition end");
        return pageInfo;
    }

    // 按照typeid查询cp信息(参数typeindex)
    public PageInfo queryCmsProgramtypeByCpId(CmsProgramtypecpMap cmsProgramtypecpMap, int start, int pageSize)
            throws DAOException
    {
        log.debug("page query cmsProgramtypecpMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypeByCpIdCount", cmsProgramtypecpMap))
                .intValue();
        if (totalCnt > 0)
        {

            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;

            List<CmsProgramtypecpMap> rsList = (List<CmsProgramtypecpMap>) super.pageQuery("queryCmsProgramtypeByCpId",
                    cmsProgramtypecpMap, start, fetchSize);

            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);

        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsProgramtypecpMap by condition end");
        return pageInfo;
    }

    public Integer queryCPByTypidCount(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        int totalCnt = ((Integer) super.queryForObject("queryCmsProgramtypeByCpIdCount", cmsProgramtypecpMap))
                .intValue();
        return totalCnt;
    }

    public void insertProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        super.insert("insertProgramtypecpMap", cmsProgramtypecpMap);
    }

    public void deleteProgramtypecpMap(CmsProgramtypecpMap cmsProgramtypecpMap)
    {
        super.delete("deleteProgramtypecpMap", cmsProgramtypecpMap);
    }

}
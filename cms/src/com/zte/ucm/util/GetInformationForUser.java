package com.zte.ucm.util;

import com.zte.cms.common.DbUtil;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class GetInformationForUser {

    public static String getCompanyName(String cpCode) throws Exception {
        StringBuilder companyNames = new StringBuilder();
        String cpCodes = cpCode.toString();
        String[] cpCodesList = cpCodes.split(",");
        int length = cpCodesList.length;
        if (StringUtils.isNotBlank(cpCodes)){
            for(int i = 0 ;i < length;i++){
                String sql = "select cpcnshortname from zxdbm_umap.cms_operator_power where cpid = " + cpCodesList[i];
                DbUtil db = new DbUtil();
                List rtnlist = db.getQuery(sql);
                for (int j = 0; j < rtnlist.size(); j++)
                {
                    Map<String, String> tmpMap = (Map<String, String>) rtnlist.get(j);
                    String cpcnshortname=tmpMap.get("cpcnshortname").toString();
                    companyNames.append(cpcnshortname).append(",");//把所有的id都放在一个StringBuilder里，用于后面进行查询用户的邮箱
                }
            }

        }
        return companyNames.toString();
    }




}

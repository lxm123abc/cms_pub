package com.zte.cms.channel.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CategoryChannelMap extends DynamicBaseObject
{
    private java.lang.Long mapindex;
    private java.lang.Long categoryindex;
    private java.lang.String categoryid;
    private java.lang.Long channelindex;
    private java.lang.String channelid;
    private java.lang.Integer sequence;
    private java.lang.Integer status;
    private java.lang.String createtime;
    private java.lang.String publishtime;
    private java.lang.String mappingid;
    private java.lang.String categorycode;
    private java.lang.String cpcontentid;

    private String categoryname;

    private String cancelpubtime;  
    
    
	public java.lang.String getCategorycode() {
		return categorycode;
	}

	public void setCategorycode(java.lang.String categorycode) {
		this.categorycode = categorycode;
	}

	public java.lang.String getCpcontentid() {
		return cpcontentid;
	}

	public void setCpcontentid(java.lang.String cpcontentid) {
		this.cpcontentid = cpcontentid;
	}

	public java.lang.String getMappingid() {
		return mappingid;
	}

	public void setMappingid(java.lang.String mappingid) {
		this.mappingid = mappingid;
	}

	public String getCancelpubtime()
    {
        return cancelpubtime;
    }

    public void setCancelpubtime(String cancelpubtime)
    {
        this.cancelpubtime = cancelpubtime;
    }

    public String getCategoryname()
    {
        return categoryname;
    }

    public void setCategoryname(String categoryname)
    {
        this.categoryname = categoryname;
    }

    public java.lang.Long getMapindex()
    {
        return mapindex;
    }

    public void setMapindex(java.lang.Long mapindex)
    {
        this.mapindex = mapindex;
    }

    public java.lang.Long getCategoryindex()
    {
        return categoryindex;
    }

    public void setCategoryindex(java.lang.Long categoryindex)
    {
        this.categoryindex = categoryindex;
    }

    public java.lang.String getCategoryid()
    {
        return categoryid;
    }

    public void setCategoryid(java.lang.String categoryid)
    {
        this.categoryid = categoryid;
    }

    public java.lang.Long getChannelindex()
    {
        return channelindex;
    }

    public void setChannelindex(java.lang.Long channelindex)
    {
        this.channelindex = channelindex;
    }

    public java.lang.String getChannelid()
    {
        return channelid;
    }

    public void setChannelid(java.lang.String channelid)
    {
        this.channelid = channelid;
    }

    public java.lang.Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(java.lang.Integer sequence)
    {
        this.sequence = sequence;
    }

    public java.lang.Integer getStatus()
    {
        return status;
    }

    public void setStatus(java.lang.Integer status)
    {
        this.status = status;
    }

    public java.lang.String getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(java.lang.String createtime)
    {
        this.createtime = createtime;
    }

    public java.lang.String getPublishtime()
    {
        return publishtime;
    }

    public void setPublishtime(java.lang.String publishtime)
    {
        this.publishtime = publishtime;
    }

    public void initRelation()
    {
        this.addRelation("mapindex", "MAPINDEX");
        this.addRelation("categoryindex", "CATEGORYINDEX");
        this.addRelation("categoryid", "CATEGORYID");
        this.addRelation("channelindex", "CHANNELINDEX");
        this.addRelation("channelid", "CHANNELID");
        this.addRelation("sequence", "SEQUENCE");
        this.addRelation("status", "STATUS");
        this.addRelation("createtime", "CREATETIME");
        this.addRelation("publishtime", "PUBLISHTIME");
    }
}


  package com.zte.cms.impSyncTask.dao;

import java.util.List;

import com.zte.cms.impSyncTask.dao.IObjectCpcntRecordDAO;
import com.zte.cms.impSyncTask.model.ObjectCpcntRecord;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class ObjectCpcntRecordDAO extends DynamicObjectBaseDao implements IObjectCpcntRecordDAO
{	
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
    public void insertObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("insert objectCpcntRecord starting...");
    	super.insert( "insertObjectCpcntRecord", objectCpcntRecord );
    	log.debug("insert objectCpcntRecord end");
    }
	
	public void insertObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException
	{
		log.debug("insert objectCpcntRecordList starting...");
		if( null != objectCpcntRecordList )
		{
		super.insertBatch( "insertObjectCpcntRecord", objectCpcntRecordList );
		}
		log.debug("insert objectCpcntRecordList end");
	}
     
    public void updateObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("update objectCpcntRecord by pk starting...");
       	super.update( "updateObjectCpcntRecord", objectCpcntRecord );
       	log.debug("update objectCpcntRecord by pk end");
    }
    public void updateObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException
    {
    	log.debug("update objectCpcntRecordList by pk starting...");
	super.updateBatch( "updateObjectCpcntRecord", objectCpcntRecordList );
       	log.debug("update objectCpcntRecordList by pk end");
    }

    public void updateObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("update objectCpcntRecord by conditions starting...");
    	super.update( "updateObjectCpcntRecordByCond", objectCpcntRecord );
    	log.debug("update objectCpcntRecord by conditions end");
    }
    public void updateObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException
    {
    	log.debug("update objectCpcntRecordList by conditions starting...");
    	super.updateBatch( "updateObjectCpcntRecordByCond", objectCpcntRecordList );
    	log.debug("update objectCpcntRecordList by conditions end");
    }   

    public void deleteObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("delete objectCpcntRecord by pk starting...");
       	super.delete( "deleteObjectCpcntRecord", objectCpcntRecord );
       	log.debug("delete objectCpcntRecord by pk end");
    }
    public void deleteObjectCpcntRecordList( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException
    {
    	log.debug("delete objectCpcntRecordList by pk starting...");
       	super.deleteBatch( "deleteObjectCpcntRecord", objectCpcntRecordList );
       	log.debug("delete objectCpcntRecordList by pk end");
    }

    public void deleteObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("delete objectCpcntRecord by conditions starting...");
    	super.delete( "deleteObjectCpcntRecordByCond", objectCpcntRecord );
    	log.debug("delete objectCpcntRecord by conditions end");
    }
    public void deleteObjectCpcntRecordListByCond( List<ObjectCpcntRecord> objectCpcntRecordList )throws DAOException
    {
    	log.debug("delete objectCpcntRecordList by conditions starting...");
    	super.deleteBatch( "deleteObjectCpcntRecordByCond", objectCpcntRecordList );
    	log.debug("delete objectCpcntRecordList by conditions end");
    }  

    public ObjectCpcntRecord getObjectCpcntRecord( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("query objectCpcntRecord starting...");
       	ObjectCpcntRecord resultObj = (ObjectCpcntRecord)super.queryForObject( "getObjectCpcntRecord",objectCpcntRecord);
       	log.debug("query objectCpcntRecord end");
       	return resultObj;
    }

	@SuppressWarnings("unchecked")
    public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord )throws DAOException
    {
    	log.debug("query objectCpcntRecord by condition starting...");
    	List<ObjectCpcntRecord> rList = (List<ObjectCpcntRecord>)super.queryForList( "queryObjectCpcntRecordListByCond",objectCpcntRecord);
    	log.debug("query objectCpcntRecord by condition end");
    	return rList;
    }
	@SuppressWarnings("unchecked")
    public List<ObjectCpcntRecord> getObjectCpcntRecordByCond( ObjectCpcntRecord objectCpcntRecord, PageUtilEntity puEntity )throws DAOException
    {
        log.debug("query objectCpcntRecord by condition starting...");
        List<ObjectCpcntRecord> rList = (List<ObjectCpcntRecord>)super.queryForList( "queryObjectCpcntRecordListByCond",objectCpcntRecord,puEntity);
        log.debug("query objectCpcntRecord by condition end");
        return rList;
    }
	
	@SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)throws DAOException
    {
    	log.debug("page query objectCpcntRecord by condition starting...");
     	PageInfo pageInfo = null;
    	int totalCnt = ((Integer) super.queryForObject("queryObjectCpcntRecordListCntByCond",  objectCpcntRecord)).intValue();
    	if( totalCnt > 0 )
    	{
    		int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
    		List<ObjectCpcntRecord> rsList = (List<ObjectCpcntRecord>)super.pageQuery( "queryObjectCpcntRecordListByCond" ,  objectCpcntRecord , start , fetchSize );
    		pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
    	}
    	else
    	{
    		pageInfo = new PageInfo();
    	}
    	log.debug("page query objectCpcntRecord by condition end");
    	return pageInfo;
    }
    
    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize, PageUtilEntity puEntity)throws DAOException
    {
    	return super.indexPageQuery( "queryObjectCpcntRecordListByCond", "queryObjectCpcntRecordListCntByCond", objectCpcntRecord, start, pageSize, puEntity);
    }

    public PageInfo getObjectCpcntRecord(ObjectCpcntRecord objectCpcntRecord, int start, int pageSize)
            throws DAOException
    {

        log.debug("page query objectCpcntRecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("getObjectCpcntRecordListCntByTaskindex",  objectCpcntRecord)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ObjectCpcntRecord> rsList = (List<ObjectCpcntRecord>)super.pageQuery( "getObjectCpcntRecordListByTaskindex" ,  objectCpcntRecord , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query objectCpcntRecord by condition end");
        return pageInfo;
    
    }
    
} 


package com.zte.cms.impSyncTask.service;

import java.util.List;
import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IImpCmsSynctaskDS
{
    /**
  * 新增ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */
	public void insertImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;
    /**
  * 新增ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */
	public void insertImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException;	
	
    /**
  * 更新ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */
	public void updateImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;

    /**
  * 批量更新ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */
	public void updateImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException;

	/**
  * 根据条件更新ImpCmsSynctask对象  
  *
  * @param impCmsSynctask ImpCmsSynctask更新条件
  * @throws DomainServiceException ds异常
  */ 
	public void updateImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;

	/**
  * 根据条件批量更新ImpCmsSynctask对象  
  *
  * @param impCmsSynctask ImpCmsSynctask更新条件
  * @throws DomainServiceException ds异常
  */ 
	public void updateImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException;

	/**
  * 删除ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */ 
	public void removeImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;

	/**
  * 批量删除ImpCmsSynctask对象
  *
  * @param impCmsSynctask ImpCmsSynctask对象
  * @throws DomainServiceException ds异常
  */ 
	public void removeImpCmsSynctaskList( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException;

    /**
  * 根据条件删除ImpCmsSynctask对象
  * 
  * @param impCmsSynctask ImpCmsSynctask删除条件
  * @throws DomainServiceException ds异常
  */  
    public void removeImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;	

    /**
  * 根据条件批量删除ImpCmsSynctask对象
  * 
  * @param impCmsSynctask ImpCmsSynctask删除条件
  * @throws DomainServiceException ds异常
  */ 
    public void removeImpCmsSynctaskListByCond( List<ImpCmsSynctask> impCmsSynctaskList )throws DomainServiceException;	

	/**
  * 查询ImpCmsSynctask对象
  
  * @param impCmsSynctask ImpCmsSynctask对象
  * @return ImpCmsSynctask对象
  * @throws DomainServiceException ds异常 
  */
	 public ImpCmsSynctask getImpCmsSynctask( ImpCmsSynctask impCmsSynctask )throws DomainServiceException; 

	 /**
   * 根据条件查询ImpCmsSynctask对象 
   * 
   * @param impCmsSynctask ImpCmsSynctask对象
   * @return 满足条件的ImpCmsSynctask对象集
   * @throws DomainServiceException ds异常
   */
     public List<ImpCmsSynctask> getImpCmsSynctaskByCond( ImpCmsSynctask impCmsSynctask )throws DomainServiceException;

	 /**
   * 根据条件分页查询ImpCmsSynctask对象 
   *
   * @param impCmsSynctask ImpCmsSynctask对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DomainServiceException ds异常
   */
     public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize)throws DomainServiceException;
     
	 /**
   * 根据条件分页查询ImpCmsSynctask对象 
   *
   * @param impCmsSynctask ImpCmsSynctask对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws DomainServiceException ds异常
   */
     public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
}
package com.zte.cms.seriescrmmap.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.seriescrmmap.dao.ISeriesCrmMapDAO;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.cms.seriescrmmap.service.ISeriesCrmMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class SeriesCrmMapDS extends DynamicObjectBaseDS implements ISeriesCrmMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private ISeriesCrmMapDAO dao = null;

    public void setDao(ISeriesCrmMapDAO dao)
    {
        this.dao = dao;
    }

    public String insertSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("insert seriesCrmMap starting...");
        Long mapindex = null;
        try
        {
            mapindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("series_crm_map");
            seriesCrmMap.setMapindex(mapindex);
            String mappingid = String.format("%02d", ObjectType.SERIESCASTROLEMAPMAP_TYPE)+ String.format("%030d", mapindex);
            seriesCrmMap.setMappingid(mappingid);
            seriesCrmMap.setSequence(0);
            dao.insertSeriesCrmMap(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert seriesCrmMap end");
        return mapindex.toString();
    }

    public void updateSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("update seriesCrmMap by pk starting...");
        try
        {
            dao.updateSeriesCrmMap(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesCrmMap by pk end");
    }

    public void updateSeriesCrmMapList(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException
    {
        log.debug("update seriesCrmMapList by pk starting...");
        if (null == seriesCrmMapList || seriesCrmMapList.size() == 0)
        {
            log.debug("there is no datas in seriesCrmMapList");
            return;
        }
        try
        {
            for (SeriesCrmMap seriesCrmMap : seriesCrmMapList)
            {
                dao.updateSeriesCrmMap(seriesCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesCrmMapList by pk end");
    }

    public void updateSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("update seriesCrmMap by condition starting...");
        try
        {
            dao.updateSeriesCrmMapByCond(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesCrmMap by condition end");
    }

    public void updateSeriesCrmMapListByCond(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException
    {
        log.debug("update seriesCrmMapList by condition starting...");
        if (null == seriesCrmMapList || seriesCrmMapList.size() == 0)
        {
            log.debug("there is no datas in seriesCrmMapList");
            return;
        }
        try
        {
            for (SeriesCrmMap seriesCrmMap : seriesCrmMapList)
            {
                dao.updateSeriesCrmMapByCond(seriesCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update seriesCrmMapList by condition end");
    }

    public void removeSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("remove seriesCrmMap by pk starting...");
        try
        {
            dao.deleteSeriesCrmMap(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesCrmMap by pk end");
    }

    public void removeSeriesCrmMapList(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException
    {
        log.debug("remove seriesCrmMapList by pk starting...");
        if (null == seriesCrmMapList || seriesCrmMapList.size() == 0)
        {
            log.debug("there is no datas in seriesCrmMapList");
            return;
        }
        try
        {
            for (SeriesCrmMap seriesCrmMap : seriesCrmMapList)
            {
                dao.deleteSeriesCrmMap(seriesCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesCrmMapList by pk end");
    }

    public void removeSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("remove seriesCrmMap by condition starting...");
        try
        {
            dao.deleteSeriesCrmMapByCond(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesCrmMap by condition end");
    }

    public void removeSeriesCrmMapListByCond(List<SeriesCrmMap> seriesCrmMapList) throws DomainServiceException
    {
        log.debug("remove seriesCrmMapList by condition starting...");
        if (null == seriesCrmMapList || seriesCrmMapList.size() == 0)
        {
            log.debug("there is no datas in seriesCrmMapList");
            return;
        }
        try
        {
            for (SeriesCrmMap seriesCrmMap : seriesCrmMapList)
            {
                dao.deleteSeriesCrmMapByCond(seriesCrmMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove seriesCrmMapList by condition end");
    }

    public SeriesCrmMap getSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("get seriesCrmMap by pk starting...");
        SeriesCrmMap rsObj = null;
        try
        {
            rsObj = dao.getSeriesCrmMap(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesCrmMapList by pk end");
        return rsObj;
    }

    public List<SeriesCrmMap> getSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DomainServiceException
    {
        log.debug("get seriesCrmMap by condition starting...");
        List<SeriesCrmMap> rsList = null;
        try
        {
            rsList = dao.getSeriesCrmMapByCond(seriesCrmMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get seriesCrmMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get seriesCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesCrmMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesCrmMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get seriesCrmMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(seriesCrmMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<SeriesCrmMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get seriesCrmMap page info by condition end");
        return tableInfo;
    }
}

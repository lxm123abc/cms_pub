package com.zte.cms.cast.castsync.dao;

import java.util.List;

import com.zte.cms.cast.castsync.model.CastPlatformSync;
import com.zte.cms.content.program.programsync.model.ProgramPlatformSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CastPlatformSyncDAO extends DynamicObjectBaseDao implements ICastPlatformSyncDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());
    
    public PageInfo pageInfoQuery(CastPlatformSync castPlatformSync, int start, int pageSize)throws DAOException
    {
        log.debug("page query castPlatformSyn by condition starting...");
        PageInfo pageInfo = null;
        if (castPlatformSync.getTypePlatform().equals("iptv3") )
        {
            int totalCnt = ((Integer) super.queryForObject("queryCastPlatfSynListCntByCond",  castPlatformSync)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCastPlatfSynListByCond" ,  castPlatformSync , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }
            
        }
        if (castPlatformSync.getTypePlatform().equals("iptv2") )
        {

            int totalCnt = ((Integer) super.queryForObject("queryCastPlatfSyn2ListCntByCond",  castPlatformSync)).intValue();
            if( totalCnt > 0 )
            {
                int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
                List<ProgramPlatformSyn> rsList = (List<ProgramPlatformSyn>)super.pageQuery( "queryCastPlatfSyn2ListByCond" ,  castPlatformSync , start , fetchSize );
                pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
            }
            else
            {
                pageInfo = new PageInfo();
            }
            
        }

        log.debug("page query castPlatformSyn by condition end");
        return pageInfo;
    }
    
    
    public CastPlatformSync getCastPlatformSync( CastPlatformSync castPlatformSync )throws DAOException
    {
        log.debug("query castPlatformSyn starting...");
        CastPlatformSync resultObj = (CastPlatformSync)super.queryForObject( "getCastPlatformSyn",castPlatformSync);
        log.debug("query castPlatformSyn end");
        return resultObj;
    }

}

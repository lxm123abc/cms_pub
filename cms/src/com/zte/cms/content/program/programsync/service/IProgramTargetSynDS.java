

package com.zte.cms.content.program.programsync.service;

import java.util.List;

import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IProgramTargetSynDS
{
   

	/**
	 * 查询CntTargetsysSyn对象
	 
	 * @param programTargetSyn CntTargetsysSyn对象
	 * @return CntTargetsysSyn对象
	 * @throws DomainServiceException ds异常 
	 */
	 public ProgramTargetSyn getProgramTargetSyn( ProgramTargetSyn programTargetSyn )throws DomainServiceException; 

	/**
	 * 查询CntTargetsysSyn对象
	 
	 * @param programTargetSyn CntTargetsysSyn对象
	 * @return CntTargetsysSyn对象
	 * @throws DomainServiceException ds异常 
	 */
	public List<ProgramTargetSyn> getProgramTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DomainServiceException;


	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programTargetSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DomainServiceException;
     
	 /**
	  * 根据条件分页查询CntTargetsysSyn对象 
	  *
	  * @param programTargetSyn CntTargetsysSyn对象，作为查询条件的参数 
	  * @param start 起始行
	  * @param pageSize 页面大小
	  * @param puEntity 排序空置参数@see PageUtilEntity
	  * @return  查询结果
	  * @throws DomainServiceException ds异常
	  */
     public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException;
    
 	public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV3( ProgramTargetSyn programTargetSyn )throws DomainServiceException;

 	public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV2( ProgramTargetSyn programTargetSyn )throws DomainServiceException;

 	
	public List<ProgramTargetSyn> getProTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DomainServiceException;
	
	public TableDataInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DomainServiceException;
}
package com.zte.ismp.common.util;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.zte.sapi.ServiceAccess;
import com.zte.sapi.ServiceAccessException;
import com.zte.sapi.protocol.FTPService;

/**
 * <p>
 * 文件名称: FtpUtil.java
 * </p>
 * <p>
 * 文件描述: Ftp工具对象类
 * </p>
 * <p>
 * 版权所有: 版权所有(C)2001-2007
 * </p>
 * <p>
 * 公 司: 中兴通讯股份有限公司
 * </p>
 * <p>
 * 内容摘要: wsmapTag为true是采用wasmap平台ftp底层,为false时采用原来的ftp底层
 * </p>
 * <p>
 * 其他说明:
 * </p>
 * <p>
 * 建立日期：2007-10-10
 * </p>
 * <p>
 * 修改日期：
 * </p>
 * 
 * @version 1.0
 * @author 张东涛
 */
public class FtpUtil
{

    // ****** 代码段: 常量定义 *******************************************************************************/

    // ****** 代码段: 变量定义 *******************************************************************************/

    FTPService ftpService = null;

    private Logger logger = Logger.getLogger(this.getClass().getName());

    // ****** 代码段: 构造方法 *******************************************************************************/
    public FtpUtil()
    {

        try
        {
            ftpService = ServiceAccess.getFTPService();
        }
        catch (ServiceAccessException e)
        {
            logger.error("ftpService init error: ", e);
        }

    }

    // ****** 代码段: 公共方法 *******************************************************************************/
    /**
     * 建立FTP连接
     * 
     * @param host String
     * @param user String
     * @param password String
     * @return boolean
     */
    public boolean connect(String host, String user, String password)
    {

        // host：FTP服务器的IP地址；user:登录FTP服务器的用户名
        // password：登录FTP服务器的用户名的口令；

        boolean re = false;
        try
        {
            re = ftpService.connect(host, user, password);
        }
        catch (IOException e)
        {
            logger.debug("ftpClient.login error: ", e);
        }
        return re;

    }

    /**
     * 建立FTP连接
     * 
     * @param host String
     * @param port int
     * @param user String
     * @param password String
     * @return boolean
     */
    public boolean connect(String host, int port, String user, String password)
    {

        // host：FTP服务器的IP地址；user:登录FTP服务器的用户名
        // password：登录FTP服务器的用户名的口令；

        boolean re = false;
        try
        {
            re = ftpService.connect(host, port, user, password);
        }
        catch (IOException e)
        {
            logger.debug("ftpClient.login error: ", e);
        }
        return re;

    }

    /**
     * 断开FTP链接
     */
    public void disconnect()
    {

        try
        {
            ftpService.disconnect();
        }
        catch (IOException ex)
        {
            logger.error("ftpClient.closeServer() error: ", ex);
        }

    }

    /**
     * 通过FTP上传文件，将路径为localUrl文件保存到FTP服务器的remotePath路径上
     * 
     * @param localUrl String
     * @param remotePath String
     * @return boolean
     */
    public boolean upload(String localUrl, String remotePath)
    {

        boolean re = false;
        try
        {
            String localName = localUrl.substring(localUrl.lastIndexOf("/") + 1);
            String localPath = localUrl.substring(0, localUrl.lastIndexOf("/") + 1);
            re = ftpService.upload(localPath, localName, remotePath, localName, false);
        }
        catch (Exception ex)
        {
            logger.debug("上传[" + localUrl + "]文件到[" + remotePath + "]目录失败!", ex);
        }
        return re;

    }

    /**
     * 通过FTP上传文件，可以更改文件名
     * 
     * @param localPath String
     * @param localName String
     * @param remotePath String
     * @param remoteName String
     * @return boolean
     */
    public boolean upload(String localPath, String localName, String remotePath, String remoteName)
    {

        boolean re = false;
        try
        {
            re = ftpService.upload(localPath, localName, remotePath, remoteName, false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            logger.debug("上传[" + localPath + "]文件到[" + remotePath + "]目录失败!", ex);
        }
        return re;

    }

    /**
     * 下载文件
     * 
     * @param remotePath String
     * @param remoteName String
     * @param localPath String
     * @param localName String
     * @return boolean
     */
    public boolean download(String remotePath, String remoteName, String localPath, String localName)
    {

        boolean re = false;
        try
        {
            re = ftpService.download(remotePath, remoteName, localPath, localName, false);
        }
        catch (IOException ex)
        {
            logger.error("下载[" + remoteName + "]文件到[" + localPath + "]目录失败!", ex);
        }
        return re;

    }

    /**
     * 删除文件
     * 
     * @param remotePath String
     * @param remoteName String
     * @param ft FtpUtil
     * @return boolean
     */
    public boolean deleteFile(String fileName)
    {

        boolean re = false;
        try
        {
            re = ftpService.deleteFile(fileName);
            logger.debug("删除下的文件[" + fileName + "]" + re);
        }
        catch (IOException e)
        {
            logger.error("删除[" + fileName + "]失败!", e);
        }
        return re;

    }

    /**
     * 删除目录
     * 
     * @param folderName String
     * @return boolean
     */
    public boolean deleteFolder(String fileName)
    {

        boolean re = false;
        try
        {
            re = ftpService.removeDirectory(fileName);
            logger.debug("删除下的目录[" + fileName + "]" + re);
        }
        catch (IOException e)
        {
            logger.error("删除目录[" + fileName + "]失败!", e);
        }
        return re;

    }

    /**
     * 返回指定目录的所有文件及文件夹
     * 
     * @return ArrayList
     * @throws IOException
     */
    public ArrayList getFileList(String path)
    {

        ArrayList al = new ArrayList();
        try
        {
            String[] names = ftpService.listNames(path);
            for (int i = 0; i < names.length; i++)
            {
                al.add(names[i]);
            }
        }
        catch (IOException ex)
        {
            logger.error("getFileList error: ", ex);
        }
        return al;

    }

    /**
     * 创建目录
     * 
     * @param remotePath String
     * @param dirName String
     * @param ft FtpUtil
     * @return boolean
     */
    public boolean mkDir(String pathName)
    {

        boolean re = false;
        try
        {
            re = ftpService.makeDirectory(pathName);
        }
        catch (Exception ex)
        {
            logger.error("make Directory error: ", ex);
        }
        return re;

    }

    /**
     * 判断文件是否存在
     * 
     * @param remotePath String
     * @param remoteName String
     * @return boolean
     */
    public boolean isExistFile(String fileName)
    {

        try
        {
            // 列出指定目录下所有的文件名,如果参数为某个文件,则如果该文件不存在,返回null.
            String[] names = ftpService.listNames(fileName);
            if (names == null || names.length == 0)
            {
                return false;
            }
        }
        catch (IOException ex)
        {
            logger.error("isExistFile error: ", ex);
            return false;
        }
        return true;

    }

    /**
     * 获取文件的大小 注意：文件的名称中目前暂时不能含有空格
     * 
     * @param fileName String
     * @return long
     */
    public long getFileSize(String fileName)
    {

        long ret = -1L; // 表示不存在
        try
        {
            ret = ftpService.getFileSize(fileName);
        }
        catch (IOException ex)
        {
            logger.error("isExistFile error: ", ex);
        }
        return ret;

    }

    public boolean rename(String srcFileName, String desFileName)
    {

        boolean re = false;
        try
        {
            re = ftpService.rename(srcFileName, desFileName);
            logger.debug("更改文件[" + srcFileName + "] 为 [" + desFileName + "]");
        }
        catch (Exception ex)
        {
            logger.error("rename error: ", ex);
        }
        return re;

    }

    // ****** 代码段: 公共方法,方法内自带FTP建立与释放 *******************************************************************************/
    /**
     * 上传一个文件
     * 
     * @param host ： FTP服务器IP地址
     * @param user ：用户名
     * @param password ：密码
     * @param localPath ：本地路径
     * @param localName ：本地文件名
     * @param remotePath ：文件在ftp服务器上的绝对路径
     * @param remoteName ：文件在ftp服务器上的名称
     */
    public boolean upload(String host, String user, String password, String localPath, String localName,
            String remotePath, String remoteName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);

            if (isconnect)
            {
                if (!isExistFile(remotePath))
                {
                    mkDir(remotePath);
                }

                ret = this.upload(localPath, localName, remotePath, remoteName);
                logger.debug("上传文件" + localName + "为" + remoteName + " ——成功");
            }
            else
            {
                logger.error("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 将remotePath目录的文件下载并保存到localPath目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param remotePath String
     * @param remoteName String
     * @param localPath String
     * @param localName String
     * @return boolean Modify:Dongyang,20060922
     */
    public boolean download(String host, String user, String password, String remotePath, String remoteName,
            String localPath, String localName)
    {

        boolean ret = false;

        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.download(remotePath, remoteName, localPath, localName);
                logger.debug("下载文件" + remoteName + "为" + localName + " ——成功");
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
            ret = false;
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 删除FTP服务器上的文件
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean deleteFile(String host, String user, String password, String fileName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                boolean b = this.isExistFile(fileName);

                if (!b)
                { // 无此文件，就当作删除成功
                    ret = true;
                }
                else
                { // 有此文件，执行删除
                    ret = this.deleteFile(fileName);
                }
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 删除FTP服务器上的目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean deleteFolder(String host, String user, String password, String fileName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                boolean b = this.isExistFile(fileName);

                if (!b)
                { // 无此文件，就当作删除成功
                    ret = true;
                }
                else
                { // 有此文件，执行删除
                    ret = this.deleteFolder(fileName);
                }
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.debug(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 判断文件是否存在
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return boolean
     */
    public boolean isExistFile(String host, String user, String password, String fileName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.isExistFile(fileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 获取文件的大小
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param fileName String
     * @return long
     */
    public long getFileSize(String host, String user, String password, String fileName)
    {

        long ret = -1;

        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.getFileSize(fileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 重命名文件
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param srcFileName String
     * @param desFileName String
     * @return boolean
     */
    public boolean rename(String host, String user, String password, String srcFileName, String desFileName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.rename(srcFileName, desFileName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    /**
     * 创建目录
     * 
     * @param host String
     * @param user String
     * @param password String
     * @param pathName String
     * @return boolean
     */
    public boolean mkDir(String host, String user, String password, String pathName)
    {

        boolean ret = false;
        try
        {
            boolean isconnect = this.connect(host, user, password);
            if (isconnect)
            {
                ret = this.mkDir(pathName);
            }
            else
            {
                logger.debug("连接" + host + "失败");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        finally
        {
            this.disconnect();
        }
        return ret;
    }

    // 建目录ok,判断目录ok,上传文件ok,判断文件ok,下载文件ok,删文件ok,删目录ok,上传目录no,下载目录no,改名ok,文件列表ok,文件大小ok
    public static void test()
    {

        FtpUtil ftpOperation = new FtpUtil();
        ftpOperation.connect("127.0.0.1", "user", "Pass2007");
        // ftpOperation.mkDir("aaa");
        // ftpOperation.upload("C:\\111\\/bad_egg.jpg", "/aaa/");
        // ftpOperation.rename("/aaa/bad_egg.jpg", "/aaa/bad2.jpg");
        // System.out.print(ftpOperation.getFileSize("/aaa/bad_egg.jpg"));
        // System.out.print(ftpOperation.getFileList("aaa").size());
        // ftpOperation.download("/aaa/", "bad_egg.jpg", "c:/", "111.jpg");
        // ftpOperation.deleteFolder("aaa");
        // System.out.println(ftpOperation.isExistFile("/aaa/"));
        // ftpOperation.deleteFile("/aaa/bad_egg.jpg");
        // System.out.println(ftpOperation.isExistFile("/aaa/bad_egg.jpg"));
        ftpOperation.disconnect();

    }

    // 自连:建目录ok,判断目录ok,上传文件ok,判断文件ok,下载文件ok,删文件ok,删目录ok,上传目录no,下载目录no,改名ok,文件列表no,文件大小ok
    public static void test2()
    {

        FtpUtil ftpOperation = new FtpUtil();
        // ftpOperation.rename("127.0.0.1", "user", "Pass2007", "/aaa/bad2.jpg", "/aaa/bad.jpg");
        // ftpOperation.mkDir("127.0.0.1","user","Pass2007","bbb");
        // ftpOperation.upload("127.0.0.1", "user", "Pass2007","C:\\111\\","/bad_egg.jpg", "/bbb/","bad.jpg");
        // ftpOperation.rename("127.0.0.1", "user", "Pass2007","/aaa/bad_egg.jpg", "/aaa/bad2.jpg");
        // System.out.print(ftpOperation.getFileSize("127.0.0.1", "user", "Pass2007","/bbb/bad.jpg"));
        // ftpOperation.download("127.0.0.1", "user", "Pass2007","/bbb/", "bad.jpg", "c:/", "111.jpg");
        // System.out.println(ftpOperation.isExistFile("127.0.0.1", "user", "Pass2007","/bbb/"));
        // ftpOperation.deleteFolder("127.0.0.1", "user", "Pass2007","bbb");
        // ftpOperation.deleteFile("127.0.0.1", "user", "Pass2007","/bbb/bad.jpg");
        // System.out.println(ftpOperation.isExistFile("127.0.0.1", "user", "Pass2007","/bbb/bad.jpg"));
    }

    public static void main(String[] args)
    {

    }
}

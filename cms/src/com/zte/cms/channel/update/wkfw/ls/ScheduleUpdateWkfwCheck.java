package com.zte.cms.channel.update.wkfw.ls;

import com.zte.cms.channel.ls.ChannelConstants;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsChannelBuf;
import com.zte.cms.channel.model.CmsSchedule;
import com.zte.cms.channel.model.CmsScheduleBuf;
import com.zte.cms.channel.service.ICmsChannelBufDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.channel.service.ICmsScheduleBufDS;
import com.zte.cms.channel.service.ICmsScheduleDS;
import com.zte.cms.service.ls.ServiceConstants;

import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.ResourceMgt;

/**
 * 
 * @author Administrator
 * 
 */
public class ScheduleUpdateWkfwCheck implements IScheduleUpdateWkfwCheck
{

    private ICmsScheduleDS scheduleds = null;
    private ICmsScheduleBufDS scheduleBufds = null;

    // 构造方法
    ScheduleUpdateWkfwCheck()
    {
        ResourceMgt.addDefaultResourceBundle(ChannelConstants.RESOURCE_UCDN_CHANNEL);
    }

    

    public ICmsScheduleDS getScheduleds() {
		return scheduleds;
	}



	public void setScheduleds(ICmsScheduleDS scheduleds) {
		this.scheduleds = scheduleds;
	}



	public ICmsScheduleBufDS getScheduleBufds() {
		return scheduleBufds;
	}



	public void setScheduleBufds(ICmsScheduleBufDS scheduleBufds) {
		this.scheduleBufds = scheduleBufds;
	}



	/**
     * @param channelindex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkScheduleUpdateWkfw(Long scheduleindex) throws DomainServiceException
    {
        CmsSchedule schedule = new CmsSchedule();
        schedule.setScheduleindex(scheduleindex);
        schedule = scheduleds.getCmsSchedule(schedule);

        if (schedule == null)
        {
            return "119";// + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_WKFW_NO_EXISTS); // 不存在
        }
        // 状态不为待发布0，新增同步成功20 修改同步成功50 修改同步失败60 取消同步成功80
        if ((schedule.getStatus() != 0) && (schedule.getStatus() != 20) && (schedule.getStatus() != 50)
                && (schedule.getStatus() != 60) && (schedule.getStatus() != 80))
        {
            return "117";// + ResourceMgt.findDefaultText(ChannelConstants.RESOURCE_UCDN_CHANNEL_STATUS_WRONG); // 状态不正确
        }
        // 已经发起工作流
        if (schedule.getWorkflow() != null)
        {
            if (schedule.getWorkflow() != 0)
            {
                return "916";// + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_IN_ALREADY);
                // 已发起工作流未结束
            }
        }
        if (schedule.getWorkflowlife() != null)
        {
            if (schedule.getWorkflowlife() != 0)
            {
                return "917";// + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);
                // 工作流流程状态不正确
            }
        }
        return "success";// + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

    /**
     * @param channelIndex Long
     * @return String
     * @throws DomainServiceException DomainServiceException
     */
    public String checkScheduleUpdateWkfwAudit(Long scheduleIndex) throws DomainServiceException
    {
        CmsScheduleBuf schedulebuf = new CmsScheduleBuf();
        schedulebuf.setScheduleindex(scheduleIndex);
        schedulebuf = scheduleBufds.getCmsScheduleBuf(schedulebuf);

        if (schedulebuf == null)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_BUF_NO_EXISTS);
            // buf表中数据不存在
        }
        if (schedulebuf.getWorkflow() != 2)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_NOT_IN_WKFW_OR_WRONG_WKFW);
            // 不在工作流中或所处工作流不正确
        }
        if (schedulebuf.getWorkflowlife() != 1)
        {
            return "1:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_STATUS_WRONG);
            // 工作流流程状态不正确
        }
        return "0:" + ResourceMgt.findDefaultText(ServiceConstants.RESOURCE_UCDN_SERVICE_WKFW_CHECK_OK); // 校验成功，可以操作
    }

}

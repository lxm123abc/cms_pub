package com.zte.cms.storageManage.dao;

import java.util.List;

import com.zte.cms.storageManage.model.CmsStoragemanage;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ICmsStoragemanageDAO
{
    /**
     * 新增CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DAOException dao异常
     */
    public void insertCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException;

    /**
     * 根据主键更新CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DAOException dao异常
     */
    public void updateCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException;

    /**
     * 根据主键删除CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @throws DAOException dao异常
     */
    public void deleteCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException;

    /**
     * 根据主键查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return 满足条件的CmsStoragemanage对象
     * @throws DAOException dao异常
     */
    public CmsStoragemanage getCmsStoragemanage(CmsStoragemanage cmsStoragemanage) throws DAOException;

    /**
     * 根据条件查询CmsStoragemanage对象
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @return 满足条件的CmsStoragemanage对象集
     * @throws DAOException dao异常
     */
    public List<CmsStoragemanage> getCmsStoragemanageByCond(CmsStoragemanage cmsStoragemanage) throws DAOException;

    /**
     * 根据条件分页查询CmsStoragemanage对象，作为查询条件的参数 从设备的优先级得到空间的优先级
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQueryPriority(CmsStoragemanage cmsStoragemanage, int start, int pageSize)
            throws DAOException;

    /**
     * 根据条件分页查询CmsStoragemanage对象，作为查询条件的参数
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询CmsStoragemanage对象，作为查询条件的参数
     * 
     * @param cmsStoragemanage CmsStoragemanage对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(CmsStoragemanage cmsStoragemanage, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

    // 通过连表查询，使空间得到其所对应的设备的优先级
    public List<CmsStoragemanage> getCmsStoragemanageByCondPriority(CmsStoragemanage cmsStoragemanage)
            throws DAOException;

    // 查找绑定的空间中优先级最高，容量最大，且可用的空间
    public List<CmsStoragemanage> getCmsStoragemanageofbindedSpace(CmsStoragemanage cmsStoragemanage)
            throws DAOException;

    /*******************************************************************************************************************
     * 查询storageManageme中设备下面各存储区的信息
     ******************************************************************************************************************/
    public List<CmsStoragemanage> getCmsStoragemanageLists() throws DomainServiceException;

    public CmsStoragemanage getCmsStoragemanageList(String storagearea) throws DomainServiceException;
}
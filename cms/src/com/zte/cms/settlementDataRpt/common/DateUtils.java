package com.zte.cms.settlementDataRpt.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * ProjectName:
 *
 * @Author: Liang Xiaomin
 * @Date: Creating in 15:22 2019/5/6
 * @Description:
 */
public class DateUtils {

    public static final String fit_a = "yyyy-MM-dd HH:mm:ss";
    public static final String fit_yyyy_MM = "yyyy-MM";
    public static final String fit_yyyy_MM_DD = "yyyy-MM-dd";
    public static final String fit_HH_mm_ss = "HH:mm:ss";



    public static final String fit_HH = "HH";
    public static final String fit_yyyyMM = "yyyyMM";
    public static final String fit_yyyyMMDD = "yyyyMMDD";
    public static Date convert_fit_a(String dateStr){
        Date d = null;
        SimpleDateFormat format = new SimpleDateFormat(fit_a);
        try {
            d  = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    public static Date convert_fit_yyyyMM(String dateStr){
        Date d = null;
        SimpleDateFormat format = new SimpleDateFormat(fit_yyyyMM);
        try {
            d  = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     *  Date-Str
     * @param date
     * @param fit
     * @return
     */
    public static String convertDataToString_fit(Date date, String fit){
        String dateStr = "";
        SimpleDateFormat format = new SimpleDateFormat(fit);
        dateStr = format.format(date);
        return dateStr;
    }

    public static Date convert_strToDate(String dateStr,String fit){
        Date d = null;
        SimpleDateFormat format = new SimpleDateFormat(fit);
        try {
            d  = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 给定日期是当月的第几天
     */
    public static int getDayOfMonth(String date) {
        int day = Integer.parseInt(date.substring(6,8));
        return day;
    }

    /**
     * 根据日期 yyyyMMdd，获取天数
     */
    public static int getMaxDays(String date) {
        int year = Integer.parseInt(date.substring(0,4));
        int month = Integer.parseInt(date.substring(4,6));
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }


    /**
     * 计算两个给定日期相差天数.
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public static int daysBetween(String startDate,String endDate) throws Exception{
        Calendar cal = Calendar.getInstance();
        cal.setTime(convert_strToDate(startDate, fit_yyyy_MM_DD));
        long time1 = cal.getTimeInMillis();
        cal.setTime(convert_strToDate(endDate, fit_yyyy_MM_DD));
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    public static void main(String[] args) {

        String s = "cpspQualifications/";
        String substring = s.substring(0, s.indexOf("/"));
        System.out.println(substring);

    }

}

package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.ProgramPictureMap;
import com.zte.cms.picture.dao.IProgramPictureMapDAO;
import com.zte.cms.picture.service.IProgramPictureMapDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class ProgramPictureMapDS extends DynamicObjectBaseDS implements IProgramPictureMapDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IProgramPictureMapDAO dao = null;

    public void setDao(IProgramPictureMapDAO dao)
    {
        this.dao = dao;
    }

    public void insertProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException
    {
        log.debug("insert programPictureMap starting...");
        try
        {
            if (null == programPictureMap.getMapindex())
            {
                Long mapIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_program_picture_map_index");
                String mappingid = String.format("%02d", ObjectType.PROGRAMPICTURE_TYPE)+ String.format("%030d", mapIndex);
                programPictureMap.setMapindex(mapIndex);
                programPictureMap.setMappingid(mappingid);
            }
            dao.insertProgramPictureMap(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert programPictureMap end");
    }

    public void updateProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException
    {
        log.debug("update programPictureMap by pk starting...");
        try
        {
            dao.updateProgramPictureMap(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programPictureMap by pk end");
    }

    public void updateProgramPictureMapList(List<ProgramPictureMap> programPictureMapList)
            throws DomainServiceException
    {
        log.debug("update programPictureMapList by pk starting...");
        if (null == programPictureMapList || programPictureMapList.size() == 0)
        {
            log.debug("there is no datas in programPictureMapList");
            return;
        }
        try
        {
            for (ProgramPictureMap programPictureMap : programPictureMapList)
            {
                dao.updateProgramPictureMap(programPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update programPictureMapList by pk end");
    }

    public void removeProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException
    {
        log.debug("remove programPictureMap by pk starting...");
        try
        {
            dao.deleteProgramPictureMap(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programPictureMap by pk end");
    }

    public void removeProgramPictureMapList(List<ProgramPictureMap> programPictureMapList)
            throws DomainServiceException
    {
        log.debug("remove programPictureMapList by pk starting...");
        if (null == programPictureMapList || programPictureMapList.size() == 0)
        {
            log.debug("there is no datas in programPictureMapList");
            return;
        }
        try
        {
            for (ProgramPictureMap programPictureMap : programPictureMapList)
            {
                dao.deleteProgramPictureMap(programPictureMap);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove programPictureMapList by pk end");
    }

    public ProgramPictureMap getProgramPictureMap(ProgramPictureMap programPictureMap) throws DomainServiceException
    {
        log.debug("get programPictureMap by pk starting...");
        ProgramPictureMap rsObj = null;
        try
        {
            rsObj = dao.getProgramPictureMap(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programPictureMapList by pk end");
        return rsObj;
    }

    public List<ProgramPictureMap> getProgramPictureMapByCond(ProgramPictureMap programPictureMap)
            throws DomainServiceException
    {
        log.debug("get programPictureMap by condition starting...");
        List<ProgramPictureMap> rsList = null;
        try
        {
            rsList = dao.getProgramPictureMapByCond(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programPictureMap by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(programPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programPictureMap page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ProgramPictureMap programPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get programPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(programPictureMap, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programPictureMap page info by condition end");
        return tableInfo;
    }
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryproPicMap(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryproPicMap(programPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programPictureMap page info by condition end");
        return tableInfo;
    }
    
    
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryproPicTargetMap(ProgramPictureMap programPictureMap, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get programPictureMap page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryproPicTargetMap(programPictureMap, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramPictureMap>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get programPictureMap page info by condition end");
        return tableInfo;
    }    
    
    public List<ProgramPictureMap> getProgramnormalPictureMapByCond(ProgramPictureMap programPictureMap)
            throws DomainServiceException
    {
        log.debug("get programPictureMap by condition starting...");
        List<ProgramPictureMap> rsList = null;
        try
        {
            rsList = dao.getProgramnormalPictureMapByCond(programPictureMap);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get programPictureMap by condition end");
        return rsList;
    }    
}

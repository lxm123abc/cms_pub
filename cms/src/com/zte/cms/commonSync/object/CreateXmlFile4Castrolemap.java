package com.zte.cms.commonSync.object;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.cast.model.Castcdn;
import com.zte.cms.castrolemap.model.Castrolemap;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.commonSync.common.AbstractCreateXmlFile4Object;

public class CreateXmlFile4Castrolemap extends AbstractCreateXmlFile4Object
{

    public String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST";
        }
        else if(2 == syncType)
        {
            action = "UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        
        Date date = new Date();
        String fileName = "castrolemap_" + "20_" +date.getTime()+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "castrolemap" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        List<Castrolemap> castrolemapList = objectMap.get("castrolemapList");
        Castrolemap castrolemap = castrolemapList.get(0);
        List<Castcdn> castcodeList = objectMap.get("castcodeList");
        Castcdn cast = castcodeList.get(0);
        String fileFullName = absolutePath + fileName;
        
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element crObjectElement = null;
        Element crPertyElement = null;

        // 元素节点
        crObjectElement = objectsElement.addElement("Object");
        crObjectElement.addAttribute("ElementType", "CastRoleMap");
        crObjectElement.addAttribute("ID", castrolemap.getCastrolemapcode());
        crObjectElement.addAttribute("Code", castrolemap.getCastrolemapcode());
        crObjectElement.addAttribute("Action", action);

        writeElement(crObjectElement, crPertyElement, "CastRole",castrolemap.getCastrole(), false);
        writeElement(crObjectElement, crPertyElement, "CastID", cast.getCastcode(), false);        
        writeElement(crObjectElement, crPertyElement, "CastCode", cast.getCastcode(), false);  
        
        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
         return retPath;
    }

    public String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)
    {
        String action = null;
        // 获取发布Action类型
        if (1 == syncType)
        {
            action = "REGIST or UPDATE";
        }
        else if(2 == syncType)
        {
            action = "REGIST or UPDATE";
        }else  if (3 == syncType)
        {
            action = "DELETE";
        }
        else
        {
            return null;
        }
        
        Date date = new Date();
        String fileName = "castrolemap_epg_" + date.getTime()+ ".xml";
        String relativePath = tempAreaPath + File.separator + CNTSYNCXML
                + File.separator + getCurrentTime() + File.separator
                + "castrolemap" + File.separator ;    
        String absolutePath = mountPoint + relativePath;
        String retPath = GlobalConstants.filterSlashStr(relativePath + fileName);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        List<Castrolemap> castrolemapList = objectMap.get("castrolemapList");
        Castrolemap castrolemap = castrolemapList.get(0);
        String fileFullName = absolutePath + fileName;
        
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        Element objectsElement = adiElement.addElement("Objects");
        Element objectElement = null;
        Element propertyElement = null;
        // 必填属性
        objectElement = objectsElement.addElement("Object");
        objectElement.addAttribute("ElementType", "CastRoleMap");
        objectElement.addAttribute("RoleMapID", castrolemap.getCastrolemapid());
        objectElement.addAttribute("Action", action);

        // 必有的元素
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "CastRole");
        propertyElement.addCDATA(String.valueOf(castrolemap.getCastrole()));

        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", "CastID");
        propertyElement.addCDATA(castrolemap.getCastid());

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            retPath = null;
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    retPath = null;
                }
            }
        }

        return retPath;
    }
    
    //央视规范没有castrolemap
    public String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)
    {
        return "";
    }

}

package com.zte.cms.channel.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.cms.categorycdn.model.Categorycdn;
import com.zte.cms.categorycdn.service.ICategorycdnDS;
import com.zte.cms.channel.common.CmsChannelConstant;
import com.zte.cms.channel.model.CategoryChannelMap;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.service.ICategoryChannelMapDS;
import com.zte.cms.channel.service.ICmsChannelDS;
import com.zte.cms.cntSyncRecord.service.ICntSyncRecordDS;
import com.zte.cms.cntSyncTask.model.CntSyncTask;
import com.zte.cms.cntSyncTask.service.ICntSyncTaskDS;
import com.zte.cms.common.GlobalConstants;
import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.commonSync.common.StandardImpFactory;
import com.zte.cms.commonSync.common.StatusTranslator;
import com.zte.cms.content.batchUpload.ls.OperateInfo;
import com.zte.cms.content.batchUpload.ls.ReturnInfo;
import com.zte.cms.content.objectSynRecord.model.ObjectSyncRecord;
import com.zte.cms.content.objectSynRecord.service.IObjectSyncRecordDS;
import com.zte.cms.content.platformSyn.model.CntPlatformSync;
import com.zte.cms.content.platformSyn.service.ICntPlatformSyncDS;
import com.zte.cms.content.targetSyn.model.CntTargetSync;
import com.zte.cms.content.targetSyn.service.ICntTargetSyncDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.cms.storageArea.ls.ICmsStorageareaLS;
import com.zte.cms.targetSystem.model.Targetsystem;
import com.zte.cms.targetSystem.service.ITargetsystemDS;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.log.Logger;
import com.zte.umap.common.log.LoggerFactory;
import com.zte.umap.common.log.LoggerModel;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.sys.model.UsysConfig;
import com.zte.umap.sys.service.IUsysConfigDS;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class CategoryChannelMapLS extends com.zte.umap.common.dynamicobj.DynamicObjectBaseDS implements
        ICategoryChannelMapLS
{

    private Logger log = LoggerFactory.getLogger(LoggerModel.MODEL_CHANNEL, getClass());
    private ICategoryChannelMapDS categoryChannelMapDS;
    private ICmsChannelDS channelDS;
    private ICategorycdnDS categorycdnDS;

    private ICntSyncTaskDS cntsyncTaskDS;
    private IBatchObjectRecordDS batchObjectRecordDS;// 批处理
    private IUsysConfigDS usysConfigDS;
    private ITargetsystemDS targetSystemDS;
    private ICntSyncRecordDS cntSyncRecordDS;
    private ICntPlatformSyncDS cntPlatformSyncDS;
    private IObjectSyncRecordDS objectSyncRecordDS;
    private ICmsChannelLS cmschannells;
    private static final String TEMPAREA_ID = "1"; // 临时区ID
    private static final String TEMPAREA_ERRORSiGN = "1056020"; // 获取临时区目录失败
    private final static String CMSCASTMTYPE_CNTSYNCXML = "xmlsync";
    private final static String CMSCHANNELMTYPE_PROGRAMTYPE = "categoryChannelMap";

    private ICntTargetSyncDS cntTargetSyncDS;
    private static final String SUCCESS = "cmsprogramtype.success";
    private static final String PUBLISH_SUCCESS = "cmsprogramtype.publish.success";

    public ICmsChannelLS getCmschannells()
    {
        return cmschannells;
    }

    public void setCmschannells(ICmsChannelLS cmschannells)
    {
        this.cmschannells = cmschannells;
    }

    public IObjectSyncRecordDS getObjectSyncRecordDS()
    {
        return objectSyncRecordDS;
    }

    public void setObjectSyncRecordDS(IObjectSyncRecordDS objectSyncRecordDS)
    {
        this.objectSyncRecordDS = objectSyncRecordDS;
    }

    public ICntTargetSyncDS getCntTargetSyncDS()
    {
        return cntTargetSyncDS;
    }

    public void setCntTargetSyncDS(ICntTargetSyncDS cntTargetSyncDS)
    {
        this.cntTargetSyncDS = cntTargetSyncDS;
    }

    public ICntPlatformSyncDS getCntPlatformSyncDS()
    {
        return cntPlatformSyncDS;
    }

    public void setCntPlatformSyncDS(ICntPlatformSyncDS cntPlatformSyncDS)
    {
        this.cntPlatformSyncDS = cntPlatformSyncDS;
    }

    /** 列表展示频道信息 */
    public TableDataInfo pageInfo(CategoryChannelMap categoryChannelMap, int start, int pageSize)
    {
        log.debug("pageInfo CmsChannel start...");
        TableDataInfo dataInfo = null;
        try
        {
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(categoryChannelMap.getChannelid());
            channel = (CmsChannel) channelDS.getChannelByCond(channel).get(0);
            // 只有频道新增同步成功后才能进行栏目关联管理
            if (channel.getStatus() != 0)
            {
                return null;
            }
            dataInfo = categoryChannelMapDS.pageInfoQuery(categoryChannelMap, start, pageSize);
        }
        catch (DomainServiceException e)
        {
            log.error("pageInfo error");
        }
        log.debug("pageInfo CmsChannel end");
        return dataInfo;
    }

    public String insertCategoryChannel(CategoryChannelMap categoryChannelMap)
    {
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        try
        {
            // 判断频道是否存在
            CmsChannel channel = new CmsChannel();
            channel.setChannelindex(categoryChannelMap.getChannelindex());
            channel = channelDS.getCmsChannel(channel);
            if (channel == null)
            {// 频道不存在
                return "1:频道已不存在";
            }
            else
            {
                if (channel.getStatus() != 0)
                {
                    return "1：频道状态不正确";
                }
            }
            Categorycdn categorycdn = new Categorycdn();
            categorycdn.setCategoryindex(categoryChannelMap.getCategoryindex());
            categorycdn = categorycdnDS.getCategorycdn(categorycdn);
            if (categorycdn == null)
            {// 栏目不存在
                return "1:栏目不存在";
            }

            // 判断栏目是否存在
            categoryChannelMapDS.insertCategoryChannelMap(categoryChannelMap);

            // 关联平台数据
            // 增加频道关联栏目的mapping

            CntTargetSync channelctg = new CntTargetSync();
            channelctg.setObjecttype(5);
            channelctg.setObjectid(channel.getChannelid());
            List channelctglist = cntTargetSyncDS.getCntTargetSyncByCond(channelctg);

            CntTargetSync ctgsync = new CntTargetSync();
            ctgsync.setObjecttype(2);
            ctgsync.setObjectindex(categoryChannelMap.getCategoryindex());
            List synclist = cntTargetSyncDS.getCntTargetSyncByCond(ctgsync);

            for (int j = 0; j < channelctglist.size(); j++)
            {

                if (synclist != null && synclist.size() > 0)
                {
                    for (int kk = 0; kk < synclist.size(); kk++)
                    {
                        long targetindex = ((CntTargetSync) synclist.get(kk)).getTargetindex();
                        if (targetindex == ((CntTargetSync) channelctglist.get(j)).getTargetindex())
                        {// 栏目的关联平台同频道的一致
                            Targetsystem tar = new Targetsystem();
                            tar.setTargetindex(targetindex);
                            List<Targetsystem> targetlist = targetSystemDS.getTargetsystemByCond(tar);
                            cmschannells.preparetopublishForMap(targetlist.get(0), categoryChannelMap.getMapindex(),
                                    categoryChannelMap.getMappingid(), 28, categoryChannelMap.getChannelid(),
                                    categoryChannelMap.getCategoryid());
                        }
                    }
                }
            }

            CommonLogUtil
                    .insertOperatorLog(String.valueOf(categoryChannelMap.getChannelid() + ","
                            + categoryChannelMap.getCategoryid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_ADD,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_ADD_INFO,
                            CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
            return "0:操作成功";
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
            return "1:系统错误";
        }
    }

    public String deleteCategoryChannel(CategoryChannelMap categoryChannelMapList[])
    {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";

        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int index = 1;

        try
        {
            List synclist = new ArrayList();
            List sucList = new ArrayList();
            for (int i = 0; i < categoryChannelMapList.length; i++)
            {
                CategoryChannelMap categoryChannelMap = new CategoryChannelMap();
                categoryChannelMap.setMapindex(categoryChannelMapList[i].getMapindex());
                categoryChannelMap = categoryChannelMapDS.getCategoryChannelMap(categoryChannelMap);
                if (categoryChannelMap == null)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(categoryChannelMapList[i]
                            .getMapindex()), "操作失败", "关联栏目不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                else
                {
                    boolean flagbool = false;
                    CntTargetSync sync = new CntTargetSync();
                    sync.setObjectid(categoryChannelMap.getMappingid());
                    synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                    if (synclist != null && synclist.size() > 0)
                    {
                        for (int j = 0; j < synclist.size(); j++)
                        {
                            sync = (CntTargetSync) synclist.get(j);
                            int status = sync.getStatus();
                            if (status == 300 || status == 500)
                            {
                                flagbool = true;
                            }
                        }
                    }

                    if (flagbool == true)
                    {
                        ifail++;
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(categoryChannelMapList[i]
                                .getMapindex()), "操作失败", "当前状态不能删除");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                    else
                    {
                        sucList.add(categoryChannelMap);
                        iSuccessed++;
                        operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(categoryChannelMapList[i]
                                .getMapindex()), "操作成功", "删除频道栏目关联关系成功");
                        returnInfo.appendOperateInfo(operateInfo);
                        continue;
                    }
                }
            }

            for (Iterator iterator = sucList.iterator(); iterator.hasNext();)
            {
                CategoryChannelMap categoryChannelMap = (CategoryChannelMap) iterator.next();
                CntTargetSync sync = new CntTargetSync();
                sync.setObjectid(categoryChannelMap.getMappingid());
                synclist = cntTargetSyncDS.getCntTargetSyncByCond(sync);
                categoryChannelMapDS.removeCategoryChannelMap(categoryChannelMap);
                if (synclist != null && synclist.size() > 0)
                {
                    cntTargetSyncDS.removeCntTargetSyncList(synclist);
                }
                CntPlatformSync syncplt = new CntPlatformSync();
                syncplt.setObjectid(categoryChannelMap.getMappingid());
                List syncpltlist = cntPlatformSyncDS.getCntPlatformSyncByCond(syncplt);
                if (syncpltlist != null && syncpltlist.size() > 0)
                {
                    cntPlatformSyncDS.removeCntPlatformSyncList(syncpltlist);
                }
                CommonLogUtil.insertOperatorLog(String.valueOf(categoryChannelMap.getMapindex()),
                        CmsChannelConstant.MGTTYPE_CHANNELCONTENT, CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_DEL,
                        CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_DEL_INFO,
                        CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);

            }
        }
        catch (DomainServiceException e)
        {
            e.printStackTrace();
        }
        if (categoryChannelMapList.length == 0 || ifail == categoryChannelMapList.length)
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < categoryChannelMapList.length)
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    public HashMap publishCategoryChannelTar(List<CntTargetSync> tarlist)
    {
        log.debug("publishChannelcontent start ");
        HashMap mapfinal = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 0;// 发布操作
        int index = 1;

        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                mapfinal.put("msg", "1:获取挂载点失败");
                return mapfinal;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                mapfinal.put("msg", "1:获取临时区失败");
                return mapfinal;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                mapfinal.put("msg", "1:获取发布XML文件FTP地址失败");
                return mapfinal;
            }
            // 先判断频道，栏目是否发布

            String channelid = tarlist.get(0).getElementid();// 频道id
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(channelid);
            List channellist = channelDS.getChannelByCond(channel);
            channel = (CmsChannel) channellist.get(0);

            String categoryid = tarlist.get(0).getParentid();// 栏目id
            Categorycdn cdn = new Categorycdn();
            cdn.setCategoryid(categoryid);
            List categorylist = categorycdnDS.selectByCategoryindex(cdn);
            cdn = (Categorycdn) categorylist.get(0);

            Map<String, List> objMap = new HashMap();
            CategoryChannelMap map = new CategoryChannelMap();
            map.setCategoryid(categoryid);
            map.setChannelid(channelid);

            List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
            if (listmap != null && listmap.size() > 0)
            {
                map = (CategoryChannelMap) listmap.get(0);
                map.setCategorycode(cdn.getCatagorycode());
                map.setCpcontentid(channel.getCpcontentid());
                objMap.put("categorychannelmaplist", listmap);
            }
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setSyncindex(tarlist.get(0).getRelateindex());
            cntPlatformSync = (CntPlatformSync) cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync).get(0);
            if (tarlist != null && tarlist.size() > 0)
            {
                Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                        .toString());
                for (int j = 0; j < tarlist.size(); j++)
                {

                    CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                    List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                    {

                    }
                    else
                    {
                        mapfinal.put("msg", "1:发布数据不存在");

                        return mapfinal;
                    }
                    int status = tarsync.getStatus();

                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        mapfinal.put("msg", "1:目标系统状态不正确");

                        return mapfinal;
                    }
                    long targetindex = tarsync.getTargetindex();
                    CntTargetSync sync = new CntTargetSync();
                    sync.setObjecttype(5);// 频道
                    sync.setTargetindex(targetindex);
                    sync.setObjectid(channelid);
                    List list = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                    CntTargetSync synccat = new CntTargetSync();
                    synccat.setObjecttype(2);// 栏目
                    synccat.setTargetindex(targetindex);
                    synccat.setObjectid(categoryid);
                    List listcat = cntTargetSyncDS.getCntTargetSyncByCond(synccat);

                    if (list != null && list.size() > 0 && listcat != null && listcat.size() > 0)
                    {
                        sync = (CntTargetSync) list.get(0);
                        synccat = (CntTargetSync) listcat.get(0);
                        if ((sync.getStatus() == 300 || sync.getStatus() == 500)
                                && (synccat.getStatus() == 300 || synccat.getStatus() == 500))
                        {

                            if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                            {
                                actiontype = 1;// 新增发布
                                tarsync.setOperresult(10);// 新增同步中
                            }
                            else if (tarsync.getStatus() == 500)
                            {
                                actiontype = 2;// 修改发布
                                tarsync.setOperresult(40);// 修改同步中
                            }
                            else
                            {
                                // ifail++;
                                // operateInfo = new OperateInfo(String
                                // .valueOf(index++), tarsync
                                // .getSyncindex().toString(), "操作失败",
                                // "当前状态不能发布");
                                // returnInfo.appendOperateInfo(operateInfo);
                                mapfinal.put("msg", "1:发布数据状态不正确");
                                continue;// 中断进入下一个循环
                            }
                        }
                        else
                        {
                            // ifail++;
                            // operateInfo = new OperateInfo(String
                            // .valueOf(index++), sync.getSyncindex()
                            // .toString(), "操作失败", "频道或栏目尚未发布");
                            // returnInfo.appendOperateInfo(operateInfo);
                            mapfinal.put("msg", "1:频道或栏目尚未发布");
                            continue;// 中断进入下一个循环
                        }
                    }
                    else
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String
                        // .valueOf(index++), channel.getChannelid()
                        // , "操作失败", "频道或栏目尚未发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        mapfinal.put("msg", "1:频道或栏目尚未发布");
                        continue;// 中断进入下一个循环
                    }
                    // 结束对频道及栏目在网元上的发布状态的判断
                    String xmladdress = "";
                    List maplist = new ArrayList();

                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {

                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                    }

                    insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(), 28,
                            actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn.getCatagorycode(), channel
                                    .getChannelid(), batchid, 1);// 调用同步任务模块DS代码

                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    // iSuccessed++;
                    //
                    // operateInfo = new OperateInfo(String.valueOf(index++),
                    // tarsync.getSyncindex().toString(), ResourceMgt
                    // .findDefaultText(SUCCESS), ResourceMgt
                    // .findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    mapfinal.put("msg", "0:操作成功");

                    successlist.add(map);
                    CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                            + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH_INFO,
                            CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (successlist.size() == 0 || ifail == successlist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < successlist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return mapfinal;
    }

    public HashMap cancelpublishCategoryChannelTar(List<CntTargetSync> tarlist)
    {
        log.debug("publishChannelcontent start ");
        HashMap mapfinal = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 3;// 发布操作
        int index = 1;

        try
        {
            CntPlatformSync cntPlatformSync = new CntPlatformSync();
            cntPlatformSync.setSyncindex(tarlist.get(0).getRelateindex());
            cntPlatformSync = (CntPlatformSync) cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync).get(0);
            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                mapfinal.put("msg", "1:获取挂载点失败");
                return mapfinal;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                mapfinal.put("msg", "1:获取临时区失败");
                return mapfinal;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                mapfinal.put("msg", "1:获取发布XML文件FTP地址失败");
                return mapfinal;
            }
            // 先判断频道，栏目是否发布

            String channelid = tarlist.get(0).getElementid();// 频道id
            CmsChannel channel = new CmsChannel();
            channel.setChannelid(channelid);
            List channellist = channelDS.getChannelByCond(channel);
            channel = (CmsChannel) channellist.get(0);

            String categoryid = tarlist.get(0).getParentid();// 栏目id
            Categorycdn cdn = new Categorycdn();
            cdn.setCategoryid(categoryid);
            List categorylist = categorycdnDS.selectByCategoryindex(cdn);
            cdn = (Categorycdn) categorylist.get(0);

            if (tarlist != null && tarlist.size() > 0)
            {
                for (int j = 0; j < tarlist.size(); j++)
                {
                    Map<String, List> objMap = new HashMap();
                    CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                    List tarsynclistinit = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                    if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                    {

                    }
                    else
                    {
                        mapfinal.put("msg", "1:发布数据不存在");

                        return mapfinal;
                    }
                    int statusbegin = tarsync.getStatus();
                    Targetsystem targetSystem = new Targetsystem();
                    targetSystem.setTargetindex(tarsync.getTargetindex());
                    List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                    if (targetlist != null && targetlist.size() > 0)
                    {
                        targetSystem = (Targetsystem) targetlist.get(0);
                    }
                    if (targetSystem.getStatus() != 0)
                    {
                        mapfinal.put("msg", "1:目标系统状态不正确");

                        return mapfinal;
                    }
                    long targetindex = tarsync.getTargetindex();

                    if (tarsync.getStatus() != 300 && tarsync.getStatus() != 500)
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String.valueOf(index++),
                        // String.valueOf(tarsync.getObjectid()), "操作失败",
                        // "当前状态不能取消发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        mapfinal.put("msg", "1:发布数据状态不正确");
                        continue;
                    }

                    // 结束对频道及栏目在网元上的发布状态的判断
                    String xmladdress = "";
                    List maplist = new ArrayList();
                    CategoryChannelMap map = new CategoryChannelMap();
                    map.setCategoryid(categoryid);
                    map.setChannelid(channelid);
                    List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
                    if (listmap != null && listmap.size() > 0)
                    {
                        map = (CategoryChannelMap) listmap.get(0);
                        map.setCategorycode(cdn.getCatagorycode());
                        map.setCpcontentid(channel.getCpcontentid());
                        objMap.put("categorychannelmaplist", listmap);
                    }
                    if (cntPlatformSync.getPlatform() == 2)
                    {// 3.0平台,1个平台生成1个xml
                        xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                    }
                    else
                    {

                        xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                    }
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(), 28,
                            actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn.getCatagorycode(), channel
                                    .getChannelid(), batchid, 1);// 调用同步任务模块DS代码

                    tarsync.setOperresult(70);
                    tarsync.setStatus(200);// 200发布中
                    cntTargetSyncDS.updateCntTargetSync(tarsync);
                    // iSuccessed++;
                    //
                    // operateInfo = new OperateInfo(String.valueOf(index++),
                    // tarsync.getSyncindex().toString(), ResourceMgt
                    // .findDefaultText(SUCCESS), ResourceMgt
                    // .findDefaultText(PUBLISH_SUCCESS));
                    // returnInfo.appendOperateInfo(operateInfo);
                    mapfinal.put("msg", "0:操作成功");

                    successlist.add(map);
                    CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                            + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH,
                            CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH_INFO,
                            CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                }
                CntTargetSync finaltarget = new CntTargetSync();
                finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                if (finaltargetlist != null && finaltargetlist.size() > 0)
                {
                    int[] targetSyncStatus = new int[finaltargetlist.size()];
                    for (int ii = 0; ii < finaltargetlist.size(); ii++)
                    {
                        targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                    }
                    int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                    int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                    if (originalstatus != statussync)
                    {
                        cntPlatformSync.setStatus(statussync);
                        cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                    }
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (successlist.size() == 0 || ifail == successlist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < successlist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return mapfinal;
    }

    // 单个取消发布平台
    public HashMap cancelpublishCategoryChannelSingle(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");
        HashMap mapinit = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 3;// 发布操作
        int index = 1;

        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                mapinit.put("msg", "1:获取挂载点失败");
                return mapinit;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                mapinit.put("msg", "1:获取临时区失败");
                return mapinit;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                mapinit.put("msg", "1:获取发布XML文件FTP地址失败");
                return mapinit;
            }
            boolean flagbool = false;
            String msg = "";
            // 先判断频道，栏目是否发布
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    mapinit.put("msg", "1:发布数据不存在");

                    return mapinit;
                }
                int statusinit = cntPlatformSync.getStatus();
                if (statusinit == 0 || statusinit == 200 || statusinit == 400)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), String
                            .valueOf(cntPlatformSync.getObjectid()), "失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                String channelid = cntPlatformSync.getElementid();// 频道id
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(channelid);
                List channellist = channelDS.getChannelByCond(channel);
                channel = (CmsChannel) channellist.get(0);

                String categoryid = cntPlatformSync.getParentid();// 栏目id
                Categorycdn cdn = new Categorycdn();
                cdn.setCategoryid(categoryid);
                List categorylist = categorycdnDS.selectByCategoryindex(cdn);
                cdn = (Categorycdn) categorylist.get(0);
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());// 得到这个平台下的关联了的网元
                tarsync.setObjecttype(28);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    for (int j = 0; j < tarlist.size(); j++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {

                        }
                        else
                        {
                            msg = msg + " 发布数据不存在";

                            continue;
                        }
                        int status = tarsync.getStatus();

                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                            continue;
                        }
                        long targetindex = tarsync.getTargetindex();

                        if (tarsync.getStatus() != 300 && tarsync.getStatus() != 500)
                        {
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), String
                            // .valueOf(tarsync.getObjectid()), "操作失败", "当前状态不能取消发布");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                            continue;
                        }

                        // 结束对频道及栏目在网元上的发布状态的判断
                        String xmladdress = "";
                        List maplist = new ArrayList();
                        CategoryChannelMap map = new CategoryChannelMap();
                        map.setCategoryid(categoryid);
                        map.setChannelid(channelid);
                        List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
                        if (listmap != null && listmap.size() > 0)
                        {
                            map = (CategoryChannelMap) listmap.get(0);
                            map.setCategorycode(cdn.getCatagorycode());
                            map.setCpcontentid(channel.getCpcontentid());
                            objMap.put("categorychannelmaplist", listmap);
                        }
                        if (cntPlatformSync.getPlatform() == 2)
                        {// 3.0平台,1个平台生成1个xml
                            xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                        }
                        else
                        {
                          //如果是2.0平台就每个网元生成一个不同的batchid
                            batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                            xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                        }
                        int pristatus = 1;
                        // bms和egp的情况且有别的网元的情况,status可为0或1
                        if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                                && (tarlist.size() > 0))
                        {
                            for (int count = 0; count < tarlist.size(); count++)
                            {
                                CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                                {
                                    Targetsystem tsnew = new Targetsystem();
                                    tsnew.setTargetindex(cs.getTargetindex());
                                    List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                    if (target != null && target.size() > 0)
                                    {
                                        tsnew = (Targetsystem) target.get(0);
                                    }
                                    if ((tsnew.getTargettype() == 2))
                                    {// cdn
                                        pristatus = 0;
                                    }
                                    else if (tsnew.getTargettype() == 1)
                                    {// bms
                                        pristatus = 0;
                                    }

                                }
                            }
                        }
                        insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(), 28,
                                actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn.getCatagorycode(),
                                channel.getChannelid(), batchid, pristatus);// 调用同步任务模块DS代码

                        tarsync.setOperresult(70);
                        tarsync.setStatus(200);// 200发布中
                        cntTargetSyncDS.updateCntTargetSync(tarsync);
                        flagbool = true;
                        // iSuccessed++;
                        //
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                        // returnInfo.appendOperateInfo(operateInfo);
                        msg = msg + " 从目标系统" + targetSystem.getTargetid() + "取消发布:操作成功";

                        successlist.add(map);
                        CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                                + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH_INFO,
                                CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }
                if (flagbool == true)
                {
                    msg = "0:" + msg;
                    mapinit.put("msg", msg);
                }
                else
                {
                    msg = "1:" + msg;
                    mapinit.put("msg", msg);
                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        log.debug("canclePuhlishProgramtype end...");
        return mapinit;
    }

    // 平台批量取消发布
    public String cancelpublishCategoryChannel(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 3;// 发布操作
        int index = 1;

        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                return "1:获取挂载点失败";
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                return "1:获取临时区失败";
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1:获取发布XML文件FTP地址失败";
            }
            boolean flagbool = false;

            // 先判断频道，栏目是否发布
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                String msg = "";
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);

                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), String
                            .valueOf(cntPlatformSync.getObjectid()), "失败", "发布数据不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }

                int statusinit = cntPlatformSync.getStatus();
                if (statusinit == 0 || statusinit == 200 || statusinit == 400)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), String
                            .valueOf(cntPlatformSync.getObjectid()), "失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;
                }
                String channelid = cntPlatformSync.getElementid();// 频道id
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(channelid);
                List channellist = channelDS.getChannelByCond(channel);
                channel = (CmsChannel) channellist.get(0);

                String categoryid = cntPlatformSync.getParentid();// 栏目id
                Categorycdn cdn = new Categorycdn();
                cdn.setCategoryid(categoryid);
                List categorylist = categorycdnDS.selectByCategoryindex(cdn);
                cdn = (Categorycdn) categorylist.get(0);
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());// 得到这个平台下的关联了的网元
                tarsync.setObjecttype(28);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    for (int j = 0; j < tarlist.size(); j++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {

                        }
                        else
                        {
                            msg = msg + " 发布数据不存在";

                            continue;
                        }
                        int status = tarsync.getStatus();

                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                            continue;
                        }
                        long targetindex = tarsync.getTargetindex();

                        if (tarsync.getStatus() != 300 && tarsync.getStatus() != 500)
                        {
                            // ifail++;
                            // operateInfo = new OperateInfo(String.valueOf(index++), String
                            // .valueOf(tarsync.getObjectid()), "操作失败", "当前状态不能取消发布");
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 目标系统" + targetSystem.getTargetid() + "发布数据状态不正确";
                            continue;
                        }

                        // 结束对频道及栏目在网元上的发布状态的判断
                        String xmladdress = "";
                        List maplist = new ArrayList();
                        CategoryChannelMap map = new CategoryChannelMap();
                        map.setCategoryid(categoryid);
                        map.setChannelid(channelid);
                        List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
                        if (listmap != null && listmap.size() > 0)
                        {
                            map = (CategoryChannelMap) listmap.get(0);
                            map.setCategorycode(cdn.getCatagorycode());
                            map.setCpcontentid(channel.getCpcontentid());
                            objMap.put("categorychannelmaplist", listmap);
                        }
                        if (cntPlatformSync.getPlatform() == 2)
                        {// 3.0平台,1个平台生成1个xml
                            xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                        }
                        else
                        {
                          //如果是2.0平台就每个网元生成一个不同的batchid
                            batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                            xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                        }
                        int pristatus = 1;
                        // bms和egp的情况且有别的网元的情况,status可为0或1
                        if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 3))
                                && (tarlist.size() > 0))
                        {
                            for (int count = 0; count < tarlist.size(); count++)
                            {
                                CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                if (!cs.getTargetindex().equals(targetSystem.getTargetindex()))
                                {
                                    Targetsystem tsnew = new Targetsystem();
                                    tsnew.setTargetindex(cs.getTargetindex());
                                    List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                    if (target != null && target.size() > 0)
                                    {
                                        tsnew = (Targetsystem) target.get(0);
                                    }
                                    if ((tsnew.getTargettype() == 2))
                                    {// cdn
                                        pristatus = 0;
                                    }
                                    else if (tsnew.getTargettype() == 1)
                                    {// bms
                                        pristatus = 0;
                                    }

                                }
                            }
                        }
                        insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(), 28,
                                actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn.getCatagorycode(),
                                channel.getChannelid(), batchid, pristatus);// 调用同步任务模块DS代码

                        tarsync.setOperresult(70);
                        tarsync.setStatus(200);// 200发布中
                        cntTargetSyncDS.updateCntTargetSync(tarsync);
                        flagbool = true;
                        // iSuccessed++;
                        //
                        // operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(),
                        // ResourceMgt.findDefaultText(SUCCESS), ResourceMgt.findDefaultText(PUBLISH_SUCCESS));
                        // returnInfo.appendOperateInfo(operateInfo);
                        msg = msg + " 发送到目标系统" + targetSystem.getTargetid() + ":操作成功";

                        successlist.add(map);
                        CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                                + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_CANCLEPUBLISH_INFO,
                                CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);
                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }
                if (flagbool == true)
                {
                    iSuccessed++;

                    operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getSyncindex().toString(), "成功", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), String.valueOf(tarsync.getObjectid()), "失败",
                            msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    /** 新增同步单个 */
    public HashMap publishCategoryChannelSingle(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");
        HashMap mapinit = new HashMap();
        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 0;// 发布操作
        int index = 1;

        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                mapinit.put("msg", "1:获取挂载点失败");
                return mapinit;
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                mapinit.put("msg", "1:获取临时区失败");
                return mapinit;
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                mapinit.put("msg", "1:获取发布XML文件FTP地址失败");
                return mapinit;
            }
            boolean flagbool = false;
            String msg = "";
            // 先判断频道，栏目是否发布
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    mapinit.put("msg", "1:发布数据不存在");

                    return mapinit;
                }
                int statusinit = cntPlatformSync.getStatus();
                if (statusinit == 300 || statusinit == 200)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }

                String channelid = cntPlatformSync.getElementid();// 频道id
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(channelid);
                List channellist = channelDS.getChannelByCond(channel);
                channel = (CmsChannel) channellist.get(0);

                String categoryid = cntPlatformSync.getParentid();// 栏目id
                Categorycdn cdn = new Categorycdn();
                cdn.setCategoryid(categoryid);
                List cdnlist = categorycdnDS.selectByCategoryindex(cdn);
                cdn = (Categorycdn) cdnlist.get(0);
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());// 得到这个平台下的关联了的网元
                tarsync.setObjecttype(28);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);

                int pritargettype = -1;
                if (tarlist != null && tarlist.size() > 0)
                {
                    
                    for (int count = 0; count < tarlist.size(); count++)
                    {
                        // 找出优先级最高的网元

                        CntTargetSync cntTargetSync = (CntTargetSync) tarlist.get(count);
                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(cntTargetSync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getTargettype() == 3)
                        {
                            pritargettype = 3;
                        }
                        else if ((targetSystem.getTargettype() == 1) && (pritargettype != 3))
                        {
                            pritargettype = 1;
                        }
                        else if ((targetSystem.getTargettype() != 1) && (pritargettype != 3) && (pritargettype != 1))
                        {
                            pritargettype = targetSystem.getTargettype();
                        }
                    }

                    boolean priflagbool = false;
                    for (int sed = 0; sed < tarlist.size(); sed++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(sed);// 得到网元信息,判断频道，栏目是否发布

                        int status = tarsync.getStatus();

                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }

                        if (targetSystem.getTargettype() == pritargettype)
                        {

                            if (targetSystem.getStatus() != 0)
                            {
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                tarlist.remove(sed);
                                continue;
                            }
                            long targetindex = tarsync.getTargetindex();
                            CntTargetSync sync = new CntTargetSync();
                            sync.setObjecttype(5);// 频道
                            sync.setTargetindex(targetindex);
                            sync.setObjectid(channelid);
                            List list = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                            CntTargetSync synccat = new CntTargetSync();
                            synccat.setObjecttype(2);// 栏目
                            synccat.setTargetindex(targetindex);
                            synccat.setObjectid(categoryid);
                            List listcat = cntTargetSyncDS.getCntTargetSyncByCond(synccat);

                            if (list != null && list.size() > 0 && listcat != null && listcat.size() > 0)
                            {
                                sync = (CntTargetSync) list.get(0);
                                synccat = (CntTargetSync) listcat.get(0);
                                if ((sync.getStatus() == 300 || sync.getStatus() == 500)
                                        && (synccat.getStatus() == 300 || synccat.getStatus() == 500))
                                {

                                    if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                                    {
                                       
                                    }
                                    else if (tarsync.getStatus() == 500)
                                    {
                                        
                                    }
                                    else
                                    {
                                        // ifail++;
                                        // operateInfo = new OperateInfo(String
                                        // .valueOf(index++), tarsync
                                        // .getSyncindex().toString(), "操作失败",
                                        // "当前状态不能发布");
                                        // returnInfo.appendOperateInfo(operateInfo);
//                                        msg = msg + " 目标系统" + targetSystem.getTargetid() + ":发布数据状态不正确";
//                                        continue;// 中断进入下一个循环
                                        tarlist.remove(sed);
                                    }
                                }
                                else
                                {
                                    // ifail++;
                                    // operateInfo = new OperateInfo(String
                                    // .valueOf(index++), sync.getSyncindex()
                                    // .toString(), "操作失败", "频道或栏目尚未发布");
                                    // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目尚未发布";
                                    tarlist.remove(sed);
                                    continue;// 中断进入下一个循环
                                }
                            }
                            else
                            {
                                msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目没有发布数据";
                                tarlist.remove(sed);
                                continue;// 中断进入下一个循环
                            }
                            priflagbool = true;
                        }
                    }

                    if (priflagbool == true)
                    {

                        Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                                .toString());
                        for (int j = 0; j < tarlist.size(); j++)
                        {
                            Map<String, List> objMap = new HashMap();
                            tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                            List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                            if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                            {

                            }
                            else
                            {
                                msg = msg + " 发布数据不存在";
                                
                                continue;
                            }
                            int status = tarsync.getStatus();

                            Targetsystem targetSystem = new Targetsystem();
                            targetSystem.setTargetindex(tarsync.getTargetindex());
                            List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                            if (targetlist != null && targetlist.size() > 0)
                            {
                                targetSystem = (Targetsystem) targetlist.get(0);
                            }
                            if (targetSystem.getStatus() != 0)
                            {
                                msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                                
                                continue;
                            }
                            long targetindex = tarsync.getTargetindex();
                            CntTargetSync sync = new CntTargetSync();
                            sync.setObjecttype(5);// 频道
                            sync.setTargetindex(targetindex);
                            sync.setObjectid(channelid);
                            List list = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                            CntTargetSync synccat = new CntTargetSync();
                            synccat.setObjecttype(2);// 栏目
                            synccat.setTargetindex(targetindex);
                            synccat.setObjectid(categoryid);
                            List listcat = cntTargetSyncDS.getCntTargetSyncByCond(synccat);

                            if (list != null && list.size() > 0 && listcat != null && listcat.size() > 0)
                            {
                                sync = (CntTargetSync) list.get(0);
                                synccat = (CntTargetSync) listcat.get(0);
                                if ((sync.getStatus() == 300 || sync.getStatus() == 500)
                                        && (synccat.getStatus() == 300 || synccat.getStatus() == 500))
                                {

                                    if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                                    {
                                        actiontype = 1;// 新增发布
                                        tarsync.setOperresult(10);// 新增同步中
                                    }
                                    else if (tarsync.getStatus() == 500)
                                    {
                                        actiontype = 2;// 修改发布
                                        tarsync.setOperresult(40);// 修改同步中
                                    }
                                    else
                                    {
                                        // ifail++;
                                        // operateInfo = new OperateInfo(String
                                        // .valueOf(index++), tarsync
                                        // .getSyncindex().toString(), "操作失败",
                                        // "当前状态不能发布");
                                        // returnInfo.appendOperateInfo(operateInfo);
                                        msg = msg + " 目标系统" + targetSystem.getTargetid() + ":发布数据状态不正确";
                                        
                                        continue;// 中断进入下一个循环
                                    }
                                }
                                else
                                {
                                    // ifail++;
                                    // operateInfo = new OperateInfo(String
                                    // .valueOf(index++), sync.getSyncindex()
                                    // .toString(), "操作失败", "频道或栏目尚未发布");
                                    // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目尚未发布";
                                    
                                    continue;// 中断进入下一个循环
                                }
                            }
                            else
                            {
                                msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目没有发布数据";
                                
                                continue;// 中断进入下一个循环
                            }
                            // 结束对频道及栏目在网元上的发布状态的判断
                            String xmladdress = "";
                            List maplist = new ArrayList();
                            CategoryChannelMap map = new CategoryChannelMap();
                            map.setCategoryid(categoryid);
                            map.setChannelid(channelid);
                            List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
                            if (listmap != null && listmap.size() > 0)
                            {
                                map = (CategoryChannelMap) listmap.get(0);
                                map.setCategorycode(cdn.getCatagorycode());
                                map.setCpcontentid(channel.getCpcontentid());
                                objMap.put("categorychannelmaplist", listmap);
                            }
                            if (cntPlatformSync.getPlatform() == 2)
                            {// 3.0平台,1个平台生成1个xml
                                xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                            }
                            else
                            {
                              //如果是2.0平台就每个网元生成一个不同的batchid
                                batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                                xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                        targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                            }
                            int pristatus = 1;
                            // bms和egp的情况且有别的网元的情况,status可为0或1
                            if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                                    && (tarlist.size() > 0))
                            {
                                for (int count = 0; count < tarlist.size(); count++)
                                {
                                    CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                    if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                    {
                                        Targetsystem tsnew = new Targetsystem();
                                        tsnew.setTargetindex(cs.getTargetindex());
                                        List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                        if (target != null && target.size() > 0)
                                        {
                                            tsnew = (Targetsystem) target.get(0);
                                        }
                                        if ((tsnew.getTargettype() == 3))
                                        {// cdn
                                            pristatus = 0;
                                        }
                                        else if (tsnew.getTargettype() == 1)
                                        {// bms
                                            pristatus = 0;
                                        }

                                    }
                                }
                            }
                            insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(),
                                    28, actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn
                                            .getCatagorycode(), channel.getChannelid(), batchid, pristatus);// 调用同步任务模块DS代码

                            tarsync.setStatus(200);// 200发布中
                            cntTargetSyncDS.updateCntTargetSync(tarsync);
                            flagbool = true;
                            // iSuccessed++;
                            //
                            // operateInfo = new OperateInfo(String.valueOf(index++),
                            // tarsync.getSyncindex().toString(), ResourceMgt
                            // .findDefaultText(SUCCESS), ResourceMgt
                            // .findDefaultText(PUBLISH_SUCCESS));
                            // returnInfo.appendOperateInfo(operateInfo);
                            msg = msg + " 发送到目标系统" + targetSystem.getTargetid() + ":操作成功";
                            successlist.add(map);
                            CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                                    + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                    CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH,
                                    CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH_INFO,
                                    CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);

                        }
                        CntTargetSync finaltarget = new CntTargetSync();
                        finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                        List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                        if (finaltargetlist != null && finaltargetlist.size() > 0)
                        {
                            int[] targetSyncStatus = new int[finaltargetlist.size()];
                            for (int ii = 0; ii < finaltargetlist.size(); ii++)
                            {
                                targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                            }
                            int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                            int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                            if (originalstatus != statussync)
                            {
                                cntPlatformSync.setStatus(statussync);
                                cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                            }
                        }
                    }
                }

                if (flagbool == true)
                {
                    msg = "0:" + msg;
                    mapinit.put("msg", msg);
                }
                else
                {
                    msg = "1:" + msg;
                    mapinit.put("msg", msg);

                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        log.debug("canclePuhlishProgramtype end...");
        return mapinit;
    }

    public List checkCanPublicList(CntPlatformSync cntPlatformSync,List tarlist){
        ArrayList array = new ArrayList();
        for (int j = 0; j < tarlist.size(); j++)
        {
            Map<String, List> objMap = new HashMap();
            CntTargetSync tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
            List tarsynclistinittar = null;
            try
            {
                tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
            }
            catch (DomainServiceException e2)
            {
                // TODO Auto-generated catch block
                e2.printStackTrace();
            }
            if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
            {

            }
            else
            {
                continue;
            }
            int status = tarsync.getStatus();

            Targetsystem targetSystem = new Targetsystem();
            targetSystem.setTargetindex(tarsync.getTargetindex());
            List targetlist = null;
            try
            {
                targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
            }
            catch (DomainServiceException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            if (targetlist != null && targetlist.size() > 0)
            {
                targetSystem = (Targetsystem) targetlist.get(0);
            }
            if (targetSystem.getStatus() != 0)
            {
                continue;
            }
            long targetindex = tarsync.getTargetindex();
            CntTargetSync sync = new CntTargetSync();
            sync.setObjecttype(5);// 频道
            sync.setTargetindex(targetindex);
            sync.setObjectid(cntPlatformSync.getElementid());
            List list = null;
            try
            {
                list = cntTargetSyncDS.getCntTargetSyncByCond(sync);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            CntTargetSync synccat = new CntTargetSync();
            synccat.setObjecttype(2);// 栏目
            synccat.setTargetindex(targetindex);
            synccat.setObjectid(cntPlatformSync.getParentid());
            List listcat = null;
            try
            {
                listcat = cntTargetSyncDS.getCntTargetSyncByCond(synccat);
            }
            catch (DomainServiceException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (list != null && list.size() > 0 && listcat != null && listcat.size() > 0)
            {
                sync = (CntTargetSync) list.get(0);
                synccat = (CntTargetSync) listcat.get(0);
                if ((sync.getStatus() == 300 || sync.getStatus() == 500)
                        && (synccat.getStatus() == 300 || synccat.getStatus() == 500))
                {

                    if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                    {
                        
                    }
                    else if (tarsync.getStatus() == 500)
                    {
                        
                    }
                    else
                    {
                        // ifail++;
                        // operateInfo = new OperateInfo(String
                        // .valueOf(index++), tarsync
                        // .getSyncindex().toString(), "操作失败",
                        // "当前状态不能发布");
                        // returnInfo.appendOperateInfo(operateInfo);
                        continue;// 中断进入下一个循环
                    }
                }
                else
                {
                    // ifail++;
                    // operateInfo = new OperateInfo(String
                    // .valueOf(index++), sync.getSyncindex()
                    // .toString(), "操作失败", "频道或栏目尚未发布");
                    // returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
            }
            else
            {
                continue;// 中断进入下一个循环
            }
            array.add(tarsync);
        }
        return array;
    }
    
    /** 新增同步 */
    public String publishCategoryChannel(List<CntPlatformSync> cntPlatformSynclist)
    {
        log.debug("publishChannelcontent start ");

        ReturnInfo returnInfo = new ReturnInfo();
        OperateInfo operateInfo = null;
        String flag = "";
        String errorcode = "";
        List successlist = new ArrayList();
        ResourceMgt.addDefaultResourceBundle(CommonLogConstant.RESOURCE_CMS_CONTENT_LOG);
        Integer ifail = 0;
        Integer iSuccessed = 0;
        int actiontype = 0;// 发布操作
        int index = 1;

        try
        {

            // 获取挂载点
            String mountPoint = GlobalConstants.getMountPoint();
            if (mountPoint == null || mountPoint.trim().equals(""))
            {
                return "1:获取挂载点失败";
            }

            // 获取临时区绑定空间的路径
            String tempAreaPath = getTempAreaPath();// 获取临时区绑定空间的路径
            if ("".equals(tempAreaPath))
            {
                return "1:获取临时区失败";
            }

            String synXMLFTPAdd = getSynXMLFTPAddr();
            if (synXMLFTPAdd == null)
            {
                return "1:获取发布XML文件FTP地址失败";
            }
            boolean flagbool = false;

            // 先判断频道，栏目是否发布
            for (int i = 0; i < cntPlatformSynclist.size(); i++)
            {
                String msg = "";
                CntPlatformSync cntPlatformSync = cntPlatformSynclist.get(i);
                List tarsynclistinit = cntPlatformSyncDS.getCntPlatformSyncByCond(cntPlatformSync);
                if (tarsynclistinit != null && tarsynclistinit.size() > 0)
                {

                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "发布数据不存在");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }
                int statusinit = cntPlatformSync.getStatus();
                if (statusinit == 300 || statusinit == 200)
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), cntPlatformSync.getObjectid().toString(),
                            "失败", "发布数据状态不正确");
                    returnInfo.appendOperateInfo(operateInfo);
                    continue;// 中断进入下一个循环
                }

                String channelid = cntPlatformSync.getElementid();// 频道id
                CmsChannel channel = new CmsChannel();
                channel.setChannelid(channelid);
                List channellist = channelDS.getChannelByCond(channel);
                channel = (CmsChannel) channellist.get(0);

                String categoryid = cntPlatformSync.getParentid();// 栏目id
                Categorycdn cdn = new Categorycdn();
                cdn.setCategoryid(categoryid);
                List cdnlist = categorycdnDS.selectByCategoryindex(cdn);
                cdn = (Categorycdn) cdnlist.get(0);
                List tarlist = new ArrayList();
                CntTargetSync tarsync = new CntTargetSync();
                tarsync.setRelateindex(cntPlatformSync.getSyncindex());// 得到这个平台下的关联了的网元
                tarsync.setObjecttype(28);
                tarlist = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                if (tarlist != null && tarlist.size() > 0)
                {
                    tarlist = checkCanPublicList(cntPlatformSync,tarlist);
                    Long batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id")
                            .toString());
                    for (int j = 0; j < tarlist.size(); j++)
                    {
                        Map<String, List> objMap = new HashMap();
                        tarsync = (CntTargetSync) tarlist.get(j);// 得到网元信息,判断频道，栏目是否发布
                        List tarsynclistinittar = cntTargetSyncDS.getCntTargetSyncByCond(tarsync);
                        if (tarsynclistinittar != null && tarsynclistinittar.size() > 0)
                        {

                        }
                        else
                        {
                            msg = msg + " 发布数据不存在";

                            continue;
                        }
                        int status = tarsync.getStatus();

                        Targetsystem targetSystem = new Targetsystem();
                        targetSystem.setTargetindex(tarsync.getTargetindex());
                        List targetlist = targetSystemDS.getTargetsystemByCond(targetSystem);
                        if (targetlist != null && targetlist.size() > 0)
                        {
                            targetSystem = (Targetsystem) targetlist.get(0);
                        }
                        if (targetSystem.getStatus() != 0)
                        {
                            msg = msg + " 发布到目标系统" + targetSystem.getTargetid() + ":目标系统状态不正确";
                            continue;
                        }
                        long targetindex = tarsync.getTargetindex();
                        CntTargetSync sync = new CntTargetSync();
                        sync.setObjecttype(5);// 频道
                        sync.setTargetindex(targetindex);
                        sync.setObjectid(channelid);
                        List list = cntTargetSyncDS.getCntTargetSyncByCond(sync);

                        CntTargetSync synccat = new CntTargetSync();
                        synccat.setObjecttype(2);// 栏目
                        synccat.setTargetindex(targetindex);
                        synccat.setObjectid(categoryid);
                        List listcat = cntTargetSyncDS.getCntTargetSyncByCond(synccat);

                        if (list != null && list.size() > 0 && listcat != null && listcat.size() > 0)
                        {
                            sync = (CntTargetSync) list.get(0);
                            synccat = (CntTargetSync) listcat.get(0);
                            if ((sync.getStatus() == 300 || sync.getStatus() == 500)
                                    && (synccat.getStatus() == 300 || synccat.getStatus() == 500))
                            {

                                if (tarsync.getStatus() == 0 || tarsync.getStatus() == 400)
                                {
                                    actiontype = 1;// 新增发布
                                    tarsync.setOperresult(10);// 新增同步中
                                }
                                else if (tarsync.getStatus() == 500)
                                {
                                    actiontype = 2;// 修改发布
                                    tarsync.setOperresult(40);// 修改同步中
                                }
                                else
                                {
                                    // ifail++;
                                    // operateInfo = new OperateInfo(String
                                    // .valueOf(index++), tarsync
                                    // .getSyncindex().toString(), "操作失败",
                                    // "当前状态不能发布");
                                    // returnInfo.appendOperateInfo(operateInfo);
                                    msg = msg + " 目标系统" + targetSystem.getTargetid() + ":发布数据状态不正确";
                                    continue;// 中断进入下一个循环
                                }
                            }
                            else
                            {
                                // ifail++;
                                // operateInfo = new OperateInfo(String
                                // .valueOf(index++), sync.getSyncindex()
                                // .toString(), "操作失败", "频道或栏目尚未发布");
                                // returnInfo.appendOperateInfo(operateInfo);
                                msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目尚未发布";
                                continue;// 中断进入下一个循环
                            }
                        }
                        else
                        {
                            msg = msg + " 目标系统" + targetSystem.getTargetid() + ":频道或栏目没有发布数据";
                            continue;// 中断进入下一个循环
                        }
                        // 结束对频道及栏目在网元上的发布状态的判断
                        String xmladdress = "";
                        List maplist = new ArrayList();
                        CategoryChannelMap map = new CategoryChannelMap();
                        map.setCategoryid(categoryid);
                        map.setChannelid(channelid);
                        List listmap = categoryChannelMapDS.getCategoryChannelMapByCond2(map);
                        if (listmap != null && listmap.size() > 0)
                        {
                            map = (CategoryChannelMap) listmap.get(0);
                            map.setCategorycode(cdn.getCatagorycode());
                            map.setCpcontentid(channel.getCpcontentid());
                            objMap.put("categorychannelmaplist", listmap);
                        }
                        if (cntPlatformSync.getPlatform() == 2)
                        {// 3.0平台,1个平台生成1个xml
                            xmladdress = StandardImpFactory.getInstance().create(2).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目

                        }
                        else
                        {
                          //如果是2.0平台就每个网元生成一个不同的batchid
                            batchid = Long.valueOf(this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_batch_id").toString());
                            xmladdress = StandardImpFactory.getInstance().create(1).createXmlFile(objMap,
                                    targetSystem.getTargettype(), 28, actiontype);// 文广1是regist2是栏目
                        }
                        int pristatus = 1;
                        // bms和egp的情况且有别的网元的情况,status可为0或1
                        if (((targetSystem.getTargettype() == 1) || (targetSystem.getTargettype() == 2))
                                && (tarlist.size() > 0))
                        {
                            for (int count = 0; count < tarlist.size(); count++)
                            {
                                CntTargetSync cs = (CntTargetSync) tarlist.get(count);
                                if (!cs.getTargetindex().equals(tarsync.getTargetindex()))
                                {
                                    Targetsystem tsnew = new Targetsystem();
                                    tsnew.setTargetindex(cs.getTargetindex());
                                    List target = targetSystemDS.getTargetsystemByCond(tsnew);
                                    if (target != null && target.size() > 0)
                                    {
                                        tsnew = (Targetsystem) target.get(0);
                                    }
                                    if ((tsnew.getTargettype() == 3))
                                    {// cdn
                                        pristatus = 0;
                                    }
                                    else if (tsnew.getTargettype() == 1)
                                    {// bms
                                        pristatus = 0;
                                    }

                                }
                            }
                        }
                        insertSyncTask(tarsync.getTargetindex(), xmladdress, map.getMapindex(), map.getMappingid(), 28,
                                actiontype, cdn.getCategoryid(), channel.getCpcontentid(), cdn.getCatagorycode(),
                                channel.getChannelid(), batchid, pristatus);// 调用同步任务模块DS代码

                        tarsync.setStatus(200);// 200发布中
                        cntTargetSyncDS.updateCntTargetSync(tarsync);
                        flagbool = true;
                        // iSuccessed++;
                        //
                        // operateInfo = new OperateInfo(String.valueOf(index++),
                        // tarsync.getSyncindex().toString(), ResourceMgt
                        // .findDefaultText(SUCCESS), ResourceMgt
                        // .findDefaultText(PUBLISH_SUCCESS));
                        // returnInfo.appendOperateInfo(operateInfo);
                        msg = msg + " 发送到目标系统" + targetSystem.getTargetid() + ":操作成功";
                        successlist.add(map);
                        CommonLogUtil.insertOperatorLog(String.valueOf(map.getMappingid() + ","
                                + targetSystem.getTargetid()), CmsChannelConstant.MGTTYPE_CHANNELCONTENT,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH,
                                CmsChannelConstant.OPERTYPE_CATEGORYCHANNEL_PUBLISH_INFO,
                                CmsChannelConstant.RESOURCE_OPERATION_SUCCESS);

                    }
                    CntTargetSync finaltarget = new CntTargetSync();
                    finaltarget.setRelateindex(cntPlatformSync.getSyncindex());
                    List finaltargetlist = cntTargetSyncDS.getCntTargetSyncByCond(finaltarget);

                    if (finaltargetlist != null && finaltargetlist.size() > 0)
                    {
                        int[] targetSyncStatus = new int[finaltargetlist.size()];
                        for (int ii = 0; ii < finaltargetlist.size(); ii++)
                        {
                            targetSyncStatus[ii] = ((CntTargetSync) finaltargetlist.get(ii)).getStatus();
                        }
                        int statussync = StatusTranslator.getPlatformSyncStatus(targetSyncStatus);

                        int originalstatus = cntPlatformSync.getStatus();// 平台的原始状态
                        if (originalstatus != statussync)
                        {
                            cntPlatformSync.setStatus(statussync);
                            cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                        }
                    }
                }
                if (successlist != null && successlist.size() > 0)
                {
                    cntPlatformSync.setStatus(200);
                    cntPlatformSyncDS.updateCntPlatformSync(cntPlatformSync);
                }

                if (flagbool == true)
                {
                    iSuccessed++;

                    operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getObjectid().toString(), "成功", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }
                else
                {
                    ifail++;
                    operateInfo = new OperateInfo(String.valueOf(index++), tarsync.getObjectid().toString(), "失败", msg);
                    returnInfo.appendOperateInfo(operateInfo);
                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (cntPlatformSynclist.size() == 0 || ifail == cntPlatformSynclist.size())
        {
            flag = "1"; // "1"
            errorcode = "失败数量：" + ifail; // "350099"
        }
        else if (ifail > 0 && ifail < cntPlatformSynclist.size())
        {
            flag = "2";
            errorcode = "成功数量：" + iSuccessed + "  失败数量：" + ifail + ""; // "350098"
        }
        else
        {
            flag = "0"; // "0"
            errorcode = "成功数量：" + iSuccessed;
        }
        returnInfo.setFlag(flag);
        returnInfo.setReturnMessage(errorcode);

        log.debug("canclePuhlishProgramtype end...");
        return returnInfo.toString();
    }

    // 插入同步任务
    public String insertSyncTask(Long targetindex, String contentmngxmlurl, Long objindex, String objectid,
            int objtype, int actiontype, String parentid, String elementcode, String parentcode, String channelid,
            Long batchid, int status) throws DomainServiceException
    {

        Long taskindex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cnt_sync_task");
        String correlateid = this.getPrimaryKeyGenerator().getPrimarykey("ucdn_task_correlate_id").toString();

        CntSyncTask cntsyncTask = new CntSyncTask();
        cntsyncTask.setBatchid(batchid);
        cntsyncTask.setStatus(status);// 任务状态：0-初始 1-待执行 2-执行中 3-成功结束 4-失败结束
        cntsyncTask.setPriority(5);// 任务优先级：1-高 2-中 3-低
        cntsyncTask.setCorrelateid(correlateid);
        cntsyncTask.setContentmngxmlurl(contentmngxmlurl);
        cntsyncTask.setDestindex(targetindex);
        cntsyncTask.setSource(1);
        cntsyncTask.setTaskindex(taskindex);
        cntsyncTask.setRetrytimes(3);
        try
        {
            cntsyncTaskDS.insertCntSyncTask(cntsyncTask);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        toRecordObj(taskindex, objindex, targetindex, objectid, channelid, objtype, actiontype, parentid, elementcode,
                parentcode);// 记录到发布日志表,2是栏目
        // 栏目

        return null;
    }

    // elementid、parentid和elementcode、parentcode都必须填写，填写mapping关联的两个对象的contentid和文广code
    public String toRecordObj(Long taskindex, Long objindex, Long destindex, String objid, String elementid,
            int objtype, int actiontype, String parentid, String elementcode, String parentcode)
            throws DomainServiceException
    {
        ObjectSyncRecord objectSyncRecord = new ObjectSyncRecord();// 对象日志表
        objectSyncRecord.setTaskindex(taskindex);
        objectSyncRecord.setObjectindex(objindex);
        objectSyncRecord.setActiontype(actiontype);// 同步类型：1-REGIST，2-UPDATE，3-DELETE
        objectSyncRecord.setObjecttype(objtype);// 对象类型编号：1-service，2-Category，3-Program

        Targetsystem targetsystem = new Targetsystem();
        targetsystem.setTargetindex(destindex);
        List list = targetSystemDS.getTargetsystemByCond(targetsystem);
        int plattype = ((Targetsystem) list.get(0)).getPlatform();
        if (plattype == 1)
        {

            objectSyncRecord.setParentid(parentid);
            objectSyncRecord.setParentcode(parentcode);
            objectSyncRecord.setElementcode(elementcode);
        }
        else
        {
            objectSyncRecord.setParentid(parentid);
        }
        objectSyncRecord.setObjectid(objid);// mappingid
        objectSyncRecord.setDestindex(destindex);
        objectSyncRecord.setElementid(elementid);
        try
        {
            objectSyncRecordDS.insertObjectSyncRecord(objectSyncRecord);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 创建XML
     * 
     * @param categoryChannelMapList
     * @param syncType
     * @param tempAreaPath
     * @param mountPoint
     * @return
     */
    public String createCategoryChannelXML(List categoryChannelMapList, int syncType, String tempAreaPath,
            String mountPoint)
    {
        Date date = new Date();
        String dir = getCurrentTime();
        String fileName = date.getTime() + "_categoryChannelMap.xml";
        String synctype = "";
        String tempFilePath = tempAreaPath + File.separator + CMSCASTMTYPE_CNTSYNCXML + File.separator + dir
                + File.separator + CMSCHANNELMTYPE_PROGRAMTYPE + File.separator;
        String mountAndTempfilePath = mountPoint + tempFilePath;
        String retPath = filterSlashStr(tempFilePath + fileName);
        File file = new File(mountAndTempfilePath);
        if (!file.exists())
        {
            file.mkdirs();
        }
        String fileFullName = mountAndTempfilePath + fileName;
        Document dom = DocumentHelper.createDocument();
        Element adiElement = dom.addElement("ADI");
        adiElement.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        Element mappingsElement = adiElement.addElement("Mappings");
        Element mappingElement = null;
        Element propertyElement = null;

        if (syncType == 1 || syncType == 2)
        {
            synctype = "REGIST or UPDATE";
        }
        else if (syncType == 3)
        {
            synctype = "DELETE";
        }

        for (Iterator iterator = categoryChannelMapList.iterator(); iterator.hasNext();)
        {
            CategoryChannelMap categoryChannelMap = (CategoryChannelMap) iterator.next();

            // 元素节点
            mappingElement = mappingsElement.addElement("Mapping");
            String objectId = String.format("%02d", 28) + String.format("%030d", categoryChannelMap.getMapindex());
            mappingElement.addAttribute("ObjectID", objectId);
            mappingElement.addAttribute("ParentType", "Category");
            mappingElement.addAttribute("ParentID", categoryChannelMap.getCategoryid());
            mappingElement.addAttribute("ElementType", "Channel");
            mappingElement.addAttribute("ElementID", categoryChannelMap.getChannelid());
            mappingElement.addAttribute("Action", synctype);

            // 属性节点
            propertyElement = mappingElement.addElement("Property");
            propertyElement.addAttribute("Name", "Sequence");
            if (categoryChannelMap.getSequence() != null)
            {
                propertyElement.addText(categoryChannelMap.getSequence().toString());
            }
            else
            {
                propertyElement.addText("");
            }
        }

        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            log.error("createScheduleXml exception:" + e);
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                    log.error("createScheduleXml exception:" + e);
                }
            }
        }

        return retPath;

    }

    // 获取临时区目录
    private String getTempAreaPath() throws DomainServiceException
    {
        log.debug("getTempAreaPath  start");
        String stroageareaPath = "";
        // 获取临时区路径
        try
        {
            ICmsStorageareaLS storageareaLs = (ICmsStorageareaLS) SSBBus.findDomainService("cmsStorageareaLS");
            stroageareaPath = storageareaLs.getAddress(TEMPAREA_ID);// 临时区
            if (null == stroageareaPath || "".equals(stroageareaPath) || (TEMPAREA_ERRORSiGN).equals(stroageareaPath))
            {// 获取临时区地址失败
                return "";
            }
        }
        catch (Exception e)
        {
            log.error("Error occured in getTempAreaPath method");
            throw new DomainServiceException(e);
        }

        log.debug("getTempAreaPath  end");
        return stroageareaPath;
    }

    /**
     * 获取时间 如"20111002"
     * 
     * @return
     */

    private static String getCurrentTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String currentTime = "";
        currentTime = sdf.format(date);
        return currentTime;
    }

    /**
     * 将字符中的"\"转换成"/"
     * 
     * @param str
     * @return
     */

    private static String filterSlashStr(String str)
    {
        String rtnStr = "";
        if (!str.trim().equals(""))
        {
            if (str.indexOf("\\", 0) != -1)
            {
                while (str.length() > 0)
                {
                    if (str.indexOf("\\", 0) > -1)
                    {
                        rtnStr += str.subSequence(0, str.indexOf("\\", 0));
                        rtnStr += "/";
                        str = str.substring(str.indexOf("\\", 0) + 1, str.length());
                    }
                    else
                    {
                        rtnStr += str;
                        str = "";
                    }
                }
            }
            else
            {

                rtnStr = str;
            }
        }
        return rtnStr;
    }

    /**
     * 取出同步XML文件FTP地址
     * 
     * @return
     */

    private String getSynXMLFTPAddr()
    {
        log.debug("getSynXMLFTPAdd starting...");
        UsysConfig usysConfig = null;
        String synXMLFTPAddress = "";
        try
        {
            usysConfig = usysConfigDS.getUsysConfigByCfgkey("cms.cntsyn.ftpaddress");
            if (null == usysConfig && null == usysConfig.getCfgvalue() && (usysConfig.getCfgvalue().endsWith("")))
            {
                return null;
            }
            else
            {
                synXMLFTPAddress = usysConfig.getCfgvalue();
            }
        }
        catch (DomainServiceException e)
        {
            log.error("getSynXMLFTPAdd exception:" + e);
            return null;

        }
        log.debug("getSynXMLFTPAdd end");
        return synXMLFTPAddress;
    }

    public ICategoryChannelMapDS getCategoryChannelMapDS()
    {
        return categoryChannelMapDS;
    }

    public void setCategoryChannelMapDS(ICategoryChannelMapDS categoryChannelMapDS)
    {
        this.categoryChannelMapDS = categoryChannelMapDS;
    }

    public ICmsChannelDS getChannelDS()
    {
        return channelDS;
    }

    public void setChannelDS(ICmsChannelDS channelDS)
    {
        this.channelDS = channelDS;
    }

    public ICategorycdnDS getCategorycdnDS()
    {
        return categorycdnDS;
    }

    public void setCategorycdnDS(ICategorycdnDS categorycdnDS)
    {
        this.categorycdnDS = categorycdnDS;
    }

    public ICntSyncTaskDS getCntsyncTaskDS()
    {
        return cntsyncTaskDS;
    }

    public void setCntsyncTaskDS(ICntSyncTaskDS cntsyncTaskDS)
    {
        this.cntsyncTaskDS = cntsyncTaskDS;
    }

    public IBatchObjectRecordDS getBatchObjectRecordDS()
    {
        return batchObjectRecordDS;
    }

    public void setBatchObjectRecordDS(IBatchObjectRecordDS batchObjectRecordDS)
    {
        this.batchObjectRecordDS = batchObjectRecordDS;
    }

    public IUsysConfigDS getUsysConfigDS()
    {
        return usysConfigDS;
    }

    public void setUsysConfigDS(IUsysConfigDS usysConfigDS)
    {
        this.usysConfigDS = usysConfigDS;
    }

    public ITargetsystemDS getTargetSystemDS()
    {
        return targetSystemDS;
    }

    public void setTargetSystemDS(ITargetsystemDS targetSystemDS)
    {
        this.targetSystemDS = targetSystemDS;
    }

    public ICntSyncRecordDS getCntSyncRecordDS()
    {
        return cntSyncRecordDS;
    }

    public void setCntSyncRecordDS(ICntSyncRecordDS cntSyncRecordDS)
    {
        this.cntSyncRecordDS = cntSyncRecordDS;
    }

}

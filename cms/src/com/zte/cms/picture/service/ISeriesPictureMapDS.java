package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.picture.model.CategoryPictureMap;
import com.zte.cms.picture.model.SeriesPictureMap;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface ISeriesPictureMapDS
{
    /**
     * 新增SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void insertSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException;

    /**
     * 更新SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException;

    /**
     * 批量更新SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void updateSeriesPictureMapList(List<SeriesPictureMap> seriesPictureMapList) throws DomainServiceException;

    /**
     * 删除SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException;

    /**
     * 批量删除SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public void removeSeriesPictureMapList(List<SeriesPictureMap> seriesPictureMapList) throws DomainServiceException;

    /**
     * 查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @return SeriesPictureMap对象
     * @throws DomainServiceException ds异常
     */
    public SeriesPictureMap getSeriesPictureMap(SeriesPictureMap seriesPictureMap) throws DomainServiceException;

    /**
     * 根据条件查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象
     * @return 满足条件的SeriesPictureMap对象集
     * @throws DomainServiceException ds异常
     */
    public List<SeriesPictureMap> getSeriesPictureMapByCond(SeriesPictureMap seriesPictureMap)
            throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(SeriesPictureMap seriesPictureMap, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    /**
     * 根据连续剧Index查询其下面已发布海报的数量
     * 
     * @seriesPictureMap SeriesPictureMap对象
     * @return 该连续剧下已发布的海报的数量
     * @throws DomainServiceException ds异常
     * 
     */
    public Integer getPublishedMapBySeriesIndexCnt(SeriesPictureMap seriesPictureMap) throws DomainServiceException;
    /**
     * 根据条件分页查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryseriesPicMap(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException;
    
    /**
     * 根据条件分页查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuerypseriesPicTargetMap(SeriesPictureMap seriesPictureMap, int start, int pageSize)
            throws DomainServiceException;   
    
    /**
     * 根据条件分页查询SeriesPictureMap对象
     * 
     * @param seriesPictureMap SeriesPictureMap对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public List<SeriesPictureMap> getSeriesnormalPictureMapByCond(SeriesPictureMap seriesPictureMap)
            throws DomainServiceException;     
    
}

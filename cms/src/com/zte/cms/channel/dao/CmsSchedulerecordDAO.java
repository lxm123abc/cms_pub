package com.zte.cms.channel.dao;

import java.util.List;

import com.zte.cms.channel.dao.ICmsSchedulerecordDAO;
import com.zte.cms.channel.model.CmsChannel;
import com.zte.cms.channel.model.CmsSchedulerecord;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class CmsSchedulerecordDAO extends DynamicObjectBaseDao implements ICmsSchedulerecordDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("insert cmsSchedulerecord starting...");
        super.insert("insertCmsSchedulerecord", cmsSchedulerecord);
        log.debug("insert cmsSchedulerecord end");
    }

    public void updateCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("update cmsSchedulerecord by pk starting...");
        super.update("updateCmsSchedulerecord", cmsSchedulerecord);
        log.debug("update cmsSchedulerecord by pk end");
    }

    public void updateCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("update cmsSchedulerecord by conditions starting...");
        super.update("updateCmsSchedulerecordByCond", cmsSchedulerecord);
        log.debug("update cmsSchedulerecord by conditions end");
    }

    public void deleteCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("delete cmsSchedulerecord by pk starting...");
        super.delete("deleteCmsSchedulerecord", cmsSchedulerecord);
        log.debug("delete cmsSchedulerecord by pk end");
    }

    public void deleteCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("delete cmsSchedulerecord by conditions starting...");
        super.delete("deleteCmsSchedulerecordByCond", cmsSchedulerecord);
        log.debug("update cmsSchedulerecord by conditions end");
    }

    public CmsSchedulerecord getCmsSchedulerecord(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("query cmsSchedulerecord starting...");
        CmsSchedulerecord resultObj = (CmsSchedulerecord) super.queryForObject("getCmsSchedulerecord",
                cmsSchedulerecord);
        log.debug("query cmsSchedulerecord end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<CmsSchedulerecord> getCmsSchedulerecordByCond(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("query cmsSchedulerecord by condition starting...");
        List<CmsSchedulerecord> rList = (List<CmsSchedulerecord>) super.queryForList(
                "queryCmsSchedulerecordListByCond", cmsSchedulerecord);
        log.debug("query cmsSchedulerecord by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsSchedulerecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSchedulerecordListCntByCond", cmsSchedulerecord))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSchedulerecord> rsList = (List<CmsSchedulerecord>) super.pageQuery(
                    "queryCmsSchedulerecordListByCond", cmsSchedulerecord, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsSchedulerecord by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("queryCmsSchedulerecordListByCond", "queryCmsSchedulerecordListCntByCond",
                cmsSchedulerecord, start, pageSize, puEntity);
    }
    
    
    public PageInfo pageInfoQuerychannel(CmsSchedulerecord cmsSchedulerecord, int start, int pageSize) throws DAOException
    {
        log.debug("page query cmsSchedulerecord by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCmsSchedulerecordListCntByCondchannel", cmsSchedulerecord))
                .intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<CmsSchedulerecord> rsList = (List<CmsSchedulerecord>) super.pageQuery(
                    "queryCmsSchedulerecordListByCondchannel", cmsSchedulerecord, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query cmsSchedulerecord by condition end");
        return pageInfo;
    }

    @Override
    public List<CmsSchedulerecord> getCmsSchedulerecordByID(CmsSchedulerecord cmsSchedulerecord) throws DAOException
    {
        log.debug("query cmsSchedulerecord by scheduleid starting...");
        List<CmsSchedulerecord> rList = (List<CmsSchedulerecord>) super.queryForList(
                "queryCmsSchedulerecordListByID", cmsSchedulerecord);
        log.debug("query cmsSchedulerecord by scheduleid end");
        return rList;
    }
   

}
package com.zte.cms.seriescrmmap.dao;

import java.util.List;

import com.zte.cms.seriescrmmap.dao.ISeriesCrmMapDAO;
import com.zte.cms.seriescrmmap.model.SeriesCrmMap;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;

public class SeriesCrmMapDAO extends DynamicObjectBaseDao implements ISeriesCrmMapDAO
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    public void insertSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("insert seriesCrmMap starting...");
        super.insert("insertSeriesCrmMap", seriesCrmMap);
        log.debug("insert seriesCrmMap end");
    }

    public void updateSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("update seriesCrmMap by pk starting...");
        super.update("updateSeriesCrmMap", seriesCrmMap);
        log.debug("update seriesCrmMap by pk end");
    }

    public void updateSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("update seriesCrmMap by conditions starting...");
        super.update("updateSeriesCrmMapByCond", seriesCrmMap);
        log.debug("update seriesCrmMap by conditions end");
    }

    public void deleteSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("delete seriesCrmMap by pk starting...");
        super.delete("deleteSeriesCrmMap", seriesCrmMap);
        log.debug("delete seriesCrmMap by pk end");
    }

    public void deleteSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("delete seriesCrmMap by conditions starting...");
        super.delete("deleteSeriesCrmMapByCond", seriesCrmMap);
        log.debug("update seriesCrmMap by conditions end");
    }

    public SeriesCrmMap getSeriesCrmMap(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("query seriesCrmMap starting...");
        SeriesCrmMap resultObj = (SeriesCrmMap) super.queryForObject("getSeriesCrmMap", seriesCrmMap);
        log.debug("query seriesCrmMap end");
        return resultObj;
    }

    @SuppressWarnings("unchecked")
    public List<SeriesCrmMap> getSeriesCrmMapByCond(SeriesCrmMap seriesCrmMap) throws DAOException
    {
        log.debug("query seriesCrmMap by condition starting...");
        List<SeriesCrmMap> rList = (List<SeriesCrmMap>) super.queryForList("querySeriesCrmMapListByCond", seriesCrmMap);
        log.debug("query seriesCrmMap by condition end");
        return rList;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize) throws DAOException
    {
        log.debug("page query seriesCrmMap by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("querySeriesCrmMapListCntByCond", seriesCrmMap)).intValue();
        if (totalCnt > 0)
        {
            int fetchSize = pageSize > (totalCnt - start) ? (totalCnt - start) : pageSize;
            List<SeriesCrmMap> rsList = (List<SeriesCrmMap>) super.pageQuery("querySeriesCrmMapListByCond",
                    seriesCrmMap, start, fetchSize);
            pageInfo = new PageInfo(start, totalCnt, fetchSize, rsList);
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query seriesCrmMap by condition end");
        return pageInfo;
    }

    @SuppressWarnings("unchecked")
    public PageInfo pageInfoQuery(SeriesCrmMap seriesCrmMap, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException
    {
        return super.indexPageQuery("querySeriesCrmMapListByCond", "querySeriesCrmMapListCntByCond", seriesCrmMap,
                start, pageSize, puEntity);
    }

}
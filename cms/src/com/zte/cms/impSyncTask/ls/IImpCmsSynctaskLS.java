package com.zte.cms.impSyncTask.ls;

import com.zte.cms.impSyncTask.model.ImpCmsSynctask;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;

public interface IImpCmsSynctaskLS
{
	/**
  * 查询ImpCmsSynctask对象
  *
  * @param taskindex Long类型
  * @return ImpCmsSynctask对象
  * @throws Exception 异常 
  */
	 public ImpCmsSynctask getImpCmsSynctask( Long taskindex )throws Exception; 

	 /**
   * 根据条件分页查询ImpCmsSynctask对象 
   *
   * @param impCmsSynctask ImpCmsSynctask对象，作为查询条件的参数 
   * @param start 起始行
   * @param pageSize 页面大小
   * @param puEntity 排序空置参数@see PageUtilEntity
   * @return  查询结果
   * @throws Exception 异常
   */
     public TableDataInfo pageInfoQuery(ImpCmsSynctask impCmsSynctask, int start, int pageSize)throws Exception;
     
}
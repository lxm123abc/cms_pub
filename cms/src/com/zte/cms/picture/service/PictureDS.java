package com.zte.cms.picture.service;

import java.util.List;

import com.zte.cms.common.Generator;
import com.zte.cms.common.ObjectType;
import com.zte.cms.picture.model.Picture;
import com.zte.cms.picture.common.PictureConstants;
import com.zte.cms.picture.dao.IPictureDAO;
import com.zte.cms.picture.service.IPictureDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class PictureDS extends DynamicObjectBaseDS implements IPictureDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IPictureDAO dao = null;

    public void setDao(IPictureDAO dao)
    {
        this.dao = dao;
    }

    public void insertPicture(Picture picture) throws DomainServiceException
    {
        log.debug("insert picture starting...");
        try
        {
            if (null == picture.getPictureindex())
            {
                Long pictureIndex = (Long) this.getPrimaryKeyGenerator().getPrimarykey("cms_picture_index");
                picture.setPictureindex(pictureIndex);
            }
            dao.insertPicture(picture);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert picture end");
    }

    public void updatePicture(Picture picture) throws DomainServiceException
    {
        log.debug("update picture by pk starting...");
        try
        {
            dao.updatePicture(picture);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update picture by pk end");
    }

    public void updatePictureList(List<Picture> pictureList) throws DomainServiceException
    {
        log.debug("update pictureList by pk starting...");
        if (null == pictureList || pictureList.size() == 0)
        {
            log.debug("there is no datas in pictureList");
            return;
        }
        try
        {
            for (Picture picture : pictureList)
            {
                dao.updatePicture(picture);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update pictureList by pk end");
    }

    public void removePicture(Picture picture) throws DomainServiceException
    {
        log.debug("remove picture by pk starting...");
        try
        {
            dao.deletePicture(picture);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove picture by pk end");
    }

    public void removePictureList(List<Picture> pictureList) throws DomainServiceException
    {
        log.debug("remove pictureList by pk starting...");
        if (null == pictureList || pictureList.size() == 0)
        {
            log.debug("there is no datas in pictureList");
            return;
        }
        try
        {
            for (Picture picture : pictureList)
            {
                dao.deletePicture(picture);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove pictureList by pk end");
    }

    public Picture getPicture(Picture picture) throws DomainServiceException
    {
        log.debug("get picture by pk starting...");
        Picture rsObj = null;
        try
        {
            rsObj = dao.getPicture(picture);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get pictureList by pk end");
        return rsObj;
    }

    public List<Picture> getPictureByCond(Picture picture) throws DomainServiceException
    {
        log.debug("get picture by condition starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByCond(picture);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by condition end");
        return rsList;
    }

    // 关联对象类型:1-Service 2-Catagory 3-Program 4-Series 5-Channel 11-Cast
    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByServiceIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByServiceIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByCatagoryIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByCatagoryIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByProgramIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByProgramIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureBySeriesIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureBySeriesIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByChannelIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByChannelIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public List<Picture> getPictureByCastIndex(Long index) throws DomainServiceException
    {
        log.debug("get picture by Index starting...");
        List<Picture> rsList = null;
        try
        {
            rsList = dao.getPictureByCastIndex(index);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get picture by Index end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Picture picture, int start, int pageSize) throws DomainServiceException
    {
        log.debug("get picture page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(picture, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Picture>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get picture page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(Picture picture, int start, int pageSize, PageUtilEntity puEntity)
            throws DomainServiceException
    {
        log.debug("get picture page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(picture, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Picture>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get picture page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get picture page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            switch (objType.intValue())
            {
                case PictureConstants.SERVIEC:
                    pageInfo = dao.pageInfoQueryService(relativeIndex, start, pageSize);
                    break;
                case PictureConstants.CATAGORY:
                    pageInfo = dao.pageInfoQueryCatagory(relativeIndex, start, pageSize);
                    break;
                case PictureConstants.PROGRAM:
                    pageInfo = dao.pageInfoQueryProgram(relativeIndex, start, pageSize);
                    break;
                case PictureConstants.SERIES:
                    pageInfo = dao.pageInfoQuerySeries(relativeIndex, start, pageSize);
                    break;
                case PictureConstants.CHANNEL:
                    pageInfo = dao.pageInfoQueryChannel(relativeIndex, start, pageSize);
                    break;
                case PictureConstants.CAST:
                    pageInfo = dao.pageInfoQueryCast(relativeIndex, start, pageSize);
                    break;
                default:
                    break;
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Picture>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get picture page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQueryByType(Integer objType, Long relativeIndex, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get picture page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryByType(objType, relativeIndex, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<Picture>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get picture page info by condition end");
        return tableInfo;
    }

    public List<Picture> queryChannelPictureListByChnanelindex(Long index) throws DomainServiceException
    {
        List<Picture> list = null;
        try
        {
            list = dao.queryChannelPictureListByChnanelindex(index);
        }
        catch (DAOException e)
        {
            e.printStackTrace();
        }
        return list;
    }
}

package com.zte.cms.cmsframe.model;

import com.zte.ssb.dynamicobj.DynamicBaseObject;

public class CmsFrame extends DynamicBaseObject
{
    private java.lang.Long frameindex;
    private java.lang.String framname;
    private java.lang.Integer framestatus;
    private java.lang.String framdesc;
    private java.lang.Integer cpindex;
    private java.lang.Integer cpid;
    private java.lang.Integer cpcontentid;
    private java.lang.Integer ordernumber;
    private java.lang.Integer namecn;
    private java.lang.Integer originalnamecn;
    private java.lang.Integer firstletter;
    private java.lang.Integer nameen;
    private java.lang.Integer riginalnameen;
    private java.lang.Integer sortname;
    private java.lang.Integer searchname;
    private java.lang.Integer genre;
    private java.lang.Integer desccn;
    private java.lang.Integer descen;
    private java.lang.Integer contentversion;
    private java.lang.Integer seriesflag;
    private java.lang.Integer contenttype;
    private java.lang.Integer contenttypeid;
    private java.lang.Integer platform;
    private java.lang.Integer isvalid;
    private java.lang.Integer oldstatus;
    private java.lang.Integer keywords;
    private java.lang.Integer copyrightcn;
    private java.lang.Integer copyrighten;
    private java.lang.Integer effectivedate;
    private java.lang.Integer expirydate;
    private java.lang.Integer exclusivevalidity;
    private java.lang.Integer copyrightexpiration;
    private java.lang.Integer llicensofflinetime;
    private java.lang.Integer deletetime;
    private java.lang.Integer countrytype;
    private java.lang.Integer country;
    private java.lang.Integer copyproperty;
    private java.lang.Integer authorizationway;
    private java.lang.Integer importlisence;
    private java.lang.Integer releaselisence;
    private java.lang.Integer provid;
    private java.lang.Integer cityid;
    private java.lang.Integer license;
    private java.lang.Integer tradetype;
    private java.lang.Integer contenttags;
    private java.lang.Integer pricetaxin;
    private java.lang.Integer newcomedays;
    private java.lang.Integer remainingdays;
    private java.lang.Integer clicknumber;
    private java.lang.Integer recommendstart;
    private java.lang.Integer rating;
    private java.lang.Integer actors;
    private java.lang.Integer writers;
    private java.lang.Integer directors;
    private java.lang.Integer language;
    private java.lang.Integer releaseyear;
    private java.lang.Integer orgairdate;
    private java.lang.Integer licensingstart;
    private java.lang.Integer licensingend;
    private java.lang.Integer macrovision;
    private java.lang.Integer sourcetype;
    private java.lang.Integer viewpoint;
    private java.lang.Integer awards;
    private java.lang.Integer duration;
    private java.lang.Integer programtype;
    private java.lang.Integer productcorp;
    private java.lang.Integer producer;
    private java.lang.Integer publisher;
    private java.lang.Integer subtitlelanguage;
    private java.lang.Integer dubbinglanguage;
    private java.lang.Integer title;

    public void initRelation()
    {
        this.addRelation("frameindex", "FRAMEINDEX");
        this.addRelation("framname", "FRAMNAME");
        this.addRelation("framestatus", "FRAMESTATUS");
        this.addRelation("framdesc", "FRAMDESC");
        this.addRelation("cpindex", "CPINDEX");
        this.addRelation("cpid", "CPID");
        this.addRelation("cpcontentid", "CPCONTENTID");
        this.addRelation("ordernumber", "ORDERNUMBER");
        this.addRelation("namecn", "NAMECN");
        this.addRelation("originalnamecn", "ORIGINALNAMECN");
        this.addRelation("firstletter", "FIRSTLETTER");
        this.addRelation("nameen", "NAMEEN");
        this.addRelation("riginalnameen", "RIGINALNAMEEN");
        this.addRelation("sortname", "SORTNAME");
        this.addRelation("searchname", "SEARCHNAME");
        this.addRelation("genre", "GENRE");
        this.addRelation("desccn", "DESCCN");
        this.addRelation("descen", "DESCEN");
        this.addRelation("contentversion", "CONTENTVERSION");
        this.addRelation("seriesflag", "SERIESFLAG");
        this.addRelation("contenttype", "CONTENTTYPE");
        this.addRelation("contenttypeid", "CONTENTTYPEID");
        this.addRelation("platform", "PLATFORM");
        this.addRelation("isvalid", "ISVALID");
        this.addRelation("oldstatus", "OLDSTATUS");
        this.addRelation("keywords", "KEYWORDS");
        this.addRelation("copyrightcn", "COPYRIGHTCN");
        this.addRelation("copyrighten", "COPYRIGHTEN");
        this.addRelation("effectivedate", "EFFECTIVEDATE");
        this.addRelation("expirydate", "EFFECTIVEDATE");
        this.addRelation("exclusivevalidity", "EXCLUSIVEVALIDITY");
        this.addRelation("copyrightexpiration", "COPYRIGHTEXPIRATION");
        this.addRelation("llicensofflinetime", "LLICENSOFFLINETIME");
        this.addRelation("deletetime", "DELETETIME");
        this.addRelation("countrytype", "COUNTRYTYPE");
        this.addRelation("country", "COUNTRY");
        this.addRelation("copyproperty", "COPYPROPERTY");
        this.addRelation("authorizationway", "AUTHORIZATIONWAY");
        this.addRelation("importlisence", "IMPORTLISENCE");
        this.addRelation("releaselisence", "RELEASELISENCE");
        this.addRelation("provid", "PROVID");
        this.addRelation("cityid", "CITYID");
        this.addRelation("license", "LICENSE");
        this.addRelation("tradetype", "TRADETYPE");
        this.addRelation("contenttags", "CONTENTTAGS");
        this.addRelation("pricetaxin", "PRICETAXIN");
        this.addRelation("newcomedays", "NEWCOMEDAYS");
        this.addRelation("remainingdays", "REMAININGDAYS");
        this.addRelation("clicknumber", "CLICKNUMBER");
        this.addRelation("recommendstart", "RECOMMENDSTART");
        this.addRelation("rating", "RATING");
        this.addRelation("actors", "ACTORS");
        this.addRelation("writers", "WRITERS");
        this.addRelation("directors", "DIRECTORS");
        this.addRelation("language", "LANGUAGE");
        this.addRelation("releaseyear", "RELEASEYEAR");
        this.addRelation("orgairdate", "ORGAIRDATE");
        this.addRelation("licensingstart", "LICENSINGSTART");
        this.addRelation("licensingend", "LICENSINGEND");
        this.addRelation("macrovision", "MACROVISION");
        this.addRelation("sourcetype", "SOURCETYPE");
        this.addRelation("viewpoint", "VIEWPOINT");
        this.addRelation("awards", "AWARDS");
        this.addRelation("duration", "DURATION");
        this.addRelation("programtype", "PROGRAMTYPE");
        this.addRelation("productcorp", "PRODUCTCORP");
        this.addRelation("producer", "PRODUCER");
        this.addRelation("publisher", "PUBLISHER");
        this.addRelation("subtitlelanguage", "SUBTITLELANGUAGE");
        this.addRelation("dubbinglanguage", "DUBBINGLANGUAGE");
        this.addRelation("title", "TITLE");

    }

    public java.lang.Long getFrameindex()
    {
        return frameindex;
    }

    public void setFrameindex(java.lang.Long frameindex)
    {
        this.frameindex = frameindex;
    }

    public java.lang.String getFramname()
    {
        return framname;
    }

    public void setFramname(java.lang.String framname)
    {
        this.framname = framname;
    }

    public java.lang.Integer getFramestatus()
    {
        return framestatus;
    }

    public void setFramestatus(java.lang.Integer framestatus)
    {
        this.framestatus = framestatus;
    }

    public java.lang.String getFramdesc()
    {
        return framdesc;
    }

    public void setFramdesc(java.lang.String framdesc)
    {
        this.framdesc = framdesc;
    }

    public java.lang.Integer getCpindex()
    {
        return cpindex;
    }

    public void setCpindex(java.lang.Integer cpindex)
    {
        this.cpindex = cpindex;
    }

    public java.lang.Integer getCpid()
    {
        return cpid;
    }

    public void setCpid(java.lang.Integer cpid)
    {
        this.cpid = cpid;
    }

    public java.lang.Integer getCpcontentid()
    {
        return cpcontentid;
    }

    public void setCpcontentid(java.lang.Integer cpcontentid)
    {
        this.cpcontentid = cpcontentid;
    }

    public java.lang.Integer getOrdernumber()
    {
        return ordernumber;
    }

    public void setOrdernumber(java.lang.Integer ordernumber)
    {
        this.ordernumber = ordernumber;
    }

    public java.lang.Integer getNamecn()
    {
        return namecn;
    }

    public void setNamecn(java.lang.Integer namecn)
    {
        this.namecn = namecn;
    }

    public java.lang.Integer getOriginalnamecn()
    {
        return originalnamecn;
    }

    public void setOriginalnamecn(java.lang.Integer originalnamecn)
    {
        this.originalnamecn = originalnamecn;
    }

    public java.lang.Integer getFirstletter()
    {
        return firstletter;
    }

    public void setFirstletter(java.lang.Integer firstletter)
    {
        this.firstletter = firstletter;
    }

    public java.lang.Integer getNameen()
    {
        return nameen;
    }

    public void setNameen(java.lang.Integer nameen)
    {
        this.nameen = nameen;
    }

    public java.lang.Integer getRiginalnameen()
    {
        return riginalnameen;
    }

    public void setRiginalnameen(java.lang.Integer riginalnameen)
    {
        this.riginalnameen = riginalnameen;
    }

    public java.lang.Integer getSortname()
    {
        return sortname;
    }

    public void setSortname(java.lang.Integer sortname)
    {
        this.sortname = sortname;
    }

    public java.lang.Integer getSearchname()
    {
        return searchname;
    }

    public void setSearchname(java.lang.Integer searchname)
    {
        this.searchname = searchname;
    }

    public java.lang.Integer getGenre()
    {
        return genre;
    }

    public void setGenre(java.lang.Integer genre)
    {
        this.genre = genre;
    }

    public java.lang.Integer getDesccn()
    {
        return desccn;
    }

    public void setDesccn(java.lang.Integer desccn)
    {
        this.desccn = desccn;
    }

    public java.lang.Integer getDescen()
    {
        return descen;
    }

    public void setDescen(java.lang.Integer descen)
    {
        this.descen = descen;
    }

    public java.lang.Integer getContentversion()
    {
        return contentversion;
    }

    public void setContentversion(java.lang.Integer contentversion)
    {
        this.contentversion = contentversion;
    }

    public java.lang.Integer getSeriesflag()
    {
        return seriesflag;
    }

    public void setSeriesflag(java.lang.Integer seriesflag)
    {
        this.seriesflag = seriesflag;
    }

    public java.lang.Integer getContenttype()
    {
        return contenttype;
    }

    public void setContenttype(java.lang.Integer contenttype)
    {
        this.contenttype = contenttype;
    }

    public java.lang.Integer getContenttypeid()
    {
        return contenttypeid;
    }

    public void setContenttypeid(java.lang.Integer contenttypeid)
    {
        this.contenttypeid = contenttypeid;
    }

    public java.lang.Integer getPlatform()
    {
        return platform;
    }

    public void setPlatform(java.lang.Integer platform)
    {
        this.platform = platform;
    }

    public java.lang.Integer getIsvalid()
    {
        return isvalid;
    }

    public void setIsvalid(java.lang.Integer isvalid)
    {
        this.isvalid = isvalid;
    }

    public java.lang.Integer getOldstatus()
    {
        return oldstatus;
    }

    public void setOldstatus(java.lang.Integer oldstatus)
    {
        this.oldstatus = oldstatus;
    }

    public java.lang.Integer getKeywords()
    {
        return keywords;
    }

    public void setKeywords(java.lang.Integer keywords)
    {
        this.keywords = keywords;
    }

    public java.lang.Integer getCopyrightcn()
    {
        return copyrightcn;
    }

    public void setCopyrightcn(java.lang.Integer copyrightcn)
    {
        this.copyrightcn = copyrightcn;
    }

    public java.lang.Integer getCopyrighten()
    {
        return copyrighten;
    }

    public void setCopyrighten(java.lang.Integer copyrighten)
    {
        this.copyrighten = copyrighten;
    }

    public java.lang.Integer getEffectivedate()
    {
        return effectivedate;
    }

    public void setEffectivedate(java.lang.Integer effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public java.lang.Integer getExpirydate()
    {
        return expirydate;
    }

    public void setExpirydate(java.lang.Integer expirydate)
    {
        this.expirydate = expirydate;
    }

    public java.lang.Integer getExclusivevalidity()
    {
        return exclusivevalidity;
    }

    public void setExclusivevalidity(java.lang.Integer exclusivevalidity)
    {
        this.exclusivevalidity = exclusivevalidity;
    }

    public java.lang.Integer getCopyrightexpiration()
    {
        return copyrightexpiration;
    }

    public void setCopyrightexpiration(java.lang.Integer copyrightexpiration)
    {
        this.copyrightexpiration = copyrightexpiration;
    }

    public java.lang.Integer getLlicensofflinetime()
    {
        return llicensofflinetime;
    }

    public void setLlicensofflinetime(java.lang.Integer llicensofflinetime)
    {
        this.llicensofflinetime = llicensofflinetime;
    }

    public java.lang.Integer getDeletetime()
    {
        return deletetime;
    }

    public void setDeletetime(java.lang.Integer deletetime)
    {
        this.deletetime = deletetime;
    }

    public java.lang.Integer getCountrytype()
    {
        return countrytype;
    }

    public void setCountrytype(java.lang.Integer countrytype)
    {
        this.countrytype = countrytype;
    }

    public java.lang.Integer getCountry()
    {
        return country;
    }

    public void setCountry(java.lang.Integer country)
    {
        this.country = country;
    }

    public java.lang.Integer getCopyproperty()
    {
        return copyproperty;
    }

    public void setCopyproperty(java.lang.Integer copyproperty)
    {
        this.copyproperty = copyproperty;
    }

    public java.lang.Integer getAuthorizationway()
    {
        return authorizationway;
    }

    public void setAuthorizationway(java.lang.Integer authorizationway)
    {
        this.authorizationway = authorizationway;
    }

    public java.lang.Integer getImportlisence()
    {
        return importlisence;
    }

    public void setImportlisence(java.lang.Integer importlisence)
    {
        this.importlisence = importlisence;
    }

    public java.lang.Integer getReleaselisence()
    {
        return releaselisence;
    }

    public void setReleaselisence(java.lang.Integer releaselisence)
    {
        this.releaselisence = releaselisence;
    }

    public java.lang.Integer getProvid()
    {
        return provid;
    }

    public void setProvid(java.lang.Integer provid)
    {
        this.provid = provid;
    }

    public java.lang.Integer getCityid()
    {
        return cityid;
    }

    public void setCityid(java.lang.Integer cityid)
    {
        this.cityid = cityid;
    }

    public java.lang.Integer getLicense()
    {
        return license;
    }

    public void setLicense(java.lang.Integer license)
    {
        this.license = license;
    }

    public java.lang.Integer getTradetype()
    {
        return tradetype;
    }

    public void setTradetype(java.lang.Integer tradetype)
    {
        this.tradetype = tradetype;
    }

    public java.lang.Integer getContenttags()
    {
        return contenttags;
    }

    public void setContenttags(java.lang.Integer contenttags)
    {
        this.contenttags = contenttags;
    }

    public java.lang.Integer getPricetaxin()
    {
        return pricetaxin;
    }

    public void setPricetaxin(java.lang.Integer pricetaxin)
    {
        this.pricetaxin = pricetaxin;
    }

    public java.lang.Integer getNewcomedays()
    {
        return newcomedays;
    }

    public void setNewcomedays(java.lang.Integer newcomedays)
    {
        this.newcomedays = newcomedays;
    }

    public java.lang.Integer getRemainingdays()
    {
        return remainingdays;
    }

    public void setRemainingdays(java.lang.Integer remainingdays)
    {
        this.remainingdays = remainingdays;
    }

    public java.lang.Integer getClicknumber()
    {
        return clicknumber;
    }

    public void setClicknumber(java.lang.Integer clicknumber)
    {
        this.clicknumber = clicknumber;
    }

    public java.lang.Integer getRecommendstart()
    {
        return recommendstart;
    }

    public void setRecommendstart(java.lang.Integer recommendstart)
    {
        this.recommendstart = recommendstart;
    }

    public java.lang.Integer getRating()
    {
        return rating;
    }

    public void setRating(java.lang.Integer rating)
    {
        this.rating = rating;
    }

    public java.lang.Integer getActors()
    {
        return actors;
    }

    public void setActors(java.lang.Integer actors)
    {
        this.actors = actors;
    }

    public java.lang.Integer getWriters()
    {
        return writers;
    }

    public void setWriters(java.lang.Integer writers)
    {
        this.writers = writers;
    }

    public java.lang.Integer getDirectors()
    {
        return directors;
    }

    public void setDirectors(java.lang.Integer directors)
    {
        this.directors = directors;
    }

    public java.lang.Integer getLanguage()
    {
        return language;
    }

    public void setLanguage(java.lang.Integer language)
    {
        this.language = language;
    }

    public java.lang.Integer getReleaseyear()
    {
        return releaseyear;
    }

    public void setReleaseyear(java.lang.Integer releaseyear)
    {
        this.releaseyear = releaseyear;
    }

    public java.lang.Integer getOrgairdate()
    {
        return orgairdate;
    }

    public void setOrgairdate(java.lang.Integer orgairdate)
    {
        this.orgairdate = orgairdate;
    }

    public java.lang.Integer getLicensingstart()
    {
        return licensingstart;
    }

    public void setLicensingstart(java.lang.Integer licensingstart)
    {
        this.licensingstart = licensingstart;
    }

    public java.lang.Integer getLicensingend()
    {
        return licensingend;
    }

    public void setLicensingend(java.lang.Integer licensingend)
    {
        this.licensingend = licensingend;
    }

    public java.lang.Integer getMacrovision()
    {
        return macrovision;
    }

    public void setMacrovision(java.lang.Integer macrovision)
    {
        this.macrovision = macrovision;
    }

    public java.lang.Integer getSourcetype()
    {
        return sourcetype;
    }

    public void setSourcetype(java.lang.Integer sourcetype)
    {
        this.sourcetype = sourcetype;
    }

    public java.lang.Integer getViewpoint()
    {
        return viewpoint;
    }

    public void setViewpoint(java.lang.Integer viewpoint)
    {
        this.viewpoint = viewpoint;
    }

    public java.lang.Integer getAwards()
    {
        return awards;
    }

    public void setAwards(java.lang.Integer awards)
    {
        this.awards = awards;
    }

    public java.lang.Integer getDuration()
    {
        return duration;
    }

    public void setDuration(java.lang.Integer duration)
    {
        this.duration = duration;
    }

    public java.lang.Integer getProgramtype()
    {
        return programtype;
    }

    public void setProgramtype(java.lang.Integer programtype)
    {
        this.programtype = programtype;
    }

    public java.lang.Integer getProductcorp()
    {
        return productcorp;
    }

    public void setProductcorp(java.lang.Integer productcorp)
    {
        this.productcorp = productcorp;
    }

    public java.lang.Integer getProducer()
    {
        return producer;
    }

    public void setProducer(java.lang.Integer producer)
    {
        this.producer = producer;
    }

    public java.lang.Integer getPublisher()
    {
        return publisher;
    }

    public void setPublisher(java.lang.Integer publisher)
    {
        this.publisher = publisher;
    }

    public java.lang.Integer getSubtitlelanguage()
    {
        return subtitlelanguage;
    }

    public void setSubtitlelanguage(java.lang.Integer subtitlelanguage)
    {
        this.subtitlelanguage = subtitlelanguage;
    }

    public java.lang.Integer getDubbinglanguage()
    {
        return dubbinglanguage;
    }

    public void setDubbinglanguage(java.lang.Integer dubbinglanguage)
    {
        this.dubbinglanguage = dubbinglanguage;
    }

    public java.lang.Integer getTitle()
    {
        return title;
    }

    public void setTitle(java.lang.Integer title)
    {
        this.title = title;
    }

}

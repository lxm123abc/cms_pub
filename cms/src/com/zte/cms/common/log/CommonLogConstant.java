package com.zte.cms.common.log;

public class CommonLogConstant
{
    // CMS业务键
    public static final String SYSTEM_SERVICE_KEY = "CMS";
    // 内容资源文件
    public static final String RESOURCE_CMS_CONTENT = "cms_content_resource";

    // 内容管理操作日志资源文件
    public static final String RESOURCE_CMS_CONTENT_LOG = "cms_log_resource";

    // 内容管理操作结果资源标识
    public static final String RESOURCE_OPERATION_SUCCESS = "operation.success";
    public static final String RESOURCE_OPERATION_FAIL = "operation.fail";
    public static final String RESOURCE_OPERATION_PARTIALSUCCESS = "operation.partialsuccess";
    public static final String RESOURCE_CMS_CONTENT_UNKNOW = "cms.strategy.log.unknow";

    /**
     * mgttype 管理类型：内容管理、角色管理、数据管理、存储管理、直播管理、节目单管理、收录管理
     */
    public static final String MGTTYPE_CONTENT = "log.content.mgt";
    public static final String MGTTYPE_ROLE = "log.role.mgt";
    public static final String MGTTYPE_DATA = "log.data.mgt";
    public static final String MGTTYPE_STORAGE = "log.storage.mgt";
    public static final String MGTTYPE_LIVE = "log.live.mgt";
    public static final String MGTTYPE_PROGRAMlIST = "log.programlist.mgt";
    public static final String MGTTYPE_lIVERECIEVE = "log.liverecieve.mgt";
    public static final String MGTTYPE_SERVICE = "log.service.mgt"; // 服务管理
    public static final String MGTTYPE_PROGRAM = "log.program.mgt"; // 内容管理
    public static final String MGTTYPE_MOVIE = "log.movie.mgt"; // 子内容管理
    public static final String MGTTYPE_VODTARGETSYN = "log.vodtargetsyn.mgt"; //点播内容发布
    public static final String MGTTYPE_VODTARGETCATGORYSYN = "log.vodtargetcatgorysyn.mgt"; //点播内容关联栏目发布
    public static final String MGTTYPE_SERVICEPUBLISHSYN= "log.servicepulish.mgt"; //服务发布管理
    
    public static final String MGTTYPE_CATEGORYSYN = "log.categorysyn.mgt"; //栏目发布管理
    public static final String MGTTYPE_CHANNELSYN = "log.channel.mgt"; //频道发布管理
    public static final String MGTTYPE_SERIESSYN = "log.seriessyn.mgt"; //连续剧发布管理

    public static final String MGTTYPE_CP = "log.cp.mgt"; //cp信息管理
    public static final String MGTTYPE_CPAZF = "log.cpazf.mgt"; //牌照方信息管理
    public static final String MGTTYPE_NOAUDIT = "log.noaudit.mgt"; //免审直通设置管理
    /**
     * opertype 操作类型（int) 内容管理：1-内容新增，2-内容修改，3-内容删除，4-内容下线，5-内容发布，6-子内容新增，7-子内容修改，8-子内容删除
     * 角色管理：20-CP新增，21-CP修改，22-CP删除，23-牌照方新增，24-牌照方修改，25-牌照方删除，26-牌照方免审设置，27-牌照方免审取消， 28-业务运营管理员免审设置，29-业务运营管理员免审取消
     * 数据管理：40- 存储管理：60 直播管理: 80- 节目单管理： 100- 收录管理： 120-
     */

    /**
     * opertype 操作类型（String)
     */
    // 内容管理
    public static final String OPERTYPE_CONTENT_ADD = "log.content.add";// 内容新增
    public static final String OPERTYPE_CONTENT_MOD = "log.content.modify";// 内容修改
    public static final String OPERTYPE_CONTENT_DEL = "log.content.delete";// 内容删除
    public static final String OPERTYPE_CONTENT_RMV = "log.content.remove";// 内容销毁
    public static final String OPERTYPE_CONTENT_SYNC = "log.content.sync";// 内容同步
    public static final String OPERTYPE_CONTENT_ONLINE = "log.content.online";// 内容上线
    public static final String OPERTYPE_CONTENT_OFFLINE = "log.content.offline";// 内容下线
    public static final String OPERTYPE_CONTENT_RECYCLE = "log.content.recycle";// 内容送入回收站
    public static final String OPERTYPE_CONTENT_COAUDIT = "log.content.commitaudit";// 内容提审
    public static final String OPERTYPE_CONTENT_PUB = "log.content.publish";// 内容发布
    public static final String OPERTYPE_SUBCONTENT_ADD = "log.subcontent.add";// 子内容新增
    public static final String OPERTYPE_SUBCONTENT_MOD = "log.subcontent.modify";// 子内容修改
    public static final String OPERTYPE_SUBCONTENT_DEL = "log.subcontent.delete";// 子内容删除
    public static final String OPERTYPE_CONTENT_BATCHIMPORT = "log.content.batchimport";// 内容批量导入
    public static final String OPERTYPE_UNKNOWN = "log.unknown";
    public static final String OPERTYPE_CONTENT_SOURCEFILE_ADD = "log.content.sourcefile.add";// 母片新增
    public static final String OPERTYPE_CONTENT_SOURCEFILE_DELETE = "log.content.sourcefile.delete";// 内容新增

    public static final String OPERTYPE_CONTENT_BATCHOFFLINE_TASK_DEL = "log.contentbatchoffline.task.del";// 批量下线任务删除
    // 角色管理
    public static final String OPERTYPE_CP_ADD = "log.cp.add";// CP新增
    public static final String OPERTYPE_CP_MOD = "log.cp.modify";// CP修改
    public static final String OPERTYPE_CP_DEL = "log.cp.delete";// CP删除
    public static final String OPERTYPE_CP_BIND = "log.cp.bindLisence";// CP绑定牌照方
    public static final String OPERTYPE_CP_SYNC = "log.cp.sync"; // CP新增同步
    public static final String OPERTYPE_CP_SYNCDELETE = "log.cp.syncdelete"; // CP删除同步
    public static final String OPERTYPE_CP_POUSE = "log.cp.pouse";// cp暂停
    public static final String OPERTYPE_CP_REWORK = "log.cp.rework";// cp恢复
    public static final String OPERTYPE_LISENCE_ADD = "log.lisence.add";// 牌照方新增
    public static final String OPERTYPE_LISENCE_MOD = "log.lisence.modify";// 牌照方修改
    public static final String OPERTYPE_LISENCE_DEL = "log.lisence.delete";// 牌照方删除
    public static final String OPERTYPE_LISENCE_NONAUDIT_SET = "log.lisence.nonaudit.set";// 牌照方免审设置
    public static final String OPERTYPE_LISENCE_NONAUDIT_CANCEL = "log.lisence.nonaudit.cancel";// 牌照方免审设置取消
    public static final String OPERTYPE_OPERATOROPER_NONAUDIT_SET = "log.operatoroper.noaudit.set";// 运营商管理员免审设置
    public static final String OPERTYPE_OPERATOROPER_NONAUDIT_CANCEL = "log.operatoroper.noaudit.cancel";// 运营商管理员设置取消

    public static final String OPERTYPE_CP_NOAUDITONEADD = "log.cp.noauditoneadd";// CP免一审新增
    public static final String OPERTYPE_CP_NOAUDITTWOADD = "log.cp.noaudittwoadd";// CP免二审新增
    public static final String OPERTYPE_CP_NOAUDITADD = "log.cp.noauditadd";// CP免审新增
    public static final String OPERTYPE_CP_PASSADD = "log.cp.passadd";// CP直通新增
    public static final String OPERTYPE_CP_NOAUDITONEDEL = "log.cp.noauditonedel";// CP免一审删除
    public static final String OPERTYPE_CP_NOAUDITTWODEL = "log.cp.noaudittwodel";// CP免二审删除
    public static final String OPERTYPE_CP_NOAUDITDEL = "log.cp.noauditdel";// CP免审删除
    public static final String OPERTYPE_CP_PASSDEL = "log.cp.passdel";// CP直通删除
    /**
     * opertypeinfo 操作类型（String)
     */
    public static final String OPERTYPE_CONTENT_ADD_INFO = "log.content.add.info";
    public static final String OPERTYPE_CONTENT_MOD_INFO = "log.content.modify.info";
    public static final String OPERTYPE_CONTENT_DEL_INFO = "log.content.delete.info";
    public static final String OPERTYPE_CONTENT_RMV_INFO = "log.content.remove.info";
    public static final String OPERTYPE_CONTENT_SYNC_INFO = "log.content.sync.info";
    public static final String OPERTYPE_CONTENT_ONLINE_INFO = "log.content.online.info";
    public static final String OPERTYPE_CONTENT_OFFLINE_INFO = "log.content.offline.info";
    public static final String OPERTYPE_CONTENT_RECYCLE_INFO = "log.content.recycle.info";
    public static final String OPERTYPE_CONTENT_PUB_INFO = "log.content.publish.info";
    public static final String OPERTYPE_CONTENT_COAUDIT_INFO = "log.content.commitaudit.info";
    public static final String OPERTYPE_SUBCONTENT_ADD_INFO = "log.subcontent.add.info";
    public static final String OPERTYPE_SUBCONTENT_MOD_INFO = "log.subcontent.modify.info";
    public static final String OPERTYPE_SUBCONTENT_DEL_INFO = "log.subcontent.delete.info";
    public static final String OPERTYPE_CONTENT_BATCHIMPORT_INFO = "log.content.batchimport.info";
    public static final String OPERTYPE_CONTENT_SOURCEFILE_ADD_INFO = "log.content.sourcefile.add.info";// 母片新增
    public static final String OPERTYPE_CONTENT_SOURCEFILE_DELETE_INFO = "log.content.sourcefile.delete.info";// 内容新增

    public static final String OPERTYPE_CONTENT_BATCH_OFFLINE_TASK_DEL_INFO = "log.contentbatchonffline.task.del.info";// 内容批量下线任务删除信息

    // 角色管理
    public static final String OPERTYPE_CP_ADD_INFO = "log.cp.add.info";// CP新增
    public static final String OPERTYPE_CP_MOD_INFO = "log.cp.modify.info";// CP修改
    public static final String OPERTYPE_CP_DEL_INFO = "log.cp.delete.info";// CP删除
    public static final String OPERTYPE_CP_BIND_INFO = "log.cp.bindLisence.info";// CP绑定牌照方
    public static final String OPERTYPE_CP_SYNC_INFO = "log.cp.sync.info";// CP新增同步
    public static final String OPERTYPE_CP_SYNCDELETE_INFO = "log.cp.syncdelete.info";// CP取消同步
    public static final String OPERTYPE_CP_POUSE_INFO = "log.cp.pouse.info";// CP暂停
    public static final String OPERTYPE_CP_REWORK_INFO = "log.cp.rework.info";// CP恢复
    public static final String OPERTYPE_LISENCE_ADD_INFO = "log.lisence.add.info";// 牌照方新增
    public static final String OPERTYPE_LISENCE_MOD_INFO = "log.lisence.modify.info";// 牌照方修改
    public static final String OPERTYPE_LISENCE_DEL_INFO = "log.lisence.delete.info";// 牌照方删除
    public static final String OPERTYPE_LISENCE_NONAUDIT_SET_INFO = "log.lisence.nonaudit.set.info";// 牌照方免审设置
    public static final String OPERTYPE_LISENCE_NONAUDIT_CANCEL_INFO = "log.lisence.nonaudit.cancel.info";// 牌照方免审设置取消
    public static final String OPERTYPE_OPERATOROPER_NONAUDIT_SET_INFO = "log.operatoroper.nonaudit.set.info";// 运营商管理员免审设置
    public static final String OPERTYPE_OPERATOROPER_NONAUDIT_CANCEL_INFO = "log.operatoroper.nonaudit.cancel.info";// 运营商管理员设置取消

    public static final String OPERTYPE_CP_NOAUDITONEADD_INFO = "log.cp.noauditoneadd.info";// CP免一审新增
    public static final String OPERTYPE_CP_NOAUDITTWOADD_INFO = "log.cp.noaudittwoadd.info";// CP免二审新增
    public static final String OPERTYPE_CP_NOAUDITADD_INFO = "log.cp.noauditadd.info";// CP免审新增
    public static final String OPERTYPE_CP_PASSADD_INFO = "log.cp.passadd.info";// CP直通新增
    public static final String OPERTYPE_CP_NOAUDITONEDEL_INFO = "log.cp.noauditonedel.info";// CP免一审删除
    public static final String OPERTYPE_CP_NOAUDITTWODEL_INFO = "log.cp.noaudittwodel.info";// CP免二审删除
    public static final String OPERTYPE_CP_NOAUDITDEL_INFO = "log.cp.noauditdel.info";// CP免审删除
    public static final String OPERTYPE_CP_PASSDEL_INFO = "log.cp.passdel.info";// CP直通删除
    // 服务管理
    // 操作类型
    public static final String OPERTYPE_SERVICE_ADD = "log.service.add";// 服务新增
    public static final String OPERTYPE_SERVICE_DEL = "log.service.del";// 服务删除
    public static final String OPERTYPE_CMSFRAME_DEL = "log.cmsframe.del";// 内容元数据模板删除
    public static final String OPERTYPE_CMSFRAME_EFFECT = "log.cmsframe.effect";// 内容元数据模板生效
    public static final String OPERTYPE_CMSFRAME_UNEFFECT = "log.cmsframe.uneffect";// 内容元数据模板失效

    public static final String OPERTYPE_CMSFRAME_ADD = "log.cmsframe.add";// 内容元数据模板新增
    public static final String OPERTYPE_CMSFRAME_UPDATE = "log.cmsframe.update";// 内容元数据模板修改

    public static final String OPERTYPE_SERVICE_UPDATE = "log.service.update";// 服务修改
    public static final String OPERTYPE_SERVICE_PUBLISH_DEL = "log.service.publish.del";// 服务取消发布
    public static final String OPERTYPE_SERVICE_PUBLISH = "log.service.publish";// 服务发布
    public static final String OPERTYPE_SERVICE_BING_PUBLISH = "log.servicemap.publish";// 服务关联关系发布
    public static final String OPERTYPE_SERVICE_BING_PUBLISH_DEL = "log.servicemap.publish.del";// 服务关联关系取消发布
    public static final String OPERTYPE_SERVICE_BIND_CNT = "log.service.bind.cnt";// 内容绑定
    public static final String OPERTYPE_SERVICE_BIND_PROGRAM = "log.service.bind.program";// VOD内容绑定
    public static final String OPERTYPE_SERVICE_BIND_CHANNEL = "log.service.bind.channel";// 直播内容绑定
    public static final String OPERTYPE_SERVICE_BIND_SERIES = "log.service.bind.series";// 连续剧内容绑定
    public static final String OPERTYPE_SERVICE_BIND_DEL_CNT = "log.service.bind.del.cnt";// 内容绑定删除
    public static final String OPERTYPE_SERVICE_BIND_DEL_PROGRAM = "log.service.bind.del.program";// VOD内容绑定删除
    public static final String OPERTYPE_SERVICE_BIND_DEL_CHANNEL = "log.service.bind.del.channel";// 直播内容绑定删除
    public static final String OPERTYPE_SERVICE_BIND_DEL_SERIES = "log.service.bind.del.series";// 连续剧内容绑定删除
    public static final String OPERTYPE_SERVICE_CNT_BIND_PUBLISH = "log.service.cnt.bind.publish";// 服务内容绑定关系发布
    public static final String OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL = "log.service.cnt.bind.publish.del";// 服务内容绑定关系取消发布
    public static final String OPERTYPE_SERVICE_CATE_MAP = "log.service.cate.map"; //服务栏目关联*
    public static final String OPERTYPE_SERVICE_CATE_MAP_DEL = "log.service.cate.map.del"; //服务栏目关联关系删除*

    public static final String OPERTYPE_SERVICE_ADD_PLATFORM ="log.service.bind.add.platform";//服务关联平台
    public static final String OPERTYPE_SERVICE_ADD_TARGETSYSTEM ="log.service.bind.add.targetsystem";//服务关联网元
    public static final String OPERTYPE_SERVICE_DEL_PLATFORM ="log.service.bind.del.platform";//服务删除关联平台
    public static final String OPERTYPE_SERVICE_DEL_TARGETSYSTEM ="log.service.bind.del.targetsystem";//服务删除关联网元
    public static final String OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM ="log.service.bind.content.add.platform";//服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM ="log.service.bind.content.add.targetsystem";//服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM ="log.service.bind.content.del.platform";//删除服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM ="log.service.bind.content.del.targetsystem";//删除服务绑定内容关联平台
    
    // 操作详情
    public static final String OPERTYPE_SERVICE_ADD_INFO = "log.service.add.info";// 服务新增的详情
    public static final String OPERTYPE_SERVICE_DEL_INFO = "log.service.del.info";// 服务删除的详情
    public static final String MGTTYPE_CMSFRAME = "log.cmsframe.mgt"; // 内容元数据自定义模板管理
    public static final String OPERTYPE_CMSFRAME_DEL_INFO = "log.cmsframe.del.info";// 内容元数据模板删除的详情
    public static final String OPERTYPE_CMSFRAME_ADD_INFO = "log.cmsframe.add.info";// 内容元数据模板新增详情
    public static final String OPERTYPE_CMSFRAME_UPDATE_INFO = "log.cmsframe.update.info";// 内容元数据模板修改详情
    public static final String OPERTYPE_CMSFRAME_EFFECT_INFO = "log.cmsframe.effect.info";// 内容元数据模板生效详情
    public static final String OPERTYPE_CMSFRAME_UNEFFECT_INFO = "log.cmsframe.uneffect.info";// 内容元数据模板失效详情
    public static final String OPERTYPE_SERVICE_UPDATE_INFO = "log.service.update.info";// 服务修改的详情
    public static final String OPERTYPE_SERVICE_PUBLISH_INFO = "log.service.publish.info";// 服务发布详情   
    public static final String OPERTYPE_SERVICE_PUBLISH_DEL_INFO = "log.service.publish.del.info";// 服务取消发布详情
    public static final String OPERTYPE_SERVICE_PUBLISH_MAP_INFO = "log.service.publish.map.info";// 服务关联关系发布详情   
    public static final String OPERTYPE_SERVICE_PUBLISH_DEL_MAP_INFO = "log.service.publish.del.map.info";// 服务关联关系取消发布详情
    public static final String OPERTYPE_SERVICE_BIND_CNT_INFO = "log.service.bind.cnt.info";// 内容绑定详情
    public static final String OPERTYPE_SERVICE_BIND_PROGRAM_INFO = "log.service.bind.program.info";// VOD内容绑定详情
    public static final String OPERTYPE_SERVICE_BIND_CHANNEL_INFO = "log.service.bind.channel.info";// 直播内容绑定详情
    public static final String OPERTYPE_SERVICE_BIND_SERIES_INFO = "log.service.bind.series.info";// 连续剧内容绑定详情
    public static final String OPERTYPE_SERVICE_BIND_DEL_CNT_INFO = "log.service.bind.del.cnt.info";// 内容绑定删除详情
    public static final String OPERTYPE_SERVICE_BIND_DEL_PROGRAM_INFO = "log.service.bind.del.program.info";// VOD内容绑定删除详情
    public static final String OPERTYPE_SERVICE_BIND_DEL_CHANNEL_INFO = "log.service.bind.del.channel.info";// 直播内容绑定删除详情
    public static final String OPERTYPE_SERVICE_BIND_DEL_SERIES_INFO = "log.service.bind.del.series.info";// 连续剧内容绑定删除详情
    public static final String OPERTYPE_SERVICE_CNT_BIND_PUBLISH_INFO = "log.service.cnt.bind.publish.info";// 服务内容绑定关系发布详情
    public static final String OPERTYPE_SERVICE_CNT_BIND_PUBLISH_DEL_INFO = "log.service.cnt.bind.publish.del.info";// 服务内容绑定关系取消发布详情
    public static final String OPERTYPE_SERVICE_CATE_MAP_INFO = "log.service.cate.map.info"; //服务栏目关联详情*
    public static final String OPERTYPE_SERVICE_CATE_MAP_DEL_INFO = "log.service.cate.map.del.info"; //服务栏目关联关系删除详情*
    public static final String OPERTYPE_SERVICE_ADD_PLATFORM_INF ="log.service.bind.add.platform.inf";//服务关联平台
    public static final String OPERTYPE_SERVICE_ADD_TARGETSYSTEM_INF ="log.service.bind.add.targetsystem.inf";//服务关联网元
    public static final String OPERTYPE_SERVICE_DEL_PLATFORM_INF ="log.service.bind.del.platform.inf";//服务删除关联平台
    public static final String OPERTYPE_SERVICE_DEL_TARGETSYSTEM_INF ="log.service.bind.del.targetsystem.inf";//服务删除关联网元
    public static final String OPERTYPE_SERVICE_BINDCONTENT_ADD_PLATFORM_INF ="log.service.bind.content.add.platform.inf";//服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_ADD_TARGETSYSTEM_INF ="log.service.bind.content.add.targetsystem.inf";//服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_DEL_PLATFORM_INF ="log.service.bind.content.del.platform.inf";//删除服务绑定内容关联平台
    public static final String OPERTYPE_SERVICE_BINDCONTENT_DEL_TARGETSYSTEM_INF ="log.service.bind.content.del.targetsystem.inf";//删除服务绑定内容关联平台
    // 内容操作类型
    public static final String OPERTYPE_PROGRAM_ADD = "log.program.add";
    public static final String OPERTYPE_PROGRAM_MODIFY = "log.program.modify";
    public static final String OPERTYPE_PROGRAM_DELETE = "log.program.delete";
    public static final String OPERTYPE_PROGRAM_PUBLISH = "log.program.publish";
    public static final String OPERTYPE_PROGRAM_ModifyPUB= "log.program.publish.modify";
    public static final String OPERTYPE_PROGRAM_PUBLISH_DEL = "log.program.publish.del";
    public static final String OPERTYPE_PROGRAM_PREOFFLINE = "log.program.preoffline";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_ADD = "log.program.schedualrecord.add";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_DEL = "log.program.schedualrecord.del";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_PUBLISH = "log.program.schedualrecord.publish";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_DELPUBLISH = "log.program.schedualrecord.delPublish";

    public static final String OPERTYPE_PROGRAMCATEGORY_PUBLISH = "log.programcategory.publish";
    public static final String OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL = "log.programcategory.publish.del";
    public static final String OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH = "log.programcastrolemap.publish";
    public static final String OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL = "log.programcastrolemap.publish.del";
    public static final String OPERTYPE_PROGRAMPICTUREMAP_PUBLISH = "log.programpicturemap.publish";
    public static final String OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL = "log.programpicturemap.publish.del";    
    public static final String OPERTYPE_CATEGORYPICTUREMAP_PUBLISH = "log.categorypicturemap.publish";
    public static final String OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL = "log.categorypicturemap.publish.del"; 
    public static final String OPERTYPE_CHANNELPICTUREMAP_PUBLISH = "log.channelpicturemap.publish";
    public static final String OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL = "log.channelpicturemap.publish.del";     
    public static final String OPERTYPE_SERIESPICTUREMAP_PUBLISH = "log.seriespicturemap.publish";
    public static final String OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL = "log.seriespicturemap.publish.del"; 
    public static final String OPERTYPE_SERIESPROGRAMMAP_PUBLISH = "log.seriesprogrammap.publish";
    public static final String OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL = "log.seriesprogrammap.publish.del"; 
    // 内容操作详情
    public static final String OPERTYPE_PROGRAM_ADD_INFO = "log.program.add.info";
    public static final String OPERTYPE_PROGRAM_MODIFY_INFO = "log.program.modify.info";
    public static final String OPERTYPE_PROGRAM_DELETE_INFO = "log.program.delete.info";
    public static final String OPERTYPE_PROGRAM_PUBLISH_INFO = "log.program.publish.info";
    public static final String OPERTYPE_PROGRAM_ModifyPUB_INFO = "log.program.modifyPublish.info";
    
    public static final String OPERTYPE_PROGRAM_PUBLISH_DEL_INFO = "log.program.publish.del.info";
    public static final String OPERTYPE_PROGRAM_PREOFFLINE_INFO = "log.program.preoffline.info";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_ADD_INFO = "log.program.schedualrecord.add.info";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_DEL_INFO = "log.program.schedualrecord.del.info";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_PUBLISH_INFO = "log.program.schedualrecord.publish.info";
    public static final String OPERTYPE_PROGRAM_SCHEDUALRECORD_DELPUBLISH_INFO = "log.program.schedualrecord.delPublish.info";
    public static final String OPERTYPE_PROGRAMCATEGORY_PUBLISH_INFO = "log.programcategory.publish.info";
    public static final String OPERTYPE_PROGRAMCATEGORY_PUBLISH_DEL_INFO = "log.programcategory.publish.del.info";
    public static final String OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_INFO = "log.programcastrolemap.publish.info";
    public static final String OPERTYPE_PROGRAMCASTROLEMAP_PUBLISH_DEL_INFO = "log.programcastrolemap.publish.del.info";
    public static final String OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_INFO = "log.programpicturemap.publish.info";
    public static final String OPERTYPE_PROGRAMPICTUREMAP_PUBLISH_DEL_INFO = "log.programpicturemap.publish.del.info";
    public static final String OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_INFO = "log.categorypicturemap.publish.info";
    public static final String OPERTYPE_CATEGORYPICTUREMAP_PUBLISH_DEL_INFO = "log.categorypicturemap.publish.del.info";
    public static final String OPERTYPE_CHANNELPICTUREMAP_PUBLISH_INFO = "log.channelpicturemap.publish.info";
    public static final String OPERTYPE_CHANNELPICTUREMAP_PUBLISH_DEL_INFO = "log.channelpicturemap.publish.del.info";
    public static final String OPERTYPE_SERIESPICTUREMAP_PUBLISH_INFO = "log.seriespicturemap.publish.info";
    public static final String OPERTYPE_SERIESPICTUREMAP_PUBLISH_DEL_INFO = "log.seriespicturemap.publish.del.info";
    public static final String OPERTYPE_SERIESPROGRAMMAP_PUBLISH_INFO = "log.seriesprogrammap.publish.info";
    public static final String OPERTYPE_SERIESPROGRAMMAP_PUBLISH_DEL_INFO = "log.seriesprogrammap.publish.del.info";
    // 子内容操作类型
    public static final String OPERTYPE_MOVIE_ADD = "log.movie.add";
    public static final String OPERTYPE_MOVIE_MODIFY = "log.movie.modify";
    public static final String OPERTYPE_MOVIE_DELETE = "log.movie.delete";

    // 子内容操作详情
    public static final String OPERTYPE_MOVIE_ADD_INFO = "log.movie.add.info";
    public static final String OPERTYPE_MOVIE_MODIFY_INFO = "log.movie.modify.info";
    public static final String OPERTYPE_MOVIE_DELETE_INFO = "log.movie.delete.info";
    
    //同步内容监控
    public static final String MGTTYPE_TASKSYN= "log.tasksyn.mgt"; //内容同步任务监控
    public static final String OPERTYPE_TASK_RERUN = "log.task.rerun";// 任务监控
    public static final String OPERTYPE_TASK_RERUN_INFO = "log.task.rerun.info";// 服务修改的详情
    

    /**
     * optresut (int) 操作结果 成功-true，失败-false,部分成功-partrialsuccess
     */
    public static final int OPTRESUT_SUCCESS = 0;
    public static final int OPTRESUT_FALSE = 1;
    public static final int OPTRESUT_PARTIALSUCCESS = 2;
}

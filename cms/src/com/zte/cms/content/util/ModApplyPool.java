package com.zte.cms.content.util;

import java.util.Vector;

import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;

public class ModApplyPool {
    // 日志记录器
    private Log mLog = SSBBus.getLog(ModApplyPool.class);;

    private static byte[] lockByte = new byte[0];

    // 唯一实例
    private volatile static ModApplyPool instance;

    /** 共享存储池*/
    private static Vector<String> applyPool = null;

    /**
     * 返回唯一实例.如果是第一次调用此方法,则创建实例
     * 
     * @return SharedPool 唯一实例
     */
    public static ModApplyPool getInstance()
    {
        if (instance != null)
        {
            return instance;
        }
        else
        {
            synchronized (lockByte)
            {
                if (instance == null)
                {
                    instance = new ModApplyPool();
                }
                return instance;
            }
        }
    }

    /**
     * 构造函数
     * 
     */
    private ModApplyPool()
    {
        if (applyPool == null)
        {
            applyPool = new Vector<String>();
        }
    }


    /**
     * 判断共享池中是否存在当前对象
     */
    public boolean contains(String contentid)
    {
        synchronized (instance)
        {
            if (applyPool.contains(contentid))
            {
                mLog.error("Object has existed: contentid=" + contentid + ", content count=" + applyPool.size());
                return true;
            }
            else
            {
                applyPool.addElement(contentid);
                mLog.error("Add content: contentid=" + contentid + ", content count=" + applyPool.size());
                return false;
            }
            
        }        
    }

    /**
     * 移除共享存储池中的内容
     * 
     * @param contentid 内容编码
     * @throws Exception
     */
    public void remove(String contentid)
    {
        try
        {
            synchronized (instance)
            {
                if (applyPool.contains(contentid))
                {
                    // 移除共享存储池中的对象
                    applyPool.removeElement(contentid);                
                    mLog.error("Remove content: contentid=" + contentid + ", content count=" + applyPool.size());
                }
            }
        }
        catch (Exception ex)
        {
            mLog.error("Error occurred in remove() method", ex);
        }
    }
}

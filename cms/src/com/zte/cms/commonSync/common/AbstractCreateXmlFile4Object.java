package com.zte.cms.commonSync.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import com.zte.cms.common.GlobalConstants;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public abstract class AbstractCreateXmlFile4Object
{
    protected  String tempAreaPath;
    protected  String mountPoint;
    protected  String ftpAddressPrefix;
    protected final static String CNTSYNCXML = "xmlsync";
    
    public abstract String createXmlFile4WG18(Map<String, List> objectMap, int targetType, int syncType)throws DomainServiceException;

    public abstract String createXmlFile4YS26(Map<String, List> objectMap, int targetType, int syncType)throws DomainServiceException;
    
    public abstract String createXmlFile4IPTV30(Map<String, List> objectMap, int targetType, int syncType)throws DomainServiceException;
    
    /**
     * 初始化数据：临时区目录和挂载点
     * @throws Exception
     */
    protected void init() throws Exception
    {
        this.tempAreaPath = GlobalConstants.getTempAreAdd();
        this.mountPoint = GlobalConstants.getMountPoint(); 
        this.ftpAddressPrefix = GlobalConstants.getFtpAddressPrefix();
    }
    
    /**
     * 获取时间 如"20111002"
     */
    protected static String getCurrentTime()
    {
       SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
       Date date = new Date();
       String currentTime = "";
       currentTime = sdf.format(date);
       return currentTime;
    }
    
    /**
     * 写入节点属性
     * 
     * @param objectElement 父节点
     * @param propertyElement 子节点
     * @param key 节点名称
     * @param value 节点值
     */
    protected void writeElement(Element objectElement, Element propertyElement, String key, Object value, Boolean convert)
    {
        propertyElement = objectElement.addElement("Property");
        propertyElement.addAttribute("Name", key);
        if (value == null)
        {
            propertyElement.addText("");
        }
        else
        {
            if (convert)
            {
                propertyElement.addCDATA(String.valueOf(value));
            }
            else
            {
                propertyElement.addText(String.valueOf(value));
            }
        }
    }
    
    protected void writeDom(Document dom,String fileFullName)
    {
        XMLWriter writer = null;
        try
        {
            writer = new XMLWriter(new FileOutputStream(fileFullName));
            writer.write(dom);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}

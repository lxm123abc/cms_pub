package com.zte.cms.service.wkfw.service;

import java.util.List;

import com.zte.cms.service.wkfw.dao.IServiceWkfwTaskForMeDAO;
import com.zte.cms.service.wkfw.model.ServiceWkfwCondition;
import com.zte.cms.service.wkfw.model.ServiceWkfwTaskForMe;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.umap.common.EspecialCharMgt;

public class ServiceWkfwTaskForMeDS implements IServiceWkfwTaskForMeDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IServiceWkfwTaskForMeDAO serviceWkfwTaskForMedao = null;

    public void setServiceWkfwTaskForMedao(IServiceWkfwTaskForMeDAO serviceWkfwTaskForMedao)
    {
        this.serviceWkfwTaskForMedao = serviceWkfwTaskForMedao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#queryStrategyWkfwTaskForMeListCntByCond(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition)
     */
    public int queryServiceWkfwTaskForMeListCntByCond(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
            throws DomainServiceException
    {
        log.debug("query serviceWkfwTaskForMeList count by condition starting...");
        int totalCnt = 0;
        try
        {
            convertQueryParams(serviceWkfwTaskForMeCondition);
            totalCnt = serviceWkfwTaskForMedao.queryserviceWkfwTaskForMeListCntByCond(serviceWkfwTaskForMeCondition);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("query serviceWkfwTaskForMeList count by condition end");
        return totalCnt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get serviceWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(serviceWkfwTaskForMeCondition);
            pageInfo = serviceWkfwTaskForMedao.pageInfoQuery(serviceWkfwTaskForMeCondition, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get serviceWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.zte.cms.strategy.wkfw.common.service.IStrategyWkfwTaskForMeDS#pageInfoQuery(com.zte.cms.strategy.wkfw.common.model.StrategyWkfwCondition,
     *      int, int, com.zte.ssb.framework.base.util.PageUtilEntity)
     */
    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(ServiceWkfwCondition serviceWkfwTaskForMeCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get serviceWkfwTaskForMeList page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            convertQueryParams(serviceWkfwTaskForMeCondition);
            pageInfo = serviceWkfwTaskForMedao.pageInfoQuery(serviceWkfwTaskForMeCondition, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ServiceWkfwTaskForMe>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get strategyWkfwTaskForMeList page info by condition end");
        return tableInfo;
    }

    private void convertQueryParams(ServiceWkfwCondition serviceWkfwTaskForMeCondition)
    {
        serviceWkfwTaskForMeCondition.setServiceid(EspecialCharMgt.conversion(serviceWkfwTaskForMeCondition
                .getServiceid()));

        serviceWkfwTaskForMeCondition.setServicename(EspecialCharMgt.conversion(serviceWkfwTaskForMeCondition
                .getServicename()));
    }
}

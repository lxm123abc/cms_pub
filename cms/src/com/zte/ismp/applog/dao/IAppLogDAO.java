package com.zte.ismp.applog.dao;

import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.umap.ssb.logmanager.business.model.VSsblog;

public interface IAppLogDAO
{
    /**
     * 根据条件分页查询VSsblog对象，作为查询条件的参数
     * 
     * @param vSsblog VSsblog对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize) throws DAOException;

    /**
     * 根据条件分页查询VSsblog对象，作为查询条件的参数
     * 
     * @param vSsblog VSsblog对象
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DAOException dao异常
     */
    public PageInfo pageInfoQuery(VSsblog vSsblog, int start, int pageSize, PageUtilEntity puEntity)
            throws DAOException;

}
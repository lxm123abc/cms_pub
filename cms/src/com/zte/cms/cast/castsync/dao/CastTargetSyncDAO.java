package com.zte.cms.cast.castsync.dao;

import java.util.List;

import com.zte.cms.cast.castsync.model.CastTargetSync;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.dynamicobj.DynamicObjectBaseDao;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;

public class CastTargetSyncDAO extends DynamicObjectBaseDao implements ICastTargetSyncDAO
{ 
 // 日志
    private Log log = SSBBus.getLog(getClass());
    
    public PageInfo pageInfoQuery( CastTargetSync castTargetSync, int start, int pageSize)throws DAOException
    {
        log.debug("page query castTargetSync by condition starting...");
        PageInfo pageInfo = null;
        int totalCnt = ((Integer) super.queryForObject("queryCastTargetSynListTotalByCond",  castTargetSync)).intValue();
        if( totalCnt > 0 )
        {
            int fetchSize = pageSize > (totalCnt - start)? (totalCnt - start) : pageSize;
            List<ProgramTargetSyn> rsList = (List<ProgramTargetSyn>)super.pageQuery( "queryCastTargetSynListByCond" ,  castTargetSync , start , fetchSize );
            pageInfo = new PageInfo( start, totalCnt, fetchSize, rsList );
        }
        else
        {
            pageInfo = new PageInfo();
        }
        log.debug("page query castTargetSync by condition end");
        return pageInfo;
    }
}

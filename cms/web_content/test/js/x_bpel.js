
var jecallback;
var jenv;
var jarg0 = {};
var jarg1 = {};
var jarg2 = {};
var jarg3 = {};
var jarg4 = {};
var jarg5 = {};
var jarg6 = {};
var jarg7 = {};
var jarg8 = {};
var jarg9 = {};
var jresult = {};

var imgb = "images/blank.gif";
var imgp = "images/breakpoint.gif";

function beginDeclare(pclass) {
	clearQueryHTML();
	buildQueryHTMLByDeclare(pclass);
	buildDeclareButton(pclass);
	jecallback = undefined;
}

function getDeclareFromQueryCond(pclass) {
	var obj = {};
	obj.pdeclare = $("#queryBody").find("input[id='pdeclare']").val();
	obj.pclass = pclass;
	obj.pexpress = $("#queryBody").find("input[id='pexpress']").val();
	var args = {};
	args.equation = "";
	$("#queryBody").find("input[id='pname']").each(function(){
		var val = $(this).val();
		var pval = $(this).parent().parent().find("input[id='pvalue']").val();
		if (isEmpty(val) || is_numcode(val)) return false;
		args.temptemp = getJsonVal(pval);
		eval("args." + val + "=args.temptemp");
	});
	if (pclass != "variable") args.equation = undefined;
	if (args.temptemp != undefined) args.temptemp = undefined;
	obj.pparam = args;
	return obj;
}

function getJTrObj(obj) {
	var trobj = {};
	$(obj).find("label[id^='lbl']").each(function(){
		var tid = "p" + $(this).attr("id").substring(3);
		trobj.temptemp = getJsonVal($(this).text());
		eval("trobj." + tid + "=trobj.temptemp");
	});
	if (trobj.temptemp != undefined) trobj.temptemp = undefined;
	return trobj;
}

function getJTrObjs(isdebug) {
	var trobjs = [];
	var i = 0;
	$("#xtable").contents().find("input[id='trchk']").each(function(){
		var trobj = {};
		if (isdebug) {
			var img = $(this).parent().parent().find("td").eq(2).find("img").attr("src");
			trobj.breakpoint = img.endWith(imgp);
		}
		$(this).parent().parent().find("label[id^='lbl']").each(function(){
			var tid = "p" + $(this).attr("id").substring(3);
			trobj.temptemp = getJsonVal($(this).text());
			eval("trobj." + tid + "=trobj.temptemp");
		});
		if (trobj.temptemp != undefined) trobj.temptemp = undefined;
		trobjs[i++] = trobj;
	});
	return trobjs;
}

function doJTrNo() {
	$("#xtable").contents().find("table tr").each(function(i){
		$(this).find("td").eq(0).append(i);
	});
}

function doJBreakPoint(obj) {
	var img = $(obj).find("img");
	if (img.attr("src") == imgp) {
		img.attr("src", imgb);
	}
	else {
		img.attr("src", imgp);
	}
}

function doJMoveUp(obj) {
	var tr = obj.parentNode.parentNode;
	if ($(tr).prev().is("tr")) {
		var trobj = getJTrObj(tr);
		$(tr).prev().before(buildTableTRHTMLByDeclare(trobj));
		$(tr).remove();
	}
	//doJTrNo();
}

function doJMoveDown(obj) {
	var tr = obj.parentNode.parentNode;
	if ($(tr).next().is("tr"))	 {
		var trobj = getJTrObj(tr);
		$(tr).next().after(buildTableTRHTMLByDeclare(trobj));
		$(tr).remove();
	}
	//doJTrNo();
}

function doJInsert(pclass) {
	var obj = getDeclareFromQueryCond(pclass);
	var isJi = false;
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		$(this).parent().parent().after(buildTableTRHTMLByDeclare(obj));
		isJi = true;
	});
	if (isJi) return;
	$("#xtable").contents().find("table tr:last").each(function(){
		$(this).after(buildTableTRHTMLByDeclare(obj));
		isJi = true;
	});
	if (isJi) return;
	buildTableHTMLByDeclare(new Array(obj));
	//doJTrNo();
}

function doJEditCallback(pclass) {
	if (jecallback == undefined) {
		alert("对象已经失效，请重新选择待修改项");
		return;
	}
	var obj = getDeclareFromQueryCond(pclass);
	$(jecallback).find("label[id^='lbl']").each(function(){
		var tid = "p" + $(this).attr("id").substring(3);
		$(this).text(createJsonStr(obj[tid]));
	});
}

function doJEdit() {
	jecallback = undefined;
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		jecallback = $(this).parent().parent();
		return false;
	});
	if (jecallback == undefined) {
		return;
	}
	var trobj = getJTrObj(jecallback);
	clearQueryHTML();
	buildQueryHTMLByDeclare(trobj.pclass, trobj.pdeclare, createJsonStr(trobj.pexpress));
	buildDeclareButton(trobj.pclass, true);
	var args = trobj.pparam;
	for (var arg in args) {
		newQueryDeclare(true, true, false, arg, createJsonStr(args[arg]));
	}
}

function doJGC() {
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		$(this).parent().parent().remove();
	});
	//doJTrNo();
}

function doJCopy() {
	$("#xtable").contents().find("input[id='trchk'][checked]").each(function(){
		var trobj = getJTrObj($(this).parent().parent());
		$("#xtable").contents().find("table tr:last").after(buildTableTRHTMLByDeclare(trobj));
	});
	//doJTrNo();
}

function referJURL(refers, rfstr, url, iscompile) {
	var rkey, rval;
	if (typeof(rfstr) != "object") {
		if (isEmpty(rfstr) || rfstr.indexOf("->") == -1) {
			return rfstr;
		}
		if (iscompile) {
			rkey = rfstr.substring(0, rfstr.indexOf("->"));
			if (isEmpty(rkey)) {
				return rfstr;
			}
			if (refers[rkey].equation == "->") {
				return "->";
			}
		}
		url = url + " : " + rfstr;
		rfstr = rfstr.replace("->this", "").replace("->", ".");
		try {
			eval("rval=refers." + rfstr);
		} 
		catch (ex) {
			rval = undefined;
		}
		if (rval === undefined) {
			if (iscompile) {
				buildQueryHTMLByJOut(url + " is invalid reference, please change.");
			}
			else {
				buildQueryHTMLByJOut(url + " reference not found, run abort.");
			}
		}
		return rval;
	}
	if (rfstr instanceof Array) {
		if (rfstr.length == 0) {
			return rfstr;
		}
		for (var i=0; i<rfstr.length; i++) {
			rfstr[i] = referJURL(refers, rfstr[i], url+"["+i+"]", iscompile);
			if (rfstr[i] === undefined) {
				return rfstr[i];
			}
		}
		return rfstr;
	}
	for (var rf in rfstr) {
		rfstr[rf] = referJURL(refers, rfstr[rf], url+"."+rf, iscompile);
		if (rfstr[rf] === undefined) {
			return rfstr[rf];
		}
	}
	return rfstr;
}

function doJCompile() {
	var refers = {};
	var trobjs = getJTrObjs();
	var trobj = {};
	clearQueryHTML();
	for (var i=0; i<trobjs.length; i++) {
		trobj = trobjs[i];
		trobj.pparam = referJURL(refers, trobj.pparam, "("+trobj.pdeclare+")", true);
		if (trobj.pparam === undefined) {
			return;
		}
		if (trobj.pclass == "variable") {
			var obj = trobj.pparam.equation;
			if (obj.equation !== undefined) {
				for (var o in obj) {
					trobj.pparam[o] = obj[o];
				}
			}
			refers[trobj.pdeclare] = trobj.pparam;
			buildQueryHTMLByJOut(trobj.pdeclare + "=" + createJsonStr(refers[trobj.pdeclare]));
		}
		if (trobj.pclass == "invoke") {
			var j = trobj.pexpress.indexOf("->this");
			if (j == -1) continue;
			refers[trobj.pexpress.substring(0, j)] = getJsonVal("{\"equation\":\"->\"}");
			buildQueryHTMLByJOut(trobj.pexpress.substring(0, j) + "={\"equation\":\"->\"}");
		}
	}
	buildQueryHTMLByJOut("compile finish without error.<a href=\"#\" onclick=\"clearQueryHTML()\"> clear all </a>");
}

function doJExpress(pdeclare, pexpress, pparam) {
	var rtn;
	try {
		if (pexpress != undefined) {
			rtn = eval(pparam.equation + pexpress + pparam.compareValue);
		}
		else {
			rtn = eval(pparam.equation);
		}
	}
	catch (ex) {
		rtn = false;
	}
	if (!rtn) {
		buildQueryHTMLByJOut(pdeclare + " express not match, run abort.");
	}
	return rtn;
}

function doJCall(pdeclare, pparam) {
	var params = "";
	var obj = {};
	for (var i=0; i<10; i++) {
		obj = pparam["arg" + i];
		if (typeof(obj) == "undefined") break;
		if (typeof(obj) == "string" && obj == "[]") obj = [];
		if (typeof(obj) == "string" && obj == "nvl") obj = null;
		eval("jarg" + i + "=obj");
		params += ",jarg" + i;
	}
	params = params.substr(1);
	jresult = {};
	if (pdeclare.endWith(".ssp")) {
		buildQueryHTMLByJOut("start call classProxyDS/expressionClassProxy.ssm...");
		buildQueryHTMLByJOut("params=" + pdeclare + "," + params);
		callUrl("classProxyDS/expressionClassProxy.ssm", '[' + pdeclare + "," + params + ']', 'jresult', true);
		buildQueryHTMLByJOut("end call classProxyDS/expressionClassProxy.ssm...");
	}
	else {
		buildQueryHTMLByJOut("start call " + pdeclare + "...");
		buildQueryHTMLByJOut("params=" + params);
		callUrl(pdeclare, isEmpty(params) ? null : '[' + params + ']', 'jresult', true);
		buildQueryHTMLByJOut("end call " + pdeclare + "...");
	}
	if (isEmpty(jresult) || isNull(jresult.rtnValue)) return null;
	return jresult.rtnValue;
}

function doJRun() {
	var refers = {};
	var trobjs = getJTrObjs();
	var trobj = {};
	clearQueryHTML();
	for (var i=0; i<trobjs.length; i++) {
		trobj = trobjs[i];
		trobj.pparam = referJURL(refers, trobj.pparam, "("+trobj.pdeclare+")", false);
		if (trobj.pparam === undefined) {
			return;
		}
		if (trobj.pclass == "variable") {
			if (trobj.pdeclare == "statement" && !doJExpress(trobj.pdeclare, trobj.pexpress, trobj.pparam)) {
				return;
			}
			var obj = trobj.pparam.equation;
			if (obj.equation !== undefined) {
				for (var o in obj) {
					trobj.pparam[o] = obj[o];
				}
			}
			trobj.pparam.equation = undefined;
			refers[trobj.pdeclare] = trobj.pparam;
			buildQueryHTMLByJOut(trobj.pdeclare + "=" + createJsonStr(refers[trobj.pdeclare]));
		}
		if (trobj.pclass == "invoke") {
			var rtn = doJCall(trobj.pdeclare, trobj.pparam);
			var j = trobj.pexpress.indexOf("->this");
			if (j == -1) continue;
			refers[trobj.pexpress.substring(0, j)] = rtn;
			buildQueryHTMLByJOut(trobj.pexpress.substring(0, j) + "=" + createJsonStr(rtn, true));
		}
	}
	buildQueryHTMLByJOut("run finish without error.");
}

function doJDebug() {
	var refers = {};
	var point = 0;
	if (jenv != undefined && jenv.refers != undefined) {
		refers = jenv.refers;
		point = jenv.point;
	}
	var trobjs = getJTrObjs(true);
	var trobj = {};
	if (point == 0) {
		clearQueryHTML();
	}
	for (var i=point; i<trobjs.length; i++) {
		trobj = trobjs[i];
		trobj.pparam = referJURL(refers, trobj.pparam, "("+trobj.pdeclare+")", false);
		if (trobj.pparam === undefined) {
			doJStop();
			return;
		}
		if (trobj.pclass == "variable") {
			if (trobj.pdeclare == "statement" && !doJExpress(trobj.pdeclare, trobj.pexpress, trobj.pparam)) {
				doJStop();
				return;
			}
			var obj = trobj.pparam.equation;
			if (obj.equation !== undefined) {
				for (var o in obj) {
					trobj.pparam[o] = obj[o];
				}
			}
			trobj.pparam.equation = undefined;
			refers[trobj.pdeclare] = trobj.pparam;
			buildQueryHTMLByJOut(trobj.pdeclare + "=" + createJsonStr(refers[trobj.pdeclare]));
		}
		if (trobj.pclass == "invoke") {
			var rtn = doJCall(trobj.pdeclare, trobj.pparam);
			var j = trobj.pexpress.indexOf("->this");
			if (j == -1) continue;
			refers[trobj.pexpress.substring(0, j)] = rtn;
			buildQueryHTMLByJOut(trobj.pexpress.substring(0, j) + "=" + createJsonStr(rtn, true));
		}
		if (trobj.breakpoint) {
			jenv = {};
			jenv.refers = refers;
			jenv.point = i+1;
			return;
		}
	}
	buildQueryHTMLByJOut("run finish without error.");
}

function doJStop() {
	jenv = undefined;
	$("#xtable").contents().find("table tr").each(function(){
		$(this).find("td").eq(2).find("img").attr("src", imgb);
	});
}

function saveBpelObject() {
	var trobjs = getJTrObjs();
	$("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val(createJsonStr(trobjs));
}

function readBpelObject() {
	var trobjs = getJsonVal($("#itemName", parent.frames["xtree"].document).find("input[id='txtItemJsonStr']").val());
	buildTableHTMLByDeclare(trobjs);
	//doJTrNo();
}
package com.zte.cms.content.relation.ls;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.zte.cms.common.log.CommonLogConstant;
import com.zte.cms.common.log.CommonLogUtil;
import com.zte.cms.content.relation.model.CmsContentRelation;
import com.zte.cms.content.relation.model.CmsContentRelationInfo;
import com.zte.cms.content.relation.service.ICmsContentRelationDS;
import com.zte.cms.cpsp.common.CmsLpopNoauditConstants;
import com.zte.ismp.common.ResourceManager;
import com.zte.ismp.common.ReturnInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.servicecontainer.business.server.ISession;
import com.zte.ssb.servicecontainer.business.server.RIAContext;
import com.zte.umap.common.EspecialCharMgt;
import com.zte.umap.common.LoginMgt;
import com.zte.umap.common.ResourceMgt;
import com.zte.umap.common.model.OperInfo;
import com.zte.umap.terminal.ls.UhandsetConstant;

public class CmsContentRelationLS implements ICmsContentRelationLS
{

    private Log log = SSBBus.getLog(getClass());

    ICmsContentRelationDS ds = null;

    public ICmsContentRelationDS getDs()
    {
        return ds;
    }

    public void setDs(ICmsContentRelationDS ds)
    {
        this.ds = ds;
    }

    /**
     * 校验该条记录是否存在
     * 
     * @param cgid
     * @return
     */
    public String queryValidContent(String cgid) throws Exception
    {
        log.debug("validContent method starts");
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        cmsContentRelation.setCgid(cgid);
        // 校验该条记录是否存在
        List<CmsContentRelation> rtn = new ArrayList<CmsContentRelation>();
        try
        {
            rtn = ds.getCmsContentRelationByCond(cmsContentRelation);
        }
        catch (DomainServiceException e)
        {
            // TODO Auto-generated catch block
            log.error(e);
        }
        if (rtn.size() == 0)
        {//"1:该条记录已不存在";
            return ResourceMgt.findDefaultText("cnt.thisrecord.not.exist");
        }
        log.debug("validContent method ends");
        return null;

    }

    /**
     * 查询内容关联
     */
    public TableDataInfo pageInfoQueryRelation(CmsContentRelation cmsContentRelation, int start, int pageSize)
            throws Exception
    {
        log.debug("pageInfoQuery method starts");
        TableDataInfo pageInfo = new TableDataInfo();
        try
        {
            if (cmsContentRelation != null)
            {
                cmsContentRelation.setCgname(EspecialCharMgt.conversion(cmsContentRelation.getCgname()));
                if (cmsContentRelation.getCgid() != null && cmsContentRelation.getCgid() != "")
                {
                    cmsContentRelation.setCgid(EspecialCharMgt.conversion(cmsContentRelation.getCgid()));
                }
            }
            
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            cmsContentRelation.setOperid(operinfo.getOperid());
            pageInfo = ds.pageInfoQuery2(cmsContentRelation, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("Error occurred in pageInfoQuery method", e);
        }
        log.debug("pageInfoQuery method ends");
        return pageInfo;
    }

    /**
     * UCDN版本里面重写电信的内容关联的DS
     * 
     * @author shaoyan
     * @param info 传入后台的信息
     * 
     */
    public TableDataInfo queryUnSelectedContent(CmsContentRelationInfo info, int start, int pageSize) throws Exception
    {
        log.debug("queryUnSelectedContent method starts");
        TableDataInfo pageInfo = new TableDataInfo();
        try
        {
            if (info != null)
            {
                if (info.getNamecn() != null && info.getNamecn().trim() != "")
                {
                    info.setNamecn(EspecialCharMgt.conversion(info.getNamecn()));
                }
                if (info.getContentid() != null && info.getContentid().trim() != "")
                {
                    info.setContentid(EspecialCharMgt.conversion(info.getContentid()));
                }
            }
            OperInfo operinfo = LoginMgt.getOperInfo("CMS");
            info.setOperid(operinfo.getOperid());
            pageInfo = ds.queryUnSelectedContent(info, start, pageSize);
        }
        catch (Exception e)
        {
            log.error("Error occurred in queryUnSelectedContent method", e);
        }
        log.debug("queryUnSelectedContent method ends");
        return pageInfo;

    }

    /**
     * 查询一个内容关联已经关联的内容
     */
    public List<CmsContentRelation> querySelectedContentList(String cgid) throws Exception
    {
        log.debug("querySelectedContentList method starts");
        List<CmsContentRelation> list = null;
        try
        {
            if (cgid == null)
            {
                return null;
            }
            CmsContentRelation cmsContentRelation = new CmsContentRelation();
            cmsContentRelation.setCgid(cgid);
            list = ds.querySelectedContentList(cmsContentRelation);
        }
        catch (Exception e)
        {
            log.error("Error occurred in querySelectedContent method", e);
        }
        log.debug("querySelectedContentList method ends");
        return list;
    }

    /**
     * 根据CGID获取该关联的内容组合编码
     */
    public List<CmsContentRelation> getContentIdByCgid(String cgid) throws Exception
    {
        log.debug("getContentIdByCgid method starts");
        List<CmsContentRelation> relations = new ArrayList<CmsContentRelation>();
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        try
        {
            cmsContentRelation.setCgid(cgid);
            relations = ds.getCmsContentRelationByCond(cmsContentRelation);
        }
        catch (Exception e)
        {
            log.error("Error occurred in getContentIdByCgid method", e);
        }
        log.debug("getContentIdByCgid method ends");
        return relations;
    }

    /**
     * 根据结果集查询内容记录
     * 
     * @param condition String类型，包含内容的编号，用分号隔开
     * @param start 开始页数
     * @param pagesize 每页记录个数
     * @return 返回查询结果
     * @throws Exception
     */
    public TableDataInfo pageInfoQueryforStr(String condition, int start, int pagesize) throws Exception
    {
        log.debug("page info query start..............");
        TableDataInfo dataInfo = new TableDataInfo();

        try
        {
            if (condition != null && !condition.equals(""))
            {
                String ids = condition;
                if (condition.endsWith(";"))
                {
                    int last = condition.lastIndexOf(";");
                    if (last > 0)
                    {
                        ids = condition.substring(0, last);
                    }
                }
                // 拼装sql语句，根据code集合查询出频道信息列表
                ids = "'" + ids.replaceAll(";", "', '") + "'";
                String sql = "select contentid,namecn from zxdbm_cms.v_content_basicinfo where contentid in (" + ids
                        + ")";
                String sqlcount = "select count(1) from zxdbm_cms.v_content_basicinfo where contentid in (" + ids + ")";
                dataInfo = ds.pageInfoQuery(sql, sqlcount, start, pagesize);
            }

        }
        catch (Exception e)
        {
            log.error("pageInfoQueryforStr exception:" + e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("page info query end");
        return dataInfo;
    }

    /**
     * 根据结果集查询内容记录
     * 
     * @param condition String类型，包含内容的编号，用分号隔开
     * @return 返回查询结果
     * @throws Exception
     */
    public List<CmsContentRelationInfo> pageInfoQueryList(String[] condition) throws Exception
    {
        log.debug("pageInfoQueryList method starts");
        List<CmsContentRelationInfo> rsList = new ArrayList<CmsContentRelationInfo>();

        try
        {
            if (condition[0] != null && !condition[0].equals("") && !condition[0].equals(";"))
            {
                String ids = condition[0];
                if (condition[0].endsWith(";"))
                {
                    int last = condition[0].lastIndexOf(";");
                    if (last > 0)
                    {
                        ids = condition[0].substring(0, last);
                    }
                }
                // 拼装sql语句，根据code集合查询出频道信息列表
                ids = "'" + ids.replaceAll(";", "', '") + "'";
                String sql = "select t.programid as contentid,namecn from zxdbm_cms.cms_program t where programid in ("
                        + ids + ")";
                String sqlcount = "select count(1) from zxdbm_cms.cms_program where programid in (" + ids + ")";
                rsList = ds.pageInfoQueryList(sql, sqlcount);
            }

        }
        catch (Exception e)
        {
            log.error("pageInfoQueryList exception:" + e);
        }
        log.debug("pageInfoQueryList method end");
        return rsList;
    }

    /**
     * 新增一个内容关联
     * 
     * @param cgid 内容关联标识
     * @param selectList 所关联内容的内容编码组合
     * @return 返回操作结果
     * @exception Exception
     */
    public String insertRelationContent(String cgid, String cgname, String description, String selectList)
            throws Exception
    {
        log.debug("insert relation content start..............");
        ReturnInfo returninfo = new ReturnInfo("0", "0");
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        try
        {
            cmsContentRelation.setCgid(cgid);
            // 校验CGID是否重复
            List<CmsContentRelation> rtn = new ArrayList<CmsContentRelation>();
            rtn = ds.getCmsContentRelationByCond(cmsContentRelation);
            if (rtn.size() != 0)
            {
                return ResourceMgt.findDefaultText("cnt.relate.identify.exist"); // 内容关联标识已存在
            }

            // 校验名称是否重复
            CmsContentRelation cmsCntRelaCheckName = new CmsContentRelation();
            cmsCntRelaCheckName.setCgname(cgname);
            List<CmsContentRelation> rtnName = ds.getCmsContentRelationByCond(cmsCntRelaCheckName);
            if (rtnName.size() != 0)
            {
                return ResourceMgt.findDefaultText("cnt.relate.name.exist"); // 内容关联标识(名称)已存在
            }

            String ids = selectList;
            if (selectList.endsWith(";"))
            {
                int last = selectList.lastIndexOf(";");
                if (last > 0)
                {
                    ids = selectList.substring(0, last);
                }
                else
                {
                    return returninfo.toString();
                }
            }
            if (selectList.substring(0, 1).equals(";"))
            {
                ids = ids.substring(1, ids.length());
            }
            List<CmsContentRelation> relList = new ArrayList<CmsContentRelation>();
            String[] contentidList = null;
            contentidList = ids.split(";");
            if (contentidList.length != 0)
            {
                for (int i = 0; i < contentidList.length; i++)
                {
                    CmsContentRelation rel = new CmsContentRelation();
                    rel.setContentid(contentidList[i]);
                    rel.setCgname(cgname);
                    rel.setDescription(description);
                    rel.setCgid(cgid);
                    relList.add(rel);
                }
            }
            else
            {
                return returninfo.toString();
            }
            ds.insertCmsContentRelationList(relList);

            // 保存操作日志
            CommonLogUtil.insertOperatorLog(cgid, "log.contentrelation.mgt", "log.contentrelation.add",
                    "log.contentrelation.add.info", CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            log.error("insert relation content exception:" + e);
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("insert relation content end");
        //"0:操作成功"
        return ResourceMgt.findDefaultText("cnt.do.success");
    }

    /**
     * 更新一个内容关联中所关联的内容组合
     * 
     * @param cgid 内容关联标识
     * @param selectList 所关联内容的内容编码组合
     * @return String 返回操作结果
     * @exception Exception
     */
    public String updateRelationContent(String cgid, String cgname, String selectList) throws Exception
    {
        log.debug("update relation content start..............");
        // ReturnInfo returninfo = new ReturnInfo("0","0");
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        try
        {
            cmsContentRelation.setCgid(cgid);
            // 校验该条记录是否存在
            List<CmsContentRelation> rtn = new ArrayList<CmsContentRelation>();
            rtn = ds.getCmsContentRelationByCond(cmsContentRelation);
            if (rtn.size() == 0)
            {
                //return "1:该条记录已不存在";
                return ResourceMgt.findDefaultText("cnt.thisrecord.not.exist");

            }
            ds.removeCmsContentRelation(cmsContentRelation);

            String ids = selectList;
            if (selectList.endsWith(";"))
            {
                int last = selectList.lastIndexOf(";");
                ids = selectList.substring(0, last);
            }
            if (selectList.substring(0, 1).equals(";"))
            {
                ids = ids.substring(1, ids.length());
            }
            List<CmsContentRelation> relList = new ArrayList<CmsContentRelation>();
            String[] contentidList = null;
            contentidList = ids.split(";");
            if (contentidList.length != 0)
            {
                for (int i = 0; i < contentidList.length; i++)
                {
                    CmsContentRelation rel = new CmsContentRelation();
                    rel.setContentid(contentidList[i]);
                    rel.setCgid(cgid);
                    rel.setCgname(cgname);
                    relList.add(rel);
                }
            }
            else
            {//"1:系统错误"
                return ResourceMgt.findDefaultText("cnt.system.wrong");
            }
            ds.insertCmsContentRelationList(relList);

            // 保存操作日志
            CommonLogUtil.insertOperatorLog(cgid, "log.contentrelation.mgt", "log.contentrelation.modify",
                    "log.contentrelation.modify.info", CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

        }
        catch (Exception e)
        {
            log.error("insert relation content exception:" + e);
            throw new Exception(ResourceManager.getResourceText(e));
        }

        log.debug("update relation content end");
        //"0:操作成功"
        return ResourceMgt.findDefaultText("cnt.do.success");
    }

    /**
     * 内容删除时，从内容关联中删除该内容
     * 
     * @param contentid 内容ID
     * @return String 返回操作结果
     * @exception Exception
     */
    public String deleteRelationContentByContentId(String contentid) throws Exception
    {
        log.debug("delete relation content start..............");
        ReturnInfo returninfo = new ReturnInfo("0", "0");
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        try
        {
            cmsContentRelation.setContentid(contentid);

            ds.removeCmsContentRelation(cmsContentRelation);
            returninfo.setFlag("0");
            returninfo.setReturnMessage("380003"); // 删除成功
        }
        catch (Exception e)
        {
            log.error("delete relation content exception:" + e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("delete relation content end");
        return returninfo.toString();
    }

    /**
     * 根据CGID删除内容关联记录
     * 
     * @param cgid 内容关联标识
     * @return String 返回操作结果
     * @exception Exception
     */
    public String deleteRelationContentByCgId(String cgid) throws Exception
    {
        log.debug("delete relation content start..............");
        ReturnInfo returninfo = new ReturnInfo("0", "0");
        CmsContentRelation cmsContentRelation = new CmsContentRelation();
        try
        {
            cmsContentRelation.setCgid(cgid);

            // 校验该条记录是否存在
            List<CmsContentRelation> rtn = new ArrayList<CmsContentRelation>();
            rtn = ds.getCmsContentRelationByCond(cmsContentRelation);
            if (rtn.size() == 0)
            {
                returninfo.setFlag("1");
                returninfo.setReturnMessage("380002");
                return returninfo.toString();
            }

            ds.removeCmsContentRelation(cmsContentRelation);

            // 保存操作日志
            CommonLogUtil.insertOperatorLog(cgid, "log.contentrelation.mgt", "log.contentrelation.delete",
                    "log.contentrelation.delete.info", CommonLogConstant.RESOURCE_OPERATION_SUCCESS);

            returninfo.setFlag("0");
            returninfo.setReturnMessage("380003"); // 删除成功
        }
        catch (Exception e)
        {
            log.error("delete relation content exception:" + e);
            throw new Exception(ResourceManager.getResourceText(e));
        }
        log.debug("delete relation content end");
        return returninfo.toString();
    }

}

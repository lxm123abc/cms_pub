package com.zte.cms.batchobjectrecord.service;

import java.util.List;
import com.zte.cms.batchobjectrecord.model.BatchObjectRecord;
import com.zte.cms.batchobjectrecord.dao.IBatchObjectRecordDAO;
import com.zte.cms.batchobjectrecord.service.IBatchObjectRecordDS;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.SSBBus;
import com.zte.umap.common.dynamicobj.DynamicObjectBaseDS;

public class BatchObjectRecordDS extends DynamicObjectBaseDS implements IBatchObjectRecordDS
{
    // 日志
    private Log log = SSBBus.getLog(getClass());

    private IBatchObjectRecordDAO dao = null;

    public void setDao(IBatchObjectRecordDAO dao)
    {
        this.dao = dao;
    }

    public void insertBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("insert batchObjectRecord starting...");
        try
        {
            Long recordindex = (Long) getPrimaryKeyGenerator().getPrimarykey("batch_object_record");
            batchObjectRecord.setRecordindex(recordindex);
            dao.insertBatchObjectRecord(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("insert batchObjectRecord end");
    }

    public void updateBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("update batchObjectRecord by pk starting...");
        try
        {
            dao.updateBatchObjectRecord(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update batchObjectRecord by pk end");
    }

    public void updateBatchObjectRecordList(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException
    {
        log.debug("update batchObjectRecordList by pk starting...");
        if (null == batchObjectRecordList || batchObjectRecordList.size() == 0)
        {
            log.debug("there is no datas in batchObjectRecordList");
            return;
        }
        try
        {
            for (BatchObjectRecord batchObjectRecord : batchObjectRecordList)
            {
                dao.updateBatchObjectRecord(batchObjectRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update batchObjectRecordList by pk end");
    }

    public void updateBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("update batchObjectRecord by condition starting...");
        try
        {
            dao.updateBatchObjectRecordByCond(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update batchObjectRecord by condition end");
    }

    public void updateBatchObjectRecordListByCond(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException
    {
        log.debug("update batchObjectRecordList by condition starting...");
        if (null == batchObjectRecordList || batchObjectRecordList.size() == 0)
        {
            log.debug("there is no datas in batchObjectRecordList");
            return;
        }
        try
        {
            for (BatchObjectRecord batchObjectRecord : batchObjectRecordList)
            {
                dao.updateBatchObjectRecordByCond(batchObjectRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("update batchObjectRecordList by condition end");
    }

    public void removeBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("remove batchObjectRecord by pk starting...");
        try
        {
            dao.deleteBatchObjectRecord(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove batchObjectRecord by pk end");
    }

    public void removeBatchObjectRecordList(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException
    {
        log.debug("remove batchObjectRecordList by pk starting...");
        if (null == batchObjectRecordList || batchObjectRecordList.size() == 0)
        {
            log.debug("there is no datas in batchObjectRecordList");
            return;
        }
        try
        {
            for (BatchObjectRecord batchObjectRecord : batchObjectRecordList)
            {
                dao.deleteBatchObjectRecord(batchObjectRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove batchObjectRecordList by pk end");
    }

    public void removeBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("remove batchObjectRecord by condition starting...");
        try
        {
            dao.deleteBatchObjectRecordByCond(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove batchObjectRecord by condition end");
    }

    public void removeBatchObjectRecordListByCond(List<BatchObjectRecord> batchObjectRecordList)
            throws DomainServiceException
    {
        log.debug("remove batchObjectRecordList by condition starting...");
        if (null == batchObjectRecordList || batchObjectRecordList.size() == 0)
        {
            log.debug("there is no datas in batchObjectRecordList");
            return;
        }
        try
        {
            for (BatchObjectRecord batchObjectRecord : batchObjectRecordList)
            {
                dao.deleteBatchObjectRecordByCond(batchObjectRecord);
            }
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("remove batchObjectRecordList by condition end");
    }

    public BatchObjectRecord getBatchObjectRecord(BatchObjectRecord batchObjectRecord) throws DomainServiceException
    {
        log.debug("get batchObjectRecord by pk starting...");
        BatchObjectRecord rsObj = null;
        try
        {
            rsObj = dao.getBatchObjectRecord(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get batchObjectRecordList by pk end");
        return rsObj;
    }

    public List<BatchObjectRecord> getBatchObjectRecordByCond(BatchObjectRecord batchObjectRecord)
            throws DomainServiceException
    {
        log.debug("get batchObjectRecord by condition starting...");
        List<BatchObjectRecord> rsList = null;
        try
        {
            rsList = dao.getBatchObjectRecordByCond(batchObjectRecord);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        log.debug("get batchObjectRecord by condition end");
        return rsList;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize)
            throws DomainServiceException
    {
        log.debug("get batchObjectRecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(batchObjectRecord, start, pageSize);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<BatchObjectRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get batchObjectRecord page info by condition end");
        return tableInfo;
    }

    @SuppressWarnings("unchecked")
    public TableDataInfo pageInfoQuery(BatchObjectRecord batchObjectRecord, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException
    {
        log.debug("get batchObjectRecord page info by condition starting...");
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQuery(batchObjectRecord, start, pageSize, puEntity);
        }
        catch (DAOException daoEx)
        {
            log.error("dao exception:", daoEx);
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException(daoEx);
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<BatchObjectRecord>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug("get batchObjectRecord page info by condition end");
        return tableInfo;
    }
}

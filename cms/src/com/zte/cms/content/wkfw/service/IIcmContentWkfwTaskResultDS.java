package com.zte.cms.content.wkfw.service;

import com.zte.cms.content.wkfw.model.IcmContentWkfwCondition;

import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

/**
 * 提供了用于业务能力待处理工单查询的一套接口
 * 
 * @author 高小芹 gao.xiaoqin@zte.com.cn
 * @version ZXMCISMP-UMAPV2.01.01
 */
public interface IIcmContentWkfwTaskResultDS
{

    /**
     * <p>
     * 根据条件查询待我处理的工单数量
     * </p>
     * 
     * @param uappablWkfwCondition UappablWkfwCondition对象，查询条件
     * @return 查询结果
     * @throws DomainServiceException抛出异常
     * 
     */
    public long getMyNeedHandleWorkflowItemListCount(IcmContentWkfwCondition contentWkfwCondition)
            throws DomainServiceException;

    /**
     * <p>
     * 业务能力待处理工单查询接口
     * </p>
     * 根据条件分页查询UappablWkfwTaskResult对象
     * 
     * @param uappablWkfwTaskConditon 类型UappablWkfwCondition对象，作为查询条件的参数
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @return 查询结果
     * @throws DomainServiceException ds异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwTaskResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize)
            throws DomainServiceException;

    /**
     * <p>
     * 业务能力待处理工单查询接口
     * </p>
     * 根据条件分页查询UappablWkfwTaskResult对象
     * 
     * @param uappablWkfwTaskConditon 类型UappablWkfwCondition对象，作为查询条件的参数
     * @param start 类型int，起始行
     * @param pageSize 类型int，页面大小
     * @param puEntity 排序对象
     * @return 查询结果
     * @throws DomainServiceException ds异常
     * @see com.zte.umap.appabl.wkfw.common.model.UappablWkfwTaskResult
     * @since ZXMCISMP-UMAPV2.01.01
     */
    public TableDataInfo pageInfoQuery(IcmContentWkfwCondition contentWkfwCondition, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
}

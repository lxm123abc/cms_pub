package com.zte.cms.content.program.programsync.service;

import java.util.List;

import com.zte.cms.content.program.programsync.dao.IProgramTargetSynDAO;
import com.zte.cms.content.program.programsync.model.ProgramTargetSyn;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.SSBBus;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.common.log.Log;
import com.zte.ssb.framework.exception.exceptions.DAOException;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public class ProgramTargetSynDS implements IProgramTargetSynDS
{
	// 日志
	private Log log = SSBBus.getLog(getClass());
	
  	private IProgramTargetSynDAO dao = null;
	
	public void setDao(IProgramTargetSynDAO dao)
	{
	    this.dao = dao;
	}



	public ProgramTargetSyn getProgramTargetSyn( ProgramTargetSyn programTargetSyn )throws DomainServiceException
	{
		log.debug( "get programTargetSyn by pk starting..." );
		ProgramTargetSyn rsObj = null;
		try
		{
			rsObj = dao.getProgramTargetSyn( programTargetSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get cntTargetsysSynList by pk end" );
		return rsObj;
	}

	
	public List<ProgramTargetSyn> getProgramTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DomainServiceException
	{
		log.debug( "get programTargetSyn by condition starting..." );
		List<ProgramTargetSyn> rsList = null;
		try
		{
			rsList = dao.getProgramTargetSynByCond( programTargetSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programTargetSyn by condition end" );
		return rsList;
	}
	public List<ProgramTargetSyn> getProTargetSynByCond( ProgramTargetSyn programTargetSyn )throws DomainServiceException
	{
		log.debug( "get programTargetSyn by condition starting..." );
		List<ProgramTargetSyn> rsList = null;
		try
		{
			rsList = dao.getProTargetSynByCond( programTargetSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programTargetSyn by condition end" );
		return rsList;
	}
	
	public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV3( ProgramTargetSyn programTargetSyn )throws DomainServiceException
	{
		log.debug( "get programTargetSyn by condition starting..." );
		List<ProgramTargetSyn> rsList = null;
		try
		{
			rsList = dao.getProgramTargetSynByCondIPTV3( programTargetSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programTargetSyn by condition end" );
		return rsList;
	}	
	
	public List<ProgramTargetSyn> getProgramTargetSynByCondIPTV2( ProgramTargetSyn programTargetSyn )throws DomainServiceException
	{
		log.debug( "get programTargetSyn by condition starting..." );
		List<ProgramTargetSyn> rsList = null;
		try
		{
			rsList = dao.getProgramTargetSynByCondIPTV2( programTargetSyn );
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		log.debug( "get programTargetSyn by condition end" );
		return rsList;
	}	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DomainServiceException
	{
		log.debug( "get programTargetSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programTargetSyn, start, pageSize);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramTargetSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programTargetSyn page info by condition end" );
		return tableInfo;
	}
	
	@SuppressWarnings("unchecked")
	public TableDataInfo pageInfoQuery(ProgramTargetSyn programTargetSyn, int start, int pageSize, PageUtilEntity puEntity)throws DomainServiceException
	{
		log.debug( "get programTargetSyn page info by condition starting..." );
		PageInfo pageInfo = null;
		try
		{
			pageInfo = dao.pageInfoQuery(programTargetSyn, start, pageSize, puEntity);
		}
		catch( DAOException daoEx )
		{
			log.error( "dao exception:", daoEx );
			// TODO 根据实际应用，可以在此处添加异常国际化处理
			throw new DomainServiceException( daoEx );
		}
		TableDataInfo tableInfo = new TableDataInfo();
		tableInfo.setData((List<ProgramTargetSyn>) pageInfo.getResult());
		tableInfo.setTotalCount((int) pageInfo.getTotalCount());
		log.debug( "get programTargetSyn page info by condition end" );
		return tableInfo;
	}
	
	public TableDataInfo pageInfoQueryProgramTargetSynByCond(ProgramTargetSyn programTargetSyn, int start, int pageSize)throws DomainServiceException
    {
        log.debug( "get programTargetSyn page info by condition starting..." );
        PageInfo pageInfo = null;
        try
        {
            pageInfo = dao.pageInfoQueryProgramTargetSynByCond(programTargetSyn, start, pageSize);
        }
        catch( DAOException daoEx )
        {
            log.error( "dao exception:", daoEx );
            // TODO 根据实际应用，可以在此处添加异常国际化处理
            throw new DomainServiceException( daoEx );
        }
        TableDataInfo tableInfo = new TableDataInfo();
        tableInfo.setData((List<ProgramTargetSyn>) pageInfo.getResult());
        tableInfo.setTotalCount((int) pageInfo.getTotalCount());
        log.debug( "get programTargetSyn page info by condition end" );
        return tableInfo;
    }
}

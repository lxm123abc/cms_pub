package com.zte.cms.operCnttypeMap.ls;

import java.util.List;

import com.zte.cms.operCnttypeMap.model.OperCnttypemap;

public interface IOperCnttypemapLS
{
    public String saveOperCnttypeMap(List<OperCnttypemap> list, Long operid);

    public List<OperCnttypemap> getOperCnttypemapByCond(OperCnttypemap operCnttypemap);
}

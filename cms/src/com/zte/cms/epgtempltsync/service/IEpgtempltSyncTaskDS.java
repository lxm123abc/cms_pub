package com.zte.cms.epgtempltsync.service;

import java.util.List;

import com.zte.cms.epgtempltsync.model.EpgtempltSyncTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTask;
import com.zte.cms.epgtempltsync.model.UpstreamEPGTaskDetail;
import com.zte.ssb.framework.base.util.PageInfo;
import com.zte.ssb.exportExcel.tableModel.TableDataInfo;
import com.zte.ssb.framework.base.util.PageUtilEntity;
import com.zte.ssb.framework.exception.exceptions.DomainServiceException;

public interface IEpgtempltSyncTaskDS
{
    /**
     * 新增EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void insertEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException;

    /**
     * 更新EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void updateEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException;

    /**
     * 批量更新EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void updateEpgtempltSyncTaskList(List<EpgtempltSyncTask> epgtempltSyncTaskList)
            throws DomainServiceException;

    public String checkExist(Long templateindex) throws DomainServiceException;

    /**
     * 删除EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void removeEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException;

    /**
     * 批量删除EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public void removeEpgtempltSyncTaskList(List<EpgtempltSyncTask> epgtempltSyncTaskList)
            throws DomainServiceException;

    /**
     * 查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @return EpgtempltSyncTask对象
     * @throws DomainServiceException ds异常
     */
    public EpgtempltSyncTask getEpgtempltSyncTask(EpgtempltSyncTask epgtempltSyncTask) throws DomainServiceException;

    /**
     * 根据条件查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象
     * @return 满足条件的EpgtempltSyncTask对象集
     * @throws DomainServiceException ds异常
     */
    public List<EpgtempltSyncTask> getEpgtempltSyncTaskByCond(EpgtempltSyncTask epgtempltSyncTask)
            throws DomainServiceException;

    /**
     * 根据条件分页查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize)
            throws DomainServiceException;

    /**
     * 根据条件分页查询EpgtempltSyncTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQuery(EpgtempltSyncTask epgtempltSyncTask, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;
    
    /**
     * 根据条件分页查询UpstreamEPGTask对象
     * 
     * @param epgtempltSyncTask EpgtempltSyncTask对象，作为查询条件的参数
     * @param start 起始行
     * @param pageSize 页面大小
     * @param puEntity 排序空置参数@see PageUtilEntity
     * @return 查询结果
     * @throws DomainServiceException ds异常
     */
    public TableDataInfo pageInfoQueryUpstreamEPGTask(UpstreamEPGTask upstreamEPGTask, int start, int pageSize,
            PageUtilEntity puEntity) throws DomainServiceException;

    public UpstreamEPGTaskDetail getUpstreamEPGTaskDetail(UpstreamEPGTaskDetail rsObj) throws DomainServiceException;
}

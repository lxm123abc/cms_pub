package com.zte.ucm.log;

import java.io.PrintStream;

public class ConsoleLoggerOutter extends LoggerOutter
{
  public String getId()
  {
    return ConsoleLoggerOutter.class.getName();
  }

  public void print(int paramInt)
  {
    System.out.print(paramInt);
  }

  public void print(String paramString)
  {
    System.out.print(paramString);
  }

  public void print(Object paramObject)
  {
    System.out.print(paramObject);
  }
}
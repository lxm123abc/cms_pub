function curDateTime(){
    var curDate = new Date();
    var DTAsString = "";
    var y = 0;
    var m = 0;
    var d = 0;
    var h = 0;
    var mi = 0;
    var s = 0;

    y = curDate.getYear();
    m = curDate.getMonth() + 1;
    d = curDate.getDate();

    if(y < 100){
        DTAsString += (1900+y);
    }else{
        DTAsString = y;
    }
    if(m < 10){
        DTAsString += "-0" + m;
    }else{
        DTAsString += "-" + m;
    }
    if(d < 10){
        DTAsString += "-0" +d;
    }else{
        DTAsString += "-" + d;
    }

    DTAsString += " ";
    h = curDate.getHours();
    mi = curDate.getMinutes();
    s = curDate.getSeconds();
    if (h < 10) {
        DTAsString += "0" + h;
    }else{
        DTAsString += h;
    }
    if (mi < 10) {
        DTAsString += ":0" + mi;
    }else{
        DTAsString += ":" + mi;
    }
    if (s < 10) {
        DTAsString += ":0" + s;
    }else{
        DTAsString += ":" + s;
    }
    return DTAsString;
}

function getTime(str)
{
    return str.substr(0,4) + "-" + str.substr(4,2) + "-" + str.substr(6,2) + " " + str.substr(8,2) + ":" + str.substr(10,2) + ":" + str.substr(12,2)
}

function doSelectActor(page, index, name){
    var sFeatures = "dialogHeight: " + 600 + "px;dialogWidth: " + 800 + "px;";
    var array = new Array();
    array = window.showModalDialog(page, "", sFeatures);
    var rtnname = "";
    var rtnindex = "";
    for (i = 0; i < array.length; i++)
    {
        rtnname += array[i].actorname + ",";
        rtnindex += array[i].actorindex + ",";
    }
    var _id = "#" + name;
    $(_id).val(rtnname);
    _id = "#" + index;
    $(_id).val(rtnindex);
}


/**//***********************************
* 简单时间控件： version 1.0
* 作者：李禄燊 
* 时间：2007-10-31
* 
* 使用说明：
* 首先把本控件包含到页面 
* <script src="XXX/setTime.js" type="text/javascript"></script>
* 控件调用函数：_SetTime(field)
* 例如 <input name="time" type="text"   onclick="_SetTime(this)"/>
*
************************************/
var _str_ = "";
document.writeln("<div id=\"_contents\" style=\"padding:3px; background-color:#E3E3E3; font-size: 12px; border: 1px solid #666666;  position:absolute; left:?px; top:?px; width:?px; height:?px; z-index:1; visibility:hidden\">");
_str_ += "\u65f6<select name=\"_hour\" style=\"width:38px\">";
for (h = 0; h <= 9; h++) {
    _str_ += "<option value=\"0" + h + "\">0" + h + "</option>";
}
for (h = 10; h <= 23; h++) {
    _str_ += "<option value=\"" + h + "\">" + h + "</option>";
}
_str_ += "</select> \u5206<select name=\"_minute\" style=\"width:38px\">";
for (m = 0; m <= 9; m++) {
    _str_ += "<option value=\"0" + m + "\">0" + m + "</option>";
}
for (m = 10; m <= 59; m++) {
    _str_ += "<option value=\"" + m + "\">" + m + "</option>";
}
_str_ += "</select> \u79d2<select name=\"_second\" style=\"width:38px\">";
for (s = 0; s <= 9; s++) {
    _str_ += "<option value=\"0" + s + "\">0" + s + "</option>";
}
for (s = 10; s <= 59; s++) {
    _str_ += "<option value=\"" + s + "\">" + s + "</option>";
}
_str_ += "</select><input name=\"queding\" type=\"button\" onclick=\"_select()\" value=\"\u786e\u5b9a\" style=\"font-size:12px\" /></div>";
document.writeln(_str_);
var _fieldname;
function _SetTime(tt) {
    _fieldname = tt;
    var ttop = tt.offsetTop;    //TT控件的定位点高
    var thei = tt.clientHeight;    //TT控件本身的高
    var tleft = tt.offsetLeft;    //TT控件的定位点宽
    while (tt = tt.offsetParent) {
        ttop += tt.offsetTop;
        tleft += tt.offsetLeft;
    }
    document.all._contents.style.top = ttop + thei + 4;
    document.all._contents.style.left = tleft;
    document.all._contents.style.visibility = "visible";
}
function _select() {
    _fieldname.value = document.all._hour.value + ":" + document.all._minute.value + ":" + document.all._second.value;
    document.all._contents.style.visibility = "hidden";
}
function _hide(){
    document.all._contents.style.visibility = "hidden";
}